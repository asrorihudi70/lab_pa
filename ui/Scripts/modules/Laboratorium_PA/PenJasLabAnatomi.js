var CurrentPenJasLab_PA =
{
    data: Object,
    details: Array,
    row: 0
};

var mRecordLABPA = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var mRecordLABPA = Ext.data.Record.create
(
    [
       {name: 'cito', mapping:'cito'},
       {name: 'kd_tarif', mapping:'kd_tarif'},
       {name: 'kp_produk', mapping:'kp_produk'},
       {name: 'deskripsi', mapping:'deskripsi'},
       {name: 'tgl_transaksi', mapping:'tgl_transaksi'},
       {name: 'tgl_berlaku', mapping:'tgl_berlaku'},
       {name: 'harga', mapping:'harga'},
       {name: 'qty', mapping:'qty'},
       {name: 'jumlah', mapping:'jumlah'}
    ]
);
//---------TAMBAH BARU 27-SEPTEMBER-2017
var selectSetGolDarah;
var selectSetJK;
var ID_JENIS_KELAMIN;
var tombol_bayar='enable';
var tmpkd_unit = '';
var tmpkd_unit_tarif_mir='';
var FormLookUpsdetailTRPenjasLABPA;
var tmpkd_unit_asal;
var tmpnama_unit = '';
var KasirLABPADataStoreProduk=new Ext.data.ArrayStore({id: 0,fields:['kd_produk','deskripsi','harga','tglberlaku'],data: []});
var Trkdunit2;
var dsunitlabpa_viPenJasLab_PA;
var dsprinter_kasirlabpa;
var combovaluesunittujuan = "";
var panelActiveDataPasien;
var ds_customer_viPJ_LABPA;
var jeniscus_LABPA;
var kodeCustomerFilterDepan;
var dsDokterRequestEntry;
var KodeNomorLAB_PA='Semua';
var CurrentHistory =
        {
            data: Object,
            details: Array,
            row: 0
        };
var kodeunit;
var FormLookUpsdetailTRKelompokPasien_labpa
var tmp_kodeunitkamar_LABPA;
var tmp_kdspesial_LABPA;
var tmp_nokamar_LABPA;
var tmplunas;
var dsTRDetailHistoryList_labpa;
var kodepasien;
var namapasien;
var namaunit;
var tmpkddokterpengirim;
var kodepay ;
var kdkasir;
var uraianpay;
var vCustomer;
var vCo_status;
var vNoSEP;
var vKd_Pasien;
var vTanggal;
var vKdUnit;
var vKdUnitDulu = "";
var vKdDokter;
var vNamaUnit;
var vNamaDokter;
var vKdKasir;
var vUrutMasuk;
var vNoTransaksi;
var mRecordKasirLABPA = Ext.data.Record.create
        (
                [
                    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
                    {name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
                    {name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
                    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
                    {name: 'HARGA', mapping: 'HARGA'},
                    {name: 'QTY', mapping: 'QTY'},
                    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
                    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
                    {name: 'URUT', mapping: 'URUT'}
                ]
                );

var AddNewKasirLABPAKasir = true;
var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var kdpaytransfer = 'T1';
var kdkasir;
var tanggaltransaksitampung;
var kdkasirasal_labpa;
var notransaksiasal_labpa;
var kdcustomeraa;
var notransaksi;
var vflag;
var tmp_NoTransaksi;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var tampungtypedata;
var jenispay;
var tapungkd_pay;
var tranfertujuan = 'Rawat Inap';
var cellSelecteddeskripsi;
var vkd_unit;
var notransaksi;
var noTransaksiPilihan;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();
var tigaharilalu = new Date().add(Date.DAY, -5);
var nowTglTransaksiGrid = new Date();
var tglGridBawah = nowTglTransaksiGrid.format("d/M/Y");
var gridDTItemTest;

var tmphariini = nowTglTransaksiGrid.format("d/M/Y");
var tmp3harilalu = tigaharilalu.format("d/M/Y");

var selectSetDr;
var selectSetGDarah;
var selectSetJK;
var selectSetKelPasien;
var selectSetPerseorangan;
var selectSetPerusahaan;
var selectSetAsuransi;

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirLABPA='Belum Posting';
var selectCountStatusLunasByr_viKasirLABPA='Belum Lunas';
var selectCountJenTr_viPenJasLab_PA='Transaksi Baru';
var dsTRPenJasLab_PAList;
var dsTRDetailPenJasLab_PAList;
var AddNewPenJasRad = true;
var selectCountPenJasRad = 50;
var TmpNotransaksi='';
var KdKasirAsal='';
var TglTransaksi='';
var databaru = 0;
var No_Kamar='';
var Kd_Spesial=0;
var kodeUnitLab_PA;
var dsPeroranganLab_PARequestEntry;
var dsPeroranganLab_PARequestEntryDepan;

var jenisKelaminPasien;
var rowSelectedPenJasLab_PA;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRLABPA;
var valueStatusCMRWJView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
var kelompokPasien;
var tmpparams = '';
var grListPenJasLab_PA;
//-----------------ABULHASSAN------------20_10_2017------ variable Lis
var namaCustomer_LIS='';
var namaKelompokPasien_LIS='';
//VALIDASI JENIS TRANSAKSI
var combovalues = 'Transaksi Baru';
var radiovaluesLABPA = '1';
var combovaluesunit = "";
var ComboValuesKamar = "";
var dsDokterRequestEntryLABPA;
var FormLookUpsdetailTRGantiDokter_labpa;
/* ------------------------------- END --------------------------- */
var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntryLABPA = new WebApp.DataStore({fields: Field});
var vkode_customer;
CurrentPage.page = getPanelPenJasLab_PA(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
var tmpurut;


//membuat form
function getUnitDefault(opsi){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionLAB/getUnitDefault",
        params: {text:''},
        failure: function (o){
            loadMask.hide();
            ShowPesanErrorPenJasLab_PA('Gagal mendapatkan data Unit default', 'Laboratorium PA');
        },
        success: function (o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                tmpkd_unit=cst.kd_unit;
                tmpnama_unit=cst.nama_unit;
                if (opsi==="tidak"){
                    getComboDokterLab_PA(cst.kd_unit);
                    Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').setValue(tmpnama_unit);
                }
                
            } else{
                ShowPesanErrorPenJasLab_PA('Gagal mendapatkan data Unit default', 'Laboratorium PA');
            }
        }
    });
}
function msg_box_alasanbukatrans_LAB(data)
{
    var lebar = 250;
    form_msg_box_alasanbukatrans_LAB = new Ext.Window
            (
                    {
                        id: 'alasan_bukatransLAb',
                        title: 'Alasan Buka Transaksi',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
												{
													xtype: 'textfield',
													//fieldLabel: 'No. Transaksi ',
													name: 'txtAlasanbukatransLAB',
													id: 'txtAlasanbukatransLAB',
													emptyText: 'Alasan Buka Transaksi',
													anchor: '99%',
												},
                                               //mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Buka',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_bukatransLAb',
                                                                    handler: function ()
                                                                    {
																		if (Ext.getCmp('txtAlasanbukatransLAB').getValue().trim()==='' || Ext.getCmp('txtAlasanbukatransLAB').getValue().trim==='Alasan Buka Transaksi')
																		{
																			ShowPesanWarningPenJasLab_PA("Isi alasan buka transaksi !", "Perhatian");
																		}else{
																			Ext.Ajax.request
																			(
																					{
																						url: baseURL + "index.php/main/functionKasirPenunjang/bukatransaksi",
																						params: {
																							no_transaksi : data.notrans_bukatrans,
																							kd_kasir	 : data.kd_kasir_bukatrans,
																							tgl_transaksi: data.tgl_transaksi_bukatrans,
																							kd_pasien	 : data.kd_pasien_bukatrans,
																							urut_masuk	 : data.urut_masuk_bukatrans,
																							kd_unit 	 : data.kd_unit_bukatrans,
																							keterangan	 : Ext.getCmp('txtAlasanbukatransLAB').getValue(),
																							kd_bagian_shift : 4
																						},
																						success: function (o)
																						{
																							//	RefreshDatahistoribayar_LAB(Kdtransaksi);
																						   // RefreshDataFilterKasirLABKasir();
																							//RefreshDatahistoribayar_LAB('0');
																							var cst = Ext.decode(o.responseText);
																							if (cst.success === true)
																							{
																								ShowPesanInfoPenJasLab_PA("Proses Buka Transaksi Berhasil", nmHeaderHapusData);
																								Ext.getCmp('btnBukaTransaksiLAB').disable();
																								validasiJenisTrLABPA();
																								
																							} else {
																								ShowPesanWarningPenJasLab_PA(nmPesanHapusError, nmHeaderHapusData);
																							}
																							;
																						}
																					}
																			)
																			form_msg_box_alasanbukatrans_LAB.close();
																		}
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_bukatransLAb',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanbukatrans_LAB.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanbukatrans_LAB.show();
}
;
function getPanelPenJasLab_PA(mod_id) 
{
    var Field = ['KD_PASIEN','NO_TRANSAKSI','NAMA','ALAMAT','SPESIALISASI','KELAS','NAMA_KAMAR','KAMAR','MASUK','NO_TRANSAKSI_ASAL','KD_KASIR_ASAL',
                 'DOKTER','KD_DOKTER','KD_UNIT_KAMAR','KD_CUSTOMER','CUSTOMER','TGL_MASUK','URUT_MASUK','TGL_INAP','KD_SPESIAL','KD_KASIR','TGL_TRANSAKSI','KD_UNIT_ASAL','KD_UNIT',
                 'CO_STATUS','KD_USER','TGL_LAHIR','JENIS_KELAMIN','GOL_DARAH','POSTING_TRANSAKSI', 'TELEPON','CUSTOMER',
                 'NAMA_UNIT','POLIKLINIK','NAMA_UNIT_ASLI','KELPASIEN','LUNAS','DOKTER_ASAL','KD_DOKTER_ASAL','URUT','NAMA_UNIT_ASAL','HP','SJP','NO_REGISTER'];
    dsTRPenJasLab_PAList = new WebApp.DataStore({ fields: Field });

    getUnitDefault("awal");
    var k="tr.tgl_transaksi >='"+tmp3harilalu+"' and tr.tgl_transaksi <='"+tmphariini+"' and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc  limit 10";
    //getUnitDefault();
    refeshpenjaslabpa(k);
    grListPenJasLab_PA = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            id:'PenjasLaboratoriumPA',
            store: dsTRPenJasLab_PAList,
            anchor: '100% 50%',
            columnLines: false,
            autoScroll:true,
            border: false,
            sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedPenJasLab_PA = dsTRPenJasLab_PAList.getAt(row);
                            console.log(rowSelectedPenJasLab_PA);
                            vCustomer=rowSelectedPenJasLab_PA.data.CUSTOMER;
                            vNoSEP=rowSelectedPenJasLab_PA.data.SJP;
                            vKdUnit=rowSelectedPenJasLab_PA.data.KD_UNIT;
                            vKdDokter=rowSelectedPenJasLab_PA.data.KD_DOKTER;
                            vUrutMasuk=rowSelectedPenJasLab_PA.data.URUT_MASUK;
                            vTanggal=rowSelectedPenJasLab_PA.data.MASUK;
                            vKd_Pasien=rowSelectedPenJasLab_PA.data.KD_PASIEN;
                            vNamaDokter=rowSelectedPenJasLab_PA.data.DOKTER;
                            vNoTransaksi=rowSelectedPenJasLab_PA.data.NO_TRANSAKSI;
                            vKdUnitDulu=vKdUnit;
                            if (rowSelectedPenJasLab_PA.data.NAMA_UNIT_ASAL === '')
                            {
                                vNamaUnit=rowSelectedPenJasLab_PA.data.NAMA_UNIT;
                            }else
                            {
                                vNamaUnit=rowSelectedPenJasLab_PA.data.NAMA_UNIT_ASAL;
                            }
                            
                            //-------22-02-2017
                            if(Ext.get('cboJenisTr_viPenJasLab_PA').getValue()=='Transaksi Baru'){
                                Ext.getCmp('btnHapusKunjunganLABPA').disable();
                                Ext.getCmp('btnGantiKekompokPasienLABPA').disable();
								Ext.getCmp('btnGantiDokterLABPA').disable();
							   Ext.getCmp('btnBukaTransaksiLAB').disable();	
                            }
                            else
                            {
                                
                                //alert(vKdUnit);
                                if (rowSelectedPenJasLab_PA.data.LUNAS==='t' || rowSelectedPenJasLab_PA.data.LUNAS==='true' || rowSelectedPenJasLab_PA.data.LUNAS===true){
                                    Ext.getCmp('btnGantiKekompokPasienLABPA').disable();
                                    Ext.getCmp('btnGantiDokterLABPA').disable();
									if (rowSelectedPenJasLab_PA.data.CO_STATUS==='t' || rowSelectedPenJasLab_PA.data.CO_STATUS==='true' || rowSelectedPenJasLab_PA.data.CO_STATUS===true){
										Ext.getCmp('btnBukaTransaksiLAB').enable();
									}else{
										Ext.getCmp('btnBukaTransaksiLAB').disable();
									}
									Ext.getCmp('btnHapusKunjunganLABPA').disable();
                                }else{
                                    Ext.getCmp('btnGantiKekompokPasienLABPA').enable();
                                    Ext.getCmp('btnGantiDokterLABPA').enable();
									Ext.getCmp('btnBukaTransaksiLAB').enable();	
                                    Ext.getCmp('btnHapusKunjunganLABPA').enable();
                                }
                                
                            }
                            
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedPenJasLab_PA = dsTRPenJasLab_PAList.getAt(ridx);
                    noTransaksiPilihan = rowSelectedPenJasLab_PA.data.NO_TRANSAKSI;
                    if (rowSelectedPenJasLab_PA !== undefined)
                    {
                        PenJasLab_PALookUp(rowSelectedPenJasLab_PA.data);
                    }
                    else
                    {
                        PenJasLab_PALookUp();
                        Ext.getCmp('cboDOKTER_viPenJasLab_PA').disable();
                        Ext.getCmp('cboGDRLABPA').disable();
                        Ext.getCmp('cboJKLABPA').disable();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   
                    {
                        id: 'colReqIdViewLABPA',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80
                    }, 
                    {
                        id: 'colTglRWJViewLABPA',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TGL_TRANSAKSI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TGL_TRANSAKSI);

                            }
                    },
                    {
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colNoMedrecViewLABPA'
                    },
                    {
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colNamaerViewLABPA'
                    },
                    {
                        id: 'colLocationViewLABPA',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewLABPA',
                        header: 'Dokter',
                        dataIndex: 'DOKTER',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colKdUnitAsalViewLABPA',
                        header: 'KD UNIT',
                        dataIndex: 'KD_UNIT_ASAL',
                        sortable: false,
                        hideable:false,
                        hidden:true,
                        menuDisabled:true,
                        width: 90
                    },{
                        id: 'colImpactViewLABPA',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT_ASLI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 90
                    },
                    {
                        header: 'Status Transaksi',
                        width: 100,
                        sortable: false,
                        hideable:true,
                        hidden:true,
                        menuDisabled:true,
                        dataIndex: 'POSTING_TRANSAKSI',
                        id: 'txtpostingLABPA',
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                        
                    },
                    {
                        id: 'colCustomerViewLABPA',
                        header: 'Kelompok Pasien',
                        dataIndex: 'CUSTOMER',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 90
                    },
                    {
                        id: 'colCoSTatusViewLABPA',
                        header: 'Status Lunas',
                        dataIndex: 'LUNAS',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80,
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                                case '':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                                case undefined:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
					{
                        id: 'colBukaTransaksiViewLABPA',
                        header: 'Status Tutup Transaksi',
                        dataIndex: 'CO_STATUS',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80,
						renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
								case '':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
								case undefined:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    }
                   
                ]
            ),
            viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditLABPA',
                        text: 'Lookup',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedPenJasLab_PA != undefined)
                            {
                                console.log(rowSelectedPenJasLab_PA.data);
                                
                                    PenJasLab_PALookUp(rowSelectedPenJasLab_PA.data);
                            }
                            else
                            {
                            ShowPesanWarningPenJasLab_PA('Pilih data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },{
                        id: 'btnHapusKunjunganLABPA',
                        text: 'Batal Transaksi',
                        tooltip: nmEditData,
                        iconCls: 'Remove',
                        disabled: true,
                        handler: function(sm, row, rec)
                        {
                            //alert();
                            if (rowSelectedPenJasLab_PA != undefined)
                            {
                                Ext.MessageBox.confirm('Hapus Kunjungan', "Yakin Akan Hapus Kunjungan ini ?", function (btn){
                                    if (btn === 'yes') {
                                        msg_box_alasanbataltrans_LABPA();
                                        Ext.getCmp('txtAlasanBatalTransLABPA').focus();
                                        console.log(rowSelectedPenJasLab_PA);
                                        
                                    }
                                });
                            }
                            else
                            {
                            ShowPesanWarningPenJasLab_PA('Pilih data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },{                             
                        id: 'btnGantiKekompokPasienLABPA', text: 'Ganti kelompok pasien', tooltip: 'Ganti kelompok pasien', iconCls: 'gantipasien', disabled:true,
                        handler: function () {
                            //Button Ganti Kelompok;
                            panelActiveDataPasien = 0;
                            KelompokPasienLookUp_labpa();
                        }
                    },{                             
                        id: 'btnGantiDokterLABPA', text: 'Ganti Dokter', tooltip: 'Ganti dokter pasien', iconCls: 'gantipasien', disabled:true,
                        handler: function () {
                            //Button Ganti Dokter;
                            panelActiveDataPasien = 1;
                            loaddatastoredokterLABPA();
                            fnDlgLABPAPasswordDulu();
                        }
                    },{								
						id: 'btnBukaTransaksiLAB', text: 'Buka Transaksi', tooltip: 'Buka Transaksi', iconCls: 'gantipasien', disabled:true,
						handler: function () {
							
							var parameter_bukatrans = {
								notrans_bukatrans 		: rowSelectedPenJasLab_PA.data.NO_TRANSAKSI,
								kd_kasir_bukatrans 		: rowSelectedPenJasLab_PA.data.KD_KASIR,
								kd_pasien_bukatrans 	: rowSelectedPenJasLab_PA.data.KD_PASIEN,
								kd_unit_bukatrans 		: rowSelectedPenJasLab_PA.data.KD_UNIT,
								tgl_transaksi_bukatrans : rowSelectedPenJasLab_PA.data.TGL_TRANSAKSI,
								urut_masuk_bukatrans 	: rowSelectedPenJasLab_PA.data.URUT_MASUK
							}
							msg_box_alasanbukatrans_LAB(parameter_bukatrans);
						}
					},
					//------------TAMBAH BARU 27-September-2017
					{								
						id: 'btnEditDataPasienLABPA', text: 'Edit data Pasien', tooltip: 'Edit data Pasien', iconCls: 'gantipasien', disabled:true,
						handler: function () {
							console.log(rowSelectedPenJasLab_PA.data);
							var parameter_editpasien = {
								medrec 						: rowSelectedPenJasLab_PA.data.KD_PASIEN,
								nama 						: rowSelectedPenJasLab_PA.data.NAMA,
								urut_masuk 					: rowSelectedPenJasLab_PA.data.URUT_MASUK,
								jk 							: rowSelectedPenJasLab_PA.data.JENIS_KELAMIN,
								tgl_lahir 					: rowSelectedPenJasLab_PA.data.TGL_LAHIR,
								alamat 						: rowSelectedPenJasLab_PA.data.ALAMAT,
								hp 							: rowSelectedPenJasLab_PA.data.HP,
								goldarah 					: rowSelectedPenJasLab_PA.data.GOL_DARAH
							}
							setLookUpGridDataView_viLABPA(parameter_editpasien);
							//msg_box_alasanbukatrans_RAD(parameter_bukatrans);
						}
					},
                ]
            }
    );
    
    
    //form depan awal dan judul tab
    var FormDepanPenJasLab_PA = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Penata Jasa Laboratorium PA',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
                        getItemPanelPenJasLabDepan_PA(),
                        grListPenJasLab_PA
                   ],
            listeners:
            {
                'afterrender': function()
                {
                    Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
                    Ext.getCmp('cboStatusLunas_viPenJasLab_PA').disable();
                }
            }
        }
    );
    
   return FormDepanPenJasLab_PA;

};
/* 
    PERBARUAN GANTI DOKTER  
    OLEH    : ADIT
    TANGGAL : 2017 - 02 - 24
*/
/* =============================================================================================================================================== */
function KelompokPasienLookUp_labpa(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien_labpa = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien_labpa(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien_labpa.show();
    KelompokPasienbaru_labpa();

};

function KelompokPasienbaru_labpa() 
{
    jeniscus_LABPA=0;
    KelompokPasienAddNew_LABPA = true;
    Ext.getCmp('cboKelompokpasien_LABPA').show()
    Ext.getCmp('txtCustomer_labpaLama').disable();
    Ext.get('txtCustomer_labpaLama').dom.value=vCustomer;
    Ext.get('txtLABPANoSEP').dom.value = vNoSEP;
    
    RefreshDatacombo_labpa(jeniscus_LABPA);
};

function getFormEntryTRKelompokPasien_labpa(lebar) 
{
    var pnlTRKelompokPasien_labpa = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien_labpa',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
                    getItemPanelInputKelompokPasien_labpa(lebar),
                    getItemPanelButtonKelompokPasien_labpa(lebar)
            ],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasienLABPA = new Ext.Panel
    (
        {
            id: 'FormDepanKelompokPasienLABPA',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: true,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: [pnlTRKelompokPasien_labpa   
                
            ]

        }
    );

    return FormDepanKelompokPasienLABPA
};
function getItemPanelButtonKelompokPasien_labpa(lebar) 
{
    var items =
    {
        layout: 'column',
        border: false,
        height:30,
        anchor:'100%',
        style:{'margin-top':'-1px'},
        items:
        [
            {
                layout: 'hBox',
                width:400,
                border: false,
                bodyStyle: 'padding:5px 0px 5px 5px',
                defaults: { margins: '3 3 3 3' },
                anchor: '90%',
                layoutConfig: 
                {
                    align: 'middle',
                    pack:'end'
                },
                items:
                [
                    {
                        xtype:'button',
                        text:'Simpan',
                        width:70,
                        style:{'margin-left':'0px','margin-top':'0px'},
                        hideLabel:true,
                        id:Nci.getId(),
                        handler:function()
                        {
                            if(panelActiveDataPasien == 0){
                                Datasave_Kelompokpasien_labpa();
                            }
                            if(panelActiveDataPasien == 1){
                                Datasave_GantiDokter_labpa();
                            }
                            
                        }
                    },
                    {
                        xtype:'button',
                        text:'Tutup',
                        width:70,
                        hideLabel:true,
                        id:Nci.getId(),
                        handler:function() 
                        {
                            if(panelActiveDataPasien == 0){
                                FormLookUpsdetailTRKelompokPasien_labpa.close();
                            }
                            
                            if(panelActiveDataPasien == 1){
                                FormLookUpsdetailTRGantiDokter_labpa.close();
                            }
                            
                        }
                    }
                ]
            }
        ]
    }
    return items;
};
function Datasave_GantiDokter_labpa(mBol) 
{   
    //console.log(Ext.get('cboDokterRequestEntry').getValue());
    if((Ext.get('cboDokterRequestEntryLABPA').getValue() == '') || (Ext.get('cboDokterRequestEntryLABPA').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntryLABPA').dom.value  === 'Pilih Dokter...'))
    {
        ShowPesanWarningPenJasLab_PA('Dokter baru harap diisi', "Informasi");
    }else{
        Ext.Ajax.request
        (
            {
                url: baseURL +  "index.php/main/functionIGD/UpdateGantiDokter", 
                params: getParamKelompokpasien_LABPA(),
                failure: function(o)
                {
                    ShowPesanWarningPenJasLab_PA('Simpan dokter pasien gagal', 'Gagal');
                },  
                success: function(o) 
                {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true) 
                    {
                        panelActiveDataPasien = 1;
                        FormLookUpsdetailTRGantiDokter_labpa.close();
                        validasiJenisTrLABPA();
                        Ext.getCmp('btnGantiKekompokPasienLABPA').disable();    
                        Ext.getCmp('btnGantiDokterLABPA').disable();   
						Ext.getCmp('btnBukaTransaksiLAB').disable();						
                        ShowPesanInfoPenJasLab_PA("Mengganti dokter pasien berhasil", "Success");
                    }else 
                    {
                        ShowPesanWarningPenJasLab_PA('Simpan dokter pasien gagal', 'Gagal');
                    };
                }
            }
        ) 
    }
    
};
function ValidasiEntryUpdateKelompokPasien_LABPA(modul,mBolHapus)
{
    var x = 1;
    
    if((Ext.get('kelPasien_LABPA').getValue() == '') || (Ext.get('kelPasien_LABPA').dom.value  === undefined ))
    {
        if (Ext.get('kelPasien_LABPA').getValue() == '' && mBolHapus === true) 
        {
            ShowPesanWarningPenJasLab_PA(nmGetValidasiKosong('Kelompok Pasien'), modul);
            x = 0;
        }
    };
    return x;
};

function ValidasiEntryUpdateGantiDokter_LABPA(modul,mBolHapus)
{
    var x = 1;
    
    if((Ext.get('cboDokterRequestEntryLABPA').getValue() == '') || (Ext.get('cboDokterRequestEntryLABPA').dom.value  === undefined ))
    {
        ShowPesanWarningPenJasLab_PA(nmGetValidasiKosong('Dokter baru harap diisi'), modul);
        x = 0;
    };
    return x;
};
function getParamKelompokpasien_LABPA(combo) 
{
    var params;
    if(panelActiveDataPasien == 0){
        params = {
            KDCustomer  : selectKdCustomer,
            KDNoSJP     : Ext.get('txtLABPANoSEP').getValue(),
            KDNoAskes   : Ext.get('txtLABPANoAskes').getValue(),
            KdPasien    : vKd_Pasien,
            TglMasuk    : vTanggal,
            KdUnit      : vKdUnit,
            UrutMasuk   : vUrutMasuk,
            KdDokter    : vKdDokter,
            alasan      : combo,
        }
    }else if(panelActiveDataPasien == 1 || panelActiveDataPasien == 2){
        params = {
            KdPasien    : vKd_Pasien,
            TglMasuk    : vTanggal,
            KdUnit      : vKdUnit,
            UrutMasuk   : vUrutMasuk,
            KdDokter    : vKdDokter,
            NoTransaksi : vNoTransaksi,
            KdKasir     : vKdKasir,
            KdUnitDulu  : vKdUnitDulu,
        }
    }else if(panelActiveDataPasien == 'undefined'){
        params = { data : "null", }
    }else{
        params = { data : "null", }
    }
    return params
};
function Datasave_Kelompokpasien_labpa(mBol) 
{   
    if (ValidasiEntryUpdateKelompokPasien_LABPA(nmHeaderSimpanData,false) == 1 )
    {           
		var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan ganti kelompok:', function(btn, combo){
            if (btn == 'ok')
            {
				if (combo!='') {
					Ext.Ajax.request
					(
						{
							url: baseURL +  "index.php/main/functionIGD/UpdateGantiKelompok",   
							params: getParamKelompokpasien_LABPA(combo),
							failure: function(o)
							{
								ShowPesanWarningPenJasLab_PA('Simpan kelompok pasien gagal', 'Gagal');
							},  
							success: function(o) 
							{
								var cst = Ext.decode(o.responseText);
								if (cst.success === true) 
								{
									panelActiveDataPasien = 1;
									FormLookUpsdetailTRKelompokPasien_labpa.close();
									validasiJenisTrLABPA();
									Ext.getCmp('btnGantiKekompokPasienLABPA').disable();    
									Ext.getCmp('btnGantiDokterLABPA').disable();    
									Ext.getCmp('btnBukaTransaksiLAB').disable();	
									ShowPesanInfoPenJasLab_PA("Mengganti kelompok pasien berhasil", "Success");
								}else 
								{
									panelActiveDataPasien = 1;
									ShowPesanWarningPenJasLab_PA('Simpan kelompok pasien gagal', 'Gagal');
								};
							}
						}
					 )
				}else{
                    ShowPesanWarningPenJasLab_PA('Harap memasukkan alasan perpindahan', 'Peringatan');
                }
			}
		});
            
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
    
};
function getItemPanelInputKelompokPasien_labpa(lebar) 
{
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding:10px 10px 10px 0px',
        border:false,
        height:170,
        items:
        [
            {
                columnWidth: .9,
                width: lebar -35,
                labpaelWidth:100,
                layout: 'form',
                border: false,
                items:
                [
                    getKelompokpasienlama_labpa(lebar), 
                    getItemPanelNoTransksiKelompokPasien_labpa(lebar)   ,
                    
                ]
            }
        ]
    };
    return items;
};
function getKelompokpasienlama_labpa(lebar) 
{
    var items =
    {
        Width:lebar,
        height:40,
        layout: 'column',
        border: false,
        
        items:
        [
            {
                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                items:
                [{   
                        xtype: 'tbspacer',
                        height: 2
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kelompok Pasien Asal',
                            name: 'txtCustomer_labpaLama',
                            id: 'txtCustomer_labpaLama',
                            labelWidth:130,
                            editable: false,
                            width: 100,
                            anchor: '95%'
                         }
                    ]
            }
            
        ]
    }
    return items;
};
function getItemPanelNoTransksiKelompokPasien_labpa(lebar) 
{
    var items =
    {
        Width:lebar,
        height:120,
        layout: 'column',
        border: false,
        
        
        items:
        [
            {

                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                items:
                [{   
                        xtype: 'tbspacer',
                        height:3
                    },{ 
                        xtype: 'combo',
                        fieldLabel: 'Kelompok Pasien Baru',
                        id: 'kelPasien_LABPA',
                        editable: false,
                        store: new Ext.data.ArrayStore
                            (
                                {
                                id: 0,
                                fields:
                                [
                                'Id',
                                'displayText'
                                ],
                                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                }
                            ),
                              displayField: 'displayText',
                              mode: 'local',
                              width: 100,
                              forceSelection: true,
                              triggerAction: 'all',
                              emptyText: 'Pilih Salah Satu...',
                              selectOnFocus: true,
                              anchor: '95%',
                              listeners:
                                 {
                                        'select': function(a, b, c)
                                    {
                                        if(b.data.displayText =='Perseorangan')
                                        {jeniscus_LABPA='0'
                                            //Ext.getCmp('txtLABPANoSEP').disable();
                                            //Ext.getCmp('txtRWJNoAskes').disable();
                                            }
                                        else if(b.data.displayText =='Perusahaan')
                                        {jeniscus_LABPA='1';
                                            //Ext.getCmp('txtLABPANoSEP').disable();
                                            //Ext.getCmp('txtRWJNoAskes').disable();
                                            }
                                        else if(b.data.displayText =='Asuransi')
                                        {jeniscus_LABPA='2';
                                            //Ext.getCmp('txtLABPANoSEP').enable();
                                            //Ext.getCmp('txtRWJNoAskes').enable();
                                        }
                                        
                                        RefreshDatacombo_labpa(jeniscus_LABPA);
                                    }

                                }
                        },{
                            columnWidth: .990,
                            layout: 'form',
                            border: false,
                            labelWidth:130,
                            items:
                            [
                                                mComboKelompokpasien_labpa()
                            ]
                        },{
                            xtype: 'textfield',
                            fieldLabel:'No SEP  ',
                            name: 'txtLABPANoSEP',
                            id: 'txtLABPANoSEP',
                            width: 100,
                            anchor: '99%'
                         }, {
                             xtype: 'textfield',
                            fieldLabel:'No Asuransi  ',
                            name: 'txtLABPANoAskes',
                            id: 'txtLABPANoAskes',
                            width: 100,
                            anchor: '99%'
                         }
                                    
                ]
            }
            
        ]
    }
    return items;
};

function mComboKelompokpasien_labpa()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viPJ_LABPA = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    if (jeniscus_LABPA===undefined || jeniscus_LABPA==='')
    {
        jeniscus_LABPA=0;
    }
    ds_customer_viPJ_LABPA.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_LABPA +'~'
            }
        }
    )
    var cboKelompokpasien_LABPA = new Ext.form.ComboBox
    (
        {
            id:'cboKelompokpasien_LABPA',
            typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: '',
                        align: 'Right',
                        anchor: '95%',
            store: ds_customer_viPJ_LABPA,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetKelompokpasien=b.data.displayText ;
                    selectKdCustomer=b.data.KD_CUSTOMER;
                    selectNamaCustomer=b.data.CUSTOMER;
                
                }
            }
        }
    );
    return cboKelompokpasien_LABPA;
};


function RefreshDatacombo_labpa(jeniscus_LABPA) 
{
    var kosong;
    ds_customer_viPJ_LABPA.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_LABPA +'~ '
            }
        }
    )
    
    return ds_customer_viPJ_LABPA;
};

/* =============================================================================================================================================== */
/* 
    PERBARUAN GANTI DOKTER  
    OLEH    : ADIT
    TANGGAL : 2017 - 02 - 24
*/
/* =============================================================================================================================================== */
function GantiDokterPasienLookUp_labpa(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRGantiDokter_labpa = new Ext.Window
    (
        {
            id: 'idGantiDokterLABPA',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiDokter_labpa(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRGantiDokter_labpa.show();
    GantiPasien_labpa();

};

function GantiPasien_labpa() 
{
    //RefreshDatacombo_rwj(jeniscus_LABPA);
};

function getFormEntryTRGantiDokter_labpa(lebar) 
{
    var pnlTRKelompokPasien_labpa = new Ext.FormPanel
    (
        {
            id: 'PanelTRGantiLABPA',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
                    getItemPanelInputGantiDokter_labpa(lebar),
                    getItemPanelButtonKelompokPasien_labpa(lebar)
            ],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiDokterLABPA = new Ext.Panel
    (
        {
            id: 'FormDepanGantiDokterLABPA',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: true,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: [pnlTRKelompokPasien_labpa
                
            ]

        }
    );

    return FormDepanGantiDokterLABPA
};

function getItemPanelInputGantiDokter_labpa(lebar) 
{
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding:10px 10px 10px 0px',
        border:false,
        height:170,
        items:
        [
            {
                columnWidth: .9,
                width: lebar -35,
                labelWidth:100,
                layout: 'form',
                border: false,
                items:
                [   
                    {
                        xtype: 'textfield',
                        fieldLabel:  'Unit Asal ',
                        name: 'txtUnitAsal_DataPasienLABPA',
                        id: 'txtUnitAsal_DataPasienLABPA',
                        value:vNamaUnit,
                        readOnly:true,
                        width: 100,
                        anchor: '99%'
                    },{
                        xtype: 'textfield',
                        fieldLabel: 'Dokter Asal ',
                        name: 'txtDokterAsal_DataPasienLABPA',
                        id: 'txtDokterAsal_DataPasienLABPA',
                        value:vNamaDokter,
                        readOnly:true,
                        width: 100,
                        anchor: '99%'
                    },
                    mComboDokterGantiEntryLABPA()
                ]
            }
        ]
    };
    return items;
};

function loaddatastoredokterLABPA(){
    dsDokterRequestEntryLABPA.load({
         params :{
            Skip    : 0,
            Take    : 1000,
            Sort    : 'nama',
            Sortdir : 'ASC',
            target  : 'ViewDokterPenunjang',
            param   : 'kd_unit=~'+ vKdUnit+ '~'
        }
    });
}
function mComboDokterGantiEntryLABPA(){ 
    /* var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntryLABPA = new WebApp.DataStore({fields: Field}); */
    var cboDokterGantiEntryLABPA = new Ext.form.ComboBox({
        id: 'cboDokterRequestEntryLABPA',
        typeAhead: true,
        triggerAction: 'all',
        name:'txtdokter',
        lazyRender: true,
        mode: 'local',
        selectOnFocus:true,
        forceSelection: true,
        emptyText:'Pilih Dokter...',
        fieldLabel: 'Dokter Baru',
        align: 'Right',
        store: dsDokterRequestEntryLABPA,
        valueField: 'KD_DOKTER',
        displayField: 'NAMA',
        anchor:'100%',
        listeners:{
            'select': function(a,b,c){
                vKdDokter = b.data.KD_DOKTER;
            },
        }
    });
    return cboDokterGantiEntryLABPA;
};
function mComboStatusBayar_viPenJasLab_PA()
{
  var cboStatus_viPenJasLab_PA = new Ext.form.ComboBox
    (
        {
                    id:'cboStatus_viPenJasLab_PA',
                    x: 155,
                    y: 70,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusByr_viKasirLABPA,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusByr_viKasirLABPA=b.data.displayText ;
                            }
                    }
        }
    );
    return cboStatus_viPenJasLab_PA;
};
function mComboStatusLunas_viPenJasLab_PA()
{
  var cboStatusLunas_viPenJasLab_PA = new Ext.form.ComboBox
    (
        {
                    id:'cboStatusLunas_viPenJasLab_PA',
                    x: 155,
                    y: 70,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Lunas'], [3, 'Belum Lunas']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusLunasByr_viKasirLABPA,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusLunasByr_viKasirLABPA=b.data.displayText ;
                                    validasiJenisTrLABPA();
                            }
                    }
        }
    );
    return cboStatusLunas_viPenJasLab_PA;
};

//COMBO JENIS TRANSAKSI
function mComboJenisTrans_viPenJasLab_PA()
{
  var cboJenisTr_viPenJasLab_PA = new Ext.form.ComboBox
    (
        {
                    id:'cboJenisTr_viPenJasLab_PA',
                    x: 155,
                    y: 10,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS TRANSAKSI',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Transaksi Baru'],[2, 'Transaksi Lama']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountJenTr_viPenJasLab_PA,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                combovalues=b.data.displayText;
                                if(b.data.Id==1){
                                    Ext.getCmp('cbounitlabpas_viPenJasLab_PA').setValue(null);
                                    Ext.getCmp('cbounitlabpas_viPenJasLab_PA').disable();
                                    Ext.getCmp('cboKeteranganHasilLab_PAFilterDepan').disable();
                                    tmppasienbarulama = 'Baru';
                                }else{
                                    Ext.getCmp('cbounitlabpas_viPenJasLab_PA').enable();
                                    Ext.getCmp('cbounitlabpas_viPenJasLab_PA').setValue(combovaluesunittujuan);
                                    Ext.getCmp('cboKeteranganHasilLab_PAFilterDepan').enable();
                                    tmppasienbarulama = 'Lama';
                                }
                                
                                validasiJenisTrLABPA();

                            }
                    }
        }
    );
    return cboJenisTr_viPenJasLab_PA;
};

var tmppasienbarulama = 'Baru';


function validasiJenisTrLABPA(){
    var kriteria = "";
    var tmpkriteriaunittujuancrwi='';
    var strkriteria=getCriteriaFilter_viDaftar();
    var tmpKodeNomorLab="";
    var tmpKodeCustomer="";
    if (combovalues === 'Transaksi Lama')
    {
        Ext.getCmp('cboStatusLunas_viPenJasLab_PA').enable();
    
        if(radiovaluesLABPA === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriaLABPA = "";
                }else
                {
                    tmpkreteriaLABPA = "and kd_unit_asal = '" + Ext.getCmp('cboUNIT_viKasirLab_PA').getValue() + "'";
                }
            }else {
                tmpkreteriaLABPA = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasLab_PA').getValue())
            {
               tmpkriterianamaLABPA = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab_PA').dom.value + "%')";
            }else
            {
                tmpkriterianamaLABPA = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasLab_PA').getValue())
            {
               tmpkriteriamedrecLABPA = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab_PA').dom.value + "'";
            }else
            {
                tmpkriteriamedrecLABPA = "";
            }
            if(Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() != null && Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }
            /* if(selectCountStatusByr_viKasirLABPA !== "")
            {
                if(selectCountStatusByr_viKasirLABPA === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirLABPA === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
            if(selectCountStatusLunasByr_viKasirLABPA !== "")
            {
                if(selectCountStatusLunasByr_viKasirLABPA === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirLABPA === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            if(KodeNomorLAB_PA=='Semua'){
                tmpKodeNomorLab="";
            }else if(KodeNomorLAB_PA=='MF'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MF' ";
            }else if(KodeNomorLAB_PA=='MT'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MT' ";
            }else if(KodeNomorLAB_PA=='MI'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MI' ";
            }else if(KodeNomorLAB_PA=='MS'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MS' ";
            }else if(KodeNomorLAB_PA=='K'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='K' ";
            }
            if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=0";
                }else{
                    tmpKodeCustomer="and jenis_cust=0 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=1";
                }else{
                tmpKodeCustomer="and jenis_cust=1 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=2";
                }else{
                    tmpKodeCustomer="and jenis_cust=2 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            
            kriteria = " kd_bagian = 4 and kd_unit in ('"+tmpkd_unit+"') and left(kd_unit_asal, 1) in('3') "+ tmpkriteriaunittujuancrwi +""+ tmpkreteriaLABPA +" "+tmpKodeCustomer+" "+ tmpkriterianamaLABPA +" "+ tmpkriteriamedrecLABPA +"  "+tmplunas+" "+strkriteria+" "+tmpKodeNomorLab+" and left(kd_pasien, 2) not in ('LB')  and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi limit 10";
            tmpunit = 'ViewPenJasLab';
            loadpenjaslabpa(kriteria, tmpunit);
            
        }else if (radiovaluesLABPA === '2'){   
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and kd_unit_asal = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else
            {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasLab_PA').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab_PA').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasLab_PA').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab_PA').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            if(Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() != null && Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }
            /* 
            if(selectCountStatusByr_viKasirLABPA !== "")
            {
                if(selectCountStatusByr_viKasirLABPA === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirLABPA === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
            if(selectCountStatusLunasByr_viKasirLABPA !== "")
            {
                if(selectCountStatusLunasByr_viKasirLABPA === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirLABPA === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            if(KodeNomorLAB_PA=='Semua'){
                tmpKodeNomorLab="";
            }else if(KodeNomorLAB_PA=='MF'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MF' ";
            }else if(KodeNomorLAB_PA=='MT'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MT' ";
            }else if(KodeNomorLAB_PA=='MI'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MI' ";
            }else if(KodeNomorLAB_PA=='MS'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MS' ";
            }else if(KodeNomorLAB_PA=='K'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='K' ";
            }
            if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=0";
                }else{
                    tmpKodeCustomer="and jenis_cust=0 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=1";
                }else{
                tmpKodeCustomer="and jenis_cust=1 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=2";
                }else{
                    tmpKodeCustomer="and jenis_cust=2 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            kriteria = " kd_bagian = 4  and kd_unit in ('"+tmpkd_unit+"') "+tmpkriteriaunittujuancrwi+" "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" "+tmpKodeCustomer+" "+tmplunas+" "+strkriteria+"  "+tmpKodeNomorLab+"  and left(kd_pasien, 2) not in ('LB')  and left(kd_unit_asal, 1) = '1' and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' ORDER BY no_transaksi desc limit 10";
            tmpunit = 'ViewPenJasLab';
            loadpenjaslabpa(kriteria, tmpunit);
        }else if (radiovaluesLABPA === '3')
        {
            if (Ext.getCmp('txtNamaPasienPenJasLab_PA').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab_PA').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasLab_PA').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab_PA').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            if(Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() != null && Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }
            /* if(selectCountStatusByr_viKasirLABPA !== "")
            {
                if(selectCountStatusByr_viKasirLABPA === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirLABPA === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
            if(selectCountStatusLunasByr_viKasirLABPA !== "")
            {
                if(selectCountStatusLunasByr_viKasirLABPA === "Lunas")
                {
                   tmplunas = " and lunas='t' ";
                }else if(selectCountStatusLunasByr_viKasirLABPA === "Belum Lunas")
                {
                    tmplunas = " and lunas='f' ";
                }else
                {
                    tmplunas = "";
                }
            }
            if(KodeNomorLAB_PA=='Semua'){
                tmpKodeNomorLab="";
            }else if(KodeNomorLAB_PA=='MF'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MF' ";
            }else if(KodeNomorLAB_PA=='MT'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MT' ";
            }else if(KodeNomorLAB_PA=='MI'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MI' ";
            }else if(KodeNomorLAB_PA=='MS'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MS' ";
            }else if(KodeNomorLAB_PA=='K'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='K' ";
            }
            if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=0";
                }else{
                    tmpKodeCustomer="and jenis_cust=0 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=1";
                }else{
                tmpKodeCustomer="and jenis_cust=1 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=2";
                }else{
                    tmpKodeCustomer="and jenis_cust=2 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            kriteria = " kd_bagian = 4  and kd_unit in ('"+tmpkd_unit+"') "+ tmpkriterianama +""+ tmpkriteriamedrec +" "+tmpKodeCustomer+" "+ tmpkriteriaunittujuancrwi +"  "+tmpKodeNomorLab+"    and left(kd_pasien, 2) = 'LB' and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' "+tmplunas+" ORDER BY no_transaksi desc limit 10";
            tmpunit = 'ViewPenJasLabKunjunganLangsung';
            loadpenjaslabpa(kriteria, tmpunit);
        
        }else if (radiovaluesLABPA === '4')
        {
            if (Ext.getCmp('txtNamaPasienPenJasLab_PA').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab_PA').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasLab_PA').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab_PA').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            if(Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() != null && Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitlabpas_viPenJasLab_PA').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }
            /* if(selectCountStatusByr_viKasirLABPA !== "")
            {
                if(selectCountStatusByr_viKasirLABPA === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirLABPA === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
            if(selectCountStatusLunasByr_viKasirLABPA !== "")
            {
                if(selectCountStatusLunasByr_viKasirLABPA === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirLABPA === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            if(KodeNomorLAB_PA=='Semua'){
                tmpKodeNomorLab="";
            }else if(KodeNomorLAB_PA=='MF'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MF' ";
            }else if(KodeNomorLAB_PA=='MT'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MT' ";
            }else if(KodeNomorLAB_PA=='MI'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MI' ";
            }else if(KodeNomorLAB_PA=='MS'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='MS' ";
            }else if(KodeNomorLAB_PA=='K'){
                tmpKodeNomorLab=" and left(no_register,"+KodeNomorLAB_PA.length+")='K' ";
            }
            if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=0";
                }else{
                    tmpKodeCustomer="and jenis_cust=0 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=1";
                }else{
                tmpKodeCustomer="and jenis_cust=1 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=2";
                }else{
                    tmpKodeCustomer="and jenis_cust=2 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            kriteria = " kd_bagian = 4  and kd_unit in ('"+tmpkd_unit+"') and left(kd_unit_asal, 1) in('2') "+tmpkriteriamedrec+" and left(kd_pasien, 2) not in ('LB')  "+tmpkriteriaunittujuancrwi+" "+tmplunas+"  "+tmpKodeNomorLab+"  and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' "+tmpKodeCustomer+" ORDER BY no_transaksi desc limit 10";
            tmpunit = 'ViewPenJasLab';
            loadpenjaslabpa(kriteria, tmpunit);
        }else
        {
            kriteria = "posting_transaksi = 'f'  and kd_bagian = 4  and kd_unit in ('"+tmpkd_unit+"') and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasLab';
            loadpenjaslabpa(kriteria, tmpunit);
        }
        
    }else if (combovalues === 'Transaksi Baru')
    {
        Ext.getCmp('cboStatusLunas_viPenJasLab_PA').disable();
        if(radiovaluesLABPA === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriaLABPA = "";
                }else
                {
                tmpkreteriaLABPA = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirLab_PA').getValue() + "'";
                }
            }else {
                tmpkreteriaLABPA = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasLab_PA').getValue())
            {
               tmpkriterianamaLABPA = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab_PA').dom.value + "%')";
            }else{
                tmpkriterianamaLABPA = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasLab_PA').getValue())
            {
               tmpkriteriamedrecLABPA = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab_PA').dom.value + "'";
            }else{
                tmpkriteriamedrecLABPA = "";
            }
        if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 0){
            tmpKodeCustomer="";
        }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 1){
            if (Ext.getCmp('cboPerseoranganLabDepan_PA').getValue()==='Semua'){
                tmpKodeCustomer="and kontraktor.jenis_cust=0";
            }else{
                tmpKodeCustomer="and kontraktor.jenis_cust=0 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
            }
            
        }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 2){
            if (Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').getValue()==='Semua'){
                tmpKodeCustomer="and kontraktor.jenis_cust=1";
            }else{
            tmpKodeCustomer="and kontraktor.jenis_cust=1 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
            }
            
        }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 3){
            if (Ext.getCmp('cboAsuransiLabDepan_PA').getValue()==='Semua'){
                tmpKodeCustomer="and kontraktor.jenis_cust=2";
            }else{
                tmpKodeCustomer="and kontraktor.jenis_cust=2 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
            }
            
        }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' "+tmpKodeCustomer+" "+ tmpkreteriaLABPA +" "+ tmpkriterianamaLABPA +" "+ tmpkriteriamedrecLABPA +" and left(u.kd_unit,1) IN ('3') ORDER BY   tr.no_transaksi desc limit 10";
            tmpunit = 'ViewPenJasLab';
            //loadpenjaslabpa(tmpparams, tmpunit);
            refeshpenjaslabpa(tmpparams);
        }
        else if (radiovaluesLABPA === '2')
        {
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and u.kd_unit = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasLab_PA').getValue())
            {
               tmpkriterianama = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab_PA').dom.value + "%')";
            }else {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasLab_PA').getValue())
            {
               tmpkriteriamedrec = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab_PA').dom.value + "'";
            }else {
                tmpkriteriamedrec = "";
            }
            if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=0";
                }else{
                    tmpKodeCustomer="and kontraktor.jenis_cust=0 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=1";
                }else{
                tmpKodeCustomer="and kontraktor.jenis_cust=1 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=2";
                }else{
                    tmpKodeCustomer="and kontraktor.jenis_cust=2 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            if (tmpkd_unit==='44' || tmpkd_unit===44){
                 tmpparams = " nginap.tgl_inap >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and nginap.tgl_inap <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' "+ tmpkriteriakamar +" "+tmpKodeCustomer+" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" and u.kd_unit IN ('10030','10031','10032','10033','10034','10035','10036','10037','10038','10039','10040','10041','10042','10043','10044','10045','10046','10047') ORDER BY   tr.no_transaksi desc limit 10";
            }else if (tmpkd_unit==='45' || tmpkd_unit===45){
                 tmpparams = " nginap.tgl_inap >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and nginap.tgl_inap <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' "+ tmpkriteriakamar +" "+tmpKodeCustomer+" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" and (left(u.kd_unit,1) ='1' and u.kd_unit NOT IN ('10030','10031','10032','10033','10034','10035','10036','10037','10038','10039','10040','10041','10042','10043','10044','10045','10046','10047')) ORDER BY   tr.no_transaksi desc limit 10";
                 // tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' "+ tmpkriteriakamar +" "+tmpKodeCustomer+" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" and ( u.kd_unit NOT IN ('10030','10031','10032','10033','10034','10035','10036','10037','10038','10039','10040','10041','10042','10043','10044','10045','10046','10047')) ORDER BY   tr.no_transaksi desc limit 10";
            }else {
                tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' "+ tmpkriteriakamar +" "+tmpKodeCustomer+" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" and left(u.kd_unit,1)='1' ORDER BY   tr.no_transaksi desc limit 10";
            }
            
             tmpunit = 'ViewPenJasLab';
             //loadpenjaslabpa(tmpparams, tmpunit);
             refeshpenjaslabpa(tmpparams);
             
        }else if (radiovaluesLABPA === '3')
        {
            PenJasLab_PALookUp();
            Ext.getCmp('txtnotlplabpa').setReadOnly(false);
        }else if (radiovaluesLABPA === '4')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriaLABPA = "";
                }else
                {
                tmpkreteriaLABPA = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirLab_PA').getValue() + "'";
                }
            }else {
                tmpkreteriaLABPA = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasLab_PA').getValue())
            {
               tmpkriterianamaLABPA = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab_PA').dom.value + "%')";
            }else{
                tmpkriterianamaLABPA = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasLab_PA').getValue())
            {
               tmpkriteriamedrecLABPA = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab_PA').dom.value + "'";
            }else{
                tmpkriteriamedrecLABPA = "";
            }
            if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=0";
                }else{
                    tmpKodeCustomer="and kontraktor.jenis_cust=0 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=1";
                }else{
                tmpKodeCustomer="and kontraktor.jenis_cust=1 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanLab_PA').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanLab_PA').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiLabDepan_PA').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=2";
                }else{
                    tmpKodeCustomer="and kontraktor.jenis_cust=2 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            //alert(tmpparams);
            if (tmpkd_unit==='44' || tmpkd_unit===44){
                tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' "+ tmpkreteriaLABPA +" "+tmpKodeCustomer+" "+ tmpkriterianamaLABPA +" "+ tmpkriteriamedrecLABPA +" and u.kd_unit IN ('231','246','249','251','259','260','261','262','263','264','265','266','267','268','269','270','271','272','273','274','275','276') ORDER BY  tr.no_transaksi desc limit 10";
            }else if (tmpkd_unit==='45' || tmpkd_unit===45){
                tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' "+ tmpkreteriaLABPA +" "+tmpKodeCustomer+" "+ tmpkriterianamaLABPA +" "+ tmpkriteriamedrecLABPA +" and (left(u.kd_unit,1) ='2' and u.kd_unit NOT IN ('231','246','249','251','254','259','260','261','262','263','264','265','266','267','268','269','270','271','272','273','274','275','276')) ORDER BY  tr.no_transaksi desc limit 10";
            }else{
                tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() + "' "+ tmpkreteriaLABPA +" "+tmpKodeCustomer+" "+ tmpkriterianamaLABPA +" "+ tmpkriteriamedrecLABPA +" and left(u.kd_unit,1) ='2' ORDER BY  tr.no_transaksi desc limit 10";
            }
            
            tmpunit = 'ViewPenJasLab';
            //loadpenjaslabpa(tmpparams, tmpunit);
            refeshpenjaslabpa(tmpparams);
        }
        
    }
}

//VALIDASI COMBO UNIT/POLI
function getDataCariUnitPenjasLab_PA(kriteria)
{
    if (kriteria===undefined)
    {
        kriteria="kd_bagian=3 and parent<>'0'";
    }
    dsunit_viKasirLab_PA.load
    (
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'kd_unit',
                        Sortdir: 'ASC',
                        target: 'ViewSetupUnit',
                        param: kriteria
                    }
            }
    );
    return dsunit_viKasirLab_PA;
}
function mComboUnit_viKasirLABPA() 
{
    
    var Field = ['KD_UNIT','NAMA_UNIT'];
    
    dsunit_viKasirLab_PA = new WebApp.DataStore({ fields: Field });
    getDataCariUnitPenjasLab_PA();
    var cboUNIT_viKasirLab_PA = new Ext.form.ComboBox
    (
            {
                id: 'cboUNIT_viKasirLab_PA',
                x: 155,
                y: 70,
                typeAhead: true,
                triggerAction: 'all',
                emptyText:'Poli',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 100,
                store: dsunit_viKasirLab_PA,
                valueField: 'KD_UNIT',
                displayField: 'NAMA_UNIT',
                value:'All',
                listeners:
                    {

                        'select': function(a, b, c) 
                            {                          
                                //RefreshDataFilterPenJasRad();
                                combovaluesunit=b.data.valueField;
                                validasiJenisTrLABPA();
                            }

                    }
            }
    );
    
    return cboUNIT_viKasirLab_PA;
};

function mcomboKamarSpesialLABPA()
{
    var Field = ['no_kamar','kamar'];
    ds_KamarSpesial_viJasRad = new WebApp.DataStore({fields: Field});

    ds_KamarSpesial_viJasRad.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewSetupKelasSpesial',
                param: ""
            }
        }
    )

    var cboRujukanKamarSpesialJasRadRequestEntry = new Ext.form.ComboBox
    (
        {
            x: 155,
            y: 70,
            id: 'cboRujukanKamarSpesialJasRadRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Kamar...',
            fieldLabel: 'Kamar ',
            align: 'Right',
            store: ds_KamarSpesial_viJasRad,
            valueField: 'no_kamar',
            displayField: 'kamar',
            Width:'150',
            listeners:
                {
                    'select': function(a, b, c)
                    {
                        ComboValuesKamar=b.data.valueField;
                        validasiJenisTrLABPA();    
                    },
                    'render': function(c)
                    {
                        
                    }


        }
        }
    )

    return cboRujukanKamarSpesialJasRadRequestEntry;
}

//LOOKUP DETAIL TRANSAKSI LABPAORATORIUM
function PenJasLab_PALookUp(rowdata) 
{
    var lebar = 900;
    FormLookUpsdetailTRLABPA = new Ext.Window
    (
        {
            id: 'gridPenJasLab_PA',
            title: 'Penata Jasa Laboratorium PA',
            closeAction: 'destroy',
            width: lebar,
            height: 580,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryPenJasLab_PA(lebar,rowdata),
            listeners:
            {
                 activate: function()
                {
                    if (rowdata === undefined){
                        Ext.getCmp('txtNamaPasienLABPA').focus(false,100);
                        Ext.getCmp('txtNoMedrecLABPA').setReadOnly(true);
                        Ext.getCmp('txtNamaUnitLABPA').setReadOnly(true);
                        Ext.getCmp('txtNamaPasienLABPA').setReadOnly(false);
                        Ext.getCmp('txtAlamatLABPA').setReadOnly(false);
                        Ext.getCmp('dtpTtlLABPA').setReadOnly(false);
                        Ext.getCmp('txtCustomerLamaHide').hide();
                    }else{
                        Ext.getCmp('cboDOKTER_viPenJasLab_PA').focus(false,1000);
                    }
                    
                    
                }
            }
        }
    );

    FormLookUpsdetailTRLABPA.show();
    
    if (rowdata === undefined) 
    {
        LABPAAddNew();
    }
    else 
    {
        TRLABPAInit(rowdata);
    }

};
function load_data_printer_kasirlabpa(param)
{

    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/functionLAB/getPrinter",
        params:{
            command: param
        } ,
        failure: function(o)
        {
             var cst = Ext.decode(o.responseText);
            
        },      
        success: function(o) {
            //cbopasienorder_mng_apotek.store.removeAll();
                var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = dsprinter_kasirlabpa.recordType;
                var o=cst['listData'][i];
                
                recs.push(new recType(o));
                dsprinter_kasirlabpa.add(recs);
            }
        }
    });
}
function mCombo_printer_kasirlabpa(){ 
    var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];
    dsprinter_kasirlabpa = new WebApp.DataStore({ fields: Field });
    load_data_printer_kasirlabpa();
    var cbo_printer_kasirlabpa= new Ext.form.ComboBox
    (
        {
            id: 'cbopasienorder_printer_kasirlabpa',
            typeAhead       : true,
            triggerAction   : 'all',
            lazyRender      : true,
            hidden :true,
            mode            : 'local',
            emptyText: 'Pilih Printer',
            fieldLabel:  '',
            align: 'Right',
            width: 200,
            store: dsprinter_kasirlabpa,
            valueField: 'name',
            displayField: 'name',
            //hideTrigger       : true,
            listeners:
            {
                                
            }
        }
    );return cbo_printer_kasirlabpa;
};
function PenjasLookUpLABPA(rowdata)
{
    var lebar = 700;
    FormLookUpsdetailTRPenjasLABPA = new Ext.Window
            (
                    {
                        id: 'gridPenjasLABPA',
                        title: 'Penata Jasa Laboratorium PA',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 500,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        
                        items: getFormEntryPenjasBayarLABPA(lebar),
                        enableKeyEvents:true,
                        listeners:
                                {
                                    keydown:function(text,e){
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            alert('hai');
                                        }
                                    }
                                }
                    }
            );

    FormLookUpsdetailTRPenjasLABPA.show();
    if (rowdata == undefined)
    {
        TRPenjasBayarLABPAInit(rowdata);
    } else
    {
        TRPenjasBayarLABPAInit(rowdata)
    }

}
;
function mEnabledKasirLABPACM(mBol)
{
   /*  Ext.get('btnLookupKasirLABPA').dom.disabled = mBol;
    Ext.get('btnHpsBrsKasirLABPA').dom.disabled = mBol; */
}
;
function PenjasBayarLABPAAddNew()
{
    AddNewKasirLABPAKasir = true;
    Ext.get('txtNoTransaksiKasirLABPAKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
    Ext.get('txtNoMedrecDetransaksi').dom.value = '';
    Ext.get('txtNamaPasienDetransaksi').dom.value = '';

    //Ext.get('txtKdUrutMasuk').dom.value = '';
    Ext.get('cboStatus_viKasirLABPAKasir').dom.value = ''
    rowSelectedPenJasLab_PA = undefined;
    dsTRDetailPenJasLab_PAList.removeAll();
    mEnabledKasirLABPACM(false);


}
;
function loaddatastorePembayaran(jenis_pay)
{
    console.log(jenis_pay);
    dsComboBayar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'nama',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboBayar',
                                    param: 'jenis_pay=~' + jenis_pay + '~'
                                }
                    }
            )
}
function TRPenjasBayarLABPAInit(rowdata)
{
    AddNewKasirLABPAKasir = false;
    
    
    console.log(rowdata);
    
    
    
    if (rowdata===undefined)
    {
        Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganLABPA').getValue().format('d/M/Y');
        Ext.get('txtNoMedrecDetransaksi').dom.value = Ext.getCmp('txtNoMedrecLABPA').getValue();
        Ext.get('txtNamaPasienDetransaksi').dom.value = Ext.getCmp('txtNamaPasienLABPA').getValue();
        tanggaltransaksitampung = Ext.getCmp('dtpKunjunganLABPA').getValue();
    }
    else
    {
        Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganLABPA').getValue().format('d/M/Y');
        Ext.get('txtNoMedrecDetransaksi').dom.value = rowdata.KD_PASIEN;
        Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
        tanggaltransaksitampung = rowdata.TGL_TRANSAKSI;
    }
    
    // take the displayField value 
    var notransnya;
    if (Ext.getCmp('txtNoTransaksiPenJasLab_PA').getValue()==='' || Ext.getCmp('txtNoTransaksiPenJasLab_PA').getValue()===undefined){
        notransnya=rowdata.NO_TRANSAKSI;
    }
    else
    {
        notransnya=Ext.getCmp('txtNoTransaksiPenJasLab_PA').getValue();
    }
    var modul='';
    if(radiovaluesLABPA == 1){
        modul='igd';
    } else if(radiovaluesLABPA == 2){
        modul='rwi';
    } else if(radiovaluesLABPA == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    Ext.get('txtNoTransaksiKasirLABPAKasir').dom.value = notransnya;
    RefreshDataKasirLABPAKasirDetail(notransnya,kd_kasir_labpa);
    Ext.Ajax.request({
        url: baseURL + "index.php/lab_pa/functionLABPA/cekPembayaran",
        params: {
            notrans: notransnya,
            Modul:modul,
            kdkasir: kd_kasir_labpa
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            console.log(cst.ListDataObj);
            loaddatastorePembayaran(cst.ListDataObj[0].jenis_pay);
            notransaksi=cst.ListDataObj[0].no_transaksi;
            kodeunit=cst.ListDataObj[0].kd_unit;
            vkd_unit = cst.ListDataObj[0].kd_unit;
            kdkasir = cst.ListDataObj[0].kd_kasir;
            //alert(kdkasir);
            notransaksiasal_labpa = cst.ListDataObj[0].no_transaksi_asal;
            kdkasirasal_labpa = cst.ListDataObj[0].kd_kasir_asal;
            Ext.get('cboPembayaran').dom.value =cst.ListDataObj[0].ket_payment;
            Ext.get('cboJenisByr').dom.value =cst.ListDataObj[0].cara_bayar; 
            Ext.get('txtNomorLab_PA').dom.value =cst.ListDataObj[0].no_register; 
            vflag = cst.ListDataObj[0].flag;
            tapungkd_pay = cst.ListDataObj[0].kd_pay;
            vkode_customer =  cst.ListDataObj[0].kd_customer;
            
        }

    });
    
    
    tampungtypedata = 0;
    
    jenispay = 1;
    
    showCols(Ext.getCmp('gridDTItemTest'));
    hideCols(Ext.getCmp('gridDTItemTest'));
   
    //(rowdata.NO_TRANSAKSI;


    Ext.Ajax.request({
        url: baseURL + "index.php/main/getcurrentshift",
        params: {
            command: '0',
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {

            tampungshiftsekarang = o.responseText
        }

    });
    


}
;
function getItemPanelNoTransksiLABPAKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Transaksi ',
                                                name: 'txtNoTransaksiKasirLABPAKasir',
                                                id: 'txtNoTransaksiKasirLABPAKasir',
                                                emptyText: nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%',
                                                enableKeyEvents:true,
                                                listeners:
                                                        {
                                                            keydown:function(text,e){
                                                                if(e.keyCode == 122){
                                                                    e.preventDefault();
                                                                    printbillRadLab_PA();
                                                                }else if(e.keyCode == 123){
                                                                    e.preventDefault();
                                                                    printkwitansiRadLab_PA();
                                                                }
                                                            }
                                                        },
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 55,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTanggalDetransaksi',
                                                name: 'dtpTanggalDetransaksi',
                                                format: 'd/M/Y',
                                                //readOnly: true,
                                                value: now,
                                                anchor: '100%',
                                                enableKeyEvents:true,
                                                listeners:
                                                        {
                                                            keydown:function(text,e){
                                                                if(e.keyCode == 122){
                                                                    e.preventDefault();
                                                                    printbillRadLab_PA();
                                                                }else if(e.keyCode == 123){
                                                                    e.preventDefault();
                                                                    printkwitansiRadLab_PA();
                                                                }
                                                            }
                                                        },
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function mComboJenisByrView()
{
    var Field = ['JENIS_PAY', 'DESKRIPSI', 'TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({fields: Field});
    dsJenisbyrView.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'jenis_pay',
                                    Sortdir: 'ASC',
                                    target: 'ComboJenis',
                                }
                    }
            );

    var cboJenisByr = new Ext.form.ComboBox
            (
                    {
                        id: 'cboJenisByr',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Pembayaran      ',
                        align: 'Right',
                        anchor: '100%',
                        store: dsJenisbyrView,
                        valueField: 'JENIS_PAY',
                        displayField: 'DESKRIPSI',
                        enableKeyEvents:true,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        loaddatastorePembayaran(b.data.JENIS_PAY);
                                        tampungtypedata = b.data.TYPE_DATA;
                                        jenispay = b.data.JENIS_PAY;
                                        showCols(Ext.getCmp('gridDTItemTest'));
                                        hideCols(Ext.getCmp('gridDTItemTest'));
                                        getTotalDetailProdukLABPA();
                                        Ext.get('cboPembayaran').dom.value = 'Pilih Pembayaran...';
                                    },
                                    keydown:function(text,e)
                                    {
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            printbillRadLab_PA();
                                        }else if(e.keyCode == 123){
                                            e.preventDefault();
                                            printkwitansiRadLab_PA();
                                        }
                                    }
                                }
                    }
            );

    return cboJenisByr;
}
;
function getTotalDetailProdukLABPA()
{
    
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < dsTRDetailKasirLABPAKasirList.getCount(); i++)
    {

        var recordterakhir;

        //alert(TotalProduk);
        if (tampungtypedata == 0)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABPAKasirList.data.items[i].data.BAYARTR);

            //recordterakhir= tampunggrid
            //TotalProduk.toString().replace(/./gi, "");
            TotalProduk += tampunggrid

            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLABPA').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 3)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABPAKasirList.data.items[i].data.PIUTANG);
            //TotalProduk.toString().replace(/./gi, "");
            //recordterakhir=tampunggrid
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLABPA').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 1)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABPAKasirList.data.items[i].data.DISCOUNT);

            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLABPA').dom.value = formatCurrency(recordterakhir);
        }
    }
    
    if (Ext.getCmp('txtJumlah2EditData_viKasirLABPA').getValue()==='0' || Ext.getCmp('txtJumlah2EditData_viKasirLABPA').getValue()===0 )
    {
        Ext.getCmp('btnSimpanKasirLABPA').disable();
		tombol_bayar='disable';
        Ext.getCmp('btnTransferKasirLABPA').disable();
    }
    else
    {
        Ext.getCmp('btnSimpanKasirLABPA').enable();
		tombol_bayar='enable';
        Ext.getCmp('btnTransferKasirLABPA').enable();
    }
    bayar = Ext.get('txtJumlah2EditData_viKasirLABPA').getValue();
    return bayar;
}
;


    
function hideCols(grid)
{
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);

    }
}
;
function showCols(grid) {
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);

    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
    }

}
;
function mComboPembayaran()
{
    var Field = ['KD_PAY', 'JENIS_PAY', 'PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});

    var cboPembayaran = new Ext.form.ComboBox
            (
                    {
                        id: 'cboPembayaran',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Pembayaran...',
                        labelWidth: 80,
                        align: 'Right',
                        store: dsComboBayar,
                        valueField: 'KD_PAY',
                        displayField: 'PAYMENT',
                        anchor: '100%',
                        enableKeyEvents:true,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tapungkd_pay = b.data.KD_PAY;
                                    },
                                    keydown:function(text,e)
                                    {
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            printbillRadLab_PA();
                                        }else if(e.keyCode == 123){
                                            e.preventDefault();
                                            printkwitansiRadLab_PA();
                                        }
                                    }
                                }
                    }
            );

    return cboPembayaran;
}
;
function getItemPanelmedreckasirLABPA(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Medrec ',
                                                name: 'txtNoMedrecDetransaksi',
                                                id: 'txtNoMedrecDetransaksi',
                                                readOnly: true,
                                                anchor: '99%',
                                                enableKeyEvents:true,
                                                listeners:
                                                        {
                                                            keydown:function(text,e){
                                                                if(e.keyCode == 122){
                                                                    e.preventDefault();
                                                                    printbillRadLab_PA();
                                                                }else if(e.keyCode == 123){
                                                                    e.preventDefault();
                                                                    printkwitansiRadLab_PA();
                                                                }
                                                            }
                                                        },
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: '',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtNamaPasienDetransaksi',
                                                id: 'txtNamaPasienDetransaksi',
                                                anchor: '100%',
                                                enableKeyEvents:true,
                                                listeners:
                                                        {
                                                            keydown:function(text,e){
                                                                if(e.keyCode == 122){
                                                                    e.preventDefault();
                                                                    printbillRadLab_PA();
                                                                }else if(e.keyCode == 123){
                                                                    e.preventDefault();
                                                                    printkwitansiRadLab_PA();
                                                                }
                                                            }
                                                        },
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getItemPanelInputKasirLABPA(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: true,
                height: 110,
                enableKeyEvents:true,
                        listeners:
                                {
                                    keydown:function(text,e){
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            alert('hai');
                                        }
                                    }
                                },
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getItemPanelNoTransksiLABPAKasir(lebar),
                                            getItemPanelmedreckasirLABPA(lebar), getItemPanelUnitKasirLABPA(lebar)
                                        ]
                            }
                        ]
            };
    return items;
}
;



function getItemPanelUnitKasirLABPA(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboJenisByrView()
                                        ]
                            }, {
                                columnWidth: .60,
                                layout: 'form',
                                labelWidth: 0.9,
                                border: false,
                                items:
                                        [
                                            mComboPembayaran()
                                        ]
                            }
                        ]
            }
    return items;
}
;

function RefreshDataKasirLABPAKasirDetail(no_transaksi,kd_kasir)
{
    var strKriteriaKasirLABPA = '';
     if (kd_kasir !== undefined) {
        strKriteriaKasirLABPA = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
    }else{
        strKriteriaKasirLABPA = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
    }
    //strKriteriaKasirLABPA = 'no_transaksi = ~' + no_transaksi + '~';

    dsTRDetailKasirLABPAKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailbayarLAB',
                                    param: strKriteriaKasirLABPA
                                }
                    }
            );
    return dsTRDetailKasirLABPAKasirList;
}
;
function GetDTLTRKasirLABPAGrid()
{
    var fldDetailLABPA = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'BAYARTR', 'DISCOUNT', 'PIUTANG','TAG'];

    dsTRDetailKasirLABPAKasirList = new WebApp.DataStore({fields: fldDetailLABPA})
    //RefreshDataKasirLABPAKasirDetail();
    gridDTLTRKasirLABPA = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Detail Pembayaran',
                        stripeRows: true,
                        id: 'gridDTLTRKasirLABPA',
                        store: dsTRDetailKasirLABPAKasirList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        height: 230,
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        cm: TRKasirLab_PAColumModel()
                    }
            );

    return gridDTLTRKasirLABPA;
}
;

function TRKasirLab_PAColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiKasirLABPA',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 250,
                            menuDisabled: true,
                            hidden: true

                        },{
                            id: 'coltagKasirLab_PA',
                            header: 'Tag',
                            dataIndex: 'TAG',
                            width:30,
                            xtype:'checkcolumn',
							checked:true
                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiKasirLABPA',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            id: 'colURUTKasirLABPA',
                            header: 'Urut',
                            dataIndex: 'URUT',
                            sortable: false,
                            hidden: true,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            hidden: true,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colQtyKasirLABPA',
                            header: 'Qty',
                            width: 91,
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                        },
                        {
                            id: 'colHARGAKasirLABPA',
                            header: 'Harga',
                            align: 'right',
                            hidden: false,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colPiutangKasirLABPA',
                            header: 'Puitang',
                            width: 80,
                            dataIndex: 'PIUTANG',
                            align: 'right',
                            //hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolPuitangLABPA',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProdukLABPA();
                                return formatCurrency(record.data.PIUTANG);
                            }
                        },
                        {
                            id: 'colTunaiKasirLABPA',
                            header: 'Tunai',
                            width: 80,
                            dataIndex: 'BAYARTR',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolTunaiLABPA',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProdukLABPA();
                                
                                return formatCurrency(record.data.BAYARTR);
                            }

                        },
                        {
                            id: 'colDiscountKasirLABPA',
                            header: 'Discount',
                            width: 80,
                            dataIndex: 'DISCOUNT',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolDiscountLABPA',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.DISCOUNT);
                            }
                        }

                    ]
                    )
}
;

function getParamDetailTransaksiKasirLABPA()
{
    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    }
    ;

    var params =
            {
                kdUnit: vkd_unit,
                TrKodeTranskasi: Ext.get('txtNoTransaksiKasirLABPAKasir').getValue(),
                Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
                Shift: tampungshiftsekarang,
                kdKasir: kdkasir,
                bayar: tapungkd_pay,
                Flag: vflag,
                Typedata: tampungtypedata,
                Totalbayar: getTotalDetailProdukLABPA(),
                List: getArrDetailTrKasirLABPA(),
                JmlField: mRecordKasirLABPA.prototype.fields.length - 4,
                JmlList: GetListCountDetailTransaksiLABPA(),
                Hapus: 1,
                Ubah: 0,
                TglBayar : Ext.get('dtpTanggalDetransaksi').dom.value,
            };
    return params
}
;

function getArrDetailTrKasirLABPA()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirLABPAKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirLABPAKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirLABPAKasirList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirLABPAKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.QTY
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.BAYARTR
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.PIUTANG
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.DISCOUNT



            if (i === (dsTRDetailKasirLABPAKasirList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }

    return x;
}
;

function ValidasiEntryCMKasirLABPA(modul, mBolHapus)
{
    var x = 1;

    if ((Ext.get('txtNoTransaksiKasirLABPAKasir').getValue() == '')

            || (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
            || (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
            || (Ext.get('dtpTanggalDetransaksi').getValue() == '')
            || dsTRDetailKasirLABPAKasirList.getCount() === 0
            || (Ext.get('cboPembayaran').getValue() == '')
            || (Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...'))
    {
        if (Ext.get('txtNoTransaksiKasirLABPAKasir').getValue() == '' && mBolHapus === true)
        {
            x = 0;
        } else if (Ext.get('cboPembayaran').getValue() == '' || Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...')
        {
            ShowPesanWarningPenJasLab_PA(('Data pembayaran tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasLab_PA(('Data no. medrec tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasLab_PA('Nama pasien belum terisi', modul);
            x = 0;
        } else if (Ext.get('dtpTanggalDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasLab_PA(('Data tanggal kunjungan tidak boleh kosong'), modul);
            x = 0;
        }
        //cboPembayaran

        else if (dsTRDetailKasirLABPAKasirList.getCount() === 0)
        {
            ShowPesanWarningPenJasLab_PA('Data dalam tabel kosong', modul);
            x = 0;
        }
        ;
    }
    ;
    return x;
}
;
function Datasave_KasirLABPAKasir(mBol)
{
    if (ValidasiEntryCMKasirLABPA(nmHeaderSimpanData, false) == 1)
    {
        Ext.Ajax.request
                (
                        {
                            //url: "./Datapool.mvc/CreateDataObj",
                            url: baseURL + "index.php/lab_pa/functionLABPA/savepembayaran",
                            params: getParamDetailTransaksiKasirLABPA(),
                            failure: function (o)
                            {
                                ShowPesanWarningPenJasLab_PA('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                //RefreshDataFilterKasirLABPAKasir();
                            },
                            success: function (o)
                            {
                                /*  RefreshDatahistoribayar_LABPA('0');
                                RefreshDataPenJasLab_PADetail('0'); */
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
									Ext.getCmp('btnSimpanKasirLABPA').disable();
									 tombol_bayar='disable';
									 Ext.getCmp('btnTransferKasirLABPA').disable();
                                    ShowPesanInfoPenJasLab_PA("Pembayaran berhasil", "Pembayaran");
                                    Ext.getCmp('txtNoTransaksiKasirLABPAKasir').focus();
                                    RefreshDatahistoribayar_LABPA(Ext.get('txtNoTransaksiKasirLABPAKasir').dom.value);
                                    RefreshDataKasirLABPAKasirDetail(Ext.get('txtNoTransaksiKasirLABPAKasir').dom.value,kd_kasir_labpa);
                                    if (radiovaluesLABPA !=='3' && combovalues !== 'Transaksi Baru'){
                                        validasiJenisTrLABPA();
                                    }
                                    
                                    //FormLookUpsdetailTRPenjasLABPA.close();
                                    //ViewGridBawahLookupPenjasLab_PA(Ext.getCmp('txtNoTransaksiKasirLABPAKasir').getValue(),kd_kasir_labpa);
                                    //RefreshDataKasirLABPAKasir();
                                    /* if (mBol === false)
                                    {
                                        ViewGridBawahLookupPenjasLab_PA();
                                    }
                                    ; */
                                } else
                                {
                                    ShowPesanWarningPenJasLab_PA('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                }
                                ;
                            }
                        }
                )

    } else
    {
        if (mBol === true)
        {
            return false;
        }
        ;
    }
    ;

}

function TransferLookUp_labpa(rowdata)
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_Lab_PA = new Ext.Window
            (
                    {
                        id: 'gridTransfer_Lab_PA',
                        title: 'Transfer',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 410,
                        border: false,
                        resizable: false,
                        plain: false,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryTRTransfer_labpa(lebar),
                        listeners:
                                {
                                     activate: function()
                                    {
                                        Ext.getCmp('cboalasan_transferLABPA').setValue('Pembayaran Disatukan');                                        
                                    }
                                }
                    }
            );

    FormLookUpsdetailTRTransfer_Lab_PA.show();
    //  Transferbaru();

};
function getParamTransferRwi_dari_labpa()
{

/*
       KDkasirIGD:'01',
        TrKodeTranskasi: notransaksi,
        KdUnit: kodeunit,
        Kdpay: kdpaytransfer,
        Jumlahtotal: Ext.get(txtjumlahbiayasalLABPA).dom.value,
        Tglasal:  ShowDate(tgltrans),
        Shift: tampungshiftsekarang,
        TRKdTransTujuan:Ext.get(txtTranfernoTransaksiRWJ).dom.value,
        KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRWJ).dom.value,
        TglTranasksitujuan : Ext.get(dtpTanggaltransaksiRujuanRWJ).dom.value,
        KDunittujuan : Trkdunit2,
        KDalasan :Ext.get(cboalasan_transferLABPA).dom.value,
        KasirRWI:'05',
        Kdcustomer:kdcustomeraa,
        kodeunitkamar:tmp_kodeunitkamar,
        kdspesial:tmp_kdspesial,
        nokamar:tmp_nokamar,


*/
    var params =
            {
                KDkasirIGD: kdkasir,
                Kdcustomer: vkode_customer,
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                Kdpay: kdpaytransfer,
                Jumlahtotal: Ext.get(txtjumlahbiayasalLABPA).dom.value,
                Tglasal: ShowDate(TglTransaksi),
                Shift: tampungshiftsekarang,
                TRKdTransTujuan: Ext.get(txtTranfernoTransaksiLABPA).dom.value,
                KdpasienIGDtujuan: Ext.get(txtTranfernomedrecLABPA).dom.value,
                TglTranasksitujuan: Ext.get(dtpTanggaltransaksiRujuanLABPA).dom.value,
                KDunittujuan: Trkdunit2,
                KDalasan: Ext.get(cboalasan_transferLABPA).dom.value,
                KasirRWI: kdkasirasal_labpa,
                kodeunitkamar:tmp_kodeunitkamar_LABPA,
                kdspesial:tmp_kdspesial_LABPA,
                nokamar:tmp_nokamar_LABPA,
                TglTransfer:Ext.get('dtpTanggalDetransaksi').dom.value,

            };
    return params
}
;
function TransferData_labpa(mBol)
{
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/getcurrentshift",
         params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
             var cst = Ext.decode(o.responseText);
            
        },      
        success: function(o) {
            tampungshiftsekarang=o.responseText
            
        }
        
    
    });
	var UrlTransferLABPA="";
	/* if (radiovaluesLABPA === 2 || radiovaluesLABPA ==='2'){
		UrlTransferLABPA="index.php/lab_pa/functionLABPA/saveTransferRWI";
	}else{ */
		UrlTransferLABPA="index.php/lab_pa/functionLABPA/saveTransfer";
	//}
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + UrlTransferLABPA,//"index.php/lab_pa/functionLABPA/saveTransfer",
                        params: getParamTransferRwi_dari_labpa(),
                        failure: function (o)
                        {

                            ShowPesanWarningPenJasLab_PA('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            ViewGridBawahLookupPenjasLab_PA();
                        },
                        success: function (o)
                        {   
                        
                            
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {   Ext.getCmp('btnSimpanKasirLABPA').disable();
								tombol_bayar='disable';
                                Ext.getCmp('btnTransferKasirLABPA').disable();
                                ShowPesanInfoPenJasLab_PA('Transfer Berhasil', 'transfer ');
                                Ext.getCmp('txtNoTransaksiKasirLABPAKasir').focus();
                                RefreshDatahistoribayar_LABPA(Ext.getCmp('txtNoTransaksiPenJasLab_PA').getValue());
                                FormLookUpsdetailTRTransfer_Lab_PA.close();
                                FormLookUpsdetailTRPenjasLABPA.close();
                                
                            } else
                            {
                                if(cst.message != undefined){
                                    ShowPesanWarningPenJasLab_PA(cst.message, 'Gagal');
                                }else{
                                    ShowPesanWarningPenJasLab_PA('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                                }
                                
                            }
                            ;
                        }
                    }
            )

}
;

function getItemPanelButtonTransfer_labpa(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                height: 30,
                anchor: '100%',
                style: {'margin-top': '-1px'},
                items:
                        [
                            {
                                layout: 'hBox',
                                width: 400,
                                border: false,
                                bodyStyle: 'padding:5px 0px 5px 5px',
                                defaults: {margins: '3 3 3 3'},
                                anchor: '90%',
                                layoutConfig:
                                        {
                                            align: 'middle',
                                            pack: 'end'
                                        },
                                items:
                                        [
                                            {
                                                xtype: 'button',
                                                text: 'Simpan',
                                                width: 70,
                                                style: {'margin-left': '0px', 'margin-top': '0px'},
                                                hideLabel: true,
                                                id: 'btnOkTransfer_labpa',
                                                handler: function ()
                                                {
                                                    loadMask.show();
                                                    TransferData_labpa(false);
                                                    loadMask.hide();
                                                     Ext.getCmp('btnSimpanKasirLABPA').disable();
													 tombol_bayar='disable';
                                                     Ext.getCmp('btnTransferKasirLABPA').disable();
                                                    
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                text: 'Tutup',
                                                width: 70,
                                                hideLabel: true,
                                                id: 'btnCancelTransfer_labpa',
                                                handler: function ()
                                                {
                                                    FormLookUpsdetailTRTransfer_Lab_PA.close();
                                                }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getFormEntryTRTransfer_labpa(lebar)
{
    var pnlTRTransfer = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRTransfer',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 425,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputTransfer_LABPA(lebar), getItemPanelButtonTransfer_labpa(lebar)],
                        tbar:
                                [
                                ]
                    }
            );

    var FormDepanTransfer = new Ext.Panel
            (
                    {
                        id: 'FormDepanTransfer',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        items:
                                [
                                    pnlTRTransfer

                                ]

                    }
            );

    return FormDepanTransfer
}
;

function mComboTransferTujuan()
{
    var cboTransferTujuan = new Ext.form.ComboBox
            (
                    {
                        id: 'cboTransferTujuan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        hidden: true,
                        fieldLabel: 'Transfer',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Gawat Darurat'], [2, 'Rawat Inap'], [3, 'Rawat Jalan']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: tranfertujuan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tranfertujuan = b.data.displayText;
                                        //RefreshDataSetDokter();
                                        ViewGridBawahLookupPenjasLab_PA();

                                    }
                                }
                    }
            );
    return cboTransferTujuan;
}
;

function getTransfertujuan_labpa(lebar)
{
    var items =
            {
                Width: lebar - 2,
                height: 170,
                layout: 'form',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                labelWidth: 130,
                items:
                        [
                            {
                                xtype: 'tbspacer',
                                height: 2
                            },
                            mComboTransferTujuan(),
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Transaksi',
                                //maxLength: 200,
                                name: 'txtTranfernoTransaksiLABPA',
                                id: 'txtTranfernoTransaksiLABPA',
                                labelWidth: 130,
                                readonly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                id: 'dtpTanggaltransaksiRujuanLABPA',
                                name: 'dtpTanggaltransaksiRujuanLABPA',
                                format: 'd/M/Y',
                                readOnly: true,
                                //  value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Medrec',
                                //maxLength: 200,
                                name: 'txtTranfernomedrecLABPA',
                                id: 'txtTranfernomedrecLABPA',
                                labelWidth: 130,
                                readOnly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama Pasien',
                                //maxLength: 200,
                                name: 'txtTranfernamapasienLABPA',
                                id: 'txtTranfernamapasienLABPA',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Unit Perawatan',
                                //maxLength: 200,
                                name: 'txtTranferunitLABPA',
                                id: 'txtTranferunitLABPA',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        name: 'txtTranferkelaskamar_LABPA',
                                        id: 'txtTranferkelaskamar_LABPA',
                                        readOnly : true,
                                        labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                            }
                        ]
            }
    return items;
}
;

function getItemPanelNoTransksiTransferLABPA(lebar)
{
    var items =
            {
                Width: lebar,
                height: 110,
                layout: 'column',
                border: true,
                items:
                        [
                            {
                                columnWidth: .990,
                                layout: 'form',
                                Width: lebar - 10,
                                labelWidth: 130,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah Biaya',
                                                maxLength: 200,
                                                name: 'txtjumlahbiayasalLABPA',
                                                id: 'txtjumlahbiayasalLABPA',
                                                width: 100,
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Paid',
                                                maxLength: 200,
                                                name: 'txtpaidLABPA',
                                                id: 'txtpaidLABPA',
                                                readOnly: true,
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                value: 0,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah dipindahkan',
                                                maxLength: 200,
                                                name: 'txtjumlahtranferLABPA',
                                                id: 'txtjumlahtranferLABPA',
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Saldo tagihan',
                                                maxLength: 200,
                                                name: 'txtsaldotagihanLABPA',
                                                id: 'txtsaldotagihanLABPA',
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                value: 0,
                                                width: 100,
                                                anchor: '95%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function mComboalasan_transferLABPA()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_transferLABPA = new WebApp.DataStore({fields: Field});
    dsalasan_transferLABPA.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_alasan',
                                    Sortdir: 'ASC',
                                    target: 'ComboAlasanTransfer',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboalasan_transferLABPA = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_transferLABPA',
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Alasan Transfer ',
                        align: 'Right',
                        width: 100,
                        editable: false,
                        anchor: '100%',
                        store: dsalasan_transferLABPA,
                        valueField: 'ALASAN',
                        displayField: 'ALASAN',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                    }

                                }
                    }
            );

    return cboalasan_transferLABPA;
};

function getItemPanelInputTransfer_LABPA(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: false,
                height: 330,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                height: 330,
                                border: false,
                                items:
                                        [
                                            getTransfertujuan_labpa(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 5
                                            },
                                            getItemPanelNoTransksiTransferLABPA(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            mComboalasan_transferLABPA()

                                        ]
                            },
                        ]
            };
    return items;
}
;
function showhide_unit(kode)
{
    if(kode==='05')
    {
    Ext.getCmp('txtTranferunitLABPA').hide();
    Ext.getCmp('txtTranferkelaskamar_LABPA').show();

    }else{
    Ext.getCmp('txtTranferunitLABPA').show();
    Ext.getCmp('txtTranferkelaskamar_LABPA').hide();
    }
}
function getFormEntryPenjasBayarLABPA(lebar)
{
    var pnlTRKasirLABPA = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirLABPA',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 130,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        enableKeyEvents:true,
                        listeners:
                                {
                                    keydown:function(text,e){
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            alert('hai');
                                        }
                                    }
                                },
                        items: [getItemPanelInputKasirLABPA(lebar)],
                        tbar:
                                [
                                ]
                    }
            );
    var x;
    var paneltotalLABPA = new Ext.Panel
            (
                    {
                        id: 'paneltotalLABPA',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        height: 67,
                        anchor: '100% 8.0000%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        html: " Total :"
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirLABPA',
                                                        name: 'txtJumlah2EditData_viKasirLABPA',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        hidden: true,
                                                        html: " Bayar :"
                                                    },
                                                    {
                                                        xtype: 'numberfield',
                                                        id: 'txtJumlahEditData_viKasirLABPA',
                                                        name: 'txtJumlahEditData_viKasirLABPA',
                                                        hidden: true,
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        //readOnly: true,
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                ]
                    }
            )
    var GDtabDetailKasirLABPA = new Ext.Panel
            (
                    {
                        id: 'GDtabDetailKasirLABPA',
                        region: 'center',
                        activeTab: 0,
                        height: 380,
                        anchor: '100% 100%',
                        border: false,
                        plain: true,
                        defaults: {autoScroll: true},
                        items: [GetDTLTRKasirLABPAGrid(), paneltotalLABPA],
                        tbar:
                                [
                                    {
                                        text: 'Bayar',
                                        id: 'btnSimpanKasirLABPA',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        handler: function ()
                                        {
                                            Datasave_KasirLABPAKasir(false);
                                           // FormDepan2KasirLABPA.close();
                                            Ext.getCmp('btnSimpanKasirLABPA').disable();
											tombol_bayar='disable';
                                            Ext.getCmp('btnTransferKasirLABPA').disable();
                                            //Ext.getCmp('btnHpsBrsKasirLABPA').disable();
                                        }
                                    },
                                    
                                    {
                                        id: 'btnTransferKasirLABPA',
                                        text: 'Transfer',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                        /* disabled: true, */
                                        handler: function (sm, row, rec)
                                        {
                                            //Disini
                                            TransferLookUp_labpa();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/GetPasienTranferLab_rad",
                                                params: {
                                                    notr: notransaksiasal_labpa,
                                                    notransaksi: Ext.getCmp('txtNoTransaksiKasirLABPAKasir').getValue(),//tmp_NoTransaksi,
                                                    kdkasir: kdkasirasal_labpa
                                                },
                                                success: function (o)
                                                {   
                                                showhide_unit(kdkasirasal_labpa);
                                                    if (o.responseText == "") {
                                                        ShowPesanWarningPenJasLab_PA('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                    } else {
                                                        var tmphasil = o.responseText;
                                                        var tmp = tmphasil.split("<>");
                                                        if (tmp[5] == undefined && tmp[3] == undefined) {
                                                            ShowPesanWarningPenJasLab_PA('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                            FormLookUpsdetailTRTransfer_Lab_PA.close();
                                                        } else {
                                                            console.log(tmp[12]);
                                                            Ext.get(txtTranfernoTransaksiLABPA).dom.value = tmp[3];
                                                            Ext.get(dtpTanggaltransaksiRujuanLABPA).dom.value = ShowDate(tmp[4]);
                                                            Ext.get(txtTranfernomedrecLABPA).dom.value = tmp[5];
                                                            Ext.get(txtTranfernamapasienLABPA).dom.value = tmp[2];
                                                            Ext.get(txtTranferunitLABPA).dom.value = tmp[1];
                                                            Trkdunit2 = tmp[6];
                                                            Ext.get(txtTranferkelaskamar_LABPA).dom.value=tmp[9];
                                                            Ext.get(txtjumlahbiayasalLABPA).dom.value=tmp[12];
                                                            Ext.get(txtjumlahtranferLABPA).dom.value=tmp[12];
                                                            
                                                            tmp_kodeunitkamar_LABPA=tmp[8];
                                                            tmp_kdspesial_LABPA=tmp[10];
                                                            tmp_nokamar_LABPA=tmp[11];
                                                            var kasir = tmp[7];
                                                            Ext.Ajax.request({
                                                                url: baseURL + "index.php/main/GettotalTranfer",
                                                                params: {
                                                                    notr: notransaksi,
                                                                    kd_kasir:kdkasir
                                                                },
                                                                success: function (o)
                                                                {
                                                                    console.log(o);
                                                                    Ext.get(txtjumlahbiayasalLABPA).dom.value = formatCurrency(o.responseText);
                                                                    Ext.get(txtjumlahtranferLABPA).dom.value = formatCurrency(o.responseText);
                                                                    /* Ext.get(txtjumlahbiayasalLABPA).dom.value=tmp[12];
                                                                    Ext.get(txtjumlahtranferLABPA).dom.value=tmp[12]; */
                                                                }
                                                            });
                                                        }

                                                    }
                                                }
                                            });

                                        }, //disabled :true
                                    },
                                    mCombo_printer_kasirlabpa(),
                                    {
                                        xtype: 'splitbutton',
                                        text: 'Cetak',
                                        iconCls: 'print',
                                        id: 'btnPrint_viDaftar',
                                        handler: function ()
                                        {
                                        },
                                        menu: new Ext.menu.Menu({
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
                                                            id: 'btnPrintBillRadLab_PA',
                                                            handler: function ()
                                                            {
                                                                /* if (Ext.getCmp('cbopasienorder_printer_kasirlabpa').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirlabpa').getValue()===undefined){
                                                                    ShowPesanWarningPenJasLab_PA('Pilih printer terlebih dahulu !', 'Cetak Data');
                                                                }else{ */
                                                                    printbillRadLab_PA();
                                                                //}
                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Kwitansi',
                                                            id: 'btnPrintKwitansiRadLab_PA',
                                                            handler: function ()
                                                            {
                                                                /* if (Ext.getCmp('cbopasienorder_printer_kasirlabpa').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirlabpa').getValue()===undefined){
                                                                    ShowPesanWarningPenJasLab_PA('Pilih printer terlebih dahulu !', 'Cetak Data');
                                                                }else{ */
                                                                    printkwitansiRadLab_PA();
                                                                //}
                                                            }
                                                        }
                                                    ]
                                        })
                                    }
                                ]
                    }
            );



    var pnlTRKasirLABPA2 = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirLABPA2',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 380,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [GDtabDetailKasirLABPA,
                        ]
                    }
            );




    FormDepan2KasirLABPA = new Ext.Panel
            (
                    {
                        id: 'FormDepan2KasirLABPA',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                height: 380,
                        shadhow: true,
                        items: [pnlTRKasirLABPA, pnlTRKasirLABPA2,
                        ]
                    }
            );

    return FormDepan2KasirLABPA
}
function paramUpdateTransaksi()
{
    var params =
            {
                TrKodeTranskasi: Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,
                KDkasir: kdkasir
            };
    return params
}
;
function UpdateTutuptransaksi(mBol)
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/ubah_co_status_transksi",
                        params: paramUpdateTransaksi(),
                        failure: function (o)
                        {
                            ShowPesanInfoPenJasLab_PA('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                            validasiJenisTrLABPA();
                        },
                        success: function (o)
                        {
                            //RefreshDatahistoribayar_LABPA(Ext.get('cellSelectedtutup);

                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoPenJasLab_PA(nmPesanSimpanSukses, nmHeaderSimpanData);

                                //RefreshDataKasirLABPAKasir();
                                if (mBol === false)
                                {
                                    validasiJenisTrLABPA();
                                }
                                ;
                                cellSelecteddeskripsi = '';
                            } else
                            {
                                ShowPesanInfoPenJasLab_PA('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                                cellSelecteddeskripsi = '';
                            }
                            ;
                        }
                    }
            )
}
;
function RecordBaruLABPA()
{
    var TanggalTransaksiGrid;
    //alert(TglTransaksi);
    /* if (TglTransaksi === undefined)
    {
        TanggalTransaksiGrid=tglGridBawah;
    }else{
        TanggalTransaksiGrid=TglTransaksi;
    } */
    var TanggalTransaksiGrid = Ext.getCmp('dtpKunjunganLABPA').getValue();
    var p = new mRecordLABPA
    (
        {
            'cito':'',
            'kd_tarif':'',
            'kd_produk':'',
            'kp_produk':'',
            'deskripsi':'',
            'tgl_transaksi':TanggalTransaksiGrid,
            'tgl_berlaku':null,
            'harga':'',
            'qty':'',
            'jumlah':''
            
        }
    );
    
    return p;
};
function TambahBarisLABPA()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruLABPA();
        dsTRDetailPenJasLab_PAList.insert(dsTRDetailPenJasLab_PAList.getCount(), p);
    };
};
function getFormEntryPenJasLab_PA(lebar,data) 
{
    console.log(lebar);
    var pnlTRLABPA = new Ext.FormPanel
    (
        {
            id: 'PanelTRPenJasLab_PA',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding: 5px 5px 5px 5px',
            height:285,
            width: lebar,
            border: false,
            items: [getItemPanelPenJasLab_PA(lebar)],
            tbar:
            [
                {
                        text: 'Tambah Baru',
                        id: 'btnTambahBaruLABPA',
                        tooltip: nmSimpan,
                        iconCls: 'add',
                        handler: function()
                        {
                            LABPAAddNew();
                            ViewGridBawahLookupPenjasLab_PA();
                            Ext.getCmp('txtNamaPasienLABPA').focus();                 
                            Ext.getCmp('txtnotlplabpa').setReadOnly(false);                 
                        }
                },  
                //'-',
                {
                        text: 'Simpan',
                        id: 'btnSimpanLABPA',
                        tooltip: nmSimpan,
                        iconCls: 'save',
                        hidden:true,
                        handler: function()
                        {
                            var tmp_nomer_lab = Ext.getCmp('txtNomorLab_PA').getValue();
                            if (tmp_nomer_lab.length > 0) {
                                loadMask.show();
                                Datasave_PenJasLab_PA(false); 
                                loadMask.hide();
                            }else{
                                Ext.Msg.confirm('Warning', 'Nomer LAB masih kosong, apakah anda yakin untuk melanjutkan pembayaran?', function(button){
                                    if (button == 'yes'){
                                        loadMask.show();
                                        Datasave_PenJasLab_PA(false); 
                                        loadMask.hide();
                                    }else{
                                        Ext.getCmp('txtNomorLab_PA').focus();
                                    }
                                });
                            }                       
                            // Datasave_PenJasLab_PA_SQL();                                  
                        }
                },  
                
                '-',
                {
                    id: 'btnPembayaranPenjasLABPA',
                    text: 'Pembayaran',
                    tooltip: nmEditData,
                    iconCls: 'Edit_Tr',
                    handler: function (sm, row, rec)
                    {
                                    if (rowSelectedPenJasLab_PA != undefined) {
                                        PenjasLookUpLABPA(rowSelectedPenJasLab_PA.data);
                                     } else {
                                        PenjasLookUpLABPA();
                                        //ShowPesanWarningPenJasLab_PA('Pilih data tabel  ', 'Pembayaran');
                                    } 
                    }//, disabled: true
                }, //'-',
                {
                    text: 'Tutup Transaksi',
                    id: 'btnTutupTransaksiPenjasLABPA',
                    tooltip: nmHapus,
                    iconCls: 'remove',
					//hidden : true,
                    handler: function ()
                    {
                        if (noTransaksiPilihan == '' || noTransaksiPilihan == 'undefined') {
                            ShowPesanWarningPenJasLab_PA('Pilih data tabel  ', 'Pembayaran');
                        } else {
                            UpdateTutuptransaksi(false);
                            Ext.getCmp('btnPembayaranPenjasLABPA').disable();
                            Ext.getCmp('btnTutupTransaksiPenjasLABPA').disable();
                            Ext.getCmp('btnHpsBrsItemLab_PA').disable();
                        }
                    }//, disabled: true
                }   
            ]
        }
    );
 var x;
 var GDtabDetailLABPA = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailLABPA',
        region: 'center',
        activeTab: 0,
        height:250,
        width:855,
        //anchor: '100%',
        border:true,
        plain: true,
        defaults:
        {
            autoScroll: true
        },
        items: [GridDetailItemPemeriksaan(),GetDTLTRHistoryGrid()],
        
            listeners:
            {   
                'tabchange' : function (panel, tab) {
                    if (x ==1)
                    {
                        Ext.getCmp('btnLookupPenJasLab_PA').hide()
                        //Ext.getCmp('btnSimpanLABPA').hide()
                        Ext.getCmp('btnHpsBrsItemLab_PA').hide()
                        x=2;
                        return x;
                    }else 
                    {   
                        Ext.getCmp('btnLookupPenJasLab_PA').show()
                        //Ext.getCmp('btnSimpanLABPA').show()
                        Ext.getCmp('btnHpsBrsItemLab_PA').show()
                        x=1;    
                        return x;
                    }

                }
            }
        }
        
    );
    
   
   
   var pnlTRLABPA2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRPenJasLab_PA2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:460,
            //anchor: '100%',
            width: lebar,
            border: false,
            items: [    GDtabDetailLABPA
            
            ]
        }
    );

    
   
   
    var FormDepanPenJasLab_PA = new Ext.Panel
    (
        {
            id: 'FormDepanPenJasLab_PA',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: true,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: 
            [
                pnlTRLABPA,pnlTRLABPA2  
            ]

        }
    );
    return FormDepanPenJasLab_PA
};

function DataDeletePenJasRadDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeletePenJasRadDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoPenJasLab_PA(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailPenJasLab_PAList.removeAt(CurrentPenJasLab_PA.row);
                    cellSelecteddeskripsi=undefined;
                    ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,kd_kasir_labpa);
                    AddNewPenJasRad = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningPenJasLab_PA(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningPenJasLab_PA(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeletePenJasRadDetail()
{
    var params =
    {//^^^
        Table: 'ViewDetailTransaksiPenJasLab',
        TrKodeTranskasi: CurrentPenJasLab_PA.data.data.NO_TRANSAKSI,
        TrTglTransaksi:  CurrentPenJasLab_PA.data.data.TGL_TRANSAKSI,
        TrKdPasien :     CurrentPenJasLab_PA.data.data.KD_PASIEN,
        TrKdNamaPasien : Ext.get('txtNamaPasienLABPA').getValue(),
        //TrAlamatPasien : Ext.get('txtAlamatLABPA').getValue(),    
        TrKdUnit :       Ext.get('txtKdUnitLab_PA').getValue(),
        TrNamaUnit :     Ext.get('txtNamaUnitLABPA').getValue(),
        Uraian :         CurrentPenJasLab_PA.data.data.DESKRIPSI2,
        AlasanHapus :    variablehistori,
        TrHarga :        CurrentPenJasLab_PA.data.data.HARGA,
        
        TrKdProduk :     CurrentPenJasLab_PA.data.data.KD_PRODUK,
        RowReq: CurrentPenJasLab_PA.data.data.URUT,
        Hapus:2
    };
    
    return params
};

function getParamDataupdatePenJasRadDetail()
{
    var params =
    {
        Table: 'ViewDetailTransaksiPenJasLab',
        TrKodeTranskasi: CurrentPenJasLab_PA.data.data.NO_TRANSAKSI,
        RowReq: CurrentPenJasLab_PA.data.data.URUT,

        Qty: CurrentPenJasLab_PA.data.data.QTY,
        Ubah:1
    };
    
    return params
};

function GridDetailItemPemeriksaan() 
{
    var fldDetailLABPA = ['kp_produk','kd_produk','deskripsi','deskripsi2','kd_tarif','harga','qty','desc_req','tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi','kd_unit','kd_kasir'];
    
    dsTRDetailPenJasLab_PAList = new WebApp.DataStore({ fields: fldDetailLABPA })
    //ViewGridBawahLookupPenjasLab_PA() ;
    gridDTItemTest = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Item Test',
            id: 'gridDTItemTest',
            stripeRows: true,
            store: dsTRDetailPenJasLab_PAList,
            border: false,
            columnLines: true,
            frame: false,
            width:50,
            height:100,
            //anchor: '100%',
            autoScroll:true,
            viewConfig: {forceFit: true},
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailPenJasLab_PAList.getAt(row);
                            CurrentPenJasLab_PA.row = row;
                            CurrentPenJasLab_PA.data = cellSelecteddeskripsi;
                            // console.log(cellSelecteddeskripsi);
                        }
                    }
                }
            ),
            
            cm: TRPenJasLab_PAColumModel(),
            tbar:
            [
                {
                    text: 'Tambah Baris',
                    id: 'btnTambahBarisPenJasLab_PA',
                    tooltip: nmLookup,
                    iconCls: 'Edit_Tr',
                    handler: function()
                    {
                        TambahBarisLABPA()
                        
                    }
                },{
                    text: 'Tambah Item Pemeriksaan (Lookup)',
                    id: 'btnLookupPenJasLab_PA',
                    tooltip: nmLookup,
                    iconCls: 'find',
                    handler: function()
                    {
                        getTindakanLab_PA();
                        setLookUp_getTindakanPenjasLab_PA();
                        
                    }
                },
                {
                        id:'btnHpsBrsItemLab_PA',
                        text: 'Hapus Baris',
                        tooltip: 'Hapus Baris',
                        disabled:false,
                        iconCls: 'RemoveRow',
                        handler: function()
                        {
                            if (dsTRDetailPenJasLab_PAList.getCount() > 0 ) {
                                if (cellSelecteddeskripsi != undefined)
                                {
                                    Ext.Msg.confirm('Warning', 'Apakah item pemeriksaan ini akan dihapus?', function(button){
                                        if (button == 'yes'){
                                            if(CurrentPenJasLab_PA != undefined) {
                                                loadMask.show();
                                                var line = gridDTItemTest.getSelectionModel().selection.cell[0];
                                                Ext.Ajax.request
                                                 (
                                                    {
                                                        url: baseURL + "index.php/lab_pa/functionLABPA/deletedetaillab",
                                                        params: getParamHapusDetailTransaksiLABPA(),
                                                        failure: function(o)
                                                        {
                                                            ShowPesanWarningPenJasLab_PA('Error simpan. Hubungi Admin!', 'Gagal');
                                                            loadMask.hide();
                                                        },  
                                                        success: function(o) 
                                                        {
                                                            loadMask.hide();
                                                            var cst = Ext.decode(o.responseText);
                                                            if (cst.success === true && cst.cari===false) 
                                                            {
                                                                
                                                                ShowPesanInfoPenJasLab_PA('Data Berhasil Di hapus', 'Sukses');
                                                                dsTRDetailPenJasLab_PAList.removeAt(line);
                                                                ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,kd_kasir_labpa);
                                                                gridDTItemTest.getView().refresh();
                                                            }else if (cst.success === true && cst.cari===true)
                                                            {
                                                                dsTRDetailPenJasLab_PAList.removeAt(line);
                                                            }
                                                            else 
                                                            {
                                                                    ShowPesanWarningPenJasLab_PA('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
                                                            };
                                                        }
                                                    }
                                                )
                                                
                                            }
                                        }
                                    })
                                } else {
                                    ShowPesanWarningPenJasLab_PA('Pilih record ','Hapus data');
                                }
                            }
                        }
                },
                        
            ],
        }
        
        
    );
    
    

    return gridDTItemTest;
};

function TRPenJasLab_PAColumModel()
{

    checkColumn_penata__labpa = new Ext.grid.CheckColumn({
       header: 'Cito',
       dataIndex: 'cito',
       id: 'checkid',
       width: 55,
       renderer: function(value)
       {
        console.log("Grid");
        },
    });
    return new Ext.grid.ColumnModel
    (
        [ 
            new Ext.grid.RowNumberer(),
            {
                header: 'Cito',
                dataIndex: 'cito',
                width:65,
                menuDisabled:true,
                listeners:{
                    itemclick: function(dv, record, items, index, e)
                    {
                                    Ext.getCmp('btnHpsBrsItemLab_PAL').enable();
                                    console.log("test");
                    },
                },
                renderer:function (v, metaData, record)
                {
                    if ( record.data.cito=='0')
                    {
                    record.data.cito='Tidak'
                    }else if (record.data.cito=='1')
                    {
                    metaData.style  ='background:#FF0000;  "font-weight":"bold";';
                    record.data.cito='Ya'
                    }else if (record.data.cito=='Ya')
                    {
                    metaData.style  ='background:#FF0000;  "font-weight":"bold";';
                    }
                    
                    return record.data.cito; 
                },
                editor:new Ext.form.ComboBox
                ({
                    id: 'cboKasus',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    selectOnFocus: true,
                    forceSelection: true,
                    emptyText: 'Silahkan Pilih...',
                    width: 50,
                    anchor: '95%',
                    value: 1,
                    store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: ['Id', 'displayText'],
                        data: [[1, 'Ya'], [2, 'Tidak']]
                    }),
                    valueField: 'displayText',
                    displayField: 'displayText',
                    value       : '',
                       
                })
            },
            {
                header: 'kd_tarif',
                dataIndex: 'kd_tarif',
                width:250,
                menuDisabled:true,
                hidden :true

            },
            {
                header: 'KD Produk',
                dataIndex: 'kd_produk',
                width:100,
                menuDisabled:true,
                hidden:true
            },{
                header: 'Kode Produk',
                dataIndex: 'kp_produk',
                width:100,
                menuDisabled:true,
                //hidden:true,
                editor      : new Ext.form.TextField({
                    id                  : 'fieldcolKdProduk',
                    allowBlank          : true,
                    enableKeyEvents     : true,
                    width               : 30,
                    listeners           :
                    { 
                        'specialkey' : function(a, b)
                        {
                            if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
                                var kd_cus_gettarif=vkode_customer;
                                /* if(Ext.get('cboKelPasienLab_PA').getValue()=='Perseorangan'){
                                    kd_cus_gettarif=Ext.getCmp('cboPerseoranganLab_PA').getValue();
                                }else if(Ext.get('cboKelPasienLab_PA').getValue()=='Perusahaan'){
                                    kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryLab_PA').getValue();
                                }else {
                                    kd_cus_gettarif=Ext.getCmp('cboAsuransiLab_PA').getValue();
                                  } */
                                var modul='';
                                if(radiovaluesLABPA == 1){
                                    modul='igd';
                                } else if(radiovaluesLABPA == 2){
                                    modul='rwi';
                                } else if(radiovaluesLABPA == 4){
                                    modul='rwj';
                                } else{
                                    modul='langsung';
                                }
                                Ext.Ajax.request
                                ({
                                    store   : KasirLABPADataStoreProduk,
                                    url     : baseURL + "index.php/lab_pa/functionLABPA/getGridProdukKey",
                                    params  :  {
                                        kd_unit:kodeUnitLab_PA,
                                     kd_customer:kd_cus_gettarif,
                                     notrans:Ext.getCmp('txtNoTransaksiPenJasLab_PA').getValue(),
                                     penjas:modul,
                                     kdunittujuan:tmpkd_unit,
                                        text            : Ext.getCmp('fieldcolKdProduk').getValue(),
                                    },
                                    failure : function(o)
                                    {
                                        ShowPesanWarningPenJasLab_PA("Data produk tidak ada!",'WARNING');
                                    },
                                    success : function(o)
                                    {
                                        var cst = Ext.decode(o.responseText);
                                        if (cst.processResult == 'SUCCESS'){
                                            kd_produk           = cst.listData.kd_produk;
                                            kp_produk           = cst.listData.kp_produk;
                                            kd_tarif            = cst.listData.kd_tarif;
                                            deskripsi           = cst.listData.deskripsi;
                                            harga               = cst.listData.harga;
                                            tgl_berlaku         = cst.listData.tgl_berlaku;
                                            jumlah              = cst.listData.jumlah;
                                            var line = gridDTItemTest.getSelectionModel().selection.cell[0];
                                            
                                            /* dsTRDetailPenJasLab_PAList.getRange()[CurrentPenJasLab_PA.row].set('deskripsi', cst.listData.deskripsi);
                                            dsTRDetailPenJasLab_PAList.getRange()[CurrentPenJasLab_PA.row].set('qty', 1); */
                                            //alert(dsTRDetailPenJasLab_PAList.data.items[line].data.deskripsi);
                                            dsTRDetailPenJasLab_PAList.data.items[line].data.kd_produk         = cst.listData.kd_produk;
                                            dsTRDetailPenJasLab_PAList.data.items[line].data.kp_produk         = cst.listData.kp_produk;
                                            dsTRDetailPenJasLab_PAList.data.items[line].data.kd_klas           = cst.listData.kd_klas;
                                            dsTRDetailPenJasLab_PAList.data.items[line].data.kd_unit           = cst.listData.kd_unit;
                                            dsTRDetailPenJasLab_PAList.data.items[line].data.harga             = cst.listData.harga;
                                            dsTRDetailPenJasLab_PAList.data.items[line].data.deskripsi         = cst.listData.deskripsi;
                                            console.log(dsTRDetailPenJasLab_PAList.data.items[line]);
                                            dsTRDetailPenJasLab_PAList.data.items[line].data.tgl_berlaku       = cst.listData.tgl_berlaku;
                                            dsTRDetailPenJasLab_PAList.data.items[line].data.kd_tarif          = cst.listData.kd_tarif;
                                            //dsTRDetailPenJasLab_PAList.data.items[line].data.tgl_transaksi     = Tgltransaksi;
                                            dsTRDetailPenJasLab_PAList.data.items[line].data.qty               = '1';
                                            
                                            gridDTItemTest.getView().refresh();
                                            gridDTItemTest.startEditing(line, 9);
                                            
                                        }else{
                                            ShowPesanWarningPenJasLab_PA('Data produk tidak ada!','WARNING');
                                        };
                                    }
                                })
                            }
                        },
                    }
                }), 
            },
            /* {
                header:'Nama Produk',
                dataIndex: 'deskripsi',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320

            }, */
            {   id:'nama_pemereksaaan_labpa',
                dataIndex: 'deskripsi',
                header: 'Nama Pemeriksaan',
                sortable: true,
                menuDisabled:true,
                width: 320,
                /* editor:new Nci.form.Combobox.autoComplete({
                    store   : TrPenJasLab_PA.form.ArrayStore.produk,
                    select  : function(a,b,c){
                    //  console.log(b);
                    //Disini
                        Ext.Ajax.request
                        (
                            {
                                url: baseURL + "index.php/main/functionLAB/cekProduk",
                                params:{kd_labpa:b.data.kd_produk} ,
                                failure: function(o)
                                {
                                    ShowPesanErrorPenJasLab_PA('Hubungi Admin', 'Error');
                                },
                                success: function(o)
                                {
                                    Ext.getCmp('btnHpsBrsItemLab_PAL').enable();
                                    console.log("test");
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        var line = gridDTItemTest.getSelectionModel().selection.cell[0];
                                        dsTRDetailPenJasLab_PAList.data.items[line].data.deskripsi=b.data.deskripsi;
                                        dsTRDetailPenJasLab_PAList.data.items[line].data.uraian=b.data.uraian;
                                        dsTRDetailPenJasLab_PAList.data.items[line].data.kd_tarif=b.data.kd_tarif;
                                        dsTRDetailPenJasLab_PAList.data.items[line].data.kd_produk=b.data.kd_produk;
                                        dsTRDetailPenJasLab_PAList.data.items[line].data.tgl_transaksi=b.data.tgl_transaksi;
                                        dsTRDetailPenJasLab_PAList.data.items[line].data.tgl_berlaku=b.data.tgl_berlaku;
                                        dsTRDetailPenJasLab_PAList.data.items[line].data.harga=b.data.harga;
                                        dsTRDetailPenJasLab_PAList.data.items[line].data.qty=b.data.qty;
                                        dsTRDetailPenJasLab_PAList.data.items[line].data.jumlah=b.data.jumlah;

                                        gridDTItemTest.getView().refresh();
                                    }
                                    else
                                    {
                                        ShowPesanInfoPenJasLab_PA('Nilai normal item '+b.data.deskripsi+' belum tersedia', 'Information');
                                    };
                                }
                            }

                        )
                        
                    },
                    insert  : function(o){
                        return {
                            uraian          : o.uraian,
                            kd_tarif        : o.kd_tarif,
                            kd_produk       : o.kd_produk,
                            tgl_transaksi   : o.tgl_transaksi,
                            tgl_berlaku     : o.tgl_berlaku,
                            harga           : o.harga,
                            qty             : o.qty,
                            deskripsi       : o.deskripsi,
                            jumlah          : o.jumlah,
                            text            :  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_produk+'</td><td width="150">'+o.deskripsi+'</td></tr></table>'
                        }
                    },
                    param   : function(){
                    var kd_cus_gettarif;
                    if(Ext.get('cboKelPasienLab_PA').getValue()=='Perseorangan'){
                        kd_cus_gettarif=Ext.getCmp('cboPerseoranganLab_PA').getValue();
                    }else if(Ext.get('cboKelPasienLab_PA').getValue()=='Perusahaan'){
                        kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryLab_PA').getValue();
                    }else {
                        kd_cus_gettarif=Ext.getCmp('cboAsuransiLab_PA').getValue();
                          }
                    var params={};
                    params['kd_unit']=kodeUnitLab_PA;
                    params['kd_customer']=kd_cus_gettarif;
                    return params;
                    },
                    url     : baseURL + "index.php/main/functionLAB/getProduk",
                    valueField: 'deskripsi',
                    displayField: 'text',
                    listWidth: 210
                }) */
            },
            {
                header: 'Tanggal Transaksi',
                dataIndex: 'tgl_transaksi',
                width: 130,
                menuDisabled:true,
                renderer: function(v, params, record)
                {
                    if(record.data.tgl_transaksi == undefined){
                        record.data.tgl_transaksi=tglGridBawah;
                        return record.data.tgl_transaksi;
                    } else{
                        if(record.data.tgl_transaksi.substring(5, 4) == '-'){
                            return ShowDate(record.data.tgl_transaksi);
                        } else{
                            var tgl=record.data.tgl_transaksi.split("/");

                            if(tgl[2].length == 4 && isNaN(tgl[1])){
                                return record.data.tgl_transaksi;
                            } else{
                                return ShowDate(record.data.tgl_transaksi);
                            }
                        }

                    }
                }
            },
            {
                header: 'Tanggal Berlaku',
                dataIndex: 'tgl_berlaku',
                width: 130,
                menuDisabled:true,
                //hidden: true,
                renderer: function(v, params, record)
                {
                    if(record.data.tgl_berlaku == undefined || record.data.tgl_berlaku == null){
                        record.data.tgl_berlaku=tglGridBawah;
                        return record.data.tgl_berlaku;
                    } else{
                        if(record.data.tgl_berlaku.substring(5, 4) == '-'){
                            return ShowDate(record.data.tgl_berlaku);
                        } else{
                            var tglb=record.data.tgl_berlaku.split("-");

                            if(tglb[2].length == 4 && isNaN(tglb[1])){
                                return record.data.tgl_berlaku;
                            } else{
                                return ShowDate(record.data.tgl_berlaku);
                            }
                        }

                    } 
                }
            },
            {
                header: 'Harga',
                align: 'right',
                hidden: false,
                menuDisabled:true,
                dataIndex: 'harga',
                width:100,
                renderer: function(v, params, record)
                {
                    return formatCurrency(record.data.harga);
                }
            },
            {   id:'qty_labpa',
                header: 'Qty',
                width:91,
                align: 'right',
                menuDisabled:true,
                dataIndex: 'qty',
                editor      : new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemLABPA',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
                        listeners:
                        { 
                            'specialkey' : function(a, b)
                            {
                                if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
                                    var line = gridDTItemTest.getSelectionModel().selection.cell[0];
                                    
                                    TambahBarisLABPA();
                                    gridDTItemTest.startEditing(line+1, 4);
                                    loadMask.show();
                                    Datasave_PenJasLab_PA_LangsungTambahBaris(false); 
                                    loadMask.hide();    
                                }
                            },
                        }
                    }
                )
                   /*  {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                Dataupdate_PenJasRad(false);
                            }
                        }
                    } */
              //  ),



            },
             {
                header: 'Dokter',
                width:91,
                menuDisabled:true,
                dataIndex: 'jumlah',
                hidden: true,

            },

        ]
    )
};

/* function TRPenJasLab_PAColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsirwj',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
                menuDisabled:true,
                hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
                menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiRWJ',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320
                
            }
            ,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
               menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colHARGARWj',
                header: 'Harga',
                align: 'right',
                hidden: true,
                menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
                renderer: function(v, params, record) 
                            {
                            return formatCurrency(record.data.HARGA);
                            
                            }   
            },
            {
                id: 'colProblemRWJ',
                header: 'Qty',
                width:91,
                align: 'right',
                menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
                        listeners:
                            { 
                                'specialkey' : function()
                                {
                                    
                                            Dataupdate_PenJasRad(false);

                                }
                            }
                    }
                ),
                
                
              
            },

            {
                id: 'colImpactRWJ',
                header: 'CR',
                width:80,
                dataIndex: 'IMPACT',
                hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactRWJ',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
                
            }

        ]
    )
}; */


function RecordBaruLABPA()
{
    var tgltranstoday = Ext.getCmp('dtpKunjunganLABPA').getValue();
    var p = new mRecordLABPA
    (
        {
            'DESKRIPSI2':'',
            'KD_PRODUK':'',
            'DESKRIPSI':'', 
            'KD_TARIF':'', 
            'HARGA':'',
            'QTY':'',
            'TGL_TRANSAKSI':tgltranstoday.format('Y/m/d'), 
            'DESC_REQ':'',
            'KD_TARIF':'',
            'URUT':''
        }
    );
    
    return p;
};
var kd_kasir_labpa;
//----------MEMASUKAN DATA YG DIPILIH DARI DATA GRID KEDALAM LOOKUP EDIT DATA PASIEN
function TRLABPAInit(rowdata)
{
    //alert(rowdata.URUT)
    Ext.getCmp('btnTambahBaruLABPA').hide();
    tmpurut = rowdata.URUT_MASUK;
    AddNewPenJasRad = false;
    
    if(Ext.get('cboJenisTr_viPenJasLab_PA').getValue()=='Transaksi Lama'){
        Ext.get('txtNoTransaksiPenJasLab_PA').dom.value = rowdata.NO_TRANSAKSI;
        //-------22-02-2017
        Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').disable();
    }
    else
    {
        //-------22-02-2017
        getUnitDefault("tidak");
        Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').enable();
    }
    console.log(rowdata);
    TmpNotransaksi=rowdata.NO_TRANSAKSI;
    KdKasirAsal=rowdata.KD_KASIR;
    TglTransaksi=rowdata.TGL_TRANSAKSI;
    Kd_Spesial=rowdata.KD_SPESIAL;
    No_Kamar=rowdata.KAMAR;
    kodeunit=rowdata.KD_UNIT;
    if (combovalues==='Transaksi Lama' )
    {
        ViewGridBawahLookupPenjasLab_PA(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
        kdkasir=rowdata.KD_KASIR;
        //alert(kdkasir);
        Ext.getCmp('btnPembayaranPenjasLABPA').enable();
        Ext.getCmp('btnTutupTransaksiPenjasLABPA').enable();
        Ext.getCmp('btnHpsBrsKasirLABPA').enable();
        RefreshDatahistoribayar_LABPA(rowdata.NO_TRANSAKSI);
        kodeUnitLab_PA=rowdata.KD_UNIT_ASAL;
        if (rowdata.LUNAS==='t')
        {
            //Ext.getCmp('btnPembayaranPenjasLABPA').disable();
            //Ext.getCmp('btnHpsBrsKasirLABPA').disable();
            Ext.getCmp('btnTutupTransaksiPenjasLABPA').disable();
        }
		if (rowdata.CO_STATUS==='t'){
			   
			Ext.getCmp('btnTambahBarisPenJasLab_PA').disable();
			Ext.getCmp('btnLookupPenJasLab_PA').disable();
			Ext.getCmp('btnHpsBrsItemLab_PA').disable();
		}else{
			Ext.getCmp('btnTambahBarisPenJasLab_PA').enable();
			Ext.getCmp('btnLookupPenJasLab_PA').enable();
			Ext.getCmp('btnHpsBrsItemLab_PA').enable();
		}
        if (rowdata.NO_REGISTER === null || rowdata.NO_REGISTER === '' || rowdata.NO_REGISTER === 'null'){
            
            Ext.getCmp('cboKeteranganHasilLab_PA').setValue(1);
        }else{
            if (rowdata.NO_REGISTER.substr(0,2) === 'MF'){
                Ext.getCmp('cboKeteranganHasilLab_PA').setValue(1);
            }else if (rowdata.NO_REGISTER.substr(0,2) === 'MT'){
                Ext.getCmp('cboKeteranganHasilLab_PA').setValue(2);
            }else if (rowdata.NO_REGISTER.substr(0,2) === 'MI'){
                Ext.getCmp('cboKeteranganHasilLab_PA').setValue(3);
            }else if (rowdata.NO_REGISTER.substr(0,2) === 'MS'){
                Ext.getCmp('cboKeteranganHasilLab_PA').setValue(4);
            }else if (rowdata.NO_REGISTER.substr(0,2) === 'K.'){
                Ext.getCmp('cboKeteranganHasilLab_PA').setValue(5);
            }else if (rowdata.NO_REGISTER.substr(0,2) === 'VC'){
                Ext.getCmp('cboKeteranganHasilLab_PA').setValue(6);
            }else if (rowdata.NO_REGISTER.substr(0,3) === 'SVC'){
                Ext.getCmp('cboKeteranganHasilLab_PA').setValue(7);
            }else{
                Ext.getCmp('cboKeteranganHasilLab_PA').setValue(1);
            }
        }
        Ext.getCmp('btnSimpanNoLabPenjasLABPA').show();
    }
    else
    {
        Ext.getCmp('btnPembayaranPenjasLABPA').disable();
        Ext.getCmp('btnHpsBrsKasirLABPA').disable();
        Ext.getCmp('btnTutupTransaksiPenjasLABPA').disable();
        kodeUnitLab_PA=rowdata.KD_UNIT;
		Ext.getCmp('btnSimpanNoLabPenjasLABPA').hide();
    }
    var modul='';
    if(radiovaluesLABPA == 1){
        modul='igd';
    } else if(radiovaluesLABPA == 2){
        modul='rwi';
    } else if(radiovaluesLABPA == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/lab_pa/functionLABPA/cekPembayaran",
        params: {
            notrans: rowdata.NO_TRANSAKSI,
            Modul:modul,
            kdkasir: rowdata.KD_KASIR
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            console.log(cst.ListDataObj);
            kodepasien = rowdata.KD_PASIEN;
            namapasien = rowdata.NAMA;
            namaunit = rowdata.NAMA_UNIT;
            kodepay = cst.ListDataObj[0].kd_pay;
            uraianpay = cst.ListDataObj[0].cara_bayar;
            kdkasir = cst.ListDataObj[0].kd_kasir;
            kdcustomeraa = rowdata.KD_CUSTOMER;
            Ext.get('txtNomorLab_PA').dom.value =cst.ListDataObj[0].no_register; 
            /* loaddatastorePembayaran(cst.ListDataObj[0].jenis_pay);
            Ext.get('cboPembayaran').dom.value =cst.ListDataObj[0].ket_payment;
            Ext.get('cboJenisByr').dom.value =cst.ListDataObj[0].cara_bayar; 
            vflag = cst.ListDataObj[0].flag;
            tapungkd_pay = cst.ListDataObj[0].kd_pay; */
            
        }

    });
    
    Ext.get('dtpTtlLABPA').dom.value=ShowDate(rowdata.TGL_LAHIR);
    var tahunLahir= ShowDate(rowdata.TGL_LAHIR);
    var tahunLahirSplit= tahunLahir.split('/');
    var tahunSekarang=nowTglTransaksiGrid.format("Y");
    var selisihTahunLahir=tahunSekarang-tahunLahirSplit[2];
    
    Ext.get('txtUmurLab_PA').dom.value=selisihTahunLahir;
    Ext.get('txtNoMedrecLABPA').dom.value= rowdata.KD_PASIEN;
    Ext.get('txtNamaPasienLABPA').dom.value = rowdata.NAMA;
    Ext.get('txtAlamatLABPA').dom.value = rowdata.ALAMAT;
    Ext.get('txtKdDokter').dom.value   = rowdata.KD_DOKTER;
    
    // if (rowdata.NAMA_UNIT_ASLI==='')
    // {
    //  Ext.get('txtNamaUnitLABPA').dom.value = rowdata.NAMA_UNIT;
    // }
    // else
    // {
    //  Ext.get('txtNamaUnitLABPA').dom.value = rowdata.NAMA_UNIT_ASAL;
    // }
    Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
    
    jenisKelaminPasien=rowdata.JENIS_KELAMIN;       
    Ext.get('cboJKLABPA').dom.value = rowdata.JENIS_KELAMIN;
    if (rowdata.JENIS_KELAMIN === 't')
    {
        Ext.get('cboJKLABPA').dom.value= 'Laki-laki';
        
    }else
    {
        Ext.get('cboJKLABPA').dom.value= 'Perempuan'
        
    }
    Ext.getCmp('cboJKLABPA').disable();
    
    Ext.get('cboGDRLABPA').dom.value = rowdata.GOL_DARAH;
    if (rowdata.GOL_DARAH === '0')
    {
        Ext.get('cboGDRLABPA').dom.value= '-';
    }else if (rowdata.GOL_DARAH === '1')
    {
        Ext.get('cboGDRLABPA').dom.value= 'A+'
    }else if (rowdata.GOL_DARAH === '2')
    {
        Ext.get('cboGDRLABPA').dom.value= 'B+'
    }else if (rowdata.GOL_DARAH === '3')
    {
        Ext.get('cboGDRLABPA').dom.value= 'AB+'
    }else if (rowdata.GOL_DARAH === '4')
    {
        Ext.get('cboGDRLABPA').dom.value= 'O+'
    }else if (rowdata.GOL_DARAH === '5')
    {
        Ext.get('cboGDRLABPA').dom.value= 'A-'
    }else if (rowdata.GOL_DARAH === '6')
    {
        Ext.get('cboGDRLABPA').dom.value= 'B-'
    }
    Ext.getCmp('cboGDRLABPA').disable();
    
    //alert(rowdata.KD_CUSTOMER);
    Ext.get('txtKdCustomerLamaHide').dom.value= rowdata.KD_CUSTOMER;//tampung kd customer untuk transaksi selain kunjungan langsung
    vkode_customer = rowdata.KD_CUSTOMER;
    Ext.getCmp('cboKelPasienLab_PA').disable();
    
    Ext.get('cboKelPasienLab_PA').dom.value= rowdata.KELPASIEN;
    Combo_Select(rowdata.KELPASIEN);
    
    kelompokPasien= rowdata.KELPASIEN;
    Ext.getCmp('cboKelPasienLab_PA').enable();
    Ext.get('txtCustomerLamaHide').hide();
    if(rowdata.KELPASIEN == 'Perseorangan'){
        Ext.getCmp('cboPerseoranganLab_PA').setValue(rowdata.CUSTOMER);
        Ext.getCmp('cboPerseoranganLab_PA').show();
    } else if(rowdata.KELPASIEN == 'Perusahaan'){
        Ext.getCmp('cboPerusahaanRequestEntryLab_PA').setValue(rowdata.CUSTOMER);
        Ext.getCmp('cboPerusahaanRequestEntryLab_PA').show();
    } else{
        Ext.getCmp('cboAsuransiLab_PA').setValue(rowdata.CUSTOMER);
        Ext.getCmp('cboAsuransiLab_PA').show();
    }

    // 'DOKTER_ASAL','KD_DOKTER_ASAL'
    // Ext.getCmp('txtDokterPengirimLABPA').setValue(rowdata.DOKTER_ASAL);
    // Ext.getCmp('cboDOKTER_viPenJasLab_PA').setValue(rowdata.DOKTER);
    // Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').setValue(rowdata.NAMA_UNIT_ASLI);
    // getDokterPengirim(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
    if(tmppasienbarulama == 'Baru'){
        //Ext.getCmp('txtDokterPengirimLABPA').setValue(rowdata.DOKTER);
        Ext.getCmp('cboDokterPengirimLABPA').setValue(rowdata.DOKTER);
        Ext.getCmp('cboDOKTER_viPenJasLab_PA').setValue('');
        Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').setValue('');
        tmpkd_unit = '';
        Ext.get('txtNamaUnitLABPA').dom.value = rowdata.NAMA_UNIT_ASLI;
        tmpkddoktertujuan = '';
        tmpkddokterpengirim = rowdata.KD_DOKTER;
        kd_kasir_labpa = '';
        tmpkd_unit_asal= rowdata.KD_UNIT;
        Ext.get('txtKdUnitLab_PA').dom.value   = rowdata.KD_UNIT;
    }else{
        //Ext.getCmp('txtDokterPengirimLABPA').setValue(rowdata.DOKTER_ASAL);
        Ext.getCmp('cboDokterPengirimLABPA').setValue(rowdata.DOKTER_ASAL)
        Ext.getCmp('cboDOKTER_viPenJasLab_PA').setValue(rowdata.DOKTER);
        //Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').setValue(rowdata.KD_UNIT);
        Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').setValue(rowdata.NAMA_UNIT_ASLI);
        tmpkd_unit = rowdata.KD_UNIT;
        //alert(tmpkd_unit);
        tmpkddoktertujuan = rowdata.KD_DOKTER;
        tmpkddokterpengirim = rowdata.KD_DOKTER;
        //Ext.get('txtNamaUnitLABPA').dom.value = rowdata.NAMA_UNIT;
        kd_kasir_labpa = rowdata.KD_KASIR;
        //Ext.get('txtKdUnitLab_PA').dom.value   = rowdata.KD_UNIT_ASAL;
        if (radiovaluesLABPA !=='3'){
            Ext.get('txtNamaUnitLABPA').dom.value = rowdata.NAMA_UNIT_ASAL;
            Ext.get('txtKdUnitLab_PA').dom.value   = rowdata.KD_UNIT_ASAL;
        }else{
            Ext.get('txtNamaUnitLABPA').dom.value = rowdata.NAMA_UNIT;
            Ext.get('txtKdUnitLab_PA').dom.value   = rowdata.KD_UNIT;
        }
        
        tmpkd_unit_asal=rowdata.KD_UNIT_ASAL;
    }
    Ext.getCmp('txtnotlplabpa').setValue(rowdata.TELEPON);
    Ext.getCmp('txtNoSJPLab_PA').setValue(rowdata.SJP); 
        
    
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/getcurrentshift",
         params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
             var cst = Ext.decode(o.responseText);
            
        },      
        success: function(o) {
            tampungshiftsekarang=o.responseText
            
        }
        
    
    });
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionLAB/getUnitDefault",
        params: {
            notrans: rowdata.NO_TRANSAKSI,
            Modul:modul
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            //Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').setValue(cst.kd_unit);
            getComboDokterLab_PA(cst.kd_unit);
            //Ext.getCmp('cbounitrads_viPenJasRad').setValue(cst.kd_unit);
            //combovaluesunittujuan=cst.kd_unit;
            //tmpkd_unit = cst.kd_unit;
            
            Ext.Ajax.request({
                url: baseURL + "index.php/lab_pa/functionLABPA/getTarifMir",
                params: {
                    kd_unit:tmpkd_unit_asal,
                     //kd_unit_asal:tmpkd_unit_asal,
                     kd_customer:vkode_customer,
                     penjas:modul,
                     kdunittujuan:cst.kd_unit
                },
                failure: function (o)
                {
                    var cst = Ext.decode(o.responseText);
                },
                success: function (o) {
                    var cst = Ext.decode(o.responseText);
                    console.log(cst.ListDataObj);
                    tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                }

            });
        }

    });
    //,,
    //alert(tampungshiftsekarang);
    //--------------------ABULHASSAN ------------- 20_01_17
    namaCustomer_LIS=rowdata.CUSTOMER;
    namaKelompokPasien_LIS=rowdata.KELPASIEN;
    
    
};


function LABPAAddNew() 
{
    Ext.getCmp('btnTambahBaruLABPA').show();
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/lab_pa/functionLABPA/getCurrentShiftLab",
        params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function(o) {
            tampungshiftsekarang=o.responseText;
        }
    });
    AddNewPenJasRad = true;
    Ext.get('txtNoTransaksiPenJasLab_PA').dom.value = '';
    Ext.get('txtNoMedrecLABPA').dom.value='';
    Ext.get('txtNamaPasienLABPA').dom.value = '';
    //Ext.get('cboDOKTER_viPenJasLab_PA').dom.value = '';
    Ext.get('cboUnitLab_PA_viPenJasLab_PA').dom.value = '';
    Ext.get('cboJKLABPA').dom.value = '';
    Ext.get('cboGDRLABPA').dom.value = '';
    Ext.get('txtUmurLab_PA').dom.value = '';
    Ext.getCmp('dtpTtlLABPA').setValue(now)
    Ext.get('txtnotlplabpa').dom.value = '';
    Ext.get('txtKdDokter').dom.value   = undefined;
    Ext.get('txtNamaUnitLABPA').dom.value   = 'Laboratorium Anatomi';
    //Ext.getCmp('txtDokterPengirimLABPA').setValue('DOKTER LUAR');
    Ext.getCmp('cboDokterPengirimLABPA').setValue('DOKTER LUAR');
    tmpkddokterpengirim = '';
    Ext.get('txtKdUrutMasuk').dom.value = '';
    rowSelectedPenJasLab_PA=undefined;
    dsTRDetailPenJasLab_PAList.removeAll();
    
    databaru = 1;
    Ext.get('txtNamaUnitLABPA').readOnly;
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionLAB/getUnitDefault",
        params: {
           text:''
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').setValue(cst.kd_unit);
            getComboDokterLab_PA(cst.kd_unit);
            //Ext.getCmp('cbounitrads_viPenJasRad').setValue(cst.kd_unit);
            //combovaluesunittujuan=cst.kd_unit;
            //tmpkd_unit = cst.kd_unit;
            
            Ext.Ajax.request({
                url: baseURL + "index.php/lab_pa/functionLABPA/getTarifMir",
                params: {
                    kd_unit:cst.kd_unit,
                     //kd_unit_asal:tmpkd_unit_asal,
                     kd_customer:vkode_customer,
                     penjas:'langsung',
                     kdunittujuan:cst.kd_unit
                },
                failure: function (o)
                {
                    var cst = Ext.decode(o.responseText);
                },
                success: function (o) {
                    var cst = Ext.decode(o.responseText);
                    console.log(cst.ListDataObj);
                    tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                }

            });
        }

    });

};

function RefreshDataPenJasLab_PADetail(no_transaksi) 
{
    var strKriteriaLABPA='';
    //strKriteriaLABPA = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaLABPA = "\"no_transaksi\" = ~" + no_transaksi + "~" + " And kd_unit ='4'";
    //strKriteriaLABPA = 'no_transaksi = ~0000004~';
   
    dsTRDetailPenJasLab_PAList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target: 'ViewDetailRWJGridBawah',
                param: strKriteriaLABPA
            }
        }
    );
    return dsTRDetailPenJasLab_PAList;
};

//tampil grid bawah penjas labpa
function ViewGridBawahLookupPenjasLab_PA(no_transaksi,kd_kasir) 
{
    var strKriteriaLABPA='';
     if (kd_kasir !== undefined) {
        strKriteriaLABPA = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
    }else{
        strKriteriaLABPA = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
    }
   // strKriteriaLABPA = "\"no_transaksi\" = ~" + no_transaksi + "~" ;//+ " And kd_kasir ='03'";
 
   
    dsTRDetailPenJasLab_PAList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target: 'ViewDetailGridBawahPenJasLab',
                param: strKriteriaLABPA
            }
        }
    );
    return dsTRDetailPenJasLab_PAList;
};

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiLABPA() 
{
    
    var KdCust='';
    var TmpCustoLama='';
    var pasienBaru;
    var modul='';
    if(radiovaluesLABPA == 1){
        modul='igd';
    } else if(radiovaluesLABPA == 2){
        modul='rwi';
    } else if(radiovaluesLABPA == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    /* if(Ext.get('cboKelPasienLab_PA').getValue()=='Perseorangan'){
        KdCust=Ext.getCmp('cboPerseoranganLab_PA').getValue();
    }else if(Ext.get('cboKelPasienLab_PA').getValue()=='Perusahaan'){
        KdCust=Ext.getCmp('cboPerusahaanRequestEntryLab_PA').getValue();
    }else {
        KdCust=Ext.getCmp('cboAsuransiLab_PA').getValue();
    } */
    
    if(Ext.getCmp('txtNoMedrecLABPA').getValue() == ''){
        pasienBaru=1;
    } else{
        pasienBaru=0;
    }
    //alert(tmpkd_unit);
    var params =
    {
        //Table:'ViewDetailTransaksiPenJasLab_PA',
        
        KdTransaksi: Ext.get('txtNoTransaksiPenJasLab_PA').getValue(),
        KdPasien:Ext.getCmp('txtNoMedrecLABPA').getValue(),
        NmPasien:Ext.getCmp('txtNamaPasienLABPA').getValue(),
        Ttl:Ext.getCmp('dtpTtlLABPA').getValue(),
        Alamat:Ext.getCmp('txtAlamatLABPA').getValue(),
        JK:Ext.getCmp('cboJKLABPA').getValue(),
        GolDarah:Ext.getCmp('cboGDRLABPA').getValue(),
        KdUnit: Ext.getCmp('txtKdUnitLab_PA').getValue(),
        KdDokter:tmpkddoktertujuan,
        KdDokterPengirim:tmpkddokterpengirim,
        KdUnitTujuan:tmpkd_unit,
        Tgl: Ext.getCmp('dtpKunjunganLABPA').getValue(),
        TglTransaksiAsal:TglTransaksi,
        Tlp: Ext.getCmp('txtnotlplabpa').getValue(),
        urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
        KdCusto:vkode_customer,
        TmpCustoLama:vkode_customer,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
        NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiLab_PA').getValue(),
        NoAskes:Ext.getCmp('txtNoAskesLab_PA').getValue(),
        NoSJP:Ext.getCmp('txtNoSJPLab_PA').getValue(),
        Shift:tampungshiftsekarang,
        List:getArrPenJasLab_PA(),
        JmlField: mRecordLABPA.prototype.fields.length-4,
        JmlList:GetListCountDetailTransaksiLABPA(),
        dtBaru:databaru,
        Hapus:1,
        Ubah:0,
        unit:41,
        TmpNotransaksi:TmpNotransaksi,
        KdKasirAsal:KdKasirAsal,
        KdSpesial:Kd_Spesial,
        Kamar:No_Kamar,
        pasienBaru:pasienBaru,
        listTrDokter    : '',
        Modul:modul,
        KdProduk:'',//sTRDetailPenJasLab_PAList.data.items[CurrentPenJasLab_PA.row].data.KD_PRODUK
        URUT :tmpurut,
        ketHasilLab : Ext.getCmp('cboKeteranganHasilLab_PA').getValue(),
        nomorLABPA : Ext.getCmp('txtNomorLab_PA').getValue()
    };
    return params
};

function getParamHapusDetailTransaksiLABPA() 
{   
    var tmpparam = cellSelecteddeskripsi.data;
    var line = gridDTItemTest.getSelectionModel().selection.cell[0];
    var params =
    {       
        no_tr: tmpparam.no_transaksi,
        urut:tmpparam.urut,
        tgl_transaksi:tmpparam.tgl_transaksi,
        //ABULHASSAN--------------20_01_2017
        no_trans_cadangan:Ext.getCmp('txtNoTransaksiPenJasLab_PA').getValue(),
        kd_produk: dsTRDetailPenJasLab_PAList.data.items[line].data.kd_produk,
        kd_kasir: dsTRDetailPenJasLab_PAList.data.items[line].data.kd_kasir,
    };
    return params
};

function getParamKonsultasi() 
{

    var params =
    {
        
        Table:'ViewDetailTransaksiPenJasLab', //data access listnya belum dibuat
        
        //TrKodeTranskasi: Ext.get('txtNoTransaksiPenJasLab_PA').getValue(),
        KdUnitAsal : Ext.get('txtKdUnitLab_PA').getValue(),
        KdDokterAsal : Ext.get('txtKdDokter').getValue(),
        KdUnit: selectKlinikPoli,
        KdDokter:selectDokter,
        KdPasien:Ext.get('txtNoMedrecLABPA').getValue(),
        TglTransaksi : Ext.get('dtpKunjunganLABPA').dom.value,
        KDCustomer :vkode_customer,
    };
    return params
};

function GetListCountDetailTransaksiLABPA()
{
    
    var x=0;
    for(var i = 0 ; i < dsTRDetailPenJasLab_PAList.getCount();i++)
    {
        if (dsTRDetailPenJasLab_PAList.data.items[i].data.KD_PRODUK != '' || dsTRDetailPenJasLab_PAList.data.items[i].data.DESKRIPSI  != '')
        {
            x += 1;
        };
    }
    return x;
    
};


function getArrPenJasLab_PA()
{
    //console.log(dsTRDetailPenJasLab_PAList.data);
    var abc=dsTRDetailPenJasLab_PAList.data;
    var x='';
    var arr=[];
    for(var i = 0 ; i < dsTRDetailPenJasLab_PAList.getCount();i++)
    {
        if (dsTRDetailPenJasLab_PAList.data.items[i].data.kd_produk != undefined && dsTRDetailPenJasLab_PAList.data.items[i].data.deskripsi != undefined && dsTRDetailPenJasLab_PAList.data.items[i].data.kd_produk != '' && dsTRDetailPenJasLab_PAList.data.items[i].data.deskripsi != '')
        {
            /* var y='';
            var z='@@##$$@@';
            
            y =  dsTRDetailPenJasLab_PAList.data.items[i].data.URUT
            y += z + dsTRDetailPenJasLab_PAList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailPenJasLab_PAList.data.items[i].data.QTY
            y += z + ShowDate(dsTRDetailPenJasLab_PAList.data.items[i].data.TGL_BERLAKU)
            y += z +dsTRDetailPenJasLab_PAList.data.items[i].data.HARGA
            y += z +dsTRDetailPenJasLab_PAList.data.items[i].data.KD_TARIF
            y += z +dsTRDetailPenJasLab_PAList.data.items[i].data.URUT
            
            
            if (i === (dsTRDetailPenJasLab_PAList.getCount()-1))
            {
                x += y 
            }
            else
            {
                x += y + '##[[]]##'
            }; */
            var o={};
            var y='';
            var z='@@##$$@@';
            o['URUT']= dsTRDetailPenJasLab_PAList.data.items[i].data.urut;
            o['KD_PRODUK']= dsTRDetailPenJasLab_PAList.data.items[i].data.kd_produk;
            o['QTY']= dsTRDetailPenJasLab_PAList.data.items[i].data.qty;
            o['TGL_TRANSAKSI']= dsTRDetailPenJasLab_PAList.data.items[i].data.tgl_transaksi;
            o['TGL_BERLAKU']= dsTRDetailPenJasLab_PAList.data.items[i].data.tgl_berlaku;
            o['HARGA']= dsTRDetailPenJasLab_PAList.data.items[i].data.harga;
            o['KD_TARIF']= dsTRDetailPenJasLab_PAList.data.items[i].data.kd_tarif;
            o['cito']= dsTRDetailPenJasLab_PAList.data.items[i].data.cito;
            o['kd_unit']= dsTRDetailPenJasLab_PAList.data.items[i].data.kd_unit;
            //--------------ABULHASSAN--------------20_01_2017
            o['DESKRIPSI']= dsTRDetailPenJasLab_PAList.data.items[i].data.deskripsi;
            arr.push(o);
            //console.log(abc.items[i].data);
        };
    }   
    
    return Ext.encode(arr);
};

//panel dalam lookup detail pasien
function getItemPanelPenJasLab_PA(lebar) 
{
    //pengaturan panel data pasien
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding: 5px 5px 5px 5px',
        border:false,
        height:255,
        items:
        [
                    { //awal panel biodata pasien
                        columnWidth:.99,
                        layout: 'absolute',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: true,
                        width: 100,
                        height: 120,
                        anchor: '100% 100%',
                        items:
                        [
                            //bagian pinggir kiri                            
                            {
                                x: 10,
                                y: 10,
                                xtype: 'label',
                                text: 'No. Transaksi  '
                            },
                            {
                                x: 100,
                                y: 10,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x : 110,
                                y : 10,
                                xtype: 'textfield',
                                name: 'txtNoTransaksiPenJasLab_PA',
                                id: 'txtNoTransaksiPenJasLab_PA',
                                width: 100,
                                emptyText:nmNomorOtomatis,
                                readOnly:true

                            },
                            {
                                x: 10,
                                y: 40,
                                xtype: 'label',
                                text: 'No. Medrec  '
                            },
                            {
                                x: 100,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 110,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: 'No. Medrec',
                                name: 'txtNoMedrecLABPA',
                                id: 'txtNoMedrecLABPA',
                                readOnly:true,
                                width: 120,
                                emptyText:'Otomatis',
                                listeners: 
                                { 
                                    
                                }
                            },
                            {
                                x: 10,
                                y: 70,
                                xtype: 'label',
                                text: 'Unit  '
                            },
                            {
                                x: 100,
                                y: 70,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 110,
                                y: 70,
                                xtype: 'textfield',
                                name: 'txtNamaUnitLABPA',
                                id: 'txtNamaUnitLABPA',
                                readOnly:true,
                                width: 120
                            },
                            {
                                x: 10,
                                y: 100,
                                xtype: 'label',
                                text: 'Dokter Pengirim'
                            },
                            {
                                x: 100,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },mComboDokterPengirim()
                            /* {
                                x: 110,
                                y: 100,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtDokterPengirimLABPA',
                                id: 'txtDokterPengirimLABPA',
                                width: 200
                            } */,
                            {
                                x: 10,
                                y: 130,
                                xtype: 'label',
                                text: 'Lab. Tujuan'
                            },
                            {
                                x: 100,
                                y: 130,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboUnitLab_PA(),
                            {
                                x: 360,
                                y: 130,
                                xtype: 'label',
                                text: 'No. Tlp'
                            },
                            {
                                x: 400,
                                y: 130,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 410,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtnotlplabpa',
                                id: 'txtnotlplabpa',
                                width: 200,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            Ext.getCmp('cboDOKTER_viPenJasLab_PA').focus();
                                        }
                                    }
                                }
                            },
                            //bagian tengah
                            {
                                x: 260,
                                y: 10,
                                xtype: 'label',
                                text: 'Tanggal Kunjung '
                            },
                            {
                                x: 350,
                                y: 10,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 10,
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal hari ini ',
                                id: 'dtpKunjunganLABPA',
                                name: 'dtpKunjunganLABPA',
                                format: 'd/M/Y',
                                //readOnly : true,
                                value: now,
                                width: 110
                            },
                            {
                                x: 260,
                                y: 40,
                                xtype: 'label',
                                text: 'Nama Pasien '
                            },
                            {
                                x: 350,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtNamaPasienLABPA',
                                id: 'txtNamaPasienLABPA',
                                width: 200,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            Ext.getCmp('dtpTtlLABPA').focus();
                                        }
                                    }
                                }
                            },
                            {
                                x: 260,
                                y: 70,
                                xtype: 'label',
                                text: 'Alamat '
                            },
                            {
                                x: 350,
                                y: 70,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 70,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtAlamatLABPA',
                                id: 'txtAlamatLABPA',
                                width: 395,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            if (combovalues === 'Transaksi Lama')
                                            {
                                                Ext.getCmp('txtnotlplabpa').focus();
                                            }
                                            else
                                            {
                                                Ext.getCmp('cboJKLABPA').focus();
                                            }
                                            
                                        }
                                    }
                                }
                            },
                            {
                                x: 360,
                                y: 100,
                                xtype: 'label',
                                text: 'Jenis Kelamin '
                            },
                            {
                                x: 445,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboJK(),
                            {
                                x: 570,
                                y: 100,
                                xtype: 'label',
                                text: 'Gol. Darah '
                            },
                            {
                                x: 640,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboGDarah(),
                            {
                                x: 570,
                                y: 40,
                                xtype: 'label',
                                text: 'Tanggal lahir '
                            },
                            {
                                x: 640,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 645,
                                y: 40,
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                selectOnFocus: true,
                                forceSelection: true,
                                id: 'dtpTtlLABPA',
                                name: 'dtpTtlLABPA',
                                format: 'd/M/Y',
                                //readOnly : true,
                                value: now,
                                width: 110,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            var tmptanggal = Ext.get('dtpTtlLABPA').getValue();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/GetUmur_LAB",
                                                params: {
                                                    TanggalLahir: tmptanggal
                                                },
                                                success: function (o){
                                                    var tmphasil = o.responseText;
                                                    var tmp = tmphasil.split(' ');
                                                    if (tmp.length == 6){
                                                        Ext.getCmp('txtUmurLab_PA').setValue(tmp[0]);
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 4){
                                                        if(tmp[1]== 'years' && tmp[3] == 'day'){
                                                            Ext.getCmp('txtUmurLab_PA').setValue(tmp[0]);
                                                        }else if(tmp[1]== 'years' && tmp[3] == 'days'){
                                                            Ext.getCmp('txtUmurLab_PA').setValue(tmp[0]);
                                                        }else if(tmp[1]== 'year' && tmp[3] == 'days'){
                                                            Ext.getCmp('txtUmurLab_PA').setValue(tmp[0]);
                                                        }else{
                                                            Ext.getCmp('txtUmurLab_PA').setValue(tmp[0]);
                                                        }
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 2 ){
                                                        if (tmp[1] == 'year' ){
                                                            Ext.getCmp('txtUmurLab_PA').setValue(tmp[0]);
                                                        }else if (tmp[1] == 'years' ){
                                                            Ext.getCmp('txtUmurLab_PA').setValue(tmp[0]);
                                                        }else if (tmp[1] == 'mon'  ){
                                                            Ext.getCmp('txtUmurLab_PA').setValue('0');
                                                        }else if (tmp[1] == 'mons'  ){
                                                            Ext.getCmp('txtUmurLab_PA').setValue('0');
                                                        }else{
                                                            Ext.getCmp('txtUmurLab_PA').setValue('0');
                                                        }
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 1){
                                                        Ext.getCmp('txtUmurLab_PA').setValue('0');
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else{
                                                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                                                    }   
                                                }
                                            });
                                            Ext.getCmp('txtAlamatLABPA').focus();
                                        }
                                    }
                                }
                            },
                            {
                                x: 765,
                                y: 40,
                                xtype: 'label',
                                text: 'Umur '
                            },
                            {
                                x: 795,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 805,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: '',
                                name: 'txtUmurLab_PA',
                                id: 'txtUmurLab_PA',
                                width: 30,
                                //anchor: '95%',
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {
                                                    Ext.Ajax.request
                                                    (
                                                            {
                                                                url: baseURL + "index.php/main/functionLAB/cekUsia",
                                                                params: {umur: Ext.get('txtUmurLab_PA').getValue()},
                                                                failure: function (o)
                                                                {
                                                                    ShowPesanErrorPenJasLab_PA('Proses Error ! ', 'Laboratorium PA');
                                                                },
                                                                success: function (o)
                                                                {
                                                                    var cst = Ext.decode(o.responseText);
                                                                    if (cst.success === true)
                                                                    {
                                                                        Ext.getCmp('dtpTtlLABPA').setValue(cst.tahunumur);
                                                                    }
                                                                    else
                                                                    {
                                                                        ShowPesanErrorPenJasLab_PA('Proses Error ! ', 'Laboratorium PA');
                                                                    }
                                                                }
                                                            }
                                                    )
                                                }
                                            }
                                        }
                            },
                            {
                                x: 10,
                                y: 160,
                                xtype: 'label',
                                text: 'Dokter Lab '
                            },
                            {
                                x: 100,
                                y: 160,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboDOKTER(),
                            {
                                x: 10,
                                y: 185,
                                xtype: 'label',
                                text: 'Kelompok Pasien'
                            },
                            {
                                x: 100,
                                y: 185,
                                xtype: 'label',
                                text: ':'
                            },
                            
                            {
                                x: 360,
                                y: 185,
                                xtype: 'label',
                                text: 'No. SEP'
                            },
                            {
                                x: 400,
                                y: 185,
                                xtype: 'label',
                                text: ':'
                            },
                            mCboKelompokpasien(),
                            mComboPerseorangan(),
                            mComboPerusahaan(),
                            mComboAsuransi(),
                            {
                                x: 415,
                                y: 185,
                                xtype: 'textfield',
                                fieldLabel: 'Nama Peserta',
                                maxLength: 200,
                                name: 'txtNamaPesertaAsuransiLab_PA',
                                id: 'txtNamaPesertaAsuransiLab_PA',
                                emptyText:'Nama Peserta Asuransi',
                                width: 125
                            },
                            {
                                x: 545,
                                y: 185,
                                xtype: 'textfield',
                                fieldLabel: 'No. Askes',
                                maxLength: 200,
                                name: 'txtNoAskesLab_PA',
                                id: 'txtNoAskesLab_PA',
                                emptyText:'No Askes',
                                width: 125
                            },
                            {
                                x: 675,
                                y: 185,
                                xtype: 'textfield',
                                fieldLabel: 'No. SEP',
                                maxLength: 200,
                                name: 'txtNoSJPLab_PA',
                                id: 'txtNoSJPLab_PA',
                                emptyText:'No SEP',
                                width: 125
                            },
                            {
                                x: 110,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel:'Dokter  ',
                                name: 'txtCustomerLamaHide',
                                id: 'txtCustomerLamaHide',
                                readOnly:true,
                                width: 280
                            },
                            {
                                x: 110,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel:'Kode customer  ',
                                name: 'txtKdCustomerLamaHide',
                                id: 'txtKdCustomerLamaHide',
                                readOnly:true,
                                hidden:true,
                                width: 280
                            },
                            {
                                x: 10,
                                y: 210,
                                xtype: 'label',
                                text: 'Kode Nomor Lab'
                            },
                            {
                                x: 100,
                                y: 210,
                                xtype: 'label',
                                text: ':'
                            },
                            mCboKeteranganHasilLab(),
                            {
                                x: 230,
                                y: 210,
                                xtype: 'label',
                                text: 'Nomor Lab'
                            },
                            {
                                x: 300,
                                y: 210,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 310,
                                y: 210,
                                xtype: 'textfield',
                                maxLength: 200,
                                name: 'txtNomorLab_PA',
                                id: 'txtNomorLab_PA',
                                emptyText:'Nomor Lab',
                                width: 125
                            },{
								x: 450,
                                y: 210,
								xtype: 'button',
								id: 'btnSimpanNoLabPenjasLABPA',
								text: 'Simpan Nomor Lab',
								tooltip: nmEditData,
								//iconCls: 'Edit_Tr',
								//hidden: true,
								handler: function (sm, row, rec)
								{
									Ext.Ajax.request
										(
											{
												url: baseURL +  "index.php/lab_pa/functionLABPA/UpdateNoLab",   
												params: {
															KDPASIEN  		: Ext.getCmp('txtNoMedrecLABPA').getValue(),
															KDUNIT     		: tmpkd_unit,
															TGLTRANSAKSI   	: Ext.getCmp('dtpKunjunganLABPA').getValue(),
															NOLAB    		: Ext.getCmp('txtNomorLab_PA').getValue(),
												},
												failure: function(o)
												{
													ShowPesanWarningPenJasLab_PA('Simpan Nomor LAB gagal', 'Gagal');
												},  
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) 
													{
														 
														ShowPesanInfoPenJasLab_PA("Mengganti Nomor LAB berhasil", "Success");

													}else 
													{
														ShowPesanWarningPenJasLab_PA('Simpan Nomor LAB gagal', 'Gagal');
													};
												}
											}
										);
								}//, disabled: true
							},
                            //---------------------hidden----------------------------------
                            
                            {
                                xtype: 'textfield',
                                fieldLabel:'Dokter  ',
                                name: 'txtKdDokter',
                                id: 'txtKdDokter',
                                readOnly:true,
                                hidden:true,
                                anchor: '99%'
                            },
                            
                            {
                                xtype: 'textfield',
                                fieldLabel:'Unit  ',
                                name: 'txtKdUnitLab_PA',
                                id: 'txtKdUnitLab_PA',
                                readOnly:true,
                                hidden:true,
                                anchor: '99%'
                            },
                            {
                                xtype: 'textfield',
                                name: 'txtKdUrutMasuk',
                                id: 'txtKdUrutMasuk',
                                readOnly:true,
                                hidden:true,
                                anchor: '100%'
                            }
                                
                            //-------------------------------------------------------------
                            
                        ]
                    },  //akhir panel biodata pasien
            
        ]
    };
    return items;
};


function getParamBalikanHitungUmur(tgl){
    Ext.getCmp('dtpTtlLABPA').setValue(tgl);
}
function getItemPanelUnit(lebar) 
{
    var items =
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                columnWidth: .40,
                layout: 'form',
                labelWidth:100,
                border: false,
                items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel:'Unit  ',
                        name: 'txtKdUnitLab_PA',
                        id: 'txtKdUnitLab_PA',
                        readOnly:true,
                        anchor: '99%'
                    }
                ]
            },
            {
                columnWidth: .60,
                layout: 'form',
                border: false,
                labelWidth:1,
                items:
                [
                    {
                        xtype: 'textfield',
                        name: 'txtNamaUnitLABPA',
                        id: 'txtNamaUnitLABPA',
                        readOnly:true,
                        anchor: '100%'
                    }
                ]
            }
        ]
    }
    return items;
};

function refeshpenjaslabpa(kriteria)
{
    /* dsTRPenJasLab_PAList.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPenJasLab',
                    param : kriteria
                }           
            }
        );   
    return dsTRPenJasLab_PAList; */
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/lab/viewpenjaslab/getPasien",
            params: getParamRefreshLab_PA(kriteria),
            failure: function(o)
            {
                ShowPesanWarningPenJasLab_PA('Hubungi Admin', 'Error');
            },
            success: function(o)
            {   
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    dsTRPenJasLab_PAList.removeAll();
                    var recs=[],
                    recType=dsTRPenJasLab_PAList.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    dsTRPenJasLab_PAList.add(recs);
                    grListPenJasLab_PA.getView().refresh();

                }
                else
                {
                    ShowPesanWarningPenJasLab_PA('Gagal membaca Data ini', 'Error');
                };
            }
        }

    );
}


function getParamRefreshLab_PA(krite){
    var unit;
    if(radiovaluesLABPA == 1){
        unit = 'IGD';
    } else if(radiovaluesLABPA == 2){
        unit = 'RWI';
    } else if(radiovaluesLABPA == 3){
        unit = 'Langsung';
    } else{
        unit = 'RWJ';
    }
    var params =
    {
        unit:unit,
        kriteria:krite
        /* tglAwal:
        tglAkhir:dtpTglAwalFilterPenJasLab_PA */
        
    }
    return params;
}

function loadpenjaslabpa(tmpparams, tmpunit)
{
    dsTRPenJasLab_PAList.load
        (
                    { 
                        params:  
                        {   
                            Skip: 0, 
                            Take: '',
                            Sort: '',
                            Sortdir: 'ASC', 
                            target: tmpunit,
                            param : tmpparams
                        }           
                    }
        );   
    return dsTRPenJasLab_PAList;
}

function Datasave_PenJasLab_PA(mBol) 
{   
     if (ValidasiEntryPenJasLab_PA(nmHeaderSimpanData,false) == 1 )
     {
         loadMask.show();
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/lab_pa/functionLABPA/savedetaillab",
                    params: getParamDetailTransaksiLABPA(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasLab_PA('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,kd_kasir_labpa);
                    },  
                    success: function(o) 
                    {
                        
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            loadMask.hide();
                            ShowPesanInfoPenJasLab_PA("Berhasil menyimpan data ini","Information");
                            kd_kasir_labpa = cst.kdkasir;
                            
                            Ext.get('txtNoTransaksiPenJasLab_PA').dom.value=cst.notrans;
                            Ext.get('txtNoMedrecLABPA').dom.value=cst.kdPasien;
                            ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,cst.kdkasir);
                            Ext.getCmp('btnPembayaranPenjasLABPA').enable();
                            Ext.getCmp('btnTutupTransaksiPenjasLABPA').enable();
                            if(mBol === false)
                            {
                                ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,cst.kdkasir);
                            };
                            
                            tmpkd_pasien_sql = cst.kdPasien;
                            tmpno_trans_sql = cst.notrans;
                            tmpkd_kasir_sql = cst.kdkasir;
                            tmp_tgl_sql = cst.tgl;
                            tmp_urut_sql = cst.urut;
                            if (Ext.get('cboJenisTr_viPenJasLab_PA').getValue()!='Transaksi Baru' && radiovaluesLABPA != '3')
                            {
                                validasiJenisTrLABPA();
                            }
                            //
                            //coba disatukan di function php yang sama
                            //Datasave_PenJasLab_PA_SQL();
                            //Datasave_LABPA_LIS();
                        }
                        else 
                        {
                                ShowPesanWarningPenJasLab_PA('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

function Datasave_PenJasLab_PA_LangsungTambahBaris(mBol) 
{   
     if (ValidasiEntryPenJasLab_PA(nmHeaderSimpanData,false) == 1 )
     {
         loadMask.show();
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/lab_pa/functionLABPA/savedetaillab",
                    params: getParamDetailTransaksiLABPA(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasLab_PA('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,kd_kasir_labpa);
                    },  
                    success: function(o) 
                    {
                        
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            loadMask.hide();
                            //ShowPesanInfoPenJasLab_PA("Berhasil menyimpan data ini","Information");
                            kd_kasir_labpa = cst.kdkasir;
                            
                            Ext.get('txtNoTransaksiPenJasLab_PA').dom.value=cst.notrans;
                            Ext.get('txtNoMedrecLABPA').dom.value=cst.kdPasien;
                            //ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,cst.kdkasir);
                            Ext.getCmp('btnPembayaranPenjasLABPA').enable();
                            Ext.getCmp('btnTutupTransaksiPenjasLABPA').enable();
                            /* if(mBol === false)
                            {
                                ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,cst.kdkasir);
                            }; */
                            
                            tmpkd_pasien_sql = cst.kdPasien;
                            tmpno_trans_sql = cst.notrans;
                            tmpkd_kasir_sql = cst.kdkasir;
                            tmp_tgl_sql = cst.tgl;
                            tmp_urut_sql = cst.urut;
                            if (Ext.get('cboJenisTr_viPenJasLab_PA').getValue()!='Transaksi Baru' && radiovaluesLABPA != '3')
                            {
                                validasiJenisTrLABPA();
                            }
                            //
                            //coba disatukan di function php yang sama
                            //Datasave_PenJasLab_PA_SQL();
                            //Datasave_LABPA_LIS();
                        }
                        else 
                        {
                                ShowPesanWarningPenJasLab_PA('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

var tmpkd_pasien_sql = '';
var tmpno_trans_sql = '';
var tmpkd_kasir_sql = '';
var tmp_tgl_sql = '';
var tmp_urut_sql = '';

function DataHapus_PenJasLab_PA(mBol) 
{   
     if (ValidasiEntryPenJasLab_PA(nmHeaderSimpanData,false) == 1 )
     {
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/lab_pa/functionLABPA/deletedetaillab",
                    params: getParamHapusDetailTransaksiLABPA(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasLab_PA('Error simpan. Hubungi Admin!', 'Gagal');
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            ShowPesanInfoPenJasLab_PA('Data Berhasil Di hapus', 'Sukses');
                            
                        }
                        else 
                        {
                                ShowPesanWarningPenJasLab_PA('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};


function Dataupdate_PenJasRad(mBol) 
{   
    if (ValidasiEntryPenJasLab_PA(nmHeaderSimpanData,false) == 1 )
    {
        
            Ext.Ajax.request
             (
                {
                    //url: "./Datapool.mvc/CreateDataObj",
                    url: baseURL + "index.php/main/CreateDataObj",
                    params: getParamDataupdatePenJasRadDetail(),
                    success: function(o) 
                    {
                        ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,kd_kasir_labpa);
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            ShowPesanInfoPenJasLab_PA(nmPesanSimpanSukses,nmHeaderSimpanData);
                            //RefreshDataPenJasRad();
                            if(mBol === false)
                            {
                                ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,kd_kasir_labpa);
                            };
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarningPenJasLab_PA(nmPesanSimpanGagal,nmHeaderSimpanData);
                        }
                        else if (cst.success === false && cst.pesan===1)
                        {
                            ShowPesanWarningPenJasLab_PA(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
                        }
                        else 
                        {
                            ShowPesanErrorPenJasLab_PA(nmPesanSimpanError,nmHeaderSimpanData);
                        };
                    }
                }
            )
        
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
};
function mComboUnitTujuans_viPenJasLab_PA(){
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitlabpas_viPenJasLab_PA = new WebApp.DataStore({ fields: Field });
    dsunitlabpas_viPenJasLab_PA.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'nama_unit',
            Sortdir: 'ASC',
            target: 'ViewSetupUnit',
            param: "parent = '4' and kd_unit in ('44','45')"
        }
    });
    var cbounitlabpas_viPenJasLab_PA = new Ext.form.ComboBox({
        id: 'cbounitlabpas_viPenJasLab_PA',
        x: 455,
        y: 10,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        emptyText: '',
        disabled:true,
        fieldLabel:  ' ',
        align: 'Right',
        width: 230,
        emptyText:'Pilih Unit',
        store: dsunitlabpas_viPenJasLab_PA,
        valueField: 'KD_UNIT',
        displayField: 'NAMA_UNIT',
        //value:'All',
        editable: false,
        listeners:{
            'select': function(a,b,c){
                combovaluesunittujuan=b.data.KD_UNIT;
                validasiJenisTrLABPA();
            }
        }
    });
    return cbounitlabpas_viPenJasLab_PA;
    
};
function ValidasiEntryPenJasLab_PA(modul,mBolHapus)
{
    var x = 1;
    
    if((Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').getValue() == '') ||(Ext.get('txtNamaPasienLABPA').getValue() == '') || (Ext.get('cboDOKTER_viPenJasLab_PA').getValue() == 'Pilih DOkter') || (Ext.get('dtpKunjunganLABPA').getValue() == '') || (dsTRDetailPenJasLab_PAList.getCount() === 0 ))
    {
        if (Ext.get('txtNoTransaksiPenJasLab_PA').getValue() == '' && mBolHapus === true) 
        {
            x = 0;
        }
         else if (Ext.get('txtNoMedrecLABPA').getValue() == '') 
        {
            ShowPesanWarningPenJasLab_PA('No. Medrec tidak boleh kosong!','Warning');
            x = 0;
        } else if (Ext.get('cboUnitLab_PA_viPenJasLab_PA').getValue() == '') 
        {
            ShowPesanWarningPenJasLab_PA('Laboratorium tujuan Wajib diisi!','Warning');
            x = 0;
        }
        else if (Ext.get('txtNamaPasienLABPA').getValue() == '') 
        {
            ShowPesanWarningPenJasLab_PA('Nama tidak boleh kosong!','Warning');
            x = 0;
        }
        else if (Ext.get('dtpKunjunganLABPA').getValue() == '') 
        {
            ShowPesanWarningPenJasLab_PA('Tanggal kunjungan tidak boleh kosong!','Warning');
            x = 0;
        }
        else if (Ext.get('cboDOKTER_viPenJasLab_PA').getValue() == 'Pilih Dokter' || Ext.get('cboDOKTER_viPenJasLab_PA').dom.value  === undefined) 
        {
            ShowPesanWarningPenJasLab_PA('Dokter Laboratorium tidak boleh kosong!','Warning');
            x = 0;
        }
        else if (dsTRDetailPenJasLab_PAList.getCount() === 0) 
        {
            ShowPesanWarningPenJasLab_PA('Daftar item test tidak boleh kosong!','Warning');
            x = 0;
        };
    };
    return x;
};

function ShowPesanWarningPenJasLab_PA(str, modul) 
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width:250
        }
    );
};

function ShowPesanErrorPenJasLab_PA(str, modul) {
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width:250
        }
    );
};

function ShowPesanInfoPenJasLab_PA(str, modul) {
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width:250
        }
    );
};


function DataDeletePenJasRad() 
{
   if (ValidasiEntryPenJasLab_PA(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                //url: "./Datapool.mvc/DeleteDataObj",
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiLABPA(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoPenJasLab_PA(nmPesanHapusSukses,nmHeaderHapusData);
                                        //RefreshDataPenJasRad();
                                        LABPAAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningPenJasLab_PA(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningPenJasLab_PA(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorPenJasLab_PA(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};

function RefreshDatacombo(jeniscus) 
{

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customer+'~)'
            }
        }
    )
    
    // rowSelectedPenJasLab_PA = undefined;
    return ds_customer_viDaftar;
};
function mComboKelompokpasien()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    )
    var cboKelompokpasien = new Ext.form.ComboBox
    (
        {
            id:'cboKelompokpasien',
            typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: labelisi,
                        align: 'Right',
                        anchor: '95%',
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetKelompokpasien=b.data.displayText ;
                    selectKdCustomer=b.data.KD_CUSTOMER;
                    selectNamaCustomer=b.data.CUSTOMER;
                
                }
            }
        }
    );
    return cboKelompokpasien;
};

function getKelompokpasienlama(lebar) 
{
    var items =
    {
        Width:lebar,
        height:40,
        layout: 'column',
        border: false,
    //  title: 'Kelompok Pasien Lama',
        
        items:
        [
            {
                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                //title: 'Kelompok Pasien Lama',
                items:
                [
                    {    
                        xtype: 'tbspacer',
                        height: 2
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kelompok Pasien Asal',
                        //maxLength: 200,
                        name: 'txtCustomer_labpaLama',
                        id: 'txtCustomer_labpaLama',
                        labelWidth:130,
                        width: 100,
                        anchor: '95%'
                    },
                    
                    
                ]
            }
            
        ]
    }
    return items;
};


function getItemPanelNoTransksiKelompokPasien(lebar) 
{
    var items =
    {
        Width:lebar,
        height:120,
        layout: 'column',
        border: false,
        
        
        items:
        [
            {

                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                items:
                [
                    
                    
                    {    
                        xtype: 'tbspacer',
                        
                        height:3
                    },
                        {  

                        xtype: 'combo',
                        fieldLabel: 'Kelompok Pasien Baru',
                        id: 'kelPasien',
                         editable: false,
                        //value: 'Perseorangan',
                        store: new Ext.data.ArrayStore
                            (
                                {
                                id: 0,
                                fields:
                                [
                                'Id',
                                'displayText'
                                ],
                                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                }
                            ),
                              displayField: 'displayText',
                              mode: 'local',
                              width: 100,
                              forceSelection: true,
                              triggerAction: 'all',
                              emptyText: 'Pilih Salah Satu...',
                              selectOnFocus: true,
                              anchor: '95%',
                              listeners:
                                {
                                        'select': function(a, b, c)
                                    {
                                        if(b.data.displayText =='Perseorangan')
                                        {
                                            jeniscus='0'
                                            Ext.getCmp('txtNoSJP').disable();
                                            Ext.getCmp('txtNoAskes').disable();}
                                        else if(b.data.displayText =='Perusahaan')
                                        {
                                            jeniscus='1';
                                            Ext.getCmp('txtNoSJP').disable();
                                            Ext.getCmp('txtNoAskes').disable();}
                                        else if(b.data.displayText =='Asuransi')
                                        {
                                            jeniscus='2';
                                            Ext.getCmp('txtNoSJP').enable();
                                            Ext.getCmp('txtNoAskes').enable();
                                        }
                                    
                                        RefreshDatacombo(jeniscus);
                                    }

                                }
                        }, 
                        {
                            columnWidth: .990,
                            layout: 'form',
                            border: false,
                            labelWidth:130,
                            items:
                            [
                                mComboKelompokpasien()
                            ]
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. SJP',
                            maxLength: 200,
                            name: 'txtNoSJP',
                            id: 'txtNoSJP',
                            width: 100,
                            anchor: '95%'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Askes',
                            maxLength: 200,
                            name: 'txtNoAskes',
                            id: 'txtNoAskes',
                            width: 100,
                            anchor: '95%'
                         }
                        
                        //mComboKelompokpasien
        
        
                ]
            }
            
        ]
    }
    return items;
};

//combo jenis kelamin
function mComboJK()
{
    var cboJKLABPA = new Ext.form.ComboBox
    (
        {
            id:'cboJKLABPA',
            x: 455,
            y: 100,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            tabIndex:3,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Jenis Kelamin',
            fieldLabel: 'Jenis Kelamin ',
            editable: false,
            width:95,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[true, 'Laki - Laki'], [false, 'Perempuan']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetJK,
            enableKeyEvents: true,
            listeners:
            {
                'specialkey': function (){
                    if (Ext.EventObject.getKey() === 13){
                        Ext.getCmp('cboGDRLABPA').focus();
                    }
                },
                'select': function(a,b,c)
                {
                    selectSetJK=b.data.Id ;
                    jenisKelaminPasien=b.data.Id ;
                    console.log(jenisKelaminPasien);
                },
/*              'render': function(c) {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('txtTempatLahir').focus();
                                    }, c);
                                }
 */         }
        }
    );
    return cboJKLABPA;
};

//combo golongan darah
function mComboGDarah()
{
    var cboGDRLABPA = new Ext.form.ComboBox
    (
        {
            id:'cboGDRLABPA',
            x: 645,
            y: 100,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            tabIndex:3,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Gol. Darah',
            fieldLabel: 'Gol. Darah ',
            width:50,
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetGDarah,
            enableKeyEvents: true,
            listeners:
            {
                'specialkey': function (){
                    if (Ext.EventObject.getKey() === 13){
                        Ext.getCmp('txtnotlplabpa').focus();
                    }
                },
                'select': function(a,b,c)
                {
                    selectSetGDarah=b.data.displayText ;
                },
            }
        }
    );
    return cboGDRLABPA;
};

//Combo Dokter Lookup
function getComboDokterLab_PA(kdUnit)
{
    dsdokter_viPenJasLab_PA.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang',
            param: "kd_unit = '"+kdUnit+"'"
        }
    });
    return dsdokter_viPenJasLab_PA;
}
function mComboDOKTER()
{ 
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viPenJasLab_PA = new WebApp.DataStore({ fields: Field });
    
    var cboDOKTER_viPenJasLab_PA = new Ext.form.ComboBox
    (
            {
                id: 'cboDOKTER_viPenJasLab_PA',
                x: 110,
                y: 160,
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 230,
                emptyText:'Pilih Dokter',
                store: dsdokter_viPenJasLab_PA,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
               // value:'All',
                editable: false,
                enableKeyEvents: true,
                
            listeners:
            {
                'specialkey': function (){
                        if (Ext.EventObject.getKey() === 13){
                            Ext.getCmp('cboKelPasienLab_PA').focus();
                        }
                    },
                'select': function(a,b,c)
                {
                    
                    tmpkddoktertujuan=b.data.KD_DOKTER ;
                    Ext.getCmp('cboKelPasienLab_PA').focus();
                },
            }
            }
    );
    
    return cboDOKTER_viPenJasLab_PA;
};
var tmpkddoktertujuan;

function mCboKelompokpasien(){  
 var cboKelPasienLab_PA = new Ext.form.ComboBox
    (
        {
            
            id:'cboKelPasienLab_PA',
            fieldLabel: 'Kelompok Pasien',
            x: 110,
            y: 185,
            mode: 'local',
            width: 130,
            forceSelection: true,
            lazyRender:true,
            triggerAction: 'all',
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                id: 0,
                fields:
                [
                    'Id',
                    'displayText'
                ],
                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                }
            ),          
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetKelPasien,
            selectOnFocus: true,
            tabIndex:22,
            enableKeyEvents: true,
                
            listeners:
            {
                'specialkey': function (a, b, c){
                        if (Ext.EventObject.getKey() === 13){
                            if(kelompokPasien == 'Perseorangan' || Ext.getCmp('cboKelPasienLab_PA').getValue() == 1)
                           {
                              // cboPerseorangan
                                //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                                //Ext.getCmp('cboRujukanRequestEntry').hide();
                                Ext.getCmp('cboPerseoranganLab_PA').focus(false,100);
                           }
                           else if(kelompokPasien == 'Perusahaan' || Ext.getCmp('cboKelPasienLab_PA').getValue() == 2)
                           {
                                //Ext.getCmp('cboRujukanDariRequestEntryLab_PA').show();
                                //Ext.getCmp('cboRujukanRequestEntry').show(); 
                                Ext.getCmp('cboPerusahaanRequestEntryLab_PA').focus(false,100);
                           }
                           else if(kelompokPasien == 'Asuransi' || Ext.getCmp('cboKelPasienLab_PA').getValue() == 3)
                           {
                                //Ext.getCmp('cboRujukanDariRequestEntryLab_PA').show();
                                //Ext.getCmp('cboRujukanRequestEntry').show();  
                                 Ext.getCmp('cboAsuransiLab_PA').focus(false,100);
                           }
                        }
                    },
                'select': function(a, b, c)
                    {
                       Combo_Select(b.data.displayText);
                       if(b.data.displayText == 'Perseorangan')
                       {
                          // cboPerseorangan
                            //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                            //Ext.getCmp('cboRujukanRequestEntry').hide();
                            Ext.getCmp('cboPerseoranganLab_PA').focus(false,100);
                       }
                       else if(b.data.displayText == 'Perusahaan')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryLab_PA').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show(); 
                            Ext.getCmp('cboPerusahaanRequestEntryLab_PA').focus(false,100);
                       }
                       else if(b.data.displayText == 'Asuransi')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryLab_PA').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show();  
                             Ext.getCmp('cboAsuransiLab_PA').focus(false,100);
                       }
                      
                    },
                'render': function(a,b,c)
                {
                    
                    Ext.getCmp('txtNamaPesertaAsuransiLab_PA').hide();
                    Ext.getCmp('txtNoAskesLab_PA').hide();
                    Ext.getCmp('txtNoSJPLab_PA').hide();
                    
                    Ext.getCmp('cboPerseoranganLab_PA').hide();
                    Ext.getCmp('cboAsuransiLab_PA').hide();
                    Ext.getCmp('cboPerusahaanRequestEntryLab_PA').hide();
                    
                }
            }

        }
    );
    return cboKelPasienLab_PA;
};

function mCboKelompokpasien_Depan(){  
 var cboKelPasienDepanLab_PA = new Ext.form.ComboBox
    (
        {
            
            id:'cboKelPasienDepanLab_PA',
            fieldLabel: 'Kelompok Pasien',
            x: 155,
            y: 130,
            mode: 'local',
            width: 130,
            forceSelection: true,
            lazyRender:true,
            triggerAction: 'all',
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                id: 0,
                fields:
                [
                    'Id',
                    'displayText'
                ],
                   data: [[0, 'Semua'],[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                }
            ),          
            valueField: 'Id',
            displayField: 'displayText',
            value:'Semua',
            selectOnFocus: true,
            tabIndex:22,
            enableKeyEvents: true,
                
            listeners:
            {
                'specialkey': function (a, b, c){
                        if (Ext.EventObject.getKey() === 13){
                            if(kelompokPasien == 'Perseorangan' || Ext.getCmp('cboKelPasienLab_PA').getValue() == 1)
                           {
                              // cboPerseorangan
                                //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                                //Ext.getCmp('cboRujukanRequestEntry').hide();
                                Ext.getCmp('cboPerseoranganLabDepan_PA').focus(false,100);
                           }
                           else if(kelompokPasien == 'Perusahaan' || Ext.getCmp('cboKelPasienLab_PA').getValue() == 2)
                           {
                                //Ext.getCmp('cboRujukanDariRequestEntryLab_PA').show();
                                //Ext.getCmp('cboRujukanRequestEntry').show(); 
                                Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').focus(false,100);
                           }
                           else if(kelompokPasien == 'Asuransi' || Ext.getCmp('cboKelPasienLab_PA').getValue() == 3)
                           {
                                //Ext.getCmp('cboRujukanDariRequestEntryLab_PA').show();
                                //Ext.getCmp('cboRujukanRequestEntry').show();  
                                 Ext.getCmp('cboAsuransiLabDepan_PA').focus(false,100);
                           }
                        }
                    },
                'select': function(a, b, c)
                    {
                       Combo_Select_Depan(b.data.displayText);
                       if(b.data.displayText == 'Perseorangan')
                       {
                          // cboPerseorangan
                            //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                            //Ext.getCmp('cboRujukanRequestEntry').hide();
                            Ext.getCmp('cboPerseoranganLabDepan_PA').focus(false,100);
                       }
                       else if(b.data.displayText == 'Perusahaan')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryLab_PA').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show(); 
                            Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').focus(false,100);
                       }
                       else if(b.data.displayText == 'Asuransi')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryLab_PA').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show();  
                             Ext.getCmp('cboAsuransiLabDepan_PA').focus(false,100);
                       }
                      
                    },
                'render': function(a,b,c)
                {
                    Ext.getCmp('cboPerseoranganLabDepan_PA').hide();
                    Ext.getCmp('cboAsuransiLabDepan_PA').hide();
                    Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').hide();
                    
                } 
            }

        }
    );
    return cboKelPasienDepanLab_PA;
};
function mCboKeteranganHasilLab(){  
 var cboKeteranganHasilLab_PA = new Ext.form.ComboBox
    (
        {
            
            id:'cboKeteranganHasilLab_PA',
            fieldLabel: 'Ket. Hasil Lab',
            x: 110,
            y: 210,
            mode: 'local',
            width: 100,
            forceSelection: true,
            lazyRender:true,
            triggerAction: 'all',
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                id: 0,
                fields:
                [
                    'Id',
                    'displayText'
                ],
                   data: [[1, 'MF'], [2, 'MT'], [3, 'MI'], [4, 'MS'], [5, 'K'], [6, 'VC'], [7, 'SVC']]
                }
            ),          
            valueField: 'Id',
            displayField: 'displayText',
            value:1,
            selectOnFocus: true,
            tabIndex:22,
            enableKeyEvents: true,
                
            listeners:
            {
                
            }

        }
    );
    return cboKeteranganHasilLab_PA;
};

function mCboKeteranganHasilLabFilterDepan(){  
 var cboKeteranganHasilLab_PAFilterDepan = new Ext.form.ComboBox
    (
        {
            
            id:'cboKeteranganHasilLab_PAFilterDepan',
            fieldLabel: 'Ket. Hasil Lab',
            x: 155,
            y: 100,
            mode: 'local',
            disabled: true,
            width: 100,
            forceSelection: true,
            lazyRender:true,
            triggerAction: 'all',
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                id: 0,
                fields:
                [
                    'Id',
                    'displayText'
                ],
                   data: [[0,'Semua'],[1, 'MF'], [2, 'MT'], [3, 'MI'], [4, 'MS'], [5, 'K']]
                }
            ),          
            valueField: 'Id',
            displayField: 'displayText',
            value:0,
            selectOnFocus: true,
            tabIndex:22,
            enableKeyEvents: true,
                
            listeners:
            {
                'select': function(a,b,c)
                {
                    KodeNomorLAB_PA=b.data.displayText;
                }
            }

        }
    );
    return cboKeteranganHasilLab_PAFilterDepan;
};

function mComboPerseorangan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganLab_PARequestEntry = new WebApp.DataStore({fields: Field});
    dsPeroranganLab_PARequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=0'
            }
        }
    )
    var cboPerseoranganLab_PA = new Ext.form.ComboBox
    (
        {
            id:'cboPerseoranganLab_PA',
            x: 260,
            y: 185,
            editable: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            fieldLabel: 'Jenis',
            tabIndex:23,
            width:150,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            store:dsPeroranganLab_PARequestEntry,
            value:selectSetPerseorangan,
            listeners:
            {
                'select': function(a,b,c)
                {
                    vkode_customer = b.data.KD_CUSTOMER;
                    selectSetPerseorangan=b.data.displayText ;
                }
            }
        }
    );
    return cboPerseoranganLab_PA;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=1'
            }
        }
    )
    var cboPerusahaanRequestEntryLab_PA = new Ext.form.ComboBox
    (
        {
            id: 'cboPerusahaanRequestEntryLab_PA',
            x: 260,
            y: 185,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: 'Perusahaan',
            align: 'Right',
            width:150,
            store: dsPerusahaanRequestEntry,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            //anchor: '95%',
            listeners:
            {
                'select': function(a,b,c)
                {
                    vkode_customer = b.data.KD_CUSTOMER;
                    selectSetPerusahaan=b.data.valueField ;
                }
            }
        }
    );

    return cboPerusahaanRequestEntryLab_PA;
};

function mComboPerseoranganDepan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganLab_PARequestEntryDepan = new WebApp.DataStore({fields: Field});
    dsPeroranganLab_PARequestEntryDepan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=0'
            }
        }
    )
    var cboPerseoranganLabDepan_PA = new Ext.form.ComboBox
    (
        {
            id:'cboPerseoranganLabDepan_PA',
            x: 305,
            y: 130,
            editable: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            fieldLabel: 'Jenis',
            tabIndex:23,
            width:150,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            store:dsPeroranganLab_PARequestEntryDepan,
            value:selectSetPerseorangan,
            listeners:
            {
                'select': function(a,b,c)
                {
                    kodeCustomerFilterDepan=b.data.KD_CUSTOMER;
                    /* vkode_customer = b.data.KD_CUSTOMER;
                    selectSetPerseorangan=b.data.displayText ; */
                }
            }
        }
    );
    return cboPerseoranganLabDepan_PA;
};

function mComboPerusahaanDepan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntryDepan = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntryDepan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=1'
            }
        }
    )
    var cboPerusahaanRequestEntryLabDepan_PA = new Ext.form.ComboBox
    (
        {
            id: 'cboPerusahaanRequestEntryLabDepan_PA',
            x: 305,
            y: 130,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: 'Perusahaan',
            align: 'Right',
            width:150,
            store: dsPerusahaanRequestEntryDepan,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            //anchor: '95%',
            value:'Semua',
            listeners:
            {
                'select': function(a,b,c)
                {
                    kodeCustomerFilterDepan=b.data.KD_CUSTOMER;
                    /* vkode_customer = b.data.KD_CUSTOMER;
                    selectSetPerusahaan=b.data.valueField ; */
                }
            }
        }
    );

    return cboPerusahaanRequestEntryLabDepan_PA;
};

function mComboAsuransiDepan()
{
var Field_poli_viDaftarDepan = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftarDepan = new WebApp.DataStore({fields: Field_poli_viDaftarDepan});

    ds_customer_viDaftarDepan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=2'
            }
           /*  params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomer',
                param: "status=true"
            } */
        }
    )
    var cboAsuransiLabDepan_PA = new Ext.form.ComboBox
    (
        {
            id:'cboAsuransiLabDepan_PA',
            x: 305,
            y: 130,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: 'Asuransi',
            align: 'Right',
            width:150,
            //anchor: '95%',
            store: ds_customer_viDaftarDepan,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            enableKeyEvents: true,
            value:'Semua',  
            listeners:
            {
                'specialkey': function (){
                        if (Ext.EventObject.getKey() === 13){
                            //Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').focus();
                        }
                    },
                'select': function(a,b,c)
                {
                    kodeCustomerFilterDepan=b.data.KD_CUSTOMER;
                    /* vkode_customer = b.data.KD_CUSTOMER;
                    selectSetAsuransi=b.data.valueField ; */
                }
            }
        }
    );
    return cboAsuransiLabDepan_PA;
};

function mComboUnitLab_PA(){ 
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitlabpa_viPenJasLab_PA= new WebApp.DataStore({ fields: Field });
    dsunitlabpa_viPenJasLab_PA.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'nama_unit',
            Sortdir: 'ASC',
            target: 'ViewSetupUnit',
            param: "parent = '4' and kd_unit in('44','45')"
        }
    });
    var cbounitlabpa_viPenJasLab_PA = new Ext.form.ComboBox({
        id: 'cboUnitLab_PA_viPenJasLab_PA',
        x: 110,
        y: 130,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        emptyText: '',
        fieldLabel:  ' ',
        align: 'Right',
        width: 230,
        emptyText:'Pilih Unit',
        store: dsunitlabpa_viPenJasLab_PA,
        valueField: 'KD_UNIT',
        displayField: 'NAMA_UNIT',
        //value:'All',
        editable: false,
            listeners:
            {
                'select': function(a,b,c)
                {
                    tmpkd_unit = b.data.KD_UNIT;
                    getComboDokterLab_PA(b.data.KD_UNIT);
                    if (Ext.getCmp('cboDokterPengirimLABPA').getValue()=='DOKTER LUAR')
                    {
                        Ext.Ajax.request({
                            url: baseURL + "index.php/lab_pa/functionLABPA/getTarifMir",
                            params: {
                                kd_unit:b.data.KD_UNIT,
                                 //kd_unit_asal:tmpkd_unit_asal,
                                 kd_customer:vkode_customer,
                                 penjas:'langsung',
                                 kdunittujuan:b.data.KD_UNIT
                            },
                            failure: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                            },
                            success: function (o) {
                                var cst = Ext.decode(o.responseText);
                                console.log(cst.ListDataObj);
                                tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                            }

                        });
                    }
                }
            }
    });
    return cbounitlabpa_viPenJasLab_PA;
}
function mComboDokterPengirim(){ 
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry= new WebApp.DataStore({ fields: Field });
    dsDokterRequestEntry.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'nama',
            Sortdir: 'ASC',
            target: 'ViewComboDokterPengirim',
            param: ''
        }
    })
    
    var cboDokterPengirimLABPA = new Ext.form.ComboBox({
        id: 'cboDokterPengirimLABPA',
        x: 110,
        y: 100,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        emptyText: '',
        fieldLabel:  ' ',
        align: 'Right',
        width: 230,
        emptyText:'Pilih Dokter Pengirim',
        store: dsDokterRequestEntry,
        valueField: 'KD_DOKTER',
        displayField: 'NAMA',
        //value:'All',
        //editable: false,
            listeners:
            {
                'select': function(a,b,c)
                {
                    tmpkddokterpengirim=b.data.KD_DOKTER;
                    /* tmpkd_unit = b.data.KD_UNIT;
                    getComboDokterLab_PA(b.data.KD_UNIT); */
                }
            }
    });
    return cboDokterPengirimLABPA;
}

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=2'
            }
           /*  params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomer',
                param: "status=true"
            } */
        }
    )
    var cboAsuransiLab_PA = new Ext.form.ComboBox
    (
        {
            id:'cboAsuransiLab_PA',
            x: 260,
            y: 185,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: 'Asuransi',
            align: 'Right',
            width:150,
            //anchor: '95%',
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            enableKeyEvents: true,
                
            listeners:
            {
                'specialkey': function (){
                        if (Ext.EventObject.getKey() === 13){
                            Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').focus();
                        }
                    },
                'select': function(a,b,c)
                {
                    vkode_customer = b.data.KD_CUSTOMER;
                    selectSetAsuransi=b.data.valueField ;
                }
            }
        }
    );
    return cboAsuransiLab_PA;
};


function Combo_Select(combo)
{
   var value = combo
   //var txtgetnamaPeserta = Ext.getCmp('txtNamaPesertaAsuransiLab_PA') ;

   if(value == "Perseorangan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiLab_PA').hide()
        Ext.getCmp('txtNoAskesLab_PA').hide()
        Ext.getCmp('txtNoSJPLab_PA').hide()
        Ext.getCmp('cboPerseoranganLab_PA').show()
        Ext.getCmp('cboAsuransiLab_PA').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab_PA').hide()
        

   }
   else if(value == "Perusahaan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiLab_PA').hide()
        Ext.getCmp('txtNoAskesLab_PA').hide()
        Ext.getCmp('txtNoSJPLab_PA').hide()
        Ext.getCmp('cboPerseoranganLab_PA').hide()
        Ext.getCmp('cboAsuransiLab_PA').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab_PA').show()
        
   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiLab_PA').show()
         Ext.getCmp('txtNoAskesLab_PA').show()
         Ext.getCmp('txtNoSJPLab_PA').show()
         Ext.getCmp('cboPerseoranganLab_PA').hide()
         Ext.getCmp('cboAsuransiLab_PA').show()
         Ext.getCmp('cboPerusahaanRequestEntryLab_PA').hide()
         
       }
}

function Combo_Select_Depan(combo)
{
   var value = combo
   //var txtgetnamaPeserta = Ext.getCmp('txtNamaPesertaAsuransiLab_PA') ;

   if(value == "Perseorangan")
   {    

        Ext.getCmp('cboPerseoranganLabDepan_PA').show()
        Ext.getCmp('cboAsuransiLabDepan_PA').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').hide()
        

   }
   else if(value == "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLabDepan_PA').hide()
        Ext.getCmp('cboAsuransiLabDepan_PA').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').show()
        
   }
   else if(value == "Asuransi")
       {
         Ext.getCmp('cboPerseoranganLabDepan_PA').hide()
         Ext.getCmp('cboAsuransiLabDepan_PA').show()
         Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').hide()
         
       }
    else if(value == "Semua")
       {
         Ext.getCmp('cboPerseoranganLabDepan_PA').hide()
         Ext.getCmp('cboAsuransiLabDepan_PA').hide()
         Ext.getCmp('cboPerusahaanRequestEntryLabDepan_PA').hide()
         
       }
}


function setdisablebutton()
{
Ext.getCmp('btnLookUpKonsultasi_viKasirLABPA').disable();
Ext.getCmp('btnLookUpGantiDokter_viKasirLABPA').disable();
Ext.getCmp('btngantipasien').disable();
Ext.getCmp('btnposting').disable(); 

Ext.getCmp('btnLookupPenJasLab_PA').disable()
Ext.getCmp('btnSimpanLABPA').disable()
Ext.getCmp('btnHpsBrsLABPA').disable()
Ext.getCmp('btnHpsBrsDiagnosa').disable()
Ext.getCmp('btnSimpanDiagnosa').disable()
Ext.getCmp('btnLookupDiagnosa').disable()
}

function setenablebutton()
{
//Ext.getCmp('btnLookUpKonsultasi_viKasirLABPA').enable();
Ext.getCmp('btnLookUpGantiDokter_viKasirLABPA').enable();
Ext.getCmp('btngantipasien').enable();
//Ext.getCmp('btnposting').enable();    

Ext.getCmp('btnLookupPenJasLab_PA').enable()
Ext.getCmp('btnSimpanLABPA').enable()
//Ext.getCmp('btnHpsBrsLABPA').enable()
//Ext.getCmp('btnHpsBrsDiagnosa').enable()
//Ext.getCmp('btnSimpanDiagnosa').enable()
//Ext.getCmp('btnLookupDiagnosa').enable()  
}

//Criteria untuk pencarian berdasarkan no medrec, nama pasien dll
function getCriteriaFilter_viDaftar()//^^^
{
         var strKriteria = "";

           /* if (Ext.get('txtNoMedrecPenJasLab_PA').getValue() != "")
            {
                strKriteria = " pasien.kd_pasien = " + "'" + Ext.get('txtNoMedrecPenJasLab_PA').getValue() +"'";
            }
            
            if (Ext.get('txtNamaPasienPenJasLab_PA').getValue() != "")//^^^
            {
                if (strKriteria == "")
                    {
                        strKriteria = " lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasienPenJasLab_PA').getValue() +"%')" ;
                    }
                    else
                        {
                            strKriteria += " lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasienPenJasLab_PA').getValue() +"%')";
                        }
                
            }
            if (Ext.get('cboStatusLunas_viPenJasLab_PA').getValue() != "")
            {
                if (Ext.get('cboStatusLunas_viPenJasLab_PA').getValue()==='Belum Lunas')
                {
                    if (strKriteria == "")
                    {
                        strKriteria = " transaksi.co_status " + "= 'f'" ;
                    }
                    else
                       {
                            strKriteria += " and transaksi.co_status " + "= 'f'";
                       }
                }
                if (Ext.get('cboStatusLunas_viPenJasLab_PA').getValue()==='Posting')
                {
                    if (strKriteria == "")
                    {
                        strKriteria = " transaksi.co_status " + "='t'" ;
                    }
                    else
                       {
                            strKriteria += " and transaksi.co_status " + "='t'";
                       }
                }
                if (Ext.get('cboStatusLunas_viPenJasLab_PA').getValue()==='Semua')
                {
                    
                }
                
                
            } */
            if (Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() != "")
            {
                 if (strKriteria == "")
                    {
                        strKriteria = " and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue()+"'";
                        }
                
            }
            if (Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue() != "")
            {
                if (strKriteria == "")
                    {
                        strKriteria = " and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue()+"'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasLab_PA').getValue()+"'";
                        }
                
            }
            /* if (Ext.get('cboUNIT_viKasirLab_PA').getValue() != "")
            {
                    if (strKriteria == "")
                    {
                        strKriteria = " unit.kd_unit ='" + Ext.getCmp('cboUNIT_viKasirLab_PA').getValue() + "'"  ;
                    }
                    else
                       {
                            strKriteria += " and unit.kd_unit ='" + Ext.getCmp('cboUNIT_viKasirLab_PA').getValue() + "'";
                       }
                
            } */
    
     return strKriteria;
}

function getItemPanelPenJasLabDepan_PA()
{
    //var tmptruefalse = true;
    var items =
    {
        layout:'column',
        border:true,
        items:
        [
            {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 100,
                height: 100,
                anchor: '50% 50%',
                items:
                [
                    //-----------COMBO JENIS TRANSAKSI-----------------------------
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Jenis Transaksi '
                    },
                    {
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    mComboJenisTrans_viPenJasLab_PA(),
                    {
                        x: 310,
                        y: 10,
                        xtype: 'label',
                        text: 'Unit Lab '
                    },
                    {
                        x: 445,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    mComboUnitTujuans_viPenJasLab_PA(),
                    //--------RADIO BUTTON PILIHAN ASAL PASIEN---------------------
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Asal Pasien '
                    },
                    {
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 40,
                        xtype: 'radiogroup',
                        id: 'rbrujukanLABPA',
                        fieldLabel: 'Asal Pasien ',
                        items: [
                                    {
                                        boxLabel: 'IGD',
                                        name: 'rb_autoLABPA',
                                        id: 'rb_pilihanLABPA1',
                                        checked: true,
                                        inputValue: '1',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
												Ext.getCmp('dtpTglAwalFilterPenJasLab_PA').setValue(now);
												Ext.getCmp('dtpTglAkhirFilterPenJasLab_PA').setValue(now);
                                                getDataCariUnitPenjasLab_PA("kd_bagian=3 and parent<>'0'");
                                                Ext.getCmp('cboUNIT_viKasirLab_PA').show();
                                                Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
												//--------------TAMBAH BARU 27-SEPTEMBER-2017
												Ext.getCmp('btnEditDataPasienLABPA').disable();
                                                radiovaluesLABPA='1';
                                                validasiJenisTrLABPA();
                                                /* tmpparams = "'" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "'";//tanya
                                                tmpunit = 'ViewRwjPenjasLab';
                                                loadpenjaslabpa(tmpparams, tmpunit); */
                                        }
                                        }
                                    },
                                    {
                                        boxLabel: 'RWJ',
                                        name: 'rb_autoLABPA',
                                        id: 'rb_pilihanLABPA4',
                                        inputValue: '1',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
												Ext.getCmp('dtpTglAwalFilterPenJasLab_PA').setValue(now);
												Ext.getCmp('dtpTglAkhirFilterPenJasLab_PA').setValue(now);
                                                getDataCariUnitPenjasLab_PA("kd_bagian=2 and type_unit=false");
                                                Ext.getCmp('cboUNIT_viKasirLab_PA').show();
                                                Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
                                                radiovaluesLABPA='4';
												//--------------TAMBAH BARU 27-SEPTEMBER-2017
												Ext.getCmp('btnEditDataPasienLABPA').disable();
                                                validasiJenisTrLABPA();
                                                /* tmpparams = "'" + Ext.get('dtpTglAwalFilterPenJasLab_PA').getValue() + "'";//tanya
                                                tmpunit = 'ViewRwjPenjasLab';
                                                loadpenjaslabpa(tmpparams, tmpunit); */
                                        }
                                        }
                                    },
                                    {
                                        boxLabel: 'RWI',
                                        name: 'rb_autoLABPA',
                                        id: 'rb_pilihanLABPA2',
                                        inputValue: '2',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
											Ext.getCmp('dtpTglAwalFilterPenJasLab_PA').setValue(now);
											Ext.getCmp('dtpTglAkhirFilterPenJasLab_PA').setValue(now);
                                            Ext.getCmp('cboUNIT_viKasirLab_PA').hide();
                                            Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').show();
                                            radiovaluesLABPA='2';
											//--------------TAMBAH BARU 27-SEPTEMBER-2017
											Ext.getCmp('btnEditDataPasienLABPA').disable();
                                            validasiJenisTrLABPA();
                                            /* tmpparams = " transaksi.co_Status='0' ORDER BY transaksi.no_transaksi desc limit 100";
                                            tmpunit = 'ViewRwiPenjasLab';
                                            loadpenjaslabpa(tmpparams, tmpunit); */
                                        }
                                     }
                                    },
                                    {
                                        boxLabel: 'Kunj. Langsung',
                                        name: 'rb_autoLABPA',
                                        id: 'rb_pilihanLABPA3',
                                        inputValue: 'K.Kd_Pasien',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
											Ext.getCmp('dtpTglAwalFilterPenJasLab_PA').setValue(now);
											Ext.getCmp('dtpTglAkhirFilterPenJasLab_PA').setValue(now);
                                            Ext.getCmp('cboUNIT_viKasirLab_PA').hide();
                                            Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').show();
                                            radiovaluesLABPA='3';
											//--------------TAMBAH BARU 27-SEPTEMBER-2017
											if (combovalues==='Transaksi Lama'){
												Ext.getCmp('btnEditDataPasienLABPA').enable();
											}else{
												Ext.getCmp('btnEditDataPasienLABPA').disable();
											}
                                            validasiJenisTrLABPA();
                                            //PenJasLab_PALookUp();
                                            //belum
                                            Ext.getCmp('txtNamaUnitLABPA').setValue="Laboratorium Anatomi";
                                        }
                                    }   
                                    },
                                ]
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Detail Asal Pasien '
                    },
                    {
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },
                    //COMBO POLI dan KAMAR
                    mComboUnit_viKasirLABPA(),
                    mcomboKamarSpesialLABPA(),
                    
                ]
            },
            //---------------JARAK 1----------------------------
             {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: false,
                width: 100,
                height: 5,
                anchor: '50% 50%',
                items:
                [
                        
                ]
             },
            //--------------------------------------------------
            {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 100,
                height: 190,
                anchor: '100% 100%',
                items:
                [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec  '
                    },
                    {
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x : 155,
                        y : 10,
                        xtype: 'textfield',
                        fieldLabel: 'No. Medrec (Enter Untuk Mencari)',
                        name: 'txtNoMedrecPenJasLab_PA',
                        id: 'txtNoMedrecPenJasLab_PA',
                        enableKeyEvents: true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrecPenJasLab_PA').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrecPenJasLab_PA').getValue())
                                             Ext.getCmp('txtNoMedrecPenJasLab_PA').setValue(tmpgetNoMedrec);
                                             /* var tmpkriteria = getCriteriaFilter_viDaftar();
                                             refeshpenjaslabpa(tmpkriteria); */
                                             validasiJenisTrLABPA();
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                        /* tmpkriteria = getCriteriaFilter_viDaftar();
                                                        refeshpenjaslabpa(tmpkriteria); */
                                                        validasiJenisTrLABPA();
                                                    }
                                                    else
                                                    Ext.getCmp('txtNoMedrecPenJasLab_PA').setValue('')
                                                    validasiJenisTrLABPA();
                                            }
                                }
                            }

                        }

                    },  
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
                    {
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 40,
                        xtype: 'textfield',
                        name: 'txtNamaPasienPenJasLab_PA',
                        id: 'txtNamaPasienPenJasLab_PA',
                        width: 200,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) 
                                        {
                                            //getCriteriaFilter_viDaftar();
                                            validasiJenisTrLABPA();
                                            /* tmpkriteria = getCriteriaFilter_viDaftar();
                                            refeshpenjaslabpa(tmpkriteria); */

                                            //RefreshDataFilterPenJasRad();

                                        }                       
                                }
                        }
                    },
                    /* {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Posting '
                    },
                    {
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },                  
                    mComboStatusBayar_viPenJasLab_PA(), */
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Lunas '
                    },
                    {
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },                  
                    mComboStatusLunas_viPenJasLab_PA(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kode Nomor Lab '
                    },
                    {
                        x: 145,
                        y: 100,
                        xtype: 'label',
                        text: ':'
                    },                  
                    mCboKeteranganHasilLabFilterDepan(),
                    
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Kelompok Pasien '
                    },
                    {
                        x: 145,
                        y: 130,
                        xtype: 'label',
                        text: ':'
                    },
                    mCboKelompokpasien_Depan(),
                    mComboPerusahaanDepan(),
                    mComboPerseoranganDepan(),
                    mComboAsuransiDepan(),
                    {
                        x: 10,
                        y: 160,
                        xtype: 'label',
                        text: 'Tanggal Kunjungan '
                    },
                    {
                        x: 145,
                        y: 160,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 160,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterPenJasLab_PA',
                        format: 'd/M/Y',
                        value: tigaharilalu
                    },
                    {
                        x: 270,
                        y: 160,
                        xtype: 'label',
                        text: 's/d '
                    },
                    {
                        x: 305,
                        y: 160,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterPenJasLab_PA',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 415,
                        y: 160,
                        xtype:'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        anchor: '25%',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function(sm, row, rec)
                        {
                            validasiJenisTrLABPA();
                            /* tmpparams = getCriteriaFilter_viDaftar();
                            refeshpenjaslabpa(tmpparams); */
                        }
                    }
                    
                ]
            },
            
        //----------------------------------------------------------------  
            
            {
                columnWidth:.10,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 10,
                height: 135,
                anchor: '100% 100%',
                items:
                [
                    
                ]
            },
        //----------------------------------------------------------------    
        ]
    }
    return items;
};


function getTindakanLab_PA(){
    var kd_cus_gettarif=vkode_customer;
    /* if(Ext.get('cboKelPasienLab_PA').getValue()=='Perseorangan'){
        kd_cus_gettarif=Ext.getCmp('cboPerseoranganLab_PA').getValue();
    }else if(Ext.get('cboKelPasienLab_PA').getValue()=='Perusahaan'){
        kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryLab_PA').getValue();
    }else {
        kd_cus_gettarif=Ext.getCmp('cboAsuransiLab_PA').getValue();
      } */
    var modul='';
    if(radiovaluesLABPA == 1){
        modul='igd';
    } else if(radiovaluesLABPA == 2){
        modul='rwi';
    } else if(radiovaluesLABPA == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/lab_pa/functionLABPA/getGridProduk",
            params: {kd_unit:tmpkd_unit_tarif_mir,
                     kd_customer:kd_cus_gettarif,
                     notrans:Ext.getCmp('txtNoTransaksiPenJasLab_PA').getValue(),
                     penjas:modul,
                     kdunittujuan:tmpkd_unit
                    },
            failure: function(o)
            {
                ShowPesanErrorPenJasLab_PA('Error, pasien! Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                
                if (cst.success === true) 
                {
                    
                    var recs=[],
                        recType=dsDataGrd_getTindakanPenjasLab_PA.recordType;
                        
                    for(var i=0; i<cst.ListDataObj.length; i++){
                
                        recs.push(new recType(cst.ListDataObj[i]));
                        
                    }
                    dsDataGrd_getTindakanPenjasLab_PA.add(recs);
                    
                    GridGetTindakanPenjasLab_PA.getView().refresh();
                    
                    for(var i = 0 ; i < dsDataGrd_getTindakanPenjasLab_PA.getCount();i++)
                    {
                        var o= dsDataGrd_getTindakanPenjasLab_PA.getRange()[i].data;
                        for(var je = 0 ; je < dsTRDetailPenJasLab_PAList.getCount();je++)
                        {
                            
                            var p= dsTRDetailPenJasLab_PAList.getRange()[je].data;
                            if (o.kd_produk === p.kd_produk)
                            {
                                o.pilihchkproduklabpa = true;
                                
                            }
                            
                        }
                    }
                    GridGetTindakanPenjasLab_PA.getView().refresh();
                }
                else 
                {
                    ShowPesanErrorPenJasLab_PA('Gagal membaca data pasien ini', 'Error');
                };
            }
        }
        
    )
    
}

function setLookUp_getTindakanPenjasLab_PA(rowdata){
    var lebar = 985;
    setLookUps_getTindakanPenjasLab_PA = new Ext.Window({
        id: 'FormLookUpGetTindakan',
        title: 'Daftar Pemeriksaan', 
        closeAction: 'destroy',        
        width: 600,
        height: 330,
        resizable:false,
        autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,        
        items: getFormItemEntry_getTindakanPenjasLab_PA(lebar,rowdata),
        listeners:{
            activate: function(){
                
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
               // rowSelected_getTindakanPenjasLab_PA=undefined;
            }
        }
    });

    setLookUps_getTindakanPenjasLab_PA.show();
   
}


function getFormItemEntry_getTindakanPenjasLab_PA(lebar,rowdata){
    var pnlFormDataBasic_getTindakanPenjasLab_PA = new Ext.FormPanel({
        title: '',
        region: 'north',
        layout: 'form',
        bodyStyle: 'padding:10px 10px 10px 10px',
        anchor: '100%',
        labelWidth: 1,
        autoWidth: true,
        width: lebar,
        border: false,
        items:[
                gridDataViewEdit_getTindakanPenjasLab_PA()
            ],
            fileUpload: true,
            tbar: {
                xtype: 'toolbar',
                items: 
                [
                    {
                        xtype: 'button',
                        text: 'Tambahkan',
                        id:'btnTambahKanPermintaan_getTindakanPenjasLab_PA',
                        iconCls: 'add',
                        //disabled:true,
                        handler:function()
                        {
                            /* var jumlah=0;
                            var items=[];
                            for(var i = 0 ; i < dsDataGrd_getTindakanPenjasLab_PA.getCount();i++){
                                var o=dsDataGrd_getTindakanPenjasLab_PA.getRange()[i].data;
                                    if(o.pilihchkproduklabpa == true){
                                        jumlah += 1;
                                        
                                        // for(var x = 0; x < 100; x++){
                                            // arr[x] = [];    
                                            // for(var y = 0; y < 100; y++){ 
                                                // arr[x][y] = x*y;    
                                            // }    
                                        // }
                                        var recs=[],
                                            recType=dsTRDetailPenJasLab_PAList.recordType;

                                        for(var i=0; i<dsDataGrd_getTindakanPenjasLab_PA.getCount(); i++){

                                            recs.push(new recType(dsDataGrd_getTindakanPenjasLab_PA.data.items[i].data));

                                        }
                                        dsTRDetailPenJasLab_PAList.add(recs);

                                        gridDTItemTest.getView().refresh();
                                    }
                            }
                            console.log(items[0][0]) */
                            
                            
                            
                            var tmp_nomer_lab = Ext.getCmp('txtNomorLab_PA').getValue();
                            if (tmp_nomer_lab.length > 0) {
                                console.log(dsDataGrd_getTindakanPenjasLab_PA.getCount());
                                dsTRDetailPenJasLab_PAList.removeAll();
                                var recs=[],
                                recType=dsTRDetailPenJasLab_PAList.recordType;
                                var adadata=false;
                                for(var i = 0 ; i < dsDataGrd_getTindakanPenjasLab_PA.getCount();i++)
                                {
                                    console.log(dsDataGrd_getTindakanPenjasLab_PA.data.items[i].data.pilihchkproduklabpa);
                                    if (dsDataGrd_getTindakanPenjasLab_PA.data.items[i].data.pilihchkproduklabpa === true)
                                    {
                                        console.log(dsDataGrd_getTindakanPenjasLab_PA.data.items[i].data);
                                        recs.push(new recType(dsDataGrd_getTindakanPenjasLab_PA.data.items[i].data));
                                        adadata=true;
                                    }
                                }
                                dsTRDetailPenJasLab_PAList.add(recs);
                                gridDTItemTest.getView().refresh(); 
                                console.log(recs);
                                if (adadata===true)
                                {
                                    setLookUps_getTindakanPenjasLab_PA.close(); 
                                    loadMask.show();
                                    Datasave_PenJasLab_PA(false); 
                                    loadMask.hide();    
                                }
                                else
                                {
                                    ShowPesanWarningPenJasLab_PA('Ceklis data item pemeriksaan  ','Lab_PAoratorium');
                                }
                            }else{
                                Ext.Msg.confirm('Warning', 'Nomer LAB masih kosong, apakah anda yakin untuk melanjutkan pembayaran?', function(button){
                                    if (button == 'yes'){
                                        console.log(dsDataGrd_getTindakanPenjasLab_PA.getCount());
                                        dsTRDetailPenJasLab_PAList.removeAll();
                                        var recs=[],
                                        recType=dsTRDetailPenJasLab_PAList.recordType;
                                        var adadata=false;
                                        for(var i = 0 ; i < dsDataGrd_getTindakanPenjasLab_PA.getCount();i++)
                                        {
                                            console.log(dsDataGrd_getTindakanPenjasLab_PA.data.items[i].data.pilihchkproduklabpa);
                                            if (dsDataGrd_getTindakanPenjasLab_PA.data.items[i].data.pilihchkproduklabpa === true)
                                            {
                                                console.log(dsDataGrd_getTindakanPenjasLab_PA.data.items[i].data);
                                                recs.push(new recType(dsDataGrd_getTindakanPenjasLab_PA.data.items[i].data));
                                                adadata=true;
                                            }
                                        }
                                        dsTRDetailPenJasLab_PAList.add(recs);
                                        gridDTItemTest.getView().refresh(); 
                                        console.log(recs);
                                        if (adadata===true)
                                        {
                                            setLookUps_getTindakanPenjasLab_PA.close(); 
                                            loadMask.show();
                                            Datasave_PenJasLab_PA(false); 
                                            loadMask.hide();    
                                        }
                                        else
                                        {
                                            ShowPesanWarningPenJasLab_PA('Ceklis data item pemeriksaan  ','Lab_PAoratorium');
                                        }
                                    }else{
                                        Ext.getCmp('txtNomorLab_PA').focus();
                                    }
                                });
                            }                           
                        }
                          
                    },
                    
                    
                ]
            }//,items:
        }
    )

    return pnlFormDataBasic_getTindakanPenjasLab_PA;
}

function RefreshDatahistoribayar_LABPA(no_transaksi)
{
    var strKriteriaKasirLABPA = '';

    strKriteriaKasirLABPA = 'no_transaksi= ~' + no_transaksi + '~ and kd_kasir= ~'+kdkasir+'~';

    dsTRDetailHistoryList_labpa.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewHistoryBayar',
                                    param: strKriteriaKasirLABPA
                                }
                    }
            );
    return dsTRDetailHistoryList_labpa;
}
;

function GetDTLTRHistoryGrid()
{

    var fldDetailLABPA = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'SHIFT', 'KD_USER'];

    dsTRDetailHistoryList_labpa = new WebApp.DataStore({fields: fldDetailLABPA})

    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'History Bayar',
                        stripeRows: true,
                        store: dsTRDetailHistoryList_labpa,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100% 25%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
                                                            console.log(row);
                                                            cellSelecteddeskripsi = dsTRDetailHistoryList_labpa.getAt(row);
                                                            CurrentHistory.row = row;
                                                            CurrentHistory.data = cellSelecteddeskripsi;
                                                            Ext.getCmp('btnHpsBrsKasirLABPA').enable();
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRHistoryColumModel(),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnHpsBrsKasirLABPA',
                                        text: 'Hapus Pembayaran',
                                        tooltip: 'Hapus Baris',
                                        iconCls: 'RemoveRow',
                                        //hidden :true,
                                        handler: function ()
                                        {
                                            if (dsTRDetailHistoryList_labpa.getCount() > 0)
                                            {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                    if (CurrentHistory != undefined)
                                                    {
                                                        HapusBarisDetailbayar();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningPenJasLab('Pilih record ', 'Hapus data');
                                                }
                                            }
                                            /* Ext.getCmp('btnEditKasirLABPA').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirLABPA').disable();
                                            Ext.getCmp('btnHpsBrsKasirLABPA').disable(); */
                                        },
                                        disabled: true
                                    }
                                ]
                    }

            );

    return gridDTLTRHistory;
}
;
function HapusBarisDetailbayar()
{
    if (cellSelecteddeskripsi != undefined)
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
            //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
            /*var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
             if (btn == 'ok')
             {
             variablehistori=combo;
             if (variablehistori!='')
             {
             DataDeleteKasirLABPAKasirDetail();
             }
             else
             {
             ShowPesanWarningKasirLABPA('Silahkan isi alasan terlebih dahaulu','Keterangan');
             }
             }  
             });  */
            msg_box_alasanhapus_LABPA();
        } else
        {
            dsTRDetailHistoryList.removeAt(CurrentHistory.row);
        }
        ;
    }
}
;
function msg_box_alasanhapus_LABPA()
{
    var lebar = 250;
    form_msg_box_alasanhapus_LABPA = new Ext.Window
            (
                    {
                        id: 'alasan_hapusLAb',
                        title: 'Alasan Hapus Pembayaran',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    //fieldLabel: 'No. Transaksi ',
                                                    name: 'txtAlasanHapusLABPA',
                                                    id: 'txtAlasanHapusLABPA',
                                                    emptyText: 'Alasan Hapus',
                                                    anchor: '99%',
                                                },
                                               //mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'hapus',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        DataDeleteKasirLABPAKasirDetail();
                                                                        form_msg_box_alasanhapus_LABPA.close();
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanhapus_LABPA.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanhapus_LABPA.show();
}
;

function msg_box_alasanbataltrans_LABPA()
{
    var lebar = 250;
    form_msg_box_alasanbataltrans_LABPA = new Ext.Window
            (
                    {
                        id: 'alasan_bataltransLAb',
                        title: 'Alasan Batal Transaksi',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    //fieldLabel: 'No. Transaksi ',
                                                    name: 'txtAlasanBatalTransLABPA',
                                                    id: 'txtAlasanBatalTransLABPA',
                                                    emptyText: 'Alasan Batal',
                                                    anchor: '99%',
                                                },
                                               //mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Ok',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        var params = {
                                                                            kd_unit: rowSelectedPenJasLab_PA.data.KD_UNIT,
                                                                            Tglkunjungan: rowSelectedPenJasLab_PA.data.MASUK,
                                                                            Kodepasein: rowSelectedPenJasLab_PA.data.KD_PASIEN,
                                                                            urut: rowSelectedPenJasLab_PA.data.URUT_MASUK,
                                                                            keterangan : Ext.getCmp('txtAlasanBatalTransLABPA').getValue()
                                                                        };
                                                                        Ext.Ajax.request({
                                                                            url: baseURL + "index.php/lab_pa/functionLABPA/deletekunjungan",
                                                                            params: params,
                                                                            failure: function (o){
                                                                                loadMask.hide();
                                                                                ShowPesanInfoPenJasLab_PA('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
                                                                                validasiJenisTrLABPA();
                                                                            },
                                                                            success: function (o){
                                                                                var cst = Ext.decode(o.responseText);
                                                                                if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === false){
                                                                                    ShowPesanWarningPenJasLab_PA('Data transaksi tidak berhasil ditemukan', 'Hapus Data Kunjungan');
                                                                                } else if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === true){
                                                                                    ShowPesanWarningPenJasLab_PA('Anda telah melakukan pembayaran', 'Hapus Data Kunjungan');
                                                                                } else if (cst.success === true){
                                                                                    validasiJenisTrLABPA();
                                                                                    ShowPesanInfoPenJasLab_PA('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
                                                                                } else if (cst.success === false && cst.pesan === 0){
                                                                                    ShowPesanWarningPenJasLab_PA('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                                                } else{
                                                                                    ShowPesanErrorPenJasLab_PA('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                                                }
                                                                            }
                                                                        }); 
                                                                        form_msg_box_alasanbataltrans_LABPA.close();
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanbataltrans_LABPA.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanbataltrans_LABPA.show();
}
;

function mComboalasan_hapusLAb()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_hapusLAb = new WebApp.DataStore({fields: Field});

    dsalasan_hapusLAb.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'alasanhapus',
                                    param: ''
                                }
                    }
            );
    var cboalasan_hapusLAb = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_hapusLAb',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Alasan...',
                        labelWidth: 1,
                        store: dsalasan_hapusLAb,
                        valueField: 'KD_ALASAN',
                        displayField: 'ALASAN',
                        anchor: '95%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
//                                  var selectalasan_hapusLAb = b.data.KD_PROPINSI;
                                        //alert("is");
                                        selectDokter = b.data.KD_DOKTER;
                                    },
                                    'render': function (c)
                                    {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('kelPasien').focus();
                                        }, c);
                                    }
                                }
                    }
            );

    return cboalasan_hapusLAb;
}
;
function DataDeleteKasirLABPAKasirDetail()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/deletedetail_bayar",
                        params: getParamDataDeleteKasirLABPAKasirDetail(),
                        success: function (o)
                        {
                            //  RefreshDatahistoribayar_LABPA(Kdtransaksi);
                           // RefreshDataFilterKasirLABPAKasir();
                            //RefreshDatahistoribayar_LABPA('0');
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoPenJasLab_PA("Proses Hapus Pembayaran Berhasil", nmHeaderHapusData);
                                Ext.getCmp('btnPembayaranPenjasLABPA').enable();
                                Ext.getCmp('btnTutupTransaksiPenjasLABPA').enable();
                                RefreshDatahistoribayar_LABPA(Ext.getCmp('txtNoTransaksiPenJasLab_PA').getValue());
                                if (radiovaluesLABPA !=='3' && combovalues !== 'Transaksi Baru'){
                                    validasiJenisTrLABPA();
                                }
                                //alert(Kdtransaksi);                    

                                //refeshKasirLABPAKasir();
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningPenJasLab_PA(nmPesanHapusGagal, nmHeaderHapusData);
                            }	else  if (cst.success === false && cst.type === 6)
                            {
                                ShowPesanWarningPenJasLab_PA("Transaksi sudah ditutup.", nmHeaderHapusData);
                            } else
                            {
                                ShowPesanWarningPenJasLab_PA(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirLABPAKasirDetail()
{
    var params =
            {
                TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
                TrTglbayar: CurrentHistory.data.data.TGL_BAYAR,
                Urut: CurrentHistory.data.data.URUT,
                Jumlah: CurrentHistory.data.data.BAYAR,
                Username: CurrentHistory.data.data.USERNAME,
                Shift: tampungshiftsekarang,
                Shift_hapus: CurrentHistory.data.data.SHIFT,
                Kd_user: CurrentHistory.data.data.KD_USER,
                Tgltransaksi: TglTransaksi,
                Kodepasein: kodepasien,
                NamaPasien: namapasien,
                KodeUnit: kodeunit,
                Namaunit: namaunit,
                Kodepay: kodepay,
                Uraian: CurrentHistory.data.data.DESKRIPSI,
                KDkasir: kdkasir,
                KeTterangan: Ext.get('txtAlasanHapusLABPA').dom.value

            };
    Kdtransaksi = CurrentHistory.data.data.NO_TRANSAKSI;
    return params
}
;


function TRHistoryColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Bayar',
                            dataIndex: 'URUT',
                            //hidden:true

                        },
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        },
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }
                        },
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colStatHistory',
                            header: 'History',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colPetugasHistory',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME',
                            //hidden:true
                        }
                    ]
                    )
}
;
//var a={};
function gridDataViewEdit_getTindakanPenjasLab_PA(){
    var FieldGrdKasir_getTindakanPenjasLab_PA = [];
    dsDataGrd_getTindakanPenjasLab_PA= new WebApp.DataStore({
        fields: FieldGrdKasir_getTindakanPenjasLab_PA
    });
    chkgetTindakanPenjasLab_PA = new Ext.grid.CheckColumn
        (
            {
                
                id: 'chkgetTindakanPenjasLab_PA',
                header: 'Pilih',
                align: 'center',
                //disabled:false,
                sortable: true,
                dataIndex: 'pilihchkproduklabpa',
                anchor: '10% 100%',
                width: 30,
                listeners: 
                {
                    checkchange: function()
                    {
                        alert('hai');
                    }
                }
                /* renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    switch (value) {
                        case 't':
                            alert('hai');
                            return true;
                            
                            break;
                        case 'f':
                            alert('fei');
                            return false;
                            break;
                    }
                    return false;
                } */  
        
            }
        ); 
    GridGetTindakanPenjasLab_PA =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
        //title: 'Dafrar Pemeriksaan',
        store: dsDataGrd_getTindakanPenjasLab_PA,
        autoScroll: true,
        columnLines: true,
        border: false,
        height: 250,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
                (
                        {
                            singleSelect: true,
                            listeners:
                                    {
                                    }
                        }
                ),
        listeners:
                {
                    // Function saat ada event double klik maka akan muncul form view
                    rowclick: function (sm, ridx, cidx)
                    {
                        console.log(GridGetTindakanPenjasLab_PA);
                        //Ext.getCmp('chkgetTindakanPenjasLab_PA').setValue(true);
                    },
                    rowdblclick: function (sm, ridx, cidx)
                    {
                        
                    }

                },
        /* xtype: 'editorgrid',
        store: dsDataGrd_getTindakanPenjasLab_PA,
        height: 250,
        autoScroll: true,
        columnLines: true,
        border: false,
        plugins: [ new Ext.ux.grid.FilterRow(),chkgetTindakanPenjasLab_PA ],
        selModel: new Ext.grid.RowSelectionModel ({
            singleSelect: true,
            listeners:{
                rowclick: function(sm, row, rec){
                    /* rowSelectedGridPasien_viGzPermintaanDiet = undefined;
                    rowSelectedGridPasien_viGzPermintaanDiet = dsDataGrdPasien_viGzPermintaanDiet.getAt(row);
                    CurrentDataPasien_viGzPermintaanDiet
                    CurrentDataPasien_viGzPermintaanDiet.row = row;
                    CurrentDataPasien_viGzPermintaanDiet.data = rowSelectedGridPasien_viGzPermintaanDiet.data;
                    
                    dsTRDetailDietGzPermintaanDiet.removeAll();
                    getGridWaktu(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
                    
                    if(GzPermintaanDiet.vars.status_order == 'false'){
                        Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
                        Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
                    } else{
                        Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
                        Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
                    }
                    Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue(CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
                     
                },
            }
        }), */
        //stripeRows: true,
        
        colModel:new Ext.grid.ColumnModel([
            //new Ext.grid.RowNumberer(),   
            {
                dataIndex: 'kd_produk',
                header: 'KD Produk',
                sortable: true,
                width: 70,
                hidden : true,
            },{
                dataIndex: 'kp_produk',
                header: 'Kode Produk',
                sortable: true,
                width: 70,
                 filter: {}
            },
            {
                dataIndex: 'deskripsi',
                header: 'Nama Pemeriksaan',
                width: 70,
                align:'left',
                 filter: {}
            },{
                dataIndex: 'uraian',
                header: 'Uraian',
                hidden : true,
                sortable: true,
                width: 70
            },
            {
                dataIndex: 'kd_tarif',
                header: 'Kd Tarif',
                width: 70,
                hidden : true,
                align:'center'
            },{
                dataIndex: 'tgl_transaksi',
                header: 'Tgl Transaksi',
                hidden : true,
                sortable: true,
                width: 70
            },
            {
                dataIndex: 'tgl_berlaku',
                header: 'Tgl Berlaku',
                width: 70,
                hidden : true,
                align:'center'
            },{
                dataIndex: 'harga',
                header: 'harga',
                hidden : true,
                sortable: true,
                width: 70
            },
            {
                dataIndex: 'qty',
                header: 'qty',
                width: 70,
                hidden : true,
                align:'center'
            },{
                dataIndex: 'jumlah',
                header: 'jumlah',
                width: 70,
                hidden : true,
                align:'center'
            },
            chkgetTindakanPenjasLab_PA
        ]),
        viewConfig:{
            forceFit: true
        } 
    });
    return GridGetTindakanPenjasLab_PA;
}

function getDokterPengirim(no_transaksi,kd_kasir){
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionLAB/getDokterPengirim",
            params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
            failure: function(o)
            {
                ShowPesanErrorPenJasLab_PA('Hubungi Admin', 'Error');
            },
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    //Ext.getCmp('txtDokterPengirimLABPA').setValue(cst.nama);
                    Ext.getCmp('cboDokterPengirimLABPA').setValue(cst.nama);
                }
                else
                {
                    ShowPesanErrorPenJasLab_PA('Gagal membaca dokter pengirim', 'Error');
                };
            }
        }

    )
}

function printbillRadLab_PA()
{
	if (tombol_bayar==='disable'){
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakbill_vikasirDaftarRadLab_PA(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarning_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            } else
                            {
                                ShowPesanError_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            }
                        }
                    }
            );
	}else{
		ShowPesanWarningPenJasLab_PA('Lakukan pembayaran atau transfer terlebih dahulu !', 'Cetak Data');
	}
}
;

var tmpkdkasirRadLab_PA = '1';
function dataparamcetakbill_vikasirDaftarRadLab_PA()
{
    var paramscetakbill_vikasirDaftarRadLab_PA =
            {
                Table: 'DirectPrintingRadLab',
                notrans_asal:notransaksiasal_labpa,
                No_TRans: Ext.get('txtNoTransaksiKasirLABPAKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'lab',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirLABPA').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirLABPA').getValue(),
                printer: Ext.getCmp('cbopasienorder_printer_kasirlabpa').getValue()
            };
    var urut=[];
    var kd_produk=[];
    var items=Ext.getCmp('gridDTLTRKasirLABPA').getStore().data.items;
    for(var i=0,iLen=items.length; i<iLen; i++){
        if(items[i].data.TAG==true){
            urut.push(items[i].data.URUT);
            kd_produk.push(items[i].data.KD_PRODUK);
        }
    }
    paramscetakbill_vikasirDaftarRadLab_PA['no_urut[]']=urut;
    paramscetakbill_vikasirDaftarRadLab_PA['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarRadLab_PA;
    return paramscetakbill_vikasirDaftarRadLab_PA;
}
;

function printkwitansiRadLab_PA()
{
	if (tombol_bayar==='disable'){
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakkwitansi_vikasirDaftarRadLab_PA(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan !== '')
                            {
                                ShowPesanWarningKasirLABPA('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanWarningKasirLABPA('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            );
	}else{
		ShowPesanWarningPenJasLab_PA('Lakukan pembayaran atau transfer terlebih dahulu !', 'Cetak Data');
	}
}
;

function dataparamcetakkwitansi_vikasirDaftarRadLab_PA()
{
    var paramscetakbill_vikasirDaftarRadLab_PA =
            {
                Table: 'DirectKwitansiRadLab',
                No_TRans: Ext.get('txtNoTransaksiKasirLABPAKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'lab',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirLABPA').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirLABPA').getValue(),
                printer: Ext.getCmp('cbopasienorder_printer_kasirlabpa').getValue()
            };
    var urut=[];
    var kd_produk=[];
    var items=Ext.getCmp('gridDTLTRKasirLABPA').getStore().data.items;
    for(var i=0,iLen=items.length; i<iLen; i++){
        if(items[i].data.TAG==true){
            urut.push(items[i].data.URUT);
            kd_produk.push(items[i].data.KD_PRODUK);
        }
    }
    paramscetakbill_vikasirDaftarRadLab_PA['no_urut[]']=urut;
    paramscetakbill_vikasirDaftarRadLab_PA['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarRadLab_PA;
}
;

function mCombo_printer_kasirLABPA()
{ 
 
    var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];

    dsprinter_kasirLABPA = new WebApp.DataStore({ fields: Field });
    
    load_data_printer_kasirLABPA();
    var cbo_printer_kasirLABPA= new Ext.form.ComboBox
    (
        {
            id: 'cbopasienorder_printer_kasirLABPA',
            typeAhead       : true,
            triggerAction   : 'all',
            lazyRender      : true,
            mode            : 'local',
            emptyText: 'Pilih Printer',
            fieldLabel:  '',
            align: 'Right',
            width: 100,
            store: dsprinter_kasirLABPA,
            valueField: 'name',
            displayField: 'name',
            //hideTrigger       : true,
            listeners:
            {
                                
            }
        }
    );return cbo_printer_kasirLABPA;
};

function load_data_printer_kasirLABPA(param)
{

    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/functionLAB/getPrinter",
        params:{
            command: param
        } ,
        failure: function(o)
        {
             var cst = Ext.decode(o.responseText);
            
        },      
        success: function(o) {
            //cbopasienorder_mng_apotek.store.removeAll();
                var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = dsprinter_kasirLABPA.recordType;
                var o=cst['listData'][i];
                
                recs.push(new recType(o));
                dsprinter_kasirLABPA.add(recs);
                console.log(o);
            }
        }
    });
}


function Datasave_PenJasLab_PA_SQL(mBol) 
{   
     if (ValidasiEntryPenJasLab_PA(nmHeaderSimpanData,false) == 1 )
     {
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/CreateDataObj",
                    params: getParamDetailTransaksiLABPA_SQL(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasLab_PA('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value);
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            // loadMask.hide();
                            // ShowPesanInfoPenJasLab_PA("Berhasil menyimpan data ini","Information");
                            // Ext.get('txtNoTransaksiPenJasLab_PA').dom.value=cst.notrans;
                            // Ext.get('txtNoMedrecLABPA').dom.value=cst.kdPasien;
                            // ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value);
                            // Ext.getCmp('btnPembayaranPenjasLABPA').enable();
                            // Ext.getCmp('btnTutupTransaksiPenjasLABPA').enable();
                            // if(mBol === false)
                            // {
                            //     ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value);
                            // };
                        }
                        else 
                        {
                                ShowPesanWarningPenJasLab_PA('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

function Datasave_LABPA_LIS(mBol) 
{   
     /* if (ValidasiEntryPenJasLab_PA(nmHeaderSimpanData,false) == 1 )
     { */
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/functionLAB/savedblis",
                    params: getParamDetailTransaksiLABPA_SQL(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasLab_PA('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value,kd_kasir_labpa);
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            // loadMask.hide();
                            // ShowPesanInfoPenJasLab_PA("Berhasil menyimpan data ini","Information");
                            // Ext.get('txtNoTransaksiPenJasLab_PA').dom.value=cst.notrans;
                            // Ext.get('txtNoMedrecLABPA').dom.value=cst.kdPasien;
                            // ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value);
                            // Ext.getCmp('btnPembayaranPenjasLABPA').enable();
                            // Ext.getCmp('btnTutupTransaksiPenjasLABPA').enable();
                            // if(mBol === false)
                            // {
                            //     ViewGridBawahLookupPenjasLab_PA(Ext.get('txtNoTransaksiPenJasLab_PA').dom.value);
                            // };
                        }
                        else 
                        {
                                ShowPesanWarningPenJasLab_PA('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     /* }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    };  */
    
};

function getParamDetailTransaksiLABPA_SQL() 
{
    
    var KdCust='';
    var TmpCustoLama='';
    var pasienBaru;
    var modul='';
    if(radiovaluesLABPA == 1){
        modul='igd';
    } else if(radiovaluesLABPA == 2){
        modul='rwi';
    } else if(radiovaluesLABPA == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    
    if(Ext.getCmp('txtNoMedrecLABPA').getValue() == ''){
        pasienBaru=1;
    } else{
        pasienBaru=0;
    }
    var params =
    {
        Table: 'SQL_LAB',
        KdTransaksi: Ext.get('txtNoTransaksiPenJasLab_PA').getValue(),
        KdPasien:Ext.getCmp('txtNoMedrecLABPA').getValue(),
        NmPasien:Ext.getCmp('txtNamaPasienLABPA').getValue(),
        Ttl:Ext.getCmp('dtpTtlLABPA').getValue(),
        Alamat:Ext.getCmp('txtAlamatLABPA').getValue(),
        JK:jenisKelaminPasien,
        GolDarah:Ext.getCmp('cboGDRLABPA').getValue(),
        KdUnit: Ext.getCmp('txtKdUnitLab_PA').getValue(),
        KdDokter:tmpkddoktertujuan,
        KdUnitTujuan:tmpkd_unit,
        Tgl: Ext.getCmp('dtpKunjunganLABPA').getValue(),
        TglTransaksiAsal:TglTransaksi,
        Tlp: Ext.getCmp('txtnotlplabpa').getValue(),
        urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
        KdCusto:vkode_customer,
        TmpCustoLama:vkode_customer,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
        NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiLab_PA').getValue(),
        NoAskes:Ext.getCmp('txtNoAskesLab_PA').getValue(),
        NoSJP:Ext.getCmp('txtNoSJPLab_PA').getValue(),
        Shift:tampungshiftsekarang,
        List:getArrPenJasLab_PA(),
        JmlField: mRecordLABPA.prototype.fields.length-4,
        JmlList:GetListCountDetailTransaksiLABPA(),
        dtBaru:databaru,
        Hapus:1,
        Ubah:0,
        unit:41,
        TmpNotransaksi:TmpNotransaksi,
        KdKasirAsal:KdKasirAsal,
        KdSpesial:Kd_Spesial,
        Kamar:No_Kamar,
        pasienBaru:pasienBaru,
        listTrDokter : '',
        Modul:modul,
        KdProduk:'',
        URUT :tmp_urut_sql,
        kunjungan_labpa:tmppasienbarulama,
        //-----------ABULHASSAN-----------20_01_2017
        kodeKasir_LIS:tmpkd_kasir_sql,
        namaCusto_LIS: namaCustomer_LIS,
        guarantorNM_LIS: namaKelompokPasien_LIS,
        kodeDokterPengirim_LIS:tmpkddokterpengirim
        
    };
    return params
};
//---- baru
function fnDlgLABPAPasswordDulu()
{
    winLABPAPasswordDulu = new Ext.Window
    (
        {
            id: 'winLABPAPasswordDulu',
            title: 'Password',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLABPAPasswordDulu()]

        }
    );

    winLABPAPasswordDulu.show();
};
function ItemDlgLABPAPasswordDulu()
{
    var PnlLapLABPASPasswordDulu = new Ext.Panel
    (
        {
            id: 'PnlLapLABPASPasswordDulu',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapLABPAPasswordDulu_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                         {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnOkLapLABPAPasswordDulu',
                            handler: function()
                            {
								Ext.Ajax.request
								(
									{
										//url: "./Datapool.mvc/CreateDataObj",
										url: baseURL + "index.php/lab_pa/functionLABPA/cekpassword_labpa",
										params: {passDulu:Ext.getCmp('TxPasswordDuluLABPA').getValue()},
										failure: function(o)
										{
											ShowPesanWarningPenJasLab_PA('Proses Gagal segera Hubungi Admin', 'Gagal');
										//RefreshDataKasirRwiDetail(notransaksi);
										},
										success: function(o)
										{
											//RefreshDataKasirRwiDetail(notransaksi);
											var cst = Ext.decode(o.responseText);
											if (cst.success === true)
											{
												winLABPAPasswordDulu.close();
												GantiDokterPasienLookUp_labpa();
														
											}
											else
											{
												ShowPesanWarningPenJasLab_PA('Password salah segera Hubungi Admin', 'Gagal');
											};
										}
									}
								)
							   /* */

								    
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnCancelLapLABPAPasswordDulu',
                            handler: function()
                            {
                                    winLABPAPasswordDulu.close();
                            }
                        } 
                    ]
                }
            ]
        }
    );

    return PnlLapLABPASPasswordDulu;
};
function getItemLapLABPAPasswordDulu_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .90,
			    layout: 'form',
				 labelWidth:120,
				  border: false,
				 
			    items:
				[
					{
						xtype: 'textfield',
						inputType: 'password',
						fieldLabel: 'Masukan Password ',
						id: 'TxPasswordDuluLABPA',
					    anchor: '99%'
					},
				
					
				]
			},
     
        ]
    }
    return items;
};

//------------------TAMBAH BARU 27-September-2017
function setLookUpGridDataView_viLABPA(rowdata){
	var lebar = 819; // Lebar Form saat PopUp
	setLookUpsGridDataView_viLABPA = new Ext.Window({
		id: 'setLookUpsGridDataView_viLABPA',
		title: 'Edit Data Pasien',
		closeAction: 'destroy',
		autoScroll: true,
		width: 640,
		height: 225,
		resizable: false,
		border: false,
		plain: true,
		layout: 'fit',
		modal: true,
		items: getFormItemEntryGridDataView_viLABPA(lebar, rowdata),
		listeners:{
			activate: function (){
			},
			afterShow: function (){
				this.activate();
			},
			deactivate: function (){
				
			}
		}
	});
	setLookUpsGridDataView_viLABPA.show();
	datainit_viLABPA(rowdata)
	
}
function getFormItemEntryGridDataView_viLABPA(lebar, rowdata)
{
	var pnlFormDataWindowPopup_viLABPA = new Ext.FormPanel
			(
					{
						title: '',
						// region: 'center',
						fileUpload: true,
						margin:true,
						// layout: 'anchor',
						// padding: '8px',
						bodyStyle: 'padding-top:5px;',
						// Tombol pada tollbar Edit Data Pasien
						tbar: {
							xtype: 'toolbar',
							items:
									[
										{
											xtype: 'button',
											text: 'Save',
											id: 'btnSimpanWindowPopup_viLABPA',
											iconCls: 'save',
											handler: function (){
												datasave_EditDataPasienLABPA(false); //1.1
												
											}
										},
										//-------------- ## --------------
										{
											xtype: 'tbseparator'
										},
										//-------------- ## --------------
										{
											xtype: 'button',
											text: 'Save & Close',
											id: 'btnSimpanExitWindowPopup_viLABPA',
											iconCls: 'saveexit',
											handler: function ()
											{
												var x = datasave_EditDataPasienLABPA(false); //1.2
												
												if (x === undefined)
												{
													setLookUpsGridDataView_viLABPA.close();
												}
											}
										},
										//-------------- ## --------------
										{
											xtype: 'tbseparator'
										},
										//-------------- ## --------------
									]
						},
						//-------------- #items# --------------
						items:
								[
									// Isian Pada Edit Data Pasien
									{
										xtype: 'panel',
										title: '',
										layout:'form',
										margin:true,
										border:false,
										labelAlign: 'right',
										width: 610,
										items:
												[//---------------# Penampung data untuk fungsi update # ---------------
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpAgama_viLABPA',
														name: 'TxtTmpAgama_viLABPA',
														readOnly: true,
														hidden: true
													},
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpPekerjaan_viLABPA',
														name: 'TxtTmpPekerjaan_viLABPA',
														readOnly: true,
														hidden: true
													},
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpPendidikan_viLABPA',
														name: 'TxtTmpPendidikan_viLABPA',
														readOnly: true,
														hidden: true
													},
													//------------------------ end ---------------------------------------------								
													//-------------- # Pelengkap untuk kriteria ke Net. # --------------
													{
														xtype: 'textfield',
														fieldLabel: 'KD_CUSTOMER',
														id: 'TxtWindowPopup_KD_CUSTOMER_viLABPA',
														name: 'TxtWindowPopup_KD_CUSTOMER_viLABPA',
														readOnly: true,
														hidden: true
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'URUT_MASUK',
														id: 'TxtWindowPopup_Urut_Masuk_viLABPA',
														name: 'TxtWindowPopup_Urut_Masuk_viLABPA',
														readOnly: true,
														hidden: true
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'KD_PASIEN',
														id: 'TxtWindowPopup_KD_PASIEN_viLABPA',
														name: 'TxtWindowPopup_KD_PASIEN_viLABPA',
														readOnly: true,
														hidden: true
													},
													//-------------- # End Pelengkap untuk kriteria ke Net. # --------------
													{
														xtype: 'textfield',
														fieldLabel: 'No. MedRec',
														id: 'TxtWindowPopup_NO_RM_viLABPA',
														name: 'TxtWindowPopup_NO_RM_viLABPA',
														emptyText: 'No. MedRec',
														labelSeparator: '',
														readOnly: true,
														flex: 1,
														width: 195
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'Nama',
														id: 'TxtWindowPopup_NAMA_viLABPA',
														name: 'TxtWindowPopup_NAMA_viLABPA',
														emptyText: 'Nama',
														labelSeparator: '',
														flex: 1,
														anchor: '100%'
													},{
														xtype: 'textfield',
														fieldLabel: 'Alamat',
														id: 'TxtWindowPopup_ALAMAT_viLABPA',
														name: 'TxtWindowPopup_ALAMAT_viLABPA',
														emptyText: 'Alamat',
														labelSeparator: '',
														flex: 1,
														anchor: '100%'
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'No Tlp',
														labelSeparator: '',
														anchor: '100%',
														width: 250,
														items:
																[
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_No_Tlp_viLABPA',
																		name: 'TxtWindowPopup_No_Tlp_viLABPA',
																		emptyText: 'No Tlp',
																		flex: 1,
																		width: 195
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'tbspacer',
																		width: 17,
																		height: 23
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Tgl. Lahir',
																		style: {'text-align': 'right'},
																		id: '',
																		name: '',
																		flex: 1,
																		width: 58
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'datefield',
																		fieldLabel: 'Tanggal',
																		id: 'DateWindowPopup_TGL_LAHIR_viLABPA',
																		name: 'DateWindowPopup_TGL_LAHIR_viLABPA',
																		width: 198,
																		format: 'd/m/Y',
                                                                        listeners:{
                                                                            'specialkey': function (){
																				if (Ext.EventObject.getKey() === 13){
																					var tmptanggal = Ext.get('DateWindowPopup_TGL_LAHIR_viLABPA').getValue();
																					Ext.Ajax.request({
																						url: baseURL + "index.php/main/GetUmur_LAB",
																						params: {
																							TanggalLahir: tmptanggal
																						},
																						success: function (o){
																							var tmphasil = o.responseText;
																							var tmp = tmphasil.split(' ');
																							if (tmp.length == 6){
																								Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
																								getParamBalikanHitungUmurPopUp(tmptanggal);
																							}else if(tmp.length == 4){
																								if(tmp[1]== 'years' && tmp[3] == 'day'){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
																								}else if(tmp[1]== 'years' && tmp[3] == 'days'){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
																								}else if(tmp[1]== 'year' && tmp[3] == 'days'){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
																								}else{
																									Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
																								}
																								getParamBalikanHitungUmurPopUp(tmptanggal);
																							}else if(tmp.length == 2 ){
																								if (tmp[1] == 'year' ){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
																								}else if (tmp[1] == 'years' ){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
																								}else if (tmp[1] == 'mon'  ){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue('0');
																								}else if (tmp[1] == 'mons'  ){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue('0');
																								}else{
																									Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue('0');
																								}
																								getParamBalikanHitungUmurPopUp(tmptanggal);
																							}else if(tmp.length == 1){
																								Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue('0');
																								getParamBalikanHitungUmurPopUp(tmptanggal);
																							}else{
																								alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
																							}	
																						}
																					});
																				}
																			},
                                                                        }
																	}
																	//-------------- ## --------------				
																]
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Umur',
														labelSeparator: '',
														anchor: '100%',
														width: 50,
														items:
																[
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_TAHUN_viLABPA',
																		name: 'TxtWindowPopup_TAHUN_viLABPA',
																		emptyText: 'Thn',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Thn',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	},
																	//-------------- ## --------------
																	/* {
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_BULAN_viRAD',
																		name: 'TxtWindowPopup_BULAN_viRAD',
																		emptyText: 'Bln',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Bln',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_HARI_viRAD',
																		name: 'TxtWindowPopup_HARI_viRAD',
																		emptyText: 'Hari',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Hari',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	} */
																	//-------------- ## --------------			
																]
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Jenis Kelamin',
														labelSeparator: '',
														anchor: '100%',
														width: 250,
														items:
																[
																	mComboJKLABPA(),
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Golongan Darah',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 90
																	},
																	mComboGolDarahLABPA()
																			//-------------- ## --------------				
																]
													},
													//-------------- ## --------------						

													//-------------- ## --------------
												]
									},
									//-------------- ## --------------
									/*{
									 xtype: 'spacer',
									 width: 10,
									 height: 1
									 },*/
									//-------------- ## --------------
									
											//-------------- ## --------------
								]
								//-------------- #items# --------------
					}
			)
	return pnlFormDataWindowPopup_viLABPA;
}

function datainit_viLABPA(rowdata){
	var tmpjk;
	var tmp_data;
    //addNew_viDataPasien = false;
	if (rowdata.jk === "t" || rowdata.jk === 1){
        tmpjk = "1";
        tmp_data = "Laki- laki";
		Ext.get('cboJKLABPA').dom.value= 'Laki-laki';
    }else{
        tmpjk = "2";
        tmp_data = "Perempuan";
		Ext.get('cboJKLABPA').dom.value= 'Perempuan'
    }
    ID_JENIS_KELAMIN = rowdata.jk;
    Ext.getCmp('cboGolDarahLABPA').setValue(rowdata.goldarah);
    Ext.getCmp('TxtWindowPopup_ALAMAT_viLABPA').setValue(rowdata.alamat);
    Ext.getCmp('TxtWindowPopup_NAMA_viLABPA').setValue(rowdata.nama);
    Ext.getCmp('TxtWindowPopup_NO_RM_viLABPA').setValue(rowdata.medrec);
    Ext.getCmp('TxtWindowPopup_No_Tlp_viLABPA').setValue(rowdata.hp);
	Ext.get('DateWindowPopup_TGL_LAHIR_viLABPA').dom.value=ShowDate(rowdata.tgl_lahir);
	setUsiaPopUp(ShowDate(rowdata.tgl_lahir));
}
function mComboGolDarahLABPA()
        {
            var cboGolDarah = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboGolDarahLABPA',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                //allowBlank: false,
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Gol. Darah...',
                                fieldLabel: 'Gol. Darah ',
                                width: 50,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[0, '-'], [1, 'A+'], [2, 'B+'], [3, 'AB+'], [4, 'O+'], [5, 'A-'], [6, 'B-'], [7, 'AB-'], [8, 'O-']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetGolDarah,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetGolDarah = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                /* c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboWarga').focus();
                                                }, c); */
                                            }
                                        }
                            }
                    );
            return cboGolDarah;
        }
        ;

        function mComboJKLABPA()
        {
			
            var cboJK = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboJKLABPA',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Jenis Kelamin...',
                                fieldLabel: 'Jenis Kelamin ',
                                width: 100,
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[true, 'Laki - Laki'], [false, 'Perempuan']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetJK,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetJK = b.data.displayText;
//                                        getdatajeniskelamin(b.data.displayText)
                                                //alert(jenis_kelamin)
                                            },
                                            'render': function (c) {
                                                /* c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtTempatLahir').focus();
                                                }, c); */
                                            }
                                        }
                            }
                    );
            return cboJK;
        }
        ;
function setUsiaPopUp(Tanggal)
{
    Ext.Ajax.request
            ({
                url: baseURL + "index.php/main/GetUmur",
                params: {
                    TanggalLahir: Tanggal
                },
                success: function (o)
                {
                    var tmphasil = o.responseText;
                    var tmp = tmphasil.split(' ');

                    if (tmp.length === 6)
                    {
                        Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[4]);
                    } else if (tmp.length === 4) {
                        if (tmp[1] === 'years') {
                            if (tmp[3] === 'day') {
                                Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                            } else {
                                Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                            }
                        } else {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                        }
                    } else if (tmp.length === 2) {
                        if (tmp[1] === 'year') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'years') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mon') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mons') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[0]);
                        }
                    } else if (tmp.length === 1) {
                        Ext.getCmp('TxtWindowPopup_TAHUN_viLABPA').setValue('0');
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue('1');
                    } else {
                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                    }
                }
            });
}
function datasave_EditDataPasienLABPA(mBol){  
    
	if (ValidasiEntry_viDataPasienLABPA('Simpan Data', true) == 1){
		Ext.Ajax.request({
			url: baseURL + "index.php/main/functionKasirPenunjang/simpanEditDataPasien",
			params: dataparam_datapasviDataPasienLABPA(),
			failure: function (o){
				ShowPesanErrorPenJasLab_PA('Data tidak berhasil diupdate. Hubungi admin! ', 'Edit Data');
			},
			success: function (o){
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					ShowPesanInfoPenJasLab_PA('Data berhasil disimpan', 'Edit Data');
					validasiJenisTrLABPA();
					//DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
				} else if (cst.success === false && cst.pesan === 0){
					ShowPesanErrorPenJasLab_PA('Data tidak berhasil diupdate ', 'Edit Data');
				} else{
					ShowPesanErrorPenJasLab_PA('Data tidak berhasil diupdate. ', 'Edit Data');
				}
			}
		});
	} else{
		if (mBol === true){
			return false;
		}
	}
    
}
function ValidasiEntry_viDataPasienLABPA(modul, mBolHapus)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viLABPA').getValue() === 'Nama' ||
            (Ext.get('TxtWindowPopup_ALAMAT_viLABPA').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_NAMA_viLABPA').getValue() === '' ||
                     (Ext.get('TxtWindowPopup_ALAMAT_viLABPA').getValue() === ''))))
    {
        if (Ext.get('TxtWindowPopup_NAMA_viLABPA').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viLABPA').getValue() === ''))
        {
            ShowPesanWarningPenJasLab_PA('Nama belum terisi', modul);
            x = 0;
        }  
    }

    return x;
}

function dataparam_datapasviDataPasienLABPA(){
    var params_viDataPasienLABPA ={
                //-------------- # modelist Net. # --------------
		//-------------- # textfield # --------------
		NO_MEDREC: Ext.getCmp('TxtWindowPopup_NO_RM_viLABPA').getValue(),
		NAMA: Ext.getCmp('TxtWindowPopup_NAMA_viLABPA').getValue(),
		ALAMAT: Ext.getCmp('TxtWindowPopup_ALAMAT_viLABPA').getValue(),
		NO_TELP: Ext.getCmp('TxtWindowPopup_No_Tlp_viLABPA').getValue(),
		//NO_HP: Ext.get('TxtWindowPopup_NO_HP_viDataPasien').getValue(),

		//-------------- # datefield # --------------
		TGL_LAHIR: Ext.get('DateWindowPopup_TGL_LAHIR_viLABPA').getValue(),
		//-------------- # combobox # --------------
		ID_JENIS_KELAMIN: Ext.getCmp('cboJKLABPA').getValue(),
		ID_GOL_DARAH: Ext.getCmp('cboGolDarahLABPA').getValue(),
		TRUESQL: false
	}
    return params_viDataPasienLABPA
}
function getParamBalikanHitungUmurPopUp(tgl){
	Ext.getCmp('DateWindowPopup_TGL_LAHIR_viLABPA').setValue(tgl);
}