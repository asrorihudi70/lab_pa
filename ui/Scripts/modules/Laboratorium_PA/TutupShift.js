var CurrentKasirShift =
{
    data: Object,
    details: Array,
    row: 0
};

var tampungshiftsekarang_TutupShiftLab;
var tampungshiftnanti_TutupShiftLab;
var AddNewKasirShift = true;
var now = new Date();
var FormLookUp_TutupShiftLab;
var nowTglTransaksi = new Date();
//var FocusCtrlCMShift;

CurrentPage.page = getPanelShift_TutupShiftLab(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelShift_TutupShiftLab(mod_id) 
{
   
    var FormDepanShift_TutupShiftLab = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Shift Laboratorium',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [ShiftLookUp_TutupShiftLab()],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	

	
   return FormDepanShift_TutupShiftLab

};


function ShiftLookUp_TutupShiftLab(rowdata) 
{
    var lebar = 350;
    FormLookUp_TutupShiftLab = new Ext.Window
    (
        {
            id: 'gridShift',
            title: 'Tutup Shift',
            closeAction: 'destroy',
            width: lebar,
            height: 240,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
			bodyStyle: 'padding:5px 5px 5px 5px',
            modal: true,
            items: getFormEntry_TutupShiftLab(lebar),
            listeners:
            {
                activate: function()
                {
                    getShiftLab();
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
					
                }
            },
			fbar:[
				{
					xtype:'button',
					text:'Ok',
					width:70,
					style:{'margin-left':'0px','margin-top':'0px'},
					hideLabel:true,
					id: 'btnOkShift_TutupShiftLab',
					handler:function()
					{
						TutupShiftSave_TutupShiftLab(false);
						
					}
				},
				{
						xtype:'button',
						text:'Batal' ,
						width:70,
						hideLabel:true,
						id: 'btnCancelShift_TutupShiftLab',
						handler:function() 
						{
							FormLookUp_TutupShiftLab.close();
						}
				}
			]
        }
    );

    FormLookUp_TutupShiftLab.show();
    if (rowdata == undefined) 
	{
        getShiftLab();
    }
    else 
	{
        
    }

};

function getFormEntry_TutupShiftLab(lebar) 
{
    var pnlTutupShift_TutupShiftLab = new Ext.FormPanel
    (
        {
            id: 'PanelTRShift',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:300,
            anchor: '100%',
            width: lebar,
            border: false,
            items: 
			[
				getItemPanelInputShift_TutupShiftLab(lebar)
			],
			tbar:
            [
               
               
            ]
        }
    );
    var FormPanelDepanShift_TutupShiftLab = new Ext.Panel
	(
		{
		    id: 'FormPanelShiftDepan_TutupShiftLab',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTutupShift_TutupShiftLab	
				
			]

		}
	);

    return FormPanelDepanShift_TutupShiftLab
};
//---------------------------------------------------------------------------------------///

function getItemPanelInputShift_TutupShiftLab(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-45,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:120,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelDetailShift_TutupShiftLab(lebar)		
				]
			}
		]
	};
    return items;
};


function getItemPanelDetailShift_TutupShiftLab(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .70,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'numberfield',
					    fieldLabel:  'Shift Ke',
					    name: 'txtNilaiShift_TutupShiftLab',
					    id: 'txtNilaiShift_TutupShiftLab',
						readOnly:true,
					    anchor: '100%'
					},
					
					{
					    xtype: 'textfield',
					    fieldLabel: 'Shift Selanjutnya',
					    name: 'txtNilaiShiftSelanjutnya_TutupShiftLab',
					    id: 'txtNilaiShiftSelanjutnya_TutupShiftLab',
						readOnly:true,
					    anchor: '100%'
						
					},
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tgl Shift ',
					    id: 'dtpTanggalShift_TutupShiftLab',
					    name: 'dtpTanggalShift_TutupShiftLab',
					    format: 'd/M/Y',
						readOnly:true,
					    value: now,
					    anchor: '100%'
					
				
					}
					
				]
			},
			
		]
	}
    return items;
};


//---------------------------------------------------------------------------------------///
function getShiftLab() 
{
    AddNewKasirShift = true;
   // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request
   // ajax request untuk mengambil data current shift	
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/lab_pa/functionTutupShift/getCurrentShiftLab",
		params: {
	        //UserID: 'Admin',
	        command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			tampungshiftsekarang_TutupShiftLab=o.responseText
			Ext.get('txtNilaiShift_TutupShiftLab').dom.value =tampungshiftsekarang_TutupShiftLab ;
	    }
	
	});
	
	
	 // ajax request untuk mengambil data max shift	
	Ext.Ajax.request(
	{ 
	    url: baseURL + "index.php/lab_pa/functionTutupShift/getMaxkdbagian",
		params: {
	        command: '0',
		
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			tampungshiftnanti_TutupShiftLab= o.responseText;
			if (tampungshiftsekarang_TutupShiftLab <tampungshiftnanti_TutupShiftLab) {
				Ext.get('txtNilaiShiftSelanjutnya_TutupShiftLab').dom.value =parseFloat(tampungshiftsekarang_TutupShiftLab)+1;
			} else{
				Ext.get('txtNilaiShiftSelanjutnya_TutupShiftLab').dom.value =1;
			}

	    }
	});

};



//---------------------------------------------------------------------------------------///


function TutupShiftSave_TutupShiftLab(mBol)
{
    if (ValidasiEntryTutupShift(nmHeaderSimpanData,false) == 1 )
    {
            
		Ext.Ajax.request({
			url: baseURL + "index.php/lab_pa/functionTutupShift/tutupShift",
			params: getParamTutupShift(),
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfoShift_TutupShiftLab('Simpan Shift Berhasil','Simpan Shift');
					FormLookUp_TutupShiftLab.close();
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanErrorShift_TutupShiftLab('Simpan Shift Gagal','Simpan Shift');
				}
				else
				{
					ShowPesanErrorShift_TutupShiftLab('Simpan Shift Gagal','Simpan Shift');
				};
			}
		})
            
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };

};//END FUNCTION TutupShiftSave_TutupShiftLab
//---------------------------------------------------------------------------------------///
function getParamTutupShift()
{
    var params =
	{
	    shiftKe: Ext.getCmp('txtNilaiShift_TutupShiftLab').getValue(),
		shiftSelanjutnya :Ext.getCmp('txtNilaiShiftSelanjutnya_TutupShiftLab').getValue(),
		tanggal :Ext.getCmp('dtpTanggalShift_TutupShiftLab').getValue(),
		
	};
    return params
};


function ValidasiEntryTutupShift(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtNilaiShift_TutupShiftLab').getValue() == '' || (Ext.get('txtNilaiShiftSelanjutnya_TutupShiftLab').getValue() == ''))
	{
		if (Ext.get('txtNilaiShift_TutupShiftLab').getValue() == '' && mBolHapus === true)
		{
			x=0;
		}
		else if (Ext.get('txtNilaiShiftSelanjutnya_TutupShiftLab').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningShift_TutupShiftLab(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};

function ShowPesanWarningShift_TutupShiftLab(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorShift_TutupShiftLab(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoShift_TutupShiftLab(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};










