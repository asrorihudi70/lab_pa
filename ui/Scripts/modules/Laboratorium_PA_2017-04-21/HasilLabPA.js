var CurrentHasilLabAnatomi =
{
    data: Object,
    details: Array,
    row: 0
};


var now = new Date();

var dsTRListHasilLabAnatomi;
var dsDiagnosaLabAnatomi;
var AddNewHasilLabAnatomi = true;
var selectCountHasilLabAnatomi = 50;
var tigaharilalu_LabAnatomi = new Date().add(Date.DAY, -3);

var tmpkriteria_LabAnatomi;
var rowSelectedHasilLabAnatomi;
var cellSelecteddeskripsi_LabAnatomi;
var FormLookUpsdetailHasilLabAnatomi;

var tmpnama_unit_asal_labanatomi;
var tmpkd_dokter_asal_labanatomi;
var tmpjam_masuk_labanatomi;
var tmpnama_dokter_asal_labanatomi;
var tmpno_pa_labanatomi;
var tmptgl_hasil_labanatomi;
var grListPasienHasilLabAnatomi;
var tmpkd_pasien;
var tmpnama;
var tmptglAwal;
var tmptglAkhir;
var gridDTHasilLabAnatomi;

arraystorePenyakit	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_penyakit', 'penyakit', 'kd_pasien', 'urut', 'urut_masuk', 'tgl_masuk', 'kasus', 'stat_diag', 'note', 'detail'],
	data: []
});

CurrentPage.page = getPanelHasilLabAnatomi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


//membuat form
function getPanelHasilLabAnatomi(mod_id) 
{
    var Field = ['kd_pasien','nama','alamat','tgl_masuk','nama_unit','kd_kelas','kd_dokter','urut_masuk',
				'nama_dokter','tgl_lahir','jenis_kelamin','gol_darah','nama_unit_asal','kd_dokter_asal','jam_masuk','nama_dokter_asal','no_transaksi'];
    dsTRListHasilLabAnatomi = new WebApp.DataStore({ fields: Field });
    //refeshhasillabanatomi();
	dataGridListPasienLabPA()
    grListPasienHasilLabAnatomi = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            store: dsTRListHasilLabAnatomi,
            columnLines: false,
            autoScroll:true,
            border: false,
			sort :false,
			height:390,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedHasilLabAnatomi = dsTRListHasilLabAnatomi.getAt(row);
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedHasilLabAnatomi = dsTRListHasilLabAnatomi.getAt(ridx);
                    if (rowSelectedHasilLabAnatomi !== undefined)
                    {
                        HasilLookUp_LabAnatomi(rowSelectedHasilLabAnatomi.data);
                    }
                    else
                    {
                        HasilLookUp_LabAnatomi();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
			
                [
                    new Ext.grid.RowNumberer(),
					{
                        header: 'Tanggal Masuk',
                        dataIndex: 'tgl_masuk',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.tgl_masuk);

							}
                    },
					{
                        header: 'No Transaksi',
                        dataIndex: 'no_transaksi',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 50
                    },
                    {
                        header: 'No Medrec',
                        dataIndex: 'kd_pasien',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 50
                    },
                    {
                        header: 'Nama Pasien',
                        dataIndex: 'nama',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 130
                    },
					{
						header: 'Alamat',
                        width: 170,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'alamat'                        
                    },
                    {
                        header: 'Tanggal Lahir',
                        dataIndex: 'tgl_lahir',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 65,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_lahir);

						}
                    },
                    {
                        header: 'Dokter',
                        dataIndex: 'nama_dokter',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 150
                    },
                    {
                        header: 'Unit Asal',
                        dataIndex: 'nama_unit_asal',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					
                   
                ]
            ),
            viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditHasilLabAnatomi',
                        text: nmEditData,
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedHasilLabAnatomi != undefined)
                            {
								HasilLookUp_LabAnatomi(rowSelectedHasilLabAnatomi.data);
                            }
                            else
                            {
								ShowPesanWarningHasilLabAnatomi('Pilih data data tabel  ','Edit data');
                            }
                        }
                    }
                ]
            }
	);
	
	
	//form depan awal dan judul tab
    var PanelDepanHasilLabAnatomi = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Hasil Laboratorium Anatomi',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
			//height:200,
			autoHeight:true,
            items: [
                    getPanelPencarianHasilLabAnatomi(),
                    grListPasienHasilLabAnatomi
                   ],
            listeners:
            {
                'afterrender': function()
                {
				}
            }
        }
    );
	
   return PanelDepanHasilLabAnatomi;

};

//mengatur lookup edit data
function HasilLookUp_LabAnatomi(rowdata) 
{
    var lebar = 1000;
    FormLookUpsdetailHasilLabAnatomi = new Ext.Window
    (
        {
            id: 'gridHasilLab',
            title: 'Hasil Laboratorium Anatomi',
            closeAction: 'destroy',
            width: 850,
            height: 550,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryHasilLabAnatomi(lebar,rowdata),
			tbar:
			[
				'-',
				{
					text: 'Simpan',
					id: 'btnSimpanHasilLabAnatomi',
					tooltip: nmSimpan,
					iconCls: 'save',
					handler: function()
					{
						Datasave_HasilLabAnatomi(false);
					   
					}
				},  
				'-',
				{
					id:'btnCetakHasilLabAnatomi',
					text: 'Cetak Hasil',
					tooltip: 'Cetak Hasil',
					iconCls:'print',
					handler: function()
					{
					   DataPrint_HasilLabAnatomi();
					}
				},
				'-'
			],
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailHasilLabAnatomi.show();
    if (rowdata === undefined) {
		
	}
	else{
		DataInitHasilLabAnatomi(rowdata);
	}

};


//mengatur lookup toolbar
function getFormEntryHasilLabAnatomi(lebar,data) 
{
	var TabPanelEntryHasilLabAnatomi = new Ext.TabPanel   
    (
        {
			id:'TabPanelEntryHasilLabAnatomi',
			region: 'center',
			activeTab: 0,
			height:330,
			width:828,
			border:true,
			plain: true,
			defaults:
			{
				autoScroll: true
			},
			items: [
				panelDetailHasilPemeriksaanLabAnatomi(),
				panelDiagnosaHasilPemeriksaanLabAnatomi()
			]
        }
		
    );
	
    var pnlHasilLabAnatomi = new Ext.FormPanel
    (
        {
            id: 'PanelHasilLab',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:5px 5px 5px 5px',
            height:145,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [panelInfoDataPasienLabAnatomi(lebar)]
        }
    );

	var pnlDetailHasilLabAnatomi2 = new Ext.FormPanel
    (
        {
            id: 'pnlDetailHasilLabAnatomi2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:5px 5px 5px 5px',
            height:345,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
				TabPanelEntryHasilLabAnatomi
			]
        }
    );

    var PanelHasilLabAnatomi = new Ext.Panel
	(
		{
		    id: 'PanelHasilLabAnatomi',
		    region: 'center',
		    width: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[ 
				pnlHasilLabAnatomi,pnlDetailHasilLabAnatomi2
			]

		}
	);
	
    return PanelHasilLabAnatomi
};


//AWAL INPUT
function getPanelPencarianHasilLabAnatomi()
{
    var items =
    {
        layout:'column',
		bodyStyle: 'padding: 5px 0px 5px 10px',
        border:true,
        items:
        [
			//--------------------------------------------------
            {
                columnWidth:.99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 600,
                height: 120,
                anchor: '100% 100%',
                items:
                [
					{
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec  '
                    },
					{
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
					{
						x : 155,
                        y : 10,
                        xtype: 'textfield',
                        name: 'txtNoMedrec_LabAnatomi',
                        id: 'txtNoMedrec_LabAnatomi',
                        enableKeyEvents: true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrec_LabAnatomi').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
											var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrec_LabAnatomi').getValue())
											Ext.getCmp('txtNoMedrec_LabAnatomi').setValue(tmpgetNoMedrec);
											
											tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
											tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
											tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
											tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
											dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
                                        }
                                        else
                                            {
												tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
												tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
												tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
												tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
												dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
                                                    
                                            }
                                } else{
									tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
									tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
									tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
									tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
									dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
								}
								
                            }

                        }

                    },	
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
					{
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 40,
                        xtype: 'textfield',
                        name: 'TxtNamaHasilLabAnatomi',
                        id: 'TxtNamaHasilLabAnatomi',
                        width: 200,
                        enableKeyEvents: true,
                        listeners:
                        { 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
									tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
									tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
									tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
									dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
								} 						
							}
                        }
                    },
					{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Tanggal Kunjungan '
                    },
					{
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 70,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterHasilLabAnatomi',
                        format: 'd/M/Y',
                        value: tigaharilalu_LabAnatomi,
                        listeners:
                        { 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
									tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
									tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
									tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
									dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
								} 						
							}
                        }
                    },
					{
                        x: 270,
                        y: 70,
                        xtype: 'label',
                        text: 's/d '
                    },
					{
                        x: 305,
                        y: 70,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterHasilLab',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        listeners:
                        { 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
									tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
									tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
									tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
									dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
								} 						
							}
                        }
                    },
					{
                        x: 555,
                        y: 70,
                        xtype:'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        anchor: '25%',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function(sm, row, rec)
                        {
							dataGridListPasienLabPA();
                        }
                    },
					{
                        x: 153,
                        y: 97,
                        xtype: 'label',
                        text: '*)Tekan enter untuk mencari',
						style :{ fontSize:10}
                    },
                    
                ]
            },
        ]
    }
    return items;
};
//LOOKUP hasil laboratorium
function panelInfoDataPasienLabAnatomi(lebar){
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 828,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:true,
	    height:135,
	    items:
		[
			{
					columnWidth: .99,
					layout: 'absolute',
					bodyStyle: 'padding: 0px 0px 0px 0px',
					border: false,
					width: 100,
					height: 140,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'No. Medrec '
						},
						{
							x: 110,
							y: 10,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 10,
							xtype: 'textfield',
							name: 'TxtPopupMedrec_LabAnatomi',
							id: 'TxtPopupMedrec_LabAnatomi',
							width: 80,
							readOnly:true
						},
						{
							x: 210,
							y: 10,
							xtype: 'label',
							text: 'Tanggal '
						},
						{
							x: 250,
							y: 10,
							xtype: 'label',
							text: ' : '
						},
						{   //cobacoba
							x : 260,
							y : 10,
							xtype: 'datefield',
							name: 'dPopupTglCekPasien_LabAnatomi',
							id: 'dPopupTglCekPasien_LabAnatomi',
							width: 110,
							format: 'd/M/Y',
							value: now,
							readOnly:true
						},
						{
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Nama Pasien '
						},
						{
							x: 110,
							y: 40,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupNamaPasien_LabAnatomi',
							id: 'TxtPopupNamaPasien_LabAnatomi',
							width: 250,
							readOnly:true
						},
						{
							x: 380,
							y: 20,
							xtype: 'label',
							text: 'Tgl Lahir '
						},
						{   
							x : 380,
							y : 40,
							xtype: 'datefield',
							name: 'dPopupTglLahirPasien_LabAnatomi',
							id: 'dPopupTglLahirPasien_LabAnatomi',
							width: 100,
							format: 'd/M/Y',
							readOnly:true
						},
						{
							x: 490,
							y: 20,
							xtype: 'label',
							text: 'Thn '
						},
						{   
							x : 490,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupThnLahirPasien_LabAnatomi',
							id: 'TxtPopupThnLahirPasien_LabAnatomi',
							width: 30,
							readOnly:true
						},
						{
							x: 530,
							y: 20,
							xtype: 'label',
							text: 'Bln '
						},
						{   
							x : 530,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupBlnLahirPasien_LabAnatomi',
							id: 'TxtPopupBlnLahirPasien_LabAnatomi',
							width: 30,
							readOnly:true
						},
						{
							x: 570,
							y: 20,
							xtype: 'label',
							text: 'Hari '
						},
						{   
							x : 570,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupHariLahirPasien_LabAnatomi',
							id: 'TxtPopupHariLahirPasien_LabAnatomi',
							width: 30,
							readOnly:true
						},
						{
							x: 610,
							y: 20,
							xtype: 'label',
							text: 'Jenis Kelamin '
						},
						{   
							x : 610,
							y : 40,
							xtype: 'textfield',
							name: 'TxtJK_LabAnatomi',
							id: 'TxtJK_LabAnatomi',
							width: 70,
							readOnly:true,
							//hidden:false
						},
						{
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Alamat '
						},
						{
							x: 110,
							y: 70,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 70,
							xtype: 'textfield',
							name: 'TxtPopupAlamat_LabAnatomi',
							id: 'TxtPopupAlamat_LabAnatomi',
							width: 560,
							readOnly:true
						},
						{
							x: 10,
							y: 100,
							xtype: 'label',
							text: 'Dokter Lab'
						},
						{
							x: 110,
							y: 100,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtPopupNamaDokter_LabAnatomi',
							id: 'TxtPopupNamaDokter_LabAnatomi',
							width: 250,
							readOnly:true
						},
						//-------HIDDEN---------------------
						{   
							x : 480,
							y : 10,
							xtype: 'datefield',
							name: 'dPopupTglMasuk_LabAnatomi',
							id: 'dPopupTglMasuk_LabAnatomi',
							width: 150,
							readOnly:true,
							format: 'd/M/Y',
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtNoTransaksi_LabAnatomi',
							id: 'TxtNoTransaksi_LabAnatomi',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtKdUnit_LabAnatomi',
							id: 'TxtKdUnit_LabAnatomi',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 480,
							y : 10,
							xtype: 'datefield',
							name: 'dPopupJamMasuk',
							id: 'dPopupJamMasuk',
							width: 150,
							readOnly:true,
							format: 'd/M/Y',
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtUrutMasuk_LabAnatomi',
							id: 'TxtUrutMasuk_LabAnatomi',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtNmDokter_LabAnatomi',
							id: 'TxtNmDokter_LabAnatomi',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtPopPoli_LabAnatomi',
							id: 'TxtPopPoli_LabAnatomi',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtPopKddokter_LabAnatomi',
							id: 'TxtPopKddokter_LabAnatomi',
							width: 250,
							readOnly:true,
							hidden:true
						}
					]
				},
	
		]
	};
	return items;
}
function panelDetailHasilPemeriksaanLabAnatomi(){
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 828,
		title: 'Hasil Laboratorium',
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:false,
		height:320,
	    items:
		[
			{
				columnWidth: .99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width: 100,
				//height: 312,
				anchor: '100% 100%',
				items:
				[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Makroskopik '
					},
					{
						x: 110,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 10,
						xtype: 'textarea',
						name: 'TxtMakroskopik_LabAnatomi',
						id: 'TxtMakroskopik_LabAnatomi',
						width: 700,
						height: 85
					},
					
					{
						x: 10,
						y: 105,
						xtype: 'label',
						text: 'Mikroskopik '
					},
					{
						x: 110,
						y: 105,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 105,
						xtype: 'textarea',
						name: 'TxtMikroskopik_LabAnatomi',
						id: 'TxtMikroskopik_LabAnatomi',
						width: 700,
						height: 85
					},
					{
						x: 10,
						y: 205,
						xtype: 'label',
						text: 'Kesimpulan '
					},
					{
						x: 110,
						y: 205,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 205,
						xtype: 'textarea',
						name: 'TxtKesimpulan_LabAnatomi',
						id: 'TxtKesimpulan_LabAnatomi',
						width: 700,
						height: 85
					},
					
				]
			},
			
	
		]
	};
	return items;
}
function panelDiagnosaHasilPemeriksaanLabAnatomi(){
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 828,
		title: 'Entry Diagnosa',
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:false,
		height:100,
		tbar:[
			'-',
			{
				text: 'Tambah',
				id: 'btnAddGridDiagnosaLabAnatomi',
				iconCls: 'add',
				handler: function()
				{
					var records = new Array();
					records.push(new dsDiagnosaLabAnatomi.recordType());
					dsDiagnosaLabAnatomi.add(records);

				}
			},
			'-',
			{
				text: 'Simpan',
				id: 'btnSimpasDiagnosaLabAnatomi',
				tooltip: nmSimpan,
				iconCls: 'save',
				handler: function()
				{
					DatasaveDiagnosa_HasilLabAnatomi();
				   
				}
			},  
			'-',
			{
				id:'btnHapusDiagnosaLabAnatomi',
				text: 'Hapus',
				iconCls:'remove',
				handler: function()
				{
					var line = gridDTHasilLabAnatomi.getSelectionModel().selection.cell[0];
					var o = dsDiagnosaLabAnatomi.getRange()[line].data;
					if(dsDiagnosaLabAnatomi.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah dignosa ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDiagnosaLabAnatomi.getRange()[line].data.urut != undefined || dsDiagnosaLabAnatomi.getRange()[line].data.urut != ''){
									Ext.Ajax.request
									({
										url: baseURL + "index.php/lab_pa/functionLABPA/hapusDiagnosa",
										params:{
											kd_penyakit:o.kd_penyakit, 
											kd_pasien:Ext.getCmp('TxtPopupMedrec_LabAnatomi').getValue(),
											kd_unit:Ext.getCmp('TxtKdUnit_LabAnatomi').getValue(), 
											tgl_masuk:Ext.get('dPopupTglMasuk_LabAnatomi').dom.value,
											urut_masuk:Ext.getCmp('TxtUrutMasuk_LabAnatomi').getValue(), 
											urut:o.urut
										} ,
										failure: function(o)
										{
											loadMask.hide();
											ShowPesanErrorHasilLabAnatomi('Hubungi Admin', 'Error');
										},	
										success: function(o) 
										{
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) 
											{
												dsDiagnosaLabAnatomi.removeAt(line);
												gridDTHasilLabAnatomi.getView().refresh();
												
											}
											else 
											{
												loadMask.hide();
												ShowPesanErrorHasilLabAnatomi('Gagal melakukan penghapusan', 'Error');
											};
										}
									})
								}else{
									dsDiagnosaLabAnatomi.removeAt(line);
									gridDTHasilLabAnatomi.getView().refresh();
								}
							} 
							
						});
					} else{
						loadMask.hide();
						ShowPesanErrorHasilLabAnatomi('Data tidak bisa dihapus karena minimal dignosa 1','Error');
					}
				}
			},
			'-'
		],
	    items:
		[
			
			{
				columnWidth: 250,
				layout: 'form',
				labelWidth: 50,
				height:20,
				border: false,
				items:
				[/* 
					{xtype: 'tbspacer',height: 10, width:5},
					{   
						xtype: 'textfield',
						fieldLabel: '',
						name: 'TxtPopupMedrec_LabAnatomi',
						id: 'TxtPopupMedrec_LabAnatomi',
						width: 80,
						readOnly:true
					},
					{xtype: 'tbspacer',height: 10, width:5}, */
					GetDTGridDiagnosaHasilLabAnatomi()
				]
			},
		]
	};
	return items;
}

//------------GRID DALAM LOOK UP HASIL LAB--------------------------------------------------------------------------
function GetDTGridDiagnosaHasilLabAnatomi() 
{
	var fm = Ext.form;
    var fldDetailHasilLab = ['KLASIFIKASI', 'DESKRIPSI', 'KD_LAB', 'KD_TEST', 'ITEM_TEST', 'SATUAN', 'NORMAL', 'NORMAL_W',  'NORMAL_A', 'NORMAL_B', 'COUNTABLE', 'MAX_M', 'MIN_M', 'MAX_F', 'MIN_F', 'MAX_A', 'MIN_A', 'MAX_B', 'MIN_B', 'KD_METODE', 'HASIL', 'KET','KD_UNIT_ASAL','NAMA_UNIT_ASAL','URUT','METODE','JUDUL_ITEM','KET_HASIL'];
	
    dsDiagnosaLabAnatomi = new WebApp.DataStore({ fields: fldDetailHasilLab })
    gridDTHasilLabAnatomi = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Diagnosa',
            stripeRows: true,
            store: dsDiagnosaLabAnatomi,
            border: true,
            columnLines: true,
            frame: false,
            autoScroll:true,
			height:275,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi_LabAnatomi = dsDiagnosaLabAnatomi.getAt(row);
                            CurrentHasilLabAnatomi.row = row;
                            CurrentHasilLabAnatomi.data = cellSelecteddeskripsi_LabAnatomi;
                            //FocusCtrlCMRWJ='txtAset';
                        }
                    }
                }
            ),
			cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(), 
					{
						header: 'No ICD',
						dataIndex: 'kd_penyakit',
						width:80,
						menuDisabled:true,
						hidden:false,
						editor		:new Nci.form.Combobox.autoCompleteId({
							store	: arraystorePenyakit,
							select	: function(a,b,c){
								var line	= gridDTHasilLabAnatomi.getSelectionModel().selection.cell[0];
								
								dsDiagnosaLabAnatomi.getRange()[line].data.penyakit=b.data.penyakit;
								dsDiagnosaLabAnatomi.getRange()[line].data.kd_penyakit=b.data.kd_penyakit;
								gridDTHasilLabAnatomi.getView().refresh();
								
							},
							insert	: function(o){
								return {
									kd_penyakit	: o.kd_penyakit,
									penyakit	: o.penyakit,
									text		: '<table style="font-size: 11px;"><tr><td width="50">' + o.kd_penyakit + '</td><td width="200">' + o.penyakit + '</td></tr></table>',
								}
							},
							/* param:function(){
									return {
										kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue()
									}
							}, */
							url			: baseURL + "index.php/lab_pa/functionLABPA/getKodePenyakit",
							valueField	: 'kd_penyakit',
							displayField: 'text',
							listWidth	: 250
						})
					},
					{
						header: 'Penyakit',
						dataIndex: 'penyakit',
						hideable:false,
						menuDisabled: true,
						width: 200,
						editor		:new Nci.form.Combobox.autoCompleteId({
							store	: arraystorePenyakit,
							select	: function(a,b,c){
								var line	= gridDTHasilLabAnatomi.getSelectionModel().selection.cell[0];
								
								dsDiagnosaLabAnatomi.getRange()[line].data.penyakit=b.data.penyakit;
								dsDiagnosaLabAnatomi.getRange()[line].data.kd_penyakit=b.data.kd_penyakit;
								gridDTHasilLabAnatomi.getView().refresh();
								
							},
							insert	: function(o){
								return {
									kd_penyakit	: o.kd_penyakit,
									penyakit	: o.penyakit,
									text		: '<table style="font-size: 11px;"><tr><td width="50">' + o.kd_penyakit + '</td><td width="200">' + o.penyakit + '</td></tr></table>',
								}
							},
							/* param:function(){
									return {
										kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue()
									}
							}, */
							url			: baseURL + "index.php/lab_pa/functionLABPA/getPenyakit",
							valueField	: 'penyakit',
							displayField: 'text',
							listWidth	: 250
						})
					},
					{
						header: 'Diagnosa',
						width: 100,
						menuDisabled: true,
						dataIndex: 'stat_diag',
						editor: new Ext.form.ComboBox({
							typeAhead: true,
							triggerAction: 'all',
							lazyRender: true,
							mode: 'local',
							selectOnFocus: true,
							forceSelection: true,
							width: 50,
							anchor: '95%',
							value: 1,
							store: new Ext.data.ArrayStore({
								id: 0,
								fields: ['Id', 'displayText'],
								data: [[1, 'Diagnosa Awal'], [2, 'Diagnosa Utama'], [3, 'Komplikasi'], [4, 'Diagnosa Sekunder']]
							}),
							valueField: 'displayText',
							displayField: 'displayText',
							value		: '',
							listeners: {}
						})
					},
					{
						header: 'Kasus',
						width: 80,
						menuDisabled: true,
						dataIndex: 'kasus',
						editor: new Ext.form.ComboBox({
							typeAhead: true,
							triggerAction: 'all',
							lazyRender: true,
							mode: 'local',
							selectOnFocus: true,
							forceSelection: true,
							width: 50,
							anchor: '95%',
							value: 1,
							store: new Ext.data.ArrayStore({
								id: 0,
								fields: ['Id', 'displayText'],
								data: [[1, 'Baru'], [2, 'Lama']]
							}),
							valueField: 'displayText',
							displayField: 'displayText',
							value		: '',
							listeners: {}
						})
					}, 
					{
						header:'urut',
						dataIndex: 'urut',
						width:250,
						hidden:true
						
					}

				]
			),
			viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTHasilLabAnatomi;
};


//------------------MEMASUKAN DATA YG DIPILIH KEDALAM TEXTBOX YG ADA DALAM LOOKUP--------------------------------------------------
function DataInitHasilLabAnatomi(rowdata)
{
    AddNewHasilLabAnatomi = false;
	
	tmpnama_unit_asal_labanatomi=rowdata.nama_unit_asal;
	tmpkd_dokter_asal_labanatomi=rowdata.kd_dokter_asal;
	tmpjam_masuk_labanatomi=rowdata.jam_masuk;
	tmpnama_dokter_asal_labanatomi=rowdata.nama_dokter_asal;
	tmpno_pa_labanatomi=rowdata.tag;
	tmptgl_hasil_labanatomi=rowdata.tgl_hasil;
	//alert(tmpjam_masuk_labanatomi);
	Ext.get('TxtPopupMedrec_LabAnatomi').dom.value= rowdata.kd_pasien;
	Ext.get('TxtPopupNamaPasien_LabAnatomi').dom.value = rowdata.nama;
	Ext.get('TxtPopupAlamat_LabAnatomi').dom.value = rowdata.alamat;
	Ext.get('TxtPopupNamaDokter_LabAnatomi').dom.value = rowdata.nama_dokter;
	Ext.get('dPopupTglLahirPasien_LabAnatomi').dom.value = ShowDate(rowdata.tgl_lahir);
	Ext.get('dPopupTglMasuk_LabAnatomi').dom.value = ShowDate(rowdata.tgl_masuk);
	
	if(rowdata.jenis_kelamin=='t'){
		Ext.get('TxtJK_LabAnatomi').dom.value = 'Laki-laki';
	}else{
		Ext.get('TxtJK_LabAnatomi').dom.value = 'Perempuan'
	}
	Ext.get('TxtPopPoli_LabAnatomi').dom.value = rowdata.nama_unit;
	Ext.get('TxtNmDokter_LabAnatomi').dom.value = rowdata.nama_dokter;
	Ext.get('TxtUrutMasuk_LabAnatomi').dom.value = rowdata.urut_masuk;
	Ext.get('TxtPopKddokter_LabAnatomi').dom.value = rowdata.kd_dokter;
	Ext.get('TxtNoTransaksi_LabAnatomi').dom.value = rowdata.no_transaksi;
	setUsia_LabAnatomi(ShowDate(rowdata.tgl_lahir));
	Ext.getCmp('TxtMakroskopik_LabAnatomi').setValue(rowdata.makroskopik);
	Ext.getCmp('TxtMikroskopik_LabAnatomi').setValue(rowdata.mikroskopik);
	Ext.getCmp('TxtKesimpulan_LabAnatomi').setValue(rowdata.kesimpulan);
	Ext.getCmp('TxtKdUnit_LabAnatomi').setValue(rowdata.kd_unit);
	
	dataGridDiagnosaLabPA(rowdata.kd_pasien, rowdata.kd_unit, rowdata.tgl_masuk, rowdata.urut_masuk);
	
	
};

//-----------------------------------------MENGHITUNG USIA DALAM LOOKUP------------------------------------------------------------
function setUsia_LabAnatomi(Tanggal)
{

	Ext.Ajax.request
		( 
		{
		   url: baseURL + "index.php/main/GetUmur",
		   params: {
					TanggalLahir: ShowDateReal(Tanggal)
		   },
		   
		   success: function(o)
		   {
				//alert('test');  
			
				var tmphasil = o.responseText;
				 //alert(tmphasil);
				var tmp = tmphasil.split(' ');
				//alert(tmp.length);
				if (tmp.length == 6)
				{
					Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue(tmp[0]);
					Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue(tmp[2]);
					Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue(tmp[4]);
				}
				else if(tmp.length == 4)
				{
					if(tmp[1]== 'years' && tmp[3] == 'day')
					{
						Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue(tmp[2]);  
					}else{
					Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue(tmp[0]);
					Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue(tmp[2]);
						  }
				}
				else if(tmp.length == 2 )
				{
					
					if (tmp[1] == 'year' )
					{
						Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue('0');
					}
					else if (tmp[1] == 'years' )
					{
						Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue('0');
					}
					else if (tmp[1] == 'mon'  )
					{
						Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue('0');
						Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue(tmp[0]);
						Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue('0');
					}
					else if (tmp[1] == 'mons'  )
					{
						Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue('0');
						Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue(tmp[0]);
						Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue('0');
					}
					else{
							Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue('0');
							Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue('0');
							Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue(tmp[0]);
						}
				}
				
				else if(tmp.length == 1)
				{
					Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue('0');
					Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue('1');
				}else
				{
					alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
				}
		   }
		   
		   
		}
		);


	
}




//------------------MENAMPILKAN DETAIL HASIL LAB---------------------------------------------------

function dataGridListPasienLabPA(kd_pasien,nama,tglAwal,tglAkhir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/lab_pa/functionLABPA/getGridListPasien",
			params: {
				kd_pasien:kd_pasien,
				nama:nama,
				tglAwal:tglAwal,
				tglAkhir:tglAkhir
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorHasilLabAnatomi('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsTRListHasilLabAnatomi.removeAll();
					var recs=[],
					recType=dsTRListHasilLabAnatomi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsTRListHasilLabAnatomi.add(recs);
					grListPasienHasilLabAnatomi.getView().refresh();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorHasilLabAnatomi('Gagal membaca list data pasien', 'Error');
				};
			}
		}
		
	)
	
}

function dataGridDiagnosaLabPA(kd_pasien, kd_unit, tgl_masuk, urut_masuk){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/lab_pa/functionLABPA/getGridDiagnosa",
			params: {
				kd_pasien:kd_pasien,
				kd_unit:kd_unit, 
				tgl_masuk:tgl_masuk, 
				urut_masuk:urut_masuk
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorHasilLabAnatomi('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDiagnosaLabAnatomi.removeAll();
					var recs=[],
					recType=dsDiagnosaLabAnatomi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsDiagnosaLabAnatomi.add(recs);
					gridDTHasilLabAnatomi.getView().refresh();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorHasilLabAnatomi('Gagal membaca list data pasien', 'Error');
				};
			}
		}
		
	)
	
}

function DataPrint_HasilLabAnatomi() 
{
	var params={
		no_transaksi:Ext.getCmp('TxtNoTransaksi_LabAnatomi').getValue(),
		no_pa:tmpno_pa_labanatomi,
		kd_pasien:Ext.getCmp('TxtPopupMedrec_LabAnatomi').getValue(),
		nama:Ext.getCmp('TxtPopupNamaPasien_LabAnatomi').getValue(),
		tahun:Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').getValue(),
		bulan:Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').getValue(),
		hari:Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').getValue(),
		jk:Ext.getCmp('TxtJK_LabAnatomi').getValue(),
		unitasal:tmpnama_unit_asal_labanatomi,
		kd_unit:Ext.getCmp('TxtKdUnit_LabAnatomi').getValue(),
		urut_masuk:Ext.getCmp('TxtUrutMasuk_LabAnatomi').getValue(),
		kd_dokter_lab:Ext.getCmp('TxtPopKddokter_LabAnatomi').getValue(),
		dokter_lab:Ext.getCmp('TxtNmDokter_LabAnatomi').getValue(),
		dokter_pengirim:tmpnama_dokter_asal_labanatomi,
		kd_dokter_pengirim:tmpkd_dokter_asal_labanatomi,
		tgl_masuk:Ext.getCmp('dPopupTglMasuk_LabAnatomi').getValue(),
		tgl_hasil:tmptgl_hasil_labanatomi,
		makroskopik:Ext.getCmp('TxtMakroskopik_LabAnatomi').getValue(),
		mikroskopik:Ext.getCmp('TxtMikroskopik_LabAnatomi').getValue(),
		kesimpulan:Ext.getCmp('TxtKesimpulan_LabAnatomi').getValue(),
	} ;
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	form.setAttribute("action", baseURL + "index.php/lab_pa/lap_laboratorium_pa/cetakHasilLabPA");
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();

};




function Datasave_HasilLabAnatomi(mBol) 
{	
	if (ValidasiEntryHasilLab(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/lab_pa/functionLABPA/ubahHasil",
				params: getParamHasilLabAnatomi(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanWarningHasilLabAnatomi('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoHasilLabAnatomi('Data berhasil disimpan','Information');
						dataGridListPasienLabPA();
					}
					else 
					{
						ShowPesanWarningHasilLabAnatomi('Data gagal disimpan', 'Error');
					};
				}
			}
		)
		
	}
	
};

function DatasaveDiagnosa_HasilLabAnatomi(mBol) 
{	
	if (ValidasiEntryHasilLab(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/lab_pa/functionLABPA/savediagnosa",
				params: getParamDiagnosaHasilLabAnatomi(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanWarningHasilLabAnatomi('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoHasilLabAnatomi('Diagnosa berhasil disimpan','Information');
						dataGridListPasienLabPA();
					}
					else 
					{
						ShowPesanWarningHasilLabAnatomi('Diagnosa gagal disimpan', 'Error');
					};
				}
			}
		)
		
	}
	
};



function getdatahasillabcetakLabAnatomi()
{
	var tmpumur='';
	if(Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').getValue() !== ''){
		if(Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').getValue() ==='0' || Ext.get('TxtPopupThnLahirPasien_LabAnatomi').getValue() ===0){
			if(Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').getValue() ==='0' || Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').getValue() ===0){
				tmpumur=Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').getValue() + ' Hari';
			} else{
				tmpumur=Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').getValue() + ' Bulan';
			}
		} else{
			tmpumur=Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').getValue() + ' Tahun';
		}
	}
     var params = {
        Tgl: Ext.get('dPopupTglMasuk_LabAnatomi').getValue(),
        KdPasien: Ext.get('TxtPopupMedrec_LabAnatomi').getValue(),
        Nama: Ext.get('TxtPopupNamaPasien_LabAnatomi').getValue(),        
        JenisKelamin: Ext.get('TxtJK_LabAnatomi').getValue(),
		Ttl:Ext.get('dPopupTglLahirPasien_LabAnatomi').getValue(),
		Umur:tmpumur,
        Alamat: Ext.get('TxtPopupAlamat_LabAnatomi').getValue(),
        Poli: Ext.get('TxtPopPoli_LabAnatomi').getValue(),
        urutmasuk: Ext.get('TxtUrutMasuk_LabAnatomi').getValue(),
        Dokter:Ext.get('TxtNmDokter_LabAnatomi').getValue(),
		NamaUnitAsal:tmpnama_unit_asal_labanatomi,
		KdDokterAsal:tmpkd_dokter_asal_labanatomi,
		JamMasuk:tmpjam_masuk_labanatomi,
		NamaDokterAsal:tmpnama_dokter_asal_labanatomi
    };
    return params;
}


function getParamHasilLabAnatomi() 
{
    var params =
	{
		KdPasien: Ext.getCmp('TxtPopupMedrec_LabAnatomi').getValue(),
		TglMasuk: Ext.get('dPopupTglMasuk_LabAnatomi').dom.value,
		UrutMasuk:Ext.getCmp('TxtUrutMasuk_LabAnatomi').getValue(),
		Makroskopik:Ext.getCmp('TxtMakroskopik_LabAnatomi').getValue(),
		Mikroskopik:Ext.getCmp('TxtMikroskopik_LabAnatomi').getValue(),
		Kesimpulan:Ext.getCmp('TxtKesimpulan_LabAnatomi').getValue()
	};
    return params
};

function getParamDiagnosaHasilLabAnatomi(){
	var params =
	{
		KdPasien: Ext.getCmp('TxtPopupMedrec_LabAnatomi').getValue(),
		TglMasuk: Ext.get('dPopupTglMasuk_LabAnatomi').dom.value,
		UrutMasuk:Ext.getCmp('TxtUrutMasuk_LabAnatomi').getValue(),		
	};
	
	params['jumlah']=dsDiagnosaLabAnatomi.getCount();
	for(var i = 0 ; i < dsDiagnosaLabAnatomi.getCount();i++)
	{
		params['kd_penyakit-'+i]=dsDiagnosaLabAnatomi.data.items[i].data.kd_penyakit;
		params['stat_diag-'+i]=dsDiagnosaLabAnatomi.data.items[i].data.stat_diag
		params['kasus-'+i]=dsDiagnosaLabAnatomi.data.items[i].data.kasus
	}
	
    return params
}

function ValidasiEntryHasilLab(modul,mBolHapus)
{
	var x = 1;dsDiagnosaLabAnatomi
	
	if((Ext.get('TxtPopupMedrec_LabAnatomi').getValue() == '') || (Ext.get('dPopupTglCekPasien_LabAnatomi').getValue() == ''))
	{
		if (Ext.get('TxtPopupMedrec_LabAnatomi').getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningHasilLabAnatomi('No.Medrec tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.get('dPopupTglCekPasien_LabAnatomi').getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningHasilLabAnatomi('Tanggal pengisian hasil tidak boleh kosong', 'Warning');
			x = 0;
		}
	};
	return x;
};








function ShowPesanWarningHasilLabAnatomi(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorHasilLabAnatomi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoHasilLabAnatomi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

