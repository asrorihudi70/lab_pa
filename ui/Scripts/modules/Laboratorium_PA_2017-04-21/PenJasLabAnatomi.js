var PenJasLabAnatomi={};
PenJasLabAnatomi.form={};
PenJasLabAnatomi.func={};
PenJasLabAnatomi.vars={};
PenJasLabAnatomi.func.parent=PenJasLabAnatomi;
PenJasLabAnatomi.form.ArrayStore={};
PenJasLabAnatomi.form.ComboBox={};
PenJasLabAnatomi.form.DataStore={};
PenJasLabAnatomi.form.Record={};
PenJasLabAnatomi.form.Form={};
PenJasLabAnatomi.form.Grid={};
PenJasLabAnatomi.form.Panel={};
PenJasLabAnatomi.form.TextField={};
PenJasLabAnatomi.form.Button={};
var cellCurrentLabAnatomi='';
var dspasienorder_mng_LabAnatomi;
var cbopasienorder_mng_LabAnatomi;
var checkColumn_penata_labAnatomi;
var dsGetdetail_order_LabAnatomi;
var gridGetdetail_order_LabAnatomi;
var var_tampung_labAnatomi_order=false;
var dsTrDokter_LabAnatomi	= new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA','KD_JOB','KD_PRODUK'] });
var dataTrDokter_LabAnatomi = [];


PenJasLabAnatomi.form.ArrayStore.produk=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'uraian','kd_tarif','kd_produk','tgl_transaksi','tgl_berlaku','harga','qty'
				],
		data: []
	});

PenJasLabAnatomi.form.ArrayStore.kodepasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});

PenJasLabAnatomi.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});

PenJasLabAnatomi.form.ArrayStore.notransaksi=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});

var CurrentPenJasLabAnatomi =
{
    data: Object,
    details: Array,
    row: 0
};



var now = new Date();
var cellSelecteddeskripsi_LabAnatomi;
var nowTglTransaksi_LabAnatomi = new Date();
var nowTglTransaksiGrid_LabAnatomi = new Date();
var tglGridBawah_LabAnatomi = nowTglTransaksiGrid_LabAnatomi.format("d/M/Y");
var tigaharilalu_LabAnatomi = new Date().add(Date.DAY, -3);

var selectSetGDarahLabAnatomi;
var selectSetJKLabAnatomi;
var selectSetKelPasienLabAnatomi;
var selectSetPerseoranganLabAnatomi;
var selectSetPerusahaanLabAnatomi;
var selectSetAsuransiLabAnatomi;

var dsTRDetailPenJasLabList_LabAnatomi;
var TmpNotransaksi_LabAnatomi='';
var KdKasirAsal_LabAnatomi='';
var TglTransaksiAsal_LabAnatomi='';
var No_Kamar_LabAnatomi='';
var Kd_Spesial_LabAnatomi=0;

var gridDTItemTest_LabAnatomi;
var tampungshiftsekarang_LabAnatomi;

var vkode_customer_LabAnatomi;

CurrentPage.page = getPanelPenJasLabAnatomi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
//viewGridOrderAll_RASEPRWI();


//membuat form
function getPanelPenJasLabAnatomi(mod_id)
{

/* var i = setInterval(function(){
total_pasien_order_mng();
load_data_pasienorder();
viewGridOrderAll_RASEPRWI();
}, 150000); */

    var FormDepanPenJasLabAnatomi = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Penata Jasa Laboratorium Patologi Anatomi',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
						getItemPanelInputPenJasLabAnatomi(),
						getGridEntryPenJasLabAnatomi()
                   ],
			tbar:
            [
				'-',
				{
                        text: 'Add new',
                        id: 'btnAddPenJasLabAnatomi',
                        iconCls: 'add',
                        handler: function()
						{
							addNewData_LabAnatomi();

						}
				},
				'-',
				{
                        text: 'Save',
                        id: 'btnSimpanPenJasLabAnatomi',
                        tooltip: nmSimpan,
                        iconCls: 'save',
						disabled:true,
                        handler: function()
						{
							Datasave_PenJasLabAnatomi(false);
						}
				},
				/* {
                        text: 'Save',
                        id: 'btnSimpanPenJasLabAnatomi_kiriman',
                        tooltip: nmSimpan,
                        iconCls: 'save',
						hidden : true,
                        handler: function()
						{
							Dataupdate_order_PenJasLabAnatomi();
						}
				}, */
				'-',
				{
                        text: 'Find',
                        id: 'btnCariPenJasLabAnatomi',
                        iconCls: 'find',
						hidden:true,
                        handler: function()
						{

						}
				},
				

			],
            listeners:
            {
                'afterrender': function()
                {
					Ext.Ajax.request(
					{
						url: baseURL + "index.php/lab_pa/functionLABPA/getCurrentShiftLab",
						params: {
							command: '0',
							// parameter untuk url yang dituju (fungsi didalam controller)
						},
						failure: function(o)
						{
							var cst = Ext.decode(o.responseText);
						},
						success: function(o) {
							tampungshiftsekarang_LabAnatomi=o.responseText;
						}
					});
				},
				//'beforeclose': function(){clearInterval(i);}


            }
        }
    );

   return FormDepanPenJasLabAnatomi;

};



function getGridEntryPenJasLabAnatomi(lebar,data)
{

 var GDtabDetailItemPeriksaPenJasLabAnatomi = new Ext.TabPanel
    (
        {
			id:'GDtabDetailItemPeriksaPenJasLabAnatomi',
			region: 'center',
			activeTab: 0,
			height:380,
			border:false,
			plain: true,
			defaults:{autoScroll: true},
			items:
			[
				gridFunctionDetailItemPemeriksaan()//Panel tab detail pemerikasaan lab anatomi
				//-------------- ## --------------
			],tbar:
			[
				{
					text: 'Tambah Item Pemeriksaan',
					id: 'btnAddItemPemeriksaan_LabAnatomi',
					tooltip: nmLookup,
					iconCls: 'Edit_Tr',
					disabled:true,
					handler: function()
					{
						var records = new Array();
						records.push(new dsTRDetailPenJasLabList_LabAnatomi.recordType());
						dsTRDetailPenJasLabList_LabAnatomi.add(records);
					}
				},
				{
					id:'btnDeleteItemPemeriksaan_LabAnatomi',
					text: 'Hapus Baris',
					tooltip: 'Hapus Baris',
					disabled:true,
					iconCls: 'RemoveRow',
					handler: function()
					{
						if (dsTRDetailPenJasLabList_LabAnatomi.getCount() > 0 ) {
							if (cellSelecteddeskripsi_LabAnatomi != undefined)
							{
								Ext.Msg.confirm('Warning', 'Apakah item pemeriksaan ini akan dihapus?', function(button){
									if (button == 'yes'){
										if(CurrentPenJasLabAnatomi != undefined) {
											var line = gridDTItemTest_LabAnatomi.getSelectionModel().selection.cell[0];
											dsTRDetailPenJasLabList_LabAnatomi.removeAt(line);
											gridDTItemTest_LabAnatomi.getView().refresh();
										}
									}
								})
							} else {
								ShowPesanWarningPenJasLabAnatomi('Pilih record ','Hapus data');
							}
						}
					}
				},{
					xtype: 'checkbox',
					id: 'checkboxCitoLabAnatomi',
					text: 'Cito',
					hideLabel:false,
					checked: false,
					handler: function()
					  {
						if (this.getValue()===true)
						{
						
							for (var i = 0; i < dsTRDetailPenJasLabList_LabAnatomi.getCount(); i++) 
							{
								dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.cito='Ya';
							}
							
							gridDTItemTest_LabAnatomi.getView().refresh();
						}
						else
						{
							for (var i = 0; i < dsTRDetailPenJasLabList_LabAnatomi.getCount(); i++) 
							{
								dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.cito='Tidak';
							}
							gridDTItemTest_LabAnatomi.getView().refresh();
						}
					  }
                },{ 
					xtype : 'tbtext', 
					text  : 'CITO ',
					cls   : 'left-label',
					style : {
						'font-weight':'bold',
						  'font-style': 'italic',
							'color' : 'red'
						},			
					width: 90
				  }

			],
        }

    );

    return GDtabDetailItemPeriksaPenJasLabAnatomi
};


function gridFunctionDetailItemPemeriksaan(){
	var gridPanelDetailItemPemeriksaan = new Ext.Panel({
		title: 'Item Test',
		id:'gridFunctionDetailItemPemeriksaan',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:160,
        anchor: '100%',
        width: 815,
        border: false,
        items: [GetGridDetailItemPemeriksaanPenJasLabAnatomi()]//,Getdetail_order()]
    });
	return gridPanelDetailItemPemeriksaan;
}

function GetGridDetailItemPemeriksaanPenJasLabAnatomi()
{
    var fldDetail = [];

    dsTRDetailPenJasLabList_LabAnatomi = new WebApp.DataStore({ fields: fldDetail })
	ViewGridBawahLookupPenjasLabAnatomi() ;
    gridDTItemTest_LabAnatomi = new Ext.grid.EditorGridPanel
    (
        {
            title: '',
            stripeRows: true,
            store: dsTRDetailPenJasLabList_LabAnatomi,
            border: true,
			height:150,
            columnLines: true,
            frame: true,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi_LabAnatomi = dsTRDetailPenJasLabList_LabAnatomi.getAt(row);
                            CurrentPenJasLabAnatomi.row = row;
                            CurrentPenJasLabAnatomi.data = cellSelecteddeskripsi_LabAnatomi;
                        }
                    }
                }
            ), 
			plugins: [checkColumn_penata_labAnatomi],
            cm: PenJasLabAnatomiColumModel(),
			listeners: {
				rowclick: function( $this, rowIndex, e )
				{
					cellCurrentLabAnatomi = rowIndex;
				},
				celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
					var NoTrans =  Ext.getCmp('nci-gen1').getValue();
					var TglTrans =  Ext.get('dtpKunjungan_LabAnatomi').dom.value;
					var kdPrdk = dsTRDetailPenJasLabList_LabAnatomi.data.items[cellCurrentLabAnatomi].data.kd_produk;
					var kdTrf = dsTRDetailPenJasLabList_LabAnatomi.data.items[cellCurrentLabAnatomi].data.kd_tarif;
					var tglBerlaku = dsTRDetailPenJasLabList_LabAnatomi.data.items[cellCurrentLabAnatomi].data.tgl_berlaku;
					var trf = dsTRDetailPenJasLabList_LabAnatomi.data.items[cellCurrentLabAnatomi].data.harga;
				}
			}
        }
    );

    return gridDTItemTest_LabAnatomi;
};


function PenJasLabAnatomiColumModel()
{

	checkColumn_penata_labAnatomi = new Ext.grid.CheckColumn({
		header: 'Cito',
		dataIndex: 'cito',
		id: 'checkid_LabAnatomi',
		width: 55,
		renderer: function(value)
		{
			console.log(value);
		}
	});
    return new Ext.grid.ColumnModel
    (
        [ 
            new Ext.grid.RowNumberer(),
			{
				header: 'Cito',
                dataIndex: 'cito',
                width:65,
				menuDisabled:true,
				renderer:function (v, metaData, record)
					{
						if ( record.data.cito=='0')
						{
							record.data.cito='Tidak'
						}else if (record.data.cito=='1')
						{
							metaData.style  ='background:#FF0000;  "font-weight":"bold";';
							record.data.cito='Ya'
						}else if (record.data.cito=='Ya')
						{
							metaData.style  ='background:#FF0000;  "font-weight":"bold";';
						}
						
						return record.data.cito; 
					},
				editor:new Ext.form.ComboBox
				({
					id: 'cboGridCito_LabPA',
					typeAhead: true,
					triggerAction: 'all',
					lazyRender: true,
					mode: 'local',
					selectOnFocus: true,
					forceSelection: true,
					emptyText: 'Silahkan Pilih...',
					width: 50,
					anchor: '95%',
					value: 1,
					store: new Ext.data.ArrayStore({
						id: 0,
						fields: ['Id', 'displayText'],
						data: [[1, 'Ya'], [2, 'Tidak']]
					}),
					valueField: 'displayText',
					displayField: 'displayText',
					value		: '',
									   
				})
			},
			{
                header: 'kd_tarif',
                dataIndex: 'kd_tarif',
                width:250,
				menuDisabled:true,
				hidden :true

            },
            {
                header: 'Kode Produk',
                dataIndex: 'kd_produk',
                width:100,
                menuDisabled:true,
                hidden:true
            },
			{	
				dataIndex: 'deskripsi',
				header: 'Nama Pemeriksaan',
				sortable: true,
				menuDisabled:true,
				width: 320,
				editor:new Nci.form.Combobox.autoComplete({
					store	: PenJasLabAnatomi.form.ArrayStore.produk,
					select	: function(a,b,c){
						var line = gridDTItemTest_LabAnatomi.getSelectionModel().selection.cell[0];
						dsTRDetailPenJasLabList_LabAnatomi.data.items[line].data.deskripsi=b.data.deskripsi;
						dsTRDetailPenJasLabList_LabAnatomi.data.items[line].data.uraian=b.data.uraian;
						dsTRDetailPenJasLabList_LabAnatomi.data.items[line].data.kd_tarif=b.data.kd_tarif;
						dsTRDetailPenJasLabList_LabAnatomi.data.items[line].data.kd_produk=b.data.kd_produk;
						dsTRDetailPenJasLabList_LabAnatomi.data.items[line].data.tgl_transaksi=b.data.tgl_transaksi;
						dsTRDetailPenJasLabList_LabAnatomi.data.items[line].data.tgl_berlaku=b.data.tgl_berlaku;
						dsTRDetailPenJasLabList_LabAnatomi.data.items[line].data.harga=b.data.harga;
						dsTRDetailPenJasLabList_LabAnatomi.data.items[line].data.qty=b.data.qty;
						dsTRDetailPenJasLabList_LabAnatomi.data.items[line].data.jumlah=b.data.jumlah;

						gridDTItemTest_LabAnatomi.getView().refresh();
						
					},
					insert	: function(o){
						return {
							uraian        	: o.uraian,
							kd_tarif 		: o.kd_tarif,
							kd_produk		: o.kd_produk,
							tgl_transaksi	: o.tgl_transaksi,
							tgl_berlaku		: o.tgl_berlaku,
							harga			: o.harga,
							qty				: o.qty,
							deskripsi		: o.deskripsi,
							jumlah			: o.jumlah,
							text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_produk+'</td><td width="150">'+o.deskripsi+'</td></tr></table>'
						}
					},
					param	: function(){
					var kd_cus_gettarif_LabAnatomi;
					if(Ext.get('cboKelPasienLabAnatomi').getValue()=='Perseorangan'){
						kd_cus_gettarif_LabAnatomi=Ext.getCmp('cboPerseoranganLabAnatomi').getValue();
					}else if(Ext.get('cboKelPasienLabAnatomi').getValue()=='Perusahaan'){
						kd_cus_gettarif_LabAnatomi=Ext.getCmp('cboPerusahaanLabAnatomi').getValue();
					}else {
						kd_cus_gettarif_LabAnatomi=Ext.getCmp('cboAsuransiLabAnatomi').getValue();
					}
					var params={};
						params['kd_unit']="";
						params['kd_customer']=kd_cus_gettarif_LabAnatomi;
						return params;
					},
					url		: baseURL + "index.php/lab_pa/functionLABPA/getProduk",
					valueField: 'deskripsi',
					displayField: 'text',
					listWidth: 210
				})
			},
            {
				header: 'Tanggal Transaksi',
				dataIndex: 'tgl_transaksi',
				width: 130,
				menuDisabled:true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_transaksi == undefined){
						record.data.tgl_transaksi=tglGridBawah_LabAnatomi;
						return record.data.tgl_transaksi;
					} else{
						if(record.data.tgl_transaksi.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_transaksi);
						} else{
							var tgl=record.data.tgl_transaksi.split("/");

							if(tgl[2].length == 4 && isNaN(tgl[1])){
								return record.data.tgl_transaksi;
							} else{
								return ShowDate(record.data.tgl_transaksi);
							}
						}

					}
				}
            },
			{
				header: 'Tanggal Berlaku',
				dataIndex: 'tgl_berlaku',
				width: 130,
				menuDisabled:true,
				hidden: true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_berlaku == undefined || record.data.tgl_berlaku == null){
						record.data.tgl_berlaku=tglGridBawah_LabAnatomi;
						return record.data.tgl_berlaku;
					} else{
						if(record.data.tgl_berlaku.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_berlaku);
						} else{
							var tglb=record.data.tgl_berlaku.split("-");

							if(tglb[2].length == 4 && isNaN(tglb[1])){
								return record.data.tgl_berlaku;
							} else{
								return ShowDate(record.data.tgl_berlaku);
							}
						}

					}
				}
            },
            {
                header: 'Harga',
                align: 'right',
                hidden: false,
                menuDisabled:true,
                dataIndex: 'harga',
                width:100,
				renderer: function(v, params, record)
				{
					return formatCurrency(record.data.harga);
				}
            },
            {	id:'qty_lab',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'qty',
                editor: new Ext.form.NumberField (
					{
					
					}
                ),
            },
			 {
                header: 'Dokter',
                width:91,
				menuDisabled:true,
                dataIndex: 'jumlah',
				hidden: true,

            },

        ]
    )
};


//tampil grid bawah penjas lab
function ViewGridBawahLookupPenjasLabAnatomi(no_transaksi)
{
     var strKriteriaRWJ='';
    strKriteriaRWJ = "\"no_transaksi\" = ~" + no_transaksi + "~" + " And kd_kasir ='03'";

    dsTRDetailPenJasLabList_LabAnatomi.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailGridBawahPenJasLab',
			    param: strKriteriaRWJ
			}
		}
	);
    return dsTRDetailPenJasLabList_LabAnatomi;
}

function ViewGridBawahPenjasLabAnatomi(no_transaksi,data)
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/lab_pa/functionLABPA/getItemPemeriksaan",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				//ShowPesanErrorPenJasLabAnatomi('Hubungi Admin', 'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					dsTRDetailPenJasLabList_LabAnatomi.removeAll();
					var recs=[],
						recType=dsTRDetailPenJasLabList_LabAnatomi.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){

						recs.push(new recType(cst.ListDataObj[i]));

					}
					dsTRDetailPenJasLabList_LabAnatomi.add(recs);

					gridDTItemTest_LabAnatomi.getView().refresh();

					if(dsTRDetailPenJasLabList_LabAnatomi.getCount() > 0 ){
						TmpNotransaksi_LabAnatomi=data.no_transaksi;
						KdKasirAsal_LabAnatomi=data.kd_kasir;
						TglTransaksiAsal_LabAnatomi=data.tgl_transaksi;
						Kd_Spesial_LabAnatomi=data.kd_spesial;
						No_Kamar_LabAnatomi=data.no_kamar;
						getDokterPengirimLabAnatomi(data.no_transaksi,data.kd_kasir);
						Ext.getCmp('txtNamaUnitLabAnatomi').setValue(data.nama_unit);
						Ext.getCmp('txtKdUnitLabAnatomi').setValue(data.kd_unit);
						Ext.getCmp('txtKdDokter').setValue(data.kd_dokter);
						Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').setValue(data.kd_dokter);
						Ext.getCmp('txtAlamat_LabAnatomi').setValue(data.alamat);
						Ext.getCmp('dtpTtl_LabAnatomi').setValue(ShowDate(data.tgl_lahir));
						Ext.getCmp('txtKdUrutMasuk_LabAnatomi').setValue(data.urut_masuk);
												
						Ext.getCmp('cboJKPenjasLabAnatomi').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKPenjasLabAnatomi').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKPenjasLabAnatomi').setValue('Perempuan');
						}
						Ext.getCmp('cboJKPenjasLabAnatomi').disable();

						Ext.getCmp('cboGDRPenJasLabAnatomi').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('B-');
						}
						Ext.getCmp('cboGDRPenJasLabAnatomi').disable();

						Ext.getCmp('txtKdCustomerLamaHide_LabAnatomi').setValue(data.kd_customer);

						//tampung kd customer untuk transaksi selain kunjungan langsung
						vkode_customer_LabAnatomi = data.kd_customer;

						if(data.kelpasien == 'Perseorangan'){
							Ext.getCmp('cboKelPasienLabAnatomi').setValue(data.kelpasien);
							Ext.getCmp('cboPerseoranganLabAnatomi').setValue(data.kd_customer);
							Ext.getCmp('cboPerseoranganLabAnatomi').show();
						} else if(data.kelpasien == 'Perusahaan'){
							Ext.getCmp('cboKelPasienLabAnatomi').setValue(data.kelpasien);
							Ext.getCmp('cboPerusahaanLabAnatomi').setValue(data.kd_customer);
							Ext.getCmp('cboPerusahaanLabAnatomi').show();
						} else{
							Ext.getCmp('cboKelPasienLabAnatomi').setValue(data.kelpasien);
							Ext.getCmp('cboAsuransiLabAnatomi').setValue(data.kd_customer);
							Ext.getCmp('cboAsuransiLabAnatomi').show();
						}
						Ext.getCmp('txtDokterPengirim_LabAnatomi').disable();
						Ext.getCmp('txtNamaUnitLabAnatomi').disable();
						Ext.getCmp('txtAlamat_LabAnatomi').disable();
						Ext.getCmp('cboJKPenjasLabAnatomi').disable();
						Ext.getCmp('cboGDRPenJasLabAnatomi').disable();
						Ext.getCmp('dtpTtl_LabAnatomi').disable();
						Ext.getCmp('dtpKunjungan_LabAnatomi').disable();
						Ext.getCmp('btnAddItemPemeriksaan_LabAnatomi').disable();
						Ext.getCmp('btnDeleteItemPemeriksaan_LabAnatomi').disable();
						
						if (var_tampung_labAnatomi_order===true)
						{
							Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').enable();
							Ext.getCmp('btnSimpanPenJasLabAnatomi').hide();
							gridDTItemTest_LabAnatomi.disable();
							Ext.getCmp('cboKelPasienLabAnatomi').enable();
							Ext.getCmp('cboAsuransiLabAnatomi').enable();
							Ext.getCmp('cboPerseoranganLabAnatomi').enable();
							Ext.getCmp('cboPerusahaanLabAnatomi').enable();
							//Ext.getCmp('btnSimpanPenJasLabAnatomi_kiriman').show();
						
						}else{
							Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').disable();
							Ext.getCmp('btnSimpanPenJasLabAnatomi').show();
							Ext.getCmp('btnSimpanPenJasLabAnatomi').disable();
							//Ext.getCmp('btnSimpanPenJasLabAnatomi_kiriman').hide();
							
							gridDTItemTest_LabAnatomi.enable();
							Ext.getCmp('cboKelPasienLabAnatomi').disable();
							Ext.getCmp('cboAsuransiLabAnatomi').disable();
							Ext.getCmp('cboPerseoranganLabAnatomi').disable();
							Ext.getCmp('cboPerusahaanLabAnatomi').disable();
							var_tampung_labAnatomi_order=false;	
						}
						
					} else {
						TmpNotransaksi_LabAnatomi=data.no_transaksi;
						KdKasirAsal_LabAnatomi=data.kd_kasir;
						TglTransaksiAsal_LabAnatomi=data.tgl_transaksi;
						Kd_Spesial_LabAnatomi=data.kd_spesial;
						No_Kamar_LabAnatomi=data.no_kamar;
						Ext.getCmp('btnSimpanPenJasLabAnatomi').show();
						//Ext.getCmp('btnSimpanPenJasLabAnatomi_kiriman').hide();
						Ext.getCmp('txtNamaUnitLabAnatomi').setValue(data.nama_unit_asli);
						Ext.getCmp('txtKdUnitLabAnatomi').setValue(data.kd_unit);
						Ext.getCmp('txtDokterPengirim_LabAnatomi').setValue(data.dokter);
						Ext.getCmp('txtAlamat_LabAnatomi').setValue(data.alamat);
						Ext.getCmp('dtpTtl_LabAnatomi').setValue(ShowDate(data.tgl_lahir));

						Ext.getCmp('cboJKPenjasLabAnatomi').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKPenjasLabAnatomi').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKPenjasLabAnatomi').setValue('Perempuan');
						}
						Ext.getCmp('cboJKPenjasLabAnatomi').disable();

						Ext.getCmp('cboGDRPenJasLabAnatomi').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('B-');
						}
						Ext.getCmp('cboGDRPenJasLabAnatomi').disable();
						Ext.getCmp('txtKdCustomerLamaHide_LabAnatomi').setValue(data.kd_customer);
						vkode_customer_LabAnatomi = data.kd_customer;
						Ext.getCmp('cboKelPasienLabAnatomi').enable();
						if(data.kelpasien == 'Perseorangan'){
							Ext.getCmp('cboKelPasienLabAnatomi').setValue(data.kelpasien);
							Ext.getCmp('cboPerseoranganLabAnatomi').setValue(data.kd_customer);
							Ext.getCmp('cboPerseoranganLabAnatomi').show();
						} else if(data.kelpasien == 'Perusahaan'){
							Ext.getCmp('cboKelPasienLabAnatomi').setValue(data.kelpasien);
							Ext.getCmp('cboPerusahaanLabAnatomi').setValue(data.kd_customer);
							Ext.getCmp('cboPerusahaanLabAnatomi').show();
						} else{
							Ext.getCmp('cboKelPasienLabAnatomi').setValue(data.kelpasien);
							Ext.getCmp('cboAsuransiLabAnatomi').setValue(data.kd_customer);
							Ext.getCmp('cboAsuransiLabAnatomi').show();
						}

						Ext.getCmp('txtDokterPengirim_LabAnatomi').disable();
						Ext.getCmp('txtNamaUnitLabAnatomi').disable();
						Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').enable();
						Ext.getCmp('txtAlamat_LabAnatomi').disable();
						Ext.getCmp('cboJKPenjasLabAnatomi').disable();
						Ext.getCmp('cboGDRPenJasLabAnatomi').disable();
						Ext.getCmp('cboKelPasienLabAnatomi').enable();
						Ext.getCmp('dtpTtl_LabAnatomi').disable();
						Ext.getCmp('dtpKunjungan_LabAnatomi').disable();
						Ext.getCmp('btnAddItemPemeriksaan_LabAnatomi').enable();
						Ext.getCmp('btnDeleteItemPemeriksaan_LabAnatomi').disable();
						Ext.getCmp('btnSimpanPenJasLabAnatomi').enable();
						Ext.getCmp('cboKelPasienLabAnatomi').disable();
						Ext.getCmp('cboAsuransiLabAnatomi').disable();
						Ext.getCmp('cboPerseoranganLabAnatomi').disable();
						Ext.getCmp('cboPerusahaanLabAnatomi').disable();
						gridDTItemTest_LabAnatomi.enable();
						var_tampung_labAnatomi_order=false;
					
					
					}
				}
			}
		}

	)
};

//---------------------------------------------------------------------------------------///

//---------------------------------------------------------------------------------------///
function getParamDetailSaveLabAnatomi()
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/lab_pa/functionLABPA/getCurrentShiftLab",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			tampungshiftsekarang_LabAnatomi=o.responseText;
		}
	});
	var KdCust='';
	var TmpCustoLama='';
	var pasienBaru;

	if(Ext.get('cboKelPasienLabAnatomi').getValue()=='Perseorangan'){
		KdCust=Ext.getCmp('cboPerseoranganLabAnatomi').getValue();
	}else if(Ext.get('cboKelPasienLabAnatomi').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('cboPerusahaanLabAnatomi').getValue();
	}else {
		KdCust=Ext.getCmp('cboAsuransiLabAnatomi').getValue();
	}

	if(PenJasLabAnatomi.form.ComboBox.kdpasien.getValue() == ''){
		pasienBaru=1;
	} else{
		pasienBaru=0;
	}

    var params =
	{
		KdTransaksi: PenJasLabAnatomi.form.ComboBox.notransaksi.getValue(),
		KdPasien:PenJasLabAnatomi.form.ComboBox.kdpasien.getValue(),
		NmPasien:PenJasLabAnatomi.form.ComboBox.pasien.getValue(),
		Ttl:Ext.getCmp('dtpTtl_LabAnatomi').getValue(),
		Alamat:Ext.getCmp('txtAlamat_LabAnatomi').getValue(),
		JK:Ext.getCmp('cboJKPenjasLabAnatomi').getValue(),
		GolDarah:Ext.getCmp('cboGDRPenJasLabAnatomi').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitLabAnatomi').getValue(),
		KdDokter:Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').getValue(),
		Tgl: Ext.getCmp('dtpKunjungan_LabAnatomi').getValue(),
		TglTransaksiAsal:TglTransaksiAsal_LabAnatomi,
		urutmasuk:Ext.getCmp('txtKdUrutMasuk_LabAnatomi').getValue(),
		KdCusto:KdCust,
		TmpCustoLama:KdCust, //Ext.getCmp('txtKdCustomerLamaHide_LabAnatomi').getValue(),//jika customer dengan transaksi lama
		NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiLab').getValue(),
		NoAskes:Ext.getCmp('txtNoAskesLabAnatomi').getValue(),
		NoSJP:Ext.getCmp('txtNoSJPLabAnatomi').getValue(),
		Shift: tampungshiftsekarang_LabAnatomi,
		List:getArrPenJasLab(),
		JmlList:GetListCountDetailTransaksi(),
		unit:42,
		TmpNotransaksi:TmpNotransaksi_LabAnatomi,
		KdKasirAsal:KdKasirAsal_LabAnatomi,
		KdSpesial:Kd_Spesial_LabAnatomi,
		Kamar:No_Kamar_LabAnatomi,
		pasienBaru:pasienBaru,
		listTrDokter	: Ext.encode(dataTrDokter_LabAnatomi),

	};
    return params
};

function GetListCountDetailTransaksi()
{

	var x=0;
	for(var i = 0 ; i < dsTRDetailPenJasLabList_LabAnatomi.getCount();i++)
	{
		if (dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.KD_PRODUK != '' || dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;

};


function getArrPenJasLab()
{
	var x='';
	var arr=[];
	for(var i = 0 ; i < dsTRDetailPenJasLabList_LabAnatomi.getCount();i++)
	{

		if (dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.kd_produk != '' && dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.deskripsi != '')
		{
			var o={};
			var y='';
			var z='@@##$$@@';
			o['URUT']= dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.urut;
			o['KD_PRODUK']= dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.kd_produk;
			o['QTY']= dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.tgl_berlaku;
			o['HARGA']= dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.harga;
			o['KD_TARIF']= dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.kd_tarif;
			o['cito']= dsTRDetailPenJasLabList_LabAnatomi.data.items[i].data.cito;
			arr.push(o);

		};
	}

	return Ext.encode(arr);
};

//panel dalam lookup detail pasien
function getItemPanelInputPenJasLabAnatomi(lebar)
{
	//pengaturan panel data pasien
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:5px 5px 5px 5px',
	    border:false,
	    height:225,
	    items:
		[
					{ //awal panel biodata pasien
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: true,
						width: 100,
						height: 125,
						anchor: '100% 100%',
						items:
						[
                            //bagian pinggir kiri
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Transaksi  '
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							PenJasLabAnatomi.form.ComboBox.notransaksi= new Nci.form.Combobox.autoComplete({
								store	: PenJasLabAnatomi.form.ArrayStore.notransaksi,
								select	: function(a,b,c){
									ViewGridBawahPenjasLabAnatomi(b.data.no_transaksi,b.data);
									PenJasLabAnatomi.form.ComboBox.kdpasien.disable();
									PenJasLabAnatomi.form.ComboBox.kdpasien.setValue(b.data.kd_pasien);
									PenJasLabAnatomi.form.ComboBox.pasien.disable();
									PenJasLabAnatomi.form.ComboBox.pasien.setValue(b.data.nama);

									Ext.getCmp('btnAddItemPemeriksaan_LabAnatomi').disable();
									Ext.getCmp('btnDeleteItemPemeriksaan_LabAnatomi').disable();
									Ext.getCmp('btnSimpanPenJasLabAnatomi').disable();
									//Ext.getCmp('btnSimpanPenJasLabAnatomi_kiriman').hide();
									PenJasLabAnatomi.vars.kd_tarif=b.data.kd_tarif;

									Ext.Ajax.request(
									{
										url: baseURL + "index.php/lab_pa/functionLABPA/getCurrentShiftLab",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},
										success: function(o) {
											tampungshiftsekarang_LabAnatomi=o.responseText;
										}
									});

								},
								width	: 100,
								insert	: function(o){
									return {
										nama        		:o.nama,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										no_transaksi		:o.no_transaksi,
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjungan_LabAnatomi').getValue(),
										a:0
									}
								},
								url		: baseURL + "index.php/lab_pa/functionLABPA/getPasien",
								valueField: 'no_transaksi',
								displayField: 'text',
								listWidth: 480,
								x : 130,
								y : 10,
								emptyText: 'No Transaksi'
							}),
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'No. Medrec  '
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							/* {
								x: 130,
								y: 40,
								xtype: 'textfield',
								fieldLabel: 'No. Medrec',
								name: 'txtNoMedrecL',
								id: 'txtNoMedrecL',
								width: 120,
								emptyText:'Otomatis',
								listeners:
								{

								}
							}, */
							PenJasLabAnatomi.form.ComboBox.kdpasien= new Nci.form.Combobox.autoComplete({
								store	: PenJasLabAnatomi.form.ArrayStore.kodepasien,
								select	: function(a,b,c){
									PenJasLabAnatomi.form.ComboBox.notransaksi.setValue('');
									ViewGridBawahPenjasLabAnatomi(b.data.no_transaksi,b.data);
									PenJasLabAnatomi.form.ComboBox.pasien.disable();
									PenJasLabAnatomi.form.ComboBox.pasien.setValue(b.data.nama);
									PenJasLabAnatomi.form.ComboBox.notransaksi.disable();
									PenJasLabAnatomi.form.ComboBox.notransaksi.setValue(b.data.no_transaksi);

									PenJasLabAnatomi.vars.kd_tarif=b.data.kd_tarif;

									Ext.Ajax.request(
									{
										url: baseURL + "index.php/lab_pa/functionLABPA/getCurrentShiftLab",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},
										success: function(o) {
											tampungshiftsekarang_LabAnatomi=o.responseText;
										}
									});

								},
								width	: 120,
								insert	: function(o){
									return {
										no_transaksi		:o.no_transaksi,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										nama        		:o.nama,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										kd_pasien	 		:o.kd_pasien
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjungan_LabAnatomi').getValue(),
										a:1
									}
								},
								url		: baseURL + "index.php/lab_pa/functionLABPA/getPasien",
								valueField: 'kd_pasien',
								displayField: 'text',
								listWidth: 480,
								x: 130,
								y: 40,
								emptyText: 'Kode Pasien'
							}),
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Unit  '
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 70,
								xtype: 'textfield',
								name: 'txtNamaUnitLabAnatomi',
								id: 'txtNamaUnitLabAnatomi',
								readOnly:true,
								width: 120
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Dokter Pengirim'
							},
							{
								x: 120,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 100,
								xtype: 'textfield',
								fieldLabel: '',
								readOnly:true,
								name: 'txtDokterPengirim_LabAnatomi',
								id: 'txtDokterPengirim_LabAnatomi',
								width: 200
							},
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Dokter Lab'
							},
							{
								x: 120,
								y: 130,
								xtype: 'label',
								text: ':'
							},
							mComboDOKTER_LabAnatomi(),
							{
								x: 10,
								y: 160,
								xtype: 'label',
								text: 'Kelompok Pasien '
							},
							{
								x: 120,
								y: 160,
								xtype: 'label',
								text: ':'
							},
							mCboKelompokpasienAnatomi(),
							mComboPerseorangan_LabAnatomi(),
							mComboPerusahaan_LabAnatomi(),
							mComboAsuransi_LabAnatomi(),
							//bagian tengah
							{
								x: 280,
								y: 10,
								xtype: 'label',
								text: 'Tanggal Kunjung '
							},
							{
								x: 390,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 400,
								y: 10,
								xtype: 'datefield',
								fieldLabel: 'Tanggal kunjung/tanggal hari ini',
								id: 'dtpKunjungan_LabAnatomi',
								name: 'dtpKunjungan_LabAnatomi',
								format: 'd/M/Y',
								readOnly : false,
								value: now,
								width: 110
							},
							{
								x: 280,
								y: 40,
								xtype: 'label',
								text: 'Nama Pasien '
							},
							{
								x: 390,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							/* {
								x: 400,
								y: 40,
								xtype: 'textfield',
								fieldLabel: '',
								name: 'txtNamaPasienL',
								id: 'txtNamaPasienL',
								width: 200
							}, */
							PenJasLabAnatomi.form.ComboBox.pasien= new Nci.form.Combobox.autoComplete({
								store	: PenJasLabAnatomi.form.ArrayStore.pasien,
								select	: function(a,b,c){
									PenJasLabAnatomi.form.ComboBox.notransaksi.setValue('');
									ViewGridBawahPenjasLabAnatomi(b.data.no_transaksi,b.data);
									PenJasLabAnatomi.form.ComboBox.kdpasien.disable();
									PenJasLabAnatomi.form.ComboBox.kdpasien.setValue(b.data.kd_pasien);
									PenJasLabAnatomi.form.ComboBox.notransaksi.disable();
									PenJasLabAnatomi.form.ComboBox.notransaksi.setValue(b.data.no_transaksi);

									PenJasLabAnatomi.vars.kd_tarif=b.data.kd_tarif;

									Ext.Ajax.request(
									{
										url: baseURL + "index.php/lab_pa/functionLABPA/getCurrentShiftLab",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},
										success: function(o) {
											tampungshiftsekarang_LabAnatomi=o.responseText;
										}
									});

								},
								width	: 200,
								insert	: function(o){
									return {
										no_transaksi		:o.no_transaksi,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										nama        		:o.nama,
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjungan_LabAnatomi').getValue(),
										a:2
									}
								},
								url		: baseURL + "index.php/lab_pa/functionLABPA/getPasien",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 480,
								x: 400,
								y: 40,
								emptyText: 'Nama'
							}),
							{
								x: 280,
								y: 70,
								xtype: 'label',
								text: 'Alamat '
							},
							{
								x: 390,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 400,
								y: 70,
								xtype: 'textfield',
								fieldLabel: '',
								name: 'txtAlamat_LabAnatomi',
								id: 'txtAlamat_LabAnatomi',
								width: 395
							},
							{
								x: 400,
								y: 100,
								xtype: 'label',
								text: 'Jenis Kelamin '
							},
							{
								x: 485,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							mComboJK_LabAnatomi(),
							{
								x: 610,
								y: 100,
								xtype: 'label',
								text: 'Gol. Darah '
							},
							{
								x: 680,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							mComboGDarah_LabAnatomi(),
							{
								x: 610,
								y: 40,
								xtype: 'label',
								text: 'Tanggal lahir '
							},
							{
								x: 700,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 710,
								y: 40,
								xtype: 'datefield',
								fieldLabel: 'Tanggal ',
								id: 'dtpTtl_LabAnatomi',
								name: 'dtpTtl_LabAnatomi',
								format: 'd/M/Y',
								value: now,
								width: 100
							},

							{
								x: 475,
								y: 160,
								xtype: 'textfield',
								fieldLabel: 'Nama Peserta',
								maxLength: 200,
								name: 'txtNamaPesertaAsuransiLab',
								id: 'txtNamaPesertaAsuransiLab',
								emptyText:'Nama Peserta Asuransi',
								width: 150
							},
							{
								x: 280,
								y: 190,
								xtype: 'textfield',
								fieldLabel: 'No. Askes',
								maxLength: 200,
								name: 'txtNoAskesLabAnatomi',
								id: 'txtNoAskesLabAnatomi',
								emptyText:'No Askes',
								width: 150
							},
							{
								x: 475,
								y: 190,
								xtype: 'textfield',
								fieldLabel: 'No. SJP',
								maxLength: 200,
								name: 'txtNoSJPLabAnatomi',
								id: 'txtNoSJPLabAnatomi',
								emptyText:'No SJP',
								width: 150
							},


							//---------------------hidden----------------------------------
							{
								x: 130,
								y: 160,
								xtype: 'textfield',
								fieldLabel:'Nama customer lama',
								name: 'txtCustomerLamaHide',
								id: 'txtCustomerLamaHide',
								readOnly:true,
								hidden:true,
								width: 280
							},
							{
								x: 130,
								y: 160,
								xtype: 'textfield',
								fieldLabel:'Kode customer  ',
								name: 'txtKdCustomerLamaHide_LabAnatomi',
								id: 'txtKdCustomerLamaHide_LabAnatomi',
								readOnly:true,
								hidden:true,
								width: 280
							},

							{
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtKdDokter',
								id: 'txtKdDokter',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtKdDokterPengirim',
								id: 'txtKdDokterPengirim',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								fieldLabel:'Unit  ',
								name: 'txtKdUnitLabAnatomi',
								id: 'txtKdUnitLabAnatomi',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								name: 'txtKdUrutMasuk_LabAnatomi',
								id: 'txtKdUrutMasuk_LabAnatomi',
								readOnly:true,
								hidden:true,
								anchor: '100%'
							}

							//-------------------------------------------------------------

						]
					},	//akhir panel biodata pasien

		]
	};
    return items;
};


function Datasave_PenJasLabAnatomi(mBol)
{
	if (ValidasiEntryPenJasLabAnatomi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/lab_pa/functionLABPA/savedetaillab",
				params: getParamDetailSaveLabAnatomi(),
				failure: function(o)
				{	var_tampung_labAnatomi_order=false;
					loadMask.hide();
					ShowPesanErrorPenJasLabAnatomi('Error! Gagal menyimpan data ini. Hubungi Admin', 'Error');
					ViewGridBawahPenjasLabAnatomi(PenJasLabAnatomi.form.ComboBox.notransaksi.getValue());
				},
				success: function(o)
				{  	var_tampung_labAnatomi_order=false;
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)
					{
						loadMask.hide();
						ShowPesanInfoPenJasLabAnatomi("Berhasil menyimpan data ini","Information");
						PenJasLabAnatomi.form.ComboBox.notransaksi.setValue(cst.notrans);
						PenJasLabAnatomi.form.ComboBox.kdpasien.setValue(cst.kdPasien);
						if(cst.kdPasien.substring(0, 1) ==='L'){
							Ext.getCmp('txtNamaUnitLabAnatomi').setValue('Laboratorium');
							Ext.getCmp('txtDokterPengirim_LabAnatomi').setValue('-');
						}
						Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').disable();
						Ext.getCmp('btnAddItemPemeriksaan_LabAnatomi').disable();
						Ext.getCmp('btnDeleteItemPemeriksaan_LabAnatomi').disable();
						Ext.getCmp('btnSimpanPenJasLabAnatomi').disable();
						//Ext.getCmp('btnSimpanPenJasLabAnatomi_kiriman').hide();
						ViewGridBawahPenjasLabAnatomi(PenJasLabAnatomi.form.ComboBox.notransaksi.getValue());
						Ext.getCmp('cboKelPasienLabAnatomi').disable();
						Ext.getCmp('cboAsuransiLabAnatomi').disable();
						Ext.getCmp('cboPerseoranganLabAnatomi').disable();
						Ext.getCmp('cboPerusahaanLabAnatomi').disable();
					
						if(mBol === false)
						{
							loadMask.hide();
							ViewGridBawahPenjasLabAnatomi(PenJasLabAnatomi.form.ComboBox.notransaksi.getValue());
						};
					}
					else
					{
						loadMask.hide();
						ShowPesanErrorPenJasLabAnatomi('Gagal menyimpan data ini. Hubungi Admin', 'Error');
					};
				}
			}
		)

	}
	else
	{
		if(mBol === true)
		{
			loadMask.hide();
			return false;
		};
	};

};

function Dataupdate_order_PenJasLabAnatomi(mBol)
{
	if (ValidasiEntryPenJasLabAnatomi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/lab_pa/functionLABPA/update_dokter",
				params: getParamDetailSaveLabAnatomi(),
				failure: function(o)
				{	var_tampung_labAnatomi_order=false;
					loadMask.hide();
					ShowPesanErrorPenJasLabAnatomi('Error! Gagal menyimpan data ini. Hubungi Admin', 'Error');
					ViewGridBawahPenjasLabAnatomi(PenJasLabAnatomi.form.ComboBox.notransaksi.getValue());
				},
				success: function(o)
				{  	
					/* total_pasien_order_mng();
					load_data_pasienorder();
					viewGridOrderAll_RASEPRWI(); */
					var_tampung_labAnatomi_order=false;
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)
					{
						loadMask.hide();
						ShowPesanInfoPenJasLabAnatomi("Berhasil menyimpan data ini","Information");
						PenJasLabAnatomi.form.ComboBox.notransaksi.setValue(cst.notrans);
						PenJasLabAnatomi.form.ComboBox.kdpasien.setValue(cst.kdPasien);
						if(cst.kdPasien.substring(0, 1) ==='L'){
							Ext.getCmp('txtNamaUnitLabAnatomi').setValue('Laboratorium');
							Ext.getCmp('txtDokterPengirim_LabAnatomi').setValue('-');
						}
						Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').disable();
						Ext.getCmp('btnAddItemPemeriksaan_LabAnatomi').disable();
						Ext.getCmp('btnDeleteItemPemeriksaan_LabAnatomi').disable();
						Ext.getCmp('btnSimpanPenJasLabAnatomi').disable();
						//Ext.getCmp('btnSimpanPenJasLabAnatomi_kiriman').hide();
						Ext.getCmp('btnSimpanPenJasLabAnatomi').show();
						ViewGridBawahPenjasLabAnatomi(PenJasLabAnatomi.form.ComboBox.notransaksi.getValue());
						Ext.getCmp('cboKelPasienLabAnatomi').disable();
						Ext.getCmp('cboAsuransiLabAnatomi').disable();
						Ext.getCmp('cboPerseoranganLabAnatomi').disable();
						Ext.getCmp('cboPerusahaanLabAnatomi').disable();
						
						if(mBol === false)
						{
							loadMask.hide();
							ViewGridBawahPenjasLabAnatomi(PenJasLabAnatomi.form.ComboBox.notransaksi.getValue());
						};
					}
					else
					{
						loadMask.hide();
						ShowPesanErrorPenJasLabAnatomi('Gagal menyimpan data ini. Hubungi Admin', 'Error');
					};
				}
			}
		)

	}
	else
	{
		if(mBol === true)
		{
			loadMask.hide();
			return false;
		};
	};

};

function getDokterPengirimLabAnatomi(no_transaksi,kd_kasir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/lab_pa/functionLABPA/getDokterPengirim",
			params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
			failure: function(o)
			{
				ShowPesanErrorPenJasLabAnatomi('Hubungi Admin', 'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					Ext.getCmp('txtDokterPengirim_LabAnatomi').setValue(cst.nama);
				}
				else
				{
					ShowPesanErrorPenJasLabAnatomi('Gagal membaca dokter pengirim', 'Error');
				};
			}
		}

	)
}


function ValidasiEntryPenJasLabAnatomi(modul,mBolHapus)
{
	var x = 1;

	if((PenJasLabAnatomi.form.ComboBox.pasien.getValue() == '') || (Ext.get('cboDOKTER_viPenJasLabAnatomi').getValue() == '') || (Ext.get('dtpKunjungan_LabAnatomi').getValue() == '') ||
		(dsTRDetailPenJasLabList_LabAnatomi.getCount() === 0 ) || (Ext.get('cboJKPenjasLabAnatomi').getValue() == '') || (Ext.get('cboGDRPenJasLabAnatomi').getValue() == ''))
	{
		if (PenJasLabAnatomi.form.ComboBox.pasien.getValue() == '')
		{
			loadMask.hide();
			ShowPesanWarningPenJasLabAnatomi('Nama Pasien tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.get('dtpKunjungan_LabAnatomi').getValue() == '')
		{
			loadMask.hide();
			ShowPesanWarningPenJasLabAnatomi('Tanggal kunjungan tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').getValue() == '' || Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').getValue()  === '')
		{
			loadMask.hide();
			ShowPesanWarningPenJasLabAnatomi('Dokter tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (dsTRDetailPenJasLabList_LabAnatomi.getCount() === 0)
		{
			loadMask.hide();
			ShowPesanWarningPenJasLabAnatomi('Item pemeriksaan tidak boleh kosong','Warning');
			x = 0;
		} else if(Ext.get('cboJKPenjasLabAnatomi').getValue() == ''){
			loadMask.hide();
			ShowPesanWarningPenJasLabAnatomi('Jenis kelamin tidak boleh kosong','Warning');
			x = 0;
		} else if(Ext.get('cboGDRPenJasLabAnatomi').getValue() == ''){
			loadMask.hide();
			ShowPesanWarningPenJasLabAnatomi('Golongan darah tidak boleh kosong','Warning');
			x = 0;
		}
	};
	if(Ext.get('cboKelPasienLabAnatomi').getValue() == ''){// &&  Ext.get('txtCustomerLamaHide').getValue() == ''){
		loadMask.hide();
		ShowPesanWarningPenJasLabAnatomi('Jenis kelompok pasien tidak boleh kosong','Warning');
		x = 0;
	}
	if(Ext.getCmp('txtKdCustomerLamaHide_LabAnatomi').getValue() == '' && Ext.get('cboPerseoranganLabAnatomi').getValue() == '' && Ext.get('cboPerusahaanLabAnatomi').getValue() == ''&& Ext.get('cboAsuransiLabAnatomi').getValue() == ''){
		loadMask.hide();
		ShowPesanWarningPenJasLabAnatomi('Kelompok pasien tidak boleh kosong','Warning');
		x = 0;
	}
	return x;
};


function ShowPesanWarningPenJasLabAnatomi(str, modul)
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPenJasLabAnatomi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPenJasLabAnatomi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


//combo jenis kelamin
function mComboJK_LabAnatomi()
{
    var cboJKPenjasLabAnatomi = new Ext.form.ComboBox
	(
		{
			id:'cboJKPenjasLabAnatomi',
			x: 495,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis Kelamin ',
			editable: false,
			width:95,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetJKLabAnatomi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJKLabAnatomi=b.data.valueField ;
				},
			}
		}
	);
	return cboJKPenjasLabAnatomi;
};

//combo golongan darah
function mComboGDarah_LabAnatomi()
{
    var cboGDRPenJasLabAnatomi = new Ext.form.ComboBox
	(
		{
			id:'cboGDRPenJasLabAnatomi',
			x: 690,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Gol. Darah ',
			width:50,
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetGDarahLabAnatomi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGDarahLabAnatomi=b.data.displayText ;
				},

			}
		}
	);
	return cboGDRPenJasLabAnatomi;
};

//Combo Dokter Lookup
function mComboDOKTER_LabAnatomi()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viPenJasLab = new WebApp.DataStore({ fields: Field });
    dsdokter_viPenJasLab.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,

                        Sort: 'nama',
                        Sortdir: 'ASC',
                        target: 'ViewDokterPenunjang',
                        param: "kd_unit = '42'"
                    }
            }
	);
	
    var cboDOKTER_viPenJasLabAnatomi = new Ext.form.ComboBox
	(
		{
			id: 'cboDOKTER_viPenJasLabAnatomi',
			x: 130,
			y: 130,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 230,
			store: dsdokter_viPenJasLab,
			valueField: 'KD_DOKTER',
			displayField: 'NAMA',
			editable: true,
			listeners:
			{
				'select': function(a, b, c)
				{
					if(var_tampung_labAnatomi_order===true)
					{
					var_tampung_labAnatomi_order=true;
					Ext.getCmp('btnAddItemPemeriksaan_LabAnatomi').disable();
					}else{
					var_tampung_labAnatomi_order=false;
					Ext.getCmp('btnAddItemPemeriksaan_LabAnatomi').enable();
					}
				}
			}
		}
	);

    return cboDOKTER_viPenJasLabAnatomi;
};

function mCboKelompokpasienAnatomi(){
 var cboKelPasienLabAnatomi = new Ext.form.ComboBox
	(
		{

			id:'cboKelPasienLabAnatomi',
			fieldLabel: 'Kelompok Pasien',
			x: 130,
			y: 160,
			mode: 'local',
			width: 130,
			//anchor: '95%',
			forceSelection: true,
			lazyRender:true,
			triggerAction: 'all',
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
				id: 0,
				fields:
				[
					'Id',
					'displayText'
				],
				   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetKelPasienLabAnatomi,
			selectOnFocus: true,
			tabIndex:22,
			listeners:
			{
				'select': function(a, b, c)
					{
					   Combo_Select_LabAnatomi(b.data.displayText);
					   if(b.data.displayText == 'Perseorangan')
					   {

					   }
					   else if(b.data.displayText == 'Perusahaan')
					   {

					   }
					   else if(b.data.displayText == 'Asuransi')
					   {

					   }
					},
				'render': function(a,b,c)
				{
					Ext.getCmp('txtNamaPesertaAsuransiLab').hide();
					Ext.getCmp('txtNoAskesLabAnatomi').hide();
					Ext.getCmp('txtNoSJPLabAnatomi').hide();
					Ext.getCmp('cboPerseoranganLabAnatomi').hide();
					Ext.getCmp('cboAsuransiLabAnatomi').hide();
					Ext.getCmp('cboPerusahaanLabAnatomi').hide();

				}
			}

		}
	);
	return cboKelPasienLabAnatomi;
};

function mComboPerseorangan_LabAnatomi()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganLabAnatomi = new WebApp.DataStore({fields: Field});
    dsPeroranganLabAnatomi.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'customer.status=true and kontraktor.jenis_cust=0 order by customer.customer'
			}
		}
	)
    var cboPerseoranganLabAnatomi = new Ext.form.ComboBox
	(
		{
			id:'cboPerseoranganLabAnatomi',
			x: 280,
			y: 160,
			editable: false,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis',
			tabIndex:23,
			width:150,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			store:dsPeroranganLabAnatomi,
			value:selectSetPerseoranganLabAnatomi,
			listeners:
			{
				'select': function(a,b,c)
				{
				    selectSetPerseoranganLabAnatomi=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseoranganLabAnatomi;
};

function mComboPerusahaan_LabAnatomi()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanLabAnatomi = new WebApp.DataStore({fields: Field});
    dsPerusahaanLabAnatomi.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'customer.status=true and kontraktor.jenis_cust=1 order by customer.customer'
			}
		}
	)
    var cboPerusahaanLabAnatomi = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanLabAnatomi',
			x: 280,
			y: 160,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
			width:150,
		    store: dsPerusahaanLabAnatomi,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
		    listeners:
			{
			    'select': function(a,b,c)
				{
					selectSetPerusahaanLabAnatomi=b.data.valueField ;
				}
			}
		}
	);

    return cboPerusahaanLabAnatomi;
};

function mComboAsuransi_LabAnatomi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    dsAsuransiLabAnatomi = new WebApp.DataStore({fields: Field_poli_viDaftar});

	dsAsuransiLabAnatomi.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: "customer.status=true and kontraktor.jenis_cust=2 order by customer.customer"
            }
        }
    )
    var cboAsuransiLabAnatomi = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransiLabAnatomi',
			x: 280,
			y: 160,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Asuransi',
			align: 'Right',
			width:150,
			//anchor: '95%',
			store: dsAsuransiLabAnatomi,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransiLabAnatomi=b.data.valueField ;
				}
			}
		}
	);
	return cboAsuransiLabAnatomi;
};


function Combo_Select_LabAnatomi(combo)
{
   var value = combo

   if(value == "Perseorangan")
   {
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLabAnatomi').hide()
        Ext.getCmp('txtNoSJPLabAnatomi').hide()
        Ext.getCmp('cboPerseoranganLabAnatomi').show()
        Ext.getCmp('cboAsuransiLabAnatomi').hide()
        Ext.getCmp('cboPerusahaanLabAnatomi').hide()


   }
   else if(value == "Perusahaan")
   {
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLabAnatomi').hide()
        Ext.getCmp('txtNoSJPLabAnatomi').hide()
        Ext.getCmp('cboPerseoranganLabAnatomi').hide()
        Ext.getCmp('cboAsuransiLabAnatomi').hide()
        Ext.getCmp('cboPerusahaanLabAnatomi').show()

   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiLab').show()
         Ext.getCmp('txtNoAskesLabAnatomi').show()
         Ext.getCmp('txtNoSJPLabAnatomi').show()
         Ext.getCmp('cboPerseoranganLabAnatomi').hide()
         Ext.getCmp('cboAsuransiLabAnatomi').show()
         Ext.getCmp('cboPerusahaanLabAnatomi').hide()

       }
}



function addNewData_LabAnatomi(){
	TmpNotransaksi_LabAnatomi='';
	KdKasirAsal_LabAnatomi='';
	TglTransaksiAsal_LabAnatomi='';
	Kd_Spesial_LabAnatomi='';
	vkode_customer_LabAnatomi = '';
	No_Kamar_LabAnatomi='';
	PenJasLabAnatomi.vars.kd_tarif='';
	//Ext.getCmp('btnSimpanPenJasLabAnatomi_kiriman').hide();
	Ext.getCmp('btnSimpanPenJasLabAnatomi').show();
	PenJasLabAnatomi.form.ComboBox.kdpasien.setValue('');
	Ext.getCmp('txtDokterPengirim_LabAnatomi').setValue('');
	PenJasLabAnatomi.form.ComboBox.notransaksi.setValue('');
	Ext.getCmp('txtNamaUnitLabAnatomi').setValue('');
	Ext.getCmp('txtKdUnitLabAnatomi').setValue('');
	Ext.getCmp('txtKdDokter').setValue('');
	Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').setValue('');
	Ext.getCmp('txtAlamat_LabAnatomi').setValue('');
	PenJasLabAnatomi.form.ComboBox.pasien.setValue('');
	Ext.getCmp('dtpTtl_LabAnatomi').setValue(nowTglTransaksi_LabAnatomi);
	Ext.getCmp('txtKdUrutMasuk_LabAnatomi').setValue('');
	Ext.getCmp('cboJKPenjasLabAnatomi').setValue('');
	Ext.getCmp('cboGDRPenJasLabAnatomi').setValue('')
	Ext.getCmp('txtKdCustomerLamaHide_LabAnatomi').setValue('');//tampung kd customer untuk transaksi selain kunjungan langsung
	Ext.getCmp('cboKelPasienLabAnatomi').setValue('');
	//Ext.getCmp('txtCustomerLamaHide').setValue('');
	Ext.getCmp('cboAsuransiLabAnatomi').setValue('');
	Ext.getCmp('txtNamaPesertaAsuransiLab').setValue('');
	Ext.getCmp('txtNoSJPLabAnatomi').setValue('');
	Ext.getCmp('txtNoAskesLabAnatomi').setValue('');
	Ext.getCmp('cboPerusahaanLabAnatomi').setValue('');
	Ext.getCmp('txtKdDokterPengirim').setValue('');

	//Ext.getCmp('txtCustomerLamaHide').hide();
	Ext.getCmp('cboAsuransiLabAnatomi').hide();
	Ext.getCmp('txtNamaPesertaAsuransiLab').hide();
	Ext.getCmp('txtNoSJPLabAnatomi').hide();
	Ext.getCmp('txtNoAskesLabAnatomi').hide();
	Ext.getCmp('cboPerusahaanLabAnatomi').hide();
	Ext.getCmp('cboKelPasienLabAnatomi').show();

	Ext.getCmp('btnAddItemPemeriksaan_LabAnatomi').disable();
	Ext.getCmp('btnDeleteItemPemeriksaan_LabAnatomi').disable();

	PenJasLabAnatomi.form.ComboBox.notransaksi.enable();
	Ext.getCmp('txtDokterPengirim_LabAnatomi').enable();
	Ext.getCmp('txtNamaUnitLabAnatomi').enable();
	Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').enable();
	PenJasLabAnatomi.form.ComboBox.pasien.enable();
	PenJasLabAnatomi.form.ComboBox.kdpasien.enable();
	Ext.getCmp('txtAlamat_LabAnatomi').enable();
	Ext.getCmp('cboJKPenjasLabAnatomi').enable();
	Ext.getCmp('cboGDRPenJasLabAnatomi').enable();
	Ext.getCmp('cboKelPasienLabAnatomi').enable();
	Ext.getCmp('dtpTtl_LabAnatomi').enable();
	Ext.getCmp('dtpKunjungan_LabAnatomi').enable();
	Ext.getCmp('btnSimpanPenJasLabAnatomi').enable();
	PenJasLabAnatomi.form.ComboBox.notransaksi.setReadOnly(false);
	Ext.getCmp('txtNamaUnitLabAnatomi').setReadOnly(true);
	//Ext.getCmp('cboDOKTER_viPenJasLabAnatomi').setReadOnly(false);
	PenJasLabAnatomi.form.ComboBox.pasien.setReadOnly(false);
	Ext.getCmp('txtAlamat_LabAnatomi').setReadOnly(false);
	Ext.getCmp('cboJKPenjasLabAnatomi').setReadOnly(false);
	Ext.getCmp('cboGDRPenJasLabAnatomi').setReadOnly(false);
	Ext.getCmp('cboKelPasienLabAnatomi').setReadOnly(false);
	Ext.getCmp('dtpTtl_LabAnatomi').setReadOnly(false);
	//Ext.getCmp('dtpKunjungan_LabAnatomi').setReadOnly(true);
	gridDTItemTest_LabAnatomi.enable();
	ViewGridBawahLookupPenjasLabAnatomi('');
	ViewGridBawahPenjasLabAnatomi('');
	var_tampung_labAnatomi_order=false;
}


function PilihDokterLookUp(NoTrans,TglTrans,kdPrdk,kdTrf,tglBerlaku,trf)
{


	RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk);

	var FormPilihDokter =
	{
		layout: 'column',
		border: false,
		anchor: '90%',
		items:
		[
			{
				columnWidth: 0.18,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						hideLabel:true,
						id: 'BtnOktrDokter',
						handler:function()
						{
							if(dataTrDokter_LabAnatomi.length == '' || dataTrDokter_LabAnatomi.length== 'undefined')
							{
								var jumlah =0;
							}
							else
							{
								var jumlah = dataTrDokter_LabAnatomi.length;
							}
							
							for(var i = 0 ;i< dsTrDokter_LabAnatomi.getCount()  ;i++)
							{
								dataTrDokter_LabAnatomi.push({
									index		: i,
									no_tran 	: NoTrans,
									//urut		: Urt,
									tgl_trans 	: TglTrans,
									kd_produk 	: kdPrdk,
									kd_tarif	: kdTrf,
									tgl_berlaku	: tglBerlaku,
									tarif		: trf,
									kd_dokter 	: dsTrDokter_LabAnatomi.data.items[i].data.KD_DOKTER ,
									kd_job 		: dsTrDokter_LabAnatomi.data.items[i].data.KD_JOB ,
								});
							}
							FormLookUDokter.close();
						}
					},
				],
			},
			{
				columnWidth: 0.2,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype:'button',
						text:'Batal' ,
						width:70,
						hideLabel:true,
						id: 'BtnCancelGantiDokter',
						handler:function()
						{
							dataTrDokter_LabAnatomi = [];
							FormLookUDokter.close()
						}
					}
				]
			}


		]
  }

    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
         new Ext.grid.RowNumberer(),
        {
        	 header			: 'kd_dokter',
        	 dataIndex		: 'KD_DOKTER',
			 width			: 80,
        	 menuDisabled	: true,
        	 hidden 		: false
        },{
            header			:'Nama Dokter',
            dataIndex		: 'NAMA',
            sortable		: false,
            hidden			: false,
			menuDisabled	: true,
			width			: 200,
            editor			: getTrDokter(dsTrDokter_LabAnatomi)
	    },{
            header			: 'Job',
            dataIndex		: 'KD_JOB',
            width			:150,
		   	menuDisabled	:true,
			editor			: JobDokter(),

        },
        ]
    );

  var GridPilihDokter= new Ext.grid.EditorGridPanel({
        title		: '',
		id			: 'GridDokterTr',
		stripeRows	: true,
		width		: 490,
		height		: 245,
        store		: RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk),
        border		: false,
        frame		: false,
        anchor		: '100% 60%',
        autoScroll	: true,
        cm			: GridTrDokterColumnModel,

        //viewConfig	: {forceFit: true},
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
        	trcellCurrentTindakan = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
   		  }
		}
    });



	var PanelTrDokter = new Ext.Panel
	({
            id: 'PanelPilitTrDokter',
            region: 'center',
			width: 490,
			height: 260,
            border:false,
            layout: 'form',
            frame:true,
            anchor: '100% 8.0001%',
            autoScroll:false,

            items:
            [
			GridPilihDokter,
			{xtype: 'tbspacer',height: 3, width:100},
			FormPilihDokter
			]
	})


    var lebar = 500;
    var FormLookUDokter = new Ext.Window
    (
        {
            id: 'gridTrDokter',
            title: 'Pilih Dokter Tindakan',
            closeAction: 'destroy',
            width: lebar,
            height: 270,
            border: false,
            resizable: false,
            plain: true,
            layout: 'column',
            iconCls: 'Request',
            modal: true,
			tbar		:[
						{
						xtype	: 'button',
						id		: 'btnaddtrdokter',
						iconCls	: 'add',
						text	: 'Tambah Data Dokter',
						handler	:  function()
						{
						dataTrDokter_LabAnatomi = [];
						TambahBaristrDokter(dsTrDokter_LabAnatomi);
						}
						},
						'-',
						{
						xtype	: 'button',
						id		: 'btndeltrdokter',
						iconCls	: 'remove',
						text	: 'Delete Data Dokter',
						handler	: function()
						{
						    if (dsTrDokter_LabAnatomi.getCount() > 0 )
							{
                            if(trcellCurrentTindakan != undefined)
							{
                             HapusDataTrDokter(dsTrDokter_LabAnatomi);
                            }
                        	}else{
                            ShowPesanWarningDiagnosa('Pilih record ','Hapus data');
                    		}
							}
						},
						'-',
			],
           	items: [
			//GridPilihDokter,

			PanelTrDokter
			],
            listeners:
            {
            }
        }
    );


 FormLookUDokter.show();




};

var mtrDataDokter = Ext.data.Record.create([
	   {name: 'KD_DOKTER', mapping:'KD_DOKTER'},
	   {name: 'NAMA', mapping:'NAMA'},
	   {name: 'KD_JOB', mapping:'KD_JOB'},
	]);

function TambahBaristrDokter(store){
    var x=true;
    if (x === true) {
        var p = BaristrDokter();
        store.insert(store.getCount(), p);
    }
}

function BaristrDokter(){
	var p = new mtrDataDokter({
		'KD_DOKTER':'',
		'NAMA':'',
	    'KD_JOB':'',
	});
	return p;
};

function getTrDokter(store){
	var trDokterData = new Ext.form.ComboBox({
	   typeAhead: true,
	   triggerAction: 'all',
	   lazyRender: true,
	   mode: 'local',
	   hideTrigger:true,
	   forceSelection: true,
	   selectOnFocus:true,
	   store: GetDokter(),
	   valueField: 'NAMA',
	   displayField: 'NAMA',
	   anchor: '95%',
	   listeners:{
		   select: function(a, b, c){
			  store.data.items[trcellCurrentTindakan].data.KD_DOKTER = b.data.KD_DOKTER;
		   }
	   }
	});
	return trDokterData;
}

function GetDokter(){
	var dataDokter  = new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA'] });
	dataDokter.load({
		params: {
			Skip: 0,
			Take: 50,
			target:'ViewComboDokter',
			param: ''
		}
	});
	return dataDokter;
}

function JobDokter(){
	var combojobdokter = new Ext.form.ComboBox({
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '96.8%',
		emptyText		: '',
		store			: new Ext.data.ArrayStore({
			fields	: ['Id','displayText'],
			data	: [[1, 'Dokter'],[2, 'Dokter Anastesi']]
		}),
		valueField		: 'displayText',
		displayField	: 'displayText'
	});
	return combojobdokter;
};


function RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk)
{
	dsTrDokter_LabAnatomi.load({
		params	: {
			Skip	: 0,
			Take	: 50,
			target	:'ViewTrDokter',
			param	:"b.kd_kasir='06' and b.no_transaksi=~"+NoTrans+"~ and b.tgl_transaksi=~"+TglTrans+"~ and a.kd_produk = ~"+kdPrdk+"~"
		}
	});

	return dsTrDokter_LabAnatomi;
}

function HapusDataTrDokter(store)
{

    if ( trcellCurrentTindakan != undefined ){
        if (store.data.items[trcellCurrentTindakan].data.KD_DOKTER != '' && store.data.items[trcellCurrentTindakan].data.KD_PRODUK != ''){
					DataDeleteTrDokter(store);
					dsTrDokter_LabAnatomi.removeAt(trcellCurrentTindakan);
				}

        }/*else{
            dsTrDokter_LabAnatomi.removeAt(trcellCurrentTindakan);
        }*/


}

function DataDeleteTrDokter(store){
    Ext.Ajax.request({
        url: baseURL + "index.php/lab_pa/DeleteDataObj",
        params:
		{
			Table			:'ViewTrDokter',
			no_transaksi	:Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),
			kd_unit			:Ext.getCmp('txtKdUnitIGD').getValue(),
			kd_produk		:store.data.items[trcellCurrentTindakan].data.KD_PRODUK,
			kd_kasir		:'06',
			kd_dokter		:store.data.items[trcellCurrentTindakan].data.KD_DOKTER,
			urut			:PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,
			tgl_transaksi	:PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,
		},
		//getParamDataDeleteKasirIGDDetail(),
        success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                store.removeAt(CurrentKasirIGD.row);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
                RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.KD_PRODUK);
				trcellCurrentTindakan=undefined;
            }else{
                ShowPesanInfoDiagnosa(nmPesanHapusError,nmHeaderHapusData);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
				RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.KD_PRODUK);
            }
        }
    });
}


//---------------------ORDER MANAJEMEN-----------------------------------
function load_data_pasienorder(param)
{
	//console.log(param);
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionLABPoliklinik/getPasien",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			cbopasienorder_mng_LabAnatomi.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dspasienorder_mng_LabAnatomi.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dspasienorder_mng_LabAnatomi.add(recs);
				//console.log(o);
			}
		}
	});
}

function total_pasien_order_mng()
{
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionLABPoliklinik/gettotalpasienorder_mng",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttr').setValue(cst.jumlahpasien);
			//console.log(cst);

		}
	});
};


function viewGridOrderAll_RASEPRWI(nama){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLABPoliklinik/getPasien",
			params: {text:nama},
			failure: function(o)
			{
				ShowPesanWarningPenJasLabAnatomi('Hubungi Admin', 'Error');
			},
			success: function(o)
			{   dsGetdetail_order_LabAnatomi.removeAll();


				var cst = Ext.decode(o.responseText);

				if (cst.processResult === 'SUCCESS')
				{


					var recs=[],
					recType=dsGetdetail_order_LabAnatomi.recordType;

					for(var i=0; i<cst.listData.length; i++){

						recs.push(new recType(cst.listData[i]));


					}
					dsGetdetail_order_LabAnatomi.add(recs);

					console.log(dsGetdetail_order_LabAnatomi);
					gridGetdetail_order_LabAnatomi.getView().refresh();

				}
				else
				{
					ShowPesanWarningPenJasLabAnatomi('Gagal membaca Data ini', 'Error');
				};
			}
		}

	);
}
function Getdetail_order()
{
    dsGetdetail_order_LabAnatomi = new WebApp.DataStore({ fields: ['kd_pasien','nama']});



    gridGetdetail_order_LabAnatomi = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Order Unit Lain',
            stripeRows: true,
            store: dsGetdetail_order_LabAnatomi,
			height:140,
			anchor: '100%',
            border: false,
            columnLines: true,
			frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi_LabAnatomi = dsGetdetail_order_LabAnatomi.getAt(row);
                            CurrentPenJasLabAnatomi.row = row;
                            CurrentPenJasLabAnatomi.data = cellSelecteddeskripsi_LabAnatomi;
							var_tampung_labAnatomi_order=true;
							ViewGridBawahPenjasLabAnatomi(cellSelecteddeskripsi_LabAnatomi.data.no_transaksi,cellSelecteddeskripsi_LabAnatomi.data);
							PenJasLabAnatomi.form.ComboBox.kdpasien.disable();
							PenJasLabAnatomi.form.ComboBox.kdpasien.setValue(cellSelecteddeskripsi_LabAnatomi.data.kd_pasien);
							PenJasLabAnatomi.form.ComboBox.pasien.disable();
							PenJasLabAnatomi.form.ComboBox.pasien.setValue(cellSelecteddeskripsi_LabAnatomi.data.nama);
							PenJasLabAnatomi.form.ComboBox.notransaksi.setValue(cellSelecteddeskripsi_LabAnatomi.data.no_transaksi);
							PenJasLabAnatomi.form.ComboBox.notransaksi.setReadOnly(true);


							Ext.getCmp('btnAddItemPemeriksaan_LabAnatomi').disable();
							Ext.getCmp('btnDeleteItemPemeriksaan_LabAnatomi').disable();
							Ext.getCmp('btnSimpanPenJasLabAnatomi').disable();
							//Ext.getCmp('btnSimpanPenJasLabAnatomi_kiriman').show();
							PenJasLabAnatomi.vars.kd_tarif=cellSelecteddeskripsi_LabAnatomi.data.kd_tarif;

							Ext.Ajax.request(
							{
								url: baseURL + "index.php/lab_pa/functionLABPA/getCurrentShiftLab",
								params: {
									command: '0',
									// parameter untuk url yang dituju (fungsi didalam controller)
								},
								failure: function(o)
								{
									var cst = Ext.decode(o.responseText);
								},
								success: function(o) {
									tampungshiftsekarang_LabAnatomi=o.responseText;
								}
							});
                        }
                    }
                }
            ),
            cm: new Ext.grid.ColumnModel
				(
					[
						new Ext.grid.RowNumberer(),
						{
							header: 'Medrec',
							dataIndex: 'kd_pasien',
							width:250,
							menuDisabled:true,
							hidden :false

						},{
							header: 'Nama',
							dataIndex: 'nama',
							width:250,
							menuDisabled:true,
							hidden :false
						 },{
							header: 'alamat',
							dataIndex: 'alamat',
							width:250,
							menuDisabled:true,
							hidden :false
						 },{
							header: 'Asal Pasien',
							dataIndex: 'nama_unit',
							width:250,
							menuDisabled:true,
							hidden :false

						}


					   ]
				),
				listeners: {
							rowclick: function( $this, rowIndex, e )
							{
								cellCurrentLabAnatomi = rowIndex;
								//console.log(cellCurrentLabAnatomi);
							},
							celldblclick: function(gridView,htmlElement,columnIndex,dataRecord)
							{

							}
						},
				tbar :[
				{
					xtype: 'label',
					text: 'Order Unit lain : ' ,
					style : {
						color : 'red'
					}
				},
				{xtype: 'tbspacer',height: 3, width:5},
				 {
					x: 40,
					y: 40,
					xtype: 'textfield',
					fieldLabel: 'No. Medrec',
					name: 'txtcounttr',
					id: 'txtcounttr',
					width: 50,
					disabled:true,
					style : {
						color : 'red'
					},
					listeners:
					{

					}
				},mComboorder(),
				{xtype: 'tbspacer',height: 3, width:30},
				{
					x: 290,
					y: 100,
					xtype: 'button',
					text: 'Refresh Order',
					iconCls: 'refresh',
					hideLabel: false,
					handler: function(sm, row, rec) {
						total_pasien_order_mng()
					}
				}],
        }


    );

    return gridGetdetail_order_LabAnatomi;
};

function mComboorder()
{


 var Field = ['no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
				'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin',
				'gol_darah','kd_customer','customer',
				'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien','display'];
    dspasienorder_mng_LabAnatomi = new WebApp.DataStore({ fields: Field });
	total_pasien_order_mng();
	load_data_pasienorder();
	cbopasienorder_mng_LabAnatomi= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_mng_LabAnatomi',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 230,
			store: dspasienorder_mng_LabAnatomi,
			valueField: 'kd_pasien',
			displayField: 'display',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
					var_tampung_labAnatomi_order=true;
					ViewGridBawahPenjasLabAnatomi(b.data.no_transaksi,b.data);
					PenJasLabAnatomi.form.ComboBox.kdpasien.disable();
					PenJasLabAnatomi.form.ComboBox.kdpasien.setValue(b.data.kd_pasien);
					PenJasLabAnatomi.form.ComboBox.pasien.disable();
					PenJasLabAnatomi.form.ComboBox.pasien.setValue(b.data.nama);
					PenJasLabAnatomi.form.ComboBox.notransaksi.setValue(b.data.no_transaksi);
					PenJasLabAnatomi.form.ComboBox.notransaksi.setReadOnly(true);


					Ext.getCmp('btnAddItemPemeriksaan_LabAnatomi').disable();
					Ext.getCmp('btnDeleteItemPemeriksaan_LabAnatomi').disable();
					Ext.getCmp('btnSimpanPenJasLabAnatomi').disable();
					//Ext.getCmp('btnSimpanPenJasLabAnatomi_kiriman').hide();
					PenJasLabAnatomi.vars.kd_tarif=b.data.kd_tarif;

					Ext.Ajax.request(
					{
						url: baseURL + "index.php/lab_pa/functionLABPA/getCurrentShiftLab",
						params: {
							command: '0',
							// parameter untuk url yang dituju (fungsi didalam controller)
						},
						failure: function(o)
						{
							var cst = Ext.decode(o.responseText);
						},
						success: function(o) {
							tampungshiftsekarang_LabAnatomi=o.responseText;
						}
					});

				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);

						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								load_data_pasienorder(param);
								viewGridOrderAll_RASEPRWI(param);

							}
						},1000);
					}
				}
			}
		}
	);return cbopasienorder_mng_LabAnatomi;
};




