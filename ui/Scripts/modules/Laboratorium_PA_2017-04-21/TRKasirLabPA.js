
var CurrentHistory =
        {
            data: Object,
            details: Array,
            row: 0
        };
var FormDepan2KasirLABPA;
var CurrentPenataJasaKasirLABPA =
        {
            data: Object,
            details: Array,
            row: 0
        };
var tmp_kodeunitkamar_LABPA;
var tmp_kdspesial_LABPA;
var tmp_nokamar_LABPA;
var msg_box_alasanhapus_LABPA;
var tampungshiftsekarang_LABPA;
var tampungtypedata_LABPA;
var tanggaltransaksitampung_LABPA;
var kdpaytransfer_LABPA = 'T1';
var mRecordKasirLABPA = Ext.data.Record.create
        (
                [
                    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
                    {name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
                    {name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
                    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
                    {name: 'HARGA', mapping: 'HARGA'},
                    {name: 'QTY', mapping: 'QTY'},
                    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
                    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
                    {name: 'URUT', mapping: 'URUT'}
                ]
                );
var selectindukunitLABPAKasir = 'Radiologi';
var cellSelecteddeskripsi_LABPA;
var cellSelected2deskripsii_LABPA;
var Kdtransaksi_LABPA;
var tapungkd_pay_LABPA;
var dsTRDetailHistoryList_LABPA;
var AddNewHistory = true;
var selectCountHistory = 50;
var now = new Date();
var rowSelectedHistory_LABPA;

var FormLookUpsdetailTRHistory_LABPA;
var cellSelectedtutup_LABPA;
var vkd_unit_LABPA;
var kdcustomeraa_LABPA;
var nowTglTransaksi_LABPA = new Date();
var FormLookUpsdetailTRTransfer_LabPa;
var labelisi_LabPa;
var jenispay_LabPa;
var variablehistori_LabPa;
var selectCountStatusByr_viKasirLABPAKasir = 'Belum Lunas';
var tranfertujuan_LabPa = 'Rawat Inap';
var dsTRKasirLABPAKasirList;
var dsTRDetailKasirLABPAKasirList;
var AddNewKasirLABPAKasir = true;
var selectCountKasirLABPAKasir = 50;
var rowSelectedKasirLABPAKasir;

var FormLookUpsdetailTRKasirLABPA;
var valueStatusCMKasirLABPAView = 'All';
var nowTglTransaksi_LABPA = new Date();
var dsComboBayar_LabPa;
var vkode_customer_LabPa;
var vflag_LabPa;
var gridDTLTRKasirLABPA;
var notransaksi_LABPA;
var tgltrans_LABPA;
var kodepasien_LABPA;
var namapasien_LABPA;
var kodeunit_LABPA;
var namaunit_LABPA;
var kodepay_LABPA;
var kdkasir_LABPA;
var uraianpay_LABPA;
CurrentPage.page = getPanelKasirLABPA(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelKasirLABPA(mod_id)
{

    var Field = ['KD_DOKTER', 'NO_TRANSAKSI', 'KD_UNIT', 'KD_PASIEN', 'NAMA', 'NAMA_UNIT', 'ALAMAT',
        'TANGGAL_TRANSAKSI', 'NAMA_DOKTER', 'KD_CUSTOMER', 'CUSTOMER', 'URUT_MASUK', 'LUNAS',
        'KET_PAYMENT', 'CARA_BAYAR', 'JENIS_PAY', 'KD_PAY', 'POSTING', 'TYPE_DATA', 'CO_STATUS', 'KD_KASIR',
        'NAMAUNITASAL', 'NO_TRANSAKSI_ASAL', 'KD_KASIR_ASAL', 'FLAG'];
    dsTRKasirLABPAKasirList = new WebApp.DataStore({fields: Field});
    refeshKasirLABPAKasir();
    var grListTRKasirLABPA = new Ext.grid.EditorGridPanel
            (
                    {
                        stripeRows: true,
                        store: dsTRKasirLABPAKasirList,
                        columnLines: false,
                        autoScroll: true,
                        anchor: '100% 45%',
                        //height:325,
                        border: false,
                        sort: false,
                        sm: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        rowselect: function (sm, row, rec)
                                                        {
                                                            rowSelectedKasirLABPAKasir = dsTRKasirLABPAKasirList.getAt(row);


                                                        }
                                                    }
                                        }
                                ),
                        listeners:
                                {
                                    rowdblclick: function (sm, ridx, cidx)
                                    {

                                        rowSelectedKasirLABPAKasir = dsTRKasirLABPAKasirList.getAt(ridx);

                                        if (rowSelectedKasirLABPAKasir != undefined)
                                        {
                                            if (rowSelectedKasirLABPAKasir.data.LUNAS == 'f')
                                            {
                                                KasirLookUpLAB(rowSelectedKasirLABPAKasir.data);
                                            } else
                                            {
                                                ShowPesanWarningKasirLABPA('Pembayaran telah balance', 'Pembayaran');
                                            }
                                        } else {
                                            ShowPesanWarningKasirLABPA('Silahkan Pilih data   ', 'Pembayaran');
                                        }

                                    },
                                    rowclick: function (sm, ridx, cidx)
                                    {
                                        cellSelectedtutup_LABPA = rowSelectedKasirLABPAKasir.data.NO_TRANSAKSI;

                                        if (rowSelectedKasirLABPAKasir.data.LUNAS == 't' && rowSelectedKasirLABPAKasir.data.CO_STATUS == 't')
                                        {

                                            Ext.getCmp('btnEditKasirLABPA').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirLABPA').disable();
                                            Ext.getCmp('btnHpsBrsKasirLABPA').disable();
                                            Ext.getCmp('btnLookupKasirLABPA').disable();
                                            Ext.getCmp('btnSimpanLABPA').disable();
                                            Ext.getCmp('btnHpsBrsLABPA').disable();
                                            //Ext.getCmp('btnTransferKasirLABPA').disable();

                                        } else if (rowSelectedKasirLABPAKasir.data.LUNAS == 'f' && rowSelectedKasirLABPAKasir.data.CO_STATUS == 'f')
                                        {
                                            Ext.getCmp('btnTutupTransaksiKasirLABPA').disable();
                                            Ext.getCmp('btnHpsBrsKasirLABPA').enable();
                                            Ext.getCmp('btnEditKasirLABPA').enable();
                                            Ext.getCmp('btnLookupKasirLABPA').enable();
                                            Ext.getCmp('btnSimpanLABPA').enable();
                                            Ext.getCmp('btnHpsBrsLABPA').enable();
                                            //Ext.getCmp('btnTransferKasirLABPA').enable();

                                        } else if (rowSelectedKasirLABPAKasir.data.LUNAS == 't' && rowSelectedKasirLABPAKasir.data.CO_STATUS == 'f')
                                        {
                                            Ext.getCmp('btnEditKasirLABPA').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirLABPA').enable();
                                            Ext.getCmp('btnHpsBrsKasirLABPA').enable();
                                            Ext.getCmp('btnLookupKasirLABPA').disable();
                                            Ext.getCmp('btnSimpanLABPA').disable();
                                            Ext.getCmp('btnHpsBrsLABPA').disable();
                                            //Ext.getCmp('btnTransferKasirLABPA').disable();

                                        }

                                        kdkasirasal = rowSelectedKasirLABPAKasir.data.KD_KASIR_ASAL;
                                        notransaksiasal = rowSelectedKasirLABPAKasir.data.NO_TRANSAKSI_ASAL;
                                        notransaksi_LABPA = rowSelectedKasirLABPAKasir.data.NO_TRANSAKSI;
                                        tgltrans_LABPA = rowSelectedKasirLABPAKasir.data.TANGGAL_TRANSAKSI;
                                        kodepasien_LABPA = rowSelectedKasirLABPAKasir.data.KD_PASIEN;
                                        namapasien_LABPA = rowSelectedKasirLABPAKasir.data.NAMA;
                                        kodeunit_LABPA = rowSelectedKasirLABPAKasir.data.KD_UNIT;
                                        namaunit_LABPA = rowSelectedKasirLABPAKasir.data.NAMA_UNIT;
                                        kodepay_LABPA = rowSelectedKasirLABPAKasir.data.KD_PAY;
                                        kdkasir_LABPA = rowSelectedKasirLABPAKasir.data.KD_KASIR;
                                        uraianpay_LABPA = rowSelectedKasirLABPAKasir.data.CARA_BAYAR;
                                        kdcustomeraa_LABPA = rowSelectedKasirLABPAKasir.data.KD_CUSTOMER;

                                        Ext.Ajax.request({
                                            url: baseURL + "index.php/main/getcurrentshift",
                                            params: {
                                                //UserID: 'Admin',
                                                command: '0',
                                                // parameter untuk url yang dituju (fungsi didalam controller)
                                            },
                                            failure: function (o)
                                            {
                                                var cst = Ext.decode(o.responseText);
                                            },
                                            success: function (o) {
                                                tampungshiftsekarang_LABPA = o.responseText
                                            }
                                        });

                                        RefreshDatahistoribayar(rowSelectedKasirLABPAKasir.data.NO_TRANSAKSI);
                                        RefreshDataKasirLABPADetail(rowSelectedKasirLABPAKasir.data.NO_TRANSAKSI);

                                    }


                                },
                        cm: new Ext.grid.ColumnModel
                                (
                                        [
                                            //CUSTOMER
                                            {
                                                id: 'colLUNAScoba',
                                                header: 'Status Lunas',
                                                dataIndex: 'LUNAS',
                                                sortable: true,
                                                width: 90,
                                                align: 'center',
                                                renderer: function (value, metaData, record, rowIndex, colIndex, store)
                                                {
                                                    switch (value)
                                                    {
                                                        case 't':
                                                            metaData.css = 'StatusHijau'; // 
                                                            break;
                                                        case 'f':
                                                            metaData.css = 'StatusMerah'; // rejected

                                                            break;
                                                    }
                                                    return '';
                                                }
                                            },
                                            {
                                                id: 'coltutuptr',
                                                header: 'Tutup transaksi',
                                                dataIndex: 'CO_STATUS',
                                                sortable: true,
                                                width: 90,
                                                align: 'center',
                                                renderer: function (value, metaData, record, rowIndex, colIndex, store)
                                                {
                                                    switch (value)
                                                    {
                                                        case 't':
                                                            metaData.css = 'StatusHijau'; // 
                                                            break;
                                                        case 'f':
                                                            metaData.css = 'StatusMerah'; // rejected

                                                            break;
                                                    }
                                                    return '';
                                                }
                                            },
                                            {
                                                id: 'colReqIdViewKasirLABPA',
                                                header: 'No. Transaksi',
                                                dataIndex: 'NO_TRANSAKSI',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 80
                                            },
                                            {
                                                id: 'colTglKasirLABPAViewKasirLAB',
                                                header: 'Tgl Transaksi',
                                                dataIndex: 'TANGGAL_TRANSAKSI',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 75,
                                                renderer: function (v, params, record)
                                                {
                                                    return ShowDate(record.data.TANGGAL_TRANSAKSI);
                                                }
                                            },
                                            {
                                                header: 'No. Medrec',
                                                width: 65,
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                dataIndex: 'KD_PASIEN',
                                                id: 'colKasirLABPAerViewKasirLAB'
                                            },
                                            {
                                                header: 'Pasien',
                                                width: 190,
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                dataIndex: 'NAMA',
                                                id: 'colKasirLABPAerViewKasirLAB'
                                            },
                                            {
                                                id: 'colLocationViewKasirLABPA',
                                                header: 'Alamat',
                                                dataIndex: 'ALAMAT',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 170
                                            },
                                            {
                                                id: 'colDeptViewKasirLABPA',
                                                header: 'Dokter',
                                                dataIndex: 'NAMA_DOKTER',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 150
                                            },
                                            {
                                                id: 'colunitViewKasirLABPA',
                                                header: 'Unit',
                                                dataIndex: 'NAMA_UNIT',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                hidden: true,
                                                width: 90
                                            },
                                            {
                                                id: 'colasaluViewKasirLABPA',
                                                header: 'Unit asal',
                                                dataIndex: 'NAMAUNITASAL',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 90
                                            },
                                            {
                                                id: 'colcustomerViewKasirLABPA',
                                                header: 'Kel. Pasien',
                                                dataIndex: 'CUSTOMER',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 90
                                            }


                                        ]
                                        ),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnEditKasirLABPA',
                                        text: 'Pembayaran',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                        handler: function (sm, row, rec)
                                        {
                                            if (rowSelectedKasirLABPAKasir != undefined) {
                                                KasirLookUpLAB(rowSelectedKasirLABPAKasir.data);
                                            } else {
                                                ShowPesanWarningKasirLABPA('Pilih data tabel  ', 'Pembayaran');
                                            }
                                        }, disabled: true
                                    }, ' ', ' ', '' + ' ', '' + ' ', '' + ' ', '' + ' ', ' ', '' + '-',
                                    {
                                        text: 'Tutup Transaksi',
                                        id: 'btnTutupTransaksiKasirLABPA',
                                        tooltip: nmHapus,
                                        iconCls: 'remove',
                                        handler: function ()
                                        {
                                            if (cellSelectedtutup_LABPA == '' || cellSelectedtutup_LABPA == 'undefined') {
                                                ShowPesanWarningKasirLABPA('Pilih data tabel  ', 'Pembayaran');
                                            } else {
                                                UpdateTutuptransaksi(false);
                                                Ext.getCmp('btnEditKasirLABPA').disable();
                                                Ext.getCmp('btnTutupTransaksiKasirLABPA').disable();
                                                Ext.getCmp('btnHpsBrsKasirLABPA').disable();
                                            }
                                        }, disabled: true
                                    }
                                ]
                    }
            );

    var LegendViewCMRequest = new Ext.Panel
            (
                    {
                        id: 'LegendViewCMRequest',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        //height:32,
                        anchor: '100% 8.0001%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        columnWidth: .033,
                                        layout: 'form',
                                        style: {'margin-top': '-1px'},
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        border: false,
                                        html: '<img src="' + baseURL + 'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                                    },
                                    {
                                        columnWidth: .08,
                                        layout: 'form',
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        style: {'margin-top': '1px'},
                                        border: false,
                                        html: " Lunas"
                                    },
                                    {
                                        columnWidth: .033,
                                        layout: 'form',
                                        style: {'margin-top': '-1px'},
                                        border: false,
                                        //height:35,
                                        anchor: '100% 8.0001%',
                                        //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                                        html: '<img src="' + baseURL + 'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                                    },
                                    {
                                        columnWidth: .1,
                                        layout: 'form',
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        style: {'margin-top': '1px'},
                                        border: false,
                                        html: " Belum Lunas"
                                    }
                                ]

                    }
            )
    var GDtabDetailLAB = new Ext.TabPanel
            (
                    {
                        id: 'GDtabDetailLAB',
                        region: 'center',
                        activeTab: 0,
                        anchor: '100% 40%',
                        border: false,
                        plain: true,
                        defaults:
                                {
                                    autoScroll: true
                                },
                        items: [
                            GetDTLTRHistoryGrid(), GetDTLTRLABGrid()
                                    //-------------- ## --------------
                        ],
                        listeners:
                                {
                                }
                    }

            );

    var FormDepanKasirLABPA = new Ext.Panel
            (
                    {
                        id: mod_id,
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        title: 'Kasir Laboratorium',
                        border: false,
                        shadhow: true,
                        iconCls: 'Request',
                        margins: '0 5 5 0',
                        items: [
                            {
                                xtype: 'panel',
                                layout: 'column',
                                plain: true,
                                activeTab: 0,
                                height: 160,
                                defaults:
                                        {
                                            bodyStyle: 'padding:10px',
                                            autoScroll: true
                                        },
                                items:
                                        [
                                            {
                                                columnWidth: .99,
                                                height: 150,
                                                layout: 'form',
                                                bodyStyle: 'padding:6px 6px 3px 3px',
                                                border: false,
                                                anchor: '100% 100%',
                                                items:
                                                        [
                                                            mComboUnitInduk(),
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: ' No. Medrec' + ' ',
                                                                id: 'txtFilterKasirNomedrecLab',
                                                                anchor: '35%',
                                                                onInit: function () { },
                                                                listeners:
                                                                        {
                                                                            'specialkey': function ()
                                                                            {
                                                                                var tmpNoMedrec = Ext.get('txtFilterKasirNomedrecLab').getValue()
                                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                                {
                                                                                    if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10) {
                                                                                        var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterKasirNomedrecLab').getValue())
                                                                                        Ext.getCmp('txtFilterKasirNomedrecLab').setValue(tmpgetNoMedrec);
                                                                                        var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                        RefreshDataFilterKasirLABPAKasir();
                                                                                    } else {
                                                                                        if (tmpNoMedrec.length === 10) {
                                                                                            RefreshDataFilterKasirLABPAKasir();
                                                                                        } else
                                                                                            Ext.getCmp('txtFilterKasirNomedrecLab').setValue('')
                                                                                    }
                                                                                }
                                                                            }

                                                                        }
                                                            },
                                                            {
                                                                xtype: 'tbspacer',
                                                                height: 3
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: ' Pasien' + ' ',
                                                                id: 'TxtFilterGridDataView_NAMA_viKasirLABPAKasir',
                                                                anchor: '50%',
                                                                enableKeyEvents: true,
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                RefreshDataFilterKasirLABPAKasir();
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                xtype: 'tbspacer',
                                                                height: 3
                                                            },
                                                            getItemPanelcombofilter(),
                                                            getItemPaneltgl_filter()
                                                        ]
                                            },
                                        ]
                            },
                            grListTRKasirLABPA, GDtabDetailLAB, LegendViewCMRequest],
                        tbar:
                                [
                                ],
                        listeners:
                                {
                                    'afterrender': function ()
                                    {
                                        loaddatastoreunitRadLab();
                                    }
                                }
                    }
            );

    return FormDepanKasirLABPA;

};
function showhide_unit(kode)
{
	if(kode==='05')
	{
	Ext.getCmp('txtTranferunitLABPA').hide();
	Ext.getCmp('txtTranferkelaskamar_LABPA').show();

	}else{
	Ext.getCmp('txtTranferunitLABPA').show();
	Ext.getCmp('txtTranferkelaskamar_LABPA').hide();
	}
}
function dataparam_viradlab()
{
    var paramsload_viradlab =
            {
                Table: 'ViewComboUnitRadLab'
            }
    return paramsload_viradlab;
}

var dsunitinduk_viKasirLABPAKasir;

function loaddatastoreunitRadLab()
{
    dsunitinduk_viKasirLABPAKasir.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_unit',
                                    Sortdir: 'ASC',
                                    target: 'ComboUnitRadLab',
                                    param: ""
                                }
                    }
            );
}

function TransferLookUp_lab(rowdata)
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_LabPa = new Ext.Window
            (
                    {
                        id: 'gridTransfer_Lab',
                        title: 'Transfer',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 410,
                        border: false,
                        resizable: false,
                        plain: false,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryTRTransfer_lab(lebar),
                        listeners:
                                {
                                }
                    }
            );

    FormLookUpsdetailTRTransfer_LabPa.show();
    //  Transferbaru();

}
;

function getFormEntryTRTransfer_lab(lebar)
{
    var pnlTRTransfer = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRTransfer',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 425,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputTransfer_LAB(lebar), getItemPanelButtonTransfer_lab(lebar)],
                        tbar:
                                [
                                ]
                    }
            );

    var FormDepanTransfer = new Ext.Panel
            (
                    {
                        id: 'FormDepanTransfer',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        items:
                                [
                                    pnlTRTransfer

                                ]

                    }
            );

    return FormDepanTransfer
}
;

function getItemPanelInputTransfer_LAB(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: false,
                height: 330,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                height: 330,
                                border: false,
                                items:
                                        [
                                            getTransfertujuan_lab(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 5
                                            },
                                            getItemPanelNoTransksiTransfer(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            mComboalasan_transfer()

                                        ]
                            },
                        ]
            };
    return items;
}
;

function mComboalasan_transfer()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_transfer = new WebApp.DataStore({fields: Field});
    dsalasan_transfer.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_alasan',
                                    Sortdir: 'ASC',
                                    target: 'ComboAlasanTransfer',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboalasan_transfer = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_transfer',
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Alasan Transfer ',
                        align: 'Right',
                        width: 100,
                        editable: false,
                        anchor: '100%',
                        store: dsalasan_transfer,
                        valueField: 'ALASAN',
                        displayField: 'ALASAN',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                    }

                                }
                    }
            );

    return cboalasan_transfer;
}
;

function getItemPanelNoTransksiTransfer(lebar)
{
    var items =
            {
                Width: lebar,
                height: 110,
                layout: 'column',
                border: true,
                items:
                        [
                            {
                                columnWidth: .990,
                                layout: 'form',
                                Width: lebar - 10,
                                labelWidth: 130,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah Biaya',
                                                maxLength: 200,
                                                name: 'txtjumlahbiayasal',
                                                id: 'txtjumlahbiayasal',
                                                width: 100,
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Paid',
                                                maxLength: 200,
                                                name: 'txtpaid',
                                                id: 'txtpaid',
                                                readOnly: true,
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                value: 0,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah dipindahkan',
                                                maxLength: 200,
                                                name: 'txtjumlahtranfer',
                                                id: 'txtjumlahtranfer',
                                                align: 'right',
                                                readOnly: true,
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Saldo tagihan',
                                                maxLength: 200,
                                                name: 'txtsaldotagihan',
                                                id: 'txtsaldotagihan',
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                value: 0,
                                                width: 100,
                                                anchor: '95%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getTransfertujuan_lab(lebar)
{
    var items =
            {
                Width: lebar - 2,
                height: 170,
                layout: 'form',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                labelWidth: 130,
                items:
                        [
                            {
                                xtype: 'tbspacer',
                                height: 2
                            },
                            mComboTransferTujuan(),
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Transaksi',
                                //maxLength: 200,
                                name: 'txtTranfernoTransaksiLABPA',
                                id: 'txtTranfernoTransaksiLABPA',
                                labelWidth: 130,
                                readonly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                id: 'dtpTanggaltransaksiRujuanLABPA',
                                name: 'dtpTanggaltransaksiRujuanLABPA',
                                format: 'd/M/Y',
                                readOnly: true,
                                //  value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Medrec',
                                //maxLength: 200,
                                name: 'txtTranfernomedrecLABPA',
                                id: 'txtTranfernomedrecLABPA',
                                labelWidth: 130,
                                readOnly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama Pasien',
                                //maxLength: 200,
                                name: 'txtTranfernamapasienLABPA',
                                id: 'txtTranfernamapasienLABPA',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Unit Perawatan',
                                //maxLength: 200,
                                name: 'txtTranferunitLABPA',
                                id: 'txtTranferunitLABPA',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
							{
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        name: 'txtTranferkelaskamar_LABPA',
                                        id: 'txtTranferkelaskamar_LABPA',
										readOnly : true,
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                            }
                        ]
            }
    return items;
}
;
function getItemPanelButtonTransfer_lab(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                height: 30,
                anchor: '100%',
                style: {'margin-top': '-1px'},
                items:
                        [
                            {
                                layout: 'hBox',
                                width: 400,
                                border: false,
                                bodyStyle: 'padding:5px 0px 5px 5px',
                                defaults: {margins: '3 3 3 3'},
                                anchor: '90%',
                                layoutConfig:
                                        {
                                            align: 'middle',
                                            pack: 'end'
                                        },
                                items:
                                        [
                                            {
                                                xtype: 'button',
                                                text: 'Simpan',
                                                width: 70,
                                                style: {'margin-left': '0px', 'margin-top': '0px'},
                                                hideLabel: true,
                                                id: 'btnOkTransfer_lab',
                                                handler: function ()
                                                {
												
                                                    TransferData_lab(false);
													 Ext.getCmp('btnSimpanKasirLABPA').disable();
													 Ext.getCmp('btnTransferKasirLABPA').disable();
													 RefreshDataKasirLABPADetail(Ext.getCmp('txtNoTransaksiKasirLABPAKasir').getValue());
													
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                text: 'Tutup',
                                                width: 70,
                                                hideLabel: true,
                                                id: 'btnCancelTransfer_lab',
                                                handler: function ()
                                                {
                                                    FormLookUpsdetailTRTransfer_LabPa.close();
                                                }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getParamTransferRwi_dari_lab()
{

/*
       KDkasirIGD:'01',
		TrKodeTranskasi: notransaksi_LABPA,
		KdUnit: kodeunit_LABPA,
		Kdpay: kdpaytransfer_LABPA,
		Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
		Tglasal:  ShowDate(tgltrans_LABPA),
		Shift: tampungshiftsekarang_LABPA,
		TRKdTransTujuan:Ext.get(txtTranfernoTransaksiRWJ).dom.value,
		KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRWJ).dom.value,
		TglTranasksitujuan : Ext.get(dtpTanggaltransaksiRujuanRWJ).dom.value,
		KDunittujuan : Trkdunit2,
		KDalasan :Ext.get(cboalasan_transfer).dom.value,
		KasirRWI:'05',
		Kdcustomer:kdcustomeraa_LABPA,
		kodeunitkamar:tmp_kodeunitkamar,
		kdspesial:tmp_kdspesial,
		nokamar:tmp_nokamar,


*/
    var params =
            {
                KDkasirIGD: kdkasir_LABPA,
                Kdcustomer: kdcustomeraa_LABPA,
                TrKodeTranskasi: notransaksi_LABPA,
                KdUnit: kodeunit_LABPA,
                Kdpay: kdpaytransfer_LABPA,
                Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
                Tglasal: ShowDate(tgltrans_LABPA),
                Shift: tampungshiftsekarang_LABPA,
                TRKdTransTujuan: Ext.get(txtTranfernoTransaksiLABPA).dom.value,
                KdpasienIGDtujuan: Ext.get(txtTranfernomedrecLABPA).dom.value,
                TglTranasksitujuan: Ext.get(dtpTanggaltransaksiRujuanLABPA).dom.value,
                KDunittujuan: Trkdunit2,
                KDalasan: Ext.get(cboalasan_transfer).dom.value,
                KasirRWI: kdkasirasal,
				kodeunitkamar:tmp_kodeunitkamar_LABPA,
				kdspesial:tmp_kdspesial_LABPA,
				nokamar:tmp_nokamar_LABPA,


            };
    return params
}
;
function TransferData_lab(mBol)
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/saveTransfer",
                        params: getParamTransferRwi_dari_lab(),
                        failure: function (o)
                        {

                            ShowPesanWarningKasirLABPA('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            RefreshDataFilterKasirLABPAKasir();
                        },
                        success: function (o)
                        {	
						
                            RefreshDataFilterKasirLABPAKasir();
                            
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {	Ext.getCmp('btnSimpanKasirLABPA').disable();
                                Ext.getCmp('btnTransferKasirLABPA').disable();
                                ShowPesanInfoKasirLABPA('Transfer Berhasil', 'transfer ');
                                FormLookUpsdetailTRTransfer_LabPa.close();
								
                            } else
                            {
                                ShowPesanWarningKasirLABPA('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            }
                            ;
                        }
                    }
            )

}
;


function RefreshDataKasirLABPADetail(no_transaksi)
{

    var strKriteriaLAB = '';
    strKriteriaLAB = "\"no_transaksi\" = ~" + no_transaksi + "~";

    dsTRDetailKasirLABPAList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailRWJGridBawah',
                                    param: strKriteriaLAB
                                }
                    }
            );
    return dsTRDetailKasirLABPAList;
}
;


function GetDTLTRLABGrid()
{
    var fldDetail = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI'];

    dsTRDetailKasirLABPAList = new WebApp.DataStore({fields: fldDetail})
    RefreshDataKasirLABPADetail();
    var gridDTLTRLAB = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Item Pemeriksaan',
                        stripeRows: true,
                        store: dsTRDetailKasirLABPAList,
                        border: true,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
                                                            cellSelected2deskripsii_LABPA = dsTRDetailKasirLABPAList.getAt(row);
                                                            CurrentPenataJasaKasirLABPA.row = row;
                                                            CurrentPenataJasaKasirLABPA.data = cellSelected2deskripsii_LABPA;
                                                            //FocusCtrlCMLAB='txtAset'; */
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRRawatJalanColumModel(),
                        tbar:
                                [
                                    {
                                        text: 'Tambah Produk',
                                        id: 'btnLookupKasirLABPA',
                                        tooltip: nmLookup,
                                        iconCls: 'find',
                                        disabled: true,
                                        hidden: true,
                                        handler: function ()
                                        {

                                            var p = RecordBaruLAB();
                                            var str = '';
                                            str = kodeunit_LABPA;
                                            FormLookupKasirLABPA(str, dsTRDetailKasirLABPAList, p, true, '', true);
                                        }
                                    },
                                    {
                                        text: 'Simpan',
                                        id: 'btnSimpanLABPA',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        disabled: true,
                                        hidden: true,
                                        handler: function ()
                                        {
                                            Datasave_KasirLABPA_2(false);
                                            Ext.getCmp('btnLookupKasirLABPA').disable();
                                            Ext.getCmp('btnSimpanLABPA').disable();
                                            Ext.getCmp('btnHpsBrsLABPA').disable();
											Ext.getCmp('btnTransferKasirLABPA').disable();



                                        }
                                    },
                                    {
                                        id: 'btnHpsBrsLABPA',
                                        text: 'Hapus Baris',
                                        tooltip: 'Hapus Baris',
                                        disabled: true,
                                        hidden: true,
                                        iconCls: 'RemoveRow',
                                        handler: function ()
                                        {
                                            if (dsTRDetailKasirLABPAList.getCount() > 0)
                                            {
                                                if (cellSelected2deskripsii_LABPA != undefined)
                                                {
                                                    if (CurrentPenataJasaKasirLABPA != undefined)
                                                    {
                                                        HapusBarisLAB();
                                                        Ext.getCmp('btnLookupKasirLABPA').disable();
                                                        Ext.getCmp('btnSimpanLABPA').disable();
                                                        Ext.getCmp('btnHpsBrsLABPA').disable();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningLAB('Pilih record ', 'Hapus data');
                                                }
                                            }
                                        }
                                    }

                                ],
                        viewConfig: {forceFit: true}
                    }


            );



    return gridDTLTRLAB;
}
;

function HapusBarisLAB()
{
    if (cellSelected2deskripsii_LABPA != undefined)
    {
        if (cellSelected2deskripsii_LABPA.data.DESKRIPSI2 != '' && cellSelected2deskripsii_LABPA.data.KD_PRODUK != '')
        {
            var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function (btn, combo) {
                if (btn == 'ok') {
                    variablehistori_LabPa = combo;
                    DataDeleteKasirLABPADetail();
                    dsTRDetailKasirLABPAList.removeAt(CurrentPenataJasaKasirLABPA.row);
                }
            });
        } else {
            //dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
            dsTRDetailKasirLABPAList.removeAt(CurrentPenataJasaKasirLABPA.row);
        }
        ;
    }

}
;

function DataDeleteKasirLABPADetail()
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/DeleteDataObj",
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: getParamDataDeleteKasirLABPADetail(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true) {
                                ShowPesanInfoKasirLABPA(nmPesanHapusSukses, nmHeaderHapusData);
                                dsTRDetailKasirLABPAList.removeAt(CurrentPenataJasaKasirLABPA.row);
                                cellSelecteddeskripsi_LABPA = undefined;
                                RefreshDataKasirLABPADetail(notransaksi_LABPA);
                                AddNewKasirLABPA = false;
                            } else if (cst.success === false && cst.pesan === 0) {
                                ShowPesanWarningKasirLABPA(nmPesanHapusGagal, nmHeaderHapusData);
                            } else {
                                ShowPesanWarningKasirLABPA(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirLABPADetail()
{

    var params =
            {
                Table: 'ViewTrKasirRwj',
                TrKodeTranskasi: CurrentPenataJasaKasirLABPA.data.data.NO_TRANSAKSI,
                TrTglTransaksi: CurrentPenataJasaKasirLABPA.data.data.TGL_TRANSAKSI,
                TrKdPasien: CurrentPenataJasaKasirLABPA.data.data.KD_PASIEN,
                TrKdNamaPasien: namapasien_LABPA,
                TrKdUnit: kodeunit_LABPA,
                TrNamaUnit: namaunit_LABPA,
                Uraian: CurrentPenataJasaKasirLABPA.data.data.DESKRIPSI2,
                AlasanHapus: variablehistori_LabPa,
                TrHarga: CurrentPenataJasaKasirLABPA.data.data.HARGA,
                TrKdProduk: CurrentPenataJasaKasirLABPA.data.data.KD_PRODUK,
                RowReq: CurrentPenataJasaKasirLABPA.data.data.URUT,
                Hapus: 2
            };

    return params
}
;


function Datasave_KasirLABPA_2(mBol)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/savedetailpenyakit",
                        params: getParamDetailTransaksiLAB_2(),
                        failure: function (o)
                        {
                            ShowPesanWarningLAB('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                            RefreshDataKasirLABPADetail(notransaksi_LABPA);
                        },
                        success: function (o)
                        {
                            RefreshDataKasirLABPADetail(notransaksi_LABPA);
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirLABPA(nmPesanSimpanSukses, nmHeaderSimpanData);
                                RefreshDataFilterKasirLABPA();
                                if (mBol === false)
                                {
                                    RefreshDataKasirLABPADetail(notransaksi_LABPA);
                                }
                                ;
                            } else
                            {
                                ShowPesanWarningKasirLABPA('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDetailTransaksiLAB_2()
{
    var params =
            {
                TrKodeTranskasi: notransaksi_LABPA,
                KdUnit: kodeunit_LABPA,
                //DeptId:Ext.get('txtKdDokter').dom.value ,tampungshiftsekarang_LABPA
                Tgl: tgltrans_LABPA,
                Shift: tampungshiftsekarang_LABPA,
                List: getArrDetailTrLAB(),
                JmlField: mRecordLAB.prototype.fields.length - 4,
                JmlList: GetListCountPenataDetailTransaksi(),
                Hapus: 1,
                Ubah: 0
            };
    return params
}
;
function getArrDetailTrLAB()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirLABPAList.getCount(); i++)
    {
        if (dsTRDetailKasirLABPAList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirLABPAList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirLABPAList.data.items[i].data.URUT
            y += z + dsTRDetailKasirLABPAList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirLABPAList.data.items[i].data.QTY
            y += z + ShowDate(dsTRDetailKasirLABPAList.data.items[i].data.TGL_BERLAKU)
            y += z + dsTRDetailKasirLABPAList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirLABPAList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirLABPAList.data.items[i].data.URUT


            if (i === (dsTRDetailKasirLABPAList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }

    return x;
}
;


function RecordBaruLAB()
{

    var p = new mRecordLAB
            (
                    {
                        'DESKRIPSI2': '',
                        'KD_PRODUK': '',
                        'DESKRIPSI': '',
                        'KD_TARIF': '',
                        'HARGA': '',
                        'QTY': '',
                        'TGL_TRANSAKSI': tanggaltransaksitampung_LABPA,
                        'DESC_REQ': '',
                        'KD_TARIF':'',
                                'URUT': ''
                    }
            );

    return p;
}
;
var mRecordLAB = Ext.data.Record.create
        (
                [
                    {name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
                    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
                    {name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
                    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
                    {name: 'HARGA', mapping: 'HARGA'},
                    {name: 'QTY', mapping: 'QTY'},
                    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
                    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
                    // {name: 'KD_TARIF', mapping:'KD_TARIF'},
                    {name: 'URUT', mapping: 'URUT'}
                ]
                );


function TRRawatJalanColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiLAB',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            // width:.30,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiLAB',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colHARGALAB',
                            header: 'Harga',
                            align: 'right',
                            hidden: true,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            //  width:.10,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colProblemLAB',
                            header: 'Qty',
                            width: '100%',
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolProblemLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 100,
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                Dataupdate_KasirLABPA(false);
                                                                //RefreshDataFilterKasirLABPA();
                                                                //RefreshDataFilterKasirLABPA();
                                                            }
                                                        }
                                            }
                                    ),
                        },
                        {
                            id: 'colImpactLAB',
                            header: 'CR',
                            // width:80,
                            dataIndex: 'IMPACT',
                            hidden: true,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolImpactLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30
                                            }
                                    )
                        }

                    ]
                    )
}
;


function getItemPaneltgl_filter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTglAwalFilterKasirLABPA',
                                                name: 'dtpTglAwalFilterKasirLABPA',
                                                format: 'd/M/Y',
                                                //readOnly : true,
                                                value: now,
                                                anchor: '96.7%',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                var tmpNoMedrec = Ext.get('txtFilterKasirNomedrecLab').getValue()
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterKasirLABPAKasir();
                                                                }
                                                            }
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 's/d ',
                                                id: 'dtpTglAkhirFilterKasirLABPA',
                                                name: 'dtpTglAkhirFilterKasirLABPA',
                                                format: 'd/M/Y',
                                                //readOnly : true,
                                                value: now,
                                                anchor: '100%',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                var tmpNoMedrec = Ext.get('txtFilterKasirNomedrecLab').getValue()
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterKasirLABPAKasir();
                                                                }
                                                            }
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getItemPanelcombofilter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboStatusBayar_viKasirLABPAKasir()
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                        [
                                            mComboUnit_viKasirLABPAKasir()
                                        ]
                            }
                        ]
            }
    return items;
}
;


function mComboStatusBayar_viKasirLABPAKasir()
{
    var cboStatus_viKasirLABPAKasir = new Ext.form.ComboBox
            (
                    {
                        id: 'cboStatus_viKasirLABPAKasir',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        fieldLabel: 'Status Lunas',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua'], [2, 'Lunas'], [3, 'Belum Lunas']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectCountStatusByr_viKasirLABPAKasir,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectCountStatusByr_viKasirLABPAKasir = b.data.displayText;
                                        //RefreshDataSetDokter();
                                        RefreshDataFilterKasirLABPAKasir();

                                    }
                                }
                    }
            );
    return cboStatus_viKasirLABPAKasir;
}
;





function mComboTransferTujuan()
{
    var cboTransferTujuan = new Ext.form.ComboBox
            (
                    {
                        id: 'cboTransferTujuan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        hidden: true,
                        fieldLabel: 'Transfer',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Gawat Darurat'], [2, 'Rawat Inap'], [3, 'Rawat Jalan']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: tranfertujuan_LabPa,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tranfertujuan_LabPa = b.data.displayText;
                                        //RefreshDataSetDokter();
                                        RefreshDataFilterKasirLABPAKasir();

                                    }
                                }
                    }
            );
    return cboTransferTujuan;
}
;

function mComboUnitInduk()
{
    var Field = ['kd_bagian', 'nama_unit'];

    dsunitinduk_viKasirLABPAKasir = new WebApp.DataStore({fields: Field});

    var cboUnitInduk = new Ext.form.ComboBox
            (
                    {
                        id: 'cboUnitInduk',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '22,7%',
                        emptyText: '',
                        fieldLabel: 'Pilih Poli',
                        store: dsunitinduk_viKasirLABPAKasir,
                        valueField: 'kd_bagian',
                        displayField: 'nama_unit',
                        hidden: true,
                        value: selectindukunitLABPAKasir,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectindukunitLABPAKasir = b.data.displayText;
                                        RefreshDataFilterKasirLABPAKasir();
                                    }
                                }
                    }
            );
    return cboUnitInduk;
}
;

function mComboUnit_viKasirLABPAKasir()
{
    var Field = ['KD_UNIT', 'NAMA_UNIT'];

    dsunit_viKasirLABPAKasir = new WebApp.DataStore({fields: Field});
    dsunit_viKasirLABPAKasir.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_unit',
                                    Sortdir: 'ASC',
                                    target: 'ComboUnit',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboUNIT_viKasirLABPAKasir = new Ext.form.ComboBox
            (
                    {
                        id: 'cboUNIT_viKasirLABPAKasir',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Poli ',
                        align: 'Right',
                        //width: 100,
                        anchor: '100%',
                        store: dsunit_viKasirLABPAKasir,
                        valueField: 'KD_UNIT',
                        displayField: 'NAMA_UNIT',
                        value: 'All',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        RefreshDataFilterKasirLABPAKasir();
                                    }
                                }
                    }
            );

    return cboUNIT_viKasirLABPAKasir;
}
;



function KasirLookUpLAB(rowdata)
{
    var lebar = 580;
    FormLookUpsdetailTRKasirLABPA = new Ext.Window
            (
                    {
                        id: 'gridKasirLABPA',
                        title: 'Kasir Laboratorium',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 500,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryKasirLABPA(lebar),
                        listeners:
                                {
                                }
                    }
            );

    FormLookUpsdetailTRKasirLABPA.show();
    if (rowdata == undefined)
    {
        KasirLABPAAddNew();
    } else
    {
        TRKasirLABPAInit(rowdata)
    }

}
;



function getFormEntryKasirLABPA(lebar)
{
    var pnlTRKasirLABPA = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirLABPA',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 130,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputKasir(lebar)],
                        tbar:
                                [
                                ]
                    }
            );
    var x;
    var paneltotal = new Ext.Panel
            (
                    {
                        id: 'paneltotal',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        height: 67,
                        anchor: '100% 8.0000%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        html: " Total :"
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirLABPA',
                                                        name: 'txtJumlah2EditData_viKasirLABPA',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        hidden: true,
                                                        html: " Bayar :"
                                                    },
                                                    {
                                                        xtype: 'numberfield',
                                                        id: 'txtJumlahEditData_viKasirLABPA',
                                                        name: 'txtJumlahEditData_viKasirLABPA',
                                                        hidden: true,
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        //readOnly: true,
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                ]
                    }
            )
    var GDtabDetailKasirLABPA = new Ext.Panel
            (
                    {
                        id: 'GDtabDetailKasirLABPA',
                        region: 'center',
                        activeTab: 0,
                        height: 350,
                        anchor: '100% 100%',
                        border: false,
                        plain: true,
                        defaults: {autoScroll: true},
                        items: [GetDTLTRKasirLABPAGrid(), paneltotal],
                        tbar:
                                [
                                    {
                                        text: 'Bayar',
                                        id: 'btnSimpanKasirLABPA',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        handler: function ()
                                        {
                                            Datasave_KasirLABPAKasir(false);
                                           // FormDepan2KasirLABPA.close();
                                            Ext.getCmp('btnSimpanKasirLABPA').disable();
                                            Ext.getCmp('btnTransferKasirLABPA').disable();
                                            //Ext.getCmp('btnHpsBrsKasirLABPA').disable();
                                        }
                                    },
									
                                    {
                                        id: 'btnTransferKasirLABPA',
                                        text: 'Transfer',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                        /* disabled: true, */
                                        handler: function (sm, row, rec)
                                        {
                                            TransferLookUp_lab();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/GetPasienTranferLab_rad",
                                                params: {
                                                    notr: notransaksiasal,
                                                    kdkasir: kdkasirasal
                                                },
                                                success: function (o)
                                                {	
												showhide_unit(kdkasirasal);
                                                    if (o.responseText == "") {
                                                        ShowPesanWarningKasirLABPA('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                    } else {
                                                        var tmphasil = o.responseText;
                                                        var tmp = tmphasil.split("<>");
                                                        if (tmp[5] == undefined && tmp[3] == undefined) {
                                                            ShowPesanWarningKasirLABPA('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                            FormLookUpsdetailTRTransfer_LabPa.close();
                                                        } else {
                                                            Ext.get(txtTranfernoTransaksiLABPA).dom.value = tmp[3];
                                                            Ext.get(dtpTanggaltransaksiRujuanLABPA).dom.value = ShowDate(tmp[4]);
                                                            Ext.get(txtTranfernomedrecLABPA).dom.value = tmp[5];
                                                            Ext.get(txtTranfernamapasienLABPA).dom.value = tmp[2];
                                                            Ext.get(txtTranferunitLABPA).dom.value = tmp[1];
                                                            Trkdunit2 = tmp[6];
															Ext.get(txtTranferkelaskamar_LABPA).dom.value=tmp[9];
															
															tmp_kodeunitkamar_LABPA=tmp[8];
															tmp_kdspesial_LABPA=tmp[10];
															tmp_nokamar_LABPA=tmp[11];
                                                            var kasir = tmp[7];
                                                            Ext.Ajax.request({
                                                                url: baseURL + "index.php/main/GettotalTranfer",
                                                                params: {
                                                                    notr: notransaksi_LABPA,
																	kd_kasir:kdkasir_LABPA
                                                                },
                                                                success: function (o)
                                                                {
                                                                    Ext.get(txtjumlahbiayasal).dom.value = formatCurrency(o.responseText);
                                                                    Ext.get(txtjumlahtranfer).dom.value = formatCurrency(o.responseText);
                                                                }
                                                            });
                                                        }

                                                    }
                                                }
                                            });

                                        }, //disabled :true
                                    },
                                    {
                                        xtype: 'splitbutton',
                                        text: 'Cetak',
                                        iconCls: 'print',
                                        id: 'btnPrint_viDaftar',
                                        handler: function ()
                                        {
                                        },
                                        menu: new Ext.menu.Menu({
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
                                                            id: 'btnPrintBillRadLab',
                                                            handler: function ()
                                                            {
                                                                printbillRadLab();
                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Kwitansi',
                                                            id: 'btnPrintKwitansiRadLab',
                                                            handler: function ()
                                                            {
                                                                printkwitansiRadLab();
                                                            }
                                                        }
                                                    ]
                                        })
                                    }
                                ]
                    }
            );



    var pnlTRKasirLABPA2 = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirLABPA2',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 380,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [GDtabDetailKasirLABPA,
                        ]
                    }
            );




    FormDepan2KasirLABPA = new Ext.Panel
            (
                    {
                        id: 'FormDepan2KasirLABPA',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                height: 380,
                        shadhow: true,
                        items: [pnlTRKasirLABPA, pnlTRKasirLABPA2,
                        ]
                    }
            );

    return FormDepan2KasirLABPA
}
;






function TambahBarisKasirLABPA()
{
    var x = true;

    if (x === true)
    {
        var p = RecordBaruKasirLABPA();
        dsTRDetailKasirLABPAKasirList.insert(dsTRDetailKasirLABPAKasirList.getCount(), p);
    }
    ;
}
;





function GetDTLTRHistoryGrid()
{

    var fldDetail = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'SHIFT', 'KD_USER'];

    dsTRDetailHistoryList_LABPA = new WebApp.DataStore({fields: fldDetail})

    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'History Bayar',
                        stripeRows: true,
                        store: dsTRDetailHistoryList_LABPA,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100% 25%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
                                                            cellSelecteddeskripsi_LABPA = dsTRDetailHistoryList_LABPA.getAt(row);
                                                            CurrentHistory.row = row;
                                                            CurrentHistory.data = cellSelecteddeskripsi_LABPA;
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRHistoryColumModel(),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnHpsBrsKasirLABPA',
                                        text: 'Hapus Pembayaran',
                                        tooltip: 'Hapus Baris',
                                        iconCls: 'RemoveRow',
                                        //hidden :true,
                                        handler: function ()
                                        {
                                            if (dsTRDetailHistoryList_LABPA.getCount() > 0)
                                            {
                                                if (cellSelecteddeskripsi_LABPA != undefined)
                                                {
                                                    if (CurrentHistory != undefined)
                                                    {
                                                        HapusBarisDetailbayar();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningKasirLABPA('Pilih record ', 'Hapus data');
                                                }
                                            }
                                            Ext.getCmp('btnEditKasirLABPA').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirLABPA').disable();
                                            Ext.getCmp('btnHpsBrsKasirLABPA').disable();
                                        },
                                        disabled: true
                                    }
                                ]
                    }

            );

    return gridDTLTRHistory;
}
;

function TRHistoryColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Bayar',
                            dataIndex: 'URUT',
                            //hidden:true

                        },
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        },
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }
                        },
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colStatHistory',
                            header: 'History',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colPetugasHistory',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME',
                            //hidden:true
                        }
                    ]
                    )
}
;
function HapusBarisDetailbayar()
{
    if (cellSelecteddeskripsi_LABPA != undefined)
    {
        if (cellSelecteddeskripsi_LABPA.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi_LABPA.data.URUT != '')
        {
            //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
            /*var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
             if (btn == 'ok')
             {
             variablehistori_LabPa=combo;
             if (variablehistori_LabPa!='')
             {
             DataDeleteKasirLABPAKasirDetail();
             }
             else
             {
             ShowPesanWarningKasirLABPA('Silahkan isi alasan terlebih dahaulu','Keterangan');
             }
             }	
             });  */
            msg_box_alasanhapus_LABPA();
        } else
        {
            dsTRDetailHistoryList_LABPA.removeAt(CurrentHistory.row);
        }
        ;
    }
}
;
function msg_box_alasanhapus_LABPA()
{
    var lebar = 250;
    form_msg_box_alasanhapus_LAB = new Ext.Window
            (
                    {
                        id: 'alasan_hapusLAb',
                        title: 'Alasan Hapus Pembayaran',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'hapus',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        DataDeleteKasirLABPAKasirDetail();
                                                                        form_msg_box_alasanhapus_LAB.close();
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanhapus_LAB.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanhapus_LAB.show();
}
;

function mComboalasan_hapusLAb()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_hapusLAb = new WebApp.DataStore({fields: Field});

    dsalasan_hapusLAb.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'alasanhapus',
                                    param: ''
                                }
                    }
            );
    var cboalasan_hapusLAb = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_hapusLAb',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Alasan...',
                        labelWidth: 1,
                        store: dsalasan_hapusLAb,
                        valueField: 'KD_ALASAN',
                        displayField: 'ALASAN',
                        anchor: '95%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
//                                  var selectalasan_hapusLAb = b.data.KD_PROPINSI;
                                        //alert("is");
                                        selectDokter = b.data.KD_DOKTER;
                                    },
                                    'render': function (c)
                                    {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('kelPasien').focus();
                                        }, c);
                                    }
                                }
                    }
            );

    return cboalasan_hapusLAb;
}
;
function DataDeleteKasirLABPAKasirDetail()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/deletedetail_bayar",
                        params: getParamDataDeleteKasirLABPAKasirDetail(),
                        success: function (o)
                        {
                            //	RefreshDatahistoribayar(Kdtransaksi_LABPA);
                            RefreshDataFilterKasirLABPAKasir();
                            RefreshDatahistoribayar('0');
                            RefreshDataKasirLABPADetail('0');
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirLABPA(nmPesanHapusSukses, nmHeaderHapusData);
                                //alert(Kdtransaksi_LABPA);					 

                                //refeshKasirLABPAKasir();
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningKasirLABPA(nmPesanHapusGagal, nmHeaderHapusData);
                            } else
                            {
                                ShowPesanWarningKasirLABPA(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirLABPAKasirDetail()
{
    var params =
            {
                TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
                TrTglbayar: CurrentHistory.data.data.TGL_BAYAR,
                Urut: CurrentHistory.data.data.URUT,
                Jumlah: CurrentHistory.data.data.BAYAR,
                Username: CurrentHistory.data.data.USERNAME,
                Shift: tampungshiftsekarang_LABPA,
                Shift_hapus: CurrentHistory.data.data.SHIFT,
                Kd_user: CurrentHistory.data.data.KD_USER,
                Tgltransaksi: tgltrans_LABPA,
                Kodepasein: kodepasien_LABPA,
                NamaPasien: namapasien_LABPA,
                KodeUnit: kodeunit_LABPA,
                Namaunit: namaunit_LABPA,
                Kodepay: kodepay_LABPA,
                Uraian: CurrentHistory.data.data.DESKRIPSI,
                KDkasir: kdkasir_LABPA,
                KeTterangan: Ext.get('cboalasan_hapusLAb').dom.value

            };
    Kdtransaksi_LABPA = CurrentHistory.data.data.NO_TRANSAKSI;
    return params
}
;



function GetDTLTRKasirLABPAGrid()
{
    var fldDetail = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'BAYARTR', 'DISCOUNT', 'PIUTANG'];

    dsTRDetailKasirLABPAKasirList = new WebApp.DataStore({fields: fldDetail})
    RefreshDataKasirLABPAKasirDetail();
    gridDTLTRKasirLABPA = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Detail Bayar',
                        stripeRows: true,
                        id: 'gridDTLTRKasirLABPA',
                        store: dsTRDetailKasirLABPAKasirList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        height: 230,
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        cm: TRKasirRawatJalanColumModel()
                    }
            );

    return gridDTLTRKasirLABPA;
}
;

function TRKasirRawatJalanColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiKasirLABPA',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 250,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiKasirLABPA',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            id: 'colURUTKasirLABPA',
                            header: 'Urut',
                            dataIndex: 'URUT',
                            sortable: false,
                            hidden: true,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            hidden: true,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colQtyKasirLABPA',
                            header: 'Qty',
                            width: 91,
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                        },
                        {
                            id: 'colHARGAKasirLABPA',
                            header: 'Harga',
                            align: 'right',
                            hidden: false,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colPiutangKasirLABPA',
                            header: 'Puitang',
                            width: 80,
                            dataIndex: 'PIUTANG',
                            align: 'right',
                            //hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolPuitangLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                //getTotalDetailProduk();
                                return formatCurrency(record.data.PIUTANG);
                            }
                        },
                        {
                            id: 'colTunaiKasirLABPA',
                            header: 'Tunai',
                            width: 80,
                            dataIndex: 'BAYARTR',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolTunaiLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProduk();

                                return formatCurrency(record.data.BAYARTR);
                            }

                        },
                        {
                            id: 'colDiscountKasirLABPA',
                            header: 'Discount',
                            width: 80,
                            dataIndex: 'DISCOUNT',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolDiscountLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.DISCOUNT);
                            }
                        }

                    ]
                    )
}
;




function RecordBaruKasirLABPA()
{

    var p = new mRecordKasirLABPA
            (
                    {
                        'DESKRIPSI2': '',
                        'KD_PRODUK': '',
                        'DESKRIPSI': '',
                        'KD_TARIF': '',
                        'HARGA': '',
                        'QTY': '',
                        'TGL_TRANSAKSI': tanggaltransaksitampung_LABPA,
                        'DESC_REQ': '',
                        'KD_TARIF':'',
                                'URUT': ''
                    }
            );

    return p;
}
;
function TRKasirLABPAInit(rowdata)
{
    AddNewKasirLABPAKasir = false;
	notransaksi_LABPA=rowdata.NO_TRANSAKSI;
    vkd_unit_LABPA = rowdata.KD_UNIT;
    RefreshDataKasirLABPAKasirDetail(rowdata.NO_TRANSAKSI);
    Ext.get('txtNoTransaksiKasirLABPAKasir').dom.value = rowdata.NO_TRANSAKSI;
    tanggaltransaksitampung_LABPA = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
    Ext.get('txtNoMedrecDetransaksi').dom.value = rowdata.KD_PASIEN;
    Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
    Ext.get('cboPembayaran').dom.value = rowdata.KET_PAYMENT;
    Ext.get('cboJenisByr').dom.value = rowdata.CARA_BAYAR; // take the displayField value 
    loaddatastorePembayaran(rowdata.JENIS_PAY);
    vkode_customer_LabPa = rowdata.KD_CUSTOMER;
    tampungtypedata_LABPA = 0;
    tapungkd_pay_LABPA = rowdata.KD_PAY;
    jenispay_LabPa = 1;
    var vkode_customer_LabPa = rowdata.LUNAS
	kdkasirasal = rowdata.KD_KASIR_ASAL;
    notransaksiasal = rowdata.NO_TRANSAKSI_ASAL;
    showCols(Ext.getCmp('gridDTLTRKasirLABPA'));
    hideCols(Ext.getCmp('gridDTLTRKasirLABPA'));
    vflag_LabPa = rowdata.FLAG;
    //(rowdata.NO_TRANSAKSI;


    Ext.Ajax.request({
        url: baseURL + "index.php/main/getcurrentshift",
        params: {
            command: '0',
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {

            tampungshiftsekarang_LABPA = o.responseText
        }

    });



}
;

function mEnabledKasirLABPACM(mBol)
{
    Ext.get('btnLookupKasirLABPA').dom.disabled = mBol;
    Ext.get('btnHpsBrsKasirLABPA').dom.disabled = mBol;
}
;


///---------------------------------------------------------------------------------------///
function KasirLABPAAddNew()
{
    AddNewKasirLABPAKasir = true;
    Ext.get('txtNoTransaksiKasirLABPAKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi_LABPA.format('d/M/Y');
    Ext.get('txtNoMedrecDetransaksi').dom.value = '';
    Ext.get('txtNamaPasienDetransaksi').dom.value = '';

    //Ext.get('txtKdUrutMasuk').dom.value = '';
    Ext.get('cboStatus_viKasirLABPAKasir').dom.value = ''
    rowSelectedKasirLABPAKasir = undefined;
    dsTRDetailKasirLABPAKasirList.removeAll();
    mEnabledKasirLABPACM(false);


}
;

function RefreshDataKasirLABPAKasirDetail(no_transaksi)
{
    var strKriteriaKasirLABPA = '';

    strKriteriaKasirLABPA = 'no_transaksi = ~' + no_transaksi + '~';

    dsTRDetailKasirLABPAKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailbayar',
                                    param: strKriteriaKasirLABPA
                                }
                    }
            );
    return dsTRDetailKasirLABPAKasirList;
}
;

function RefreshDatahistoribayar(no_transaksi)
{
    var strKriteriaKasirLABPA = '';

    strKriteriaKasirLABPA = 'no_transaksi= ~' + no_transaksi + '~';

    dsTRDetailHistoryList_LABPA.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewHistoryBayar',
                                    param: strKriteriaKasirLABPA
                                }
                    }
            );
    return dsTRDetailHistoryList_LABPA;
}
;


//---------------------------------------------------------------------------------------///

function GetListCountPenataDetailTransaksi()
{

    var x = 0;
    for (var i = 0; i < dsTRDetailKasirLABPAList.getCount(); i++)
    {
        if (dsTRDetailKasirLABPAList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirLABPAList.data.items[i].data.DESKRIPSI != '')
        {
            x += 1;
        }
        ;
    }
    return x;

}
;

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiKasirLABPA()
{
    if (tampungtypedata_LABPA === '')
    {
        tampungtypedata_LABPA = 0;
    }
    ;

    var params =
            {
                kdUnit: vkd_unit_LABPA,
                TrKodeTranskasi: Ext.get('txtNoTransaksiKasirLABPAKasir').getValue(),
                Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
                Shift: tampungshiftsekarang_LABPA,
                kdKasir: kdkasir_LABPA,
                bayar: tapungkd_pay_LABPA,
                Flag: vflag_LabPa,
                Typedata: tampungtypedata_LABPA,
                Totalbayar: getTotalDetailProduk(),
                List: getArrDetailTrKasirLABPA(),
                JmlField: mRecordKasirLABPA.prototype.fields.length - 4,
                JmlList: GetListCountDetailTransaksi(),
                Hapus: 1,
                Ubah: 0
            };
    return params
}
;

function paramUpdateTransaksi()
{
    var params =
            {
                TrKodeTranskasi: cellSelectedtutup_LABPA,
                KDkasir: kdkasir_LABPA
            };
    return params
}
;

function GetListCountDetailTransaksi()
{

    var x = 0;
    for (var i = 0; i < dsTRDetailKasirLABPAKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirLABPAKasirList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirLABPAKasirList.data.items[i].data.DESKRIPSI != '')
        {
            x += 1;
        }
        ;
    }
    return x;

}
;

function getTotalDetailProduk()
{
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < dsTRDetailKasirLABPAKasirList.getCount(); i++)
    {

        var recordterakhir;

        //alert(TotalProduk);
        if (tampungtypedata_LABPA == 0)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABPAKasirList.data.items[i].data.BAYARTR);

            //recordterakhir= tampunggrid
            //TotalProduk.toString().replace(/./gi, "");
            TotalProduk += tampunggrid

            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLABPA').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata_LABPA == 3)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABPAKasirList.data.items[i].data.PIUTANG);
            //TotalProduk.toString().replace(/./gi, "");
            //recordterakhir=tampunggrid
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLABPA').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata_LABPA == 1)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABPAKasirList.data.items[i].data.DISCOUNT);

            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLABPA').dom.value = formatCurrency(recordterakhir);
        }
    }
    bayar = Ext.get('txtJumlah2EditData_viKasirLABPA').getValue();
    return bayar;
}
;







function getArrDetailTrKasirLABPA()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirLABPAKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirLABPAKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirLABPAKasirList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirLABPAKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.QTY
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.BAYARTR
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.PIUTANG
            y += z + dsTRDetailKasirLABPAKasirList.data.items[i].data.DISCOUNT



            if (i === (dsTRDetailKasirLABPAKasirList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }

    return x;
}
;


function getItemPanelInputKasir(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: true,
                height: 110,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getItemPanelNoTransksiLABKasir(lebar),
                                            getItemPanelmedreckasir(lebar), getItemPanelUnitKasir(lebar)
                                        ]
                            }
                        ]
            };
    return items;
}
;



function getItemPanelUnitKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboJenisByrView()
                                        ]
                            }, {
                                columnWidth: .60,
                                layout: 'form',
                                labelWidth: 0.9,
                                border: false,
                                items:
                                        [
                                            mComboPembayaran()
                                        ]
                            }
                        ]
            }
    return items;
}
;



function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar_LabPa.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'nama',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboBayar',
                                    param: 'jenis_pay=~' + jenis_pay + '~'
                                }
                    }
            )
}

function mComboPembayaran()
{
    var Field = ['KD_PAY', 'JENIS_PAY', 'PAYMENT'];

    dsComboBayar_LabPa = new WebApp.DataStore({fields: Field});

    var cboPembayaran = new Ext.form.ComboBox
            (
                    {
                        id: 'cboPembayaran',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Pembayaran...',
                        labelWidth: 80,
                        align: 'Right',
                        store: dsComboBayar_LabPa,
                        valueField: 'KD_PAY',
                        displayField: 'PAYMENT',
                        anchor: '100%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tapungkd_pay_LABPA = b.data.KD_PAY;
                                    },
                                }
                    }
            );

    return cboPembayaran;
}
;


function mComboJenisByrView()
{
    var Field = ['JENIS_PAY', 'DESKRIPSI', 'TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({fields: Field});
    dsJenisbyrView.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'jenis_pay',
                                    Sortdir: 'ASC',
                                    target: 'ComboJenis',
                                }
                    }
            );

    var cboJenisByr = new Ext.form.ComboBox
            (
                    {
                        id: 'cboJenisByr',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Pembayaran      ',
                        align: 'Right',
                        anchor: '100%',
                        store: dsJenisbyrView,
                        valueField: 'JENIS_PAY',
                        displayField: 'DESKRIPSI',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        loaddatastorePembayaran(b.data.JENIS_PAY);
                                        tampungtypedata_LABPA = b.data.TYPE_DATA;
                                        jenispay_LabPa = b.data.JENIS_PAY;
                                        showCols(Ext.getCmp('gridDTLTRKasirLABPA'));
                                        hideCols(Ext.getCmp('gridDTLTRKasirLABPA'));
                                        getTotalDetailProduk();
                                        Ext.get('cboPembayaran').dom.value = 'Pilih Pembayaran...';
                                    },
                                }
                    }
            );

    return cboJenisByr;
}
;
function hideCols(grid)
{
    if (tampungtypedata_LABPA == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata_LABPA == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata_LABPA == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);

    }
}
;
function showCols(grid) {
    if (tampungtypedata_LABPA == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);

    } else if (tampungtypedata_LABPA == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
    } else if (tampungtypedata_LABPA == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
    }

}
;



function getItemPanelDokter(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Dokter  ',
                                                name: 'txtKdDokter',
                                                id: 'txtKdDokter',
                                                readOnly: true,
                                                anchor: '99%'
                                            }, {
                                                xtype: 'textfield',
                                                fieldLabel: 'Kelompok Pasien',
                                                readOnly: true,
                                                name: 'txtCustomer',
                                                id: 'txtCustomer',
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .600,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                name: 'txtNamaDokter',
                                                id: 'txtNamaDokter',
                                                readOnly: true,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'txtKdUrutMasuk',
                                                id: 'txtKdUrutMasuk',
                                                readOnly: true,
                                                hidden: true,
                                                anchor: '100%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getItemPanelNoTransksiLABKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Transaksi ',
                                                name: 'txtNoTransaksiKasirLABPAKasir',
                                                id: 'txtNoTransaksiKasirLABPAKasir',
                                                emptyText: nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%'
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 55,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTanggalDetransaksi',
                                                name: 'dtpTanggalDetransaksi',
                                                format: 'd/M/Y',
                                                readOnly: true,
                                                value: now,
                                                anchor: '100%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function getItemPanelmedreckasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Medrec ',
                                                name: 'txtNoMedrecDetransaksi',
                                                id: 'txtNoMedrecDetransaksi',
                                                readOnly: true,
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: '',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtNamaPasienDetransaksi',
                                                id: 'txtNamaPasienDetransaksi',
                                                anchor: '100%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function RefreshDataKasirLABPAKasir()
{
    dsTRKasirLABPAKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountKasirLABPAKasir,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewKasirLABPA',
                                    param: ''
                                }
                    }
            );

    rowSelectedKasirLABPAKasir = undefined;
    return dsTRKasirLABPAKasirList;
}
;

function refeshKasirLABPAKasir()
{
    dsTRKasirLABPAKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountKasirLABPAKasir,
                                    //Sort: 'no_transaksi',
                                    Sort: '',
                                    //Sort: 'no_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewKasirLABPA',
                                    param: ''
                                }
                    }
            );
    return dsTRKasirLABPAKasirList;
}

function RefreshDataFilterKasirLABPAKasir()
{

    var KataKunci = '';

    if (Ext.getCmp('txtFilterKasirNomedrecLab').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = '   LOWER(kode_pasien) like  LOWER( ~' + Ext.getCmp('txtFilterKasirNomedrecLab').getValue() + '%~)';
        } else
        {
            KataKunci += ' and  LOWER(kode_pasien) like  LOWER( ~' + Ext.getCmp('txtFilterKasirNomedrecLab').getValue() + '%~)';
        }
        ;

    }
    ;
    if (KataKunci == '')
    {
        //alert ('');
        KataKunci = '   kd_bagian = 8';
    } else
    {
        KataKunci += ' and  kd_bagian = 8';
    }
    ;

    if (Ext.getCmp('TxtFilterGridDataView_NAMA_viKasirLABPAKasir').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = '  LOWER(nama) like  LOWER( ~' + Ext.getCmp('TxtFilterGridDataView_NAMA_viKasirLABPAKasir').getValue() + '%~)';
        } else
        {
            KataKunci += ' and  LOWER(nama) like  LOWER( ~' + Ext.getCmp('TxtFilterGridDataView_NAMA_viKasirLABPAKasir').getValue() + '%~)';
        }
        ;

    }
    ;


    if (Ext.getCmp('cboUNIT_viKasirLABPAKasir').getValue() != '' && Ext.getCmp('cboUNIT_viKasirLABPAKasir').getValue() != 'All' && Ext.getCmp('cboUNIT_viKasirLABPAKasir').getValue() != 'Semua')
    {
        if (KataKunci == '')
        {
            KataKunci = '  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirLABPAKasir').getValue() + '%~)';
        } else
        {
            KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirLABPAKasir').getValue() + '%~)';
        }
        ;
    }
    ;


    if (Ext.get('cboStatus_viKasirLABPAKasir').getValue() == 'Lunas' || Ext.getCmp('cboStatus_viKasirLABPAKasir').getValue() == 2)
    {
        if (KataKunci == '')
        {
            KataKunci = '   lunas = ~true~';
        } else
        {
            KataKunci += ' and lunas =  ~true~';
        }
        ;

    }
    ;


    if (Ext.get('cboStatus_viKasirLABPAKasir').getValue() == 'Semua' || Ext.getCmp('cboStatus_viKasirLABPAKasir').getValue() == 1)
    {
    }
    ;
    if (Ext.get('cboStatus_viKasirLABPAKasir').getValue() == 'Belum Lunas' || Ext.getCmp('cboStatus_viKasirLABPAKasir').getValue() == 3)
    {
        if (KataKunci == '')
        {

            KataKunci = '   lunas = ~false~';
        } else
        {

            KataKunci += ' and lunas =  ~false~';
        }
        ;


    }
    ;
    if (Ext.get('dtpTglAwalFilterKasirLABPA').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = " (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirLABPA').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirLABPA').getValue() + "~)";
        } else
        {
            KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirLABPA').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirLABPA').getValue() + "~)";
        }
        ;

    }
    ;

    if (KataKunci != undefined || KataKunci != '')
    {
        dsTRKasirLABPAKasirList.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCountKasirLABPAKasir,
                                        //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                                        //Sort: 'no_transaksi',
                                        Sortdir: 'ASC',
                                        target: 'ViewKasirLABPA',
                                        param: KataKunci
                                    }
                        }
                );
    } else
    {

        dsTRKasirLABPAKasirList.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCountKasirLABPAKasir,
                                        //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                                        //Sort: 'no_transaksi',
                                        Sortdir: 'ASC',
                                        target: 'ViewKasirLABPA',
                                        param: ''
                                    }
                        }
                );
    }
    ;

    return dsTRKasirLABPAKasirList;
}
;



function Datasave_KasirLABPAKasir(mBol)
{
    if (ValidasiEntryCMKasirLABPA(nmHeaderSimpanData, false) == 1)
    {
        Ext.Ajax.request
                (
                        {
                            //url: "./Datapool.mvc/CreateDataObj",
                            url: baseURL + "index.php/main/functionKasirPenunjang/savepembayaran",
                            params: getParamDetailTransaksiKasirLABPA(),
                            failure: function (o)
                            {
                                ShowPesanWarningKasirLABPA('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                RefreshDataFilterKasirLABPAKasir();
                            },
                            success: function (o)
                            {
                                RefreshDatahistoribayar('0');
                                RefreshDataKasirLABPADetail('0');
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfoKasirLABPA(nmPesanSimpanSukses, nmHeaderSimpanData);
                                    RefreshDataKasirLABPAKasirDetail(Ext.get('txtNoTransaksiKasirLABPAKasir').dom.value);
                                    //RefreshDataKasirLABPAKasir();
                                    if (mBol === false)
                                    {
                                        RefreshDataFilterKasirLABPAKasir();
                                    }
                                    ;
                                } else
                                {
                                    ShowPesanWarningKasirLABPA('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                }
                                ;
                            }
                        }
                )

    } else
    {
        if (mBol === true)
        {
            return false;
        }
        ;
    }
    ;

}
;
function UpdateTutuptransaksi(mBol)
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/ubah_co_status_transksi",
                        params: paramUpdateTransaksi(),
                        failure: function (o)
                        {
                            ShowPesanWarningKasirLABPA('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                            RefreshDataFilterKasirLABPAKasir();
                        },
                        success: function (o)
                        {
                            //RefreshDatahistoribayar(Ext.get('cellSelectedtutup_LABPA);

                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirLABPA(nmPesanSimpanSukses, nmHeaderSimpanData);

                                //RefreshDataKasirLABPAKasir();
                                if (mBol === false)
                                {
                                    RefreshDataFilterKasirLABPAKasir();
                                }
                                ;
                                cellSelectedtutup_LABPA = '';
                            } else
                            {
                                ShowPesanWarningKasirLABPA('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                                cellSelectedtutup_LABPA = '';
                            }
                            ;
                        }
                    }
            )
}
;

function ValidasiEntryCMKasirLABPA(modul, mBolHapus)
{
    var x = 1;

    if ((Ext.get('txtNoTransaksiKasirLABPAKasir').getValue() == '')

            || (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
            || (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
            || (Ext.get('dtpTanggalDetransaksi').getValue() == '')
            || dsTRDetailKasirLABPAKasirList.getCount() === 0
            || (Ext.get('cboPembayaran').getValue() == '')
            || (Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...'))
    {
        if (Ext.get('txtNoTransaksiKasirLABPAKasir').getValue() == '' && mBolHapus === true)
        {
            x = 0;
        } else if (Ext.get('cboPembayaran').getValue() == '' || Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...')
        {
            ShowPesanWarningKasirLABPA(('Data pembayaran tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirLABPA(('Data no. medrec tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirLABPA('Nama pasien belum terisi', modul);
            x = 0;
        } else if (Ext.get('dtpTanggalDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirLABPA(('Data tanggal kunjungan tidak boleh kosong'), modul);
            x = 0;
        }
        //cboPembayaran

        else if (dsTRDetailKasirLABPAKasirList.getCount() === 0)
        {
            ShowPesanWarningKasirLABPA('Data dalam tabel kosong', modul);
            x = 0;
        }
        ;
    }
    ;
    return x;
}
;




function ShowPesanWarningKasirLABPA(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanErrorKasirLABPA(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoKasirLABPA(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function printbillRadLab()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakbill_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarning_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            } else
                            {
                                ShowPesanError_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            }
                        }
                    }
            );
}
;

var tmpkdkasirRadLab = '1';
function dataparamcetakbill_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectPrintingRadLab',
                No_TRans: Ext.get('txtNoTransaksiKasirLABPAKasir').getValue(),
                KdKasir: tmpkdkasirRadLab,
                modul: 'lab'
            };
    return paramscetakbill_vikasirDaftarRadLab;
}
;

function printkwitansiRadLab()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakkwitansi_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan !== '')
                            {
                                ShowPesanWarningKasirrwj('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanWarningKasirrwj('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            );
}
;

function dataparamcetakkwitansi_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectKwitansiLab',
                No_TRans: Ext.get('txtNoTransaksiKasirLABPAKasir').getValue(),
            };
    return paramscetakbill_vikasirDaftarRadLab;
}
;

