var CurrentHasilLabAnatomi =
{
    data: Object,
    details: Array,
    row: 0
};


var now = new Date();

var dsTRListHasilLabAnatomi;
var dsDiagnosaLabAnatomi;
var AddNewHasilLabAnatomi = true;
var selectCountHasilLabAnatomi = 50;
var tigaharilalu_LabAnatomi = new Date().add(Date.DAY, -3);

var tmpkriteria_LabAnatomi;
var rowSelectedHasilLabAnatomi;
var cellSelecteddeskripsi_LabAnatomi;
var FormLookUpsdetailHasilLabAnatomi;

var tmpnama_unit_asal_labanatomi;
var tmpkd_dokter_asal_labanatomi;
var tmpjam_masuk_labanatomi;
var tmpnama_dokter_asal_labanatomi;
var tmpno_pa_labanatomi;
var tmptgl_hasil_labanatomi;
var grListPasienHasilLabAnatomi;
var tmpkd_pasien;
var tmpnama;
var tmptglAwal;
var tmptglAkhir;
var gridDTHasilLabAnatomi;

var Kepuasan='';
var Normal='';
var AtipiaSquoenosa='';
var Jinak='';
var Radang='';
var Inflamasia='';
var Metaplasia='';
var Kokobasilus='';
var Kandida='';
var Trikhomonas='';
var HerpesSimplek='';
var HPV='';
var EfekRadiasi='';

var Displasia='';
var SelKoilosit='';
var Karsinoma='';
var Adenokarsinoma='';
arraystorePenyakit	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_penyakit', 'penyakit', 'kd_pasien', 'urut', 'urut_masuk', 'tgl_masuk', 'kasus', 'stat_diag', 'note', 'detail'],
	data: []
});

CurrentPage.page = getPanelHasilLabAnatomi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


//membuat form
function getPanelHasilLabAnatomi(mod_id) 
{
    var Field = ['kd_pasien','nama','alamat','tgl_masuk','nama_unit','kd_kelas','kd_dokter','urut_masuk',
				'nama_dokter','tgl_lahir','jenis_kelamin','gol_darah','nama_unit_asal','kd_dokter_asal','jam_masuk','nama_dokter_asal','no_transaksi','no_register'];
    dsTRListHasilLabAnatomi = new WebApp.DataStore({ fields: Field });
    //refeshhasillabanatomi();
	dataGridListPasienLabPA()
    grListPasienHasilLabAnatomi = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            store: dsTRListHasilLabAnatomi,
            columnLines: false,
            autoScroll:true,
            border: false,
			sort :false,
			height:390,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedHasilLabAnatomi = dsTRListHasilLabAnatomi.getAt(row);
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedHasilLabAnatomi = dsTRListHasilLabAnatomi.getAt(ridx);
                    if (rowSelectedHasilLabAnatomi !== undefined)
                    {
                        HasilLookUp_LabAnatomi(rowSelectedHasilLabAnatomi.data);
                    }
                    else
                    {
                        HasilLookUp_LabAnatomi();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
			
                [
                    new Ext.grid.RowNumberer(),
					{
                        header: 'No Lab',
                        dataIndex: 'no_register',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 100
                    },
					{
                        header: 'Tanggal Masuk',
                        dataIndex: 'tgl_masuk',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.tgl_masuk);

							}
                    },
					{
                        header: 'No Transaksi',
                        dataIndex: 'no_transaksi',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 50
                    },
                    {
                        header: 'No Medrec',
                        dataIndex: 'kd_pasien',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 50
                    },
                    {
                        header: 'Nama Pasien',
                        dataIndex: 'nama',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 130
                    },
					{
						header: 'Alamat',
                        width: 170,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'alamat'                        
                    },
                    {
                        header: 'Tanggal Lahir',
                        dataIndex: 'tgl_lahir',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 65,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_lahir);

						}
                    },
                    {
                        header: 'Dokter',
                        dataIndex: 'nama_dokter',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 150
                    },
                    {
                        header: 'Unit Asal',
                        dataIndex: 'nama_unit_asal',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					
                   
                ]
            ),
            viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditHasilLabAnatomi',
                        text: nmEditData,
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedHasilLabAnatomi != undefined)
                            {
								HasilLookUp_LabAnatomi(rowSelectedHasilLabAnatomi.data);
                            }
                            else
                            {
								ShowPesanWarningHasilLabAnatomi('Pilih data data tabel  ','Edit data');
                            }
                        }
                    }
                ]
            }
	);
	
	
	//form depan awal dan judul tab
    var PanelDepanHasilLabAnatomi = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Hasil Laboratorium Anatomi',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
			//height:200,
			autoHeight:true,
            items: [
                    getPanelPencarianHasilLabAnatomi(),
                    grListPasienHasilLabAnatomi
                   ],
            listeners:
            {
                'afterrender': function()
                {
				}
            }
        }
    );
	
   return PanelDepanHasilLabAnatomi;

};

//mengatur lookup edit data
function HasilLookUp_LabAnatomi(rowdata) 
{
    var lebar = 1000;
    FormLookUpsdetailHasilLabAnatomi = new Ext.Window
    (
        {
            id: 'gridHasilLab',
            title: 'Hasil Laboratorium Anatomi',
            closeAction: 'destroy',
            width: 850,
            height: 550,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryHasilLabAnatomi(lebar,rowdata),
			tbar:
			[
				'-',
				{
					text: 'Simpan',
					id: 'btnSimpanHasilLabAnatomi',
					tooltip: nmSimpan,
					iconCls: 'save',
					handler: function()
					{
						Datasave_HasilLabAnatomi(false);
					   
					}
				},  
				'-',
				{
					id:'btnCetakHasilLabAnatomi',
					text: 'Cetak Hasil',
					tooltip: 'Cetak Hasil',
					iconCls:'print',
					handler: function()
					{
						var no_register = rowSelectedHasilLabAnatomi.data.no_register.substr(0,2);
						DataPrint_HasilLabAnatomi(no_register);
						
					   
					}
				},
				'-'
			],
            listeners:
            {
                close: function(){	
            		Kepuasan='';
					Normal='';
					AtipiaSquoenosa='';
					Jinak='';
					Radang='';
					Inflamasia='';
					Metaplasia='';
					Kokobasilus='';
					Kandida='';
					Trikhomonas='';
					HerpesSimplek='';
					HPV='';
					EfekRadiasi='';

					Displasia='';
					SelKoilosit='';
					Karsinoma='';
					Adenokarsinoma='';
					
        		},'beforeclose': function(){},'afterclose': function(){}
            }
        }
    );

    FormLookUpsdetailHasilLabAnatomi.show();
    if (rowdata === undefined) {
		
	}
	else{
		DataInitHasilLabAnatomi(rowdata);
	}

};


//mengatur lookup toolbar
function getFormEntryHasilLabAnatomi(lebar,data) 
{
	var TabPanelEntryHasilLabAnatomi = new Ext.TabPanel   
    (
        {
			id:'TabPanelEntryHasilLabAnatomi',
			region: 'center',
			//activeTab: 0,
			height:330,
			width:828,
			border:true,
			plain: true,
			defaults:
			{
				autoScroll: true
			},
			items: [
				panelJawabanPemeriksaanLabAnatomi_MI(),
				panelDetailHasilPemeriksaanLabAnatomi(),
				//panelDiagnosaHasilPemeriksaanLabAnatomi(),
				panelBahanHasilLabAnatomi(),
				panelDetailRingkasanKetKlinik(),
				panelDetailHasilLabFormatMS()
			]
        }
		
    );
	
    var pnlHasilLabAnatomi = new Ext.FormPanel
    (
        {
            id: 'PanelHasilLab',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:5px 5px 5px 5px',
            height:145,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [panelInfoDataPasienLabAnatomi(lebar)]
        }
    );

	var pnlDetailHasilLabAnatomi2 = new Ext.FormPanel
    (
        {
            id: 'pnlDetailHasilLabAnatomi2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:5px 5px 5px 5px',
            height:345,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
				TabPanelEntryHasilLabAnatomi
			]
        }
    );

    var PanelHasilLabAnatomi = new Ext.Panel
	(
		{
		    id: 'PanelHasilLabAnatomi',
		    region: 'center',
		    width: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[ 
				pnlHasilLabAnatomi,pnlDetailHasilLabAnatomi2
			]

		}
	);
	
    return PanelHasilLabAnatomi
};


//AWAL INPUT
function getPanelPencarianHasilLabAnatomi()
{
    var items =
    {
        layout:'column',
		bodyStyle: 'padding: 5px 0px 5px 10px',
        border:true,
        items:
        [
			//--------------------------------------------------
            {
                columnWidth:.99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 600,
                height: 120,
                anchor: '100% 100%',
                items:
                [
					{
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec  '
                    },
					{
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
					{
						x : 155,
                        y : 10,
                        xtype: 'textfield',
                        name: 'txtNoMedrec_LabAnatomi',
                        id: 'txtNoMedrec_LabAnatomi',
                        enableKeyEvents: true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrec_LabAnatomi').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
											var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrec_LabAnatomi').getValue())
											Ext.getCmp('txtNoMedrec_LabAnatomi').setValue(tmpgetNoMedrec);
											
											tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
											tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
											tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
											tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
											dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
                                        }
                                        else
                                            {
												tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
												tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
												tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
												tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
												dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
                                                    
                                            }
                                } else{
									tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
									tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
									tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
									tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
									dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
								}
								
                            }

                        }

                    },	
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
					{
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 40,
                        xtype: 'textfield',
                        name: 'TxtNamaHasilLabAnatomi',
                        id: 'TxtNamaHasilLabAnatomi',
                        width: 200,
                        enableKeyEvents: true,
                        listeners:
                        { 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
									tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
									tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
									tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
									dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
								} 						
							}
                        }
                    },
					{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Tanggal Kunjungan '
                    },
					{
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 70,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterHasilLabAnatomi',
                        format: 'd/M/Y',
                        value: tigaharilalu_LabAnatomi,
                        listeners:
                        { 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
									tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
									tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
									tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
									dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
								} 						
							}
                        }
                    },
					{
                        x: 270,
                        y: 70,
                        xtype: 'label',
                        text: 's/d '
                    },
					{
                        x: 305,
                        y: 70,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterHasilLab',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        listeners:
                        { 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									tmpkd_pasien=Ext.getCmp('txtNoMedrec_LabAnatomi').getValue();
									tmpnama=Ext.getCmp('TxtNamaHasilLabAnatomi').getValue();
									tmptglAwal=Ext.getCmp('dtpTglAwalFilterHasilLabAnatomi').getValue();
									tmptglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
									dataGridListPasienLabPA(tmpkd_pasien,tmpnama,tmptglAwal,tmptglAkhir);
								} 						
							}
                        }
                    },
					{
                        x: 555,
                        y: 70,
                        xtype:'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        anchor: '25%',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function(sm, row, rec)
                        {
							dataGridListPasienLabPA();
                        }
                    },
					{
                        x: 153,
                        y: 97,
                        xtype: 'label',
                        text: '*)Tekan enter untuk mencari',
						style :{ fontSize:10}
                    },
                    
                ]
            },
        ]
    }
    return items;
};
//LOOKUP hasil laboratorium
function panelInfoDataPasienLabAnatomi(lebar){
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 828,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:true,
	    height:135,
	    items:
		[
			{
					columnWidth: .99,
					layout: 'absolute',
					bodyStyle: 'padding: 0px 0px 0px 0px',
					border: false,
					width: 100,
					height: 140,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'No. Medrec '
						},
						{
							x: 110,
							y: 10,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 10,
							xtype: 'textfield',
							name: 'TxtPopupMedrec_LabAnatomi',
							id: 'TxtPopupMedrec_LabAnatomi',
							width: 80,
							readOnly:true
						},
						{
							x: 210,
							y: 10,
							xtype: 'label',
							text: 'Tanggal '
						},
						{
							x: 250,
							y: 10,
							xtype: 'label',
							text: ' : '
						},
						{   //cobacoba
							x : 260,
							y : 10,
							xtype: 'datefield',
							name: 'dPopupTglCekPasien_LabAnatomi',
							id: 'dPopupTglCekPasien_LabAnatomi',
							width: 110,
							format: 'd/M/Y',
							value: now,
							readOnly:true
						},
						{
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Nama Pasien '
						},
						{
							x: 110,
							y: 40,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupNamaPasien_LabAnatomi',
							id: 'TxtPopupNamaPasien_LabAnatomi',
							width: 250,
							readOnly:true
						},
						{
							x: 380,
							y: 20,
							xtype: 'label',
							text: 'Tgl Lahir '
						},
						{   
							x : 380,
							y : 40,
							xtype: 'datefield',
							name: 'dPopupTglLahirPasien_LabAnatomi',
							id: 'dPopupTglLahirPasien_LabAnatomi',
							width: 100,
							format: 'd/M/Y',
							readOnly:true
						},
						{
							x: 490,
							y: 20,
							xtype: 'label',
							text: 'Thn '
						},
						{   
							x : 490,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupThnLahirPasien_LabAnatomi',
							id: 'TxtPopupThnLahirPasien_LabAnatomi',
							width: 30,
							readOnly:true
						},
						{
							x: 530,
							y: 20,
							xtype: 'label',
							text: 'Bln '
						},
						{   
							x : 530,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupBlnLahirPasien_LabAnatomi',
							id: 'TxtPopupBlnLahirPasien_LabAnatomi',
							width: 30,
							readOnly:true
						},
						{
							x: 570,
							y: 20,
							xtype: 'label',
							text: 'Hari '
						},
						{   
							x : 570,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupHariLahirPasien_LabAnatomi',
							id: 'TxtPopupHariLahirPasien_LabAnatomi',
							width: 30,
							readOnly:true
						},
						{
							x: 610,
							y: 20,
							xtype: 'label',
							text: 'Jenis Kelamin '
						},
						{   
							x : 610,
							y : 40,
							xtype: 'textfield',
							name: 'TxtJK_LabAnatomi',
							id: 'TxtJK_LabAnatomi',
							width: 70,
							readOnly:true,
							//hidden:false
						},
						{
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Alamat '
						},
						{
							x: 110,
							y: 70,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 70,
							xtype: 'textfield',
							name: 'TxtPopupAlamat_LabAnatomi',
							id: 'TxtPopupAlamat_LabAnatomi',
							width: 560,
							readOnly:true
						},
						{
							x: 10,
							y: 100,
							xtype: 'label',
							text: 'Dokter Lab'
						},
						{
							x: 110,
							y: 100,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtPopupNamaDokter_LabAnatomi',
							id: 'TxtPopupNamaDokter_LabAnatomi',
							width: 250,
							readOnly:true
						},
						//-------HIDDEN---------------------
						{   
							x : 480,
							y : 10,
							xtype: 'datefield',
							name: 'dPopupTglMasuk_LabAnatomi',
							id: 'dPopupTglMasuk_LabAnatomi',
							width: 150,
							readOnly:true,
							format: 'd/M/Y',
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtNoTransaksi_LabAnatomi',
							id: 'TxtNoTransaksi_LabAnatomi',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtKdUnit_LabAnatomi',
							id: 'TxtKdUnit_LabAnatomi',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 480,
							y : 10,
							xtype: 'datefield',
							name: 'dPopupJamMasuk',
							id: 'dPopupJamMasuk',
							width: 150,
							readOnly:true,
							format: 'd/M/Y',
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtUrutMasuk_LabAnatomi',
							id: 'TxtUrutMasuk_LabAnatomi',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtNmDokter_LabAnatomi',
							id: 'TxtNmDokter_LabAnatomi',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtPopPoli_LabAnatomi',
							id: 'TxtPopPoli_LabAnatomi',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtPopKddokter_LabAnatomi',
							id: 'TxtPopKddokter_LabAnatomi',
							width: 250,
							readOnly:true,
							hidden:true
						}
					]
				},
	
		]
	};
	return items;
}
function panelDetailHasilPemeriksaanLabAnatomi(){
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 828,
		title: 'Hasil Laboratorium',
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:false,
		height:320,
	    items:
		[
			{
				columnWidth: .99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width: 100,
				//height: 312,
				anchor: '100% 100%',
				items:
				[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Makroskopik '
					},
					{
						x: 110,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 10,
						xtype: 'textarea',
						name: 'TxtMakroskopik_LabAnatomi',
						id: 'TxtMakroskopik_LabAnatomi',
						width: 700,
						height: 85
					},
					
					{
						x: 10,
						y: 105,
						xtype: 'label',
						text: 'Mikroskopik '
					},
					{
						x: 110,
						y: 105,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 105,
						xtype: 'textarea',
						name: 'TxtMikroskopik_LabAnatomi',
						id: 'TxtMikroskopik_LabAnatomi',
						width: 700,
						height: 85
					},
					{
						x: 10,
						y: 205,
						xtype: 'label',
						text: 'Kesimpulan '
					},
					{
						x: 110,
						y: 205,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 205,
						xtype: 'textarea',
						name: 'TxtKesimpulan_LabAnatomi',
						id: 'TxtKesimpulan_LabAnatomi',
						width: 700,
						height: 85
					},
					
				]
			},
			
	
		]
	};
	return items;
}

function panelJawabanPemeriksaanLabAnatomi_MI(){
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 828,
		title: 'Jawaban Pemeriksaan',
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:false,
		height:320,
	    items:
		[
			{
				columnWidth: .99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width: 100,
				//height: 312,
				anchor: '100% 100%',
				items:
				[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Jawaban Pemeriksaan Immunohistokimia '
					},
					{
						x: 250,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 260,
						y : 10,
						xtype: 'textarea',
						name: 'TxtJawabanPemeriksaan_LabAnatomi',
						id: 'TxtJawabanPemeriksaan_LabAnatomi',
						width: 560,
						height: 85
					},
					
					
				]
			},
			
	
		]
	};
	return items;
}
function panelDetailRingkasanKetKlinik(){
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 828,
		title: 'Ringkasan Keterangan Klinik',
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:false,
		height:320,
	    items:
		[
			{
				columnWidth: .99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width: 100,
				height: 312,
				anchor: '100% 100%',
				items:
				[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Diagnosa Klinik '
					},
					{
						x: 270,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 280,
						y : 10,
						xtype: 'textarea',
						name: 'TxtDiagKlinik_LabAnatomi',
						id: 'TxtDiagKlinik_LabAnatomi',
						width: 540,
						height: 85
					},
					
					{
						x: 10,
						y: 105,
						xtype: 'label',
						text: 'Diagnosa Pemeriksaan Patologi/Sitologi Sebelumnya '
					},
					{
						x: 270,
						y: 105,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 280,
						y : 105,
						xtype: 'textarea',
						name: 'TxtDiagPemeriksaan_LabAnatomi',
						id: 'TxtDiagPemeriksaan_LabAnatomi',
						width: 540,
						height: 85
					},
					{
						x: 10,
						y: 205,
						xtype: 'label',
						text: 'No Register '
					},
					{
						x: 270,
						y: 205,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 280,
						y : 205,
						xtype: 'textfield',
						name: 'TxtNoRegister_LabAnatomi',
						id: 'TxtNoRegister_LabAnatomi',
						width: 100,
					},
					
				]
			},
			
	
		]
	};
	return items;
}

function panelDetailHasilLabFormatMS(){
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 828,
		title: 'Hasil Laboratorium',
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:false,
		autoScroll:true,
		height:320,
	    items:
		[
			{
				columnWidth: .99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				autoScroll:true,
				width: 100,
				height: 312,
				anchor: '100% 100%',
				items:
				[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Kualitas Sediaan '
					},
					
					{
						x: 10,
						y: 30,
						boxLabel: 'Memuaskan',
						xtype: 'radio',
						id:'radiopuas',
						handler: function (field, value) 
						{if (value === true)
							{
								Ext.getCmp('radiokurangpuas').setValue(false);
								Ext.getCmp('radiotidakpuas').setValue(false);
								Kepuasan='puas';
							}else
							{
								/* Ext.getCmp('radiokurangpuas').setValue(false);
								Ext.getCmp('radiotidakpuas').setValue(false); */
							}
						}
					},{
						x: 100,
						y: 30,
						boxLabel: 'Kurang Memuaskan',
						xtype: 'radio',
						id:'radiokurangpuas',
						handler: function (field, value) 
						{if (value === true)
							{
								Ext.getCmp('radiopuas').setValue(false);
								Ext.getCmp('radiotidakpuas').setValue(false);
								Kepuasan='kurangpuas';
							}else
							{
								/* Ext.getCmp('radiopuas').setValue(false);
								Ext.getCmp('radiotidakpuas').setValue(false); */
							}
						}
					},{
						x: 230,
						y: 30,
						boxLabel: 'Tidak Memuaskan',
						xtype: 'radio',
						id:'radiotidakpuas',
						handler: function (field, value) 
						{if (value === true)
							{
								Ext.getCmp('radiokurangpuas').setValue(false);
								Ext.getCmp('radiopuas').setValue(false);
								Kepuasan='tidakpuas';
							}else
							{
								/* Ext.getCmp('radiokurangpuas').setValue(false);
								Ext.getCmp('radiopuas').setValue(false); */
							}
						}
					},{   
						x : 10,
						y : 50,
						text: 'Alasan :',
						xtype: 'label',
						
					},{   
						x : 50,
						y : 50,
						xtype: 'textfield',
						name: 'TxtAlasanFormatMS_LabAnatomi',
						id: 'TxtAlasanFormatMS_LabAnatomi',
						width: 280,
						
					},
					{
						x: 10,
						y: 80,
						xtype: 'checkbox',
						id: 'chkNormal_MS',
						hideLabel:false,
						boxLabel: 'Normal',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('chkNormal_MS').getValue()===true)
								{
									Normal=true;
								}
								else
								{
									Normal=false;
								}
							}
					   }
					},{
						x: 10,
						y: 100,
						xtype: 'checkbox',
						id: 'chkAtipiaSquoenosa_MS',
						hideLabel:false,
						boxLabel: 'Atipia Squoenosa',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkAtipiaSquoenosa_MS').getValue()===true)
								{
									AtipiaSquoenosa=true;
								}
								else
								{
									AtipiaSquoenosa=false;
								}
							}
					   }
					},{
						x: 150,
						y: 100,
						xtype: 'checkbox',
						id: 'chkJinak_MS',
						hideLabel:false,
						boxLabel: 'Jinak',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkJinak_MS').getValue()===true)
								{
									Jinak=true;
								}
								else
								{
									Jinak=false;
								}
							}
					   }
					},{
						x: 10,
						y: 120,
						xtype: 'checkbox',
						id: 'chkRadang_MS',
						hideLabel:false,
						boxLabel: 'Radang',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkRadang_MS').getValue()===true)
								{
									Radang=true;
								}
								else
								{
									Radang=false;
								}
							}
					   }
					},{
						x: 150,
						y: 120,
						xtype: 'checkbox',
						id: 'chkInflamasia_MS',
						hideLabel:false,
						boxLabel: 'Inflamasia',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkInflamasia_MS').getValue()===true)
								{
									Inflamasia=true;
								}
								else
								{
									Inflamasia=false;
								}
							}
					   }
					},{
						x: 290,
						y: 120,
						xtype: 'checkbox',
						id: 'chkMetaplasia_MS',
						hideLabel:false,
						boxLabel: 'Metaplasia',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkMetaplasia_MS').getValue()===true)
								{
									Metaplasia=true;
								}
								else
								{
									Metaplasia=false;
								}
							}
					   }
					},{
						x: 10,
						y: 140,
						xtype: 'checkbox',
						id: 'chkKokobasilus_MS',
						hideLabel:false,
						boxLabel: 'Kokobasilus',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkKokobasilus_MS').getValue()===true)
								{
									Kokobasilus=true;
								}
								else
								{
									Kokobasilus=false;
								}
							}
					   }
					},{
						x: 150,
						y: 140,
						xtype: 'checkbox',
						id: 'chkKandida_MS',
						hideLabel:false,
						boxLabel: 'Kandida',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkKandida_MS').getValue()===true)
								{
									Kandida=true;
								}
								else
								{
									Kandida=false;
								}
							}
					   }
					},{
						x: 290,
						y: 140,
						xtype: 'checkbox',
						id: 'chkTrikhomonas_MS',
						hideLabel:false,
						boxLabel: 'Trikhomonas',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkTrikhomonas_MS').getValue()===true)
								{
									Trikhomonas=true;
								}
								else
								{
									Trikhomonas=false;
								}
							}
					   }
					},{
						x: 10,
						y: 160,
						xtype: 'checkbox',
						id: 'chkHerpesSimplek_MS',
						hideLabel:false,
						boxLabel: 'Herpes Simplek',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('chkHerpesSimplek_MS').getValue()===true)
								{
									HerpesSimplek=true;
								}
								else
								{
									HerpesSimplek=false;
								}
							}
					   }
					},{
						x: 150,
						y: 160,
						xtype: 'checkbox',
						id: 'chkHPV_MS',
						hideLabel:false,
						boxLabel: 'HPV',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkHPV_MS').getValue()===true)
								{
									HPV=true;
								}
								else
								{
									HPV=false;
								}
							}
					   }
					},{
						x: 10,
						y: 180,
						xtype: 'checkbox',
						id: 'chkEfekRadiasi_MS',
						hideLabel:false,
						boxLabel: 'Efek Radiasi',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkEfekRadiasi_MS').getValue()===true)
								{
									EfekRadiasi=true;
								}
								else
								{
									EfekRadiasi=false;
								}
							}
					   }
					},{
						x: 10,
						y: 210,
						xtype: 'radio',
						id: 'radDisplasiaRingan_MS',
						hideLabel:false,
						boxLabel: 'Displasia Ringan',
						handler: function (field, value) 
						{
							if (value === true)
							{
								Ext.getCmp('chSelKoilosit_MS').enable();
								Ext.getCmp('radDisplasiaBerat_MS').setValue(false);
								Ext.getCmp('radDisplasiaSedang_MS').setValue(false);
								Displasia='ringan';
							}else
							{
								//Ext.getCmp('chSelKoilosit_MS').disable();
								/* Ext.getCmp('radDisplasiaBerat_MS').setValue(false);
								Ext.getCmp('radDisplasiaSedang_MS').setValue(false); */
								//Displasia='';
							}
						}
					},{
						x: 150,
						y: 210,
						xtype: 'checkbox',
						id: 'chSelKoilosit_MS',
						hideLabel:false,
						boxLabel: 'Sel Koilosit',
						checked: false,
						disabled:true,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chSelKoilosit_MS').getValue()===true)
								{
									SelKoilosit=true;
								}
								else
								{
									SelKoilosit=false;
								}
							}
					   }
					},{
						x: 10,
						y: 230,
						xtype: 'radio',
						id: 'radDisplasiaSedang_MS',
						hideLabel:false,
						boxLabel: 'Displasia Sedang',
						checked: false,
						handler: function (field, value) 
						{
							if (value === true)
							{
								Ext.getCmp('chSelKoilosit_MS').disable();
								Ext.getCmp('radDisplasiaBerat_MS').setValue(false);
								Ext.getCmp('radDisplasiaRingan_MS').setValue(false);
								Displasia='sedang';
							}else
							{
								//Ext.getCmp('chSelKoilosit_MS').disable();
								/* Ext.getCmp('radDisplasiaBerat_MS').setValue(false);
								Ext.getCmp('radDisplasiaRingan_MS').setValue(false); */
								//Displasia='';
							}
						}
					},{
						x: 10,
						y: 250,
						xtype: 'radio',
						id: 'radDisplasiaBerat_MS',
						hideLabel:false,
						boxLabel: 'Displasia Berat',
						checked: false,
						handler: function (field, value) 
						{
							if (value === true)
							{
								Ext.getCmp('chSelKoilosit_MS').disable();
								Ext.getCmp('radDisplasiaSedang_MS').setValue(false);
								Ext.getCmp('radDisplasiaRingan_MS').setValue(false);
								Displasia='berat';
							}else
							{
								//Ext.getCmp('chSelKoilosit_MS').disable();
								/* Ext.getCmp('radDisplasiaSedang_MS').setValue(false);
								Ext.getCmp('radDisplasiaRingan_MS').setValue(false); */
								//Displasia='';
							}
						}
					},{
						x: 10,
						y: 280,
						xtype: 'checkbox',
						id: 'chkKarsinoma_MS',
						hideLabel:false,
						boxLabel: 'Karsinoma Squamosa Invasif',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkKarsinoma_MS').getValue()===true)
								{
									Karsinoma=true;
								}
								else
								{
									Karsinoma=false;
								}
							}
					   }
					},{
						x: 10,
						y: 300,
						xtype: 'checkbox',
						id: 'chkAdenokarsinoma_MS',
						hideLabel:false,
						boxLabel: 'Adenokarsinoma',
						checked: false,
						listeners: 
					   {
							check: function()
							{
							  if(Ext.getCmp('chkAdenokarsinoma_MS').getValue()===true)
								{
									Adenokarsinoma=true;
								}
								else
								{
									Adenokarsinoma=false;
								}
							}
					   }
					},{   
						x : 10,
						y : 330,
						text: 'Kesimpulan :',
						xtype: 'label',
						
					},{   
						x : 100,
						y : 330,
						xtype: 'textarea',
						name: 'TxtKesimpulanFormatMS_LabAnatomi',
						id: 'TxtKesimpulanFormatMS_LabAnatomi',
						width: 700,
						height : 85
					},{   
						x : 10,
						y : 420,
						text: 'Rekomendasi :',
						xtype: 'label',
						
					},{   
						x : 100,
						y : 420,
						xtype: 'textarea',
						name: 'TxtRekomendasiFormatMS_LabAnatomi',
						id: 'TxtRekomendasiFormatMS_LabAnatomi',
						width: 700,
						height : 85
					},
					
				]
			},
			
	
		]
	};
	return items;
}
function panelBahanHasilLabAnatomi(){
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 828,
		title: 'Bahan',
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:false,
		height:320,
	    items:
		[
			{
				columnWidth: .99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width: 100,
				height: 312,
				anchor: '100% 100%',
				items:
				[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Bahan '
					},
					{
						x: 110,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 10,
						xtype: 'textarea',
						name: 'TxtBahan_LabAnatomi',
						id: 'TxtBahan_LabAnatomi',
						width: 700,
						height: 85
					},
					
					{
						x: 10,
						y: 105,
						xtype: 'label',
						text: 'ICDOT '
					},
					{
						x: 110,
						y: 105,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 105,
						xtype: 'textarea',
						name: 'TxtICDOT_LabAnatomi',
						id: 'TxtICDOT_LabAnatomi',
						width: 700,
						height: 85
					},
					{
						x: 10,
						y: 205,
						xtype: 'label',
						text: 'ICDOM '
					},
					{
						x: 110,
						y: 205,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 205,
						xtype: 'textarea',
						name: 'TxtICDOM_LabAnatomi',
						id: 'TxtICDOM_LabAnatomi',
						width: 700,
						height: 85
					},
					
				]
			},
			
	
		]
	};
	return items;
}
function panelDiagnosaHasilPemeriksaanLabAnatomi(){
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 828,
		title: 'Entry Diagnosa',
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:false,
		height:100,
		tbar:[
			'-',
			{
				text: 'Tambah',
				id: 'btnAddGridDiagnosaLabAnatomi',
				iconCls: 'add',
				handler: function()
				{
					var records = new Array();
					records.push(new dsDiagnosaLabAnatomi.recordType());
					dsDiagnosaLabAnatomi.add(records);

				}
			},
			'-',
			{
				text: 'Simpan',
				id: 'btnSimpasDiagnosaLabAnatomi',
				tooltip: nmSimpan,
				iconCls: 'save',
				handler: function()
				{
					DatasaveDiagnosa_HasilLabAnatomi();
				   
				}
			},  
			'-',
			{
				id:'btnHapusDiagnosaLabAnatomi',
				text: 'Hapus',
				iconCls:'remove',
				handler: function()
				{
					var line = gridDTHasilLabAnatomi.getSelectionModel().selection.cell[0];
					var o = dsDiagnosaLabAnatomi.getRange()[line].data;
					if(dsDiagnosaLabAnatomi.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah dignosa ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDiagnosaLabAnatomi.getRange()[line].data.urut != undefined || dsDiagnosaLabAnatomi.getRange()[line].data.urut != ''){
									Ext.Ajax.request
									({
										url: baseURL + "index.php/lab_pa/functionLABPA/hapusDiagnosa",
										params:{
											kd_penyakit:o.kd_penyakit, 
											kd_pasien:Ext.getCmp('TxtPopupMedrec_LabAnatomi').getValue(),
											kd_unit:Ext.getCmp('TxtKdUnit_LabAnatomi').getValue(), 
											tgl_masuk:Ext.get('dPopupTglMasuk_LabAnatomi').dom.value,
											urut_masuk:Ext.getCmp('TxtUrutMasuk_LabAnatomi').getValue(), 
											urut:o.urut
										} ,
										failure: function(o)
										{
											loadMask.hide();
											ShowPesanErrorHasilLabAnatomi('Hubungi Admin', 'Error');
										},	
										success: function(o) 
										{
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) 
											{
												dsDiagnosaLabAnatomi.removeAt(line);
												gridDTHasilLabAnatomi.getView().refresh();
												
											}
											else 
											{
												loadMask.hide();
												ShowPesanErrorHasilLabAnatomi('Gagal melakukan penghapusan', 'Error');
											};
										}
									})
								}else{
									dsDiagnosaLabAnatomi.removeAt(line);
									gridDTHasilLabAnatomi.getView().refresh();
								}
							} 
							
						});
					} else{
						loadMask.hide();
						ShowPesanErrorHasilLabAnatomi('Data tidak bisa dihapus karena minimal dignosa 1','Error');
					}
				}
			},
			'-'
		],
	    items:
		[
			
			{
				columnWidth: 250,
				layout: 'form',
				labelWidth: 50,
				height:20,
				border: false,
				items:
				[/* 
					{xtype: 'tbspacer',height: 10, width:5},
					{   
						xtype: 'textfield',
						fieldLabel: '',
						name: 'TxtPopupMedrec_LabAnatomi',
						id: 'TxtPopupMedrec_LabAnatomi',
						width: 80,
						readOnly:true
					},
					{xtype: 'tbspacer',height: 10, width:5}, */
					GetDTGridDiagnosaHasilLabAnatomi()
				]
			},
		]
	};
	return items;
}

//------------GRID DALAM LOOK UP HASIL LAB--------------------------------------------------------------------------
function GetDTGridDiagnosaHasilLabAnatomi() 
{
	var fm = Ext.form;
    var fldDetailHasilLab = ['KLASIFIKASI', 'DESKRIPSI', 'KD_LAB', 'KD_TEST', 'ITEM_TEST', 'SATUAN', 'NORMAL', 'NORMAL_W',  'NORMAL_A', 'NORMAL_B', 'COUNTABLE', 'MAX_M', 'MIN_M', 'MAX_F', 'MIN_F', 'MAX_A', 'MIN_A', 'MAX_B', 'MIN_B', 'KD_METODE', 'HASIL', 'KET','KD_UNIT_ASAL','NAMA_UNIT_ASAL','URUT','METODE','JUDUL_ITEM','KET_HASIL'];
	
    dsDiagnosaLabAnatomi = new WebApp.DataStore({ fields: fldDetailHasilLab })
    gridDTHasilLabAnatomi = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Diagnosa',
            stripeRows: true,
            store: dsDiagnosaLabAnatomi,
            border: true,
            columnLines: true,
            frame: false,
            autoScroll:true,
			height:275,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi_LabAnatomi = dsDiagnosaLabAnatomi.getAt(row);
                            CurrentHasilLabAnatomi.row = row;
                            CurrentHasilLabAnatomi.data = cellSelecteddeskripsi_LabAnatomi;
                            //FocusCtrlCMRWJ='txtAset';
                        }
                    }
                }
            ),
			cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(), 
					{
						header: 'No ICD',
						dataIndex: 'kd_penyakit',
						width:80,
						menuDisabled:true,
						hidden:false,
						editor		:new Nci.form.Combobox.autoCompleteId({
							store	: arraystorePenyakit,
							select	: function(a,b,c){
								var line	= gridDTHasilLabAnatomi.getSelectionModel().selection.cell[0];
								
								dsDiagnosaLabAnatomi.getRange()[line].data.penyakit=b.data.penyakit;
								dsDiagnosaLabAnatomi.getRange()[line].data.kd_penyakit=b.data.kd_penyakit;
								gridDTHasilLabAnatomi.getView().refresh();
								
							},
							insert	: function(o){
								return {
									kd_penyakit	: o.kd_penyakit,
									penyakit	: o.penyakit,
									text		: '<table style="font-size: 11px;"><tr><td width="50">' + o.kd_penyakit + '</td><td width="200">' + o.penyakit + '</td></tr></table>',
								}
							},
							/* param:function(){
									return {
										kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue()
									}
							}, */
							url			: baseURL + "index.php/lab_pa/functionLABPA/getKodePenyakit",
							valueField	: 'kd_penyakit',
							displayField: 'text',
							listWidth	: 250
						})
					},
					{
						header: 'Penyakit',
						dataIndex: 'penyakit',
						hideable:false,
						menuDisabled: true,
						width: 200,
						editor		:new Nci.form.Combobox.autoCompleteId({
							store	: arraystorePenyakit,
							select	: function(a,b,c){
								var line	= gridDTHasilLabAnatomi.getSelectionModel().selection.cell[0];
								
								dsDiagnosaLabAnatomi.getRange()[line].data.penyakit=b.data.penyakit;
								dsDiagnosaLabAnatomi.getRange()[line].data.kd_penyakit=b.data.kd_penyakit;
								gridDTHasilLabAnatomi.getView().refresh();
								
							},
							insert	: function(o){
								return {
									kd_penyakit	: o.kd_penyakit,
									penyakit	: o.penyakit,
									text		: '<table style="font-size: 11px;"><tr><td width="50">' + o.kd_penyakit + '</td><td width="200">' + o.penyakit + '</td></tr></table>',
								}
							},
							/* param:function(){
									return {
										kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue()
									}
							}, */
							url			: baseURL + "index.php/lab_pa/functionLABPA/getPenyakit",
							valueField	: 'penyakit',
							displayField: 'text',
							listWidth	: 250
						})
					},
					{
						header: 'Diagnosa',
						width: 100,
						menuDisabled: true,
						dataIndex: 'stat_diag',
						editor: new Ext.form.ComboBox({
							typeAhead: true,
							triggerAction: 'all',
							lazyRender: true,
							mode: 'local',
							selectOnFocus: true,
							forceSelection: true,
							width: 50,
							anchor: '95%',
							value: 1,
							store: new Ext.data.ArrayStore({
								id: 0,
								fields: ['Id', 'displayText'],
								data: [[1, 'Diagnosa Awal'], [2, 'Diagnosa Utama'], [3, 'Komplikasi'], [4, 'Diagnosa Sekunder']]
							}),
							valueField: 'displayText',
							displayField: 'displayText',
							value		: '',
							listeners: {}
						})
					},
					{
						header: 'Kasus',
						width: 80,
						menuDisabled: true,
						dataIndex: 'kasus',
						editor: new Ext.form.ComboBox({
							typeAhead: true,
							triggerAction: 'all',
							lazyRender: true,
							mode: 'local',
							selectOnFocus: true,
							forceSelection: true,
							width: 50,
							anchor: '95%',
							value: 1,
							store: new Ext.data.ArrayStore({
								id: 0,
								fields: ['Id', 'displayText'],
								data: [[1, 'Baru'], [2, 'Lama']]
							}),
							valueField: 'displayText',
							displayField: 'displayText',
							value		: '',
							listeners: {}
						})
					}, 
					{
						header:'urut',
						dataIndex: 'urut',
						width:250,
						hidden:true
						
					}

				]
			),
			viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTHasilLabAnatomi;
};


//------------------MEMASUKAN DATA YG DIPILIH KEDALAM TEXTBOX YG ADA DALAM LOOKUP--------------------------------------------------
function DataInitHasilLabAnatomi(rowdata)
{
	Kepuasan='';
	Normal='';
	AtipiaSquoenosa='';
	Jinak='';
	Radang='';
	Inflamasia='';
	Metaplasia='';
	Kokobasilus='';
	Kandida='';
	Trikhomonas='';
	HerpesSimplek='';
	HPV='';
	EfekRadiasi='';

	Displasia='';
	SelKoilosit='';
	Karsinoma='';
	Adenokarsinoma='';
	
    AddNewHasilLabAnatomi = false;
	var tabPanel = Ext.getCmp('TabPanelEntryHasilLabAnatomi');
	
	if (rowdata.no_register.substr(0,2) === 'MF'){
		tabPanel.hideTabStripItem(0);
		tabPanel.hideTabStripItem(4);
		Ext.getCmp('gridHasilLab').setTitle('Hasil Laboratorium Anatomi (MF)');
		tabPanel.setActiveTab(1);
	}else if (rowdata.no_register.substr(0,2) === 'MT'){
		tabPanel.hideTabStripItem(0);
		tabPanel.hideTabStripItem(4);
		Ext.getCmp('gridHasilLab').setTitle('Hasil Laboratorium Anatomi (MT)');
		tabPanel.setActiveTab(1);
	}else if (rowdata.no_register.substr(0,2) === 'MI'){
		tabPanel.hideTabStripItem(1);
		tabPanel.hideTabStripItem(4);
		Ext.getCmp('gridHasilLab').setTitle('Hasil Laboratorium Anatomi (MI)');
		tabPanel.setActiveTab(0);
	}else if (rowdata.no_register.substr(0,2) === 'MS'){
		tabPanel.hideTabStripItem(0);
		tabPanel.hideTabStripItem(1);
		tabPanel.hideTabStripItem(2);
		tabPanel.hideTabStripItem(3);
		Ext.getCmp('gridHasilLab').setTitle('Hasil Laboratorium Anatomi (MS)');
		tabPanel.setActiveTab(4);
	}else if (rowdata.no_register.substr(0,2) === 'K.'){
		tabPanel.hideTabStripItem(0);
		tabPanel.hideTabStripItem(4);
		Ext.getCmp('gridHasilLab').setTitle('Hasil Laboratorium Anatomi (K)');
		tabPanel.setActiveTab(1);
	}else{
		Ext.getCmp('gridHasilLab').setTitle('Hasil Laboratorium Anatomi ');
		tabPanel.hideTabStripItem(0);
		tabPanel.hideTabStripItem(4);
		tabPanel.setActiveTab(1);
	}
	if (rowdata.jawab_periksa === null || rowdata.jawab_periksa === 'null' || rowdata.jawab_periksa === ''){
		Ext.getCmp('TxtJawabanPemeriksaan_LabAnatomi').setValue('');
	}else{
		Ext.getCmp('TxtJawabanPemeriksaan_LabAnatomi').setValue(rowdata.jawab_periksa.replace(/~~!!!~~/g,"\n"));
	}
	
	tmpnama_unit_asal_labanatomi=rowdata.nama_unit_asal;
	tmpkd_dokter_asal_labanatomi=rowdata.kd_dokter_asal;
	tmpjam_masuk_labanatomi=rowdata.jam_masuk;
	tmpnama_dokter_asal_labanatomi=rowdata.nama_dokter_asal;
	tmpno_pa_labanatomi=rowdata.tag;
	tmptgl_hasil_labanatomi=rowdata.tgl_hasil;
	//alert(tmpjam_masuk_labanatomi);
	Ext.get('TxtPopupMedrec_LabAnatomi').dom.value= rowdata.kd_pasien;
	Ext.get('TxtPopupNamaPasien_LabAnatomi').dom.value = rowdata.nama;
	Ext.get('TxtPopupAlamat_LabAnatomi').dom.value = rowdata.alamat;
	Ext.get('TxtPopupNamaDokter_LabAnatomi').dom.value = rowdata.nama_dokter;
	Ext.get('dPopupTglLahirPasien_LabAnatomi').dom.value = ShowDate(rowdata.tgl_lahir);
	Ext.get('dPopupTglMasuk_LabAnatomi').dom.value = ShowDate(rowdata.tgl_masuk);
	
	if(rowdata.jenis_kelamin=='t'){
		Ext.get('TxtJK_LabAnatomi').dom.value = 'Laki-laki';
	}else{
		Ext.get('TxtJK_LabAnatomi').dom.value = 'Perempuan'
	}
	Ext.get('TxtPopPoli_LabAnatomi').dom.value = rowdata.nama_unit;
	Ext.get('TxtNmDokter_LabAnatomi').dom.value = rowdata.nama_dokter;
	Ext.get('TxtUrutMasuk_LabAnatomi').dom.value = rowdata.urut_masuk;
	Ext.get('TxtPopKddokter_LabAnatomi').dom.value = rowdata.kd_dokter;
	Ext.get('TxtNoTransaksi_LabAnatomi').dom.value = rowdata.no_transaksi;
	setUsia_LabAnatomi(ShowDate(rowdata.tgl_lahir));
	if (rowdata.makroskopik === null || rowdata.makroskopik === 'null' || rowdata.makroskopik === ''){
		Ext.getCmp('TxtMakroskopik_LabAnatomi').setValue('');
	}else{
		Ext.getCmp('TxtMakroskopik_LabAnatomi').setValue(rowdata.makroskopik.replace(/~~!!!~~/g,"\n"));
	}
	
	if (rowdata.mikroskopik === null || rowdata.mikroskopik === 'null' || rowdata.mikroskopik === ''){
		Ext.getCmp('TxtMikroskopik_LabAnatomi').setValue('');
	}else{
		Ext.getCmp('TxtMikroskopik_LabAnatomi').setValue(rowdata.mikroskopik.replace(/~~!!!~~/g,"\n"));
	}
	
	if (rowdata.kesimpulan === null || rowdata.kesimpulan === 'null' || rowdata.kesimpulan === ''){
		Ext.getCmp('TxtKesimpulan_LabAnatomi').setValue('');
	}else{
		Ext.getCmp('TxtKesimpulan_LabAnatomi').setValue(rowdata.kesimpulan.replace(/~~!!!~~/g,"\n"));
	}
	
	if (rowdata.kesimpulan_ms === null || rowdata.kesimpulan_ms === 'null' || rowdata.kesimpulan_ms === ''){
		Ext.getCmp('TxtKesimpulanFormatMS_LabAnatomi').setValue('');
	}else{
		Ext.getCmp('TxtKesimpulanFormatMS_LabAnatomi').setValue(rowdata.kesimpulan_ms.replace(/~~!!!~~/g,"\n"));
	}
	
	if (rowdata.rekomendasi_ms === null || rowdata.rekomendasi_ms === 'null' || rowdata.rekomendasi_ms === ''){
		Ext.getCmp('TxtRekomendasiFormatMS_LabAnatomi').setValue('');
	}else{
		Ext.getCmp('TxtRekomendasiFormatMS_LabAnatomi').setValue(rowdata.rekomendasi_ms.replace(/~~!!!~~/g,"\n"));
	}
	
	if (rowdata.bahan === null || rowdata.bahan === 'null' || rowdata.bahan === ''){
		Ext.getCmp('TxtBahan_LabAnatomi').setValue('');
	}else{
		Ext.getCmp('TxtBahan_LabAnatomi').setValue(rowdata.bahan.replace(/~~!!!~~/g,"\n"));
	}
	
	if (rowdata.icdot === null || rowdata.icdot === 'null' || rowdata.icdot === ''){
		Ext.getCmp('TxtICDOT_LabAnatomi').setValue('');
	}else{
		Ext.getCmp('TxtICDOT_LabAnatomi').setValue(rowdata.icdot.replace(/~~!!!~~/g,"\n"));
	}
	
	if (rowdata.icdom === null || rowdata.icdom === 'null' || rowdata.icdom === ''){
		Ext.getCmp('TxtICDOM_LabAnatomi').setValue('');
	}else{
		Ext.getCmp('TxtICDOM_LabAnatomi').setValue(rowdata.icdom.replace(/~~!!!~~/g,"\n"));
	}
	
	if (rowdata.diagklinik === null || rowdata.diagklinik === 'null' || rowdata.diagklinik === ''){
		Ext.getCmp('TxtDiagKlinik_LabAnatomi').setValue('');
	}else{
		Ext.getCmp('TxtDiagKlinik_LabAnatomi').setValue(rowdata.diagklinik.replace(/~~!!!~~/g,"\n"));
	}

	if (rowdata.diagperiksa === null || rowdata.diagperiksa === 'null' || rowdata.diagperiksa === ''){
		Ext.getCmp('TxtDiagPemeriksaan_LabAnatomi').setValue('');
	}else{
		Ext.getCmp('TxtDiagPemeriksaan_LabAnatomi').setValue(rowdata.diagperiksa.replace(/~~!!!~~/g,"\n"));
	}
	
	
	Ext.getCmp('TxtNoRegister_LabAnatomi').setValue(rowdata.noregister);
	Ext.getCmp('TxtAlasanFormatMS_LabAnatomi').setValue(rowdata.alasan);
	if (rowdata.kepuasan === 'puas'){
		Ext.getCmp('radiopuas').setValue(true);
		Ext.getCmp('radiokurangpuas').setValue(false);
		Ext.getCmp('radiotidakpuas').setValue(false);
	}else if (rowdata.kepuasan === 'kurangpuas'){
		Ext.getCmp('radiopuas').setValue(false);
		Ext.getCmp('radiokurangpuas').setValue(true);
		Ext.getCmp('radiotidakpuas').setValue(false);
	}else if (rowdata.kepuasan === 'tidakpuas'){
		Ext.getCmp('radiopuas').setValue(false);
		Ext.getCmp('radiokurangpuas').setValue(false);
		Ext.getCmp('radiotidakpuas').setValue(true);
	}
	if (rowdata.normal === 'false' || rowdata.normal === 'f' || rowdata.normal === false || rowdata.normal === '' 
		|| rowdata.normal === null	|| rowdata.normal === 'null'){
		Ext.getCmp('chkNormal_MS').setValue(false);
	}else{
		Ext.getCmp('chkNormal_MS').setValue(true);
	}
	
	if (rowdata.atipia === 'false' || rowdata.atipia === 'f' || rowdata.atipia === false || rowdata.atipia === '' 
		|| rowdata.atipia === null	|| rowdata.atipia === 'null'){
		Ext.getCmp('chkAtipiaSquoenosa_MS').setValue(false);
	}else{
		Ext.getCmp('chkAtipiaSquoenosa_MS').setValue(true);
	}
	
	if (rowdata.jinak === 'false' || rowdata.jinak === 'f' || rowdata.jinak === false || rowdata.jinak === '' 
		|| rowdata.jinak === null	|| rowdata.jinak === 'null'){
		Ext.getCmp('chkJinak_MS').setValue(false);
	}else{
		Ext.getCmp('chkJinak_MS').setValue(true);
	}
	
	if (rowdata.radang === 'false' || rowdata.radang === 'f' || rowdata.radang === false || rowdata.radang === '' 
		|| rowdata.radang === null	|| rowdata.radang === 'null'){
		Ext.getCmp('chkRadang_MS').setValue(false);
	}else{
		Ext.getCmp('chkRadang_MS').setValue(true);
	}
	
	
	
	if (rowdata.inflamasia === 'false' || rowdata.inflamasia === 'f' || rowdata.inflamasia === false || rowdata.inflamasia === '' 
		|| rowdata.inflamasia === null	|| rowdata.inflamasia === 'null'){
		Ext.getCmp('chkInflamasia_MS').setValue(false);
	}else{
		Ext.getCmp('chkInflamasia_MS').setValue(true);
	}
	
	if (rowdata.metaplasia === 'false' || rowdata.metaplasia === 'f' || rowdata.metaplasia === false || rowdata.metaplasia === '' 
		|| rowdata.metaplasia === null	|| rowdata.metaplasia === 'null'){
		Ext.getCmp('chkMetaplasia_MS').setValue(false);
	}else{
		Ext.getCmp('chkMetaplasia_MS').setValue(true);
	}
	
	if (rowdata.kokobasilus === 'false' || rowdata.kokobasilus === 'f' || rowdata.kokobasilus === false || rowdata.kokobasilus === '' 
		|| rowdata.kokobasilus === null	|| rowdata.kokobasilus === 'null'){
		Ext.getCmp('chkKokobasilus_MS').setValue(false);
	}else{
		Ext.getCmp('chkKokobasilus_MS').setValue(true);
	}
	
	
	
	if (rowdata.kandida === 'false' || rowdata.kandida === 'f' || rowdata.kandida === false || rowdata.kandida === '' 
		|| rowdata.kandida === null	|| rowdata.kandida === 'null'){
		Ext.getCmp('chkKandida_MS').setValue(false);
	}else{
		Ext.getCmp('chkKandida_MS').setValue(true);
	}
	
	if (rowdata.trikhomonas === 'false' || rowdata.trikhomonas === 'f' || rowdata.trikhomonas === false || rowdata.trikhomonas === '' 
		|| rowdata.trikhomonas === null	|| rowdata.trikhomonas === 'null'){
		Ext.getCmp('chkTrikhomonas_MS').setValue(false);
	}else{
		Ext.getCmp('chkTrikhomonas_MS').setValue(true);
	}
	
	if (rowdata.herpessimplek === 'false' || rowdata.herpessimplek === 'f' || rowdata.herpessimplek === false || rowdata.herpessimplek === ''
		|| rowdata.herpessimplek === null	|| rowdata.herpessimplek === 'null'){
		Ext.getCmp('chkHerpesSimplek_MS').setValue(false);
	}else{
		Ext.getCmp('chkHerpesSimplek_MS').setValue(true);
	}
	
	if (rowdata.hpv === 'false' || rowdata.hpv === 'f' || rowdata.hpv === false || rowdata.hpv === '' 
		|| rowdata.hpv === null	|| rowdata.hpv === 'null'){
		Ext.getCmp('chkHPV_MS').setValue(false);
	}else{
		Ext.getCmp('chkHPV_MS').setValue(true);
	}
	
	if (rowdata.efekradiasi === 'false' || rowdata.efekradiasi === 'f' || rowdata.efekradiasi === false || rowdata.efekradiasi === ''
		|| rowdata.efekradiasi === null	|| rowdata.efekradiasi === 'null'){
		Ext.getCmp('chkEfekRadiasi_MS').setValue(false);
	}else{
		Ext.getCmp('chkEfekRadiasi_MS').setValue(true);
	}
	
	if (rowdata.karsinoma === 'false' || rowdata.karsinoma === 'f' || rowdata.karsinoma === false || rowdata.karsinoma === ''
		|| rowdata.karsinoma === null	|| rowdata.karsinoma === 'null'){
		Ext.getCmp('chkKarsinoma_MS').setValue(false);
	}else{
		Ext.getCmp('chkKarsinoma_MS').setValue(true);
	}
	
	if (rowdata.adenokarsinoma === 'false' || rowdata.adenokarsinoma === 'f' || rowdata.adenokarsinoma === false || rowdata.adenokarsinoma === '' 
		|| rowdata.adenokarsinoma === null || rowdata.adenokarsinoma === 'null'){
		Ext.getCmp('chkAdenokarsinoma_MS').setValue(false);
	}else{
		Ext.getCmp('chkAdenokarsinoma_MS').setValue(true);
	}
	
	if (rowdata.displasia === 'ringan'){
		Ext.getCmp('radDisplasiaRingan_MS').setValue(true);
		Ext.getCmp('radDisplasiaSedang_MS').setValue(false);
		Ext.getCmp('radDisplasiaBerat_MS').setValue(false);
	}else if (rowdata.displasia === 'sedang'){
		Ext.getCmp('radDisplasiaRingan_MS').setValue(false);
		Ext.getCmp('radDisplasiaSedang_MS').setValue(true);
		Ext.getCmp('radDisplasiaBerat_MS').setValue(false);
	}else if (rowdata.displasia === 'berat'){
		Ext.getCmp('radDisplasiaRingan_MS').setValue(false);
		Ext.getCmp('radDisplasiaSedang_MS').setValue(false);
		Ext.getCmp('radDisplasiaBerat_MS').setValue(true);
	}
	Ext.getCmp('TxtKdUnit_LabAnatomi').setValue(rowdata.kd_unit);
	//alert(rowdata.kd_unit);
	//dataGridDiagnosaLabPA(rowdata.kd_pasien, rowdata.kd_unit, rowdata.tgl_masuk, rowdata.urut_masuk);
	
	
};

//-----------------------------------------MENGHITUNG USIA DALAM LOOKUP------------------------------------------------------------
function setUsia_LabAnatomi(Tanggal)
{

	Ext.Ajax.request
		( 
		{
		   url: baseURL + "index.php/main/GetUmur",
		   params: {
					TanggalLahir: Tanggal
		   },
		   
		   success: function(o)
		   {
				//alert('test');  
			
				var tmphasil = o.responseText;
				 //alert(tmphasil);
				var tmp = tmphasil.split(' ');
				//alert(tmp.length);
				if (tmp.length == 6)
				{
					Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue(tmp[0]);
					Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue(tmp[2]);
					Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue(tmp[4]);
				}
				else if(tmp.length == 4)
				{
					if(tmp[1]== 'years' && tmp[3] == 'day')
					{
						Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue(tmp[2]);  
					}else{
					Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue(tmp[0]);
					Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue(tmp[2]);
						  }
				}
				else if(tmp.length == 2 )
				{
					
					if (tmp[1] == 'year' )
					{
						Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue('0');
					}
					else if (tmp[1] == 'years' )
					{
						Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue('0');
					}
					else if (tmp[1] == 'mon'  )
					{
						Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue('0');
						Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue(tmp[0]);
						Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue('0');
					}
					else if (tmp[1] == 'mons'  )
					{
						Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue('0');
						Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue(tmp[0]);
						Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue('0');
					}
					else{
							Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue('0');
							Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue('0');
							Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue(tmp[0]);
						}
				}
				
				else if(tmp.length == 1)
				{
					Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').setValue('0');
					Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').setValue('1');
				}else
				{
					alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
				}
		   }
		   
		   
		}
		);


	
}




//------------------MENAMPILKAN DETAIL HASIL LAB---------------------------------------------------

function dataGridListPasienLabPA(kd_pasien,nama,tglAwal,tglAkhir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/lab_pa/functionLABPA/getGridListPasien",
			params: {
				kd_pasien:kd_pasien,
				nama:nama,
				tglAwal:tglAwal,
				tglAkhir:tglAkhir
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorHasilLabAnatomi('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsTRListHasilLabAnatomi.removeAll();
					var recs=[],
					recType=dsTRListHasilLabAnatomi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsTRListHasilLabAnatomi.add(recs);
					grListPasienHasilLabAnatomi.getView().refresh();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorHasilLabAnatomi('Gagal membaca list data pasien', 'Error');
				};
			}
		}
		
	)
	
}

function dataGridDiagnosaLabPA(kd_pasien, kd_unit, tgl_masuk, urut_masuk){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/lab_pa/functionLABPA/getGridDiagnosa",
			params: {
				kd_pasien:kd_pasien,
				kd_unit:kd_unit, 
				tgl_masuk:tgl_masuk, 
				urut_masuk:urut_masuk
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorHasilLabAnatomi('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDiagnosaLabAnatomi.removeAll();
					var recs=[],
					recType=dsDiagnosaLabAnatomi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsDiagnosaLabAnatomi.add(recs);
					gridDTHasilLabAnatomi.getView().refresh();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorHasilLabAnatomi('Gagal membaca list data pasien', 'Error');
				};
			}
		}
		
	)
	
}

function DataPrint_HasilLabAnatomi(format) 
{
	var params={
		no_transaksi:Ext.getCmp('TxtNoTransaksi_LabAnatomi').getValue(),
		no_pa:tmpno_pa_labanatomi,
		kd_pasien:Ext.getCmp('TxtPopupMedrec_LabAnatomi').getValue(),
		nama:Ext.getCmp('TxtPopupNamaPasien_LabAnatomi').getValue(),
		tahun:Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').getValue(),
		bulan:Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').getValue(),
		hari:Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').getValue(),
		jk:Ext.getCmp('TxtJK_LabAnatomi').getValue(),
		unitasal:tmpnama_unit_asal_labanatomi,
		kd_unit:Ext.getCmp('TxtKdUnit_LabAnatomi').getValue(),
		urut_masuk:Ext.getCmp('TxtUrutMasuk_LabAnatomi').getValue(),
		kd_dokter_lab:Ext.getCmp('TxtPopKddokter_LabAnatomi').getValue(),
		dokter_lab:Ext.getCmp('TxtNmDokter_LabAnatomi').getValue(),
		dokter_pengirim:tmpnama_dokter_asal_labanatomi,
		kd_dokter_pengirim:tmpkd_dokter_asal_labanatomi,
		tgl_masuk:Ext.getCmp('dPopupTglMasuk_LabAnatomi').getValue(),
		tgl_hasil:tmptgl_hasil_labanatomi,
		makroskopik:Ext.getCmp('TxtMakroskopik_LabAnatomi').getValue(),
		mikroskopik:Ext.getCmp('TxtMikroskopik_LabAnatomi').getValue(),
		kesimpulan:Ext.getCmp('TxtKesimpulan_LabAnatomi').getValue(),
		bahan:Ext.getCmp('TxtBahan_LabAnatomi').getValue(),
		icdot:Ext.getCmp('TxtICDOT_LabAnatomi').getValue(),
		icdom:Ext.getCmp('TxtICDOM_LabAnatomi').getValue(),
		diagklinik:Ext.getCmp('TxtDiagKlinik_LabAnatomi').getValue(),
		diagperiksa:Ext.getCmp('TxtDiagPemeriksaan_LabAnatomi').getValue(),
		noregister:Ext.getCmp('TxtNoRegister_LabAnatomi').getValue(),
		format_lap : format
	} ;
	if (format === 'MI'){
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		form.setAttribute("action", baseURL + "index.php/lab_pa/lap_laboratorium_pa/cetakHasilLabPA_MI");
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
	}else if (format === 'MS'){
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		form.setAttribute("action", baseURL + "index.php/lab_pa/lap_laboratorium_pa/cetakHasilLabPA_MS");
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
	}else{
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		form.setAttribute("action", baseURL + "index.php/lab_pa/lap_laboratorium_pa/cetakHasilLabPA");
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
	}
	

};




function Datasave_HasilLabAnatomi(mBol) 
{	
	if (ValidasiEntryHasilLab(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/lab_pa/functionLABPA/ubahHasil",
				params: getParamHasilLabAnatomi(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanWarningHasilLabAnatomi('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						
						ShowPesanInfoHasilLabAnatomi('Data berhasil disimpan','Information');
						dataGridListPasienLabPA();
					}
					else 
					{
						ShowPesanWarningHasilLabAnatomi('Data gagal disimpan', 'Error');
					};
				}
			}
		)
		
	}
	
};

function DatasaveDiagnosa_HasilLabAnatomi(mBol) 
{	
	if (ValidasiEntryHasilLab(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/lab_pa/functionLABPA/savediagnosa",
				params: getParamDiagnosaHasilLabAnatomi(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanWarningHasilLabAnatomi('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoHasilLabAnatomi('Diagnosa berhasil disimpan','Information');
						dataGridListPasienLabPA();
					}
					else 
					{
						ShowPesanWarningHasilLabAnatomi('Diagnosa gagal disimpan', 'Error');
					};
				}
			}
		)
		
	}
	
};



function getdatahasillabcetakLabAnatomi()
{
	var tmpumur='';
	if(Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').getValue() !== ''){
		if(Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').getValue() ==='0' || Ext.get('TxtPopupThnLahirPasien_LabAnatomi').getValue() ===0){
			if(Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').getValue() ==='0' || Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').getValue() ===0){
				tmpumur=Ext.getCmp('TxtPopupHariLahirPasien_LabAnatomi').getValue() + ' Hari';
			} else{
				tmpumur=Ext.getCmp('TxtPopupBlnLahirPasien_LabAnatomi').getValue() + ' Bulan';
			}
		} else{
			tmpumur=Ext.getCmp('TxtPopupThnLahirPasien_LabAnatomi').getValue() + ' Tahun';
		}
	}
     var params = {
        Tgl: Ext.get('dPopupTglMasuk_LabAnatomi').getValue(),
        KdPasien: Ext.get('TxtPopupMedrec_LabAnatomi').getValue(),
        Nama: Ext.get('TxtPopupNamaPasien_LabAnatomi').getValue(),        
        JenisKelamin: Ext.get('TxtJK_LabAnatomi').getValue(),
		Ttl:Ext.get('dPopupTglLahirPasien_LabAnatomi').getValue(),
		Umur:tmpumur,
        Alamat: Ext.get('TxtPopupAlamat_LabAnatomi').getValue(),
        Poli: Ext.get('TxtPopPoli_LabAnatomi').getValue(),
        urutmasuk: Ext.get('TxtUrutMasuk_LabAnatomi').getValue(),
        Dokter:Ext.get('TxtNmDokter_LabAnatomi').getValue(),
		NamaUnitAsal:tmpnama_unit_asal_labanatomi,
		KdDokterAsal:tmpkd_dokter_asal_labanatomi,
		JamMasuk:tmpjam_masuk_labanatomi,
		NamaDokterAsal:tmpnama_dokter_asal_labanatomi
    };
    return params;
}


function getParamHasilLabAnatomi() 
{
    var params =
	{
		KdPasien: Ext.getCmp('TxtPopupMedrec_LabAnatomi').getValue(),
		TglMasuk: Ext.get('dPopupTglMasuk_LabAnatomi').dom.value,
		UrutMasuk:Ext.getCmp('TxtUrutMasuk_LabAnatomi').getValue(),
		Makroskopik:Ext.getCmp('TxtMakroskopik_LabAnatomi').getValue(),
		Mikroskopik:Ext.getCmp('TxtMikroskopik_LabAnatomi').getValue(),
		Kesimpulan:Ext.getCmp('TxtKesimpulan_LabAnatomi').getValue(),
		bahan:Ext.getCmp('TxtBahan_LabAnatomi').getValue(),
		icdot:Ext.getCmp('TxtICDOT_LabAnatomi').getValue(),
		icdom:Ext.getCmp('TxtICDOM_LabAnatomi').getValue(),
		diagklinik:Ext.getCmp('TxtDiagKlinik_LabAnatomi').getValue(),
		diagperiksa:Ext.getCmp('TxtDiagPemeriksaan_LabAnatomi').getValue(),
		noregister:Ext.getCmp('TxtNoRegister_LabAnatomi').getValue(),
		
		jawabanperiksa: Ext.getCmp('TxtJawabanPemeriksaan_LabAnatomi').getValue(),
		
		kepuasan : Kepuasan,
		normal : Normal,
		alasan :  Ext.getCmp('TxtAlasanFormatMS_LabAnatomi').getValue(),
		atipia : AtipiaSquoenosa,
		jinak : Jinak,
		radang : Radang,
		inflamasia : Inflamasia,
		metaplasia : Metaplasia,
		kokobasilus : Kokobasilus,
		kandida : Kandida,
		trikhomonas : Trikhomonas,
		herpessimplek : HerpesSimplek,
		hpv : HPV,
		efekradiasi : EfekRadiasi,
		
		displasia : Displasia,
		selkoilosit : SelKoilosit,
		karsinoma : Karsinoma,
		adenokarsinoma : Adenokarsinoma,
		
		kesimpulan_MS : Ext.getCmp('TxtKesimpulanFormatMS_LabAnatomi').getValue(),
		rekomendasi_MS : Ext.getCmp('TxtRekomendasiFormatMS_LabAnatomi').getValue(),
	};
    return params
};

function getParamDiagnosaHasilLabAnatomi(){
	var params =
	{
		KdPasien: Ext.getCmp('TxtPopupMedrec_LabAnatomi').getValue(),
		TglMasuk: Ext.get('dPopupTglMasuk_LabAnatomi').dom.value,
		UrutMasuk:Ext.getCmp('TxtUrutMasuk_LabAnatomi').getValue(),		
	};
	
	params['jumlah']=dsDiagnosaLabAnatomi.getCount();
	for(var i = 0 ; i < dsDiagnosaLabAnatomi.getCount();i++)
	{
		params['kd_penyakit-'+i]=dsDiagnosaLabAnatomi.data.items[i].data.kd_penyakit;
		params['stat_diag-'+i]=dsDiagnosaLabAnatomi.data.items[i].data.stat_diag
		params['kasus-'+i]=dsDiagnosaLabAnatomi.data.items[i].data.kasus
	}
	
    return params
}

function ValidasiEntryHasilLab(modul,mBolHapus)
{
	var x = 1;dsDiagnosaLabAnatomi
	
	if((Ext.get('TxtPopupMedrec_LabAnatomi').getValue() == '') || (Ext.get('dPopupTglCekPasien_LabAnatomi').getValue() == ''))
	{
		if (Ext.get('TxtPopupMedrec_LabAnatomi').getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningHasilLabAnatomi('No.Medrec tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.get('dPopupTglCekPasien_LabAnatomi').getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningHasilLabAnatomi('Tanggal pengisian hasil tidak boleh kosong', 'Warning');
			x = 0;
		}
	};
	return x;
};








function ShowPesanWarningHasilLabAnatomi(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorHasilLabAnatomi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoHasilLabAnatomi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

