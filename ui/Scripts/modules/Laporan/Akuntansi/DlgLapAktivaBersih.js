var dsUnitKerja_LapAktivaBersih;
var selectUnitKerja_LapAktivaBersih;
var now = new Date();
var selectStsApp_LapAktivaBersih;
// var winTitle_LapAktivaBersih =  varLapMainPage +' '+ CurrentPage.title;//'Revisi Anggaran';
var winTitle_LapAktivaBersih =  'Lap. Perubahan Ekuitas'; 

var winDlg_LapAktivaBersih= fnDlg_LapAktivaBersih();
winDlg_LapAktivaBersih.show();
// Ext.get('comboAppRevisiAnggaran').dom.value = "Belum Approve";
var Akumulasi=0;
function fnDlg_LapAktivaBersih()
{   
    var winDlg_LapAktivaBersih = new Ext.Window
    (
        { 
            id: 'winDlg_LapAktivaBersih',
            title: winTitle_LapAktivaBersih,
            closeAction: 'destroy',
            width:390,
            height: 190,
            resizable:false,
            border: false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [getItemDlg_LapAktivaBersih()]          
        }
    );
    
    var chkApprove_LapAktivaBersih = new Ext.grid.CheckColumn
    (
        {
            id: 'chkApprove_LapAktivaBersih',
            header: "APPROVE",
            align: 'center',
            disable:false,
            dataIndex: 'APPROVE',
            width: 70
        }
    );  
    selectUnitKerja_LapAktivaBersih='';
    selectStsApp_LapAktivaBersih='';
    
    return winDlg_LapAktivaBersih; 
};

function getItemDlg_LapAktivaBersih() {
    var Pnl_LapAktivaBersih = new Ext.Panel
    (
        { 
            id: 'Pnl_LapAktivaBersih',
            fileUpload: true,
            layout: 'anchor',
            height: '90',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items: 
            [
                {
                    xtype: 'compositefield',
                    fieldLabel: '',
                    anchor: '100%',
                    items: 
                    [
                        {
                            xtype: 'fieldset',
                            title: '',
                            width: 380,
                            height: 100,
                            items: 
                            [
                                
                                // {
                                    // xtype: 'compositefield',
                                    // fieldLabel: 'Satuan kerja',
                                    // items: 
                                    // [
                                        // mcomboUnitKerja_LapAktivaBersih()
                                    // ]
                                // },
                                {
                                    xtype: 'compositefield',
                                    fieldLabel: 'Periode',
                                    items: 
                                    [
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: '',
                                            name: 'DtpPeriode_LapAktivaBersih',
                                            id: 'DtpPeriode_LapAktivaBersih',
                                            format: 'd/M/Y',
                                            width: 105,
                                            value:now
                                            //anchor: '35%'
                                        },
                        
                                        
                                    ]
                                },
                                {
                                    xtype: 'compositefield',
                                    fieldLabel: 'Draft Mode',
                                    items: 
                                    [
                                        {
                                            xtype: 'checkbox',
                                            id: 'iddraftmode',
                                            boxLabel: '',
                                            listeners: 
                                            {
                                                check: function()
                                                {
                                                }
                                            }
                                        },
                                    ]
                                }
                            ]
                        }
                    ]
                },              
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'21px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig: 
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'rigth'
                    },
                    items: 
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOk_LapAktivaBersih',
                            handler: function() 
                            {
                                if(Validasi_LapAktivaBersih() == 1)
                                {
                                    var params={
                                        periode        :Ext.getCmp('DtpPeriode_LapAktivaBersih').getValue(),
                                        //tag_akumulasi  :Akumulasi,
                                       // bulan          :Ext.getCmp('cboPilihanBulanLapAktivaBersih').getValue(),
                                        //tahun          :Ext.getCmp('cboPilihanTahunLapAktivaBersih').getValue()
                                       
                                    } 
                                    var form = document.createElement("form");
                                    form.setAttribute("method", "post");
                                    form.setAttribute("target", "_blank");
                                    form.setAttribute("action", baseURL + "index.php/akuntansi/functionLapAktivaBersih/cetak");
                                    var hiddenField = document.createElement("input");
                                    hiddenField.setAttribute("type", "hidden");
                                    hiddenField.setAttribute("name", "data");
                                    hiddenField.setAttribute("value", Ext.encode(params));
                                    form.appendChild(hiddenField);
                                    document.body.appendChild(form);
                                    form.submit();  
                                    winDlg_LapAktivaBersih.close();
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel',
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancel_LapAktivaBersih',
                            handler: function() 
                            {
                                winDlg_LapAktivaBersih.close();
                            }
                        }
                    ]
                }
            ]
        }
    );
 
    return Pnl_LapAktivaBersih;
};

function ShowPesanWarningRKAReport_LapAktivaBersih(str,modul)
{
    Ext.MessageBox.show
    (       
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING
        }
    );
};



function mComboTahun_LapAktivaBersih()
{
  var currYear = parseInt(now_LapAktivaBersih.format('Y'));
  var cboTahun_LapAktivaBersih = new Ext.form.ComboBox
    (
        {
            id:'cboTahun_LapAktivaBersih',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Periode ',         
            width:60,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'Id',
                        'displayText'
                    ],
                    data: [[1, currYear + 2], [2, currYear + 1], [3, currYear], [4, currYear - 1], [5, currYear - 2]]
                }
            ),
            valueField: 'displayText',
            displayField: 'displayText',
            value: now_LapAktivaBersih.format('Y')         
        }
    );
    
    return cboTahun_LapAktivaBersih;
};

function mcomboBulanLapAktivaBersih(){
  var cboPilihanBulanLapAktivaBersih = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'cboPilihanBulanLapAktivaBersih',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: '',
                disable:0,
                width: 50,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, '1'], [2, '2'], [2, '2'], [3, '3'], [4, '4'], [5, '5'], [6, '6'], [7, '7'], [8, '8'], [9, '9'], [10, '10'], [11, '11'], [12, '12']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                //value:selectSetPilihanAsalPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                          //  selectSetPilihanAsalPasien=b.data.displayText;
                           // Combo_Select(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanBulanLapAktivaBersih;
}

function mcomboTahunLapAktivaBersih(){
  var cboPilihanTahunLapAktivaBersih = new Ext.form.ComboBox
    (
            {
                x: 220,
                y: 70,
                id:'cboPilihanTahunLapAktivaBersih',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: '',
                width: 100,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[2015, '2015'], [2016, '2016'], [2017, '2017'], [2018, '2018'], [2019, '2019'], [2020, '2020']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                //value:selectSetPilihanAsalPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                          //  selectSetPilihanAsalPasien=b.data.displayText;
                           // Combo_Select(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanTahunLapAktivaBersih;
}

function getKriteria_LapAktivaBersih()
{
    var strKriteria = "";   
    
    
    
    strKriteria =  ShowDate(Ext.getCmp('DtpPeriode_LapAktivaBersih').getValue());
    strKriteria +=  "##"+ShowDate(Ext.getCmp('DtpAkhir_LapAktivaBersih').getValue()); ;
    strKriteria += "##test";
    strKriteria += "##"+ selectStsApp_LapAktivaBersih ;    
    strKriteria += ""+ "##"
    + 1 + "##"
    
    return strKriteria;
}

function Validasi_LapAktivaBersih()
{
    var x = 1;
    
    
     if(Ext.getCmp('DtpPeriode_LapAktivaBersih').getValue() == '' )
    {
        ShowPesanWarning_LapAktivaBersih('Periode Harus Di isi');
        x = 0;
    } 
    
     

    return x;
};

function ShowPesanWarning_LapAktivaBersih(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING
        }
    );
};