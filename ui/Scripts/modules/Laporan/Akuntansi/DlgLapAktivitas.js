﻿var dsUnitKerja_LapAktivitas;
var selectUnitKerja_LapAktivitas;
var now = new Date();
var selectStsApp_LapAktivitas;
// var winTitle_LapAktivitas =  varLapMainPage +' '+ CurrentPage.title;//'Revisi Anggaran';
var winTitle_LapAktivitas =  'Lap. Operasional'; 

var winDlg_LapAktivitas= fnDlg_LapAktivitas();
winDlg_LapAktivitas.show();
// Ext.get('comboAppRevisiAnggaran').dom.value = "Belum Approve";


function fnDlg_LapAktivitas()
{  	
    var winDlg_LapAktivitas = new Ext.Window
	(
		{ 
			id: 'winDlg_LapAktivitas',
			title: winTitle_LapAktivitas,
			closeAction: 'destroy',
			width:410,
			height: 200,
			resizable:false,
			border: false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [getItemDlg_LapAktivitas()]			
		}
	);
	
	var chkApprove_LapAktivitas = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprove_LapAktivitas',
			header: "APPROVE",
			align: 'center',
			disable:false,
			dataIndex: 'APPROVE',
			width: 70
		}
	);	
	selectUnitKerja_LapAktivitas='';
	selectStsApp_LapAktivitas='';
	
    return winDlg_LapAktivitas; 
};

function getItemDlg_LapAktivitas() 
{	
	var Pnl_LapAktivitas = new Ext.Panel
	(
		{ 
			id: 'Pnl_LapAktivitas',
			fileUpload: true,
			layout: 'anchor',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 380,
							height: 120,
							items: 
							[
								
								// {
									// xtype: 'compositefield',
									// fieldLabel: 'Satuan kerja',
									// items: 
									// [
										// mcomboUnitKerja_LapAktivitas()
									// ]
								// },
								{
									xtype: 'compositefield',
									fieldLabel: 'Periode',
									items: 
									[
										{
											xtype: 'datefield',
											fieldLabel: '',
											name: 'DtpAwal_LapAktivitas',
											id: 'DtpAwal_LapAktivitas',
											format: 'd/M/Y',
											width: 105,
											value:now
											//anchor: '35%'
										},
										{
											xtype: 'displayfield',
											flex: 1,
											width: 15,
											name: '',
											value: 'S.d',
											fieldLabel: 'Label',
											id: 'lblnopeg_LapAktivitas',
											name: 'lblnopeg_LapAktivitas'
										},
										{
											xtype: 'datefield',
											fieldLabel: 'Tanggal',
											name: 'DtpAkhir_LapAktivitas',
											id: 'DtpAkhir_LapAktivitas',
											format: 'd/M/Y',
											width: 105,
											value:now
											//anchor: '35%'
										}
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Level',
									items: 
									[
										mcomboLevelAkun()
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Draft Mode',
									items: 
									[
										{
											xtype: 'checkbox',
											id: 'iddraftmode',
											boxLabel: '',
											listeners: 
											{
												check: function()
												{
												}
											}
										},
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Excel',
									items: 
									[
										{
											xtype: 'checkbox',
											id: 'idExcel',
											boxLabel: '',
											listeners: 
											{
												check: function()
												{
												}
											}
										},
									]
								}
							]
						}
					]
				},				
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'rigth'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOk_LapAktivitas',
							handler: function() 
							{
								if(Validasi_LapAktivitas() == 1)
								{
									var params={
										tgl_awal		:Ext.getCmp('DtpAwal_LapAktivitas').getValue(),
										tgl_awal_label	:Ext.get('DtpAwal_LapAktivitas').getValue(),
										tgl_akhir		:Ext.getCmp('DtpAkhir_LapAktivitas').getValue(),
										tgl_akhir_label	:Ext.get('DtpAkhir_LapAktivitas').getValue(),
										level			:Ext.getCmp('idcomboLevelAkun').getValue(),
										excel			:Ext.getCmp('idExcel').getValue(),
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/akuntansi/functionLapAktivitas/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									winDlg_LapAktivitas.close();
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancel_LapAktivitas',
							handler: function() 
							{
								winDlg_LapAktivitas.close();
							}
						}
					]
				}
			]
		}
	);
 
    return Pnl_LapAktivitas;
};

function ShowPesanWarningRKAReport_LapAktivitas(str,modul)
{
	Ext.MessageBox.show
	(		
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

// function mcomboUnitKerja_LapAktivitas()
// {
	// var Field = ['KD_UNIT_KERJA', 'NAMA_UNIT_KERJA','UNITKERJA'];
	// dsUnitKerja_LapAktivitas = new WebApp.DataStore({ fields: Field });

  // var comboUnitKerja_LapAktivitas = new Ext.form.ComboBox
	// (
		// {
		
		// // id:Nama_ID,
			// // typeAhead: true,
			// // triggerAction: 'all',
			// // lazyRender:true,
			// // mode: 'local',
			// // emptyText:'',
			// // fieldLabel: "Kategori",			
			// // width:lebar,
		
			// id:'comboUnitKerja_LapAktivitas',
			// typeAhead: true,
			// triggerAction: 'all',
			// lazyRender:true,
			// mode: 'local',
			// emptyText:'Semua Unit/Sub Satuan kerja',
			// fieldLabel: 'Satuan kerja ',			
			// align:'Right',
			// width:240,
			// valueField: 'KD_UNIT_KERJA',
			// displayField: 'UNITKERJA',
			// store:dsUnitKerja_LapAktivitas,
			// listeners:  
			// {
				// 'select': function(a,b,c)
				// {   
					// selectUnitKerja_LapAktivitas=b.data.KD_UNIT_KERJA ;
				// } 
			// }
		// }
	// );

    // dsUnitKerja_LapAktivitas.load
	// (
		// { 
			// params: 
			// { 
				// Skip: 0, 
				// Take: 1000, 
				// Sort: 'KD_UNIT_KERJA', 
				// Sortdir: 'ASC', 
				// target:'viCboUnitKerja',
				// param: "kdunit=" + gstrListUnitKerja
			// } 
		// }
	// );
	// return comboUnitKerja_LapAktivitas;
// };

function mComboTahun_LapAktivitas()
{
  var currYear = parseInt(now_LapAktivitas.format('Y'));
  var cboTahun_LapAktivitas = new Ext.form.ComboBox
	(
		{
			id:'cboTahun_LapAktivitas',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, currYear + 2], [2, currYear + 1], [3, currYear], [4, currYear - 1], [5, currYear - 2]]
				}
			),
			valueField: 'displayText',
			displayField: 'displayText',
			value: now_LapAktivitas.format('Y')			
		}
	);
	
	return cboTahun_LapAktivitas;
};

function mcomboLevelAkun()
{	
	var Field = ['levels'];
	var dslevelakun = new WebApp.DataStore({ fields: Field });
	
	 Ext.Ajax.request ({
		url: baseURL + "index.php/akuntansi/functionLapAktivitas/getLevelAkun",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dslevelakun.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dslevelakun.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dslevelakun.add(recs);
				
			} else {
				
			};
		}
	});
	var idcomboLevelAkun = new Ext.form.ComboBox
	(
		{
			id:'idcomboLevelAkun',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Level',			
			align:'Right',
			width:50,
			store: dslevelakun,
			valueField: 'levels',
			displayField: 'levels',
			// value:'0',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					// selectStsApp_LapAktivitas=b.data.Id ;					
				} 
			}
		}
	);
	
	return idcomboLevelAkun;
} ;

// function mcomboLapRekap()
// {
	// var Field = ['Account','Name','Groups','AKUN'];
	// dsLapRekap = new WebApp.DataStore({ fields: Field });
	// dsLapRekap.load
	// (
		// {
			// params:
			// {
			    // Skip: 0,
			    // Take: 1000,
			    // Sort: '',
			    // Sortdir: 'ASC',
			    // target: 'viewCboaktivaFilter',
			    // param: 'REKAP'
			// }
		// }
	// );
	
 // var comboLapRekap = new Ext.form.ComboBox
	// (
		// {
			// id:'comboLapRekap',
			// typeAhead: true,
			// triggerAction: 'all',
			// lazyRender:true,
			// mode: 'local',
			// emptyText:'Pilih Kas / Bank...',
			// fieldLabel: 'Kas / Bank ',			
			// align:'Right',
			// width:240,
			// store: dsLapRekap,
			// valueField: 'Account',
			// displayField: 'AKUN',
			// listeners:  
			// {
				// 'select': function(a,b,c)
				// {   
					// selectStsApp_LapAktivitas=b.data.Account ;
					
				// } 
			// }
		// }
	// );
	
	// return comboLapRekap;
// } ;


/* function getItemKelompokRKA_LapAktivitas()
{
	var cboKelRKA_LapAktivitas = new Ext.form.ComboBox
	(
		{
			id:'optPN_LapAktivitas',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Jenis ',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[2, "Pengembangan"], [1, "Rutin"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboKelRKA_LapAktivitas;
} */

function getKriteria_LapAktivitas()
{
	var strKriteria = "";	
	
	
	
	strKriteria =  ShowDate(Ext.getCmp('DtpAwal_LapAktivitas').getValue());
	strKriteria +=  "##"+ShowDate(Ext.getCmp('DtpAkhir_LapAktivitas').getValue()); ;
	strKriteria += "##test";
	strKriteria += "##"+ selectStsApp_LapAktivitas ;	
	strKriteria += ""+ "##"
	+ 1 + "##"
	
	return strKriteria;
}

function Validasi_LapAktivitas()
{
	var x = 1;
	
	if(Ext.getCmp('DtpAkhir_LapAktivitas').getValue() == '')
	{
		ShowPesanWarning_LapAktivitas('Tanggal Akhir Belum di isi!','Laporan Revisi RAB ');
		x = 0;
	}
	if(Ext.getCmp('DtpAwal_LapAktivitas').getValue() == '')
	{
		ShowPesanWarning_LapAktivitas('Tanggal Awal Belum di isi!','Laporan Revisi RAB ');
		x = 0;
	}
	/* if(Ext.getCmp('comboUnitKerja_LapAktivitas').getValue() == '' )
	{
		ShowPesanWarning_LapAktivitas('Pilihan Unit/Sub Satuan kerja salah!','Laporan RAPB Pengembangan');
		x = 0;
	} 
	*/
	/* if(Ext.getCmp('comboAppRevisiAnggaran').getValue() == '' )
	{
		ShowPesanWarning_LapAktivitas('Status Approve belum dipilih!','Laporan Revisi RAB ');
		x = 0;
	} */

	return x;
};

function ShowPesanWarning_LapAktivitas(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};