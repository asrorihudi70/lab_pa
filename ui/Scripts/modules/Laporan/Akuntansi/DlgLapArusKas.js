var dsUnitKerja_LapArusKas;
var selectUnitKerja_LapArusKas;
var now = new Date();
var selectStsApp_LapArusKas;
// var winTitle_LapArusKas =  varLapMainPage +' '+ CurrentPage.title;//'Revisi Anggaran';
var winTitle_LapArusKas =  'Laporan Arus Kas'; 

var winDlg_LapArusKas= fnDlg_LapArusKas();
winDlg_LapArusKas.show();
// Ext.get('comboAppRevisiAnggaran').dom.value = "Belum Approve";
var Akumulasi=0;
function fnDlg_LapArusKas()
{   
    var winDlg_LapArusKas = new Ext.Window
    (
        { 
            id: 'winDlg_LapArusKas',
            title: winTitle_LapArusKas,
            closeAction: 'destroy',
            width:320,
            height: 190,
            resizable:false,
            border: false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [getItemDlg_LapArusKas()]          
        }
    );
    
    var chkApprove_LapArusKas = new Ext.grid.CheckColumn
    (
        {
            id: 'chkApprove_LapArusKas',
            header: "APPROVE",
            align: 'center',
            disable:false,
            dataIndex: 'APPROVE',
            width: 70
        }
    );  
    selectUnitKerja_LapArusKas='';
    selectStsApp_LapArusKas='';
    
    return winDlg_LapArusKas; 
};

function getItemDlg_LapArusKas() {
    var Pnl_LapArusKas = new Ext.Panel
    (
        { 
            id: 'Pnl_LapArusKas',
            fileUpload: true,
            layout: 'anchor',
            height: '90',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items: 
            [
                {
                    xtype: 'compositefield',
                    fieldLabel: '',
                    anchor: '100%',
                    items: 
                    [
                        {
                            xtype: 'fieldset',
                            title: '',
                            width: 280,
                            height: 100,
                            items: 
                            [
                                
                                // {
                                    // xtype: 'compositefield',
                                    // fieldLabel: 'Satuan kerja',
                                    // items: 
                                    // [
                                        // mcomboUnitKerja_LapArusKas()
                                    // ]
                                // },
                                {
                                    xtype: 'compositefield',
                                    fieldLabel: 'Periode',
                                    items: 
                                    [
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: '',
                                            name: 'DtpPeriode_LapArusKas',
                                            id: 'DtpPeriode_LapArusKas',
                                            format: 'd/M/Y',
                                            width: 105,
                                            value:now
                                            //anchor: '35%'
                                        },
                        
                                        
                                    ]
                                },
                                {
                                    xtype: 'compositefield',
                                    fieldLabel: 'Akumulasi Bulan',
									hidden:true,
                                    items: 
                                    [
                                        {
                                            xtype: 'checkbox',
                                            id: 'idakumbulan',
                                            boxLabel: '',
                                            listeners: 
                                            {
                                                check: function(){
                                                   Akumulasi=1; 
                                                   console.log(Akumulasi);
                                                   Ext.getCmp('cboPilihanBulanLapArusKas').enable();
                                                }
                                            }
                                        },
                                        mcomboBulanLapArusKas(),
                                        mcomboTahunLapArusKas()
                                    ]
                                },
                                {
                                    xtype: 'compositefield',
                                    fieldLabel: 'Draft Mode',
                                    items: 
                                    [
                                        {
                                            xtype: 'checkbox',
                                            id: 'iddraftmode',
                                            boxLabel: '',
                                            listeners: 
                                            {
                                                check: function()
                                                {
                                                }
                                            }
                                        },
                                    ]
                                }
                            ]
                        }
                    ]
                },              
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'21px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig: 
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'rigth'
                    },
                    items: 
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOk_LapArusKas',
                            handler: function() 
                            {
                                // if(Validasi_LapArusKas() == 1)
                                // {
                                    var params={
                                        periode        :Ext.getCmp('DtpPeriode_LapArusKas').getValue()
                                       
                                    } 
                                    var form = document.createElement("form");
                                    form.setAttribute("method", "post");
                                    form.setAttribute("target", "_blank");
                                    form.setAttribute("action", baseURL + "index.php/akuntansi/functionLapArusKas/laporan");
                                    var hiddenField = document.createElement("input");
                                    hiddenField.setAttribute("type", "hidden");
                                    hiddenField.setAttribute("name", "data");
                                    hiddenField.setAttribute("value", Ext.encode(params));
                                    form.appendChild(hiddenField);
                                    document.body.appendChild(form);
                                    form.submit();  
                                    winDlg_LapArusKas.close();
                                // }
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel',
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancel_LapArusKas',
                            handler: function() 
                            {
                                winDlg_LapArusKas.close();
                            }
                        }
                    ]
                }
            ]
        }
    );
 
    return Pnl_LapArusKas;
};

function ShowPesanWarningRKAReport_LapArusKas(str,modul)
{
    Ext.MessageBox.show
    (       
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING
        }
    );
};



function mComboTahun_LapArusKas()
{
  var currYear = parseInt(now_LapArusKas.format('Y'));
  var cboTahun_LapArusKas = new Ext.form.ComboBox
    (
        {
            id:'cboTahun_LapArusKas',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Periode ',         
            width:60,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'Id',
                        'displayText'
                    ],
                    data: [[1, currYear + 2], [2, currYear + 1], [3, currYear], [4, currYear - 1], [5, currYear - 2]]
                }
            ),
            valueField: 'displayText',
            displayField: 'displayText',
            value: now_LapArusKas.format('Y')         
        }
    );
    
    return cboTahun_LapArusKas;
};

function mcomboBulanLapArusKas(){
  var cboPilihanBulanLapArusKas = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'cboPilihanBulanLapArusKas',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: '',
                disable:0,
                width: 50,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, '1'], [2, '2'], [2, '2'], [3, '3'], [4, '4'], [5, '5'], [6, '6'], [7, '7'], [8, '8'], [9, '9'], [10, '10'], [11, '11'], [12, '12']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                //value:selectSetPilihanAsalPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                          //  selectSetPilihanAsalPasien=b.data.displayText;
                           // Combo_Select(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanBulanLapArusKas;
}

function mcomboTahunLapArusKas(){
  var cboPilihanTahunLapArusKas = new Ext.form.ComboBox
    (
            {
                x: 220,
                y: 70,
                id:'cboPilihanTahunLapArusKas',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: '',
                width: 100,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[2015, '2015'], [2016, '2016'], [2017, '2017'], [2018, '2018'], [2019, '2019'], [2020, '2020']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                //value:selectSetPilihanAsalPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                          //  selectSetPilihanAsalPasien=b.data.displayText;
                           // Combo_Select(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanTahunLapArusKas;
}

function getKriteria_LapArusKas()
{
    var strKriteria = "";   
    
    
    
    strKriteria =  ShowDate(Ext.getCmp('DtpPeriode_LapArusKas').getValue());
    strKriteria +=  "##"+ShowDate(Ext.getCmp('DtpAkhir_LapArusKas').getValue()); ;
    strKriteria += "##test";
    strKriteria += "##"+ selectStsApp_LapArusKas ;    
    strKriteria += ""+ "##"
    + 1 + "##"
    
    return strKriteria;
}

function Validasi_LapArusKas()
{
    var x = 1;
    
    if(Ext.getCmp('DtpPeriode_LapArusKas').getValue() == '')
    {
        ShowPesanWarning_LapArusKas('Periode Belum di isi!','Laporan Arus Kas');
        x = 0;
    }
     if(Ext.getCmp('cboPilihanBulanLapArusKas').getValue() == '' )
    {
        ShowPesanWarning_LapArusKas('Akumulasi Bulan Harus Di isi!','Laporan Arus Kas');
        x = 0;
    } 
    
     if(Ext.getCmp('cboPilihanTahunLapArusKas').getValue() == '' )
    {
        ShowPesanWarning_LapArusKas('Akumulasi Bulan Harus Di isi!','Laporan Arus Kas');
        x = 0;
    } 

    return x;
};

function ShowPesanWarning_LapArusKas(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING
        }
    );
};