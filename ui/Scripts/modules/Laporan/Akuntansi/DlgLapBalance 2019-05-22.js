
var dsAkuntansiSumAkuntansi;
var selectAkuntansiSumAkuntansi;
var selectNamaAkuntansiSumAkuntansi;
var now = new Date();
var selectSetUNIT='All';
var frmDlgAkuntansiSumAkuntansi;
var varLapAkuntansiSumAkuntansi= ShowFormLapAkuntansiSumAkuntansi();
var dsAkuntansi;
function ShowFormLapAkuntansiSumAkuntansi()
{
    frmDlgAkuntansiSumAkuntansi= fnDlgAkuntansiSumAkuntansi();
    frmDlgAkuntansiSumAkuntansi.show();
};

function fnDlgAkuntansiSumAkuntansi()
{
    var winAkuntansiSumAkuntansiReport = new Ext.Window
    (
        {
            id: 'winAkuntansiSumAkuntansiReport',
            title: 'Balance Sheet',
            closeAction: 'destroy',
            width:450,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgAkuntansiSumAkuntansi()]

        }
    );

    return winAkuntansiSumAkuntansiReport;
};


function ItemDlgAkuntansiSumAkuntansi()
{
    var PnlLapAkuntansiSumAkuntansi = new Ext.Panel
    (
        {
            id: 'PnlLapAkuntansiSumAkuntansi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
               getItemLapAkuntansicboAkuntansi(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Cetak',
                            width: 70,
                            hideLabel: true,
                            style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnOkLapAkuntansiSumAkuntansi',
                            handler: function()
                            {
                                window.open( baseURL + "index.php/main/balance/cetak/"+Ext.getCmp('cboBulan').getValue()+"/"+Ext.getCmp('txttahun_balacesheet').getValue());	
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Keluar',
                            width: 70,
                            hideLabel: true,
						 style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnCancelLapAkuntansiSumAkuntansi',
                            handler: function()
                            {
                                    frmDlgAkuntansiSumAkuntansi.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapAkuntansiSumAkuntansi;
};

function GetCriteriaAkuntansiSumAkuntansi()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapAkuntansiSumAkuntansi').dom.value !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapAkuntansiSumAkuntansi').dom.value;
	};
	if (Ext.get('dtpTglAkhirLapAkuntansiSumAkuntansi').dom.value !== '')
	{
		strKriteria += '##@@##'+Ext.get('dtpTglAkhirLapAkuntansiSumAkuntansi').dom.value;
	};
	if (selectSetUNIT !== undefined)
	{
		strKriteria += '##@@##' + selectSetUNIT;
	};

	return strKriteria;
};


function ValidasiTanggalReportAkuntansiSumAkuntansi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapAkuntansiSumAkuntansi').dom.value > Ext.get('dtpTglAkhirLapAkuntansiSumAkuntansi').dom.value)
    {
        ShowPesanWarningAkuntansiSumAkuntansiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningAkuntansiSumAkuntansiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapAkuntansiSumAkuntansi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};




function getItemLapAkuntansicboAkuntansi()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .43,
			    layout: 'form',
				labelWidth:60,
				border: false,
			    items:
				[
				
					mComboBulan()
					
				]
			},
			{
			    columnWidth: .57,
			    layout: 'form',
				labelWidth:60,
				border: false,
			    items:
				[
					{
						xtype: 'numberfield',
						anchor:'100%',
						id: 'txttahun_balacesheet',
						fieldLabel:'Tahun',
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
								
								}
							}
						}
					}
					
				]
			
			}
        ]
    }
    return items;
};



function mComboBulan()
{
    var cboBulan = new Ext.form.ComboBox
	(
		{
			id:'cboBulan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih...',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Bulan ',
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, '1'], [2, '2'],[3, '3'], [4, '4'],[5, '5'], [6, '6'],[7, '7'], [8, '8'],
						 [9, '9'],[10, '10'], [11, '11'],[12, '12']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'',
			listeners:
			{
				'select': function(a,b,c)
				{
					//selectSetWarga=b.data.Id ;
                            
                                        //alert(StatusWargaNegara)
				},
                                'render': function(c)
                                {
                                 /*   c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    
                                    }, c);*/
                                }
			}
		}
	);
	return cboBulan;
};
