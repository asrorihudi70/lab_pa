var dsUnitKerja_LapBukuBesars;
var selectUnitKerja_LapBukuBesars;
var now = new Date();
var selectStsApp_LapBukuBesars;
// var winTitle_LapBukuBesars =  varLapMainPage +' '+ CurrentPage.title;//'Revisi Anggaran';
var winTitle_LapBukuBesars =  'Laporan Buku Besar'; 
var txt_no_akun_dari='';
var txt_no_akun_sampai='';
var posted='false';
var dsDaftarAkun_dari=new Ext.data.ArrayStore({id: 0,fields: ['text','account','name'],data: []});
var dsDaftarAkun_sampai=new Ext.data.ArrayStore({id: 0,fields: ['text','account','name'],data: []});
var winDlg_LapBukuBesars= fnDlg_LapBukuBesars();
winDlg_LapBukuBesars.show();
// Ext.get('comboAppRevisiAnggaran').dom.value = "Belum Approve";
var Akumulasi=0;
function fnDlg_LapBukuBesars()
{   
    var winDlg_LapBukuBesars = new Ext.Window
    (
        { 
            id: 'winDlg_LapBukuBesars',
            title: winTitle_LapBukuBesars,
            closeAction: 'destroy',
            width:390,
            height: 210,
            resizable:false,
            border: false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [getItemDlg_LapBukuBesars()]          
        }
    );
    
    var chkApprove_LapBukuBesars = new Ext.grid.CheckColumn
    (
        {
            id: 'chkApprove_LapBukuBesars',
            header: "APPROVE",
            align: 'center',
            disable:false,
            dataIndex: 'APPROVE',
            width: 70
        }
    );  
    selectUnitKerja_LapBukuBesars='';
    selectStsApp_LapBukuBesars='';
    
    return winDlg_LapBukuBesars; 
};

function getItemDlg_LapBukuBesars() {
     daftar_account_sampai= Nci.form.Combobox.autoComplete({
                x: 120,
                y: 40,
                store   : dsDaftarAkun_dari,
                keydown:function(text,e){
                    var nav=navigator.platform.match("Mac");
                    if (e.keyCode == 9 || e.keyCode == 13){
                         Ext.getCmp('txt_no_akun_sampai').focus(false, 200);
                    }
                },
                select  : function(a,b,c){                           
                        txt_no_akun_sampai=b.data.account;                 
                },
                insert  : function(o){
                    return {
                        account         : o.account,
                        name            : o.name,
                        text            :  '<table style="font-size: 11px;"><tr><td width="50">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
                    }
                },
                keypress:function(c,e){
                    console.log('a');
                    if (e.keyCode == 13 || e.keyCode == 9){
                        //Ext.getCmp('txt_no_akun_sampai').focus(false, 200);
                    }
                },
                url: baseURL + "index.php/anggaran/penerimaan/accounts",
                valueField: 'account',
                displayField: 'text',
                tabIndex: 1,
                listWidth: 300,
                width:100,
                enableKeyEvents:true,
                tabIndex: 1
            });
      daftar_account_dari= Nci.form.Combobox.autoComplete({
                x: 120,
                y: 40,
                store   : dsDaftarAkun_sampai,
                keydown:function(text,e){
                    var nav=navigator.platform.match("Mac");
                    if (e.keyCode == 9 || e.keyCode == 13){
                         Ext.getCmp('txt_no_akun_sampai').focus(false, 200);
                    }
                },
                select  : function(a,b,c){                           
                        txt_no_akun_dari=b.data.account;                 
                },
                insert  : function(o){
                    return {
                        account         : o.account,
                        name            : o.name,
                        text            :  '<table style="font-size: 11px;"><tr><td width="50">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
                    }
                },
                keypress:function(c,e){
                    console.log('a');
                    if (e.keyCode == 13 || e.keyCode == 9){
                        //Ext.getCmp('txt_no_akun_sampai').focus(false, 200);
                    }
                },
                url: baseURL + "index.php/anggaran/penerimaan/accounts",
                valueField: 'account',
                displayField: 'text',
                tabIndex: 1,
                listWidth: 300,
                width:100,
                enableKeyEvents:true,
                tabIndex: 1
            });
    var Pnl_LapBukuBesars = new Ext.Panel
    (
        { 
            id: 'Pnl_LapBukuBesars',
            fileUpload: true,
            layout: 'anchor',
            height: '90',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items: 
            [
                {
                    xtype: 'compositefield',
                    fieldLabel: '',
                    anchor: '100%',
                    items: 
                    [
                        {
                            xtype: 'fieldset',
                            title: '',
                            width: 380,
                            height: 130,
                            items: 
                            [
                                {
                                    xtype: 'compositefield',
                                    fieldLabel: 'No Akun Dari',
                                    items: 
                                    [
                                      daftar_account_dari,
                                    
                                        
                                    ]
                                },
                                {
                                    xtype: 'compositefield',
                                    fieldLabel: 'Sampai',
                                    items: 
                                    [
                                       /*{
                                          
                                            xtype: 'textfield',
                                            id: 'txt_no_akun_sampai',
                                            name: 'txt_no_akun_sampai',
                                            width: 100,
                                            allowBlank: false,
                                            tabIndex:1,
                                            enableKeyEvents: true,
                                        },*/
                                        daftar_account_sampai,
                                    ]
                                },

                                {
                                    xtype: 'compositefield',
                                    fieldLabel: 'Periode',
                                    items: 
                                    [
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: '',
                                            name: 'DtpPeriode_LapBukuBesars',
                                            id: 'DtpPeriode_LapBukuBesars',
                                            format: 'd/M/Y',
                                            width: 105,
                                            value:now
                                            //anchor: '35%'
                                        },
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: '',
                                            name: 'DtpAkhirPeriode_LapBukuBesars',
                                            id: 'DtpAkhirPeriode_LapBukuBesars',
                                            format: 'd/M/Y',
                                            width: 105,
                                            value:now
                                            //anchor: '35%'
                                        },
                        
                                        
                                    ]
                                },

                                {
                                    xtype: 'compositefield',
                                    fieldLabel: 'Posted Only',
                                    items: 
                                    [
                                        {
                                            xtype: 'checkbox',
                                            id: 'id_draft_mode',
                                            boxLabel: 'Draft',
                                            listeners: 
                                            {
                                                check: function(){
                                                    posted='true'
                                                }
                                            }
                                        },

                                     
                                        {
                                            xtype: 'checkbox',
                                            id: 'id_posted_only',
                                            boxLabel: '',
                                            listeners: 
                                            {
                                                check: function(){
                                                   // posted='true'
                                                }
                                            }
                                        },

                                    ]
                                }
                            ]
                        }
                    ]
                },              
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'21px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig: 
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'rigth'
                    },
                    items: 
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOk_LapBukuBesars',
                            handler: function() 
                            {
                                if(Validasi_LapBukuBesars() == 1)   {
                                    var params={
                                        periode_1      :Ext.getCmp('DtpPeriode_LapBukuBesars').getValue(),
                                        periode_2      :Ext.getCmp('DtpAkhirPeriode_LapBukuBesars').getValue(),
                                        dari_akun      :txt_no_akun_dari,
                                        sampai_akun    :txt_no_akun_sampai,
                                        posted         :posted
                                    } 
                                    var form = document.createElement("form");
                                    form.setAttribute("method", "post");
                                    form.setAttribute("target", "_blank");
                                    form.setAttribute("action", baseURL + "index.php/akuntansi/functionLapBukuBesar/cetak");
                                    var hiddenField = document.createElement("input");
                                    hiddenField.setAttribute("type", "hidden");
                                    hiddenField.setAttribute("name", "data");
                                    hiddenField.setAttribute("value", Ext.encode(params));
                                    form.appendChild(hiddenField);
                                    document.body.appendChild(form);
                                    form.submit();  
                                    txt_no_akun_dari='';
                                    txt_no_akun_sampai='';
                                    winDlg_LapBukuBesars.close();
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel',
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancel_LapBukuBesars',
                            handler: function() 
                            {
                                winDlg_LapBukuBesars.close();
                            }
                        }
                    ]
                }
            ]
        }
    );
 
    return Pnl_LapBukuBesars;
};

function ShowPesanWarningRKAReport_LapBukuBesars(str,modul)
{
    Ext.MessageBox.show
    (       
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING
        }
    );
};



function mComboTahun_LapBukuBesars()
{
  var currYear = parseInt(now_LapBukuBesars.format('Y'));
  var cboTahun_LapBukuBesars = new Ext.form.ComboBox
    (
        {
            id:'cboTahun_LapBukuBesars',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Periode ',         
            width:60,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'Id',
                        'displayText'
                    ],
                    data: [[1, currYear + 2], [2, currYear + 1], [3, currYear], [4, currYear - 1], [5, currYear - 2]]
                }
            ),
            valueField: 'displayText',
            displayField: 'displayText',
            value: now_LapBukuBesars.format('Y')         
        }
    );
    
    return cboTahun_LapBukuBesars;
};

function mcomboBulanLapBukuBesars(){
  var cboPilihanBulanLapBukuBesars = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'cboPilihanBulanLapBukuBesars',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: '',
                disable:0,
                width: 50,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, '1'], [2, '2'], [2, '2'], [3, '3'], [4, '4'], [5, '5'], [6, '6'], [7, '7'], [8, '8'], [9, '9'], [10, '10'], [11, '11'], [12, '12']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                //value:selectSetPilihanAsalPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                          //  selectSetPilihanAsalPasien=b.data.displayText;
                           // Combo_Select(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanBulanLapBukuBesars;
}

function mcomboTahunLapBukuBesars(){
  var cboPilihanTahunLapBukuBesars = new Ext.form.ComboBox
    (
            {
                x: 220,
                y: 70,
                id:'cboPilihanTahunLapBukuBesars',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: '',
                width: 100,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[2015, '2015'], [2016, '2016'], [2017, '2017'], [2018, '2018'], [2019, '2019'], [2020, '2020']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                //value:selectSetPilihanAsalPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                          //  selectSetPilihanAsalPasien=b.data.displayText;
                           // Combo_Select(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanTahunLapBukuBesars;
}

function getKriteria_LapBukuBesars()
{
    var strKriteria = "";   
    
    
    
    strKriteria =  ShowDate(Ext.getCmp('DtpPeriode_LapBukuBesars').getValue());
    strKriteria +=  "##"+ShowDate(Ext.getCmp('DtpAkhir_LapBukuBesars').getValue()); ;
    strKriteria += "##test";
    strKriteria += "##"+ selectStsApp_LapBukuBesars ;    
    strKriteria += ""+ "##"
    + 1 + "##"
    
    return strKriteria;
}

function Validasi_LapBukuBesars()
{
    var x = 1;
    
    if(Ext.getCmp('DtpPeriode_LapBukuBesars').getValue() == '')
    {
        ShowPesanWarning_LapBukuBesars('Periode Belum di isi!','Laporan Arus Kas');
        x = 0;
    }
     if(txt_no_akun_dari==null || txt_no_akun_dari=='' && txt_no_akun_sampai==null || txt_no_akun_sampai=='')
    {
        ShowPesanWarning_LapBukuBesars('Kriteria Akun Harus Di isi!','Laporan Arus Kas');
        x = 0;
    } 
    
    

    return x;
};

function ShowPesanWarning_LapBukuBesars(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING
        }
    );
};

