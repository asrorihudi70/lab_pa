var asalpasienpilihan;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsIGDRegistrasi;
var selectNamaIGDRegistrasi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgIGDRegistrasi;
var varLapIGDRegistrasi= ShowFormLapAMBRegistrasi();
var selectSetUmum;
var selectSetkelpas;
var winAMBRegistrasiReport;
var pilih_asal_pasien;
var selectsetperusahaan_combo;
function ShowFormLapAMBRegistrasi(){
    frmDlgIGDRegistrasi= fnDlgAMBRegistrasi();
  //  frmDlgIGDRegistrasi.show();
};

function fnDlgAMBRegistrasi()
{
     winAMBRegistrasiReport = new Ext.Window
    (
        {
            id: 'winAMBRegistrasiReport',
            title: 'Laporan Registrasi Ambulance',
            closeAction: 'destroy',
            width:400,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgAMBRegistrasi()],
            listeners:
        {
            activate: function()
            {
              
            }
        }

        }
    );

     winAMBRegistrasiReport.show();
    dataaddnew();
};

function dataaddnew()
{
                Ext.getCmp('cboPerseoranganTrIGD').hide();
                Ext.getCmp('cboAsuransiTrIGD').hide();
                Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
                Ext.getCmp('cboUmumTrAMB').show();
                Ext.getCmp('cboPilihanJenisRegistrasiAMBkelompokPasien').setValue('Semua');
                Ext.getCmp('cboUmumTrAMB').setValue('Semua');
            //    Ext.getCmp('cboPilihanJenisRegistrasiAMB').setValue('Summary');
                
}


function ItemDlgAMBRegistrasi()
{
    var PnlLapAMBRegistrasi = new Ext.Panel
    (
        {
            id: 'PnlLapAMBRegistrasi',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [   getItemLapAMBRegistrasi_tengah(),
                getItemLapAMBRegistrasi_Atas(),
                getItemLapAMBRegistrasi_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapAMBRegistrasi',
                            handler: function(){                    
                                if (ValidasiReportAMBRegistrasi() === 1){
                                    console.log(selectsetperusahaan);
                                  if(Ext.getCmp('cboPilihanAsalPasienRegistrasiAMB').getValue()==''){
                                    pilih_asal_pasien='';
                                  }else{
                                     pilih_asal_pasien=Ext.getCmp('cboPilihanAsalPasienRegistrasiAMB').getValue();
                                  }
                                  if(selectsetperusahaan==undefined){
                                     selectsetperusahaan_combo='Semua';
                                  }else{
                                    selectsetperusahaan_combo=selectsetnamaperusahaan;
                                  }
                                    var Shift='';
                                    if (Ext.getCmp('Shift_All_AMBRegistrasi').getValue() === true){  
                                                    Shift =  "IN ('1','2','3','4')";
                                                
                                                }else{
                                                    if (Ext.getCmp('Shift_1_AMBRegistrasi').getValue() === true)
                                                    {
                                                    
                                                        Shift ="IN ('1')";
                                                    }
                                                    if (Ext.getCmp('Shift_2_AMBRegistrasi').getValue() === true)
                                                    {
                                                        
                                                        Shift  ="IN ('2')";
                                                    }
                                                    if (Ext.getCmp('Shift_3_AMBRegistrasi').getValue() === true)
                                                    {
                                                        
                                                        Shift  ="IN ('3','4')";
                                                    }
                                                }
                                                    console.log(selectsetperusahaan_combo);
                                                var params={
                                                    periode_awal: Ext.get('dtpTglAwalFilterIGDRegistrasi').getValue(),
                                                    periode_akhir:Ext.getCmp('dtpTglAkhirFilterIGDRegistrasi').getValue(),
                                                    pasien_asal:pilih_asal_pasien,
                                                    kelompok_pasien:selectsetperusahaan_combo,
                                                    shift:Shift     
                                                    }
                                                                            
                                                    console.log(params);
                                                    var form = document.createElement("form");
                                                    form.setAttribute("method", "post");
                                                    form.setAttribute("target", "_blank");
                                                    form.setAttribute("action", baseURL + "index.php/ambulance/lap_ambulance/laporan_buku_registrasi"); 
                                                    var hiddenField = document.createElement("input");
                                                    hiddenField.setAttribute("type", "hidden");
                                                    hiddenField.setAttribute("name", "data");
                                                    hiddenField.setAttribute("value", Ext.encode(params));
                                                    form.appendChild(hiddenField);
                                                    document.body.appendChild(form);
                                                    form.submit();                                                                            
                                            }
                            
                            }   
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapAMBRegistrasi',
                            handler: function()
                            {
                            winAMBRegistrasiReport.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapAMBRegistrasi;
};

function GetCriteriaAMBRegistrasi(){
    var Shift='';
    if (Ext.getCmp('Shift_All_AMBRegistrasi').getValue() === true){  
        Shift =  "IN ('1','2','3')";
    
    }else{
        if (Ext.getCmp('Shift_1_AMBRegistrasi').getValue() === true)
        {
        
            Shift ="IN ('1')";
        }
        if (Ext.getCmp('Shift_2_AMBRegistrasi').getValue() === true)
        {
            
            Shift  ="IN ('2')";
        }
        if (Ext.getCmp('Shift_3_AMBRegistrasi').getValue() === true)
        {
            
            Shift  ="IN ('3','4')";
        }
    }
    var strKriteria = {
            periode_awal: Ext.get('dtpTglAwalFilterIGDRegistrasi').getValue(),
            periode_akhir:Ext.getCmp('dtpTglAkhirFilterIGDRegistrasi').getValue(),
            pasien_asal:Ext.getCmp('cboPilihanJenisRegistrasiAMB').getValue(),
            kelompok_pasien:Ext.getCmp('cboperoranganRequestEntryTrIGD').getValue(),
            shift:Shift
    }
    return strKriteria;
};

function getKodeReportIGDRegistrasi()
{   var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanJenisRegistrasiAMB').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanJenisRegistrasiAMB').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }   
    return tmppilihan;
}

function ValidasiReportAMBRegistrasi()
{
    var x=1;
    
    if(Ext.get('dtpTglAwalFilterIGDRegistrasi').dom.value > Ext.get('dtpTglAkhirFilterIGDRegistrasi').dom.value)
    {
        ShowPesanWarningIGDRegistrasiReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }
   /* if(Ext.getCmp('cboPilihanJenisRegistrasiAMBkelompokPasien').getValue() === ''){
        ShowPesanWarningIGDRegistrasiReport('Asal Pasien Belum Dipilih','Laporan Registrasi');
        x=0;
    }*/

    if(Ext.getCmp('Shift_All_AMBRegistrasi').getValue() === false && Ext.getCmp('Shift_1_AMBRegistrasi').getValue() === false && Ext.getCmp('Shift_2_AMBRegistrasi').getValue() === false && Ext.getCmp('Shift_3_AMBRegistrasi').getValue() === false){
        ShowPesanWarningIGDRegistrasiReport('Sfift Harus Di pilih !','Laporan Registrasi');
        x=0;
    }
    

    return x;
};

function ShowPesanWarningIGDRegistrasiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapAMBRegistrasi_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien'
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanAsalPasienJenisRegistrasi(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterIGDRegistrasi',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 40,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterIGDRegistrasi',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanAsalPasienJenisRegistrasiKelompokPasien(),
                mComboPerseoranganIGDRegistrasi(),
                mComboAsuransiIGDRegistrasi(),
                mComboPerusahaanIGDRegistrasi(),
                mComboUmumIGDRegistrasi(),
                mComboUmumLabRegis()
            ]
        }]
    };
    return items;
};

function getItemLapAMBRegistrasi_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {boxLabel: 'Semua',name: 'Shift_All_AMBRegistrasi',id : 'Shift_All_AMBRegistrasi',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_AMBRegistrasi').setValue(true);Ext.getCmp('Shift_2_AMBRegistrasi').setValue(true);Ext.getCmp('Shift_3_AMBRegistrasi').setValue(true);Ext.getCmp('Shift_1_AMBRegistrasi').disable();Ext.getCmp('Shift_2_AMBRegistrasi').disable();Ext.getCmp('Shift_3_AMBRegistrasi').disable();}else{Ext.getCmp('Shift_1_AMBRegistrasi').setValue(false);Ext.getCmp('Shift_2_AMBRegistrasi').setValue(false);Ext.getCmp('Shift_3_AMBRegistrasi').setValue(false);Ext.getCmp('Shift_1_AMBRegistrasi').enable();Ext.getCmp('Shift_2_AMBRegistrasi').enable();Ext.getCmp('Shift_3_AMBRegistrasi').enable();}}},
                                    {boxLabel: 'Shift 1',name: 'Shift_1_AMBRegistrasi',id : 'Shift_1_AMBRegistrasi'},
                                    {boxLabel: 'Shift 2',name: 'Shift_2_AMBRegistrasi',id : 'Shift_2_AMBRegistrasi'},
                                    {boxLabel: 'Shift 3',name: 'Shift_3_AMBRegistrasi',id : 'Shift_3_AMBRegistrasi'}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};


function getItemLapAMBRegistrasi_tengah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            /*{
                    xtype: 'fieldset',
                    title: '',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {boxLabel: 'Pendaftaran',name: 'PendaftaranIGDRegistrasi',id : 'PendaftaranIGDRegistrasi'},
                                    {boxLabel: 'Tindakan IGD',name: 'TindakanIGDRegistrasi',id : 'TindakanIGDRegistrasi'}
                               ]
                        }
                    ]
            }*/
        ]
            
    };
    return items;
};


var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;

/*function mComboPilihanAsalPasienJenisRegistrasi()
{

   var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

    ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewComboigdx',
                param: "kd_bagian=3 and type_unit=false"
            }
        }
    )
    var cboPilihanJenisRegistrasiAMB = new Ext.form.ComboBox
    (
             {
              x: 120,
                y: 10,
            id: 'cboPilihanJenisRegistrasiAMB',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poli',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
             width:240,
            tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
                {
                    
                     
                        asalpasienpilihan=b.data.KD_UNIT;
                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    {
                                        Ext.getCmp('cboDokterRequestEntry').focus();
                                        asalpasienpilihan= c.value;
                                    }else if(e.getKey()==9)
                                    {
                                        asalpasienpilihan= c.value; 
                                           
                                    }
                                    }, c);
                                }


        }
        }
    );
    return cboPilihanJenisRegistrasiAMB;
};
*/
function mComboPilihanAsalPasienJenisRegistrasi()
{
  var cboPilihanJenisRegistrasiAMB = new Ext.form.ComboBox
    (
        {
                    id:'cboPilihanAsalPasienRegistrasiAMB',
                    x: 120,
                    y: 10,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 150,
                    emptyText:'Pilih',
                   // fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [['Semua', 'Semua'],['RWJ', 'RWJ'],['IGD', 'IGD'],['RWI', 'RWI']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                  //  value:selectCountStatusLunasByr_viKasirRwj,
                   listeners:
                    {
                    'select': function(a, b, c){
                    
                     
                        asalpasienpilihan=b.data.id;
                    },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    {
                                        Ext.getCmp('cboDokterRequestEntry').focus();
                                        asalpasienpilihan= c.value;
                                    }else if(e.getKey()==9)
                                    {
                                        asalpasienpilihan= c.value; 
                                           
                                    }
                                    }, c);
                                }


        }
        }
    );
    return cboPilihanJenisRegistrasiAMB;
};



function mComboPilihanAsalPasienJenisRegistrasiKelompokPasien()
{
    var cboPilihanJenisRegistrasiAMBkelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'cboPilihanJenisRegistrasiAMBkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPilihankelompokPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanJenisRegistrasiAMBkelompokPasien;
};

function mComboPerseoranganIGDRegistrasi()
{
    var cboPerseoranganTrIGD = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganTrIGD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
    );
    return cboPerseoranganTrIGD;
};

function mComboUmumIGDRegistrasi()
{
  var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboLookupCustomer',
                param: 'jenis_cust=0 order by customer asc'
            }
        }
    );
    var cboperoranganRequestEntryTrIGD = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 100,
            id: 'cboperoranganRequestEntryTrIGD',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Customer...',
            fieldLabel: '',
            align: 'Right',
            store: dsPerusahaanRequestEntry,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            width:240,
            value: selectsetperusahaan,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectsetperusahaan = b.data.KD_CUSTOMER;
                    selectsetnamaperusahaan = b.data.CUSTOMER;
                }
            }
        }
    );

    return cboperoranganRequestEntryTrIGD;

};

function mComboPerusahaanIGDRegistrasi()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboLookupCustomer',
                param: 'jenis_cust=1 order by customer asc'
            }
        }
    );
    var cboPerusahaanRequestEntryTrIGD = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 100,
            id: 'cboPerusahaanRequestEntryTrIGD',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: '',
            align: 'Right',
            store: dsPerusahaanRequestEntry,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            width:240,
            value: selectsetperusahaan,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectsetperusahaan = b.data.KD_CUSTOMER;
                    selectsetnamaperusahaan = b.data.CUSTOMER;
                }
            }
        }
    );

    return cboPerusahaanRequestEntryTrIGD;
};

function mComboAsuransiIGDRegistrasi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: 'jenis_cust=2 order by customer asc'
            }
        }
    );
    var cboAsuransiTrIGD = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 100,
            id:'cboAsuransiTrIGD',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: '',
            align: 'Right',
            width:240,
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            value: selectSetAsuransi,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetAsuransi=b.data.KD_CUSTOMER ;
                    selectsetnamaAsuransi = b.data.CUSTOMER ;
                }
            }
        }
    );
    return cboAsuransiTrIGD;
};
function mComboUmumLabRegis()
{
    var cboUmumTrAMB = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 100,
            id:'cboUmumTrAMB',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            width:240,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[1, 'Semua']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetUmum,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetUmum=b.data.displayText ;
                }
                                
                            
            }
        }
    );
    return cboUmumTrAMB;
};
function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboperoranganRequestEntryTrIGD').show();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrAMB').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboperoranganRequestEntryTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').show();
        Ext.getCmp('cboUmumTrAMB').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboperoranganRequestEntryTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').show();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrAMB').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrAMB').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrAMB').show();
   }
}

function mCombounitIGDRegistrasi()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

    ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryIGDRegistrasi = new Ext.form.ComboBox
    (
        {
            id: 'cbounitRequestEntryIGDRegistrasi',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
                {
                                   
                                }
                    
                }
        }
    )

    return cbounitRequestEntryIGDRegistrasi;
};
