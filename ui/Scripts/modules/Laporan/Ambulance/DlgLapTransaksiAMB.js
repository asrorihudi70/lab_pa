var asalpasienpilihan;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsIGDTransaksi;
var selectNamaIGDTransaksi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgIGDTransaksi;
var varLapIGDTransaksi= ShowFormLapAMBTransaksi();
var selectSetUmum;
var selectSetkelpas;
var winAMBTransaksiReport;
function ShowFormLapAMBTransaksi(){
    frmDlgIGDTransaksi= fnDlgAMBTransaksi();
    frmDlgIGDTransaksi.show();
};

function fnDlgAMBTransaksi()
{
     winAMBTransaksiReport = new Ext.Window
    (
        {
            id: 'winAMBTransaksiReport',
            title: 'Laporan Transaksi Ambulance',
            closeAction: 'destroy',
            width:400,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgAMBTransaksi()],
            listeners:
        {
            activate: function()
            {
              
            }
        }

        }
    );

     winAMBTransaksiReport.show();
	dataaddnew();
};

function dataaddnew()
{
				Ext.getCmp('cboPerseoranganTrIGD').hide();
                Ext.getCmp('cboAsuransiTrIGD').hide();
                Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
                Ext.getCmp('cboUmumTrAMB').show();
				Ext.getCmp('cboPilihanJenisTransaksiAMBkelompokPasien').setValue('Semua');
				Ext.getCmp('cboUmumTrAMB').setValue('Semua');
				Ext.getCmp('cboPilihanJenisTransaksiAMB').setValue('Summary');
                Ext.getCmp('cboPilihanAsalPasien').setValue('Semua');
				
}


function ItemDlgAMBTransaksi()
{
    var PnlLapAMBTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapAMBTransaksi',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [	getItemLapAMBTransaksi_tengah(),
                getItemLapAMBTransaksi_Atas(),
                getItemLapAMBTransaksi_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapAMBTransaksi',
                            handler: function(){					
							    if (ValidasiReportAMBTransaksi() === 1){
									//var params=GetCriteriaAMBTransaksi();	
									var Shift='';
									if (Ext.getCmp('Shift_All_AMBTransaksi').getValue() === true){	
													Shift =  "IN ('1','2','3','4')";
												
												}else{
													if (Ext.getCmp('Shift_1_AMBTransaksi').getValue() === true)
													{
													
														Shift ="IN ('1')";
													}
													if (Ext.getCmp('Shift_2_AMBTransaksi').getValue() === true)
													{
														
														Shift  ="IN ('2')";
													}
													if (Ext.getCmp('Shift_3_AMBTransaksi').getValue() === true)
													{
														
														Shift  ="IN ('3','4')";
													}
												}
										var jenis_transaksi= Ext.getCmp('cboPilihanJenisTransaksiAMB').getValue();
										console.log(jenis_transaksi);
                                        console.log(selectsetperusahaan)
                                        var selectsetperusahaan_combo='';
                                        if(selectsetperusahaan==''||selectsetperusahaan ==undefined){
                                            selectsetperusahaan_combo='Semua';
                                        }
										if(jenis_transaksi==='Summary' || jenis_transaksi=='1'){
                                            if(selectsetperusahaan==''){}
												var params={
													periode_awal: Ext.get('dtpTglAwalFilterIGDTransaksi').getValue(),
											        periode_akhir:Ext.getCmp('dtpTglAkhirFilterIGDTransaksi').getValue(),
											        pasien_asal:Ext.getCmp('cboPilihanAsalPasien').getValue(),
											        kelompok_pasien:selectsetperusahaan_combo,
											        shift:Shift		
													}					
													console.log(params);
													var form = document.createElement("form");
													form.setAttribute("method", "post");
													form.setAttribute("target", "_blank");
													form.setAttribute("action", baseURL + "index.php/ambulance/lap_transaksi_ambulance/cetak_summary"); 
													var hiddenField = document.createElement("input");
													hiddenField.setAttribute("type", "hidden");
													hiddenField.setAttribute("name", "data");
													hiddenField.setAttribute("value", Ext.encode(params));
													form.appendChild(hiddenField);
													document.body.appendChild(form);
													form.submit();
                                        }
										  if(jenis_transaksi=='2'){
												var params={
													periode_awal: Ext.get('dtpTglAwalFilterIGDTransaksi').getValue(),
													periode_akhir:Ext.getCmp('dtpTglAkhirFilterIGDTransaksi').getValue(),
													pasien_asal:Ext.getCmp('cboPilihanAsalPasien').getValue(),
													kelompok_pasien:selectsetperusahaan_combo,
													shift:Shift		
													}						
													console.log(params);
													var form = document.createElement("form");
													form.setAttribute("method", "post");
													form.setAttribute("target", "_blank");
													form.setAttribute("action", baseURL + "index.php/ambulance/lap_transaksi_ambulance/cetak_detail"); 
													var hiddenField = document.createElement("input");
													hiddenField.setAttribute("type", "hidden");
													hiddenField.setAttribute("name", "data");
													hiddenField.setAttribute("value", Ext.encode(params));
													form.appendChild(hiddenField);
													document.body.appendChild(form);
													form.submit();

										}
									
							}
                        }	
                    },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapAMBTransaksi',
                            handler: function()
                            {
                            winAMBTransaksiReport.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapAMBTransaksi;
};

function GetCriteriaAMBTransaksi(){
	var Shift='';
	if (Ext.getCmp('Shift_All_AMBTransaksi').getValue() === true){	
		Shift =  "IN ('1','2','3')";
	
	}else{
		if (Ext.getCmp('Shift_1_AMBTransaksi').getValue() === true)
		{
		
			Shift ="IN ('1')";
		}
		if (Ext.getCmp('Shift_2_AMBTransaksi').getValue() === true)
		{
			
			Shift  ="IN ('2')";
		}
		if (Ext.getCmp('Shift_3_AMBTransaksi').getValue() === true)
		{
			
			Shift  ="IN ('3','4')";
		}
	}
	var strKriteria = {
			periode_awal: Ext.get('dtpTglAwalFilterIGDTransaksi').getValue(),
	        periode_akhir:Ext.getCmp('dtpTglAkhirFilterIGDTransaksi').getValue(),
	        pasien_asal:Ext.getCmp('cboPilihanJenisTransaksiAMB').getValue(),
	        kelompok_pasien:Ext.getCmp('cboperoranganRequestEntryTrIGD').getValue(),
	        shift:Shift
    }
	return strKriteria;
};

function getKodeReportIGDTransaksi()
{   var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanJenisTransaksiAMB').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanJenisTransaksiAMB').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportAMBTransaksi()
{
    var x=1;
	
  /*  if(Ext.get('dtpTglAwalFilterIGDTransaksi').dom.value > Ext.get('dtpTglAkhirFilterIGDTransaksi').dom.value)
    {
        ShowPesanWarningIGDTransaksiReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }*/
	if(Ext.getCmp('cboPilihanJenisTransaksiAMBkelompokPasien').getValue() === ''){
		ShowPesanWarningIGDTransaksiReport('Asal Pasien Belum Dipilih','Laporan Transaksi');
        x=0;
	}

	if(Ext.getCmp('Shift_All_AMBTransaksi').getValue() === false && Ext.getCmp('Shift_1_AMBTransaksi').getValue() === false && Ext.getCmp('Shift_2_AMBTransaksi').getValue() === false && Ext.getCmp('Shift_3_AMBTransaksi').getValue() === false){
		ShowPesanWarningIGDTransaksiReport(nmRequesterRequest,'Laporan Transaksi');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningIGDTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapAMBTransaksi_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 160,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Jenis Laporan '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanAmbJenisTransaksi(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterIGDTransaksi',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 40,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterIGDTransaksi',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanAmbJenisTransaksiKelompokPasien(),
                mComboPerseoranganIGDTransaksi(),
                mComboAsuransiIGDTransaksi(),
                mComboPerusahaanIGDTransaksi(),
                mComboUmumIGDTransaksi(),
		  		mComboUmumLabRegis(),
            {   
                x: 10,
                y: 130,
                xtype: 'label',
                text: 'Asal pasien '
            }, 
            {
                x: 110,
                y: 130,
                xtype: 'label',
                text: ' : '
            },
            mComboPilihanAmbAsalPasien()
            ]
        }]
    };
    return items;
};

function getItemLapAMBTransaksi_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {boxLabel: 'Semua',name: 'Shift_All_AMBTransaksi',id : 'Shift_All_AMBTransaksi',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_AMBTransaksi').setValue(true);Ext.getCmp('Shift_2_AMBTransaksi').setValue(true);Ext.getCmp('Shift_3_AMBTransaksi').setValue(true);Ext.getCmp('Shift_1_AMBTransaksi').disable();Ext.getCmp('Shift_2_AMBTransaksi').disable();Ext.getCmp('Shift_3_AMBTransaksi').disable();}else{Ext.getCmp('Shift_1_AMBTransaksi').setValue(false);Ext.getCmp('Shift_2_AMBTransaksi').setValue(false);Ext.getCmp('Shift_3_AMBTransaksi').setValue(false);Ext.getCmp('Shift_1_AMBTransaksi').enable();Ext.getCmp('Shift_2_AMBTransaksi').enable();Ext.getCmp('Shift_3_AMBTransaksi').enable();}}},
                                    {boxLabel: 'Shift 1',name: 'Shift_1_AMBTransaksi',id : 'Shift_1_AMBTransaksi'},
                                    {boxLabel: 'Shift 2',name: 'Shift_2_AMBTransaksi',id : 'Shift_2_AMBTransaksi'},
                                    {boxLabel: 'Shift 3',name: 'Shift_3_AMBTransaksi',id : 'Shift_3_AMBTransaksi'}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};


function getItemLapAMBTransaksi_tengah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            /*{
                    xtype: 'fieldset',
                    title: '',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
									{boxLabel: 'Pendaftaran',name: 'PendaftaranIGDTransaksi',id : 'PendaftaranIGDTransaksi'},
                                    {boxLabel: 'Tindakan IGD',name: 'TindakanIGDTransaksi',id : 'TindakanIGDTransaksi'}
                               ]
                        }
                    ]
            }*/
        ]
            
    };
    return items;
};


var selectSetPilihanAsalPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;

/*function mComboPilihanAmbJenisTransaksi()
{

   var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewComboigdx',
                param: "kd_bagian=3 and type_unit=false"
            }
        }
    )
    var cboPilihanJenisTransaksiAMB = new Ext.form.ComboBox
	(
             {
			  x: 120,
                y: 10,
            id: 'cboPilihanJenisTransaksiAMB',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poli',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
             width:240,
			tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
				{
					
                     
						asalpasienpilihan=b.data.KD_UNIT;
				},
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    {
										Ext.getCmp('cboDokterRequestEntry').focus();
										asalpasienpilihan= c.value;
									}else if(e.getKey()==9)
									{
										asalpasienpilihan= c.value;	
										   
									}
                                    }, c);
                                }


		}
        }
	);
	return cboPilihanJenisTransaksiAMB;
};
*/
function mComboPilihanAmbJenisTransaksi()
{
  var cboPilihanJenisTransaksiAMB = new Ext.form.ComboBox
    (
        {
                    id:'cboPilihanJenisTransaksiAMB',
                    x: 120,
                	y: 10,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 150,
                    emptyText:'',
                   // fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Summary'],[2, 'Detail']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                  //  value:selectCountStatusLunasByr_viKasirRwj,
                   listeners:
                    {
                    'select': function(a, b, c){
					
                     
						asalpasienpilihan=b.data.id;
					},
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    {
										Ext.getCmp('cboDokterRequestEntry').focus();
										asalpasienpilihan= c.value;
									}else if(e.getKey()==9)
									{
										asalpasienpilihan= c.value;	
										   
									}
                                    }, c);
                                }


		}
        }
    );
    return cboPilihanJenisTransaksiAMB;
};



function mComboPilihanAmbJenisTransaksiKelompokPasien()
{
    var cboPilihanJenisTransaksiAMBkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanJenisTransaksiAMBkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPilihanAsalPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihanAsalPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanJenisTransaksiAMBkelompokPasien;
};

function mComboPerseoranganIGDTransaksi()
{
    var cboPerseoranganTrIGD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganTrIGD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganTrIGD;
};

function mComboUmumIGDTransaksi()
{
  var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by customer asc'
			}
		}
	);
    var cboperoranganRequestEntryTrIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboperoranganRequestEntryTrIGD',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Customer...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboperoranganRequestEntryTrIGD;

};

function mComboPerusahaanIGDTransaksi()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 order by customer asc'
			}
		}
	);
    var cboPerusahaanRequestEntryTrIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryTrIGD',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryTrIGD;
};

function mComboAsuransiIGDTransaksi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: 'jenis_cust=2 order by customer asc'
            }
        }
    );
    var cboAsuransiTrIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiTrIGD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiTrIGD;
};
function mComboUmumLabRegis()
{
    var cboUmumTrAMB = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumTrAMB',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetUmum,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumTrAMB;
};
function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboperoranganRequestEntryTrIGD').show();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrAMB').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboperoranganRequestEntryTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').show();
        Ext.getCmp('cboUmumTrAMB').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboperoranganRequestEntryTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').show();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrAMB').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrAMB').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrAMB').show();
   }
}

function mCombounitIGDTransaksi()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryIGDTransaksi = new Ext.form.ComboBox
    (
        {
            id: 'cbounitRequestEntryIGDTransaksi',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                }
                    
                }
        }
    )

    return cbounitRequestEntryIGDTransaksi;
};

function mComboPilihanAmbAsalPasien()
{
    var cboPilihanAsalPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 130,
                id:'cboPilihanAsalPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Rawat Jalan'], [2, 'Rawat Inap'],[3, 'IGD'],[4,'Semua']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPilihanAsalPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihanAsalPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanAsalPasien;
};