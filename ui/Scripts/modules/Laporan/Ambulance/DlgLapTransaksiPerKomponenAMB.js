var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapTransaksiPerkomponenDetailAMB;
var selectNamaLapTransaksiPerkomponenDetailAMB;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapTransaksiPerkomponenDetailAMB;
var varLapLapTransaksiPerkomponenDetailAMB= ShowFormLapLapTransaksiPerkomponenDetailAMB();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapTransaksiPerkomponenDetailAMB;
var customer;
var kdcustomer;
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var kdkelpas;
var winLapTransaksiPerkomponenDetailAMBReport;
var type_file=0;
function ShowFormLapLapTransaksiPerkomponenDetailAMB()
{
    frmDlgLapTransaksiPerkomponenDetailAMB= fnDlgLapTransaksiPerkomponenDetailAMB();
    frmDlgLapTransaksiPerkomponenDetailAMB.show();
    loadDataComboUserLapTransaksiPerkomponenDetailAMB();
};

function fnDlgLapTransaksiPerkomponenDetailAMB()
{
    winLapTransaksiPerkomponenDetailAMBReport = new Ext.Window
    (
        {
            id: 'winLapTransaksiPerkomponenDetailAMBReport',
            title: 'Laporan Transaksi Perkomponen Detail',
            closeAction: 'destroy',
            width: 370,
            height: 440,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapTransaksiPerkomponenDetailAMB()],
            listeners:
            {
                activate: function()
                {
                    Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailAMB').hide();
                    Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailAMB').hide();
                    Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB').hide();
                    Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailAMB').show();
                }
            },
            fbar:[
                    {
                        xtype: 'button',
                        text: 'OK',
                        width: 70,
                        hideLabel: true,
                        id: 'btnOkLapLapTransaksiPerkomponenDetailAMB',
                        handler: function()
                        {
                            if (ValidasiReportLapTransaksiPerkomponenDetailAMB() === 1)
                            {
                                if (Ext.get('cboPilihanLapTransaksiPerkomponenDetailAMBkelompokPasien').getValue() === 'Semua')
                                {
                                    tipe='Semua';
                                    customer='Semua';
                                    kdcustomer='Semua';
                                } else if (Ext.get('cboPilihanLapTransaksiPerkomponenDetailAMBkelompokPasien').getValue() === 'Perseorangan'){
                                    customer=Ext.get('cboPerseoranganLapTransaksiPerkomponenDetailAMB').getValue();
                                    tipe='Perseorangan';
                                    kdkelpas=Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailAMBkelompokPasien').getValue();
                                    kdcustomer=Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailAMB').getValue();
                                } else if (Ext.get('cboPilihanLapTransaksiPerkomponenDetailAMBkelompokPasien').getValue() === 'Perusahaan'){
                                    customer=Ext.get('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB').getValue();
                                    tipe='Perusahaan';
                                    kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB').getValue();
                                    kdkelpas=Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailAMBkelompokPasien').getValue();
                                } else {
                                    customer=Ext.get('cboAsuransiLapTransaksiPerkomponenDetailAMB').getValue();
                                    tipe='Asuransi';
                                    kdcustomer=Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailAMB').getValue();
                                    kdkelpas=Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailAMBkelompokPasien').getValue();
                                } 
                                
                                if(Ext.getCmp('radioasal').getValue() === true){
                                    periode='tanggal';
                                    tglAwal=Ext.getCmp('dtpTglAwalFilterHasilAMB').getValue();
                                    tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilAMB').getValue();
                                } else{
                                    periode='bulan';
                                    tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilAMB').getValue();
                                    tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilAMB').getValue();
                                }
                                
                                if (Ext.getCmp('Shift_All_LapTransaksiPerkomponenDetailAMB').getValue() === true){
                                    shift='All';
                                    shift1='false';
                                    shift2='false';
                                    shift3='false';
                                }else{
                                    shift='';
                                    if (Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailAMB').getValue() === true){
                                        shift1='true';
                                    } else{
                                        shift1='false';
                                    }
                                    if (Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailAMB').getValue() === true){
                                        shift2='true';
                                    }else{
                                        shift2='false';
                                    }
                                    if (Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailAMB').getValue() === true){
                                        shift3='true';
                                    }else{
                                        shift3='false';
                                    }
                                }
                                console.log(Ext.getCmp('cboPilihanJenisTransaksiAMB').getValue());
                                if(Ext.getCmp('cboPilihanJenisTransaksiAMB').getValue()==='Detail'|| Ext.getCmp('cboPilihanJenisTransaksiAMB').getValue()==2){
                                    console.log('detail');
                                    var params={
                                    asal_pasien:Ext.get('cboPilihanLapTransaksiPerkomponenDetailAMB').getValue(),
                                    tipe:tipe,
                                    kdkelpas:kdkelpas,
                                    customer:customer,
                                    kdcustomer:kdcustomer,
                                    periode:periode,
                                    tglAwal:tglAwal,
                                    tglAkhir:tglAkhir,
                                    shift:shift,
                                    shift1:shift1,
                                    shift2:shift2,
                                    shift3:shift3,
                                    type_file:type_file
                                } ;
                                var form = document.createElement("form");
                                form.setAttribute("method", "post");
                                form.setAttribute("target", "_blank");
                                form.setAttribute("action", baseURL + "index.php/ambulance/lap_transaksi_ambulance/cetak_detail_komponen");
                                var hiddenField = document.createElement("input");
                                hiddenField.setAttribute("type", "hidden");
                                hiddenField.setAttribute("name", "data");
                                hiddenField.setAttribute("value", Ext.encode(params));
                                form.appendChild(hiddenField);
                                document.body.appendChild(form);
                                form.submit();      
                                //winLapTransaksiPerkomponenDetailAMBReport.close();
                                }else{
                                      console.log('Summary');
                                     var params={
                                    asal_pasien:Ext.get('cboPilihanLapTransaksiPerkomponenDetailAMB').getValue(),
                                    tipe:tipe,
                                    kdkelpas:kdkelpas,
                                    customer:customer,
                                    kdcustomer:kdcustomer,
                                    periode:periode,
                                    tglAwal:tglAwal,
                                    tglAkhir:tglAkhir,
                                    shift:shift,
                                    shift1:shift1,
                                    shift2:shift2,
                                    shift3:shift3,
                                    type_file:type_file
                                } ;
                                var form = document.createElement("form");
                                form.setAttribute("method", "post");
                                form.setAttribute("target", "_blank");
                                form.setAttribute("action", baseURL + "index.php/ambulance/lap_transaksi_ambulance/cetak_summary_komponen");
                                var hiddenField = document.createElement("input");
                                hiddenField.setAttribute("type", "hidden");
                                hiddenField.setAttribute("name", "data");
                                hiddenField.setAttribute("value", Ext.encode(params));
                                form.appendChild(hiddenField);
                                document.body.appendChild(form);
                                form.submit();
                                }
                                
                                
                            };
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Cancel' ,
                        width: 70,
                        hideLabel: true,
                        id: 'btnCancelLapLapTransaksiPerkomponenDetailAMB',
                        handler: function()
                        {
                            winLapTransaksiPerkomponenDetailAMBReport.close();
                        }
                    }
            ]

        }
    );

    return winLapTransaksiPerkomponenDetailAMBReport;
};


function ItemDlgLapTransaksiPerkomponenDetailAMB()
{
    var PnlLapLapTransaksiPerkomponenDetailAMB = new Ext.Panel
    (
        {
            id: 'PnlLapLapTransaksiPerkomponenDetailAMB',
            fileUpload: true,
            layout: 'form',
            height: '400',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapTransaksiPerkomponenDetailAMB_Atas(),
                getItemLapLapTransaksiPerkomponenDetailAMB_Batas(),
                getItemLapLapTransaksiPerkomponenDetailAMB_Bawah(),
                getItemLapLapTransaksiPerkomponenDetailAMB_Batas(),
                getItemLapLapTransaksiPerkomponenDetailAMB_Samping(),
              
            ]
        }
    );

    return PnlLapLapTransaksiPerkomponenDetailAMB;
};


function getKodeReportLapTransaksiPerkomponenDetailAMB()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailAMB').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailAMB').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLapTransaksiPerkomponenDetailAMB()
{
    var x=1;
    if(Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailAMB').getValue() === ''){
        ShowPesanWarningLapTransaksiPerkomponenDetailAMBReport('Pasien Belum Dipilih','Warning');
        x=0;
    }
    if(Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailAMBkelompokPasien').getValue() === ''){
        ShowPesanWarningLapTransaksiPerkomponenDetailAMBReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
    }
    if(Ext.get('cboPerseoranganLapTransaksiPerkomponenDetailAMB').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB').getValue() === '' &&  Ext.get('cboAsuransiLapTransaksiPerkomponenDetailAMB').getValue() === '' && Ext.get('cboUmumRegisAMB').getValue() === '' ){
        ShowPesanWarningLapTransaksiPerkomponenDetailAMBReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
    }
   /*  if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapTransaksiPerkomponenDetailAMBReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    } */
    if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapTransaksiPerkomponenDetailAMBReport('Periode belum dipilih','Warning');
        x=0;
    }
    if(Ext.getCmp('Shift_All_LapTransaksiPerkomponenDetailAMB').getValue() === false && Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailAMB').getValue() === false && Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailAMB').getValue() === false && Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailAMB').getValue() === false){
        ShowPesanWarningLapTransaksiPerkomponenDetailAMBReport('Shift Belum Dipilih','Warning');
        x=0;
    }

    return x;
};

function ShowPesanWarningLapTransaksiPerkomponenDetailAMBReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapTransaksiPerkomponenDetailAMB_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 160,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTransaksiPerkomponenDetailAMB(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Jenis Laporan '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanAmbJenisTransaksi(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTransaksiPerkomponenDetailAMBKelompokPasien(),
                mComboPerseoranganLapTransaksiPerkomponenDetailAMB(),
                mComboAsuransiLapTransaksiPerkomponenDetailAMB(),
                mComboPerusahaanLapTransaksiPerkomponenDetailAMB(),
                mComboUmumLapTransaksiPerkomponenDetailAMB(),
            /*{
                x: 10,
                y: 130,
                xtype: 'label',
                text: 'Tipe File '
            }, {
                x: 110,
                y: 130,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 120,
                y: 130,
                xtype: 'checkbox',
                id: 'CekLapPilihType',
                hideLabel:false,
                boxLabel: 'Excel',
                checked: false,
                listeners: 
                {
                    check: function()
                    {
                       if(Ext.getCmp('CekLapPilihType').getValue()===true)
                        {
                            type_file =1;
                        }
                        else
                        {
                            type_file =0;
                            
                        }
                    }
                }
            }*/
            ]
        }]
    };
    return items;
};


function getItemLapLapTransaksiPerkomponenDetailAMB_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapTransaksiPerkomponenDetailAMB_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width:  345,
                height: 50,
                anchor: '100% 100%',
                defaultType: 'checkbox',
                items: 
                [
                
                    {
                        x: 40,
                        y: 10,
                        boxLabel: 'Semua',
                        name: 'Shift_All_LapTransaksiPerkomponenDetailAMB',
                        id : 'Shift_All_LapTransaksiPerkomponenDetailAMB',
                        handler: function (field, value) {
                            if (value === true){
                                Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailAMB').setValue(true);
                                Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailAMB').setValue(true);
                                Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailAMB').setValue(true);
                                Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailAMB').disable();
                                Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailAMB').disable();
                                Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailAMB').disable();
                            }else{
                                Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailAMB').setValue(false);
                                Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailAMB').setValue(false);
                                Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailAMB').setValue(false);
                                Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailAMB').enable();
                                Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailAMB').enable();
                                Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailAMB').enable();
                            }
                        }
                    },
                    {
                        x: 110,
                        y: 10,
                        boxLabel: 'Shift 1',
                        name: 'Shift_1_LapTransaksiPerkomponenDetailAMB',
                        id : 'Shift_1_LapTransaksiPerkomponenDetailAMB'
                    },
                    {
                        x: 180,
                        y: 10,
                        boxLabel: 'Shift 2',
                        name: 'Shift_2_LapTransaksiPerkomponenDetailAMB',
                        id : 'Shift_2_LapTransaksiPerkomponenDetailAMB'
                    },
                    {
                        x: 250,
                        y: 10,
                        boxLabel: 'Shift 3',
                        name: 'Shift_3_LapTransaksiPerkomponenDetailAMB',
                        id : 'Shift_3_LapTransaksiPerkomponenDetailAMB'
                    }
                ]
            }
        ]
            
    };
    return items;
};

function getItemLapLapTransaksiPerkomponenDetailAMB_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
                    if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilAMB').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilAMB').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilAMB').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilAMB').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilAMB').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilAMB').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilAMB').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilAMB').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
            {
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilAMB',
                format: 'd/M/Y',
                value: now
            }, 
            {
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
            {
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilAMB',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilAMB').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilAMB').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilAMB').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilAMB').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilAMB').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilAMB').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilAMB').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilAMB').enable();
                    }
                }
            }, 
            {
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
            {
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilAMB',
                format: 'M/Y',
                value: now
            }, 
            {
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
            {
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilAMB',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function loadDataComboUserLapTransaksiPerkomponenDetailAMB(param){
    if (param==='' || param===undefined) {
        param={
            text: '0',
        };
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/hemodialisa/lap_trans_komponen_det/getUser",
        params: param,
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            cboUserRequestEntryLapTransaksiPerkomponenDetailAMB.store.removeAll();
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = ds_User_LapTransaksiPerkomponenDetailAMB.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                ds_User_LapTransaksiPerkomponenDetailAMB.add(recs);
                console.log(o);
            }
        }
    });
}

function mComboPilihanLapTransaksiPerkomponenDetailAMB()
{
    var cboPilihanLapTransaksiPerkomponenDetailAMB = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapTransaksiPerkomponenDetailAMB',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ'],[3, 'RWI'], [4, 'IGD']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanLapTransaksiPerkomponenDetailAMB;
};

function mComboPilihanLapTransaksiPerkomponenDetailAMBKelompokPasien()
{
    var cboPilihanLapTransaksiPerkomponenDetailAMBkelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'cboPilihanLapTransaksiPerkomponenDetailAMBkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanLapTransaksiPerkomponenDetailAMBkelompokPasien;
};

function mComboPerseoranganLapTransaksiPerkomponenDetailAMB()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapTransaksiPerkomponenDetailAMB = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapTransaksiPerkomponenDetailAMB.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboLookupCustomer',
                param: 'jenis_cust=0 order by CUSTOMER'
            }
        }
    );
    var cboPerseoranganLapTransaksiPerkomponenDetailAMB = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganLapTransaksiPerkomponenDetailAMB',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapTransaksiPerkomponenDetailAMB,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
    );
    return cboPerseoranganLapTransaksiPerkomponenDetailAMB;
};

function mComboUmumLapTransaksiPerkomponenDetailAMB()
{
    var cboUmumLapTransaksiPerkomponenDetailAMB = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 100,
            id:'cboUmumLapTransaksiPerkomponenDetailAMB',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            width:200,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[1, 'Semua']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:'Semua',
            listeners:
            {
                'select': function(a,b,c)
                {
                                  selectSetUmum=b.data.displayText ;
                }
                                
                            
            }
        }
    );
    return cboUmumLapTransaksiPerkomponenDetailAMB;
};

function mComboPerusahaanLapTransaksiPerkomponenDetailAMB()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboLookupCustomer',
                param: 'jenis_cust=1 ORDER BY CUSTOMER'
            }
        }
    );
    var cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 100,
            id: 'cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: '',
            align: 'Right',
            store: dsPerusahaanRequestEntry,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            width:200,
            value: selectsetperusahaan,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectsetperusahaan = b.data.KD_CUSTOMER;
                    selectsetnamaperusahaan = b.data.CUSTOMER;
                }
            }
        }
    );

    return cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB;
};

function mComboAsuransiLapTransaksiPerkomponenDetailAMB()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapTransaksiPerkomponenDetailAMB = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 100,
            id:'cboAsuransiLapTransaksiPerkomponenDetailAMB',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: '',
            align: 'Right',
            width:200,
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            value: selectSetAsuransi,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetAsuransi=b.data.KD_CUSTOMER ;
                    selectsetnamaAsuransi=b.data.CUSTOMER ;
                }
            }
        }
    );
    return cboAsuransiLapTransaksiPerkomponenDetailAMB;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailAMB').show();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailAMB').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB').show();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailAMB').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailAMB').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailAMB').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailAMB').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailAMB').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailAMB').show();
   }
}

function mComboPilihanAmbJenisTransaksi()
{
  var cboPilihanJenisTransaksiAMB = new Ext.form.ComboBox
    (
        {
                    id:'cboPilihanJenisTransaksiAMB',
                    x: 120,
                    y: 40,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 200,
                    emptyText:'',
                   // fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Summary'],[2, 'Detail']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:'Detail',
                   listeners:
                    {
                    'select': function(a, b, c){
                    
                     
                        asalpasienpilihan=b.data.id;
                    },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    {
                                        Ext.getCmp('cboDokterRequestEntry').focus();
                                        asalpasienpilihan= c.value;
                                    }else if(e.getKey()==9)
                                    {
                                        asalpasienpilihan= c.value; 
                                           
                                    }
                                    }, c);
                                }


        }
        }
    );
    return cboPilihanJenisTransaksiAMB;
};