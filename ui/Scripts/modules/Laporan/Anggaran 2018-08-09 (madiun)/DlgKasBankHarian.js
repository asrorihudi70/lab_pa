
var dsLapKasBankHarian;
var selectNamaLapKasBankHarian;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapKasBankHarian;
var varLapKasBankHarian= ShowFormLapKasBankHarian();

function ShowFormLapKasBankHarian()
{
    frmDlgLapKasBankHarian= fnDlgLapKasBankHarian();
    frmDlgLapKasBankHarian.show();
};

function fnDlgLapKasBankHarian()
{
    var winLapKasBankHarianReport = new Ext.Window
    (
        {
            id: 'winLapKasBankHarianReport',
            title: 'Laporan Kas / Bank Harian',
            closeAction: 'destroy',
            width: 230,
            height: 130,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapKasBankHarian()],
            listeners:
			{
				activate: function()
				{
				   
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapKasBankHarian',
					handler: function()
					{
						// var criteria = GetCriteriaLapKasBankHarian();
						
						// loadlaporanAnggaran('0', 'LapRealisasiBulanan', criteria, function(){
							
						// });
						var params={
							tanggal:Ext.getCmp('dtpTglAwalFilterKasBankHarian').getValue(),
						} 

						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/keuangan/lap_keuangan/cetakKasBankHarian");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();	
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapKasBankHarian',
					handler: function()
					{
							frmDlgLapKasBankHarian.close();
					}
				}
			]

        }
    );

    return winLapKasBankHarianReport;
};


function ItemDlgLapKasBankHarian()
{
    var PnlLapKasBankHarian = new Ext.Panel
    (
        {
            id: 'PnlLapKasBankHarian',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapKasBankHarian_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        
                    ]
                }
            ]
        }
    );

    return PnlLapKasBankHarian;
};

function GetCriteriaLapKasBankHarian()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterKasBankHarian').getValue();
	return strKriteria;
};

function getItemLapKasBankHarian_Tanggal()
{
    var items = {
        layout: 'column',
        width: 200,
        border: true,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  200,
            height: 45,
            items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Tanggal'
				}, 
				{
					x: 60,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				{
					x: 70,
					y: 10,
					xtype: 'datefield',
					id: 'dtpTglAwalFilterKasBankHarian',
					format: 'd/M/Y',
					value: now
				}
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapKasBankHarianReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
