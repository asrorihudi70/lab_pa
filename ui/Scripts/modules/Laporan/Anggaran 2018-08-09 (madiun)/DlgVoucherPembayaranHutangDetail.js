
var dsLapVoucherPembayaranHutangDetail;
var selectNamaLapVoucherPembayaranHutangDetail;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapVoucherPembayaranHutangDetail;
var varLapVoucherPembayaranHutangDetail= ShowFormLapVoucherPembayaranHutangDetail();

function ShowFormLapVoucherPembayaranHutangDetail()
{
    frmDlgLapVoucherPembayaranHutangDetail= fnDlgLapVoucherPembayaranHutangDetail();
    frmDlgLapVoucherPembayaranHutangDetail.show();
};

function fnDlgLapVoucherPembayaranHutangDetail()
{
    var winLapVoucherPembayaranHutangDetailReport = new Ext.Window
    (
        {
            id: 'winLapVoucherPembayaranHutangDetailReport',
            title: 'Voucher Penerimaan Piutang',
            closeAction: 'destroy',
            width: 480,
            height: 240,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapVoucherPembayaranHutangDetail()],
            listeners:
			{
				activate: function()
				{
				   
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapVoucherPembayaranHutangDetail',
					handler: function()
					{
						if(Ext.getCmp('cboDariVendorVoucherPembayaranHutangDetail').getValue() == ''){
							ShowPesanWarningLapVoucherPenerimaanPiutangDetailReport('Dari vendor tidak boleh kosong!','WARNING');
						} else if(Ext.getCmp('cboKeVendor_VoucherPembayaranHutangDetail').getValue() == ''){
							ShowPesanWarningLapVoucherPenerimaanPiutangDetailReport('Ke vendor tidak boleh kosong!','WARNING');
						} else{
							var params={
								dari_kd_vendor:Ext.getCmp('cboDariVendorVoucherPembayaranHutangDetail').getValue(),
								ke_kd_vendor:Ext.getCmp('cboKeVendor_VoucherPembayaranHutangDetail').getValue(),
								dari_vendor:Ext.get('cboDariVendorVoucherPembayaranHutangDetail').getValue(),
								ke_vendor:Ext.get('cboKeVendor_VoucherPembayaranHutangDetail').getValue(),
								tglAwal:Ext.getCmp('dtpTglAwalVoucherPembayaranHutangDetail').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirVoucherPembayaranHutangDetail').getValue(),
								numberAwal:Ext.getCmp('cboNumberAwal_VoucherPembayaranHutangDetail').getValue(),
								numberAkhir:Ext.getCmp('cboNumberAkhir_VoucherPembayaranHutangDetail').getValue(),
								orderby:Ext.get('cboOrderBY_VoucherPembayaranHutangDetail').getValue(),
							} 

							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/keuangan/lap_voucher/cetakVoucherPembayaranHutangDetail");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();	
						}							
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapVoucherPembayaranHutangDetail',
					handler: function()
					{
							frmDlgLapVoucherPembayaranHutangDetail.close();
					}
				}
			]
        }
    );

    return winLapVoucherPembayaranHutangDetailReport;
};


function ItemDlgLapVoucherPembayaranHutangDetail()
{
    var PnlLapVoucherPembayaranHutangDetail = new Ext.Panel
    (
        {
            id: 'PnlLapVoucherPembayaranHutangDetail',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapVoucherPembayaranHutangDetail_Customer(),
				{xtype: 'tbspacer',height: 10, width:5},
				getItemLapVoucherPembayaranHutangDetail_Periode()
            ]
        }
    );

    return PnlLapVoucherPembayaranHutangDetail;
};

function GetCriteriaLapVoucherPembayaranHutangDetail()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterVoucherPembayaranHutangDetail').getValue();
	return strKriteria;
};

function getItemLapVoucherPembayaranHutangDetail_Customer()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Dari Vendor'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboDariVendor_VoucherPembayaranHutangDetail(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Ke Vendor'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				ComboKeVendorVoucherPembayaranHutangDetail()
            ]
        }]
    };
    return items;
};

function getItemLapVoucherPembayaranHutangDetail_Periode()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Order by'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboOrderBYVoucherPembayaranHutangDetail(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Periode'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				{
					x: 100,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAwalVoucherPembayaranHutangDetail',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAwalVoucherPembayaranHutangDetail(),
				{
					x: 260,
					y: 40,
					xtype: 'label',
					text: ' s/d '
				},
				{
					x: 285,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAkhirVoucherPembayaranHutangDetail',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAkhirVoucherPembayaranHutangDetail()
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapVoucherPembayaranHutangDetailReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkoderec;
function mComboRekening()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboRekening = new WebApp.DataStore({fields: Field});
    dsUnitComboRekening.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboRekening',
                param: " MA_type = 'D'  and isdebit = 't' order by MA.ma_id"
            }
        }
    );
    var cboRekening = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboRekening',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:350,
                store: dsUnitComboRekening,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkoderec=b.data.displayText ;
                        }
                }
            }
    );
    return cboRekening;
};

function ComboDariVendor_VoucherPembayaranHutangDetail()
{
	var Fields = ['Vend_Code', 'Vendor', 'VendorName'];
    dsCboDariVendorVoucherPembayaranHutangDetail = new WebApp.DataStore({ fields: Fields });
	
	dsCboDariVendorVoucherPembayaranHutangDetail.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'Vendor', 
				Sortdir: 'ASC', 
				target: 'viewCboVendLengkap',
				param: ''
			} 
		}
	);
    
    var cbDariVendorVoucherPembayaranHutangDetail = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboDariVendorVoucherPembayaranHutangDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Vendor ...',
		    fieldLabel: 'Vendor ',
		    align: 'right',
		    anchor:'90%',
		    listWidth:350,
		    store: dsCboDariVendorVoucherPembayaranHutangDetail,
		    valueField: 'Vend_Code',
		    displayField: 'VendorName',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        // selectCboEntryCustARForm = b.data.kd_Vendor;
			    }
			}
		}
	);
	
	return cbDariVendorVoucherPembayaranHutangDetail;
};

function ComboKeVendorVoucherPembayaranHutangDetail()
{
	var Fields = ['Vend_Code', 'Vendor', 'VendorName'];
    dsCboKeVendor_VoucherPembayaranHutangDetail = new WebApp.DataStore({ fields: Fields });
	
	dsCboKeVendor_VoucherPembayaranHutangDetail.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'Vendor', 
				Sortdir: 'ASC', 
				target: 'viewCboVendLengkap',
				param: ''
			} 
		}
	);
    
    var cbKeVendor_VoucherPembayaranHutangDetail = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 40,
		    id: 'cboKeVendor_VoucherPembayaranHutangDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Vendor ...',
		    fieldLabel: 'Vendor ',
		    align: 'right',
		    anchor:'90%',
		    listWidth:350,
		    store: dsCboKeVendor_VoucherPembayaranHutangDetail,
		    valueField: 'Vend_Code',
		    displayField: 'VendorName',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        // selectCboEntryCustARForm = b.data.kd_Vendor;
			    }
			}
		}
	);
	
	return cbKeVendor_VoucherPembayaranHutangDetail;
};
function ComboNumberAwalVoucherPembayaranHutangDetail()
{
	var Fields = ['number'];
    dsCboNumberAwal_VoucherPembayaranHutangDetail = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPembayaranHutangDetail",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAwal_VoucherPembayaranHutangDetail.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAwal_VoucherPembayaranHutangDetail.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAwal_VoucherPembayaranHutangDetail.add(recs);
			}
		}
	});
    
    var cbNumberAwal_VoucherPembayaranHutangDetail = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 40,
		    id: 'cboNumberAwal_VoucherPembayaranHutangDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAwal_VoucherPembayaranHutangDetail,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        
			    }
			}
		}
	);
	
	return cbNumberAwal_VoucherPembayaranHutangDetail;
};

function ComboNumberAkhirVoucherPembayaranHutangDetail()
{
	var Fields = ['number'];
    dsCboNumberAkhir_VoucherPembayaranHutangDetail = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPembayaranHutangDetail",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAkhir_VoucherPembayaranHutangDetail.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAkhir_VoucherPembayaranHutangDetail.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAkhir_VoucherPembayaranHutangDetail.add(recs);
			}
		}
	});
    
    var cbNumberAkhir_VoucherPembayaranHutangDetail = new Ext.form.ComboBox
	(
		{
			x: 285,
			y: 40,
		    id: 'cboNumberAkhir_VoucherPembayaranHutangDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAkhir_VoucherPembayaranHutangDetail,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			    }
			}
		}
	);
	
	return cbNumberAkhir_VoucherPembayaranHutangDetail;
};

function ComboOrderBYVoucherPembayaranHutangDetail(){
	var cbOrderBY_VoucherPembayaranHutangDetail = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboOrderBY_VoucherPembayaranHutangDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Oder by',
		    fieldLabel: 'Vendor ',
		    align: 'right',
		    anchor:'60%',
			value:'Tanggal',
		    store:  new Ext.data.ArrayStore
			(
					{
						id: 0,
						fields:
						[
							'Id',
							'displayText'
						],
						data: [[1, 'Tanggal'],[2, 'Number']]
					}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        if(b.data.displayText == 'Tanggal'){
						Ext.getCmp('cboNumberAkhir_VoucherPembayaranHutangDetail').hide();
						Ext.getCmp('cboNumberAwal_VoucherPembayaranHutangDetail').hide();
						Ext.getCmp('dtpTglAwalVoucherPembayaranHutangDetail').show();
						Ext.getCmp('dtpTglAkhirVoucherPembayaranHutangDetail').show();
					} else{
						Ext.getCmp('cboNumberAkhir_VoucherPembayaranHutangDetail').show();
						Ext.getCmp('cboNumberAwal_VoucherPembayaranHutangDetail').show();
						Ext.getCmp('dtpTglAwalVoucherPembayaranHutangDetail').hide();
						Ext.getCmp('dtpTglAkhirVoucherPembayaranHutangDetail').hide();
					}
			    }
			}
		}
	);
	
	return cbOrderBY_VoucherPembayaranHutangDetail;
};