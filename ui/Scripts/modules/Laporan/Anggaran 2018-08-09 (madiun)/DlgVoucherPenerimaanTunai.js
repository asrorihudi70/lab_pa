
var dsLapVoucherPenerimaanTunai;
var selectNamaLapVoucherPenerimaanTunai;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapVoucherPenerimaanTunai;
var varLapVoucherPenerimaanTunai= ShowFormLapVoucherPenerimaanTunai();

function ShowFormLapVoucherPenerimaanTunai()
{
    frmDlgLapVoucherPenerimaanTunai= fnDlgLapVoucherPenerimaanTunai();
    frmDlgLapVoucherPenerimaanTunai.show();
};

function fnDlgLapVoucherPenerimaanTunai()
{
    var winLapVoucherPenerimaanTunaiReport = new Ext.Window
    (
        {
            id: 'winLapVoucherPenerimaanTunaiReport',
            title: 'Lap. Penerimaan Tunai',
            closeAction: 'destroy',
            width: 490,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapVoucherPenerimaanTunai()],
            listeners:
			{
				activate: function()
				{
				   
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapVoucherPenerimaanTunai',
					handler: function()
					{
						var params={
							tglAwal:Ext.getCmp('dtpTglAwalVoucherPenerimaanTunai').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirVoucherPenerimaanTunai').getValue(),
							numberAwal:Ext.getCmp('cboNumberAwal_VoucherPenerimaanTunai').getValue(),
							numberAkhir:Ext.getCmp('cboNumberAkhir_VoucherPenerimaanTunai').getValue(),
							orderby:Ext.get('cboOrderBY_VoucherPenerimaanTunai').getValue(),
						} 

						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/keuangan/lap_voucher/cetakVoucherPenerimaanTunai");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();		
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapVoucherPenerimaanTunai',
					handler: function()
					{
							frmDlgLapVoucherPenerimaanTunai.close();
					}
				}
			]

        }
    );

    return winLapVoucherPenerimaanTunaiReport;
};


function ItemDlgLapVoucherPenerimaanTunai()
{
    var PnlLapVoucherPenerimaanTunai = new Ext.Panel
    (
        {
            id: 'PnlLapVoucherPenerimaanTunai',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapVoucherPenerimaanTunai_Tanggal(),
            ]
        }
    );

    return PnlLapVoucherPenerimaanTunai;
};

function GetCriteriaLapVoucherPenerimaanTunai()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterVoucherPenerimaanTunai').getValue();
	return strKriteria;
};

function getItemLapVoucherPenerimaanTunai_Tanggal()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Order by'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboOrderBYVoucherPenerimaanTunai(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Periode'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				{
					x: 100,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAwalVoucherPenerimaanTunai',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAwalVoucherPenerimaanTunai(),
				{
					x: 260,
					y: 40,
					xtype: 'label',
					text: ' s/d '
				},
				{
					x: 285,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAkhirVoucherPenerimaanTunai',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAkhirVoucherPenerimaanTunai()
            ]
        }]
    };
    return items;
};


function ComboNumberAwalVoucherPenerimaanTunai()
{
	var Fields = ['number'];
    dsCboNumberAwal_VoucherPenerimaanTunai = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPenerimaanTunai",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAwal_VoucherPenerimaanTunai.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAwal_VoucherPenerimaanTunai.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAwal_VoucherPenerimaanTunai.add(recs);
			}
		}
	});
    
    var cbNumberAwal_VoucherPenerimaanTunai = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 40,
		    id: 'cboNumberAwal_VoucherPenerimaanTunai',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAwal_VoucherPenerimaanTunai,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        
			    }
			}
		}
	);
	
	return cbNumberAwal_VoucherPenerimaanTunai;
};

function ComboNumberAkhirVoucherPenerimaanTunai()
{
	var Fields = ['number'];
    dsCboNumberAkhir_VoucherPenerimaanTunai = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPenerimaanTunai",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAkhir_VoucherPenerimaanTunai.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAkhir_VoucherPenerimaanTunai.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAkhir_VoucherPenerimaanTunai.add(recs);
			}
		}
	});
    
    var cbNumberAkhir_VoucherPenerimaanTunai = new Ext.form.ComboBox
	(
		{
			x: 285,
			y: 40,
		    id: 'cboNumberAkhir_VoucherPenerimaanTunai',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAkhir_VoucherPenerimaanTunai,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			    }
			}
		}
	);
	
	return cbNumberAkhir_VoucherPenerimaanTunai;
};

function ComboOrderBYVoucherPenerimaanTunai(){
	var cbOrderBY_VoucherPenerimaanTunai = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboOrderBY_VoucherPenerimaanTunai',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Oder by',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'60%',
			value:'Tanggal',
		    store:  new Ext.data.ArrayStore
			(
					{
						id: 0,
						fields:
						[
							'Id',
							'displayText'
						],
						data: [[1, 'Tanggal'],[2, 'Number']]
					}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        if(b.data.displayText == 'Tanggal'){
						Ext.getCmp('cboNumberAkhir_VoucherPenerimaanTunai').hide();
						Ext.getCmp('cboNumberAwal_VoucherPenerimaanTunai').hide();
						Ext.getCmp('dtpTglAwalVoucherPenerimaanTunai').show();
						Ext.getCmp('dtpTglAkhirVoucherPenerimaanTunai').show();
					} else{
						Ext.getCmp('cboNumberAkhir_VoucherPenerimaanTunai').show();
						Ext.getCmp('cboNumberAwal_VoucherPenerimaanTunai').show();
						Ext.getCmp('dtpTglAwalVoucherPenerimaanTunai').hide();
						Ext.getCmp('dtpTglAkhirVoucherPenerimaanTunai').hide();
					}
			    }
			}
		}
	);
	
	return cbOrderBY_VoucherPenerimaanTunai;
};


function ShowPesanWarningLapVoucherPenerimaanTunaiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkodedariAkun;
var tmpkodekeAkun;
function mComboDariAkunVoucherPenerimaanTunai()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboDariAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboDariAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboAccount',
                param: ""
            }
        }
    );
    var cboDariAkun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboDariAkun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:360,
                store: dsUnitComboDariAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodedariAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboDariAkun;
};

function mComboKeAkunVoucherPenerimaanTunai()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboKeAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboKeAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboAccount',
                param: " "
            }
        }
    );
    var cboKeAKun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 40,
                id:'cboKeAKun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:360,
                store: dsUnitComboKeAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodekeAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboKeAKun;
};

function mComboPilihanRangking()
{
    var cboPilihanLapPilihanRangking = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 100,
                id:'cboPilihanLapPilihanRangking',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: ' ',
                width:100,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Tanggal'], [2, 'Referensi'],[3, 'voucer']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Tanggal',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                // selectSetPilihan=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanLapPilihanRangking;
};

function mComboBendahara()
{
    var cboBendahara = new Ext.form.ComboBox
    (
            {
                x: 310,
                y: 100,
                id:'cboBendahara',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: ' ',
                width:150,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Bendahara Penerimaan'], [2, 'Bendahara Pengeluaran']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Bendahara Penerimaan',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                        }
                }
            }
    );
    return cboBendahara;
};