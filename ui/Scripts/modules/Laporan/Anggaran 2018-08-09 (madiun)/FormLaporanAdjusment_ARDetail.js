
var dsLapAdjusment_ARDetail;
var selectNamaLapAdjusment_ARDetail;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapAdjusment_ARDetail;
var varLapAdjusment_ARDetail= ShowFormLapAdjusment_ARDetail();

function ShowFormLapAdjusment_ARDetail()
{
    frmDlgLapAdjusment_ARDetail= fnDlgLapAdjusment_ARDetail();
    frmDlgLapAdjusment_ARDetail.show();
};

function fnDlgLapAdjusment_ARDetail()
{
    var winLapAdjusment_ARDetailReport = new Ext.Window
    (
        {
            id: 'winLapAdjusment_ARDetailReport',
            title: 'Laporan Adjustment AR Detail',
            closeAction: 'destroy',
            width: 230,
            height: 130,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapAdjusment_ARDetail()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapAdjusment_ARDetailReport;
};


function ItemDlgLapAdjusment_ARDetail()
{
    var PnlLapAdjusment_ARDetail = new Ext.Panel
    (
        {
            id: 'PnlLapAdjusment_ARDetail',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapAdjusment_ARDetail_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapAdjusment_ARDetail',
                            handler: function()
                            {
                                loadMask.show();
								var params={
									tanggal:Ext.getCmp('dtpTglAwalFilterAdjusment_ARDetail').getValue()
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/keuangan/cetaklaporanGeneralCashier/adj_ar_detail");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//frmDlgLapSTS.close();
								loadMask.hide();
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapAdjusment_ARDetail',
                            handler: function()
                            {
                                    frmDlgLapAdjusment_ARDetail.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapAdjusment_ARDetail;
};

function GetCriteriaLapAdjusment_ARDetail()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterAdjusment_ARDetail').getValue();
	return strKriteria;
};

function getItemLapAdjusment_ARDetail_Tanggal()
{
    var items = {
        layout: 'column',
        width: 200,
        border: true,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  200,
            height: 45,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode'
            }, 
			{
                x: 60,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			{
                x: 70,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterAdjusment_ARDetail',
                format: 'M/Y',
                value: now
            }
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapAdjusment_ARDetailReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
