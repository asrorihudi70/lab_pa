
var dsLapPanjar;
var selectNamaLapPanjar;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapPanjar;
var varLapPanjar= ShowFormLapPanjar();

function ShowFormLapPanjar()
{
    frmDlgLapPanjar= fnDlgLapPanjar();
    frmDlgLapPanjar.show();
};

function fnDlgLapPanjar()
{
    var winLapPanjarReport = new Ext.Window
    (
        {
            id: 'winLapPanjarReport',
            title: 'Lap. Panjar',
            closeAction: 'destroy',
            width: 490,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapPanjar()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapPanjarReport;
};


function ItemDlgLapPanjar()
{
    var PnlLapPanjar = new Ext.Panel
    (
        {
            id: 'PnlLapPanjar',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapPanjar_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'0px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapPanjar',
                            handler: function()
                            {
                                // if (ValidasiReportLapPanjar() === 1)
                                // {
                                        //var tmppilihan = getKodeReportLapPanjar();
                                        var criteria = GetCriteriaLapPanjar();
                                        // loadMask.show();
                                        // var criteria = '';
                                        loadlaporanAnggaran('0', 'LapPanjar', criteria, function(){
											// frmDlgLapPanjar.close();
											// loadMask.hide();
										});
                                // };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapPanjar',
                            handler: function()
                            {
                                    frmDlgLapPanjar.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapPanjar;
};

function GetCriteriaLapPanjar()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterPanjar').getValue();
	return strKriteria;
};

function getItemLapPanjar_Tanggal()
{
    var items = {
        layout: 'column',
        width: 500,
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  500,
            height: 130,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Dari Account'
            }, 
			{
                x: 90,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            mComboDariAkunPanjar(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Ke Account'
            }, 
            {
                x: 90,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            mComboKeAkunPanjar(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Tanggal'
            }, 
            {
                x: 90,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 100,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterPanjar',
                format: 'd/M/Y',
                value: now
            },
            {
                x: 220,
                y: 75,
                xtype: 'label',
                text: 's/d'
            },
            {
                x: 250,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterPanjar',
                format: 'd/M/Y',
                value: now
            }, 
            {
                x: 10,
                y: 100,
                xtype: 'label',
                text: 'Rangking By'
            }, 
            {
                x: 90,
                y: 100,
                xtype: 'label',
                text: ' : '
            },
            mComboPilihanRangking(),
            // {
            //     x: 220,
            //     y: 100,
            //     xtype: 'label',
            //     text: 'Bendahara '
            // }, 
            // {
            //     x: 300,
            //     y: 100,
            //     xtype: 'label',
            //     text: ' : '
            // },
            // mComboBendahara()
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapPanjarReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkodedariAkun;
var tmpkodekeAkun;
function mComboDariAkunPanjar()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboDariAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboDariAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboAccountPanjar',
                param: ""
            }
        }
    );
    var cboDariAkun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboDariAkun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:360,
                store: dsUnitComboDariAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodedariAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboDariAkun;
};

function mComboKeAkunPanjar()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboKeAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboKeAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboAccountPanjar',
                param: " "
            }
        }
    );
    var cboKeAKun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 40,
                id:'cboKeAKun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:360,
                store: dsUnitComboKeAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodekeAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboKeAKun;
};

function mComboPilihanRangking()
{
    var cboPilihanLapPilihanRangking = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 100,
                id:'cboPilihanLapPilihanRangking',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: ' ',
                width:100,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Tanggal'], [2, 'Referensi'],[3, 'voucer']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Tanggal',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                // selectSetPilihan=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanLapPilihanRangking;
};

function mComboBendahara()
{
    var cboBendahara = new Ext.form.ComboBox
    (
            {
                x: 310,
                y: 100,
                id:'cboBendahara',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: ' ',
                width:150,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Bendahara Penerimaan'], [2, 'Bendahara Pengeluaran']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Bendahara Penerimaan',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                        }
                }
            }
    );
    return cboBendahara;
};