
var dsLapRealisasiPengeluaranbulanan;
var selectNamaLapRealisasiPengeluaranbulanan;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRealisasiPengeluaranbulanan;
var varLapRealisasiPengeluaranbulanan= ShowFormLapRealisasiPengeluaranbulanan();

function ShowFormLapRealisasiPengeluaranbulanan()
{
    frmDlgLapRealisasiPengeluaranbulanan= fnDlgLapRealisasiPengeluaranbulanan();
    frmDlgLapRealisasiPengeluaranbulanan.show();
};

function fnDlgLapRealisasiPengeluaranbulanan()
{
    var winLapRealisasiPengeluaranbulananReport = new Ext.Window
    (
        {
            id: 'winLapRealisasiPengeluaranbulananReport',
            title: 'Laporan Realisasi Pengeluaran Per Bulan',
            closeAction: 'destroy',
            width: 230,
            height: 130,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRealisasiPengeluaranbulanan()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapRealisasiPengeluaranbulananReport;
};


function ItemDlgLapRealisasiPengeluaranbulanan()
{
    var PnlLapRealisasiPengeluaranbulanan = new Ext.Panel
    (
        {
            id: 'PnlLapRealisasiPengeluaranbulanan',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapRealisasiPengeluaranbulanan_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRealisasiPengeluaranbulanan',
                            handler: function()
                            {
                                // if (ValidasiReportLapRealisasiPengeluaranbulanan() === 1)
                                // {
                                        //var tmppilihan = getKodeReportLapRealisasiPengeluaranbulanan();
                                        var criteria = GetCriteriaLapRealisasiPengeluaranbulanan();
                                        // loadMask.show();
                                        // var criteria = '';
                                        loadlaporanAnggaran('0', 'LapRealisasiPengeluaranBulanan', criteria, function(){
                                            // frmDlgLapRealisasiPengeluaranbulanan.close();
                                            // loadMask.hide();
                                        });
                                // };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRealisasiPengeluaranbulanan',
                            handler: function()
                            {
                                    frmDlgLapRealisasiPengeluaranbulanan.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRealisasiPengeluaranbulanan;
};

function GetCriteriaLapRealisasiPengeluaranbulanan()
{
    var strKriteria = '';
    
    strKriteria = 'Tanggal';
    strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterRealisasiPengeluaranbulanan').getValue();
    return strKriteria;
};

function getItemLapRealisasiPengeluaranbulanan_Tanggal()
{
    var items = {
        layout: 'column',
        width: 200,
        border: true,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  200,
            height: 45,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Tanggal'
            }, 
            {
                x: 60,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 70,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterRealisasiPengeluaranbulanan',
                format: 'M/Y',
                value: now
            }
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapRealisasiPengeluaranbulananReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
