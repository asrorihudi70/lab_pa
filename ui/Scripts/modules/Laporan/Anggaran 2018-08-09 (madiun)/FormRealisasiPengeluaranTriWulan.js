
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapRealisasiPengeluaranTriWulan;
var selectNamaLapRealisasiPengeluaranTriWulan;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRealisasiPengeluaranTriWulan;
var varLapRealisasiPengeluaranTriWulan= ShowFormLapRealisasiPengeluaranTriWulan();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;

function ShowFormLapRealisasiPengeluaranTriWulan()
{
    frmDlgLapRealisasiPengeluaranTriWulan= fnDlgLapRealisasiPengeluaranTriWulan();
    frmDlgLapRealisasiPengeluaranTriWulan.show();
};

function fnDlgLapRealisasiPengeluaranTriWulan()
{
    var winLapRealisasiPengeluaranTriWulanReport = new Ext.Window
    (
        {
            id: 'winLapRealisasiPengeluaranTriWulanReport',
            title: 'Laporan Realisasi Per Bulan',
            closeAction: 'destroy',
            width: 200,
            height: 150,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRealisasiPengeluaranTriWulan()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapRealisasiPengeluaranTriWulanReport;
};


function ItemDlgLapRealisasiPengeluaranTriWulan()
{
    var PnlLapRealisasiPengeluaranTriWulan = new Ext.Panel
    (
        {
            id: 'PnlLapRealisasiPengeluaranTriWulan',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapRealisasiPengeluaranTriWulan_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRealisasiPengeluaranTriWulan',
                            handler: function()
                            {
                                // if (ValidasiReportLapRealisasiPengeluaranTriWulan() === 1)
                                // {
                                        //var tmppilihan = getKodeReportLapRealisasiPengeluaranTriWulan();
                                        // var criteria = GetCriteriaLapRealisasiPengeluaranTriWulan();
                                        // loadMask.show();
                                        var criteria = '';
                                        loadlaporanAnggaran('0', 'LapRealisasiPengeluaranTriWulan', criteria, function(){
											// frmDlgLapRealisasiPengeluaranTriWulan.close();
											// loadMask.hide();
										});
                                // };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRealisasiPengeluaranTriWulan',
                            handler: function()
                            {
                                    frmDlgLapRealisasiPengeluaranTriWulan.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRealisasiPengeluaranTriWulan;
};

function GetCriteriaLapRealisasiPengeluaranTriWulan()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterRealisasiPengeluaranTriWulan').getValue();
	strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterRealisasiPengeluaranTriWulan').getValue();
	
	if (Ext.get('cboPilihanLapRealisasiPengeluaranTriWulan').getValue() !== '' || Ext.get('cboPilihanLapRealisasiPengeluaranTriWulan').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'asal pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanLapRealisasiPengeluaranTriWulan').getValue();
	}
	if (Ext.get('cboUnitFarLapRealisasiPengeluaranTriWulan').getValue() !== '' || Ext.get('cboUnitFarLapRealisasiPengeluaranTriWulan').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + Ext.get('cboUnitFarLapRealisasiPengeluaranTriWulan').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitFarLapRealisasiPengeluaranTriWulan').getValue();
	}
	if (Ext.get('cboUserRequestEntryLapRealisasiPengeluaranTriWulan').getValue() !== ''|| Ext.get('cboUserRequestEntryLapRealisasiPengeluaranTriWulan').getValue() !== 'Pilih User...'){
		strKriteria += '##@@##' + 'operator';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryLapRealisasiPengeluaranTriWulan').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryLapRealisasiPengeluaranTriWulan').getValue();
	}
	
	if (Ext.getCmp('Shift_All_LapRealisasiPengeluaranTriWulan').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_LapRealisasiPengeluaranTriWulan').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_LapRealisasiPengeluaranTriWulan').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_LapRealisasiPengeluaranTriWulan').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	}
	
	return strKriteria;
};

function ValidasiReportLapRealisasiPengeluaranTriWulan()
{
	var x=1;
	if(Ext.get('cboUnitFarLapRealisasiPengeluaranTriWulan').getValue() === ''|| Ext.get('cboUnitFarLapRealisasiPengeluaranTriWulan').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapRealisasiPengeluaranTriWulanReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapRealisasiPengeluaranTriWulan').getValue() === '' || Ext.get('cboUserRequestEntryLapRealisasiPengeluaranTriWulan').getValue() == 'Pilih User...'){
		ShowPesanWarningLapRealisasiPengeluaranTriWulanReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalFilterRealisasiPengeluaranTriWulan').dom.value > Ext.get('dtpTglAkhirFilterRealisasiPengeluaranTriWulan').dom.value)
    {
        ShowPesanWarningLapRealisasiPengeluaranTriWulanReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }

    return x;
};



function getItemLapRealisasiPengeluaranTriWulan_Unit()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  380,
            height: 115,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapRealisasiPengeluaranTriWulan(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Unit '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            mComboUnitLapRealisasiPengeluaranTriWulan(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
			mComboUserLapRealisasiPengeluaranTriWulan()
            ]
        }]
    };
    return items;
};


function getItemLapRealisasiPengeluaranTriWulan_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  380,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapRealisasiPengeluaranTriWulan_Shift()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  380,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
					{
						x: 60,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapRealisasiPengeluaranTriWulan',
						id : 'Shift_All_LapRealisasiPengeluaranTriWulan',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapRealisasiPengeluaranTriWulan').setValue(true);
								Ext.getCmp('Shift_2_LapRealisasiPengeluaranTriWulan').setValue(true);
								Ext.getCmp('Shift_3_LapRealisasiPengeluaranTriWulan').setValue(true);
								Ext.getCmp('Shift_1_LapRealisasiPengeluaranTriWulan').disable();
								Ext.getCmp('Shift_2_LapRealisasiPengeluaranTriWulan').disable();
								Ext.getCmp('Shift_3_LapRealisasiPengeluaranTriWulan').disable();
							}else{
								Ext.getCmp('Shift_1_LapRealisasiPengeluaranTriWulan').setValue(false);
								Ext.getCmp('Shift_2_LapRealisasiPengeluaranTriWulan').setValue(false);
								Ext.getCmp('Shift_3_LapRealisasiPengeluaranTriWulan').setValue(false);
								Ext.getCmp('Shift_1_LapRealisasiPengeluaranTriWulan').enable();
								Ext.getCmp('Shift_2_LapRealisasiPengeluaranTriWulan').enable();
								Ext.getCmp('Shift_3_LapRealisasiPengeluaranTriWulan').enable();
							}
						}
					},
					{
						x: 130,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapRealisasiPengeluaranTriWulan',
						id : 'Shift_1_LapRealisasiPengeluaranTriWulan'
					},
					{
						x: 200,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapRealisasiPengeluaranTriWulan',
						id : 'Shift_2_LapRealisasiPengeluaranTriWulan'
					},
					{
						x: 270,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapRealisasiPengeluaranTriWulan',
						id : 'Shift_3_LapRealisasiPengeluaranTriWulan'
					}
				]
            }
        ]
            
    };
    return items;
};

function getItemLapRealisasiPengeluaranTriWulan_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  380,
            height: 45,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Tanggal'
            }, 
			{
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			{
                x: 120,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterRealisasiPengeluaranTriWulan',
                format: 'M/Y',
                value: now
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function mComboPilihanLapRealisasiPengeluaranTriWulan()
{
    var cboPilihanLapRealisasiPengeluaranTriWulan = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapRealisasiPengeluaranTriWulan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapRealisasiPengeluaranTriWulan;
};

function mComboUnitLapRealisasiPengeluaranTriWulan()
{
	var Field = ['KD_UNIT_FAR','NM_UNIT_FAR'];
    dsUnitFarLapRealisasiPengeluaranTriWulan = new WebApp.DataStore({fields: Field});
    dsUnitFarLapRealisasiPengeluaranTriWulan.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboUnitFar',
			    param: " kd_unit_far <>'' order by urut,nm_unit_far"
			}
		}
	);
    var cboUnitFarLapRealisasiPengeluaranTriWulan = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 40,
                id:'cboUnitFarLapRealisasiPengeluaranTriWulan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsUnitFarLapRealisasiPengeluaranTriWulan,
                valueField: 'KD_UNIT_FAR',
                displayField: 'NM_UNIT_FAR',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboUnitFarLapRealisasiPengeluaranTriWulan;
};

function mComboUserLapRealisasiPengeluaranTriWulan()
{
    var Field = ['KD_USER','FULL_NAME'];
    ds_User_viDaftar = new WebApp.DataStore({fields: Field});

	ds_User_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: '',
                    Sortdir: '',
                    target:'ViewComboUser',
                    param: ""
                }
            }
        );

    var cboUserRequestEntryLapRealisasiPengeluaranTriWulan = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 70,
            id: 'cboUserRequestEntryLapRealisasiPengeluaranTriWulan',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:'Semua',
            store: ds_User_viDaftar,
            valueField: 'KD_USER',
            displayField: 'FULL_NAME',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.KD_USER;
				} 
			}
        }
    )

    return cboUserRequestEntryLapRealisasiPengeluaranTriWulan;
};

function ShowPesanWarningLapRealisasiPengeluaranTriWulanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
