
var dsLapRekapPiutang;
var selectNamaLapRekapPiutang;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRekapPiutang;
var varLapRekapPiutang= ShowFormLapRekapPiutang();

function ShowFormLapRekapPiutang()
{
    frmDlgLapRekapPiutang= fnDlgLapRekapPiutang();
    frmDlgLapRekapPiutang.show();
};

function fnDlgLapRekapPiutang()
{
    var winLapRekapPiutangReport = new Ext.Window
    (
        {
            id: 'winLapRekapPiutangReport',
            title: 'Lap. Rekap Piutang',
            closeAction: 'destroy',
            width: 490,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRekapPiutang()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapRekapPiutangReport;
};


function ItemDlgLapRekapPiutang()
{
    var PnlLapRekapPiutang = new Ext.Panel
    (
        {
            id: 'PnlLapRekapPiutang',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapRekapPiutang_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'0px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRekapPiutang',
                            handler: function()
                            {
                                // if (ValidasiReportLapRekapPiutang() === 1)
                                // {
                                        //var tmppilihan = getKodeReportLapRekapPiutang();
                                        var criteria = GetCriteriaLapRekapPiutang();
                                        // loadMask.show();
                                        // var criteria = '';
                                        loadlaporanAnggaran('0', 'LapRekapPiutang', criteria, function(){
											// frmDlgLapRekapPiutang.close();
											// loadMask.hide();
										});
                                // };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRekapPiutang',
                            handler: function()
                            {
                                    frmDlgLapRekapPiutang.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRekapPiutang;
};

function GetCriteriaLapRekapPiutang()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterRekapPiutang').getValue();
	return strKriteria;
};

function getItemLapRekapPiutang_Tanggal()
{
    var items = {
        layout: 'column',
        width: 500,
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  500,
            height: 130,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Dari Account'
            }, 
			{
                x: 90,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            mComboDariAkunRekapPiutang(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Tanggal'
            }, 
            {
                x: 90,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 100,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterRekapPiutang',
                format: 'd/M/Y',
                value: now
            },
            {
                x: 220,
                y: 45,
                xtype: 'label',
                text: 's/d'
            },
            {
                x: 250,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterRekapPiutang',
                format: 'd/M/Y',
                value: now
            }, 
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Posted'
            }, 
            {
                x: 90,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 100,
                y: 70,
                xtype: 'checkbox',
                id: 'checkboxposted',
                boxLabel: '',
                name: '',
                checked: true,
                inputValue: 't'
            },
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapRekapPiutangReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkodedariAkun;
var tmpkodekeAkun;
function mComboDariAkunRekapPiutang()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboDariAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboDariAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboAccountRekapPiutang',
                param: ""
            }
        }
    );
    var cboDariAkun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboDariAkun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:360,
                store: dsUnitComboDariAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodedariAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboDariAkun;
};