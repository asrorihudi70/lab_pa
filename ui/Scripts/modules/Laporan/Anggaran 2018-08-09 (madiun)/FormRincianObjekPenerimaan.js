
var dsLapRincianObjekPenerimaan;
var selectNamaLapRincianObjekPenerimaan;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRincianObjekPenerimaan;
var varLapRincianObjekPenerimaan= ShowFormLapRincianObjekPenerimaan();

function ShowFormLapRincianObjekPenerimaan()
{
    frmDlgLapRincianObjekPenerimaan= fnDlgLapRincianObjekPenerimaan();
    frmDlgLapRincianObjekPenerimaan.show();
};

function fnDlgLapRincianObjekPenerimaan()
{
    var winLapRincianObjekPenerimaanReport = new Ext.Window
    (
        {
            id: 'winLapRincianObjekPenerimaanReport',
            title: 'Buku Pembantu Perincian Objek Penerimaan',
            closeAction: 'destroy',
            width: 480,
            height: 150,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRincianObjekPenerimaan()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapRincianObjekPenerimaanReport;
};


function ItemDlgLapRincianObjekPenerimaan()
{
    var PnlLapRincianObjekPenerimaan = new Ext.Panel
    (
        {
            id: 'PnlLapRincianObjekPenerimaan',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapRincianObjekPenerimaan_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'0px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRincianObjekPenerimaan',
                            handler: function()
                            {
                                // if (ValidasiReportLapRincianObjekPenerimaan() === 1)
                                // {
                                        //var tmppilihan = getKodeReportLapRincianObjekPenerimaan();
                                        var criteria = GetCriteriaLapRincianObjekPenerimaan();
                                        // loadMask.show();
                                        // var criteria = '';
                                        loadlaporanAnggaran('0', 'LapRincianObjekPenerimaan', criteria, function(){
											// frmDlgLapRincianObjekPenerimaan.close();
											// loadMask.hide();
										});
                                // };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRincianObjekPenerimaan',
                            handler: function()
                            {
                                    frmDlgLapRincianObjekPenerimaan.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRincianObjekPenerimaan;
};

function GetCriteriaLapRincianObjekPenerimaan()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterRincianObjekPenerimaan').getValue();
	return strKriteria;
};

function getItemLapRincianObjekPenerimaan_Tanggal()
{
    var items = {
        layout: 'column',
        width: 500,
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  500,
            height: 70,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Kode Rekening'
            }, 
			{
                x: 90,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			// {
   //              x: 100,
   //              y: 10,
   //              xtype: 'datefield',
   //              id: 'dtpTglAwalFilterRincianObjekPenerimaan',
   //              format: 'M/Y',
   //              value: now
   //          },
            mComboRekening(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Bulan / Tahun'
            }, 
            {
                x: 90,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 100,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterRincianObjekPenerimaan',
                format: 'M/Y',
                value: now
            },
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapRincianObjekPenerimaanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkoderec;
function mComboRekening()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboRekening = new WebApp.DataStore({fields: Field});
    dsUnitComboRekening.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboRekening',
                param: " MA_type = 'D'  and isdebit = 't' order by MA.ma_id"
            }
        }
    );
    var cboRekening = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboRekening',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:350,
                store: dsUnitComboRekening,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkoderec=b.data.displayText ;
                        }
                }
            }
    );
    return cboRekening;
};