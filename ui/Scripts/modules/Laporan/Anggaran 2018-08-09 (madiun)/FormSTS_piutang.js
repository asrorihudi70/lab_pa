
var dsLapSTS;
var selectNamaLapSTS;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapSTS;
var varLapSTS= ShowFormLapSTS();

function ShowFormLapSTS()
{
    frmDlgLapSTS= fnDlgLapSTS();
    frmDlgLapSTS.show();
};

function fnDlgLapSTS()
{
    var winLapSTSReport = new Ext.Window
    (
        {
            id: 'winLapSTSReport',
            title: 'Lap. Surat Tanda Setoran Piutang',
            closeAction: 'destroy',
            width: 300,
            height: 150,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapSTS()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapSTSReport;
};


function ItemDlgLapSTS()
{
    var PnlLapSTS = new Ext.Panel
    (
        {
            id: 'PnlLapSTS',
            fileUpload: true,
            layout: 'form',
            height: 200,
            width:300,
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapSTS_Tanggal(),
            ]
        }
    );

    return PnlLapSTS;
};

function GetCriteriaLapSTS()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterSTS').getValue();
	return strKriteria;
};

function getItemLapSTS_Tanggal()
{
    var items = {
        layout: 'column',
        width: 300,
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  300,
            height: 80,
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Tanggal'
            }, 
            {
                x: 90,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 100,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterSTS',
                format: 'd/M/Y',
                value: now
            },
            {
                x: 60,
                y: 40,
                xtype: 'button',
                text: 'Cetak',
                iconCls:'print',
                width: 70,
                hideLabel: true,
                id: 'btnOkLapSTS',
                handler: function()
                {
					var criteria = GetCriteriaLapSTS();
					loadMask.show();
					loadlaporanAnggaran('0', 'LapSTS_piutang', criteria, function(){
					frmDlgLapSTS.close();
					loadMask.hide();
					});
                    // };
                }
            },
            {
                x: 139,
                y: 40,
                xtype: 'button',
                text: 'Batal' ,
                iconCls: 'remove',
                width: 70,
                hideLabel: true,
                id: 'btnCancelLapSTS',
                handler: function()
                {
                        frmDlgLapSTS.close();
                }
            }
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapSTSReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};