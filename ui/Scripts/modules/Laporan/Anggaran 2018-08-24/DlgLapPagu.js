﻿var dsUnitKerja_LapPagu;
var selectUnitKerja_LapPagu;
var now_LapPagu = new Date();
var now = new Date();
// var winTitle_LapPagu = varLapMainPage +' '+ CurrentPage.title;
var winTitle_LapPagu = 'Laporan Plafond Unit Kerja';
// var winTitle_LapPagu = 'Plafond Anggaran';
var winDlg_LapPagu= fnDlg_LapPagu();
winDlg_LapPagu.show();

function fnDlg_LapPagu()
{  	
    var winDlg_LapPagu = new Ext.Window
	(
		{ 
			id: 'winDlg_LapPagu',
			title: winTitle_LapPagu,
			closeAction: 'destroy',
			width:500,
			height: 180,
			resizable:false,
			border: false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [getItemDlg_LapPagu()]
			
		}
	);
	
    return winDlg_LapPagu; 
};


function getItemDlg_LapPagu() 
{	
	var Pnl_LapPagu = new Ext.Panel
	(
		{ 
			id: 'Pnl_LapPagu',
			fileUpload: true,
			layout: 'anchor',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 472,
							height: 109,
							items: 
							[
								mComboTahun_LapPagu(),
								mcomboUnitKerja_LapPagu(),
								//mComboTrans_LapPagu()
							]
						}
					]
				},				
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'rigth'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOk_LapPagu',
							handler: function() 
							{
								if(Validasi_LapPagu() == 1)
								{
									var params={
										tahun_anggaran	:Ext.getCmp('cboTahun_LapPagu').getValue(),
										unit_kerja		:Ext.getCmp('comboUnitKerja_LapPagu').getValue(),
										
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/anggaran_module/functionLapPlafondUnitKerja/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();		
									winDlg_LapPagu.close();
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancel_LapPagu',
							handler: function() 
							{
								winDlg_LapPagu.close();
							}
						}
					]
				}
			]
		}
	);
 
    return Pnl_LapPagu;
};

function ShowPesanWarningRKAReport_LapPagu(str,modul)
{
	Ext.MessageBox.show
	(		
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function mcomboUnitKerja_LapPagu()
{
	var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
	dsUnitKerja_LapPagu = new WebApp.DataStore({ fields: Field });

  var comboUnitKerja_LapPagu = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_LapPagu',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Semua Unit Kerja ',
			fieldLabel: 'Unit Kerja ',			
			align:'Right',
			anchor:'96%',
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			store:dsUnitKerja_LapPagu,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnitKerja_LapPagu=b.data.kd_unit ;
				} 
			}
		}
	);

    Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerja_LapPagu.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerja_LapPagu.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerja_LapPagu.add(recs);
			} else {
			};
		}
	});
	return comboUnitKerja_LapPagu;
};

function mComboTahun_LapPagu()
{
  // var currYear = parseInt(gstrTahunAngg);//parseInt(now_LapPagu.format('Y'));
  var currYear = parseInt(now.format('Y'));
  var cboTahun_LapPagu = new Ext.form.ComboBox
	(
		{
			id:'cboTahun_LapPagu',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Thn. Anggaran ',			
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					// data: [[1, currYear+1 +'/'+(Ext.num(gstrTahunAngg)+2)], [2, currYear +'/'+(Ext.num(gstrTahunAngg)+1)], [3, currYear-1 +'/'+gstrTahunAngg], [4, currYear-2 +'/'+(Ext.num(gstrTahunAngg)-1)]]
					data:[						
									[currYear + 1,currYear + 1 +'/' + (Ext.num(currYear) + 2)], 
									[currYear,currYear +'/' + (Ext.num(currYear) + 1)]
								]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			// value: currYear +'/'+(Ext.num(gstrTahunAngg)+1)//now_LapRKARutin.format('Y')			
			value:Ext.num(now.format('Y'))
		}
	);
	
	return cboTahun_LapPagu;
};

function mComboTrans_LapPagu()
{
  var cboTrans_LapPagu = new Ext.form.ComboBox
	(
		{
			id:'cboTrans_LapPagu',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih semua',
			fieldLabel: 'Jenis Anggaran ',			
			width:198,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, "OPERASIONAL"], [2, "KEMAHASISWAAN"], [3,'BANTUAN']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText'			
		}
	);
	
	return cboTrans_LapPagu;
};

function getItemKelompokRKA_LapPagu()
{
	var cboKelRKA_LapPagu = new Ext.form.ComboBox
	(
		{
			id:'optPN_LapPagu',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Jenis ',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[2, "Pengembangan"], [1, "Rutin"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboKelRKA_LapPagu;
}

function getKriteria_LapPagu()
{
	var strKriteria = "";	
	
	// if(Ext.getCmp('comboUnitKerja_LapPagu').getValue() !== undefined || Ext.getCmp('comboUnitKerja_LapPagu').getValue() != "")
	// {
	// 	if(strKriteria == "")
	// 	{
	// 		strKriteria = " Where PLA.KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerja_LapPagu').getValue() + "'";
	// 	}
	// 	else
	// 	{	
	// 		if(Ext.getCmp('comboUnitKerja_LapPagu').getValue() != "")
	// 		{
	// 			strKriteria += " and PLA.KD_UNIT_KERJA='" + Ext.getCmp('comboUnitKerja_LapPagu').getValue() + "'";
	// 		}else{
	// 			strKriteria += " ";
	// 		}
	// 	}
	// }else
	// {
		
	// }
	// if(Ext.getCmp('cboTahun_LapPagu').getValue() !== undefined || Ext.getCmp('cboTahun_LapPagu').getValue() != "")
	// {		
	// 	strKriteria += " Where PLA.TAHUN_ANGGARAN_TA ='" + Ext.getCmp('cboTahun_LapPagu').getValue() + "'";	
	// }else
	// {
	// 	strKriteria +=''+"##";
	// }
	
	// if(Ext.get('cboTrans_LapPagu').getValue() != "Pilih semua")
	// {
	// 	strKriteria += " and PLA.KD_JNS_PLAFOND ='" + Ext.getCmp('cboTrans_LapPagu').getValue() + "'";
	// }
	strKriteria += Ext.getCmp('comboUnitKerja_LapPagu').getValue()+' '+"##";
	strKriteria += Ext.getCmp('cboTahun_LapPagu').getValue() +' '+"##";		
	strKriteria += ""+"##";	//strKriteria += Ext.getCmp('cboTrans_LapPagu').getValue() +' '+"##";		
	strKriteria += " "+ 1 + "##"
	
	return strKriteria;
}

function Validasi_LapPagu()
{
	var x = 1;
	
	if(Ext.getCmp('cboTahun_LapPagu').getValue() == '')
	{
		ShowPesanWarning_LapPagu('Pilihan Tahun Anggaran salah!','Laporan Rekap RAPB ');
		x = 0;
	}
	
	/*if(Ext.getCmp('cboTrans_LapPagu').getValue() == '')
	{
		ShowPesanWarning_LapPagu('Pilihan Transaksi Anggaran salah!','Laporan Rekap RAPB ');
		x = 0;
	}
	/* if(Ext.getCmp('comboUnitKerja_LapPagu').getValue() == '' )
	{
		ShowPesanWarning_LapPagu('Pilihan Unit/Sub '+gstrSatker+' salah!','Laporan RAPB Pengembangan');
		x = 0;
	} 
	if(Ext.getCmp('optPN_LapPagu').getValue() == '' )
	{
		ShowPesanWarning_LapPagu('Pilihan Jenis RAPB salah!','Laporan Rekap RAPB ');
		x = 0;
	}*/

	return x;
};

function ShowPesanWarning_LapPagu(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};