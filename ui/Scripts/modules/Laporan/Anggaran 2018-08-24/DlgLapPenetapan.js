﻿var dsUnitKerja_LapPengesahanRKAT;
var selectUnitKerja_LapPengesahanRKAT;
var now_LapPengesahanRKAT = new Date();
// var winTitle_LapPengesahanRKAT = varLapMainPage +' '+ CurrentPage.title;;//'Pengesahan RAPB';
var winTitle_LapPengesahanRKAT = 'Laporan Pengesahan RAB';

var winDlg_LapPengesahanRKAT= fnDlg_LapPengesahanRKAT();
winDlg_LapPengesahanRKAT.show();

function fnDlg_LapPengesahanRKAT()
{  	
    var winDlg_LapPengesahanRKAT = new Ext.Window
	(
		{ 
			id: 'winDlg_LapPengesahanRKAT',
			title: winTitle_LapPengesahanRKAT,
			closeAction: 'destroy',
			width:380,
			height: 150,
			resizable:false,
			border: false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [getItemDlg_LapPengesahanRKAT()]
			
		}
	);
	
    return winDlg_LapPengesahanRKAT; 
};


function getItemDlg_LapPengesahanRKAT() 
{	
	var Pnl_LapPengesahanRKAT = new Ext.Panel
	(
		{ 
			id: 'Pnl_LapPengesahanRKAT',
			fileUpload: true,
			layout: 'anchor',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 350,
							height: 70,
							items: 
							[
								{
									xtype: 'compositefield',
									fieldLabel: 'Thn. Anggaran',
									items: 
									[
										mComboTahun_LapPengesahanRKAT(),
										{
											xtype: 'displayfield',
											value: 's/d ',									
											width: 20
										},
										mComboTahun2_LapPengesahanRKAT()
									]
								}
							]
						}
					]
				},				
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'rigth'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOk_LapPengesahanRKAT',
							handler: function() 
							{
								if(Validasi_LapPengesahanRKAT() == 1)
								{
									/* var criteria = getKriteria_LapPengesahanRKAT();
									ShowReport('', '011302', criteria); */
									var params={
										tahun_anggaran1	:Ext.getCmp('cboTahun_LapPengesahanRKAT').getValue(),
										tahun_anggaran2	:Ext.getCmp('cboTahun2_LapPengesahanRKAT').getValue(),
										
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/anggaran_module/functionLapPengesahan/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();		
									winDlg_LapPengesahanRKAT.close();
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancel_LapPengesahanRKAT',
							handler: function() 
							{
								winDlg_LapPengesahanRKAT.close();
							}
						}
					]
				}
			]
		}
	);
 
    return Pnl_LapPengesahanRKAT;
};

function ShowPesanWarningRKAReport_LapPengesahanRKAT(str,modul)
{
	Ext.MessageBox.show
	(		
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function mcomboUnitKerja_LapPengesahanRKAT()
{
	var Field = ['KD_UNIT_KERJA', 'NAMA_UNIT_KERJA','UNITKERJA'];
	dsUnitKerja_LapPengesahanRKAT = new WebApp.DataStore({ fields: Field });

  var comboUnitKerja_LapPengesahanRKAT = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_LapPengesahanRKAT',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: gstrSatker,			
			align:'Right',
			anchor:'96%',
			valueField: 'KD_UNIT_KERJA',
			displayField: 'UNITKERJA',
			store:dsUnitKerja_LapPengesahanRKAT,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnitKerja_LapPengesahanRKAT=b.data.KD_UNIT_KERJA ;
				} 
			}
		}
	);

    dsUnitKerja_LapPengesahanRKAT.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'KD_UNIT_KERJA', 
				Sortdir: 'ASC', 
				target:'viCboUnitKerja',
				param: ''
			} 
		}
	);
	return comboUnitKerja_LapPengesahanRKAT;
};

function mComboTahun_LapPengesahanRKAT()
{
  // var currYear = parseInt(gstrTahunAngg);//parseInt(now_LapPengesahanRKAT.format('Y'));
  var currYear = parseInt(now_LapPengesahanRKAT.format('Y'));;
  var cboTahun_LapPengesahanRKAT = new Ext.form.ComboBox
	(
		{
			id:'cboTahun_LapPengesahanRKAT',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:90,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					// data: [[1, currYear+1 +'/'+(Ext.num(gstrTahunAngg)+2)], [2, currYear +'/'+(Ext.num(gstrTahunAngg)+1)], [3, currYear-1 +'/'+gstrTahunAngg], [4, currYear-2 +'/'+(Ext.num(gstrTahunAngg)-1)]]
					data:[						
									[currYear + 1,currYear + 1 +'/' + (Ext.num(currYear) + 2)], 
									[currYear,currYear +'/' + (Ext.num(currYear) + 1)]
								]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			// value: currYear +'/'+(Ext.num(gstrTahunAngg)+1)//now_LapRKARutin.format('Y')			
			value:Ext.num(now_LapPengesahanRKAT.format('Y'))
		}
	);
	
	return cboTahun_LapPengesahanRKAT;
};

function mComboTahun2_LapPengesahanRKAT()
{
  var currYear = parseInt(now_LapPengesahanRKAT.format('Y'));
  var cboTahun2_LapPengesahanRKAT = new Ext.form.ComboBox
	(
		{
			id:'cboTahun2_LapPengesahanRKAT',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:90,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					// data: [[1, (Ext.num(gstrTahunAngg)+1) +'/'+(Ext.num(gstrTahunAngg)+2)], [2, (Ext.num(gstrTahunAngg)+0) +'/'+(Ext.num(gstrTahunAngg)+1)], [3, (Ext.num(gstrTahunAngg)-1) +'/'+(Ext.num(gstrTahunAngg)+0)], [4, (Ext.num(gstrTahunAngg)-2) +'/'+(Ext.num(gstrTahunAngg)-1)]]
					data:[						
									[currYear + 1,currYear + 1 +'/' + (Ext.num(currYear) + 2)], 
									[currYear,currYear +'/' + (Ext.num(currYear) + 1)]
								]
				}
			),
			valueField: 'Id',
			displayField: 'displayText'	,		
			// value: (Ext.num(gstrTahunAngg)+1)+'/'+(Ext.num(gstrTahunAngg)+2)//now_LapPagu.format('Y')						
			value:Ext.num(now_LapPengesahanRKAT.format('Y'))		
		}
	);
	
	return cboTahun2_LapPengesahanRKAT;
};

/* function getItemKelompokRKA_LapPengesahanRKAT()
{
	var cboKelRKA_LapPengesahanRKAT = new Ext.form.ComboBox
	(
		{
			id:'optPN_LapPengesahanRKAT',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Jenis ',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[2, "Pengembangan"], [1, "Rutin"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboKelRKA_LapPengesahanRKAT;
} */

function getKriteria_LapPengesahanRKAT()
{
	var strKriteria = "";	
	
	if(
		Ext.getCmp('cboTahun_LapPengesahanRKAT').getValue() !== undefined || Ext.getCmp('cboTahun_LapPengesahanRKAT').getValue() != "" &&
		Ext.getCmp('cboTahun2_LapPengesahanRKAT').getValue() !== undefined || Ext.getCmp('cboTahun2_LapPengesahanRKAT').getValue() != ""
		)
	{
		// strKriteria = " Where AR.TAHUN_ANGGARAN_TA >='" + Ext.getCmp('cboTahun_LapPengesahanRKAT').getValue() + "' ";
		// strKriteria += " and AR.TAHUN_ANGGARAN_TA <='" + Ext.getCmp('cboTahun2_LapPengesahanRKAT').getValue() + "'";	
		strKriteria = Ext.getCmp('cboTahun_LapPengesahanRKAT').getValue() + "' ";
		strKriteria += " "+ "##"+ Ext.getCmp('cboTahun2_LapPengesahanRKAT').getValue() + "'";	
		
		// strKriteria += " "+ "##"+ "Tahun " +Ext.getCmp('cboTahun_LapPengesahanRKAT').getValue() + "";	
  //       strKriteria += "  s.d Tahun " + Ext.getCmp('cboTahun2_LapPengesahanRKAT').getValue() + "";			
	}
	
	strKriteria += " "+ "##"
	+ 1 + "##"
	
	return strKriteria;
}

function Validasi_LapPengesahanRKAT()
{
	var x = 1;
	
	if(Ext.getCmp('cboTahun_LapPengesahanRKAT').getValue() == '')
	{
		ShowPesanWarning_LapPengesahanRKAT('Pilihan Tahun Anggaran salah!','Laporan Rekap RAPB ');
		x = 0;
	}
	/* if(Ext.getCmp('comboUnitKerja_LapPengesahanRKAT').getValue() == '' )
	{
		ShowPesanWarning_LapPengesahanRKAT('Pilihan Unit/Sub Satuan kerja salah!','Laporan RAPB Pengembangan');
		x = 0;
	} 
	if(Ext.getCmp('optPN_LapPengesahanRKAT').getValue() == '' )
	{
		ShowPesanWarning_LapPengesahanRKAT('Pilihan Jenis RAPB salah!','Laporan Rekap RAPB ');
		x = 0;
	}*/

	return x;
};

function ShowPesanWarning_LapPengesahanRKAT(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};