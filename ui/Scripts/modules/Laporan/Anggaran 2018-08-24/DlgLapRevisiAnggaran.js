﻿var dsUnitKerja_LapRevisiAnggaran;
var selectUnitKerja_LapRevisiAnggaran;
var now = new Date();
var selectStsApp_LapRevisiAnggaran;
// var winTitle_LapRevisiAnggaran =  varLapMainPage +' '+ CurrentPage.title;//'Revisi Anggaran';
var winTitle_LapRevisiAnggaran =  'Laporan Revisi Anggaran'; 

var winDlg_LapRevisiAnggaran= fnDlg_LapRevisiAnggaran();
winDlg_LapRevisiAnggaran.show();
// Ext.get('comboAppRevisiAnggaran').dom.value = "Belum Approve";


function fnDlg_LapRevisiAnggaran()
{  	
    var winDlg_LapRevisiAnggaran = new Ext.Window
	(
		{ 
			id: 'winDlg_LapRevisiAnggaran',
			title: winTitle_LapRevisiAnggaran,
			closeAction: 'destroy',
			width:410,
			height: 180,
			resizable:false,
			border: false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [getItemDlg_LapRevisiAnggaran()]			
		}
	);
	
	var chkApprove_LapRevisiAnggaran = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprove_LapRevisiAnggaran',
			header: "APPROVE",
			align: 'center',
			disable:false,
			dataIndex: 'APPROVE',
			width: 70
		}
	);	
	selectUnitKerja_LapRevisiAnggaran='';
	selectStsApp_LapRevisiAnggaran='';
	
    return winDlg_LapRevisiAnggaran; 
};

function getItemDlg_LapRevisiAnggaran() 
{	
	var Pnl_LapRevisiAnggaran = new Ext.Panel
	(
		{ 
			id: 'Pnl_LapRevisiAnggaran',
			fileUpload: true,
			layout: 'anchor',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 380,
							height: 100,
							items: 
							[
								
								// {
									// xtype: 'compositefield',
									// fieldLabel: 'Satuan kerja',
									// items: 
									// [
										// mcomboUnitKerja_LapRevisiAnggaran()
									// ]
								// },
								{
									xtype: 'compositefield',
									fieldLabel: 'Tanggal',
									items: 
									[
										{
											xtype: 'datefield',
											fieldLabel: '',
											name: 'DtpAwal_LapRevisiAnggaran',
											id: 'DtpAwal_LapRevisiAnggaran',
											format: 'd/M/Y',
											width: 105,
											value:now
											//anchor: '35%'
										},
										{
											xtype: 'displayfield',
											flex: 1,
											width: 15,
											name: '',
											value: 'S.d',
											fieldLabel: 'Label',
											id: 'lblnopeg_LapRevisiAnggaran',
											name: 'lblnopeg_LapRevisiAnggaran'
										},
										{
											xtype: 'datefield',
											fieldLabel: 'Tanggal',
											name: 'DtpAkhir_LapRevisiAnggaran',
											id: 'DtpAkhir_LapRevisiAnggaran',
											format: 'd/M/Y',
											width: 105,
											value:now
											//anchor: '35%'
										}
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Status',
									items: 
									[
										// mcomboLapRekap()
										mcomboAppRevisiAnggaran()
									]
								}
							]
						}
					]
				},				
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'rigth'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOk_LapRevisiAnggaran',
							handler: function() 
							{
								if(Validasi_LapRevisiAnggaran() == 1)
								{
									var params={
										tgl_awal		:Ext.getCmp('DtpAwal_LapRevisiAnggaran').getValue(),
										tgl_akhir		:Ext.getCmp('DtpAkhir_LapRevisiAnggaran').getValue(),
										app_revisi		:Ext.getCmp('comboAppRevisiAnggaran').getValue(),
										status_app		:Ext.get('comboAppRevisiAnggaran').getValue(),
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/anggaran_module/functionLapRevisi/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									winDlg_LapRevisiAnggaran.close();
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancel_LapRevisiAnggaran',
							handler: function() 
							{
								winDlg_LapRevisiAnggaran.close();
							}
						}
					]
				}
			]
		}
	);
 
    return Pnl_LapRevisiAnggaran;
};

function ShowPesanWarningRKAReport_LapRevisiAnggaran(str,modul)
{
	Ext.MessageBox.show
	(		
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

// function mcomboUnitKerja_LapRevisiAnggaran()
// {
	// var Field = ['KD_UNIT_KERJA', 'NAMA_UNIT_KERJA','UNITKERJA'];
	// dsUnitKerja_LapRevisiAnggaran = new WebApp.DataStore({ fields: Field });

  // var comboUnitKerja_LapRevisiAnggaran = new Ext.form.ComboBox
	// (
		// {
		
		// // id:Nama_ID,
			// // typeAhead: true,
			// // triggerAction: 'all',
			// // lazyRender:true,
			// // mode: 'local',
			// // emptyText:'',
			// // fieldLabel: "Kategori",			
			// // width:lebar,
		
			// id:'comboUnitKerja_LapRevisiAnggaran',
			// typeAhead: true,
			// triggerAction: 'all',
			// lazyRender:true,
			// mode: 'local',
			// emptyText:'Semua Unit/Sub Satuan kerja',
			// fieldLabel: 'Satuan kerja ',			
			// align:'Right',
			// width:240,
			// valueField: 'KD_UNIT_KERJA',
			// displayField: 'UNITKERJA',
			// store:dsUnitKerja_LapRevisiAnggaran,
			// listeners:  
			// {
				// 'select': function(a,b,c)
				// {   
					// selectUnitKerja_LapRevisiAnggaran=b.data.KD_UNIT_KERJA ;
				// } 
			// }
		// }
	// );

    // dsUnitKerja_LapRevisiAnggaran.load
	// (
		// { 
			// params: 
			// { 
				// Skip: 0, 
				// Take: 1000, 
				// Sort: 'KD_UNIT_KERJA', 
				// Sortdir: 'ASC', 
				// target:'viCboUnitKerja',
				// param: "kdunit=" + gstrListUnitKerja
			// } 
		// }
	// );
	// return comboUnitKerja_LapRevisiAnggaran;
// };

function mComboTahun_LapRevisiAnggaran()
{
  var currYear = parseInt(now_LapRevisiAnggaran.format('Y'));
  var cboTahun_LapRevisiAnggaran = new Ext.form.ComboBox
	(
		{
			id:'cboTahun_LapRevisiAnggaran',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, currYear + 2], [2, currYear + 1], [3, currYear], [4, currYear - 1], [5, currYear - 2]]
				}
			),
			valueField: 'displayText',
			displayField: 'displayText',
			value: now_LapRevisiAnggaran.format('Y')			
		}
	);
	
	return cboTahun_LapRevisiAnggaran;
};

function mcomboAppRevisiAnggaran()
{	
	var comboAppRevisiAnggaran = new Ext.form.ComboBox
	(
		{
			id:'comboAppRevisiAnggaran',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Status Approve',
			fieldLabel: 'Status Approve',			
			align:'Right',
			width:240,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[0, 'Belum Approve'], [1, 'Sudah Approve']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			// value:'0',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectStsApp_LapRevisiAnggaran=b.data.Id ;					
				} 
			}
		}
	);
	
	return comboAppRevisiAnggaran;
} ;

// function mcomboLapRekap()
// {
	// var Field = ['Account','Name','Groups','AKUN'];
	// dsLapRekap = new WebApp.DataStore({ fields: Field });
	// dsLapRekap.load
	// (
		// {
			// params:
			// {
			    // Skip: 0,
			    // Take: 1000,
			    // Sort: '',
			    // Sortdir: 'ASC',
			    // target: 'viewCboaktivaFilter',
			    // param: 'REKAP'
			// }
		// }
	// );
	
 // var comboLapRekap = new Ext.form.ComboBox
	// (
		// {
			// id:'comboLapRekap',
			// typeAhead: true,
			// triggerAction: 'all',
			// lazyRender:true,
			// mode: 'local',
			// emptyText:'Pilih Kas / Bank...',
			// fieldLabel: 'Kas / Bank ',			
			// align:'Right',
			// width:240,
			// store: dsLapRekap,
			// valueField: 'Account',
			// displayField: 'AKUN',
			// listeners:  
			// {
				// 'select': function(a,b,c)
				// {   
					// selectStsApp_LapRevisiAnggaran=b.data.Account ;
					
				// } 
			// }
		// }
	// );
	
	// return comboLapRekap;
// } ;


/* function getItemKelompokRKA_LapRevisiAnggaran()
{
	var cboKelRKA_LapRevisiAnggaran = new Ext.form.ComboBox
	(
		{
			id:'optPN_LapRevisiAnggaran',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Jenis ',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[2, "Pengembangan"], [1, "Rutin"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboKelRKA_LapRevisiAnggaran;
} */

function getKriteria_LapRevisiAnggaran()
{
	var strKriteria = "";	
	
	
	
	strKriteria =  ShowDate(Ext.getCmp('DtpAwal_LapRevisiAnggaran').getValue());
	strKriteria +=  "##"+ShowDate(Ext.getCmp('DtpAkhir_LapRevisiAnggaran').getValue()); ;
	strKriteria += "##test";
	strKriteria += "##"+ selectStsApp_LapRevisiAnggaran ;	
	strKriteria += ""+ "##"
	+ 1 + "##"
	
	return strKriteria;
}

function Validasi_LapRevisiAnggaran()
{
	var x = 1;
	
	if(Ext.getCmp('DtpAkhir_LapRevisiAnggaran').getValue() == '')
	{
		ShowPesanWarning_LapRevisiAnggaran('Tanggal Akhir Belum di isi!','Laporan Revisi RAB ');
		x = 0;
	}
	if(Ext.getCmp('DtpAwal_LapRevisiAnggaran').getValue() == '')
	{
		ShowPesanWarning_LapRevisiAnggaran('Tanggal Awal Belum di isi!','Laporan Revisi RAB ');
		x = 0;
	}
	/* if(Ext.getCmp('comboUnitKerja_LapRevisiAnggaran').getValue() == '' )
	{
		ShowPesanWarning_LapRevisiAnggaran('Pilihan Unit/Sub Satuan kerja salah!','Laporan RAPB Pengembangan');
		x = 0;
	} 
	*/
	if(Ext.getCmp('comboAppRevisiAnggaran').getValue() == '' )
	{
		ShowPesanWarning_LapRevisiAnggaran('Status Approve belum dipilih!','Laporan Revisi RAB ');
		x = 0;
	}

	return x;
};

function ShowPesanWarning_LapRevisiAnggaran(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};