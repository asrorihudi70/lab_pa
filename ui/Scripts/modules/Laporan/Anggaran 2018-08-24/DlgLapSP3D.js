﻿var dsUnitKerja_LapRekapSp3d;
var selectUnitKerja_LapRekapSp3d;
var now = new Date();
var selectaccount_LapRekapSp3d;
// var winTitle_LapRekapSp3d =  varLapMainPage +' '+ CurrentPage.title;//'Rekap Pengajuan Pencairan Dana';
var winTitle_LapRekapSp3d =  'Rekap Pengajuan Pencairan Dana';

var winDlg_LapRekapSp3d= fnDlg_LapRekapSp3d();
winDlg_LapRekapSp3d.show();

function fnDlg_LapRekapSp3d()
{  	
    var winDlg_LapRekapSp3d = new Ext.Window
	(
		{ 
			id: 'winDlg_LapRekapSp3d',
			title: winTitle_LapRekapSp3d,
			closeAction: 'destroy',
			width:410,
			height: 160,
			resizable:false,
			border: false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [getItemDlg_LapRekapSp3d()]
			
		}
	);
	
	var chkApprove_LapRekapSp3d = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprove_LapRekapSp3d',
			header: "APPROVE",
			align: 'center',
			disable:false,
			dataIndex: 'APPROVE',
			width: 70
		}
	);	
	selectUnitKerja_LapRekapSp3d='';
	selectaccount_LapRekapSp3d='';
	
    return winDlg_LapRekapSp3d; 
};

function getItemDlg_LapRekapSp3d() 
{	
	var Pnl_LapRekapSp3d = new Ext.Panel
	(
		{ 
			id: 'Pnl_LapRekapSp3d',
			fileUpload: true,
			layout: 'anchor',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 380,
							height: 80,
							items: 
							[
								
								{
									xtype: 'compositefield',
									fieldLabel: 'Unit Kerja',
									items: 
									[
										mcomboUnitKerja_LapRekapSp3d()
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Tanggal',
									items: 
									[
										{
											xtype: 'datefield',
											fieldLabel: '',
											name: 'DtpAwal_LapRekapSp3d',
											id: 'DtpAwal_LapRekapSp3d',
											format: 'd/M/Y',
											width: 105,
											value:now
											//anchor: '35%'
										},
										{
											xtype: 'displayfield',
											flex: 1,
											width: 15,
											name: '',
											value: 'S.d',
											fieldLabel: 'Label',
											id: 'lblnopeg_LapRekapSp3d',
											name: 'lblnopeg_LapRekapSp3d'
										},
										{
											xtype: 'datefield',
											fieldLabel: 'Tanggal',
											name: 'DtpAkhir_LapRekapSp3d',
											id: 'DtpAkhir_LapRekapSp3d',
											format: 'd/M/Y',
											width: 105,
											value:now
											//anchor: '35%'
										}
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Sumber',
									hidden:true	,
									items: 
									[
										mcomboLapRekap()
									]
								}
							]
						}
					]
				},				
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'rigth'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOk_LapRekapSp3d',
							handler: function() 
							{
								if(Validasi_LapRekapSp3d() == 1)
								{
									var params={
										tgl_awal	:Ext.getCmp('DtpAwal_LapRekapSp3d').getValue(), 
										tgl_akhir	:Ext.getCmp('DtpAkhir_LapRekapSp3d').getValue(),
										unit_kerja	:Ext.getCmp('comboUnitKerja_LapRekapSp3d').getValue(),
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/anggaran_module/functionLapRekapPPD/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									// winDlg_LapRekapSp3d.close();
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancel_LapRekapSp3d',
							handler: function() 
							{
								winDlg_LapRekapSp3d.close();
							}
						}
					]
				}
			]
		}
	);
 
    return Pnl_LapRekapSp3d;
};

function ShowPesanWarningRKAReport_LapRekapSp3d(str,modul)
{
	Ext.MessageBox.show
	(		
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function mcomboUnitKerja_LapRekapSp3d()
{
	var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
	dsUnitKerja_LapRekapSp3d = new WebApp.DataStore({ fields: Field });

  var comboUnitKerja_LapRekapSp3d = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_LapRekapSp3d',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Semua Unit Kerja',
			fieldLabel: 'Unit Kerja ',			
			align:'Right',
			width:240,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			store:dsUnitKerja_LapRekapSp3d,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnitKerja_LapRekapSp3d=b.data.kd_unit ;
				} 
			}
		}
	);

     Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerja_LapRekapSp3d.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerja_LapRekapSp3d.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerja_LapRekapSp3d.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				// ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	return comboUnitKerja_LapRekapSp3d;
};

function mComboTahun_LapRekapSp3d()
{
  var currYear = parseInt(now_LapRekapSp3d.format('Y'));
  var cboTahun_LapRekapSp3d = new Ext.form.ComboBox
	(
		{
			id:'cboTahun_LapRekapSp3d',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, currYear + 2], [2, currYear + 1], [3, currYear], [4, currYear - 1], [5, currYear - 2]]
				}
			),
			valueField: 'displayText',
			displayField: 'displayText',
			value: now_LapRekapSp3d.format('Y')			
		}
	);
	
	return cboTahun_LapRekapSp3d;
};


function mcomboLapRekap()
{
	var Field = ['Account','Name','Groups','AKUN'];
	dsLapRekap = new WebApp.DataStore({ fields: Field });
	dsLapRekap.load
	(
		{
			params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: 'ASC',
			    target: 'viewCboaktivaFilter',
			    param: 'REKAP'
			}
		}
	);
	
 var comboLapRekap = new Ext.form.ComboBox
	(
		{
			id:'comboLapRekap',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Kas / Bank...',
			fieldLabel: 'Kas / Bank ',			
			align:'Right',
			width:240,
			store: dsLapRekap,
			valueField: 'Account',
			displayField: 'AKUN',
			hidden:true,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectaccount_LapRekapSp3d=b.data.Account ;
					
				} 
			}
		}
	);
	
	return comboLapRekap;
} ;


/* function getItemKelompokRKA_LapRekapSp3d()
{
	var cboKelRKA_LapRekapSp3d = new Ext.form.ComboBox
	(
		{
			id:'optPN_LapRekapSp3d',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Jenis ',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[2, "Pengembangan"], [1, "Rutin"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboKelRKA_LapRekapSp3d;
} */

function getKriteria_LapRekapSp3d()
{
	var strKriteria = "";	
	
	
	
	strKriteria =  ShowDate(Ext.getCmp('DtpAwal_LapRekapSp3d').getValue());
	strKriteria +=  "##"+ShowDate(Ext.getCmp('DtpAkhir_LapRekapSp3d').getValue());
	strKriteria += "##"+ selectUnitKerja_LapRekapSp3d ;
	strKriteria += "##"+ selectaccount_LapRekapSp3d ;
	
	strKriteria += ""+ "##"
	+ 1 + "##"
	
	return strKriteria;
}

function Validasi_LapRekapSp3d()
{
	var x = 1;
	
	if(Ext.getCmp('DtpAkhir_LapRekapSp3d').getValue() == '')
	{
		ShowPesanWarning_LapRekapSp3d('Tanggal Akhir Belum di isi!','Laporan Rekap RAPB ');
		x = 0;
	}
	if(Ext.getCmp('DtpAwal_LapRekapSp3d').getValue() == '')
	{
		ShowPesanWarning_LapRekapSp3d('Tanggal Awal Belum di isi!','Laporan Rekap RAPB ');
		x = 0;
	}
	/* if(Ext.getCmp('comboUnitKerja_LapRekapSp3d').getValue() == '' )
	{
		ShowPesanWarning_LapRekapSp3d('Pilihan Unit/Sub '+gstrSatker+' salah!','Laporan RAPB Pengembangan');
		x = 0;
	} 
	if(Ext.getCmp('optPN_LapRekapSp3d').getValue() == '' )
	{
		ShowPesanWarning_LapRekapSp3d('Pilihan Jenis RAPB salah!','Laporan Rekap RAPB ');
		x = 0;
	}*/

	return x;
};

function ShowPesanWarning_LapRekapSp3d(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};