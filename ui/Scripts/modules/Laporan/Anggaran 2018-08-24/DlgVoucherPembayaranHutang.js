
var dsLapVoucherPembayaranHutang;
var selectNamaLapVoucherPembayaranHutang;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapVoucherPembayaranHutang;
var varLapVoucherPembayaranHutang= ShowFormLapVoucherPembayaranHutang();

function ShowFormLapVoucherPembayaranHutang()
{
    frmDlgLapVoucherPembayaranHutang= fnDlgLapVoucherPembayaranHutang();
    frmDlgLapVoucherPembayaranHutang.show();
};

function fnDlgLapVoucherPembayaranHutang()
{
    var winLapVoucherPembayaranHutangReport = new Ext.Window
    (
        {
            id: 'winLapVoucherPembayaranHutangReport',
            title: 'Voucher Penerimaan Piutang',
            closeAction: 'destroy',
            width: 480,
            height: 240,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapVoucherPembayaranHutang()],
            listeners:
			{
				activate: function()
				{
				   
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapVoucherPembayaranHutang',
					handler: function()
					{
						if(Ext.getCmp('cboDariVendorVoucherPembayaranHutang').getValue() == ''){
							ShowPesanWarningLapVoucherPenerimaanPiutangDetailReport('Dari vendor tidak boleh kosong!','WARNING');
						} else if(Ext.getCmp('cboKeVendor_VoucherPembayaranHutang').getValue() == ''){
							ShowPesanWarningLapVoucherPenerimaanPiutangDetailReport('Ke vendor tidak boleh kosong!','WARNING');
						} else{
							var params={
								dari_kd_vendor:Ext.getCmp('cboDariVendorVoucherPembayaranHutang').getValue(),
								ke_kd_vendor:Ext.getCmp('cboKeVendor_VoucherPembayaranHutang').getValue(),
								dari_vendor:Ext.get('cboDariVendorVoucherPembayaranHutang').getValue(),
								ke_vendor:Ext.get('cboKeVendor_VoucherPembayaranHutang').getValue(),
								tglAwal:Ext.getCmp('dtpTglAwalVoucherPembayaranHutang').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirVoucherPembayaranHutang').getValue(),
								numberAwal:Ext.getCmp('cboNumberAwal_VoucherPembayaranHutang').getValue(),
								numberAkhir:Ext.getCmp('cboNumberAkhir_VoucherPembayaranHutang').getValue(),
								orderby:Ext.get('cboOrderBY_VoucherPembayaranHutang').getValue(),
							} 

							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/keuangan/lap_voucher/cetakVoucherPembayaranHutang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
						}
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapVoucherPembayaranHutang',
					handler: function()
					{
							frmDlgLapVoucherPembayaranHutang.close();
					}
				}
			]
        }
    );

    return winLapVoucherPembayaranHutangReport;
};


function ItemDlgLapVoucherPembayaranHutang()
{
    var PnlLapVoucherPembayaranHutang = new Ext.Panel
    (
        {
            id: 'PnlLapVoucherPembayaranHutang',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapVoucherPembayaranHutang_Customer(),
				{xtype: 'tbspacer',height: 10, width:5},
				getItemLapVoucherPembayaranHutang_Periode()
            ]
        }
    );

    return PnlLapVoucherPembayaranHutang;
};

function GetCriteriaLapVoucherPembayaranHutang()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterVoucherPembayaranHutang').getValue();
	return strKriteria;
};

function getItemLapVoucherPembayaranHutang_Customer()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Dari Vendor'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboDariVendor_VoucherPembayaranHutang(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Ke Vendor'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				ComboKeVendorVoucherPembayaranHutang()
            ]
        }]
    };
    return items;
};

function getItemLapVoucherPembayaranHutang_Periode()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Order by'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboOrderBYVoucherPembayaranHutang(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Periode'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				{
					x: 100,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAwalVoucherPembayaranHutang',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAwalVoucherPembayaranHutang(),
				{
					x: 260,
					y: 40,
					xtype: 'label',
					text: ' s/d '
				},
				{
					x: 285,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAkhirVoucherPembayaranHutang',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAkhirVoucherPembayaranHutang()
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapVoucherPembayaranHutangReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkoderec;
function mComboRekening()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboRekening = new WebApp.DataStore({fields: Field});
    dsUnitComboRekening.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboRekening',
                param: " MA_type = 'D'  and isdebit = 't' order by MA.ma_id"
            }
        }
    );
    var cboRekening = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboRekening',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:350,
                store: dsUnitComboRekening,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkoderec=b.data.displayText ;
                        }
                }
            }
    );
    return cboRekening;
};

function ComboDariVendor_VoucherPembayaranHutang()
{
	var Fields = ['Vend_Code', 'Vendor', 'VendorName'];
    dsCboDariVendorVoucherPembayaranHutang = new WebApp.DataStore({ fields: Fields });
	
	dsCboDariVendorVoucherPembayaranHutang.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'Vendor', 
				Sortdir: 'ASC', 
				target: 'viewCboVendLengkap',
				param: ''
			} 
		}
	);
    
    var cbDariVendorVoucherPembayaranHutang = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboDariVendorVoucherPembayaranHutang',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Vendor ...',
		    fieldLabel: 'Vendor ',
		    align: 'right',
		    anchor:'90%',
		    listWidth:350,
		    store: dsCboDariVendorVoucherPembayaranHutang,
		    valueField: 'Vend_Code',
		    displayField: 'VendorName',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        // selectCboEntryCustARForm = b.data.kd_Vendor;
			    }
			}
		}
	);
	
	return cbDariVendorVoucherPembayaranHutang;
};

function ComboKeVendorVoucherPembayaranHutang()
{
	var Fields = ['Vend_Code', 'Vendor', 'VendorName'];
    dsCboKeVendor_VoucherPembayaranHutang = new WebApp.DataStore({ fields: Fields });
	
	dsCboKeVendor_VoucherPembayaranHutang.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'Vendor', 
				Sortdir: 'ASC', 
				target: 'viewCboVendLengkap',
				param: ''
			} 
		}
	);
    
    var cbKeVendor_VoucherPembayaranHutang = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 40,
		    id: 'cboKeVendor_VoucherPembayaranHutang',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Vendor ...',
		    fieldLabel: 'Vendor ',
		    align: 'right',
		    anchor:'90%',
		    listWidth:350,
		    store: dsCboKeVendor_VoucherPembayaranHutang,
		    valueField: 'Vend_Code',
		    displayField: 'VendorName',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        // selectCboEntryCustARForm = b.data.kd_Vendor;
			    }
			}
		}
	);
	
	return cbKeVendor_VoucherPembayaranHutang;
};
function ComboNumberAwalVoucherPembayaranHutang()
{
	var Fields = ['number'];
    dsCboNumberAwal_VoucherPembayaranHutang = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPembayaranHutang",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAwal_VoucherPembayaranHutang.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAwal_VoucherPembayaranHutang.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAwal_VoucherPembayaranHutang.add(recs);
			}
		}
	});
    
    var cbNumberAwal_VoucherPembayaranHutang = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 40,
		    id: 'cboNumberAwal_VoucherPembayaranHutang',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAwal_VoucherPembayaranHutang,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        
			    }
			}
		}
	);
	
	return cbNumberAwal_VoucherPembayaranHutang;
};

function ComboNumberAkhirVoucherPembayaranHutang()
{
	var Fields = ['number'];
    dsCboNumberAkhir_VoucherPembayaranHutang = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPembayaranHutang",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAkhir_VoucherPembayaranHutang.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAkhir_VoucherPembayaranHutang.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAkhir_VoucherPembayaranHutang.add(recs);
			}
		}
	});
    
    var cbNumberAkhir_VoucherPembayaranHutang = new Ext.form.ComboBox
	(
		{
			x: 285,
			y: 40,
		    id: 'cboNumberAkhir_VoucherPembayaranHutang',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAkhir_VoucherPembayaranHutang,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			    }
			}
		}
	);
	
	return cbNumberAkhir_VoucherPembayaranHutang;
};

function ComboOrderBYVoucherPembayaranHutang(){
	var cbOrderBY_VoucherPembayaranHutang = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboOrderBY_VoucherPembayaranHutang',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Oder by',
		    fieldLabel: 'Vendor ',
		    align: 'right',
		    anchor:'60%',
			value:'Tanggal',
		    store:  new Ext.data.ArrayStore
			(
					{
						id: 0,
						fields:
						[
							'Id',
							'displayText'
						],
						data: [[1, 'Tanggal'],[2, 'Number']]
					}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        if(b.data.displayText == 'Tanggal'){
						Ext.getCmp('cboNumberAkhir_VoucherPembayaranHutang').hide();
						Ext.getCmp('cboNumberAwal_VoucherPembayaranHutang').hide();
						Ext.getCmp('dtpTglAwalVoucherPembayaranHutang').show();
						Ext.getCmp('dtpTglAkhirVoucherPembayaranHutang').show();
					} else{
						Ext.getCmp('cboNumberAkhir_VoucherPembayaranHutang').show();
						Ext.getCmp('cboNumberAwal_VoucherPembayaranHutang').show();
						Ext.getCmp('dtpTglAwalVoucherPembayaranHutang').hide();
						Ext.getCmp('dtpTglAkhirVoucherPembayaranHutang').hide();
					}
			    }
			}
		}
	);
	
	return cbOrderBY_VoucherPembayaranHutang;
};