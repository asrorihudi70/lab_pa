
var dsLapVoucherPembayaranTunai;
var selectNamaLapVoucherPembayaranTunai;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapVoucherPembayaranTunai;
var varLapVoucherPembayaranTunai= ShowFormLapVoucherPembayaranTunai();

function ShowFormLapVoucherPembayaranTunai()
{
    frmDlgLapVoucherPembayaranTunai= fnDlgLapVoucherPembayaranTunai();
    frmDlgLapVoucherPembayaranTunai.show();
};

function fnDlgLapVoucherPembayaranTunai()
{
    var winLapVoucherPembayaranTunaiReport = new Ext.Window
    (
        {
            id: 'winLapVoucherPembayaranTunaiReport',
            title: 'Lap. Pembayaran Tunai',
            closeAction: 'destroy',
            width: 490,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapVoucherPembayaranTunai()],
            listeners:
			{
				activate: function()
				{
				   
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapVoucherPembayaranTunai',
					handler: function()
					{
						var params={
							tglAwal:Ext.getCmp('dtpTglAwalVoucherPembayaranTunai').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirVoucherPembayaranTunai').getValue(),
							numberAwal:Ext.getCmp('cboNumberAwal_VoucherPembayaranTunai').getValue(),
							numberAkhir:Ext.getCmp('cboNumberAkhir_VoucherPembayaranTunai').getValue(),
							orderby:Ext.get('cboOrderBY_VoucherPembayaranTunai').getValue(),
						} 

						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/keuangan/lap_voucher/cetakVoucherPembayaranTunai");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();		
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapVoucherPembayaranTunai',
					handler: function()
					{
							frmDlgLapVoucherPembayaranTunai.close();
					}
				}
			]

        }
    );

    return winLapVoucherPembayaranTunaiReport;
};


function ItemDlgLapVoucherPembayaranTunai()
{
    var PnlLapVoucherPembayaranTunai = new Ext.Panel
    (
        {
            id: 'PnlLapVoucherPembayaranTunai',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapVoucherPembayaranTunai_Tanggal(),
            ]
        }
    );

    return PnlLapVoucherPembayaranTunai;
};

function GetCriteriaLapVoucherPembayaranTunai()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterVoucherPembayaranTunai').getValue();
	return strKriteria;
};

function getItemLapVoucherPembayaranTunai_Tanggal()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Order by'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboOrderBYVoucherPembayaranTunai(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Periode'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				{
					x: 100,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAwalVoucherPembayaranTunai',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAwalVoucherPembayaranTunai(),
				{
					x: 260,
					y: 40,
					xtype: 'label',
					text: ' s/d '
				},
				{
					x: 285,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAkhirVoucherPembayaranTunai',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAkhirVoucherPembayaranTunai()
            ]
        }]
    };
    return items;
};


function ComboNumberAwalVoucherPembayaranTunai()
{
	var Fields = ['number'];
    dsCboNumberAwal_VoucherPembayaranTunai = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPembayaranTunai",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAwal_VoucherPembayaranTunai.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAwal_VoucherPembayaranTunai.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAwal_VoucherPembayaranTunai.add(recs);
			}
		}
	});
    
    var cbNumberAwal_VoucherPembayaranTunai = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 40,
		    id: 'cboNumberAwal_VoucherPembayaranTunai',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAwal_VoucherPembayaranTunai,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        
			    }
			}
		}
	);
	
	return cbNumberAwal_VoucherPembayaranTunai;
};

function ComboNumberAkhirVoucherPembayaranTunai()
{
	var Fields = ['number'];
    dsCboNumberAkhir_VoucherPembayaranTunai = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPembayaranTunai",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAkhir_VoucherPembayaranTunai.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAkhir_VoucherPembayaranTunai.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAkhir_VoucherPembayaranTunai.add(recs);
			}
		}
	});
    
    var cbNumberAkhir_VoucherPembayaranTunai = new Ext.form.ComboBox
	(
		{
			x: 285,
			y: 40,
		    id: 'cboNumberAkhir_VoucherPembayaranTunai',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAkhir_VoucherPembayaranTunai,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			    }
			}
		}
	);
	
	return cbNumberAkhir_VoucherPembayaranTunai;
};

function ComboOrderBYVoucherPembayaranTunai(){
	var cbOrderBY_VoucherPembayaranTunai = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboOrderBY_VoucherPembayaranTunai',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Oder by',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'60%',
			value:'Tanggal',
		    store:  new Ext.data.ArrayStore
			(
					{
						id: 0,
						fields:
						[
							'Id',
							'displayText'
						],
						data: [[1, 'Tanggal'],[2, 'Number']]
					}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        if(b.data.displayText == 'Tanggal'){
						Ext.getCmp('cboNumberAkhir_VoucherPembayaranTunai').hide();
						Ext.getCmp('cboNumberAwal_VoucherPembayaranTunai').hide();
						Ext.getCmp('dtpTglAwalVoucherPembayaranTunai').show();
						Ext.getCmp('dtpTglAkhirVoucherPembayaranTunai').show();
					} else{
						Ext.getCmp('cboNumberAkhir_VoucherPembayaranTunai').show();
						Ext.getCmp('cboNumberAwal_VoucherPembayaranTunai').show();
						Ext.getCmp('dtpTglAwalVoucherPembayaranTunai').hide();
						Ext.getCmp('dtpTglAkhirVoucherPembayaranTunai').hide();
					}
			    }
			}
		}
	);
	
	return cbOrderBY_VoucherPembayaranTunai;
};


function ShowPesanWarningLapVoucherPembayaranTunaiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkodedariAkun;
var tmpkodekeAkun;
function mComboDariAkunVoucherPembayaranTunai()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboDariAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboDariAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboAccount',
                param: ""
            }
        }
    );
    var cboDariAkun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboDariAkun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:360,
                store: dsUnitComboDariAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodedariAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboDariAkun;
};

function mComboKeAkunVoucherPembayaranTunai()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboKeAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboKeAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboAccount',
                param: " "
            }
        }
    );
    var cboKeAKun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 40,
                id:'cboKeAKun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:360,
                store: dsUnitComboKeAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodekeAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboKeAKun;
};

function mComboPilihanRangking()
{
    var cboPilihanLapPilihanRangking = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 100,
                id:'cboPilihanLapPilihanRangking',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: ' ',
                width:100,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Tanggal'], [2, 'Referensi'],[3, 'voucer']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Tanggal',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                // selectSetPilihan=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanLapPilihanRangking;
};

function mComboBendahara()
{
    var cboBendahara = new Ext.form.ComboBox
    (
            {
                x: 310,
                y: 100,
                id:'cboBendahara',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: ' ',
                width:150,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Bendahara Penerimaan'], [2, 'Bendahara Pengeluaran']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Bendahara Penerimaan',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                        }
                }
            }
    );
    return cboBendahara;
};