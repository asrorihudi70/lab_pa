
var dsLapVoucherPenerimaanPiutang;
var selectNamaLapVoucherPenerimaanPiutang;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapVoucherPenerimaanPiutang;
var varLapVoucherPenerimaanPiutang= ShowFormLapVoucherPenerimaanPiutang();

function ShowFormLapVoucherPenerimaanPiutang()
{
    frmDlgLapVoucherPenerimaanPiutang= fnDlgLapVoucherPenerimaanPiutang();
    frmDlgLapVoucherPenerimaanPiutang.show();
};

function fnDlgLapVoucherPenerimaanPiutang()
{
    var winLapVoucherPenerimaanPiutangReport = new Ext.Window
    (
        {
            id: 'winLapVoucherPenerimaanPiutangReport',
            title: 'Voucher Penerimaan Piutang',
            closeAction: 'destroy',
            width: 480,
            height: 240,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapVoucherPenerimaanPiutang()],
            listeners:
			{
				activate: function()
				{
				   
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapVoucherPenerimaanPiutang',
					handler: function()
					{
						if(Ext.getCmp('cboDariCustomerVoucherPenerimaanPiutang').getValue() == ''){
							ShowPesanWarningLapVoucherPenerimaanPiutangDetailReport('Dari customer tidak boleh kosong!','WARNING');
						} else if(Ext.getCmp('cboKeCustomer_VoucherPenerimaanPiutang').getValue() == ''){
							ShowPesanWarningLapVoucherPenerimaanPiutangDetailReport('Ke customer tidak boleh kosong!','WARNING');
						} else{
							var params={
								dari_kd_customer:Ext.getCmp('cboDariCustomerVoucherPenerimaanPiutang').getValue(),
								ke_kd_customer:Ext.getCmp('cboKeCustomer_VoucherPenerimaanPiutang').getValue(),
								dari_customer:Ext.get('cboDariCustomerVoucherPenerimaanPiutang').getValue(),
								ke_customer:Ext.get('cboKeCustomer_VoucherPenerimaanPiutang').getValue(),
								tglAwal:Ext.getCmp('dtpTglAwalVoucherPenerimaanPiutang').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirVoucherPenerimaanPiutang').getValue(),
								numberAwal:Ext.getCmp('cboNumberAwal_VoucherPenerimaanPiutang').getValue(),
								numberAkhir:Ext.getCmp('cboNumberAkhir_VoucherPenerimaanPiutang').getValue(),
								orderby:Ext.get('cboOrderBY_VoucherPenerimaanPiutang').getValue(),
							} 

							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/keuangan/lap_voucher/cetakVoucherPenerimaanPiutang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();	
						}							
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapVoucherPenerimaanPiutang',
					handler: function()
					{
							frmDlgLapVoucherPenerimaanPiutang.close();
					}
				}
			]
        }
    );

    return winLapVoucherPenerimaanPiutangReport;
};


function ItemDlgLapVoucherPenerimaanPiutang()
{
    var PnlLapVoucherPenerimaanPiutang = new Ext.Panel
    (
        {
            id: 'PnlLapVoucherPenerimaanPiutang',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapVoucherPenerimaanPiutang_Customer(),
				{xtype: 'tbspacer',height: 10, width:5},
				getItemLapVoucherPenerimaanPiutang_Periode()
            ]
        }
    );

    return PnlLapVoucherPenerimaanPiutang;
};

function GetCriteriaLapVoucherPenerimaanPiutang()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterVoucherPenerimaanPiutang').getValue();
	return strKriteria;
};

function getItemLapVoucherPenerimaanPiutang_Customer()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Dari Customer'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboDariCustomer_VoucherPenerimaanPiutang(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Ke Customer'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				ComboKeCustomerVoucherPenerimaanPiutang()
            ]
        }]
    };
    return items;
};

function getItemLapVoucherPenerimaanPiutang_Periode()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Order by'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboOrderBYVoucherPenerimaanPiutang(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Periode'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				{
					x: 100,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAwalVoucherPenerimaanPiutang',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAwalVoucherPenerimaanPiutang(),
				{
					x: 260,
					y: 40,
					xtype: 'label',
					text: ' s/d '
				},
				{
					x: 285,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAkhirVoucherPenerimaanPiutang',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAkhirVoucherPenerimaanPiutang()
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapVoucherPenerimaanPiutangReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkoderec;
function mComboRekening()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboRekening = new WebApp.DataStore({fields: Field});
    dsUnitComboRekening.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboRekening',
                param: " MA_type = 'D'  and isdebit = 't' order by MA.ma_id"
            }
        }
    );
    var cboRekening = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboRekening',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:350,
                store: dsUnitComboRekening,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkoderec=b.data.displayText ;
                        }
                }
            }
    );
    return cboRekening;
};

function ComboDariCustomer_VoucherPenerimaanPiutang()
{
	var Fields = ['kd_customer', 'customer', 'customer_name'];
    dsCboDariCustomerVoucherPenerimaanPiutang = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getCustomer",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbDariCustomerVoucherPenerimaanPiutang.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboDariCustomerVoucherPenerimaanPiutang.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboDariCustomerVoucherPenerimaanPiutang.add(recs);
			}
		}
	});
    
    var cbDariCustomerVoucherPenerimaanPiutang = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboDariCustomerVoucherPenerimaanPiutang',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih customer ...',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'90%',
		    listWidth:350,
		    store: dsCboDariCustomerVoucherPenerimaanPiutang,
		    valueField: 'kd_customer',
		    displayField: 'customer_name',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        // selectCboEntryCustARForm = b.data.kd_customer;
			    }
			}
		}
	);
	
	return cbDariCustomerVoucherPenerimaanPiutang;
};

function ComboKeCustomerVoucherPenerimaanPiutang()
{
	var Fields = ['kd_customer', 'customer', 'customer_name'];
    dsCboKeCustomer_VoucherPenerimaanPiutang = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getCustomer",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbKeCustomer_VoucherPenerimaanPiutang.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboKeCustomer_VoucherPenerimaanPiutang.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboKeCustomer_VoucherPenerimaanPiutang.add(recs);
			}
		}
	});
    
    var cbKeCustomer_VoucherPenerimaanPiutang = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 40,
		    id: 'cboKeCustomer_VoucherPenerimaanPiutang',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih customer ...',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'90%',
		    listWidth:350,
		    store: dsCboKeCustomer_VoucherPenerimaanPiutang,
		    valueField: 'kd_customer',
		    displayField: 'customer_name',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        // selectCboEntryCustARForm = b.data.kd_customer;
			    }
			}
		}
	);
	
	return cbKeCustomer_VoucherPenerimaanPiutang;
};

function ComboNumberAwalVoucherPenerimaanPiutang()
{
	var Fields = ['number'];
    dsCboNumberAwal_VoucherPenerimaanPiutang = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPenerimaanPiutang",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAwal_VoucherPenerimaanPiutang.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAwal_VoucherPenerimaanPiutang.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAwal_VoucherPenerimaanPiutang.add(recs);
			}
		}
	});
    
    var cbNumberAwal_VoucherPenerimaanPiutang = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 40,
		    id: 'cboNumberAwal_VoucherPenerimaanPiutang',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAwal_VoucherPenerimaanPiutang,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        
			    }
			}
		}
	);
	
	return cbNumberAwal_VoucherPenerimaanPiutang;
};

function ComboNumberAkhirVoucherPenerimaanPiutang()
{
	var Fields = ['number'];
    dsCboNumberAkhir_VoucherPenerimaanPiutang = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPenerimaanPiutang",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAkhir_VoucherPenerimaanPiutang.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAkhir_VoucherPenerimaanPiutang.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAkhir_VoucherPenerimaanPiutang.add(recs);
			}
		}
	});
    
    var cbNumberAkhir_VoucherPenerimaanPiutang = new Ext.form.ComboBox
	(
		{
			x: 285,
			y: 40,
		    id: 'cboNumberAkhir_VoucherPenerimaanPiutang',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAkhir_VoucherPenerimaanPiutang,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			    }
			}
		}
	);
	
	return cbNumberAkhir_VoucherPenerimaanPiutang;
};

function ComboOrderBYVoucherPenerimaanPiutang(){
	var cbOrderBY_VoucherPenerimaanPiutang = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboOrderBY_VoucherPenerimaanPiutang',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Oder by',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'60%',
			value:'Tanggal',
		    store:  new Ext.data.ArrayStore
			(
					{
						id: 0,
						fields:
						[
							'Id',
							'displayText'
						],
						data: [[1, 'Tanggal'],[2, 'Number']]
					}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        if(b.data.displayText == 'Tanggal'){
						Ext.getCmp('cboNumberAkhir_VoucherPenerimaanPiutang').hide();
						Ext.getCmp('cboNumberAwal_VoucherPenerimaanPiutang').hide();
						Ext.getCmp('dtpTglAwalVoucherPenerimaanPiutang').show();
						Ext.getCmp('dtpTglAkhirVoucherPenerimaanPiutang').show();
					} else{
						Ext.getCmp('cboNumberAkhir_VoucherPenerimaanPiutang').show();
						Ext.getCmp('cboNumberAwal_VoucherPenerimaanPiutang').show();
						Ext.getCmp('dtpTglAwalVoucherPenerimaanPiutang').hide();
						Ext.getCmp('dtpTglAkhirVoucherPenerimaanPiutang').hide();
					}
			    }
			}
		}
	);
	
	return cbOrderBY_VoucherPenerimaanPiutang;
};