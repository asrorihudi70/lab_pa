
var dsLapVoucherPenerimaanPiutangDetail;
var selectNamaLapVoucherPenerimaanPiutangDetail;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapVoucherPenerimaanPiutangDetail;
var varLapVoucherPenerimaanPiutangDetail= ShowFormLapVoucherPenerimaanPiutangDetail();

function ShowFormLapVoucherPenerimaanPiutangDetail()
{
    frmDlgLapVoucherPenerimaanPiutangDetail= fnDlgLapVoucherPenerimaanPiutangDetail();
    frmDlgLapVoucherPenerimaanPiutangDetail.show();
};

function fnDlgLapVoucherPenerimaanPiutangDetail()
{
    var winLapVoucherPenerimaanPiutangDetailReport = new Ext.Window
    (
        {
            id: 'winLapVoucherPenerimaanPiutangDetailReport',
            title: 'Voucher Penerimaan Piutang',
            closeAction: 'destroy',
            width: 480,
            height: 240,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapVoucherPenerimaanPiutangDetail()],
            listeners:
			{
				activate: function()
				{
				   
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapVoucherPenerimaanPiutangDetail',
					handler: function()
					{
						if(Ext.getCmp('cboDariCustomerVoucherPenerimaanPiutangDetail').getValue() == ''){
							ShowPesanWarningLapVoucherPenerimaanPiutangDetailReport('Dari customer tidak boleh kosong!','WARNING');
						} else if(Ext.getCmp('cboKeCustomer_VoucherPenerimaanPiutangDetail').getValue() == ''){
							ShowPesanWarningLapVoucherPenerimaanPiutangDetailReport('Ke customer tidak boleh kosong!','WARNING');
						} else{
							var params={
								dari_kd_customer:Ext.getCmp('cboDariCustomerVoucherPenerimaanPiutangDetail').getValue(),
								ke_kd_customer:Ext.getCmp('cboKeCustomer_VoucherPenerimaanPiutangDetail').getValue(),
								dari_customer:Ext.get('cboDariCustomerVoucherPenerimaanPiutangDetail').getValue(),
								ke_customer:Ext.get('cboKeCustomer_VoucherPenerimaanPiutangDetail').getValue(),
								tglAwal:Ext.getCmp('dtpTglAwalVoucherPenerimaanPiutangDetail').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirVoucherPenerimaanPiutangDetail').getValue(),
								numberAwal:Ext.getCmp('cboNumberAwal_VoucherPenerimaanPiutangDetail').getValue(),
								numberAkhir:Ext.getCmp('cboNumberAkhir_VoucherPenerimaanPiutangDetail').getValue(),
								orderby:Ext.get('cboOrderBY_VoucherPenerimaanPiutangDetail').getValue(),
							} 

							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/keuangan/lap_voucher/cetakVoucherPenerimaanPiutangDetail");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
						}
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapVoucherPenerimaanPiutangDetail',
					handler: function()
					{
						frmDlgLapVoucherPenerimaanPiutangDetail.close();
					}
				}
			]
        }
    );

    return winLapVoucherPenerimaanPiutangDetailReport;
};


function ItemDlgLapVoucherPenerimaanPiutangDetail()
{
    var PnlLapVoucherPenerimaanPiutangDetail = new Ext.Panel
    (
        {
            id: 'PnlLapVoucherPenerimaanPiutangDetail',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapVoucherPenerimaanPiutangDetail_Customer(),
				{xtype: 'tbspacer',height: 10, width:5},
				getItemLapVoucherPenerimaanPiutangDetail_Periode()
            ]
        }
    );

    return PnlLapVoucherPenerimaanPiutangDetail;
};

function GetCriteriaLapVoucherPenerimaanPiutangDetail()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterVoucherPenerimaanPiutangDetail').getValue();
	return strKriteria;
};

function getItemLapVoucherPenerimaanPiutangDetail_Customer()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Dari Customer'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboDariCustomer_VoucherPenerimaanPiutangDetail(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Ke Customer'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				ComboKeCustomerVoucherPenerimaanPiutangDetail()
            ]
        }]
    };
    return items;
};

function getItemLapVoucherPenerimaanPiutangDetail_Periode()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Order by'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboOrderBYVoucherPenerimaanPiutangDetail(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Periode'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				{
					x: 100,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAwalVoucherPenerimaanPiutangDetail',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAwalVoucherPenerimaanPiutangDetail(),
				{
					x: 260,
					y: 40,
					xtype: 'label',
					text: ' s/d '
				},
				{
					x: 285,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAkhirVoucherPenerimaanPiutangDetail',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				ComboNumberAkhirVoucherPenerimaanPiutangDetail()
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapVoucherPenerimaanPiutangDetailReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkoderec;
function mComboRekening()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboRekening = new WebApp.DataStore({fields: Field});
    dsUnitComboRekening.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboRekening',
                param: " MA_type = 'D'  and isdebit = 't' order by MA.ma_id"
            }
        }
    );
    var cboRekening = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboRekening',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:350,
                store: dsUnitComboRekening,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkoderec=b.data.displayText ;
                        }
                }
            }
    );
    return cboRekening;
};

function ComboDariCustomer_VoucherPenerimaanPiutangDetail()
{
	var Fields = ['kd_customer', 'customer', 'customer_name'];
    dsCboDariCustomerVoucherPenerimaanPiutangDetail = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getCustomer",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbDariCustomerVoucherPenerimaanPiutangDetail.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboDariCustomerVoucherPenerimaanPiutangDetail.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboDariCustomerVoucherPenerimaanPiutangDetail.add(recs);
			}
		}
	});
    
    var cbDariCustomerVoucherPenerimaanPiutangDetail = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboDariCustomerVoucherPenerimaanPiutangDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih customer ...',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'90%',
		    listWidth:350,
		    store: dsCboDariCustomerVoucherPenerimaanPiutangDetail,
		    valueField: 'kd_customer',
		    displayField: 'customer_name',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        // selectCboEntryCustARForm = b.data.kd_customer;
			    }
			}
		}
	);
	
	return cbDariCustomerVoucherPenerimaanPiutangDetail;
};

function ComboKeCustomerVoucherPenerimaanPiutangDetail()
{
	var Fields = ['kd_customer', 'customer', 'customer_name'];
    dsCboKeCustomer_VoucherPenerimaanPiutangDetail = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getCustomer",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbKeCustomer_VoucherPenerimaanPiutangDetail.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboKeCustomer_VoucherPenerimaanPiutangDetail.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboKeCustomer_VoucherPenerimaanPiutangDetail.add(recs);
			}
		}
	});
    
    var cbKeCustomer_VoucherPenerimaanPiutangDetail = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 40,
		    id: 'cboKeCustomer_VoucherPenerimaanPiutangDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih customer ...',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'90%',
		    listWidth:350,
		    store: dsCboKeCustomer_VoucherPenerimaanPiutangDetail,
		    valueField: 'kd_customer',
		    displayField: 'customer_name',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        // selectCboEntryCustARForm = b.data.kd_customer;
			    }
			}
		}
	);
	
	return cbKeCustomer_VoucherPenerimaanPiutangDetail;
};

function ComboNumberAwalVoucherPenerimaanPiutangDetail()
{
	var Fields = ['number'];
    dsCboNumberAwal_VoucherPenerimaanPiutangDetail = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPenerimaanPiutangDetail",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAwal_VoucherPenerimaanPiutangDetail.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAwal_VoucherPenerimaanPiutangDetail.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAwal_VoucherPenerimaanPiutangDetail.add(recs);
			}
		}
	});
    
    var cbNumberAwal_VoucherPenerimaanPiutangDetail = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 40,
		    id: 'cboNumberAwal_VoucherPenerimaanPiutangDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAwal_VoucherPenerimaanPiutangDetail,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        
			    }
			}
		}
	);
	
	return cbNumberAwal_VoucherPenerimaanPiutangDetail;
};

function ComboNumberAkhirVoucherPenerimaanPiutangDetail()
{
	var Fields = ['number'];
    dsCboNumberAkhir_VoucherPenerimaanPiutangDetail = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_voucher/getNumberPenerimaanPiutangDetail",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbNumberAkhir_VoucherPenerimaanPiutangDetail.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboNumberAkhir_VoucherPenerimaanPiutangDetail.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboNumberAkhir_VoucherPenerimaanPiutangDetail.add(recs);
			}
		}
	});
    
    var cbNumberAkhir_VoucherPenerimaanPiutangDetail = new Ext.form.ComboBox
	(
		{
			x: 285,
			y: 40,
		    id: 'cboNumberAkhir_VoucherPenerimaanPiutangDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    hidden: true,
		    mode: 'local',
		    emptyText: 'Pilih number ...',
		    fieldLabel: 'number ',
		    align: 'right',
		    width:150,
		    store: dsCboNumberAkhir_VoucherPenerimaanPiutangDetail,
		    valueField: 'number',
		    displayField: 'number',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			    }
			}
		}
	);
	
	return cbNumberAkhir_VoucherPenerimaanPiutangDetail;
};

function ComboOrderBYVoucherPenerimaanPiutangDetail(){
	var cbOrderBY_VoucherPenerimaanPiutangDetail = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboOrderBY_VoucherPenerimaanPiutangDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Oder by',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'60%',
			value:'Tanggal',
		    store:  new Ext.data.ArrayStore
			(
					{
						id: 0,
						fields:
						[
							'Id',
							'displayText'
						],
						data: [[1, 'Tanggal'],[2, 'Number']]
					}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        if(b.data.displayText == 'Tanggal'){
						Ext.getCmp('cboNumberAkhir_VoucherPenerimaanPiutangDetail').hide();
						Ext.getCmp('cboNumberAwal_VoucherPenerimaanPiutangDetail').hide();
						Ext.getCmp('dtpTglAwalVoucherPenerimaanPiutangDetail').show();
						Ext.getCmp('dtpTglAkhirVoucherPenerimaanPiutangDetail').show();
					} else{
						Ext.getCmp('cboNumberAkhir_VoucherPenerimaanPiutangDetail').show();
						Ext.getCmp('cboNumberAwal_VoucherPenerimaanPiutangDetail').show();
						Ext.getCmp('dtpTglAwalVoucherPenerimaanPiutangDetail').hide();
						Ext.getCmp('dtpTglAkhirVoucherPenerimaanPiutangDetail').hide();
					}
			    }
			}
		}
	);
	
	return cbOrderBY_VoucherPenerimaanPiutangDetail;
};