
var dsLapAdjusment_APDetail;
var selectNamaLapAdjusment_APDetail;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapAdjusment_APDetail;
var varLapAdjusment_APDetail= ShowFormLapAdjusment_APDetail();

function ShowFormLapAdjusment_APDetail()
{
    frmDlgLapAdjusment_APDetail= fnDlgLapAdjusment_APDetail();
    frmDlgLapAdjusment_APDetail.show();
};

function fnDlgLapAdjusment_APDetail()
{
    var winLapAdjusment_APDetailReport = new Ext.Window
    (
        {
            id: 'winLapAdjusment_APDetailReport',
            title: 'Laporan Adjustment AP Detail',
            closeAction: 'destroy',
            width: 230,
            height: 130,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapAdjusment_APDetail()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapAdjusment_APDetailReport;
};


function ItemDlgLapAdjusment_APDetail()
{
    var PnlLapAdjusment_APDetail = new Ext.Panel
    (
        {
            id: 'PnlLapAdjusment_APDetail',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapAdjusment_APDetail_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapAdjusment_APDetail',
                            handler: function()
                            {
                                loadMask.show();
								var params={
									tanggal:Ext.getCmp('dtpTglAwalFilterAdjusment_APDetail').getValue()
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/keuangan/cetaklaporanGeneralCashier/adj_ap_detail");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//frmDlgLapSTS.close();
								loadMask.hide();
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapAdjusment_APDetail',
                            handler: function()
                            {
                                    frmDlgLapAdjusment_APDetail.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapAdjusment_APDetail;
};

function GetCriteriaLapAdjusment_APDetail()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterAdjusment_APDetail').getValue();
	return strKriteria;
};

function getItemLapAdjusment_APDetail_Tanggal()
{
    var items = {
        layout: 'column',
        width: 200,
        border: true,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  200,
            height: 45,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode'
            }, 
			{
                x: 60,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			{
                x: 70,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterAdjusment_APDetail',
                format: 'M/Y',
                value: now
            }
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapAdjusment_APDetailReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WAPNING,
           width:300
        }
    );
};
