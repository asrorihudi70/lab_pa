
var dsLapRealisasiPemasukanbulanan;
var selectNamaLapRealisasiPemasukanbulanan;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRealisasiPemasukanbulanan;
var varLapRealisasiPemasukanbulanan= ShowFormLapRealisasiPemasukanbulanan();

function ShowFormLapRealisasiPemasukanbulanan()
{
    frmDlgLapRealisasiPemasukanbulanan= fnDlgLapRealisasiPemasukanbulanan();
    frmDlgLapRealisasiPemasukanbulanan.show();
};

function fnDlgLapRealisasiPemasukanbulanan()
{
    var winLapRealisasiPemasukanbulananReport = new Ext.Window
    (
        {
            id: 'winLapRealisasiPemasukanbulananReport',
            title: 'Laporan Realisasi Per Bulan',
            closeAction: 'destroy',
            width: 230,
            height: 130,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRealisasiPemasukanbulanan()],
            listeners:
			{
				activate: function()
				{
				   
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkLapRealisasiPemasukanbulanan',
					handler: function()
					{
						var criteria = GetCriteriaLapRealisasiPemasukanbulanan();
						
						loadlaporanAnggaran('0', 'LapRealisasiBulanan', criteria, function(){
							
						});
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRealisasiPemasukanbulanan',
					handler: function()
					{
							frmDlgLapRealisasiPemasukanbulanan.close();
					}
				}
			]

        }
    );

    return winLapRealisasiPemasukanbulananReport;
};


function ItemDlgLapRealisasiPemasukanbulanan()
{
    var PnlLapRealisasiPemasukanbulanan = new Ext.Panel
    (
        {
            id: 'PnlLapRealisasiPemasukanbulanan',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapRealisasiPemasukanbulanan_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        
                    ]
                }
            ]
        }
    );

    return PnlLapRealisasiPemasukanbulanan;
};

function GetCriteriaLapRealisasiPemasukanbulanan()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterRealisasiPemasukanbulanan').getValue();
	return strKriteria;
};

function getItemLapRealisasiPemasukanbulanan_Tanggal()
{
    var items = {
        layout: 'column',
        width: 200,
        border: true,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  200,
            height: 45,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Tanggal'
            }, 
			{
                x: 60,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			{
                x: 70,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterRealisasiPemasukanbulanan',
                format: 'M/Y',
                value: now
            }
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapRealisasiPemasukanbulananReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
