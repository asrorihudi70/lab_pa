
var dsLapRekapHutang;
var selectNamaLapRekapHutang;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRekapHutang;
var varLapRekapHutang= ShowFormLapRekapHutang();

function ShowFormLapRekapHutang()
{
    frmDlgLapRekapHutang= fnDlgLapRekapHutang();
    frmDlgLapRekapHutang.show();
};

function fnDlgLapRekapHutang()
{
    var winLapRekapHutangReport = new Ext.Window
    (
        {
            id: 'winLapRekapHutangReport',
            title: 'Lap. Rekap Hutang',
            closeAction: 'destroy',
            width: 490,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRekapHutang()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapRekapHutangReport;
};


function ItemDlgLapRekapHutang()
{
    var PnlLapRekapHutang = new Ext.Panel
    (
        {
            id: 'PnlLapRekapHutang',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapRekapHutang_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'0px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRekapHutang',
                            handler: function()
                            {
                                // if (ValidasiReportLapRekapHutang() === 1)
                                // {
                                        //var tmppilihan = getKodeReportLapRekapHutang();
                                        var criteria = GetCriteriaLapRekapHutang();
                                        // loadMask.show();
                                        // var criteria = '';
                                        loadlaporanAnggaran('0', 'LapRekapHutang', criteria, function(){
											// frmDlgLapRekapHutang.close();
											// loadMask.hide();
										});
                                // };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRekapHutang',
                            handler: function()
                            {
                                    frmDlgLapRekapHutang.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRekapHutang;
};

function GetCriteriaLapRekapHutang()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterRekapHutang').getValue();
	return strKriteria;
};

function getItemLapRekapHutang_Tanggal()
{
    var items = {
        layout: 'column',
        width: 500,
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  500,
            height: 130,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Vendor'
            }, 
			{
                x: 90,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            mComboDariAkunRekapHutang(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Tanggal'
            }, 
            {
                x: 90,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 100,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterRekapHutang',
                format: 'Y',
                value: now
            }
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapRekapHutangReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkodedariAkun;
var tmpkodekeAkun;
function mComboDariAkunRekapHutang()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboDariAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboDariAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'vendor',
                Sortdir: 'ASC',
                target: 'ViewListVendor',
                param: ""
            }
        }
    );
    var cboDariAkun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboDariAkun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:360,
                store: dsUnitComboDariAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodedariAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboDariAkun;
};