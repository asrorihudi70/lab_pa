
var dsLapBukuKasHarian;
var selectNamaLapBukuKasHarian;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapBukuKasHarian;
var varLapBukuKasHarian= ShowFormLapBukuKasHarian();

function ShowFormLapBukuKasHarian()
{
    frmDlgLapBukuKasHarian= fnDlgLapBukuKasHarian();
    frmDlgLapBukuKasHarian.show();
};

function fnDlgLapBukuKasHarian()
{
    var winLapBukuKasHarianReport = new Ext.Window
    (
        {
            id: 'winLapBukuKasHarianReport',
            title: 'Lap. Buku Kas Harian',
            closeAction: 'destroy',
            width: 480,
            height: 240,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapBukuKasHarian()],
            listeners:
			{
				activate: function()
				{
				   
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapBukuKasHarian',
					handler: function()
					{
						if(Ext.getCmp('cboDariCustomerBukuKasHarian').getValue() == ''){
							ShowPesanWarningLapBukuKasHarianDetailReport('Dari customer tidak boleh kosong!','WARNING');
						} else if(Ext.getCmp('cboKeCustomer_BukuKasHarian').getValue() == ''){
							ShowPesanWarningLapBukuKasHarianDetailReport('Ke customer tidak boleh kosong!','WARNING');
						} else{
							var params={
								dariAccount:Ext.getCmp('cboDariCustomerBukuKasHarian').getValue(),
								keAccount:Ext.getCmp('cboKeCustomer_BukuKasHarian').getValue(),
								tglAwal:Ext.getCmp('dtpTglAwalBukuKasHarian').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirBukuKasHarian').getValue(),
								orderby:Ext.get('cboOrderBY_BukuKasHarian').getValue(),
							} 

							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/keuangan/lap_keuangan/cetakBukuHarianKas");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();	
						}							
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapBukuKasHarian',
					handler: function()
					{
							frmDlgLapBukuKasHarian.close();
					}
				}
			]
        }
    );

    return winLapBukuKasHarianReport;
};


function ItemDlgLapBukuKasHarian()
{
    var PnlLapBukuKasHarian = new Ext.Panel
    (
        {
            id: 'PnlLapBukuKasHarian',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapBukuKasHarian_Customer(),
				{xtype: 'tbspacer',height: 10, width:5},
				getItemLapBukuKasHarian_Periode()
            ]
        }
    );

    return PnlLapBukuKasHarian;
};

function GetCriteriaLapBukuKasHarian()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterBukuKasHarian').getValue();
	return strKriteria;
};

function getItemLapBukuKasHarian_Customer()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Dari Account'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboDariAccount_BukuKasHarian(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Ke Account'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				ComboKeAccount_BukuKasHarian()
            ]
        }]
    };
    return items;
};

function getItemLapBukuKasHarian_Periode()
{
    var items = {
        layout: 'column',
        width: 455,
        border: true,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  500,
				height: 70,
				items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Order by'
				}, 
				{
					x: 90,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				ComboOrderBYBukuKasHarian(),
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Periode'
				}, 
				{
					x: 90,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				{
					x: 100,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAwalBukuKasHarian',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				// ComboNumberAwalBukuKasHarian(),
				{
					x: 260,
					y: 40,
					xtype: 'label',
					text: ' s/d '
				},
				{
					x: 285,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAkhirBukuKasHarian',
					format: 'd/M/Y',
					width:150,
					value: now
				},
				// ComboNumberAkhirBukuKasHarian()
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapBukuKasHarianReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkoderec;
function mComboRekening()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboRekening = new WebApp.DataStore({fields: Field});
    dsUnitComboRekening.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboRekening',
                param: " MA_type = 'D'  and isdebit = 't' order by MA.ma_id"
            }
        }
    );
    var cboRekening = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboRekening',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:350,
                store: dsUnitComboRekening,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkoderec=b.data.displayText ;
                        }
                }
            }
    );
    return cboRekening;
};

function ComboDariAccount_BukuKasHarian()
{
	var Fields = ['account',  'name_account'];
    dsCboDariAccount_BukuKasHarian = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_keuangan/getAccountKasBankActivity",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbDariAccountBukuKasHarian.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboDariAccount_BukuKasHarian.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboDariAccount_BukuKasHarian.add(recs);
			}
		}
	});
    
    var cbDariAccountBukuKasHarian = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboDariCustomerBukuKasHarian',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih account ...',
		    fieldLabel: 'Account ',
		    align: 'right',
		    anchor:'90%',
		    listWidth:350,
		    store: dsCboDariAccount_BukuKasHarian,
		    valueField: 'account',
		    displayField: 'name_account',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        // selectCboEntryCustARForm = b.data.kd_customer;
			    }
			}
		}
	);
	
	return cbDariAccountBukuKasHarian;
};

function ComboKeAccount_BukuKasHarian()
{
	var Fields = ['account', 'name_account'];
    dsCboKeAccount_BukuKasHarian = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/lap_keuangan/getAccountKasBankActivity",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbKeAccount_BukuKasHarian.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboKeAccount_BukuKasHarian.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboKeAccount_BukuKasHarian.add(recs);
			}
		}
	});
    
    var cbKeAccount_BukuKasHarian = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 40,
		    id: 'cboKeCustomer_BukuKasHarian',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih account ...',
		    fieldLabel: 'account ',
		    align: 'right',
		    anchor:'90%',
		    listWidth:350,
		    store: dsCboKeAccount_BukuKasHarian,
		    valueField: 'account',
		    displayField: 'name_account',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        // selectCboEntryCustARForm = b.data.kd_customer;
			    }
			}
		}
	);
	
	return cbKeAccount_BukuKasHarian;
};
function ComboOrderBYBukuKasHarian(){
	var cbOrderBY_BukuKasHarian = new Ext.form.ComboBox
	(
		{
			x: 100,
            y: 10,
		    id: 'cboOrderBY_BukuKasHarian',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Oder by',
		    fieldLabel: 'Order by ',
		    align: 'right',
		    anchor:'60%',
			value:'Tanggal',
		    store:  new Ext.data.ArrayStore
			(
					{
						id: 0,
						fields:
						[
							'Id',
							'displayText'
						],
						data: [[1, 'Tanggal'],[2, 'Referensi'],[3, 'Voucher']]
					}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        
			    }
			}
		}
	);
	
	return cbOrderBY_BukuKasHarian;
};