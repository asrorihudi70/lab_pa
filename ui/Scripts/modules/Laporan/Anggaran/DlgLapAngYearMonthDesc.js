var now = new Date();
var jenisTrans;
// var title_LapAngYearMonthDesc = varLapMainPage +' '+ CurrentPage.title;//
var title_LapAngYearMonthDesc = 'Laporan Anggaran & Realisasi';//
var frmDlg_LapAngYearMonthDesc= fnDlg_LapAngYearMonthDesc();


frmDlg_LapAngYearMonthDesc.show();
var StrThnAnggaran = gstrTahunAngg; //variable tahun anggaran dari SYS_SETTINGS

function fnDlg_LapAngYearMonthDesc()
{  	
    var win_LapAngYearMonthDescReport = new Ext.Window
	(
		{ 
			id: 'win_LapAngYearMonthDescReport',
			title: title_LapAngYearMonthDesc,//'Anggaran & Realisasi',
			closeAction: 'destroy',
			width:350,
			height: 170,//220, jika ada account, gunakan height ini
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlg_LapAngYearMonthDesc()]
			
		}
	);
	
    return win_LapAngYearMonthDescReport; 
};

function ItemDlg_LapAngYearMonthDesc() 
{	
/*Sementara di komentar*/
	/* if(strKdUser == gstrHakUnapp)
	{ */
		var PnlLap_LapAngYearMonthDesc = new Ext.Panel
		(
			{ 
				id: 'PnlLap_LapAngYearMonthDesc',
				fileUpload: true,
				layout: 'form',
				width:350,
				height: '100',
				anchor: '100%',
				bodyStyle: 'padding:15px',
				border: true,
				items: 
				[
					getItemLap_LapAngYearMonthDesc_Tanggal(),
					getUnit_LapAngYearMonthDesc(),
					{
						xtype: 'textfield',
						fieldLabel: 'No. Account',
						id: 'txtNoAcc_LapAngYearMonthDesc',
						// readOnly:true,
						hidden:true,
						width: 170					
					},
					getAccount_LapAngYearMonthDesc(),
					getAccAnak_LapAngYearMonthDesc(),
					getTrans_LapAngYearMonthDesc(),
					getOutput_LapAngYearMonthDesc(),
					{
						layout: 'hBox',
						border: false,
						defaults: { margins: '0 5 0 0' },
						style:{'margin-left':'35px','margin-top':'12px'},
						anchor: '94%',
						layoutConfig: 
						{
							padding: '3',
							pack: 'end',
							align: 'middle'
						},
						items: 
						[
							{
								xtype: 'button',
								text: 'Ok',
								width: 70,
								hideLabel: true,
								id: 'btnOkLap_LapAngYearMonthDesc',
								handler: function() 
								{
									if(ValidasiReport_LapAngYearMonthDesc() ===1)
									{
										/* var criteria = GetCriteria_LapAngYearMonthDesc();
										frmDlg_LapAngYearMonthDesc.close();									
										ShowReport('', '0206211', criteria); */
										
										var params={
											thn_anggaran	:Ext.getCmp('txtThnangg_LapAngYearMonthDesc').getValue(), 
											unit_kerja		:Ext.getCmp('comboUnitKerja_LapAngYearMonthDesc').getValue(),
											jenis_lap		:Ext.getCmp('cboOutput_LapAngYearMonthDesc').getValue(),
										} 
										var form = document.createElement("form");
										form.setAttribute("method", "post");
										form.setAttribute("target", "_blank");
										form.setAttribute("action", baseURL + "index.php/anggaran_module/functionLapAnggaranRealisasi/cetak");
										var hiddenField = document.createElement("input");
										hiddenField.setAttribute("type", "hidden");
										hiddenField.setAttribute("name", "data");
										hiddenField.setAttribute("value", Ext.encode(params));
										form.appendChild(hiddenField);
										document.body.appendChild(form);
										form.submit();	
									};
								}
							},
							{
								xtype: 'button',
								text: 'Cancel',
								width: 70,
								hideLabel: true,
								id: 'btnCancelLap_LapAngYearMonthDesc',
								handler: function() 
								{
									frmDlg_LapAngYearMonthDesc.close();
								}
							}
						],
						/*listeners: 
						{ 
							'afterrender': function() 
							{           
								Ext.getCmp('btnHitungLap_LapAngYearMonthDesc').disable();
							}
						},*/
					}
				]
			}
		);
	/* }
	else
	{
		var PnlLap_LapAngYearMonthDesc = new Ext.Panel
		(
			{ 
				id: 'PnlLap_LapAngYearMonthDesc',
				fileUpload: true,
				layout: 'form',
				width:350,
				height: '100',
				anchor: '100%',
				bodyStyle: 'padding:15px',
				border: true,
				items: 
				[
					getItemLap_LapAngYearMonthDesc_Tanggal(),
					getUnit_LapAngYearMonthDesc(),
					{
						xtype: 'textfield',
						fieldLabel: 'No. Account',
						id: 'txtNoAcc_LapAngYearMonthDesc',
						hidden:true,
						width: 170					
					},
					getAccount_LapAngYearMonthDesc(),
					getAccAnak_LapAngYearMonthDesc(),
					getTrans_LapAngYearMonthDesc(),
					getOutput_LapAngYearMonthDesc(),
					{
						layout: 'hBox',
						border: false,
						defaults: { margins: '0 5 0 0' },
						style:{'margin-left':'35px','margin-top':'12px'},
						anchor: '94%',
						layoutConfig: 
						{
							padding: '3',
							pack: 'end',
							align: 'middle'
						},
						items: 
						[
							{
								xtype: 'button',
								text: 'Hitung',
								width: 70,
								hideLabel: true,
								hidden: true,
								id: 'btnHitungLap_LapAngYearMonthDesc',
								handler: function() 
								{
									if (confirm('Proses Hitung Disarankan Diluar Jam Kerja. Proses Akan Tetap Dilanjutkan ?') == true) 
									{
									    showProgress_LapAngYearMonthDesc("Harap tunggu sedang diproses");
										Hitung_LapAngYearMonthDesc();
									};
								},							
							},
							{
								xtype: 'button',
								text: 'Ok',
								width: 70,
								hideLabel: true,
								id: 'btnOkLap_LapAngYearMonthDesc',
								handler: function() 
								{
									if(ValidasiReport_LapAngYearMonthDesc() ===1)
									{
										var criteria = GetCriteria_LapAngYearMonthDesc();
										frmDlg_LapAngYearMonthDesc.close();									
										ShowReport('', '0206211', criteria);
									};
								}
							},
							{
								xtype: 'button',
								text: 'Cancel',
								width: 70,
								hideLabel: true,
								id: 'btnCancelLap_LapAngYearMonthDesc',
								handler: function() 
								{
									frmDlg_LapAngYearMonthDesc.close();
								}
							}
						],
						
					}
				]
			}
		);
	} */
 
    return PnlLap_LapAngYearMonthDesc;
};

function getAccount_LapAngYearMonthDesc()
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    hidden:true,
	    items:
		[
		    {
		        columnWidth: 0.9,
		        layout: 'form',
		        border: false,
		        labelAlign: 'left',
		        labelWidth:100,
		        items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'Account',
						id: 'txtAcc_LapAngYearMonthDesc',
						readOnly: true,
						width: 170,//198,
						style: 'font-weight;text-align: right;'
					}
				]
		    },
			{
		        columnWidth: 0.1,
		        layout: 'form',
		        border: false,
		        labelAlign: 'left',
		        labelWidth:100,
		        items:
				[			
					{
						xtype: 'button',
						id: 'btnAcc_LapAngYearMonthDesc',						
						text: '...',
						handler: function()
						{ 	
							FormLookupRKATlapSP3Dwip("",Ext.getCmp('comboUnitKerja_LapAngYearMonthDesc').getValue());
						}
					}
				]
		    }
		]
	}
    return items;
};

function showProgress_LapAngYearMonthDesc(message)
{
	Ext.MessageBox.show
	({
		   msg: message,
		   progressText: 'Processing ... ',
		   width: 480,
		   wait: true,
		   waitConfig: {interval: 100},
		   // icon: 'refresh', 
		   animEl: 'buttonID'
    });
   
    setTimeout
	(
		function()
		{
			//This simulates a long-running operation like a database save or XHR call.
			//In real code, this would be in a callback function.
			Ext.MessageBox.hide();
			Ext.example.msg('Selesai', 'Data telah berhasil diproses.');
		}, 100000
	);
}

function Hitung_LapAngYearMonthDesc()
{
	Ext.Ajax.request
	(
		{
			url: "./Datapool.mvc/CreateDataObj",
			params:getACC_RKATKOR_HITUNGParam(),
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);							
				if (cst.success === true) 
				{
					ShowPesanWarning_LapAngYearMonthDescReport('Data berhasil dihitung','Hitung Data');				                      						
				}
				else
				{											
					ShowPesanWarning_LapAngYearMonthDescReport('Data gagal dihitung ','Hitung Data'); 
				}
			}
		}
	)
}

function getACC_RKATKOR_HITUNGParam() 
{
    // var tgl = new Date();
	var tgl = Ext.getCmp('txtThnangg_LapAngYearMonthDesc').getValue()
	var params = 
	{
		Table: 'viHitung_RKATKOR',   
		KD_UNIT_KERJA: Ext.getCmp("comboUnitKerja_LapAngYearMonthDesc").getValue(),
		TAHUN_SEKRANG: Ext.getCmp("txtThnangg_LapAngYearMonthDesc").getValue(), //tgl.getFullYear()
	};
	return params
};

function GetCriteria_LapAngYearMonthDesc()
{
	var strKriteria = "";	
	
	
	if(Ext.getCmp('txtThnangg_LapAngYearMonthDesc').getValue() !== undefined || fExt.getCmp('txtThnangg_LapAngYearMonthDesc').getValue() != "")
	{
	//	strKriteria += FormatDateReport(Ext.getCmp('txtThnangg_LapAngYearMonthDesc').getValue()) + "##";	
		strKriteria += Ext.getCmp('txtThnangg_LapAngYearMonthDesc').getValue() + "##";	
	}
	strKriteria += Ext.getCmp('cboTrans_LapAngYearMonthDesc').getValue() + "##";
	
	if(Ext.get('comboUnitKerja_LapAngYearMonthDesc').getValue() != "Semua Unit/Sub "+gstrSatker)
	{
		strKriteria +=  Ext.getCmp('comboUnitKerja_LapAngYearMonthDesc').getValue() + "##";	
	}else{
		strKriteria += "0"+ "##";	
	}
	
	strKriteria += strKdUser + "##";	

	
	//.......................................................................................
	strKriteria +=  Ext.getCmp('txtNoAcc_LapAngYearMonthDesc').getValue() + "##";		
	if(mPrioritas_LookupRKATlapSP3D == undefined)
	{
		strKriteria +=  "" + "##";
	}
	else
	{
		strKriteria +=  mPrioritas_LookupRKATlapSP3D + "##";
	}
	if(mUrut_LookupRKATlapSP3D == undefined)
	{
		strKriteria +=  "" + "##";
	}
	else
	{
		strKriteria +=  mUrut_LookupRKATlapSP3D + "##";
	}
	strKriteria +=  Ext.getCmp('cboAccAnak_LapAngYearMonthDesc').getValue() + "##";	
	//.......................................................................................

	strKriteria +=  Ext.getCmp('cboOutput_LapAngYearMonthDesc').getValue() + "##";		

	return strKriteria;
}

function ValidasiReport_LapAngYearMonthDesc()
{
	var x=1;
	
	// if (Ext.getCmp('chPengembangan_LapAngYearMonthDesc').getValue() == true && Ext.getCmp('chRutin_LapAngYearMonthDesc').getValue() == false){
		// jenisTrans = 1;
	// }else if(Ext.getCmp('chRutin_LapAngYearMonthDesc').getValue() == true && Ext.getCmp('chPengembangan_LapAngYearMonthDesc').getValue() == false) {
		// jenisTrans = 2;
	// }else if(Ext.getCmp('chOther_LapAngYearMonthDesc').getValue() == true){
		// jenisTrans = 3;
	// }else if(Ext.getCmp('chPengembangan_LapAngYearMonthDesc').getValue() == true && Ext.getCmp('chRutin_LapAngYearMonthDesc').getValue() == true){
		// jenisTrans = "1,2";
	// }else if(Ext.getCmp('chPengembangan_LapAngYearMonthDesc').getValue() == true && Ext.getCmp('chRutin_LapAngYearMonthDesc').getValue() == true && Ext.getCmp('chOther_LapAngYearMonthDesc').getValue() == true){
		// jenisTrans = "1,2,3";
	// }else{
		// jenisTrans = 0;
		// ShowPesanWarning_LapAngYearMonthDescReport('Jenis Trans. belum di isi atau belum benar','Lap. Realisasi Anggaran');
		// x=0;
	// }
	
	if((Ext.get('txtThnangg_LapAngYearMonthDesc').getValue() === '') 	)
	{
		if(Ext.get('txtThnangg_LapAngYearMonthDesc').getValue() === '')
		{
			ShowPesanWarning_LapAngYearMonthDescReport('Kriteria Periode belum di isi','Lap. Realisasi Anggaran'		);
			x=0;
		}
		
	};
	
	if(Ext.getCmp('cboTrans_LapAngYearMonthDesc').getValue() == '') 	
	{
		ShowPesanWarning_LapAngYearMonthDescReport('Kriteria Anggaran Belum di isi','Lap. Realisasi Anggaran'		);
		x=0;
	};
	
	
	

	return x;
};

function ShowPesanWarning_LapAngYearMonthDescReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:180
		}
	);
};

function getItemLap_LapAngYearMonthDesc_Tanggal() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 0.9,
		        layout: 'form',
		        border: false,
		        labelAlign: 'left',
		        labelWidth: 100,
		        items:
				[
					{
						xtype: 'numberfield',
						fieldLabel: 'Thn. Anggaran',
						id: 'txtThnangg_LapAngYearMonthDesc',
						// value:gstrTahunAngg,
						value:'2018',
						width:50
					}
				]
		    }
		]
	}
    return items;
};

function getUnit_LapAngYearMonthDesc()
{
	var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
	dsUnitKerja_LapAngYearMonthDesc = new WebApp.DataStore({ fields: Field });

	var comboUnitKerja_LapAngYearMonthDesc = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_LapAngYearMonthDesc',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Semua Unit Kerja',
			fieldLabel: 'Unit Kerja ',			
			align:'Right',
			width:198,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			store:dsUnitKerja_LapAngYearMonthDesc,
			listeners:  
			{
				'select': function(a,b,c)
				{
					
				} 
			}
		}
	);
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerja_LapAngYearMonthDesc.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerja_LapAngYearMonthDesc.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerja_LapAngYearMonthDesc.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				// ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});

    /* dsUnitKerja_LapAngYearMonthDesc.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'KD_UNIT_KERJA', 
				Sortdir: 'ASC', 
				target:'viCboUnitKerja',
				// param: "kdunit=" + gstrListUnitKerja
				param: ''
			} 
		}
	); */
	return comboUnitKerja_LapAngYearMonthDesc;
}

function getAccAnak_LapAngYearMonthDesc()
{
	var cboAccAnak_LapAngYearMonthDesc = new Ext.form.ComboBox
	(
		{
			id:'cboAccAnak_LapAngYearMonthDesc',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width:198,
			hidden:true,
			fieldLabel: 'Account Anak',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 1,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, "YA"], [2, "TIDAK"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:2,
			listeners:  
			{
			}
		}
	);
	return cboAccAnak_LapAngYearMonthDesc;
}

function getTrans_LapAngYearMonthDesc()
{
	var cboTrans_LapAngYearMonthDesc = new Ext.form.ComboBox
	(
		{
			id:'cboTrans_LapAngYearMonthDesc',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			//emptyText:'Pilih semua',
			width:198,
			hidden:true,
			fieldLabel: 'Kriteria Anggaran',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 1,
					fields: 
					[
						'Id',
						'displayText'
					],
					// data: [[1, "Year"], [2, "Month"]]
					data: [[1, "Year"]]
				}
			),
			valueField: 'Id',
			value:1,
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboTrans_LapAngYearMonthDesc;
}

function getOutput_LapAngYearMonthDesc()
{
	var cboOutput_LapAngYearMonthDesc = new Ext.form.ComboBox
	(
		{
			id:'cboOutput_LapAngYearMonthDesc',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width:198,
			fieldLabel: 'Output',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 1,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, "PDF"], [0, "EXCEL"]]
				}
			),
			valueField: 'Id',
			value:1,
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboOutput_LapAngYearMonthDesc;
}
// form lookup
var winFormLookupRKATlapSP3Dwip;
var dsGLLookupRKATlapSP3D;
var selectedrowGLLookupRKATSP3DGaji;
var mCancel=true;
 
var mkdaccount_LookupRKATlapSP3D;
var mNMaccount_LookupRKATlapSP3D;
var mNMdeskripsi_LookupRKATlapSP3D;
var mPrioritas_LookupRKATlapSP3D;
var mUrut_LookupRKATlapSP3D;

 
function FormLookupRKATlapSP3Dwip(criteria, unit)
{	
	winFormLookupRKATlapSP3Dwip = new Ext.Window
	(		
		{
		    id: 'winFormLookupRKATlapSP3Dwip',
		    title: 'Lookup RAPB',
		    closeAction: 'destroy',
		    closable: true,
		    width: 800,
		    height: 480,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'border',
		    iconCls: 'find',
		    modal: true,
			autoScroll: true,
		    items: [getItemFormLookupRKATlapSP3Dwip(criteria,unit)],
			listeners:
			{
				'beforeclose': function()
				{
					if (mCancel != true)
					{						
						// loadDetailRKATEntrySP3DFormGaji(getCriteriaDetailRKATEntrySP3DFormGaji());
					}
				},
				activate: function()
				{
					mCancel=true;										
				},
				'afterrender': function()				
				{
					RefreshDataLookupRKATUnit(getCriteriaLookupRKATUnit(unit));
				}
			}
		}
	);
	
	winFormLookupRKATlapSP3Dwip.show();	
}

function getItemFormLookupRKATlapSP3Dwip(criteria,unit)
{
	var pnlButtonLookupRKATUnit = new Ext.Panel(
		{
			id: 'pnlButtonLookupRKATUnit',
			layout: 'hbox',			
			border: false,
			region: 'south',
			defaults: { margins: '0 5 0 0' },
			style: { 'margin-left': '4px', 'margin-top': '3px' },
			anchor: '96.5%',
			layoutConfig:
			{
				padding: '3',
				pack: 'end',
				align: 'middle'
			},
			items:
			[
				{
					xtype: 'button',
					width: 70,
					text: 'Ok',
					handler: function()
					{
						if(selectedrowGLLookupRKATSP3DGaji !== undefined)
						{						
							mCancel=false;							

							mkdaccount_LookupRKATlapSP3D = selectedrowGLLookupRKATSP3DGaji.data.ACCOUNT;	
							mNMaccount_LookupRKATlapSP3D = selectedrowGLLookupRKATSP3DGaji.data.Name;
							mPrioritas_LookupRKATlapSP3D = selectedrowGLLookupRKATSP3DGaji.data.PRIORITAS;
							mUrut_LookupRKATlapSP3D = selectedrowGLLookupRKATSP3DGaji.data.LINE;	
							mNMdeskripsi_LookupRKATlapSP3D = selectedrowGLLookupRKATSP3DGaji.data.DESKRIPSI;
							
							
							Ext.getCmp('txtNoAcc_LapAngYearMonthDesc').setValue(mkdaccount_LookupRKATlapSP3D);
							Ext.getCmp('txtAcc_LapAngYearMonthDesc').setValue(mNMaccount_LookupRKATlapSP3D);
							winFormLookupRKATlapSP3Dwip.close();									
						}									
						else
						{
							Ext.Msg.alert('Lookup RAPB','RAPB belum dipilih.');
						}
					}
				},
				{
					xtype: 'button',
					width: 70,
					text: 'Cancel',
					handler: function()
					{
						selectedrowGLLookupRKATSP3DGaji = undefined;
						winFormLookupRKATlapSP3Dwip.close();
					}
				}
			]
		}
	);
	
	var frmListLookupRKATUnit = new Ext.Panel
	(
		{
			id: 'frmListLookupRKATUnit',
			layout: 'form',
			region: 'center',
			autoScroll: true,
			items: 
			[
				getGridListKUPPA(criteria),
				pnlButtonLookupRKATUnit
			],
			tbar:
			{
				xtype: 'toolbar',
				items: 
				[
					{ xtype: 'tbtext', text: 'RAPB : ', cls: 'left-label', width: 60 },
						new Ext.form.ComboBox(
						{
							id: 'cboRKATLookupRKATUnit',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender: true,
							mode: 'local',
							displayField: 'value',
							valueField: 'key',
							store: new Ext.data.ArrayStore
							(
								{
									id: 'dscboRKATLookupRKATUnit',
									fields:['key','value'],
									data:[
										['','Semua'],
										['Rutin','Rutin'],
										['Pengembangan','Pengembangan'],
										['Other Pengembangan','Other Pengembangan'],
										['Other Rutin','Other Rutin']
									]
								}
							)
						}
					),
					{
						xtype: 'spacer',
						width: 30
					},
					{ xtype: 'tbtext', text: 'No. Akun : ', cls: 'left-label', width: 60 },
					{
						xtype: 'textfield',
						id: 'numAkunLookupRKATUnit',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									RefreshDataLookupRKATUnit(getCriteriaLookupRKATUnit(unit));								
								} 						
							}
						}
					},
					{
						xtype: 'tbfill'
					},
					{
						xtype: 'button',
						id: 'btnRefreshGLLookupRKATUnit',
						iconCls: 'refresh',
						handler: function()
						{
							RefreshDataLookupRKATUnit(getCriteriaLookupRKATUnit(unit));
						}
					}
				]
			}
		}
	);
	
	return frmListLookupRKATUnit;
}

function getGridListKUPPA(criteria)
{
	var _fields = 
	[
		'TAHUN_ANGGARAN_TA', 'KD_UNIT_KERJA', 'KD_JNS_RKAT_JRKA', 'NO_PROGRAM_PROG',
		'PRIORITAS', 'KEGIATAN', 'RKAT', 'NAMA_PROGRAM_PROG','UNITKERJA', 
		'JUMLAH','ACCOUNT', 'DESKRIPSI','APPROVED','total','Name','LINE'
	];
	dsGLLookupRKATlapSP3D = new WebApp.DataStore({ fields: _fields });
	
	// RefreshDataLookupRKATUnit(criteria);
	
	var gridListLookupRKATUnit = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			id: 'gridListLookupRKATUnit',
			store: dsGLLookupRKATlapSP3D,			
			anchor: '100% 90%',
			columnLines:true,
			bodyStyle: 'padding:0px',
			autoScroll: true,
			border: false,
			viewConfig : 
			{
				forceFit: true
			},		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							selectedrowGLLookupRKATSP3DGaji = dsGLLookupRKATlapSP3D.getAt(row);
						}						
					}
				}
			),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),					
					{ 
						id: 'colRKATGLLookupRKATUnit',
						header: 'RAPB',
						dataIndex: 'RKAT',
						width: 80,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colNoAkunGLLookupRKATUnit',
						header: 'No. Akun',
						dataIndex: 'ACCOUNT',
						width: 50,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colNmAkunGLLookupRKATUnit',
						header: 'Nama Akun',
						dataIndex: 'Name',
						width: 100,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colNUraianProgGLLookupRKATUnit',
						header: 'Deskripsi',
						dataIndex: 'DESKRIPSI',
						width: 100,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					/* { 
						id: 'colKegGLLookupRKATUnit',
						header: 'Kegiatan',
						dataIndex: 'KEGIATAN',
						width: 130,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					}, */
					{ 
						id: 'colJumGLLookupRKATUnit',
						header: 'Jumlah (Rp.)',
						dataIndex: 'JUMLAH',
						width: 80,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px;text-align: right;'>" + formatCurrency(value) + "</div>";
							return str;
						}						
					}/* ,
					{ 
						id: 'colTotalGLLookupRKATUnit',
						header: 'Total Per Kegiatan(Rp.)',
						dataIndex: 'total',
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px;text-align: right;'>" + formatCurrency(value) + "</div>";
							return str;
						}						
					} */
				]
			)
		}
	);
		
	return gridListLookupRKATUnit;
}

function RefreshDataLookupRKATUnit(criteria)
{	
	
	dsGLLookupRKATlapSP3D.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: '',
			    target: 'viewLookupRKAT_All',
				//target:'viLookupRKAT',
			    // param: criteria + " &rkat=Other Rutin "
				param: criteria 
			}
		}
	);
	
	return dsGLLookupRKATlapSP3D;
}

function getCriteriaLookupRKATUnit(unit)
{
	var strKriteria = "";
	if(Ext.getCmp('cboRKATLookupRKATUnit').getValue() !== "")
	{
		strKriteria += "rkat=" + Ext.getCmp('cboRKATLookupRKATUnit').getValue();
	}
	if(Ext.getCmp('numAkunLookupRKATUnit').getValue() !== "")
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "akun=" + Ext.getCmp('numAkunLookupRKATUnit').getValue();
	}

	if (unit != '')
	{	
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "unit=" + unit;
	}
	
	if(strKriteria != "")
	{
		strKriteria += "&";
	}
	strKriteria += "tahun=" + Ext.getCmp('txtThnangg_LapAngYearMonthDesc').getValue();//((Ext.getCmp('txtThnangg_LapAngYearMonthDesc').getValue()).getYear() < 1900 ? 1900 + (Ext.getCmp('txtThnangg_LapAngYearMonthDesc').getValue()).getYear() : (Ext.getCmp('txtThnangg_LapAngYearMonthDesc').getValue()).getYear());
	
	//strKriteria += " AND x.RKAT like '%Rutin' ";
	return strKriteria;
}