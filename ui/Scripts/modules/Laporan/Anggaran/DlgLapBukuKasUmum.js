﻿var dsUnitKerja_LapBKU;
var selectUnitKerja_LapBKU;
var now_LapBKU = new Date();
var now = new Date();
// var winTitle_LapBKU = varLapMainPage +' '+ CurrentPage.title;
var winTitle_LapBKU = 'Laporan Buku Kas Umum';
// var winTitle_LapBKU = 'Plafond Anggaran';
var winDlg_LapBKU= fnDlg_LapBKU();
winDlg_LapBKU.show();

function fnDlg_LapBKU()
{  	
    var winDlg_LapBKU = new Ext.Window
	(
		{ 
			id: 'winDlg_LapBKU',
			title: winTitle_LapBKU,
			closeAction: 'destroy',
			width:360,
			height: 250,
			resizable:false,
			border: false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [
				getItemDlg_LapBKU()
			]
		}
	);
	
    return winDlg_LapBKU; 
};


function getItemDlg_LapBKU() 
{	
	var Pnl_LapBKU = new Ext.Panel
	(
		{ 
			id: 'Pnl_LapBKU',
			fileUpload: true,
			layout: 'anchor',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 330,
							height: 150,
							items: 
							[
								mComboAccount(),
								mComboTahun_LapBKU(),
								mcomboBulan_LapBKU(),
								{
									xtype: 'checkbox',
									id: 'chkLapBKU',
									fieldLabel: 'Posting',
								},{
									xtype: 'datefield',
									id: 'dtpTglCetak',
									format: 'd/M/Y',
									readOnly: false,
									width: 120,
									fieldLabel: 'Tgl.Cetak',
									value: now.format('d/M/Y')
									
								}
								// mcomboUnitKerja_LapBKU(),
								//mComboTrans_LapBKU()
							]
						}
					]
				},				
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'rigth'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOk_LapBKU',
							handler: function() 
							{
								if(Validasi_LapBKU() == 1)
								{
									var params={
										tahun_anggaran	: Ext.getCmp('cboTahun_LapBKU').getValue(),
										sd_bulan		: Ext.getCmp('comboBulan_LapBKU').getValue(),
										nama_bulan		: Ext.get('comboBulan_LapBKU').getValue(),
										approve			: Ext.getCmp('chkLapBKU').getValue(),
										tgl_cetak		: Ext.getCmp('dtpTglCetak').getValue(),
										account		: Ext.getCmp('cboAccount').getValue(),
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/keuangan/functionLapBKU/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();		
									winDlg_LapBKU.close();
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancel_LapBKU',
							handler: function() 
							{
								winDlg_LapBKU.close();
							}
						}
					]
				}
			]
		}
	);
 
    return Pnl_LapBKU;
};

function ShowPesanWarningRKAReport_LapBKU(str,modul)
{
	Ext.MessageBox.show
	(		
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function mcomboBulan_LapBKU()
{
	/* var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
	dsUnitKerja_LapBKU = new WebApp.DataStore({ fields: Field });
 */
  var comboBulan_LapBKU = new Ext.form.ComboBox
	(
		{
			id:'comboBulan_LapBKU',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Bulan ',			
			align:'Right',
			anchor:'100%',
			valueField: 'Id',
			displayField: 'displayText',
			store:new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						['01','JANUARI'], 
						['02','FEBRUARI'], 
						['03','MARET'], 
						['04','APRIL'], 
						['05','MEI'], 
						['06','JUNI'], 
						['07','JULI'], 
						['08','AGUSTUS'], 
						['09','SEPTEMBER'], 
						['10','OKTOBER'], 
						['11','NOVEMBER'], 
						['12','DESEMBER']
					]
				}
			),
			listeners:  
			{
				'select': function(a,b,c)
				{   
					
				} 
			}
		}
	);

   
	return comboBulan_LapBKU;
};

function mComboAccount(){
	var Fields = ['ACCOUNT', 'DESKRIPSI', 'LEVEL', 'NAME'];
	var dsCboAccount 	= new WebApp.DataStore({ fields: Fields });
    dsCboAccount.load({
	    params:{
		    Skip	: 0,
		    Take	: 1000,
            Sort	: '',
		    Sortdir	: 'ASC',
		    target	: 'ViewListAccountInt',
            param	: "int_code in ('CB', 'CH')"
		}
	});
  	var cboAccount 		= new Ext.form.ComboBox({
		id 				:'cboAccount',
		typeAhead 		: true,
		triggerAction 	: 'all',
		lazyRender	  	: true,
		mode 			: 'local',
		emptyText 		:'',
		fieldLabel 		: 'Account',			
		// width 			: '100%',
		anchor:'100%',
		store			: dsCboAccount,
		valueField 		: 'ACCOUNT',
		displayField 	: 'DESKRIPSI',
	});
	return cboAccount;
}

function mComboTahun_LapBKU()
{
  // var currYear = parseInt(gstrTahunAngg);//parseInt(now_LapBKU.format('Y'));
	var currYear = parseInt(now.format('Y'));
	var Fields = ['tahun_anggaran_ta'];
	var dsCboTahunAnggaran = new WebApp.DataStore({ fields: Fields });
    
  Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionLapBKU/getTahunAnggaran",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboTahunAnggaran.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboTahunAnggaran.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboTahunAnggaran.add(recs);
			} else {
			};
		}
	});
  var cboTahun_LapBKU = new Ext.form.ComboBox
	(
		{
			id:'cboTahun_LapBKU',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Thn. Anggaran ',			
			width:100,
			store: dsCboTahunAnggaran,
			valueField: 'tahun_anggaran_ta',
			displayField: 'tahun_anggaran_ta',
			// value: currYear +'/'+(Ext.num(gstrTahunAngg)+1)//now_LapRKARutin.format('Y')			
			value:Ext.num(now.format('Y'))
		}
	);
	return cboTahun_LapBKU;
};

function mComboTrans_LapBKU()
{
  var cboTrans_LapBKU = new Ext.form.ComboBox
	(
		{
			id:'cboTrans_LapBKU',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih semua',
			fieldLabel: 'Jenis Anggaran ',			
			width:198,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, "OPERASIONAL"], [2, "KEMAHASISWAAN"], [3,'BANTUAN']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText'			
		}
	);
	
	return cboTrans_LapBKU;
};

function getItemKelompokRKA_LapBKU()
{
	var cboKelRKA_LapBKU = new Ext.form.ComboBox
	(
		{
			id:'optPN_LapBKU',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Jenis ',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[2, "Pengembangan"], [1, "Rutin"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboKelRKA_LapBKU;
}

function getKriteria_LapBKU()
{
	var strKriteria = "";	
	
	// if(Ext.getCmp('comboUnitKerja_LapBKU').getValue() !== undefined || Ext.getCmp('comboUnitKerja_LapBKU').getValue() != "")
	// {
	// 	if(strKriteria == "")
	// 	{
	// 		strKriteria = " Where PLA.KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerja_LapBKU').getValue() + "'";
	// 	}
	// 	else
	// 	{	
	// 		if(Ext.getCmp('comboUnitKerja_LapBKU').getValue() != "")
	// 		{
	// 			strKriteria += " and PLA.KD_UNIT_KERJA='" + Ext.getCmp('comboUnitKerja_LapBKU').getValue() + "'";
	// 		}else{
	// 			strKriteria += " ";
	// 		}
	// 	}
	// }else
	// {
		
	// }
	// if(Ext.getCmp('cboTahun_LapBKU').getValue() !== undefined || Ext.getCmp('cboTahun_LapBKU').getValue() != "")
	// {		
	// 	strKriteria += " Where PLA.TAHUN_ANGGARAN_TA ='" + Ext.getCmp('cboTahun_LapBKU').getValue() + "'";	
	// }else
	// {
	// 	strKriteria +=''+"##";
	// }
	
	// if(Ext.get('cboTrans_LapBKU').getValue() != "Pilih semua")
	// {
	// 	strKriteria += " and PLA.KD_JNS_PLAFOND ='" + Ext.getCmp('cboTrans_LapBKU').getValue() + "'";
	// }
	strKriteria += Ext.getCmp('comboUnitKerja_LapBKU').getValue()+' '+"##";
	strKriteria += Ext.getCmp('cboTahun_LapBKU').getValue() +' '+"##";		
	strKriteria += ""+"##";	//strKriteria += Ext.getCmp('cboTrans_LapBKU').getValue() +' '+"##";		
	strKriteria += " "+ 1 + "##"
	
	return strKriteria;
}

function Validasi_LapBKU()
{
	var x = 1;
	
	if(Ext.getCmp('cboTahun_LapBKU').getValue() == '')
	{
		ShowPesanWarning_LapBKU('Pilihan Tahun Anggaran salah!','Laporan Rekap RAPB ');
		x = 0;
	}
	
	/*if(Ext.getCmp('cboTrans_LapBKU').getValue() == '')
	{
		ShowPesanWarning_LapBKU('Pilihan Transaksi Anggaran salah!','Laporan Rekap RAPB ');
		x = 0;
	}
	/* if(Ext.getCmp('comboUnitKerja_LapBKU').getValue() == '' )
	{
		ShowPesanWarning_LapBKU('Pilihan Unit/Sub '+gstrSatker+' salah!','Laporan RAPB Pengembangan');
		x = 0;
	} 
	if(Ext.getCmp('optPN_LapBKU').getValue() == '' )
	{
		ShowPesanWarning_LapBKU('Pilihan Jenis RAPB salah!','Laporan Rekap RAPB ');
		x = 0;
	}*/

	return x;
};

function ShowPesanWarning_LapBKU(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};