var now = new Date();
var frmDlg_LapFaktur= fnDlg_LapFaktur();
frmDlg_LapFaktur.show();
var check_posting='t';
var selectjenisLap_LapFaktur;
function fnDlg_LapFaktur()
{  	
    var win_LapFaktur = new Ext.Window
	(
		{ 
			id: 'win_LapFaktur',
			// title: 'Lap. '+gstrSp3d,
			title: 'Faktur AR/AP',
			closeAction: 'destroy',
			width:425,
			height: 200,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlg_LapFaktur()],
			listeners:
		    {
		      afterrender: function()
		      {
		       // Ext.getCmp('rgperiodeTglFaktur_LapFaktur').setValue(true);
		      }
		    }
			
		}
	);
	
    return win_LapFaktur; 
};

function ItemDlg_LapFaktur() 
{	
	var PnlLap_LapFaktur = new Ext.Panel
	(
		{ 
			id: 'PnlLap_LapFaktur',
			fileUpload: true,
			layout: 'anchor',
			// width:350,
			height: 120,
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[

				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 400,
							height: 130, 
							items: 
							[
								mComboPilihan_LapFaktur(),
								getItemLap_LapFaktur_Tanggal(),
								mComboJenisLap_LapFaktur(),
								{
									xtype: 'checkbox',
									id: 'Chkapprove_LapFaktur',
									fieldLabel: 'Approve Only' ,
									checked: true,
									handler: function()
									{
										if (this.getValue()===true)
										{
											check_posting='t';
										}else
										{
											check_posting='f';

										}
								    }
								},				
								{
									xtype: 'compositefield',
									items:
									[
										mComboOrder_LapFaktur(),
										{
												xtype: 'displayfield',
												flex: 1,
												width: 20,
												name: '',
												value: 'Asc',
												fieldLabel: '',
												id: 'lblAScDesc_LapFaktur',
												name: 'lblAScDesc_LapFaktur'
										},
										{
											xtype: 'checkbox',
											id: 'ChkAscDesc_LapFaktur',
											// fieldLabel: 'Asc ' ,
											checked: true,
											handler: function()
											{
												if (this.getValue()===true)
												{
												}else
												{

												}
										    }
										}
									]
								}
								
							]
						}						
					]
				},
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLap_LapFaktur',
							handler: function() 
							{
								if(ValidasiReport_LapFaktur() ===1)
								{
									var url_tmp='';

									if(Ext.getCmp('cboPilihanLap_LapFaktur').getValue() == 1)
									{
										url_tmp= baseURL + "index.php/anggaran/functionLapFaktur/lap_AR";
									}else
									{
										url_tmp= baseURL + "index.php/anggaran/functionLapFaktur/lap_AP";
									}
									
									var params={
										tgl_awal		:Ext.getCmp('dtpTglAwalLap_LapFaktur').getValue(),
										tgl_akhir		:Ext.getCmp('dtpTglAkhirLap_LapFaktur').getValue(),
										posting			:check_posting,
										sort			:Ext.getCmp('cboOrderLap_LapFaktur').getValue(),
										asc				:Ext.getCmp('ChkAscDesc_LapFaktur').getValue(),
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action",url_tmp);
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									frmDlg_LapFaktur.close();
								};
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLap_LapFaktur',
							handler: function() 
							{
								frmDlg_LapFaktur.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLap_LapFaktur;
};

function GetCriteria_LapFaktur()
{
	var strKriteria = "";	
	
	if(Ext.getCmp('cboPilihanLap_LapFaktur').getValue() !== undefined || Ext.getCmp('cboPilihanLap_LapFaktur').getValue() != "")
	{
		strKriteria += Ext.getCmp('cboPilihanLap_LapFaktur').getValue() + "##";
	}//1

/*	if(Ext.getCmp('rgperiodeTglFaktur_LapFaktur').getValue() !== undefined || Ext.getCmp('rgperiodeTglFaktur_LapFaktur').getValue() != "")
	{
		strKriteria += Ext.getCmp('rgperiodeTglFaktur_LapFaktur').getValue() + "##";
	}//2*/
	

	if(Ext.getCmp('dtpTglAwalLap_LapFaktur').getValue() !== undefined || Ext.getCmp('dtpTglAwalLap_LapFaktur').getValue() != "")
	{
		strKriteria += ShowDate(Ext.getCmp('dtpTglAwalLap_LapFaktur').getValue()) + "##";
	}
	if(Ext.getCmp('dtpTglAkhirLap_LapFaktur').getValue() !== undefined || Ext.getCmp('dtpTglAkhirLap_LapFaktur').getValue() != "")
	{
		strKriteria += ShowDate(Ext.getCmp('dtpTglAkhirLap_LapFaktur').getValue()) + "##";
	}//3

	if(Ext.getCmp('cbojenisLap_LapFaktur').getValue() != "")
	{
		strKriteria +=  Ext.getCmp('cbojenisLap_LapFaktur').getValue()  + "##";
	}else{
		strKriteria += "1"  + "##";
	}//4

	strKriteria +=Ext.get('Chkapprove_LapFaktur').dom.checked + "##";	//5
	strKriteria += strKdUser + "##"//6

	if(Ext.getCmp('cboOrderLap_LapFaktur').getValue() !== undefined || Ext.getCmp('cboOrderLap_LapFaktur').getValue() != "")
	{
		strKriteria += Ext.getCmp('cboOrderLap_LapFaktur').getValue() + "##";
	}else
	{
		strKriteria += 3 + "##";
	}//7
	strKriteria +=Ext.get('ChkAscDesc_LapFaktur').dom.checked + "##";	//8
	return strKriteria;
}

function ValidasiReport_LapFaktur()
{
	var x=1;
	
	if(Ext.getCmp('cboPilihanLap_LapFaktur').getValue() == undefined || Ext.getCmp('cboPilihanLap_LapFaktur').getValue() == "")
	{
		ShowPesanWarning_LapFaktur('Pilihan Laporan belum dipilih','Open AR/AP');
		x=0;		
	};

	if((Ext.get('dtpTglAwalLap_LapFaktur').getValue() === ''))
	{
		if(Ext.get('dtpTglAwalLap_LapFaktur').getValue() === '')
		{
			ShowPesanWarning_LapFaktur('Kriteria tanggal awal belum di isi','Open AR/AP');
			x=0;
		}
		
	};

	return x;
};

function ShowPesanWarning_LapFaktur(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:180
		}
	);
};

function getItemLap_LapFaktur_Tanggal() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		/*{
			xtype: 'compositefield',
			fieldLabel: 'Periode',
			anchor: '100%',
			labelWidth: 100,
			items: 
			[

				{
					xtype: 'radiogroup',
					fieldLabel:'Periode',
					id: 'rgperiodepilih_LapFaktur',
					columns: 2,
					anchor:'99%',
					items:
					[
						{
							boxLabel: 'Tanggal Faktur',
							autoWidth: true,
							inputValue: 0,
							name: 'rgperiodepilih_LapFaktur',
							id: 'rgperiodeTglFaktur_LapFaktur'
						},
						{											
							boxLabel: 'Tanggal Jatuh Tempo',
							autoWidth: true,
							inputValue: 1,
							name: 'rgperiodepilih_LapFaktur',
							id: 'rgperiodeTglDueDate_LapFaktur'
						}
					
					],
					listeners:
					{
						'change': function(rg, rc)
						{
							
						}
					}
				},

			]
		},	
*/

		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Periode ',
							id: 'dtpTglAwalLap_LapFaktur',
							format: 'd/M/Y',
							value:now,
							width:105
						}
					]
		    	}
			 ,
			 {
		         columnWidth: 0.47,
		         layout: 'form',
		         border: false,
				 labelWidth: 30,
		         labelAlign: 'right',
		         items:
				 [
					 {
						 xtype: 'datefield',
						 fieldLabel: 's/d ',
						 id: 'dtpTglAkhirLap_LapFaktur',
						 format: 'd/M/Y',
						 value:now,
						 width:105
					 }
				 ]
		     }
			]
		}
		   
		]
	}
    return items;
};
function mComboJenisLap_LapFaktur()
{
	var cbojenisLap_LapFaktur = new Ext.form.ComboBox
	(
		{
			id:'cbojenisLap_LapFaktur',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			hidden:true,
			//emptyText:'Pilih semua',
			readOnly:true,
			width:198,
			fieldLabel: 'Jenis Laporan ',		
			value: '1',
			store: new Ext.data.ArrayStore
			(
				{
					id: 1,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, "Rekap"], [2, "Detail"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				
			}
		}
	);
	return cbojenisLap_LapFaktur;
}

function mComboPilihan_LapFaktur()
{
	var cboPilihanLap_LapFaktur = new Ext.form.ComboBox
	(
		{
			id:'cboPilihanLap_LapFaktur',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Laporan',
			fieldLabel: 'Laporan ',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[
						[1, "Faktur AR"], 
						[2, "Faktur AP"]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					//selectAktivaLancar_LapDailyCash=b.data.Account ;
					//alert(Ext.getCmp('cboPilihanLap_LapFaktur').getValue());
				} 
			}
		}
	);
	return cboPilihanLap_LapFaktur;
}

function mComboOrder_LapFaktur()
{
	var cboOrderLap_LapFaktur = new Ext.form.ComboBox
	(
		{
			id:'cboOrderLap_LapFaktur',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Urut Berdasarkan ',
			fieldLabel: 'Urut Berdasarkan',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[
						[1, "Vendor/Customer"], 
						[2, "Faktur"],
						[3, "Tanggal"],
						[4, "Jumlah"]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					//selectAktivaLancar_LapDailyCash=b.data.Account ;
					//alert(Ext.getCmp('cboOrderLap_LapFaktur').getValue());
				} 
			}
		}
	);
	return cboOrderLap_LapFaktur;
}