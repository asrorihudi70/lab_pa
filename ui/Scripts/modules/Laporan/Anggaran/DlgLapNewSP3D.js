var now = new Date();
var jenisTrans;
// var title_NewSP3D = varLapMainPage +' '+ CurrentPage.title; //varLapMainPage +' '+ CurrentPage.title;//
var title_NewSP3D = 'Laporan Pengajuan Pencairan Dana';
var frmDlg_NewSP3D= fnDlg_NewSP3D();

frmDlg_NewSP3D.show();

function fnDlg_NewSP3D()
{  	
    var win_NewSP3DReport = new Ext.Window
	(
		{ 
			id: 'win_NewSP3DReport',
			title: title_NewSP3D, //'Pengajuan Pencairan Dana',
			closeAction: 'destroy',
			width:380,
			height: 150,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlg_NewSP3D()]
			
		}
	);
	
    return win_NewSP3DReport; 
};

function ItemDlg_NewSP3D() 
{	
	var PnlLap_NewSP3D = new Ext.Panel
	(
		{ 
			id: 'PnlLap_NewSP3D',
			fileUpload: true,
			layout: 'form',
			width:350,
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[
				getItemLap_NewSP3D_Tanggal(),
				getJenis_NewSP3D()
				,
				
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'22px','margin-top':'12px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLap_NewSP3D',
							handler: function() 
							{
								if(ValidasiReport_NewSP3D() ===1)
								{
									/* var criteria = GetCriteria_NewSP3D();
								
									ShowReport('', '011210', criteria); */
									var params={
										tgl_awal	:Ext.getCmp('dtpTglAwalLap_NewSP3D').getValue(),
										tgl_akhir	:Ext.getCmp('dtpTglAkhirLap_NewSP3D').getValue(),
										unit_kerja	:Ext.getCmp('comboUnitKerja_NewSP3D').getValue(),
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/anggaran_module/functionLapPPD/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();		
									frmDlg_NewSP3D.close();
								};
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLap_NewSP3D',
							handler: function() 
							{
								frmDlg_NewSP3D.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLap_NewSP3D;
};

function GetCriteria_NewSP3D()
{
	var strKriteria = "";	
	
	//if(Ext.getCmp('chDetail_NewSP3D').getValue() == true){
		if(Ext.getCmp('dtpTglAwalLap_NewSP3D').getValue() !== undefined || Ext.getCmp('dtpTglAwalLap_NewSP3D').getValue() != "")
		{
			//strKriteria += FormatDateReport(Ext.getCmp('dtpTglAwalLap_NewSP3D').getValue()) + "##";	
			strKriteria += ShowDate(Ext.getCmp('dtpTglAwalLap_NewSP3D').getValue()) + "##";	
			//alert(FormatDateReport(Ext.getCmp('dtpTglAwalLap_NewSP3D').getValue()) +'  - '+ShowDate(Ext.getCmp('dtpTglAwalLap_NewSP3D').getValue()))
		
		}
		if(Ext.getCmp('dtpTglAkhirLap_NewSP3D').getValue() !== undefined || Ext.getCmp('dtpTglAkhirLap_NewSP3D').getValue() != "")
		{
			//strKriteria += FormatDateReport(Ext.getCmp('dtpTglAkhirLap_NewSP3D').getValue()) + "##";	
			strKriteria += ShowDate(Ext.getCmp('dtpTglAkhirLap_NewSP3D').getValue()) + "##";	
		
		}
		
		
	if(Ext.get('comboUnitKerja_NewSP3D').getValue() != "Semua Unit/Sub "+gstrSatker)
		{
			strKriteria +=  Ext.getCmp('comboUnitKerja_NewSP3D').getValue()  + "##";
		}else{
			strKriteria += "0"  + "##";
		}
	
	strKriteria += strKdUser
	
	// if(Ext.getCmp('chDetail_NewSP3D').getValue() == true)
	// {
		// strKriteria += "1" + "##";	
	// }else{
		// strKriteria += "0" + "##";
	// }
	return strKriteria;
}

function ValidasiReport_NewSP3D()
{
	var x=1;
	
	
	if((Ext.get('dtpTglAwalLap_NewSP3D').getValue() === '') 	)
	{
		if(Ext.get('dtpTglAwalLap_NewSP3D').getValue() === '')
		{
			ShowPesanWarning_NewSP3DReport('Kriteria tanggal awal belum di isi','Lap. Realisasi Anggaran'		);
			x=0;
		}
		
	};

	return x;
};

function ShowPesanWarning_NewSP3DReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:180
		}
	);
};

function getItemLap_NewSP3D_Tanggal() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Periode ',
							id: 'dtpTglAwalLap_NewSP3D',
							format: 'd/M/Y',
							value:now,
							width:105
						}
					]
		    }
			 ,
			 {
		         columnWidth: 0.47,
		         layout: 'form',
		         border: false,
				 labelWidth: 30,
		         labelAlign: 'right',
		         items:
				 [
					 {
						 xtype: 'datefield',
						 fieldLabel: 's/d ',
						 id: 'dtpTglAkhirLap_NewSP3D',
						 format: 'd/M/Y',
						 value:now,
						 width:105
					 }
				 ]
		     }
					]
				}
		   
		]
	}
    return items;
};

function getJenis_NewSP3D()
{
	var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
	dsUnitKerja_NewSP3D = new WebApp.DataStore({ fields: Field });

	var comboUnitKerja_NewSP3D = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_NewSP3D',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			// emptyText:'Semua Unit/Sub '+gstrSatker,
			emptyText:'Semua Unit Kerja',
			fieldLabel: 'Unit Kerja ',			
			align:'center',
			anchor: '100%',
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			store:dsUnitKerja_NewSP3D,
			listeners:  
			{
				'select': function(a,b,c)
				{
					
				} 
			}
		}
	);

     Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerja_NewSP3D.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerja_NewSP3D.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerja_NewSP3D.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				// ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	return comboUnitKerja_NewSP3D;
}

function getTrans_NewSP3D()
{
	var cboTrans_NewSP3D = new Ext.form.ComboBox
	(
		{
			id:'cboTrans_NewSP3D',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			//emptyText:'Pilih semua',
			width:198,
			fieldLabel: 'Kriteria Anggaran',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 1,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, "Year"], [2, "Month"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboTrans_NewSP3D;
}