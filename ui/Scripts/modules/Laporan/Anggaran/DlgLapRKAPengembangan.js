﻿var dsUnitKerjaLapRKA_LapRKAPengembangan;
var selectUnitKerjaLapRKA_LapRKAPengembangan;
var now_LapRKAPengembangan = new Date();
//var radJenisRKA_LapRKAPengembangan = 1;
//var radKelompokRKA_LapRKAPengembangan = 'PL';
// var winTitleLapRKA_LapRKAPengembangan =  varLapMainPage +' '+ CurrentPage.title;//'RAPB Non Rutin';
var winTitleLapRKA_LapRKAPengembangan = 'Laporan RAB Non Rutin';//'RAPB Non Rutin';

var winDlgLapRKA_LapRKAPengembangan= fnDlgLapRKA_LapRKAPengembangan();
winDlgLapRKA_LapRKAPengembangan.show();

function fnDlgLapRKA_LapRKAPengembangan()
{  	
    var winDlgLapRKA_LapRKAPengembangan = new Ext.Window
	(
		{ 
			id: 'winDlgLapRKA_LapRKAPengembangan',
			title: winTitleLapRKA_LapRKAPengembangan, //+ radKelompokRKA_LapRKAPengembangan + '-' + radJenisRKA_LapRKAPengembangan.toString() + ')',
			closeAction: 'destroy',
			width:500,
			height: 180,
			resizable:false,
			border: false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [getItemDlgLapRKA_LapRKAPengembangan()]
			
		}
	);
	
    return winDlgLapRKA_LapRKAPengembangan; 
};


function getItemDlgLapRKA_LapRKAPengembangan() 
{	
	var PnlLapRKA_LapRKAPengembangan = new Ext.Panel
	(
		{ 
			id: 'PnlLapRKA_LapRKAPengembangan',
			fileUpload: true,
			layout: 'anchor',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 472,
							height: 105,
							items: 
							[
								mComboTahunLapRKA_LapRKAPengembangan(),
								mcomboUnitKerjaLapRKA_LapRKAPengembangan(),
								getItemKelompokRKA_LapRKAPengembangan()
								//getItemLapRKA_Tahun(),
								//getItemLapRKA_UnitKerja(),
								// getItemJenisRKA(),
								
							]
						}
					]
				},				
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'rigth'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLapRKA',
							handler: function() 
							{
								if(Validasi_LapRKAPengembangan() == 1)
								{
									var criteria = getLapRKAKriteria_LapRKAPengembangan();
									ShowReport('', '011202', criteria);
									winDlgLapRKA_LapRKAPengembangan.close();
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapRKA_LapRKAPengembangan',
							handler: function() 
							{
								winDlgLapRKA_LapRKAPengembangan.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapRKA_LapRKAPengembangan;
};

function ShowPesanWarningRKAReport_LapRKAPengembangan(str,modul)
{
	Ext.MessageBox.show
	(		
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

/* function getItemLapRKA_UnitKerja()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mcomboUnitKerjaLapRKA_LapRKAPengembangan()
				]
			}
		]
	}
	return items;
}; */

/* function getItemLapRKA_Tahun() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 1,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 65,
		        items:
				[mComboTahunLapRKA()]
		    }
		]
	}
    return items;
}; */

function mcomboUnitKerjaLapRKA_LapRKAPengembangan()
{
	var Field = ['KD_UNIT_KERJA', 'NAMA_UNIT_KERJA','UNITKERJA'];
	dsUnitKerjaLapRKA_LapRKAPengembangan = new WebApp.DataStore({ fields: Field });

  var comboUnitKerjaLapRKA_LapRKAPengembangan = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerjaLapRKA_LapRKAPengembangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Unit Kerja',
			fieldLabel: 'Unit Kerja ',			
			align:'Right',
			anchor:'96%',
			valueField: 'KD_UNIT_KERJA',
			displayField: 'UNITKERJA',
			store:dsUnitKerjaLapRKA_LapRKAPengembangan,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnitKerjaLapRKA_LapRKAPengembangan=b.data.KD_UNIT_KERJA ;
				} 
			}
		}
	);

    dsUnitKerjaLapRKA_LapRKAPengembangan.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'KD_UNIT_KERJA', 
				Sortdir: 'ASC', 
				target:'viCboUnitKerja',
				// param: "kdunit=" + gstrListUnitKerja
				param:''
			} 
		}
	);
	return comboUnitKerjaLapRKA_LapRKAPengembangan;
};

function mComboTahunLapRKA_LapRKAPengembangan()
{
  // var currYear = parseInt(gstrTahunAngg);//parseInt(now_LapRKAPengembangan.format('Y'));
  var currYear = '';//parseInt(now_LapRKAPengembangan.format('Y'));
  var cboTahunLapRKA_LapRKAPengembangan = new Ext.form.ComboBox
	(
		{
			id:'cboTahunLapRKA_LapRKAPengembangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Thn. Anggaran ',			
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					//data: [[1, currYear + 2], [2, currYear + 1], [3, currYear], [4, currYear - 1], [5, currYear - 2]]
					// data: [[1, currYear+1 +'/'+(Ext.num(gstrTahunAngg)+2)], [2, currYear +'/'+(Ext.num(gstrTahunAngg)+1)], [3, currYear-1 +'/'+gstrTahunAngg], [4, currYear-2 +'/'+(Ext.num(gstrTahunAngg)-3)]]
					data:''
				}
			),
			valueField: 'displayText',
			displayField: 'displayText',
			// value: currYear +'/'+(Ext.num(gstrTahunAngg)+1)//now_LapRKAPengembangan.format('Y')			
			value: '2017/2018'
		}
	);
	
	return cboTahunLapRKA_LapRKAPengembangan;
};
function getItemKelompokRKA_LapRKAPengembangan()
{
	var cboKelRKA_LapRKAPengembangan = new Ext.form.ComboBox
	(
		{
			id:'optPN_LapRKAPengembangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Jenis',
			fieldLabel: 'Jenis ',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[2, "Penerimaan"], [1, "Pengeluaran"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboKelRKA_LapRKAPengembangan;
}
	

/* 
function getItemJenisRKA()
{
	var items = 
	{
		xtype: 'compositefield',
		fieldLabel: 'Jenis RKA',
		items:
		[
			{
				xtype: 'radiogroup',
				id: 'rgJnsRKA',
				columns: 2,
				items:
				[
					{boxLabel: 'Pengembangan', name: 'jnsrka', inputValue: '1', id:'opt1', autoWidth: true, checked: true},
					{boxLabel: 'Rutin', name: 'jnsrka', inputValue: '2', id:'opt2', autoWidth: true}
				],
				listeners: 
				{
					'change': function(rg,rc)
					{
						
						radJenisRKA = rc.getId() == "opt1" ? 1 : 2;							
						winDlgLapRKA.setTitle(winTitleLapRKA + radKelompokRKA + '-' + radJenisRKA.toString() + ')');						
					}
				}
			}
		]
	}	
	return items;
} 

function getItemKelompokRKA()
{
	var items = 
	{
		xtype: 'compositefield',
		fieldLabel: 'Kelompok RKA',
		items:
		[
			{
				xtype: 'radiogroup',
				columns: 2,
				items:
				[
					{boxLabel: 'Pengeluaran', name: 'klmpkrka', inputValue: 'PL', id:'optPL', autoWidth: true, checked: true},
					{boxLabel: 'Penerimaan', name: 'klmpkrka', inputValue: 'PN', id:'optPN_LapRKAPengembangan', autoWidth: true}
				],
				listeners: 
				{
					'change': function(rg,rc)
					{
						radKelompokRKA = rc.getId() == "optPL" ? "PL" : "PN";
						winDlgLapRKA.setTitle(winTitleLapRKA + radKelompokRKA + '-' + radJenisRKA.toString() + ')');
					}
				}
			}
		]
	}
	
	return items;
}
*/


function getLapRKAKriteria_LapRKAPengembangan()
{
	var strKriteria = "";	
	/* var StrKdJns; 
	if (Ext.getCmp('optPN_LapRKAPengembangan').getValue()===true)
	{
		StrKdJns=2;
	}
	else
	{
		StrKdJns=1;
	}	 */
	// if(Ext.getCmp('cboTahunLapRKA_LapRKAPengembangan').getValue() !== undefined || Ext.getCmp('cboTahunLapRKA_LapRKAPengembangan').getValue() != "")
	// {		
	// 	strKriteria = " Where TAHUN_ANGGARAN_TA ='" + Ext.getCmp('cboTahunLapRKA_LapRKAPengembangan').getValue() + "'";	
	// }
//	if(Ext.getCmp('comboUnitKerjaLapRKA_LapRKAPengembangan').getValue() !== undefined || Ext.getCmp('comboUnitKerjaLapRKA_LapRKAPengembangan').getValue() != "")
	//{
		/*if(strKriteria == "")
		{
			//strKriteria = " Where KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKAPengembangan').getValue() + "'";			
			strKriteria = " where ( KD_PARENT_UNIT ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKAPengembangan').getValue() + "'";
			strKriteria += " OR KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKAPengembangan').getValue() + "')"
		}
		else
		{
			//strKriteria += " and KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKAPengembangan').getValue() + "'";
			strKriteria += " and ( KD_PARENT_UNIT ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKAPengembangan').getValue() + "'";
			strKriteria += " OR KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKAPengembangan').getValue() + "')"
		}*/
	//}	
	// if(Ext.getCmp('optPN_LapRKAPengembangan').getValue() !== undefined || Ext.getCmp('optPN_LapRKAPengembangan').getValue() != "")
	// {
	// 	if(strKriteria == "")
	// 	{
	// 		strKriteria = " Where KD_JNS_RKAT_JRKA ='" + Ext.getCmp('optPN_LapRKAPengembangan').getValue() + "'";			
	// 	}
	// 	else
	// 	{
	// 		strKriteria += " and KD_JNS_RKAT_JRKA ='" + Ext.getCmp('optPN_LapRKAPengembangan').getValue() + "'";
	// 	}
	// }
	strKriteria += Ext.getCmp('cboTahunLapRKA_LapRKAPengembangan').getValue() + "##";
	strKriteria += Ext.getCmp('optPN_LapRKAPengembangan').getValue() + "##";
	strKriteria += Ext.getCmp('optPN_LapRKAPengembangan').getValue() + "##";
	strKriteria +=  Ext.getCmp('comboUnitKerjaLapRKA_LapRKAPengembangan').getValue()+"##"
	strKriteria += Ext.getCmp('cboTahunLapRKA_LapRKAPengembangan').getValue() +" "+ "##"
	
	
	return strKriteria;
}

function Validasi_LapRKAPengembangan()
{
	var x = 1;
	
	if(Ext.getCmp('cboTahunLapRKA_LapRKAPengembangan').getValue() == '')
	{
		ShowPesanWarning('Pilihan Tahun Anggaran salah!',winTitleLapRKA_LapRKAPengembangan)//'Laporan '+gstrSatker+' Pengembangan');
		x = 0;
	}
	/*if(Ext.getCmp('comboUnitKerjaLapRKA_LapRKAPengembangan').getValue() == '' )
	{
		ShowPesanWarning('Pilihan Unit/Sub '+gstrSatker+' salah!','Laporan '+gstrSatker+' Pengembangan');
		x = 0;
	}*/
	if(Ext.getCmp('optPN_LapRKAPengembangan').getValue() == '' )
	{
		ShowPesanWarning('Pilihan Jenis salah!',winTitleLapRKA_LapRKAPengembangan)//'Laporan '+gstrSatker+' Pengembangan');
		x = 0;
	}

	return x;
};

function ShowPesanWarning(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};