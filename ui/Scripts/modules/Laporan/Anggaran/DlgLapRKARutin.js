
var dsUnitKerjaLapRKA_LapRKARutin;
var dsThAnggPaguRAB;
var selectUnitKerjaLapRKA_LapRKARutin;
var now_LapRKARutin = new Date();
var now = new Date();
//var radJenisRKA_LapRKARutin = 1;
//var radKelompokRKA_LapRKARutin = 'PL';

// var winTitleLapRKA_LapRKARutin =  varLapMainPage +' '+ CurrentPage.title;//'RAPB Rutin';
var winTitleLapRKA_LapRKARutin =  'Laporan RAB Rutin'; 

var winDlgLapRKA_LapRKARutin= fnDlgLapRKA_LapRKARutin();
winDlgLapRKA_LapRKARutin.show();

function fnDlgLapRKA_LapRKARutin()
{  	
    var winDlgLapRKA_LapRKARutin = new Ext.Window
	(
		{ 
			id: 'winDlgLapRKA_LapRKARutin',
			title: winTitleLapRKA_LapRKARutin, //+ radKelompokRKA_LapRKARutin + '-' + radJenisRKA_LapRKARutin.toString() + ')',
			closeAction: 'destroy',
			width:500,
			height: 180,
			resizable:false,
			border: false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [getItemDlgLapRKA_LapRKARutin()]
			
		}
	);
	
    return winDlgLapRKA_LapRKARutin; 
};


function getItemDlgLapRKA_LapRKARutin() 
{	
	var PnlLapRKA_LapRKARutin = new Ext.Panel
	(
		{ 
			id: 'PnlLapRKA_LapRKARutin',
			fileUpload: true,
			layout: 'anchor',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 472,
							height: 105,
							items: 
							[
								mComboTahunLapRKA_LapRKARutin(),
								mcomboUnitKerjaLapRKA_LapRKARutin(),
								getItemKelompokRKA_LapRKARutin()
								//getItemLapRKA_Tahun(),
								//getItemLapRKA_UnitKerja(),
								// getItemJenisRKA(),
								
							]
						}
					]
				},				
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'rigth'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLapRKA',
							handler: function() 
							{
								if(Validasi_LapRKARutin() == 1)
								{
									var params={
										tahun_anggaran	:Ext.getCmp('cboTahunLapRKA_LapRKARutin').getValue(),
										unit_kerja		:Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue(),
										jenis			:Ext.getCmp('optPN_LapRKARutin').getValue()
										
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/anggaran_module/functionLapRAB/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();		
									
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapRKA_LapRKARutin',
							handler: function() 
							{
								winDlgLapRKA_LapRKARutin.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapRKA_LapRKARutin;
};

function ShowPesanWarningRKAReport_LapRKARutin(str,modul)
{
	Ext.MessageBox.show
	(		
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

/* function getItemLapRKA_UnitKerja()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mcomboUnitKerjaLapRKA_LapRKARutin()
				]
			}
		]
	}
	return items;
}; */

/* function getItemLapRKA_Tahun() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 1,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 65,
		        items:
				[mComboTahunLapRKA()]
		    }
		]
	}
    return items;
}; */

function mcomboUnitKerjaLapRKA_LapRKARutin()
{
	var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
    dsUnitKerjaLapRKA_LapRKARutin = new WebApp.DataStore({ fields: Field });

  var comboUnitKerjaLapRKA_LapRKARutin = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerjaLapRKA_LapRKARutin',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Unit Kerja',
			fieldLabel: 'Unit Kerja ',			
			align:'Right',
			anchor:'96%',
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			store:dsUnitKerjaLapRKA_LapRKARutin,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnitKerjaLapRKA_LapRKARutin=b.data.kd_unit ;
				} 
			}
		}
	);

    Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerjaLap",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerjaLapRKA_LapRKARutin.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerjaLapRKA_LapRKARutin.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerjaLapRKA_LapRKARutin.add(recs);
			} else {
			};
		}
	});
	return comboUnitKerjaLapRKA_LapRKARutin;
};

function mComboTahunLapRKA_LapRKARutin()
{
  // var currYear = parseInt(gstrTahunAngg);//parseInt(now_LapRKARutin.format('Y'));
  var Field = ['tahun_anggaran_ta', 'closed_ta'];
	dsThAnggPaguRAB = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getThnAnggaran",
		params: {
			text:''
		},
		failure: function(o)
		{
			//ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsThAnggPaguRAB.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsThAnggPaguRAB.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsThAnggPaguRAB.add(recs);
			} 
		}
	});
	var currYear = parseInt(now.format('Y'));
  var cboTahunLapRKA_LapRKARutin = new Ext.form.ComboBox
	(
		{
			id:'cboTahunLapRKA_LapRKARutin',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Thn. Anggaran ',			
			width:100,
			/* store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					// data: [[1, currYear+1 +'/'+(Ext.num(gstrTahunAngg)+2)], [2, currYear +'/'+(Ext.num(gstrTahunAngg)+1)], [3, currYear-1 +'/'+gstrTahunAngg], [4, currYear-2 +'/'+(Ext.num(gstrTahunAngg)-3)]]
					data:[						
									[currYear + 1,currYear + 1 +'/' + (Ext.num(currYear) + 2)], 
									[currYear,currYear +'/' + (Ext.num(currYear) + 1)]
								]
				}
			),
			valueField: 'Id',
			displayField: 'displayText', */
			store:dsThAnggPaguRAB,
			valueField: 'tahun_anggaran_ta',
			displayField: 'tahun_anggaran_ta'	,
			value: Ext.num(now.format('Y'))
			// value: currYear +'/'+(Ext.num(gstrTahunAngg)+1)//now_LapRKARutin.format('Y')	
		}
	);
	
	return cboTahunLapRKA_LapRKARutin;
};
function getItemKelompokRKA_LapRKARutin()
{
	var cboKelRKA_LapRKARutin = new Ext.form.ComboBox
	(
		{
			id:'optPN_LapRKARutin',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Jenis',
			fieldLabel: 'Jenis ',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[2, "Penerimaan"], [1, "Pengeluaran"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboKelRKA_LapRKARutin;
}
	

/* 
function getItemJenisRKA()
{
	var items = 
	{
		xtype: 'compositefield',
		fieldLabel: 'Jenis RKA',
		items:
		[
			{
				xtype: 'radiogroup',
				id: 'rgJnsRKA',
				columns: 2,
				items:
				[
					{boxLabel: 'Pengembangan', name: 'jnsrka', inputValue: '1', id:'opt1', autoWidth: true, checked: true},
					{boxLabel: 'Rutin', name: 'jnsrka', inputValue: '2', id:'opt2', autoWidth: true}
				],
				listeners: 
				{
					'change': function(rg,rc)
					{
						
						radJenisRKA = rc.getId() == "opt1" ? 1 : 2;							
						winDlgLapRKA.setTitle(winTitleLapRKA + radKelompokRKA + '-' + radJenisRKA.toString() + ')');						
					}
				}
			}
		]
	}	
	return items;
} 

function getItemKelompokRKA()
{
	var items = 
	{
		xtype: 'compositefield',
		fieldLabel: 'Kelompok RKA',
		items:
		[
			{
				xtype: 'radiogroup',
				columns: 2,
				items:
				[
					{boxLabel: 'Pengeluaran', name: 'klmpkrka', inputValue: 'PL', id:'optPL', autoWidth: true, checked: true},
					{boxLabel: 'Penerimaan', name: 'klmpkrka', inputValue: 'PN', id:'optPN_LapRKARutin', autoWidth: true}
				],
				listeners: 
				{
					'change': function(rg,rc)
					{
						radKelompokRKA = rc.getId() == "optPL" ? "PL" : "PN";
						winDlgLapRKA.setTitle(winTitleLapRKA + radKelompokRKA + '-' + radJenisRKA.toString() + ')');
					}
				}
			}
		]
	}
	
	return items;
}
*/


function getLapRKAKriteria_LapRKARutin()
{
	var strKriteria = "";	
	/* var StrKdJns; 
	if (Ext.getCmp('optPN_LapRKARutin').getValue()===true)
	{
		StrKdJns=2;
	}
	else
	{
		StrKdJns=1;
	} */	
	// if(Ext.getCmp('cboTahunLapRKA_LapRKARutin').getValue() !== undefined || Ext.getCmp('cboTahunLapRKA_LapRKARutin').getValue() != "")
	// {		
	// 	strKriteria = " Where TAHUN_ANGGARAN_TA ='" + Ext.getCmp('cboTahunLapRKA_LapRKARutin').getValue() + "'";	
	// }
	/*if(Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue() !== undefined || Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue() != "")
	{
		if(strKriteria == "")
		{
			//strKriteria = " Where KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue() + "'";
			strKriteria += " Where ( PARENT_KD_UNIT ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue() + "'";
			strKriteria += " OR KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue() + "')"			
		}
		else
		{
			//strKriteria += " and KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue() + "'";
			strKriteria += " and ( PARENT_KD_UNIT ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue() + "'";
			strKriteria += " OR KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue() + "')"
		}
	}*/	
	// if(Ext.getCmp('optPN_LapRKARutin').getValue() !== undefined || Ext.getCmp('optPN_LapRKARutin').getValue() != "")
	// {
	// 	if(strKriteria == "")
	// 	{
	// 		strKriteria = " Where KD_JNS_RKAT_JRKA ='" + Ext.getCmp('optPN_LapRKARutin').getValue() + "'";			
	// 	}
	// 	else
	// 	{
	// 		strKriteria += " and KD_JNS_RKAT_JRKA ='" + Ext.getCmp('optPN_LapRKARutin').getValue() + "'";
	// 	}
	// }
	
	strKriteria += Ext.getCmp('cboTahunLapRKA_LapRKARutin').getValue() + " "+ "##";	
	strKriteria += Ext.getCmp('optPN_LapRKARutin').getValue() + " "+ "##";
	strKriteria += Ext.getCmp('optPN_LapRKARutin').getValue() + " "+ "##";
	strKriteria += Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue() + "##"
	strKriteria += Ext.getCmp('cboTahunLapRKA_LapRKARutin').getValue() + "##"
	return strKriteria;
}

function Validasi_LapRKARutin()
{
	var x = 1;
	console.log(Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue());
	if(Ext.getCmp('cboTahunLapRKA_LapRKARutin').getValue() == '')
	{
		ShowPesanWarning('Tahun Anggaran belum dipilih!');//'Laporan '+gstrrkat+' Rutin');
		x = 0;
	}
	if(Ext.getCmp('comboUnitKerjaLapRKA_LapRKARutin').getValue() == '' )
	{
		ShowPesanWarning('Unit Kerja belum dipilih!');
		x = 0;
	}
	if(Ext.getCmp('optPN_LapRKARutin').getValue() == '' )
	{
		ShowPesanWarning('Jenis belum dipilih!')//'Laporan '+gstrrkat+' Rutin');
		x = 0;
	}

	return x;
};

function ShowPesanWarning(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};