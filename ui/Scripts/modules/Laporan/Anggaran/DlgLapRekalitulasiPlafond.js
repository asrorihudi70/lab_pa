﻿var dsUnitKerja_LapRekapPlafond;
var dsThAnggPaguPlafond;
var dsThAnggPaguPlafond2;
var selectUnitKerja_LapRekapPlafond;
var now_LapRekapPlafond = new Date();
var now = new Date();
// var winTitle_LapRekapPlafond =  varLapMainPage +' '+ CurrentPage.title;//'Rekapitulasi Plafond Anggaran';
var winTitle_LapRekapPlafond =  'Laporan Plafond Rumah Sakit';//'Rekapitulasi Plafond Anggaran';

var winDlg_LapRekapPlafond= fnDlg_LapRekapPlafond();
winDlg_LapRekapPlafond.show();

function fnDlg_LapRekapPlafond()
{  	
    var winDlg_LapRekapPlafond = new Ext.Window
	(
		{ 
			id: 'winDlg_LapRekapPlafond',
			title: winTitle_LapRekapPlafond,
			closeAction: 'destroy',
			width:360,
			height: 125,
			resizable:false,
			border: false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [getItemDlg_LapRekapPlafond()]
			
		}
	);
	
    return winDlg_LapRekapPlafond; 
};


function getItemDlg_LapRekapPlafond() 
{	
	var Pnl_LapRekapPlafond = new Ext.Panel
	(
		{ 
			id: 'Pnl_LapRekapPlafond',
			fileUpload: true,
			layout: 'anchor',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 330,
							height: 50,
							items: 
							[
								{
									xtype: 'compositefield',
									fieldLabel: 'Thn. Anggaran',
									items: 
									[
										mComboTahun_LapRekapPlafond(),
										{
											xtype: 'displayfield',
											value: 's/d ',
											width: 15
										},
										mComboTahun2_LapRekapPlafond(),
									]
								},
								// mComboTrans_LapRekapPlafond()
							]
						}
					]
				},				
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'rigth'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOk_LapRekapPlafond',
							handler: function() 
							{
								if(Validasi_LapRekapPlafond() == 1)
								{
									var params={
										tahun_anggaran1	:Ext.getCmp('cboTahun_LapRekapPlafond').getValue(),
										tahun_anggaran2	:Ext.getCmp('cboTahun2_LapRekapPlafond').getValue(),
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/anggaran_module/functionLapPlafondRS/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									winDlg_LapRekapPlafond.close();
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancel_LapRekapPlafond',
							handler: function() 
							{
								winDlg_LapRekapPlafond.close();
							}
						}
					]
				}
			]
		}
	);
 
    return Pnl_LapRekapPlafond;
};

function ShowPesanWarningRKAReport_LapRekapPlafond(str,modul)
{
	Ext.MessageBox.show
	(		
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function mcomboUnitKerja_LapRekapPlafond()
{
	var Field = ['KD_UNIT_KERJA', 'NAMA_UNIT_KERJA','UNITKERJA'];
	dsUnitKerja_LapRekapPlafond = new WebApp.DataStore({ fields: Field });

  var comboUnitKerja_LapRekapPlafond = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_LapRekapPlafond',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Semua Unit/Sub '+gstrSatker,
			fieldLabel: gstrSatker+' ',			
			align:'Right',
			anchor:'96%',
			valueField: 'KD_UNIT_KERJA',
			displayField: 'UNITKERJA',
			store:dsUnitKerja_LapRekapPlafond,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnitKerja_LapRekapPlafond=b.data.KD_UNIT_KERJA ;
				} 
			}
		}
	);

    dsUnitKerja_LapRekapPlafond.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'KD_UNIT_KERJA', 
				Sortdir: 'ASC', 
				target:'viCboUnitKerja',
				param: "kdunit=" + gstrListUnitKerja
			} 
		}
	);
	return comboUnitKerja_LapRekapPlafond;
};

function mComboTahun_LapRekapPlafond()
{
  // var currYear = parseInt(gstrTahunAngg);//parseInt(now_LapRekapPlafond.format('Y'));
  var Field = ['tahun_anggaran_ta', 'closed_ta'];
	dsThAnggPaguPlafond = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getThnAnggaran",
		params: {
			text:''
		},
		failure: function(o)
		{
			//ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsThAnggPaguPlafond.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsThAnggPaguPlafond.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsThAnggPaguPlafond.add(recs);
			} 
		}
	});
	var currYear = parseInt(now.format('Y'));
	var cboTahun_LapRekapPlafond = new Ext.form.ComboBox
	(
		{
			id:'cboTahun_LapRekapPlafond',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:90,
			/* store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					// data: [[1, currYear+1 +'/'+(Ext.num(gstrTahunAngg)+2)], [2, currYear +'/'+(Ext.num(gstrTahunAngg)+1)], [3, currYear-1 +'/'+gstrTahunAngg], [4, currYear-2 +'/'+(Ext.num(gstrTahunAngg)-1)]]
					data: [
						[currYear + 1,currYear + 1 +'/' + (Ext.num(currYear) + 2)], 
									[currYear,currYear +'/' + (Ext.num(currYear) + 1)]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText', */
			store:dsThAnggPaguPlafond,
			valueField: 'tahun_anggaran_ta',
			displayField: 'tahun_anggaran_ta'	,
			// value: currYear +'/'+(Ext.num(gstrTahunAngg)+1)//now_LapRekapPlafond.format('Y')			
			value:Ext.num(now.format('Y'))		
		}
	);
	
	return cboTahun_LapRekapPlafond;
};

function mComboTahun2_LapRekapPlafond()
{
	var Field = ['tahun_anggaran_ta', 'closed_ta'];
	dsThAnggPaguPlafond2 = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getThnAnggaran",
		params: {
			text:''
		},
		failure: function(o)
		{
			//ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsThAnggPaguPlafond2.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsThAnggPaguPlafond2.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsThAnggPaguPlafond2.add(recs);
			} 
		}
	});
 	var currYear = parseInt(now.format('Y'));
  var cboTahun2_LapRekapPlafond = new Ext.form.ComboBox
	(
		{
			id:'cboTahun2_LapRekapPlafond',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:90,
			/* store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					// data: [[1, (Ext.num(gstrTahunAngg)+1) +'/'+(Ext.num(gstrTahunAngg)+2)], [2, (Ext.num(gstrTahunAngg)+0) +'/'+(Ext.num(gstrTahunAngg)+1)], [3, (Ext.num(gstrTahunAngg)-1) +'/'+(Ext.num(gstrTahunAngg)+0)], [4, (Ext.num(gstrTahunAngg)-2) +'/'+(Ext.num(gstrTahunAngg)-1)]]
					data: [
						[currYear + 1,currYear + 1 +'/' + (Ext.num(currYear) + 2)], 
									[currYear,currYear +'/' + (Ext.num(currYear) + 1)]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText', */
			
			store:dsThAnggPaguPlafond2,
			valueField: 'tahun_anggaran_ta',
			displayField: 'tahun_anggaran_ta'	,
			// value: currYear +'/'+(Ext.num(gstrTahunAngg)+1)//now_LapRekapPlafond.format('Y')			
			value:Ext.num(now.format('Y'))			
		}
	);
	
	return cboTahun2_LapRekapPlafond;
};

function mComboTrans_LapRekapPlafond()
{
  var cboTrans_LapRekapPlafond = new Ext.form.ComboBox
	(
		{
			id:'cboTrans_LapRekapPlafond',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih semua',
			fieldLabel: 'Jenis Anggaran ',			
			width:198,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, "OPERASIONAL"], [2, "KEMAHASISWAAN"], [3, "BANTUAN"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText'			
		}
	);
	
	return cboTrans_LapRekapPlafond;
};

function getItemKelompokRKA_LapRekapPlafond()
{
	var cboKelRKA_LapRekapPlafond = new Ext.form.ComboBox
	(
		{
			id:'optPN_LapRekapPlafond',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Jenis ',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[2, "Pengembangan"], [1, "Rutin"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboKelRKA_LapRekapPlafond;
}

function getKriteria_LapRekapPlafond()
{
	var strKriteria = "";	
	
	if(Ext.getCmp('cboTahun_LapRekapPlafond').getValue() !== undefined || Ext.getCmp('cboTahun_LapRekapPlafond').getValue() != "")
	{
		strKriteria = " Where G.TAHUN_ANGGARAN_TA >='" + Ext.getCmp('cboTahun_LapRekapPlafond').getValue() + "'";	
		strKriteria += " and G.TAHUN_ANGGARAN_TA <='" + Ext.getCmp('cboTahun_LapRekapPlafond').getValue() + "'";	
	}
	
	// if(Ext.get('cboTrans_LapRekapPlafond').getValue() != "Pilih semua")
	// {		
	// 	strKriteria += " and D.KD_JNS_PLAFOND ='" + Ext.getCmp('cboTrans_LapRekapPlafond').getValue() + "'";	
	// }
	strKriteria += " "+ "##"
	strKriteria += Ext.getCmp('cboTahun_LapRekapPlafond').getValue() + "##"
	strKriteria += Ext.getCmp('cboTahun2_LapRekapPlafond').getValue() + "##"
	+ 1 + "##"
	
	return strKriteria;
}

function Validasi_LapRekapPlafond()
{
	var x = 1;
	
	if(Ext.getCmp('cboTahun_LapRekapPlafond').getValue() == '')
	{
		ShowPesanWarning_LapRekapPlafond('Pilihan Tahun Anggaran salah!','Laporan Rekap RAPB ');
		x = 0;
	}
	return x;
};

function ShowPesanWarning_LapRekapPlafond(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};