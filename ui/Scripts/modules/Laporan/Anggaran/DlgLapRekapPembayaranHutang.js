var now = new Date();
var frmDlg_LapPaymentAP= fnDlg_LapPaymentAP();
frmDlg_LapPaymentAP.show();

var check_posting='t';
function fnDlg_LapPaymentAP()
{  	
    var win_LapPaymentAP = new Ext.Window
	(
		{ 
			id: 'win_LapPaymentAP',
			// title: 'Lap. '+gstrSp3d,
			title: 'Payment AP',
			closeAction: 'destroy',
			width:425,
			height: 220,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlg_LapPaymentAP()]
			
		}
	);
	
    return win_LapPaymentAP; 
};

function ItemDlg_LapPaymentAP() 
{	
	var PnlLap_LapPaymentAP = new Ext.Panel
	(
		{ 
			id: 'PnlLap_LapPaymentAP',
			fileUpload: true,
			layout: 'anchor',
			// width:350,
			height: 90,
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[

				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 400,
							height: 150, 
							items: 
							[
								//mComboPilihan_LapPaymentAP(),
								mComboVendor_LapPaymentAP(),
								mComboVendor2_LapPaymentAP(),
								getItemLap_LapPaymentAP_Tanggal(),
								//mComboJenisLap_LapPaymentAP(),
								{
									xtype: 'checkbox',
									id: 'Chkapprove_LapPaymentAP',
									fieldLabel: 'Approve Only' ,
									checked: true,
									handler: function()
									{
										if (this.getValue()===true)
										{
											check_posting='t';
										}else
										{
											check_posting='f';
										}
								    }
								},				
							]
						}						
					]
				},
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLap_LapPaymentAP',
							handler: function() 
							{
								if(ValidasiReport_LapPaymentAP() ===1)
								{
									/* if(Ext.getCmp('cbojenisLap_LapPaymentAP').getValue() == 1)
									{
										var criteria = GetCriteria_LapPaymentAP();
										ShowReport('', '020283', criteria);
										frmDlg_LapPaymentAP.close();
									}
									else
									{
										var criteria = GetCriteria_LapPaymentAP();
										ShowReport('', '020283b', criteria);
										frmDlg_LapPaymentAP.close();
									} */
									
									url_tmp= baseURL + "index.php/anggaran/functionLapPembayaranHutang/laporan";
									
									var params={
										cust_awal		:Ext.getCmp('cboCust_LapPaymentAP').getValue(),
										cust_akhir		:Ext.getCmp('cboCust2_LapPaymentAP').getValue(),
										tgl_awal		:Ext.getCmp('dtpTglAwalLap_LapPaymentAP').getValue(),
										tgl_akhir		:Ext.getCmp('dtpTglAkhirLap_LapPaymentAP').getValue(),
										posting			:check_posting,
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action",url_tmp);
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									frmDlg_LapPaymentAP.close();
								};
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLap_LapPaymentAP',
							handler: function() 
							{
								frmDlg_LapPaymentAP.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLap_LapPaymentAP;
};

function GetCriteria_LapPaymentAP()
{
	var strKriteria = "";	
	
	if(SelectVendor_LapPaymentAP !== undefined || SelectVendor_LapPaymentAP != "")
	{
		strKriteria += SelectVendor_LapPaymentAP + "##";
	}

	if(SelectVendor2_LapPaymentAP !== undefined || SelectVendor2_LapPaymentAP != "")
	{
		strKriteria += SelectVendor2_LapPaymentAP + "##";
	}


	if(Ext.getCmp('dtpTglAwalLap_LapPaymentAP').getValue() !== undefined || Ext.getCmp('dtpTglAwalLap_LapPaymentAP').getValue() != "")
	{
		strKriteria += ShowDate(Ext.getCmp('dtpTglAwalLap_LapPaymentAP').getValue()) + "##";
	}
	if(Ext.getCmp('dtpTglAkhirLap_LapPaymentAP').getValue() !== undefined || Ext.getCmp('dtpTglAkhirLap_LapPaymentAP').getValue() != "")
	{
		strKriteria += ShowDate(Ext.getCmp('dtpTglAkhirLap_LapPaymentAP').getValue()) + "##";
	}
	if(Ext.getCmp('cbojenisLap_LapPaymentAP').getValue() != "")
	{
		strKriteria +=  Ext.getCmp('cbojenisLap_LapPaymentAP').getValue()  + "##";
	}else{
		strKriteria += "1"  + "##";
	}

	strKriteria +=Ext.get('Chkapprove_LapPaymentAP').dom.checked + "##";	
	strKriteria += strKdUser
	return strKriteria;
}

function ValidasiReport_LapPaymentAP()
{
	var x=1;
	
	if(SelectVendor_LapPaymentAP == undefined || SelectVendor_LapPaymentAP == "")
	{
		ShowPesanWarning_LapPaymentAP('Vendor belum dipilih','Payment AP');
		x=0;		
	};

	if(SelectVendor2_LapPaymentAP == undefined || SelectVendor2_LapPaymentAP == "")
	{
		ShowPesanWarning_LapPaymentAP('Vendor belum dipilih','Payment AP');
		x=0;		
	};

	if((Ext.get('dtpTglAwalLap_LapPaymentAP').getValue() === ''))
	{
		if(Ext.get('dtpTglAwalLap_LapPaymentAP').getValue() === '')
		{
			ShowPesanWarning_LapPaymentAP('Kriteria tanggal awal belum di isi','Payment AP');
			x=0;
		}
		
	};

	return x;
};

function ShowPesanWarning_LapPaymentAP(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:180
		}
	);
};

function getItemLap_LapPaymentAP_Tanggal() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Periode ',
							id: 'dtpTglAwalLap_LapPaymentAP',
							format: 'd/M/Y',
							value:now,
							width:105
						}
					]
		    	}
			 ,
			 {
		         columnWidth: 0.47,
		         layout: 'form',
		         border: false,
				 labelWidth: 30,
		         labelAlign: 'right',
		         items:
				 [
					 {
						 xtype: 'datefield',
						 fieldLabel: 's/d ',
						 id: 'dtpTglAkhirLap_LapPaymentAP',
						 format: 'd/M/Y',
						 value:now,
						 width:105
					 }
				 ]
		     }
			]
		}
		   
		]
	}
    return items;
};
function mComboJenisLap_LapPaymentAP()
{
	var cbojenisLap_LapPaymentAP = new Ext.form.ComboBox
	(
		{
			id:'cbojenisLap_LapPaymentAP',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			//hidden:true,
			//emptyText:'Pilih semua',
			//readOnly:true,
			width:198,
			fieldLabel: 'Jenis Laporan ',		
			value: '1',
			store: new Ext.data.ArrayStore
			(
				{
					id: 1,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, "Rekap"], [2, "Detail"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cbojenisLap_LapPaymentAP;
}

/*function mComboPilihan_LapPaymentAP()
{
	var cboPilihanLap_LapPaymentAP = new Ext.form.ComboBox
	(
		{
			id:'cboPilihanLap_LapPaymentAP',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Laporan',
			fieldLabel: 'Laporan ',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[
						[1, "Faktur AR"], 
						[2, "Faktur AP"]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboPilihanLap_LapPaymentAP;
}
*/

var dsCboVendor_LapPaymentAP;
var SelectVendor_LapPaymentAP;
var dsCboVendor2_LapPaymentAP;
var SelectVendor2_LapPaymentAP;
function mComboVendor_LapPaymentAP()
{
	var Fields = ['kd_vendor', 'vendor', 'nama_vendor','DUE_DAY','ACCOUNT'];
    dsCboVendor_LapPaymentAP = new WebApp.DataStore({ fields: Fields });
	
	/* dsCboVendor_LapPaymentAP.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'Vendor', 
				Sortdir: 'ASC', 
				target: 'viewCboVendLengkap2',
				param: ''
			} 
		}
	); */
	
	 Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran/functionLapPembayaranHutang/getVendor",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboVendor_LapPaymentAP.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboVendor_LapPaymentAP.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboVendor_LapPaymentAP.add(recs);
		
			} else {
			};
		}
	});
    
    
    var cboCust_LapPaymentAP = new Ext.form.ComboBox
	(
		{
		    id: 'cboCust_LapPaymentAP',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Vendor ...',
		    fieldLabel: 'Vendor ',
		    align: 'right',
		    //anchor:'90%',
		    width:250,
		    listWidth:350,
		    store: dsCboVendor_LapPaymentAP,
		    valueField: 'kd_vendor',
		    displayField: 'nama_vendor',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        SelectVendor_LapPaymentAP = b.data.kd_vendor;
			    }
			}
		}
	);
	
	return cboCust_LapPaymentAP;
};

function mComboVendor2_LapPaymentAP()
{
	var Fields2 = ['kd_vendor', 'vendor', 'nama_vendor','DUE_DAY','ACCOUNT'];
    
    dsCboVendor2_LapPaymentAP = new WebApp.DataStore({ fields: Fields2 });
	
	 Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran/functionLapPembayaranHutang/getVendor",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboVendor2_LapPaymentAP.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboVendor2_LapPaymentAP.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboVendor2_LapPaymentAP.add(recs);
		
			} else {
			};
		}
	});
    
    var cboCust2_LapPaymentAP = new Ext.form.ComboBox
	(
		{
		    id: 'cboCust2_LapPaymentAP',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Vendor ...',
		    fieldLabel: 'Sampai ',
		    align: 'right',
		    //anchor:'90%',
		    width:250,
		    listWidth:350,
		    store: dsCboVendor2_LapPaymentAP,
		    valueField: 'kd_vendor',
		    displayField: 'nama_vendor',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        SelectVendor2_LapPaymentAP = b.data.kd_vendor;
			    }
			}
		}
	);
	
	return cboCust2_LapPaymentAP;
};