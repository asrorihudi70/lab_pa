var now = new Date();
var check_posting='t';
var frmDlg_LapReceiveAR= fnDlg_LapReceiveAR();
frmDlg_LapReceiveAR.show();

function fnDlg_LapReceiveAR()
{  	
    var win_LapReceiveAR = new Ext.Window
	(
		{ 
			id: 'win_LapReceiveAR',
			// title: 'Lap. '+gstrSp3d,
			title: 'Receive AR',
			closeAction: 'destroy',
			width:425,
			height: 220,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlg_LapReceiveAR()]
			
		}
	);
	
    return win_LapReceiveAR; 
};

function ItemDlg_LapReceiveAR() 
{	
	var PnlLap_LapReceiveAR = new Ext.Panel
	(
		{ 
			id: 'PnlLap_LapReceiveAR',
			fileUpload: true,
			layout: 'anchor',
			// width:350,
			height: 90,
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[

				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 400,
							height: 150, 
							items: 
							[
								//mComboPilihan_LapReceiveAR(),
								mComboCustomer_LapReceiveAR(),
								mComboCustomer2_LapReceiveAR(),
								getItemLap_LapReceiveAR_Tanggal(),
								//mComboJenisLap_LapReceiveAR(),
								{
									xtype: 'checkbox',
									id: 'Chkapprove_LapReceiveAR',
									fieldLabel: 'Approve Only' ,
									checked: true,
									handler: function()
									{
										if (this.getValue()===true)
										{
											check_posting='t';
										}else
										{
											check_posting='f';
										}
								    }
								},				
							]
						}						
					]
				},
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLap_LapReceiveAR',
							handler: function() 
							{
								if(ValidasiReport_LapReceiveAR() ===1)
								{
									/* if(Ext.getCmp('cbojenisLap_LapReceiveAR').getValue() == 1)
									{
										var criteria = GetCriteria_LapReceiveAR();
										ShowReport('', '020282', criteria);
										frmDlg_LapReceiveAR.close();
									}
									else
									{
										var criteria = GetCriteria_LapReceiveAR();
										ShowReport('', '020282b', criteria);
										frmDlg_LapReceiveAR.close();
									} */
									
									/* if(Ext.getCmp('cboPilihanLap_LapFaktur').getValue() == 1)
									{
										url_tmp= baseURL + "index.php/anggaran/functionLapFaktur/lap_AR";
									}else
									{
										url_tmp= baseURL + "index.php/anggaran/functionLapFaktur/lap_AP";
									}
 */
									url_tmp= baseURL + "index.php/anggaran/functionLapPenerimaanPiutang/laporan";
									
									var params={
										cust_awal		:Ext.getCmp('cboCust_LapReceiveAR').getValue(),
										cust_akhir		:Ext.getCmp('cboCust2_LapReceiveAR').getValue(),
										tgl_awal		:Ext.getCmp('dtpTglAwalLap_LapReceiveAR').getValue(),
										tgl_akhir		:Ext.getCmp('dtpTglAkhirLap_LapReceiveAR').getValue(),
										posting			:check_posting,
									} 
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action",url_tmp);
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									frmDlg_LapFaktur.close();
								};
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLap_LapReceiveAR',
							handler: function() 
							{
								frmDlg_LapReceiveAR.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLap_LapReceiveAR;
};

function GetCriteria_LapReceiveAR()
{
	var strKriteria = "";	
	
	if(SelectCust_LapReceiveAR !== undefined || SelectCust_LapReceiveAR != "")
	{
		strKriteria += SelectCust_LapReceiveAR + "##";
	}

	if(SelectCust2_LapReceiveAR !== undefined || SelectCust2_LapReceiveAR != "")
	{
		strKriteria += SelectCust2_LapReceiveAR + "##";
	}


	if(Ext.getCmp('dtpTglAwalLap_LapReceiveAR').getValue() !== undefined || Ext.getCmp('dtpTglAwalLap_LapReceiveAR').getValue() != "")
	{
		strKriteria += ShowDate(Ext.getCmp('dtpTglAwalLap_LapReceiveAR').getValue()) + "##";
	}
	if(Ext.getCmp('dtpTglAkhirLap_LapReceiveAR').getValue() !== undefined || Ext.getCmp('dtpTglAkhirLap_LapReceiveAR').getValue() != "")
	{
		strKriteria += ShowDate(Ext.getCmp('dtpTglAkhirLap_LapReceiveAR').getValue()) + "##";
	}
	if(Ext.getCmp('cbojenisLap_LapReceiveAR').getValue() != "")
	{
		strKriteria +=  Ext.getCmp('cbojenisLap_LapReceiveAR').getValue()  + "##";
	}else{
		strKriteria += "1"  + "##";
	}

	strKriteria +=Ext.get('Chkapprove_LapReceiveAR').dom.checked + "##";	
	strKriteria += strKdUser
	return strKriteria;
}

function ValidasiReport_LapReceiveAR()
{
	var x=1;
	
	if(SelectCust_LapReceiveAR == undefined || SelectCust_LapReceiveAR == "")
	{
		ShowPesanWarning_LapReceiveAR('Customer belum dipilih','Receive AR');
		x=0;		
	};

	if(SelectCust2_LapReceiveAR == undefined || SelectCust2_LapReceiveAR == "")
	{
		ShowPesanWarning_LapReceiveAR('Customer belum dipilih','Receive AR');
		x=0;		
	};

	if((Ext.get('dtpTglAwalLap_LapReceiveAR').getValue() === ''))
	{
		if(Ext.get('dtpTglAwalLap_LapReceiveAR').getValue() === '')
		{
			ShowPesanWarning_LapReceiveAR('Kriteria tanggal awal belum di isi','Receive AR');
			x=0;
		}
		
	};

	return x;
};

function ShowPesanWarning_LapReceiveAR(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:180
		}
	);
};

function getItemLap_LapReceiveAR_Tanggal() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Periode ',
							id: 'dtpTglAwalLap_LapReceiveAR',
							format: 'd/M/Y',
							value:now,
							width:105
						}
					]
		    	}
			 ,
			 {
		         columnWidth: 0.47,
		         layout: 'form',
		         border: false,
				 labelWidth: 30,
		         labelAlign: 'right',
		         items:
				 [
					 {
						 xtype: 'datefield',
						 fieldLabel: 's/d ',
						 id: 'dtpTglAkhirLap_LapReceiveAR',
						 format: 'd/M/Y',
						 value:now,
						 width:105
					 }
				 ]
		     }
			]
		}
		   
		]
	}
    return items;
};
function mComboJenisLap_LapReceiveAR()
{
	var cbojenisLap_LapReceiveAR = new Ext.form.ComboBox
	(
		{
			id:'cbojenisLap_LapReceiveAR',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			//hidden:true,
			//emptyText:'Pilih semua',
			//readOnly:true,
			width:198,
			fieldLabel: 'Jenis Laporan ',		
			value: '1',
			store: new Ext.data.ArrayStore
			(
				{
					id: 1,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, "Rekap"], [2, "Detail"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cbojenisLap_LapReceiveAR;
}

/*function mComboPilihan_LapReceiveAR()
{
	var cboPilihanLap_LapReceiveAR = new Ext.form.ComboBox
	(
		{
			id:'cboPilihanLap_LapReceiveAR',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Laporan',
			fieldLabel: 'Laporan ',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[
						[1, "Faktur AR"], 
						[2, "Faktur AP"]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cboPilihanLap_LapReceiveAR;
}
*/

var dsCboCustomer_LapReceiveAR;
var SelectCust_LapReceiveAR;
var dsCboCustomer2_LapReceiveAR;
var SelectCust2_LapReceiveAR;
function mComboCustomer_LapReceiveAR()
{
	var Fields = ['kd_customer', 'customer','nama_customer'];
    dsCboCustomer_LapReceiveAR = new WebApp.DataStore({ fields: Fields });
	
	 Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran/functionLapPenerimaanPiutang/getCustomer",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboCustomer_LapReceiveAR.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboCustomer_LapReceiveAR.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboCustomer_LapReceiveAR.add(recs);
		
			} else {
			};
		}
	});
    
    var cboCust_LapReceiveAR = new Ext.form.ComboBox
	(
		{
		    id: 'cboCust_LapReceiveAR',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih customer ...',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    //anchor:'90%',
		    width:250,
		    listWidth:350,
		    store: dsCboCustomer_LapReceiveAR,
		    valueField: 'kd_customer',
		    displayField: 'nama_customer',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        SelectCust_LapReceiveAR = b.data.kd_customer;
			    }
			}
		}
	);
	
	return cboCust_LapReceiveAR;
};

function mComboCustomer2_LapReceiveAR()
{
	var Fields2 = ['kd_customer', 'customer','nama_customer'];
    dsCboCustomer2_LapReceiveAR = new WebApp.DataStore({ fields: Fields2 });
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran/functionLapPenerimaanPiutang/getCustomer",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboCustomer2_LapReceiveAR.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboCustomer2_LapReceiveAR.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboCustomer2_LapReceiveAR.add(recs);
		
			} else {
			};
		}
	});
	
	
    var cboCust2_LapReceiveAR = new Ext.form.ComboBox
	(
		{
		    id: 'cboCust2_LapReceiveAR',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih customer ...',
		    fieldLabel: 'Sampai ',
		    align: 'right',
		    //anchor:'90%',
		    listWidth:350,
		    width:250,
		    store: dsCboCustomer2_LapReceiveAR,
		    valueField: 'kd_customer',
		    displayField: 'nama_customer',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        SelectCust2_LapReceiveAR = b.data.kd_customer;
			    }
			}
		}
	);
	
	return cboCust2_LapReceiveAR;
};