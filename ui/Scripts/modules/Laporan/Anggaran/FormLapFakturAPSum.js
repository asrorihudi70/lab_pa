
var dsLapLapFakturAPSum;
var selectNamaLapLapFakturAPSum;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapLapFakturAPSum;
var varLapLapFakturAPSum= ShowFormLapLapFakturAPSum();
var posted=1;
function ShowFormLapLapFakturAPSum()
{
    frmDlgLapLapFakturAPSum= fnDlgLapLapFakturAPSum();
    frmDlgLapLapFakturAPSum.show();
};
var kode_vendor_laporan_faktursum_AP = '000';
function fnDlgLapLapFakturAPSum()
{
    var winLapLapFakturAPSumReport = new Ext.Window
    (
        {
            id: 'winLapLapFakturAPSumReport',
            title: 'Lap. Faktur AP Summary',
            closeAction: 'destroy',
            width: 390,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapLapFakturAPSum()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapLapFakturAPSumReport;
};


function ItemDlgLapLapFakturAPSum()
{
    var PnlLapLapFakturAPSum = new Ext.Panel
    (
        {
            id: 'PnlLapLapFakturAPSum',
            fileUpload: true,
            layout: 'form',
            height: 280,
            width:390,
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapFakturAPSum_Tanggal(),
            ]
        }
    );

    return PnlLapLapFakturAPSum;
};

function GetCriteriaLapLapFakturAPSum()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterLapFakturAPSum').getValue();
	return strKriteria;
};
function mCboCustEntry_LapOpenAPForm()
{
	var Fields = ['KODE', 'NAMA','ACCOUNT','TERM'];
    var dsCboEntryCustOpenLapAPForm = new WebApp.DataStore({ fields: Fields });
	
	dsCboEntryCustOpenLapAPForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'NAMA', 
				Sortdir: 'ASC', 
				target: 'viewCboVendLengkap_Laporan',
				param: ''
			} 
		}
	);
    
    var cboCustEntryLapAPForm = new Ext.form.ComboBox
	(
		{
		    id: 'cboCustEntryLapAPForm',
			x: 100,
            y: 10,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Vendor ...',
		    fieldLabel: 'Vendor ',
		    align: 'right',
		    anchor:'90%',
			value : 'Semua',
		    store: dsCboEntryCustOpenLapAPForm,
		    valueField: 'KODE',
		    displayField: 'NAMA',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					kode_vendor_laporan_faktursum_AP = b.data.KODE;
			    }
			}
		}
	);
	
	return cboCustEntryLapAPForm;
};
function getItemLapLapFakturAPSum_Tanggal()
{
    var items = {
        layout: 'column',
        width: 360,
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  390,
            height: 280,
            items: [
			{
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Vendor'
            }, 
            {
                x: 90,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			mCboCustEntry_LapOpenAPForm()
			,
			{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Posted'
            }, 
            {
                x: 90,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 100,
				y: 40,
			   xtype: 'checkbox',
			   fieldLabel: 'Posted ',
			   id: 'CekLapPostedFakturAP',
			   hideLabel:false,
			   boxLabel: 'Posted',
			   checked: true,
			   listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPostedFakturAP').getValue()===true)
						{
							posted=1;
						}
						else
						{
							posted=0;
						}
					}
				}
			},
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Tanggal'
            }, 
            {
                x: 90,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 100,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterLapFakturAPSum',
                format: 'd/M/Y',
                value: now
            },{
                x: 220,
                y: 70,
                xtype: 'label',
                text: ' s / d '
            },{
                x: 245,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterLapFakturAPSum',
                format: 'd/M/Y',
                value: now
            },
            {
                x: 60,
                y: 130,
                xtype: 'button',
                text: 'Cetak',
                iconCls:'print',
                width: 70,
                hideLabel: true,
                id: 'btnOkLapLapFakturAPSum',
                handler: function()
                {
                            //var criteria = GetCriteriaLapLapFakturAPSum();
                            loadMask.show();
							var params={
								kd_vendor : kode_vendor_laporan_faktursum_AP,
								tglAwal:Ext.getCmp('dtpTglAwalFilterLapFakturAPSum').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirFilterLapFakturAPSum').getValue(),
								posted:posted
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/keuangan/cetaklaporanGeneralCashier/faktur_ap_Summary");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgLapLapFakturAPSum.close();
							loadMask.hide();
                    // };
                }
            },
            {
                x: 139,
                y: 130,
                xtype: 'button',
                text: 'Batal' ,
                iconCls: 'remove',
                width: 70,
                hideLabel: true,
                id: 'btnCancelLapLapFakturAPSum',
                handler: function()
                {
                        frmDlgLapLapFakturAPSum.close();
                }
            }
            ]
        }]
    };
    return items;
};

function ShowPesanWAPningLapLapFakturAPSumReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WAPNING,
           width:300
        }
    );
};