
var dsLapAdjusment_ARJournal;
var selectNamaLapAdjusment_ARJournal;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapAdjusment_ARJournal;
var varLapAdjusment_ARJournal= ShowFormLapAdjusment_ARJournal();

function ShowFormLapAdjusment_ARJournal()
{
    frmDlgLapAdjusment_ARJournal= fnDlgLapAdjusment_ARJournal();
    frmDlgLapAdjusment_ARJournal.show();
};

function fnDlgLapAdjusment_ARJournal()
{
    var winLapAdjusment_ARJournalReport = new Ext.Window
    (
        {
            id: 'winLapAdjusment_ARJournalReport',
            title: 'Laporan Adjustment AR Journal',
            closeAction: 'destroy',
            width: 230,
            height: 130,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapAdjusment_ARJournal()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapAdjusment_ARJournalReport;
};


function ItemDlgLapAdjusment_ARJournal()
{
    var PnlLapAdjusment_ARJournal = new Ext.Panel
    (
        {
            id: 'PnlLapAdjusment_ARJournal',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapAdjusment_ARJournal_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapAdjusment_ARJournal',
                            handler: function()
                            {
                                loadMask.show();
								var params={
									tanggal:Ext.getCmp('dtpTglAwalFilterAdjusment_ARJournal').getValue()
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/keuangan/cetaklaporanGeneralCashier/adj_ar_journal");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//frmDlgLapSTS.close();
								loadMask.hide();
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapAdjusment_ARJournal',
                            handler: function()
                            {
                                    frmDlgLapAdjusment_ARJournal.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapAdjusment_ARJournal;
};

function GetCriteriaLapAdjusment_ARJournal()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterAdjusment_ARJournal').getValue();
	return strKriteria;
};

function getItemLapAdjusment_ARJournal_Tanggal()
{
    var items = {
        layout: 'column',
        width: 200,
        border: true,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  200,
            height: 45,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode'
            }, 
			{
                x: 60,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			{
                x: 70,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterAdjusment_ARJournal',
                format: 'M/Y',
                value: now
            }
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapAdjusment_ARJournalReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
