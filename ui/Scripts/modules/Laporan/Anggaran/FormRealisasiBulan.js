
var dsLapRealisasiPemasukanbulanan;
var selectNamaLapRealisasiPemasukanbulanan;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRealisasiPemasukanbulanan;
var varLapRealisasiPemasukanbulanan= ShowFormLapRealisasiPemasukanbulanan();

function ShowFormLapRealisasiPemasukanbulanan()
{
    frmDlgLapRealisasiPemasukanbulanan= fnDlgLapRealisasiPemasukanbulanan();
    frmDlgLapRealisasiPemasukanbulanan.show();
};

function fnDlgLapRealisasiPemasukanbulanan()
{
    var winLapRealisasiPemasukanbulananReport = new Ext.Window
    (
        {
            id: 'winLapRealisasiPemasukanbulananReport',
            title: 'Kas/ Bank Harian',
            closeAction: 'destroy',
            width: 320,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            bodyStyle : 'padding:5px;',
            iconCls: 'icon_lapor',
            modal: true,
            items: [
                {
                    columnWidth: .9,
                    width: 320 -35,
                    labelWidth:100,
                    layout: 'form',
                    border: false,
                    items:[

                        {
                            xtype           : 'combo',
                            id              : 'cboJenisLaporan',
                            typeAhead       : true,
                            triggerAction   : 'all',
                            lazyRender      : true,
                            mode            : 'local',
                            emptyText       : '',
                            fieldLabel      : 'Laporan',
                            align           : 'Right',
                            anchor          : '100%',
                            store           : new Ext.data.ArrayStore({
                                fields  : ['id','displayText'],
                                data    : [
                                    [0, 'Penerimaan'],
                                    [1, 'Pengeluaran'],
                                ]
                            }),
                            valueField      : 'id',
                            displayField    : 'displayText',
                            value           : 'Penerimaan',
                            listeners       :{
                                select: function(a, b, c){
                                }
                            }
                        },{
                            xtype: 'datefield',
                            fieldLabel:  'Dari Tgl',
                            name: 'txtTanggalAwal',
                            id: 'txtTanggalAwal',
                            value:now.format('Y-m-d'),
                            format : 'Y-m-d',
                            anchor: '100%'
                        },{
                            xtype: 'datefield',
                            fieldLabel:  'Sampai Tgl',
                            name: 'txtTanggalAkhir',
                            id: 'txtTanggalAkhir',
                            value:now.format('Y-m-d'),
                            format : 'Y-m-d',
                            anchor: '100%'
                        },{
                            xtype: 'checkbox',
                            fieldLabel:  'Approve',
                            name: 'CheckApprove',
                            id: 'CheckApprove',
                            anchor: '100%'
                        },
                    ]
                }
            ],
            listeners:
			{
				activate: function()
				{
				   
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'Print',
					width: 70,
                    iconCls : 'print',
					hideLabel: true,
					id: 'btnOkLapRealisasiPemasukanbulanan',
					handler: function()
					{  
                        var params = {
                            laporan     : Ext.getCmp('cboJenisLaporan').getValue(),
                            tgl_awal    : Ext.getCmp('txtTanggalAwal').getValue().format('Y-m-d'),
                            tgl_akhir   : Ext.getCmp('txtTanggalAkhir').getValue().format('Y-m-d'),
                            approved    : Ext.getCmp('CheckApprove').getValue(),
                        };
						// var criteria = GetCriteriaLapRealisasiPemasukanbulanan();
						//   loadlaporanAnggaran('0', 'LapRealisasiBulanan', criteria, function(){
						// });
                        
                        window.open(baseURL + "index.php/anggaran/laporan/laporan_kas_bank/"+params.laporan+"/"+params.tgl_awal+"/"+params.tgl_akhir+"/"+params.approved);
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRealisasiPemasukanbulanan',
					handler: function()
					{
							frmDlgLapRealisasiPemasukanbulanan.close();
					}
				}
			]

        }
    );

    return winLapRealisasiPemasukanbulananReport;
};


function ItemDlgLapRealisasiPemasukanbulanan()
{
    var PnlLapRealisasiPemasukanbulanan = new Ext.Panel
    (
        {
            id: 'PnlLapRealisasiPemasukanbulanan',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapRealisasiPemasukanbulanan_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        
                    ]
                }
            ]
        }
    );

    return PnlLapRealisasiPemasukanbulanan;
};

function GetCriteriaLapRealisasiPemasukanbulanan()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterRealisasiPemasukanbulanan').getValue();
	return strKriteria;
};

function getItemLapRealisasiPemasukanbulanan_Tanggal()
{
    var items = {
        layout: 'column',
        width: 200,
        border: true,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  200,
            height: 45,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Tanggal'
            }, 
			{
                x: 60,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			{
                x: 70,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterRealisasiPemasukanbulanan',
                format: 'M/Y',
                value: now
            }
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapRealisasiPemasukanbulananReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
