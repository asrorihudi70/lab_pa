
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapRealisasiTriWulan;
var selectNamaLapRealisasiTriWulan;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRealisasiTriWulan;
var varLapRealisasiTriWulan= ShowFormLapRealisasiTriWulan();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;

function ShowFormLapRealisasiTriWulan()
{
    frmDlgLapRealisasiTriWulan= fnDlgLapRealisasiTriWulan();
    frmDlgLapRealisasiTriWulan.show();
};

function fnDlgLapRealisasiTriWulan()
{
    var winLapRealisasiTriWulanReport = new Ext.Window
    (
        {
            id: 'winLapRealisasiTriWulanReport',
            title: 'Laporan Realisasi Per Triwulan',
            closeAction: 'destroy',
            width: 230,
            height: 130,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRealisasiTriWulan()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapRealisasiTriWulanReport;
};


function ItemDlgLapRealisasiTriWulan()
{
    var PnlLapRealisasiTriWulan = new Ext.Panel
    (
        {
            id: 'PnlLapRealisasiTriWulan',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapRealisasiTriWulan_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRealisasiTriWulan',
                            handler: function()
                            {
                                // if (ValidasiReportLapRealisasiTriWulan() === 1)
                                // {
                                        //var tmppilihan = getKodeReportLapRealisasiTriWulan();
                                        // var criteria = GetCriteriaLapRealisasiTriWulan();
                                        // loadMask.show();
                                        var criteria = '';
                                        loadlaporanAnggaran('0', 'LapRealisasiTriwulan', criteria, function(){
											// frmDlgLapRealisasiTriWulan.close();
											// loadMask.hide();
										});
                                // };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRealisasiTriWulan',
                            handler: function()
                            {
                                    frmDlgLapRealisasiTriWulan.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRealisasiTriWulan;
};

function GetCriteriaLapRealisasiTriWulan()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterRealisasiTriWulan').getValue();
	strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterRealisasiTriWulan').getValue();
	
	if (Ext.get('cboPilihanLapRealisasiTriWulan').getValue() !== '' || Ext.get('cboPilihanLapRealisasiTriWulan').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'asal pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanLapRealisasiTriWulan').getValue();
	}
	if (Ext.get('cboUnitFarLapRealisasiTriWulan').getValue() !== '' || Ext.get('cboUnitFarLapRealisasiTriWulan').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + Ext.get('cboUnitFarLapRealisasiTriWulan').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitFarLapRealisasiTriWulan').getValue();
	}
	if (Ext.get('cboUserRequestEntryLapRealisasiTriWulan').getValue() !== ''|| Ext.get('cboUserRequestEntryLapRealisasiTriWulan').getValue() !== 'Pilih User...'){
		strKriteria += '##@@##' + 'operator';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryLapRealisasiTriWulan').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryLapRealisasiTriWulan').getValue();
	}
	
	if (Ext.getCmp('Shift_All_LapRealisasiTriWulan').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_LapRealisasiTriWulan').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_LapRealisasiTriWulan').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_LapRealisasiTriWulan').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	}
	
	return strKriteria;
};

function ValidasiReportLapRealisasiTriWulan()
{
	var x=1;
	if(Ext.get('cboUnitFarLapRealisasiTriWulan').getValue() === ''|| Ext.get('cboUnitFarLapRealisasiTriWulan').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapRealisasiTriWulanReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapRealisasiTriWulan').getValue() === '' || Ext.get('cboUserRequestEntryLapRealisasiTriWulan').getValue() == 'Pilih User...'){
		ShowPesanWarningLapRealisasiTriWulanReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalFilterRealisasiTriWulan').dom.value > Ext.get('dtpTglAkhirFilterRealisasiTriWulan').dom.value)
    {
        ShowPesanWarningLapRealisasiTriWulanReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }

    return x;
};



function getItemLapRealisasiTriWulan_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  380,
            height: 45,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Tanggal'
            }, 
			{
                x: 60,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            mComboPilihanLapRealisasiTriWulan()
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function mComboPilihanLapRealisasiTriWulan()
{
    var cboPilihanLapRealisasiTriWulan = new Ext.form.ComboBox
	(
            {
                x: 70,
                y: 10,
                id:'cboPilihanLapRealisasiTriWulan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: ' ',
                width:100,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Triwulan 1'], [2, 'Triwulan 2'],[3, 'Triwulan 3'], [4, 'Triwulan 4']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Triwulan 1',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                // selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapRealisasiTriWulan;
};


function ShowPesanWarningLapRealisasiTriWulanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
