
var dsLapSTS;
var selectNamaLapSTS;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapSTS;
var varLapSTS= ShowFormLapSTS();
var posted=1;
function ShowFormLapSTS()
{
    frmDlgLapSTS= fnDlgLapSTS();
    frmDlgLapSTS.show();
};
var kode_customer_laporan_faktursum_ar = '000';
function fnDlgLapSTS()
{
    var winLapSTSReport = new Ext.Window
    (
        {
            id: 'winLapSTSReport',
            title: 'Lap. Surat Tanda Setoran',
            closeAction: 'destroy',
            width: 390,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapSTS()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapSTSReport;
};


function ItemDlgLapSTS()
{
    var PnlLapSTS = new Ext.Panel
    (
        {
            id: 'PnlLapSTS',
            fileUpload: true,
            layout: 'form',
            height: 280,
            width:390,
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapSTS_Tanggal(),
            ]
        }
    );

    return PnlLapSTS;
};

function GetCriteriaLapSTS()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterSTS').getValue();
	return strKriteria;
};
function mCboCustEntry_LapOpenArForm()
{
	var Fields = ['KODE', 'NAMA','ACCOUNT','TERM'];
    var dsCboEntryCustOpenLapArForm = new WebApp.DataStore({ fields: Fields });
	
	dsCboEntryCustOpenLapArForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'NAMA', 
				Sortdir: 'ASC', 
				target: 'ViewComboCustomer_Keuangan_Laporan',
				param: ''
			} 
		}
	);
    
    var cboCustEntryLapARForm = new Ext.form.ComboBox
	(
		{
		    id: 'cboCustEntryLapARForm',
			x: 100,
            y: 10,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih customer ...',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'90%',
			value : 'Semua',
		    store: dsCboEntryCustOpenLapArForm,
		    valueField: 'KODE',
		    displayField: 'NAMA',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					kode_customer_laporan_faktursum_ar = b.data.KODE;
			    }
			}
		}
	);
	
	return cboCustEntryLapARForm;
};
function getItemLapSTS_Tanggal()
{
    var items = {
        layout: 'column',
        width: 360,
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  390,
            height: 280,
            items: [
			{
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Customer'
            }, 
            {
                x: 90,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			mCboCustEntry_LapOpenArForm()
			,
			{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Posted'
            }, 
            {
                x: 90,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 100,
				y: 40,
			   xtype: 'checkbox',
			   fieldLabel: 'Posted ',
			   id: 'CekLapPostedFakturAR',
			   hideLabel:false,
			   boxLabel: 'Posted',
			   checked: true,
			   listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPostedFakturAR').getValue()===true)
						{
							posted=1;
						}
						else
						{
							posted=0;
						}
					}
				}
			},
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Tanggal'
            }, 
            {
                x: 90,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 100,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterSTS',
                format: 'd/M/Y',
                value: now
            },{
                x: 220,
                y: 70,
                xtype: 'label',
                text: ' s / d '
            },{
                x: 245,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterSTS',
                format: 'd/M/Y',
                value: now
            },
            {
                x: 60,
                y: 130,
                xtype: 'button',
                text: 'Cetak',
                iconCls:'print',
                width: 70,
                hideLabel: true,
                id: 'btnOkLapSTS',
                handler: function()
                {
                            //var criteria = GetCriteriaLapSTS();
                            loadMask.show();
							var params={
								kd_customer : kode_customer_laporan_faktursum_ar,
								tglAwal:Ext.getCmp('dtpTglAwalFilterSTS').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirFilterSTS').getValue(),
								posted:posted
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/keuangan/cetaklaporanGeneralCashier/faktur_ar_summary");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgLapSTS.close();
							loadMask.hide();
                    // };
                }
            },
            {
                x: 139,
                y: 130,
                xtype: 'button',
                text: 'Batal' ,
                iconCls: 'remove',
                width: 70,
                hideLabel: true,
                id: 'btnCancelLapSTS',
                handler: function()
                {
                        frmDlgLapSTS.close();
                }
            }
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapSTSReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};