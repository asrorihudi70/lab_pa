
var dsLapVoucerPenerimaanPiutang;
var selectNamaLapVoucerPenerimaanPiutang;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapVoucerPenerimaanPiutang;
var varLapVoucerPenerimaanPiutang= ShowFormLapVoucerPenerimaanPiutang();

function ShowFormLapVoucerPenerimaanPiutang()
{
    frmDlgLapVoucerPenerimaanPiutang= fnDlgLapVoucerPenerimaanPiutang();
    frmDlgLapVoucerPenerimaanPiutang.show();
};

function fnDlgLapVoucerPenerimaanPiutang()
{
    var winLapVoucerPenerimaanPiutangReport = new Ext.Window
    (
        {
            id: 'winLapVoucerPenerimaanPiutangReport',
            title: 'Lap. Voucer Penerimaan Piutang',
            closeAction: 'destroy',
            width: 490,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapVoucerPenerimaanPiutang()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapVoucerPenerimaanPiutangReport;
};


function ItemDlgLapVoucerPenerimaanPiutang()
{
    var PnlLapVoucerPenerimaanPiutang = new Ext.Panel
    (
        {
            id: 'PnlLapVoucerPenerimaanPiutang',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapVoucerPenerimaanPiutang_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'0px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapVoucerPenerimaanPiutang',
                            handler: function()
                            {
                                // if (ValidasiReportLapVoucerPenerimaanPiutang() === 1)
                                // {
                                        //var tmppilihan = getKodeReportLapVoucerPenerimaanPiutang();
                                        var criteria = GetCriteriaLapVoucerPenerimaanPiutang();
                                        // loadMask.show();
                                        // var criteria = '';
                                        loadlaporanAnggaran('0', 'LapVoucerPenerimaanPiutang', criteria, function(){
											// frmDlgLapVoucerPenerimaanPiutang.close();
											// loadMask.hide();
										});
                                // };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapVoucerPenerimaanPiutang',
                            handler: function()
                            {
                                    frmDlgLapVoucerPenerimaanPiutang.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapVoucerPenerimaanPiutang;
};

function GetCriteriaLapVoucerPenerimaanPiutang()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterVoucerPenerimaanPiutang').getValue();
	return strKriteria;
};

function getItemLapVoucerPenerimaanPiutang_Tanggal()
{
    var items = {
        layout: 'column',
        width: 500,
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  500,
            height: 130,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Dari Account'
            }, 
			{
                x: 90,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            mComboDariAkunVoucerPenerimaanPiutang(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Ke Account'
            }, 
            {
                x: 90,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            mComboKeAkunVoucerPenerimaanPiutang(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Tanggal'
            }, 
            {
                x: 90,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 100,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterVoucerPenerimaanPiutang',
                format: 'd/M/Y',
                value: now
            },
            {
                x: 220,
                y: 75,
                xtype: 'label',
                text: 's/d'
            },
            {
                x: 250,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterVoucerPenerimaanPiutang',
                format: 'd/M/Y',
                value: now
            }, 
            {
                x: 10,
                y: 100,
                xtype: 'label',
                text: 'Rangking By'
            }, 
            {
                x: 90,
                y: 100,
                xtype: 'label',
                text: ' : '
            },
            mComboPilihanRangking(),
            // {
            //     x: 220,
            //     y: 100,
            //     xtype: 'label',
            //     text: 'Bendahara '
            // }, 
            // {
            //     x: 300,
            //     y: 100,
            //     xtype: 'label',
            //     text: ' : '
            // },
            // mComboBendahara()
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapVoucerPenerimaanPiutangReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkodedariAkun;
var tmpkodekeAkun;
function mComboDariAkunVoucerPenerimaanPiutang()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboDariAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboDariAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboAccountVoucerPenerimaanPiutang',
                param: ""
            }
        }
    );
    var cboDariAkun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboDariAkun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:360,
                store: dsUnitComboDariAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodedariAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboDariAkun;
};

function mComboKeAkunVoucerPenerimaanPiutang()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboKeAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboKeAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboAccountVoucerPenerimaanPiutang',
                param: " "
            }
        }
    );
    var cboKeAKun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 40,
                id:'cboKeAKun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:360,
                store: dsUnitComboKeAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodekeAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboKeAKun;
};

function mComboPilihanRangking()
{
    var cboPilihanLapPilihanRangking = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 100,
                id:'cboPilihanLapPilihanRangking',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: ' ',
                width:100,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Tanggal'], [2, 'Referensi'],[3, 'voucer']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Tanggal',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                // selectSetPilihan=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanLapPilihanRangking;
};

function mComboBendahara()
{
    var cboBendahara = new Ext.form.ComboBox
    (
            {
                x: 310,
                y: 100,
                id:'cboBendahara',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'',
                fieldLabel: ' ',
                width:150,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Bendahara Penerimaan'], [2, 'Bendahara Pengeluaran']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Bendahara Penerimaan',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                        }
                }
            }
    );
    return cboBendahara;
};