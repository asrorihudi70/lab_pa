
var dsLapKasBankHarian;
var selectNamaLapKasBankHarian;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapKasBankHarian;
var varLapKasBankHarian= ShowFormLapKasBankHarian();

function ShowFormLapKasBankHarian()
{
    frmDlgLapKasBankHarian= fnDlgLapKasBankHarian();
    frmDlgLapKasBankHarian.show();
};

function fnDlgLapKasBankHarian()
{
    var winLapKasBankHarianReport = new Ext.Window
    (
        {
            id: 'winLapKasBankHarianReport',
            title: 'Lap. Kas/Bank Harian',
            closeAction: 'destroy',
            width: 480,
            height: 300,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapKasBankHarian()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapKasBankHarianReport;
};


function ItemDlgLapKasBankHarian()
{
    var PnlLapKasBankHarian = new Ext.Panel
    (
        {
            id: 'PnlLapKasBankHarian',
            fileUpload: true,
            layout: 'form',
            height: '200',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
                getItemLapKasBankHarian_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'0px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapKasBankHarian',
                            handler: function()
                            {
                                // if (ValidasiReportLapKasBankHarian() === 1)
                                // {
                                        //var tmppilihan = getKodeReportLapKasBankHarian();
                                        var criteria = GetCriteriaLapKasBankHarian();
                                        // loadMask.show();
                                        // var criteria = '';
                                        loadlaporanAnggaran('0', 'LapKasBankHarian', criteria, function(){
											// frmDlgLapKasBankHarian.close();
											// loadMask.hide();
										});
                                // };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapKasBankHarian',
                            handler: function()
                            {
                                    frmDlgLapKasBankHarian.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapKasBankHarian;
};

function GetCriteriaLapKasBankHarian()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterKasBankHarian').getValue();
	return strKriteria;
};

function getItemLapKasBankHarian_Tanggal()
{
    var items = {
        layout: 'column',
        width: 500,
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  500,
            height: 200,
            // anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Dari Account'
            }, 
			{
                x: 90,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            mComboDariAkun(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Ke Account'
            }, 
            {
                x: 90,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            // mComboDariAkun(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Tanggal'
            }, 
            {
                x: 90,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 100,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterKasBankHarian',
                format: 'd/M/Y',
                value: now
            },
            {
                x: 220,
                y: 75,
                xtype: 'label',
                text: 's/d'
            },
            {
                x: 250,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterKasBankHarian',
                format: 'd/M/Y',
                value: now
            }, 
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLapKasBankHarianReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

var tmpkodedariAkun;
var tmpkodekeAkun;
function mComboDariAkun()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboDariAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboDariAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboAccount',
                param: ""
            }
        }
    );
    var cboDariAkun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboDariAkun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:350,
                store: dsUnitComboDariAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodedariAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboDariAkun;
};

function mComboKeAkun()
{
    var Field = ['KODE','NAMA'];
    dsUnitComboKeAkun = new WebApp.DataStore({fields: Field});
    dsUnitComboKeAkun.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewAccount',
                param: " MA_type = 'D'  and isdebit = 't' order by MA.ma_id"
            }
        }
    );
    var cboKeAKun = new Ext.form.ComboBox
    (
            {
                x: 100,
                y: 10,
                id:'cboKeAKun',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:350,
                store: dsUnitComboKeAkun,
                valueField: 'KODE',
                displayField: 'NAMA',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          tmpkodekeAkun=b.data.displayText ;
                        }
                }
            }
    );
    return cboKeAKun;
};

