
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapKasirSummary;
var selectNamaLapKasirSummary;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapKasirSummary;
var varLapKasirSummary= ShowFormLapKasirSummary();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var pilihshift=false;
function ShowFormLapKasirSummary()
{
    frmDlgLapKasirSummary= fnDlgLapKasirSummary();
    frmDlgLapKasirSummary.show();
};

function fnDlgLapKasirSummary()
{
    var winLapKasirSummaryReport = new Ext.Window
    (
        {
            id: 'winLapKasirSummaryReport',
            title: 'Laporan Kasir (Summary)',
            closeAction: 'destroy',
            width: 405,
            height: 300,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapKasirSummary()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapKasirSummaryReport;
};


function ItemDlgLapKasirSummary()
{
    var PnlLapKasirSummary = new Ext.Panel
    (
        {
            id: 'PnlLapKasirSummary',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapKasirSummary_Tanggal(),
                getItemLapKasirSummary_Batas(),
                getItemLapKasirSummary_Shift(),
				getItemLapKasirSummary_Batas(),
				getItemLapKasirSummary_Unit(),
                {
                    layout: 'absolute',
                    border: false,
					height:30,
					width:380,
                    items:
                    [
                        {
							x:140,
							y:8,
                            xtype: 'button',
                            text: 'Preview',
                            width: 70,
                            hideLabel: true,
                            id: 'btnPreviewLapKasirSummary',
                            handler: function()
                            {
                                if (ValidasiReportLapKasirSummary() === 1)
                                {
									var criteria = GetCriteriaLapKasirSummary();
									var params={
											criteria:criteria
									} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/apotek/lap_kasir_summary/preview");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();
									frmDlgLapKasirSummary.close();
									
                                };
                            }
                        },
						{
							x:230,
							y:8,
                            xtype: 'button',
                            text: 'Print',
                            width: 70,
                            hideLabel: true,
                            id: 'btnPrintLapKasirSummary',
                            handler: function()
                            {
                                if (ValidasiReportLapKasirSummary() === 1)
                                {
									var criteria = GetCriteriaLapKasirSummary();
									var params={
											criteria:criteria
									} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/apotek/lap_kasir_summary/print_direct");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									frmDlgLapKasirSummary.close();
                                };
                            }
                        }, 
                        {
							x:310,
							y:8,
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapKasirSummary',
                            handler: function()
                            {
                                    frmDlgLapKasirSummary.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapKasirSummary;
};

function GetCriteriaLapKasirSummary()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterHasilLab').getValue();
	strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterHasilLab').getValue();
	
	if (Ext.get('cboPilihanLapKasirSummary').getValue() !== '' || Ext.get('cboPilihanLapKasirSummary').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'asal pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanLapKasirSummary').getValue();
	}
	if (Ext.get('cboUnitFarLapKasirSummary').getValue() !== '' || Ext.get('cboUnitFarLapKasirSummary').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + Ext.get('cboUnitFarLapKasirSummary').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitFarLapKasirSummary').getValue();
	}
	if (Ext.get('cboUserRequestEntryLapKasirSummary').getValue() !== ''|| Ext.get('cboUserRequestEntryLapKasirSummary').getValue() !== 'Pilih User...'){
		strKriteria += '##@@##' + 'operator';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryLapKasirSummary').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryLapKasirSummary').getValue();
	}
	
	if (Ext.getCmp('Shift_All_LapKasirSummary').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
		
	}else{
		if (Ext.getCmp('Shift_1_LapKasirSummary').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
			
		}
		if (Ext.getCmp('Shift_2_LapKasirSummary').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
			
		}
		if (Ext.getCmp('Shift_3_LapKasirSummary').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
			
		}
	}
	
	return strKriteria;
};

function ValidasiReportLapKasirSummary()
{
	var x=1;
	if(Ext.get('cboUnitFarLapKasirSummary').getValue() === ''|| Ext.get('cboUnitFarLapKasirSummary').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapKasirSummaryReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapKasirSummary').getValue() === '' || Ext.get('cboUserRequestEntryLapKasirSummary').getValue() == 'Pilih User...'){
		ShowPesanWarningLapKasirSummaryReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapKasirSummaryReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }
	if(pilihshift == false){
		ShowPesanWarningLapKasirSummaryReport('Shift belum dipilih!','Warning');
        x=0;
	}
    return x;
};



function getItemLapKasirSummary_Unit()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  380,
            height: 115,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapKasirSummary(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Unit '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            mComboUnitLapKasirSummary(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
			mComboUserLapKasirSummary()
            ]
        }]
    };
    return items;
};


function getItemLapKasirSummary_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  380,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapKasirSummary_Shift()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  380,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
					{
						x: 60,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapKasirSummary',
						id : 'Shift_All_LapKasirSummary',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapKasirSummary').setValue(true);
								Ext.getCmp('Shift_2_LapKasirSummary').setValue(true);
								Ext.getCmp('Shift_3_LapKasirSummary').setValue(true);
								Ext.getCmp('Shift_1_LapKasirSummary').disable();
								Ext.getCmp('Shift_2_LapKasirSummary').disable();
								Ext.getCmp('Shift_3_LapKasirSummary').disable();
								pilihshift=true;
							}else{
								Ext.getCmp('Shift_1_LapKasirSummary').setValue(false);
								Ext.getCmp('Shift_2_LapKasirSummary').setValue(false);
								Ext.getCmp('Shift_3_LapKasirSummary').setValue(false);
								Ext.getCmp('Shift_1_LapKasirSummary').enable();
								Ext.getCmp('Shift_2_LapKasirSummary').enable();
								Ext.getCmp('Shift_3_LapKasirSummary').enable();
							}
						}
					},
					{
						x: 130,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapKasirSummary',
						id : 'Shift_1_LapKasirSummary',
						handler: function (field, value) {
							if (value === true){
								pilihshift=true;
							}
						}
					},
					{
						x: 200,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapKasirSummary',
						id : 'Shift_2_LapKasirSummary',
						handler: function (field, value) {
							if (value === true){
								pilihshift=true;
							}
						}
					},
					{
						x: 270,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapKasirSummary',
						id : 'Shift_3_LapKasirSummary',
						handler: function (field, value) {
							if (value === true){
								pilihshift=true;
							}
						}
					}
				]
            }
        ]
            
    };
    return items;
};

function getItemLapKasirSummary_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  380,
            height: 45,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Tanggal'
            }, 
			{
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			{
                x: 120,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilLab',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 235,
                y: 10,
                xtype: 'label',
                text: ' s/d '
            }, 
			{
                x: 265,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function mComboPilihanLapKasirSummary()
{
    var cboPilihanLapKasirSummary = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapKasirSummary',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapKasirSummary;
};

function mComboUnitLapKasirSummary()
{
	var Field = ['KD_UNIT_FAR','NM_UNIT_FAR'];
    dsUnitFarLapKasirSummary = new WebApp.DataStore({fields: Field});
    dsUnitFarLapKasirSummary.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboUnitFar',
			    param: " kd_unit_far <>'' order by urut,nm_unit_far"
			}
		}
	);
    var cboUnitFarLapKasirSummary = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 40,
                id:'cboUnitFarLapKasirSummary',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsUnitFarLapKasirSummary,
                valueField: 'KD_UNIT_FAR',
                displayField: 'NM_UNIT_FAR',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboUnitFarLapKasirSummary;
};

function mComboUserLapKasirSummary()
{
    var Field = ['KD_USER','FULL_NAME'];
    ds_User_viDaftar = new WebApp.DataStore({fields: Field});

	ds_User_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: '',
                    Sortdir: '',
                    target:'ViewComboUser',
                    param: ""
                }
            }
        );

    var cboUserRequestEntryLapKasirSummary = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 70,
            id: 'cboUserRequestEntryLapKasirSummary',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:'Semua',
            store: ds_User_viDaftar,
            valueField: 'KD_USER',
            displayField: 'FULL_NAME',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.KD_USER;
				} 
			}
        }
    )

    return cboUserRequestEntryLapKasirSummary;
};

function ShowPesanWarningLapKasirSummaryReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
