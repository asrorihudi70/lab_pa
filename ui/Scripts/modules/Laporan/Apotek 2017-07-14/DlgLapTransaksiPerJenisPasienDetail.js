var print=true;
var DlgLapTransaksiPerJenisPasienDetail={
	ArrayStore:{
		unit1:Q().arraystore(),
		unit2:Q().arraystore(),
		unit3:Q().arraystore(),
		unit4:Q().arraystore()
	},
	CheckBox:{
		shift1:null,
		shift2:null,
		shift3:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	DropDown:{
		operator:null,
		unitRawat:null,
		unit:null,
		jenisPasien:null
	},
	Grid:{
		unit1:null,
		unit2:null,
		unit3:null,
		unit4:null
	},
	Window:{
		main:null
	},
	GetCriteriaLapResepPasienPerFaktur:function()
	{
		$this=this;
		var strKriteria = '';
		
		strKriteria = 'Operator';
		strKriteria += '##@@##' + $this.DropDown.operator.getValue();
		
		// strKriteria += '##@@##' + 'unit_rawat';
		// strKriteria += '##@@##' + $this.DropDown.unitRawat.getValue();
		
		strKriteria += '##@@##' + 'unit';
		strKriteria += '##@@##' + $this.DropDown.unit.getValue();
		
		// strKriteria += '##@@##' + 'jenis_pasien';
		// strKriteria += '##@@##' + $this.DropDown.jenisPasien.getValue();
		
		strKriteria += '##@@##' + 'start_date';
		strKriteria += '##@@##' + Q($this.DateField.startDate).val();
		
		strKriteria += '##@@##' + 'last_date';
		strKriteria += '##@@##' + Q($this.DateField.endDate).val();
		
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + $this.CheckBox.shift1.getValue();
		
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + $this.CheckBox.shift2.getValue();
		
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + $this.CheckBox.shift3.getValue();
		
		return strKriteria;
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
	
		/* $.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/main/cetaklaporanApotek/lapResepPasienPerFaktur",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		}); */
		loadMask.show();
		var criteria = $this.GetCriteriaLapResepPasienPerFaktur();
		// loadlaporanApotek('0', 'lapResepPasienPerFaktur', criteria, function(){
			// $this.Window.main.close();
			// loadMask.hide();
		// });
		var params={};
		var sendDataArrayUnit_Rawat = [];
		$this.ArrayStore.unit2.each(function(record){
			var recordArrayUnit_Rawat= [record.get("text")];
			sendDataArrayUnit_Rawat.push(recordArrayUnit_Rawat);
		});
		
		var sendDataArrayUnit_JP = [];
		$this.ArrayStore.unit4.each(function(record){
			var recordArrayUnit_JP= [record.get("text")];
			sendDataArrayUnit_JP.push(recordArrayUnit_JP);
		});
		
		params['criteria']=criteria;
		params['tmp_jenis_pasien'] 	= sendDataArrayUnit_JP;
		params['tmp_unit_rawat'] 	= sendDataArrayUnit_Rawat;
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/apotek/lap_transaksi_perjenis_pasien_det/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/apotek/lap_transaksi_perjenis_pasien_det/preview");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Transaksi Per Jenis Pasien (Detail)',
			fbar:[
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Preview',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Close',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Tanggal',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield(),
								Q().display({value:'s/d'}),
								$this.DateField.endDate=Q().datefield()
							]
						}),
						Q().input({
							xWidth:100,
							separator:'',
							items:[
								Q().display({value:'Shift 1'}),
								$this.CheckBox.shift1=Q().checkbox({checked:true}),
								Q().display({value:'Shift 2'}),
								$this.CheckBox.shift2=Q().checkbox({checked:true}),
								Q().display({value:'Shift 3'}),
								$this.CheckBox.shift3=Q().checkbox({checked:true})
							]
						}),
						Q().input({
							label:'Unit',
							items:[
								$this.DropDown.unit=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						}),
						Q().input({
							label:'Operator',
							width: 350,
							items:[
								$this.DropDown.operator=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						}),
						/* Q().input({
							label:'Unit Rawat',
							items:[
								$this.DropDown.unitRawat=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'},
										{id:3,text:'Inst. Gawat Darurat'},
										{id:1,text:'Rawat Inap'},
										{id:2,text:'Rawat Jalan'}
									]
								})
							]
						}),
						Q().input({
							label	:'Jenis Pasien',
							width	:350,
							items	:[
								$this.DropDown.jenisPasien=Q().autocomplete({
									width	: 200,
									insert	: function(o){
										return {
											id		:o.id,
											text 	: o.text,
											display		:  '<table style="font-size: 11px;"><tr><td width="50">'+o.id+'</td><td width="300"> - '+o.text+'</td></tr></table>'
										}
									},
									success:function(res){return res.data},
									url			: baseURL + "index.php/apotek/lap_transaksi_perjenis_pasien_det/getCustomer",
									keyField	: 'id',
									valueField	: 'text',
									displayField: 'display',
									listWidth	: 350
								})
							]
						}),
						Q().input({
							label	:'Jenis Pasien',
							width	:350,
							items	:[
								$this.DropDown.jenisPasien=Q().autocomplete({
									width	: 200,
									insert	: function(o){
										return {
											id		:o.id,
											text 	: o.text,
											display		:  '<table style="font-size: 11px;"><tr><td width="50">'+o.id+'</td><td width="300"> - '+o.text+'</td></tr></table>'
										}
									},
									success:function(res){return res.data},
									url			: baseURL + "index.php/apotek/lap_transaksi_perjenis_pasien_det/getCustomer",
									keyField	: 'id',
									valueField	: 'text',
									displayField: 'display',
									listWidth	: 350
								})
							]
						}),
					 */
						Q().input({
							label	:'Unit Rawat',
							width	:350,
							items:[
									{
									   xtype: 'checkbox',
									   id: 'CekLapPilihSemuaUnitRawat',
									   hideLabel:true,
									   labelSeparator: '',
									   boxLabel: 'Pilih Semua',
									   checked: false,
									   listeners: 
									   {
											check: function()
											{
											   if(Ext.getCmp('CekLapPilihSemuaUnitRawat').getValue()===true)
												{
													$this.Grid.unit1.getSelectionModel().selectAll();
												}
												else
												{
													$this.Grid.unit1.getSelectionModel().clearSelections();
												}
											}
									   }
									},
								]
						}),
						{
							layout:'hbox',
							border: false,
							items:[
								$this.Grid.unit1=new Ext.grid.GridPanel({
						            ddGroup          : 'secondGridDDGroup',
						            store            : $this.ArrayStore.unit1,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            flex			: 1,
						            height           : 100,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : '',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                sortable: true,
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                sortable: true,
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit1.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'firstGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
				                                    $this.Grid.unit1.store.add(records);
				                                    $this.Grid.unit1.store.sort('KD_UNIT', 'ASC');
				                                    return true
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        }),
						        $this.Grid.unit2=new Ext.grid.GridPanel({
						            ddGroup          : 'firstGridDDGroup',
						            store            : $this.ArrayStore.unit2,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            style:'margin-left:-1px;',
						            flex			: 1,
						            height           : 100,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : '',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit2.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'secondGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    if((Q($this.ArrayStore.unit2).size()+records.length)<=8){
					                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
					                                    $this.Grid.unit2.store.add(records);
					                                    $this.Grid.unit2.store.sort('text', 'ASC');
					                                    return true
				                                    }else if((Q($this.ArrayStore.unit2).size()+records.length)>8){
				                                    	if(Q($this.ArrayStore.unit2).size()<8){
				                                    		var sisa=8-Q($this.ArrayStore.unit2).size();
				                                    		var a=[];
				                                    		for(var i=0; i<sisa; i++){
				                                    			a.push(records[i].data);
				                                    		}
				                                    		Ext.each(a, ddSource.grid.store.remove, ddSource.grid.store);
						                                    Q($this.ArrayStore.unit2).add(a);
						                                    $this.Grid.unit2.store.sort('text', 'ASC');
						                                    Ext.Msg.alert('Informasi','List tidak boleh lebih dari 8');
						                                    return true
				                                    	}else{
				                                    		Ext.Msg.alert('Informasi','List tidak boleh lebih dari 8');
				                                    		return false;
				                                    	}
				                                    }
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        })
							]
						},
						Q().input({
							label	:'Jenis Pasien',
							width	:350,
							items:[
									{
									   xtype: 'checkbox',
									   id: 'CekLapPilihSemuaJenisPasien',
									   hideLabel:true,
									   labelSeparator: '',
									   boxLabel: 'Pilih Semua',
									   checked: false,
									   listeners: 
									   {
											check: function()
											{
											   if(Ext.getCmp('CekLapPilihSemuaJenisPasien').getValue()===true)
												{
													$this.Grid.unit3.getSelectionModel().selectAll();
												}
												else
												{
													$this.Grid.unit3.getSelectionModel().clearSelections();
												}
											}
									   }
									},
								]
						}),
						{
							layout:'hbox',
							border: false,
							items:[
								$this.Grid.unit3=new Ext.grid.GridPanel({
						            ddGroup          : 'secondGridDDGroup',
						            store            : $this.ArrayStore.unit3,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            flex			: 1,
						            height           : 150,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : '',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                sortable: true,
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                sortable: true,
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit3.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'firstGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
				                                    $this.Grid.unit3.store.add(records);
				                                    $this.Grid.unit3.store.sort('KD_UNIT', 'ASC');
				                                    return true
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        }),
						        $this.Grid.unit4=new Ext.grid.GridPanel({
						            ddGroup          : 'firstGridDDGroup',
						            store            : $this.ArrayStore.unit4,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            style:'margin-left:-1px;',
						            flex			: 1,
						            height           : 150,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : '',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit4.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'secondGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    if((Q($this.ArrayStore.unit4).size()+records.length)<=8){
					                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
					                                    $this.Grid.unit4.store.add(records);
					                                    $this.Grid.unit4.store.sort('text', 'ASC');
					                                    return true
				                                    }else if((Q($this.ArrayStore.unit4).size()+records.length)>8){
				                                    	if(Q($this.ArrayStore.unit4).size()<8){
				                                    		var sisa=8-Q($this.ArrayStore.unit4).size();
				                                    		var a=[];
				                                    		for(var i=0; i<sisa; i++){
				                                    			a.push(records[i].data);
				                                    		}
				                                    		Ext.each(a, ddSource.grid.store.remove, ddSource.grid.store);
						                                    Q($this.ArrayStore.unit4).add(a);
						                                    $this.Grid.unit4.store.sort('text', 'ASC');
						                                    Ext.Msg.alert('Informasi','List tidak boleh lebih dari 8');
						                                    return true
				                                    	}else{
				                                    		Ext.Msg.alert('Informasi','List tidak boleh lebih dari 8');
				                                    		return false;
				                                    	}
				                                    }
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        })
							]
						},
					]
				})
			]
		});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/lap_transaksi_perjenis_pasien_det/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.show();
					Q($this.DropDown.unit).add(r.data.unit);
					Q($this.DropDown.operator).add(r.data.user);
					Q($this.ArrayStore.unit1).add(r.data.unit_rawat);
					Q($this.ArrayStore.unit3).add(r.data.customer);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}
}
DlgLapTransaksiPerJenisPasienDetail.init();