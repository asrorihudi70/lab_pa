var excel=false;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapKasirDetail;
var selectNamaLapKasirDetail;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapKasirDetail;
var varLapKasirDetail= ShowFormLapKasirDetail();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var pilihshift=false;
function ShowFormLapKasirDetail()
{
    frmDlgLapKasirDetail= fnDlgLapKasirDetail();
    frmDlgLapKasirDetail.show();
};

function fnDlgLapKasirDetail()
{
    var winLapKasirDetailReport = new Ext.Window
    (
        {
            id: 'winLapKasirDetailReport',
            title: 'Laporan Kasir (Detail)',
            closeAction: 'destroy',
            width: 405,
            height: 310,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapKasirDetail()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapKasirDetailReport;
};


function ItemDlgLapKasirDetail()
{
    var PnlLapKasirDetail = new Ext.Panel
    (
        {
            id: 'PnlLapKasirDetail',
            fileUpload: true,
            layout: 'form',
            height: '490',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapKasirDetail_Tanggal(),
                getItemLapKasirDetail_Batas(),
                getItemLapKasirDetail_Shift(),
				getItemLapKasirDetail_Batas(),
				getItemLapKasirDetail_Unit(),
                {
                    layout: 'absolute',
                    border: false,
					height:30,
					width:380,
                    items:
                    [
                        {
							x:140,
							y:8,
                            xtype: 'button',
                            text: 'Preview',
                            width: 70,
                            hideLabel: true,
                            id: 'btnPreviewLapKasirDetail',
                            handler: function()
                            {
                                if (ValidasiReportLapKasirDetail() === 1)
                                {
									var criteria = GetCriteriaLapKasirDetail();
									var params={
											criteria:criteria,
											excel:excel
									} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/apotek/lap_kasir_detail/preview");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();
									frmDlgLapKasirDetail.close();
									
                                };
                            }
                        },
						{
							x:230,
							y:8,
                            xtype: 'button',
                            text: 'Print',
                            width: 70,
                            hideLabel: true,
                            id: 'btnPrintLapKasirDetail',
                            handler: function()
                            {
                                if (ValidasiReportLapKasirDetail() === 1)
                                {
									var criteria = GetCriteriaLapKasirDetail();
									var params={
											criteria:criteria
									} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/apotek/lap_kasir_detail/print_direct");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									frmDlgLapKasirDetail.close();
                                };
                            }
                        }, 
                        {
							x:310,
							y:8,
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapKasirDetail',
                            handler: function()
                            {
                                    frmDlgLapKasirDetail.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapKasirDetail;
};

function GetCriteriaLapKasirDetail()
{
	var strKriteria = '';
	
	strKriteria = 'Tanggal';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterHasilLab').getValue();
	strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterHasilLab').getValue();
	
	if (Ext.get('cboPilihanLapKasirDetail').getValue() !== '' || Ext.get('cboPilihanLapKasirDetail').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'asal pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanLapKasirDetail').getValue();
	}
	if (Ext.get('cboUnitFarLapKasirDetail').getValue() !== '' || Ext.get('cboUnitFarLapKasirDetail').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + Ext.get('cboUnitFarLapKasirDetail').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitFarLapKasirDetail').getValue();
	}
	if (Ext.get('cboUserRequestEntryLapKasirDetail').getValue() !== ''|| Ext.get('cboUserRequestEntryLapKasirDetail').getValue() !== 'Pilih User...'){
		strKriteria += '##@@##' + 'operator';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryLapKasirDetail').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryLapKasirDetail').getValue();
	}
	
	if (Ext.getCmp('Shift_All_LapKasirDetail').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
		
	}else{
		if (Ext.getCmp('Shift_1_LapKasirDetail').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
			
		}
		if (Ext.getCmp('Shift_2_LapKasirDetail').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
			
		}
		if (Ext.getCmp('Shift_3_LapKasirDetail').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
			
		}
	}
	
	return strKriteria;
};

function ValidasiReportLapKasirDetail()
{
	var x=1;
	if(Ext.get('cboUnitFarLapKasirDetail').getValue() === ''|| Ext.get('cboUnitFarLapKasirDetail').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapKasirDetailReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapKasirDetail').getValue() === '' || Ext.get('cboUserRequestEntryLapKasirDetail').getValue() == 'Pilih User...'){
		ShowPesanWarningLapKasirDetailReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapKasirDetailReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }
	if(pilihshift == false){
		ShowPesanWarningLapKasirDetailReport('Shift belum dipilih!','Warning');
        x=0;
	}
    return x;
};



function getItemLapKasirDetail_Unit()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  380,
            height: 125,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapKasirDetail(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Unit '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            mComboUnitLapKasirDetail(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
			mComboUserLapKasirDetail(),
			{
				x: 10,
                y: 100,
                xtype: 'label',
                text: 'Tipe File '
			},
			{
				x: 110,
                y: 100,
                xtype: 'label',
                text: ' : '
			},
			{
				x:120,
				y:100,
				xtype: 'checkbox',
				id: 'CekLapExcelKasirDetail',
				hideLabel:false,
				boxLabel: 'Excel',
				checked: false,
				listeners: 
				{
					check: function()
					{
					   if(Ext.getCmp('CekLapExcelKasirDetail').getValue()===true)
						{
							excel=true;
						}else{
							excel=false;
						}
					}
				}
			},
           ]
        }]
    };
    return items;
};


function getItemLapKasirDetail_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  380,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapKasirDetail_Shift()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  380,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
					{
						x: 60,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapKasirDetail',
						id : 'Shift_All_LapKasirDetail',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapKasirDetail').setValue(true);
								Ext.getCmp('Shift_2_LapKasirDetail').setValue(true);
								Ext.getCmp('Shift_3_LapKasirDetail').setValue(true);
								Ext.getCmp('Shift_1_LapKasirDetail').disable();
								Ext.getCmp('Shift_2_LapKasirDetail').disable();
								Ext.getCmp('Shift_3_LapKasirDetail').disable();
								pilihshift=true;
							}else{
								Ext.getCmp('Shift_1_LapKasirDetail').setValue(false);
								Ext.getCmp('Shift_2_LapKasirDetail').setValue(false);
								Ext.getCmp('Shift_3_LapKasirDetail').setValue(false);
								Ext.getCmp('Shift_1_LapKasirDetail').enable();
								Ext.getCmp('Shift_2_LapKasirDetail').enable();
								Ext.getCmp('Shift_3_LapKasirDetail').enable();
							}
						}
					},
					{
						x: 130,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapKasirDetail',
						id : 'Shift_1_LapKasirDetail',
						handler: function (field, value) {
							if (value === true){
								pilihshift=true;
							}
						}
					},
					{
						x: 200,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapKasirDetail',
						id : 'Shift_2_LapKasirDetail',
						handler: function (field, value) {
							if (value === true){
								pilihshift=true;
							}
						}
					},
					{
						x: 270,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapKasirDetail',
						id : 'Shift_3_LapKasirDetail',
						handler: function (field, value) {
							if (value === true){
								pilihshift=true;
							}
						}
					}
				]
            }
        ]
            
    };
    return items;
};

function getItemLapKasirDetail_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  380,
            height: 45,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Tanggal'
            }, 
			{
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			{
                x: 120,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilLab',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 235,
                y: 10,
                xtype: 'label',
                text: ' s/d '
            }, 
			{
                x: 265,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function mComboPilihanLapKasirDetail()
{
    var cboPilihanLapKasirDetail = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapKasirDetail',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapKasirDetail;
};

function mComboUnitLapKasirDetail()
{
	var Field = ['KD_UNIT_FAR','NM_UNIT_FAR'];
    dsUnitFarLapKasirDetail = new WebApp.DataStore({fields: Field});
    dsUnitFarLapKasirDetail.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboUnitFar',
			    param: " kd_unit_far <>'' order by urut,nm_unit_far"
			}
		}
	);
    var cboUnitFarLapKasirDetail = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 40,
                id:'cboUnitFarLapKasirDetail',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsUnitFarLapKasirDetail,
                valueField: 'KD_UNIT_FAR',
                displayField: 'NM_UNIT_FAR',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboUnitFarLapKasirDetail;
};

function mComboUserLapKasirDetail()
{
    var Field = ['KD_USER','FULL_NAME'];
    ds_User_viDaftar = new WebApp.DataStore({fields: Field});

	ds_User_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: '',
                    Sortdir: '',
                    target:'ViewComboUser',
                    param: ""
                }
            }
        );

    var cboUserRequestEntryLapKasirDetail = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 70,
            id: 'cboUserRequestEntryLapKasirDetail',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:'Semua',
            store: ds_User_viDaftar,
            valueField: 'KD_USER',
            displayField: 'FULL_NAME',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.KD_USER;
				} 
			}
        }
    )

    return cboUserRequestEntryLapKasirDetail;
};

function ShowPesanWarningLapKasirDetailReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
