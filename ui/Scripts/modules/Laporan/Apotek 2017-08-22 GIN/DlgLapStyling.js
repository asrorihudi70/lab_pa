
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapStyling;
var selectNamaLapStyling;
var now = new Date();
var selectSetPerseorangan;
var frmLapStyling;
var varLapLapStyling= ShowFormLapLapStyling();
var tglAwal;
var tglAkhir;
var tipe;
var winLapStylingReport;
var cboUnitFar_LapStyling;
var print=true;
var excel=false;
var resep=false;
var retur=false;


function ShowFormLapLapStyling()
{
    frmLapStyling= fnLapStyling();
    frmLapStyling.show();
	loadDataComboKepemilikan_LapStyling();
};


function fnLapStyling()
{
    winLapStylingReport = new Ext.Window
    (
        {
            id: 'winLapStylingReport',
            title: 'Laporan Styling',
            closeAction: 'destroy',
            width: 420,
            height: 170,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapStyling()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{
					   xtype: 'checkbox',
					   id: 'CekLapPilihExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihExcel').getValue()===true)
								{
									excel =true;
								}
								else
								{
									excel =false;
								}
							}
					   }
					},
					{
					  xtype: 'label',
					  html: '&nbsp;'
					},
					{
						xtype: 'button',
						text: 'OK',
						width: 70,
						hideLabel: true,
						id: 'btnPreviewLapLapStyling',
						handler: function()
						{
							print=false;
							cetak();
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapStyling',
						handler: function()
						{
							winLapStylingReport.close();
						}
					}
			]

        }
    );

    return winLapStylingReport;
};


function ItemLapStyling()
{
    var PnlLapLapStyling = new Ext.Panel
    (
        {
            id: 'PnlLapLapStyling',
            fileUpload: true,
            layout: 'form',
            height: '150',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapStyling_Atas(),
            ]
        }
    );

    return PnlLapLapStyling;
};

function ValidasiReportLapStyling()
{
	var x=1;
	if(Ext.getCmp('cboKepemilikan_LapStyling').getValue() === ''){
		ShowPesanWarningLapStylingReport('Kepemilikan tidak boleh kosong','Warning');
        x=0;
	}
    return x;
};

function ShowPesanWarningLapStylingReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapStyling_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  395,
            height: 95,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Kepemilikan '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            mComboKepemilikanLapStyling(),
			
			{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode '
            }, 
			{
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 40,
				xtype: 'datefield',
				id: 'dtpTglAwalFilter_StokOpname',
				format: 'd/M/Y',
				value: now,
				width: 100
			}, 
			{
				x: 228,
				y: 40,
				xtype: 'label',
				text: ' s/d Bulan'
			}, 
			{
				x: 280,
				y: 40,
				xtype: 'datefield',
				id: 'dtpTglAkhirFilter_StokOpname',
				format: 'd/M/Y',
				value: now,
				width: 100
			},
			{
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Jenis Transaksi '
            }, 
			{
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
			{
			   x: 130,
               y: 70,
			   xtype: 'checkbox',
			   id: 'CekLapPilihResep',
			   hideLabel:false,
			   boxLabel: 'Resep',
			   checked: false,
			   listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihResep').getValue()===true)
						{
							resep =true;
						}
						else
						{
							resep =false;
						}
					}
			   }
			},
			/* {
			   x: 210,
               y: 70,
			   xtype: 'checkbox',
			   id: 'CekLapPilihRetur',
			   hideLabel:false,
			   boxLabel: 'Retur',
			   checked: false,
			   listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihRetur').getValue()===true)
						{
							retur =true;
						}
						else
						{
							retur =false;
						}
					}
			   }
			} */
            ]
        }]
    };
    return items;
};


function getItemLapLapStyling_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

var selectSetPilihan;

function loadDataComboKepemilikan_LapStyling(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/lap_styling/getKepemilikan",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboKepemilikan_LapStyling.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_Kepemilikan_LapStyling.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_Kepemilikan_LapStyling.add(recs);
				console.log(o);
			}
		}
	});
}


function mComboKepemilikanLapStyling()
{
	var Field = ['kd_milik','milik'];
    ds_Kepemilikan_LapStyling = new WebApp.DataStore({fields: Field});
    cboKepemilikan_LapStyling = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboKepemilikan_LapStyling',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:200,
                store: ds_Kepemilikan_LapStyling,
                valueField: 'kd_milik',
                displayField: 'milik',
                value:'SEMUA',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboKepemilikan_LapStyling;
};

function cetak(){
	if (ValidasiReportLapStyling() === 1)
	{
		var params={
			milik:Ext.getCmp('cboKepemilikan_LapStyling').getValue(),
			milik_nama:Ext.get('cboKepemilikan_LapStyling').getValue(),
			tglAwal:Ext.getCmp('dtpTglAwalFilter_StokOpname').getValue(),
			tglAkhir:Ext.getCmp('dtpTglAkhirFilter_StokOpname').getValue(),
			excel:excel,
			resep:resep,
			retur:retur
		} 
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		form.setAttribute("action", baseURL + "index.php/apotek/lap_styling/preview");
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();		
		//winLapStylingReport.close(); 
		
	};
}
