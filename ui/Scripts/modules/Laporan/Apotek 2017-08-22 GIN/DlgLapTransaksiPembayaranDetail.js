var print=true;
var excel =false;
var DlgLapTransaksiPembayaranDetail={
	ArrayStore:{
		unit1:Q().arraystore(),
		unit2:Q().arraystore(),
		payment1:Q().arraystore(),
		payment2:Q().arraystore()
	},
	CheckBox:{
		shift1:null,
		shift2:null,
		shift3:null,
		semuaunit:null,
		semuapayment:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	DropDown:{
		operator:null,
		unitRawat:null,
	},
	Grid:{
		unit1:null,
		unit2:null,
		payment1:null,
		payment2:null
	},
	Window:{
		main:null
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		
		var params={};
		var sendDataArrayUnit = [];
		$this.ArrayStore.unit2.each(function(record){
			var recordArrayUnit= [record.get("text")];
			sendDataArrayUnit.push(recordArrayUnit);
		});
		var sendDataArrayPayment = [];
		$this.ArrayStore.payment2.each(function(record){
			var recordArrayPayment= [record.get("text")];
			sendDataArrayPayment.push(recordArrayPayment);
		});
		
		params['operator']=$this.DropDown.operator.getValue();
		params['unit_rawat']=$this.DropDown.unitRawat.getValue();
		params['start_date']=Q($this.DateField.startDate).val();
		params['last_date']=Q($this.DateField.endDate).val();
		params['shift1']=$this.CheckBox.shift1.getValue();
		params['shift2']=$this.CheckBox.shift2.getValue();
		params['shift3']=$this.CheckBox.shift3.getValue();
		params['tmp_unit'] 	= sendDataArrayUnit;
		params['tmp_payment'] 	= sendDataArrayPayment;
		params['excel'] 	= excel;
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/apotek/lap_transaksipembayarandetail/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/apotek/lap_transaksipembayarandetail/doPrint");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Transaksi Per Pembayaran Detail',
			fbar:[
					{
					   xtype: 'checkbox',
					   id: 'CekLapPilihExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihExcel').getValue()===true)
								{
									excel =true;
								}
								else
								{
									excel =false;
								}
							}
					   }
				},
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Preview',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Close',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						{
							layout:'hbox',
							border: false,
							items:[
								$this.Grid.unit1=new Ext.grid.GridPanel({
						            ddGroup          : 'secondGridDDGroup',
						            store            : $this.ArrayStore.unit1,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            flex			: 1,
						            height           : 150,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : 'Unit',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                sortable: true,
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                sortable: true,
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit1.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'firstGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
				                                    $this.Grid.unit1.store.add(records);
				                                    $this.Grid.unit1.store.sort('KD_UNIT', 'ASC');
				                                    return true
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        }),
						        $this.Grid.unit2=new Ext.grid.GridPanel({
						            ddGroup          : 'firstGridDDGroup',
						            store            : $this.ArrayStore.unit2,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            style:'margin-left:-1px;',
						            flex			: 1,
						            height           : 150,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : 'Unit',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit2.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'secondGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    if((Q($this.ArrayStore.unit2).size()+records.length)<=8){
					                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
					                                    $this.Grid.unit2.store.add(records);
					                                    $this.Grid.unit2.store.sort('text', 'ASC');
					                                    return true
				                                    }else if((Q($this.ArrayStore.unit2).size()+records.length)>8){
				                                    	if(Q($this.ArrayStore.unit2).size()<8){
				                                    		var sisa=8-Q($this.ArrayStore.unit2).size();
				                                    		var a=[];
				                                    		for(var i=0; i<sisa; i++){
				                                    			a.push(records[i].data);
				                                    		}
				                                    		Ext.each(a, ddSource.grid.store.remove, ddSource.grid.store);
						                                    Q($this.ArrayStore.unit2).add(a);
						                                    $this.Grid.unit2.store.sort('text', 'ASC');
						                                    Ext.Msg.alert('Informasi','List tidak boleh lebih dari 8');
						                                    return true
				                                    	}else{
				                                    		Ext.Msg.alert('Informasi','List tidak boleh lebih dari 8');
				                                    		return false;
				                                    	}
				                                    }
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        })
							]
						},
						//
						{
							layout:'hbox',
							border: false,
							items:[
								$this.Grid.payment1=new Ext.grid.GridPanel({
						            ddGroup          : 'secondGridDDGroup',
						            store            : $this.ArrayStore.payment1,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            flex			: 1,
						            height           : 150,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : 'Payment',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                sortable: true,
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                sortable: true,
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.payment1.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'firstGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
				                                    $this.Grid.payment1.store.add(records);
				                                    $this.Grid.payment1.store.sort('KD_PAY', 'ASC');
				                                    return true
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        }),
						        $this.Grid.payment2=new Ext.grid.GridPanel({
						            ddGroup          : 'firstGridDDGroup',
						            store            : $this.ArrayStore.payment2,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            style			 :'margin-left:-1px;',
						            flex			 : 1,
						            height           : 150,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : 'Payment',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.payment2.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'secondGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
													Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
													$this.Grid.payment2.store.add(records);
													$this.Grid.payment2.store.sort('text', 'ASC');
													return true
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        })
							]
						},
						//
						Q().fieldset({
							items:[
								Q().input({
									label:'Pilih Semua',
									width: 350,
									items:[
										Q().display({value:'Unit'}),
										$this.CheckBox.semuaunit=Q().checkbox({checked:false,
											handler: function (field, value) 
											{
												if (value === true){
													$this.Grid.unit1.getSelectionModel().selectAll();
												}else{
													$this.Grid.unit1.getSelectionModel().clearSelections();
												}
											}
										}),
										Q().display({value:'Payment'}),
										$this.CheckBox.semuapayment=Q().checkbox({checked:false,
											handler: function (field, value) 
											{
												if (value === true){
													$this.Grid.payment1.getSelectionModel().selectAll();
												}else{
													$this.Grid.payment1.getSelectionModel().clearSelections();
												}
											}
										})
									]
								}),
								Q().input({
									label:'Operator',
									width: 350,
									items:[
										$this.DropDown.operator=Q().dropdown({
											width: 150,
											data:[
												{id:'',text:'Semua'}
											]
										})
									]
								}),
								Q().input({
									label:'Unit Rawat',
									items:[
										$this.DropDown.unitRawat=Q().dropdown({
											width: 150,
											data:[
												{id:'',text:'Semua'},
												{id:3,text:'Inst. Gawat Darurat'},
												{id:1,text:'Rawat Inap'},
												{id:2,text:'Rawat Jalan'}
											]
										})
									]
								}),
								Q().input({
									label:'Tanggal',
									items:[
										$this.DateField.startDate=Q().datefield(),
										Q().display({value:'s/d'})
									]
								}),
								Q().input({
									label:'',
									items:[
										$this.DateField.endDate=Q().datefield()
									]
								}),
								Q().input({
									xWidth:100,
									separator:'',
									items:[
										Q().display({value:'Shift 1'}),
										$this.CheckBox.shift1=Q().checkbox({checked:true}),
										Q().display({value:'Shift 2'}),
										$this.CheckBox.shift2=Q().checkbox({checked:true}),
										Q().display({value:'Shift 3'}),
										$this.CheckBox.shift3=Q().checkbox({checked:true})
									]
								})
							]
						})
					]
				})
			]
		});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/lap_transaksipembayarandetail/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.show();
					Q($this.ArrayStore.unit1).add(r.data.unit);
					Q($this.ArrayStore.payment1).add(r.data.payment);
					Q($this.DropDown.operator).add(r.data.user);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}
}
DlgLapTransaksiPembayaranDetail.init();