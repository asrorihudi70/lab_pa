var print=true;
var DlgLapJumlahLembarResep={
	CheckBox:{
		shift1:null,
		shift2:null,
		shift3:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	DropDown:{
		operator:null,
		unitRawat:null,
		dokter:null,
		unit:null,
		jenisPasien:null
	},
	Window:{
		main:null
	},
	GetCriteriaLapJumlahLembarResep:function()
	{
		$this=this;
		var strKriteria = '';
		
		strKriteria = 'Operator';
		strKriteria += '##@@##' + $this.DropDown.operator.getValue();
		
		strKriteria += '##@@##' + 'unit_rawat';
		strKriteria += '##@@##' + $this.DropDown.unitRawat.getValue();
		
		strKriteria += '##@@##' + 'unit';
		strKriteria += '##@@##' + $this.DropDown.unit.getValue();
		
		strKriteria += '##@@##' + 'dokter';
		strKriteria += '##@@##' + $this.DropDown.dokter.getValue();
		
		strKriteria += '##@@##' + 'start_date';
		strKriteria += '##@@##' + Q($this.DateField.startDate).val();
		
		strKriteria += '##@@##' + 'last_date';
		strKriteria += '##@@##' + Q($this.DateField.endDate).val();
		
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + $this.CheckBox.shift1.getValue();
		
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + $this.CheckBox.shift2.getValue();
		
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + $this.CheckBox.shift3.getValue();
		
		return strKriteria;
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
	
		/* $.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/main/cetaklaporanApotek/LapJumlahLembarResep",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		}); */
		loadMask.show();
		var criteria = $this.GetCriteriaLapJumlahLembarResep();
		// loadlaporanApotek('0', 'LapJumlahLembarResep', criteria, function(){
			// $this.Window.main.close();
			// loadMask.hide();
		// });
		var params={};
		params['criteria']=criteria;
		params['excel']=Ext.getCmp('checkboxExcel').getValue();
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/apotek/lap_JumlahLembarResep/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/apotek/lap_JumlahLembarResep/preview");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Jumlah Lembar Resep',
			fbar:[
				{	
				   xtype: 'checkbox',
				   id: 'checkboxExcel',
				   hideLabel:false,
				   boxLabel: 'Excel',
				   checked: false
				},
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Preview',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Close',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Tanggal',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield({
									width:100
								}),
								Q().display({value:'s/d',width:20}),
								$this.DateField.endDate=Q().datefield({
									width:100
								})
							]
						}),
						Q().input({
							xWidth:100,
							separator:'',
							items:[
								Q().display({value:'Shift 1'}),
								$this.CheckBox.shift1=Q().checkbox({checked:true}),
								Q().display({value:'Shift 2'}),
								$this.CheckBox.shift2=Q().checkbox({checked:true}),
								Q().display({value:'Shift 3'}),
								$this.CheckBox.shift3=Q().checkbox({checked:true})
							]
						}),
						Q().input({
							label:'Unit',
							items:[
								$this.DropDown.unit=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						}),
						Q().input({
							label:'Operator',
							width: 350,
							items:[
								$this.DropDown.operator=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						}),
						Q().input({
							label:'Unit Rawat',
							items:[
								$this.DropDown.unitRawat=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'},
										{id:3,text:'Inst. Gawat Darurat'},
										{id:1,text:'Rawat Inap'},
										{id:2,text:'Rawat Jalan'}
									]
								})
							]
						}),
						Q().input({
							label	:'Dokter',
							width	:350,
							items	:[
								$this.DropDown.dokter=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						})
					]
				})
			]
		});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/lap_JumlahLembarResep/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.show();
					Q($this.DropDown.unit).add(r.data.unit);
					Q($this.DropDown.operator).add(r.data.user);
					Q($this.DropDown.dokter).add(r.data.dokter);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}
}
DlgLapJumlahLembarResep.init();