var print = true;
var DlgLapObatKronis = {
	ArrayStore: {
		unit1: Q().arraystore(),
		unit2: Q().arraystore(),
		unit3: Q().arraystore(),
		unit4: Q().arraystore()
	},
	// CheckBox:{
	// 	shift1:null,
	// 	shift2:null,
	// 	shift3:null
	// },
	DateField: {
		startDate: null,
		endDate: null
	},
	DropDown: {
		operator: null,
		unitRawat: null,
		unit: null,
		jenisPasien: null
	},
	Grid: {
		unit1: null,
		unit2: null,
		unit3: null,
		unit4: null
	},
	Window: {
		main: null
	},
	GetCriteriaLapObatKronis: function () {
		$this = this;
		var strKriteria = '';

		//strKriteria = 'Operator';
		//strKriteria += '##@@##' + $this.DropDown.operator.getValue();

		// strKriteria += '##@@##' + 'unit_rawat';
		// strKriteria += '##@@##' + $this.DropDown.unitRawat.getValue();

		//strKriteria += '##@@##' + 'unit';
		//strKriteria += '##@@##' + $this.DropDown.unit.getValue();

		// strKriteria += '##@@##' + 'jenis_pasien';
		// strKriteria += '##@@##' + $this.DropDown.jenisPasien.getValue();

		strKriteria += '##@@##' + 'start_date';
		strKriteria += '##@@##' + Q($this.DateField.startDate).val();

		strKriteria += '##@@##' + 'last_date';
		strKriteria += '##@@##' + Q($this.DateField.endDate).val();

		//strKriteria += '##@@##' + 'shift1';
		//strKriteria += '##@@##' + $this.CheckBox.shift1.getValue();

		//strKriteria += '##@@##' + 'shift2';
		//strKriteria += '##@@##' + $this.CheckBox.shift2.getValue();

		//strKriteria += '##@@##' + 'shift3';
		//strKriteria += '##@@##' + $this.CheckBox.shift3.getValue();

		return strKriteria;
	},
	doPrint: function () {
		var $this = this;
		loadMask.show();

		/* $.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/main/cetaklaporanApotek/lapResepPasienPerFaktur",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		}); */
		loadMask.show();
		var criteria = $this.GetCriteriaLapObatKronis();
		// loadlaporanApotek('0', 'lapResepPasienPerFaktur', criteria, function(){
		// $this.Window.main.close();
		// loadMask.hide();
		// });
		var params = {};
		var sendDataArrayUnit_Rawat = [];
		$this.ArrayStore.unit2.each(function (record) {
			var recordArrayUnit_Rawat = [record.get("text")];
			sendDataArrayUnit_Rawat.push(recordArrayUnit_Rawat);
		});

		var sendDataArrayUnit_apotek = [];
		$this.ArrayStore.unit4.each(function (record) {
			var recordArrayUnit_JP = [record.get("text")];
			sendDataArrayUnit_apotek.push(recordArrayUnit_JP);
		});

		params['criteria'] = criteria;
		params['unit_apotek'] = sendDataArrayUnit_apotek;
		params['startDate'] = Q($this.DateField.startDate).val();
		params['endDate'] = Q($this.DateField.endDate).val();
		params['tmp_unit_rawat'] = sendDataArrayUnit_Rawat;
		params['excel'] = Ext.getCmp('CekLapExcel').getValue();
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");

		//alert(Ext.getCmp('cmbKategori_obat').getValue());

		if (Ext.getCmp('cmbKategori_obat').getValue() === 1) {
			// lap kronis
			form.setAttribute("action", baseURL + "index.php/apotek/lap_obat_kronis/preview");
		} else {
			// lap antibiotik
			form.setAttribute("action", baseURL + "index.php/apotek/lap_obat_kronis/preview2");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
	},
	init: function () {
		var $this = this;
		$this.Window.main = Q().window({
			title: 'Laporan Obat Kronis & Antibiotik',
			fbar: [
				new Ext.Button({
					text: 'Print',
					hidden: true,
					handler: function () {
						print = true;
						$this.doPrint()
					}
				}),
				{
					xtype: 'checkbox',
					id: 'CekLapExcel',
					hideLabel: true,
					labelSeparator: '',
					boxLabel: 'Excel',
					checked: false,
					listeners: {
						check: function () {
							//   if(Ext.getCmp('CekLapExcel').getValue()===true)
							// {
							// 	$this.Grid.unit1.getSelectionModel().selectAll();
							// }
							// else
							// {
							// 	$this.Grid.unit1.getSelectionModel().clearSelections();
							// }
						}
					}
				},
				new Ext.Button({
					text: 'Ok',
					handler: function () {

						if (Ext.getCmp('cmbKategori_obat').getValue() === 'Pilih') {
							Ext.Msg.alert('Informasi', 'Pilih Kategori Obat Terlebih Dahulu');
						} else {
							print = false;
							$this.doPrint()
						}


					}
				}),
				new Ext.Button({
					text: 'Close',
					handler: function () {
						$this.Window.main.close();
					}
				})
			],
			items: [
				Q().panel({
					border: true,
					width: 450,
					items: [
						Q().input({
							label: 'Tanggal',
							items: [
								$this.DateField.startDate = Q().datefield({
									width: 120,
								}),
								Q().display({
									value: 's/d'
								}),
							]
						}),
						Q().input({
							label: '',
							items: [
								$this.DateField.endDate = Q().datefield({
									width: 120,
								})
							]
						}),
						// Q().input({
						// 	separator:'',
						// 	items:[
						// 		Q().display({value:'Shift 1'}),
						// 		$this.CheckBox.shift1=Q().checkbox({checked:true}),
						// 		Q().display({value:'Shift 2'}),
						// 		$this.CheckBox.shift2=Q().checkbox({checked:true}),
						// 		Q().display({value:'Shift 3'}),
						// 		$this.CheckBox.shift3=Q().checkbox({checked:true})
						// 	]
						// }),
						// Q().input({
						// 	label:'Unit',
						// 	items:[
						// 		$this.DropDown.unit=Q().dropdown({
						// 			width: 150,
						// 			data:[
						// 				{id:'',text:'Semua'}
						// 			]
						// 		})
						// 	]
						// }),
						Q().input({
							label: 'Kategori Obat',
							width: 350,
							items: [{
								xtype: 'combo',
								fieldLabel: '',
								id: 'cmbKategori_obat',
								editable: true,
								store: new Ext.data.ArrayStore({
									id: 0,
									fields: [
										'Id',
										'displayText'
									],
									data: [
										[1, 'Obat Kronis'],
										[2, 'Obat Antibiotik']
									]
								}),
								displayField: 'displayText',
								valueField: 'Id',
								mode: 'local',
								width: 120,
								triggerAction: 'all',
								emptyText: 'Pilih Salah Satu...',
								selectOnFocus: true,
								tabIndex: 30,
								value: 'Pilih',
								enableKeyEvents: true,
								listeners: {

								}
							}]
						}),
						Q().input({
							label: 'Unit Rawat',
							width: 350,
							items: [{
								xtype: 'checkbox',
								id: 'CekLapPilihSemuaUnitRawat',
								hideLabel: true,
								labelSeparator: '',
								boxLabel: 'Pilih Semua',
								checked: false,
								listeners: {
									check: function () {
										if (Ext.getCmp('CekLapPilihSemuaUnitRawat').getValue() === true) {
											$this.Grid.unit1.getSelectionModel().selectAll();
										} else {
											$this.Grid.unit1.getSelectionModel().clearSelections();
										}
									}
								}
							}, ]
						}),
						{
							layout: 'hbox',
							border: false,
							items: [
								$this.Grid.unit1 = new Ext.grid.GridPanel({
									ddGroup: 'secondGridDDGroup',
									store: $this.ArrayStore.unit1,
									autoScroll: true,
									columnLines: true,
									border: true,
									enableDragDrop: true,
									flex: 1,
									height: 100,
									stripeRows: true,
									trackMouseOver: true,
									title: '',
									colModel: new Ext.grid.ColumnModel([{
										dataIndex: 'id',
										sortable: true,
										hidden: true
									}, {
										header: 'Nama',
										dataIndex: 'text',
										sortable: true,
										width: 50
									}]),
									listeners: {
										afterrender: function (comp) {
											var secondGridDropTargetEl = $this.Grid.unit1.getView().scroller.dom;
											var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
												ddGroup: 'firstGridDDGroup',
												notifyDrop: function (ddSource, e, data) {
													var records = ddSource.dragData.selections;
													Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
													$this.Grid.unit1.store.add(records);
													$this.Grid.unit1.store.sort('KD_UNIT', 'ASC');
													return true
												}
											});
										}
									},
									viewConfig: {
										forceFit: true
									}
								}),
								$this.Grid.unit2 = new Ext.grid.GridPanel({
									ddGroup: 'firstGridDDGroup',
									store: $this.ArrayStore.unit2,
									autoScroll: true,
									columnLines: true,
									border: true,
									enableDragDrop: true,
									style: 'margin-left:-1px;',
									flex: 1,
									height: 100,
									stripeRows: true,
									trackMouseOver: true,
									title: '',
									colModel: new Ext.grid.ColumnModel([{
										dataIndex: 'id',
										hidden: true
									}, {
										header: 'Nama',
										dataIndex: 'text',
										width: 50
									}]),
									listeners: {
										afterrender: function (comp) {
											var secondGridDropTargetEl = $this.Grid.unit2.getView().scroller.dom;
											var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
												ddGroup: 'secondGridDDGroup',
												notifyDrop: function (ddSource, e, data) {
													var records = ddSource.dragData.selections;
													if ((Q($this.ArrayStore.unit2).size() + records.length) <= 8) {
														Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
														$this.Grid.unit2.store.add(records);
														$this.Grid.unit2.store.sort('text', 'ASC');
														return true
													} else if ((Q($this.ArrayStore.unit2).size() + records.length) > 8) {
														if (Q($this.ArrayStore.unit2).size() < 8) {
															var sisa = 8 - Q($this.ArrayStore.unit2).size();
															var a = [];
															for (var i = 0; i < sisa; i++) {
																a.push(records[i].data);
															}
															Ext.each(a, ddSource.grid.store.remove, ddSource.grid.store);
															Q($this.ArrayStore.unit2).add(a);
															$this.Grid.unit2.store.sort('text', 'ASC');
															Ext.Msg.alert('Informasi', 'List tidak boleh lebih dari 8');
															return true
														} else {
															Ext.Msg.alert('Informasi', 'List tidak boleh lebih dari 8');
															return false;
														}
													}
												}
											});
										}
									},
									viewConfig: {
										forceFit: true
									}
								})
							]
						},
						Q().input({
							label: 'Unit Apotek',
							width: 350,
							items: [{
								xtype: 'checkbox',
								id: 'CekLapPilihSemuaJenisApotek',
								hideLabel: true,
								labelSeparator: '',
								boxLabel: 'Pilih Semua',
								checked: false,
								listeners: {
									check: function () {
										if (Ext.getCmp('CekLapPilihSemuaJenisApotek').getValue() === true) {
											$this.Grid.unit3.getSelectionModel().selectAll();
										} else {
											$this.Grid.unit3.getSelectionModel().clearSelections();
										}
									}
								}
							}, ]
						}),
						{
							layout: 'hbox',
							border: false,
							items: [
								$this.Grid.unit3 = new Ext.grid.GridPanel({
									ddGroup: 'secondGridDDGroup',
									store: $this.ArrayStore.unit3,
									autoScroll: true,
									columnLines: true,
									border: true,
									enableDragDrop: true,
									flex: 1,
									height: 150,
									stripeRows: true,
									trackMouseOver: true,
									title: '',
									colModel: new Ext.grid.ColumnModel([{
										dataIndex: 'id',
										sortable: true,
										hidden: true
									}, {
										header: 'Nama Unit',
										dataIndex: 'text',
										sortable: true,
										width: 50
									}]),
									listeners: {
										afterrender: function (comp) {
											var secondGridDropTargetEl = $this.Grid.unit3.getView().scroller.dom;
											var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
												ddGroup: 'firstGridDDGroup',
												notifyDrop: function (ddSource, e, data) {
													var records = ddSource.dragData.selections;
													Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
													$this.Grid.unit3.store.add(records);
													$this.Grid.unit3.store.sort('kd_unit_far', 'ASC');
													return true
												}
											});
										}
									},
									viewConfig: {
										forceFit: true
									}
								}),
								$this.Grid.unit4 = new Ext.grid.GridPanel({
									ddGroup: 'firstGridDDGroup',
									store: $this.ArrayStore.unit4,
									autoScroll: true,
									columnLines: true,
									border: true,
									enableDragDrop: true,
									style: 'margin-left:-1px;',
									flex: 1,
									height: 150,
									stripeRows: true,
									trackMouseOver: true,
									title: '',
									colModel: new Ext.grid.ColumnModel([{
										dataIndex: 'id',
										hidden: true
									}, {
										header: 'Nama Unit',
										dataIndex: 'text',
										width: 50
									}]),
									listeners: {
										afterrender: function (comp) {
											// var secondGridDropTargetEl = $this.Grid.unit4.getView().scroller.dom;
											// var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
											// 	ddGroup: 'secondGridDDGroup',
											// 	notifyDrop: function (ddSource, e, data) {
											// 		var records = ddSource.dragData.selections;
											// 		if ((Q($this.ArrayStore.unit4).size() + records.length) <= 8) {
											// 			Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											// 			$this.Grid.unit4.store.add(records);
											// 			$this.Grid.unit4.store.sort('text', 'ASC');
											// 			return true
											// 		} else if ((Q($this.ArrayStore.unit4).size() + records.length) > 8) {
											// 			if (Q($this.ArrayStore.unit4).size() < 8) {
											// 				var sisa = 8 - Q($this.ArrayStore.unit4).size();
											// 				var a = [];
											// 				for (var i = 0; i < sisa; i++) {
											// 					a.push(records[i].data);
											// 				}
											// 				Ext.each(a, ddSource.grid.store.remove, ddSource.grid.store);
											// 				Q($this.ArrayStore.unit4).add(a);
											// 				$this.Grid.unit4.store.sort('text', 'ASC');
											// 				Ext.Msg.alert('Informasi', 'List tidak boleh lebih dari 8');
											// 				return true
											// 			} else {
											// 				Ext.Msg.alert('Informasi', 'List tidak boleh lebih dari 8');
											// 				return false;
											// 			}
											// 		}
											// 	}
											// });

											// y
											var secondGridDropTargetEl = $this.Grid.unit4.getView().scroller.dom;
											var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
												ddGroup: 'secondGridDDGroup',
												notifyDrop: function (ddSource, e, data) {
													var records = ddSource.dragData.selections;
													if ((Q($this.ArrayStore.unit4).size() + records.length) > 0) {
														Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
														$this.Grid.unit4.store.add(records);
														$this.Grid.unit4.store.sort('text', 'ASC');
														return true
													}
												}
											});
											// end y
										}
									},
									viewConfig: {
										forceFit: true
									}
								})
							]
						},
					]
				})
			]
		});
		$.ajax({
			type: 'POST',
			dataType: 'JSON',
			url: baseURL + "index.php/apotek/lap_obat_kronis/getData",
			success: function (r) {
				loadMask.hide();
				if (r.result == 'SUCCESS') {
					$this.Window.main.show();
					//Q($this.DropDown.unit).add(r.data.unit);
					Q($this.DropDown.operator).add(r.data.user);
					Q($this.ArrayStore.unit1).add(r.data.unit_rawat);
					Q($this.ArrayStore.unit3).add(r.data.unit);
				} else {
					Ext.Msg.alert('Gagal', r.message);
				}
			},
			error: function (jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}
}
DlgLapObatKronis.init();