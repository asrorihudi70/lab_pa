
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapEvaluasiFarmasi;
var selectNamaLapEvaluasiFarmasi;
var now = new Date();
var selectSetPerseorangan;
var frmLapEvaluasiFarmasi;
var varLapLapEvaluasiFarmasi= ShowFormLapLapEvaluasiFarmasi();
var tglAwal;
var tglAkhir;
var tipe;
var winLapEvaluasiFarmasiReport;
var cboUnitFar_LapEvaluasiFarmasi;
var print=true;
var excel=false;
var resep=false;
var retur=false;


function ShowFormLapLapEvaluasiFarmasi()
{
    frmLapEvaluasiFarmasi= fnLapEvaluasiFarmasi();
    frmLapEvaluasiFarmasi.show();
	loadDataComboUnitFar_LapEvaluasiFarmasi();
};


function fnLapEvaluasiFarmasi()
{
    winLapEvaluasiFarmasiReport = new Ext.Window
    (
        {
            id: 'winLapEvaluasiFarmasiReport',
            title: 'Laporan Evaluasi Farmasi',
            closeAction: 'destroy',
            width: 420,
            height: 170,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapEvaluasiFarmasi()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{
					   xtype: 'checkbox',
					   id: 'CekLapPilihExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihExcel').getValue()===true)
								{
									excel =true;
								}
								else
								{
									excel =false;
								}
							}
					   }
					},
					{
					  xtype: 'label',
					  html: '&nbsp;'
					},
					{
						xtype: 'button',
						text: 'OK',
						width: 70,
						hideLabel: true,
						id: 'btnPreviewLapLapEvaluasiFarmasi',
						handler: function()
						{
							print=false;
							cetak();
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapEvaluasiFarmasi',
						handler: function()
						{
							winLapEvaluasiFarmasiReport.close();
						}
					}
			]

        }
    );

    return winLapEvaluasiFarmasiReport;
};


function ItemLapEvaluasiFarmasi()
{
    var PnlLapLapEvaluasiFarmasi = new Ext.Panel
    (
        {
            id: 'PnlLapLapEvaluasiFarmasi',
            fileUpload: true,
            layout: 'form',
            height: '150',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapEvaluasiFarmasi_Atas(),
            ]
        }
    );

    return PnlLapLapEvaluasiFarmasi;
};

function ValidasiReportLapEvaluasiFarmasi()
{
	var x=1;
	if(Ext.getCmp('cboUnitFar_LapEvaluasiFarmasi').getValue() === ''){
		ShowPesanWarningLapEvaluasiFarmasiReport('UnitFar tidak boleh kosong','Warning');
        x=0;
	}
    return x;
};

function ShowPesanWarningLapEvaluasiFarmasiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapEvaluasiFarmasi_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  395,
            height: 95,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 15,
                xtype: 'label',
                text: 'Unit Farmasi '
            }, {
                x: 110,
                y: 15,
                xtype: 'label',
                text: ' : '
            },
            mComboUnitFarmasiLapEvaluasiFarmasi(),
			
			{
                x: 10,
                y: 50,
                xtype: 'label',
                text: 'Periode '
            }, 
			{
                x: 110,
                y: 50,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 50,
				xtype: 'datefield',
				id: 'dtpTglAwalFilter_StokOpname',
				format: 'd/M/Y',
				value: now,
				width: 100
			}, 
			{
				x: 228,
				y: 50,
				xtype: 'label',
				text: ' s/d Bulan'
			}, 
			{
				x: 280,
				y: 50,
				xtype: 'datefield',
				id: 'dtpTglAkhirFilter_StokOpname',
				format: 'd/M/Y',
				value: now,
				width: 100
			},
			
            ]
        }]
    };
    return items;
};


function getItemLapLapEvaluasiFarmasi_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

var selectSetPilihan;

function loadDataComboUnitFar_LapEvaluasiFarmasi(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/lap_evaluasiFarmasi/getUnitFar",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUnitFar_LapEvaluasiFarmasi.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_UnitFar_LapEvaluasiFarmasi.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_UnitFar_LapEvaluasiFarmasi.add(recs);
				console.log(o);
			}
		}
	});
}


function mComboUnitFarmasiLapEvaluasiFarmasi()
{
	var Field = ['kd_unit_far','nm_unit_far'];
    ds_UnitFar_LapEvaluasiFarmasi = new WebApp.DataStore({fields: Field});
    cboUnitFar_LapEvaluasiFarmasi = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 15,
                id:'cboUnitFar_LapEvaluasiFarmasi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:200,
                store: ds_UnitFar_LapEvaluasiFarmasi,
                valueField: 'kd_unit_far',
                displayField: 'nm_unit_far',
                value:'SEMUA',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboUnitFar_LapEvaluasiFarmasi;
};

function cetak(){
	if (ValidasiReportLapEvaluasiFarmasi() === 1)
	{
		var params={
			kd_unit_far:Ext.getCmp('cboUnitFar_LapEvaluasiFarmasi').getValue(),
			nm_unit_far:Ext.get('cboUnitFar_LapEvaluasiFarmasi').getValue(),
			tglAwal:Ext.getCmp('dtpTglAwalFilter_StokOpname').getValue(),
			tglAkhir:Ext.getCmp('dtpTglAkhirFilter_StokOpname').getValue(),
			excel:excel
		} 
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		form.setAttribute("action", baseURL + "index.php/apotek/lap_evaluasiFarmasi/preview");
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();		
		//winLapEvaluasiFarmasiReport.close(); 
		
	};
}
