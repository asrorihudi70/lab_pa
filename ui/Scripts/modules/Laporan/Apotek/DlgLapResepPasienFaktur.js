var fields = [
	{name: 'kd_customer', mapping : 'kd_customer'},
	{name: 'customer', mapping : 'customer'}
];
var cols = [
	{ id : 'kd_customer', header: "Kode Customer", width: 160, sortable: true, dataIndex: 'kd_customer',hidden : true},
	{ header: "Customer", width: 50, sortable: true, dataIndex: 'customer'}
];
var print=true;
var DlgLapResepPasienFaktur={
	CheckBox:{
		shift1:null,
		shift2:null,
		shift3:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	DropDown:{
		operator:null,
		unitRawat:null,
		unit:null,
		jenisPasien:null
	},
	Window:{
		main:null
	},
	GetCriteriaLapResepPasienPerFaktur:function()
	{
		$this=this;
		var strKriteria = '';
		
		strKriteria = 'Operator';
		strKriteria += '##@@##' + $this.DropDown.operator.getValue();
		
		strKriteria += '##@@##' + 'unit_rawat';
		strKriteria += '##@@##' + $this.DropDown.unitRawat.getValue();
		
		strKriteria += '##@@##' + 'unit';
		strKriteria += '##@@##' + $this.DropDown.unit.getValue();
		
		strKriteria += '##@@##' + 'jenis_pasien';
		strKriteria += '##@@##' + $this.DropDown.jenisPasien.getValue();
		
		strKriteria += '##@@##' + 'start_date';
		strKriteria += '##@@##' + Q($this.DateField.startDate).val();
		
		strKriteria += '##@@##' + 'last_date';
		strKriteria += '##@@##' + Q($this.DateField.endDate).val();
		
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + $this.CheckBox.shift1.getValue();
		
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + $this.CheckBox.shift2.getValue();
		
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + $this.CheckBox.shift3.getValue();
		
		return strKriteria;
	},
	doPrint1:function(){

		var $this=this;
		loadMask.show();
	
		/* $.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/main/cetaklaporanApotek/lapResepPasienPerFaktur",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		}); */
		var sendDataArrayCustomer = [];
		secondGridStoreLapCustomer.each(function(record){
			var recordArrayCustomer= [record.get("kd_customer")];
			sendDataArrayCustomer.push(recordArrayCustomer);
		});
		if (sendDataArrayCustomer.length === 0){  
			this.messageBox('Peringatan','Isi customer unit dengan drag and drop','WARNING');
			loadMask.hide();
		}
		//var criteria = $this.GetCriteriaLapResepPasienPerFaktur();
		// loadlaporanApotek('0', 'lapResepPasienPerFaktur', criteria, function(){
			// $this.Window.main.close();
			// loadMask.hide();
		// });
		
		var params={};
		params['start_date'] = timestimetodate($this.DateField.startDate.getValue());
		params['last_date']  = timestimetodate($this.DateField.endDate.getValue());
		params['unit_far']   = $this.DropDown.unit.getValue();
		params['operator']   = $this.DropDown.operator.getValue();
		params['unit']       = $this.DropDown.unitRawat.getValue();
		params['shift1']=$this.CheckBox.shift1.getValue();
		params['shift2']=$this.CheckBox.shift2.getValue();
		params['shift3']=$this.CheckBox.shift3.getValue();
		params['kd_customer'] 	= sendDataArrayCustomer;
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			// form.setAttribute("action", baseURL + "index.php/apotek/lap_reseppasienfaktur/cetak/");
		} else{
			// form.setAttribute("action", baseURL + "index.php/apotek/lap_reseppasienfaktur/cetak/");
		}
		form.setAttribute("action", baseURL + "index.php/apotek/lap_reseppasienfaktur/cetak/"+print);
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
	},
	doPrint:function(){
		console.log('masuk');
		var $this=this;
		//loadMask.show();
	
		/* $.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/main/cetaklaporanApotek/lapResepPasienPerFaktur",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		}); */
		var sendDataArrayCustomer = [];
		secondGridStoreLapCustomer.each(function(record){
			var recordArrayCustomer= [record.get("kd_customer")];
			sendDataArrayCustomer.push(recordArrayCustomer);
		});
		console.log(sendDataArrayCustomer);
		if (sendDataArrayCustomer.length === 0){  
			ShowPesanWarning('Isi customer unit dengan drag and drop','WARNING');
			loadMask.hide();
		}else{
			var TglAwal = timestimetodate($this.DateField.startDate.getValue());
			var TglAkhir= timestimetodate($this.DateField.endDate.getValue());
			var TglAwalPisah =TglAwal.split('-');
			var TglAkhirPisah=TglAkhir.split('-');
			var TglNow= new Date();
			var TglAwalX=TglNow.setFullYear(TglAwalPisah[0],TglAwalPisah[1],TglAwalPisah[2]);
			var TglAkhirX=TglNow.setFullYear(TglAkhirPisah[0],TglAkhirPisah[1],TglAkhirPisah[2]);
			var hasil= (TglAkhirX - TglAwalX)/(60*60*24*1000);
			console.log(hasil);
			var params={};
			params['start_date'] = timestimetodate($this.DateField.startDate.getValue());
			params['last_date']  = timestimetodate($this.DateField.endDate.getValue());
			params['unit_far']   = $this.DropDown.unit.getValue();
			params['operator']   = $this.DropDown.operator.getValue();
			params['unit']       = $this.DropDown.unitRawat.getValue();
			params['shift1']=$this.CheckBox.shift1.getValue();
			params['shift2']=$this.CheckBox.shift2.getValue();
			params['shift3']=$this.CheckBox.shift3.getValue();
			params['kd_customer'] 	= sendDataArrayCustomer;
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("target", "_blank");
			if(hasil >= 31 ){
				if($this.DropDown.unitRawat.getValue()=="" && $this.DropDown.unit.getValue()==""){
					ShowPesanWarning('Penarikan Laporan Lebih Dari 1 Bulan (Maksimal 3 Bulan), Harap Memfilter Unit Dan Unit Rawat !','Peringatan');	
				}else if($this.DropDown.unitRawat.getValue()!="" || $this.DropDown.unit.getValue()!=""){
					console.log('1213');
					form.setAttribute("action", baseURL + "index.php/apotek/lap_reseppasienfaktur/cetak/"+print);
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", "data");
					hiddenField.setAttribute("value", Ext.encode(params));
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();
				}	
				
			}else{
				console.log('oadsid');
				form.setAttribute("action", baseURL + "index.php/apotek/lap_reseppasienfaktur/cetak/"+print);
				var hiddenField = document.createElement("input");
				hiddenField.setAttribute("type", "hidden");
				hiddenField.setAttribute("name", "data");
				hiddenField.setAttribute("value", Ext.encode(params));
				form.appendChild(hiddenField);
				document.body.appendChild(form);
				form.submit();
			}
		}
		
			/*form.setAttribute("action", baseURL + "index.php/apotek/lap_reseppasienfaktur/cetak/"+print);
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", "data");
			hiddenField.setAttribute("value", Ext.encode(params));
			form.appendChild(hiddenField);
			document.body.appendChild(form);
			form.submit();*/
			//loadMask.hide();
		
		
	},	
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Resep Pasien PerFaktur',
			fbar:[
				/*new Ext.Button({
					text:'Excel',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),*/
				new Ext.Button({
					text:'Excel',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Close',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Tanggal',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield({
									width:100
								}),
								Q().display({value:'s/d',width:20}),
								$this.DateField.endDate=Q().datefield({
									width:100
								})
							]
						}),
						Q().input({
							xWidth:100,
							separator:'',
							items:[
								Q().display({value:'Shift 1'}),
								$this.CheckBox.shift1=Q().checkbox({checked:true}),
								Q().display({value:'Shift 2'}),
								$this.CheckBox.shift2=Q().checkbox({checked:true}),
								Q().display({value:'Shift 3'}),
								$this.CheckBox.shift3=Q().checkbox({checked:true})
							]
						}),
						Q().input({
							label:'Unit',
							items:[
								$this.DropDown.unit=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						}),
						Q().input({
							label:'Operator',
							width: 350,
							items:[
								$this.DropDown.operator=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						}),
						Q().input({
							label:'Unit Rawat',
							items:[
								$this.DropDown.unitRawat=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'},
										{id:3,text:'Inst. Gawat Darurat'},
										{id:1,text:'Rawat Inap'},
										{id:2,text:'Rawat Jalan'}
									]
								})
							]
						}),
						{
							xtype : 'fieldset',
							title : 'Customer',
							layout:'column',
							border:true,
							height: 170,
							// bodyStyle:'margin-top: 2px',
							items:[
								{
									border:false,
									columnWidth:.45,
									// bodyStyle:'margin-top: 2px',
									items:[
										firstGridCustomer(),
									]
								},
								{
									border:false,
									columnWidth:.1,
									bodyStyle:'margin-top: 2px',
								},
								{
									border:false,
									columnWidth:.45,
									// bodyStyle:'margin-top: 2px; align:right;',
									items:[
										seconGridCustomer(),
									]
								}
							]
						},
						{
							// xtype : 'fieldset',
							// title : 'Kelompok pasien',
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							//height: 60,
							items:[
									{
										xtype : 'fieldset',
										title : '',
										layout: 'column',
										border: false,
										bodyStyle:'padding: 3px 0px  2px 10px',
										width:300,
										height: 50,
										items:[
											{	
												/*x:20,
												y:4,*/
												xtype: 'checkbox',
												id: 'CekLapPilihSemuaCustomerResepFaktur',
												hideLabel:false,
												boxLabel: 'Pilih Semua Customer',
												checked: false,
												listeners: 
												{
													check: function()
													{
													   if(Ext.getCmp('CekLapPilihSemuaCustomerResepFaktur').getValue()===true)
														{
															firstGrid.getSelectionModel().selectAll();
														}
														else
														{
															firstGrid.getSelectionModel().clearSelections();
														}
													}
											   }
											},				
										]
									}
							]
						},
						/*Q().input({
							label	:'Jenis Pasien',
							width	:350,
							items	:[
								$this.DropDown.jenisPasien=Q().autocomplete({
									width	: 200,
									insert	: function(o){
										return {
											id		:o.id,
											text 	: o.text,
											display		:  '<table style="font-size: 11px;"><tr><td width="50">'+o.id+'</td><td width="300"> - '+o.text+'</td></tr></table>'
										}
									},
									success:function(res){return res.data},
									url			: baseURL + "index.php/apotek/lap_reseppasienfaktur/getCustomer",
									keyField	: 'id',
									valueField	: 'text',
									displayField: 'display',
									listWidth	: 350
								})
							]
						})*/
					]
				})
			]
		});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/lap_reseppasienfaktur/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.show();
					Q($this.DropDown.unit).add(r.data.unit);
					Q($this.DropDown.operator).add(r.data.user);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
		



	}
}
DlgLapResepPasienFaktur.init();
	 function seconGridCustomer(){
		
		var secondGrid;
		secondGridStoreLapCustomer = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroup',
					store            : secondGridStoreLapCustomer,
					columns          : cols,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 130,
					stripeRows       : true,
					autoExpandColumn : 'kd_unit',
					title            : 'Customer yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroup',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGrid.store.add(records);
											secondGrid.store.sort('kd_unit', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGrid;
	};
	  function firstGridCustomer(){
		var dataSource_unit;
		var Field_poli_viDaftar = ['kd_customer','customer'];
		dataSource_unit         = new WebApp.DataStore({fields: Field_poli_viDaftar});
		Ext.Ajax.request({
			url: baseURL + "index.php/apotek/lap_reseppasienfaktur/getCustomer",
			params: '0',
			failure: function(o){
				var cst = Ext.decode(o.responseText);
			},	    
			success: function(o) {
				 firstGrid.store.removeAll();
				var cst = Ext.decode(o.responseText);

				for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
					var recs    = [],recType =  dataSource_unit.recordType;
					var o=cst['listData'][i];
			
					recs.push(new recType(o));
					dataSource_unit.add(recs);
					console.log(o);
				}
			}
		});
		/* dataSource_unit.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'NAMA_UNIT',
					Sortdir: 'ASC',
					target:'ViewSetupUnitB',
					param: "kd_bagian='2' and type_unit=false "
				}
			}
		); */
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 130,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'kd_customer',
                                                    dataIndex: 'kd_customer',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Customer',
                                                    dataIndex: 'customer',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
			listeners : {
				afterrender : function(comp) {
					var secondGridDropTargetEl = firstGrid.getView().scroller.dom;
					var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroup',
							notifyDrop : function(ddSource, e, data){
									var records =  ddSource.dragData.selections;
									Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
									firstGrid.store.add(records);
									firstGrid.store.sort('kd_unit_far', 'ASC');
									return true
							}
					});
				}
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid;
	};	
	function ShowPesanWarning(str, modul) {
	    Ext.MessageBox.show
		(
			{
			    title: modul,
			    msg: str,
			    buttons: Ext.MessageBox.OK,
			    icon: Ext.MessageBox.WARNING,
				width:250
			}
		);
	};	