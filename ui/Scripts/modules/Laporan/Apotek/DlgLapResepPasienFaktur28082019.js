var print=true;
var DlgLapResepPasienFaktur={
	CheckBox:{
		shift1:null,
		shift2:null,
		shift3:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	DropDown:{
		operator:null,
		unitRawat:null,
		unit:null,
		jenisPasien:null
	},
	Window:{
		main:null
	},
	GetCriteriaLapResepPasienPerFaktur:function()
	{
		$this=this;
		var strKriteria = '';
		
		strKriteria = 'Operator';
		strKriteria += '##@@##' + $this.DropDown.operator.getValue();
		
		strKriteria += '##@@##' + 'unit_rawat';
		strKriteria += '##@@##' + $this.DropDown.unitRawat.getValue();
		
		strKriteria += '##@@##' + 'unit';
		strKriteria += '##@@##' + $this.DropDown.unit.getValue();
		
		strKriteria += '##@@##' + 'jenis_pasien';
		strKriteria += '##@@##' + $this.DropDown.jenisPasien.getValue();
		
		strKriteria += '##@@##' + 'start_date';
		strKriteria += '##@@##' + Q($this.DateField.startDate).val();
		
		strKriteria += '##@@##' + 'last_date';
		strKriteria += '##@@##' + Q($this.DateField.endDate).val();
		
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + $this.CheckBox.shift1.getValue();
		
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + $this.CheckBox.shift2.getValue();
		
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + $this.CheckBox.shift3.getValue();
		
		return strKriteria;
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
	
		/* $.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/main/cetaklaporanApotek/lapResepPasienPerFaktur",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		}); */
		loadMask.show();
		var criteria = $this.GetCriteriaLapResepPasienPerFaktur();
		// loadlaporanApotek('0', 'lapResepPasienPerFaktur', criteria, function(){
			// $this.Window.main.close();
			// loadMask.hide();
		// });
		var params={};
		params['criteria']=criteria;
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			// form.setAttribute("action", baseURL + "index.php/apotek/lap_reseppasienfaktur/cetak/");
		} else{
			// form.setAttribute("action", baseURL + "index.php/apotek/lap_reseppasienfaktur/cetak/");
		}
		form.setAttribute("action", baseURL + "index.php/apotek/lap_reseppasienfaktur/cetak/"+print);
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Resep Pasien PerFaktur1',
			fbar:[
				new Ext.Button({
					text:'Excel',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'PDF',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Close',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Tanggal',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield({
									width:100
								}),
								Q().display({value:'s/d',width:20}),
								$this.DateField.endDate=Q().datefield({
									width:100
								})
							]
						}),
						Q().input({
							xWidth:100,
							separator:'',
							items:[
								Q().display({value:'Shift 1'}),
								$this.CheckBox.shift1=Q().checkbox({checked:true}),
								Q().display({value:'Shift 2'}),
								$this.CheckBox.shift2=Q().checkbox({checked:true}),
								Q().display({value:'Shift 3'}),
								$this.CheckBox.shift3=Q().checkbox({checked:true})
							]
						}),
						Q().input({
							label:'Unit',
							items:[
								$this.DropDown.unit=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						}),
						Q().input({
							label:'Operator',
							width: 350,
							items:[
								$this.DropDown.operator=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						}),
						Q().input({
							label:'Unit Rawat',
							items:[
								$this.DropDown.unitRawat=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'},
										{id:3,text:'Inst. Gawat Darurat'},
										{id:1,text:'Rawat Inap'},
										{id:2,text:'Rawat Jalan'}
									]
								})
							]
						}),
						Q().input({
							label	:'Jenis Pasien',
							width	:350,
							items	:[
								$this.DropDown.jenisPasien=Q().autocomplete({
									width	: 200,
									insert	: function(o){
										return {
											id		:o.id,
											text 	: o.text,
											display		:  '<table style="font-size: 11px;"><tr><td width="50">'+o.id+'</td><td width="300"> - '+o.text+'</td></tr></table>'
										}
									},
									success:function(res){return res.data},
									url			: baseURL + "index.php/apotek/lap_reseppasienfaktur/getCustomer",
									keyField	: 'id',
									valueField	: 'text',
									displayField: 'display',
									listWidth	: 350
								})
							]
						})
					]
				})
			]
		});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/lap_reseppasienfaktur/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.show();
					Q($this.DropDown.unit).add(r.data.unit);
					Q($this.DropDown.operator).add(r.data.user);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}
}
DlgLapResepPasienFaktur.init();