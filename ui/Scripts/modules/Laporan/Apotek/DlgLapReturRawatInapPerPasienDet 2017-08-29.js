var DlgLapReturRawatInapPerPasienDet={
	vars:{
		no:null,
		mComboPasienLapResepPasienRWI:null
	},
	Radio:{
		satu:null,
		dua:null,
		tiga:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	DropDown:{
		noMedrec:null,
		payment:null,
		detailBayar:null,
		unit:null,
		jenis:null,
		
	},
	TextField:{
		nama:null,
		kamar:null,
		kelas:null,
		tglMasuk:null,
		tglKeluar:null
	},
	Window:{
		main:null
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		if($this.Radio.satu.checked==true){
			params['cara']=1;
		}else if($this.Radio.dua.checked==true){
			params['cara']=2;
		}else{
			params['cara']=3;
		}
		params['kd_pasien']=$this.DropDown.noMedrec.getValue();
		params['tglawal']=Q($this.DateField.startDate).val();
		params['tglakhir']=Q($this.DateField.endDate).val();
		params['nama']=$this.TextField.nama.getValue();
		params['pembayaran']=$this.DropDown.payment.getValue();
		params['detail_bayar']=$this.DropDown.detailBayar.getValue();
		params['unit']=$this.DropDown.unit.getValue();
		params['jenis']=$this.DropDown.jenis.getValue();
		params['no']=$this.vars.no;
		/* params.push({name:'masuk',value:Q($this.TextField.tglMasuk).val()});
		params.push({name:'keluar',value:Q($this.TextField.tglKeluar).val()}); 
		params.push({name:'tglawal',value:Q($this.DateField.startDate).val()});
		params.push({name:'tglakhir',value:Q($this.DateField.endDate).val()});
		params.push({name:'nama',value:$this.TextField.nama.getValue()});
		params.push({name:'pembayaran',value:$this.DropDown.payment.getValue()});
		params.push({name:'detail_bayar',value:$this.DropDown.detailBayar.getValue()});
		params.push({name:'unit',value:$this.DropDown.unit.getValue()});
		params.push({name:'jenis',value:$this.DropDown.jenis.getValue()});
		params.push({name:'no',value:$this.vars.no});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/apotek/lap_returRWIpasiendetail/doPrint",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.close();
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});*/
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/apotek/lap_returRWIpasiendetail/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/apotek/lap_returRWIpasiendetail/preview");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
	},
	loadDataPasienLapResepPasienRWI:function(param){
		if (param==='' || param===undefined) {
			param={
				text: '0',
			};
		}
		Ext.Ajax.request({
			url: baseURL + "index.php/apotek/lap_returRWIpasiendetail/getPasien",
			params: {text:param},
			failure: function(o){
				var cst = Ext.decode(o.responseText);
			},	    
			success: function(o) {
				cboPasienLapResepPasienRWI.store.removeAll();
				var cst = Ext.decode(o.responseText);

				for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
					var recs    = [],recType = dsPasien_LapResepPasienRWI.recordType;
					var o=cst['listData'][i];
			
					recs.push(new recType(o));
					dsPasien_LapResepPasienRWI.add(recs);
					console.log(o);
				}
			}
		});
	},
	
/* 
	mComboPasienLapResepPasienRWI:function(){
		var $this=this;
		var Field = ['kd_pasien', 'nama', 'alamat', 'nama_unit', 'no_kamar', 'kd_unit', 'tgl_inap', 'tgl_keluar', 'no_transaksi'];
		dsPasien_LapResepPasienRWI = new WebApp.DataStore({fields: Field});
		$this.loadDataPasienLapResepPasienRWI();
		cboPasienLapResepPasienRWI = new Ext.form.ComboBox
		(
			{
				id: 'cboPasienLapResepPasienRWI',
				typeAhead: true,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'local',
				selectOnFocus:true,
				forceSelection: true,
				hideTrigger		: true,
				labelName:'No. Medrec',
				store: dsPasien_LapResepPasienRWI,
				valueField: 'kd_pasien',
				displayField: 'kd_pasien',
				width:150,
				listeners:
				{
					'select': function(a, b, c)
					{
						$this.TextField.nama.setValue(b.data.nama);
						$this.TextField.kelas.setValue(b.data.kelas);
						$this.TextField.kamar.setValue(b.data.no_kamar);
						$this.TextField.kelas.setValue(b.data.nama_unit);
						$this.vars.no=b.data.no_transaksi;
					},
					keyUp: function(a,b,c){
						
						if(  b.getKey()!=127 ){
							clearTimeout(this.time);
					
							this.time=setTimeout(function(){
								if(cboPasienLapResepPasienRWI.lastQuery != '' ){
									var value="";
									
									if (value!=cboPasienLapResepPasienRWI.lastQuery)
									{
										if (a.rendered && a.innerList != null) {
											a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
											a.restrictHeight();
											a.selectedIndex = 0;
										}
										a.expand();
										Ext.Ajax.request({
											url: baseURL + "index.php/apotek/lap_returRWIpasiendetail/getPasien",
											params: {text:cboPasienLapResepPasienRWI.lastQuery},
											failure: function(o){
												var cst = Ext.decode(o.responseText);
											},	    
											success: function(o) {
												cboPasienLapResepPasienRWI.store.removeAll();
												var cst = Ext.decode(o.responseText);

												for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
													var recs    = [],recType = dsPasien_LapResepPasienRWI.recordType;
													var o=cst['listData'][i];
											
													recs.push(new recType(o));
													dsPasien_LapResepPasienRWI.add(recs);
												}
												a.expand();
												if(dsPasien_LapResepPasienRWI.onShowList != undefined)
													dsPasien_LapResepPasienRWI.onShowList(cst[showVar]);
												if(cst['listData'].length>0){
														
													a.doQuery(a.allQuery, true);
													a.expand();
													a.selectText(value.length,value.length);
												}else{
												
														a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
														a.restrictHeight();
														a.selectedIndex = 0;
													
												}
											}
										});
										value=cboPasienLapResepPasienRWI.lastQuery;
									}
								}
							},1000);
						}
					} 
				}
			}
		)
		return cboPasienLapResepPasienRWI;
	}, */
	
	getSelect:function(jenis_pay){
		var $this=this;
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/lap_returRWIpasiendetail/getSelect",
			data:{jenis_pay:jenis_pay},
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.DropDown.detailBayar).reset();
					Q($this.DropDown.detailBayar).add({id:'',text:'Semua'});
					Q($this.DropDown.detailBayar).add(r.data);
					Q($this.DropDown.detailBayar).val('');
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Retur Rawat Inap Per Pasien Detail',
			fbar:[
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Preview',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Close',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().fieldset({
							title:'Info',
							items:[
							// Q().input({
									// label:'No. Medrec',
									// items:[
										// $this.mComboPasienLapResepPasienRWI(),
									// ]
								// }),
								
								 Q().input({
									label:'No. MedRec',
									items:[
										$this.DropDown.noMedrec=Q().autocomplete({
											width	: 200,
											select	:function(a,b){
												$this.TextField.nama.setValue(b.data.id);
												$this.TextField.kelas.setValue(b.data.kelas);
												$this.TextField.kamar.setValue(b.data.no_kamar);
												$this.TextField.kelas.setValue(b.data.nama_unit);
												//$this.TextField.tglMasuk.setValue(new Date(b.data.tgl_inap));
												// $this.TextField.tglKeluar.setValue(new Date(b.data.tgl_keluar));
												$this.vars.no=b.data.no_transaksi;
												// alert($this.vars.no);
												// console.log(b);
											},
											insert	: function(o){
												return {
													id			:o.id,//nama
													no_kamar 	: o.no_kamar,
													nama_unit 	: o.nama_unit,
													tgl_keluar 	: o.tgl_keluar,
													tgl_inap 	: o.tgl_inap,
													no_transaksi:o.no_transaksi,
													text 		: o.text,//no_medrec
													display		:  '<table style="font-size: 11px;"><tr><td width="80">'+o.text+'</td><td width="80">'+o.no_transaksi+'</td><td width="270"> - '+o.id+'</td></tr></table>'
												}
											},
											
											success:function(res){return res.data},
											url			: baseURL + "index.php/apotek/lap_returRWIpasiendetail/getPasien",
											keyField	: 'text',
											valueField	: 'text',
											displayField: 'display',
											listWidth	: 350
										})
									]
								}), 
								Q().input({
									label:'Nama Pasien',
									items:[
										$this.TextField.nama=new Ext.form.TextField({
											width: 200,disabled:true
										})
									]
								}),
								{
									layout:'column',
									border:false,
									width: 320,
									items:[
										Q().input({
											label:'Kamar',
											width:190,
											items:[
												$this.TextField.kamar=new Ext.form.TextField({
													width: 70,disabled:true
												})
											]
										}),
										Q().input({
											label:'Kelas',
											width:120,
											xWidth:40,
											items:[
												$this.TextField.kelas=new Ext.form.TextField({
													width: 70,disabled:true
												})
											]
										})
										
									]
								},{
									layout:'column',
									border:false,
									width: 360,
									items:[
										Q().input({
											label:'Tgl Resep',
											width: 350,
											items:[
												$this.DateField.startDate=Q().datefield(),
												Q().display({value:'s/d'}),
												$this.DateField.endDate=Q().datefield()
											]
										}),
										/* Q().input({
											label:'Tgl Resep',
											width:190,
											items:[
												$this.TextField.tglMasuk=Q().datefield({
													width: 70,disabled:false,value:''
												})
											]
										}),
										Q().input({
											label:'s/d',
											width:120,
											xWidth:40,
											items:[
												$this.TextField.tglKeluar=Q().datefield({
													width: 70,disabled:false,value:''
												})
											]
										}) */
										
									]
								}
							]
						}),
						Q().input({
							label:'Cara Pembayaran',
							width: 350,
							items:[
								new Ext.form.RadioGroup({
			                        xtype: 'radiogroup',
			                        fieldLabel: 'Choose your favorite',
			                        items: [
			                        	$this.Radio.satu=new Ext.form.Radio({
			                        		boxLabel: 'Pilih',
			                        		name: 'rb',
			                        		checked:true
			                        	}),
			                        	$this.Radio.dua=new Ext.form.Radio({
			                        		boxLabel: 'Transfer',
			                        		name: 'rb',
			                        		width: 90
			                        	}),
			                        	$this.Radio.tiga=new Ext.form.Radio({
			                        		boxLabel: 'Kredit',
			                        		name: 'rb',
			                        		width:60
			                        	})
			                        ],
			                        listeners:{
	                        			change:function(a,b){
	                        				if($this.Radio.satu.checked==true){
	                        					$this.DropDown.payment.enable();
	                        					$this.DropDown.detailBayar.enable();
	                        				}else{
	                        					$this.DropDown.payment.disable();
	                        					$this.DropDown.detailBayar.disable();
	                        				}
	                        			}
	                        		}
			                    })
							]
						}),
						Q().input({
							separator:'',
							items:[
								$this.DropDown.payment=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									],
									select:function(a){
										$this.getSelect(a.getValue());
									}
								})
							]
						}),
						Q().input({
							separator:'',
							items:[
								$this.DropDown.detailBayar=Q().dropdown({
									width: 200,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						}),
						Q().input({
							label:'Unit Apotek',
							items:[
								$this.DropDown.unit=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						}),
						Q().input({
							label:'Unit',
							items:[
								$this.DropDown.jenis=Q().dropdown({
									width: 150,
									data:[
										{id:'',text:'Semua'}
									]
								})
							]
						})
					]
				})
			]
		});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/lap_returRWIpasiendetail/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.show();
					Q($this.DropDown.payment).add(r.data.payment);
					Q($this.DropDown.unit).add(r.data.unit);
					Q($this.DropDown.jenis).add(r.data.jenis);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}
}
DlgLapReturRawatInapPerPasienDet.init();