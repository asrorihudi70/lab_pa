
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapStokOpname;
var selectNamaLapStokOpname;
var now = new Date();
var selectSetPerseorangan;
var frmLapStokOpname;
var varLapLapStokOpname= ShowFormLapLapStokOpname();
var tglAwal;
var tglAkhir;
var tipe;
var winLapStokOpnameReport;
var cboUnitFar_LapStokOpname;
var print=true;


function ShowFormLapLapStokOpname()
{
    frmLapStokOpname= fnLapStokOpname();
    frmLapStokOpname.show();
	loadDataComboUnitFar_LapStokOpname();
};


function fnLapStokOpname()
{
    winLapStokOpnameReport = new Ext.Window
    (
        {
            id: 'winLapStokOpnameReport',
            title: 'Laporan Stok Opname',
            closeAction: 'destroy',
            width: 420,
            height: 160,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapStokOpname()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{	
					   xtype: 'checkbox',
					   id: 'checkboxExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false
					},
					{
						xtype: 'button',
						text: 'Print',
						width: 70,
						hideLabel: true,
						id: 'btnPrintLapLapStokOpname',
						handler: function()
						{
							print=true;
							cetak();
						}
					},
					{
						xtype: 'button',
						text: 'Preview',
						width: 70,
						hideLabel: true,
						id: 'btnPreviewLapLapStokOpname',
						handler: function()
						{
							print=false;
							cetak();
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapStokOpname',
						handler: function()
						{
							winLapStokOpnameReport.close();
						}
					}
			]

        }
    );

    return winLapStokOpnameReport;
};


function ItemLapStokOpname()
{
    var PnlLapLapStokOpname = new Ext.Panel
    (
        {
            id: 'PnlLapLapStokOpname',
            fileUpload: true,
            layout: 'form',
            height: '150',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapStokOpname_Atas(),
            ]
        }
    );

    return PnlLapLapStokOpname;
};

function ValidasiReportLapStokOpname()
{
	var x=1;
	if(Ext.getCmp('cboUnitFar_LapStokOpname').getValue() === ''){
		ShowPesanWarningLapStokOpnameReport('Unit Far tidak boleh kosong','Warning');
        x=0;
	}
    return x;
};

function ShowPesanWarningLapStokOpnameReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapStokOpname_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  395,
            height: 75,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Unit Far '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            mComboUnitFarLapStokOpname(),
			
			{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode '
            }, 
			{
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 40,
				xtype: 'datefield',
				id: 'dtpTglAwalFilter_StokOpname',
				format: 'd/M/Y',
				value: now,
				width: 100
			}, 
			{
				x: 228,
				y: 40,
				xtype: 'label',
				text: ' s/d Bulan'
			}, 
			{
				x: 280,
				y: 40,
				xtype: 'datefield',
				id: 'dtpTglAkhirFilter_StokOpname',
				format: 'd/M/Y',
				value: now,
				width: 100
			},
            ]
        }]
    };
    return items;
};


function getItemLapLapStokOpname_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

var selectSetPilihan;

function loadDataComboUnitFar_LapStokOpname(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/lap_stokopname/getUnitFar",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUnitFar_LapStokOpname.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_UnitFar_LapStokOpname.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_UnitFar_LapStokOpname.add(recs);
				console.log(o);
			}
		}
	});
}


function mComboUnitFarLapStokOpname()
{
	var Field = ['kd_unit_far','nm_unit_far'];
    ds_UnitFar_LapStokOpname = new WebApp.DataStore({fields: Field});
    cboUnitFar_LapStokOpname = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboUnitFar_LapStokOpname',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:200,
                store: ds_UnitFar_LapStokOpname,
                valueField: 'kd_unit_far',
                displayField: 'nm_unit_far',
                value:'SEMUA',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboUnitFar_LapStokOpname;
};

function cetak(){
	if (ValidasiReportLapStokOpname() === 1)
	{
		var params={
			kd_unit_far:Ext.getCmp('cboUnitFar_LapStokOpname').getValue(),
			tglAwal:Ext.getCmp('dtpTglAwalFilter_StokOpname').getValue(),
			tglAkhir:Ext.getCmp('dtpTglAkhirFilter_StokOpname').getValue(),
			excel:Ext.getCmp('checkboxExcel').getValue(),
		} 

		console.log(Ext.getCmp('cboUnitFar_LapStokOpname').getValue());
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print==true){
			form.setAttribute("action", baseURL + "index.php/apotek/lap_stokopname/cetakStokOpname");
		}else{
			form.setAttribute("action", baseURL + "index.php/apotek/lap_stokopname/previewStokOpname");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();		
		//winLapStokOpnameReport.close(); 
		
	};
}
