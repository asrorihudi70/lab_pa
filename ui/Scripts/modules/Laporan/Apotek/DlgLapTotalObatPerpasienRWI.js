var DlgLapTotalObatPerpasienRWI = {
    vars: {
        no: null,
        mComboPasienLapResepPasienRWI: null
    },
    Radio: {
        satu: null,
        dua: null,
        tiga: null
    },
    ArrayStore: {
        kunjungan: new Ext.data.ArrayStore({ fields: [] }),
    },
    DateField: {
        startDate: null,
        endDate: null
    },
    DropDown: {
        noMedrec: null,
        payment: null,
        detailBayar: null,
        unit: null,
        jenis: null,

    },
    Grid: {
        kunjungan: null
    },
    TextField: {
        nama: null,
        kamar: null,
        kelas: null,
        tglMasuk: null,
        tglKeluar: null,
        no_medrec: null
    },
    Window: {
        main: null,
        lookup_kunjungan: null
    },
    doPrint: function () {
        var $this = this;
        loadMask.show();
        var params = {};
        // if ($this.Radio.satu.checked == true) {
        //     params['cara'] = 1;
        // } else if ($this.Radio.dua.checked == true) {
        //     params['cara'] = 2;
        // } else {
        //     params['cara'] = 3;
        // }
        params['kd_pasien'] = $this.TextField.no_medrec.getValue();
        params['tglawal'] = Q($this.DateField.startDate).val();
        params['tglakhir'] = Q($this.DateField.endDate).val();
        params['nama'] = $this.TextField.nama.getValue();
        params['kelpasien'] = $this.DropDown.kelpasien.getValue();
        // params['pembayaran'] = $this.DropDown.payment.getValue();
        // params['detail_bayar'] = $this.DropDown.detailBayar.getValue();
        // params['unit'] = $this.DropDown.unit.getValue();
        // params['jenis'] = $this.DropDown.jenis.getValue();
        params['no'] = $this.vars.no;
        params['excel'] = Ext.getCmp('checkboxExcel').getValue();
        /* params.push({name:'masuk',value:Q($this.TextField.tglMasuk).val()});
        params.push({name:'keluar',value:Q($this.TextField.tglKeluar).val()}); 
        params.push({name:'tglawal',value:Q($this.DateField.startDate).val()});
        params.push({name:'tglakhir',value:Q($this.DateField.endDate).val()});
        params.push({name:'nama',value:$this.TextField.nama.getValue()});
        params.push({name:'pembayaran',value:$this.DropDown.payment.getValue()});
        params.push({name:'detail_bayar',value:$this.DropDown.detailBayar.getValue()});
        params.push({name:'unit',value:$this.DropDown.unit.getValue()});
        params.push({name:'jenis',value:$this.DropDown.jenis.getValue()});
        params.push({name:'no',value:$this.vars.no});
        $.ajax({
            type: 'POST',
            dataType:'JSON',
            data:params,
            url:baseURL + "index.php/apotek/lap_totalobatpasienrwi/doPrint",
            success: function(r){
                loadMask.hide();
                if(r.result=='SUCCESS'){
                    $this.Window.main.close();
                    window.open(r.data, '_blank', 'location=0,resizable=1', false);
                }else{
                    Ext.Msg.alert('Gagal',r.message);
                }
            },
            error: function(jqXHR, exception) {
                loadMask.hide();
                Nci.ajax.ErrorMessage(jqXHR, exception);
            }
        });*/
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("target", "_blank");
        form.setAttribute("action", baseURL + "index.php/apotek/lap_totalobatpasienrwi/preview");
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "data");
        hiddenField.setAttribute("value", Ext.encode(params));
        form.appendChild(hiddenField);
        document.body.appendChild(form);
        form.submit();
        loadMask.hide();
    },
    loadDataPasienLapResepPasienRWI: function (param) {
        if (param === '' || param === undefined) {
            param = {
                text: '0',
            };
        }
        Ext.Ajax.request({
            url: baseURL + "index.php/apotek/lap_totalobatpasienrwi/getPasien",
            params: { text: param },
            failure: function (o) {
                var cst = Ext.decode(o.responseText);
            },
            success: function (o) {
                cboPasienLapResepPasienRWI.store.removeAll();
                var cst = Ext.decode(o.responseText);

                for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
                    var recs = [], recType = dsPasien_LapTotalObatPasienRWI.recordType;
                    var o = cst['listData'][i];

                    recs.push(new recType(o));
                    dsPasien_LapTotalObatPasienRWI.add(recs);
                    console.log(o);
                }
            }
        });
    },

    /* 
        mComboPasienLapResepPasienRWI:function(){
            var $this=this;
            var Field = ['kd_pasien', 'nama', 'alamat', 'nama_unit', 'no_kamar', 'kd_unit', 'tgl_inap', 'tgl_keluar', 'no_transaksi'];
            dsPasien_LapTotalObatPasienRWI = new WebApp.DataStore({fields: Field});
            $this.loadDataPasienLapResepPasienRWI();
            cboPasienLapResepPasienRWI = new Ext.form.ComboBox
            (
                {
                    id: 'cboPasienLapResepPasienRWI',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
                    hideTrigger		: true,
                    labelName:'No. Medrec',
                    store: dsPasien_LapTotalObatPasienRWI,
                    valueField: 'kd_pasien',
                    displayField: 'kd_pasien',
                    width:150,
                    listeners:
                    {
                        'select': function(a, b, c)
                        {
                            $this.TextField.nama.setValue(b.data.nama);
                            $this.TextField.kelas.setValue(b.data.kelas);
                            $this.TextField.kamar.setValue(b.data.no_kamar);
                            $this.TextField.kelas.setValue(b.data.nama_unit);
                            $this.vars.no=b.data.no_transaksi;
                        },
                        keyUp: function(a,b,c){
                        	
                            if(  b.getKey()!=127 ){
                                clearTimeout(this.time);
                    	
                                this.time=setTimeout(function(){
                                    if(cboPasienLapResepPasienRWI.lastQuery != '' ){
                                        var value="";
                                    	
                                        if (value!=cboPasienLapResepPasienRWI.lastQuery)
                                        {
                                            if (a.rendered && a.innerList != null) {
                                                a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
                                                a.restrictHeight();
                                                a.selectedIndex = 0;
                                            }
                                            a.expand();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/apotek/lap_totalobatpasienrwi/getPasien",
                                                params: {text:cboPasienLapResepPasienRWI.lastQuery},
                                                failure: function(o){
                                                    var cst = Ext.decode(o.responseText);
                                                },	    
                                                success: function(o) {
                                                    cboPasienLapResepPasienRWI.store.removeAll();
                                                    var cst = Ext.decode(o.responseText);
    
                                                    for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                                                        var recs    = [],recType = dsPasien_LapTotalObatPasienRWI.recordType;
                                                        var o=cst['listData'][i];
                                            	
                                                        recs.push(new recType(o));
                                                        dsPasien_LapTotalObatPasienRWI.add(recs);
                                                    }
                                                    a.expand();
                                                    if(dsPasien_LapTotalObatPasienRWI.onShowList != undefined)
                                                        dsPasien_LapTotalObatPasienRWI.onShowList(cst[showVar]);
                                                    if(cst['listData'].length>0){
                                                        	
                                                        a.doQuery(a.allQuery, true);
                                                        a.expand();
                                                        a.selectText(value.length,value.length);
                                                    }else{
                                                	
                                                            a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
                                                            a.restrictHeight();
                                                            a.selectedIndex = 0;
                                                    	
                                                    }
                                                }
                                            });
                                            value=cboPasienLapResepPasienRWI.lastQuery;
                                        }
                                    }
                                },1000);
                            }
                        } 
                    }
                }
            )
            return cboPasienLapResepPasienRWI;
        }, */

    getSelect: function (jenis_pay) {
        var $this = this;
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: baseURL + "index.php/apotek/lap_totalobatpasienrwi/getSelect",
            data: { jenis_pay: jenis_pay },
            success: function (r) {
                loadMask.hide();
                if (r.result == 'SUCCESS') {
                    Q($this.DropDown.detailBayar).reset();
                    Q($this.DropDown.detailBayar).add({ id: '', text: 'Semua' });
                    Q($this.DropDown.detailBayar).add(r.data);
                    Q($this.DropDown.detailBayar).val('');
                } else {
                    Ext.Msg.alert('Gagal', r.message);
                }
            },
            error: function (jqXHR, exception) {
                loadMask.hide();
                Nci.ajax.ErrorMessage(jqXHR, exception);
            }
        });
    },
    LookUpKunjungan: function (no_medrec) {
        //		console.log(no_medrec);
        var $this = this;
        $this.Window.lookup_kunjungan = Q().window({
            title: 'Data Kunjungan',
            width: 600,
            height: 300,
            items: [
                {
                    layout: 'Form',
                    anchor: '100%',
                    //width: lebar- 80,
                    labelAlign: 'Left',
                    labelWidth: 100,
                    autoWidth: true,
                    border: true,
                    items: [
                        $this.gridKunjungan()
                    ]
                }
            ]
        });
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            data: { no_medrec: no_medrec },
            url: baseURL + "index.php/apotek/lap_totalobatpasienrwi/getKunjungan",
            success: function (r) {
                loadMask.hide();
                if (r.processResult == 'SUCCESS') {
                    $this.Window.lookup_kunjungan.show();
                    $this.ArrayStore.kunjungan.loadData([], false);
                    for (var i = 0, iLen = r.listData.length; i < iLen; i++) {
                        var records = [];
                        records.push(new $this.ArrayStore.kunjungan.recordType());
                        $this.ArrayStore.kunjungan.add(records);
                        $this.ArrayStore.kunjungan.data.items[i].data.kd_pasien = r.listData[i].kd_pasien;
                        $this.ArrayStore.kunjungan.data.items[i].data.nama = r.listData[i].nama;
                        $this.ArrayStore.kunjungan.data.items[i].data.alamat = r.listData[i].alamat;
                        $this.ArrayStore.kunjungan.data.items[i].data.nama_unit = r.listData[i].nama_unit;
                        $this.ArrayStore.kunjungan.data.items[i].data.no_kamar = r.listData[i].no_kamar;
                        $this.ArrayStore.kunjungan.data.items[i].data.kd_unit = r.listData[i].kd_unit;
                        $this.ArrayStore.kunjungan.data.items[i].data.tgl_inap = r.listData[i].tgl_inap;
                        $this.ArrayStore.kunjungan.data.items[i].data.tgl_keluar = r.listData[i].tgl_keluar;
                        $this.ArrayStore.kunjungan.data.items[i].data.no_transaksi = r.listData[i].no_transaksi;
                    }
                    $this.Grid.kunjungan.getView().refresh();
                } else {
                    Ext.Msg.alert('Gagal', r.message);
                }
            },
            error: function (jqXHR, exception) {
                loadMask.hide();
                Nci.ajax.ErrorMessage(jqXHR, exception);
            }
        });
    },
    getKelPasien: function () {
        var $this = this;
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            data: {
                no_medrec: currentRowSelectionKunjungan.data.kd_pasien,
                no_transaksi: currentRowSelectionKunjungan.data.no_transaksi
            },
            url: baseURL + "index.php/apotek/lap_totalobatpasienrwi/getKelPasien",
            success: function (r) {
                loadMask.hide();
                if (r.result == 'SUCCESS') {
                    $this.Window.main.show();
                    Q($this.DropDown.kelpasien).add(r.data.customer);
                } else {
                    Ext.Msg.alert('Gagal', r.message);
                }
            },
            error: function (jqXHR, exception) {
                loadMask.hide();
                Nci.ajax.ErrorMessage(jqXHR, exception);
            }
        });
    },
    gridKunjungan: function () {
        var $this = this;

        GridColumnModel = new Ext.grid.ColumnModel([
            new Ext.grid.RowNumberer(),
            {
                dataIndex: 'no_transaksi',
                header: 'no transaksi',
                sortable: true,
                hidden: true,
                width: 30
            },
            {
                dataIndex: 'kd_pasien',
                header: 'No. Medrec',
                sortable: true,
                width: 70
            },
            {
                dataIndex: 'nama',
                header: 'Nama',
                sortable: true,
                width: 120
            },
            {
                dataIndex: 'nama_unit',
                header: 'Nama Unit',
                sortable: true,
                width: 70
            },
            {
                dataIndex: 'no_kamar',
                header: 'No. Kamar',
                sortable: true,
                width: 60
            },
            {
                dataIndex: 'tgl_inap',
                header: 'Tgl Inap',
                sortable: true,
                width: 80
            },
            {
                dataIndex: 'tgl_keluar',
                header: 'Tgl Keluar',
                sortable: true,
                width: 80
            }
        ]);
        $this.Grid.kunjungan = new Ext.grid.EditorGridPanel({
            xtype: 'editorgrid',
            store: $this.ArrayStore.kunjungan,
            title: '',
            autoScroll: true,
            columnLines: true,
            border: true,
            width: 600,
            height: 290,
            plugins: [new Ext.ux.grid.FilterRow()],
            cm: GridColumnModel,
            selModel: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function (sm, row, rec) {
                        currentRowSelectionKunjungan = undefined;
                        currentRowSelectionKunjungan = $this.ArrayStore.kunjungan.getAt(row);
                        console.log(currentRowSelectionKunjungan);
                    }
                }
            }),
            listeners: {
                rowdblclick: function (sm, row, cidx) {
                    $this.TextField.no_medrec.setValue(currentRowSelectionKunjungan.data.kd_pasien);
                    $this.TextField.nama.setValue(currentRowSelectionKunjungan.data.nama);
                    $this.TextField.kamar.setValue(currentRowSelectionKunjungan.data.no_kamar);
                    $this.TextField.kelas.setValue(currentRowSelectionKunjungan.data.nama_unit);
                    $this.DateField.startDate.setValue(currentRowSelectionKunjungan.data.tgl_inap);
                    $this.DateField.endDate.setValue(currentRowSelectionKunjungan.data.tgl_keluar);
                    $this.vars.no = currentRowSelectionKunjungan.data.no_transaksi;
                    $this.Window.lookup_kunjungan.close();
                    $this.getKelPasien()
                },
                'keydown': function (e) {
                    if (e.getKey() == 13) {
                        $this.TextField.no_medrec.setValue(currentRowSelectionKunjungan.data.kd_pasien);
                        $this.TextField.nama.setValue(currentRowSelectionKunjungan.data.nama);
                        $this.TextField.kamar.setValue(currentRowSelectionKunjungan.data.no_kamar);
                        $this.TextField.kelas.setValue(currentRowSelectionKunjungan.data.nama_unit);
                        $this.DateField.startDate.setValue(currentRowSelectionKunjungan.data.tgl_inap);
                        $this.DateField.endDate.setValue(currentRowSelectionKunjungan.data.tgl_keluar);
                        $this.vars.no = currentRowSelectionKunjungan.data.no_transaksi;
                        $this.Window.lookup_kunjungan.close();
                        $this.getKelPasien()
                    }
                }
            },
            viewConfig: {
                forceFit: true
            }
        });
        return $this.Grid.kunjungan;
    },
    init: function () {
        var $this = this;
        $this.Window.main = Q().window({
            title: 'Laporan Total Obat Perpasien Rawat Inap',
            fbar: [
                new Ext.Button({
                    text: 'Print',
                    hidden: true,
                    handler: function () {
                        print = true;
                        $this.doPrint()
                    }
                }),
                new Ext.Button({
                    text: 'Ok',
                    handler: function () {
                        print = false;
                        $this.doPrint()
                    }
                }),
                new Ext.Button({
                    text: 'Close',
                    handler: function () {
                        $this.Window.main.close();
                    }
                })
            ],
            items: [
                Q().panel({
                    items: [
                        Q().fieldset({
                            title: 'Info',
                            items: [
                                // Q().input({
                                // label:'No. Medrec',
                                // items:[
                                // $this.mComboPasienLapResepPasienRWI(),
                                // ]
                                // }),

                                Q().input({
                                    label: 'No. MedRec',
                                    items: [
                                        /* $this.DropDown.noMedrec=Q().autocomplete({
                                            width	: 200,
                                            select	:function(a,b){
                                                $this.TextField.nama.setValue(b.data.id);
                                                $this.TextField.kelas.setValue(b.data.kelas);
                                                $this.TextField.kamar.setValue(b.data.no_kamar);
                                                $this.TextField.kelas.setValue(b.data.nama_unit);
                                                //$this.TextField.tglMasuk.setValue(new Date(b.data.tgl_inap));
                                                // $this.TextField.tglKeluar.setValue(new Date(b.data.tgl_keluar));
                                                $this.vars.no=b.data.no_transaksi;
                                                // alert($this.vars.no);
                                                // console.log(b);
                                            },
                                            insert	: function(o){
                                                return {
                                                    id			:o.id,//nama
                                                    no_kamar 	: o.no_kamar,
                                                    nama_unit 	: o.nama_unit,
                                                    tgl_keluar 	: o.tgl_keluar,
                                                    tgl_inap 	: o.tgl_inap,
                                                    no_transaksi:o.no_transaksi,
                                                    text 		: o.text,//no_medrec
                                                    display		:  '<table style="font-size: 11px;"><tr><td width="80">'+o.text+'</td><td width="80">'+o.no_transaksi+'</td><td width="270"> - '+o.id+'</td></tr></table>'
                                                }
                                            },
                                        	
                                            success:function(res){return res.data},
                                            url			: baseURL + "index.php/apotek/lap_totalobatpasienrwi/getPasien",
                                            keyField	: 'text',
                                            valueField	: 'text',
                                            displayField: 'display',
                                            listWidth	: 350
                                        }) */
                                        $this.TextField.no_medrec = new Ext.form.TextField({
                                            width: 200,
                                            disabled: false,
                                            listeners: {
                                                'select': function (a, b, c) {
                                                },
                                                'specialkey': function () {
                                                    if (Ext.EventObject.getKey() === 13) {
                                                        var tmpNoIIMedrec = $this.TextField.no_medrec.getValue();
                                                        if (tmpNoIIMedrec.length !== 0 && tmpNoIIMedrec.length < 10) {
                                                            var tmpgetNoIIMedrec = formatnomedrec($this.TextField.no_medrec.getValue());
                                                            $this.TextField.no_medrec.setValue(tmpgetNoIIMedrec);
                                                            $this.LookUpKunjungan(tmpgetNoIIMedrec);

                                                        } else {
                                                            if (tmpNoIIMedrec.length === 10) {
                                                                $this.TextField.no_medrec.setValue(tmpgetNoIIMedrec);
                                                                $this.LookUpKunjungan(tmpNoIIMedrec);
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        })
                                    ]
                                }),
                                Q().input({
                                    label: 'Nama Pasien',
                                    items: [
                                        $this.TextField.nama = new Ext.form.TextField({
                                            width: 200, disabled: true
                                        })
                                    ]
                                }),
                                {
                                    layout: 'column',
                                    border: false,
                                    width: 320,
                                    items: [
                                        Q().input({
                                            label: 'Kamar',
                                            width: 190,
                                            items: [
                                                $this.TextField.kamar = new Ext.form.TextField({
                                                    width: 70, disabled: true
                                                })
                                            ]
                                        }),
                                        Q().input({
                                            label: 'Kelas',
                                            width: 120,
                                            xWidth: 40,
                                            items: [
                                                $this.TextField.kelas = new Ext.form.TextField({
                                                    width: 70, disabled: true
                                                })
                                            ]
                                        })

                                    ]
                                }, {
                                    layout: 'column',
                                    border: false,
                                    width: 360,
                                    items: [
                                        Q().input({
                                            label: 'Tgl Resep',
                                            width: 350,
                                            items: [
                                                $this.DateField.startDate = Q().datefield({
                                                    width: 100
                                                }),
                                                Q().display({ value: 's/d', width: 20 }),
                                                $this.DateField.endDate = Q().datefield({
                                                    width: 100
                                                })
                                            ]
                                        }),
                                        /* Q().input({
                                            label:'Tgl Resep',
                                            width:190,
                                            items:[
                                                $this.TextField.tglMasuk=Q().datefield({
                                                    width: 70,disabled:false,value:''
                                                })
                                            ]
                                        }),
                                        Q().input({
                                            label:'s/d',
                                            width:120,
                                            xWidth:40,
                                            items:[
                                                $this.TextField.tglKeluar=Q().datefield({
                                                    width: 70,disabled:false,value:''
                                                })
                                            ]
                                        }) */

                                    ]
                                }
                            ]
                        }),
                        Q().input({
                            label: 'Kelompok Pasien',
                            items: [
                                $this.DropDown.kelpasien = Q().dropdown({
                                    width: 150,
                                    data: [
                                        { id: '', text: 'Semua' }
                                    ],
                                })
                            ]
                        }),
                        // Q().input({
                        //     label: 'Cara Pembayaran',
                        //     width: 350,
                        //     items: [
                        //         new Ext.form.RadioGroup({
                        //             xtype: 'radiogroup',
                        //             fieldLabel: 'Choose your favorite',
                        //             items: [
                        //                 $this.Radio.satu = new Ext.form.Radio({
                        //                     boxLabel: 'Pilih',
                        //                     name: 'rb',
                        //                     checked: true,
                        //                     width: 60
                        //                 }),
                        //                 $this.Radio.dua = new Ext.form.Radio({
                        //                     boxLabel: 'Transfer',
                        //                     name: 'rb',
                        //                     width: 75
                        //                 }),
                        //                 $this.Radio.tiga = new Ext.form.Radio({
                        //                     boxLabel: 'Kredit',
                        //                     name: 'rb',
                        //                     width: 60
                        //                 })
                        //             ],
                        //             listeners: {
                        //                 change: function (a, b) {
                        //                     if ($this.Radio.satu.checked == true) {
                        //                         $this.DropDown.payment.enable();
                        //                         $this.DropDown.detailBayar.enable();
                        //                     } else {
                        //                         $this.DropDown.payment.disable();
                        //                         $this.DropDown.detailBayar.disable();
                        //                     }
                        //                 }
                        //             }
                        //         })
                        //     ]
                        // }),
                        // Q().input({
                        //     separator: '',
                        //     items: [
                        //         $this.DropDown.payment = Q().dropdown({
                        //             width: 150,
                        //             data: [
                        //                 { id: '', text: 'Semua' }
                        //             ],
                        //             select: function (a) {
                        //                 $this.getSelect(a.getValue());
                        //             }
                        //         })
                        //     ]
                        // }),
                        // Q().input({
                        //     separator: '',
                        //     items: [
                        //         $this.DropDown.detailBayar = Q().dropdown({
                        //             width: 200,
                        //             data: [
                        //                 { id: '', text: 'Semua' }
                        //             ]
                        //         })
                        //     ]
                        // }),
                        // Q().input({
                        //     label: 'Unit Apotek',
                        //     items: [
                        //         $this.DropDown.unit = Q().dropdown({
                        //             width: 150,
                        //             data: [
                        //                 { id: '', text: 'Semua' }
                        //             ]
                        //         })
                        //     ]
                        // }),
                        // Q().input({
                        //     label: 'Sub Jenis',
                        //     items: [
                        //         $this.DropDown.jenis = Q().dropdown({
                        //             width: 150,
                        //             data: [
                        //                 { id: '', text: 'Semua' }
                        //             ]
                        //         })
                        //     ]
                        // }),
                        Q().input({
                            label: 'Type File',
                            items: [
                                {
                                    xtype: 'checkbox',
                                    id: 'checkboxExcel',
                                    hideLabel: false,
                                    boxLabel: 'Excel',
                                    checked: false
                                },
                            ]
                        }),

                    ]
                })
            ]
        });
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: baseURL + "index.php/apotek/lap_totalobatpasienrwi/getData",
            success: function (r) {
                loadMask.hide();
                if (r.result == 'SUCCESS') {
                    $this.Window.main.show();
                    Q($this.DropDown.payment).add(r.data.payment);
                    Q($this.DropDown.unit).add(r.data.unit);
                    Q($this.DropDown.jenis).add(r.data.jenis);
                } else {
                    Ext.Msg.alert('Gagal', r.message);
                }
            },
            error: function (jqXHR, exception) {
                loadMask.hide();
                Nci.ajax.ErrorMessage(jqXHR, exception);
            }
        });
    }
}
DlgLapTotalObatPerpasienRWI.init();