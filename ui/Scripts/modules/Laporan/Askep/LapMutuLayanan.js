
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsMutuLayanan;
var selectNamaMutuLayanan;
var now = new Date();
var selectSetPerseorangan;
var frmLapMutuLayanan;
var varLapMutuLayanan = ShowFormLapMutuLayanan();
var selectSetUmum;
var selectSetkelpas;

function ShowFormLapMutuLayanan()
{
    frmLapMutuLayanan = fnLapMutuLayanan();
    frmLapMutuLayanan.show();
}
;

function fnLapMutuLayanan()
{
    var winMutuLayananReport = new Ext.Window
            (
                    {
                        id: 'winMutuLayananReport',
                        title: 'Laporan Mutu Layanan',
                        closeAction: 'destroy',
                        width: 400,
                        height: 280,
                        border: false,
                        resizable: false,
                        plain: true,
                        constrain: true,
                        layout: 'fit',
                        iconCls: 'icon_lapor',
                        modal: true,
                        items: [ItemLapMutuLayanan()],
                        listeners:
                                {
                                    activate: function ()
                                    {
                                    }
                                }

                    }
            );

    return winMutuLayananReport;
}
;


function ItemLapMutuLayanan()
{
    var PnlLapMutuLayanan = new Ext.Panel
            (
                    {
                        id: 'PnlLapMutuLayanan',
                        fileUpload: true,
                        layout: 'form',
                        height: '500',
                        anchor: '100%',
                        bodyStyle: 'padding:5px',
                        border: true,
                        items:
                                [
                                    getItemLapMutuLayanan_Atas(),
                                    {
                                        layout: 'hBox',
                                        border: false,
                                        defaults: {margins: '0 5 0 0'},
                                        style: {'margin-left': '30px', 'margin-top': '5px'},
                                        anchor: '94%',
                                        layoutConfig:
                                                {
                                                    padding: '3',
                                                    pack: 'end',
                                                    align: 'middle'
                                                },
                                        items:
                                                [
                                                    {
                                                        xtype: 'button',
                                                        text: nmBtnOK,
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnOkLapMutuLayanan',
                                                        handler: function ()
                                                        {
                                                            if (ValidasiReportMutuLayanan() === 1)
                                                            {
                                                                var criteria = GetCriteriaMutuLayanan();
//                                                                console.log(criteria);
                                                                loadMask.show();
                                                                loadlaporanAskep('0', 'LapMutuLayanan', criteria, function () {
                                                                    frmLapMutuLayanan.close();
                                                                    loadMask.hide();
                                                                });
                                                            }
                                                            ;
                                                        }
                                                    },
                                                    {
                                                        xtype: 'button',
                                                        text: nmBtnCancel,
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnCancelLapMutuLayanan',
                                                        handler: function ()
                                                        {
                                                            frmLapMutuLayanan.close();
                                                        }
                                                    }
                                                ]
                                    }
                                ]
                    }
            );

    return PnlLapMutuLayanan;
}
;

function GetCriteriaMutuLayanan()
{
    var strKriteria = '';

    strKriteria = Ext.get('dtpTglAwalFilterLapMutuLayanan').getValue();
    strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterLapMutuLayanan').getValue();
    strKriteria += '##@@##' + Ext.getCmp('cboSpesialisasiMutuLayanan').getValue();
    strKriteria += '##@@##' + Ext.getCmp('cboKelasMutuLayanan').getValue();
    strKriteria += '##@@##' + Ext.getCmp('cboKamarMutuLayanan').getValue();
    strKriteria += '##@@##' + Ext.get('cboSpesialisasiMutuLayanan').getValue();
    strKriteria += '##@@##' + Ext.get('cboKelasMutuLayanan').getValue();
    strKriteria += '##@@##' + Ext.get('cboKamarMutuLayanan').getValue();
    return strKriteria;
}
;

function getKodeReportMutuLayanan()
{
    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanMutuLayanan').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    } else if (Ext.getCmp('cboPilihanMutuLayanan').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportMutuLayanan()
{
    var x = 1;
//    if (Ext.getCmp('cboSpesialisasiMutuLayanan').getValue() === '0')
//    {
//        if (Ext.getCmp('cboSpesialisasiMutuLayanan').getValue() === '' || Ext.get('cboKamarMutuLayanan').getValue() === 'Select a Spesialisasi...') {
//            ShowPesanWarningMutuLayananReport('Spesialisasi Belum Dipilih', 'Laporan Mutu Layanan');
//            x = 0;
//        }
//        if (Ext.get('dtpTglAwalFilterLapMutuLayanan').dom.value > Ext.get('dtpTglAkhirFilterLapMutuLayanan').dom.value)
//        {
//            ShowPesanWarningMutuLayananReport('Tanggal awal tidak boleh kurang dari tanggal akhir', nmTitleFormDlgReqCMRpt);
//            x = 0;
//        }
//    } else
//    {
    if (Ext.getCmp('cboSpesialisasiMutuLayanan').getValue() === '' || Ext.get('cboKamarMutuLayanan').getValue() === 'Select a Spesialisasi...') {
        ShowPesanWarningMutuLayananReport('Spesialisasi Belum Dipilih', 'Laporan Mutu Layanan');
        x = 0;
    }
    if (Ext.get('dtpTglAwalFilterLapMutuLayanan').dom.value > Ext.get('dtpTglAkhirFilterLapMutuLayanan').dom.value)
    {
        ShowPesanWarningMutuLayananReport('Tanggal awal tidak boleh kurang dari tanggal akhir', nmTitleFormDlgReqCMRpt);
        x = 0;
    }
    if (Ext.getCmp('cboKelasMutuLayanan').getValue() === '' || Ext.get('cboKamarMutuLayanan').getValue() === 'Select a kelas...') {
        ShowPesanWarningMutuLayananReport('Kelas Belum Dipilih', 'Laporan Mutu Layanan');
        x = 0;
    }
    if (Ext.get('cboKamarMutuLayanan').getValue() === '' || Ext.get('cboKamarMutuLayanan').getValue() === 'Select a kamar...') {
        ShowPesanWarningMutuLayananReport('Kamar Belum Dipilih', 'Laporan Mutu Layanan');
        x = 0;
    }
//    }

    return x;
}
;

function ShowPesanWarningMutuLayananReport(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 300
                    }
            );
}
;

function getItemLapMutuLayanan_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 375,
                height: 135,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    }, {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSpesialisasiMutuLayanan(),
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Kelas '
                    }, {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKelasMutuLayanan(),
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Kamar '
                    }, {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKamarMutuLayanan(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Periode Tanggal '
                    }, {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    }, {
                        x: 120,
                        y: 100,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterLapMutuLayanan',
                        format: 'd/M/Y',
                        value: tigaharilalu
                    }, {
                        x: 230,
                        y: 100,
                        xtype: 'label',
                        text: ' s/d '
                    }, {
                        x: 260,
                        y: 100,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterLapMutuLayanan',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                ]
            }]
    };
    return items;
}
;


function mComboSpesialisasiMutuLayanan()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiMutuLayanan = new WebApp.DataStore({fields: Field});
    dsSpesialisasiMutuLayanan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100, Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasiLaporan',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiMutuLayanan = new Ext.form.ComboBox(
            {
                x: 120,
                y: 10,
                id: 'cboSpesialisasiMutuLayanan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local', forceSelection: true,
                emptyText: 'Select a Spesialisasi...',
                selectOnFocus: true,
                fieldLabel: 'Propinsi',
                align: 'Right',
                store: dsSpesialisasiMutuLayanan,
                valueField: 'KD_SPESIAL',
                displayField: 'SPESIALISASI',
//                anchor: '20%',
                width: 240,
                listeners:
                        {
                            'select': function (a, b, c)
                            {
                                Ext.getCmp('cboKelasMutuLayanan').setValue('');
                                Ext.getCmp('cboKamarMutuLayanan').setValue('');
                                if (Ext.getCmp('cboSpesialisasiMutuLayanan').getValue() === '0')
                                {
                                    Ext.getCmp('cboKelasMutuLayanan').disable();
                                    Ext.getCmp('cboKamarMutuLayanan').disable();
                                } else
                                {
                                    Ext.getCmp('cboKelasMutuLayanan').enable();
                                    Ext.getCmp('cboKamarMutuLayanan').enable();
                                }
                                loaddatastoreKelasMutuLayanan(b.data.KD_SPESIAL);
                            },
                            'render': function (c) {
                                c.getEl().on('keypress', function (e) {
                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                        Ext.getCmp('cboKelasMutuLayanan').focus();
                                }, c);
                            }

                        }
            }
    );
    return cboSpesialisasiMutuLayanan;
}
;
function loaddatastoreKelasMutuLayanan(kd_spesial) {
    dsKelasMutuLayanan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100, Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function mComboKelasMutuLayanan()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasMutuLayanan = new WebApp.DataStore({fields: Field});
    var cboKelasMutuLayanan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboKelasMutuLayanan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasMutuLayanan,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        width: 240,
                        listeners:
                                {'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKamarMutuLayanan').setValue('');
                                        loaddatastoreKamarMutuLayanan(b.data.kd_unit);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarMutuLayanan').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboKelasMutuLayanan;
}
;
function loaddatastoreKamarMutuLayanan(kd_unit)
{
    dsKamarMutuLayanan.load
            (
                    {
                        params:
                                {
                                    Skip: 0, Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiMutuLayanan').getValue()
                                }
                    });
}

function mComboKamarMutuLayanan()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarMutuLayanan = new WebApp.DataStore({fields: Field});
    var cboKamarMutuLayanan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboKamarMutuLayanan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarMutuLayanan,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        width: 240,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                    }
                                }
                    }
            );
    return cboKamarMutuLayanan;
}
;