
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsSOAP;
var selectNamaSOAP;
var now = new Date();
var selectSetPerseorangan;
var frmLapSOAP;
var varLapSOAP = ShowFormLapSOAP();
var selectSetUmum;
var selectSetkelpas;
var dscombonamapasien;
var kd_pasien;
var kdunit;
var urut;
var tgl;
var tmpalamat;

function ShowFormLapSOAP()
{
    frmLapSOAP = fnLapSOAP();
    frmLapSOAP.show();
}
;

function fnLapSOAP()
{
    var winSOAPReport = new Ext.Window
            (
                    {
                        id: 'winSOAPReport',
                        title: 'Laporan SOAP',
                        closeAction: 'destroy',
                        width: 400,
                        height: 240,
                        border: false,
                        resizable: false,
                        plain: true,
                        constrain: true,
                        layout: 'fit',
                        iconCls: 'icon_lapor',
                        modal: true,
                        items: [ItemLapSOAP()],
                        listeners:
                                {
                                    activate: function ()
                                    {
                                    }
                                }

                    }
            );

    return winSOAPReport;
}
;


function ItemLapSOAP()
{
    var PnlLapSOAP = new Ext.Panel
            (
                    {
                        id: 'PnlLapSOAP',
                        fileUpload: true,
                        layout: 'form',
                        height: 50,
                        anchor: '100%',
                        bodyStyle: 'padding:5px',
                        border: true,
                        items:
                                [
                                    getItemLapSOAP_Atas(),
                                    {
                                        layout: 'hBox',
                                        border: false,
                                        defaults: {margins: '0 5 0 0'},
                                        style: {'margin-left': '0px', 'margin-top': '5px'},
                                        anchor: '100%',
                                        layoutConfig:
                                                {
                                                    padding: '3',
                                                    pack: 'end',
                                                    align: 'middle'
                                                },
                                        items:
                                                [
                                                    {
                                                        xtype: 'button',
                                                        text: nmBtnOK,
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnOkLapSOAP',
                                                        handler: function ()
                                                        {
                                                            if (ValidasiReportSOAP() === 1)
                                                            {
                                                                var criteria = GetCriteriaSOAP();
                                                                loadMask.show();
                                                                loadlaporanAskep('0', 'LapSOAP', criteria, function () {
                                                                    frmLapSOAP.close();
                                                                    loadMask.hide();
                                                                });
                                                            }
                                                            ;
                                                        }
                                                    },
                                                    {
                                                        xtype: 'button',
                                                        text: nmBtnCancel,
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnCancelLapSOAP',
                                                        handler: function ()
                                                        {
                                                            frmLapSOAP.close();
                                                        }
                                                    }
                                                ]
                                    }
                                ]
                    }
            );

    return PnlLapSOAP;
}
;

function GetCriteriaSOAP()
{
    var strKriteria = '';
    strKriteria = kd_pasien;
    strKriteria += '##@@##' + kdunit;
    strKriteria += '##@@##' + urut;
    strKriteria += '##@@##' + tgl;
    strKriteria += '##@@##' + Ext.get('cbpencariankodepasien').getValue();
    strKriteria += '##@@##' + Ext.get('cbpencarian').getValue();
    strKriteria += '##@@##' + tmpalamat;
    strKriteria += '##@@##' + Ext.get('TxtSpesialisasiPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtKelasPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtKamarPasien').getValue();
    return strKriteria;
}
;

function ValidasiReportSOAP()
{
    var x = 1;
    if (Ext.getCmp('cbpencarian').getValue() === '' || Ext.getCmp('cbpencariankodepasien').getValue() === '')
    {
        if (Ext.getCmp('cbpencarian').getValue() === '') {
            ShowPesanWarningSOAPReport('No. Medrec Tidak Boleh kosong', 'Laporan Asuhan Keperawatan');
            x = 0;
        }
        if (Ext.getCmp('cbpencariankodepasien').getValue() === '')
        {
            ShowPesanWarningSOAPReport('Nama Pasien Tidak Boleh kosong', 'Laporan Asuhan Keperawatan');
            x = 0;
        }
    }

    return x;
}
;

function ShowPesanWarningSOAPReport(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 300
                    }
            );
}
;

function getItemLapSOAP_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 375,
                height: 160,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKDPasienSOAP(),
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboMedrecSOAP(),
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    }, {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtSpesialisasiPasien',
                        id: 'TxtSpesialisasiPasien',
                        readOnly: true,
                        width: 200
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kelas '
                    }, {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 100,
                        xtype: 'textfield',
                        name: 'TxtKelasPasien',
                        id: 'TxtKelasPasien',
                        readOnly: true,
                        width: 200
                    },
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Kamar '
                    }, {
                        x: 110,
                        y: 130,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 130,
                        xtype: 'textfield',
                        name: 'TxtKamarPasien',
                        id: 'TxtKamarPasien',
                        readOnly: true,
                        width: 200
                    }
                ]
            }]
    };
    return items;
}
;


function loadcombomedrec(kreteria)
{
    if (kreteria === undefined)
    {
        kreteria = "ng.AKHIR = 't' limit 50 ";
    }

    dscombonamapasien.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepPengkajian',
                                    param: kreteria
                                }
                    }
            );
    return dscombonamapasien;
}
function mComboMedrecSOAP()
{
    var Field = ['TGL_MASUK', 'KD_PASIEN', 'NAMA', 'ALAMAT', 'SPESIALISASI', 'KELAS', 'NAMA_KAMAR', 'URUT_MASUK', 'KD_UNIT'];
    dscombonamapasien = new WebApp.DataStore({fields: Field});
    var resultTpl = new Ext.XTemplate(
            '<tpl for="."><div class="x-combo-list-item">',
            '<span>{NAMA} / {KD_PASIEN}<br />{SPESIALISASI} / {KELAS} / {NAMA_KAMAR}<br /></span></div></tpl>'
            );
    var simpleCombo = new Ext.form.ComboBox(
            {
                x: 120,
                y: 40,
                id: 'cbpencarian',
                displayField: 'NAMA',
                width: 240,
                queryMode: 'local',
                typeAhead: false,
                tpl: resultTpl,
                hideTrigger: true,
                store: dscombonamapasien,
                enableKeyEvents: true,
                triggerAction: 'all',
                lazyRender: true,
                forceSelection: false,
                selectOnFocus: true,
                matchFieldWidth: false,
                listeners: {
                    buffer: 50,
                    keyup: function (a, b)
                    {
                        if (b.getKey() === 37 || b.getKey() === 38 || b.getKey() === 39 || b.getKey() === 40 || b.getKey() === 13)
                        {

                        } else
                        {
                            loadcombomedrec(" LOWER(p.nama) like LOWER('%" + Ext.get('cbpencarian').getValue() + "%') and ng.AKHIR = 't' limit 50")
                        }
                    },
                    'select': function (a, b, c)
                    {
                        Ext.getCmp('cbpencariankodepasien').setValue(b.data.KD_PASIEN);
                        Ext.getCmp('TxtSpesialisasiPasien').setValue(b.data.SPESIALISASI);
                        Ext.getCmp('TxtKelasPasien').setValue(b.data.KELAS);
                        Ext.getCmp('TxtKamarPasien').setValue(b.data.NAMA_KAMAR);
                        kd_pasien = b.data.KD_PASIEN;
                        kdunit = b.data.KD_UNIT;
                        urut = b.data.URUT_MASUK;
                        tgl = b.data.TGL_MASUK;
                        tmpalamat = b.data.ALAMAT;
                    },
                }
            }
    );
    return simpleCombo;
}
;
function loadcombokdmedrec(kreteria)
{
    if (kreteria === undefined)
    {
        kreteria = "ng.AKHIR = 't' limit 50 ";
    }

    dscombokdmedrec.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepPengkajian',
                                    param: kreteria
                                }
                    }
            );
    return dscombokdmedrec;
}
function mComboKDPasienSOAP()
{
    var Field = ['TGL_MASUK', 'KD_PASIEN', 'NAMA', 'ALAMAT', 'SPESIALISASI', 'KELAS', 'NAMA_KAMAR', 'URUT_MASUK', 'KD_UNIT'];
    dscombokdmedrec = new WebApp.DataStore({fields: Field});
    var resultTpl = new Ext.XTemplate(
            '<tpl for="."><div class="x-combo-list-item">',
            '<span>{KD_PASIEN} / {NAMA}<br />{SPESIALISASI} / {KELAS} / {NAMA_KAMAR}<br /></span></div></tpl>'
            );
    var simpleCombo = new Ext.form.ComboBox(
            {
                x: 120,
                y: 10,
                id: 'cbpencariankodepasien',
                displayField: 'KD_PASIEN',
                width: 240,
                queryMode: 'local',
                typeAhead: false,
                tpl: resultTpl,
                hideTrigger: true,
                store: dscombokdmedrec,
                enableKeyEvents: true,
                triggerAction: 'all',
                lazyRender: true,
                forceSelection: false,
                selectOnFocus: true,
                matchFieldWidth: false,
                listeners: {
                    buffer: 50,
                    keyup: function (a, b)
                    {
                        if (b.getKey() === 37 || b.getKey() === 38 || b.getKey() === 39 || b.getKey() === 40 || b.getKey() === 13)
                        {

                        } else
                        {
                            loadcombokdmedrec(" P.KD_PASIEN = '" + Ext.get('cbpencariankodepasien').getValue() + "' and ng.AKHIR = 't' limit 50")
                        }
                    },
                    'select': function (a, b, c)
                    {
                        Ext.getCmp('cbpencarian').setValue(b.data.NAMA);
                        Ext.getCmp('TxtSpesialisasiPasien').setValue(b.data.SPESIALISASI);
                        Ext.getCmp('TxtKelasPasien').setValue(b.data.KELAS);
                        Ext.getCmp('TxtKamarPasien').setValue(b.data.NAMA_KAMAR);
                        kd_pasien = b.data.KD_PASIEN;
                        kdunit = b.data.KD_UNIT;
                        urut = b.data.URUT_MASUK;
                        tgl = b.data.TGL_MASUK;
                        tmpalamat = b.data.ALAMAT;
                    },
                    'specialkey': function ()
                    {
                        var tmpNoIIMedrec = Ext.get('cbpencariankodepasien').getValue();
                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoIIMedrec.length === 10)
                        {
                            if (tmpNoIIMedrec.length !== 0 && tmpNoIIMedrec.length < 10)
                            {
                                var tmpgetNoIIMedrec = formatnomedrec(Ext.get('cbpencariankodepasien').getValue());
                                Ext.getCmp('cbpencariankodepasien').setValue(tmpgetNoIIMedrec);
                                loadcombokdmedrec(" P.KD_PASIEN = '" + Ext.get('cbpencariankodepasien').getValue() + "' and ng.AKHIR = 't' limit 50");
                            }
                            else
                            {
                                if (tmpNoIIMedrec.length === 10)
                                {
                                    loadcombokdmedrec(" P.KD_PASIEN = '" + Ext.get('cbpencariankodepasien').getValue() + "' and ng.AKHIR = 't' limit 50");
                                }
                                else
                                    Ext.getCmp('cbpencariankodepasien').setValue('');
                            }
                        }
                    }
                }
            }
    );
    return simpleCombo;
}
;