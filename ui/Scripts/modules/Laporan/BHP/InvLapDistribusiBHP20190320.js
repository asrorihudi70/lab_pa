
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapDistribusi;
var varInvLapDistribusi= ShowFormInvLapDistribusi();
var asc=0;
var IdRootKelompokBarang_pInvLapDistribusi='0';
var KdInv_InvLapDistribusi;
var gridPanelLookUpBarang_InvLapDistribusi;
var jenis;


function ShowFormInvLapDistribusi()
{
    frmDlgInvLapDistribusi= fnDlgInvLapDistribusi();
    frmDlgInvLapDistribusi.show();
	GetStrTreeSetBarang_InvLapDistribusi();
};

function fnDlgInvLapDistribusi()
{
    var winInvLapDistribusiReport = new Ext.Window
    (
        {
            id: 'winInvLapDistribusiReport',
            title: 'Laporan Distribusi Barang',
            closeAction: 'destroy',
            width: 485,
            height: 560,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgInvLapDistribusi()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Print',
					width: 70,
					hideLabel: true,
					id: 'btnOkInvLapDistribusi',
					handler: function()
					{
						if(Ext.getCmp('rdSemua_InvLapDistribusi').getValue() == true){
							
							 var params={
								jenis:jenis,
								KdInv:KdInv_InvLapDistribusi,
								tglAwal:Ext.getCmp('dtpTglAwalInvLapDistribusi').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirInvLapDistribusi').getValue(),
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/bhp/lap_distribusi/cetakLapSemua");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgInvLapDistribusi.close(); 
						} else if(Ext.getCmp('rdNamaBarang_InvLapDistribusi').getValue() == true){
							var params={
								jenis:jenis,
								KdInv:KdInv_InvLapDistribusi,
								tglAwal:Ext.getCmp('dtpTglAwalInvLapDistribusi').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirInvLapDistribusi').getValue(),
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/bhp/lap_distribusi/cetakLapNamaBarang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgInvLapDistribusi.close();
						} else{
							var params={
								jenis:jenis,
								KdInv:KdInv_InvLapDistribusi,
								tglAwal:Ext.getCmp('dtpTglAwalInvLapDistribusi').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirInvLapDistribusi').getValue(),
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/bhp/lap_distribusi/cetakLapLokasi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgInvLapDistribusi.close();
						}
					}
				},
				{
					xtype: 'button',
					text: 'Close' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelInvLapDistribusi',
					handler: function()
					{
						frmDlgInvLapDistribusi.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   jenis:'semua';
				}
			}

        }
    );

    return winInvLapDistribusiReport;
};


function ItemDlgInvLapDistribusi()
{
    var PnlInvLapDistribusi = new Ext.Panel
    (
        {
            id: 'PnlInvLapDistribusi',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					
					xtype: 'fieldset',
					bodyStyle: 'padding: 0px',
					title: '',
					items: 
					[
						{
							xtype: 'fieldset',
							title: 'Kriteria',
							bodyStyle: 'padding:0px',
							items: 
							[
								getItemInvLapDistribusi_Combo(),
								getItemInvLapDistribusi_tree(),
							]
						},
						getItemInvLapDistribusi_Pilihan(),
					]
				}
            ]
        }
    );

    return PnlInvLapDistribusi;
};


function getItemInvLapDistribusi_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};
function getItemInvLapDistribusi_Combo()
{
    var itemss = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  415,
				height: 55,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAwalInvLapDistribusi',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 210,
						y: 5,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 235,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAkhirInvLapDistribusi',
						format: 'd/M/Y',
						value: now,
						width: 100
					},
					{
						x: 10,
						y: 35,
						xtype: 'label',
						text: 'Sub-sub Kelompok Barang'
					}, 
				]
			}
		]
    };
    return itemss;
};

function getItemInvLapDistribusi_Pilihan()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  439,
				height: 50,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Jenis laporan'
					}, 
					{
						x: 80,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 110,
						y: 5,
						xtype: 'radio',
						boxLabel: ' Semua (Detail)',
						name: 'rdPilihanLaporan_InvLapDistribusi',
						id: 'rdSemua_InvLapDistribusi',
						checked: true,
						handler: function (field, value) 
						{
							if (value === true) {
								jenis="semua";
							}
						}
					}, 
					{
						x: 110,
						y: 25,
						xtype: 'radio',
						boxLabel: ' Nama Barang(Summary)',
						name: 'rdPilihanLaporan_InvLapDistribusi',
						id:'rdNamaBarang_InvLapDistribusi',
						handler: function (field, value) 
						{
							if (value === true) {
								jenis="nama_barang";
							}
						}
					}, 
					{
						x: 270,
						y: 5,
						xtype: 'radio',
						hidden 	: true,
						boxLabel: ' Lokasi/Ruang (Summary)',
						name: 'rdPilihanLaporan_InvLapDistribusi',
						id:'rdLokasi_InvLapDistribusi',
						handler: function (field, value) 
						{
							if (value === true) {
								jenis="lokasi";
							}
						}
					}, 
					
				]
			}
		]
    };
    return items;
};
function getItemInvLapDistribusi_tree()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
           itemsTreeListKelompokBarangGridDataView_InvLapDistribusi(),
		   getItemInvLapDistribusi_Batas(),
		   gridDataViewBarang_InvLapDistribusi()
		]
    };
    return items;
};




function GetStrTreeSetBarang_InvLapDistribusi()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and kd_inv='8000000000'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKelompokBarangGridDataView_InvLapDistribusi()
{	
		
	treeKlas_kelompokbarang_InvLapDistribusi= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:170,
			width:397,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_InvLapDistribusi=n.attributes
					if (strTreeCriteriKelompokBarang_InvLapDistribusi.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_InvLapDistribusi.leaf == false)
						{
							
						}
						else
						{
							console.log(n.attributes.id);
							dataGridLookUpBarangInvLapDistribusi(n.attributes.id);
							
							if(asc==0){
								asc=1;
								GetStrTreeSetBarang_InvLapDistribusi();
								rootTreeMasterBarang_InvLapDistribusi = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdRootKelompokBarang_pInvLapDistribusi,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeKlas_kelompokbarang_InvLapDistribusi.setRootNode(rootTreeMasterBarang_InvLapDistribusi);
						    } 
							
						};
					}
					else
					{
						
					};
				},
				
				
			}
		}
	);
	
	rootTreeMasterBarang_InvLapDistribusi = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdRootKelompokBarang_pInvLapDistribusi,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_InvLapDistribusi.setRootNode(rootTreeMasterBarang_InvLapDistribusi);  
	
    var pnlTreeFormDataWindowPopup_InvLapDistribusi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_InvLapDistribusi,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_InvLapDistribusi;
}

function gridDataViewBarang_InvLapDistribusi()
{
    var FieldGrdBArang_InvLapDistribusi = [];
	
    dsDataGrdBarang_InvLapDistribusi= new WebApp.DataStore
	({
        fields: FieldGrdBArang_InvLapDistribusi
    });
    
    gridPanelLookUpBarang_InvLapDistribusi =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_InvLapDistribusi,
		height:115,
		width:415,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					//console.log(dsDataGrdBarang_InvLapDistribusi.getAt(row).data.kd_inv);
					KdInv_InvLapDistribusi = dsDataGrdBarang_InvLapDistribusi.getAt(row).data.kd_inv;
					console.log(KdInv_InvLapDistribusi);
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'inv_kd_inv',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvLapDistribusi;
}

function dataGridLookUpBarangInvLapDistribusi(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/lap_distribusi/getGridLookUpBarang",
			// params: {inv_kd_inv:inv_kd_inv},
			params: {inv_kd_inv:'0'},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_InvLapDistribusi.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_InvLapDistribusi.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_InvLapDistribusi.add(recs);
					
					
					
					gridPanelLookUpBarang_InvLapDistribusi.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}


function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
