var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapKlasifikasiBarang;
var varInvLapKlasifikasiBarang = ShowFormInvLapKlasifikasiBarang();
var asc = 0;
// var IdRootKelompokBarang_pInvLapKlasifikasiBarang='1000000000';
// y
var IdRootKelompokBarang_pInvLapKlasifikasiBarang = '3000000000';
//end y
var KdInv_InvLapKlasifikasiBarang;



function ShowFormInvLapKlasifikasiBarang() {
	frmDlgInvLapKlasifikasiBarang = fnDlgInvLapKlasifikasiBarang();
	frmDlgInvLapKlasifikasiBarang.show();
	GetStrTreeSetBarang_InvLapKlasifikasiBarang();
};

function fnDlgInvLapKlasifikasiBarang() {
	var winInvLapKlasifikasiBarangReport = new Ext.Window({
		id: 'winInvLapKlasifikasiBarangReport',
		title: 'Laporan Klasifikasi Barang',
		closeAction: 'destroy',
		width: 485,
		height: 350,
		border: false,
		resizable: false,
		plain: true,
		constrain: true,
		layout: 'fit',
		iconCls: 'icon_lapor',
		modal: true,
		items: [ItemDlgInvLapKlasifikasiBarang()],
		fbar: [{
				xtype: 'button',
				text: 'Ok',
				width: 70,
				hideLabel: true,
				id: 'btnOkInvLapKlasifikasiBarang',
				handler: function () {
					var params = {
						KdInv: KdInv_InvLapKlasifikasiBarang
					};
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("target", "_blank");
					form.setAttribute("action", baseURL + "index.php/bhp/lap_klasifikasibarang/cetak");
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", "data");
					hiddenField.setAttribute("value", Ext.encode(params));
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();
					//frmDlgInvLapKlasifikasiBarang.close();
				}
			},
			{
				xtype: 'button',
				text: 'Cancel',
				width: 70,
				hideLabel: true,
				id: 'btnCancelInvLapKlasifikasiBarang',
				handler: function () {
					frmDlgInvLapKlasifikasiBarang.close();
				}
			}
		],
		listeners: {
			activate: function () {

			}
		}

	});

	return winInvLapKlasifikasiBarangReport;
};


function ItemDlgInvLapKlasifikasiBarang() {
	var PnlInvLapKlasifikasiBarang = new Ext.Panel({
		id: 'PnlInvLapKlasifikasiBarang',
		fileUpload: true,
		layout: 'form',
		height: 300,
		anchor: '100%',
		bodyStyle: 'padding:10px',
		border: true,
		items: [
			getItemInvLapKlasifikasiBarang_tree(),
		]
	});

	return PnlInvLapKlasifikasiBarang;
};


function getItemInvLapKlasifikasiBarang_tree() {
	var items = {
		layout: 'column',
		border: false,
		items: [
			itemsTreeListKelompokBarangGridDataView_InvLapKlasifikasiBarang(),
		]
	};
	return items;
};



function GetStrTreeSetBarang_InvLapKlasifikasiBarang() {
	Ext.Ajax.request({
		url: WebAppUrl.UrlExecProcess,
		params: {
			UserID: strUser,
			ModuleID: 'ProsesGetKelompokBarang',
			//Params: " and kd_inv<>'-'"
			Params: " and kd_inv='3000000000'"
		},
		success: function (resp) {
			loadMask.hide();
			var cst = Ext.decode(resp.responseText);
			StrTreeSetKelompokBarang = cst.arr;
		},
		failure: function () {
			loadMask.hide();
		}
	});
};


function itemsTreeListKelompokBarangGridDataView_InvLapKlasifikasiBarang() {

	treeKlas_kelompokbarang_InvLapKlasifikasiBarang = new Ext.tree.TreePanel({
		autoScroll: true,
		split: true,
		height: 250,
		width: 430,
		loader: new Ext.tree.TreeLoader(),
		listeners: {
			click: function (n) {
				strTreeCriteriKelompokBarang_InvLapKlasifikasiBarang = n.attributes
				if (strTreeCriteriKelompokBarang_InvLapKlasifikasiBarang.id != ' 0') {
					if (strTreeCriteriKelompokBarang_InvLapKlasifikasiBarang.leaf == false) {

					} else {
						console.log(n.attributes.id);
						KdInv_InvLapKlasifikasiBarang = n.attributes.id;

						if (asc == 0) {
							asc = 1;
							GetStrTreeSetBarang_InvLapKlasifikasiBarang();
							rootTreeMasterBarang_InvLapKlasifikasiBarang = new Ext.tree.AsyncTreeNode({
								expanded: true,
								text: 'MASTER',
								id: IdRootKelompokBarang_pInvLapKlasifikasiBarang,
								children: StrTreeSetKelompokBarang,
								autoScroll: true
							})
							treeKlas_kelompokbarang_InvLapKlasifikasiBarang.setRootNode(rootTreeMasterBarang_InvLapKlasifikasiBarang);
						}

					};
				} else {

				};
			},
		}
	});

	rootTreeMasterBarang_InvLapKlasifikasiBarang = new Ext.tree.AsyncTreeNode({
		expanded: false,
		text: 'MASTER',
		id: IdRootKelompokBarang_pInvLapKlasifikasiBarang,
		children: StrTreeSetKelompokBarang,
		autoScroll: true
	})

	treeKlas_kelompokbarang_InvLapKlasifikasiBarang.setRootNode(rootTreeMasterBarang_InvLapKlasifikasiBarang);

	var pnlTreeFormDataWindowPopup_InvLapKlasifikasiBarang = new Ext.FormPanel({
		title: '',
		region: 'center',
		layout: 'anchor',
		padding: '8px',
		bodyStyle: 'padding:10px 0px 10px 10px;',
		fileUpload: true,
		//-------------- #items# --------------
		items: [

			//-------------- ## -------------- 
			treeKlas_kelompokbarang_InvLapKlasifikasiBarang,
			//-------------- ## -------------- 
		],
		//-------------- #End items# --------------
	})

	return pnlTreeFormDataWindowPopup_InvLapKlasifikasiBarang;
}

function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.WARNING,
		width: 300
	});
};