
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapMasterBarang;
var varInvLapMasterBarang= ShowFormInvLapMasterBarang();
var asc=0;
var IdRootKelompokBarang_pInvLapMasterBarang='1000000000';
var KdInv_InvLapMasterBarang;
var gridPanelLookUpBarang_InvLapMasterBarang;
var jenis;


function ShowFormInvLapMasterBarang()
{
    frmDlgInvLapMasterBarang= fnDlgInvLapMasterBarang();
    frmDlgInvLapMasterBarang.show();
	GetStrTreeSetBarang_InvLapMasterBarang();
};

function fnDlgInvLapMasterBarang()
{
    var winInvLapMasterBarangReport = new Ext.Window
    (
        {
            id: 'winInvLapMasterBarangReport',
            title: 'Laporan Master Barang',
            closeAction: 'destroy',
            width: 485,
            height: 585,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgInvLapMasterBarang()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkInvLapMasterBarang',
					handler: function()
					{
						if(Ext.getCmp('cbDaftarBarang_InvLapMasterBarang').getValue() == true){
							
							var params={
								judul:"DAFTAR BARANG PERSEDIAAN"
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/bhp/lap_masterbarang/cetakDaftarBarang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgInvLapMasterBarang.close();
						} else{
							var params={
								jenis:jenis,
								KdInv:KdInv_InvLapMasterBarang,
								tglAwal:Ext.getCmp('dtpTglAwalInvLapMasterBarang').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirInvLapMasterBarang').getValue(),
								bulan:Ext.get('cbBulanTerkhir_InvLapMasterBarang').getValue(),
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/bhp/lap_masterbarang/cetakHargaBarang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgInvLapMasterBarang.close();
						}
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelInvLapMasterBarang',
					handler: function()
					{
						frmDlgInvLapMasterBarang.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   Ext.getCmp('lala').disable();
				   jenis="tanggal";
				}
			}

        }
    );

    return winInvLapMasterBarangReport;
};


function ItemDlgInvLapMasterBarang()
{
    var PnlInvLapMasterBarang = new Ext.Panel
    (
        {
            id: 'PnlInvLapMasterBarang',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					
					xtype: 'fieldset',
					bodyStyle: 'padding: 0px',
					title: '',
					items: 
					[
						getItemInvLapMasterBarang_Pilihan(),
						//getItemInvLapMasterBarang_Batas(),
						{
							xtype: 'fieldset',
							title: 'Kriteria Harga Barang',
							id:'lala',
							items: 
							[
								getItemInvLapMasterBarang_Combo(),
								getItemInvLapMasterBarang_tree(),
							]
						}
					]
				}
            ]
        }
    );

    return PnlInvLapMasterBarang;
};


function getItemInvLapMasterBarang_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemInvLapMasterBarang_Pilihan()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  439,
				height: 35,
				anchor: '100% 100%',
				items: [
					{
						xtype: 'radiogroup',
						x:60,
						y:10,
						items: [
							{
								boxLabel: ' Daftar Barang',
								name: 'cbPilihanLaporan_InvLapMasterBarang',
								id: 'cbDaftarBarang_InvLapMasterBarang',
								checked: true
							},
							{
								name: 'cbPilihanLaporan_InvLapMasterBarang',
								id:'cbHargaBarang_InvLapMasterBarang',
								boxLabel: ' Harga Barang'
							}
						],
						listeners: {
							change: function(field, newValue, oldValue) {
								var value = newValue.show;
								console.log(value);
								
								if(Ext.getCmp('cbDaftarBarang_InvLapMasterBarang').getValue()==true){
									/* 
									Ext.getCmp('radioBulan_InvLapMasterBarang').disable();
									Ext.getCmp('radioTanggal_InvLapMasterBarang').disable();
									 */
									Ext.getCmp('lala').disable();
								} else{
									Ext.getCmp('lala').enable();
								}
							}
						}
					}
					
				]
			}
		]
    };
    return items;
};
function getItemInvLapMasterBarang_tree()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
           itemsTreeListKelompokBarangGridDataView_InvLapMasterBarang(),
		   getItemInvLapMasterBarang_Batas(),
		   gridDataViewBarang_InvLapMasterBarang()
		]
    };
    return items;
};
function getItemInvLapMasterBarang_Combo()
{
    var itemss = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  415,
				height: 110,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Untuk Harga Pembelian'
					}, 
					{
						x: 130,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 10,
						y: 30,
						xtype: 'radio',
						boxLabel: '(n) Bulan terakhir',
						id:'radioBulan_InvLapMasterBarang',
						name:'radioPilihanWaktu_InvLapMasterBarang',
						handler: function (field, value) 
						{
							if (value === true) {
								Ext.getCmp('dtpTglAwalInvLapMasterBarang').disable();
								Ext.getCmp('dtpTglAkhirInvLapMasterBarang').disable();
								Ext.getCmp('cbBulanTerkhir_InvLapMasterBarang').enable();
								jenis="bulan";
							}
						}
					}, 
					comboBulanTerkhir_InvLapMasterBarang(),
					{
						x: 10,
						y: 60,
						xtype: 'radio',
						boxLabel: 'Antara Tanggal',
						id:'radioTanggal_InvLapMasterBarang',
						checked:true,
						name:'radioPilihanWaktu_InvLapMasterBarang',
						handler: function (field, value) 
						{
							if (value === true) {
								Ext.getCmp('dtpTglAwalInvLapMasterBarang').enable();
								Ext.getCmp('dtpTglAkhirInvLapMasterBarang').enable();
								Ext.getCmp('cbBulanTerkhir_InvLapMasterBarang').disable();
								jenis="tanggal";
							}
						}
					}, 
					{
						x: 120,
						y: 60,
						xtype: 'datefield',
						id: 'dtpTglAwalInvLapMasterBarang',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 230,
						y: 60,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 255,
						y: 60,
						xtype: 'datefield',
						id: 'dtpTglAkhirInvLapMasterBarang',
						format: 'd/M/Y',
						value: now,
						width: 100
					},
					{
						x: 10,
						y: 90,
						xtype: 'label',
						text: 'Sub-sub Kelompok Barang'
					}, 
				]
			}
		]
    };
    return itemss;
};

function comboBulanTerkhir_InvLapMasterBarang()
{
    var cboBulanTerkhir_InvLapMasterBarang = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 30,
                id:'cbBulanTerkhir_InvLapMasterBarang',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                width:100,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, '1'], [2, '3'],[3, '6'], [4, '12']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'3',
                listeners:
                {
					'select': function(a,b,c)
					{
						
					}
                }
            }
	);
	return cboBulanTerkhir_InvLapMasterBarang;
};


function GetStrTreeSetBarang_InvLapMasterBarang()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and kd_inv='8000000000'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKelompokBarangGridDataView_InvLapMasterBarang()
{	
		
	treeKlas_kelompokbarang_InvLapMasterBarang= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:170,
			width:397,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_InvLapMasterBarang=n.attributes
					if (strTreeCriteriKelompokBarang_InvLapMasterBarang.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_InvLapMasterBarang.leaf == false)
						{
							
						}
						else
						{
							console.log(n.attributes.id);
							dataGridLookUpBarangInvLapMasterBarang(n.attributes.id);
							
							if(asc==0){
								asc=1;
								GetStrTreeSetBarang_InvLapMasterBarang();
								rootTreeMasterBarang_InvLapMasterBarang = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdRootKelompokBarang_pInvLapMasterBarang,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeKlas_kelompokbarang_InvLapMasterBarang.setRootNode(rootTreeMasterBarang_InvLapMasterBarang);
						    } 
							
						};
					}
					else
					{
						
					};
				},
				
				
			}
		}
	);
	
	rootTreeMasterBarang_InvLapMasterBarang = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdRootKelompokBarang_pInvLapMasterBarang,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_InvLapMasterBarang.setRootNode(rootTreeMasterBarang_InvLapMasterBarang);  
	
    var pnlTreeFormDataWindowPopup_InvLapMasterBarang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_InvLapMasterBarang,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_InvLapMasterBarang;
}

function gridDataViewBarang_InvLapMasterBarang()
{
    var FieldGrdBArang_InvLapMasterBarang = [];
	
    dsDataGrdBarang_InvLapMasterBarang= new WebApp.DataStore
	({
        fields: FieldGrdBArang_InvLapMasterBarang
    });
    
    gridPanelLookUpBarang_InvLapMasterBarang =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_InvLapMasterBarang,
		height:100,
		width:415,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_InvLapMasterBarang.getAt(row).data.kd_inv);
					KdInv_InvLapMasterBarang = dsDataGrdBarang_InvLapMasterBarang.getAt(row).data.kd_inv;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'inv_kd_inv',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvLapMasterBarang;
}

function dataGridLookUpBarangInvLapMasterBarang(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/lap_masterbarang/getGridLookUpBarang",
			params: {inv_kd_inv:inv_kd_inv},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_InvLapMasterBarang.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_InvLapMasterBarang.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_InvLapMasterBarang.add(recs);
					
					
					
					gridPanelLookUpBarang_InvLapMasterBarang.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}


function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
