
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapPenerimaanBarang;
var varInvLapPenerimaanBarang= ShowFormInvLapPenerimaanBarang();
var asc=0;
var IdRootKelompokBarang_pInvLapPenerimaanBarang='4000000000';
var KdInv_InvLapPenerimaanBarang;
var gridPanelLookUpBarang_InvLapPenerimaanBarang;
var cbPnerimaanDetail;


function ShowFormInvLapPenerimaanBarang()
{
    frmDlgInvLapPenerimaanBarang= fnDlgInvLapPenerimaanBarang();
    frmDlgInvLapPenerimaanBarang.show();
	GetStrTreeSetBarang_InvLapPenerimaanBarang();
};

function fnDlgInvLapPenerimaanBarang()
{
    var winInvLapPenerimaanBarangReport = new Ext.Window
    (
        {
            id: 'winInvLapPenerimaanBarangReport',
            title: 'Laporan Penerimaan Barang',
            closeAction: 'destroy',
            width: 485,
            height: 570,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgInvLapPenerimaanBarang()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Print',
					width: 70,
					hideLabel: true,
					id: 'btnOkInvLapPenerimaanBarang',
					handler: function()
					{
						if(Ext.getCmp('cbDetail_InvLapPenerimaanBarang').getValue() == true){
							//cetak berdasarkan urut order by
							if( Ext.get('cbUrutan_InvLapPenerimaanBarang').getValue() !== 'Nama barang'){
								var params={
									urut:Ext.get('cbUrutan_InvLapPenerimaanBarang').getValue(),
									KdInv:KdInv_InvLapPenerimaanBarang,
									tglAwal:Ext.getCmp('dtpTglAwalInvLapPenerimaanBarang').getValue(),
									tglAkhir:Ext.getCmp('dtpTglAkhirInvLapPenerimaanBarang').getValue(),
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/bhp/lap_penerimaanbarang/cetakDetail");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								frmDlgInvLapPenerimaanBarang.close();
							} else{
								var params={
									KdInv:KdInv_InvLapPenerimaanBarang,
									tglAwal:Ext.getCmp('dtpTglAwalInvLapPenerimaanBarang').getValue(),
									tglAkhir:Ext.getCmp('dtpTglAkhirInvLapPenerimaanBarang').getValue(),
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/bhp/lap_penerimaanbarang/cetakDetailNamaBarang");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								frmDlgInvLapPenerimaanBarang.close();
							}
							
						} else {
							if ( Ext.get('cbUrutan_InvLapPenerimaanBarang').getValue() == 'Faktur'){
								var params={
									KdInv:KdInv_InvLapPenerimaanBarang,
									tglAwal:Ext.getCmp('dtpTglAwalInvLapPenerimaanBarang').getValue(),
									tglAkhir:Ext.getCmp('dtpTglAkhirInvLapPenerimaanBarang').getValue(),
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/bhp/lap_penerimaanbarang/cetakSummaryFaktur");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								frmDlgInvLapPenerimaanBarang.close();
							} else if( Ext.get('cbUrutan_InvLapPenerimaanBarang').getValue() == 'Vendor'){
								var params={
									KdInv:KdInv_InvLapPenerimaanBarang,
									tglAwal:Ext.getCmp('dtpTglAwalInvLapPenerimaanBarang').getValue(),
									tglAkhir:Ext.getCmp('dtpTglAkhirInvLapPenerimaanBarang').getValue(),
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/bhp/lap_penerimaanbarang/cetakSummaryVendor");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								frmDlgInvLapPenerimaanBarang.close();
							} else{
								var params={
									KdInv:KdInv_InvLapPenerimaanBarang,
									tglAwal:Ext.getCmp('dtpTglAwalInvLapPenerimaanBarang').getValue(),
									tglAkhir:Ext.getCmp('dtpTglAkhirInvLapPenerimaanBarang').getValue(),
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/bhp/lap_penerimaanbarang/cetakSummaryNamaBarang");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								frmDlgInvLapPenerimaanBarang.close();
							}
						}
					}
				},
				{
					xtype: 'button',
					text: 'Close' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelInvLapPenerimaanBarang',
					handler: function()
					{
						frmDlgInvLapPenerimaanBarang.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winInvLapPenerimaanBarangReport;
};


function ItemDlgInvLapPenerimaanBarang()
{
    var PnlInvLapPenerimaanBarang = new Ext.Panel
    (
        {
            id: 'PnlInvLapPenerimaanBarang',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					
					xtype: 'fieldset',
					bodyStyle: 'padding: 0px',
					title: '',
					items: 
					[
						{
							xtype: 'fieldset',
							title: 'Kriteria',
							bodyStyle: 'padding:0px',
							items: 
							[
								getItemInvLapPenerimaanBarang_Tanggal(),
								getItemInvLapPenerimaanBarang_tree(),
							]
						},
						getItemInvLapPenerimaanBarang_Pilihan(),
					]
				}
            ]
        }
    );

    return PnlInvLapPenerimaanBarang;
};


function getItemInvLapPenerimaanBarang_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};
function getItemInvLapPenerimaanBarang_Tanggal()
{
    var itemss = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  415,
				height: 55,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAwalInvLapPenerimaanBarang',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 210,
						y: 5,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 235,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAkhirInvLapPenerimaanBarang',
						format: 'd/M/Y',
						value: now,
						width: 100
					},
					{
						x: 10,
						y: 35,
						xtype: 'label',
						text: 'Sub-sub Kelompok Barang'
					}, 
				]
			}
		]
    };
    return itemss;
};

function getItemInvLapPenerimaanBarang_Pilihan()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  439,
				height: 75,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Urut berdasarkan'
					}, 
					{
						x: 100,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					comboUrutan_InvLapPenerimaanBarang(),
					{
						x: 10,
						y: 30,
						xtype: 'label',
						text: 'Jenis laporan'
					}, 
					{
						x: 100,
						y: 30,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 120,
						y: 30,
						xtype: 'radio',
						boxLabel: ' Detail',
						name: 'cbPilihanLaporan_InvLapPenerimaanBarang',
						id: 'cbDetail_InvLapPenerimaanBarang',
						checked: true,
						handler: function (field, value) 
						{
							if (value === true) {
								
							}
						}
					}, 
					{
						x: 270,
						y: 30,
						xtype: 'radio',
						boxLabel: ' Summary',
						name: 'cbPilihanLaporan_InvLapPenerimaanBarang',
						id:'cbLokasi_InvLapPenerimaanBarang',
						handler: function (field, value) 
						{
							if (value === true) {
								
							}
						}
					}, 
					{
						x:10,
						y:50,
						xtype: 'checkbox',
						boxLabel: 'Lap. Penerimaan Detail',
						name: 'cbPenerimaanDetail_InvLapPenerimaanBarang',
						id: 'cbPenerimaanDetail_InvLapPenerimaanBarang',
						checked: false,
						handler: function (field, value) {
							if (value === true){
								cbPnerimaanDetail='true';
							}
						}
					},
				]
			}
		]
    };
    return items;
};
function getItemInvLapPenerimaanBarang_tree()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
           itemsTreeListKelompokBarangGridDataView_InvLapPenerimaanBarang(),
		   getItemInvLapPenerimaanBarang_Batas(),
		   gridDataViewBarang_InvLapPenerimaanBarang()
		]
    };
    return items;
};

function comboUrutan_InvLapPenerimaanBarang()
{
    var cboUrutan_InvLapPenerimaanBarang = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 5,
                id:'cbUrutan_InvLapPenerimaanBarang',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                width:100,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Faktur'], [2, 'Vendor'],[3, 'Nama barang']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
					'select': function(a,b,c)
					{
						
					}
                }
            }
	);
	return cboUrutan_InvLapPenerimaanBarang;
};



function GetStrTreeSetBarang_InvLapPenerimaanBarang()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and kd_inv='8000000000'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKelompokBarangGridDataView_InvLapPenerimaanBarang()
{	
		
	treeKlas_kelompokbarang_InvLapPenerimaanBarang= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:160,
			width:397,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_InvLapPenerimaanBarang=n.attributes
					if (strTreeCriteriKelompokBarang_InvLapPenerimaanBarang.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_InvLapPenerimaanBarang.leaf == false)
						{
							
						}
						else
						{
							console.log(n.attributes.id);
							dataGridLookUpBarangInvLapPenerimaanBarang(n.attributes.id);
							
							if(asc==0){
								asc=1;
								GetStrTreeSetBarang_InvLapPenerimaanBarang();
								rootTreeMasterBarang_InvLapPenerimaanBarang = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdRootKelompokBarang_pInvLapPenerimaanBarang,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeKlas_kelompokbarang_InvLapPenerimaanBarang.setRootNode(rootTreeMasterBarang_InvLapPenerimaanBarang);
						    } 
							
						};
					}
					else
					{
						
					};
				},
				
				
			}
		}
	);
	
	rootTreeMasterBarang_InvLapPenerimaanBarang = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdRootKelompokBarang_pInvLapPenerimaanBarang,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_InvLapPenerimaanBarang.setRootNode(rootTreeMasterBarang_InvLapPenerimaanBarang);  
	
    var pnlTreeFormDataWindowPopup_InvLapPenerimaanBarang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_InvLapPenerimaanBarang,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_InvLapPenerimaanBarang;
}

function gridDataViewBarang_InvLapPenerimaanBarang()
{
    var FieldGrdBArang_InvLapPenerimaanBarang = [];
	
    dsDataGrdBarang_InvLapPenerimaanBarang= new WebApp.DataStore
	({
        fields: FieldGrdBArang_InvLapPenerimaanBarang
    });
    
    gridPanelLookUpBarang_InvLapPenerimaanBarang =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_InvLapPenerimaanBarang,
		height:115,
		width:415,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_InvLapPenerimaanBarang.getAt(row).data.kd_inv);
					KdInv_InvLapPenerimaanBarang = dsDataGrdBarang_InvLapPenerimaanBarang.getAt(row).data.kd_inv;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'inv_kd_inv',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvLapPenerimaanBarang;
}

function dataGridLookUpBarangInvLapPenerimaanBarang(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/lap_penerimaanbarang/getGridLookUpBarang",
			params: {inv_kd_inv:inv_kd_inv},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_InvLapPenerimaanBarang.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_InvLapPenerimaanBarang.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_InvLapPenerimaanBarang.add(recs);
					
					
					
					gridPanelLookUpBarang_InvLapPenerimaanBarang.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}


function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
