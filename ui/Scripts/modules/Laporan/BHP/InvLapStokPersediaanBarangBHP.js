
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapStokPersediaanBarang;
var varInvLapStokPersediaanBarang= ShowFormInvLapStokPersediaanBarang();
var asc=0;
// var IdRootKelompokBarang_pInvLapStokPersediaanBarang='1000000000';
// y
var IdRootKelompokBarang_pInvLapStokPersediaanBarang='3000000000';
// end y
var KdInv_InvLapStokPersediaanBarang;
var gridPanelLookUpBarang_InvLapStokPersediaanBarang;
var jenis;


function ShowFormInvLapStokPersediaanBarang()
{
    frmDlgInvLapStokPersediaanBarang= fnDlgInvLapStokPersediaanBarang();
    frmDlgInvLapStokPersediaanBarang.show();
	GetStrTreeSetBarang_InvLapStokPersediaanBarang();
};

function fnDlgInvLapStokPersediaanBarang()
{
    var winInvLapStokPersediaanBarangReport = new Ext.Window
    (
        {
            id: 'winInvLapStokPersediaanBarangReport',
            title: 'Laporan Stok Barang',
            closeAction: 'destroy',
            width: 485,
            height: 565,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgInvLapStokPersediaanBarang()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkInvLapStokPersediaanBarang',
					handler: function()
					{
						if(Ext.getCmp('cbDetail_InvLapStokPersediaanBarang').getValue() == true){
							
							var params={
								jenis:jenis,
								KdInv:KdInv_InvLapStokPersediaanBarang,
								tglAwal:Ext.getCmp('dtpTglAwalInvLapStokPersediaanBarang').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirInvLapStokPersediaanBarang').getValue(),
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/bhp/lap_stokpersediaan/cetakDetail");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgInvLapStokPersediaanBarang.close();
						} else{
							var params={
								jenis:jenis,
								KdInv:KdInv_InvLapStokPersediaanBarang,
								tglAwal:Ext.getCmp('dtpTglAwalInvLapStokPersediaanBarang').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirInvLapStokPersediaanBarang').getValue(),
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/bhp/lap_stokpersediaan/cetakSummary");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgInvLapStokPersediaanBarang.close();
						}
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelInvLapStokPersediaanBarang',
					handler: function()
					{
						frmDlgInvLapStokPersediaanBarang.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winInvLapStokPersediaanBarangReport;
};


function ItemDlgInvLapStokPersediaanBarang()
{
    var PnlInvLapStokPersediaanBarang = new Ext.Panel
    (
        {
            id: 'PnlInvLapStokPersediaanBarang',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					
					xtype: 'fieldset',
					bodyStyle: 'padding: 0px',
					title: '',
					items: 
					[
						//getItemInvLapStokPersediaanBarang_Batas(),
						{
							xtype: 'fieldset',
							title: 'Kriteria',
							items: 
							[
								getItemInvLapStokPersediaanBarang_Tanggal(),
								getItemInvLapStokPersediaanBarang_tree(),
							]
						},
						getItemInvLapStokPersediaanBarang_Pilihan(),
					]
				}
            ]
        }
    );

    return PnlInvLapStokPersediaanBarang;
};


function getItemInvLapStokPersediaanBarang_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemInvLapStokPersediaanBarang_Pilihan()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  439,
				height: 35,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Jenis laporan'
					}, 
					{
						x: 80,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						xtype: 'radiogroup',
						x:110,
						y:10,
						items: [
							{
								boxLabel: ' Detail',
								name: 'cbPilihanLaporan_InvLapStokPersediaanBarang',
								id: 'cbDetail_InvLapStokPersediaanBarang',
								checked: true
							},
							{
								name: 'cbPilihanLaporan_InvLapStokPersediaanBarang',
								id:'cbSummary_InvLapStokPersediaanBarang',
								boxLabel: ' Summary'
							}
						],
						listeners: {
							change: function(field, newValue, oldValue) {
								var value = newValue.show;
									
								if(Ext.getCmp('cbDetail_InvLapStokPersediaanBarang').getValue()==true){
									jenis:"Detail";
								} else{
									jenis:"Summary";
								}
							}
						}
					}
					
				]
			}
		]
    };
    return items;
};
function getItemInvLapStokPersediaanBarang_tree()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
           itemsTreeListKelompokBarangGridDataView_InvLapStokPersediaanBarang(),
		   getItemInvLapStokPersediaanBarang_Batas(),
		   gridDataViewBarang_InvLapStokPersediaanBarang()
		]
    };
    return items;
};
function getItemInvLapStokPersediaanBarang_Tanggal()
{
    var itemss = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  415,
				height: 60,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAwalInvLapStokPersediaanBarang',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 210,
						y: 10,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 235,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAkhirInvLapStokPersediaanBarang',
						format: 'd/M/Y',
						value: now,
						width: 100
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Sub-sub Kelompok Barang'
					}, 
				]
			}
		]
    };
    return itemss;
};




function GetStrTreeSetBarang_InvLapStokPersediaanBarang()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and kd_inv='8000000000'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKelompokBarangGridDataView_InvLapStokPersediaanBarang()
{	
		
	treeKlas_kelompokbarang_InvLapStokPersediaanBarang= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:185,
			width:397,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_InvLapStokPersediaanBarang=n.attributes
					if (strTreeCriteriKelompokBarang_InvLapStokPersediaanBarang.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_InvLapStokPersediaanBarang.leaf == false)
						{
							
						}
						else
						{
							console.log(n.attributes.id);
							dataGridLookUpBarangInvLapStokPersediaanBarang(n.attributes.id);
							
							if(asc==0){
								asc=1;
								GetStrTreeSetBarang_InvLapStokPersediaanBarang();
								rootTreeMasterBarang_InvLapStokPersediaanBarang = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdRootKelompokBarang_pInvLapStokPersediaanBarang,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeKlas_kelompokbarang_InvLapStokPersediaanBarang.setRootNode(rootTreeMasterBarang_InvLapStokPersediaanBarang);
						    } 
							
						};
					}
					else
					{
						
					};
				},
				
				
			}
		}
	);
	
	rootTreeMasterBarang_InvLapStokPersediaanBarang = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdRootKelompokBarang_pInvLapStokPersediaanBarang,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_InvLapStokPersediaanBarang.setRootNode(rootTreeMasterBarang_InvLapStokPersediaanBarang);  
	
    var pnlTreeFormDataWindowPopup_InvLapStokPersediaanBarang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_InvLapStokPersediaanBarang,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_InvLapStokPersediaanBarang;
}

function gridDataViewBarang_InvLapStokPersediaanBarang()
{
    var FieldGrdBArang_InvLapStokPersediaanBarang = [];
	
    dsDataGrdBarang_InvLapStokPersediaanBarang= new WebApp.DataStore
	({
        fields: FieldGrdBArang_InvLapStokPersediaanBarang
    });
    
    gridPanelLookUpBarang_InvLapStokPersediaanBarang =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_InvLapStokPersediaanBarang,
		height:115,
		width:415,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_InvLapStokPersediaanBarang.getAt(row).data.kd_inv);
					KdInv_InvLapStokPersediaanBarang = dsDataGrdBarang_InvLapStokPersediaanBarang.getAt(row).data.kd_inv;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'inv_kd_inv',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvLapStokPersediaanBarang;
}

function dataGridLookUpBarangInvLapStokPersediaanBarang(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/lap_stokpersediaan/getGridLookUpBarang",
			params: {inv_kd_inv:inv_kd_inv},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_InvLapStokPersediaanBarang.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_InvLapStokPersediaanBarang.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_InvLapStokPersediaanBarang.add(recs);
					
					
					
					gridPanelLookUpBarang_InvLapStokPersediaanBarang.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}


function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
