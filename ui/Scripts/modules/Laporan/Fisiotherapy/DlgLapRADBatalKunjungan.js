var fields = [
    {name: 'KD_UNIT', mapping : 'KD_UNIT'},
    {name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];

var fieldsPayment = [
    {name: 'KD_PAY', mapping : 'KD_PAY'},
    {name: 'URAIAN', mapping : 'URAIAN'}
];

var cols = [
    { id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
    { header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
];

var colsPayment = [
    { id : 'KD_PAY', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'KD_PAY',hidden : true},
    { header: "Kode Pay", width: 50, sortable: true, dataIndex: 'URAIAN'}
];

var secondGridStoreLapKwitansi;
var secondGridStoreLapKwitansiPaymentLapKwitansi;
var firstGrid;
var jenis_pay='';
var dataSource_payment;
var firstGridPayment;
var DlgLapCetakBatalKunjungan={
    vars:{
        comboSelect:null
    },
    coba:function(){
        var $this=this;
        $this.DateField.startDate
    },
    ArrayStore:{
        dokter:new Ext.data.ArrayStore({fields:[]})
    },
    CheckboxGroup:{
        shift:null,
        tindakan:null,
    },
    ComboBox:{
        kelPasien1:null,
        combo1:null,
        combo2:null,
        combo3:null,
        combo0:null,
        poliklinik:null,
        dokter:null
    },
    DataStore:{
        combo2:null,
        combo3:null,
        poliklinik:null,
        combo1:null
    },
    DateField:{
        startDate:null,
        endDate:null
    },
    Window:{
        main:null
    },
    comboOnSelect:function(val){
        var $this=this;
        $this.ComboBox.combo0.hide();
        $this.ComboBox.combo1.hide();
        $this.ComboBox.combo2.hide();
        $this.ComboBox.combo3.hide();
        if(val==-1){
            $this.ComboBox.combo0.show();
            jenis_pay='';
            $this.datapaymentfirstgrid(jenis_pay);
        }else if(val==0){
            $this.ComboBox.combo1.show();
            jenis_pay='jenis_pay=1';
            $this.datapaymentfirstgrid(jenis_pay);
        }else if(val==1){
            $this.ComboBox.combo2.show();
            jenis_pay='jenis_pay=3';
            $this.datapaymentfirstgrid(jenis_pay);
        }else if(val==2){
            $this.ComboBox.combo3.show();
            jenis_pay='jenis_pay=4';
            $this.datapaymentfirstgrid(jenis_pay);
        }
    },
    initPrint:function(){
        var $this=this;
        loadMask.show();
        var params={};
        var sendDataArrayUnit    = [];
        var sendDataArraypayment = [];
        var sendDataTmpDataUnit     = "";

        secondGridStoreLapKwitansi.each(function(record){
            var recordArray   = [record.get("KD_UNIT")];
            sendDataArrayUnit.push(recordArray);
            sendDataTmpDataUnit = sendDataTmpDataUnit+"'"+recordArray+"',";
        });

        /*secondGridStoreLapKwitansiPaymentLapKwitansi.each(function(record){
            var recordArraypay= [record.get("KD_PAY")];
            sendDataArraypayment.push(recordArraypay);
        });*/

        if (sendDataArrayUnit.length === 0)
        {  
            this.messageBox('Peringatan','Isi kriteria unit dengan drag and drop','WARNING');
            loadMask.hide();
        }else{
            params['type_file'] = Ext.getCmp('CetakExcelCetakBatalKunjungan').getValue();
            params['tgl_awal']  = timestimetodate($this.DateField.startDate.getValue());
            params['tgl_akhir'] = timestimetodate($this.DateField.endDate.getValue());
            // params['pasien']    = $this.ComboBox.kelPasien1.getValue();
            var pasien=parseInt($this.ComboBox.kelPasien1.getValue());
            if(pasien>=0){
                if(pasien==0){
                    params['kd_customer']=$this.ComboBox.combo1.getValue();
                }else if(pasien==1){
                    params['kd_customer']=$this.ComboBox.combo2.getValue();
                }else if(pasien==2){
                    params['kd_customer']=$this.ComboBox.combo3.getValue();
                }
            } else{
                params['kd_customer']='Semua';
            }
            params['tmp_kd_unit']   = sendDataTmpDataUnit;
            // params['tmp_payment']    = sendDataArraypayment;
            // var shift    =$this.CheckboxGroup.shift.items.items;
            // var tindakan =$this.CheckboxGroup.tindakan.items.items;
            /*var shifta=false;
            var tindakan_stat=false;
            for(var i=0;i<shift.length ; i++){
                params['shift'+i]=shift[i].checked;
                if(shift[i].checked==true)shifta=true;
            }
            for(var i=0;i<tindakan.length ; i++){
                params['tindakan'+i]=tindakan[i].checked;
                if(tindakan[i].checked==true)tindakan_stat=true;
            }*/
            // params['orderBy']     = $this.ComboBox.comboOrderBy.getValue();

                console.log(params);
                var form = document.createElement("form");
                form.setAttribute("method", "post");
                form.setAttribute("target", "_blank");
                form.setAttribute("action", baseURL + "index.php/fisiotherapy/lap_fisiotherapy/cetak_batal_kunjungan");
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", "data");
                hiddenField.setAttribute("value", Ext.encode(params));
                form.appendChild(hiddenField);
                document.body.appendChild(form);
                form.submit();
                loadMask.hide();
        } 
        
    },
    messageBox:function(modul, str, icon){
        Ext.MessageBox.show
        (
            {
                title: modul,
                msg:str,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.icon,
                width:300
            }
        );
    },
    getDokter:function(){
        var $this=this;
        $this.ComboBox.dokter = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.ArrayStore.dokter,
            width: 200,
            value:'Semua',
            valueField: 'kd_dokter',
            displayField: 'nama',
            listeners:{
                'select': function(a, b, c){
                               
                }
            }
        });         
        $.ajax({
            type: 'POST',
            dataType:'JSON',
            url:baseURL + "index.php/gawat_darurat/lap_penerimaan/getDokter",
            success: function(r){
                loadMask.hide();
                if(r.processResult=='SUCCESS'){
                    $this.ArrayStore.dokter.loadData([],false);
                    for(var i=0,iLen=r.data.length; i<iLen ;i++){
                        $this.ArrayStore.dokter.add(new $this.ArrayStore.dokter.recordType(r.data[i]));
                    }
                }else{
                    Ext.Msg.alert('Gagal',r.processMessage);
                }
            },
            error: function(jqXHR, exception) {
                Nci.ajax.ErrorMessage(jqXHR, exception);
            }
        });
        return $this.ComboBox.dokter;
    },
    getPoliklinik:function(){
        var $this=this;
        var Field = ['KD_UNIT','NAMA_UNIT'];
        $this.DataStore.poliklinik = new WebApp.DataStore({fields: Field});
        $this.DataStore.poliklinik.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnitB',
                param: "kd_bagian=9 and type_unit=false"
            }
        });
        $this.ComboBox.poliklinik = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.DataStore.poliklinik,
            width: 200,
            value:'Semua',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            listeners:{
                'select': function(a, b, c){
                               
                }
            }
        });         
        return $this.ComboBox.poliklinik;
    },
    getCombo0:function(){
        var $this=this;
        $this.ComboBox.combo0 = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            width:100,
            store: new Ext.data.ArrayStore({
                id: 0,
                fields:['Id','displayText'],
                data: [[1, 'Semua']]
            }),
            valueField: 'Id',
            displayField: 'displayText',
            value:1,
            disabled:true,
            listeners:{
                'select': function(a,b,c){
                    selectSetUmum=b.data.displayText ;
                }
            }
        });
        return $this.ComboBox.combo0;
    },
    getComboOrderBy:function(){
        var $this=this;
        $this.ComboBox.comboOrderBy = new Ext.form.ComboBox({
            x:300,
            y:3,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: 'Order By',
            width:150,
            store: new Ext.data.ArrayStore({
                id: 0,
                fields:['Id','displayText'],
                data: [[1, 'Nama Unit'],[2, 'Nama Pasien'],[3, 'Kwitansi'],[4, 'Penjamin']]
            }),
            valueField: 'Id',
            displayField: 'displayText',
            value:1,
            listeners:{
                'select': function(a,b,c){
                    selectSetUmum=b.data.displayText ;
                }
            }
        });
        return $this.ComboBox.comboOrderBy;
    },
    getCombo1:function(){
        var $this=this;
        var Field = ['KD_CUSTOMER','CUSTOMER'];
        $this.DataStore.combo1 = new WebApp.DataStore({fields: Field});
        $this.DataStore.combo1.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboLookupCustomer',
                param: 'jenis_cust=0 ORDER BY CUSTOMER '
            }
        });
        $this.ComboBox.combo1 = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            hidden:true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            width:100,
            store: $this.DataStore.combo1,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            value:'Semua',
            listeners:{
                'select': function(a,b,c){
                }
            }
        });
        return $this.ComboBox.combo1;
    },
    getCombo2:function(){
        var $this=this;
        var Field = ['KD_CUSTOMER','CUSTOMER'];
        $this.DataStore.combo2 = new WebApp.DataStore({fields: Field});
        $this.DataStore.combo2.load({
            params:{
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboLookupCustomer',
                param: 'jenis_cust=1 ORDER BY CUSTOMER '
            }
        });
        $this.ComboBox.combo2 = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: '',
            align: 'Right',
            hidden:true,
            store: $this.DataStore.combo2,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            value:'Semua',
            width:100,
            listeners:{
                'select': function(a,b,c){
                }
            }
        });
        return $this.ComboBox.combo2;
    },
    seconGridPenerimaan : function(){
        
        var secondGrid;
        secondGridStoreLapKwitansi = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStoreLapKwitansi,
                    columns          : cols,
                    autoScroll       : true,
                    columnLines      : true,
                    border           : true,
                    enableDragDrop   : true,
                    enableDragDrop   : true,
                    height           : 130,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_UNIT',
                    title            : 'Unit yang dipilih',
                    listeners : {
                        afterrender : function(comp) {
                            var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                            var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                                    ddGroup    : 'secondGridDDGroup',
                                    notifyDrop : function(ddSource, e, data){
                                            var records =  ddSource.dragData.selections;
                                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                            secondGrid.store.add(records);
                                            secondGrid.store.sort('KD_UNIT', 'ASC');
                                            return true
                                    }
                            });
                        }
                    },
                viewConfig: 
                {
                        forceFit: true
                }
        });
        return secondGrid;
    },
    firstGridPenerimaan : function(){
        var dataSource_unit;
        var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
        dataSource_unit         = new WebApp.DataStore({fields: Field_poli_viDaftar});
        dataSource_unit.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnitB',
                    param: "kd_bagian='7' and type_unit=false "
                }
            }
        );
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 130,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'KD_UNIT',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA_UNIT',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
            listeners : {
                afterrender : function(comp) {
                    var secondGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                }
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid;
    },
    datapaymentfirstgrid:function(jenis_pay){
        dataSource_payment.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'KD_PAY',
                    Sortdir: 'ASC',
                    target:'ViewListPayment',
                    param: jenis_pay
                }
            }
        );
    },
    firstGridPayment : function(){
        var $this=this;
        var Field_payment = ['KD_PAY','URAIAN'];
        dataSource_payment         = new WebApp.DataStore({fields: Field_payment});
        $this.datapaymentfirstgrid('');
        firstGridPayment = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroupPayment',
            store            : dataSource_payment,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 130,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Payment',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
            (
                [
                new Ext.grid.RowNumberer(),
                    {
                        id: 'colKD_pay',
                        header: 'Kode Pay',
                        dataIndex: 'KD_PAY',
                        sortable: true,
                        hidden : true
                    },
                    {
                        id: 'colKD_uraian',
                        header: 'Uraian',
                        dataIndex: 'URAIAN',
                        sortable: true,
                        width: 50
                    }
                ]
            ),   
            listeners : {
                afterrender : function(comp) {
                    var secondGridDropTargetEl = firstGridPayment.getView().scroller.dom;
                    var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroupPayment',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGridPayment.store.add(records);
                                    firstGridPayment.store.sort('KD_PAY', 'ASC');
                                    return true
                            }
                    });
                }
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGridPayment;
    },
    seconGridPayment : function(){
        //var secondGridStoreLapKwitansiPaymentLapKwitansi;
        var secondGridPayment;
        secondGridStoreLapKwitansiPaymentLapKwitansi = new Ext.data.JsonStore({
            fields : fieldsPayment,
            root   : 'records'
        });

        // create the destination Grid
            secondGridPayment = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroupPayment',
                    store            : secondGridStoreLapKwitansiPaymentLapKwitansi,
                    columns          : colsPayment,
                    autoScroll       : true,
                    columnLines      : true,
                    border           : true,
                    enableDragDrop   : true,
                    enableDragDrop   : true,
                    height           : 130,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_PAY',
                    title            : 'Payment yang dipilih',
                    listeners : {
                        afterrender : function(comp) {
                            var secondGridDropTargetEl = secondGridPayment.getView().scroller.dom;
                            var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                                    ddGroup    : 'secondGridDDGroupPayment',
                                    notifyDrop : function(ddSource, e, data){
                                            var records =  ddSource.dragData.selections;
                                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                            secondGridPayment.store.add(records);
                                            secondGridPayment.store.sort('KD_PAY', 'ASC');
                                            return true
                                    }
                            });
                        }
                    },
                viewConfig: 
                {
                        forceFit: true
                }
        });
        return secondGridPayment;
    },
    getCombo3:function(){
        var $this=this;
        var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

        $this.DataStore.combo3 = new WebApp.DataStore({fields: Field_poli_viDaftar});

        $this.DataStore.combo3.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER "
            }
        });
        $this.ComboBox.combo3 = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: '',
            align: 'Right',
            width:100,
            hidden:true,
            store: $this.DataStore.combo3,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            value:'Semua',
//          value: 0,
            listeners:{
                'select': function(a,b,c){
//                  selectSetAsuransi=b.data.KD_CUSTOMER ;
//                  selectsetnamaAsuransi = b.data.CUSTOMER ;
                }
            }
        });
        return $this.ComboBox.combo3;
    },
    init:function(){
        var $this=this;
        $this.Window.main=new Ext.Window({
            width: 600,
            height:360,
            modal:true,
            title:'Laporan Batal Kunjungan',
            layout:'fit',
            items:[
                new Ext.Panel({
                    bodyStyle:'padding: 6px',
                    layout:'form',
                    border:false,
                    autoScroll: true,
                    fbar:[
                        new Ext.Button({
                            text:'Ok',
                            handler:function(){
                                $this.initPrint()
                            }
                        }),
                        new Ext.Button({
                            text:'Batal',
                            handler:function(){
                                $this.Window.main.close();
                            }
                        })
                    ],
                    items:[
                        {
                            xtype : 'fieldset',
                            title : 'Poliklinik',
                            layout:'column',
                            border:true,
                            height: 170,
                            // bodyStyle:'margin-top: 2px',
                            items:[
                                {
                                    border:false,
                                    columnWidth:.45,
                                    // bodyStyle:'margin-top: 2px',
                                    items:[
                                        $this.firstGridPenerimaan(),
                                    ]
                                },
                                {
                                    border:false,
                                    columnWidth:.1,
                                    bodyStyle:'margin-top: 2px',
                                },
                                {
                                    border:false,
                                    columnWidth:.45,
                                    // bodyStyle:'margin-top: 2px; align:right;',
                                    items:[
                                        $this.seconGridPenerimaan(),
                                    ]
                                }
                            ]
                        },{
                            // xtype : 'fieldset',
                            // title : '',
                            layout: 'absolute',
                            border: true,
                            height:30,
                            items:[
                                {   
                                    x:20,
                                    y:4,
                                    xtype: 'checkbox',
                                    id: 'CekLapPilihSemuaCetakBatalKunjungan',
                                    hideLabel:false,
                                    boxLabel: 'Pilih Semua Unit',
                                    checked: false,
                                    listeners: 
                                    {
                                        check: function()
                                        {
                                           if(Ext.getCmp('CekLapPilihSemuaCetakBatalKunjungan').getValue()===true)
                                            {
                                                firstGrid.getSelectionModel().selectAll();
                                            }
                                            else
                                            {
                                                firstGrid.getSelectionModel().clearSelections();
                                            }
                                        }
                                   }
                                },
                                {
                                    border:false,
                                    columnWidth:.2,
                                    bodyStyle:'margin-top: 2px',
                                },
                                {   
                                    x:300,
                                    y:4,
                                    xtype: 'checkbox',
                                    id: 'CetakExcelCetakBatalKunjungan',
                                    hideLabel:false,
                                    boxLabel: 'Cetak Excel',
                                    checked: false,
                                    listeners: 
                                    {
                                        
                                   }
                                },
                            ]
                        },
                        {
                            layout: 'column',
                            border: false,
                            bodyStyle:'margin-top: 2px',
                            height:50,
                            width : 550,
                            items:[
                                {
                                    xtype : 'fieldset',
                                    title : 'Periode',
                                    layout: 'column',
                                    width : 275,
                                    bodyStyle:'padding: 2px 0px  7px 15px',
                                    border: true,
                                    items:[
                                        
                                        $this.DateField.startDate=new Ext.form.DateField({
                                            value: new Date(),
                                            width : 100,
                                            format:'d/M/Y'
                                        }),
                                        {
                                            xtype:'displayfield',
                                            width: 30,
                                            value:'&nbsp;s/d&nbsp;'
                                        },
                                        $this.DateField.endDate=new Ext.form.DateField({
                                            value: new Date(),
                                            width : 100,
                                            format:'d/M/Y'
                                        })
                                    ]
                                },
                                {
                                    xtype:'displayfield',
                                    width: 30,
                                    value:'&nbsp;&nbsp;'
                                },
                                {
                                    xtype : 'fieldset',
                                    title : 'Kelompok pasien',
                                    layout: 'column',
                                    border: true,
                                    bodyStyle:'padding: 3px 0px  2px 10px',
                                    width : 275,
                                    items : [
                                            $this.ComboBox.kelPasien1=new Ext.form.ComboBox({
                                                triggerAction: 'all',
                                                lazyRender:true,
                                                mode: 'local',
                                                width: 100,
                                                selectOnFocus:true,
                                                forceSelection: true,
                                                emptyText:'Silahkan Pilih...',
                                                fieldLabel: 'Pendaftaran Per Shift ',
                                                store: new Ext.data.ArrayStore({
                                                    id: 0,
                                                    fields:[
                                                            'Id',
                                                            'displayText'
                                                    ],
                                                    data: [[-1, 'Semua'],[0, 'Perseorangan'],[1, 'Perusahaan'], [2, 'Asuransi']]
                                                }),
                                                valueField: 'Id',
                                                displayField: 'displayText',
                                                value:'Semua',
                                                listeners:{
                                                    'select': function(a,b,c){
                                                            $this.vars.comboSelect=b.data.displayText;
                                                            $this.comboOnSelect(b.data.Id);
                                                    }
                                                }
                                            }),
                                            {
                                                xtype:'displayfield',
                                                width: 10,
                                                value:'&nbsp;'
                                            },
                                            $this.getCombo0(),
                                            $this.getCombo1(),
                                            $this.getCombo2(),
                                            $this.getCombo3()
                                    ]
                                }
                            ]
                        },{
                            // xtype : 'fieldset',
                            // title : 'Kelompok pasien',
                            layout: 'column',
                            border: false,
                            bodyStyle:'margin-top: 2px',
                            //height: 60,
                            items:[
                                    // {
                                    //     xtype : 'fieldset',
                                    //     title : '',
                                    //     layout: 'column',
                                    //     border: true,
                                    //     bodyStyle:'padding: 3px 0px  2px 10px',
                                    //     width: 320,
                                    //     height: 67,
                                    //     items:[
                                    //     ]
                                    // },
                                    {layout: 'column',width : 15,
                                    //height: 55,
                                    bodyStyle:'margin: 5px',border: false},
                                    /*{
                                        xtype : 'fieldset',
                                        title : 'Shift',
                                        layout:'column',
                                        border: true,
                                        bodyStyle:'padding: 2px 0px  2px 10px',
                                        width : 270,
                                        //height: 60,
                                        items:[
                                            $this.CheckboxGroup.shift=new Ext.form.CheckboxGroup({
                                                xtype: 'checkboxgroup',
                                                items: [
                                                    {boxLabel: 'Semua',checked:true,name: 'Shift_All_LabRegis',id : 'Shift_All_LabRegis',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_LabRegis').setValue(true);Ext.getCmp('Shift_2_LabRegis').setValue(true);Ext.getCmp('Shift_3_LabRegis').setValue(true);Ext.getCmp('Shift_1_LabRegis').disable();Ext.getCmp('Shift_2_LabRegis').disable();Ext.getCmp('Shift_3_LabRegis').disable();}else{Ext.getCmp('Shift_1_LabRegis').setValue(false);Ext.getCmp('Shift_2_LabRegis').setValue(false);Ext.getCmp('Shift_3_LabRegis').setValue(false);Ext.getCmp('Shift_1_LabRegis').enable();Ext.getCmp('Shift_2_LabRegis').enable();Ext.getCmp('Shift_3_LabRegis').enable();}}},
                                                    {boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1_LabRegis',id : 'Shift_1_LabRegis'},
                                                    {boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2_LabRegis',id : 'Shift_2_LabRegis'},
                                                    {boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3_LabRegis',id : 'Shift_3_LabRegis'}
                                                ]
                                            })
                                        ]
                                    }*/
                            ]
                        },
                        
                        /*{
                            xtype : 'fieldset',
                            title : 'Payment',
                            layout:'column',
                            border:true,
                            height: 170,
                            // bodyStyle:'margin-top: 2px',
                            items:[
                                {
                                    border:false,
                                    columnWidth:.45,
                                    // bodyStyle:'margin-top: 2px',
                                    items:[
                                        $this.firstGridPayment(),
                                    ]
                                },
                                {
                                    border:false,
                                    columnWidth:.1,
                                    bodyStyle:'margin-top: 2px',
                                },
                                {
                                    border:false,
                                    columnWidth:.45,
                                    // bodyStyle:'margin-top: 2px; align:right;',
                                    items:[
                                        $this.seconGridPayment(),
                                    ]
                                }
                            ]
                        },
                        {
                            layout: 'absolute',
                            border: true,
                            height: 30,
                            items:[
                                {                                  
                                    x:20,
                                    y:3,
                                    xtype: 'checkbox',
                                    id: 'CekLapPilihSemuaPaymentCetakBatalKunjungan',
                                    hideLabel:false,
                                    boxLabel: 'Pilih Semua Payment',
                                    checked: false,
                                    listeners: 
                                    {
                                        check: function()
                                        {
                                           if(Ext.getCmp('CekLapPilihSemuaPaymentCetakBatalKunjungan').getValue()===true)
                                            {
                                                firstGridPayment.getSelectionModel().selectAll();
                                            }
                                            else
                                            {
                                                firstGridPayment.getSelectionModel().clearSelections();
                                            }
                                        }
                                   }
                                },
                                {                                  
                                    x:220,
                                    y:4,
                                    xtype: 'label',
                                    text:'Order by:'
                                    
                                },                                      
                                    $this.getComboOrderBy()
                                
                            ]
                        }*/
                    ]
                })
            ]
        }).show();
    }
};
DlgLapCetakBatalKunjungan.init();
