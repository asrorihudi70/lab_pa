var type_file=0;
var dsRADBukaTransaksi;
var selectRADBukaTransaksi;
var selectNamaRADBukaTransaksi;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRADBukaTransaksi;
var varLapRADBukaTransaksi= ShowFormLapRADBukaTransaksi();
var dsRAD;

function ShowFormLapRADBukaTransaksi()
{
    frmDlgRADBukaTransaksi= fnDlgRADBukaTransaksi();
    frmDlgRADBukaTransaksi.show();
};

function fnDlgRADBukaTransaksi()
{
    var winRADBukaTransaksiReport = new Ext.Window
    (
        {
            id: 'winRADBukaTransaksiReport',
            title: 'Laporan Histori Buka Transaksi',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRADBukaTransaksi()]

        }
    );

    return winRADBukaTransaksiReport;
};

function ItemDlgRADBukaTransaksi()
{
    var PnlLapRADBukaTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapRADBukaTransaksi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRADBukaTransaksi_Periode(),
				//getItemRADBukaTransaksi_Shift(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRADBukaTransaksi',
					handler: function()
					{
					   if (ValidasiReportRADBukaTransaksi() === 1)
					   {
						
							var params={
												tglAwal:Ext.getCmp('dtpTglAwalLapRADBukaTransaksi').getValue(),
												tglAkhir:Ext.getCmp('dtpTglAkhirLapRADBukaTransaksi').getValue(),
												type_file:type_file,
												
									} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/fisiotherapy/lap_fisiotherapy/cetak_laporan_buka_transaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRADBukaTransaksi.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLapRADBukaTransaksi').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapRADBukaTransaksi').getValue()+'#aje#Laporan Buka Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionRAD/cetakRADBukaTransaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRADBukaTransaksi.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRADBukaTransaksi',
					handler: function()
					{
						frmDlgRADBukaTransaksi.close();
					}
				}
			
			]
        }
    );

    return PnlLapRADBukaTransaksi;
};


function ValidasiReportRADBukaTransaksi()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRADBukaTransaksi').dom.value === '')
	{
		ShowPesanWarningRADBukaTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapRADBukaTransaksi').getValue() === false && Ext.getCmp('Shift_1_LapRADBukaTransaksi').getValue() === false && Ext.getCmp('Shift_2_LapRADBukaTransaksi').getValue() === false && Ext.getCmp('Shift_3_LapRADBukaTransaksi').getValue() === false){
		ShowPesanWarningRADBukaTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportRADBukaTransaksi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRADBukaTransaksi').dom.value > Ext.get('dtpTglAkhirLapRADBukaTransaksi').dom.value)
    {
        ShowPesanWarningRADBukaTransaksiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRADBukaTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRADBukaTransaksi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRADBukaTransaksi_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRADBukaTransaksi',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					{
					   xtype: 'checkbox',
					   fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					}
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRADBukaTransaksi',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


