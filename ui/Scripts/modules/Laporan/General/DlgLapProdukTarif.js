var type_file=0;
var dsRWJprodukTarif;
var selectRWJprodukTarif;
var selectNamaRWJprodukTarif;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJprodukTarif;
var varLapRWJprodukTarif= ShowFormLapRWJprodukTarif();
var dsRWJ;

function ShowFormLapRWJprodukTarif()
{
    frmDlgRWJprodukTarif= fnDlgRWJprodukTarif();
    frmDlgRWJprodukTarif.show();
};

function fnDlgRWJprodukTarif()
{
    var winRWJprodukTarifReport = new Ext.Window
    (
        {
            id: 'winRWJprodukTarifReport',
            title: 'Laporan Produk Tarif Per Kelas',
            closeAction: 'destroy',
            width:200,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJprodukTarif()]

        }
    );

    return winRWJprodukTarifReport;
};

function ItemDlgRWJprodukTarif()
{
    var PnlLapRWJprodukTarif = new Ext.Panel
    (
        {
            id: 'PnlLapRWJprodukTarif',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 100,
            bodyStyle: 'padding-top:7px;',
            border: true,
            items:
            [
                getItemLapRWJprodukTarif_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnPreviewLapRWJprodukTarif',
					handler: function()
					{
						if (ValidasiReportRWJprodukTarif() === 1)
						{
							var params={
								/* tglAwal:Ext.getCmp('dtpTglAwalLapRWJprodukTarif').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJprodukTarif').getValue(), */
								type_file:type_file
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/main/functionlapDataTarif/cetak");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJprodukTarif.close(); 
						};
					}
				},
				
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJprodukTarif',
					handler: function()
					{
						frmDlgRWJprodukTarif.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJprodukTarif;
};


function ValidasiReportRWJprodukTarif()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJprodukTarif').dom.value === '')
	{
		ShowPesanWarningRWJprodukTarifReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}

    return x;
};

function ValidasiTanggalReportRWJprodukTarif()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJprodukTarif').dom.value > Ext.get('dtpTglAkhirLapRWJprodukTarif').dom.value)
    {
        ShowPesanWarningRWJprodukTarifReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJprodukTarifReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJprodukTarif_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJprodukTarif_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .99,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJprodukTarif',
						format: 'd-M-Y',
						value:now,
						anchor: '99%',
						hidden:true
					},
					{
					   xtype: 'checkbox',
					   fieldLabel: '',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					}
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				hidden:true,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
				hidden:true,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJprodukTarif',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


