var type_file=0;
var ds_IndexPekerjaan;
var select_IndexPekerjaan;
var selectNama_IndexPekerjaan;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlg_IndexPekerjaan;
var varLap_IndexPekerjaan= ShowFormLap_IndexPekerjaan();
var ds;
var DataStorePekerjaan_index_;

function ShowFormLap_IndexPekerjaan()
{
    frmDlg_IndexPekerjaan= fnDlg_IndexPekerjaan();
    frmDlg_IndexPekerjaan.show();
};

function fnDlg_IndexPekerjaan()
{
    var win_IndexPekerjaanReport = new Ext.Window
    (
        {
            id: 'win_IndexPekerjaanReport',
            title: 'Laporan Index Pekerjaan',
            closeAction: 'destroy',
            width:450,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlg_IndexPekerjaan()]

        }
    );

    return win_IndexPekerjaanReport;
};

function ItemDlg_IndexPekerjaan()
{
    var PnlLap_IndexPekerjaan = new Ext.Panel
    (
        {
            id: 'PnlLap_IndexPekerjaan',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
				mComboBagian(),
                getItemLap_IndexPekerjaan_Periode(),
				//getItem_IndexPekerjaan_Shift(),
				mComboPekerjaan(),
				{
				   xtype: 'checkbox',
				   fieldLabel: 'Type ',
				   id: 'CekLapPilihTypeExcel',
				   hideLabel:false,
				   boxLabel: 'Excel',
				   checked: false,
				   listeners: 
				   {
						check: function()
						{
						   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
							{
								type_file=1;
							}
							else
							{
								type_file=0;
							}
						}
				   }
				},
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLap_IndexPekerjaan',
					handler: function(){
					   if (ValidasiReport_IndexPekerjaan() === 1){
							var params={
								tgl_awal 	: Ext.getCmp('dtpTglAwalLap_IndexPekerjaan').getValue(),
								tgl_akhir 	: Ext.getCmp('dtpTglAkhirLap_IndexPekerjaan').getValue(),
								type_file 	: type_file,
								bagian 		: Ext.getCmp('cboBagian').getValue(),
								pekerjaan 	: Ext.getCmp('cboPekerjaan').getValue(),
							};
							console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/main/function_lap/lap_index_Pekerjaan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							// frmDlg_IndexPekerjaan.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLap_IndexPekerjaan').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLap_IndexPekerjaan').getValue()+'#aje#Laporan Batal Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/function/cetak_IndexPekerjaan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlg_IndexPekerjaan.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLap_IndexPekerjaan',
					handler: function()
					{
						frmDlg_IndexPekerjaan.close();
					}
				}
			
			]
        }
    );

    return PnlLap_IndexPekerjaan;
};


function ValidasiReport_IndexPekerjaan()
{
    var x=1;
	if(Ext.get('dtpTglAwalLap_IndexPekerjaan').dom.value === '')
	{
		ShowPesanWarning_IndexPekerjaanReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_Lap_IndexPekerjaan').getValue() === false && Ext.getCmp('Shift_1_Lap_IndexPekerjaan').getValue() === false && Ext.getCmp('Shift_2_Lap_IndexPekerjaan').getValue() === false && Ext.getCmp('Shift_3_Lap_IndexPekerjaan').getValue() === false){
		ShowPesanWarning_IndexPekerjaanReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReport_IndexPekerjaan()
{
    var x=1;
    if(Ext.get('dtpTglAwalLap_IndexPekerjaan').dom.value > Ext.get('dtpTglAkhirLap_IndexPekerjaan').dom.value)
    {
        ShowPesanWarning_IndexPekerjaanReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarning_IndexPekerjaanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLap_IndexPekerjaan_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLap_IndexPekerjaan_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			// {
			// 	columnWidth: .49,
			// 	layout: 'form',
			// 	labelWidth:70,
			// 	border: false,
			// 	items:
			// 	[
			// 	]
			// },
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:100,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLap_IndexPekerjaan',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLap_IndexPekerjaan',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};



function mComboBagian()
{
    var cboBagian = new Ext.form.ComboBox
	(
		{
			/*x: 120,
			y: 220,*/
			id:'cboBagian',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Bagian ',
			// width:240,
			anchor:'99%',
			value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 'Rawat Inap'],[2, 'Rawat Jalan'],[3, 'Gawat Darurat']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
		}
	);
	return cboBagian;
};

function mComboPekerjaan()
{
	DataStorePekerjaan_index_ = new Ext.data.JsonStore();
	loaddatastorePekerjaan();
	DataStorePekerjaan_index_.insert(60,new Ext.data.Record({
		KD_PEKERJAAN 	: 'SEMUA' ,
		PEKERJAAN 		: 'SEMUA'
	}));
    var cboOrderBy = new Ext.form.ComboBox
	(
		{
			id:'cboPekerjaan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Pekerjaan',
			anchor:'99%',
			store: DataStorePekerjaan_index_,
			valueField: 'KD_PEKERJAAN',
			displayField: 'PEKERJAAN',
		}
	);
	return cboOrderBy;
};


function loaddatastorePekerjaan(){
	var Field = ['KD_PEKERJAAN','PEKERJAAN'];
	var dsPekerjaanField = new WebApp.DataStore({fields: Field});
    Ext.Ajax.request({
        url: baseURL +  "index.php/general/pekerjaan/combo",
        params: {
            null : null,
        },
        success: function(response) {
            var cst  = Ext.decode(response.responseText);
            console.log(cst);
            for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
                var recs    = [],recType = dsPekerjaanField.recordType;
                var o       = cst['ListDataObj'][i];
                recs.push(new recType(o));
                DataStorePekerjaan_index_.add(recs);
            }
        },
    });
}