var type_file=0;
var ds_IndexPendidikan;
var select_IndexPendidikan;
var selectNama_IndexPendidikan;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlg_IndexPendidikan;
var varLap_IndexPendidikan= ShowFormLap_IndexPendidikan();
var ds;
var DataStorePendidikan_index_;

function ShowFormLap_IndexPendidikan()
{
    frmDlg_IndexPendidikan= fnDlg_IndexPendidikan();
    frmDlg_IndexPendidikan.show();
};

function fnDlg_IndexPendidikan()
{
    var win_IndexPendidikanReport = new Ext.Window
    (
        {
            id: 'win_IndexPendidikanReport',
            title: 'Laporan Index Pendidikan',
            closeAction: 'destroy',
            width:450,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlg_IndexPendidikan()]

        }
    );

    return win_IndexPendidikanReport;
};

function ItemDlg_IndexPendidikan()
{
    var PnlLap_IndexPendidikan = new Ext.Panel
    (
        {
            id: 'PnlLap_IndexPendidikan',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
				mComboBagian(),
                getItemLap_IndexPendidikan_Periode(),
				//getItem_IndexPendidikan_Shift(),
				mComboPendidikan(),
				{
				   xtype: 'checkbox',
				   fieldLabel: 'Type ',
				   id: 'CekLapPilihTypeExcel',
				   hideLabel:false,
				   boxLabel: 'Excel',
				   checked: false,
				   listeners: 
				   {
						check: function()
						{
						   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
							{
								type_file=1;
							}
							else
							{
								type_file=0;
							}
						}
				   }
				},
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLap_IndexPendidikan',
					handler: function(){
					   if (ValidasiReport_IndexPendidikan() === 1){
							var params={
								tgl_awal 	: Ext.getCmp('dtpTglAwalLap_IndexPendidikan').getValue(),
								tgl_akhir 	: Ext.getCmp('dtpTglAkhirLap_IndexPendidikan').getValue(),
								type_file 	: type_file,
								bagian 		: Ext.getCmp('cboBagian').getValue(),
								pendidikan 	: Ext.getCmp('cboPendidikan').getValue(),
							};
							console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/main/function_lap/lap_index_pendidikan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							// frmDlg_IndexPendidikan.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLap_IndexPendidikan').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLap_IndexPendidikan').getValue()+'#aje#Laporan Batal Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/function/cetak_IndexPendidikan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlg_IndexPendidikan.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLap_IndexPendidikan',
					handler: function()
					{
						frmDlg_IndexPendidikan.close();
					}
				}
			
			]
        }
    );

    return PnlLap_IndexPendidikan;
};


function ValidasiReport_IndexPendidikan()
{
    var x=1;
	if(Ext.get('dtpTglAwalLap_IndexPendidikan').dom.value === '')
	{
		ShowPesanWarning_IndexPendidikanReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_Lap_IndexPendidikan').getValue() === false && Ext.getCmp('Shift_1_Lap_IndexPendidikan').getValue() === false && Ext.getCmp('Shift_2_Lap_IndexPendidikan').getValue() === false && Ext.getCmp('Shift_3_Lap_IndexPendidikan').getValue() === false){
		ShowPesanWarning_IndexPendidikanReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReport_IndexPendidikan()
{
    var x=1;
    if(Ext.get('dtpTglAwalLap_IndexPendidikan').dom.value > Ext.get('dtpTglAkhirLap_IndexPendidikan').dom.value)
    {
        ShowPesanWarning_IndexPendidikanReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarning_IndexPendidikanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLap_IndexPendidikan_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLap_IndexPendidikan_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			// {
			// 	columnWidth: .49,
			// 	layout: 'form',
			// 	labelWidth:70,
			// 	border: false,
			// 	items:
			// 	[
			// 	]
			// },
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:100,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLap_IndexPendidikan',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLap_IndexPendidikan',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};



function mComboBagian()
{
    var cboBagian = new Ext.form.ComboBox
	(
		{
			/*x: 120,
			y: 220,*/
			id:'cboBagian',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Bagian ',
			// width:240,
			anchor:'99%',
			value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 'Rawat Inap'],[2, 'Rawat Jalan'],[3, 'Gawat Darurat']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
		}
	);
	return cboBagian;
};

function mComboPendidikan()
{
	DataStorePendidikan_index_ = new Ext.data.JsonStore();
	loaddatastorePendidikan();
	DataStorePendidikan_index_.insert(60,new Ext.data.Record({
		KD_PENDIDIKAN 	: 'SEMUA' ,
		PENDIDIKAN 		: 'SEMUA'
	}));
    var cboOrderBy = new Ext.form.ComboBox
	(
		{
			id:'cboPendidikan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Pendidikan',
			anchor:'99%',
			store: DataStorePendidikan_index_,
			valueField: 'KD_PENDIDIKAN',
			displayField: 'PENDIDIKAN',
		}
	);
	return cboOrderBy;
};


function loaddatastorePendidikan(){
	var Field = ['KD_PENDIDIKAN','PENDIDIKAN'];
	var dsPendidikanField = new WebApp.DataStore({fields: Field});
    Ext.Ajax.request({
        url: baseURL +  "index.php/general/pendidikan/combo",
        params: {
            null : null,
        },
        success: function(response) {
            var cst  = Ext.decode(response.responseText);
            console.log(cst);
            for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
                var recs    = [],recType = dsPendidikanField.recordType;
                var o       = cst['ListDataObj'][i];
                recs.push(new recType(o));
                DataStorePendidikan_index_.add(recs);
            }
        },
    });
}