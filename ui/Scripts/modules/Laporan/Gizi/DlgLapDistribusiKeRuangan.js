
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapDistribusiKeRuangan_Gizi;
var selectNamaLapDistribusiKeRuangan_Gizi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapDistribusiKeRuangan_Gizi;
var varLapDistribusiKeRuangan_Gizi= ShowFormLapDistribusiKeRuangan_Gizi();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var jenis='2';

function ShowFormLapDistribusiKeRuangan_Gizi()
{
    frmDlgLapDistribusiKeRuangan_Gizi= fnDlgLapDistribusiKeRuangan_Gizi();
    frmDlgLapDistribusiKeRuangan_Gizi.show();
};

function fnDlgLapDistribusiKeRuangan_Gizi()
{
    var winLapDistribusiKeRuangan_GiziReport = new Ext.Window
    (
        {
            id: 'winLapDistribusiKeRuangan_GiziReport',
            title: 'Laporan Distribusi Ke Ruangan',
            closeAction: 'destroy',
            width: 405,
            height: 240,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapDistribusiKeRuangan_Gizi()],
			fbar:
			[
				{
					xtype: 'button',
					width: 70,
					name : 'Ok',
					text : 'Preview',
					id: 'btnOkLapDistribusiKeRuangan_Gizi',
					handler: function()
					{
						var params={
							tglAwal:Ext.getCmp('dtpTglAwalRekapPeruang_gizi').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirRekapPeruang_gizi').getValue(),
							kdUnit:Ext.getCmp('cbo_UnitLapDistribusiRuangan').getValue(),
							kdWaktu:Ext.getCmp('cbo_WaktuLapDistribusiRuangan').getValue(),
							jenis:jenis
						};
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/gizi/lap_distribusiruang/cetakdistribusiruang");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();
						frmDlgLapDistribusiKeRuangan_Gizi.close();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapDistribusiKeRuangan_Gizi',
					handler: function()
					{
							frmDlgLapDistribusiKeRuangan_Gizi.close();
					}
				},
				/* {
					xtype : 'button',
					text : 'Tes'
				} */
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapDistribusiKeRuangan_GiziReport;
};


function ItemDlgLapDistribusiKeRuangan_Gizi()
{
    var PnlLapDistribusiKeRuangan_Gizi = new Ext.Panel
    (
        {
            id: 'PnlLapDistribusiKeRuangan_Gizi',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					xtype: 'fieldset',
					title: '',
					items: 
					[
						getItemLapDistribusiKeRuangan_Gizi_KelPasien(),
						getItemLapDistribusiKeRuangan_Gizi_Tanggal(),
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 0 0 0' },
							style:{'margin-left':'30px','margin-top':'0px'},
							anchor: '94%',
							layoutConfig:
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							}
						}
					]
			}
            ]
        }
    );

    return PnlLapDistribusiKeRuangan_Gizi;
};

function GetCriteriaLapDistribusiKeRuangan_Gizi()
{
	var strKriteria = '';
	
	/* strKriteria = '';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalRekapPeruang_gizi').getValue();
	strKriteria += '##@@##' + Ext.get('dtpTglAkhirRekapPeruang_gizi').getValue();
	
	if (Ext.get('cboPilihanLapDistribusiKeRuangan_Gizi').getValue() !== '' || Ext.get('cboPilihanLapDistribusiKeRuangan_Gizi').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'asal pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanLapDistribusiKeRuangan_Gizi').getValue();
	}
	if (Ext.get('cboUnitFarLapDistribusiKeRuangan_Gizi').getValue() !== '' || Ext.get('cboUnitFarLapDistribusiKeRuangan_Gizi').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + Ext.get('cboUnitFarLapDistribusiKeRuangan_Gizi').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitFarLapDistribusiKeRuangan_Gizi').getValue();
	}
	if (Ext.get('cboUserRequestEntryLapDistribusiKeRuangan_Gizi').getValue() !== ''|| Ext.get('cboUserRequestEntryLapDistribusiKeRuangan_Gizi').getValue() !== 'Pilih User...'){
		strKriteria += '##@@##' + 'operator';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryLapDistribusiKeRuangan_Gizi').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryLapDistribusiKeRuangan_Gizi').getValue();
	}
	
	if (Ext.getCmp('Shift_All_LapDistribusiKeRuangan_Gizi').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_LapDistribusiKeRuangan_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_LapDistribusiKeRuangan_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_LapDistribusiKeRuangan_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	} */
	
	
	return strKriteria;
};

function ValidasiReportLapDistribusiKeRuangan_Gizi()
{
	var x=1;
	if(Ext.get('cboUnitFarLapDistribusiKeRuangan_Gizi').getValue() === ''|| Ext.get('cboUnitFarLapDistribusiKeRuangan_Gizi').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapDistribusiKeRuangan_GiziReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapDistribusiKeRuangan_Gizi').getValue() === '' || Ext.get('cboUserRequestEntryLapDistribusiKeRuangan_Gizi').getValue() == 'Pilih User...'){
		ShowPesanWarningLapDistribusiKeRuangan_GiziReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalRekapPeruang_gizi').dom.value > Ext.get('dtpTglAkhirRekapPeruang_gizi').dom.value)
    {
        ShowPesanWarningLapDistribusiKeRuangan_GiziReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }

    return x;
};

function getItemLapDistribusiKeRuangan_Gizi_KelPasien()
{
	radios = new Ext.form.RadioGroup({
	id :'radiosa',
	hidden : true,
	columns  : 4,
	items: 
	[
		{
			id:'rbUmumDistribusi',
			name: 'rbKelompok',
			boxLabel: 'Umum', 
			inputValue: "1"
		},
		{
			boxLabel: 'Pasien',
			id:'rbPasienDistribusi', 
			name: 'rbKelompok',
			inputValue: "2",
			checked: true,
		},
		{
			boxLabel: 'Kel. Pasein',
			id:'rbKelPasienDistribusi', 
			name: 'rbKelompok',
			inputValue: "3"
		},
		{
			boxLabel: 'Karyawan',
			id:'rbKaryawanDistribusi', 
			name: 'rbKelompok',
			inputValue: "4"
		}
	],
	listeners: {
		change : function()
		{ 
			jenis= radios.getValue().inputValue;
		
		}
	}
		
   });
    var itemss = {
        layout: 'column',
        border: false,
        items: 
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  355,
				height: 100,
				items: [
					radios,
					{
						x: 10,
						y: 30,
						xtype: 'label',
						text: 'Unit'
					}, 
					{
						x: 90,
						y: 30,
						xtype: 'label',
						text: ' : '
					},
					ComboUnitLapDistribusiRuangan(),
					{
						x: 10,
						y: 60,
						xtype: 'label',
						text: 'Waktu Makan'
					}, 
					{
						x: 90,
						y: 60,
						xtype: 'label',
						text: ' : '
					},
					ComboWaktuLapDistribusiRuangan(),
				]
			}
		]
    };
    return itemss;
};

function getItemLapDistribusiKeRuangan_Gizi_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  355,
				height: 35,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 0,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 0,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTglAwalRekapPeruang_gizi',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 0,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTglAkhirRekapPeruang_gizi',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};

function getItemLapDistribusiKeRuangan_Gizi_WaktuMakan()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Waktu Makan'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					
				]
			}
		]
    };
    return items;
};

function ComboUnitLapDistribusiRuangan()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unitLapDistribusiRuangan = new WebApp.DataStore({fields: Field_Vendor});
    ds_unitLapDistribusiRuangan.load
	(
		{
			/* params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitApotek',
					param: "parent='100' ORDER BY nama_unit"
				} */
				params:{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitGizi',
					param: "parent='1001'"
				}
		}
	);
	
    var cbo_UnitLapDistribusiRuangan = new Ext.form.ComboBox
    (
        {
			x: 100,
			y: 30,
			id: 'cbo_UnitLapDistribusiRuangan',
            fieldLabel: 'Unit',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			store: ds_unitLapDistribusiRuangan,
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					//selectSetUnit=b.data.valueField;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						/* tmpkriteria = getCriteriaCariApotekResepRWJ();
						refeshRespApotekRWJ(tmpkriteria); */
					} 						
				}
			}
        }
    )    
    return cbo_UnitLapDistribusiRuangan;
}

function ComboWaktuLapDistribusiRuangan()
{
    var Field_WaktuGzDistribusiPasien = ['kd_waktu', 'waktu'];
    ds_WaktuLapDistribusiRuangan = new WebApp.DataStore({fields: Field_WaktuGzDistribusiPasien});
    ds_WaktuLapDistribusiRuangan.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sortdir: 'ASC',
					target: 'ComboWaktuGizi',
					param: ""
				}
		}
	);
	
    var cbo_WaktuLapDistribusiRuangan = new Ext.form.ComboBox
    (
        {
			x: 100,
			y: 60,
			id: 'cbo_WaktuLapDistribusiRuangan',
			valueField: 'kd_waktu',
            displayField: 'waktu',
			store: ds_WaktuLapDistribusiRuangan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					Ext.getCmp('btnAddPasienGzDistribusiPasienL').enable();
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_WaktuLapDistribusiRuangan;
}

function ShowPesanWarningLapDistribusiKeRuangan_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
