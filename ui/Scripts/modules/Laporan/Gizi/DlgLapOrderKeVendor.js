
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapOrderVendor_Gizi;
var selectNamaLapOrderVendor_Gizi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapOrderVendor_Gizi;
var varLapOrderVendor_Gizi= ShowFormLapOrderVendor_Gizi();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;

function ShowFormLapOrderVendor_Gizi()
{
    frmDlgLapOrderVendor_Gizi= fnDlgLapOrderVendor_Gizi();
    frmDlgLapOrderVendor_Gizi.show();
};

function fnDlgLapOrderVendor_Gizi()
{
    var winLapOrderVendor_GiziReport = new Ext.Window
    (
        {
            id: 'winLapOrderVendor_GiziReport',
            title: 'Laporan Order Ke Vendor',
            closeAction: 'destroy',
            width: 405,
            height: 190,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapOrderVendor_Gizi()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkLapOrderVendor_Gizi',
					handler: function()
					{
						var params={
							tglAwal:Ext.getCmp('dtpTglAwalRekapPeruang_gizi').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirRekapPeruang_gizi').getValue(),
							kdVendor:Ext.getCmp('cbo_LapvendorGzOrderDiet').getValue()
						};
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/gizi/lap_ordervendor/cetakordervendor");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();
						frmDlgLapOrderVendor_Gizi.close();
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapOrderVendor_Gizi',
					handler: function()
					{
						frmDlgLapOrderVendor_Gizi.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapOrderVendor_GiziReport;
};


function ItemDlgLapOrderVendor_Gizi()
{
    var PnlLapOrderVendor_Gizi = new Ext.Panel
    (
        {
            id: 'PnlLapOrderVendor_Gizi',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					xtype: 'fieldset',
					title: '',
					items: 
					[
						getItemLapOrderVendor_Gizi_Vendor(),
						getItemLapOrderVendor_Gizi_Tanggal(),
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 0 0 0' },
							style:{'margin-left':'30px','margin-top':'0px'},
							anchor: '94%',
							layoutConfig:
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							}
						}
					]
			}
            ]
        }
    );

    return PnlLapOrderVendor_Gizi;
};

function getItemLapOrderVendor_Gizi_Vendor()
{
	
    var itemss = {
        layout: 'column',
        border: false,
        items: 
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width:  320,
				height: 45,
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Vendor'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					ComboVendorGzOrderDietLap()
				]
			}
		]
    };
    return itemss;
};

function getItemLapOrderVendor_Gizi_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width:  355,
				height: 35,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 0,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 0,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTglAwalRekapPeruang_gizi',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 0,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTglAkhirRekapPeruang_gizi',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};


function ComboVendorGzOrderDietLap()
{
    var Field_VendorGzOrderDiet = ['kd_vendor', 'vendor'];
    ds_VendorGzOrderDietLap = new WebApp.DataStore({fields: Field_VendorGzOrderDiet});
    ds_VendorGzOrderDietLap.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_vendor',
					Sortdir: 'ASC',
					target: 'ComboVendorGizi',
					param: ''
				}
		}
	);
	
    var cbo_LapvendorGzOrderDiet = new Ext.form.ComboBox
    (
        {
			x: 100,
			y: 10,
            flex: 1,
			id: 'cbo_LapvendorGzOrderDiet',
			valueField: 'kd_vendor',
            displayField: 'vendor',
			store: ds_VendorGzOrderDietLap,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapvendorGzOrderDiet;
};

function ShowPesanWarningLapOrderVendor_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
