
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapPenerimaanVendor_Gizi;
var selectNamaLapPenerimaanVendor_Gizi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapPenerimaanVendor_Gizi;
var varLapPenerimaanVendor_Gizi= ShowFormLapPenerimaanVendor_Gizi();

function ShowFormLapPenerimaanVendor_Gizi()
{
    frmDlgLapPenerimaanVendor_Gizi= fnDlgLapPenerimaanVendor_Gizi();
    frmDlgLapPenerimaanVendor_Gizi.show();
};

function fnDlgLapPenerimaanVendor_Gizi()
{
    var winLapPenerimaanVendor_GiziReport = new Ext.Window
    (
        {
            id: 'winLapPenerimaanVendor_GiziReport',
            title: 'Laporan Penerimaan dari Vendor',
            closeAction: 'destroy',
            width: 405,
            height: 190,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapPenerimaanVendor_Gizi()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkLapPenerimaanVendor_Gizi',
					handler: function()
					{
						var params={
							tglAwal:Ext.getCmp('dtpTglAwalPenerimaanVendor_gizi').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirPenerimaanVendor_gizi').getValue(),
							kdVendor:Ext.getCmp('cbo_vendorLapPenerimaan').getValue()
						};
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/gizi/lap_penerimaanvendor/cetakPenerimaanVendor");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();
						frmDlgLapPenerimaanVendor_Gizi.close();
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapPenerimaanVendor_Gizi',
					handler: function()
					{
							frmDlgLapPenerimaanVendor_Gizi.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapPenerimaanVendor_GiziReport;
};


function ItemDlgLapPenerimaanVendor_Gizi()
{
    var PnlLapPenerimaanVendor_Gizi = new Ext.Panel
    (
        {
            id: 'PnlLapPenerimaanVendor_Gizi',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					xtype: 'fieldset',
					title: '',
					items: 
					[
						getItemLapPenerimaanVendor_Gizi_Vendor(),
						getItemLapPenerimaanVendor_Gizi_Tanggal(),
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 0 0 0' },
							style:{'margin-left':'30px','margin-top':'0px'},
							anchor: '94%',
							layoutConfig:
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							}
						}
					]
			}
            ]
        }
    );

    return PnlLapPenerimaanVendor_Gizi;
};

function GetCriteriaLapPenerimaanVendor_Gizi()
{
	var strKriteria = '';
	
	/* strKriteria = '';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalPenerimaanVendor_gizi').getValue();
	strKriteria += '##@@##' + Ext.get('dtpTglAkhirPenerimaanVendor_gizi').getValue();
	
	if (Ext.get('cboPilihanLapPenerimaanVendor_Gizi').getValue() !== '' || Ext.get('cboPilihanLapPenerimaanVendor_Gizi').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'asal pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanLapPenerimaanVendor_Gizi').getValue();
	}
	if (Ext.get('cboUnitFarLapPenerimaanVendor_Gizi').getValue() !== '' || Ext.get('cboUnitFarLapPenerimaanVendor_Gizi').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + Ext.get('cboUnitFarLapPenerimaanVendor_Gizi').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitFarLapPenerimaanVendor_Gizi').getValue();
	}
	if (Ext.get('cboUserRequestEntryLapPenerimaanVendor_Gizi').getValue() !== ''|| Ext.get('cboUserRequestEntryLapPenerimaanVendor_Gizi').getValue() !== 'Pilih User...'){
		strKriteria += '##@@##' + 'operator';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryLapPenerimaanVendor_Gizi').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryLapPenerimaanVendor_Gizi').getValue();
	}
	
	if (Ext.getCmp('Shift_All_LapPenerimaanVendor_Gizi').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_LapPenerimaanVendor_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_LapPenerimaanVendor_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_LapPenerimaanVendor_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	} */
	
	
	return strKriteria;
};

function ValidasiReportLapPenerimaanVendor_Gizi()
{
	var x=1;
	if(Ext.get('cboUnitFarLapPenerimaanVendor_Gizi').getValue() === ''|| Ext.get('cboUnitFarLapPenerimaanVendor_Gizi').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapPenerimaanVendor_GiziReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapPenerimaanVendor_Gizi').getValue() === '' || Ext.get('cboUserRequestEntryLapPenerimaanVendor_Gizi').getValue() == 'Pilih User...'){
		ShowPesanWarningLapPenerimaanVendor_GiziReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalPenerimaanVendor_gizi').dom.value > Ext.get('dtpTglAkhirPenerimaanVendor_gizi').dom.value)
    {
        ShowPesanWarningLapPenerimaanVendor_GiziReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }

    return x;
};

function getItemLapPenerimaanVendor_Gizi_Vendor()
{
	
    var itemss = {
        layout: 'column',
        border: false,
        items: 
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width:  320,
				height: 45,
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Vendor'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					ComboVendorLapPenerimaan()
				]
			}
		]
    };
    return itemss;
};

function getItemLapPenerimaanVendor_Gizi_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width:  355,
				height: 35,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 0,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 0,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTglAwalPenerimaanVendor_gizi',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 0,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTglAkhirPenerimaanVendor_gizi',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};


function ComboVendorLapPenerimaan()
{
    var Field_VendorGzOrderDiet = ['kd_vendor', 'vendor'];
    ds_VendorGzOrderDietLapPenerimaan = new WebApp.DataStore({fields: Field_VendorGzOrderDiet});
    ds_VendorGzOrderDietLapPenerimaan.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_vendor',
					Sortdir: 'ASC',
					target: 'ComboVendorGizi',
					param: ''
				}
		}
	);
	
    var cbo_vendorLapPenerimaan = new Ext.form.ComboBox
    (
        {
			x: 100,
			y: 10,
            flex: 1,
			id: 'cbo_vendorLapPenerimaan',
			valueField: 'kd_vendor',
            displayField: 'vendor',
			store: ds_VendorGzOrderDietLapPenerimaan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_vendorLapPenerimaan;
};

function ShowPesanWarningLapPenerimaanVendor_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
