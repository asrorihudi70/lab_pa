
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapRekapMakananPerRuang_Gizi;
var selectNamaLapRekapMakananPerRuang_Gizi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRekapMakananPerRuang_Gizi;
var varLapRekapMakananPerRuang_Gizi= ShowFormLapRekapMakananPerRuang_Gizi();
var jenis="1";

function ShowFormLapRekapMakananPerRuang_Gizi()
{
    frmDlgLapRekapMakananPerRuang_Gizi= fnDlgLapRekapMakananPerRuang_Gizi();
    frmDlgLapRekapMakananPerRuang_Gizi.show();
};

function fnDlgLapRekapMakananPerRuang_Gizi()
{
    var winLapRekapMakananPerRuang_GiziReport = new Ext.Window
    (
        {
            id: 'winLapRekapMakananPerRuang_GiziReport',
            title: 'Laporan Rekapituasi Makanan PerRuang',
            closeAction: 'destroy',
            width: 405,
            height: 180,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRekapMakananPerRuang_Gizi()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapRekapMakananPerRuang_Gizi',
					handler: function()
					{
						var params={
							tglAwal:Ext.getCmp('dtpTglAwalRekapPeruang_gizi').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirRekapPeruang_gizi').getValue(),
							jenis:jenis
						};
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/gizi/lap_rekapperruang/cetakRekapPerRuang_Preview");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();
						frmDlgLapRekapMakananPerRuang_Gizi.close();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRekapMakananPerRuang_Gizi',
					handler: function()
					{
							frmDlgLapRekapMakananPerRuang_Gizi.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapRekapMakananPerRuang_GiziReport;
};


function ItemDlgLapRekapMakananPerRuang_Gizi()
{
    var PnlLapRekapMakananPerRuang_Gizi = new Ext.Panel
    (
        {
            id: 'PnlLapRekapMakananPerRuang_Gizi',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					xtype: 'fieldset',
					title: '',
					items: 
					[
						getItemLapRekapMakananPerRuang_Gizi_KelPasien(),
						getItemLapRekapMakananPerRuang_Gizi_Tanggal(),
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 0 0 0' },
							style:{'margin-left':'30px','margin-top':'0px'},
							anchor: '94%',
							layoutConfig:
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							}
						}
					]
			}
            ]
        }
    );

    return PnlLapRekapMakananPerRuang_Gizi;
};

function getItemLapRekapMakananPerRuang_Gizi_KelPasien()
{
	radios = new Ext.form.RadioGroup({
	id :'radiosa',
	hidden: true,
	columns  : 2,
	items: 
	[
		{
			id:'rbPasien',
			name: 'rbPasien',
			boxLabel: 'Pasien',
			checked: true, 
			inputValue: "1"
		},
		{
			boxLabel: 'Karyawan/Dokter',
			id:'rbDokter', 
			name: 'rbPasien',
			inputValue: "2"
		}
	],
	listeners: {
		change : function()
		{ 
			jenis= radios.getValue().inputValue;
		}
	}
		
   });
    var itemss = {
        layout: 'column',
        border: false,
		bodyStyle: 'padding: 0px 0px 5px 30px',
        items: 
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 5px 5px 20px 5px',
				border: false,
				width:  320,
				height: 10,
				items: [
					radios
				]
			}
		]
    };
    return itemss;
};

function getItemLapRekapMakananPerRuang_Gizi_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAwalRekapPeruang_gizi',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 10,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAkhirRekapPeruang_gizi',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};

function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
