
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapStokBahan_Gizi;
var selectNamaLapStokBahan_Gizi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapStokBahan_Gizi;
var kodebahanlapstokbahangizi;
var varLapStokBahan_Gizi= ShowFormLapStokBahan_Gizi();

function ShowFormLapStokBahan_Gizi()
{
    frmDlgLapStokBahan_Gizi= fnDlgLapStokBahan_Gizi();
    frmDlgLapStokBahan_Gizi.show();
};

function fnDlgLapStokBahan_Gizi()
{
    var winLapStokBahan_GiziReport = new Ext.Window
    (
        {
            id: 'winLapStokBahan_GiziReport',
            title: 'Laporan Stok Bahan',
            closeAction: 'destroy',
            width: 405,
            height: 140,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapStokBahan_Gizi()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkLapStokBahan_Gizi',
					handler: function()
					{
						var params={
									kdBahan:Ext.getCmp('cbo_bahanLapStokBahan').getValue(),
						} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/gizi/lap_stokbahan/cetakStokBahanGizi");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									frmDlgLapStokBahan_Gizi.close();
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapStokBahan_Gizi',
					handler: function()
					{
							frmDlgLapStokBahan_Gizi.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapStokBahan_GiziReport;
};


function ItemDlgLapStokBahan_Gizi()
{
    var PnlLapStokBahan_Gizi = new Ext.Panel
    (
        {
            id: 'PnlLapStokBahan_Gizi',
            fileUpload: true,
            layout: 'form',
            height: '200',
           // anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					xtype: 'fieldset',
					bodyStyle: 'padding: 0px 0px 0px 0px',
					title: '',
					height: 60,
					items: 
					[
						getItemLapStokBahan_Gizi_Bahan(),
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 0 0 0' },
							style:{'margin-left':'30px','margin-top':'0px'},
							anchor: '94%',
							layoutConfig:
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							}
						}
					]
			}
            ]
        }
    );

    return PnlLapStokBahan_Gizi;
};

function ValidasiReportLapStokBahan_Gizi()
{
	var x=1;
	if(Ext.get('cboUnitFarLapStokBahan_Gizi').getValue() === ''|| Ext.get('cboUnitFarLapStokBahan_Gizi').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapStokBahan_GiziReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapStokBahan_Gizi').getValue() === '' || Ext.get('cboUserRequestEntryLapStokBahan_Gizi').getValue() == 'Pilih User...'){
		ShowPesanWarningLapStokBahan_GiziReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalRekapPeruang_gizi').dom.value > Ext.get('dtpTglAkhirRekapPeruang_gizi').dom.value)
    {
        ShowPesanWarningLapStokBahan_GiziReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }

    return x;
};

function getItemLapStokBahan_Gizi_Bahan()
{
	
    var itemss = {
        layout: 'column',
        border: false,
        items: 
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width:  320,
				height: 45,
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Bahan'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					ComboBahanLapStokBahan()
				]
			}
		]
    };
    return itemss;
};


function ComboBahanLapStokBahan()
{
    var Field_VendorGzOrderDiet = ['kd_bahan', 'nama_bahan'];
    ds_bahanLapStokBahan = new WebApp.DataStore({fields: Field_VendorGzOrderDiet});
    ds_bahanLapStokBahan.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_bahan',
					Sortdir: 'ASC',
					target: 'ComboBahanGizi',
					param: ''
				}
		}
	);
	
    var cbo_bahanLapStokBahan = new Ext.form.ComboBox
    (
        {
			x: 100,
			y: 10,
            flex: 1,
			id: 'cbo_bahanLapStokBahan',
			valueField: 'kd_bahan',
            displayField: 'nama_bahan',
			store: ds_bahanLapStokBahan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					kodebahanlapstokbahangizi=cbo_bahanLapStokBahan.value;
				}
			}
        }
    )    
    return cbo_bahanLapStokBahan;
};

function ShowPesanWarningLapStokBahan_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
