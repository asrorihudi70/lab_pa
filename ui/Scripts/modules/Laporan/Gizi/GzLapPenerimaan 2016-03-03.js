var GzLapPenerimaan={
	vars:{
		no:null
	},
	TextField:{		
		tglAwal:null,
		tglAkhir:null
	},
	Window:{
		main:null
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params=[];
		params.push({name:'kd_pasien',value:$this.DropDown.noMedrec.getValue()});
		
		if($this.Radio.satu.checked==true){
			params.push({name:'cara',value:1});
		}else if($this.Radio.dua.checked==true){
			params.push({name:'cara',value:2});
		}else{
			params.push({name:'cara',value:3});
		}
		if($this.DropDown.noMedrec.getValue()==''){
			Ext.Msg.alert('Gagal','Pilih Pasien');
			loadMask.hide();
			return false;
		}
		params.push({name:'masuk',value:Q($this.TextField.tglMasuk).val()});
		params.push({name:'keluar',value:Q($this.TextField.tglKeluar).val()});
		params.push({name:'nama',value:$this.TextField.nama.getValue()});
		params.push({name:'pembayaran',value:$this.DropDown.payment.getValue()});
		params.push({name:'detail_bayar',value:$this.DropDown.detailBayar.getValue()});
		params.push({name:'unit',value:$this.DropDown.unit.getValue()});
		params.push({name:'jenis',value:$this.DropDown.jenis.getValue()});
		params.push({name:'no',value:$this.vars.no});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/apotek/lap_reseprwipasiendetail/doPrint",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.close();
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	getSelect:function(jenis_pay){
		var $this=this;
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/lap_reseprwipasiendetail/getSelect",
			data:{jenis_pay:jenis_pay},
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.DropDown.detailBayar).reset();
					Q($this.DropDown.detailBayar).add({id:'',text:'Semua'});
					Q($this.DropDown.detailBayar).add(r.data);
					Q($this.DropDown.detailBayar).val('');
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Penerimaan',
			fbar:[
				new Ext.Button({
					text:'Ok',
					handler:function(){
						$this.doPrint();
					}
				}),
				new Ext.Button({
					text:'Close',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().fieldset({
							title:'Periode',
							items:[{
									layout:'column',
									border:false,
									width: 290,
									items:[
										Q().input({
											label:'Tanggal',
											width:160,
											xWidth:40,
											items:[
												$this.TextField.tglAwal=Q().datefield({
													width: 100,
													value:''
												})
											]
										}),
										Q().input({
											label:'s/d',
											width:130,
											xWidth:20,
											items:[
												$this.TextField.tglAkhir=Q().datefield({
													width: 100,
													value:''
												})
											]
										})
										
									]
								}
							]
						}),
						
					]
				})
			]
		});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/lap_reseprwipasiendetail/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.show();
					/*Q($this.DropDown.payment).add(r.data.payment);
					Q($this.DropDown.unit).add(r.data.unit);
					Q($this.DropDown.jenis).add(r.data.jenis);*/
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}
}
GzLapPenerimaan.init();