
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapPenerimaan_Gizi;
var selectNamaLapPenerimaan_Gizi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapPenerimaan_Gizi;
var varLapPenerimaan_Gizi= ShowFormLapPenerimaan_Gizi();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var kodeUnitLapPenerimaan_Gz;
function ShowFormLapPenerimaan_Gizi()
{
    frmDlgLapPenerimaan_Gizi= fnDlgLapPenerimaan_Gizi();
    frmDlgLapPenerimaan_Gizi.show();
};
function dsLoadUnitLapPenerimaan_Gizi(){
	dsLapPenerimaan_Gizi.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vGzUnitLapPenerimaan',
                    param : 'kd_bagian=1'
                }			
            }
        );   
    return dsLapPenerimaan_Gizi;
	}

function fnDlgLapPenerimaan_Gizi()
{
    var winLapPenerimaan_GiziReport = new Ext.Window
    (
        {
            id: 'winLapPenerimaan_GiziReport',
            title: 'Laporan Penerimaan',
            closeAction: 'destroy',
            width: 405,
            height: 170,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapPenerimaan_Gizi()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapPenerimaan_Gizi',
					handler: function()
					{
						var params={
							//kdUnit:Ext.getCmp('CmbSetUnitLapPenerimaan_Gizi').getValue(),
							tglAwal:Ext.getCmp('dtpTglAwalPenerimaan_gizi').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirPenerimaan_gizi').getValue(),
						} ;
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/gizi/lap_penerimaan/cetakPenerimaanGizi");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();		
						frmDlgLapPenerimaan_Gizi.close();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapPenerimaan_Gizi',
					handler: function()
					{
						frmDlgLapPenerimaan_Gizi.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapPenerimaan_GiziReport;
};


function ItemDlgLapPenerimaan_Gizi()
{
    var PnlLapPenerimaan_Gizi = new Ext.Panel
    (
        {
            id: 'PnlLapPenerimaan_Gizi',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					
					xtype: 'fieldset',
					title: 'Periode',
					items: 
					[
						getItemLapPenerimaan_Gizi_Tanggal(),
					]
			}
            ]
        }
    );

    return PnlLapPenerimaan_Gizi;
};

function getItemLapPenerimaan_Gizi_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAwalPenerimaan_gizi',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 10,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAkhirPenerimaan_gizi',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};
function getItemLapPenerimaan_Gizi_Combo()
{
    var itemss = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Unit/Ruang'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					comboUnit_Gizi_Combo()
				]
			}
		]
    };
    return itemss;
};


function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
