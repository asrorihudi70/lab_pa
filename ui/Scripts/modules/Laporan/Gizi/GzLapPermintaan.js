
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapPermintaan_Gizi;
var selectNamaLapPermintaan_Gizi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapPermintaan_Gizi;
var varLapPermintaan_Gizi= ShowFormLapPermintaan_Gizi();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var kodeUnitLapPermintaan_Gz;
function ShowFormLapPermintaan_Gizi()
{
    frmDlgLapPermintaan_Gizi= fnDlgLapPermintaan_Gizi();
    frmDlgLapPermintaan_Gizi.show();
};
function dsLoadUnitLapPermintaan_Gizi(){
	dsLapPermintaan_Gizi.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vGzUnitLapPermintaan',
                    param : 'kd_bagian=1'
                }			
            }
        );   
    return dsLapPermintaan_Gizi;
	}
function comboUnit_Gizi_Combo()
{
    dsLapPermintaan_Gizi = new WebApp.DataStore({fields: ['kd_unit','kd_bagian','nama_unit']});
	dsLoadUnitLapPermintaan_Gizi();
	var CmbSetUnitLapPermintaan_Gz = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 10,
			id:'CmbSetUnitLapPermintaan_Gizi',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 150,
			store: dsLapPermintaan_Gizi,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					kodeUnitLapPermintaan_Gz=CmbSetUnitLapPermintaan_Gz.value;
					//selectSetPilihankelompokPasien=b.data.displayText;
				},
				'keyup': function(a,b,c)
				{
				}
			}
		}
	);
	return CmbSetUnitLapPermintaan_Gz;
}
function fnDlgLapPermintaan_Gizi()
{
    var winLapPermintaan_GiziReport = new Ext.Window
    (
        {
            id: 'winLapPermintaan_GiziReport',
            title: 'Laporan Permintaan',
            closeAction: 'destroy',
            width: 405,
            height: 210,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapPermintaan_Gizi()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapPermintaan_Gizi',
					handler: function()
					{
						var params={
							kdUnit:Ext.getCmp('CmbSetUnitLapPermintaan_Gizi').getValue(),
							tglAwal:Ext.getCmp('dtpTglAwalPermintaan_gizi').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirPermintaan_gizi').getValue(),
						} ;
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/gizi/lap_permintaan/cetakPermintaanGizi");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();		
						frmDlgLapPermintaan_Gizi.close();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapPermintaan_Gizi',
					handler: function()
					{
						frmDlgLapPermintaan_Gizi.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapPermintaan_GiziReport;
};


function ItemDlgLapPermintaan_Gizi()
{
    var PnlLapPermintaan_Gizi = new Ext.Panel
    (
        {
            id: 'PnlLapPermintaan_Gizi',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items:
            [
				{
					
					xtype: 'fieldset',
					title: 'Kriteria Laporan',
					items: 
					[
						getItemLapPermintaan_Gizi_Combo(),
						getItemLapPermintaan_Gizi_Tanggal(),
					]
			}
            ]
        }
    );

    return PnlLapPermintaan_Gizi;
};

function getItemLapPermintaan_Gizi_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAwalPermintaan_gizi',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 10,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAkhirPermintaan_gizi',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};
function getItemLapPermintaan_Gizi_Combo()
{
    var itemss = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Unit/Ruang'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					comboUnit_Gizi_Combo()
				]
			}
		]
    };
    return itemss;
};


function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
