
var tigaharilalu = new Date().add(Date.DAY, -3);
var selectNamaLapRekapBulanan_Gizi;
var now = new Date();
var bulan = now.format('m');
var tahun = now.format('Y');
var selectSetPerseorangan;
var frmDlgLapRekapBulanan_Gizi;
var varLapRekapBulanan_Gizi= ShowFormLapRekapBulanan_Gizi();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
function ShowFormLapRekapBulanan_Gizi()
{
    frmDlgLapRekapBulanan_Gizi= fnDlgLapRekapBulanan_Gizi();
    frmDlgLapRekapBulanan_Gizi.show();
};

function fnDlgLapRekapBulanan_Gizi()
{
    var winLapRekapBulanan_GiziReport = new Ext.Window
    (
        {
            id: 'winLapRekapBulanan_GiziReport',
            title: 'Laporan Rekap Bulanan',
            closeAction: 'destroy',
            width: 540,
            height: 170,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRekapBulanan_Gizi()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapRekapBulanan_Gizi',
					handler: function()
					{
						var params={
							bulanAwal:Ext.getCmp('dtpTglAwalRekapBulanan_gizi').getValue(),
							bulanAkhir:Ext.getCmp('dtpTglAkhirRekapBulanan_gizi').getValue(),
							tahun:Ext.getCmp('dtpTahunRekapBulanan_gizi').getValue()
						};
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/gizi/lap_rekapbulanandietpasien/cetakRekapBulanan");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();
						frmDlgLapRekapBulanan_Gizi.close();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRekapBulanan_Gizi',
					handler: function()
					{
							frmDlgLapRekapBulanan_Gizi.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapRekapBulanan_GiziReport;
};


function ItemDlgLapRekapBulanan_Gizi()
{
    var PnlLapRekapBulanan_Gizi = new Ext.Panel
    (
        {
            id: 'PnlLapRekapBulanan_Gizi',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					
					xtype: 'fieldset',
					title: 'Kriteria Laporan',
					items: 
					[
						getItemLapRekapBulanan_Gizi(),
					]
			}
            ]
        }
    );

    return PnlLapRekapBulanan_Gizi;
};

function getItemLapRekapBulanan_Gizi()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  493,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Bulan'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAwalRekapBulanan_gizi',
						format: 'M',
						value: bulan
					}, 
					{
						x: 215,
						y: 10,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAkhirRekapBulanan_gizi',
						format: 'M',
						value: bulan,
						width: 100
					},
					{
						x: 355,
						y: 10,
						xtype: 'label',
						text: 'Tahun'
					}, 
					{
						x: 395,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 405,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTahunRekapBulanan_gizi',
						format: 'Y',
						value: tahun,
						width: 60
					},
				]
			}
		]
    };
    return items;
};

function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
