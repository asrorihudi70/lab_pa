
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapRekapPemakaianBahan_Gizi;
var selectNamaLapRekapPemakaianBahan_Gizi;
var now = new Date();
var tahun = now.format('Y');
var selectSetPerseorangan;
var frmDlgLapRekapPemakaianBahan_Gizi;
var varLapRekapPemakaianBahan_Gizi= ShowFormLapRekapPemakaianBahan_Gizi();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
function ShowFormLapRekapPemakaianBahan_Gizi()
{
    frmDlgLapRekapPemakaianBahan_Gizi= fnDlgLapRekapPemakaianBahan_Gizi();
    frmDlgLapRekapPemakaianBahan_Gizi.show();
};
function dsLoadUnitLapRekapPemakaianBahan_Gizi(){
	dsLapRekapPemakaianBahan_Gizi.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vGzUnitLapPermintaan',
                    param : 'kd_bagian=1'
                }			
            }
        );   
    return dsLapRekapPemakaianBahan_Gizi;
	}

function fnDlgLapRekapPemakaianBahan_Gizi()
{
    var winLapRekapPemakaianBahan_GiziReport = new Ext.Window
    (
        {
            id: 'winLapRekapPemakaianBahan_GiziReport',
            title: 'Laporan Rekap Pemakaian Bahan',
            closeAction: 'destroy',
            width: 407,
            height: 210,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRekapPemakaianBahan_Gizi()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkLapRekapPemakaianBahan_Gizi',
					handler: function()
					{
						var params={
											tglAwal:Ext.getCmp('dtpTglAwalRekapPemakaianBahan_gizi').getValue(),
											tglAkhir:Ext.getCmp('dtpTglAkhirRekapPemakaianBahan_gizi').getValue(),
						} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/gizi/lap_rekappemakaianbahan/cetakRekapPemakaianBahanGizi");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();
									frmDlgLapRekapPemakaianBahan_Gizi.close();
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRekapPemakaianBahan_Gizi',
					handler: function()
					{
							frmDlgLapRekapPemakaianBahan_Gizi.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapRekapPemakaianBahan_GiziReport;
};


function ItemDlgLapRekapPemakaianBahan_Gizi()
{
    var PnlLapRekapPemakaianBahan_Gizi = new Ext.Panel
    (
        {
            id: 'PnlLapRekapPemakaianBahan_Gizi',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					
					xtype: 'fieldset',
					title: 'Kriteria Laporan',
					items: 
					[
						getItemLapRekapPemakaianBahan_Gizi(),
						
						/*{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 0 0 0' },
							style:{'margin-left':'30px','margin-top':'0px'},
							anchor: '94%',
							layoutConfig:
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							}
						}*/
					]
			}
            ]
        }
    );

    return PnlLapRekapPemakaianBahan_Gizi;
};

function GetCriteriaLapPermintaan_Gizi()
{
	var strKriteria = '';
	
	/* strKriteria = '';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalRekapPeruang_gizi').getValue();
	strKriteria += '##@@##' + Ext.get('dtpTglAkhirRekapPeruang_gizi').getValue();
	
	if (Ext.get('cboPilihanLapRekapMakananPerRuang_Gizi').getValue() !== '' || Ext.get('cboPilihanLapRekapMakananPerRuang_Gizi').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'asal pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanLapRekapMakananPerRuang_Gizi').getValue();
	}
	if (Ext.get('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue() !== '' || Ext.get('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + Ext.get('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue();
	}
	if (Ext.get('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue() !== ''|| Ext.get('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue() !== 'Pilih User...'){
		strKriteria += '##@@##' + 'operator';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue();
	}
	
	if (Ext.getCmp('Shift_All_LapRekapMakananPerRuang_Gizi').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_LapRekapMakananPerRuang_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_LapRekapMakananPerRuang_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_LapRekapMakananPerRuang_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	} */
	
	
	return strKriteria;
};

function ValidasiReportLapRekapMakananPerRuang_Gizi()
{
	var x=1;
	if(Ext.get('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue() === ''|| Ext.get('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue() === '' || Ext.get('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue() == 'Pilih User...'){
		ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalRekapPeruang_gizi').dom.value > Ext.get('dtpTglAkhirRekapPeruang_gizi').dom.value)
    {
        ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }

    return x;
};



function getItemLapRekapPemakaianBahan_Gizi()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  360,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAwalRekapPemakaianBahan_gizi',
						format: 'd-M-Y',
						value: now
					}, 
					{
						x: 215,
						y: 10,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAkhirRekapPemakaianBahan_gizi',
						format: 'd-M-Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};
function dataComboTahunRekapBulanan()
{
	
}
function comboRekapBulanan_Gizi_Combo()
{
	var CmbSetLapRekapBulanan_Gz = new Ext.form.ComboBox
	(
		{
			x: 405,
			y: 10,
			id:'CmbSetLapRekapBulanan_Gizi',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			store:'',
			width: 70,
			valueField: ['1','2','3'],
			displayField: ['1','2','3'],
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					kdtarifgoltarif=CmbSetTarifCustomer.value;
					//selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return CmbSetLapRekapBulanan_Gz;
}
function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
