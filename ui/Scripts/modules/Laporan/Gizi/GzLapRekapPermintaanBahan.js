
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapRekapPermintaanBahan_Gizi;
var selectNamaLapPermintaan_Gizi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRekapPermintaanBahan_Gizi;
var varLapRekapPermintaanBahan_Gizi= ShowFormLapRekapPermintaanBahan_Gizi();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
function ShowFormLapRekapPermintaanBahan_Gizi()
{
    frmDlgLapRekapPermintaanBahan_Gizi= fnDlgLapRekapPermintaanBahan_Gizi();
    frmDlgLapRekapPermintaanBahan_Gizi.show();
};
function dsLoadUnitLapRekapPermintaanBahan_Gizi(){
	dsLapRekapPermintaanBahan_Gizi.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vGzUnitLapPermintaan',
                    param : 'kd_bagian=1'
                }			
            }
        );   
    return dsLapRekapPermintaanBahan_Gizi;
	}
function comboJenisMintaLapRekapPermintaanBahan_Gizi_Combo()
{
	var CmbSeJenisMintaLapRekapPermintaanBahan_Gz = new Ext.form.ComboBox
	(
		{
			x: 100,
			y: 10,
			id:'CmbSetJenisMintaLapRekapPermintaanBahan_Gizi',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 150,
			store:  new Ext.data.ArrayStore({
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[1, 'Pasien'],[2, 'Karyawan RS'], [3, 'Keluarga Pasien'], [4, 'Umum']]
        	}),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					//selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return CmbSeJenisMintaLapRekapPermintaanBahan_Gz;
}
function fnDlgLapRekapPermintaanBahan_Gizi()
{
    var winLapRekapPermintaanBahan_GiziReport = new Ext.Window
    (
        {
            id: 'winLapRekapPermintaanBahan_GiziReport',
            title: 'Laporan Rekap Permintaan Bahan',
            closeAction: 'destroy',
            width: 405,
            height: 210,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRekapPermintaanBahan_Gizi()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkLapRekapPermintaanBahan_Gizi',
					handler: function()
					{
						var params={
										jenisMinta:Ext.getCmp('CmbSetJenisMintaLapRekapPermintaanBahan_Gizi').getValue(),
										tglAwal:Ext.getCmp('dtpTglAwalRekapPermintaanBahan_gizi').getValue(),
										tglAkhir:Ext.getCmp('dtpTglAkhirRekapPermintaanBahan_gizi').getValue(),
						} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/gizi/lap_rekappermintaanbahan/cetakRekapPermintaanGizi");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									frmDlgLapRekapPermintaanBahan_Gizi.close();	
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRekapPermintaanBahan_Gizi',
					handler: function()
					{
							frmDlgLapRekapPermintaanBahan_Gizi.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapRekapPermintaanBahan_GiziReport;
};


function ItemDlgLapRekapPermintaanBahan_Gizi()
{
    var PnlLapRekapPermintaanBahan_Gizi = new Ext.Panel
    (
        {
            id: 'PnlLapRekapPermintaanBahan_Gizi',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					
					xtype: 'fieldset',
					title: 'Kriteria Laporan',
					items: 
					[
						getItemLapRekapPermintaanBahan_Gizi_Combo(),
						getItemLapRekapPermintaanBahan_Gizi_Tanggal(),
						
						/*{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 0 0 0' },
							style:{'margin-left':'30px','margin-top':'0px'},
							anchor: '94%',
							layoutConfig:
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							}
						}*/
					]
			}
            ]
        }
    );

    return PnlLapRekapPermintaanBahan_Gizi;
};

function GetCriteriaLapPermintaan_Gizi()
{
	var strKriteria = '';
	
	/* strKriteria = '';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalRekapPeruang_gizi').getValue();
	strKriteria += '##@@##' + Ext.get('dtpTglAkhirRekapPeruang_gizi').getValue();
	
	if (Ext.get('cboPilihanLapRekapMakananPerRuang_Gizi').getValue() !== '' || Ext.get('cboPilihanLapRekapMakananPerRuang_Gizi').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'asal pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanLapRekapMakananPerRuang_Gizi').getValue();
	}
	if (Ext.get('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue() !== '' || Ext.get('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + Ext.get('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue();
	}
	if (Ext.get('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue() !== ''|| Ext.get('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue() !== 'Pilih User...'){
		strKriteria += '##@@##' + 'operator';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue();
	}
	
	if (Ext.getCmp('Shift_All_LapRekapMakananPerRuang_Gizi').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_LapRekapMakananPerRuang_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_LapRekapMakananPerRuang_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_LapRekapMakananPerRuang_Gizi').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	} */
	
	
	return strKriteria;
};

function ValidasiReportLapRekapMakananPerRuang_Gizi()
{
	var x=1;
	if(Ext.get('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue() === ''|| Ext.get('cboUnitFarLapRekapMakananPerRuang_Gizi').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue() === '' || Ext.get('cboUserRequestEntryLapRekapMakananPerRuang_Gizi').getValue() == 'Pilih User...'){
		ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalRekapPeruang_gizi').dom.value > Ext.get('dtpTglAkhirRekapPeruang_gizi').dom.value)
    {
        ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }

    return x;
};



function getItemLapRekapPermintaanBahan_Gizi_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAwalRekapPermintaanBahan_gizi',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 10,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAkhirRekapPermintaanBahan_gizi',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};
function getItemLapRekapPermintaanBahan_Gizi_Combo()
{
    var itemss = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Jenis Minta'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					comboJenisMintaLapRekapPermintaanBahan_Gizi_Combo()
				]
			}
		]
    };
    return itemss;
};


function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
