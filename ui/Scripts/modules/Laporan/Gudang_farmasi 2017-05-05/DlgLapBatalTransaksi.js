
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmLapBatalTransaksiFarmasi;
var varLapBatalTransaksiFarmasi= ShowFormLapBatalTransaksiFarmasi();
var winLapBatalTransaksiFarmasiReport;
var cboTransaksiFarmasi_LapBatalTransaksiFarmasi;






function ShowFormLapBatalTransaksiFarmasi()
{
    frmLapBatalTransaksiFarmasi= fnLapBatalTransaksiFarmasi();
    frmLapBatalTransaksiFarmasi.show();
	loadDataComboUnitFar_LapBatalTransaksiFarmasi();
};


function fnLapBatalTransaksiFarmasi()
{
    winLapBatalTransaksiFarmasiReport = new Ext.Window
    (
        {
            id: 'winLapBatalTransaksiFarmasiReport',
            title: 'Laporan Batal Transaksi',
            closeAction: 'destroy',
            width: 420,
            height: 160,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapBatalTransaksiFarmasi()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: nmBtnOK,
						width: 70,
						hideLabel: true,
						id: 'btnOkLapBatalTransaksiFarmasi',
						handler: function()
						{
							if (ValidasiReportLapBatalTransaksiFarmasi() === 1)
							{
								var params={
									transaksi:Ext.get('cboTransaksiFarmasi_LapBatalTransaksiFarmasi').getValue(),
									idtransaksi:Ext.getCmp('cboTransaksiFarmasi_LapBatalTransaksiFarmasi').getValue(),
									tglAwal:Ext.getCmp('dtpTglAwalFilter_LapBatalTransaksiFarmasi').getValue(),
									tglAkhir:Ext.getCmp('dtpTglAkhirFilter_LapBatalTransaksiFarmasi').getValue()
								} 
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_bataltransaksi/cetakBatalTransaksiFarmasi");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapBatalTransaksiFarmasiReport.close(); 
								
							};
							
							
						}
					},
					{
						xtype: 'button',
						text: nmBtnCancel ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapBatalTransaksiFarmasi',
						handler: function()
						{
							winLapBatalTransaksiFarmasiReport.close();
						}
					}
			]

        }
    );

    return winLapBatalTransaksiFarmasiReport;
};


function ItemLapBatalTransaksiFarmasi()
{
    var PnlLapBatalTransaksiFarmasi = new Ext.Panel
    (
        {
            id: 'PnlLapBatalTransaksiFarmasi',
            fileUpload: true,
            layout: 'form',
            height: '150',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapBatalTransaksiFarmasi_Atas(),
            ]
        }
    );

    return PnlLapBatalTransaksiFarmasi;
};

function ValidasiReportLapBatalTransaksiFarmasi()
{
	var x=1;
	if(Ext.getCmp('cboTransaksiFarmasi_LapBatalTransaksiFarmasi').getValue() === ''){
		ShowPesanWarningLapBatalTransaksiFarmasiReport('Unit Far tidak boleh kosong','Warning');
        x=0;
	}
    return x;
};

function ShowPesanWarningLapBatalTransaksiFarmasiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapBatalTransaksiFarmasi_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  395,
            height: 75,
            anchor: '100% 100%',
            items: [
			{
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode '
            }, 
			{
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 10,
				xtype: 'datefield',
				id: 'dtpTglAwalFilter_LapBatalTransaksiFarmasi',
				format: 'd/M/Y',
				value: now
			}, 
			{
				x: 225,
				y: 10,
				xtype: 'label',
				text: ' s/d Bulan'
			}, 
			{
				x: 280,
				y: 10,
				xtype: 'datefield',
				id: 'dtpTglAkhirFilter_LapBatalTransaksiFarmasi',
				format: 'd/M/Y',
				value: now,
				width: 100
			},
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Transaksi '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            comboTransaksiFarmasi_LapBatalTransaksiFarmasi(),
            ]
        }]
    };
    return items;
};


function getItemLapBatalTransaksiFarmasi_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

var selectSetPilihan;

function comboTransaksiFarmasi_LapBatalTransaksiFarmasi()
{
    cboTransaksiFarmasi_LapBatalTransaksiFarmasi = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 40,
                id:'cboTransaksiFarmasi_LapBatalTransaksiFarmasi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:220,
                store: new Ext.data.ArrayStore( {
					id: 0,
					fields:[
						'Id',
						'displayText'
					],
					data: [[1, 'Perencanaan'],[2, 'Pemesanan'], 
						[3, 'Penerimaan'],[4, 'Retur PBF'],
						[5, 'Distribusi Unit'],[6, 'Penghapusan'],
						[7, 'Resep Rawat Jalan / Gawat Darurat'],[8, 'Resep Rawat Inap'],
						[9, 'Retur Rawat Jalan / Gawat Darurat'],[10, 'Retur Rawat Inap']
					]
				}),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Perencanaan',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboTransaksiFarmasi_LapBatalTransaksiFarmasi;
};
