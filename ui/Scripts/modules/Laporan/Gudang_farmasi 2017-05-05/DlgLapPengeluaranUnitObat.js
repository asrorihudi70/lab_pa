var DlgLapPengeluaranUnitObat={
	ArrayStore:{
		unit1:Q().arraystore(),
		unit2:Q().arraystore()
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Grid:{
		unit1:null,
		unit2:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/lap_pengeluaranunitobat/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.ArrayStore.unit1).add(r.data.unit);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params=[];
		params.push({name:'start_date',value:timestimetodate($this.DateField.startDate.getValue())});
		params.push({name:'last_date',value:timestimetodate($this.DateField.endDate.getValue())});
		for(var i=0; i<$this.ArrayStore.unit2.getRange().length ; i++){
			params.push({name:'kd_unit[]',value:$this.ArrayStore.unit2.getRange()[i].data.id});
		}
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/gudang_farmasi/lap_pengeluaranunitobat/doPrint",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.close();
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Pengeluaran Ke Unit Per Obat',
			fbar:[
				{
                                       xtype: 'checkbox',
                                       id: 'CekLapPilihSemuaGudangFarmasiPengeluaranUnitObat',
                                       hideLabel:false,
                                       boxLabel: 'Pilih Semua',
                                       checked: false,
                                       listeners: 
                                       {
                                            check: function()
                                            {
                                               if(Ext.getCmp('CekLapPilihSemuaGudangFarmasiPengeluaranUnitObat').getValue()===true)
                                                {
                                                     $this.Grid.unit1.getSelectionModel().selectAll();
                                                }
                                                else
                                                {
                                                    $this.Grid.unit1.getSelectionModel().clearSelections();
                                                }
                                            }
                                       }
                                    },
				new Ext.Button({
					text:'Ok',
					handler:function(){
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Batal',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					
					items:[
						{
							layout:'hbox',
							border: false,
							items:[
								$this.Grid.unit1=new Ext.grid.GridPanel({
						            ddGroup          : 'secondGridDDGroup',
						            store            : $this.ArrayStore.unit1,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            flex			: 1,
						            height           : 200,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : 'Unit',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                sortable: true,
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                sortable: true,
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit1.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'firstGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
				                                    $this.Grid.unit1.store.add(records);
				                                    $this.Grid.unit1.store.sort('KD_UNIT', 'ASC');
				                                    return true
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        }),
						        $this.Grid.unit2=new Ext.grid.GridPanel({
						            ddGroup          : 'firstGridDDGroup',
						            store            : $this.ArrayStore.unit2,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            style:'margin-left:-1px;',
						            flex			: 1,
						            height           : 200,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : 'Unit',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit2.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'secondGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
				                                    $this.Grid.unit2.store.add(records);
				                                    $this.Grid.unit2.store.sort('KD_UNIT', 'ASC');
				                                    return true
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        })
							]
						},
						Q().fieldset({
							items:[
								Q().input({
									label:'Periode',
									width: 350,
									items:[
										$this.DateField.startDate=Q().datefield(),
										Q().display({value:'s/d'}),
										$this.DateField.endDate=Q().datefield()
									]
								})
							]
						})
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapPengeluaranUnitObat.init();