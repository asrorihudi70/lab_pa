var print=true;
var DlgLapKartuStokSummary={
	App	:{
		title:'Laporan Kartu Stok Summary',
		lastUpdate:'2015-08-05',
		createBy:'Asep Kamaludin'
	},
	Dropdown	:{
		bulan:null,
		milik:null,
		obat:null
	},
	NumberFiled	:{
		tahun:null
	},
	Window	:{
		main:null
	},
	getData	:function(){
		var $this=this;
		loadMask.show();
		$.ajax({
			type	: 'POST',
			dataType: 'JSON',
			url		: baseURL + "index.php/gudang_farmasi/lap_kartustoksummary/getData",
			success	: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.Dropdown.milik).add(r.data.milik);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		if($this.Dropdown.obat.getValue()!=''){
			params['kd_prd']	= $this.Dropdown.obat.getValue();
		}
		params['kd_milik']		= $this.Dropdown.milik.getValue();
		params['year']		= $this.NumberFiled.tahun.getValue();
		params['month']		= $this.Dropdown.bulan.getValue()+1;
		/* $.ajax({
			type	: 'POST',
			dataType: 'JSON',
			data	: params,
			url		: baseURL + "index.php/gudang_farmasi/lap_kartustokSummary/doPrint",
			success	: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.close();
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error	: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		}); */
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_kartustoksummary/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_kartustoksummary/doPrint");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title	:$this.App.title,
			fbar	:[
						new Ext.Button({
							text:'Print',
							handler:function(){
								print=true;
								$this.doPrint()
							}
						}),
						new Ext.Button({
							text:'Preview',
							handler:function(){
								print=false;
								$this.doPrint()
							}
						}),
						new Ext.Button({
							text:'Batal',
							handler:function(){
								$this.Window.main.close();
							}
						})
					],
			items	:[
				Q().panel({
					items	:[
						Q().input({
							label	:'Nama Obat',
							width	:350,
							items	:[
								$this.Dropdown.obat=Q().autocomplete({
									width	: 200,
									insert	: function(o){
										return {
											kd_prd		:o.kd_prd,
											nama_obat 	: o.nama_obat,
											text		:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td></tr></table>'
										}
									},
									success:function(res){return res.data},
									url			: baseURL + "index.php/gudang_farmasi/lap_kartustoksummary/getObat",
									keyField	: 'kd_prd',
									valueField	: 'nama_obat',
									displayField: 'text',
									listWidth	: 250
								})
							]
						}),
						Q().input({
							label:'Bulan',
							items:[
								$this.Dropdown.bulan=Q().dropdown({
									value:new Date().getMonth(),
									data:[
										{id	:0,text	:'Januari'},
										{id	:1,text	:'Februari'},
										{id	:2,text	:'Maret'},
										{id	:3,text	:'April'},
										{id	:4,text	:'Mei'},
										{id	:5,text	:'Juni'},
										{id	:6,text	:'Juli'},
										{id	:7,text	:'Agustus'},
										{id	:8,text	:'September'},
										{id	:9,text	:'Oktober'},
										{id	:10,text:'November'},
										{id	:11,text:'Desember'}
									]								
								})
							]
						}),
						Q().input({
							label:'Tahun',
							items:[
								$this.NumberFiled.tahun=new Ext.form.NumberField({
									width: 50,
									value:new Date().getFullYear(),
									style:'text-align:right'
								})
							]
						}),
						Q().input({
							label:'Kepemilikan',
							items:[
								$this.Dropdown.milik=Q().dropdown({
									emptyText: 'Semua'
								})
							]
						})
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapKartuStokSummary.init();
console.info(DlgLapKartuStokSummary);