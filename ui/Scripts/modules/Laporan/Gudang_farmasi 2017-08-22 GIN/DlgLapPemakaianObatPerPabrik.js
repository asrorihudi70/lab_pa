var print=true;
var DlgLapPemakaianObatPerPabrik={
	Dropdown:{
		vendor:null,
		unit:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/lap_pemakaianobatperPabrik/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.Dropdown.vendor).add(r.data.vendor);
					Q($this.Dropdown.unit).add(r.data.unit);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		params['start_date']=timestimetodate($this.DateField.startDate.getValue());
		params['last_date']=timestimetodate($this.DateField.endDate.getValue());
		params['vendor']=$this.Dropdown.vendor.getValue();
		params['unit']=$this.Dropdown.unit.getValue();
		params['c_unit']=Ext.getCmp('cbUnit').getValue();
		params['c_vendor']=Ext.getCmp('cbVendor').getValue();
		
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_pemakaianobatperPabrik/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_pemakaianobatperPabrik/doPrint");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
		
		
	},
	init:function(){
		var $this=this;
		
		$this.Window.main=Q().window({
			title:'Laporan Penerimaan Vendor Per Pabrik',
			fbar:[
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint();
					}
				}),
				new Ext.Button({
					text:'Preview',
					handler:function(){
						print=false;
						$this.doPrint();
					}
				}),
				new Ext.Button({
					text:'Batal',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Periode',
							width: 400,
							items:[
								$this.DateField.startDate=Q().datefield(),
								Q().display({value:'s/d'}),
								$this.DateField.endDate=Q().datefield()
							]
						}),
						Q().input({
							label:'Vendor / PBF',
							items:[
								$this.Dropdown.vendor=Q().dropdown({
									width: 200,
									emptyText: ''
								}),
								{
									  xtype: 'label',
									  html: '&nbsp;'
								},
								{
									 id: 'cbVendor',
									 xtype:'checkbox',
									 name: 'cbVendor',
									 labelSeparator: '',
									 hideLabel: true,
									 boxLabel: 'Semua',
									 fieldLabel: 'Semua',
									 checked: false,
									 handler: function (field, value) 
									{
										if (value === true){
											$this.Dropdown.vendor.setDisabled(true);
										}else{
											$this.Dropdown.vendor.enable();
										}
									}
								}
							]
						}),
						Q().input({
							label:'Unit',
							items:[
								$this.Dropdown.unit=Q().dropdown({
									width: 200,
									emptyText: '',
									
								}),
								{
									  xtype: 'label',
									  html: '&nbsp;'
								},
								{
									id: 'cbUnit',
									xtype:'checkbox',
									name: 'cbUnit',
									labelSeparator: '',
									hideLabel: true,
									boxLabel: 'Semua',
									fieldLabel: 'Semua',
									checked: false,
									handler: function (field, value) 
									{
										if (value === true){
											$this.Dropdown.unit.setDisabled(true);
										}else{
											$this.Dropdown.unit.enable();
										}
									}
									
								}
							]
						}),
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapPemakaianObatPerPabrik.init();