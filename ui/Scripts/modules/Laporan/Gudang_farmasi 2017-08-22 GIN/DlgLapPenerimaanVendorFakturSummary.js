var print=true;
var DlgLapPenerimaanVendorFakturSummary={
	Dropdown:{
		vendor:null,
		bayar:null,
		tempo:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/lap_penerimaanvendorfkatursummary/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.Dropdown.vendor).add(r.data.vendor);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		params['start_date']=timestimetodate($this.DateField.startDate.getValue());
		params['last_date']=timestimetodate($this.DateField.endDate.getValue());
		params['vendor']=$this.Dropdown.vendor.getValue();
		params['bayar']=$this.Dropdown.bayar.getValue();
		params['tempo']=$this.Dropdown.tempo.getValue();
		
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_penerimaanvendorfkatursummary/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_penerimaanvendorfkatursummary/doPrint");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
		
		
		// $.ajax({
			// type: 'POST',
			// dataType:'JSON',
			// data:params,
			// url:baseURL + "index.php/gudang_farmasi/lap_penerimaanvendorfkatursummary/doPrint",
			// success: function(r){
				// loadMask.hide();
				// if(r.result=='SUCCESS'){
					// $this.Window.main.close();
					// window.open(r.data, '_blank', 'location=0,resizable=1', false);
				// }else{
					// Ext.Msg.alert('Gagal',r.message);
				// }
			// },
			// error: function(jqXHR, exception) {
				// loadMask.hide();
				// Nci.ajax.ErrorMessage(jqXHR, exception);
			// }
		// });
	},
	init:function(){
		var $this=this;
		
		$this.Window.main=Q().window({
			title:'Laporan Penerimaan dari Vendor per Faktur (summary)',
			fbar:[
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint();
					}
				}),
				new Ext.Button({
					text:'Previw',
					handler:function(){
						print=false;
						$this.doPrint();
					}
				}),
				new Ext.Button({
					text:'Batal',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Periode',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield(),
								Q().display({value:'s/d'}),
								$this.DateField.endDate=Q().datefield()
							]
						}),
						Q().input({
							label:'Vendor / PBF',
							items:[
								$this.Dropdown.vendor=Q().dropdown({
									width: 200,
									emptyText: 'Semua'
								})
							]
						}),
						Q().input({
							label:'Pembayaran',
							items:[
								$this.Dropdown.bayar=Q().dropdown({
									emptyText: 'Semua',
									data : [{id:1,text:'Sudah Bayar'},{id:2,text:'Belum Bayar'}]
								})
							]
						}),
						Q().input({
							label:'Jatuh Tempo',
							items:[
								$this.Dropdown.tempo=Q().dropdown({
									emptyText: 'Semua',
									width: 150,
									data : [{id:1,text:'Sudah Jatuh Tempo'},{id:2,text:'Belum Jatuh Tempo'}]
								})
							]
						})
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapPenerimaanVendorFakturSummary.init();