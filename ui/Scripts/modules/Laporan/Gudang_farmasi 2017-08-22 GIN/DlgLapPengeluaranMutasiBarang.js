
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapPengeluaranMutasiBarang;
var selectNamaLapPengeluaranMutasiBarang;
var now = new Date();
var selectSetPerseorangan;
var frmLapPengeluaranMutasiBarang;
var varLapLapPengeluaranMutasiBarang= ShowFormLapLapPengeluaranMutasiBarang();
var tglAwal;
var tglAkhir;
var tipe;
var winLapPengeluaranMutasiBarangReport;
var cboUnitFar_LapPengeluaranMutasiBarang;
var print=true;





function ShowFormLapLapPengeluaranMutasiBarang()
{
    frmLapPengeluaranMutasiBarang= fnLapPengeluaranMutasiBarang();
    frmLapPengeluaranMutasiBarang.show();
	loadDataComboUnitFar_LapPengeluaranMutasiBarang();
};


function fnLapPengeluaranMutasiBarang()
{
    winLapPengeluaranMutasiBarangReport = new Ext.Window
    (
        {
            id: 'winLapPengeluaranMutasiBarangReport',
            title: 'Laporan Pengeluaran Mutasi Barang',
            closeAction: 'destroy',
            width: 420,
            height: 160,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapPengeluaranMutasiBarang()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'Ok',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapPengeluaranMutasiBarang',
						handler: function()
						{
							print=false;
							Cetak();						
							
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapPengeluaranMutasiBarang',
						handler: function()
						{
							winLapPengeluaranMutasiBarangReport.close();
						}
					}
			]

        }
    );

    return winLapPengeluaranMutasiBarangReport;
};


function ItemLapPengeluaranMutasiBarang()
{
    var PnlLapLapPengeluaranMutasiBarang = new Ext.Panel
    (
        {
            id: 'PnlLapLapPengeluaranMutasiBarang',
            fileUpload: true,
            layout: 'form',
            height: '150',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapPengeluaranMutasiBarang_Atas(),
            ]
        }
    );

    return PnlLapLapPengeluaranMutasiBarang;
};


function ShowPesanWarningLapPengeluaranMutasiBarangReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapPengeluaranMutasiBarang_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  395,
            height: 75,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Unit  '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            mComboUnitFarLapPengeluaranMutasiBarang(),
			
			{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode '
            }, 
			{
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 40,
				xtype: 'datefield',
				id: 'dtpTglAwalFilter_ObatExpired',
				format: 'd/M/Y',
				value: now
			}, 
			{
				x: 235,
				y: 40,
				xtype: 'label',
				text: ' s/d '
			}, 
			{
				x: 270,
				y: 40,
				xtype: 'datefield',
				id: 'dtpTglAkhirFilter_ObatExpired',
				format: 'd/M/Y',
				value: now,
				width: 100
			},
            ]
        }]
    };
    return items;
};


function getItemLapLapPengeluaranMutasiBarang_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

var selectSetPilihan;

function loadDataComboUnitFar_LapPengeluaranMutasiBarang(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/lap_daftarobatexpired/getUnitFar",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUnitFar_LapPengeluaranMutasiBarang.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_UnitFar_LapPengeluaranMutasiBarang.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_UnitFar_LapPengeluaranMutasiBarang.add(recs);
				console.log(o);
			}
		}
	});
}


function mComboUnitFarLapPengeluaranMutasiBarang()
{
	var Field = ['kd_unit_far','nm_unit_far'];
    ds_UnitFar_LapPengeluaranMutasiBarang = new WebApp.DataStore({fields: Field});
    cboUnitFar_LapPengeluaranMutasiBarang = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboUnitFar_LapPengeluaranMutasiBarang',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'SEMUA',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: ds_UnitFar_LapPengeluaranMutasiBarang,
                valueField: 'kd_unit_far',
                displayField: 'nm_unit_far',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboUnitFar_LapPengeluaranMutasiBarang;
};

function Cetak(){
	var params={
		tglAwal:Ext.getCmp('dtpTglAwalFilter_ObatExpired').getValue(),
		tglAkhir:Ext.getCmp('dtpTglAkhirFilter_ObatExpired').getValue(),
		periodeAwal:Ext.get('dtpTglAwalFilter_ObatExpired').getValue(),
		periodeAkhir:Ext.get('dtpTglAkhirFilter_ObatExpired').getValue(),
		kd_unit_far:Ext.getCmp('cboUnitFar_LapPengeluaranMutasiBarang').getValue(),
		print:print
	} 
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_pengeluaran_mutasi_barang/cetak"); 
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();		
}
