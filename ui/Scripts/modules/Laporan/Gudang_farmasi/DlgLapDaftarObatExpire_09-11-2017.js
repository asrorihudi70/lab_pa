
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapObatExpired;
var selectNamaLapObatExpired;
var now = new Date();
var selectSetPerseorangan;
var frmLapObatExpired;
var varLapLapObatExpired= ShowFormLapLapObatExpired();
var tglAwal;
var tglAkhir;
var tipe;
var winLapObatExpiredReport;
var cboUnitFar_LapObatExpired;
var print=true;





function ShowFormLapLapObatExpired()
{
    frmLapObatExpired= fnLapObatExpired();
    frmLapObatExpired.show();
	loadDataComboUnitFar_LapObatExpired();
};


function fnLapObatExpired()
{
    winLapObatExpiredReport = new Ext.Window
    (
        {
            id: 'winLapObatExpiredReport',
            title: 'Laporan Daftar Obat Exipred',
            closeAction: 'destroy',
            width: 420,
            height: 160,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapObatExpired()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'Print',
						width: 70,
						hideLabel: true,
						id: 'btnPrintLapLapObatExpired',
						handler: function()
						{
							print=true;
							Cetak();						
							
						}
					},
					{
						xtype: 'button',
						text: 'Preview',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapObatExpired',
						handler: function()
						{
							print=false;
							Cetak();						
							
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapObatExpired',
						handler: function()
						{
							winLapObatExpiredReport.close();
						}
					}
			]

        }
    );

    return winLapObatExpiredReport;
};


function ItemLapObatExpired()
{
    var PnlLapLapObatExpired = new Ext.Panel
    (
        {
            id: 'PnlLapLapObatExpired',
            fileUpload: true,
            layout: 'form',
            height: '150',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapObatExpired_Atas(),
            ]
        }
    );

    return PnlLapLapObatExpired;
};


function ShowPesanWarningLapObatExpiredReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapObatExpired_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  395,
            height: 75,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Unit Far '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            mComboUnitFarLapObatExpired(),
			
			{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode '
            }, 
			{
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 40,
				xtype: 'datefield',
				id: 'dtpTglAwalFilter_ObatExpired',
				format: 'd/M/Y',
				value: now
			}, 
			{
				x: 225,
				y: 40,
				xtype: 'label',
				text: ' s/d Bulan'
			}, 
			{
				x: 280,
				y: 40,
				xtype: 'datefield',
				id: 'dtpTglAkhirFilter_ObatExpired',
				format: 'd/M/Y',
				value: now,
				width: 100
			},
            ]
        }]
    };
    return items;
};


function getItemLapLapObatExpired_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

var selectSetPilihan;

function loadDataComboUnitFar_LapObatExpired(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/lap_daftarobatexpired/getUnitFar",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUnitFar_LapObatExpired.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_UnitFar_LapObatExpired.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_UnitFar_LapObatExpired.add(recs);
				console.log(o);
			}
		}
	});
}


function mComboUnitFarLapObatExpired()
{
	var Field = ['kd_unit_far','nm_unit_far'];
    ds_UnitFar_LapObatExpired = new WebApp.DataStore({fields: Field});
    cboUnitFar_LapObatExpired = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboUnitFar_LapObatExpired',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'SEMUA',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: ds_UnitFar_LapObatExpired,
                valueField: 'kd_unit_far',
                displayField: 'nm_unit_far',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboUnitFar_LapObatExpired;
};

function Cetak(){
	var params={
		tglAwal:Ext.getCmp('dtpTglAwalFilter_ObatExpired').getValue(),
		tglAkhir:Ext.getCmp('dtpTglAkhirFilter_ObatExpired').getValue(),
		periodeAwal:Ext.get('dtpTglAwalFilter_ObatExpired').getValue(),
		periodeAkhir:Ext.get('dtpTglAkhirFilter_ObatExpired').getValue(),
		kd_unit_far:Ext.getCmp('cboUnitFar_LapObatExpired').getValue()
	} 
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	if(print==true){
		form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_daftarobatexpired/cetakObatExpired");
	} else{
		form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_daftarobatexpired/previewObatExpired");
	}
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();		
}
