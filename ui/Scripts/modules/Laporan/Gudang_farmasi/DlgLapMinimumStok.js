var print=true;
var DlgLapMinimumStok={
	vars:{
		comboSelect:null
	},
	ArrayStore:{
		unit1:Q().arraystore(),
		unit2:Q().arraystore()
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Dropdown:{
		subJenis:null,
		milik:null,
		namaObat:null
	},
	CheckBox:{
		kelompok:null
	},
	Grid:{
		unit1:null,
		unit2:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/lap_minimumstok/getData",
			success: function(r){
				if(r.result=='SUCCESS'){
					Q($this.ArrayStore.unit1).add(r.data.unit);
					Q($this.ArrayStore.unit2).add(r.data.this_unit);
					Q($this.Dropdown.milik).add(r.data.milik);
					Q($this.Dropdown.subJenis).add(r.data.sub_jenis);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		var sendDataArrayUnit = [];
		$this.ArrayStore.unit2.each(function(record){
			var recordArrayUnit= [record.get("text")];
			sendDataArrayUnit.push(recordArrayUnit);
		});
		params['milik']=$this.Dropdown.milik.getValue();
		params['sub_jenis']=$this.Dropdown.subJenis.getValue();
		
		params['tmp_unit'] 	= sendDataArrayUnit;
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_minimumstok/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_minimumstok/doPrint");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
		/* var params=[];
		params.push({name:'milik',value:$this.Dropdown.milik.getValue()});
		params.push({name:'sub_jenis',value:$this.Dropdown.subJenis.getValue()});
		params.push({name:'kd_prd',value:$this.Dropdown.namaObat.getValue()});
		if($this.CheckBox.kelompok.getValue()==true){
			params.push({name:'group',value:$this.CheckBox.kelompok.getValue()});
		}
		for(var i=0; i<$this.ArrayStore.unit2.getRange().length ; i++){
			params.push({name:'kd_unit[]',value:$this.ArrayStore.unit2.getRange()[i].data.id});
		}
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/gudang_farmasi/lap_minimumstok/doPrint",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.close();
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		}); */
		
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Minimum Stok',
			fbar:[
			{
                                       xtype: 'checkbox',
                                       id: 'CekLapPilihSemuaGudangFarmasiStokObatPerUnit',
                                       hideLabel:false,
                                       boxLabel: 'Pilih Semua',
                                       checked: false,
                                       listeners: 
                                       {
                                            check: function()
                                            {
                                               if(Ext.getCmp('CekLapPilihSemuaGudangFarmasiStokObatPerUnit').getValue()===true)
                                                {
                                                     $this.Grid.unit1.getSelectionModel().selectAll();
                                                }
                                                else
                                                {
                                                    $this.Grid.unit1.getSelectionModel().clearSelections();
                                                }
                                            }
                                       }
                                    },
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Preview',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Close',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						{
							layout:'hbox',
							border: false,
							items:[
								$this.Grid.unit1=new Ext.grid.GridPanel({
						            ddGroup          : 'secondGridDDGroup',
						            store            : $this.ArrayStore.unit1,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            height           : 200,
						            flex: 1,
						            //width			 : 150,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : 'Unit',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                sortable: true,
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                sortable: true,
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit1.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'firstGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
			                                    	Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
				                                    $this.Grid.unit1.store.add(records);
				                                    $this.Grid.unit1.store.sort('text', 'ASC');
				                                    return true;
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        }),
						        $this.Grid.unit2=new Ext.grid.GridPanel({
						            ddGroup          : 'firstGridDDGroup',
						            store            : $this.ArrayStore.unit2,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            style:'margin-left: -1px;',
						            height           : 200,
						            flex: 1,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : 'Unit',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit2.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'secondGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    if((Q($this.ArrayStore.unit2).size()+records.length)<=8){
					                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
					                                    $this.Grid.unit2.store.add(records);
					                                    $this.Grid.unit2.store.sort('text', 'ASC');
					                                    return true
				                                    }else if((Q($this.ArrayStore.unit2).size()+records.length)>8){
				                                    	if(Q($this.ArrayStore.unit2).size()<8){
				                                    		var sisa=8-Q($this.ArrayStore.unit2).size();
				                                    		var a=[];
				                                    		for(var i=0; i<sisa; i++){
				                                    			a.push(records[i].data);
				                                    		}
				                                    		Ext.each(a, ddSource.grid.store.remove, ddSource.grid.store);
						                                    Q($this.ArrayStore.unit2).add(a);
						                                    $this.Grid.unit2.store.sort('text', 'ASC');
						                                    Ext.Msg.alert('Informasi','List tidak boleh lebih dari 8');
						                                    return true
				                                    	}else{
				                                    		Ext.Msg.alert('Informasi','List tidak boleh lebih dari 8');
				                                    		return false;
				                                    	}
				                                    }
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        })
							]
						},Q().fieldset({
							items:[
								Q().input({
									label:'Sub Jenis',
									width: 350,
									items:[
										$this.Dropdown.subJenis=Q().dropdown({
											emptyText: 'Semua',
											width: 150
										})
									]
								}),
								Q().input({
									label:'Kepemilikan',
									items:[
										$this.Dropdown.milik=Q().dropdown({
											emptyText: 'Semua',
											width: 150
										})
									]
								}),
								//$this.CheckBox.kelompok=Q().checkbox({boxLabel:'Tanpa Pengelompokan',checked:true})
							]
						})
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapMinimumStok.init();
