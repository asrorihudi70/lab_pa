var print=true;
var excel= false;
var DlgLapMutasiObatDetail={
	ArrayStore:{
		unit1:Q().arraystore(),
		unit2:Q().arraystore()
	},
	Dropdown:{
		bulan:null,
		subJenis:null,
		milik:null
	},
	Grid:{
		unit1:null,
		unit2:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	NumberField:{
		tahun:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/lap_mutasiobatdetail/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.ArrayStore.unit1).add(r.data.unit);
					Q($this.Dropdown.milik).add(r.data.milik);
					Q($this.Dropdown.subJenis).add(r.data.sub_jenis);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		var sendDataArrayUnit = [];
		$this.ArrayStore.unit2.each(function(record){
			var recordArrayUnit= [record.get("text")];
			sendDataArrayUnit.push(recordArrayUnit);
		});
		
		params['kd_milik']=$this.Dropdown.milik.getValue();
		params['sub_jenis']=$this.Dropdown.subJenis.getValue();
		// params['month']=$this.Dropdown.bulan.getValue()+1;
		// params['year']=$this.NumberField.tahun.getValue();
		params['start_date']= timestimetodate($this.DateField.startDate.getValue());
		params['last_date'] = timestimetodate($this.DateField.endDate.getValue());
		params['tmp_unit'] 	= sendDataArrayUnit;
		params['excel'] 	= excel;
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_mutasiobatdetail/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_mutasiobatdetail/doPrint");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Mutasi Obat (Detail)',
			fbar:[
			{
			   xtype: 'checkbox',
			   id: 'CekLapPilihExcelGudangFarmasiStokObatPerUnit',
			   hideLabel:false,
			   boxLabel: 'Excel',
			   checked: false,
			   listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihExcelGudangFarmasiStokObatPerUnit').getValue()===true)
						{
							excel =true;
						}
						else
						{
							excel =false;
						}
					}
			   }
			},
			{
				   xtype: 'checkbox',
				   id: 'CekLapPilihSemuaGudangFarmasiMutasiObatDetail',
				   hideLabel:false,
				   boxLabel: 'Pilih Semua',
				   checked: false,
				   listeners: 
				   {
						check: function()
						{
						   if(Ext.getCmp('CekLapPilihSemuaGudangFarmasiMutasiObatDetail').getValue()===true)
							{
								 $this.Grid.unit1.getSelectionModel().selectAll();
							}
							else
							{
								$this.Grid.unit1.getSelectionModel().clearSelections();
							}
						}
				   }
				},
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Preview',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Close',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						{
							layout:'hbox',
							border: false,
							items:[
								$this.Grid.unit1=new Ext.grid.GridPanel({
						            ddGroup          : 'secondGridDDGroup',
						            store            : $this.ArrayStore.unit1,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            flex			: 1,
						            height           : 200,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : 'Unit',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                sortable: true,
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                sortable: true,
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit1.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'firstGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
				                                    $this.Grid.unit1.store.add(records);
				                                    $this.Grid.unit1.store.sort('KD_UNIT', 'ASC');
				                                    return true
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        }),
						        $this.Grid.unit2=new Ext.grid.GridPanel({
						            ddGroup          : 'firstGridDDGroup',
						            store            : $this.ArrayStore.unit2,
						            autoScroll       : true,
						            columnLines      : true,
						            border           : true,
						            enableDragDrop   : true,
						            style:'margin-left:-1px;',
						            flex			: 1,
						            height           : 200,
						            stripeRows       : true,
						            trackMouseOver   : true,
						            title            : 'Unit',
						            colModel         : new Ext.grid.ColumnModel([
                                        {
                                                dataIndex: 'id',
                                                hidden : true
                                        },{
                                                header: 'Nama',
                                                dataIndex: 'text',
                                                width: 50
                                        }
                                    ]),
                                    listeners : {
					                    afterrender : function(comp) {
						                    var secondGridDropTargetEl = $this.Grid.unit2.getView().scroller.dom;
						                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					                            ddGroup    : 'secondGridDDGroup',
					                            notifyDrop : function(ddSource, e, data){
				                                    var records =  ddSource.dragData.selections;
				                                    if((Q($this.ArrayStore.unit2).size()+records.length)<=8){
					                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
					                                    $this.Grid.unit2.store.add(records);
					                                    $this.Grid.unit2.store.sort('text', 'ASC');
					                                    return true
				                                    }else if((Q($this.ArrayStore.unit2).size()+records.length)>8){
				                                    	if(Q($this.ArrayStore.unit2).size()<8){
				                                    		var sisa=8-Q($this.ArrayStore.unit2).size();
				                                    		var a=[];
				                                    		for(var i=0; i<sisa; i++){
				                                    			a.push(records[i].data);
				                                    		}
				                                    		Ext.each(a, ddSource.grid.store.remove, ddSource.grid.store);
						                                    Q($this.ArrayStore.unit2).add(a);
						                                    $this.Grid.unit2.store.sort('text', 'ASC');
						                                    Ext.Msg.alert('Informasi','List tidak boleh lebih dari 8');
						                                    return true
				                                    	}else{
				                                    		Ext.Msg.alert('Informasi','List tidak boleh lebih dari 8');
				                                    		return false;
				                                    	}
				                                    }
					                            }
						                    });
					                    }
					                },
					                viewConfig:{
			                            forceFit: true
				                    }
						        })
							]
						},
						Q().fieldset({
							items:[
								/* Q().input({
									label:'Bulan',
									items:[
										$this.Dropdown.bulan=Q().dropdown({
											value:new Date().getMonth(),
											data:[
												{id	:0,text	:'Januari'},
												{id	:1,text	:'Februari'},
												{id	:2,text	:'Maret'},
												{id	:3,text	:'April'},
												{id	:4,text	:'Mei'},
												{id	:5,text	:'Juni'},
												{id	:6,text	:'Juli'},
												{id	:7,text	:'Agustus'},
												{id	:8,text	:'September'},
												{id	:9,text	:'Oktober'},
												{id	:10,text:'November'},
												{id	:11,text:'Desember'}
											]								
										})
									]
								}),
								Q().input({
									label:'Tahun',
									items:[
										$this.NumberField.tahun=new Ext.form.NumberField({
											width: 50,
											value:new Date().getFullYear(),
											style:'text-align:right'
										})
									]
								}), */
								Q().input({
									label:'Periode',
									width: 350,
									items:[
										$this.DateField.startDate=Q().datefield({
											width:100,format: 'M/Y'
										}),
										Q().display({value:'s/d', width:20}),
										$this.DateField.endDate=Q().datefield({
											width:100,format: 'M/Y'
										})
									]
								}),
								Q().input({
									label:'Sub Jenis',
									items:[
										$this.Dropdown.subJenis=Q().dropdown({
											width: 150,
											emptyText: 'Semua'
										})
									]
								}),
								Q().input({
									label:'Kepemilikan',
									items:[
										$this.Dropdown.milik=Q().dropdown({
											width:150,
											emptyText: 'Semua'
										})
									]
								})
							]
						})
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapMutasiObatDetail.init();