
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapNilaiPersediaanDetail;
var selectNamaLapNilaiPersediaanDetail;
var now = new Date();
var selectSetPerseorangan;
var frmLapNilaiPersediaanDetail;
var varLapLapNilaiPersediaanDetail= ShowFormLapLapNilaiPersediaanDetail();
var tglAwal;
var tglAkhir;
var tipe;
var winLapNilaiPersediaanDetailReport;
var cboUnitFar_LapNilaiPersediaanDetail;






function ShowFormLapLapNilaiPersediaanDetail()
{
    frmLapNilaiPersediaanDetail= fnLapNilaiPersediaanDetail();
    frmLapNilaiPersediaanDetail.show();
	loadDataComboUnitFar_LapNilaiPersediaanDetail();
};


function fnLapNilaiPersediaanDetail()
{
    winLapNilaiPersediaanDetailReport = new Ext.Window
    (
        {
            id: 'winLapNilaiPersediaanDetailReport',
            title: 'Laporan Nilai Persediaan Detail',
            closeAction: 'destroy',
            width: 420,
            height: 160,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapNilaiPersediaanDetail()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'OK',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapNilaiPersediaanDetail',
						handler: function()
						{
							if (ValidasiReportLapNilaiPersediaanDetail() === 1)
							{
								var params={
									kd_unit_far:Ext.getCmp('cboUnitFar_LapNilaiPersediaanDetail').getValue(),
									tglAwal:Ext.getCmp('dtpBulanAwalFilter_NilaiPersediaanDetail').getValue(),
									tglAkhir:Ext.getCmp('dtpBulanAkhirFilter_NilaiPersediaanDetail').getValue(),
									periodeAwal:Ext.get('dtpBulanAwalFilter_NilaiPersediaanDetail').getValue(),
									periodeAkhir:Ext.get('dtpBulanAkhirFilter_NilaiPersediaanDetail').getValue()
								} 
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_nilaipersediaan/cetakNilaiPersediaanDetail");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapNilaiPersediaanDetailReport.close(); 
								
							};
							
							
						}
					},
					{
						xtype: 'button',
						text: 'Batal' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapNilaiPersediaanDetail',
						handler: function()
						{
							winLapNilaiPersediaanDetailReport.close();
						}
					}
			]

        }
    );

    return winLapNilaiPersediaanDetailReport;
};


function ItemLapNilaiPersediaanDetail()
{
    var PnlLapLapNilaiPersediaanDetail = new Ext.Panel
    (
        {
            id: 'PnlLapLapNilaiPersediaanDetail',
            fileUpload: true,
            layout: 'form',
            height: '150',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapNilaiPersediaanDetail_Atas(),
            ]
        }
    );

    return PnlLapLapNilaiPersediaanDetail;
};

function ValidasiReportLapNilaiPersediaanDetail()
{
	var x=1;
	if(Ext.getCmp('cboUnitFar_LapNilaiPersediaanDetail').getValue() === ''){
		ShowPesanWarningLapNilaiPersediaanDetailReport('Unit Far tidak boleh kosong','Warning');
        x=0;
	}
    return x;
};

function ShowPesanWarningLapNilaiPersediaanDetailReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapNilaiPersediaanDetail_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  395,
            height: 75,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Unit Far '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            mComboUnitFarLapNilaiPersediaanDetail(),
			
			{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode '
            }, 
			{
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 40,
				xtype: 'datefield',
				id: 'dtpBulanAwalFilter_NilaiPersediaanDetail',
				format: 'M/Y',
				value: now
			}, 
			{
				x: 225,
				y: 40,
				xtype: 'label',
				text: ' s/d Bulan'
			}, 
			{
				x: 280,
				y: 40,
				xtype: 'datefield',
				id: 'dtpBulanAkhirFilter_NilaiPersediaanDetail',
				format: 'M/Y',
				value: now,
				width: 100
			},
            ]
        }]
    };
    return items;
};


function getItemLapLapNilaiPersediaanDetail_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

var selectSetPilihan;

function loadDataComboUnitFar_LapNilaiPersediaanDetail(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/lap_nilaipersediaan/getUnitFar",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUnitFar_LapNilaiPersediaanDetail.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_UnitFar_LapNilaiPersediaanDetail.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_UnitFar_LapNilaiPersediaanDetail.add(recs);
				console.log(o);
			}
		}
	});
}


function mComboUnitFarLapNilaiPersediaanDetail()
{
	var Field = ['kd_unit_far','nm_unit_far'];
    ds_UnitFar_LapNilaiPersediaanDetail = new WebApp.DataStore({fields: Field});
    cboUnitFar_LapNilaiPersediaanDetail = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboUnitFar_LapNilaiPersediaanDetail',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: ds_UnitFar_LapNilaiPersediaanDetail,
                valueField: 'kd_unit_far',
                displayField: 'nm_unit_far',
                value:'GUDANG APOTEK',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboUnitFar_LapNilaiPersediaanDetail;
};
