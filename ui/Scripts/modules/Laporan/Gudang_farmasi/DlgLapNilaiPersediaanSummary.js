
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapNilaiPersediaanSummary;
var selectNamaLapNilaiPersediaanSummary;
var now = new Date();
var selectSetPerseorangan;
var frmLapNilaiPersediaanSummary;
var varLapLapNilaiPersediaanSummary= ShowFormLapLapNilaiPersediaanSummary();
var tglAwal;
var tglAkhir;
var tipe;
var winLapNilaiPersediaanSummaryReport;
var cboUnitFar_LapNilaiPersediaanSummary;






function ShowFormLapLapNilaiPersediaanSummary()
{
    frmLapNilaiPersediaanSummary= fnLapNilaiPersediaanSummary();
    frmLapNilaiPersediaanSummary.show();
	loadDataComboUnitFar_LapNilaiPersediaanSummary();
};


function fnLapNilaiPersediaanSummary()
{
    winLapNilaiPersediaanSummaryReport = new Ext.Window
    (
        {
            id: 'winLapNilaiPersediaanSummaryReport',
            title: 'Laporan Nilai Persediaan Summary',
            closeAction: 'destroy',
            width: 420,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapNilaiPersediaanSummary()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'OK',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapNilaiPersediaanSummary',
						handler: function()
						{
							
							var params={
								tglAwal:Ext.getCmp('dtpBulanAwalFilter_NilaiPersediaanSummary').getValue(),
								tglAkhir:Ext.getCmp('dtpBulanAkhirFilter_NilaiPersediaanSummary').getValue(),
								periodeAwal:Ext.get('dtpBulanAwalFilter_NilaiPersediaanSummary').getValue(),
								periodeAkhir:Ext.get('dtpBulanAkhirFilter_NilaiPersediaanSummary').getValue()
							} 
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_nilaipersediaan/cetakNilaiPersediaanSummary");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//winLapNilaiPersediaanSummaryReport.close(); 
							
							
						}
					},
					{
						xtype: 'button',
						text: 'Batal' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapNilaiPersediaanSummary',
						handler: function()
						{
							winLapNilaiPersediaanSummaryReport.close();
						}
					}
			]

        }
    );

    return winLapNilaiPersediaanSummaryReport;
};


function ItemLapNilaiPersediaanSummary()
{
    var PnlLapLapNilaiPersediaanSummary = new Ext.Panel
    (
        {
            id: 'PnlLapLapNilaiPersediaanSummary',
            fileUpload: true,
            layout: 'form',
            height: '150',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapNilaiPersediaanSummary_Atas(),
            ]
        }
    );

    return PnlLapLapNilaiPersediaanSummary;
};


function ShowPesanWarningLapNilaiPersediaanSummaryReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapNilaiPersediaanSummary_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  395,
            height: 45,
            anchor: '100% 100%',
            items: [
			{
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode '
            }, 
			{
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 10,
				xtype: 'datefield',
				id: 'dtpBulanAwalFilter_NilaiPersediaanSummary',
				format: 'M/Y',
				value: now
			}, 
			{
				x: 225,
				y: 10,
				xtype: 'label',
				text: ' s/d Bulan'
			}, 
			{
				x: 280,
				y: 10,
				xtype: 'datefield',
				id: 'dtpBulanAkhirFilter_NilaiPersediaanSummary',
				format: 'M/Y',
				value: now,
				width: 100
			},
            ]
        }]
    };
    return items;
};


function getItemLapLapNilaiPersediaanSummary_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

