var DlgLapPenerimaanJatuhTempoDetail={
	App	:{
		title:'Laporan Penerimaan Jatuh Tempo (detail)',
		lastUpdate:'2015-08-05',
		createBy:'Asep Kamaludin'
	},
	Dropdown:{
		vendor:null,
		bayar:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/lap_penerimaanjatuhtempodetail/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.Dropdown.vendor).add(r.data.vendor);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		params['start_date']=timestimetodate($this.DateField.startDate.getValue());
		params['last_date']=timestimetodate($this.DateField.endDate.getValue());
		params['vendor']=$this.Dropdown.vendor.getValue();
		params['bayar']=$this.Dropdown.bayar.getValue();
		
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/gudang_farmasi/lap_penerimaanjatuhtempodetail/doPrint",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.close();
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	init:function(){
		var $this=this;
		
		$this.Window.main=Q().window({
			title:$this.App.title,
			fbar:[
				new Ext.Button({
					text:'Ok',
					handler:function(){
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Batal',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Periode',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield({
									width:100
								}),
								Q().display({value:'s/d',width:20}),
								$this.DateField.endDate=Q().datefield({
									width:100
								})
							]
						}),
						Q().input({
							label:'Vendor / PBF',
							items:[
								$this.Dropdown.vendor=Q().dropdown({
									width: 240,
									emptyText: 'Semua'
								})
							]
						}),Q().input({
							label:'Pembayaran',
							items:[
								$this.Dropdown.bayar=Q().dropdown({
									emptyText: 'Semua',
									data : [{id:1,text:'Sudah Bayar'},{id:2,text:'Belum Bayar'}]
								})
							]
						})
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapPenerimaanJatuhTempoDetail.init();
console.info(DlgLapPenerimaanJatuhTempoDetail);