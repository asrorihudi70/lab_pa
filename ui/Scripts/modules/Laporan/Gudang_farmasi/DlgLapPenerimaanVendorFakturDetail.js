DlgLapPenerimaanVendorFakturDetail = {
	Dropdown:{
		vendor:null,
		bayar:null,
		tempo:null,
		jenisObat:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	CheckBox:{
		kelompok:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/lap_penerimaanvendorfkaturdetail/getData",
			success: function(r){
				if(r.result=='SUCCESS'){
					Q($this.Dropdown.vendor).add(r.data.vendor);
					//HUDI
					//03-11-2020
					//Penambahan data jenis obat di combobox
					Q($this.Dropdown.jenisObat).add(r.data.jenis_obat);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		params['start_date']=timestimetodate($this.DateField.startDate.getValue());
		params['last_date']=timestimetodate($this.DateField.endDate.getValue());
		params['vendor']=$this.Dropdown.vendor.getValue();
		params['bayar']=$this.Dropdown.bayar.getValue();
		params['tempo']=$this.Dropdown.tempo.getValue();
		params['jenis_obat']=$this.Dropdown.jenisObat.getValue();
		
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_penerimaanvendorfkaturdetail/doPrint/"+print);
		// if(print == true){
			// form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_penerimaanvendorfkaturdetail/doPrint");
		// } else{
			// form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_penerimaanvendorfkaturdetail/doPrint");
		// }
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();

	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Penerimaan dari Vendor per Faktur (detail)',
			fbar:[
				new Ext.Button({
					text:'Excel',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'PDF',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Batal',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Periode',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield({
									width:100
								}),
								Q().display({value:'s/d', width:20}),
								$this.DateField.endDate=Q().datefield({
									width:100
								})
							]
						}),
						Q().input({
							label:'Vendor / PBF',
							items:[
								$this.Dropdown.vendor=Q().dropdown({
									width: 225,
									emptyText: 'Semua'
								})
							]
						}),
						Q().input({
							label:'Pembayaran',
							items:[
								$this.Dropdown.bayar=Q().dropdown({
									emptyText: 'Semua',
									data : [{id:1,text:'Sudah Bayar'},{id:2,text:'Belum Bayar'}]
								})
							]
						}),
						Q().input({
							label:'Jatuh Tempo',
							items:[
								$this.Dropdown.tempo=Q().dropdown({
									emptyText: 'Semua',
									width: 150,
									data : [{id:1,text:'Sudah Jatuh Tempo'},{id:2,text:'Belum Jatuh Tempo'}]
								})
							]
						}),
						Q().input({
							label:'Jenis',
							items:[
								$this.Dropdown.jenisObat=Q().dropdown({
									width: 200,
									emptyText: 'Semua',
									//HUDI
									//03-11-2020
									//data : [{id:1,text:'Reagen'},{id:2,text:'BMHP'},{id:3,text:'Obat'},{id:4,text:'Obat gigi'}] ->> dinonaktifkan
								})
							]
						}),
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapPenerimaanVendorFakturDetail.init();