var print=true;
var DlgLapPengeluaranUnitFakturDetail={
	CheckBox:{
		qty:null,
		total:null
	},
	Dropdown:{
		unit:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/lap_pengeluaranunitfakturdetail/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.Dropdown.unit).add(r.data.unit);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		params['start_date']=timestimetodate($this.DateField.startDate.getValue());
		params['last_date']=timestimetodate($this.DateField.endDate.getValue());
		params['unit']=$this.Dropdown.unit.getValue();
		params['harga']=$this.CheckBox.harga.getValue();
		params['qty']=$this.CheckBox.qty.getValue();
		
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_pengeluaranunitfakturdetail/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_pengeluaranunitfakturdetail/doPrint");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
		
		// $.ajax({
			// type: 'POST',
			// dataType:'JSON',
			// data:params,
			// url:baseURL + "index.php/gudang_farmasi/lap_pengeluaranunitfakturdetail/doPrint",
			// success: function(r){
				// loadMask.hide();
				// if(r.result=='SUCCESS'){
					// $this.Window.main.close();
					// window.open(r.data, '_blank', 'location=0,resizable=1', false);
				// }else{
					// Ext.Msg.alert('Gagal',r.message);
				// }
			// },
			// error: function(jqXHR, exception) {
				// loadMask.hide();
				// Nci.ajax.ErrorMessage(jqXHR, exception);
			// }
		// });
	},
	init:function(){
		var $this=this;
		
		$this.Window.main=Q().window({
			title:'Laporan Pengeluaran Ke Unit Per Faktur (detail)',
			fbar:[
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Preview',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Batal',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Periode',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield(),
								Q().display({value:'s/d'}),
								$this.DateField.endDate=Q().datefield()
							]
						}),
						Q().input({
							label:'Unit',
							items:[
								$this.Dropdown.unit=Q().dropdown({
									width: 200,
									emptyText: 'Semua'
								})
							]
						}),
						Q().input({
							label:'Kolom',
							items:[
								$this.CheckBox.qty=Q().checkbox({boxLabel: 'Qty'}),
								Q().display({width: 10}),
								$this.CheckBox.harga=Q().checkbox({boxLabel : 'Harga'})
							]
						})
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapPengeluaranUnitFakturDetail.init();