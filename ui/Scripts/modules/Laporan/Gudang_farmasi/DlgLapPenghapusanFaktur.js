var print=true;
var DlgLapPenghapusanFaktur={
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		params['start_date']=timestimetodate($this.DateField.startDate.getValue());
		params['last_date']=timestimetodate($this.DateField.endDate.getValue());
		
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_penghapusanfaktur/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_penghapusanfaktur/doPrint");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
	},
	init:function(){
		var $this=this;
		
		$this.Window.main=Q().window({
			title:'LAPORAN PENGHAPUSAN OBAT PER FAKTUR',
			fbar:[
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Preview',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Batal',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Periode',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield({
									width:100
								}),
								Q().display({value:'s/d',width:20}),
								$this.DateField.endDate=Q().datefield({
									width:100
								})
							]
						})
					]
				})
			]
		}).show();
	}
};
DlgLapPenghapusanFaktur.init();