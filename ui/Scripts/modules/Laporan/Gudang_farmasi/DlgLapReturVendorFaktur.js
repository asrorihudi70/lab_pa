var print=true;
var DlgLapReturVendorFaktur={
	Dropdown:{
		vendor:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/lap_returvendorfaktur/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.Dropdown.vendor).add(r.data.vendor);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		params['start_date']=timestimetodate($this.DateField.startDate.getValue());
		params['last_date']=timestimetodate($this.DateField.endDate.getValue());
		params['vendor']=$this.Dropdown.vendor.getValue();
		// $.ajax({
			// type: 'POST',
			// dataType:'JSON',
			// data:params,
			// url:baseURL + "index.php/gudang_farmasi/lap_returvendorfaktur/doPrint",
			// success: function(r){
				// loadMask.hide();
				// if(r.result=='SUCCESS'){
					// $this.Window.main.close();
					// window.open(r.data, '_blank', 'location=0,resizable=1', false);
				// }else{
					// Ext.Msg.alert('Gagal',r.message);
				// }
			// },
			// error: function(jqXHR, exception) {
				// loadMask.hide();
				// Nci.ajax.ErrorMessage(jqXHR, exception);
			// }
		// });
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_returvendorfaktur/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_returvendorfaktur/doPrint");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
	},
	init:function(){
		var $this=this;
		
		$this.Window.main=Q().window({
			title:'Laporan Retur Vendor pe Faktur',
			fbar:[
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Preview',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Batal',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Periode',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield({
									width:100
								}),
								Q().display({value:'s/d',width:20}),
								$this.DateField.endDate=Q().datefield({
									width:100
								})
							]
						}),
						Q().input({
							label:'Vendor / PBF',
							items:[
								$this.Dropdown.vendor=Q().dropdown({
									width: 240,
									emptyText: 'Semua'
								})
							]
						})
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapReturVendorFaktur.init();