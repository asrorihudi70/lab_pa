var print=true;
var DlgLapSlowMoving={
	CheckBox:{
		qty:null,
		total:null
	},
	Dropdown:{
		unit:null,
		jenis_obat:null,
		sub_jenis_obat:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/lap_slowmoving/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.Dropdown.unit).add(r.data.unit);
					Q($this.Dropdown.jenis_obat).add(r.data.jenis_obat);
					Q($this.Dropdown.sub_jenis_obat).add(r.data.sub_jenis_obat);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		params['start_date']=timestimetodate($this.DateField.startDate.getValue());
		params['last_date']=timestimetodate($this.DateField.endDate.getValue());
		params['unit']=$this.Dropdown.unit.getValue();
		params['jenis_obat']=$this.Dropdown.jenis_obat.getValue();
		params['sub_jenis_obat']=$this.Dropdown.sub_jenis_obat.getValue();
		params['limit'] = Ext.getCmp('txtJmlRecord').getValue();
		
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_slowmoving/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/gudang_farmasi/lap_slowmoving/doPrint");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
		
		
	},
	init:function(){
		var $this=this;
		
		$this.Window.main=Q().window({
			title:'Laporan Slow Moving',
			fbar:[
				new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Preview',
					handler:function(){
						print=false;
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Batal',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Periode',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield({
									format:'M-Y',
									width: 100,
								}),
								Q().display({value:'s/d',width:20}),
								$this.DateField.endDate=Q().datefield({
									format:'M-Y',
									width: 100,
								})
							]
						}),
						Q().input({
							label:'Unit',
							items:[
								$this.Dropdown.unit=Q().dropdown({
									width: 240,
									emptyText: 'Semua'
								})
							]
						}),
						Q().input({
							label:'Jenis Obat',
							items:[
								$this.Dropdown.jenis_obat=Q().dropdown({
									width: 240,
									emptyText: 'Semua'
								})
							]
						}),
						Q().input({
							label:'Sub Jenis Obat',
							items:[
								$this.Dropdown.sub_jenis_obat=Q().dropdown({
									width: 240,
									emptyText: 'Semua'
								})
							]
						}),
						Q().input({
							label:'Maksimal Record',
							items:[
								{
								  xtype: 'textfield',
								  id: 'txtJmlRecord',
								  name: 'txtJmlRecord',
								  width: 50
								},
							]
						}),
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapSlowMoving.init();