var DlgLapStokObatSemuaUnit={
	Dropdown:{
		subJenis:null,
		milik:null
	},
	CheckBox:{
		kelompok:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/lap_stokobatsemuaunit/getData",
			success: function(r){
				if(r.result=='SUCCESS'){
					Q($this.Dropdown.milik).add(r.data.milik);
					Q($this.Dropdown.subJenis).add(r.data.sub_jenis);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params=[];
		if($this.CheckBox.kelompok.getValue()==true){
			params.push({name:'group',value:$this.CheckBox.kelompok.getValue()});
		}
		params.push({name:'milik',value:$this.Dropdown.milik.getValue()});
		params.push({name:'sub_jenis',value:$this.Dropdown.subJenis.getValue()});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/gudang_farmasi/lap_stokobatsemuaunit/doPrint",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.close();
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Stok Obat Semua Unit',
			fbar:[
				new Ext.Button({
					text:'Ok',
					handler:function(){
						$this.doPrint()
					}
				}),
				new Ext.Button({
					text:'Batal',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Sub Jenis',
							width: 300,
							items:[
								$this.Dropdown.subJenis=Q().dropdown({
									emptyText: 'Semua',
									width: 150
								})
							]
						}),
						Q().input({
							label:'Kepemilikan',
							items:[
								$this.Dropdown.milik=Q().dropdown({
									emptyText: 'Semua',
									width: 150
								})
							]
						}),
						$this.CheckBox.kelompok=Q().checkbox({boxLabel:'Tanpa Pengelompokan',checked:true})
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapStokObatSemuaUnit.init();