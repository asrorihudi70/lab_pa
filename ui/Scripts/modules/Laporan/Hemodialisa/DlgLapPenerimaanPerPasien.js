var PenerimaanPerPasien={
	ArrayStore:{
		unit1:Q().arraystore(),
		unit2:Q().arraystore()
	},
	CheckBox:{
		shiftall:null,
		shift1:null,
		shift2:null,
		shift3:null,
		excel:null,
		semua_pay:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	DropDown:{
		kelompok1:null,
		kelompok2:null,
		kelompok3:null
	},
	Grid:{
		unit1:null,
		unit2:null
	},
	Window:{
		main:null
	},
	doPrint:function(){
		var $this=this;
		var kd_pay='';
		for(var i=0; i<$this.ArrayStore.unit2.getRange().length ; i++){
			kd_pay += "'" + $this.ArrayStore.unit2.getRange()[i].data.id + "',";
		}
		
		var params={
			jenis_cust:$this.DropDown.kelompok2.getValue(),
			kd_customer:$this.DropDown.kelompok3.getValue(),
			start_date:Q($this.DateField.startDate).val(),
			last_date:Q($this.DateField.endDate).val(),
			shift1:$this.CheckBox.shift1.getValue(),
			shift2:$this.CheckBox.shift2.getValue(),
			shift3:$this.CheckBox.shift3.getValue(),
			excel:$this.CheckBox.excel.getValue()
		} ;
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		form.setAttribute("action", baseURL + "index.php/hemodialisa/lappenerimaan_perpasien/doPrint");
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
	},
	getSelect:function(kd){
		var $this=this;
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/hemodialisa/lappenerimaan_perpasien/getSelect",
			data:{cust:kd},
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.DropDown.kelompok3).reset();
					Q($this.DropDown.kelompok3).add({id:'',text:'Semua'});
					Q($this.DropDown.kelompok3).add(r.data);
					Q($this.DropDown.kelompok3).val('');
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Penerimaan Per Pasien',
			fbar:[
				new Ext.Button({
					text:'Ok',
					handler:function(){
						$this.doPrint();
					}
				}),
				new Ext.Button({
					text:'Close',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().fieldset({
							title:'Periode',
							width: 380,
							items:[
//								Q().input({
//									label:'Pengelompokan Laporan',
//									items:[
//										$this.DropDown.kelompok1=Q().dropdown({
//											width: 200
//										})
//									]
//								}),
								Q().input({
									label:'Periode',
									items:[
										$this.DateField.startDate=Q().datefield(),
										Q().display({value:'s/d'}),
										$this.DateField.endDate=Q().datefield()
									]
								}),
								Q().input({
									label:'Kelompok Pasien',
									items:[
										$this.DropDown.kelompok2=Q().dropdown({
											width: 200,
											data:[
												{id:'',text:'Semua'},
												{id:1,text:'Perorangan'},
												{id:2,text:'Perusahaan'},
												{id:3,text:'Ansuransi'}
											],
											select:function(a){
												if(a.getValue()!=''){
													$this.getSelect(a.getValue()-1);
												}else{
													Q($this.DropDown.kelompok3).reset();
													Q($this.DropDown.kelompok3).add({id:'',text:'Semua'});
												}
											}
										})
									]
								}),
								
								Q().input({
									separator:'',
									items:[
										$this.DropDown.kelompok3=Q().dropdown({
											width: 200,
											data:[
												{id:'',text:'Semua'}
											]
										})
									]
								}),
								Q().input({
									xWidth:100,
									separator:'',
									items:[
										Q().display({value:'Shift All'}),
										$this.CheckBox.shiftall=Q().checkbox({checked:true,
										handler: function (field, value) 
											{
												if (value === true){
													$this.CheckBox.shift1.setValue(true);
													$this.CheckBox.shift2.setValue(true);
													$this.CheckBox.shift3.setValue(true);
													$this.CheckBox.shift1.disable();
													$this.CheckBox.shift2.disable();
													$this.CheckBox.shift3.disable();
												}else{
													$this.CheckBox.shift1.setValue(false);
													$this.CheckBox.shift2.setValue(false);
													$this.CheckBox.shift3.setValue(false);
													$this.CheckBox.shift1.enable();
													$this.CheckBox.shift2.enable();
													$this.CheckBox.shift3.enable();
												}
											}
										}),
										Q().display({value:'Shift 1'}),
										$this.CheckBox.shift1=Q().checkbox({checked:true}),
										Q().display({value:'Shift 2'}),
										$this.CheckBox.shift2=Q().checkbox({checked:true}),
										Q().display({value:'Shift 3'}),
										$this.CheckBox.shift3=Q().checkbox({checked:true})
									]
								}),
								Q().input({
									label:'Type File',
									items:[
										Q().display({value:'Excel'}),
										$this.CheckBox.excel=Q().checkbox({checked:false}),
										
									]
								}),
								
							]
						})
					]
				})
			]
		});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/hemodialisa/lappenerimaan_perpasien/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.show();
					Q($this.ArrayStore.unit1).add(r.data.cust);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}
}
PenerimaanPerPasien.init();