
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapJumlahJenisPemeriksaanHemodialisa;
var selectNamaLapJumlahJenisPemeriksaanHemodialisa;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapJumlahJenisPemeriksaanHemodialisa;
var varLapJumlahJenisPemeriksaanHemodialisa= ShowFormLapJumlahJenisPemeriksaanHemodialisa();
var selectSetUmum;
var selectSetkelpas;

function ShowFormLapJumlahJenisPemeriksaanHemodialisa()
{
    frmDlgLapJumlahJenisPemeriksaanHemodialisa= fnDlgLapJumlahJenisPemeriksaanHemodialisa();
    frmDlgLapJumlahJenisPemeriksaanHemodialisa.show();
};

function fnDlgLapJumlahJenisPemeriksaanHemodialisa()
{
    var winLapJumlahJenisPemeriksaanHemodialisaReport = new Ext.Window
    (
        {
            id: 'winLapJumlahJenisPemeriksaanHemodialisaReport',
            title: 'Laporan Jumlah Jenis Pemeriksaan',
            closeAction: 'destroy',
            width:400,
            height: 190,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapJumlahJenisPemeriksaanHemodialisa()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapJumlahJenisPemeriksaanHemodialisaReport;
};


function ItemDlgLapJumlahJenisPemeriksaanHemodialisa()
{
    var PnlLapJumlahJenisPemeriksaanHemodialisa = new Ext.Panel
    (
        {
            id: 'PnlLapJumlahJenisPemeriksaanHemodialisa',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapJumlahJenisPemeriksaanHemodialisa_Atas(),
                getItemLapJumlahJenisPemeriksaanHemodialisa_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'OK',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapJumlahJenisPemeriksaanHemodialisa',
                            handler: function()
                            {
								if (ValidasiReportLapJumlahJenisPemeriksaanHemodialisa() === 1)
								{
									if (Ext.getCmp('Shift_All_LapJumlahJenisPemeriksaanHemodialisa').getValue() === true){
										shift='All';
										shift1='false';
										shift2='false';
										shift3='false';
									}else{
										shift='';
										if (Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaanHemodialisa').getValue() === true){
											shift1='true';
										} else{
											shift1='false';
										}
										if (Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaanHemodialisa').getValue() === true){
											shift2='true';
										}else{
											shift2='false';
										}
										if (Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaanHemodialisa').getValue() === true){
											shift3='true';
										}else{
											shift3='false';
										}
									}
									
									var params={
										tglAwal:Ext.getCmp('dtpTglAwalFilterLapJumlahJenisPemeriksaanHemodialisa').getValue(),
										tglAkhir:Ext.getCmp('dtpTglAkhirFilterLapJumlahJenisPemeriksaanHemodialisa').getValue(),
										excel:Ext.getCmp('CetakExcel').getValue(),
										shift:shift,
										shift1:shift1,
										shift2:shift2,
										shift3:shift3,
									} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/hemodialisa/lapjumlahjenispemeriksaan/cetakJumlahJenisPemeriksaan");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
								};
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapJumlahJenisPemeriksaanHemodialisa',
                            handler: function()
                            {
								frmDlgLapJumlahJenisPemeriksaanHemodialisa.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapJumlahJenisPemeriksaanHemodialisa;
};

function ValidasiReportLapJumlahJenisPemeriksaanHemodialisa()
{
    var x=1;
    if(Ext.getCmp('dtpTglAwalFilterLapJumlahJenisPemeriksaanHemodialisa').getValue() > Ext.getCmp('dtpTglAkhirFilterLapJumlahJenisPemeriksaanHemodialisa').getValue())
    {
        ShowPesanWarningLapJumlahJenisPemeriksaanHemodialisaReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapJumlahJenisPemeriksaanHemodialisa').getValue() === false && Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaanHemodialisa').getValue() === false && Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaanHemodialisa').getValue() === false && Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaanHemodialisa').getValue() === false){
		ShowPesanWarningLapJumlahJenisPemeriksaanHemodialisaReport('Shift belum dipilih!','Laporan Regis Laboratorium');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningLapJumlahJenisPemeriksaanHemodialisaReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapJumlahJenisPemeriksaanHemodialisa_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 70,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterLapJumlahJenisPemeriksaanHemodialisa',
                format: 'd-M-Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 10,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterLapJumlahJenisPemeriksaanHemodialisa',
                format: 'd-M-Y',
                value: now,
                width: 100
            },
			{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Tipe File '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            {	
				x: 120,
                y: 40,
				xtype: 'checkbox',
				id: 'CetakExcel',
				hideLabel:false,
				boxLabel: 'Cetak Excel',
				checked: false,
				listeners: 
				{
					
			   }
			},
            ]
        }]
    };
    return items;
};

function getItemLapJumlahJenisPemeriksaanHemodialisa_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
				xtype: 'fieldset',
				title: 'Shift',
				autoHeight: true,
				width: '373px',
				defaultType: 'checkbox', // each item will be a checkbox
				items: 
				[
					{
						xtype: 'checkboxgroup',
						items: [
							{
								boxLabel: 'Semua',
								name: 'Shift_All_LapJumlahJenisPemeriksaanHemodialisa',
								id : 'Shift_All_LapJumlahJenisPemeriksaanHemodialisa',
								handler: function (field, value) {
									if (value === true){
										Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaanHemodialisa').setValue(true);
										Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaanHemodialisa').setValue(true);
										Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaanHemodialisa').setValue(true);
										Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaanHemodialisa').disable();
										Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaanHemodialisa').disable();
										Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaanHemodialisa').disable();
									}else{
											Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaanHemodialisa').setValue(false);
											Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaanHemodialisa').setValue(false);
											Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaanHemodialisa').setValue(false);
											Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaanHemodialisa').enable();
											Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaanHemodialisa').enable();
											Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaanHemodialisa').enable();
										}
								}
							},
							{
								boxLabel: 'Shift 1',
								name: 'Shift_1_LapJumlahJenisPemeriksaanHemodialisa',
								id : 'Shift_1_LapJumlahJenisPemeriksaanHemodialisa'
							},
							{
								boxLabel: 'Shift 2',
								name: 'Shift_2_LapJumlahJenisPemeriksaanHemodialisa',
								id : 'Shift_2_LapJumlahJenisPemeriksaanHemodialisa'
							},
							{
								boxLabel: 'Shift 3',
								name: 'Shift_3_LapJumlahJenisPemeriksaanHemodialisa',
								id : 'Shift_3_LapJumlahJenisPemeriksaanHemodialisa'
							}
						]
					}
				]
            }
        ]
            
    };
    return items;
};


