var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapJasaPelayananDokDet;
var selectNamaLapJasaPelayananDokDet;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapJasaPelayananDokDet;
var varLapLapJasaPelayananDokDet= ShowFormLapLapJasaPelayananDokDet();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapJasaPelayananDokDet;
var customer;
var kdcustomer;
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var type_file=0;
var winLapJasaPelayananDokDetReport;

function ShowFormLapLapJasaPelayananDokDet()
{
    frmDlgLapJasaPelayananDokDet= fnDlgLapJasaPelayananDokDet();
    frmDlgLapJasaPelayananDokDet.show();
	
};

function fnDlgLapJasaPelayananDokDet()
{
    winLapJasaPelayananDokDetReport = new Ext.Window
    (
        {
            id: 'winLapJasaPelayananDokDetReport',
            title: 'Laporan Transaksi Jasa Pelayanan Dokter HD (Det)',
            closeAction: 'destroy',
            width: 370,
            height: 410,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapJasaPelayananDokDet()],
            listeners:
			{
				activate: function()
				{
					Ext.getCmp('cboPerseoranganLapJasaPelayananDokDet').hide();
					Ext.getCmp('cboAsuransiLapJasaPelayananDokDet').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokDet').hide();
					Ext.getCmp('cboUmumLapJasaPelayananDokDet').show();
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'OK',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapJasaPelayananDokDet',
						handler: function()
						{
							if (ValidasiReportLapJasaPelayananDokDet() === 1)
							{
								if (Ext.get('cboPilihanLapJasaPelayananDokDetkelompokPasien').getValue() === 'Semua')
								{
									tipe='Semua';
									customer='Semua';
									kdcustomer='Semua';
								} else if (Ext.get('cboPilihanLapJasaPelayananDokDetkelompokPasien').getValue() === 'Perseorangan'){
									customer=Ext.get('cboPerseoranganLapJasaPelayananDokDet').getValue();
									tipe='Perseorangan';
									kdcustomer=Ext.getCmp('cboPerseoranganLapJasaPelayananDokDet').getValue();
								} else if (Ext.get('cboPilihanLapJasaPelayananDokDetkelompokPasien').getValue() === 'Perusahaan'){
									customer=Ext.get('cboPerusahaanRequestEntryLapJasaPelayananDokDet').getValue();
									tipe='Perusahaan';
									kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokDet').getValue();
								} else {
									customer=Ext.get('cboAsuransiLapJasaPelayananDokDet').getValue();
									tipe='Asuransi';
									kdcustomer=Ext.getCmp('cboAsuransiLapJasaPelayananDokDet').getValue();
								} 
								
								if(Ext.getCmp('radioasal').getValue() === true){
									periode='tanggal';
									tglAwal=Ext.getCmp('dtpTglAwalFilterHasilHD').getValue();
									tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilHD').getValue();
								} else{
									periode='bulan';
									tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilHD').getValue();
									tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilHD').getValue();
								}
								
								if (Ext.getCmp('Shift_All_LapJasaPelayananDokDet').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1_LapJasaPelayananDokDet').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2_LapJasaPelayananDokDet').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3_LapJasaPelayananDokDet').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
								
								var params={
									asal_pasien:Ext.get('cboPilihanLapJasaPelayananDokDet').getValue(),
									tipe:tipe,
									type_file:type_file,
									customer:customer,
									kdcustomer:kdcustomer,
									periode:periode,
									tglAwal:tglAwal,
									tglAkhir:tglAkhir,
									shift:shift,
									shift1:shift1,
									shift2:shift2,
									shift3:shift3,
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/hemodialisa/lap_hemodialisa/lap_jasa_pelayanan_dokter_detail");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapJasaPelayananDokDetReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapJasaPelayananDokDet',
						handler: function()
						{
							winLapJasaPelayananDokDetReport.close();
						}
					}
			]

        }
    );

    return winLapJasaPelayananDokDetReport;
};


function ItemDlgLapJasaPelayananDokDet()
{
    var PnlLapLapJasaPelayananDokDet = new Ext.Panel
    (
        {
            id: 'PnlLapLapJasaPelayananDokDet',
            fileUpload: true,
            layout: 'form',
            height: '400',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapJasaPelayananDokDet_Atas(),
                getItemLapLapJasaPelayananDokDet_Batas(),
                getItemLapLapJasaPelayananDokDet_Bawah(),
                getItemLapLapJasaPelayananDokDet_Batas(),
                getItemLapLapJasaPelayananDokDet_Samping(),
              
            ]
        }
    );

    return PnlLapLapJasaPelayananDokDet;
};


function getKodeReportLapJasaPelayananDokDet()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLapJasaPelayananDokDet').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLapJasaPelayananDokDet').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLapJasaPelayananDokDet()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapJasaPelayananDokDet').getValue() === ''){
		ShowPesanWarningLapJasaPelayananDokDetReport('Pasien Belum Dipilih','Warning');
        x=0;
	}
	
	if(Ext.getCmp('cboPilihanLapJasaPelayananDokDetkelompokPasien').getValue() === ''){
		ShowPesanWarningLapJasaPelayananDokDetReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapJasaPelayananDokDet').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapJasaPelayananDokDet').getValue() === '' &&  Ext.get('cboAsuransiLapJasaPelayananDokDet').getValue() === '' && Ext.get('cboUmumRegisHD').getValue() === '' ){
		ShowPesanWarningLapJasaPelayananDokDetReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
   /*  if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapJasaPelayananDokDetReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    } */
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapJasaPelayananDokDetReport('Periode belum dipilih','Warning');
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapJasaPelayananDokDet').getValue() === false && Ext.getCmp('Shift_1_LapJasaPelayananDokDet').getValue() === false && Ext.getCmp('Shift_2_LapJasaPelayananDokDet').getValue() === false && Ext.getCmp('Shift_3_LapJasaPelayananDokDet').getValue() === false){
		ShowPesanWarningLapJasaPelayananDokDetReport('Shift Belum Dipilih','Warning');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapJasaPelayananDokDetReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapJasaPelayananDokDet_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 130,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapJasaPelayananDokDet(),
            
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapJasaPelayananDokDetKelompokPasien(),
                mComboPerseoranganLapJasaPelayananDokDet(),
                mComboAsuransiLapJasaPelayananDokDet(),
                mComboPerusahaanLapJasaPelayananDokDet(),
                mComboUmumLapJasaPelayananDokDet(),
			{
                x: 10,
                y: 100,
                xtype: 'label',
                text: 'Tipe File '
            }, {
                x: 110,
                y: 100,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 100,
				xtype: 'checkbox',
				id: 'CekLapPilihType',
				hideLabel:false,
				boxLabel: 'Excel',
				checked: false,
				listeners: 
				{
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihType').getValue()===true)
						{
							type_file =1;
						}
						else
						{
							type_file =0;
							
						}
					}
				}
			}
            ]
        }]
    };
    return items;
};


function getItemLapLapJasaPelayananDokDet_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapJasaPelayananDokDet_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  345,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
				
					{
						x: 40,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapJasaPelayananDokDet',
						id : 'Shift_All_LapJasaPelayananDokDet',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapJasaPelayananDokDet').setValue(true);
								Ext.getCmp('Shift_2_LapJasaPelayananDokDet').setValue(true);
								Ext.getCmp('Shift_3_LapJasaPelayananDokDet').setValue(true);
								Ext.getCmp('Shift_1_LapJasaPelayananDokDet').disable();
								Ext.getCmp('Shift_2_LapJasaPelayananDokDet').disable();
								Ext.getCmp('Shift_3_LapJasaPelayananDokDet').disable();
							}else{
								Ext.getCmp('Shift_1_LapJasaPelayananDokDet').setValue(false);
								Ext.getCmp('Shift_2_LapJasaPelayananDokDet').setValue(false);
								Ext.getCmp('Shift_3_LapJasaPelayananDokDet').setValue(false);
								Ext.getCmp('Shift_1_LapJasaPelayananDokDet').enable();
								Ext.getCmp('Shift_2_LapJasaPelayananDokDet').enable();
								Ext.getCmp('Shift_3_LapJasaPelayananDokDet').enable();
							}
						}
					},
					{
						x: 110,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapJasaPelayananDokDet',
						id : 'Shift_1_LapJasaPelayananDokDet'
					},
					{
						x: 180,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapJasaPelayananDokDet',
						id : 'Shift_2_LapJasaPelayananDokDet'
					},
					{
						x: 250,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapJasaPelayananDokDet',
						id : 'Shift_3_LapJasaPelayananDokDet'
					}
				]
			}
        ]
            
    };
    return items;
};

function getItemLapLapJasaPelayananDokDet_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilHD',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilHD',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilHD',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilHD',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;



function mComboPilihanLapJasaPelayananDokDet()
{
    var cboPilihanLapJasaPelayananDokDet = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapJasaPelayananDokDet',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ'],[3, 'RWI'], [4, 'IGD']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapJasaPelayananDokDet;
};

function mComboPilihanLapJasaPelayananDokDetKelompokPasien()
{
    var cboPilihanLapJasaPelayananDokDetkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 40,
                id:'cboPilihanLapJasaPelayananDokDetkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapJasaPelayananDokDetkelompokPasien;
};

function mComboPerseoranganLapJasaPelayananDokDet()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapJasaPelayananDokDet = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapJasaPelayananDokDet.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapJasaPelayananDokDet = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPerseoranganLapJasaPelayananDokDet',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapJasaPelayananDokDet,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLapJasaPelayananDokDet;
};

function mComboUmumLapJasaPelayananDokDet()
{
    var cboUmumLapJasaPelayananDokDet = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboUmumLapJasaPelayananDokDet',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumLapJasaPelayananDokDet;
};

function mComboPerusahaanLapJasaPelayananDokDet()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapJasaPelayananDokDet = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
		    id: 'cboPerusahaanRequestEntryLapJasaPelayananDokDet',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapJasaPelayananDokDet;
};

function mComboAsuransiLapJasaPelayananDokDet()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapJasaPelayananDokDet = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboAsuransiLapJasaPelayananDokDet',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapJasaPelayananDokDet;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapJasaPelayananDokDet').show();
        Ext.getCmp('cboAsuransiLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboUmumLapJasaPelayananDokDet').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboAsuransiLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokDet').show();
        Ext.getCmp('cboUmumLapJasaPelayananDokDet').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboAsuransiLapJasaPelayananDokDet').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboUmumLapJasaPelayananDokDet').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboAsuransiLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboUmumLapJasaPelayananDokDet').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboAsuransiLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokDet').hide();
        Ext.getCmp('cboUmumLapJasaPelayananDokDet').show();
   }
}


