var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapJasaPelayananDokSum;
var selectNamaLapJasaPelayananDokSum;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapJasaPelayananDokSum;
var varLapLapJasaPelayananDokSum= ShowFormLapLapJasaPelayananDokSum();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapJasaPelayananDokSum;
var customer;
var kdcustomer;
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var winLapJasaPelayananDokSumReport;
var type_file=0;
function ShowFormLapLapJasaPelayananDokSum()
{
    frmDlgLapJasaPelayananDokSum= fnDlgLapJasaPelayananDokSum();
    frmDlgLapJasaPelayananDokSum.show();
};

function fnDlgLapJasaPelayananDokSum()
{
    winLapJasaPelayananDokSumReport = new Ext.Window
    (
        {
            id: 'winLapJasaPelayananDokSumReport',
            title: 'Laporan Jasa Pelayanan Dokter Hemodialisa (Summary)',
            closeAction: 'destroy',
            width: 370,
            height: 410,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapJasaPelayananDokSum()],
            listeners:
			{
				activate: function()
				{
					Ext.getCmp('cboPerseoranganLapJasaPelayananDokSum').hide();
					Ext.getCmp('cboAsuransiLapJasaPelayananDokSum').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokSum').hide();
					Ext.getCmp('cboUmumLapJasaPelayananDokSum').show();
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'OK',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapJasaPelayananDokSum',
						handler: function()
						{
							if (ValidasiReportLapJasaPelayananDokSum() === 1)
							{
								if (Ext.get('cboPilihanLapJasaPelayananDokSumkelompokPasien').getValue() === 'Semua')
								{
									tipe='Semua';
									customer='Semua';
									kdcustomer='Semua';
								} else if (Ext.get('cboPilihanLapJasaPelayananDokSumkelompokPasien').getValue() === 'Perseorangan'){
									customer=Ext.get('cboPerseoranganLapJasaPelayananDokSum').getValue();
									tipe='Perseorangan';
									kdcustomer=Ext.getCmp('cboPerseoranganLapJasaPelayananDokSum').getValue();
								} else if (Ext.get('cboPilihanLapJasaPelayananDokSumkelompokPasien').getValue() === 'Perusahaan'){
									customer=Ext.get('cboPerusahaanRequestEntryLapJasaPelayananDokSum').getValue();
									tipe='Perusahaan';
									kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokSum').getValue();
								} else {
									customer=Ext.get('cboAsuransiLapJasaPelayananDokSum').getValue();
									tipe='Asuransi';
									kdcustomer=Ext.getCmp('cboAsuransiLapJasaPelayananDokSum').getValue();
								} 
								
								if(Ext.getCmp('radioasal').getValue() === true){
									periode='tanggal';
									tglAwal=Ext.getCmp('dtpTglAwalFilterHasilHD').getValue();
									tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilHD').getValue();
								} else{
									periode='bulan';
									tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilHD').getValue();
									tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilHD').getValue();
								}
								
								if (Ext.getCmp('Shift_All_LapJasaPelayananDokSum').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1_LapJasaPelayananDokSum').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2_LapJasaPelayananDokSum').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3_LapJasaPelayananDokSum').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
								
								var params={
									asal_pasien:Ext.get('cboPilihanLapJasaPelayananDokSum').getValue(),
									tipe:tipe,
									customer:customer,
									kdcustomer:kdcustomer,
									periode:periode,
									tglAwal:tglAwal,
									tglAkhir:tglAkhir,
									shift:shift,
									shift1:shift1,
									shift2:shift2,
									shift3:shift3,
									type_file:type_file
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/hemodialisa/lap_hemodialisa/lap_jasa_pelayanan_dokter_summary");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapJasaPelayananDokSumReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapJasaPelayananDokSum',
						handler: function()
						{
							winLapJasaPelayananDokSumReport.close();
						}
					}
			]

        }
    );

    return winLapJasaPelayananDokSumReport;
};


function ItemDlgLapJasaPelayananDokSum()
{
    var PnlLapLapJasaPelayananDokSum = new Ext.Panel
    (
        {
            id: 'PnlLapLapJasaPelayananDokSum',
            fileUpload: true,
            layout: 'form',
            height: '400',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapJasaPelayananDokSum_Atas(),
                getItemLapLapJasaPelayananDokSum_Batas(),
                getItemLapLapJasaPelayananDokSum_Bawah(),
                getItemLapLapJasaPelayananDokSum_Batas(),
                getItemLapLapJasaPelayananDokSum_Samping(),
              
            ]
        }
    );

    return PnlLapLapJasaPelayananDokSum;
};


function getKodeReportLapJasaPelayananDokSum()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLapJasaPelayananDokSum').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLapJasaPelayananDokSum').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLapJasaPelayananDokSum()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapJasaPelayananDokSum').getValue() === ''){
		ShowPesanWarningLapJasaPelayananDokSumReport('Pasien Belum Dipilih','Warning');
        x=0;
	}
	
	if(Ext.getCmp('cboPilihanLapJasaPelayananDokSumkelompokPasien').getValue() === ''){
		ShowPesanWarningLapJasaPelayananDokSumReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapJasaPelayananDokSum').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapJasaPelayananDokSum').getValue() === '' &&  Ext.get('cboAsuransiLapJasaPelayananDokSum').getValue() === '' && Ext.get('cboUmumRegisHD').getValue() === '' ){
		ShowPesanWarningLapJasaPelayananDokSumReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
   /*  if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapJasaPelayananDokSumReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    } */
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapJasaPelayananDokSumReport('Periode belum dipilih','Warning');
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapJasaPelayananDokSum').getValue() === false && Ext.getCmp('Shift_1_LapJasaPelayananDokSum').getValue() === false && Ext.getCmp('Shift_2_LapJasaPelayananDokSum').getValue() === false && Ext.getCmp('Shift_3_LapJasaPelayananDokSum').getValue() === false){
		ShowPesanWarningLapJasaPelayananDokSumReport('Shift Belum Dipilih','Warning');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapJasaPelayananDokSumReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapJasaPelayananDokSum_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 130,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapJasaPelayananDokSum(),
            
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapJasaPelayananDokSumKelompokPasien(),
                mComboPerseoranganLapJasaPelayananDokSum(),
                mComboAsuransiLapJasaPelayananDokSum(),
                mComboPerusahaanLapJasaPelayananDokSum(),
                mComboUmumLapJasaPelayananDokSum(),
			{
                x: 10,
                y: 100,
                xtype: 'label',
                text: 'Tipe File '
            }, {
                x: 110,
                y: 100,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 100,
				xtype: 'checkbox',
				id: 'CekLapPilihType',
				hideLabel:false,
				boxLabel: 'Excel',
				checked: false,
				listeners: 
				{
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihType').getValue()===true)
						{
							type_file =1;
						}
						else
						{
							type_file =0;
							
						}
					}
				}
			}
            ]
        }]
    };
    return items;
};


function getItemLapLapJasaPelayananDokSum_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapJasaPelayananDokSum_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  345,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
				
					{
						x: 40,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapJasaPelayananDokSum',
						id : 'Shift_All_LapJasaPelayananDokSum',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapJasaPelayananDokSum').setValue(true);
								Ext.getCmp('Shift_2_LapJasaPelayananDokSum').setValue(true);
								Ext.getCmp('Shift_3_LapJasaPelayananDokSum').setValue(true);
								Ext.getCmp('Shift_1_LapJasaPelayananDokSum').disable();
								Ext.getCmp('Shift_2_LapJasaPelayananDokSum').disable();
								Ext.getCmp('Shift_3_LapJasaPelayananDokSum').disable();
							}else{
								Ext.getCmp('Shift_1_LapJasaPelayananDokSum').setValue(false);
								Ext.getCmp('Shift_2_LapJasaPelayananDokSum').setValue(false);
								Ext.getCmp('Shift_3_LapJasaPelayananDokSum').setValue(false);
								Ext.getCmp('Shift_1_LapJasaPelayananDokSum').enable();
								Ext.getCmp('Shift_2_LapJasaPelayananDokSum').enable();
								Ext.getCmp('Shift_3_LapJasaPelayananDokSum').enable();
							}
						}
					},
					{
						x: 110,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapJasaPelayananDokSum',
						id : 'Shift_1_LapJasaPelayananDokSum'
					},
					{
						x: 180,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapJasaPelayananDokSum',
						id : 'Shift_2_LapJasaPelayananDokSum'
					},
					{
						x: 250,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapJasaPelayananDokSum',
						id : 'Shift_3_LapJasaPelayananDokSum'
					}
				]
			}
        ]
            
    };
    return items;
};

function getItemLapLapJasaPelayananDokSum_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilHD',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilHD',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilHD',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilHD',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;



function mComboPilihanLapJasaPelayananDokSum()
{
    var cboPilihanLapJasaPelayananDokSum = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapJasaPelayananDokSum',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ'],[3, 'RWI'], [4, 'IGD']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapJasaPelayananDokSum;
};

function mComboPilihanLapJasaPelayananDokSumKelompokPasien()
{
    var cboPilihanLapJasaPelayananDokSumkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 40,
                id:'cboPilihanLapJasaPelayananDokSumkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapJasaPelayananDokSumkelompokPasien;
};

function mComboPerseoranganLapJasaPelayananDokSum()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapJasaPelayananDokSum = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapJasaPelayananDokSum.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapJasaPelayananDokSum = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPerseoranganLapJasaPelayananDokSum',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapJasaPelayananDokSum,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLapJasaPelayananDokSum;
};

function mComboUmumLapJasaPelayananDokSum()
{
    var cboUmumLapJasaPelayananDokSum = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboUmumLapJasaPelayananDokSum',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumLapJasaPelayananDokSum;
};

function mComboPerusahaanLapJasaPelayananDokSum()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapJasaPelayananDokSum = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
		    id: 'cboPerusahaanRequestEntryLapJasaPelayananDokSum',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapJasaPelayananDokSum;
};

function mComboAsuransiLapJasaPelayananDokSum()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapJasaPelayananDokSum = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboAsuransiLapJasaPelayananDokSum',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapJasaPelayananDokSum;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapJasaPelayananDokSum').show();
        Ext.getCmp('cboAsuransiLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboUmumLapJasaPelayananDokSum').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboAsuransiLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokSum').show();
        Ext.getCmp('cboUmumLapJasaPelayananDokSum').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboAsuransiLapJasaPelayananDokSum').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboUmumLapJasaPelayananDokSum').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboAsuransiLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboUmumLapJasaPelayananDokSum').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboAsuransiLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapJasaPelayananDokSum').hide();
        Ext.getCmp('cboUmumLapJasaPelayananDokSum').show();
   }
}


