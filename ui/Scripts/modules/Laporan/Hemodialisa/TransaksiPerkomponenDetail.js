var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapTransaksiPerkomponenDetailHD;
var selectNamaLapTransaksiPerkomponenDetailHD;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapTransaksiPerkomponenDetailHD;
var varLapLapTransaksiPerkomponenDetailHD= ShowFormLapLapTransaksiPerkomponenDetailHD();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapTransaksiPerkomponenDetailHD;
var customer;
var kdcustomer;
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var kdkelpas;
var winLapTransaksiPerkomponenDetailHDReport;
var type_file=0;
function ShowFormLapLapTransaksiPerkomponenDetailHD()
{
    frmDlgLapTransaksiPerkomponenDetailHD= fnDlgLapTransaksiPerkomponenDetailHD();
    frmDlgLapTransaksiPerkomponenDetailHD.show();
	loadDataComboUserLapTransaksiPerkomponenDetailHD();
};

function fnDlgLapTransaksiPerkomponenDetailHD()
{
    winLapTransaksiPerkomponenDetailHDReport = new Ext.Window
    (
        {
            id: 'winLapTransaksiPerkomponenDetailHDReport',
            title: 'Laporan Transaksi Perkomponen Detail',
            closeAction: 'destroy',
            width: 370,
            height: 440,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapTransaksiPerkomponenDetailHD()],
            listeners:
			{
				activate: function()
				{
					Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailHD').hide();
					Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailHD').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD').hide();
					Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailHD').show();
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'OK',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapTransaksiPerkomponenDetailHD',
						handler: function()
						{
							if (ValidasiReportLapTransaksiPerkomponenDetailHD() === 1)
							{
								if (Ext.get('cboPilihanLapTransaksiPerkomponenDetailHDkelompokPasien').getValue() === 'Semua')
								{
									tipe='Semua';
									customer='Semua';
									kdcustomer='Semua';
								} else if (Ext.get('cboPilihanLapTransaksiPerkomponenDetailHDkelompokPasien').getValue() === 'Perseorangan'){
									customer=Ext.get('cboPerseoranganLapTransaksiPerkomponenDetailHD').getValue();
									tipe='Perseorangan';
									kdkelpas=Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailHDkelompokPasien').getValue();
									kdcustomer=Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailHD').getValue();
								} else if (Ext.get('cboPilihanLapTransaksiPerkomponenDetailHDkelompokPasien').getValue() === 'Perusahaan'){
									customer=Ext.get('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD').getValue();
									tipe='Perusahaan';
									kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD').getValue();
									kdkelpas=Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailHDkelompokPasien').getValue();
								} else {
									customer=Ext.get('cboAsuransiLapTransaksiPerkomponenDetailHD').getValue();
									tipe='Asuransi';
									kdcustomer=Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailHD').getValue();
									kdkelpas=Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailHDkelompokPasien').getValue();
								} 
								
								if(Ext.getCmp('radioasal').getValue() === true){
									periode='tanggal';
									tglAwal=Ext.getCmp('dtpTglAwalFilterHasilHD').getValue();
									tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilHD').getValue();
								} else{
									periode='bulan';
									tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilHD').getValue();
									tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilHD').getValue();
								}
								
								if (Ext.getCmp('Shift_All_LapTransaksiPerkomponenDetailHD').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailHD').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailHD').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailHD').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
								
								var params={
									asal_pasien:Ext.get('cboPilihanLapTransaksiPerkomponenDetailHD').getValue(),
									user:Ext.getCmp('cboUserRequestEntryLapTransaksiPerkomponenDetailHD').getValue(),
									tipe:tipe,
									kdkelpas:kdkelpas,
									customer:customer,
									kdcustomer:kdcustomer,
									periode:periode,
									tglAwal:tglAwal,
									tglAkhir:tglAkhir,
									shift:shift,
									shift1:shift1,
									shift2:shift2,
									shift3:shift3,
									type_file:type_file
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/hemodialisa/lap_hemodialisa/lap_TRPerkomponenDetail");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapTransaksiPerkomponenDetailHDReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapTransaksiPerkomponenDetailHD',
						handler: function()
						{
							winLapTransaksiPerkomponenDetailHDReport.close();
						}
					}
			]

        }
    );

    return winLapTransaksiPerkomponenDetailHDReport;
};


function ItemDlgLapTransaksiPerkomponenDetailHD()
{
    var PnlLapLapTransaksiPerkomponenDetailHD = new Ext.Panel
    (
        {
            id: 'PnlLapLapTransaksiPerkomponenDetailHD',
            fileUpload: true,
            layout: 'form',
            height: '400',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapTransaksiPerkomponenDetailHD_Atas(),
                getItemLapLapTransaksiPerkomponenDetailHD_Batas(),
                getItemLapLapTransaksiPerkomponenDetailHD_Bawah(),
                getItemLapLapTransaksiPerkomponenDetailHD_Batas(),
                getItemLapLapTransaksiPerkomponenDetailHD_Samping(),
              
            ]
        }
    );

    return PnlLapLapTransaksiPerkomponenDetailHD;
};


function getKodeReportLapTransaksiPerkomponenDetailHD()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailHD').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailHD').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLapTransaksiPerkomponenDetailHD()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailHD').getValue() === ''){
		ShowPesanWarningLapTransaksiPerkomponenDetailHDReport('Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboUserRequestEntryLapTransaksiPerkomponenDetailHD').getValue() === ''){
		ShowPesanWarningLapTransaksiPerkomponenDetailHDReport('Operator Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboPilihanLapTransaksiPerkomponenDetailHDkelompokPasien').getValue() === ''){
		ShowPesanWarningLapTransaksiPerkomponenDetailHDReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapTransaksiPerkomponenDetailHD').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD').getValue() === '' &&  Ext.get('cboAsuransiLapTransaksiPerkomponenDetailHD').getValue() === '' && Ext.get('cboUmumRegisHD').getValue() === '' ){
		ShowPesanWarningLapTransaksiPerkomponenDetailHDReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
   /*  if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapTransaksiPerkomponenDetailHDReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    } */
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapTransaksiPerkomponenDetailHDReport('Periode belum dipilih','Warning');
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapTransaksiPerkomponenDetailHD').getValue() === false && Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailHD').getValue() === false && Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailHD').getValue() === false && Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailHD').getValue() === false){
		ShowPesanWarningLapTransaksiPerkomponenDetailHDReport('Shift Belum Dipilih','Warning');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapTransaksiPerkomponenDetailHDReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapTransaksiPerkomponenDetailHD_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 160,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTransaksiPerkomponenDetailHD(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboUserLapTransaksiPerkomponenDetailHD(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTransaksiPerkomponenDetailHDKelompokPasien(),
                mComboPerseoranganLapTransaksiPerkomponenDetailHD(),
                mComboAsuransiLapTransaksiPerkomponenDetailHD(),
                mComboPerusahaanLapTransaksiPerkomponenDetailHD(),
                mComboUmumLapTransaksiPerkomponenDetailHD(),
			{
                x: 10,
                y: 130,
                xtype: 'label',
                text: 'Tipe File '
            }, {
                x: 110,
                y: 130,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 130,
				xtype: 'checkbox',
				id: 'CekLapPilihType',
				hideLabel:false,
				boxLabel: 'Excel',
				checked: false,
				listeners: 
				{
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihType').getValue()===true)
						{
							type_file =1;
						}
						else
						{
							type_file =0;
							
						}
					}
				}
			}
            ]
        }]
    };
    return items;
};


function getItemLapLapTransaksiPerkomponenDetailHD_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapTransaksiPerkomponenDetailHD_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  345,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
				
					{
						x: 40,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapTransaksiPerkomponenDetailHD',
						id : 'Shift_All_LapTransaksiPerkomponenDetailHD',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailHD').setValue(true);
								Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailHD').setValue(true);
								Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailHD').setValue(true);
								Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailHD').disable();
								Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailHD').disable();
								Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailHD').disable();
							}else{
								Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailHD').setValue(false);
								Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailHD').setValue(false);
								Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailHD').setValue(false);
								Ext.getCmp('Shift_1_LapTransaksiPerkomponenDetailHD').enable();
								Ext.getCmp('Shift_2_LapTransaksiPerkomponenDetailHD').enable();
								Ext.getCmp('Shift_3_LapTransaksiPerkomponenDetailHD').enable();
							}
						}
					},
					{
						x: 110,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapTransaksiPerkomponenDetailHD',
						id : 'Shift_1_LapTransaksiPerkomponenDetailHD'
					},
					{
						x: 180,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapTransaksiPerkomponenDetailHD',
						id : 'Shift_2_LapTransaksiPerkomponenDetailHD'
					},
					{
						x: 250,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapTransaksiPerkomponenDetailHD',
						id : 'Shift_3_LapTransaksiPerkomponenDetailHD'
					}
				]
			}
        ]
            
    };
    return items;
};

function getItemLapLapTransaksiPerkomponenDetailHD_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilHD',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilHD',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilHD',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilHD',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function loadDataComboUserLapTransaksiPerkomponenDetailHD(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/lap_trans_komponen_det/getUser",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUserRequestEntryLapTransaksiPerkomponenDetailHD.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_User_LapTransaksiPerkomponenDetailHD.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_User_LapTransaksiPerkomponenDetailHD.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboPilihanLapTransaksiPerkomponenDetailHD()
{
    var cboPilihanLapTransaksiPerkomponenDetailHD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapTransaksiPerkomponenDetailHD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ'],[3, 'RWI'], [4, 'IGD']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapTransaksiPerkomponenDetailHD;
};

function mComboPilihanLapTransaksiPerkomponenDetailHDKelompokPasien()
{
    var cboPilihanLapTransaksiPerkomponenDetailHDkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanLapTransaksiPerkomponenDetailHDkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapTransaksiPerkomponenDetailHDkelompokPasien;
};

function mComboPerseoranganLapTransaksiPerkomponenDetailHD()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapTransaksiPerkomponenDetailHD = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapTransaksiPerkomponenDetailHD.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapTransaksiPerkomponenDetailHD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganLapTransaksiPerkomponenDetailHD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapTransaksiPerkomponenDetailHD,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLapTransaksiPerkomponenDetailHD;
};

function mComboUmumLapTransaksiPerkomponenDetailHD()
{
    var cboUmumLapTransaksiPerkomponenDetailHD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumLapTransaksiPerkomponenDetailHD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumLapTransaksiPerkomponenDetailHD;
};

function mComboPerusahaanLapTransaksiPerkomponenDetailHD()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD;
};

function mComboAsuransiLapTransaksiPerkomponenDetailHD()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapTransaksiPerkomponenDetailHD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiLapTransaksiPerkomponenDetailHD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapTransaksiPerkomponenDetailHD;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailHD').show();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailHD').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD').show();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailHD').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailHD').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailHD').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailHD').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenDetailHD').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenDetailHD').show();
   }
}

function mComboUserLapTransaksiPerkomponenDetailHD()
{
    var Field = ['kd_user','full_name'];
    ds_User_LapTransaksiPerkomponenDetailHD = new WebApp.DataStore({fields: Field});
    cboUserRequestEntryLapTransaksiPerkomponenDetailHD = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cboUserRequestEntryLapTransaksiPerkomponenDetailHD',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:'Semua',
            store: ds_User_LapTransaksiPerkomponenDetailHD,
            valueField: 'kd_user',
            displayField: 'full_name',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.kd_user;
				} 
			}
        }
    )

    return cboUserRequestEntryLapTransaksiPerkomponenDetailHD;
};
