var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapTransaksiPerkomponenSummaryHD;
var selectNamaLapTransaksiPerkomponenSummaryHD;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapTransaksiPerkomponenSummaryHD;
var varLapLapTransaksiPerkomponenSummaryHD= ShowFormLapLapTransaksiPerkomponenSummaryHD();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapTransaksiPerkomponenSummaryHD;
var customer;
var kdcustomer;
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var kdkelpas;
var winLapTransaksiPerkomponenSummaryHDReport;
var type_file=0;
function ShowFormLapLapTransaksiPerkomponenSummaryHD()
{
    frmDlgLapTransaksiPerkomponenSummaryHD= fnDlgLapTransaksiPerkomponenSummaryHD();
    frmDlgLapTransaksiPerkomponenSummaryHD.show();
	loadDataComboUserLapTransaksiPerkomponenSummaryHD();
};

function fnDlgLapTransaksiPerkomponenSummaryHD()
{
    winLapTransaksiPerkomponenSummaryHDReport = new Ext.Window
    (
        {
            id: 'winLapTransaksiPerkomponenSummaryHDReport',
            title: 'Laporan Transaksi Perkomponen Summary',
            closeAction: 'destroy',
            width: 370,
            height: 440,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapTransaksiPerkomponenSummaryHD()],
            listeners:
			{
				activate: function()
				{
					Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenSummaryHD').hide();
					Ext.getCmp('cboAsuransiLapTransaksiPerkomponenSummaryHD').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD').hide();
					Ext.getCmp('cboUmumLapTransaksiPerkomponenSummaryHD').show();
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'OK',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapTransaksiPerkomponenSummaryHD',
						handler: function()
						{
							if (ValidasiReportLapTransaksiPerkomponenSummaryHD() === 1)
							{
								if (Ext.get('cboPilihanLapTransaksiPerkomponenSummaryHDkelompokPasien').getValue() === 'Semua')
								{
									tipe='Semua';
									customer='Semua';
									kdcustomer='Semua';
								} else if (Ext.get('cboPilihanLapTransaksiPerkomponenSummaryHDkelompokPasien').getValue() === 'Perseorangan'){
									customer=Ext.get('cboPerseoranganLapTransaksiPerkomponenSummaryHD').getValue();
									tipe='Perseorangan';
									kdkelpas=Ext.getCmp('cboPilihanLapTransaksiPerkomponenSummaryHDkelompokPasien').getValue();
									kdcustomer=Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenSummaryHD').getValue();
								} else if (Ext.get('cboPilihanLapTransaksiPerkomponenSummaryHDkelompokPasien').getValue() === 'Perusahaan'){
									customer=Ext.get('cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD').getValue();
									tipe='Perusahaan';
									kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD').getValue();
									kdkelpas=Ext.getCmp('cboPilihanLapTransaksiPerkomponenSummaryHDkelompokPasien').getValue();
								} else {
									customer=Ext.get('cboAsuransiLapTransaksiPerkomponenSummaryHD').getValue();
									tipe='Asuransi';
									kdcustomer=Ext.getCmp('cboAsuransiLapTransaksiPerkomponenSummaryHD').getValue();
									kdkelpas=Ext.getCmp('cboPilihanLapTransaksiPerkomponenSummaryHDkelompokPasien').getValue();
								} 
								
								if(Ext.getCmp('radioasal').getValue() === true){
									periode='tanggal';
									tglAwal=Ext.getCmp('dtpTglAwalFilterHasilHD').getValue();
									tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilHD').getValue();
								} else{
									periode='bulan';
									tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilHD').getValue();
									tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilHD').getValue();
								}
								
								if (Ext.getCmp('Shift_All_LapTransaksiPerkomponenSummaryHD').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1_LapTransaksiPerkomponenSummaryHD').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2_LapTransaksiPerkomponenSummaryHD').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3_LapTransaksiPerkomponenSummaryHD').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
								
								var params={
									asal_pasien:Ext.get('cboPilihanLapTransaksiPerkomponenSummaryHD').getValue(),
									user:Ext.getCmp('cboUserRequestEntryLapTransaksiPerkomponenSummaryHD').getValue(),
									tipe:tipe,
									kdkelpas:kdkelpas,
									customer:customer,
									kdcustomer:kdcustomer,
									periode:periode,
									tglAwal:tglAwal,
									tglAkhir:tglAkhir,
									shift:shift,
									shift1:shift1,
									shift2:shift2,
									shift3:shift3,
									type_file:type_file
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/hemodialisa/lap_hemodialisa/lap_TRPerkomponenSummary");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapTransaksiPerkomponenSummaryHDReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapTransaksiPerkomponenSummaryHD',
						handler: function()
						{
							winLapTransaksiPerkomponenSummaryHDReport.close();
						}
					}
			]

        }
    );

    return winLapTransaksiPerkomponenSummaryHDReport;
};


function ItemDlgLapTransaksiPerkomponenSummaryHD()
{
    var PnlLapLapTransaksiPerkomponenSummaryHD = new Ext.Panel
    (
        {
            id: 'PnlLapLapTransaksiPerkomponenSummaryHD',
            fileUpload: true,
            layout: 'form',
            height: '400',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapTransaksiPerkomponenSummaryHD_Atas(),
                getItemLapLapTransaksiPerkomponenSummaryHD_Batas(),
                getItemLapLapTransaksiPerkomponenSummaryHD_Bawah(),
                getItemLapLapTransaksiPerkomponenSummaryHD_Batas(),
                getItemLapLapTransaksiPerkomponenSummaryHD_Samping(),
              
            ]
        }
    );

    return PnlLapLapTransaksiPerkomponenSummaryHD;
};


function getKodeReportLapTransaksiPerkomponenSummaryHD()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLapTransaksiPerkomponenSummaryHD').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLapTransaksiPerkomponenSummaryHD').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLapTransaksiPerkomponenSummaryHD()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapTransaksiPerkomponenSummaryHD').getValue() === ''){
		ShowPesanWarningLapTransaksiPerkomponenSummaryHDReport('Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboUserRequestEntryLapTransaksiPerkomponenSummaryHD').getValue() === ''){
		ShowPesanWarningLapTransaksiPerkomponenSummaryHDReport('Operator Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboPilihanLapTransaksiPerkomponenSummaryHDkelompokPasien').getValue() === ''){
		ShowPesanWarningLapTransaksiPerkomponenSummaryHDReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapTransaksiPerkomponenSummaryHD').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD').getValue() === '' &&  Ext.get('cboAsuransiLapTransaksiPerkomponenSummaryHD').getValue() === '' && Ext.get('cboUmumRegisHD').getValue() === '' ){
		ShowPesanWarningLapTransaksiPerkomponenSummaryHDReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
   /*  if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapTransaksiPerkomponenSummaryHDReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    } */
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapTransaksiPerkomponenSummaryHDReport('Periode belum dipilih','Warning');
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapTransaksiPerkomponenSummaryHD').getValue() === false && Ext.getCmp('Shift_1_LapTransaksiPerkomponenSummaryHD').getValue() === false && Ext.getCmp('Shift_2_LapTransaksiPerkomponenSummaryHD').getValue() === false && Ext.getCmp('Shift_3_LapTransaksiPerkomponenSummaryHD').getValue() === false){
		ShowPesanWarningLapTransaksiPerkomponenSummaryHDReport('Shift Belum Dipilih','Warning');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapTransaksiPerkomponenSummaryHDReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapTransaksiPerkomponenSummaryHD_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 160,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTransaksiPerkomponenSummaryHD(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboUserLapTransaksiPerkomponenSummaryHD(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTransaksiPerkomponenSummaryHDKelompokPasien(),
                mComboPerseoranganLapTransaksiPerkomponenSummaryHD(),
                mComboAsuransiLapTransaksiPerkomponenSummaryHD(),
                mComboPerusahaanLapTransaksiPerkomponenSummaryHD(),
                mComboUmumLapTransaksiPerkomponenSummaryHD(),
			{
                x: 10,
                y: 130,
                xtype: 'label',
                text: 'Tipe File '
            }, {
                x: 110,
                y: 130,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 130,
				xtype: 'checkbox',
				id: 'CekLapPilihType',
				hideLabel:false,
				boxLabel: 'Excel',
				checked: false,
				listeners: 
				{
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihType').getValue()===true)
						{
							type_file =1;
						}
						else
						{
							type_file =0;
							
						}
					}
				}
			}
            ]
        }]
    };
    return items;
};


function getItemLapLapTransaksiPerkomponenSummaryHD_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapTransaksiPerkomponenSummaryHD_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  345,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
				
					{
						x: 40,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapTransaksiPerkomponenSummaryHD',
						id : 'Shift_All_LapTransaksiPerkomponenSummaryHD',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapTransaksiPerkomponenSummaryHD').setValue(true);
								Ext.getCmp('Shift_2_LapTransaksiPerkomponenSummaryHD').setValue(true);
								Ext.getCmp('Shift_3_LapTransaksiPerkomponenSummaryHD').setValue(true);
								Ext.getCmp('Shift_1_LapTransaksiPerkomponenSummaryHD').disable();
								Ext.getCmp('Shift_2_LapTransaksiPerkomponenSummaryHD').disable();
								Ext.getCmp('Shift_3_LapTransaksiPerkomponenSummaryHD').disable();
							}else{
								Ext.getCmp('Shift_1_LapTransaksiPerkomponenSummaryHD').setValue(false);
								Ext.getCmp('Shift_2_LapTransaksiPerkomponenSummaryHD').setValue(false);
								Ext.getCmp('Shift_3_LapTransaksiPerkomponenSummaryHD').setValue(false);
								Ext.getCmp('Shift_1_LapTransaksiPerkomponenSummaryHD').enable();
								Ext.getCmp('Shift_2_LapTransaksiPerkomponenSummaryHD').enable();
								Ext.getCmp('Shift_3_LapTransaksiPerkomponenSummaryHD').enable();
							}
						}
					},
					{
						x: 110,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapTransaksiPerkomponenSummaryHD',
						id : 'Shift_1_LapTransaksiPerkomponenSummaryHD'
					},
					{
						x: 180,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapTransaksiPerkomponenSummaryHD',
						id : 'Shift_2_LapTransaksiPerkomponenSummaryHD'
					},
					{
						x: 250,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapTransaksiPerkomponenSummaryHD',
						id : 'Shift_3_LapTransaksiPerkomponenSummaryHD'
					}
				]
			}
        ]
            
    };
    return items;
};

function getItemLapLapTransaksiPerkomponenSummaryHD_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilHD',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilHD',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilHD',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilHD',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function loadDataComboUserLapTransaksiPerkomponenSummaryHD(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/lap_trans_komponen_det/getUser",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUserRequestEntryLapTransaksiPerkomponenSummaryHD.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_User_LapTransaksiPerkomponenSummaryHD.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_User_LapTransaksiPerkomponenSummaryHD.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboPilihanLapTransaksiPerkomponenSummaryHD()
{
    var cboPilihanLapTransaksiPerkomponenSummaryHD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapTransaksiPerkomponenSummaryHD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ'],[3, 'RWI'], [4, 'IGD']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapTransaksiPerkomponenSummaryHD;
};

function mComboPilihanLapTransaksiPerkomponenSummaryHDKelompokPasien()
{
    var cboPilihanLapTransaksiPerkomponenSummaryHDkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanLapTransaksiPerkomponenSummaryHDkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapTransaksiPerkomponenSummaryHDkelompokPasien;
};

function mComboPerseoranganLapTransaksiPerkomponenSummaryHD()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapTransaksiPerkomponenSummaryHD = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapTransaksiPerkomponenSummaryHD.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapTransaksiPerkomponenSummaryHD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganLapTransaksiPerkomponenSummaryHD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapTransaksiPerkomponenSummaryHD,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLapTransaksiPerkomponenSummaryHD;
};

function mComboUmumLapTransaksiPerkomponenSummaryHD()
{
    var cboUmumLapTransaksiPerkomponenSummaryHD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumLapTransaksiPerkomponenSummaryHD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumLapTransaksiPerkomponenSummaryHD;
};

function mComboPerusahaanLapTransaksiPerkomponenSummaryHD()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD;
};

function mComboAsuransiLapTransaksiPerkomponenSummaryHD()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapTransaksiPerkomponenSummaryHD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiLapTransaksiPerkomponenSummaryHD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapTransaksiPerkomponenSummaryHD;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenSummaryHD').show();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenSummaryHD').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD').show();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenSummaryHD').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenSummaryHD').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenSummaryHD').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenSummaryHD').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboAsuransiLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiPerkomponenSummaryHD').hide();
        Ext.getCmp('cboUmumLapTransaksiPerkomponenSummaryHD').show();
   }
}

function mComboUserLapTransaksiPerkomponenSummaryHD()
{
    var Field = ['kd_user','full_name'];
    ds_User_LapTransaksiPerkomponenSummaryHD = new WebApp.DataStore({fields: Field});
    cboUserRequestEntryLapTransaksiPerkomponenSummaryHD = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cboUserRequestEntryLapTransaksiPerkomponenSummaryHD',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:'Semua',
            store: ds_User_LapTransaksiPerkomponenSummaryHD,
            valueField: 'kd_user',
            displayField: 'full_name',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.kd_user;
				} 
			}
        }
    )

    return cboUserRequestEntryLapTransaksiPerkomponenSummaryHD;
};
