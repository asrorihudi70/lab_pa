var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapRegistrasiHD;
var selectNamaLapRegistrasiHD;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRegistrasiHD;
var varLapLapRegistrasiHD= ShowFormLapLapRegistrasiHD();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapRegistrasiHD;
var customer;
var kdcustomer;
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var kdkelpas;
var winLapRegistrasiHDReport;
var type_file=0;
function ShowFormLapLapRegistrasiHD()
{
    frmDlgLapRegistrasiHD= fnDlgLapRegistrasiHD();
    frmDlgLapRegistrasiHD.show();
	loadDataComboUserLapRegistrasiHD();
};

function fnDlgLapRegistrasiHD()
{
    winLapRegistrasiHDReport = new Ext.Window
    (
        {
            id: 'winLapRegistrasiHDReport',
            title: 'Laporan Registrasi Hemodialisa',
            closeAction: 'destroy',
            width: 370,
            height: 400,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRegistrasiHD()],
            listeners:
			{
				activate: function()
				{
					Ext.getCmp('cboPerseoranganLapRegistrasiHD').hide();
					Ext.getCmp('cboAsuransiLapRegistrasiHD').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLapRegistrasiHD').hide();
					Ext.getCmp('cboUmumLapRegistrasiHD').show();
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'OK',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapRegistrasiHD',
						handler: function()
						{
							if (ValidasiReportLapRegistrasiHD() === 1)
							{
								if (Ext.get('cboPilihanLapRegistrasiHDkelompokPasien').getValue() === 'Semua')
								{
									tipe='Semua';
									customer='Semua';
									kdcustomer='Semua';
								} else if (Ext.get('cboPilihanLapRegistrasiHDkelompokPasien').getValue() === 'Perseorangan'){
									customer=Ext.get('cboPerseoranganLapRegistrasiHD').getValue();
									tipe='Perseorangan';
									kdkelpas=Ext.getCmp('cboPilihanLapRegistrasiHDkelompokPasien').getValue();
									kdcustomer=Ext.getCmp('cboPerseoranganLapRegistrasiHD').getValue();
								} else if (Ext.get('cboPilihanLapRegistrasiHDkelompokPasien').getValue() === 'Perusahaan'){
									customer=Ext.get('cboPerusahaanRequestEntryLapRegistrasiHD').getValue();
									tipe='Perusahaan';
									kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapRegistrasiHD').getValue();
									kdkelpas=Ext.getCmp('cboPilihanLapRegistrasiHDkelompokPasien').getValue();
								} else {
									customer=Ext.get('cboAsuransiLapRegistrasiHD').getValue();
									tipe='Asuransi';
									kdcustomer=Ext.getCmp('cboAsuransiLapRegistrasiHD').getValue();
									kdkelpas=Ext.getCmp('cboPilihanLapRegistrasiHDkelompokPasien').getValue();
								} 
								
								if(Ext.getCmp('radioasal').getValue() === true){
									periode='tanggal';
									tglAwal=Ext.getCmp('dtpTglAwalFilterHasilHD').getValue();
									tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilHD').getValue();
								} else{
									periode='bulan';
									tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilHD').getValue();
									tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilHD').getValue();
								}
								
								if (Ext.getCmp('Shift_All_LapRegistrasiHD').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1_LapRegistrasiHD').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2_LapRegistrasiHD').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3_LapRegistrasiHD').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
								
								var params={
									asal_pasien:Ext.get('cboPilihanLapRegistrasiHD').getValue(),
									tipe:tipe,
									kdkelpas:kdkelpas,
									customer:customer,
									kdcustomer:kdcustomer,
									periode:periode,
									tglAwal:tglAwal,
									tglAkhir:tglAkhir,
									shift:shift,
									shift1:shift1,
									shift2:shift2,
									shift3:shift3,
									type_file:type_file
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/hemodialisa/lap_hemodialisa/lap_registrasi");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapRegistrasiHDReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapRegistrasiHD',
						handler: function()
						{
							winLapRegistrasiHDReport.close();
						}
					}
			]

        }
    );

    return winLapRegistrasiHDReport;
};


function ItemDlgLapRegistrasiHD()
{
    var PnlLapLapRegistrasiHD = new Ext.Panel
    (
        {
            id: 'PnlLapLapRegistrasiHD',
            fileUpload: true,
            layout: 'form',
            height: '400',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapRegistrasiHD_Atas(),
                getItemLapLapRegistrasiHD_Batas(),
                getItemLapLapRegistrasiHD_Bawah(),
                getItemLapLapRegistrasiHD_Batas(),
                getItemLapLapRegistrasiHD_Samping(),
              
            ]
        }
    );

    return PnlLapLapRegistrasiHD;
};


function getKodeReportLapRegistrasiHD()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLapRegistrasiHD').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLapRegistrasiHD').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLapRegistrasiHD()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapRegistrasiHD').getValue() === ''){
		ShowPesanWarningLapRegistrasiHDReport('Pasien Belum Dipilih','Warning');
        x=0;
	}
	
	if(Ext.getCmp('cboPilihanLapRegistrasiHDkelompokPasien').getValue() === ''){
		ShowPesanWarningLapRegistrasiHDReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapRegistrasiHD').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapRegistrasiHD').getValue() === '' &&  Ext.get('cboAsuransiLapRegistrasiHD').getValue() === '' && Ext.get('cboUmumRegisHD').getValue() === '' ){
		ShowPesanWarningLapRegistrasiHDReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
   /*  if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapRegistrasiHDReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    } */
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapRegistrasiHDReport('Periode belum dipilih','Warning');
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapRegistrasiHD').getValue() === false && Ext.getCmp('Shift_1_LapRegistrasiHD').getValue() === false && Ext.getCmp('Shift_2_LapRegistrasiHD').getValue() === false && Ext.getCmp('Shift_3_LapRegistrasiHD').getValue() === false){
		ShowPesanWarningLapRegistrasiHDReport('Shift Belum Dipilih','Warning');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapRegistrasiHDReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapRegistrasiHD_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 130,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapRegistrasiHD(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapRegistrasiHDKelompokPasien(),
                mComboPerseoranganLapRegistrasiHD(),
                mComboAsuransiLapRegistrasiHD(),
                mComboPerusahaanLapRegistrasiHD(),
                mComboUmumLapRegistrasiHD(),
			{
                x: 10,
                y: 100,
                xtype: 'label',
                text: 'Tipe File '
            }, {
                x: 110,
                y: 100,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 100,
				xtype: 'checkbox',
				id: 'CekLapPilihType',
				hideLabel:false,
				boxLabel: 'Excel',
				checked: false,
				listeners: 
				{
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihType').getValue()===true)
						{
							type_file =1;
						}
						else
						{
							type_file =0;
							
						}
					}
				}
			}
            ]
        }]
    };
    return items;
};


function getItemLapLapRegistrasiHD_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapRegistrasiHD_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  345,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
				
					{
						x: 40,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapRegistrasiHD',
						id : 'Shift_All_LapRegistrasiHD',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapRegistrasiHD').setValue(true);
								Ext.getCmp('Shift_2_LapRegistrasiHD').setValue(true);
								Ext.getCmp('Shift_3_LapRegistrasiHD').setValue(true);
								Ext.getCmp('Shift_1_LapRegistrasiHD').disable();
								Ext.getCmp('Shift_2_LapRegistrasiHD').disable();
								Ext.getCmp('Shift_3_LapRegistrasiHD').disable();
							}else{
								Ext.getCmp('Shift_1_LapRegistrasiHD').setValue(false);
								Ext.getCmp('Shift_2_LapRegistrasiHD').setValue(false);
								Ext.getCmp('Shift_3_LapRegistrasiHD').setValue(false);
								Ext.getCmp('Shift_1_LapRegistrasiHD').enable();
								Ext.getCmp('Shift_2_LapRegistrasiHD').enable();
								Ext.getCmp('Shift_3_LapRegistrasiHD').enable();
							}
						}
					},
					{
						x: 110,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapRegistrasiHD',
						id : 'Shift_1_LapRegistrasiHD'
					},
					{
						x: 180,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapRegistrasiHD',
						id : 'Shift_2_LapRegistrasiHD'
					},
					{
						x: 250,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapRegistrasiHD',
						id : 'Shift_3_LapRegistrasiHD'
					}
				]
			}
        ]
            
    };
    return items;
};

function getItemLapLapRegistrasiHD_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilHD',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilHD',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilHD').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilHD').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilHD').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilHD').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilHD',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilHD',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function loadDataComboUserLapRegistrasiHD(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/lap_trans_komponen_det/getUser",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUserRequestEntryLapRegistrasiHD.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_User_LapRegistrasiHD.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_User_LapRegistrasiHD.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboPilihanLapRegistrasiHD()
{
    var cboPilihanLapRegistrasiHD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapRegistrasiHD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ'],[3, 'RWI'], [4, 'IGD']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapRegistrasiHD;
};

function mComboPilihanLapRegistrasiHDKelompokPasien()
{
    var cboPilihanLapRegistrasiHDkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 40,
                id:'cboPilihanLapRegistrasiHDkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapRegistrasiHDkelompokPasien;
};

function mComboPerseoranganLapRegistrasiHD()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapRegistrasiHD = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapRegistrasiHD.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapRegistrasiHD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPerseoranganLapRegistrasiHD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapRegistrasiHD,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLapRegistrasiHD;
};

function mComboUmumLapRegistrasiHD()
{
    var cboUmumLapRegistrasiHD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboUmumLapRegistrasiHD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumLapRegistrasiHD;
};

function mComboPerusahaanLapRegistrasiHD()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapRegistrasiHD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
		    id: 'cboPerusahaanRequestEntryLapRegistrasiHD',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapRegistrasiHD;
};

function mComboAsuransiLapRegistrasiHD()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapRegistrasiHD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboAsuransiLapRegistrasiHD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapRegistrasiHD;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapRegistrasiHD').show();
        Ext.getCmp('cboAsuransiLapRegistrasiHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapRegistrasiHD').hide();
        Ext.getCmp('cboUmumLapRegistrasiHD').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapRegistrasiHD').hide();
        Ext.getCmp('cboAsuransiLapRegistrasiHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapRegistrasiHD').show();
        Ext.getCmp('cboUmumLapRegistrasiHD').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapRegistrasiHD').hide();
        Ext.getCmp('cboAsuransiLapRegistrasiHD').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapRegistrasiHD').hide();
        Ext.getCmp('cboUmumLapRegistrasiHD').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapRegistrasiHD').hide();
        Ext.getCmp('cboAsuransiLapRegistrasiHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapRegistrasiHD').hide();
        Ext.getCmp('cboUmumLapRegistrasiHD').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapRegistrasiHD').hide();
        Ext.getCmp('cboAsuransiLapRegistrasiHD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapRegistrasiHD').hide();
        Ext.getCmp('cboUmumLapRegistrasiHD').show();
   }
}

function mComboUserLapRegistrasiHD()
{
    var Field = ['kd_user','full_name'];
    ds_User_LapRegistrasiHD = new WebApp.DataStore({fields: Field});
    cboUserRequestEntryLapRegistrasiHD = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cboUserRequestEntryLapRegistrasiHD',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:'Semua',
            store: ds_User_LapRegistrasiHD,
            valueField: 'kd_user',
            displayField: 'full_name',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.kd_user;
				} 
			}
        }
    )

    return cboUserRequestEntryLapRegistrasiHD;
};
