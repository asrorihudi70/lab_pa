
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapTransaksiHemodialisa;
var selectNamaLapTransaksiHemodialisa;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapTransaksiHemodialisa;
var varLapLapTransaksiHemodialisa= ShowFormLapLapTransaksiHemodialisa();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapTransaksiHemodialisa;

function ShowFormLapLapTransaksiHemodialisa()
{
    frmDlgLapTransaksiHemodialisa= fnDlgLapTransaksiHemodialisa();
    frmDlgLapTransaksiHemodialisa.show();
	loadDataComboUserLapTransaksiHemodialisa();
};

function fnDlgLapTransaksiHemodialisa()
{
    var winLapTransaksiHemodialisaReport = new Ext.Window
    (
        {
            id: 'winLapTransaksiHemodialisaReport',
            title: 'Laporan Transaksi Hemodialisa',
            closeAction: 'destroy',
            width: 370,
            height: 410,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapTransaksiHemodialisa()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganLapTransaksiHemodialisa').hide();
                Ext.getCmp('cboAsuransiLapTransaksiHemodialisa').hide();
                Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiHemodialisa').hide();
                Ext.getCmp('cboUmumLapTransaksiHemodialisa').show();
            }
        }

        }
    );

    return winLapTransaksiHemodialisaReport;
};


function ItemDlgLapTransaksiHemodialisa()
{
    var PnlLapLapTransaksiHemodialisa = new Ext.Panel
    (
        {
            id: 'PnlLapLapTransaksiHemodialisa',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapTransaksiHemodialisa_Atas(),
                getItemLapLapTransaksiHemodialisa_Batas(),
                getItemLapLapTransaksiHemodialisa_Bawah(),
                getItemLapLapTransaksiHemodialisa_Batas(),
                getItemLapLapTransaksiHemodialisa_Samping(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'OK',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapLapTransaksiHemodialisa',
                            handler: function()
                            {
                                if (ValidasiReportLapTransaksiHemodialisa() === 1)
                                {
									if (Ext.get('cboPilihanLapTransaksiHemodialisakelompokPasien').getValue() === 'Semua')
									{
										tipe='Semua';
										customer='Semua';
										kdcustomer='Semua';
									} else if (Ext.get('cboPilihanLapTransaksiHemodialisakelompokPasien').getValue() === 'Perseorangan'){
										customer=Ext.get('cboPerseoranganLapTransaksiHemodialisa').getValue();
										tipe='Perseorangan';
										kdcustomer=Ext.getCmp('cboPerseoranganLapTransaksiHemodialisa').getValue();
									} else if (Ext.get('cboPilihanLapTransaksiHemodialisakelompokPasien').getValue() === 'Perusahaan'){
										customer=Ext.get('cboPerusahaanRequestEntryLapTransaksiHemodialisa').getValue();
										tipe='Perusahaan';
										kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiHemodialisa').getValue();
									} else {
										customer=Ext.get('cboAsuransiLapTransaksiHemodialisa').getValue();
										tipe='Asuransi';
										kdcustomer=Ext.getCmp('cboAsuransiLapTransaksiHemodialisa').getValue();
									} 
									
									if(Ext.getCmp('radioasal').getValue() === true){
										periode='tanggal';
										tglAwal=Ext.getCmp('dtpTglAwalFilterHasilLab').getValue();
										tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
									} else{
										periode='bulan';
										tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilLab').getValue();
										tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilLab').getValue();
									}
									
									if (Ext.getCmp('Shift_All_LapTransaksiHemodialisa').getValue() === true){
										shift='All';
										shift1='false';
										shift2='false';
										shift3='false';
									}else{
										shift='';
										if (Ext.getCmp('Shift_1_LapTransaksiHemodialisa').getValue() === true){
											shift1='true';
										} else{
											shift1='false';
										}
										if (Ext.getCmp('Shift_2_LapTransaksiHemodialisa').getValue() === true){
											shift2='true';
										}else{
											shift2='false';
										}
										if (Ext.getCmp('Shift_3_LapTransaksiHemodialisa').getValue() === true){
											shift3='true';
										}else{
											shift3='false';
										}
									}
									
									var params={
										asal_pasien:Ext.get('cboPilihanLapTransaksiHemodialisa').getValue(),
										user:Ext.getCmp('cboUserRequestEntryLapTransaksiHemodialisa').getValue(),
										tipe:tipe,
										customer:customer,
										kdcustomer:kdcustomer,
										periode:periode,
										tglAwal:tglAwal,
										tglAkhir:tglAkhir,
										shift:shift,
										shift1:shift1,
										shift2:shift2,
										shift3:shift3,
									} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/hemodialisa/laptransaksi/cetakTransaksi");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapLapTransaksiHemodialisa',
                            handler: function()
                            {
                                    frmDlgLapTransaksiHemodialisa.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapLapTransaksiHemodialisa;
};

function ValidasiReportLapTransaksiHemodialisa()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapTransaksiHemodialisa').getValue() === ''){
		ShowPesanWarningLapTransaksiHemodialisaReport('Pasien Belum Dipilih','Laporan Transaksi Laboratorium');
        x=0;
	}
	if(Ext.getCmp('cboUserRequestEntryLapTransaksiHemodialisa').getValue() === ''){
		ShowPesanWarningLapTransaksiHemodialisaReport('Operator Belum Dipilih','Laporan Transaksi Laboratorium');
        x=0;
	}
	if(Ext.getCmp('cboPilihanLapTransaksiHemodialisakelompokPasien').getValue() === ''){
		ShowPesanWarningLapTransaksiHemodialisaReport('Kelompok Pasien Belum Dipilih','Laporan Transaksi Laboratorium');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapTransaksiHemodialisa').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapTransaksiHemodialisa').getValue() === '' &&  Ext.get('cboAsuransiLapTransaksiHemodialisa').getValue() === '' && Ext.get('cboUmumRegisLab').getValue() === '' ){
		ShowPesanWarningLapTransaksiHemodialisaReport('Sub Kelompok Pasien Belum Dipilih','Laporan Transaksi Laboratorium');
        x=0;
	}
    if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapTransaksiHemodialisaReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Laporan Transaksi Laboratorium');
        x=0;
    }
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapTransaksiHemodialisaReport('Periode belum dipilih','Laporan Transaksi Laboratorium');
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapTransaksiHemodialisa').getValue() === false && Ext.getCmp('Shift_1_LapTransaksiHemodialisa').getValue() === false && Ext.getCmp('Shift_2_LapTransaksiHemodialisa').getValue() === false && Ext.getCmp('Shift_3_LapTransaksiHemodialisa').getValue() === false){
		ShowPesanWarningLapTransaksiHemodialisaReport('Shift Belum Dipilih','Laporan Transaksi Laboratorium');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapTransaksiHemodialisaReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapTransaksiHemodialisa_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTransaksiHemodialisa(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboUserLapTransaksiHemodialisa(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTransaksiHemodialisaKelompokPasien(),
                mComboPerseoranganLapTransaksiHemodialisa(),
                mComboAsuransiLapTransaksiHemodialisa(),
                mComboPerusahaanLapTransaksiHemodialisa(),
                mComboUmumLapTransaksiHemodialisa()
            ]
        }]
    };
    return items;
};


function getItemLapLapTransaksiHemodialisa_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapTransaksiHemodialisa_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  345,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: [
				
					{
						x: 40,y: 10,boxLabel: 'Semua',name: 'Shift_All_LapTransaksiHemodialisa',id : 'Shift_All_LapTransaksiHemodialisa',handler: function (field, value) 
						{
							if (value === true){
								Ext.getCmp('Shift_1_LapTransaksiHemodialisa').setValue(true);
								Ext.getCmp('Shift_2_LapTransaksiHemodialisa').setValue(true);
								Ext.getCmp('Shift_3_LapTransaksiHemodialisa').setValue(true);
								Ext.getCmp('Shift_1_LapTransaksiHemodialisa').disable();
								Ext.getCmp('Shift_2_LapTransaksiHemodialisa').disable();
								Ext.getCmp('Shift_3_LapTransaksiHemodialisa').disable();
							}else{
								Ext.getCmp('Shift_1_LapTransaksiHemodialisa').setValue(false);
								Ext.getCmp('Shift_2_LapTransaksiHemodialisa').setValue(false);
								Ext.getCmp('Shift_3_LapTransaksiHemodialisa').setValue(false);
								Ext.getCmp('Shift_1_LapTransaksiHemodialisa').enable();
								Ext.getCmp('Shift_2_LapTransaksiHemodialisa').enable();
								Ext.getCmp('Shift_3_LapTransaksiHemodialisa').enable();
							}
						}
					},
					{x: 110,y: 10,boxLabel: 'Shift 1',name: 'Shift_1_LapTransaksiHemodialisa',id : 'Shift_1_LapTransaksiHemodialisa'},
					{x: 180,y: 10,boxLabel: 'Shift 2',name: 'Shift_2_LapTransaksiHemodialisa',id : 'Shift_2_LapTransaksiHemodialisa'},
					{x: 250,y: 10,boxLabel: 'Shift 3',name: 'Shift_3_LapTransaksiHemodialisa',id : 'Shift_3_LapTransaksiHemodialisa'}
				]
            }
        ]
            
    };
    return items;
};

function getItemLapLapTransaksiHemodialisa_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilLab',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilLab',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilLab',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function mComboPilihanLapTransaksiHemodialisa()
{
    var cboPilihanLapTransaksiHemodialisa = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapTransaksiHemodialisa',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapTransaksiHemodialisa;
};

function mComboPilihanLapTransaksiHemodialisaKelompokPasien()
{
    var cboPilihanLapTransaksiHemodialisakelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanLapTransaksiHemodialisakelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapTransaksiHemodialisakelompokPasien;
};

function mComboPerseoranganLapTransaksiHemodialisa()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapTransaksiHemodialisa = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapTransaksiHemodialisa.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapTransaksiHemodialisa = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganLapTransaksiHemodialisa',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapTransaksiHemodialisa,
				/* new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ), */
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText;
                        }
                }
            }
	);
	return cboPerseoranganLapTransaksiHemodialisa;
};

function mComboUmumLapTransaksiHemodialisa()
{
    var cboUmumLapTransaksiHemodialisa = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumLapTransaksiHemodialisa',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText;
				}
                                
                            
			}
		}
	);
	return cboUmumLapTransaksiHemodialisa;
};

function mComboPerusahaanLapTransaksiHemodialisa()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboCustomerLab',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapTransaksiHemodialisa = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryLapTransaksiHemodialisa',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapTransaksiHemodialisa;
};

function mComboAsuransiLapTransaksiHemodialisa()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomerLab',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapTransaksiHemodialisa = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiLapTransaksiHemodialisa',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapTransaksiHemodialisa;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapTransaksiHemodialisa').show();
        Ext.getCmp('cboAsuransiLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboUmumLapTransaksiHemodialisa').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboAsuransiLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiHemodialisa').show();
        Ext.getCmp('cboUmumLapTransaksiHemodialisa').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboAsuransiLapTransaksiHemodialisa').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboUmumLapTransaksiHemodialisa').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboAsuransiLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboUmumLapTransaksiHemodialisa').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboAsuransiLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTransaksiHemodialisa').hide();
        Ext.getCmp('cboUmumLapTransaksiHemodialisa').show();
   }
}

function loadDataComboUserLapTransaksiHemodialisa(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/laptransaksi/getUser",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUserRequestEntryLapTransaksiHemodialisa.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_User_LapTransaksiHemodialisa.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_User_LapTransaksiHemodialisa.add(recs);
			}
		}
	});
}

function mComboUserLapTransaksiHemodialisa()
{
	var Field = ['KD_USER','FULL_NAME'];
    ds_User_LapTransaksiHemodialisa = new WebApp.DataStore({fields: Field});
    cboUserRequestEntryLapTransaksiHemodialisa = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cboUserRequestEntryLapTransaksiHemodialisa',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:selectSetuser,
            store: ds_User_LapTransaksiHemodialisa,
            valueField: 'kd_user',
            displayField: 'full_name',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.kd_user;
				} 
			}
        }
    )

    return cboUserRequestEntryLapTransaksiHemodialisa;
};
