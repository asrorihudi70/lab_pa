var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapPerincianPasienRWI;
var selectNamaLapPerincianPasienRWI;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapPerincianPasienRWI;
var varLapLapPerincianPasienRWI= ShowFormLapLapPerincianPasienRWI();
var winLapPerincianPasienRWIReport;
var dsPasien_LapPerincianPasienRWI;
var dsNamaPasien_LapPerincianPasienRWI;
var cboPasienLapPerincianPasienRWI;
var cboNamaPasienLapPerincianPasienRWI;
var KdKasir;

function ShowFormLapLapPerincianPasienRWI()
{
    frmDlgLapPerincianPasienRWI= fnDlgLapPerincianPasienRWI();
    frmDlgLapPerincianPasienRWI.show();
	loadDataComboUserLapPerincianPasienRWI();
	loadDataComboNamaUserLapPerincianPasienRWI();
};

function fnDlgLapPerincianPasienRWI()
{
    winLapPerincianPasienRWIReport = new Ext.Window
    (
        {
            id: 'winLapPerincianPasienRWIReport',
            title: 'Laporan Perincian Pasien Rawat Inap',
            closeAction: 'destroy',
            width: 370,
            height: 210,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapPerincianPasienRWI()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'OK',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapPerincianPasienRWI',
						handler: function()
						{
							if (ValidasiReportLapPerincianPasienRWI() === 1)
							{
								
								var params={
									KdPasien:Ext.getCmp('cboPasienLapPerincianPasienRWI').getValue(),
									KdKasir:KdKasir,
									tglAwal:Ext.getCmp('dtpTglMasukAwalFilterHasilHD').getValue(),
									tglAkhir:Ext.getCmp('dtpTglMasukAkhirFilterHasilHD').getValue(),
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/hemodialisa/lapperincianpasien/cetakPerincianPasienRWI");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapPerincianPasienRWIReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapPerincianPasienRWI',
						handler: function()
						{
							winLapPerincianPasienRWIReport.close();
						}
					}
			]

        }
    );

    return winLapPerincianPasienRWIReport;
};


function ItemDlgLapPerincianPasienRWI()
{
    var PnlLapLapPerincianPasienRWI = new Ext.Panel
    (
        {
            id: 'PnlLapLapPerincianPasienRWI',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapPerincianPasienRWI_Atas()
              
            ]
        }
    );

    return PnlLapLapPerincianPasienRWI;
};



function ValidasiReportLapPerincianPasienRWI()
{
	var x=1;
	if(Ext.getCmp('cboPasienLapPerincianPasienRWI').getValue() === ''){
		ShowPesanWarningLapPerincianPasienRWIReport('Kriteria pasien masih kosong','Warning');
        x=0;
	}
    return x;
};

function ShowPesanWarningLapPerincianPasienRWIReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapPerincianPasienRWI_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
			{
                x: 10,
                y: 5,
                xtype: 'label',
                text: 'No. Medrec'
            }, 
			{
                x: 100,
                y: 5,
                xtype: 'label',
                text: ':'
            }, 
			mComboPasienLapPerincianPasienRWI(),
			{
                x: 10,
                y: 30,
                xtype: 'label',
                text: 'Nama Pasien'
            }, 
			{
                x: 100,
                y: 30,
                xtype: 'label',
                text: ':'
            }, 
			mComboNamaPasienLapPerincianPasienRWI(),
			{
                x: 10,
                y: 55,
                xtype: 'label',
                text: 'No. Kamar'
            }, 
			{
                x: 100,
                y: 55,
                xtype: 'label',
                text: ':'
            }, 
			{
				x: 110,
                y: 55,
				xtype: 'textfield',
				name: 'txtNoKamar_LapPerincianPasienRWI',
				id: 'txtNoKamar_LapPerincianPasienRWI',
				readOnly:true,
				tabIndex:0,
				width: 220
			},
			{
                x: 10,
                y: 80,
                xtype: 'label',
                text: 'Kelas'
            }, 
			{
                x: 100,
                y: 80,
                xtype: 'label',
                text: ':'
            }, 
			{
				x: 110,
                y: 80,
				xtype: 'textfield',
				name: 'txtKelas_LapPerincianPasienRWI',
				id: 'txtKelas_LapPerincianPasienRWI',
				readOnly:true,
				tabIndex:0,
				width: 220
			},
			{
                x: 10,
                y: 105,
                xtype: 'label',
                text: 'Tanggal Masuk'
            }, 
			{
                x: 100,
                y: 105,
                xtype: 'label',
                text: ':'
            }, 
			{
                x: 110,
                y: 105,
                xtype: 'datefield',
                id: 'dtpTglMasukAwalFilterHasilHD',
                format: 'd/M/Y',
                value: now,
				readOnly:true,
                width: 100
            },
			{
                x: 215,
                y: 105,
                xtype: 'label',
                text: 's/d'
            },
			{
                x: 235,
                y: 105,
                xtype: 'datefield',
                id: 'dtpTglMasukAkhirFilterHasilHD',
                format: 'd/M/Y',
				readOnly:true,
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

function loadDataComboUserLapPerincianPasienRWI(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/lapperincianpasien/getPasien",
		params: {kode:param},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboPasienLapPerincianPasienRWI.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsPasien_LapPerincianPasienRWI.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsPasien_LapPerincianPasienRWI.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboPasienLapPerincianPasienRWI()
{
    var Field = ['kd_pasien','nama','kamar','kelas','tgl_masuk','tgl_inap'];
    dsPasien_LapPerincianPasienRWI = new WebApp.DataStore({fields: Field});
	loadDataComboUserLapPerincianPasienRWI();
    cboPasienLapPerincianPasienRWI = new Ext.form.ComboBox
    (
        {
            x: 110,
			y: 5,
            id: 'cboPasienLapPerincianPasienRWI',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
			hideTrigger		: true,
            store: dsPasien_LapPerincianPasienRWI,
            valueField: 'kd_pasien',
            displayField: 'kd_pasien',
            width:150,
            listeners:
			{
				'select': function(a, b, c)
				{
					Ext.getCmp('cboNamaPasienLapPerincianPasienRWI').setValue(b.data.nama);
					Ext.getCmp('txtNoKamar_LapPerincianPasienRWI').setValue(b.data.kamar);
					Ext.getCmp('txtKelas_LapPerincianPasienRWI').setValue(b.data.kelas);
					Ext.getCmp('dtpTglMasukAwalFilterHasilHD').setValue(ShowDate(b.data.tgl_masuk));
					Ext.getCmp('dtpTglMasukAkhirFilterHasilHD').setValue(ShowDate(b.data.tgl_inap));
					KdKasir=b.data.kd_kasir;
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cboPasienLapPerincianPasienRWI.lastQuery != '' ){
								var value="";
								
								if (value!=cboPasienLapPerincianPasienRWI.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url: baseURL + "index.php/hemodialisa/lapperincianpasien/getPasien",
										params: {kode:cboPasienLapPerincianPasienRWI.lastQuery},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											cboPasienLapPerincianPasienRWI.store.removeAll();
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsPasien_LapPerincianPasienRWI.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsPasien_LapPerincianPasienRWI.add(recs);
											}
											a.expand();
											if(dsPasien_LapPerincianPasienRWI.onShowList != undefined)
												dsPasien_LapPerincianPasienRWI.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
										}
									});
									value=cboPasienLapPerincianPasienRWI.lastQuery;
								}
							}
						},1000);
					}
				} 
			}
        }
    )
    return cboPasienLapPerincianPasienRWI;
};

function loadDataComboNamaUserLapPerincianPasienRWI(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/lapperincianpasien/getPasienByNama",
		params: {nama:param},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboNamaPasienLapPerincianPasienRWI.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsNamaPasien_LapPerincianPasienRWI.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsNamaPasien_LapPerincianPasienRWI.add(recs);
				console.log(o);
			}
		}
	});
}
function mComboNamaPasienLapPerincianPasienRWI()
{
    var Field = ['kd_pasien','nama','kamar','kelas','tgl_masuk','tgl_inap'];
    dsNamaPasien_LapPerincianPasienRWI = new WebApp.DataStore({fields: Field});
	loadDataComboNamaUserLapPerincianPasienRWI();
    cboNamaPasienLapPerincianPasienRWI = new Ext.form.ComboBox
    (
        {
            x: 110,
			y: 30,
            id: 'cboNamaPasienLapPerincianPasienRWI',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
			hideTrigger		: true,
            store: dsNamaPasien_LapPerincianPasienRWI,
            valueField: 'nama',
            displayField: 'nama',
            width:150,
            listeners:
			{
				'select': function(a, b, c)
				{
					Ext.getCmp('cboPasienLapPerincianPasienRWI').setValue(b.data.kd_pasien);
					Ext.getCmp('txtNoKamar_LapPerincianPasienRWI').setValue(b.data.kamar);
					Ext.getCmp('txtKelas_LapPerincianPasienRWI').setValue(b.data.kelas);
					Ext.getCmp('dtpTglMasukAwalFilterHasilHD').setValue(ShowDate(b.data.tgl_masuk));
					Ext.getCmp('dtpTglMasukAkhirFilterHasilHD').setValue(ShowDate(b.data.tgl_inap));
					KdKasir=b.data.kd_kasir;
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cboNamaPasienLapPerincianPasienRWI.lastQuery != '' ){
								var value="";
								
								if (value!=cboNamaPasienLapPerincianPasienRWI.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url: baseURL + "index.php/hemodialisa/lapperincianpasien/getPasienByNama",
										params: {nama:cboNamaPasienLapPerincianPasienRWI.lastQuery},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											cboNamaPasienLapPerincianPasienRWI.store.removeAll();
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsNamaPasien_LapPerincianPasienRWI.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsNamaPasien_LapPerincianPasienRWI.add(recs);
											}
											a.expand();
											if(dsNamaPasien_LapPerincianPasienRWI.onShowList != undefined)
												dsNamaPasien_LapPerincianPasienRWI.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
										}
									});
									value=cboNamaPasienLapPerincianPasienRWI.lastQuery;
								}
							}
						},1000);
					}
				} 
			}
        }
    )
    return cboNamaPasienLapPerincianPasienRWI;
};
