var type_file=0;
var dsIGDBatalTransaksi;
var selectIGDBatalTransaksi;
var selectNamaIGDBatalTransaksi;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgIGDBatalTransaksi;
var varLapIGDBatalTransaksi= ShowFormLapIGDBatalTransaksi();
var dsIGD;

function ShowFormLapIGDBatalTransaksi()
{
    frmDlgIGDBatalTransaksi= fnDlgIGDBatalTransaksi();
    frmDlgIGDBatalTransaksi.show();
};

function fnDlgIGDBatalTransaksi()
{
    var winIGDBatalTransaksiReport = new Ext.Window
    (
        {
            id: 'winIGDBatalTransaksiReport',
            title: 'Laporan Pembatalan Transaksi',
            closeAction: 'destroy',
            width:450,
            height: 180,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDBatalTransaksi()]

        }
    );

    return winIGDBatalTransaksiReport;
};

function ItemDlgIGDBatalTransaksi()
{
    var PnlLapIGDBatalTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapIGDBatalTransaksi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 180,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDBatalTransaksi_Periode(),
				//getItemIGDBatalTransaksi_Shift(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapIGDBatalTransaksi',
					handler: function()
					{
					   if (ValidasiReportIGDBatalTransaksi() === 1)
					   {
						
							var params={
									start_date 	: Ext.getCmp('dtpTglAwalLapIGDBatalTransaksi').getValue(),
									last_date 	: Ext.getCmp('dtpTglAkhirLapIGDBatalTransaksi').getValue(),
									type_file 	: type_file,
									order_by 	: Ext.getCmp('cboOrderBy').getValue(),
									} ;
							var form = document.createElement("form");
							console.log(params);
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/gawat_darurat/function_lap/laporan_batal_transaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							// frmDlgIGDBatalTransaksi.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLapIGDBatalTransaksi').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapIGDBatalTransaksi').getValue()+'#aje#Laporan Batal Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionIGD/cetakIGDBatalTransaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgIGDBatalTransaksi.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapIGDBatalTransaksi',
					handler: function()
					{
						frmDlgIGDBatalTransaksi.close();
					}
				}
			
			]
        }
    );

    return PnlLapIGDBatalTransaksi;
};


function ValidasiReportIGDBatalTransaksi()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapIGDBatalTransaksi').dom.value === '')
	{
		ShowPesanWarningIGDBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapIGDBatalTransaksi').getValue() === false && Ext.getCmp('Shift_1_LapIGDBatalTransaksi').getValue() === false && Ext.getCmp('Shift_2_LapIGDBatalTransaksi').getValue() === false && Ext.getCmp('Shift_3_LapIGDBatalTransaksi').getValue() === false){
		ShowPesanWarningIGDBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportIGDBatalTransaksi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDBatalTransaksi').dom.value > Ext.get('dtpTglAkhirLapIGDBatalTransaksi').dom.value)
    {
        ShowPesanWarningIGDBatalTransaksiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDBatalTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapIGDBatalTransaksi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapIGDBatalTransaksi_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapIGDBatalTransaksi',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					mComboOrder(),
					{
					   xtype: 'checkbox',
					   fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					},
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapIGDBatalTransaksi',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};



function mComboOrder()
{
    var cboOrderBy = new Ext.form.ComboBox
	(
		{
			/*x: 120,
			y: 220,*/
			id:'cboOrderBy',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Order  ',
			// width:240,
			anchor:'99%',
			value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				    data: [[0, 'Medrec'],[1, 'Nama Pasien']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Medrec',
		}
	);
	return cboOrderBy;
};
