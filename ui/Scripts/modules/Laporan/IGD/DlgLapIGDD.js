
var dsIGD;
var selectIGD;
var selectNamaIGD;
var now = new Date();

var frmDlgIGD;
var varLapIGD= ShowFormLapIGD();

function ShowFormLapIGD()
{
    frmDlgIGD= fnDlgIGD();
    frmDlgIGD.show();
};

function fnDlgIGD()
{
    var winIGDReport = new Ext.Window
    (
        {
            id: 'winIGDReport',
            title: 'Laporan Rawat Jalan',
            closeAction: 'destroy',
            width:500,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGD()]

        }
    );

    return winIGDReport;
};


function ItemDlgIGD()
{
    var PnlLapIGD = new Ext.Panel
    (
        {
            id: 'PnlLapIGD',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGD_Tanggal(),getItemLapIGD_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapIGD',
                            handler: function()
                            {
                                if (ValidasiReportIGD() === 1)
                                {
                                    //if (ValidasiTanggalReportIGD() === 1)
                                    //{
                                        var criteria = GetCriteriaIGD();
                                        frmDlgIGD.close();
                                        ShowReport('0', 1010201, criteria);
//                                        window.open("http://localhost/Simrs/base/tmp/1421630699Tmp.pdf");
                                    //};
                                    //alert(tmp);
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapIGD',
                            handler: function()
                            {
                                    frmDlgIGD.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapIGD;
};

function GetCriteriaIGD()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapIGD').dom.value != '')
	{
		strKriteria = Ext.get('dtpTglAwalLapIGD').dom.value;
	};

	if (Ext.get('dtpTglAkhirLapIGD').dom.value != '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapIGD').dom.value;
	};

	if (selectIGD != undefined)
	{
		strKriteria += '##@@##' + selectIGD;
		strKriteria += '##@@##' + selectNamaIGD ;
	};

	return strKriteria;
};


function ValidasiReportIGD()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapIGD').dom.value === '') || (Ext.get('dtpTglAkhirLapIGD').dom.value === '') || (selectIGD === undefined) || (Ext.get('comboUnitLapIGD').dom.value === ''))
    {
            if(Ext.get('dtpTglAwalLapIGD').dom.value === '')
            {
                    ShowPesanWarningIGDReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('dtpTglAkhirLapIGD').dom.value === '')
            {
                    ShowPesanWarningIGDReport(nmGetValidasiKosong(nmFinishDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('comboUnitLapIGD').dom.value === '' )
            {
                    ShowPesanWarningIGDReport(nmGetValidasiKosong(nmDeptDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportIGD()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGD').dom.value > Ext.get('dtpTglAkhirLapIGD').dom.value)
    {
        ShowPesanWarningIGDReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapIGD_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:1,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                    mComboUnitLapIGD()
                ]
            }
        ]
    }
    return items;
};


function getItemLapIGD_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: 0.45,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 85,
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmPeriodeDlgRpt + ' ',
                    id: 'dtpTglAwalLapIGD',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        },
        {
            columnWidth: 0.48,
            layout: 'form',
            border: false,
                    labelWidth: 25,
            labelAlign: 'right',
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmSd + ' ',
                    id: 'dtpTglAkhirLapIGD',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        }
        ]
    }
    return items;
};


function mComboUnitLapIGD()
{
    var dataSource_All = new Ext.data.Store({
	reader: new Ext.data.JsonReader
		({
			//fields: ['code',  'NoFaktur',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			fields: ['KD_UNIT', 'NAMA_UNIT']
		}),
	data:
		[
			{ KD_UNIT:'xxx', NAMA_UNIT:'Semua Unit'},
                        { KD_UNIT:'208', NAMA_UNIT:'Anak'},
                        { KD_UNIT:'209', NAMA_UNIT:'Bedah'},
                        { KD_UNIT:'215', NAMA_UNIT:'DM'},
                        { KD_UNIT:'202', NAMA_UNIT:'Gigi'},
                        { KD_UNIT:'203', NAMA_UNIT:'Gizi'},
                        { KD_UNIT:'206', NAMA_UNIT:'Interna'},
                        { KD_UNIT:'216', NAMA_UNIT:'Jantung'},
                        { KD_UNIT:'214', NAMA_UNIT:'Jiwa'},
                        { KD_UNIT:'207', NAMA_UNIT:'Kandungan'},
                        { KD_UNIT:'213', NAMA_UNIT:'KIA'},
                        { KD_UNIT:'212', NAMA_UNIT:'Kulit Kelamin'},
                        { KD_UNIT:'211', NAMA_UNIT:'Mata'},
                        { KD_UNIT:'204', NAMA_UNIT:'Syaraf'},
                        { KD_UNIT:'210', NAMA_UNIT:'THT'},
                        { KD_UNIT:'201', NAMA_UNIT:'Umum'},
                        { KD_UNIT:'205', NAMA_UNIT:'VCT'},
		]
	});
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    dsIGD = new WebApp.DataStore({ fields: Field });

    dsIGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    );
    
  var comboUnitLapIGD = new Ext.form.ComboBox
    (
        {
            id:'comboUnitLapIGD',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Poliklinik ',
            align:'Right',
            anchor:'99%',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store:dataSource_All,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectIGD=b.data.KD_UNIT ;
                    selectNamaIGD=b.data.NAMA_UNIT ;
                }
            }
        }
    );

//    dsIGD.load
//    (
//        {
//            params:
//            {
//                Skip: 0,
//                Take: 1000,
//                //Sort: 'DEPT_ID',
//                Sort: 'NAMA_UNIT',
//                Sortdir: 'ASC',
//                target:'ViewSetupUnit',
//                param: ''
//            }
//        }
//    );
    return comboUnitLapIGD;
};