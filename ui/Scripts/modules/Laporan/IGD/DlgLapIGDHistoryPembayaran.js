var type_file=0;
var dsIGDHistoryPembayaran;
var selectIGDHistoryPembayaran;
var selectNamaIGDHistoryPembayaran;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgIGDHistoryPembayaran;
var varLapIGDHistoryPembayaran= ShowFormLapIGDHistoryPembayaran();
var dsIGD;

function ShowFormLapIGDHistoryPembayaran()
{
    frmDlgIGDHistoryPembayaran= fnDlgIGDHistoryPembayaran();
    frmDlgIGDHistoryPembayaran.show();
};

function fnDlgIGDHistoryPembayaran()
{
    var winIGDHistoryPembayaranReport = new Ext.Window
    (
        {
            id: 'winIGDHistoryPembayaranReport',
            title: 'Laporan Batal Pembayaran',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDHistoryPembayaran()]

        }
    );

    return winIGDHistoryPembayaranReport;
};

function ItemDlgIGDHistoryPembayaran()
{
    var PnlLapIGDHistoryPembayaran = new Ext.Panel
    (
        {
            id: 'PnlLapIGDHistoryPembayaran',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDHistoryPembayaran_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					// text: 'Preview',
					text: 'Print',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapIGDHistoryPembayaran',
					handler: function()
					{
					   if (ValidasiReportIGDHistoryPembayaran() === 1)
					   {
							var params={
								start_date 	:Ext.getCmp('dtpTglAwalLapIGDHistoryPembayaran').getValue(),
								last_date 	:Ext.getCmp('dtpTglAkhirLapIGDHistoryPembayaran').getValue(),
								type_file 	:type_file
							} ;
							console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/gawat_darurat/function_lap/laporan_batal_pembayaran");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgIGDHistoryPembayaran.close(); 
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Print',
					hidden : true,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnPrintLapIGDHistoryPembayaran',
					handler: function()
					{
					   if (ValidasiReportIGDHistoryPembayaran() === 1)
					   {
							var params={
								tglAwal:Ext.getCmp('dtpTglAwalLapIGDHistoryPembayaran').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapIGDHistoryPembayaran').getValue(),
								type_file:type_file
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_history_pembayaran/cetak_direct");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgIGDHistoryPembayaran.close(); 
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapIGDHistoryPembayaran',
					handler: function()
					{
						frmDlgIGDHistoryPembayaran.close();
					}
				}
			
			]
        }
    );

    return PnlLapIGDHistoryPembayaran;
};


function ValidasiReportIGDHistoryPembayaran()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapIGDHistoryPembayaran').dom.value === '')
	{
		ShowPesanWarningIGDHistoryPembayaranReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}

    return x;
};

function ValidasiTanggalReportIGDHistoryPembayaran()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDHistoryPembayaran').dom.value > Ext.get('dtpTglAkhirLapIGDHistoryPembayaran').dom.value)
    {
        ShowPesanWarningIGDHistoryPembayaranReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDHistoryPembayaranReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapIGDHistoryPembayaran_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapIGDHistoryPembayaran_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapIGDHistoryPembayaran',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					{
					   xtype: 'checkbox',
					   fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					}
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapIGDHistoryPembayaran',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


