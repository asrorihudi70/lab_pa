
var dsIGDKIUP;
var selectNamaIGDKIUP;
var now = new Date();

var frmDlgIGDKIUP;
var varLapIGDKIUP= ShowFormLapIGDKIUP();

function ShowFormLapIGDKIUP()
{
    frmDlgIGDKIUP= fnDlgIGDKIUP();
    frmDlgIGDKIUP.show();
};

function fnDlgIGDKIUP()
{
    var winIGDKIUPReport = new Ext.Window
    (
        {
            id: 'winIGDKIUPReport',
            title: 'Laporan Instansi Gawat Darurat',
            closeAction: 'destroy',
            width:500,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDKIUP()]

        }
    );

    return winIGDKIUPReport;
};


function ItemDlgIGDKIUP()
{
    var PnlLapIGDKIUP = new Ext.Panel
    (
        {
            id: 'PnlLapIGDKIUP',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDKIUP_Tanggal(),
                getItemLapIGDKIUP_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapIGDKIUP',
                            handler: function()
                            {
                                if (ValidasiReportIGDKIUP() === 1)
                                {
                                    //if (ValidasiTanggalReportIGDKIUP() === 1)
                                    //{
                                        var criteria = GetCriteriaIGDKIUP();
                                        frmDlgIGDKIUP.close();
                                        loadlaporanIGD('0', 'rep020201', criteria);
                                    //};
                                    //alert(tmp);
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapIGDKIUP',
                            handler: function()
                            {
                                    frmDlgIGDKIUP.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapIGDKIUP;
};

function GetCriteriaIGDKIUP()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapIGDKIUP').dom.value !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapIGDKIUP').dom.value;
	};

	if (Ext.get('dtpTglAkhirLapIGDKIUP').dom.value !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapIGDKIUP').dom.value;
	};
        if (Ext.getDom('rb_nama').checked === true)
        {
                strKriteria += '##@@##' + Ext.get('rb_nama').dom.value;
        }else
            {
                strKriteria += '##@@##' + Ext.get('rb_medrec').dom.value;
            }

	return strKriteria;
};


function ValidasiReportIGDKIUP()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapIGDKIUP').dom.value === '') || (Ext.get('dtpTglAkhirLapIGDKIUP').dom.value === ''))
    {
            if(Ext.get('dtpTglAwalLapIGDKIUP').dom.value === '')
            {
                    ShowPesanWarningIGDKIUPReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('dtpTglAkhirLapIGDKIUP').dom.value === '')
            {
                    ShowPesanWarningIGDKIUPReport(nmGetValidasiKosong(nmFinishDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
    };

    return x;
};

function ValidasiTanggalReportIGDKIUP()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDKIUP').dom.value > Ext.get('dtpTglAkhirLapIGDKIUP').dom.value)
    {
        ShowPesanWarningIGDKIUPReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDKIUPReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapIGDKIUP_Dept()
{
    var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: .90,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 150,
            items:
            [
                {
                    xtype: 'radiogroup',
                    id: 'rbrujukan',
                    fieldLabel: 'Urut Berdasarkan ',
                    items: [
                    {boxLabel: 'No. Medical Record',
                        name: 'rb_auto',
                        id: 'rb_medrec',
                        inputValue: 'K.Kd_Pasien'
                    },
                    {boxLabel: 'Nama Pasien',
                        name: 'rb_auto',
                        id: 'rb_nama',
                        inputValue: 'P.nama'
                    }
                ]
                }
            ]
        }
        ]
        
    }
    return items;
};

function getItemLapIGDKIUP_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: 0.45,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 85,
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: 'Tanggal ',
                    id: 'dtpTglAwalLapIGDKIUP',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        },
        {
            columnWidth: 0.48,
            layout: 'form',
            border: false,
                    labelWidth: 25,
            labelAlign: 'right',
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: 's/d ',
                    id: 'dtpTglAkhirLapIGDKIUP',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        }
        ]
    }
    return items;
};


function mComboUnitLapIGDKIUP()
{
    var dataSource_All = new Ext.data.Store({
	reader: new Ext.data.JsonReader
		({
			//fields: ['code',  'NoFaktur',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			fields: ['KD_UNIT', 'NAMA_UNIT']
		}),
	data:
		[
			{ KD_UNIT:'xxx', NAMA_UNIT:'Semua Unit'},
                        { KD_UNIT:'208', NAMA_UNIT:'Anak'},
                        { KD_UNIT:'209', NAMA_UNIT:'Bedah'},
                        { KD_UNIT:'215', NAMA_UNIT:'DM'},
                        { KD_UNIT:'202', NAMA_UNIT:'Gigi'},
                        { KD_UNIT:'203', NAMA_UNIT:'Gizi'},
                        { KD_UNIT:'206', NAMA_UNIT:'Interna'},
                        { KD_UNIT:'216', NAMA_UNIT:'Jantung'},
                        { KD_UNIT:'214', NAMA_UNIT:'Jiwa'},
                        { KD_UNIT:'207', NAMA_UNIT:'Kandungan'},
                        { KD_UNIT:'213', NAMA_UNIT:'KIA'},
                        { KD_UNIT:'212', NAMA_UNIT:'Kulit Kelamin'},
                        { KD_UNIT:'211', NAMA_UNIT:'Mata'},
                        { KD_UNIT:'204', NAMA_UNIT:'Syaraf'},
                        { KD_UNIT:'210', NAMA_UNIT:'THT'},
                        { KD_UNIT:'201', NAMA_UNIT:'Umum'},
                        { KD_UNIT:'205', NAMA_UNIT:'VCT'},
		]
	});
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    dsIGDKIUP = new WebApp.DataStore({ fields: Field });

    dsIGDKIUP.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    );
    
  var comboUnitLapIGDKIUP = new Ext.form.ComboBox
    (
        {
            id:'comboUnitLapIGDKIUP',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Poliklinik ',
            align:'Right',
            anchor:'99%',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store:dataSource_All,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectIGDKIUP=b.data.KD_UNIT ;
                    selectNamaIGDKIUP=b.data.NAMA_UNIT ;
                }
            }
        }
    );

//    dsIGDKIUP.load
//    (
//        {
//            params:
//            {
//                Skip: 0,
//                Take: 1000,
//                //Sort: 'DEPT_ID',
//                Sort: 'NAMA_UNIT',
//                Sortdir: 'ASC',
//                target:'ViewSetupUnit',
//                param: ''
//            }
//        }
//    );
    return comboUnitLapIGDKIUP;
};