
var dsIGDPasienPerDokter;
var selectIGDPasienPerDokter;
var selectNamaIGDPasienPerDokter;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgIGDPasienPerDokter;
var varLapIGDPasienPerDokter= ShowFormLapIGDPasienPerDokter();
var dsIGD;
function ShowFormLapIGDPasienPerDokter()
{
    frmDlgIGDPasienPerDokter= fnDlgIGDPasienPerDokter();
    frmDlgIGDPasienPerDokter.show();
};

function fnDlgIGDPasienPerDokter()
{
    var winIGDPasienPerDokterReport = new Ext.Window
    (
        {
            id: 'winIGDPasienPerDokterReport',
            title: 'Laporan Pasien per Dokter',
            closeAction: 'destroy',
            width:450,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDPasienPerDokter()]

        }
    );

    return winIGDPasienPerDokterReport;
};

function ItemDlgIGDPasienPerDokter()
{
    var PnlLapIGDPasienPerDokter = new Ext.Panel
    (
        {
            id: 'PnlLapIGDPasienPerDokter',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDPasienPerDokter_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapIGDPasienPerDokter',
					handler: function()
					{
					   if (ValidasiReportIGDPasienPerDokter() === 1)
					   {								
							var params={
								kd_poli:Ext.getCmp('cboUnitPilihanIGDPasien').getValue(),
								kd_dokter:Ext.getCmp('cboDokterPilihanIGDPasien').getValue(),
								tglAwal:Ext.getCmp('dtpTglAwalLapIGDPasienPerDokter').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapIGDPasienPerDokter').getValue(),
							} 
							//console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/gawat_darurat/lap_IGDPasienPerDokter/cetak");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgIGDPasienPerDokter.close(); 
							
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapIGDPasienPerDokter',
					handler: function()
					{
						frmDlgIGDPasienPerDokter.close();
					}
				}
			
			]
        }
    );

    return PnlLapIGDPasienPerDokter;
};


function ValidasiReportIGDPasienPerDokter()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapIGDPasienPerDokter').dom.value === '')
	{
		ShowPesanWarningIGDPasienPerDokterReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
    return x;
};

function ValidasiTanggalReportIGDPasienPerDokter()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDPasienPerDokter').dom.value > Ext.get('dtpTglAkhirLapIGDPasienPerDokter').dom.value)
    {
        ShowPesanWarningIGDPasienPerDokterReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDPasienPerDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};



function getItemLapIGDPasienPerDokter_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  '100%',
				height: 300,
				items:
				[
					{
						x: 10,
						y: 10,
						width: 170,
						xtype: 'label',
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapIGDPasienPerDokter',
						format: 'd-M-Y',
						value:now,
						//anchor: '99%'
					},
					{
						x: 190,
						y: 10,
						width: 10,
						xtype: 'label',
						text : 's/d'
					},
					{
						x: 220,
						y: 10,
						width: 170,
						xtype: 'datefield',
						id: 'dtpTglAkhirLapIGDPasienPerDokter',
						format: 'd-M-Y',
						value:now,
						//anchor: '100%'
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Instalasi '
					}, {
						x: 190,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
						mComboUnitPilihanIGDPasienPerDokter(),
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Dokter '
					}, {
						x: 190,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
						mComboDokterPilihanIGDPasienPerDokter(),
					
				]
			},
     
        ]
    }
    return items;
};

function mComboUnitPilihanIGDPasienPerDokter()
{

   var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewComboUnitLaporan',
                param: "left(kd_unit, 1)='3' and parent=~3~"
            }
        }
    )
    var cboPilihanIGDTransaksi = new Ext.form.ComboBox
	(
			{
			x: 220,
			y: 40,
            id: 'cboUnitPilihanIGDPasien',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Unit ...',
            fieldLabel: 'Instalasi ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            value:'Semua',
            width:170,
			tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
				{
					polipilihanpasien=b.data.KD_UNIT;
				},


		}
        }
	);
	return cboPilihanIGDTransaksi;
};

function mComboDokterPilihanIGDPasienPerDokter()
{

   var Field = ['KD_DOKTER','NAMA'];
    ds_Dokter_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Dokter_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA',
                Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: " WHERE left(dokter_klinik.kd_unit, 1)='2'"
            }
        }
    )
    var cboPilihanIGDTransaksi = new Ext.form.ComboBox
	(
		{
			x: 220,
			y: 70,
            id: 'cboDokterPilihanIGDPasien',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Dokter ...',
            fieldLabel: 'Dokter ',
            align: 'Right',
            store: ds_Dokter_viDaftar,
            valueField: 'KD_DOKTER',
            displayField: 'NAMA',
            width:170,
            value:'Semua',
			tabIndex:29,
            listeners:
            {
                'select': function(a, b, c)
				{
						//Dokterpilihanpasien=b.data.KD_UNIT;
				},
			}
        }
	);
	return cboPilihanIGDTransaksi;
};
