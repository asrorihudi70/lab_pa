
var dsIGDPasienPerKelompok;
var selectIGDPasienPerKelompok;
var selectNamaIGDPasienPerKelompok;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgIGDPasienPerKelompok;
var varLapIGDPasienPerKelompok= ShowFormLapIGDPasienPerKelompok();
var dsIGD;


var selectSetPilihankelompokPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapIGDPasienPerKelompok()
{
    frmDlgIGDPasienPerKelompok= fnDlgIGDPasienPerKelompok();
    frmDlgIGDPasienPerKelompok.show();
};

function fnDlgIGDPasienPerKelompok()
{
    var winIGDPasienPerKelompokReport = new Ext.Window
    (
        {
            id: 'winIGDPasienPerKelompokReport',
            title: 'Laporan Pasien per Kelompok',
            closeAction: 'destroy',
            width:450,
            height: 230,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDPasienPerKelompok()]
        }
    );

    return winIGDPasienPerKelompokReport;
};

function ItemDlgIGDPasienPerKelompok()
{
    var PnlLapIGDPasienPerKelompok = new Ext.Panel
    (
        {
            id: 'PnlLapIGDPasienPerKelompok',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 230,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDPasienPerKelompok_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapIGDPasienPerKelompok',
					handler: function()
					{
					   if (ValidasiReportIGDPasienPerKelompok() === 1)
					   {								
							var params={
								kd_poli:Ext.getCmp('cboUnitPilihanIGDPasien').getValue(),
								kd_kelompok:GetCriteriaIGDPasienPerKelompok(),
								tglAwal:Ext.getCmp('dtpTglAwalLapIGDPasienPerKelompok').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapIGDPasienPerKelompok').getValue(),
							} 
							//console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/gawat_darurat/lap_IGDPasienPerKelompok/cetak");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgIGDPasienPerKelompok.close(); 
							
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapIGDPasienPerKelompok',
					handler: function()
					{
						frmDlgIGDPasienPerKelompok.close();
					}
				}
			
			]
        }
    );

    return PnlLapIGDPasienPerKelompok;
};


function ValidasiReportIGDPasienPerKelompok()
{
    var x=1;
	if(Ext.getCmp('dtpTglAwalLapIGDPasienPerKelompok').getValue() >Ext.getCmp('dtpTglAkhirLapIGDPasienPerKelompok').getValue())
    {
        ShowPesanWarningPelayananDokterReport('Tanggal akhir tidak boleh lebih cepat dari tanggal awal','Laporan Pasien per Kelompok');
        x=0;
    }
	

    return x;
};


function ShowPesanWarningIGDPasienPerKelompokReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};



function getItemLapIGDPasienPerKelompok_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  '100%',
				height: 300,
				items:
				[
					{
						x: 10,
						y: 10,
						width: 170,
						xtype: 'label',
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapIGDPasienPerKelompok',
						format: 'd-M-Y',
						value:now,
						//anchor: '99%'
					},
					{
						x: 190,
						y: 10,
						width: 10,
						xtype: 'label',
						text : 's/d'
					},
					{
						x: 220,
						y: 10,
						width: 170,
						xtype: 'datefield',
						id: 'dtpTglAkhirLapIGDPasienPerKelompok',
						format: 'd-M-Y',
						value:now,
						//anchor: '100%'
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Instalasi '
					}, {
						x: 190,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
						mComboUnitPilihanIGDPasienPerKelompok(),
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Kelompok '
					}, {
						x: 190,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
						mComboIGDPasienPerKelompok(),
					{
						x: 10,
						y: 100,
						xtype: 'label',
						text: 'Detail '
					}, {
						x: 190,
						y: 100,
						xtype: 'label',
						text: ' : '
					},
						mComboIGDPasienPerKelompokSEMUA(),
						mComboIGDPasienPerKelompokPERORANGAN(),
						mComboIGDPasienPerKelompokPERUSAHAAN(),
						mComboIGDPasienPerKelompokASURANSI(),
					
				]
			},
     
        ]
    }
    return items;
};

function mComboUnitPilihanIGDPasienPerKelompok()
{
   var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewComboUnitLaporan',
                param: "left(kd_unit, 1)='3' and parent=~3~"
            }
        }
    )
    var cboPilihanIGDTransaksi = new Ext.form.ComboBox
	(
			{
			x: 220,
			y: 40,
            id: 'cboUnitPilihanIGDPasien',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Unit ...',
            fieldLabel: 'Instalasi ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            value: 'Semua',
            width:170,
			tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
				{
					polipilihanpasien=b.data.KD_UNIT;
				},


		}
        }
	);
	return cboPilihanIGDTransaksi;
};

function mComboIGDPasienPerKelompok()
{
    var cboPilihanPelayananDokterkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 220,
                y: 70,
                id:'cboPilihanIGDPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 170,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectIGDPasienPerKelompok(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanPelayananDokterkelompokPasien;
};

//IGDPasienPerKelompok
function mComboIGDPasienPerKelompokSEMUA()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganIGD = new Ext.form.ComboBox
	(
            {
                x: 220,
                y: 100,
                id:'IDmComboIGDPasienPerKelompokSEMUA',
                typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				selectOnFocus:true,
				forceSelection: true,
				emptyText:'Silahkan Pilih...',
				valueField: 'Id',
	            displayField: 'displayText',
	            hidden:false,
				fieldLabel: '',
				width:170,
				value:1,
				store: new Ext.data.ArrayStore
				(
						{
							id: 0,
							fields:
								[
										'Id',
										'displayText'
								],
							data: [[1, 'Semua']]
						}
				),
				listeners:
				{
					'select': function(a,b,c)
					{
						selectSetUmum=b.data.displayText ;
					}
	                                
	                            
				}
            }
	);
	return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokPERORANGAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganIGD = new Ext.form.ComboBox
	(
            {
                x: 220,
                y: 100,
                id:'IDmComboIGDPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:170,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokPERUSAHAAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1  order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganIGD = new Ext.form.ComboBox
	(
            {
                x: 220,
                y: 100,
                id:'IDmComboIGDPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width:170,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
				//value: selectsetperusahaan,
                listeners:
                {
				    'select': function(a,b,c)
					{
				        selectsetperusahaan = b.data.KD_CUSTOMER;
						selectsetnamaperusahaan = b.data.CUSTOMER;
					}
                }
            }
	);
	return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokASURANSI()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=2  order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganIGD = new Ext.form.ComboBox
	(
            {
                x: 220,
                y: 100,
                id:'IDmComboIGDPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width:170,
                store: dsPerseoranganRequestEntry,
				valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
				    'select': function(a,b,c)
					{
						selectSetAsuransi=b.data.KD_CUSTOMER ;
						selectsetnamaAsuransi = b.data.CUSTOMER ;
					}
                }
            }
	);
	return cboPerseoranganIGD;
};


function Combo_SelectIGDPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').show();
   }
};


function GetCriteriaIGDPasienPerKelompok()
{
	var strKriteria = '';
	
	if (Ext.getCmp('cboPilihanIGDPasienPerKelompok').getValue() !== '')
	{
		if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
		else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').getValue(); } 
		else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').getValue(); } 
		else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').getValue(); }
	}else{
            strKriteria = 'Semua';
	}

	
	return strKriteria;
};