
var dsIGDPasienPerStatus;
var selectIGDPasienPerStatus;
var selectNamaIGDPasienPerStatus;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgIGDPasienPerStatus;
var varLapIGDPasienPerStatus= ShowFormLapIGDPasienPerStatus();
var dsIGD;


var selectSetPilihanStatusPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapIGDPasienPerStatus()
{
    frmDlgIGDPasienPerStatus= fnDlgIGDPasienPerStatus();
    frmDlgIGDPasienPerStatus.show();
};

function fnDlgIGDPasienPerStatus()
{
    var winIGDPasienPerStatusReport = new Ext.Window
    (
        {
            id: 'winIGDPasienPerStatusReport',
            title: 'Laporan Pasien per Status',
            closeAction: 'destroy',
            width:450,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDPasienPerStatus()]
        }
    );

    return winIGDPasienPerStatusReport;
};

function ItemDlgIGDPasienPerStatus()
{
    var PnlLapIGDPasienPerStatus = new Ext.Panel
    (
        {
            id: 'PnlLapIGDPasienPerStatus',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDPasienPerStatus_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapIGDPasienPerStatus',
					handler: function()
					{
					   if (ValidasiReportIGDPasienPerStatus() === 1)
					   {								
							var params={
								kd_poli:Ext.getCmp('IDcboUnitPilihanIGDPasien').getValue(),
								kd_status:Ext.getCmp('IDmComboIGDPasienPerStatus').getValue(),
								tglAwal:Ext.getCmp('dtpTglAwalLapIGDPasienPerStatus').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapIGDPasienPerStatus').getValue(),
							} 
							//console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/gawat_darurat/lap_IGDPasienPerStatus/cetak");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgIGDPasienPerStatus.close(); 
							
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapIGDPasienPerStatus',
					handler: function()
					{
						frmDlgIGDPasienPerStatus.close();
					}
				}
			
			]
        }
    );

    return PnlLapIGDPasienPerStatus;
};


function ValidasiReportIGDPasienPerStatus()
{
    var x=1;
	if(Ext.getCmp('dtpTglAwalLapIGDPasienPerStatus').getValue() >Ext.getCmp('dtpTglAkhirLapIGDPasienPerStatus').getValue())
    {
        ShowPesanWarningPelayananDokterReport('Tanggal akhir tidak boleh lebih cepat dari tanggal awal','Laporan Pasien per Status');
        x=0;
    }
	

    return x;
};


function ShowPesanWarningIGDPasienPerStatusReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};



function getItemLapIGDPasienPerStatus_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  '100%',
				height: 200,
				items:
				[
					{
						x: 10,
						y: 10,
						width: 170,
						xtype: 'label',
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapIGDPasienPerStatus',
						format: 'd-M-Y',
						value:now,
						//anchor: '99%'
					},
					{
						x: 190,
						y: 10,
						width: 10,
						xtype: 'label',
						text : 's/d'
					},
					{
						x: 220,
						y: 10,
						width: 170,
						xtype: 'datefield',
						id: 'dtpTglAkhirLapIGDPasienPerStatus',
						format: 'd-M-Y',
						value:now,
						//anchor: '100%'
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Unit '
					}, {
						x: 190,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
						mComboUnitPilihanIGDPasienPerStatus(),
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Status '
					}, {
						x: 190,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
						mComboIGDPasienPerStatus(),
					
				]
			},
     
        ]
    }
    return items;
};

function mComboUnitPilihanIGDPasienPerStatus()
{
   var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewComboUnitLaporan',
                param: "left(kd_unit, 1)='3'"
            }
        }
    )
    var cboPilihanIGDTransaksi = new Ext.form.ComboBox
	(
			{
			x: 220,
			y: 40,
            id: 'IDcboUnitPilihanIGDPasien',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poliklinik ...',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            value:'Semua',
            width:170,
			tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
				{
					polipilihanpasien=b.data.KD_UNIT;
				},


		}
        }
	);
	return cboPilihanIGDTransaksi;
};

function mComboIGDPasienPerStatus()
{
   var Field = ['kd_status_pulang','status_pulang'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'STATUS',
                Sortdir: 'ASC',
                target:'ViewComboStatusPulang',
                param: "3"
            }
        }
    )
    var cboPilihanIGDTransaksi = new Ext.form.ComboBox
	(
			{
			x: 220,
			y: 70,
            id: 'IDmComboIGDPasienPerStatus',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Status ...',
            fieldLabel: 'Status ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'kd_status_pulang',
            displayField: 'status_pulang',
            width:170,
			tabIndex:29,
			
            listeners:
                {
                'select': function(a, b, c)
				{
					/*polipilihanpasien=b.data.KD_UNIT;*/
				},


		}
        }
	);
	return cboPilihanIGDTransaksi;
};
