
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsIGDPelayananDokter;
var selectNamaIGDPelayananDokter;
var now = new Date();
var selectSetPerseorangan;
var frmDlgIGDPelayananDokter;
var varLapIGDPelayananDokter= ShowFormLapIGDPelayananDokter();
var selectSetUmum;
var selectSetkelpas;



var selectSetPilihankelompokPasien;
var selectSetPilihanProfesi;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapIGDPelayananDokter()
{
    frmDlgIGDPelayananDokter= fnDlgIGDPelayananDokter();
    frmDlgIGDPelayananDokter.show();
};

function fnDlgIGDPelayananDokter()
{
    var winIGDPelayananDokterReport = new Ext.Window
    (
        {
            id: 'winIGDPelayananDokterReport',
            title: 'Laporan Transaksi Jasa Pelayanan Dokter Per Pasien',
            closeAction: 'destroy',
            width:400,
            height: 300,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDPelayananDokter()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganIGD').show();
                Ext.getCmp('cboAsuransiIGD').hide();
                Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
                Ext.getCmp('cboUmumIGD').hide();
            }
        }

        }
    );

    return winIGDPelayananDokterReport;
};


function ItemDlgIGDPelayananDokter()
{
    var PnlLapIGDPelayananDokter = new Ext.Panel
    (
        {
            id: 'PnlLapIGDPelayananDokter',
            fileUpload: true,
            layout: 'form',
            //width:400,
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapIGDPelayananDokter_Atas(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapIGDPelayananDokter',
                            handler: function()
                            {
                                //if (ValidasiReportIGDPasienPerDokter() === 1)
                                //{                             
                                    var params={
                        //id : 'cbPendaftaran_IGDPelayananDokter'
                        //id : 'cbTindakIGD_IGDPelayananDokter'
                        //$('.messageCheckbox:checked').val();
                                        kd_profesi:Ext.getCmp('IDcboPilihanIGDJenisProfesi').getValue(),
                                        kd_poli:Ext.getCmp('cbounitRequestEntryIGDPelayananDokter').getValue(),
                                        pelayananPendaftaran:Ext.getCmp('cbPendaftaran_IGDPelayananDokter').getValue(),
                                        pelayananTindak:Ext.getCmp('cbTindakIGD_IGDPelayananDokter').getValue(),
                                        //kd_dokter:Ext.getCmp('cboDokterIGDPelayananDokter').getValue(),
                                        kd_dokter:GetCriteriaIGDProfesi(),
                                        kd_kelompok:GetCriteriaIGDPasienPerKelompok(),
                                        tglAwal:Ext.getCmp('dtpTglAwalLapIGDPelayananDokter').getValue(),
                                        tglAkhir:Ext.getCmp('dtpTglAkhirLapIGDPelayananDokter').getValue(),
                                    } 
                                    //console.log(params);
                                    var form = document.createElement("form");
                                    form.setAttribute("method", "post");
                                    form.setAttribute("target", "_blank");
                                    form.setAttribute("action", baseURL + "index.php/gawat_darurat/lap_IGDPelayananDokter/cetak");
                                    var hiddenField = document.createElement("input");
                                    hiddenField.setAttribute("type", "hidden");
                                    hiddenField.setAttribute("name", "data");
                                    hiddenField.setAttribute("value", Ext.encode(params));
                                    form.appendChild(hiddenField);
                                    document.body.appendChild(form);
                                    form.submit();      
                                    frmDlgIGDPasienPerKelompok.close(); 
                                    
                               //};
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapIGDPelayananDokter',
                            handler: function()
                            {
                                    frmDlgIGDPelayananDokter.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapIGDPelayananDokter;
};

function getItemLapIGDPelayananDokter_Atas()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 220,
            anchor: '100% 100%',
            items: [
            {
                xtype: 'checkboxgroup',
                width:210,
                x: 120,
                y: 10,
                items: 
                [
                    {
                        
                        boxLabel: 'Pendaftaran',
                        name: 'cbPendaftaran_IGDPelayananDokter',
                        id : 'cbPendaftaran_IGDPelayananDokter'
                    },
                    {
                        boxLabel: 'Tindak IGD',
                        name: 'cbTindakIGD_IGDPelayananDokter',
                        id : 'cbTindakIGD_IGDPelayananDokter'
                    }
               ]
            },
            //  ================================================================================== POLIKLINIK
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Unit '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mCombounitIGDPelayananDokter(),
            //  ================================================================================== DOKTER
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Bagian '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboIGDJenisProfesi(),
                mComboDokterIGDPelayananDokter(),
                mComboDokterIGDPelayananPerawat(),

            {
                x: 10,
                y: 130,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 130,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 130,
                xtype: 'datefield',
                id: 'dtpTglAwalLapIGDPelayananDokter',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 130,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 130,
                xtype: 'datefield',
                id: 'dtpTglAkhirLapIGDPelayananDokter',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 160,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 160,
                xtype: 'label',
                text: ' : '
            },
                mComboIGDPasienPerKelompok(),
                mComboIGDPasienPerKelompokSEMUA(),
                mComboIGDPasienPerKelompokPERORANGAN(),
                mComboIGDPasienPerKelompokPERUSAHAAN(),
                mComboIGDPasienPerKelompokASURANSI(),
            ]
        }]
    };
    return items;
};

function ShowPesanWarningIGDPelayananDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function mComboDokterIGDPelayananDokter()
{
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: "WHERE left(dokter_klinik.kd_unit, 1)='3' AND dokter.jenis_dokter='0'"
            }
        }
    );
    var cboPilihanIGDPelayananDokter = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboDokterIGDPelayananDokter',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanIGDPelayananDokter;
};

function mComboDokterIGDPelayananPerawat()
{
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: "WHERE dokter.jenis_dokter='1'"
            }
        }
    );
    var cboPilihanIGDPelayananDokter = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboDokterIGDPelayananPerawat',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:true,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanIGDPelayananDokter;
};


function mCombounitIGDPelayananDokter()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

    ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewComboUnitLaporan',
                    param: "left(kd_unit, 1)='3'"
                }
            }
        );

    var cbounitRequestEntryIGDPelayananDokter = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cbounitRequestEntryIGDPelayananDokter',
            width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
                        selectSetUnit =b.data.displayField;                     
                    }
                    
                }
        }
    )

    return cbounitRequestEntryIGDPelayananDokter;
};


function mComboIGDPasienPerKelompok()
{
    var cboPilihanIGDPelayananDokterkelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 160,
                id:'cboPilihanIGDPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectIGDPasienPerKelompok(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanIGDPelayananDokterkelompokPasien;
};

function mComboIGDJenisProfesi()
{
    var cboPilihanIGDJenisProfesi = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'IDcboPilihanIGDJenisProfesi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Dokter'], [2, 'Perawat']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihanProfesi=b.data.displayText;
                            Combo_SelectIGDProfesi(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanIGDJenisProfesi;
};

//IGDPasienPerKelompok
function mComboIGDPasienPerKelompokSEMUA()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganIGD = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 190,
                id:'IDmComboIGDPasienPerKelompokSEMUA',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                valueField: 'Id',
                displayField: 'displayText',
                hidden:false,
                fieldLabel: '',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                        {
                            id: 0,
                            fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                            data: [[1, 'Semua']]
                        }
                ),
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectSetUmum=b.data.displayText ;
                    }
                                    
                                
                }
            }
    );
    return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokPERORANGAN()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganIGD = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 190,
                id:'IDmComboIGDPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
    );
    return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokPERUSAHAAN()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='1' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganIGD = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 190,
                id:'IDmComboIGDPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                //value: selectsetperusahaan,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectsetperusahaan = b.data.KD_CUSTOMER;
                        selectsetnamaperusahaan = b.data.CUSTOMER;
                    }
                }
            }
    );
    return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokASURANSI()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='2' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganIGD = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 190,
                id:'IDmComboIGDPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectSetAsuransi=b.data.KD_CUSTOMER ;
                        selectsetnamaAsuransi = b.data.CUSTOMER ;
                    }
                }
            }
    );
    return cboPerseoranganIGD;
};


function Combo_SelectIGDPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').show();
   }
};

function Combo_SelectIGDProfesi(combo)
{
   var value = combo;

   if(value === "Dokter")
   {    
        Ext.getCmp('cboDokterIGDPelayananDokter').show();
        Ext.getCmp('cboDokterIGDPelayananPerawat').hide();
   }
   else if(value === "Perawat")
   {    
        Ext.getCmp('cboDokterIGDPelayananPerawat').show();
        Ext.getCmp('cboDokterIGDPelayananDokter').hide();
        
   }
   else
   {
        Ext.getCmp('cboDokterIGDPelayananDokter').show();
        Ext.getCmp('cboDokterIGDPelayananPerawat').hide();
   }
};


function GetCriteriaIGDPasienPerKelompok()
{
    var strKriteria = '';
    
    if (Ext.getCmp('cboPilihanIGDPasienPerKelompok').getValue() !== '')
    {
        if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
        else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').getValue(); } 
        else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').getValue(); } 
        else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').getValue(); }
    }else{
            strKriteria = 'Semua';
    }

    
    return strKriteria;
};

function GetCriteriaIGDProfesi()
{
    var strKriteria = '';
    
    if (Ext.getCmp('IDcboPilihanIGDJenisProfesi').getValue() !== '')
    {
        if (Ext.get('IDcboPilihanIGDJenisProfesi').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
        else if (Ext.get('IDcboPilihanIGDJenisProfesi').getValue() === 'Dokter'){ strKriteria = Ext.getCmp('cboDokterIGDPelayananDokter').getValue(); } 
        else if (Ext.get('IDcboPilihanIGDJenisProfesi').getValue() === 'Perawat'){ strKriteria = Ext.getCmp('cboDokterIGDPelayananPerawat').getValue(); } 
    }else{
            strKriteria = 'Semua';
    }

    
    return strKriteria;
};

function ValidasiReportIGDPasienPerDokter()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDPasienPerDokter').dom.value === '')
    {
        ShowPesanWarningIGDPasienPerDokterReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
        x=0;
    }
    return x;
};
