
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsPelayananDokterPerPasien;
var selectNamaPelayananDokterPerPasien;
var now = new Date();
var selectSetPerseorangan;
var frmDlgPelayananDokterPerPasien;
var varLapPelayananDokterPerPasien= ShowFormLapPelayananDokterPerPasien();
var selectSetUmum;
var selectSetkelpas;



var selectSetPilihankelompokPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapPelayananDokterPerPasien()
{
    frmDlgPelayananDokterPerPasien= fnDlgPelayananDokterPerPasien();
    frmDlgPelayananDokterPerPasien.show();
};

function fnDlgPelayananDokterPerPasien()
{
    var winPelayananDokterPerPasienReport = new Ext.Window
    (
        {
            id: 'winPelayananDokterPerPasienReport',
            title: 'Laporan Transaksi Jasa Pelayanan Dokter Per Pasien',
            closeAction: 'destroy',
            width:400,
            height: 300,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgPelayananDokterPerPasien()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganIGD').hide();
                Ext.getCmp('cboAsuransiIGD').hide();
                Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
                Ext.getCmp('cboUmumIGD').show();
            }
        }

        }
    );

    return winPelayananDokterPerPasienReport;
};


function ItemDlgPelayananDokterPerPasien()
{
    var PnlLapPelayananDokterPerPasienIGD = new Ext.Panel
    (
        {
            id: 'PnlLapPelayananDokterPerPasienIGD',
            fileUpload: true,
            layout: 'form',
			//width:400,
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapPelayananDokterPerPasien_Atas(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapPelayananDokterPerPasien',
                            handler: function()
                            {
                               /* if (ValidasiReportPelayananDokterPerPasien() === 1)
                               { */		loadMask.show();
                                        var criteria = GetCriteriaPelayananDokterPerPasien();
                                        frmDlgPelayananDokterPerPasien.close();
                                        loadlaporanIGD('0', 'rep020209', criteria, function(){
											loadMask.hide();
										});
								/* }; */
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapPelayananDokterPerPasien',
                            handler: function()
                            {
                                    frmDlgPelayananDokterPerPasien.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapPelayananDokterPerPasienIGD;
};

function getItemLapPelayananDokterPerPasien_Atas()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 220,
            anchor: '100% 100%',
            items: [
			{
				xtype: 'checkboxgroup',
				width:210,
				x: 120,
				y: 10,
				items: 
				[
					{
						
						boxLabel: 'Pendaftaran',
						name: 'cbPendaftaran_PelayananDokterPerPasienIGD',
						id : 'cbPendaftaran_PelayananDokterPerPasienIGD'
					},
					{
						boxLabel: 'Tindak IGD',
						name: 'cbTindakIGD_PelayananDokterPerPasien',
						id : 'cbTindakIGD_PelayananDokterPerPasien'
					}
			   ]
			},
			{
				x: 10,
				y: 40,
				xtype: 'label',
				text: 'Unit '
			}, {
				x: 110,
				y: 40,
				xtype: 'label',
				text: ' : '
			},
				mCombounitPelayananDokterPerPasien(),
			{
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Dokter '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
                mComboRWJJenisProfesi(),
                mComboDokterPelayananDokterPerPasien(),
                mComboDokterRWJPelayananPerawat(),
			{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, {
				x: 110,
				y: 130,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAwalLapPelayananDokterPerPasien',
				format: 'd/M/Y',
				value: tigaharilalu
			}, {
				x: 230,
				y: 130,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 255,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapPelayananDokterPerPasien',
				format: 'd/M/Y',
				value: now
			},{
				x: 10,
				y: 160,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 160,
				xtype: 'label',
				text: ' : '
			},
				mComboKelompokPasienPelayananDokterPerPasien(),
				mComboUmumPelayananDokterPerPasien(),
				mComboPerseoranganPelayananDokterPerPasien(),
				mComboAsuransiPelayananDokterPerPasien(),
				mComboPerusahaanPelayananDokterPerPasien()
            ]
        }]
    };
    return items;
};

function GetCriteriaPelayananDokterPerPasien()
{
	var strKriteria = '';
	
	if (Ext.getCmp('cbPendaftaran_PelayananDokterPerPasienIGD').getValue() === true && Ext.getCmp('cbTindakIGD_PelayananDokterPerPasien').getValue() === false){
		strKriteria ='autocas';
		strKriteria += '##@@##' + 1;
	}else if(Ext.getCmp('cbPendaftaran_PelayananDokterPerPasienIGD').getValue() === false && Ext.getCmp('cbTindakIGD_PelayananDokterPerPasien').getValue() === true) {
		strKriteria ='autocas';
		strKriteria += '##@@##' + 2;
	} else if(Ext.getCmp('cbPendaftaran_PelayananDokterPerPasienIGD').getValue() === true && Ext.getCmp('cbTindakIGD_PelayananDokterPerPasien').getValue() === true){
		strKriteria ='NoCheked';
		strKriteria += '##@@##' + 3;
	} else{
		strKriteria ='NoCheked';
		strKriteria += '##@@##' + 4;
	}
		
	if(Ext.getCmp('cbounitRequestEntryPelayananDokterPerPasienIGD').getValue() === '' || Ext.getCmp('cbounitRequestEntryPelayananDokterPerPasienIGD').getValue() === 'UGD'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + 'UGD';
	} else{
		strKriteria += '##@@##' + Ext.get('cbounitRequestEntryPelayananDokterPerPasienIGD').getValue();;
		strKriteria += '##@@##' + Ext.getCmp('cbounitRequestEntryPelayananDokterPerPasienIGD').getValue();
	}
	
	if(Ext.getCmp('cboDokterPelayananDokterPerPasienIGD').getValue() === '' || Ext.getCmp('cboDokterPelayananDokterPerPasienIGD').getValue() === 'Semua'){
		strKriteria += '##@@##' + 'Dokter';
		strKriteria += '##@@##' + 'Semua';
	} else{
		strKriteria += '##@@##' + Ext.get('cboDokterPelayananDokterPerPasienIGD').getValue();;
		strKriteria += '##@@##' + Ext.getCmp('cboDokterPelayananDokterPerPasienIGD').getValue();
	}
	
	if (Ext.get('dtpTglAwalLapPelayananDokterPerPasien').getValue() !== ''){
		strKriteria += '##@@##' + Ext.get('dtpTglAwalLapPelayananDokterPerPasien').getValue();
	}
	if (Ext.get('dtpTglAkhirLapPelayananDokterPerPasien').getValue() !== ''){
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapPelayananDokterPerPasien').getValue();
	}
	
	if (Ext.getCmp('cboPilihanPelayananDokterPerPasienkelompokPasienIGD').getValue() !== '')
	{
		if (Ext.get('cboPilihanPelayananDokterPerPasienkelompokPasienIGD').getValue() === 'Semua') {
            strKriteria += '##@@##' + 'Kelpas';
			strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
        } else if (Ext.get('cboPilihanPelayananDokterPerPasienkelompokPasienIGD').getValue() === 'Perseorangan'){

			strKriteria += '##@@##' + 'Perseorangan';
            strKriteria += '##@@##' + Ext.get('cboPerseoranganIGD').getValue();
            strKriteria += '##@@##' + Ext.getCmp('cboPerseoranganIGD').getValue();
        } else if (Ext.get('cboPilihanPelayananDokterPerPasienkelompokPasienIGD').getValue() === 'Perusahaan'){
            
			if(Ext.getCmp('cboPerusahaanRequestEntryIGD').getValue() === '')
            {
				strKriteria += '##@@##' + 'Kelpas';
				strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Perusahaan';
				strKriteria += '##@@##' + Ext.get('cboPerusahaanRequestEntryIGD').getValue();
                strKriteria += '##@@##' + Ext.getCmp('cboPerusahaanRequestEntryIGD').getValue();
            }
        } else {
            
			if(Ext.getCmp('cboAsuransiIGD').getValue() === '')
            {
				strKriteria += '##@@##' + 'Kelpas';
				strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Asuransi';
				strKriteria += '##@@##' + Ext.get('cboAsuransiIGD').getValue();
                strKriteria += '##@@##' + Ext.getCmp('cboAsuransiIGD').getValue();
            }
            
        }
	}  else{
            strKriteria += '##@@##' + 'Kelpas';
			strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
	}

	
	return strKriteria;
};

function ValidasiReportPelayananDokterPerPasien()
{
    var x=1;
    if(Ext.getCmp('dtpTglAwalLapPelayananDokterPerPasien').getValue() > Ext.getCmp('dtpTglAkhirLapPelayananDokterPerPasien').getValue())
    {
        ShowPesanWarningPelayananDokterPerPasienReportIGD('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }
	if(Ext.getCmp('cboPilihanPelayananDokterPerPasienkelompokPasienIGD').getValue() === ''){
		ShowPesanWarningPelayananDokterPerPasienReportIGD('Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	if(Ext.getCmp('cboPerusahaanRequestEntryIGD').getValue() === '' &&  Ext.getCmp('cboAsuransiIGD').getValue() === '' &&  Ext.getCmp('cboPerseoranganIGD').getValue() === ''){
		ShowPesanWarningPelayananDokterPerPasienReportIGD('Sub Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningPelayananDokterPerPasienReportIGD(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function mComboRWJJenisProfesi()
{
    var cboPilihanRWJJenisProfesi = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'IDcboPilihanRWJJenisProfesi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Dokter'], [2, 'Perawat']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihanProfesi=b.data.displayText;
                            Combo_SelectRWJProfesi(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanRWJJenisProfesi;
};

function mComboDokterRWJPelayananPerawat()
{
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: "WHERE dokter.jenis_dokter='1'"
            }
        }
    );
    var cboPilihanRWJPelayananDokter = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboDokterRWJPelayananPerawat',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:true,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanRWJPelayananDokter;
};


function mComboDokterPelayananDokterPerPasien()
{
	var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: "WHERE left(dokter_klinik.kd_unit, 1)='3' AND dokter.jenis_dokter='0'"
			}
		}
	);
    var cboPilihanPelayananDokterPerPasienIGD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboDokterPelayananDokterPerPasienIGD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanPelayananDokterPerPasienIGD;
};

function mComboKelompokPasienPelayananDokterPerPasien()
{
    var cboPilihanPelayananDokterPerPasienkelompokPasienIGD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 160,
                id:'cboPilihanPelayananDokterPerPasienkelompokPasienIGD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                value:1,
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanPelayananDokterPerPasienkelompokPasienIGD;
};

function mComboPerseoranganPelayananDokterPerPasien()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganIGD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 190,
                id:'cboPerseoranganIGD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganIGD;
};

function mComboUmumPelayananDokterPerPasien()
{
    var cboUmumIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 190,
			id:'cboUmumIGD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			valueField: 'Id',
            displayField: 'displayText',
			fieldLabel: '',
			width:240,
			value:1,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
									'Id',
									'displayText'
							],
					data: [[1, 'Semua']]
					}
			),
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumIGD;
};

function mComboPerusahaanPelayananDokterPerPasien()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='1' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerusahaanRequestEntryIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 190,
		    id: 'cboPerusahaanRequestEntryIGD',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryIGD;
};

function mComboAsuransiPelayananDokterPerPasien()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='2' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboAsuransiIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 190,
			id:'cboAsuransiIGD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiIGD;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganIGD').show();
        Ext.getCmp('cboAsuransiIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
        Ext.getCmp('cboUmumIGD').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganIGD').hide();
        Ext.getCmp('cboAsuransiIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryIGD').show();
        Ext.getCmp('cboUmumIGD').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganIGD').hide();
        Ext.getCmp('cboAsuransiIGD').show();
        Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
        Ext.getCmp('cboUmumIGD').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganIGD').hide();
        Ext.getCmp('cboAsuransiIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
        Ext.getCmp('cboUmumIGD').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganIGD').hide();
        Ext.getCmp('cboAsuransiIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
        Ext.getCmp('cboUmumIGD').show();
   }
}

function mCombounitPelayananDokterPerPasien()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewComboUnitLaporan',
                    param: "left(kd_unit, 1)='3'"
                }
            }
        );

    var cbounitRequestEntryPelayananDokterPerPasienIGD = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 40,
            id: 'cbounitRequestEntryPelayananDokterPerPasienIGD',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            width:240,
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryPelayananDokterPerPasienIGD;
};

function Combo_SelectRWJProfesi(combo)
{
   var value = combo;

   if(value === "Dokter")
   {    
        Ext.getCmp('cboDokterPelayananDokter').show();
        Ext.getCmp('cboDokterRWJPelayananPerawat').hide();
   }
   else if(value === "Perawat")
   {    
        Ext.getCmp('cboDokterRWJPelayananPerawat').show();
        Ext.getCmp('cboDokterPelayananDokter').hide();
        
   }
   else
   {
        Ext.getCmp('cboDokterPelayananDokter').show();
        Ext.getCmp('cboDokterRWJPelayananPerawat').hide();
   }
};