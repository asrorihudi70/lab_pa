
var dsIGDPendapatanKuitansi;
var selectNamaIGDPendapatanKuitansi;
var tanggal = new Date();
var now = tanggal.format('Y-m-d');
var selectCount_viInformasiUnitdokter=50;
var icons_viInformasiUnitdokter="Gaji";
var dataSource_unit;
var selectSetJenisPasien='Semua Pasien';
var frmDlgIGDPendapatanKuitansi;
var ds_customer_viDaftar;
var secondGridStore;
var varLapIGDPendapatanKuitansi= ShowFormLapIGDPendapatanKuitansi();
var selectSetUmum;
var firstGrid;
var secondGrid;
var selectSetkelpas;
var selectSetPerseorangan;
var CurrentData_viDaftarIGD =
{
	data: Object,
	details: Array,
	row: 0
};

function ShowFormLapIGDPendapatanKuitansi()
{
    frmDlgIGDPendapatanKuitansi= fnDlgIGDPendapatanKuitansi();
    frmDlgIGDPendapatanKuitansi.show();
};

function fnDlgIGDPendapatanKuitansi()
{
    var winIGDPendapatanKuitansiReport = new Ext.Window
    (
        {
            id: 'winIGDPendapatanKuitansiReport',
            title: 'Laporan Rawat Jalan',
            closeAction: 'destroy',
            width:650,
            height: 420,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [dataGrid_viInformasiUnit()],
            listeners:
			{
				activate: function()
				{
				}
			}

        }
    );

    return winIGDPendapatanKuitansiReport;
};


function ItemDlgIGDPendapatanKuitansi()
{
    var PnlLapIGDPendapatanKuitansi = new Ext.Panel
    (
        {
            id: 'PnlLapIGDPendapatanKuitansi',
            fileUpload: true,
            layout: 'form',
            height: '570',
            anchor: '99%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                dataGrid_viInformasiUnit(),

            ]
        }
        
    );

    return PnlLapIGDPendapatanKuitansi;
};

function GetCriteriaIGDPendapatanKuitansi()
{
	var strKriteria = '';

	if (Ext.getCmp('kelPasien').getValue() !== '')
	{
		if (Ext.get('kelPasien').getValue() === 'Semua') { strKriteria = 'Semua'; } 
		else if (Ext.get('kelPasien').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('cboPerseoranganRequestEntry').getValue(); } 
		else if (Ext.get('kelPasien').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('cboPerusahaanRequestEntry').getValue(); } 
		else if (Ext.get('kelPasien').getValue() === 'Asuransi'){ strKriteria = Ext.getCmp('cboAsuransiRequestEntry').getValue(); } 
	}else{
        strKriteria = 'Semua';
	}

	return strKriteria;
};

function GetShiftIGDPendapatanKuitansi()
{
	var strKriteria = '';

	if (Ext.getCmp('Shift_All').getValue() == false)
	{
		if (Ext.getCmp('Shift_3').getValue() == true){ strKriteria = '3,'+strKriteria; } 
		if (Ext.getCmp('Shift_2').getValue() == true){ strKriteria = '2,'+strKriteria; } 
		if (Ext.getCmp('Shift_1').getValue() == true) { strKriteria = '1,'+strKriteria; } 
	}else{
        strKriteria = 'Semua';
	}

	return strKriteria;
};

function ValidasiTanggalReportIGDPendapatanKuitansi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDPendapatanKuitansi').dom.value > Ext.get('dtpTglAkhirLapIGDPendapatanKuitansi').dom.value)
    {
        ShowPesanWarningIGDPendapatanKuitansiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDPendapatanKuitansiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function dataGrid_viInformasiUnit(mod_id)
{
    var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    datarefresh_viInformasiUnit()

    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
	];


	// declare the source Grid
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 200,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'KD_UNIT',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA_UNIT',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),
				listeners : {
                    afterrender : function(comp) {
                    var firstGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
   
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStore,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 200,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_UNIT',
                    title            : 'Unit',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'secondGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid.store.add(records);
                                    secondGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

	
	var FrmTabs_viInformasiUnit = new Ext.Panel
        (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
            height       : 250,
			
//		    title:  'Pilih Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					anchor: '100% 100%',
					items:
					[firstGrid]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid]
				},
                                {
                                        	columnWidth: .50,
                                            layout: 'form',
                                            border: false,
                                            labelAlign: 'right',
                                            labelWidth: 100,
                                            items:
                                            [
											{
												   xtype: 'checkbox',
												   id: 'CekLapPilihSemuaIGDDetail',
												   hideLabel:false,
												   boxLabel: 'Pilih Semua',
												   checked: false,
												   listeners: 
												   {
														check: function()
														{
														   if(Ext.getCmp('CekLapPilihSemuaIGDDetail').getValue()===true)
															{
																 firstGrid.getSelectionModel().selectAll();
															}
															else
															{
																firstGrid.getSelectionModel().clearSelections();
															}
														}
												   }
											},
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: 'Tanggal ',
                                                    id: 'dtpTglAwalLapIGDPendapatanKuitansi',
                                                    format: 'd-M-Y',
                                                    value:now,
                                                    anchor: '95%'
                                                },
												{  
													xtype: 'combo',
													fieldLabel: 'Kelompok Pasien',
													id: 'kelPasien',
													editable: false,
													store: new Ext.data.ArrayStore
														(
															{
															id: 0,
															fields:
															[
																'Id',
																'displayText'
															],
															   data: [[1, 'Semua'],[2, 'Perseorangan'], [3, 'Perusahaan'], [4, 'Asuransi']]
															}
														),
													displayField: 'displayText',
													mode: 'local',
													width: 100,
													forceSelection: true,
													triggerAction: 'all',
													emptyText: 'Pilih Salah Satu...',
													selectOnFocus: true,
													anchor: '95%',
													value:selectSetkelpas,
													listeners:
													 {
															'select': function(a, b, c)
														{
														   Combo_Select(b.data.displayText);
														   selectSetkelpas=b.data.id;
														}

													}
												},
												mComboPerusahaan(),
												mComboPerseorangan(),
												mComboAsuransi(),													
                                            ]
                                        },
                                        {
                                            columnWidth: .50,
                                            layout: 'form',
                                            border: false,
                                            labelWidth: 30,
                                            items:
                                            [
												{
                                                fieldLabel: '',
												height:25,
												border:false,
												labelSeparator: '',
												boxLabel: 'Semua Data',
                                            },
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: 's/d ',
                                                    id: 'dtpTglAkhirLapIGDLaporandetail',
                                                    format: 'd-M-Y',
                                                    value:now,
                                                    anchor: '80%'
                                                }
                                            ]
                                        }, 

		                                        {
		                                        	columnWidth: .60,
		                                            layout: 'form',
		                                            border: false,
		                                            labelAlign: 'right',
		                                            labelWidth: 100,
		                                            items:
		                                            [
		                                            	{            
													        xtype: 'checkboxgroup',
													        fieldLabel: 'Shift',
													        columns: 4,
													        vertical: false,
									                        items: [
							                                    {
							                                    boxLabel: 'Semua',checked:true,name: 'Shift_All',id : 'Shift_All',
							                                    handler: function (field, value) 
							                                    {
							                                    	if (value === true){Ext.getCmp('Shift_1').setValue(true);
							                                    	Ext.getCmp('Shift_2').setValue(true);
							                                    	Ext.getCmp('Shift_3').setValue(true);
							                                    	Ext.getCmp('Shift_1').disable();
							                                    	Ext.getCmp('Shift_2').disable();
							                                    	Ext.getCmp('Shift_3').disable();
							                                    }else{
							                                    	Ext.getCmp('Shift_1').setValue(false);
							                                    	Ext.getCmp('Shift_2').setValue(false);
							                                    	Ext.getCmp('Shift_3').setValue(false);
							                                    	Ext.getCmp('Shift_1').enable();
							                                    	Ext.getCmp('Shift_2').enable();
							                                    	Ext.getCmp('Shift_3').enable();
							                                    }
							                                }
							                            },
							                                    {boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1',id : 'Shift_1'},
							                                    {boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2',id : 'Shift_2'},
							                                    {boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3',id : 'Shift_3'}
							                                ]
													    }
													]  
												},
                                {
                                columnWidth: .99,
                                layout: 'hBox',
                                border: false,
                                defaults: { margins: '0 2 0 0' },
                                style:{'margin-left':'450px','margin-top':'0px'},
                                anchor: '100%',
                                items:
                                [
                                    
									
                                    {
                                        xtype: 'button',
                                        text: 'Ok',
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnOkLapIGDPendapatanKuitansi',
                                        handler: function()
                                        {
                                            var sendDataArray = [];
                                            secondGridStore.each(function(record){
	                                            var recordArray = [record.get("KD_UNIT")];
	                                            sendDataArray.push(recordArray);
                                            });
									
											
											if (sendDataArray.length === 0)
											{
												ShowPesanWarningIGDPendapatanKuitansiReport('Isi kriteria unit dengan drag and drop','Laporan');
										    }else{
												var params={
													kd_poli:sendDataArray,
													kd_kelompok:GetCriteriaIGDPendapatanKuitansi(),
													shift:GetShiftIGDPendapatanKuitansi(),
													tglAwal:Ext.getCmp('dtpTglAwalLapIGDPendapatanKuitansi').getValue(),
													tglAkhir:Ext.getCmp('dtpTglAkhirLapIGDLaporandetail').getValue(),
												} 
												//console.log(params);
												var form = document.createElement("form");
												form.setAttribute("method", "post");
												form.setAttribute("target", "_blank");
												form.setAttribute("action", baseURL + "index.php/gawat_darurat/lap_IGDPendapatanKuitansi/cetak");
												var hiddenField = document.createElement("input");
												hiddenField.setAttribute("type", "hidden");
												hiddenField.setAttribute("name", "data");
												hiddenField.setAttribute("value", Ext.encode(params));
												form.appendChild(hiddenField);
												document.body.appendChild(form);
												form.submit();		
												//frmDlgIGDPasienPerKelompok.close(); 

												/*frmDlgIGDPendapatanKuitansi.close();
												loadlaporanIGD('0', 'rep010201', criteria); // File ada di base/modules/main/cetakLaporanIGD*/
											};
										}
									},
                                    {
                                        xtype: 'button',
                                        text: 'Cancel' ,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnCancelLapIGDPendapatanKuitansi',
                                        handler: function()
                                        {
                                                frmDlgIGDPendapatanKuitansi.close();
                                        }
                                    }
                                    ]
                                }
			],

		
    }
    )
    return FrmTabs_viInformasiUnit;
}

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    ds_customer_viDaftar = new WebApp.DataStore({fields: Field});
    ref_combo_kelpas(1);
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: ds_customer_viDaftar,
		    valueField: 'KD_CUSTOMER',
		    hidden: true,
		    displayField: 'CUSTOMER',
			anchor: '95%',
			value: 'Semua',
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        //selectsetperusahaan = b.data.KD_CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntry;
};

function mComboPerseorangan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    ds_customer_viDaftar = new WebApp.DataStore({fields: Field});
    ref_combo_kelpas(0);
    var cboPerseoranganRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerseoranganRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Customer...',
		    fieldLabel: '',
		    align: 'Right',
		    hidden: true,
		    store: ds_customer_viDaftar,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			anchor: '95%',
			value: 'Semua',
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        //selectsetperusahaan = b.data.KD_CUSTOMER;
				}
			}
		}
	);

    return cboPerseoranganRequestEntry;
};

function mComboAsuransi()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    ds_customer_viDaftar = new WebApp.DataStore({fields: Field});
    ref_combo_kelpas(2);
    var cboAsuransiRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboAsuransiRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    hidden: true,
		    store: ds_customer_viDaftar,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			anchor: '95%',
			value: 'Semua',
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        //selectsetperusahaan = b.data.KD_CUSTOMER;
				}
			}
		}
	);

    return cboAsuransiRequestEntry;
};

var selectsetperusahaan;
var selectSetAsuransi;
function ref_combo_kelpas(jeniscus)
{
	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='"+jeniscus+"' ORDER BY customer.customer ASC"
            }
        }
    );
	
	return ds_customer_viDaftar;
}

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRequestEntry').show();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboAsuransiRequestEntry').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRequestEntry').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
        Ext.getCmp('cboAsuransiRequestEntry').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRequestEntry').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboAsuransiRequestEntry').show();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRequestEntry').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboAsuransiRequestEntry').hide();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRequestEntry').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboAsuransiRequestEntry').hide();
   }
}

function checkBox_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRequestEntry').show();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboAsuransiRequestEntry').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRequestEntry').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
        Ext.getCmp('cboAsuransiRequestEntry').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRequestEntry').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboAsuransiRequestEntry').show();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRequestEntry').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboAsuransiRequestEntry').hide();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRequestEntry').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboAsuransiRequestEntry').hide();
   }
}

function DataInputKriteria()
{
    var FrmTabs_DataInputKriteria = new Ext.Panel
        (
		{
		    id: FrmTabs_DataInputKriteria,
		    closable: true,
		    region: 'center',
		    layout: 'column',
                    height       : 100,
		    title:  'Dokter Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[]
				}
			]
		
    }
    )
    
    return FrmTabs_DataInputKriteria;
}
        

function datarefresh_viInformasiUnit()
{
    dataSource_unit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "left(kd_unit,1)='3'"
            }
        }
    )
    //alert("refersh")
}

    

	
