
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapIGDRentanTunggu;
var selectNamaLapIGDRentanTunggu;
var now = new Date();
var selectSetPerseorangan;
var frmLapIGDRentanTunggu;
var varLapIGDRentanTunggu= ShowFormLapIGDRentanTunggu();
var tglAwal;
var tglAkhir;
var tipe;
var winLapIGDRentanTungguReport;
var cboPoliklinik_LapIGDRentanTunggu;






function ShowFormLapIGDRentanTunggu()
{
    frmLapIGDRentanTunggu= fnLapIGDRentanTunggu();
    frmLapIGDRentanTunggu.show();
	loadDataComboUnitFar_LapIGDRentanTunggu();
};


function fnLapIGDRentanTunggu()
{
    winLapIGDRentanTungguReport = new Ext.Window
    (
        {
            id: 'winLapIGDRentanTungguReport',
            title: 'Laporan Rentan Tunggu',
            closeAction: 'destroy',
            width: 420,
            height: 130,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapIGDRentanTunggu()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'Ok',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapIGDRentanTunggu',
						handler: function()
						{
							
								var params={
									tglAwal:Ext.getCmp('dtpTglAwalFilter_RWJRentanTunggu').getValue(),
									tglAkhir:Ext.getCmp('dtpTglAkhirFilter_RWJRentanTunggu').getValue(),
								} 
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/gawat_darurat/lap_rentantunggu/cetakIGDRentanTunggu");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapIGDRentanTungguReport.close(); 
							
							
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapIGDRentanTunggu',
						handler: function()
						{
							winLapIGDRentanTungguReport.close();
						}
					}
			]

        }
    );

    return winLapIGDRentanTungguReport;
};


function ItemLapIGDRentanTunggu()
{
    var PnlLapIGDRentanTunggu = new Ext.Panel
    (
        {
            id: 'PnlLapIGDRentanTunggu',
            fileUpload: true,
            layout: 'form',
            height: 100,
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapIGDRentanTunggu_Atas(),
            ]
        }
    );

    return PnlLapIGDRentanTunggu;
};

function ValidasiReportLapIGDRentanTunggu()
{
	var x=1;
	if(Ext.getCmp('cboPoliklinik_LapIGDRentanTunggu').getValue() === ''){
		ShowPesanWarningLapIGDRentanTungguReport('Unit Far tidak boleh kosong','Warning');
        x=0;
	}
    return x;
};

function ShowPesanWarningLapIGDRentanTungguReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapIGDRentanTunggu_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  395,
            height: 45,
            anchor: '100% 100%',
            items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Periode '
				}, 
				{
					x: 110,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				{
					x: 120,
					y: 10,
					xtype: 'datefield',
					id: 'dtpTglAwalFilter_RWJRentanTunggu',
					format: 'd/M/Y',
					value: now
				}, 
				{
					x: 225,
					y: 10,
					xtype: 'label',
					text: ' s/d Bulan'
				}, 
				{
					x: 280,
					y: 10,
					xtype: 'datefield',
					id: 'dtpTglAkhirFilter_RWJRentanTunggu',
					format: 'd/M/Y',
					value: now,
					width: 100
				},
            ]
        }]
    };
    return items;
};


function getItemLapIGDRentanTunggu_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};