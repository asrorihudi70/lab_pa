
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsIGDRujukanRSLain;
var selectNamaIGDRujukanRSLain;
var now = new Date();
var selectSetPerseorangan;
var frmDlgIGDRujukanRSLain;
var varLapIGDRujukanRSLain= ShowFormLapIGDRujukanRSLain();
var selectSetUmum;
var selectSetkelpas;



var selectSetPilihankelompokPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapIGDRujukanRSLain()
{
    frmDlgIGDRujukanRSLain= fnDlgIGDRujukanRSLain();
    frmDlgIGDRujukanRSLain.show();
};

function fnDlgIGDRujukanRSLain()
{
    var winIGDRujukanRSLainReport = new Ext.Window
    (
        {
            id: 'winIGDRujukanRSLainReport',
            title: 'Laporan Rujukan Pasien Dari RS Lain',
            closeAction: 'destroy',
            width:400,
            height: 195,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDRujukanRSLain()],
            listeners:
			{
				activate: function()
				{
					
				}
			}

        }
    );

    return winIGDRujukanRSLainReport;
};


function ItemDlgIGDRujukanRSLain()
{
    var PnlLapIGDRujukanRSLain = new Ext.Panel
    (
        {
            id: 'PnlLapIGDRujukanRSLain',
            layout: 'form',
            height: 400,
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapIGDRujukanRSLain_Atas(),
            ],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapIGDRujukanRSLain',
					handler: function()
					{
						var kd_customer;
						if(Ext.get('cboPilihanIGDRujukanRSLainkelompokPasien').getValue() == 'Perseorangan'){
							kd_customer=Ext.getCmp('cboPerseoranganRWJ').getValue();
						} else if(Ext.get('cboPilihanIGDRujukanRSLainkelompokPasien').getValue() == 'Perusahaan'){
							kd_customer=Ext.getCmp('cboPerusahaanRequestEntryRWJ').getValue();
						} else if(Ext.get('cboPilihanIGDRujukanRSLainkelompokPasien').getValue() == 'Asuransi'){
							kd_customer=Ext.getCmp('cboAsuransiRWJ').getValue();
						} else{
							kd_customer='SEMUA';
						}
					  	var params={
							tglAwal:Ext.getCmp('dtpTglAwalLapIGDRujukanRSLain').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirLapIGDRujukanRSLain').getValue(),
							kd_unit:Ext.getCmp('cbounitRequestEntryIGDRujukanRSLain').getValue(),
							kd_customer:kd_customer,
							
						} 
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/gawat_darurat/function_lap/cetakIGDRujukanRSLain");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();	
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapIGDRujukanRSLain',
					handler: function()
					{
							frmDlgIGDRujukanRSLain.close();
					}
				}
			]
        }
    );

    return PnlLapIGDRujukanRSLain;
};

function getItemLapIGDRujukanRSLain_Atas()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 140,
            anchor: '100% 100%',
            items: [
			{
				x: 10,
				y: 10,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, 
			{
				x: 110,
				y: 10,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 10,
				xtype: 'datefield',
				id: 'dtpTglAwalLapIGDRujukanRSLain',
				format: 'd/M/Y',
				value: tigaharilalu
			}, {
				x: 230,
				y: 10,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 260,
				y: 10,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapIGDRujukanRSLain',
				format: 'd/M/Y',
				value: now,
				width: 100
			},
			{
				x: 10,
				y: 40,
				xtype: 'label',
				text: 'Instalasi '
			}, {
				x: 110,
				y: 40,
				xtype: 'label',
				text: ' : '
			},
				mCombounitIGDRujukanRSLain(),
			{
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
				mComboKelompokPasienIGDRujukanRSLain(),
				mComboUmumIGDRujukanRSLain(),
				mComboPerseoranganIGDRujukanRSLain(),
				mComboAsuransiIGDRujukanRSLain(),
				mComboPerusahaanIGDRujukanRSLain()
            ]
        }]
    };
    return items;
};

function ShowPesanWarningIGDRujukanRSLainReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function mComboKelompokPasienIGDRujukanRSLain()
{
    var cboPilihanIGDRujukanRSLainkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanIGDRujukanRSLainkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanIGDRujukanRSLainkelompokPasien;
};

function mComboPerseoranganIGDRujukanRSLain()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganRWJ',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,	
				hidden:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRWJ;
};

function mComboUmumIGDRujukanRSLain()
{
    var cboUmumRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumRWJ',
			hidden:true,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			valueField: 'Id',
            displayField: 'displayText',
            value:1,
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
									'Id',
									'displayText'
							],
					data: [[1, 'Umum']]
					}
			),
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumRWJ;
};

function mComboPerusahaanIGDRujukanRSLain()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1  order by CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryRWJ',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
			hidden:true,
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRWJ;
};

function mComboAsuransiIGDRujukanRSLain()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2  order by CUSTOMER"
            }
        }
    );
    var cboAsuransiRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			hidden:true,
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiRWJ;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRWJ').show();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').show();
        Ext.getCmp('cboUmumRWJ').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').show();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').show();
   }
}

function mCombounitIGDRujukanRSLain()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_unit='3'"
                }
            }
        );

    var cbounitRequestEntryIGDRujukanRSLain = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 40,
            id: 'cbounitRequestEntryIGDRujukanRSLain',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'Instalasi ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryIGDRujukanRSLain;
};
