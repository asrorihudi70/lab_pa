
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsIGDTagihanPerusahaan;
var selectNamaIGDTagihanPerusahaan;
var now = new Date();
var selectSetPerseorangan;
var frmDlgIGDTagihanPerusahaan;
var varLapIGDTagihanPerusahaan= ShowFormLapIGDTagihanPerusahaan();
var selectSetUmum;
var selectSetkelpas;



var selectSetPilihankelompokPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapIGDTagihanPerusahaan()
{
    frmDlgIGDTagihanPerusahaan= fnDlgIGDTagihanPerusahaan();
    frmDlgIGDTagihanPerusahaan.show();
};

function fnDlgIGDTagihanPerusahaan()
{
    var winIGDTagihanPerusahaanReport = new Ext.Window
    (
        {
            id: 'winIGDTagihanPerusahaanReport',
            title: 'Laporan Transaksi Tagihan Perusahaan',
            closeAction: 'destroy',
            width:400,
            height: 250,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDTagihanPerusahaan()]
        }
    );

    return winIGDTagihanPerusahaanReport;
};


function ItemDlgIGDTagihanPerusahaan()
{
    var PnlLapIGDTagihanPerusahaan = new Ext.Panel
    (
        {
            id: 'PnlLapIGDTagihanPerusahaan',
            fileUpload: true,
            layout: 'form',
            width:400,
            height: 250,
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapIGDTagihanPerusahaan_Atas(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapIGDTagihanPerusahaan',
                            handler: function()
                            {
                                //if (ValidasiReportIGDPasienPerDokter() === 1)
                                //{                             
                                    var params={
                        //id : 'cbPendaftaran_IGDTagihanPerusahaan'
                        //id : 'cbTindakIGD_IGDTagihanPerusahaan'
                        //$('.messageCheckbox:checked').val();
                                        kd_kelompok:GetCriteriaIGDPasienPerKelompok(),
                                        status:Ext.getCmp('cboPilihanIGDStatusPembayaran').getValue(),
                                        jenis_cust:Ext.getCmp('cboPilihanIGDPasienPerKelompok').getValue(),
                                        sort:Ext.getCmp('cboPilihanIGDUrutanSorting').getValue(),
                                        tglAwal:Ext.getCmp('dtpTglAwalLapIGDTagihanPerusahaan').getValue(),
                                        tglAkhir:Ext.getCmp('dtpTglAkhirLapIGDTagihanPerusahaan').getValue(),
                                    } 
                                    //console.log(params);
                                    var form = document.createElement("form");
                                    form.setAttribute("method", "post");
                                    form.setAttribute("target", "_blank");
                                    form.setAttribute("action", baseURL + "index.php/gawat_darurat/lap_IGDTagihanPerusahaan/cetak");
                                    var hiddenField = document.createElement("input");
                                    hiddenField.setAttribute("type", "hidden");
                                    hiddenField.setAttribute("name", "data");
                                    hiddenField.setAttribute("value", Ext.encode(params));
                                    form.appendChild(hiddenField);
                                    document.body.appendChild(form);
                                    form.submit();      
                                    frmDlgIGDPasienPerKelompok.close(); 
                                    
                               //};
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapIGDTagihanPerusahaan',
                            handler: function()
                            {
                                    frmDlgIGDTagihanPerusahaan.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapIGDTagihanPerusahaan;
};

function getItemLapIGDTagihanPerusahaan_Atas()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 170,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalLapIGDTagihanPerusahaan',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 10,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAkhirLapIGDTagihanPerusahaan',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboIGDPasienPerKelompok(),
                mComboIGDPasienPerKelompokSEMUA(),
                mComboIGDPasienPerKelompokPERORANGAN(),
                mComboIGDPasienPerKelompokPERUSAHAAN(),
                mComboIGDPasienPerKelompokASURANSI(),
            {
                x: 10,
                y: 100,
                xtype: 'label',
                text: 'Status Pembayaran '
            }, {
                x: 110,
                y: 100,
                xtype: 'label',
                text: ' : '
            },
                mComboIGDStatusPembayaran(),
            {
                x: 10,
                y: 130,
                xtype: 'label',
                text: 'Sorting Berdasarkan '
            }, {
                x: 110,
                y: 130,
                xtype: 'label',
                text: ' : '
            },
                mComboIGDUrutanSorting(),
            ]
        }]
    };
    return items;
};

function ShowPesanWarningIGDTagihanPerusahaanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function mComboDokterIGDTagihanPerusahaan()
{
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: "left(p.kd_unit, 1)='2'"
            }
        }
    );
    var cboPilihanIGDTagihanPerusahaan = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'cboDokterIGDTagihanPerusahaan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanIGDTagihanPerusahaan;
};


function mCombounitIGDTagihanPerusahaan()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

    ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewComboUnitLaporan',
                    param: "left(kd_unit, 1)='2'"
                }
            }
        );

    var cbounitRequestEntryIGDTagihanPerusahaan = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cbounitRequestEntryIGDTagihanPerusahaan',
            width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
                        selectSetUnit =b.data.displayField;                     
                    }
                    
                }
        }
    )

    return cbounitRequestEntryIGDTagihanPerusahaan;
};


function mComboIGDPasienPerKelompok()
{
    var cboPilihanIGDTagihanPerusahaankelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 40,
                id:'cboPilihanIGDPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perusahaan'], [3, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectIGDPasienPerKelompok(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanIGDTagihanPerusahaankelompokPasien;
};

function mComboIGDStatusPembayaran()
{
    var cboPilihanIGDTagihanPerusahaankelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboPilihanIGDStatusPembayaran',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Status...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Lunas'], [3, 'Belum Lunas']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                    }
                }
            }
    );
    return cboPilihanIGDTagihanPerusahaankelompokPasien;
};

function mComboIGDUrutanSorting()
{
    var cboPilihanIGDTagihanPerusahaankelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 130,
                id:'cboPilihanIGDUrutanSorting',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Status...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'No Medic'], [2, 'Nama Pasien'], [3, 'Tanggal Transaksi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                    }
                }
            }
    );
    return cboPilihanIGDTagihanPerusahaankelompokPasien;
};

//IGDPasienPerKelompok
function mComboIGDPasienPerKelompokSEMUA()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganIGD = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'IDmComboIGDPasienPerKelompokSEMUA',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                valueField: 'Id',
                displayField: 'displayText',
                hidden:false,
                fieldLabel: '',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                        {
                            id: 0,
                            fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                            data: [[1, 'Semua']]
                        }
                ),
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectSetUmum=b.data.displayText ;
                    }
                                    
                                
                }
            }
    );
    return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokPERORANGAN()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganIGD = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'IDmComboIGDPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
    );
    return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokPERUSAHAAN()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='1' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganIGD = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'IDmComboIGDPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                //value: selectsetperusahaan,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectsetperusahaan = b.data.KD_CUSTOMER;
                        selectsetnamaperusahaan = b.data.CUSTOMER;
                    }
                }
            }
    );
    return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokASURANSI()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='2' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganIGD = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'IDmComboIGDPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectSetAsuransi=b.data.KD_CUSTOMER ;
                        selectsetnamaAsuransi = b.data.CUSTOMER ;
                    }
                }
            }
    );
    return cboPerseoranganIGD;
};


function Combo_SelectIGDPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').show();
   }
};


function GetCriteriaIGDPasienPerKelompok()
{
    var strKriteria = '';
    
    if (Ext.getCmp('cboPilihanIGDPasienPerKelompok').getValue() !== '')
    {
        if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
        else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').getValue(); } 
        else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').getValue(); } 
        else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').getValue(); }
    }else{
            strKriteria = 'Semua';
    }

    
    return strKriteria;
};

function ValidasiReportIGDPasienPerDokter()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDPasienPerDokter').dom.value === '')
    {
        ShowPesanWarningIGDPasienPerDokterReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
        x=0;
    }
    return x;
};
