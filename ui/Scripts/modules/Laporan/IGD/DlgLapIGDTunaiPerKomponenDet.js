var fields = [
	{name: 'KD_UNIT', mapping : 'KD_UNIT'},
	{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];

var fieldsPayment = [
	{name: 'KD_PAY', mapping : 'KD_PAY'},
	{name: 'URAIAN', mapping : 'URAIAN'}
];

var cols = [
	{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
	{ header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
];

var colsPayment = [
	{ id : 'KD_PAY', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'KD_PAY',hidden : true},
	{ header: "Kode Pay", width: 50, sortable: true, dataIndex: 'URAIAN'}
];

var secondGridStoreLapPenerimaan;
var secondGridStoreLapPenerimaanPaymentLapPenerimaan;
var DlgLapIGDPenerimaan={
	vars:{
		comboSelect:null
	},
	coba:function(){
		var $this=this;
		$this.DateField.startDate
	},
	ArrayStore:{
		dokter:new Ext.data.ArrayStore({fields:[]})
	},
	CheckboxGroup:{
		shift:null,
		tindakan:null,
	},
	ComboBox:{
		kelPasien1:null,
		combo1:null,
		combo2:null,
		combo3:null,
		combo0:null,
		poliklinik:null,
		dokter:null
	},
	DataStore:{
		combo2:null,
		combo3:null,
		poliklinik:null,
		combo1:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	comboOnSelect:function(val){
		var $this=this;
		$this.ComboBox.combo0.hide();
		$this.ComboBox.combo1.hide();
		$this.ComboBox.combo2.hide();
		$this.ComboBox.combo3.hide();
		if(val==-1){
			$this.ComboBox.combo0.show();
			
		}else if(val==0){
			$this.ComboBox.combo1.show();
		}else if(val==1){
			$this.ComboBox.combo2.show();
		}else if(val==2){
			$this.ComboBox.combo3.show();
		}
	},
	initPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		var sendDataArrayUnit    = [];
		var sendDataTmpUnit    	 = "";
		var sendDataArraypayment = [];
		var sendDataTmpPayment 	 = "";

		secondGridStoreLapPenerimaan.each(function(record){
			var recordArray   = [record.get("KD_UNIT")];
			sendDataTmpUnit += "'"+recordArray+"',";
			sendDataArrayUnit.push(recordArray);
		});
		sendDataTmpUnit = sendDataTmpUnit.substring(0, sendDataTmpUnit.length - 1);

		secondGridStoreLapPenerimaanPaymentLapPenerimaan.each(function(record){
			var recordArraypay= [record.get("KD_PAY")];
			sendDataTmpPayment += "'"+recordArraypay+"',";
			sendDataArraypayment.push(recordArraypay);
		});
		sendDataTmpPayment = sendDataTmpPayment.substring(0, sendDataTmpPayment.length - 1);

		if (sendDataArrayUnit.length === 0)
		{  
			this.messageBox('Peringatan','Isi kriteria unit dengan drag and drop','WARNING');
			loadMask.hide();
		}else if (sendDataArraypayment.length === 0){  
			this.messageBox('Peringatan','Isi cara membayar dengan drag and drop','WARNING');
			loadMask.hide();
		}else{
			params['type_file'] = Ext.getCmp('type_file_igd_penerimaanjenispenerimaan').getValue();
			params['start_date'] = timestimetodate($this.DateField.startDate.getValue());
			params['last_date']  = timestimetodate($this.DateField.endDate.getValue());
			params['pasien']     = $this.ComboBox.kelPasien1.getValue();
			var pasien=parseInt($this.ComboBox.kelPasien1.getValue());
			if(pasien>=0){
				if(pasien==0){
					params['kd_customer']=$this.ComboBox.combo1.getValue();
				}else if(pasien==1){
					params['kd_customer']=$this.ComboBox.combo2.getValue();
				}else if(pasien==2){
					params['kd_customer']=$this.ComboBox.combo3.getValue();
				}
			}else{
				params['kd_customer']="Semua";
			}
			params['tmp_kd_unit'] 	= sendDataTmpUnit;
			params['tmp_payment'] 	= sendDataTmpPayment;
			var shift    =$this.CheckboxGroup.shift.items.items;
			var tindakan =$this.CheckboxGroup.tindakan.items.items;
			var shifta=false;
			var tindakan_stat=false;
			for(var i=0;i<shift.length ; i++){
				params['shift'+i]=shift[i].checked;
				if(shift[i].checked==true)shifta=true;
			}
			for(var i=0;i<tindakan.length ; i++){
				params['tindakan'+i]=tindakan[i].checked;
				if(tindakan[i].checked==true)tindakan_stat=true;
			}
			if(shifta==true && tindakan_stat==true){
				console.log(params);
				var form = document.createElement("form");
				form.setAttribute("method", "post");
				form.setAttribute("target", "_blank");
				form.setAttribute("action", baseURL + "index.php/gawat_darurat/function_lap/laporan_tunai_per_komponen_det");
				var hiddenField = document.createElement("input");
				hiddenField.setAttribute("type", "hidden");
				hiddenField.setAttribute("name", "data");
				hiddenField.setAttribute("value", Ext.encode(params));
				form.appendChild(hiddenField);
				document.body.appendChild(form);
				form.submit();	
				loadMask.hide();
			}else{
				loadMask.hide();
				if (shifta == false){
					Ext.Msg.alert('Gagal','Pilih Shift ');
				}else{
					Ext.Msg.alert('Gagal','Pilih Tindakan ');
				}
			}
		} 
		
	},
	messageBox:function(modul, str, icon){
		Ext.MessageBox.show
		(
			{
				title: modul,
				msg:str,
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.icon,
				width:300
			}
		);
	},
	getDokter:function(){
		var $this=this;
		$this.ComboBox.dokter = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.ArrayStore.dokter,
            width: 200,
            value:'Semua',
            valueField: 'kd_dokter',
            displayField: 'nama',
            listeners:{
            	'select': function(a, b, c){
                               
            	}
            }
        });		    
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gawat_darurat/lap_penerimaan/getDokter",
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.dokter.loadData([],false);
					for(var i=0,iLen=r.data.length; i<iLen ;i++){
						$this.ArrayStore.dokter.add(new $this.ArrayStore.dokter.recordType(r.data[i]));
					}
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
		return $this.ComboBox.dokter;
	},
	getPoliklinik:function(){
		var $this=this;
		var Field = ['KD_UNIT','NAMA_UNIT'];
		$this.DataStore.poliklinik = new WebApp.DataStore({fields: Field});
		$this.DataStore.poliklinik.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=3 and type=0"
            }
        });
		$this.ComboBox.poliklinik = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.DataStore.poliklinik,
            width: 200,
            value:'Semua',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            listeners:{
            	'select': function(a, b, c){
                               
            	}
            }
        });		    
	    return $this.ComboBox.poliklinik;
	},
	getCombo0:function(){
		var $this=this;
		$this.ComboBox.combo0 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:250,
			store: new Ext.data.ArrayStore({
				id: 0,
				fields:['Id','displayText'],
				data: [[1, 'Semua']]
			}),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			disabled:true,
			listeners:{
				'select': function(a,b,c){
					selectSetUmum=b.data.displayText ;
				}
			}
		});
		return $this.ComboBox.combo0;
	},
	getCombo1:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo1 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo1.load({
    		params:{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 '
			}
		});
    	$this.ComboBox.combo1 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hidden:true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:250,
			store: $this.DataStore.combo1,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
			listeners:{
				'select': function(a,b,c){
				}
			}
		});
		return $this.ComboBox.combo1;
	},
	getCombo2:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo2 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo2.load({
		    params:{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 '
			}
		});
   		$this.ComboBox.combo2 = new Ext.form.ComboBox({
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    hidden:true,
		    store: $this.DataStore.combo2,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			value:'Semua',
			width:250,
		    listeners:{
			    'select': function(a,b,c){
				}
			}
		});
    	return $this.ComboBox.combo2;
	},
	seconGridPenerimaan : function(){
		
		var secondGrid;
		secondGridStoreLapPenerimaan = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroup',
					store            : secondGridStoreLapPenerimaan,
					columns          : cols,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 150,
					stripeRows       : true,
					autoExpandColumn : 'KD_UNIT',
					title            : 'Unit yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroup',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGrid.store.add(records);
											secondGrid.store.sort('KD_UNIT', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGrid;
	},
	firstGridPenerimaan : function(){
		var firstGrid;
		var dataSource_unit;
		var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
		dataSource_unit         = new WebApp.DataStore({fields: Field_poli_viDaftar});
		dataSource_unit.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'NAMA_UNIT',
					Sortdir: 'ASC',
					target:'ViewSetupUnit',
					param: "kd_bagian='3' and type=0 and parent='0'"
				}
			}
		);
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 150,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'KD_UNIT',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA_UNIT',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
			listeners : {
				afterrender : function(comp) {
					var secondGridDropTargetEl = firstGrid.getView().scroller.dom;
					var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroup',
							notifyDrop : function(ddSource, e, data){
									var records =  ddSource.dragData.selections;
									Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
									firstGrid.store.add(records);
									firstGrid.store.sort('KD_UNIT', 'ASC');
									return true
							}
					});
				}
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid;
	},
	firstGridPayment : function(){
		var firstGridPayment;
		var dataSource_payment;
		var Field_payment = ['KD_PAY','URAIAN'];
		dataSource_payment         = new WebApp.DataStore({fields: Field_payment});
		dataSource_payment.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_PAY',
					Sortdir: 'ASC',
					target:'ViewListPayment',
					param: " "
				}
			}
		);
            firstGridPayment = new Ext.grid.GridPanel({
				ddGroup          : 'secondGridDDGroupPayment',
				store            : dataSource_payment,
				autoScroll       : true,
				columnLines      : true,
				border           : true,
				enableDragDrop   : true,
				height           : 150,
				stripeRows       : true,
				trackMouseOver   : true,
				title            : 'Payment',
				anchor           : '100% 100%',
				plugins          : [new Ext.ux.grid.FilterRow()],
				colModel         : new Ext.grid.ColumnModel
				(
					[
					new Ext.grid.RowNumberer(),
						{
							id: 'colKD_pay',
							header: 'Kode Pay',
							dataIndex: 'KD_PAY',
							sortable: true,
							hidden : true
						},
						{
							id: 'colKD_uraian',
							header: 'Uraian',
							dataIndex: 'URAIAN',
							sortable: true,
							width: 50
						}
					]
				),   
				listeners : {
					afterrender : function(comp) {
						var secondGridDropTargetEl = firstGridPayment.getView().scroller.dom;
						var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
								ddGroup    : 'firstGridDDGroupPayment',
								notifyDrop : function(ddSource, e, data){
										var records =  ddSource.dragData.selections;
										Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
										firstGridPayment.store.add(records);
										firstGridPayment.store.sort('KD_PAY', 'ASC');
										return true
								}
						});
					}
	            },
	                viewConfig: 
	                    {
	                            forceFit: true
	                    }
        	});
        return firstGridPayment;
	},
	seconGridPayment : function(){
		//var secondGridStoreLapPenerimaanPaymentLapPenerimaan;
		var secondGridPayment;
		secondGridStoreLapPenerimaanPaymentLapPenerimaan = new Ext.data.JsonStore({
            fields : fieldsPayment,
            root   : 'records'
        });

        // create the destination Grid
            secondGridPayment = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroupPayment',
					store            : secondGridStoreLapPenerimaanPaymentLapPenerimaan,
					columns          : colsPayment,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 150,
					stripeRows       : true,
					autoExpandColumn : 'KD_PAY',
					title            : 'Payment yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGridPayment.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroupPayment',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGridPayment.store.add(records);
											secondGridPayment.store.sort('KD_PAY', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGridPayment;
	},
	getCombo3:function(){
		var $this=this;
		var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    	$this.DataStore.combo3 = new WebApp.DataStore({fields: Field_poli_viDaftar});

		$this.DataStore.combo3.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 "
            }
        });
    	$this.ComboBox.combo3 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:250,
			hidden:true,
			store: $this.DataStore.combo3,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
//			value: 0,
			listeners:{
				'select': function(a,b,c){
//					selectSetAsuransi=b.data.KD_CUSTOMER ;
//					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		});
		return $this.ComboBox.combo3;
	},
	init:function(){
		var $this=this;
		$this.Window.main=new Ext.Window({
			width: 600,
			height:650,
			modal:true,
			title:'Laporan Penerimaan Per Komponen - Detail',
			layout:'fit',
			items:[
				new Ext.Panel({
					bodyStyle:'padding: 10px',
					layout:'form',
					border:false,
           			autoScroll       : true,
					fbar:[
						new Ext.Button({
							text:'Ok',
							handler:function(){
								$this.initPrint()
							}
						}),
						new Ext.Button({
							text:'Batal',
							handler:function(){
								$this.Window.main.close();
							}
						})
					],
					items:[
						{
							xtype : 'fieldset',
							title : 'Poliklinik',
							layout:'column',
							border:true,
							height: 200,
							bodyStyle:'margin-top: 5px',
							items:[
								{
									border:false,
									columnWidth:.45,
									// bodyStyle:'margin-top: 5px',
									items:[
										$this.firstGridPenerimaan(),
									]
								},
								{
									border:false,
									columnWidth:.1,
									bodyStyle:'margin-top: 5px',
								},
								{
									border:false,
									columnWidth:.45,
									// bodyStyle:'margin-top: 5px; align:right;',
									items:[
										$this.seconGridPenerimaan(),
									]
								}
							]
						},{
							xtype : 'fieldset',
							title : 'Tindakan',
							layout: 'column',
							border: true,
							// bodyStyle:'margin-top: 5px',
							items:[
								$this.CheckboxGroup.tindakan=new Ext.form.CheckboxGroup({
			                        xtype: 'checkboxgroup',
			                        items: [
										{boxLabel: 'Pendaftaran',checked:false, name: 'tindakan_1',id : 'tindakan_1'},
										{boxLabel: 'Tindakan IRD',checked:false, name: 'tindakan_2',id : 'tindakan_2'},
										{boxLabel: 'Tindakan Transfer',checked:false, name: 'tindakan_3',id : 'tindakan_3'}
	                                ]
                                })
							]
						},{
							xtype : 'fieldset',
							title : 'Periode',
							layout: 'column',
							border: true,
							items:[
								{
									xtype:'displayfield',
									width: 100,
									value:'Periode : '
								},
								$this.DateField.startDate=new Ext.form.DateField({
									value: new Date(),
									width: 165,
									format:'d/M/Y'
								}),
								{
									xtype:'displayfield',
									width: 30,
									value:'&nbsp;s/d&nbsp;'
								},
								$this.DateField.endDate=new Ext.form.DateField({
									value: new Date(),
									width: 165,
									format:'d/M/Y'
								})
							]
						},{
							xtype : 'fieldset',
							title : 'Kelompok pasien',
							layout: 'column',
							border: true,
							// bodyStyle:'margin-top: 5px',
							items:[
								{
									xtype:'displayfield',
									width: 100,
									value:'Kelompok : '
								},
								$this.ComboBox.kelPasien1=new Ext.form.ComboBox({
					                triggerAction: 'all',
					                lazyRender:true,
					                mode: 'local',
					                width: 100,
					                selectOnFocus:true,
					                forceSelection: true,
					                emptyText:'Silahkan Pilih...',
					                fieldLabel: 'Pendaftaran Per Shift ',
					                store: new Ext.data.ArrayStore({
			                            id: 0,
			                            fields:[
			                                    'Id',
			                                    'displayText'
			                            ],
					                    data: [[-1, 'Semua'],[0, 'Perseorangan'],[1, 'Perusahaan'], [2, 'Asuransi']]
				                    }),
					                valueField: 'Id',
					                displayField: 'displayText',
					                value:'Semua',
					                listeners:{
					                    'select': function(a,b,c){
					                            $this.vars.comboSelect=b.data.displayText;
					                            $this.comboOnSelect(b.data.Id);
					                    }
					                }
				           		}),
				           		{
									xtype:'displayfield',
									width: 10,
									value:'&nbsp;'
								},
				           		$this.getCombo0(),
				           		$this.getCombo1(),
				           		$this.getCombo2(),
				           		$this.getCombo3()
							]
						},
						/*{
							xtype : 'fieldset',
							title : 'Dokter',
							layout: 'column',
							border: true,
							bodyStyle:'margin-top: 5px',
							items:[
								{
									xtype:'displayfield',
									width: 100,
									value:'Dokter : '
								},
								$this.getDokter()
							]
						},*/{
							xtype : 'fieldset',
							//title : 'Shift',
							layout:'column',
							border: true,
							// bodyStyle:'margin-top: 5px',
							items:[
								$this.CheckboxGroup.shift=new Ext.form.CheckboxGroup({
			                        xtype: 'checkboxgroup',
			                        items: [
	                                    {boxLabel: 'Semua',checked:true,name: 'Shift_All_LabRegis',id : 'Shift_All_LabRegis',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_LabRegis').setValue(true);Ext.getCmp('Shift_2_LabRegis').setValue(true);Ext.getCmp('Shift_3_LabRegis').setValue(true);Ext.getCmp('Shift_1_LabRegis').disable();Ext.getCmp('Shift_2_LabRegis').disable();Ext.getCmp('Shift_3_LabRegis').disable();}else{Ext.getCmp('Shift_1_LabRegis').setValue(false);Ext.getCmp('Shift_2_LabRegis').setValue(false);Ext.getCmp('Shift_3_LabRegis').setValue(false);Ext.getCmp('Shift_1_LabRegis').enable();Ext.getCmp('Shift_2_LabRegis').enable();Ext.getCmp('Shift_3_LabRegis').enable();}}},
	                                    {boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1_LabRegis',id : 'Shift_1_LabRegis'},
	                                    {boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2_LabRegis',id : 'Shift_2_LabRegis'},
	                                    {boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3_LabRegis',id : 'Shift_3_LabRegis'},
										{boxLabel: 'Excel',checked:false,name: 'type_file_igd_penerimaanjenispenerimaan',id : 'type_file_igd_penerimaanjenispenerimaan'},
	                                ]
                                })
							]
						},
						{
							xtype : 'fieldset',
							title : 'Payment',
							layout:'column',
							border:true,
							height: 200,
							// bodyStyle:'margin-top: 5px',
							items:[
								{
									border:false,
									columnWidth:.45,
									// bodyStyle:'margin-top: 5px',
									items:[
										$this.firstGridPayment(),
									]
								},
								{
									border:false,
									columnWidth:.1,
									bodyStyle:'margin-top: 5px',
								},
								{
									border:false,
									columnWidth:.45,
									// bodyStyle:'margin-top: 5px; align:right;',
									items:[
										$this.seconGridPayment(),
									]
								}
							]
						},
					]
				})
			]
		}).show();
	}
};
DlgLapIGDPenerimaan.init();
