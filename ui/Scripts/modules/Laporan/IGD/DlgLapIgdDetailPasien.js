
var dsIGDDetailPasien;
var selectNamaIGDDetailPasien;
var now = new Date();
var selectCount_viInformasiUnitdokter=50;
var icons_viInformasiUnitdokter="Gaji";
var dataSource_unit;
var selectSetJenisPasien='Semua Pasien';
var frmDlgIGDDetailPasien;
var secondGridStore;
var varLapIGDDetailPasien= ShowFormLapIGDDetailPasien();
var selectSetUmum;

var CurrentData_viDaftarIGD =
{
	data: Object,
	details: Array,
	row: 0
};

function ShowFormLapIGDDetailPasien()
{
    frmDlgIGDDetailPasien= fnDlgIGDDetailPasien();
    frmDlgIGDDetailPasien.show();
};

function fnDlgIGDDetailPasien()
{
    var winIGDDetailPasienReport = new Ext.Window
    (
        {
            id: 'winIGDDetailPasienReport',
            title: 'Laporan Rawat Inap',
            closeAction: 'destroy',
            width:500,
            height: 420,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [dataGrid_viSummeryPasien()],
            listeners:
					{
						activate: function()
						{
			//                Ext.getCmp('cboJenisPasien').show();
			//                Ext.getCmp('cboAsuransi').hide();
			//                Ext.getCmp('cboPerusahaanRequestEntry').hide();
			//                Ext.getCmp('cboUmum').hide();
			//                Ext.getCmp('dtpTglAkhirLapIGDKIUP').hide();
						}
					}

        }
    );

    return winIGDDetailPasienReport;
};


function GetCriteriaIGDDetailPasien()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapIGDDetailPasien').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapIGDDetailPasien').getValue();
	};
        if (Ext.get('dtpTglAkhirLapIGDRujukan').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapIGDRujukan').getValue();
	};
      if (Ext.get('rb_spesialisasi').dom.checked === true )
	{
	strKriteria += '##@@##' + 'spesialis';
		//strKriteria += '##@@##' + 'Rujukan';
       //    strKriteria += '##@@##' + Ext.getCmp('cboRujukanRequestEntry').getValue();
	};
	      if (Ext.get('rb_ruang').dom.checked === true )
	{
	strKriteria += '##@@##' + 'ruang';
		//strKriteria += '##@@##' + 'Rujukan';
       //    strKriteria += '##@@##' + Ext.getCmp('cboRujukanRequestEntry').getValue();
	};
       
	         if (Ext.get('rb_kelas').dom.checked === true )
	{
	strKriteria += '##@@##' + 'kelas';
		//strKriteria += '##@@##' + 'Rujukan';
       //    strKriteria += '##@@##' + Ext.getCmp('cboRujukanRequestEntry').getValue();
	};
       
       
      
	
	return strKriteria;
};

function ValidasiTanggalReportIGDDetailPasien()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDDetailPasien').dom.value > Ext.get('dtpTglAkhirLapIGDDetailPasien').dom.value)
    {
        ShowPesanWarningIGDDetailPasienReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDDetailPasienReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function mComboJenisPasien()
{
    var cboJenisPasien = new Ext.form.ComboBox
	(
		{
			id:'cboJenisPasien',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Jenis Pasien ',
			width:50,
                        anchor: '100%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua Pasien'],[2, 'Pasien Baru'],[3, 'Pasien Lama']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
//			value:selectSetJenisPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetJenisPasien=b.data.displayText ;
				}
			}
		}
	);
	return cboJenisPasien;
};

function mcomborujukandari()
{
    var Field = ['CARA_PENERIMAAN','PENERIMAAN'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewComboRujukanDari',
                param: ""
            }
        }
    )

    var cboRujukanDariRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanDariRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Rujukan...',
            fieldLabel: 'Rujukan Dari ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'CARA_PENERIMAAN',
            displayField: 'PENERIMAAN',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   loaddatastorerujukan(b.data.CARA_PENERIMAAN)
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }


		}
        }
    )

    return cboRujukanDariRequestEntry;
}
function mComboJenisPasien()
{
    var cboJenisPasien = new Ext.form.ComboBox
	(
		{
			id:'cboJenisPasien',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Jenis Pasien ',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua Pasien'],[2, 'Pasien Baru'],[3, 'Pasien Lama']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
//			value:selectSetJenisPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetJenisPasien=b.data.displayText ;
				}
			}
		}
	);
	return cboJenisPasien;
};
function loaddatastorerujukan(cara_penerimaan)
{
          dsRujukanRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    Sort: 'rujukan',
			    Sortdir: 'ASC',
			    target: 'ViewComboRujukan',
			    param: 'cara_penerimaan=~'+ cara_penerimaan+ '~'
			}
                    }
                )
}

function mComboRujukan()
{
    var Field = ['KD_RUJUKAN','RUJUKAN'];

    dsRujukanRequestEntry = new WebApp.DataStore({fields: Field});


    var cboRujukanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboRujukanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Rujukan...',
		    fieldLabel: '',
		    align: 'Right',
                     
//		    anchor:'60%',
		    store: dsRujukanRequestEntry,
		    valueField: 'KD_RUJUKAN',
		    displayField: 'RUJUKAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboRujukanRequestEntry;
};

function dataGrid_viSummeryPasien(mod_id)
{
    var Field_poli_viDaftar = ['kd_unit','nama_unit'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    datarefresh_viInformasiUnitSpesial('3');

    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'kd_unit', mapping : 'kd_unit'},
		{name: 'nama_unit', mapping : 'nama_unit'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'kd_unit', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'kd_unit',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'nama_unit'}
	];


	// declare the source Grid
        var firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 200,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'kd_unit',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'nama_unit',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),
    //                listeners : {
    //                afterrender : function(comp) {
    //                var firstGridDropTargetEl =  firstGrid.getView().scroller.dom;
    //                var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
    //                        ddGroup    : 'firstGridDDGroup',
    //                        notifyDrop : function(ddSource, e, data){
    //                                var records =  ddSource.dragData.selections;
    //                                Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
    //                                firstGrid.store.add(records);
    //                                firstGrid.store.sort('kd_unit', 'ASC');
    //                                return true
    //                        }
    //                });
    //                }
    //            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
        var secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStore,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 200,
                    stripeRows       : true,
                    autoExpandColumn : 'kd_unit',
                    title            : 'Unit',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'secondGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid.store.add(records);
                                    secondGrid.store.sort('kd_unit', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

	
	var FrmTabs_viInformasiUnitdokter = new Ext.Panel
        (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
            height       : 400,
//		    title:  'Pilih Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 5px 5px 5px 5px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '5 5 5 5',
		    anchor: '99%',
		    iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
                                {
					columnWidth: .97,
					layout: 'form',
					border: true,
					autoScroll: true,
					bodyStyle: 'padding: 5px 5px 5px 5px',
                                        labelAlign: 'right',
                                        labelWidth: 50,
					items:
					[
                                            {
                                                xtype: 'radiogroup',
                                                id: 'rbrujukan',
                                                fieldLabel: '',
                                                items: 
                                                [
                                                    
                                                    {
                                                        boxLabel: 'Spesialisasi',
                                                        name: 'rb_auto',
                                                        id: 'rb_spesialisasi',
                                                        inputValue: '1'
                                                    },
                                                    {
                                                        boxLabel: 'Kelas',
                                                        name: 'rb_auto',
                                                        id: 'rb_kelas',
                                                        inputValue: '2'
                                                    },
                                                    {
                                                        boxLabel: 'Ruang / Unit',
                                                        name: 'rb_auto',
                                                        id: 'rb_ruang',
                                                        checked: true,
                                                        inputValue: '3'
                                                    }
                                                ],
                                                listeners: {
                                                change: function(field, newValue, oldValue) {
                                                    
                                                    var value = newValue.inputValue;
                                                    if (value !== ' ') {
                                                        datarefresh_viInformasiUnitSpesial(value);
                                                        secondGridStore.removeAll();
                                                    }
                                                }
                                            }
                                            }
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[
					
					firstGrid
						
					]
				},
				{
					columnWidth: .47,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[
					secondGrid
						
					]
				},
                                {
                                        columnWidth: .9999,
										layout: 'absolute',
                                        bodyStyle: 'padding: 10px 10px 10px 10px',
										border: false,
                                        width: 800,
                                        height: 30,
										anchor: '100% 100%',
										items:
										[
																{
															
																	xtype: 'button',
																	text: 'Reset',
																  
																	anchor:'100%',
																	id: 'buttonreset',
																	handler: function()
																	{
																		datarefresh_viInformasiUnitSpesial('3');
																		secondGridStore.removeAll();
																	}
																}
										]
															
        
                                },
                                {
                                        
                                            columnWidth: .50,
                                            layout: 'form',
                                            border: false,
                                            labelAlign: 'right',
                                            labelWidth: 100,
                                            items:
                                            [
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: nmPeriodeDlgRpt + ' ',
                                                    id: 'dtpTglAwalLapIGDDetailPasien',
                                                    format: 'd/M/Y',
                                                    value:now,
                                                    anchor: '95%'
                                                },

                                            ]
                                        },
                                        {
                                            columnWidth: .46,
                                            layout: 'form',
                                            border: false,
                                            labelWidth: 30,
                                            items:
                                            [
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: nmSd + ' ',
                                                    id: 'dtpTglAkhirLapIGDRujukan',
                                                    format: 'd/M/Y',
                                                    value:now,
                                                    anchor: '98%'
                                                }
                                            ]
                                        },
                                        {
                                            columnWidth:1,
                                            layout: 'form',
                                            border: false,
                                            labelAlign: 'right',
                                            labelWidth: 100,
                                            items:
                                            [
                                           mComboJenisPasien()
//                                            
                                            ]

                                        },
                                        
                                {
                                columnWidth: .9999,
                                layout: 'hBox',
                                border: false,
                                defaults: { margins: '0 5 0 0' },
                                style:{'margin-left':'30px','margin-top':'5px'},
                                anchor: '100%',
                                layoutConfig:
                                {
                                    padding: '3',
                                    pack: 'end',
                                    align: 'middle'
                                },
                                items:
                                [
                                    {
                                        xtype: 'button',
                                        text: nmBtnOK,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnOkLapIGDDetailPasien',
                                        handler: function()
                                        {
                                            var sendDataArray = [];
                                            secondGridStore.each(function(record){
                                            var recordArray = [record.get("kd_unit")];
                                            sendDataArray.push(recordArray);
                                            });
            //                                if (ValidasiReportIGDDetailPasien() === 1)
            //                                {
                                                    var criteria = GetCriteriaIGDDetailPasien();
                                                    criteria += '##@@##' + sendDataArray;
														if (selectSetJenisPasien === 'Semua Pasien' )
															{
															criteria += '##@@##' + 'kabeh';
															}
															if (selectSetJenisPasien === 'Pasien Baru' )
															{
															criteria += '##@@##' + 'true';
															};
															   
															   if (selectSetJenisPasien === 'Pasien Lama' )
															{
															criteria += '##@@##' + 'false';
															};
                                                    frmDlgIGDDetailPasien.close();
                                                    loadlaporanIGD('0', 'rep030201', criteria);
                                                    
            //                                };
                                        }
                                    },
                                    {
                                        xtype: 'button',
                                        text: nmBtnCancel ,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnCancelLapIGDDetailPasien',
                                        handler: function()
                                        {
                                                frmDlgIGDDetailPasien.close();
                                        }
                                    }
                                    ]
                                }
			],
//                        bbar    : [
//			'->', // Fill
//			{
//				text    : 'Reset',
//				handler : function() {
//					//refresh source grid
//					 datarefresh_viInformasiUnit()
//
//					//purge destination grid
//					secondGridStore.removeAll();
//				}
//			}
//		]
		
    }
    )
    
        
        
    
    
    // datarefresh_viInformasiUnitdokter();
    return FrmTabs_viInformasiUnitdokter;
}



function datarefresh_viInformasiUnit()
{
    dataSource_unit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'nama_unit',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=1 and type_unit=false"
            }
        }
    )
}

function datarefresh_viInformasiUnitSpesial(kode)
{
    dataSource_unit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'nama_unit',
                Sortdir: 'ASC',
                target:'ViewSetupUnitSpesial',
                param: kode
            }
        }
    )
}

    

	
