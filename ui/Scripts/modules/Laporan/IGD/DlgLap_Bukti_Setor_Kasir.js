var type_file=0;
var excel=0;
var dsRWJLapBuktiSetorKasir;
var selectRWJHistoryPembayaran;
var selectNamaRWJHistoryPembayaran;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJBuktiSetor;
var varLapRWJHistoryPembayaran= ShowFormLapRWJBuktiSetor();
var dsRWJ;

function ShowFormLapRWJBuktiSetor()
{
    frmDlgRWJBuktiSetor= fnDlgRWJHistoryPembayaran();
    frmDlgRWJBuktiSetor.show();
};

function fnDlgRWJHistoryPembayaran()
{
    var winRWJHistoryPembayaranReport = new Ext.Window
    (
        {
            id: 'winRWJHistoryPembayaranReport',
            title: 'Laporan Bukti Setor Kasir',
            closeAction: 'destroy',
            width:470,
            height: 230,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJBuktiSetor()]

        }
    );

    return winRWJHistoryPembayaranReport;
};

function ItemDlgRWJBuktiSetor()
{
    var PnlLapRWJBuktiSetor = new Ext.Panel
    (
        {
            id: 'PnlLapRWJBuktiSetor',
            fileUpload: true,
            layout: 'form',
            width:450,
            height: 350,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJBuktiSetor_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJBuktiSetor',
					handler: function(){
							GetDTLTRGridBukti_Setor();
					}
				},
				{
					xtype: 'button',
					text: 'Print',
					width: 70,
					hidden:true,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnPrintLapRWJHistoryPembayaran',
					handler: function(){
					/*   if (ValidasiShiftRWJBuktiSetorKasir() === 1)
					   {
							var params={
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJBuktiSetor').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJBuktiSetor').getValue(),
								type_file:type_file
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_history_pembayaran/cetak_direct");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJBuktiSetor.close(); 
					   };*/
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJHistoryPembayaran',
					handler: function()
					{
						frmDlgRWJBuktiSetor.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJBuktiSetor;
};


function ValidasiShiftRWJBuktiSetorKasir(){
    var x=1;
	if(Ext.getCmp('CekLapPilihShift_1_awal').checked==false && Ext.getCmp('CekLapPilihShift_2_awal').checked==false && Ext.getCmp('CekLapPilihShift_3_awal').checked==false){
		ShowPesanWarningRWJBuktiSetorKasir('Shift Periode Awal Tidak Tepat', 'Konfirmasi');	
		x=0;
	}
	// if(Ext.getCmp('CekLapPilihShift_1_akhir').checked==false && Ext.getCmp('CekLapPilihShift_2_akhir').checked==false){
	// 	ShowPesanWarningRWJBuktiSetorKasir('Shift Periode Akhir Tidak Tepat', 'Konfirmasi');	
	// 	x=0;
	// }
    return x;
};

function ValidasiTanggalReportRWJHistoryPembayaran(){
    var x=1;
  	if(Ext.get('dtpTglAwalFilter_LapBukti_Setor').dom.value > Ext.get('dtpTglAkhirFilter_LapBukti_Setor').dom.value){
       ShowPesanWarningRWJBuktiSetorKasir('Periode Awal Tidak Boleh Melebihi Periode Akhir', 'Konfirmasi');
        x=0;
    }
    return x;
};

function ShowPesanWarningRWJBuktiSetorKasir(str,modul){
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJHistoryPembayaran_Dept(){
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.90,
                layout: 'form',
                labelWidth: 100,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};



function getItemLapRWJBuktiSetor_Periode(){
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  430,
            height: 200,
            anchor: '100% 100%',
            items: [
			{
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode Awal'
            }, 
            {
                x: 240,
                y: 10,
                xtype: 'label',
                // text: 'Shift Periode Awal'
				text: 'Shift'
            }, 
            {
                x: 330,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
            {
					   xtype: 'checkbox',
					   x: 340,
              		   y: 10,
					   fieldLabel: '',
					   id: 'CekLapPilihShift_1_awal',
					   hideLabel:true,
					   boxLabel: '1',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihShift_1_awal').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
			},
			{
					   xtype: 'checkbox',
					   x: 365,
              		   y: 10,
					   fieldLabel: '',
					   id: 'CekLapPilihShift_2_awal',
					   hideLabel:true,
					   boxLabel: '2',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihShift_2_awal').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
			},
			{
					   xtype: 'checkbox',
					   x: 390,
              		   y: 10,
					   fieldLabel: '',
					   id: 'CekLapPilihShift_3_awal',
					   hideLabel:true,
					   boxLabel: '3',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihShift_3_awal').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
			},
			{
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
			{
				x: 120,
				y: 10,
				xtype: 'datefield',
				id: 'dtpTglAwalFilter_LapBukti_Setor',
				format: 'd-M-Y',
				value: now
			}, 
			
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode Akhir '
            },
            {
                x: 240,
                y: 40,
				hidden:true,
                xtype: 'label',
                text: 'Shift Periode Akhir'
            }, 
               {
                x: 330,
                y: 40,
				hidden:true,
                xtype: 'label',
                text: ' : '
            },
            {
					   xtype: 'checkbox',
					   x: 340,
              		   y: 40,
					   fieldLabel: '',
					   id: 'CekLapPilihShift_1_akhir',
					   hideLabel:true,
					   hidden:true,
					   boxLabel: '1',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihShift_1_akhir').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
			},
			{
					   xtype: 'checkbox',
					   x: 365,
              		   y: 40,
					   hidden:true,
					   fieldLabel: '',
					   id: 'CekLapPilihShift_2_akhir',
					   hideLabel:true,
					   boxLabel: '2',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihShift_2_akhir').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
			},
			{
					   xtype: 'checkbox',
					   x: 390,
              		   y: 40,
					   fieldLabel: '',
					   hidden:true,
					   id: 'CekLapPilihShift_3_akhir',
					   hideLabel:true,
					   boxLabel: '3',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihShift_3_akhir').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
			},
            {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 120,
                y: 40,
                xtype: 'datefield',
				id: 'dtpTglAkhirFilter_LapBukti_Setor',
				format: 'd-M-Y',
				value: now
            },
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Unit'
            },
            {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
           comboUnitBuktiSetor(),
           {
                x: 10,
                y: 100,
				hidden:true,
                xtype: 'label',
                text: 'Operator Kasir'
            },
            {
                x: 110,
                y: 100,
				hidden:true,
                xtype: 'label',
                text: ' : '
            },
            comboUnitOpKasir(),
			{
                x: 10,
                y: 100,
				hidden:false,
                xtype: 'label',
                text: 'Type File'
            }, 
			{
				x: 110,
				y: 100,
				hidden:false,
				xtype: 'label',
				text: ' : '
            },
            {
					xtype: 'checkbox',
					x: 120,
					y: 100,
					fieldLabel: '',
					id: 'CetakExcelBuktiSetor',
					hideLabel:true,
					hidden:false,
					boxLabel: 'Excel',
					checked: false,
					listeners: 
					{
						check: function()
						{
							if(Ext.getCmp('CetakExcelBuktiSetor').getValue()===true)
							{
								excel=1;
							}
							else
							{
								excel=0;
							}
						}
					}
			},
            ]
        }]
    };
    return items;
  }  

  function comboUnitBuktiSetor(){
    cboTransaksi_unit_bukti_setor = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboUnitBuktiSetor',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:250,
                store: new Ext.data.ArrayStore( {
					id: 0,
					fields:[
						'Id',
						'displayText'
					],
					data: [[1,'Gawat Darurat'],
						   [2,'Rawat Jalan'], 
						   [3, 'Rawat Inap'],
						   [4, 'Laboratorium'],
						   [5, 'Radiologi'],
						   [6, 'Apotek'],
						   [200, 'Semua']
					]
				}),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboTransaksi_unit_bukti_setor;
};

 function comboUnitOpKasir(){
    cboTransaksi_op_kasir_bukti_setor = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboOPKasirBuktiSetor',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
				hidden:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:250,
                store: new Ext.data.ArrayStore( {
					id: 0,
					fields:[
						'Id',
						'displayText'
					],
					data: [[1, 'Kasir 1'],[2, 'Kasir 2 '], 
						[3, 'Kasir 3'],[11, 'Semua']
					]
				}),
                valueField: 'Id',
                displayField: 'displayText',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboTransaksi_op_kasir_bukti_setor;
};

  function GetDTLTRGridBukti_Setor(){

  	if(ValidasiShiftRWJBuktiSetorKasir(nmHeaderSimpanData,false) == 1 &&  ValidasiTanggalReportRWJHistoryPembayaran(nmHeaderSimpanData,false) == 1){
  
  		var tmp_tgl_awal=Ext.get('dtpTglAwalFilter_LapBukti_Setor').getValue();
	  	var tmp_tgl_akhir=Ext.get('dtpTglAkhirFilter_LapBukti_Setor').getValue();
	  	var tmp_unit=Ext.get('cboUnitBuktiSetor').getValue();
	  	if(Ext.getCmp('CekLapPilihShift_1_awal').checked==true){
	  		var tmp_ceklis_shift_awal_1='1'
	  	}if(Ext.getCmp('CekLapPilihShift_2_awal').checked==true){
	  		var tmp_ceklis_shift_awal_2='2'
	  	}if(Ext.getCmp('CekLapPilihShift_3_awal').checked==true){
	  		var tmp_ceklis_shift_awal_3='3'
	  	}
		// if(Ext.getCmp('CekLapPilihShift_1_akhir').checked==true){
	  	// 	var tmp_ceklis_shift_akhir_1='1'
	  	// }if(Ext.getCmp('CekLapPilihShift_2_akhir').checked==true){
	  	// 	var tmp_ceklis_shift_akhir_2='2'
	  	// }if(Ext.getCmp('CekLapPilihShift_3_akhir').checked==true){
	  	// 	var tmp_ceklis_shift_akhir_3='3'
	  	// }		
		loadMask.show();
		var params={};
		params['tgl_awal']=tmp_tgl_awal;
		params['tgl_akhir']=tmp_tgl_akhir;
		params['shift_awal_1']=tmp_ceklis_shift_awal_1;
		params['shift_awal_2']=tmp_ceklis_shift_awal_2;
		params['shift_awal_3']=tmp_ceklis_shift_awal_3;
		params['type_file']=excel;
		// params['shift_akhir_1']=tmp_ceklis_shift_akhir_1;
		// params['shift_akhir_2']=tmp_ceklis_shift_akhir_2;
		// params['shift_akhir_3']=tmp_ceklis_shift_akhir_3;
		params['unit']=tmp_unit;

		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		form.setAttribute("action", baseURL + "index.php/rawat_jalan/function_lap/laporan_bukti_sektor");
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
		
		// console.log(tmp_ceklis_shift_awal_1);
		// var url = baseURL+"index.php/rawat_jalan/lap_bukti_setor/preview_bukti_setor/"+tmp_tgl_awal+"/"+tmp_tgl_akhir+"/"+tmp_ceklis_shift_awal_1+"/"+tmp_ceklis_shift_awal_2+"/"+
		// 		tmp_ceklis_shift_awal_3+"/"+tmp_ceklis_shift_akhir_1+"/"+tmp_ceklis_shift_akhir_2+"/"+tmp_ceklis_shift_akhir_3+"/"+tmp_unit;
	  	
		// console.log(url);
		// new Ext.Window({
		// 	title: 'LAPORAN BUKTI SETOR KASIR',
		// 	width: 1000,
		// 	height: 600,
		// 	constrain: true,
		// 	modal: true,
		// 	tbar : [
	    //      		/*{ 	
	    //      			xtype  :'button',
	    //      			iconCls:'print',
	    //      			text   :'Cetak Rekap Tindakan Rawat Jalan',
	    //      			id     :'btnPrint_Rekap_Tindakan',
		// 					handler: function ()
		// 					{
		// 					    Ext.Ajax.request({
		// 							//url: baseURL + "index.php/rawat_inap/lap_bill_kasir_rwi/Cetak",
		// 							url: baseURL+"index.php/laporan/rekap_tindakan/do_print_RWJ",
		// 							params: {
		// 								no_transaksi 	: notransaksi,
		// 								tgl_transaksi 	: tgltrans,
		// 								kd_kasir 		: kodekasir,
		// 								print 			: true,
		// 							},
		// 							success: function(o){

		// 							}
		// 						});
		// 					},
		// 			},*/
	         
	    //  ],
		// 	html: "<iframe  style='width: 100%; height: 100%;' src='" + url+ "'></iframe>"
		// }).show();
	}
		
};
