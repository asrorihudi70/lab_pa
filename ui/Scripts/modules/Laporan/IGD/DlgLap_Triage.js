var type_file=0;
var ds_lapTriage;
var select_lapTriage;
var selectNama_lapTriage;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlg_lapTriage;
var varLap_lapTriage= ShowFormLap_lapTriage();
var ds;
var DataStorePendidikan_index_;

function ShowFormLap_lapTriage()
{
    frmDlg_lapTriage= fnDlg_lapTriage();
    frmDlg_lapTriage.show();
};

function fnDlg_lapTriage()
{
    var win_lapTriageReport = new Ext.Window
    (
        {
            id: 'win_lapTriageReport',
            title: 'Laporan Triage',
            closeAction: 'destroy',
            width:550,
            height: 180,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlg_lapTriage()]

        }
    );

    return win_lapTriageReport;
};

function ItemDlg_lapTriage()
{
    var PnlLap_lapTriage = new Ext.Panel
    (
        {
            id: 'PnlLap_lapTriage',
            fileUpload: true,
            layout: 'form',
            width:500,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLap_lapTriage_Periode(),
				mComboTriase(),
				mComboTindakLanjut(),
				{
				   xtype: 'checkbox',
				   fieldLabel: 'Type ',
				   id: 'CekLapPilihTypeExcel',
				   hideLabel:false,
				   boxLabel: 'Excel',
				   checked: false,
				   hidden:true,
				   labelStyle: 'width:200px; white-space: nowrap;', 
				   listeners: 
				   {
						check: function()
						{
						   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
							{
								type_file=1;
							}
							else
							{
								type_file=0;
							}
						}
				   }
				},
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLap_lapTriage',
					handler: function(){
					   if (ValidasiReport_lapTriage() === 1){
							var params={
								tgl_awal 		: Ext.getCmp('dtpTglAwalLap_lapTriage').getValue(),
								tgl_akhir 		: Ext.getCmp('dtpTglAkhirLap_lapTriage').getValue(),
								type_file 		: type_file,
								triase 			: Ext.getCmp('cboStatusTriase').getValue(),
								tindak_lanjut 	: Ext.getCmp('cboTindakLanjut').getValue(),
							};
							console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/gawat_darurat/lap_triase/laporan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLap_lapTriage',
					handler: function()
					{
						frmDlg_lapTriage.close();
					}
				}
			
			]
        }
    );

    return PnlLap_lapTriage;
};


function ValidasiReport_lapTriage()
{
    var x=1;
	if(Ext.get('dtpTglAwalLap_lapTriage').dom.value === '')
	{
		ShowPesanWarning_lapTriageReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_Lap_lapTriage').getValue() === false && Ext.getCmp('Shift_1_Lap_lapTriage').getValue() === false && Ext.getCmp('Shift_2_Lap_lapTriage').getValue() === false && Ext.getCmp('Shift_3_Lap_lapTriage').getValue() === false){
		ShowPesanWarning_lapTriageReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReport_lapTriage()
{
    var x=1;
    if(Ext.get('dtpTglAwalLap_lapTriage').dom.value > Ext.get('dtpTglAkhirLap_lapTriage').dom.value)
    {
        ShowPesanWarning_lapTriageReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarning_lapTriageReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLap_lapTriage_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLap_lapTriage_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			// {
			// 	columnWidth: .49,
			// 	layout: 'form',
			// 	labelWidth:70,
			// 	border: false,
			// 	items:
			// 	[
			// 	]
			// },
			{
				columnWidth: .65,
				layout: 'form',
				labelWidth:100,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						labelStyle: 'width:200px; white-space: nowrap;', 
						id: 'dtpTglAwalLap_lapTriage',
						format: 'd-M-Y',
						value:now,
						// anchor: '99%'
					},
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .25,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLap_lapTriage',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};



function mComboTriase()
{
    var cboStatusTriase = new Ext.form.ComboBox
	(
		{
			/*x: 120,
			y: 220,*/
			id:'cboStatusTriase',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Status Triase ',
			width:300,
			labelStyle: 'width:200px; white-space: nowrap;', 
			value:7,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, '1'],[2, '2'],[3, '3'],[4, '4'],[5, '5'],[6, 'DOA'],[7, 'SEMUA']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
		}
	);
	return cboStatusTriase;
};


function mComboTindakLanjut()
{
    var cboTindakLanjut = new Ext.form.ComboBox
	(
		{
			/*x: 120,
			y: 220,*/
			id:'cboTindakLanjut',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Tindak Lanjut/Kondisi Keluar IGD',
			labelStyle: 'width:200px; white-space: nowrap;', 
			value:6,
			width:300,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 'Pulang'],[2, 'Rawat'],[3, 'Rujuk'],[4, 'Dead'],[5, 'PAPS'],[6, 'SEMUA']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
		}
	);
	return cboTindakLanjut;
};

function loaddatastorePendidikan(){
	var Field = ['KD_PENDIDIKAN','PENDIDIKAN'];
	var dsPendidikanField = new WebApp.DataStore({fields: Field});
    Ext.Ajax.request({
        url: baseURL +  "index.php/general/pendidikan/combo",
        params: {
            null : null,
        },
        success: function(response) {
            var cst  = Ext.decode(response.responseText);
            console.log(cst);
            for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
                var recs    = [],recType = dsPendidikanField.recordType;
                var o       = cst['ListDataObj'][i];
                recs.push(new recType(o));
                DataStorePendidikan_index_.add(recs);
            }
        },
    });
}