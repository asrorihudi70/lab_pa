
var dsIGDRegis;
var selectNamaIGDRegis;
var now = new Date();
var selectSetPerseorangan;
var frmDlgIGDRegis;
var varLapIGDRegis= ShowFormLapIGDRegis();
var selectSetUmum;

function ShowFormLapIGDRegis()
{
    frmDlgIGDRegis= fnDlgIGDRegis();
    frmDlgIGDRegis.show();
};

function fnDlgIGDRegis()
{
    var winIGDRegisReport = new Ext.Window
    (
        {
            id: 'winIGDRegisReport',
            title: 'Laporan Rawat Jalan',
            closeAction: 'destroy',
            width:500,
            height: 400,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDRegis()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseorangan').show();
                Ext.getCmp('cboAsuransi').hide();
                Ext.getCmp('cboPerusahaanRequestEntry').hide();
                Ext.getCmp('cboUmum').hide();
                Ext.getCmp('dtpTglAkhirLapIGDKIUP').hide();
            }
        }

        }
    );

    return winIGDRegisReport;
};


function ItemDlgIGDRegis()
{
    var PnlLapIGDRegis = new Ext.Panel
    (
        {
            id: 'PnlLapIGDRegis',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDRegis_Dept(),
                getItemLapIGDRegis_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapIGDRegis',
                            handler: function()
                            {
//                                if (ValidasiReportIGDRegis() === 1)
//                                {
                                        var tmppilihan = getKodeReportIGDRegis();
                                        var criteria = GetCriteriaIGDRegis();
                                        frmDlgIGDRegis.close();
                                        ShowReport('0', tmppilihan, criteria);
//                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapIGDRegis',
                            handler: function()
                            {
                                    frmDlgIGDRegis.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapIGDRegis;
};

function GetCriteriaIGDRegis()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapIGDRegis').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapIGDRegis').getValue();
//                strKriteria += '##@@##' + Ext.get('kelPasien').getValue();
	};

	return strKriteria;
};


//function ValidasiReportIGDRegis(modul)
//{
//    var x=1;
//
//    if((Ext.get('dtpTglAwalLapIGDRegis').dom.value === ''))
//    {
//        ShowPesanWarningIGDRegisReport("Tanggal Tidak Boleh Kosong...", modul);
//        x=0;
//    }
//    else if(Ext.get('kelPasien').dom.value === '' || Ext.get('kelPasien').dom.value === 'Pilih Salah Satu...')
//    {
//        ShowPesanWarningIGDRegisReport("Anda Belum Memilih Kelompok pasien...", modul);
//        x=0;
//    }else{
//        x =1;
//    }
//
//    return x;
//};

function getKodeReportIGDRegis()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihan').getValue() === 1)
    {
        tmppilihan = '1020203';
    }else if (Ext.getCmp('cboPilihan').getValue() === 2)
    {
        tmppilihan = '1020204';
    }
    return tmppilihan;
}

function ValidasiTanggalReportIGDRegis()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDRegis').dom.value > Ext.get('dtpTglAkhirLapIGDRegis').dom.value)
    {
        ShowPesanWarningIGDRegisReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDRegisReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapIGDRegis_Dept()
{
    var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: .90,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 150,
            items:
            [
                mComboPilihan(),
                {
                    xtype: 'datefield',
                    fieldLabel: nmPeriodeDlgRpt + ' ',
                    id: 'dtpTglAwalLapIGDRegis',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                },
                
                {
                    xtype: 'datefield',
                    fieldLabel: nmSd + ' ',
                    id: 'dtpTglAkhirLapIGDKIUP',
                    format: 'd/M/Y',
                    value:now,
                    width:105
               },
               {
                   xtype: 'textfield',
                   fieldLabel: 'Bagian',
                   name: 'txtBagian',
                   id: 'txtBagian',
                   value: 'IGD',
                   disabled : true,
                   width:105
               },
               mComboPoli_viDaftarIGD(),
               {  
                   xtype: 'combo',
                   fieldLabel: 'Kelompok Pasien',
                   id: 'kelPasien',
                   editable: false,
                   //value: 'Perseorangan',
                   store: new Ext.data.ArrayStore
                       (
                           {
                            id: 0,
                            fields:
                            [
                                'Id',
                                'displayText'
                            ],
                               data: [[1, 'Semua'],[2, 'Umum'], [3, 'Perusahaan'], [4, 'Asuransi']]
                            }
                        ),
                    displayField: 'displayText',
                    mode: 'local',
                    width: 100,
                    forceSelection: true,
                    triggerAction: 'all',
                    emptyText: 'Pilih Salah Satu...',
                    selectOnFocus: true,
                    anchor: '95%',
                    listeners:
                     {
                            'select': function(a, b, c)
                        {
                           Combo_Select(b.data.displayText);

                        }

                    }
                },
                
                mcomborujukandari(),
                mComboRujukan()
                
            ]
        }
        ]
        
    };
    return items;
};

function getItemLapIGDRegis_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: .90,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 150,
            defaultType: 'checkbox',
            
            items:
            [{
                    fieldLabel: 'Pilih Shift',
                    boxLabel: 'Pasien Baru',
                    name: 'Pasien_Baru',
                    id : 'Pasien_Baru'
                    
                }, {
                    fieldLabel: '',
                    labelSeparator: '',
                    boxLabel: 'Pasien Lama',
                    name: 'Pasien_Lama',
                    id : 'Pasien_Lama'
                },{
                    fieldLabel: '',
                    labelSeparator: '',
                    boxLabel: 'Semua Pasien',
                    name: 'Pasien_All',
                    id : 'Pasien_All',
                    handler: function (field, value) {
                       if (value === true)
                       {
                           Ext.getCmp('Pasien_Baru').setValue(true);
                           Ext.getCmp('Pasien_Lama').setValue(true);
                           Ext.getCmp('Pasien_Baru').disable();
                           Ext.getCmp('Pasien_Lama').disable();
                       }else{
                           Ext.getCmp('Pasien_Baru').setValue(false);
                           Ext.getCmp('Pasien_Lama').setValue(false);
                           Ext.getCmp('Pasien_Baru').enable();
                           Ext.getCmp('Pasien_Lama').enable();
                       }
                    }
                }]
            }
        ]
    };
    return items;
};

var selectSetPilihan;
function mComboPilihan()
{
    var cboPilihan = new Ext.form.ComboBox
	(
		{
			id:'cboPilihan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Pendaftaran Per Shift ',
			width:150,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Detail'], [2, 'Sumary Per Hari'], [3, 'Sumary Per Bulan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetPilihan,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetPilihan=b.data.displayText ;
				}
			}
		}
	);
	return cboPilihan;
};

function mComboPerseorangan()
{
    var cboPerseorangan = new Ext.form.ComboBox
	(
		{
			id:'cboPerseorangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Umum']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
//			value:selectSetPerseorangan,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetPerseorangan=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseorangan;
};

function mComboUmum()
{
    var cboUmum = new Ext.form.ComboBox
	(
		{
			id:'cboUmum',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
//			value:selectSetUmum,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
			}
		}
	);
	return cboUmum;
};

function mComboPerusahaan()
{
    var Field = ['KD_PERUSAHAAN','PERUSAHAAN'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboPerusahaan',
			    param: ''
			}
		}
	);
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_PERUSAHAAN',
		    displayField: 'PERUSAHAAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//			        var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                }
			}
                }
	);

    return cboPerusahaanRequestEntry;
};

function mComboPoli_viDaftarIGD(lebar,Nama_ID)
{
    var Field_poli_viDaftarIGD = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftarIGD = new WebApp.DataStore({fields: Field_poli_viDaftarIGD});

	ds_Poli_viDaftarIGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=3 and type_unit=false"
            }
        }
    )

    var cbo_Poli_viDaftarIGD = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Unit',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store: ds_Poli_viDaftarIGD,
            width: lebar,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Unit...',
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            name: Nama_ID,
            lazyRender: true,
            id: 'cboPoliviDaftarIGD',
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )

    return cbo_Poli_viDaftarIGD;
}

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomer',
                param: "status=true"
            }
        }
    );
    var cboAsuransi = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransi',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Asuransi...',
                        fieldLabel: '',
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.displayText ;
				}
			}
		}
	);
	return cboAsuransi;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Umum")
   {    
        Ext.getCmp('cboPerseorangan').show();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Asuransi")
       {
         Ext.getCmp('cboPerseorangan').hide();
         Ext.getCmp('cboAsuransi').show();
         Ext.getCmp('cboPerusahaanRequestEntry').hide();
         Ext.getCmp('cboUmum').hide();
       }
       else if(value === "Semua")
       {
            Ext.getCmp('cboPerseorangan').hide();
            Ext.getCmp('cboAsuransi').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            Ext.getCmp('cboUmum').show();
       }else
       {
           Ext.getCmp('cboPerseorangan').hide();
            Ext.getCmp('cboAsuransi').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            Ext.getCmp('cboUmum').hide();
       }
}

function mcomborujukandari()
{
    var Field = ['CARA_PENERIMAAN','PENERIMAAN'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewComboRujukanDari',
                param: ""
            }
        }
    )

    var cboRujukanDariRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanDariRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Rujukan...',
            fieldLabel: 'Rujukan Dari ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'CARA_PENERIMAAN',
            displayField: 'PENERIMAAN',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   loaddatastorerujukan(b.data.CARA_PENERIMAAN)
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }


		}
        }
    )

    return cboRujukanDariRequestEntry;
}

function loaddatastorerujukan(cara_penerimaan)
{
          dsRujukanRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    Sort: 'rujukan',
			    Sortdir: 'ASC',
			    target: 'ViewComboRujukan',
			    param: 'cara_penerimaan=~'+ cara_penerimaan+ '~'
			}
                    }
                )
}

function mComboRujukan()
{
    var Field = ['KD_RUJUKAN','RUJUKAN'];

    dsRujukanRequestEntry = new WebApp.DataStore({fields: Field});


    var cboRujukanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboRujukanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Rujukan...',
		    fieldLabel: 'Rujukan ',
		    align: 'Right',
                     
//		    anchor:'60%',
		    store: dsRujukanRequestEntry,
		    valueField: 'KD_RUJUKAN',
		    displayField: 'RUJUKAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboRujukanRequestEntry;
};