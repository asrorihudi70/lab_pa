
var dsRWJPerIndex;
var selectNamaRWJPerIndex;
var now = new Date();
var selectSetPilihUnit;
var frmDlgRWJPerIndex;
var varLapRWJPerIndex= ShowFormLapRWJPerIndex();
var selectSetPilihKriteria;

function ShowFormLapRWJPerIndex()
{
    frmDlgRWJPerIndex= fnDlgRWJPerIndex();
    frmDlgRWJPerIndex.show();
};

function fnDlgRWJPerIndex()
{
    var winRWJPerIndexReport = new Ext.Window
    (
        {
            id: 'winRWJPerIndexReport',
            title: 'Laporan Rawat Jalan',
            closeAction: 'destroy',
            width:500,
            height: 400,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJPerIndex()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPilihUnit').show();
                Ext.getCmp('cboAsuransi').hide();
                Ext.getCmp('cboPerusahaanRequestEntry').hide();
                Ext.getCmp('cboPilihKriteria').hide();
                Ext.getCmp('dtpTglAkhirLapRWJKIUP').hide();
            }
        }

        }
    );

    return winRWJPerIndexReport;
};


function ItemDlgRWJPerIndex()
{
    var PnlLapRWJPerIndex = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPerIndex',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJPerIndex_Dept(),
                getItemLapRWJPerIndex_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWJPerIndex',
                            handler: function()
                            {
//                                if (ValidasiReportRWJPerIndex() === 1)
//                                {
//                                        var tmppilihan = getKodeReportRWJPerIndex();
                                        var criteria = GetCriteriaRWJPerIndex();
                                        frmDlgRWJPerIndex.close();
                                        ShowReport('0', 1020205, criteria);
//                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWJPerIndex',
                            handler: function()
                            {
                                    frmDlgRWJPerIndex.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWJPerIndex;
};

function GetCriteriaRWJPerIndex()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJPerIndex').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWJPerIndex').getValue();
//                strKriteria += '##@@##' + Ext.get('kelPasien').getValue();
	};

	return strKriteria;
};


//function ValidasiReportRWJPerIndex(modul)
//{
//    var x=1;
//
//    if((Ext.get('dtpTglAwalLapRWJPerIndex').dom.value === ''))
//    {
//        ShowPesanWarningRWJPerIndexReport("Tanggal Tidak Boleh Kosong...", modul);
//        x=0;
//    }
//    else if(Ext.get('kelPasien').dom.value === '' || Ext.get('kelPasien').dom.value === 'Pilih Salah Satu...')
//    {
//        ShowPesanWarningRWJPerIndexReport("Anda Belum Memilih Kelompok pasien...", modul);
//        x=0;
//    }else{
//        x =1;
//    }
//
//    return x;
//};

function getKodeReportRWJPerIndex()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihan').getValue() === 1)
    {
        tmppilihan = '1020205';
    }else if (Ext.getCmp('cboPilihan').getValue() === 2)
    {
        tmppilihan = '1020204';
    }
    return tmppilihan;
}

function ValidasiTanggalReportRWJPerIndex()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJPerIndex').dom.value > Ext.get('dtpTglAkhirLapRWJPerIndex').dom.value)
    {
        ShowPesanWarningRWJPerIndexReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJPerIndexReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapRWJPerIndex_Dept()
{
    var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: .90,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 150,
            items:
            [
                mComboPilihan(),
                {
                    xtype: 'datefield',
                    fieldLabel: nmPeriodeDlgRpt + ' ',
                    id: 'dtpTglAwalLapRWJPerIndex',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Bagian',
                    name: 'txtBagian',
                    id: 'txtBagian',
                    value: 'Rawat Jalan',
                    disabled : true,
                    width:105
                },
                mComboPilihKriteria(),
//                {
//                    columnWidth: 0.48,
//                    layout: 'form',
//                    border: false,
//                            labelWidth: 25,
//                    labelAlign: 'right',
//                    items:
//                    [
//                        {
//                            xtype: 'datefield',
//                            fieldLabel: nmSd + ' ',
//                            id: 'dtpTglAkhirLapRWJKIUP',
//                            format: 'd/M/Y',
//                            value:now,
//                            width:105
//                        }
//                    ]
//               }
            ]
        }
        ]
        
    };
    return items;
};

function getItemLapRWJPerIndex_Tanggal()
{
   var items =
    {
//        layout: 'column',
//        border: false,
//        items:
//        [
//        {
//            columnWidth: .90,
//            layout: 'form',
//            border: false,
//            labelAlign: 'right',
//            labelWidth: 150,
//            defaultType: 'checkbox',
//            
//            items:
//            [{
//                    fieldLabel: 'Pilih Index',
//                    boxLabel: 'Index 1',
//                    name: 'Index_1',
//                    id : 'Index_1'
//                    
//                }, {
//                    fieldLabel: '',
//                    labelSeparator: '',
//                    boxLabel: 'Index 2',
//                    name: 'Index_2',
//                    id : 'Index_2'
//                }, {
//                    fieldLabel: '',
//                    labelSeparator: '',
//                    boxLabel: 'Index 3',
//                    name: 'Index_3',
//                    id : 'Index_3'
//                },{
//                    fieldLabel: '',
//                    labelSeparator: '',
//                    boxLabel: 'All Index',
//                    name: 'Index_All',
//                    id : 'Index_All',
//                    handler: function (field, value) {
//                       if (value === true)
//                       {
//                           Ext.getCmp('Index_1').setValue(true);
//                           Ext.getCmp('Index_2').setValue(true);
//                           Ext.getCmp('Index_3').setValue(true);
//                           Ext.getCmp('Index_1').disable();
//                           Ext.getCmp('Index_2').disable();
//                           Ext.getCmp('Index_3').disable();
//                       }else{
//                           Ext.getCmp('Index_1').setValue(false);
//                           Ext.getCmp('Index_2').setValue(false);
//                           Ext.getCmp('Index_3').setValue(false);
//                           Ext.getCmp('Index_1').enable();
//                           Ext.getCmp('Index_2').enable();
//                           Ext.getCmp('Index_3').enable();
//                       }
//                    }
//                }]
//            }
//        ]
    };
    return items;
};

var selectSetPilihan;
function mComboPilihan()
{
    var cboPilihan = new Ext.form.ComboBox
	(
		{
			id:'cboPilihan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Pendaftaran Per Index ',
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'IGD']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetPilihan,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetPilihan=b.data.displayText ;
				}
			}
		}
	);
	return cboPilihan;
};

function mComboPilihKriteria()
{
    var cboPilihKriteria = new Ext.form.ComboBox
	(
		{
			id:'cboPilihKriteria',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Prop.'],[2, 'Kab.'],[3, 'Kec.'],[4, 'Kel.']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
//			value:selectSetPilihKriteria,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetPilihKriteria=b.data.displayText ;
				}
			}
		}
	);
	return cboPilihKriteria;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "PilihKriteria")
   {    
        Ext.getCmp('cboPilihUnit').show();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboPilihKriteria').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPilihUnit').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
        Ext.getCmp('cboPilihKriteria').hide();
   }
   else if(value === "Asuransi")
       {
         Ext.getCmp('cboPilihUnit').hide();
         Ext.getCmp('cboAsuransi').show();
         Ext.getCmp('cboPerusahaanRequestEntry').hide();
         Ext.getCmp('cboPilihKriteria').hide();
       }
       else if(value === "Semua")
       {
            Ext.getCmp('cboPilihUnit').hide();
            Ext.getCmp('cboAsuransi').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            Ext.getCmp('cboPilihKriteria').show();
       }else
       {
           Ext.getCmp('cboPilihUnit').hide();
            Ext.getCmp('cboAsuransi').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            Ext.getCmp('cboPilihKriteria').hide();
       }
}