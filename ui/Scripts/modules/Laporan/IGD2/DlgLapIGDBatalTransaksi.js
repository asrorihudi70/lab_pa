
var dsIGDBatalTransaksi;
var selectIGDBatalTransaksi;
var selectNamaIGDBatalTransaksi;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgIGDBatalTransaksi;
var varLapIGDBatalTransaksi= ShowFormLapIGDBatalTransaksi();
var dsRWJ;
function ShowFormLapIGDBatalTransaksi()
{
    frmDlgIGDBatalTransaksi= fnDlgIGDBatalTransaksi();
    frmDlgIGDBatalTransaksi.show();
};

function fnDlgIGDBatalTransaksi()
{
    var winIGDBatalTransaksiReport = new Ext.Window
    (
        {
            id: 'winIGDBatalTransaksiReport',
            title: 'Laporan Pembatalan Transaksi',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDBatalTransaksi()]

        }
    );

    return winIGDBatalTransaksiReport;
};

function ItemDlgIGDBatalTransaksi()
{
    var PnlLapIGDBatalTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapIGDBatalTransaksi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDBatalTransaksi_Periode(),
			//	getItemIGDBatalTransaksi_Shift(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapIGDBatalTransaksi',
					handler: function()
					{
					   if (ValidasiReportIGDBatalTransaksi() === 1)
					   {
						/* 	if (Ext.getCmp('Shift_All_LapIGDBatalTransaksi').getValue() === true){
								shift='All';
								shift1='false';
								shift2='false';
								shift3='false';
							}else{
								shift='';
								if (Ext.getCmp('Shift_1_LapIGDBatalTransaksi').getValue() === true){
									shift1='true';
								} else{
									shift1='false';
								}
								if (Ext.getCmp('Shift_2_LapIGDBatalTransaksi').getValue() === true){
									shift2='true';
								}else{
									shift2='false';
								}
								if (Ext.getCmp('Shift_3_LapIGDBatalTransaksi').getValue() === true){
									shift3='true';
								}else{
									shift3='false';
								}
							} */
							
							/* var params={
								tglAwal:Ext.getCmp('dtpTglAwalLapIGDBatalTransaksi').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAwalLapIGDBatalTransaksi').getValue(),
								/* shift:shift,
								shift1:shift1,
								shift2:shift2,
								shift3:shift3, 
							}  */
							var params=Ext.get('dtpTglAwalLapIGDBatalTransaksi').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapIGDBatalTransaksi').getValue()+'#aje#Laporan Batal Transaksi Gawat Darurat#aje#06';
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionRWJ/cetakRWJBatalTransaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgIGDBatalTransaksi.close(); 
							
					   };
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapIGDBatalTransaksi',
					handler: function()
					{
						frmDlgIGDBatalTransaksi.close();
					}
				}
			
			]
        }
    );

    return PnlLapIGDBatalTransaksi;
};


function ValidasiReportIGDBatalTransaksi()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapIGDBatalTransaksi').dom.value === '')
	{
		ShowPesanWarningIGDBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapIGDBatalTransaksi').getValue() === false && Ext.getCmp('Shift_1_LapIGDBatalTransaksi').getValue() === false && Ext.getCmp('Shift_2_LapIGDBatalTransaksi').getValue() === false && Ext.getCmp('Shift_3_LapIGDBatalTransaksi').getValue() === false){
		ShowPesanWarningIGDBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportIGDBatalTransaksi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDBatalTransaksi').dom.value > Ext.get('dtpTglAkhirLapIGDBatalTransaksi').dom.value)
    {
        ShowPesanWarningIGDBatalTransaksiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDBatalTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapIGDBatalTransaksi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapIGDBatalTransaksi_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapIGDBatalTransaksi',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapIGDBatalTransaksi',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};

function getItemIGDBatalTransaksi_Shift()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width:  395,
				height: 40,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
					{
						x: 0,
						y: 10,
						xtype:'label',
						text:'Shift :'
					}/* ,
					{
						x: 75,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapIGDBatalTransaksi',
						id : 'Shift_All_LapIGDBatalTransaksi',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapIGDBatalTransaksi').setValue(true);
								Ext.getCmp('Shift_2_LapIGDBatalTransaksi').setValue(true);
								Ext.getCmp('Shift_3_LapIGDBatalTransaksi').setValue(true);
								Ext.getCmp('Shift_1_LapIGDBatalTransaksi').disable();
								Ext.getCmp('Shift_2_LapIGDBatalTransaksi').disable();
								Ext.getCmp('Shift_3_LapIGDBatalTransaksi').disable();
							}else{
								Ext.getCmp('Shift_1_LapIGDBatalTransaksi').setValue(false);
								Ext.getCmp('Shift_2_LapIGDBatalTransaksi').setValue(false);
								Ext.getCmp('Shift_3_LapIGDBatalTransaksi').setValue(false);
								Ext.getCmp('Shift_1_LapIGDBatalTransaksi').enable();
								Ext.getCmp('Shift_2_LapIGDBatalTransaksi').enable();
								Ext.getCmp('Shift_3_LapIGDBatalTransaksi').enable();
							}
						}
					},
					{
						x: 145,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapIGDBatalTransaksi',
						id : 'Shift_1_LapIGDBatalTransaksi'
					},
					{
						x: 215,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapIGDBatalTransaksi',
						id : 'Shift_2_LapIGDBatalTransaksi'
					},
					{
						x: 285,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapIGDBatalTransaksi',
						id : 'Shift_3_LapIGDBatalTransaksi'
					} */
				]
			}
     
        ]
    }
    return items;
};
