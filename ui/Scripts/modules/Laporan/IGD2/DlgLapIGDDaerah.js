
var dsIGDDaerah;
var selectIGDDaerah;
var selectNamaIGDDaerah;
var now = new Date();
var selectSetDaerah;
var frmDlgIGDDaerah;
var varLapIGDDaerah= ShowFormLapIGDDaerah();

function ShowFormLapIGDDaerah()
{
    frmDlgIGDDaerah= fnDlgIGDDaerah();
    frmDlgIGDDaerah.show();
};

function fnDlgIGDDaerah()
{
    var winIGDDaerahReport = new Ext.Window
    (
        {
            id: 'winIGDDaerahReport',
            title: 'Laporan Laporan Instansi Gawat Darurat Pasien Per Daerah',
            closeAction: 'destroy',
            width:500,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDDaerah()]

        }
    );

    return winIGDDaerahReport;
};


function ItemDlgIGDDaerah()
{
    var PnlLapIGDDaerah = new Ext.Panel
    (
        {
            id: 'PnlLapIGDDaerah',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDDaerah_Tanggal(),getItemLapIGDDaerah_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapIGDDaerah',
                            handler: function()
                            {
                                if (ValidasiReportIGDDaerah() === 1)
                                {
                                        var criteria = GetCriteriaIGDDaerah();
                                        frmDlgIGDDaerah.close();
                                        loadlaporanIGD('0', 'rep020207', criteria);
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapIGDDaerah',
                            handler: function()
                            {
                                    frmDlgIGDDaerah.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapIGDDaerah;
};

function GetCriteriaIGDDaerah()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapIGDDaerah').dom.value !== '')
	{
		strKriteria = getnewformatdate(Ext.get('dtpTglAwalLapIGDDaerah').dom.value);
	};
	if (selectSetDaerah !== undefined)
	{
		strKriteria += '##@@##' + selectSetDaerah;
	};

	return strKriteria;
};


function ValidasiReportIGDDaerah()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapIGDDaerah').dom.value === '') || (Ext.get('cboDaerah').dom.value === '' || 
            Ext.get('cboDaerah').dom.value === 'Silahkan Pilih...'))
    {
            if(Ext.get('dtpTglAwalLapIGDDaerah').dom.value === '')
            {
                    ShowPesanWarningIGDDaerahReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('cboDaerah').dom.value === '' || Ext.get('cboDaerah').dom.value === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningIGDDaerahReport(nmGetValidasiKosong('Wilayah'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportIGDDaerah()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDDaerah').dom.value > Ext.get('dtpTglAkhirLapIGDDaerah').dom.value)
    {
        ShowPesanWarningIGDDaerahReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDDaerahReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapIGDDaerah_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                    mComboUnitLapIGDDaerah()
                ]
            }
        ]
    }
    return items;
};


function getItemLapIGDDaerah_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: 0.45,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 85,
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmPeriodeDlgRpt + ' ',
                    id: 'dtpTglAwalLapIGDDaerah',
                    format: 'M-Y',
                    value:now,
                    width:80
                }
            ]
        }
        ]
    }
    return items;
};


function mComboUnitLapIGDDaerah()
{
     var cboDaerah = new Ext.form.ComboBox
	(
		{
			id:'cboDaerah',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Wilayah ',
			width:80,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Prop'],[2, 'Kab'],[3, 'Kec'],[4, 'Kel']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetDaerah,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetDaerah=b.data.displayText ;
				}
			}
		}
	);
	return cboDaerah;
};