
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsPelayananDokter;
var selectNamaPelayananDokter;
var now = new Date();
var selectSetPerseorangan;
var frmDlgPelayananDokter;
var varLapPelayananDokter= ShowFormLapPelayananDokter();
var selectSetUmum;
var selectSetkelpas;



var selectSetPilihankelompokPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapPelayananDokter()
{
    frmDlgPelayananDokter= fnDlgPelayananDokter();
    frmDlgPelayananDokter.show();
};

function fnDlgPelayananDokter()
{
    var winPelayananDokterReport = new Ext.Window
    (
        {
            id: 'winPelayananDokterReport',
            title: 'Laporan Transaksi Jasa Pelayanan Dokter Per Pasien',
            closeAction: 'destroy',
            width:400,
            height: 285,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgPelayananDokter()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganIGD').hide();
                Ext.getCmp('cboAsuransiIGD').hide();
                Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
                Ext.getCmp('cboUmumIGD').show();
            }
        }

        }
    );

    return winPelayananDokterReport;
};


function ItemDlgPelayananDokter()
{
    var PnlLapPelayananDokterIGD = new Ext.Panel
    (
        {
            id: 'PnlLapPelayananDokterIGD',
            fileUpload: true,
            layout: 'form',
			//width:400,
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapPelayananDokter_Atas(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapPelayananDokter',
                            handler: function()
                            {
                               /* if (ValidasiReportPelayananDokter() === 1)
                               { */		loadMask.show();
                                        var criteria = GetCriteriaPelayananDokter();
                                        frmDlgPelayananDokter.close();
                                        loadlaporanIGD('0', 'rep020209', criteria, function(){
											loadMask.hide();
										});
								/* }; */
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapPelayananDokter',
                            handler: function()
                            {
                                    frmDlgPelayananDokter.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapPelayananDokterIGD;
};

function getItemLapPelayananDokter_Atas()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 200,
            anchor: '100% 100%',
            items: [
			{
				xtype: 'checkboxgroup',
				width:210,
				x: 120,
				y: 10,
				items: 
				[
					{
						
						boxLabel: 'Pendaftaran',
						name: 'cbPendaftaran_PelayananDokterIGD',
						id : 'cbPendaftaran_PelayananDokterIGD'
					},
					{
						boxLabel: 'Tindak IGD',
						name: 'cbTindakIGD_PelayananDokter',
						id : 'cbTindakIGD_PelayananDokter'
					}
			   ]
			},
			{
				x: 10,
				y: 40,
				xtype: 'label',
				text: 'Unit '
			}, {
				x: 110,
				y: 40,
				xtype: 'label',
				text: ' : '
			},
				mCombounitPelayananDokter(),
			{
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Dokter '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
			mComboDokterPelayananDokter(),
			{
				x: 10,
				y: 100,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, {
				x: 110,
				y: 100,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 100,
				xtype: 'datefield',
				id: 'dtpTglAwalLapPelayananDokter',
				format: 'd/M/Y',
				value: tigaharilalu
			}, {
				x: 230,
				y: 100,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 260,
				y: 100,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapPelayananDokter',
				format: 'd/M/Y',
				value: now,
				width: 100
			},{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 130,
				xtype: 'label',
				text: ' : '
			},
				mComboKelompokPasienPelayananDokter(),
				mComboUmumPelayananDokter(),
				mComboPerseoranganPelayananDokter(),
				mComboAsuransiPelayananDokter(),
				mComboPerusahaanPelayananDokter()
            ]
        }]
    };
    return items;
};

function GetCriteriaPelayananDokter()
{
	var strKriteria = '';
	
	if (Ext.getCmp('cbPendaftaran_PelayananDokterIGD').getValue() === true && Ext.getCmp('cbTindakIGD_PelayananDokter').getValue() === false){
		strKriteria ='autocas';
		strKriteria += '##@@##' + 1;
	}else if(Ext.getCmp('cbPendaftaran_PelayananDokterIGD').getValue() === false && Ext.getCmp('cbTindakIGD_PelayananDokter').getValue() === true) {
		strKriteria ='autocas';
		strKriteria += '##@@##' + 2;
	} else if(Ext.getCmp('cbPendaftaran_PelayananDokterIGD').getValue() === true && Ext.getCmp('cbTindakIGD_PelayananDokter').getValue() === true){
		strKriteria ='NoCheked';
		strKriteria += '##@@##' + 3;
	} else{
		strKriteria ='NoCheked';
		strKriteria += '##@@##' + 4;
	}
		
	if(Ext.getCmp('cbounitRequestEntryPelayananDokterIGD').getValue() === '' || Ext.getCmp('cbounitRequestEntryPelayananDokterIGD').getValue() === 'UGD'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + 'UGD';
	} else{
		strKriteria += '##@@##' + Ext.get('cbounitRequestEntryPelayananDokterIGD').getValue();;
		strKriteria += '##@@##' + Ext.getCmp('cbounitRequestEntryPelayananDokterIGD').getValue();
	}
	
	if(Ext.getCmp('cboDokterPelayananDokterIGD').getValue() === '' || Ext.getCmp('cboDokterPelayananDokterIGD').getValue() === 'Semua'){
		strKriteria += '##@@##' + 'Dokter';
		strKriteria += '##@@##' + 'Semua';
	} else{
		strKriteria += '##@@##' + Ext.get('cboDokterPelayananDokterIGD').getValue();;
		strKriteria += '##@@##' + Ext.getCmp('cboDokterPelayananDokterIGD').getValue();
	}
	
	if (Ext.get('dtpTglAwalLapPelayananDokter').getValue() !== ''){
		strKriteria += '##@@##' + Ext.get('dtpTglAwalLapPelayananDokter').getValue();
	}
	if (Ext.get('dtpTglAkhirLapPelayananDokter').getValue() !== ''){
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapPelayananDokter').getValue();
	}
	
	if (Ext.getCmp('cboPilihanPelayananDokterkelompokPasienIGD').getValue() !== '')
	{
		if (Ext.get('cboPilihanPelayananDokterkelompokPasienIGD').getValue() === 'Semua') {
            strKriteria += '##@@##' + 'Kelpas';
			strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
        } else if (Ext.get('cboPilihanPelayananDokterkelompokPasienIGD').getValue() === 'Perseorangan'){

			strKriteria += '##@@##' + 'Perseorangan';
            strKriteria += '##@@##' + Ext.get('cboPerseoranganIGD').getValue();
            strKriteria += '##@@##' + Ext.getCmp('cboPerseoranganIGD').getValue();
        } else if (Ext.get('cboPilihanPelayananDokterkelompokPasienIGD').getValue() === 'Perusahaan'){
            
			if(Ext.getCmp('cboPerusahaanRequestEntryIGD').getValue() === '')
            {
				strKriteria += '##@@##' + 'Kelpas';
				strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Perusahaan';
				strKriteria += '##@@##' + Ext.get('cboPerusahaanRequestEntryIGD').getValue();
                strKriteria += '##@@##' + Ext.getCmp('cboPerusahaanRequestEntryIGD').getValue();
            }
        } else {
            
			if(Ext.getCmp('cboAsuransiIGD').getValue() === '')
            {
				strKriteria += '##@@##' + 'Kelpas';
				strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Asuransi';
				strKriteria += '##@@##' + Ext.get('cboAsuransiIGD').getValue();
                strKriteria += '##@@##' + Ext.getCmp('cboAsuransiIGD').getValue();
            }
            
        }
	}  else{
            strKriteria += '##@@##' + 'Kelpas';
			strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
	}

	
	return strKriteria;
};

function ValidasiReportPelayananDokter()
{
    var x=1;
    if(Ext.getCmp('dtpTglAwalLapPelayananDokter').getValue() > Ext.getCmp('dtpTglAkhirLapPelayananDokter').getValue())
    {
        ShowPesanWarningPelayananDokterReportIGD('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }
	if(Ext.getCmp('cboPilihanPelayananDokterkelompokPasienIGD').getValue() === ''){
		ShowPesanWarningPelayananDokterReportIGD('Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	if(Ext.getCmp('cboPerusahaanRequestEntryIGD').getValue() === '' &&  Ext.getCmp('cboAsuransiIGD').getValue() === '' &&  Ext.getCmp('cboPerseoranganIGD').getValue() === ''){
		ShowPesanWarningPelayananDokterReportIGD('Sub Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningPelayananDokterReportIGD(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function mComboDokterPelayananDokter()
{
	var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokterLap',
				param: "where d.nama not in('-') and dk.kd_unit='3' order by d.nama"
			}
		}
	);
    var cboPilihanPelayananDokterIGD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboDokterPelayananDokterIGD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanPelayananDokterIGD;
};

function mComboKelompokPasienPelayananDokter()
{
    var cboPilihanPelayananDokterkelompokPasienIGD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 130,
                id:'cboPilihanPelayananDokterkelompokPasienIGD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                value:1,
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanPelayananDokterkelompokPasienIGD;
};

function mComboPerseoranganPelayananDokter()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganIGD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 160,
                id:'cboPerseoranganIGD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganIGD;
};

function mComboUmumPelayananDokter()
{
    var cboUmumIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 160,
			id:'cboUmumIGD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			valueField: 'Id',
            displayField: 'displayText',
			fieldLabel: '',
			width:240,
			value:1,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
									'Id',
									'displayText'
							],
					data: [[1, 'Semua']]
					}
			),
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumIGD;
};

function mComboPerusahaanPelayananDokter()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 order by CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 160,
		    id: 'cboPerusahaanRequestEntryIGD',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryIGD;
};

function mComboAsuransiPelayananDokter()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 order by CUSTOMER"
            }
        }
    );
    var cboAsuransiIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 160,
			id:'cboAsuransiIGD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiIGD;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganIGD').show();
        Ext.getCmp('cboAsuransiIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
        Ext.getCmp('cboUmumIGD').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganIGD').hide();
        Ext.getCmp('cboAsuransiIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryIGD').show();
        Ext.getCmp('cboUmumIGD').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganIGD').hide();
        Ext.getCmp('cboAsuransiIGD').show();
        Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
        Ext.getCmp('cboUmumIGD').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganIGD').hide();
        Ext.getCmp('cboAsuransiIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
        Ext.getCmp('cboUmumIGD').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganIGD').hide();
        Ext.getCmp('cboAsuransiIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
        Ext.getCmp('cboUmumIGD').show();
   }
}

function mCombounitPelayananDokter()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=3 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryPelayananDokterIGD = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 40,
            id: 'cbounitRequestEntryPelayananDokterIGD',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
			value: 'UGD',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryPelayananDokterIGD;
};
