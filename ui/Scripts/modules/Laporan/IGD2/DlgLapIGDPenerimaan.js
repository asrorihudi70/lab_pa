var DlgLapIGDPenerimaan={
	vars:{
		comboSelect:null
	},
	coba:function(){
		var $this=this;
		$this.DateField.startDate
	},
	ArrayStore:{
		dokter:new Ext.data.ArrayStore({fields:[]})
	},
	CheckboxGroup:{
		shift:null
	},
	ComboBox:{
		kelPasien1:null,
		combo1:null,
		combo2:null,
		combo3:null,
		combo0:null,
		poliklinik:null,
		dokter:null
	},
	DataStore:{
		combo2:null,
		combo3:null,
		poliklinik:null,
		combo1:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	comboOnSelect:function(val){
		var $this=this;
		$this.ComboBox.combo0.hide();
		$this.ComboBox.combo1.hide();
		$this.ComboBox.combo2.hide();
		$this.ComboBox.combo3.hide();
		if(val==-1){
			$this.ComboBox.combo0.show();
			
		}else if(val==0){
			$this.ComboBox.combo1.show();
		}else if(val==1){
			$this.ComboBox.combo2.show();
		}else if(val==2){
			$this.ComboBox.combo3.show();
		}
	},
	initPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		params['start_date']=timestimetodate($this.DateField.startDate.getValue());
		params['last_date']=timestimetodate($this.DateField.endDate.getValue());
		params['pasien']=$this.ComboBox.kelPasien1.getValue();
		var pasien=parseInt($this.ComboBox.kelPasien1.getValue());
		if(pasien>=0){
			if(pasien==0){
				params['kd_customer']=$this.ComboBox.combo1.getValue();
			}else if(pasien==1){
				params['kd_customer']=$this.ComboBox.combo2.getValue();
			}else if(pasien==2){
				params['kd_customer']=$this.ComboBox.combo3.getValue();
			}
		}
		if($this.ComboBox.dokter.getValue()!='Semua'){
			params['kd_dokter']=$this.ComboBox.dokter.getValue();
		}
		if($this.ComboBox.poliklinik.getValue()!='Semua'){
			params['kd_klinik']=$this.ComboBox.poliklinik.getValue();
		}
		var shift=$this.CheckboxGroup.shift.items.items;
		var shifta=false;
		for(var i=0;i<shift.length ; i++){
			params['shift'+i]=shift[i].checked;
			if(shift[i].checked==true)shifta=true;
		}
		if(shifta==true){
			$.ajax({
				type: 'POST',
				dataType:'JSON',
				data:params,
				url:baseURL + "index.php/gawat_darurat/lap_penerimaan/printData",
				success: function(r){
					loadMask.hide();
					if(r.processResult=='SUCCESS'){
						$this.Window.main.close();
						window.open(r.data, '_blank', 'location=0,resizable=1', false);
					}else{
						Ext.Msg.alert('Gagal',r.processMessage);
					}
				},
				error: function(jqXHR, exception) {
					loadMask.hide();
					Nci.ajax.ErrorMessage(jqXHR, exception);
				}
			});
		}else{
			loadMask.hide();
			Ext.Msg.alert('Gagal','Pilih Shift ');
		}
	},
	getDokter:function(){
		var $this=this;
		$this.ComboBox.dokter = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.ArrayStore.dokter,
            width: 200,
            value:'Semua',
            valueField: 'kd_dokter',
            displayField: 'nama',
            listeners:{
            	'select': function(a, b, c){
                               
            	}
            }
        });		    
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gawat_darurat/lap_penerimaan/getDokter",
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.dokter.loadData([],false);
					for(var i=0,iLen=r.data.length; i<iLen ;i++){
						$this.ArrayStore.dokter.add(new $this.ArrayStore.dokter.recordType(r.data[i]));
					}
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
		return $this.ComboBox.dokter;
	},
	getPoliklinik:function(){
		var $this=this;
		var Field = ['KD_UNIT','NAMA_UNIT'];
		$this.DataStore.poliklinik = new WebApp.DataStore({fields: Field});
		$this.DataStore.poliklinik.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=3 and type_unit=false"
            }
        });
		$this.ComboBox.poliklinik = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.DataStore.poliklinik,
            width: 200,
            value:'Semua',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            listeners:{
            	'select': function(a, b, c){
                               
            	}
            }
        });		    
	    return $this.ComboBox.poliklinik;
	},
	getCombo0:function(){
		var $this=this;
		$this.ComboBox.combo0 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: new Ext.data.ArrayStore({
				id: 0,
				fields:['Id','displayText'],
				data: [[1, 'Semua']]
			}),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			disabled:true,
			listeners:{
				'select': function(a,b,c){
					selectSetUmum=b.data.displayText ;
				}
			}
		});
		return $this.ComboBox.combo0;
	},
	getCombo1:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo1 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo1.load({
    		params:{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 ORDER BY CUSTOMER '
			}
		});
    	$this.ComboBox.combo1 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hidden:true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: $this.DataStore.combo1,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
			listeners:{
				'select': function(a,b,c){
				}
			}
		});
		return $this.ComboBox.combo1;
	},
	getCombo2:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo2 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo2.load({
		    params:{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER '
			}
		});
   		$this.ComboBox.combo2 = new Ext.form.ComboBox({
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    hidden:true,
		    store: $this.DataStore.combo2,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			value:'Semua',
			width:150,
		    listeners:{
			    'select': function(a,b,c){
				}
			}
		});
    	return $this.ComboBox.combo2;
	},
	getCombo3:function(){
		var $this=this;
		var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    	$this.DataStore.combo3 = new WebApp.DataStore({fields: Field_poli_viDaftar});

		$this.DataStore.combo3.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER "
            }
        });
    	$this.ComboBox.combo3 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:150,
			hidden:true,
			store: $this.DataStore.combo3,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
//			value: 0,
			listeners:{
				'select': function(a,b,c){
//					selectSetAsuransi=b.data.KD_CUSTOMER ;
//					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		});
		return $this.ComboBox.combo3;
	},
	init:function(){
		var $this=this;
		$this.Window.main=new Ext.Window({
			width: 400,
			height:220,
			modal:true,
			title:'Laporan Penerimaan Per Pasien',
			layout:'fit',
			items:[
				new Ext.Panel({
					bodyStyle:'padding: 10px',
					layout:'form',
					border:false,
					fbar:[
						new Ext.Button({
							text:'Ok',
							handler:function(){
								$this.initPrint()
							}
						}),
						new Ext.Button({
							text:'Batal',
							handler:function(){
								$this.Window.main.close();
							}
						})
					],
					items:[
						{
							layout:'column',
							border:false,
							items:[
								{
									xtype:'displayfield',
									width: 100,
									value:'Periode : '
								},
								$this.DateField.startDate=new Ext.form.DateField({
									value: new Date(),
									format:'d/M/Y'
								}),
								{
									xtype:'displayfield',
									width: 30,
									value:'&nbsp;s/d&nbsp;'
								},
								$this.DateField.endDate=new Ext.form.DateField({
									value: new Date(),
									format:'d/M/Y'
								})
							]
						},{
							layout:'column',
							border:false,
							bodyStyle:'margin-top: 5px',
							items:[
								{
									xtype:'displayfield',
									width: 100,
									value:'Kelompok Pasien : '
								},
								$this.ComboBox.kelPasien1=new Ext.form.ComboBox({
					                triggerAction: 'all',
					                lazyRender:true,
					                mode: 'local',
					                width: 100,
					                selectOnFocus:true,
					                forceSelection: true,
					                emptyText:'Silahkan Pilih...',
					                fieldLabel: 'Pendaftaran Per Shift ',
					                store: new Ext.data.ArrayStore({
			                            id: 0,
			                            fields:[
			                                    'Id',
			                                    'displayText'
			                            ],
					                    data: [[-1, 'Semua'],[0, 'Perseorangan'],[1, 'Perusahaan'], [2, 'Asuransi']]
				                    }),
					                valueField: 'Id',
					                displayField: 'displayText',
					                value:'Semua',
					                listeners:{
					                    'select': function(a,b,c){
					                            $this.vars.comboSelect=b.data.displayText;
					                            $this.comboOnSelect(b.data.Id);
					                    }
					                }
				           		}),
				           		{
									xtype:'displayfield',
									width: 10,
									value:'&nbsp;'
								},
				           		$this.getCombo0(),
				           		$this.getCombo1(),
				           		$this.getCombo2(),
				           		$this.getCombo3()
							]
						},{
							layout:'column',
							border:false,
							bodyStyle:'margin-top: 5px',
							items:[
								{
									xtype:'displayfield',
									width: 100,
									value:'Poliklinik : '
								},
								$this.getPoliklinik()
							]
						},{
							layout:'column',
							border:false,
							bodyStyle:'margin-top: 5px',
							items:[
								{
									xtype:'displayfield',
									width: 100,
									value:'Dokter : '
								},
								$this.getDokter()
							]
						},{
							layout:'column',
							border:false,
							bodyStyle:'margin-top: 5px',
							items:[
								$this.CheckboxGroup.shift=new Ext.form.CheckboxGroup({
			                        xtype: 'checkboxgroup',
			                        items: [
	                                    {boxLabel: 'Semua',checked:true,name: 'Shift_All_LabRegis',id : 'Shift_All_LabRegis',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_LabRegis').setValue(true);Ext.getCmp('Shift_2_LabRegis').setValue(true);Ext.getCmp('Shift_3_LabRegis').setValue(true);Ext.getCmp('Shift_1_LabRegis').disable();Ext.getCmp('Shift_2_LabRegis').disable();Ext.getCmp('Shift_3_LabRegis').disable();}else{Ext.getCmp('Shift_1_LabRegis').setValue(false);Ext.getCmp('Shift_2_LabRegis').setValue(false);Ext.getCmp('Shift_3_LabRegis').setValue(false);Ext.getCmp('Shift_1_LabRegis').enable();Ext.getCmp('Shift_2_LabRegis').enable();Ext.getCmp('Shift_3_LabRegis').enable();}}},
	                                    {boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1_LabRegis',id : 'Shift_1_LabRegis'},
	                                    {boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2_LabRegis',id : 'Shift_2_LabRegis'},
	                                    {boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3_LabRegis',id : 'Shift_3_LabRegis'}
	                                ]
                                })
							]
						}
					]
				})
			]
		}).show();
	}
};
DlgLapIGDPenerimaan.init();
