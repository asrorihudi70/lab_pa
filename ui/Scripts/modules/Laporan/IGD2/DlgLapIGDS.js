
var dsIGDS;
var selectIGDS;
var selectNamaIGDS;
var now = new Date();

var frmDlgIGDS;
var varLapIGDS= ShowFormLapIGDS();

function ShowFormLapIGDS()
{
    frmDlgIGDS= fnDlgIGDS();
//    frmDlgIGDS.show();
    frmDlgIGDS.close();
    //ShowReport('0', 1010202, '');
    reportchart();
    
};

function fnDlgIGDS()
{
    var winIGDSReport = new Ext.Window
    (
        {
            id: 'winIGDSReport',
            title: 'Laporan Rawat Jalan Sumary',
            closeAction: 'destroy',
            width:500,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDS()]

        }
    );

    return winIGDSReport;
};


function ItemDlgIGDS()
{
    var PnlLapIGDS = new Ext.Panel
    (
        {
            id: 'PnlLapIGDS',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDS_Tanggal(), getItemLapIGDS_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapIGDS',
                            handler: function()
                            {
                                if (ValidasiReportIGDS() === 1)
                                {
                                    //if (ValidasiTanggalReportIGDS() === 1)
                                    //{
                                        //var criteria = GetCriteriaIGDS();
                                        frmDlgIGDS.close();
                                        reportchart();
                                        //ShowReport('0', 1010202, '');
//                                        window.open("http://localhost/Simrs/base/tmp/1421630699Tmp.pdf");
                                    //};
                                    //alert(tmp);
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapIGDS',
                            handler: function()
                            {
                                    frmDlgIGDS.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapIGDS;
};

function GetCriteriaIGDS()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapIGDS').dom.value != '')
	{
		strKriteria = Ext.get('dtpTglAwalLapIGDS').dom.value;
	};

	if (Ext.get('dtpTglAkhirLapIGDS').dom.value != '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapIGDS').dom.value;
                strKriteria += '##@@##' + 'group by k.kd_unit, u.nama_unit';
	};

//	if (selectIGDS != undefined)
//	{
//		strKriteria += '##@@##' + selectIGDS;
//		strKriteria += '##@@##' + selectNamaIGDS ;
//	};

	return strKriteria;
};


function ValidasiReportIGDS()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapIGDS').dom.value === '') || (Ext.get('dtpTglAkhirLapIGDS').dom.value === '') || (selectIGDS === undefined) || (Ext.get('comboUnitLapIGDS').dom.value === ''))
    {
            if(Ext.get('dtpTglAwalLapIGDS').dom.value === '')
            {
                    ShowPesanWarningIGDSReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('dtpTglAkhirLapIGDS').dom.value === '')
            {
                    ShowPesanWarningIGDSReport(nmGetValidasiKosong(nmFinishDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
//            else if(Ext.get('comboUnitLapIGDS').dom.value === '' )
//            {
//                    ShowPesanWarningIGDSReport(nmGetValidasiKosong(nmDeptDlgRpt),nmTitleFormDlgReqCMRpt);
//                    x=0;
//            };
    };

    return x;
};

function ValidasiTanggalReportIGDS()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDS').dom.value > Ext.get('dtpTglAkhirLapIGDS').dom.value)
    {
        ShowPesanWarningIGDSReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDSReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapIGDS_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:1,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
//                    mComboUnitLapIGDS()
                ]
            }
        ]
    }
    return items;
};


function getItemLapIGDS_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: 0.45,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 85,
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmPeriodeDlgRpt + ' ',
                    id: 'dtpTglAwalLapIGDS',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        },
        {
            columnWidth: 0.48,
            layout: 'form',
            border: false,
                    labelWidth: 25,
            labelAlign: 'right',
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmSd + ' ',
                    id: 'dtpTglAkhirLapIGDS',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        }
        ]
    }
    return items;
};


function mComboUnitLapIGDS()
{
    var dataSource_All = new Ext.data.Store({
	reader: new Ext.data.JsonReader
		({
			//fields: ['code',  'NoFaktur',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			fields: ['KD_UNIT', 'NAMA_UNIT']
		}),
	data:
		[
			{ KD_UNIT:'xxx', NAMA_UNIT:'Semua Unit'},
                        { KD_UNIT:'208', NAMA_UNIT:'Anak'},
                        { KD_UNIT:'209', NAMA_UNIT:'Bedah'},
                        { KD_UNIT:'215', NAMA_UNIT:'DM'},
                        { KD_UNIT:'202', NAMA_UNIT:'Gigi'},
                        { KD_UNIT:'203', NAMA_UNIT:'Gizi'},
                        { KD_UNIT:'206', NAMA_UNIT:'Interna'},
                        { KD_UNIT:'216', NAMA_UNIT:'Jantung'},
                        { KD_UNIT:'214', NAMA_UNIT:'Jiwa'},
                        { KD_UNIT:'207', NAMA_UNIT:'Kandungan'},
                        { KD_UNIT:'213', NAMA_UNIT:'KIA'},
                        { KD_UNIT:'212', NAMA_UNIT:'Kulit Kelamin'},
                        { KD_UNIT:'211', NAMA_UNIT:'Mata'},
                        { KD_UNIT:'204', NAMA_UNIT:'Syaraf'},
                        { KD_UNIT:'210', NAMA_UNIT:'THT'},
                        { KD_UNIT:'201', NAMA_UNIT:'Umum'},
                        { KD_UNIT:'205', NAMA_UNIT:'VCT'},
		]
	});
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    dsIGDS = new WebApp.DataStore({ fields: Field });

    dsIGDS.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    );
    
  var comboUnitLapIGDS = new Ext.form.ComboBox
    (
        {
            id:'comboUnitLapIGDS',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Poliklinik ',
            align:'Right',
            anchor:'99%',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store:dataSource_All,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectIGDS=b.data.KD_UNIT ;
                    selectNamaIGDS=b.data.NAMA_UNIT ;
                }
            }
        }
    );

//    dsIGDS.load
//    (
//        {
//            params:
//            {
//                Skip: 0,
//                Take: 1000,
//                //Sort: 'DEPT_ID',
//                Sort: 'NAMA_UNIT',
//                Sortdir: 'ASC',
//                target:'ViewSetupUnit',
//                param: ''
//            }
//        }
//    );
    return comboUnitLapIGDS;
};

function generateData(){
    var data = [];
    for(var i = 0; i < 12; ++i){
        data.push([Date.monthNames[i], (Math.floor(Math.random() *  11) + 1) * 100]);
    }
    return data;
}


function reportchart()
{
    var store = new Ext.data.ArrayStore({
        fields: ['month', 'hits'],
        data: generateData()
    });

    new Ext.Panel({
        width: 700,
        height: 400,
        //renderTo: document.body,
        title: 'Column Chart with Reload - Hits per Month',
        tbar: [{
            text: 'Load new data set',
            handler: function(){
                store.loadData(generateData());
            }
        }],
        items: {
            xtype: 'columnchart',
            store: store,
            yField: 'hits',
	    url: 'C:/xampp/htdocs/belajarlaporan/image/charts.swf',
            xField: 'month',
            xAxis: new Ext.chart.CategoryAxis({
                title: 'Month'
            }),
            yAxis: new Ext.chart.NumericAxis({
                title: 'Hits'
            }),
            extraStyle: {
               xAxis: {
                    labelRotation: -90
                }
            }
        }
    });
}