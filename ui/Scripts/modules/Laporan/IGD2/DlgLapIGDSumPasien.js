
var dsIGDSumPasien;
var selectIGDSumPasien;
var selectNamaIGDSumPasien;
var now = new Date();
var selectSetUNIT='Inst. Rawat Darurat';
var frmDlgIGDSumPasien;
var varLapIGDSumPasien= ShowFormLapIGDSumPasien();
var dsIGD;
function ShowFormLapIGDSumPasien()
{
    frmDlgIGDSumPasien= fnDlgIGDSumPasien();
    frmDlgIGDSumPasien.show();
};

function fnDlgIGDSumPasien()
{
    var winIGDSumPasienReport = new Ext.Window
    (
        {
            id: 'winIGDSumPasienReport',
            title: 'Laporan Summary Pasien IGD Per Hari',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDSumPasien()]

        }
    );

    return winIGDSumPasienReport;
};


function mComboUnitLapIGD()
{


    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    dsIGD = new WebApp.DataStore({ fields: Field });

    dsIGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ComboUnitIGD',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    );
    
  var comboUnitLapIGD = new Ext.form.ComboBox
    (
        {
            id:'comboUnitLapIGD',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Poliklinik ',
            align:'Right',
            anchor:'100%',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store:dsIGD,
			value: selectSetUNIT,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetUNIT=b.data.KD_UNIT ;
				alert(selectSetUNIT);
                    //selectSetSumPasien=b.data.NAMA_UNIT ;
                }
            }
        }
    );

    return comboUnitLapIGD;
};

function ItemDlgIGDSumPasien()
{
    var PnlLapIGDSumPasien = new Ext.Panel
    (
        {
            id: 'PnlLapIGDSumPasien',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDSumPasien_Tanggal(),getItemLapRWJcborwj(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'10px','margin-top':'0px'},
                            id: 'btnOkLapIGDSumPasien',
                            handler: function()
                            {
                               if (ValidasiReportIGDSumPasien() === 1)
                               {
                                        var criteria = GetCriteriaIGDSumPasien();
                                        frmDlgIGDSumPasien.close();
										
                                        loadlaporanIGD('0', 'rep020202', criteria);
                               };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
						 style:{'margin-left':'10px','margin-top':'0px'},
                            id: 'btnCancelLapIGDSumPasien',
                            handler: function()
                            {
                                    frmDlgIGDSumPasien.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapIGDSumPasien;
};

function GetCriteriaIGDSumPasien()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapIGDSumPasien').dom.value !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapIGDSumPasien').dom.value;
	};
	if (Ext.get('dtpTglAkhirLapSumPasien').dom.value !== '')
	{
		strKriteria +='##@@##'+ Ext.get('dtpTglAkhirLapSumPasien').dom.value;
	};
	if (selectSetUNIT !== undefined)
	{
		strKriteria += '##@@##' + selectSetUNIT;
	};

	return strKriteria;
};


function ValidasiReportIGDSumPasien()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapIGDSumPasien').dom.value === '') || (Ext.get('comboUnitLapIGD').dom.value === '' || 
            Ext.get('comboUnitLapIGD').dom.value === 'Silahkan Pilih...'))
    {
            if(Ext.get('dtpTglAwalLapIGDSumPasien').dom.value === '')
            {
                    ShowPesanWarningIGDSumPasienReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('comboUnitLapIGD').dom.value === '' || Ext.get('comboUnitLapIGD').dom.value === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningIGDSumPasienReport(nmGetValidasiKosong('Unit'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportIGDSumPasien()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDSumPasien').dom.value > Ext.get('dtpTglAkhirLapIGDSumPasien').dom.value)
    {
        ShowPesanWarningIGDSumPasienReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDSumPasienReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapIGDSumPasien_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapIGDSumPasien_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .50,
			    layout: 'form',
				 labelWidth:70,
				  border: false,
				 
			    items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'periode ',
						id: 'dtpTglAwalLapIGDSumPasien',
						format: 'd-M-Y',
						value:now,
					     anchor: '99%'
					},
				
					
				]
			},
			
			{
			    columnWidth: .49,
			    layout: 'form',
			    border: false,
				labelWidth:20,
			    items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'to',
						id: 'dtpTglAkhirLapSumPasien',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};
function getItemLapRWJcborwj()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .99,
			    layout: 'form',
				labelWidth:70,
				  border: false,
			    items:
				[
				
					mComboUnitLapIGD()
					
				]
			},
			
			
     
        ]
    }
    return items;
};


