var polipilihanpasien;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsIGDTransaksi;
var selectNamaIGDTransaksi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgIGDTransaksi;
var varLapIGDTransaksi= ShowFormLapIGDTransaksi();
var selectSetUmum;
var selectSetkelpas;
var winIGDTransaksiReport;
function ShowFormLapIGDTransaksi()
{
    frmDlgIGDTransaksi= fnDlgIGDTransaksi();
    frmDlgIGDTransaksi.show();
};

function fnDlgIGDTransaksi()
{
     winIGDTransaksiReport = new Ext.Window
    (
        {
            id: 'winIGDTransaksiReport',
            title: 'Laporan ',
            closeAction: 'destroy',
            width:400,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDTransaksi()],
            listeners:
        {
            activate: function()
            {
              
            }
        }

        }
    );

     winIGDTransaksiReport.show();
	dataaddnew();
};

function dataaddnew()
{
				Ext.getCmp('cboPerseoranganTrIGD').hide();
                Ext.getCmp('cboAsuransiTrIGD').hide();
                Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
                Ext.getCmp('cboUmumTrIGD').show();
				Ext.getCmp('cboPilihanIGDTransaksikelompokPasien').setValue('Semua');
				Ext.getCmp('cboUmumTrIGD').setValue('Semua');
				Ext.getCmp('cboPilihanIGDTransaksi').setValue('UGD');
				
}


function ItemDlgIGDTransaksi()
{
    var PnlLapIGDTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapIGDTransaksi',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [	getItemLapIGDTransaksi_tengah(),
                getItemLapIGDTransaksi_Atas(),
                getItemLapIGDTransaksi_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapIGDTransaksi',
                            handler: function()
                            {
                               if (ValidasiReportIGDTransaksi() === 1)
                               {
										loadMask.show();                                       
										var criteria = GetCriteriaIGDTransaksi();
                             
										loadlaporanIGD('0', 'rep020211', criteria);
										loadMask.hide();
										 winIGDTransaksiReport.close();
								};
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapIGDTransaksi',
                            handler: function()
                            {
                            winIGDTransaksiReport.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapIGDTransaksi;
};

function GetCriteriaIGDTransaksi()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalFilterIGDTransaksi').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalFilterIGDTransaksi').getValue();
	}
	if (Ext.get('dtpTglAkhirFilterIGDTransaksi').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterIGDTransaksi').getValue();
	}

	if(Ext.getCmp('cboPilihanIGDTransaksi').getValue() !== ''|| Ext.getCmp('cboPilihanIGDTransaksi').getValue() === '')
	{
		if (Ext.get('cboPilihanIGDTransaksi').getValue()=== ''){
			
			
		}  else {
		strKriteria += '##@@##' + polipilihanpasien
		} 
	}
       
	if (Ext.getCmp('cboPilihanIGDTransaksikelompokPasien').getValue() !== '')
	{
		if (Ext.get('cboPilihanIGDTransaksikelompokPasien').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
			 strKriteria += '##@@##' ;
        } 
		else if (Ext.get('cboPilihanIGDTransaksikelompokPasien').getValue() === 'Perseorangan')
		{
			
			if(Ext.getCmp('cboperoranganRequestEntryTrIGD').getValue() === '')
            {
                strKriteria += '##@@##' + '0';
                strKriteria += '##@@##' + 'NULL';
				 strKriteria += '##@@##' ;
            }else{
                strKriteria += '##@@##' + '0';
                strKriteria += '##@@##' + selectsetperusahaan;
				 strKriteria += '##@@##';
            }
        } else if (Ext.get('cboPilihanIGDTransaksikelompokPasien').getValue() === 'Perusahaan'){
            
			if(Ext.getCmp('cboPerusahaanRequestEntryTrIGD').getValue() === '')
            {
                strKriteria += '##@@##' + '1';
                strKriteria += '##@@##' + 'NULL';
				 strKriteria += '##@@##' ;
            }else{
                strKriteria += '##@@##' + '1';
                strKriteria += '##@@##' + selectsetperusahaan;
				strKriteria += '##@@##' ;
            }
        } else {
            
			if(Ext.getCmp('cboAsuransiTrIGD').getValue() === '')
            {
                strKriteria += '##@@##' + '2';
                strKriteria += '##@@##' + 'NULL';
				 strKriteria += '##@@##' ;
            }else{
                strKriteria += '##@@##' + '2';
                strKriteria += '##@@##' + selectSetAsuransi;
				strKriteria += '##@@##' ;
            }
            
        } 
	}   
	if (Ext.getCmp('Shift_All_IGDTransaksi').getValue() === true)
	{
		
		strKriteria +=  '1';
		strKriteria += ',' + '2';
		strKriteria += ',' + '3,';
		strKriteria += '##@@##';
		strKriteria += '4';
	}else{
		if (Ext.getCmp('Shift_1_IGDTransaksi').getValue() === true)
		{
		
			strKriteria += '1,';
		}
		if (Ext.getCmp('Shift_2_IGDTransaksi').getValue() === true)
		{
			
			strKriteria += '2,';
		}
		if (Ext.getCmp('Shift_3_IGDTransaksi').getValue() === true)
		{
			
			strKriteria += '3,';
			strKriteria += '##@@##';
			strKriteria += '4';
		}
	}
	if (Ext.getCmp('PendaftaranIGDTransaksi').getValue() === true)
	{
	strKriteria += '##@@##'+'ya';
	} 
	if (Ext.getCmp('PendaftaranIGDTransaksi').getValue() === false)
	{
	strKriteria += '##@@##'+'tidak';
	} 
	if (Ext.getCmp('TindakanIGDTransaksi').getValue() === true)
	{
	strKriteria += '##@@##'+'ya';
	}
	if (Ext.getCmp('TindakanIGDTransaksi').getValue() === false)
	{
	strKriteria += '##@@##'+'tidak';
	}
	return strKriteria;
};

function getKodeReportIGDTransaksi()
{   var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanIGDTransaksi').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanIGDTransaksi').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportIGDTransaksi()
{
    var x=1;
	
    if(Ext.get('dtpTglAwalFilterIGDTransaksi').dom.value > Ext.get('dtpTglAkhirFilterIGDTransaksi').dom.value)
    {
        ShowPesanWarningIGDTransaksiReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }
	if(Ext.getCmp('cboPilihanIGDTransaksikelompokPasien').getValue() === ''){
		ShowPesanWarningIGDTransaksiReport('Kelompok Pasien Belum Dipilih','Laporan Transaksi');
        x=0;
	}

	if(Ext.getCmp('Shift_All_IGDTransaksi').getValue() === false && Ext.getCmp('Shift_1_IGDTransaksi').getValue() === false && Ext.getCmp('Shift_2_IGDTransaksi').getValue() === false && Ext.getCmp('Shift_3_IGDTransaksi').getValue() === false){
		ShowPesanWarningIGDTransaksiReport(nmRequesterRequest,'Laporan Transaksi');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningIGDTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapIGDTransaksi_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Poli '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanIGDTransaksi(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterIGDTransaksi',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 40,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterIGDTransaksi',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanIGDTransaksiKelompokPasien(),
                mComboPerseoranganIGDTransaksi(),
                mComboAsuransiIGDTransaksi(),
                mComboPerusahaanIGDTransaksi(),
                mComboUmumIGDTransaksi(),
				mComboUmumLabRegis()
            ]
        }]
    };
    return items;
};

function getItemLapIGDTransaksi_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {boxLabel: 'Semua',name: 'Shift_All_IGDTransaksi',id : 'Shift_All_IGDTransaksi',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_IGDTransaksi').setValue(true);Ext.getCmp('Shift_2_IGDTransaksi').setValue(true);Ext.getCmp('Shift_3_IGDTransaksi').setValue(true);Ext.getCmp('Shift_1_IGDTransaksi').disable();Ext.getCmp('Shift_2_IGDTransaksi').disable();Ext.getCmp('Shift_3_IGDTransaksi').disable();}else{Ext.getCmp('Shift_1_IGDTransaksi').setValue(false);Ext.getCmp('Shift_2_IGDTransaksi').setValue(false);Ext.getCmp('Shift_3_IGDTransaksi').setValue(false);Ext.getCmp('Shift_1_IGDTransaksi').enable();Ext.getCmp('Shift_2_IGDTransaksi').enable();Ext.getCmp('Shift_3_IGDTransaksi').enable();}}},
                                    {boxLabel: 'Shift 1',name: 'Shift_1_IGDTransaksi',id : 'Shift_1_IGDTransaksi'},
                                    {boxLabel: 'Shift 2',name: 'Shift_2_IGDTransaksi',id : 'Shift_2_IGDTransaksi'},
                                    {boxLabel: 'Shift 3',name: 'Shift_3_IGDTransaksi',id : 'Shift_3_IGDTransaksi'}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};


function getItemLapIGDTransaksi_tengah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: '',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
									{boxLabel: 'Pendaftaran',name: 'PendaftaranIGDTransaksi',id : 'PendaftaranIGDTransaksi'},
                                    {boxLabel: 'Tindakan IGD',name: 'TindakanIGDTransaksi',id : 'TindakanIGDTransaksi'}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};


var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;

function mComboPilihanIGDTransaksi()
{

   var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewComboigdx',
                param: "kd_bagian=3 and type_unit=false"
            }
        }
    )
    var cboPilihanIGDTransaksi = new Ext.form.ComboBox
	(
             {
			  x: 120,
                y: 10,
            id: 'cboPilihanIGDTransaksi',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poli',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
             width:240,
			tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
				{
					
                     
						polipilihanpasien=b.data.KD_UNIT;
				},
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    {
										Ext.getCmp('cboDokterRequestEntry').focus();
										polipilihanpasien= c.value;
									}else if(e.getKey()==9)
									{
										polipilihanpasien= c.value;	
										   
									}
                                    }, c);
                                }


		}
        }
	);
	return cboPilihanIGDTransaksi;
};





function mComboPilihanIGDTransaksiKelompokPasien()
{
    var cboPilihanIGDTransaksikelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanIGDTransaksikelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPilihankelompokPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanIGDTransaksikelompokPasien;
};

function mComboPerseoranganIGDTransaksi()
{
    var cboPerseoranganTrIGD = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganTrIGD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganTrIGD;
};

function mComboUmumIGDTransaksi()
{
  var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by customer asc'
			}
		}
	);
    var cboperoranganRequestEntryTrIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboperoranganRequestEntryTrIGD',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Customer...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboperoranganRequestEntryTrIGD;

};

function mComboPerusahaanIGDTransaksi()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 order by customer asc'
			}
		}
	);
    var cboPerusahaanRequestEntryTrIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryTrIGD',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryTrIGD;
};

function mComboAsuransiIGDTransaksi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: 'jenis_cust=2 order by customer asc'
            }
        }
    );
    var cboAsuransiTrIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiTrIGD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiTrIGD;
};
function mComboUmumLabRegis()
{
    var cboUmumTrIGD = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumTrIGD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetUmum,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumTrIGD;
};
function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboperoranganRequestEntryTrIGD').show();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrIGD').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboperoranganRequestEntryTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').show();
        Ext.getCmp('cboUmumTrIGD').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboperoranganRequestEntryTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').show();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrIGD').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrIGD').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganTrIGD').hide();
        Ext.getCmp('cboAsuransiTrIGD').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTrIGD').hide();
        Ext.getCmp('cboUmumTrIGD').show();
   }
}

function mCombounitIGDTransaksi()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryIGDTransaksi = new Ext.form.ComboBox
    (
        {
            id: 'cbounitRequestEntryIGDTransaksi',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                }
                    
                }
        }
    )

    return cbounitRequestEntryIGDTransaksi;
};
