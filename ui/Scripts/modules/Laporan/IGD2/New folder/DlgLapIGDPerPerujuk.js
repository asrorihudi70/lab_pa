
var dsIGDPerRujukan;
var selectNamaIGDPerRujukan;
var now = new Date();
var selectSetJenisPasien;
var frmDlgIGDPerRujukan;
var varLapIGDPerRujukan= ShowFormLapIGDPerRujukan();
var selectSetUmum;

function ShowFormLapIGDPerRujukan()
{
    frmDlgIGDPerRujukan= fnDlgIGDPerRujukan();
    frmDlgIGDPerRujukan.show();
};

function fnDlgIGDPerRujukan()
{
    var winIGDPerRujukanReport = new Ext.Window
    (
        {
            id: 'winIGDPerRujukanReport',
            title: 'Laporan Rawat Jalan',
            closeAction: 'destroy',
            width:500,
            height: 400,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDPerRujukan()],
            listeners:
        {
            activate: function()
            {
//                Ext.getCmp('cboJenisPasien').show();
//                Ext.getCmp('cboAsuransi').hide();
//                Ext.getCmp('cboPerusahaanRequestEntry').hide();
//                Ext.getCmp('cboUmum').hide();
//                Ext.getCmp('dtpTglAkhirLapIGDKIUP').hide();
            }
        }

        }
    );

    return winIGDPerRujukanReport;
};


function ItemDlgIGDPerRujukan()
{
    var PnlLapIGDPerRujukan = new Ext.Panel
    (
        {
            id: 'PnlLapIGDPerRujukan',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDPerRujukan_Dept(),
//                getItemLapIGDPerRujukan_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapIGDPerRujukan',
                            handler: function()
                            {
//                                if (ValidasiReportIGDPerRujukan() === 1)
//                                {
//                                        var tmppilihan = getKodeReportIGDPerRujukan();
                                        var criteria = GetCriteriaIGDPerRujukan();
                                        frmDlgIGDPerRujukan.close();
                                        ShowReport('0', 1020206, criteria);
//                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapIGDPerRujukan',
                            handler: function()
                            {
                                    frmDlgIGDPerRujukan.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapIGDPerRujukan;
};

function GetCriteriaIGDPerRujukan()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapIGDPerRujukan').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapIGDPerRujukan').getValue();
//                strKriteria += '##@@##' + Ext.get('kelPasien').getValue();
	};

	return strKriteria;
};


//function ValidasiReportIGDPerRujukan(modul)
//{
//    var x=1;
//
//    if((Ext.get('dtpTglAwalLapIGDPerRujukan').dom.value === ''))
//    {
//        ShowPesanWarningIGDPerRujukanReport("Tanggal Tidak Boleh Kosong...", modul);
//        x=0;
//    }
//    else if(Ext.get('kelPasien').dom.value === '' || Ext.get('kelPasien').dom.value === 'Pilih Salah Satu...')
//    {
//        ShowPesanWarningIGDPerRujukanReport("Anda Belum Memilih Kelompok pasien...", modul);
//        x=0;
//    }else{
//        x =1;
//    }
//
//    return x;
//};

function getKodeReportIGDPerRujukan()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihan').getValue() === 1)
    {
        tmppilihan = '1020203';
    }else if (Ext.getCmp('cboPilihan').getValue() === 2)
    {
        tmppilihan = '1020204';
    }
    return tmppilihan;
}

function ValidasiTanggalReportIGDPerRujukan()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDPerRujukan').dom.value > Ext.get('dtpTglAkhirLapIGDPerRujukan').dom.value)
    {
        ShowPesanWarningIGDPerRujukanReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDPerRujukanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapIGDPerRujukan_Dept()
{
    var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
                    columnWidth: .90,
                    layout: 'form',
                    border: false,
                    labelAlign: 'right',
                    labelWidth: 150,
                    items:
                    [
                        {
                            xtype: 'datefield',
                            fieldLabel: nmPeriodeDlgRpt + ' ',
                            id: 'dtpTglAwalLapIGDPerRujukan',
                            format: 'd/M/Y',
                            value:now,
                            width:105
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: nmSd + ' ',
                            id: 'dtpTglAkhirLapIGDKIUP',
                            format: 'd/M/Y',
                            value:now,
                            width:105
                        }
                        
                    ]
               },
            {
            columnWidth: .90,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 150,
            items:
            [
                {
                    xtype: 'textfield',
                    fieldLabel: 'Bagian',
                    name: 'txtBagian',
                    id: 'txtBagian',
                    value: 'Instalasi Gawat Darurat',
                    disabled : true,
                    width:150
                },
                mcomborujukandari(),
                mComboRujukan(),
                mComboJenisPasien()
            ]
        }
        ]
        
    };
    return items;
};

function getItemLapIGDPerRujukan_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: .90,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 150,
            defaultType: 'checkbox',
            
            items:
            [{
                    fieldLabel: 'Pilih Rujukan',
                    boxLabel: 'Pasien Baru',
                    name: 'Rujukan_1',
                    id : 'Rujukan_1'
                    
                }, {
                    fieldLabel: '',
                    labelSeparator: '',
                    boxLabel: 'Pasien Lama',
                    name: 'Rujukan_2',
                    id : 'Rujukan_2',
                    handler: function (field, value) {
                       if (value === true && Ext.getCmp('Rujukan_1').getValue() === 'true')
                       {
                           Ext.getCmp('Rujukan_All').setValue(true);
                           Ext.getCmp('Rujukan_1').disable();
                           Ext.getCmp('Rujukan_2').disable();
                       }else if (value === true && Ext.getCmp('Rujukan_1').getValue() === 'false'){
                           Ext.getCmp('Rujukan_1').setValue(false);
                           Ext.getCmp('Rujukan_2').setValue(true);
                       }
                   }
                },{
                    fieldLabel: '',
                    labelSeparator: '',
                    boxLabel: 'Semua pasien',
                    name: 'Rujukan_All',
                    id : 'Rujukan_All',
                    handler: function (field, value) {
                       if (value === true)
                       {
                           Ext.getCmp('Rujukan_1').setValue(true);
                           Ext.getCmp('Rujukan_2').setValue(true);
                           Ext.getCmp('Rujukan_1').disable();
                           Ext.getCmp('Rujukan_2').disable();
                       }else{
                           Ext.getCmp('Rujukan_1').setValue(false);
                           Ext.getCmp('Rujukan_2').setValue(false);
                           Ext.getCmp('Rujukan_1').enable();
                           Ext.getCmp('Rujukan_2').enable();
                       }
                    }
                }]
            }
        ]
    };
    return items;
};

function mComboJenisPasien()
{
    var cboJenisPasien = new Ext.form.ComboBox
	(
		{
			id:'cboJenisPasien',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua Pasien'],[2, 'Pasien Baru'],[3, 'Pasien Lama']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
//			value:selectSetJenisPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetJenisPasien=b.data.displayText ;
				}
			}
		}
	);
	return cboJenisPasien;
};

function mcomborujukandari()
{
    var Field = ['CARA_PENERIMAAN','PENERIMAAN'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewComboRujukanDari',
                param: ""
            }
        }
    )

    var cboRujukanDariRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanDariRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Rujukan...',
            fieldLabel: 'Rujukan Dari ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'CARA_PENERIMAAN',
            displayField: 'PENERIMAAN',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   loaddatastorerujukan(b.data.CARA_PENERIMAAN)
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }


		}
        }
    )

    return cboRujukanDariRequestEntry;
}

function loaddatastorerujukan(cara_penerimaan)
{
          dsRujukanRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    Sort: 'rujukan',
			    Sortdir: 'ASC',
			    target: 'ViewComboRujukan',
			    param: 'cara_penerimaan=~'+ cara_penerimaan+ '~'
			}
                    }
                )
}

function mComboRujukan()
{
    var Field = ['KD_RUJUKAN','RUJUKAN'];

    dsRujukanRequestEntry = new WebApp.DataStore({fields: Field});


    var cboRujukanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboRujukanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Rujukan...',
		    fieldLabel: 'Rujukan ',
		    align: 'Right',
                     
//		    anchor:'60%',
		    store: dsRujukanRequestEntry,
		    valueField: 'KD_RUJUKAN',
		    displayField: 'RUJUKAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboRujukanRequestEntry;
};