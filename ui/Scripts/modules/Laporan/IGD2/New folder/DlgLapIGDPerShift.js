
var dsRWJPerShift;
var selectNamaRWJPerShift;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRWJPerShift;
var varLapRWJPerShift= ShowFormLapRWJPerShift();
var selectSetUmum;

function ShowFormLapRWJPerShift()
{
    frmDlgRWJPerShift= fnDlgRWJPerShift();
    frmDlgRWJPerShift.show();
};

function fnDlgRWJPerShift()
{
    var winRWJPerShiftReport = new Ext.Window
    (
        {
            id: 'winRWJPerShiftReport',
            title: 'Laporan Rawat Jalan',
            closeAction: 'destroy',
            width:500,
            height: 400,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJPerShift()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseorangan').show();
                Ext.getCmp('cboAsuransi').hide();
                Ext.getCmp('cboPerusahaanRequestEntry').hide();
                Ext.getCmp('cboUmum').hide();
                Ext.getCmp('dtpTglAkhirLapRWJKIUP').hide();
            }
        }

        }
    );

    return winRWJPerShiftReport;
};


function ItemDlgRWJPerShift()
{
    var PnlLapRWJPerShift = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPerShift',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJPerShift_Dept(),
                getItemLapRWJPerShift_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWJPerShift',
                            handler: function()
                            {
//                                if (ValidasiReportRWJPerShift() === 1)
//                                {
                                        var tmppilihan = getKodeReportRWJPerShift();
                                        var criteria = GetCriteriaRWJPerShift();
                                        frmDlgRWJPerShift.close();
                                        ShowReport('0', tmppilihan, criteria);
//                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWJPerShift',
                            handler: function()
                            {
                                    frmDlgRWJPerShift.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWJPerShift;
};

function GetCriteriaRWJPerShift()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJPerShift').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWJPerShift').getValue();
//                strKriteria += '##@@##' + Ext.get('kelPasien').getValue();
	};

	return strKriteria;
};


//function ValidasiReportRWJPerShift(modul)
//{
//    var x=1;
//
//    if((Ext.get('dtpTglAwalLapRWJPerShift').dom.value === ''))
//    {
//        ShowPesanWarningRWJPerShiftReport("Tanggal Tidak Boleh Kosong...", modul);
//        x=0;
//    }
//    else if(Ext.get('kelPasien').dom.value === '' || Ext.get('kelPasien').dom.value === 'Pilih Salah Satu...')
//    {
//        ShowPesanWarningRWJPerShiftReport("Anda Belum Memilih Kelompok pasien...", modul);
//        x=0;
//    }else{
//        x =1;
//    }
//
//    return x;
//};

function getKodeReportRWJPerShift()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihan').getValue() === 1)
    {
        tmppilihan = '1020203';
    }else if (Ext.getCmp('cboPilihan').getValue() === 2)
    {
        tmppilihan = '1020204';
    }
    return tmppilihan;
}

function ValidasiTanggalReportRWJPerShift()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJPerShift').dom.value > Ext.get('dtpTglAkhirLapRWJPerShift').dom.value)
    {
        ShowPesanWarningRWJPerShiftReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJPerShiftReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapRWJPerShift_Dept()
{
    var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: .90,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 150,
            items:
            [
                mComboPilihan(),
                {
                    xtype: 'datefield',
                    fieldLabel: nmPeriodeDlgRpt + ' ',
                    id: 'dtpTglAwalLapRWJPerShift',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Bagian',
                    name: 'txtBagian',
                    id: 'txtBagian',
                    value: 'Rawat Jalan',
                    disabled : true,
                    width:105
                },
                {  
                    xtype: 'combo',
                    fieldLabel: 'Kelompok Pasien',
                    id: 'kelPasien',
                     editable: false,
                    //value: 'Perseorangan',
                    store: new Ext.data.ArrayStore
                        (
                            {
                            id: 0,
                            fields:
                            [
                                'Id',
                                'displayText'
                            ],
                               data: [[1, 'Semua'],[2, 'Umum'], [3, 'Perusahaan'], [4, 'Asuransi']]
                            }
                        ),
                    displayField: 'displayText',
                    mode: 'local',
                    width: 100,
                    forceSelection: true,
                    triggerAction: 'all',
                    emptyText: 'Pilih Salah Satu...',
                    selectOnFocus: true,
                    anchor: '95%',
                    listeners:
                     {
                            'select': function(a, b, c)
                        {
                           Combo_Select(b.data.displayText);

                        }

                    }
                },
                mComboPerseorangan(),
                mComboPerusahaan(),
                mComboAsuransi(),
                mComboUmum(),
                {
                    columnWidth: 0.48,
                    layout: 'form',
                    border: false,
                            labelWidth: 25,
                    labelAlign: 'right',
                    items:
                    [
                        {
                            xtype: 'datefield',
                            fieldLabel: nmSd + ' ',
                            id: 'dtpTglAkhirLapRWJKIUP',
                            format: 'd/M/Y',
                            value:now,
                            width:105
                        }
                    ]
               }
            ]
        }
        ]
        
    };
    return items;
};

function getItemLapRWJPerShift_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: .90,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 150,
            defaultType: 'checkbox',
            
            items:
            [{
                    fieldLabel: 'Pilih Shift',
                    boxLabel: 'Shift 1',
                    name: 'Shift_1',
                    id : 'Shift_1'
                    
                }, {
                    fieldLabel: '',
                    labelSeparator: '',
                    boxLabel: 'Shift 2',
                    name: 'Shift_2',
                    id : 'Shift_2'
                }, {
                    fieldLabel: '',
                    labelSeparator: '',
                    boxLabel: 'Shift 3',
                    name: 'Shift_3',
                    id : 'Shift_3'
                },{
                    fieldLabel: '',
                    labelSeparator: '',
                    boxLabel: 'All Shift',
                    name: 'Shift_All',
                    id : 'Shift_All',
                    handler: function (field, value) {
                       if (value === true)
                       {
                           Ext.getCmp('Shift_1').setValue(true);
                           Ext.getCmp('Shift_2').setValue(true);
                           Ext.getCmp('Shift_3').setValue(true);
                           Ext.getCmp('Shift_1').disable();
                           Ext.getCmp('Shift_2').disable();
                           Ext.getCmp('Shift_3').disable();
                       }else{
                           Ext.getCmp('Shift_1').setValue(false);
                           Ext.getCmp('Shift_2').setValue(false);
                           Ext.getCmp('Shift_3').setValue(false);
                           Ext.getCmp('Shift_1').enable();
                           Ext.getCmp('Shift_2').enable();
                           Ext.getCmp('Shift_3').enable();
                       }
                    }
                }]
            }
        ]
    };
    return items;
};

var selectSetPilihan;
function mComboPilihan()
{
    var cboPilihan = new Ext.form.ComboBox
	(
		{
			id:'cboPilihan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Pendaftaran Per Shift ',
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Detail'], [2, 'Sumary']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetPilihan,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetPilihan=b.data.displayText ;
				}
			}
		}
	);
	return cboPilihan;
};

function mComboPerseorangan()
{
    var cboPerseorangan = new Ext.form.ComboBox
	(
		{
			id:'cboPerseorangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Umum']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
//			value:selectSetPerseorangan,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetPerseorangan=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseorangan;
};

function mComboUmum()
{
    var cboUmum = new Ext.form.ComboBox
	(
		{
			id:'cboUmum',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
//			value:selectSetUmum,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
			}
		}
	);
	return cboUmum;
};

function mComboPerusahaan()
{
    var Field = ['KD_PERUSAHAAN','PERUSAHAAN'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboPerusahaan',
			    param: ''
			}
		}
	);
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_PERUSAHAAN',
		    displayField: 'PERUSAHAAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//			        var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                }
			}
                }
	);

    return cboPerusahaanRequestEntry;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomer',
                param: "status=true"
            }
        }
    );
    var cboAsuransi = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransi',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Asuransi...',
                        fieldLabel: '',
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.displayText ;
				}
			}
		}
	);
	return cboAsuransi;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Umum")
   {    
        Ext.getCmp('cboPerseorangan').show();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Asuransi")
       {
         Ext.getCmp('cboPerseorangan').hide();
         Ext.getCmp('cboAsuransi').show();
         Ext.getCmp('cboPerusahaanRequestEntry').hide();
         Ext.getCmp('cboUmum').hide();
       }
       else if(value === "Semua")
       {
            Ext.getCmp('cboPerseorangan').hide();
            Ext.getCmp('cboAsuransi').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            Ext.getCmp('cboUmum').show();
       }else
       {
           Ext.getCmp('cboPerseorangan').hide();
            Ext.getCmp('cboAsuransi').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            Ext.getCmp('cboUmum').hide();
       }
}