
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapInventarisBukuSubsubKelompok;
var varInvLapInventarisBukuSubsubKelompok= ShowFormInvLapInventarisBukuSubsubKelompok();
var asc=0;
var IdRootKelompokBarang_pInvLapInventarisBukuSubsubKelompok='1000000000';
var KdInv_InvLapInventarisBukuSubsubKelompok='';
var gridPanelLookUpBarang_InvLapInventarisBukuSubsubKelompok;
var cboLokasiAsal_InvLapInventarisBukuSubsubKelompok ;



function ShowFormInvLapInventarisBukuSubsubKelompok()
{
    frmDlgInvLapInventarisBukuSubsubKelompok= fnDlgInvLapInventarisBukuSubsubKelompok();
    frmDlgInvLapInventarisBukuSubsubKelompok.show();
	GetStrTreeSetBarang_InvLapInventarisBukuSubsubKelompok();
};

function fnDlgInvLapInventarisBukuSubsubKelompok()
{
    var winInvLapInventarisBukuSubsubKelompokReport = new Ext.Window
    (
        {
            id: 'winInvLapInventarisBukuSubsubKelompokReport',
            title: 'Laporan Buku Inventaris Sub-Sub Kelompok',
            closeAction: 'destroy',
            width: 485,
            height: 530,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgInvLapInventarisBukuSubsubKelompok()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkInvLapInventarisBukuSubsubKelompok',
					handler: function()
					{
						var params={
							KdInv:KdInv_InvLapInventarisBukuSubsubKelompok,
							tgl:Ext.getCmp('dtpTglInvLapStokPersediaanBarang').getValue()
							
						} ;
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/inventaris/lap_inventarisbukusubsubkelompok/cetak");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();		
						//frmDlgInvLapInventarisBukuSubsubKelompok.close();
						
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelInvLapInventarisBukuSubsubKelompok',
					handler: function()
					{
						frmDlgInvLapInventarisBukuSubsubKelompok.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winInvLapInventarisBukuSubsubKelompokReport;
};


function ItemDlgInvLapInventarisBukuSubsubKelompok()
{
    var PnlInvLapInventarisBukuSubsubKelompok = new Ext.Panel
    (
        {
            id: 'PnlInvLapInventarisBukuSubsubKelompok',
            fileUpload: true,
            layout: 'form',
            height: 525,
            anchor: '100%',
            bodyStyle: 'padding:10px',
            border: true,
            items:
            [
				getItemInvLapInventarisBukuSubsubKelompok_Tanggal(),
				getItemInvLapInventarisBukuSubsubKelompok_Batas(),
				getItemInvLapInventarisBukuSubsubKelompok_tree(),
				gridDataViewBarang_InvLapInventarisBukuSubsubKelompok()
            ]
        }
    );

    return PnlInvLapInventarisBukuSubsubKelompok;
};


function getItemInvLapInventarisBukuSubsubKelompok_tree()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
           itemsTreeListKelompokBarangGridDataView_InvLapInventarisBukuSubsubKelompok(),
		]
    };
    return items;
};



function GetStrTreeSetBarang_InvLapInventarisBukuSubsubKelompok()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and left(kd_inv,1)<>'8'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKelompokBarangGridDataView_InvLapInventarisBukuSubsubKelompok()
{	
		
	treeKlas_kelompokbarang_InvLapInventarisBukuSubsubKelompok= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:225,
			width:430,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_InvLapInventarisBukuSubsubKelompok=n.attributes
					if (strTreeCriteriKelompokBarang_InvLapInventarisBukuSubsubKelompok.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_InvLapInventarisBukuSubsubKelompok.leaf == false)
						{
							
						}
						else
						{
							console.log(n.attributes.id);
							dataGridLookUpBarangInvLapInventarisBukuSubsubKelompok(n.attributes.id);
							
							if(asc==0){
								asc=1;
								GetStrTreeSetBarang_InvLapInventarisBukuSubsubKelompok();
								rootTreeMasterBarang_InvLapInventarisBukuSubsubKelompok = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdRootKelompokBarang_pInvLapInventarisBukuSubsubKelompok,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeKlas_kelompokbarang_InvLapInventarisBukuSubsubKelompok.setRootNode(rootTreeMasterBarang_InvLapInventarisBukuSubsubKelompok);
						    } 
							
						};
					}
					else
					{
						
					};
				},
			}
		}
	);
	
	rootTreeMasterBarang_InvLapInventarisBukuSubsubKelompok = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdRootKelompokBarang_pInvLapInventarisBukuSubsubKelompok,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_InvLapInventarisBukuSubsubKelompok.setRootNode(rootTreeMasterBarang_InvLapInventarisBukuSubsubKelompok);  
	
    var pnlTreeFormDataWindowPopup_InvLapInventarisBukuSubsubKelompok = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_InvLapInventarisBukuSubsubKelompok,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_InvLapInventarisBukuSubsubKelompok;
}

function getItemInvLapInventarisBukuSubsubKelompok_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  448,
				height: 75,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Posisi buku inventaris pada tanggal'
					}, 
					{
						x: 190,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 10,
						y: 25,
						xtype: 'datefield',
						id: 'dtpTglInvLapStokPersediaanBarang',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 10,
						y: 55,
						xtype: 'label',
						text: 'Sub-sub Kelompok Barang'
					}, 
				]
			}
		]
    };
    return items;
};


function getItemInvLapInventarisBukuSubsubKelompok_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function gridDataViewBarang_InvLapInventarisBukuSubsubKelompok()
{
    var FieldGrdBArang_InvLapInventarisBukuSubsubKelompok = [];
	
    dsDataGrdBarang_InvLapInventarisBukuSubsubKelompok= new WebApp.DataStore
	({
        fields: FieldGrdBArang_InvLapInventarisBukuSubsubKelompok
    });
    
    gridPanelLookUpBarang_InvLapInventarisBukuSubsubKelompok =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_InvLapInventarisBukuSubsubKelompok,
		height:120,
		width:448,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_InvLapInventarisBukuSubsubKelompok.getAt(row).data.kd_inv);
					KdInv_InvLapInventarisBukuSubsubKelompok = dsDataGrdBarang_InvLapInventarisBukuSubsubKelompok.getAt(row).data.kd_inv;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'inv_kd_inv',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvLapInventarisBukuSubsubKelompok;
}

function dataGridLookUpBarangInvLapInventarisBukuSubsubKelompok(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/lap_inventarisbukusubsubkelompok/getGridLookUpBarang",
			params: {inv_kd_inv:inv_kd_inv},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_InvLapInventarisBukuSubsubKelompok.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_InvLapInventarisBukuSubsubKelompok.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_InvLapInventarisBukuSubsubKelompok.add(recs);
					
					
					
					gridPanelLookUpBarang_InvLapInventarisBukuSubsubKelompok.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
