
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapInventarisMutasiDetail;
var varInvLapInventarisMutasiDetail= ShowFormInvLapInventarisMutasiDetail();
var asc=0;
var IdRootKelompokBarang_pInvLapInventarisMutasiDetail='1000000000';
var KdInv_InvLapInventarisMutasiDetail='';
var gridPanelLookUpBarang_InvLapInventarisMutasiDetail;
var cboLokasiAsal_InvLapInventarisMutasiDetail ;



function ShowFormInvLapInventarisMutasiDetail()
{
    frmDlgInvLapInventarisMutasiDetail= fnDlgInvLapInventarisMutasiDetail();
    frmDlgInvLapInventarisMutasiDetail.show();
	GetStrTreeSetBarang_InvLapInventarisMutasiDetail();
	loadDataComboJenisLokasiAsalInvLapInventarisMutasiDetail();
};

function fnDlgInvLapInventarisMutasiDetail()
{
    var winInvLapInventarisMutasiDetailReport = new Ext.Window
    (
        {
            id: 'winInvLapInventarisMutasiDetailReport',
            title: 'Laporan Inventaris Mutasi Detail',
            closeAction: 'destroy',
            width: 485,
            height: 575,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgInvLapInventarisMutasiDetail()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkInvLapInventarisMutasiDetail',
					handler: function()
					{
						if(Ext.get('cbJenisMutasi_InvLapInventarisMutasiDetail').getValue()=='Bertambah/Penghapusan'){
							var params={
								KdInv:KdInv_InvLapInventarisMutasiDetail,
								tglAwal:Ext.getCmp('dtpTglAwalInvLapStokPersediaanBarang').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirInvLapStokPersediaanBarang').getValue(),
								Lokasi:Ext.getCmp('cbLokasiAsal_InvLapInventarisMutasiDetail').getValue(),
								
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/inventaris/lap_inventarismutasidetail/cetakBertambah");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgInvLapInventarisMutasiDetail.close();
						} else{
							var params={
								KdInv:KdInv_InvLapInventarisMutasiDetail,
								tglAwal:Ext.getCmp('dtpTglAwalInvLapStokPersediaanBarang').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirInvLapStokPersediaanBarang').getValue(),
								Lokasi:Ext.getCmp('cbLokasiAsal_InvLapInventarisMutasiDetail').getValue(),
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/inventaris/lap_inventarismutasidetail/cetakPindah");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgInvLapInventarisMutasiDetail.close();
						}
						
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelInvLapInventarisMutasiDetail',
					handler: function()
					{
						frmDlgInvLapInventarisMutasiDetail.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winInvLapInventarisMutasiDetailReport;
};


function ItemDlgInvLapInventarisMutasiDetail()
{
    var PnlInvLapInventarisMutasiDetail = new Ext.Panel
    (
        {
            id: 'PnlInvLapInventarisMutasiDetail',
            fileUpload: true,
            layout: 'form',
            height: 525,
            anchor: '100%',
            bodyStyle: 'padding:10px',
            border: true,
            items:
            [
				getItemInvLapInventarisMutasiDetail_Tanggal(),
				getItemInvLapInventarisMutasiDetail_Batas(),
				getItemInvLapInventarisMutasiDetail_tree(),
				gridDataViewBarang_InvLapInventarisMutasiDetail(),
				getItemInvLapInventarisMutasiDetail_Batas(),
				getItemInvLapInventarisMutasiDetail_JenisMutasi()
            ]
        }
    );

    return PnlInvLapInventarisMutasiDetail;
};


function getItemInvLapInventarisMutasiDetail_tree()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
           itemsTreeListKelompokBarangGridDataView_InvLapInventarisMutasiDetail(),
		]
    };
    return items;
};



function GetStrTreeSetBarang_InvLapInventarisMutasiDetail()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and left(kd_inv,1)<>'8'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKelompokBarangGridDataView_InvLapInventarisMutasiDetail()
{	
		
	treeKlas_kelompokbarang_InvLapInventarisMutasiDetail= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:225,
			width:430,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_InvLapInventarisMutasiDetail=n.attributes
					if (strTreeCriteriKelompokBarang_InvLapInventarisMutasiDetail.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_InvLapInventarisMutasiDetail.leaf == false)
						{
							
						}
						else
						{
							console.log(n.attributes.id);
							dataGridLookUpBarangInvLapInventarisMutasiDetail(n.attributes.id);
							
							if(asc==0){
								asc=1;
								GetStrTreeSetBarang_InvLapInventarisMutasiDetail();
								rootTreeMasterBarang_InvLapInventarisMutasiDetail = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdRootKelompokBarang_pInvLapInventarisMutasiDetail,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeKlas_kelompokbarang_InvLapInventarisMutasiDetail.setRootNode(rootTreeMasterBarang_InvLapInventarisMutasiDetail);
						    } 
							
						};
					}
					else
					{
						
					};
				},
			}
		}
	);
	
	rootTreeMasterBarang_InvLapInventarisMutasiDetail = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdRootKelompokBarang_pInvLapInventarisMutasiDetail,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_InvLapInventarisMutasiDetail.setRootNode(rootTreeMasterBarang_InvLapInventarisMutasiDetail);  
	
    var pnlTreeFormDataWindowPopup_InvLapInventarisMutasiDetail = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_InvLapInventarisMutasiDetail,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_InvLapInventarisMutasiDetail;
}

function getItemInvLapInventarisMutasiDetail_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  448,
				height: 50,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAwalInvLapStokPersediaanBarang',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 210,
						y: 5,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 235,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAkhirInvLapStokPersediaanBarang',
						format: 'd/M/Y',
						value: now,
						width: 100
					},
					{
						x: 10,
						y: 30,
						xtype: 'label',
						text: 'Sub-sub Kelompok Barang'
					}, 
				]
			}
		]
    };
    return items;
};

function getItemInvLapInventarisMutasiDetail_JenisMutasi()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  448,
				height: 65,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Jenis Mutasi'
					}, 
					{
						x: 100,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					comboJenisMutasi_InvLapInventarisMutasiDetail(),
					{
						x: 10,
						y: 30,
						xtype: 'label',
						text: 'Lokasi asal'
					}, 
					{
						x: 100,
						y: 30,
						xtype: 'label',
						text: ' : '
					},
					comboLokasiAsal_InvLapInventarisMutasiDetail()
				]
			}
		]
    };
    return items;
};

function getItemInvLapInventarisMutasiDetail_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function comboJenisMutasi_InvLapInventarisMutasiDetail()
{
    var cboJenisMutasi_InvLapInventarisMutasiDetail = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 5,
                id:'cbJenisMutasi_InvLapInventarisMutasiDetail',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Bertambah/Penghapusan'], [2, 'Pindah Lokasi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
					'select': function(a,b,c)
					{
						if(b.data.displayText=='Pindah Lokasi'){
							loadDataComboJenisLokasiAsalInvLapInventarisMutasiDetail();
							Ext.getCmp('cbLokasiAsal_InvLapInventarisMutasiDetail').enable();
						} else{
							Ext.getCmp('cbLokasiAsal_InvLapInventarisMutasiDetail').disable();
						}
						
					}
                }
            }
	);
	return cboJenisMutasi_InvLapInventarisMutasiDetail;
};

function comboLokasiAsal_InvLapInventarisMutasiDetail()
{	
	var Field = ['no_ruang','lokasi'];

    dsJenisLokasiAsalInvLapInventarisMutasiDetail = new WebApp.DataStore({ fields: Field });
    cboLokasiAsal_InvLapInventarisMutasiDetail = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 30,
                id:'cbLokasiAsal_InvLapInventarisMutasiDetail',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
				disabled:true,
                width:200,
                store: dsJenisLokasiAsalInvLapInventarisMutasiDetail,
                valueField: 'no_ruang',
                displayField: 'lokasi',
				value:'999999 - SEMUA',
                listeners:
                {
					'select': function(a,b,c)
					{
						
					}
                }
            }
	);
	return cboLokasiAsal_InvLapInventarisMutasiDetail;
};

function gridDataViewBarang_InvLapInventarisMutasiDetail()
{
    var FieldGrdBArang_InvLapInventarisMutasiDetail = [];
	
    dsDataGrdBarang_InvLapInventarisMutasiDetail= new WebApp.DataStore
	({
        fields: FieldGrdBArang_InvLapInventarisMutasiDetail
    });
    
    gridPanelLookUpBarang_InvLapInventarisMutasiDetail =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_InvLapInventarisMutasiDetail,
		height:120,
		width:448,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_InvLapInventarisMutasiDetail.getAt(row).data.kd_inv);
					KdInv_InvLapInventarisMutasiDetail = dsDataGrdBarang_InvLapInventarisMutasiDetail.getAt(row).data.kd_inv;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'inv_kd_inv',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvLapInventarisMutasiDetail;
}

function dataGridLookUpBarangInvLapInventarisMutasiDetail(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/lap_inventarismutasidetail/getGridLookUpBarang",
			params: {inv_kd_inv:inv_kd_inv},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_InvLapInventarisMutasiDetail.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_InvLapInventarisMutasiDetail.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_InvLapInventarisMutasiDetail.add(recs);
					
					
					
					gridPanelLookUpBarang_InvLapInventarisMutasiDetail.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function loadDataComboJenisLokasiAsalInvLapInventarisMutasiDetail(param){
	if (param==='' || param===undefined) {
		param={
			// parameter untuk url yang dituju (fungsi didalam controller)
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/inventaris/lap_inventarismutasidetail/getLokasiAsal",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboLokasiAsal_InvLapInventarisMutasiDetail.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsJenisLokasiAsalInvLapInventarisMutasiDetail.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsJenisLokasiAsalInvLapInventarisMutasiDetail.add(recs);
				console.log(o);
			}
		}
	});
}

function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
