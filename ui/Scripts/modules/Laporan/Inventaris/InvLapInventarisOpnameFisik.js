
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapInventarisOpnameFisik;
var varInvLapInventarisOpnameFisik= ShowFormInvLapInventarisOpnameFisik();
var asc=0;
var IdRootKelompokBarang_pInvLapInventarisOpnameFisik='1000000000';
var NoRuang_InvLapInventarisOpnameFisik='';
var gridPanelLookUpBarang_InvLapInventarisOpnameFisik;
var cboLokasiAsal_InvLapInventarisOpnameFisik ;



function ShowFormInvLapInventarisOpnameFisik()
{
    frmDlgInvLapInventarisOpnameFisik= fnDlgInvLapInventarisOpnameFisik();
    frmDlgInvLapInventarisOpnameFisik.show();
	dataGridLookUpBarangInvLapInventarisOpnameFisik();
};

function fnDlgInvLapInventarisOpnameFisik()
{
    var winInvLapInventarisOpnameFisikReport = new Ext.Window
    (
        {
            id: 'winInvLapInventarisOpnameFisikReport',
            title: 'Laporan Inventaris Opname Fisik Barang',
            closeAction: 'destroy',
            width: 485,
            height: 325,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgInvLapInventarisOpnameFisik()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkInvLapInventarisOpnameFisik',
					handler: function()
					{
						var params={
							NoRuang:NoRuang_InvLapInventarisOpnameFisik,
							tgl:Ext.getCmp('dtpTglInvLapStokPersediaanBarang').getValue()
							
						} ;
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/inventaris/lap_stokfisikbarang/cetak");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();		
						//frmDlgInvLapInventarisOpnameFisik.close();
						
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelInvLapInventarisOpnameFisik',
					handler: function()
					{
						frmDlgInvLapInventarisOpnameFisik.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winInvLapInventarisOpnameFisikReport;
};


function ItemDlgInvLapInventarisOpnameFisik()
{
    var PnlInvLapInventarisOpnameFisik = new Ext.Panel
    (
        {
            id: 'PnlInvLapInventarisOpnameFisik',
            fileUpload: true,
            layout: 'form',
            height: 525,
            anchor: '100%',
            bodyStyle: 'padding:10px',
            border: true,
            items:
            [
				getItemInvLapInventarisOpnameFisik_Tanggal(),
				getItemInvLapInventarisOpnameFisik_Batas(),
				gridDataViewBarang_InvLapInventarisOpnameFisik()
            ]
        }
    );

    return PnlInvLapInventarisOpnameFisik;
};

function getItemInvLapInventarisOpnameFisik_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  448,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Tanggal cetak'
					}, 
					{
						x: 90,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglInvLapStokPersediaanBarang',
						format: 'd/M/Y',
						value: now
					},
					{
						x: 10,
						y: 25,
						xtype: 'label',
						text: 'Ruang :'
					},
				]
			}
		]
    };
    return items;
};

function getItemInvLapInventarisOpnameFisik_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function gridDataViewBarang_InvLapInventarisOpnameFisik()
{
    var FieldGrdBArang_InvLapInventarisOpnameFisik = [];
	
    dsDataGrdBarang_InvLapInventarisOpnameFisik= new WebApp.DataStore
	({
        fields: FieldGrdBArang_InvLapInventarisOpnameFisik
    });
    
    gridPanelLookUpBarang_InvLapInventarisOpnameFisik =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_InvLapInventarisOpnameFisik,
		height:180,
		width:448,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					//console.log(dsDataGrdBarang_InvLapInventarisOpnameFisik.getAt(row).data.kd_inv);
					NoRuang_InvLapInventarisOpnameFisik = dsDataGrdBarang_InvLapInventarisOpnameFisik.getAt(row).data.no_ruang;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'jns_lokasi',
				header: 'Gedung',
				sortable: true,
				width: 210
			},
			{			
				dataIndex: 'no_ruang',
				header: 'No. Ruang',
				sortable: true,
				width: 70,
				
			},
			{
				dataIndex: 'lokasi',
				header: 'Nama ruang/Keterangan',
				sortable: true,
				width: 160
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvLapInventarisOpnameFisik;
}

function dataGridLookUpBarangInvLapInventarisOpnameFisik(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/lap_stokfisikbarang/getGridRuang",
			params: {inv_kd_inv:inv_kd_inv},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_InvLapInventarisOpnameFisik.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_InvLapInventarisOpnameFisik.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_InvLapInventarisOpnameFisik.add(recs);
					
					
					
					gridPanelLookUpBarang_InvLapInventarisOpnameFisik.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
