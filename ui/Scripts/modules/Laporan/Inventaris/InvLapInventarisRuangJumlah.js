
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapInventarisRuangJumlah;
var varInvLapInventarisRuangJumlah= ShowFormInvLapInventarisRuangJumlah();
var asc=0;
var IdRootKelompokBarang_pInvLapInventarisRuangJumlah='1000000000';
var KdInv_InvLapInventarisRuangJumlah;
var gridPanelLookUpBarang_InvLapInventarisRuangJumlah;



function ShowFormInvLapInventarisRuangJumlah()
{
    frmDlgInvLapInventarisRuangJumlah= fnDlgInvLapInventarisRuangJumlah();
    frmDlgInvLapInventarisRuangJumlah.show();
	GetStrTreeSetBarang_InvLapInventarisRuangJumlah();
};

function fnDlgInvLapInventarisRuangJumlah()
{
    var winInvLapInventarisRuangJumlahReport = new Ext.Window
    (
        {
            id: 'winInvLapInventarisRuangJumlahReport',
            title: 'Laporan Inventaris Ruang Jumlah',
            closeAction: 'destroy',
            width: 485,
            height: 530,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgInvLapInventarisRuangJumlah()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkInvLapInventarisRuangJumlah',
					handler: function()
					{
						if(Ext.get('cbUrutan_InvLapInventarisRuangJumlah').getValue()=='Nama barang'){
							var params={
								KdInv:KdInv_InvLapInventarisRuangJumlah
								
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/inventaris/lap_inventarisruangjumlah/cetakNamaBarang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgInvLapInventarisRuangJumlah.close();
						} else{
							var params={
								KdInv:KdInv_InvLapInventarisRuangJumlah
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/inventaris/lap_inventarisruangjumlah/cetakLokasiRuang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgInvLapInventarisRuangJumlah.close();
						}
						
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelInvLapInventarisRuangJumlah',
					handler: function()
					{
						frmDlgInvLapInventarisRuangJumlah.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winInvLapInventarisRuangJumlahReport;
};


function ItemDlgInvLapInventarisRuangJumlah()
{
    var PnlInvLapInventarisRuangJumlah = new Ext.Panel
    (
        {
            id: 'PnlInvLapInventarisRuangJumlah',
            fileUpload: true,
            layout: 'form',
            height: 525,
            anchor: '100%',
            bodyStyle: 'padding:10px',
            border: true,
            items:
            [
				getItemInvLapInventarisRuangJumlah_tree(),
				gridDataViewBarang_InvLapInventarisRuangJumlah(),
				getItemInvLapInventarisRuangJumlah_Batas(),
				getItemInvLapInventarisRuangJumlah_Pilihan()
            ]
        }
    );

    return PnlInvLapInventarisRuangJumlah;
};


function getItemInvLapInventarisRuangJumlah_tree()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
           itemsTreeListKelompokBarangGridDataView_InvLapInventarisRuangJumlah(),
		]
    };
    return items;
};



function GetStrTreeSetBarang_InvLapInventarisRuangJumlah()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and left(kd_inv,1)<>'8'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKelompokBarangGridDataView_InvLapInventarisRuangJumlah()
{	
		
	treeKlas_kelompokbarang_InvLapInventarisRuangJumlah= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:250,
			width:430,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_InvLapInventarisRuangJumlah=n.attributes
					if (strTreeCriteriKelompokBarang_InvLapInventarisRuangJumlah.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_InvLapInventarisRuangJumlah.leaf == false)
						{
							
						}
						else
						{
							console.log(strTreeCriteriKelompokBarang_InvLapInventarisRuangJumlah);
							console.log(n.attributes.id);
							dataGridLookUpBarangInvLapInventarisRuangJumlah(n.attributes.id);
							
							if(asc==0){
								asc=1;
								GetStrTreeSetBarang_InvLapInventarisRuangJumlah();
								rootTreeMasterBarang_InvLapInventarisRuangJumlah = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdRootKelompokBarang_pInvLapInventarisRuangJumlah,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeKlas_kelompokbarang_InvLapInventarisRuangJumlah.setRootNode(rootTreeMasterBarang_InvLapInventarisRuangJumlah);
						    } 
							
						};
					}
					else
					{
						
					};
				},
			}
		}
	);
	
	rootTreeMasterBarang_InvLapInventarisRuangJumlah = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdRootKelompokBarang_pInvLapInventarisRuangJumlah,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_InvLapInventarisRuangJumlah.setRootNode(rootTreeMasterBarang_InvLapInventarisRuangJumlah);  
	
    var pnlTreeFormDataWindowPopup_InvLapInventarisRuangJumlah = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_InvLapInventarisRuangJumlah,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_InvLapInventarisRuangJumlah;
}

function getItemInvLapInventarisRuangJumlah_Pilihan()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  448,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Urut berdasarkan'
					}, 
					{
						x: 100,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					comboUrutan_InvLapInventarisRuangJumlah(),
				]
			}
		]
    };
    return items;
};

function getItemInvLapInventarisRuangJumlah_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function comboUrutan_InvLapInventarisRuangJumlah()
{
    var cboUrutan_InvLapInventarisRuangJumlah = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cbUrutan_InvLapInventarisRuangJumlah',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Nama barang'], [2, 'Lokasi/Ruang']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
					'select': function(a,b,c)
					{
						
					}
                }
            }
	);
	return cboUrutan_InvLapInventarisRuangJumlah;
};

function gridDataViewBarang_InvLapInventarisRuangJumlah()
{
    var FieldGrdBArang_InvLapInventarisRuangJumlah = [];
	
    dsDataGrdBarang_InvLapInventarisRuangJumlah= new WebApp.DataStore
	({
        fields: FieldGrdBArang_InvLapInventarisRuangJumlah
    });
    
    gridPanelLookUpBarang_InvLapInventarisRuangJumlah =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_InvLapInventarisRuangJumlah,
		height:130,
		width:448,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_InvLapInventarisRuangJumlah.getAt(row).data.kd_inv);
					KdInv_InvLapInventarisRuangJumlah = dsDataGrdBarang_InvLapInventarisRuangJumlah.getAt(row).data.kd_inv;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'inv_kd_inv',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvLapInventarisRuangJumlah;
}

function dataGridLookUpBarangInvLapInventarisRuangJumlah(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/lap_inventarisruangjumlah/getGridLookUpBarang",
			params: {inv_kd_inv:inv_kd_inv},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_InvLapInventarisRuangJumlah.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_InvLapInventarisRuangJumlah.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_InvLapInventarisRuangJumlah.add(recs);
					
					
					
					gridPanelLookUpBarang_InvLapInventarisRuangJumlah.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
