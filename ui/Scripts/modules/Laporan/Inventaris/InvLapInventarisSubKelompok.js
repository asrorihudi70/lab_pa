
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapInventarisSubKelompok;
var varInvLapInventarisSubKelompok= ShowFormInvLapInventarisSubKelompok();
var asc=0;
var IdRootKelompokBarang_pInvLapInventarisSubKelompok='1000000000';
var KdInv_InvLapInventarisSubKelompok;
var NamaSub_InvLapInventarisSubKelompok;
var gridPanelLookUpBarang_InvLapInventarisSubKelompok;
var sendDataArray = [];


function ShowFormInvLapInventarisSubKelompok()
{
    frmDlgInvLapInventarisSubKelompok= fnDlgInvLapInventarisSubKelompok();
    frmDlgInvLapInventarisSubKelompok.show();
	GetStrTreeSetBarang_InvLapInventarisSubKelompok();
};

function fnDlgInvLapInventarisSubKelompok()
{
    var winInvLapInventarisSubKelompokReport = new Ext.Window
    (
        {
            id: 'winInvLapInventarisSubKelompokReport',
            title: 'Laporan Inventaris Sub Kelompok',
            closeAction: 'destroy',
            width: 485,
            height: 540,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgInvLapInventarisSubKelompok()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkInvLapInventarisSubKelompok',
					handler: function()
					{
							var params={
								KdInv:KdInv_InvLapInventarisSubKelompok,
								namaSub:NamaSub_InvLapInventarisSubKelompok,
								tglAwal:Ext.getCmp('dtpTglAwalInvLapStokPersediaanBarang').getValue(),
								tahun:Ext.getCmp('dtpTglAwalInvLapStokPersediaanBarang').getValue().format('Y'),
								sekarang:now
								
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/inventaris/lap_inventarissubkelompok/cetakBarang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
						
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelInvLapInventarisSubKelompok',
					handler: function()
					{
						frmDlgInvLapInventarisSubKelompok.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winInvLapInventarisSubKelompokReport;
};


function ItemDlgInvLapInventarisSubKelompok()
{
    var PnlInvLapInventarisSubKelompok = new Ext.Panel
    (
        {
            id: 'PnlInvLapInventarisSubKelompok',
            fileUpload: true,
            layout: 'form',
            height: 425,
            anchor: '100%',
            bodyStyle: 'padding:10px',
            border: true,
            items:
            [
				getItemInvLapInventarisSubKelompok_Tanggal(),
				getItemInvLapInventarisSubKelompok_Batas(),
				getItemInvLapInventarisSubKelompok_tree(),
				gridDataViewBarang_InvLapInventarisSubKelompok(),
				getItemInvLapInventarisSubKelompok_Batas(),
            ]
        }
    );

    return PnlInvLapInventarisSubKelompok;
};


function getItemInvLapInventarisSubKelompok_tree()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
           itemsTreeListKelompokBarangGridDataView_InvLapInventarisSubKelompok(),
		]
    };
    return items;
};



function GetStrTreeSetBarang_InvLapInventarisSubKelompok()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and left(kd_inv,1)<>'8'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKelompokBarangGridDataView_InvLapInventarisSubKelompok()
{	
		
	treeKlas_kelompokbarang_InvLapInventarisSubKelompok= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:225,
			width:430,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_InvLapInventarisSubKelompok=n.attributes
					
					if (strTreeCriteriKelompokBarang_InvLapInventarisSubKelompok.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_InvLapInventarisSubKelompok.leaf == false)
						{
							
						}
						else
						{
							console.log(n.attributes.id);
							dataGridLookUpBarangInvLapInventarisSubKelompok(n.attributes.id);
							KdInv_InvLapInventarisSubKelompok=n.attributes.id;
							NamaSub_InvLapInventarisSubKelompok=n.attributes.text;
							if(asc==0){
								asc=1;
								GetStrTreeSetBarang_InvLapInventarisSubKelompok();
								rootTreeMasterBarang_InvLapInventarisSubKelompok = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdRootKelompokBarang_pInvLapInventarisSubKelompok,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeKlas_kelompokbarang_InvLapInventarisSubKelompok.setRootNode(rootTreeMasterBarang_InvLapInventarisSubKelompok);
						    } 
							
						};
					}
					else
					{
						
					};
				},
			}
		}
	);
	
	rootTreeMasterBarang_InvLapInventarisSubKelompok = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdRootKelompokBarang_pInvLapInventarisSubKelompok,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_InvLapInventarisSubKelompok.setRootNode(rootTreeMasterBarang_InvLapInventarisSubKelompok);  
	
    var pnlTreeFormDataWindowPopup_InvLapInventarisSubKelompok = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_InvLapInventarisSubKelompok,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_InvLapInventarisSubKelompok;
}

function getItemInvLapInventarisSubKelompok_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  448,
				height: 50,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAwalInvLapStokPersediaanBarang',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 10,
						y: 30,
						xtype: 'label',
						text: 'Sub Kelompok Barang'
					}, 
				]
			}
		]
    };
    return items;
};


function getItemInvLapInventarisSubKelompok_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};



function gridDataViewBarang_InvLapInventarisSubKelompok()
{
    var FieldGrdBArang_InvLapInventarisSubKelompok = [];
	
    dsDataGrdBarang_InvLapInventarisSubKelompok= new WebApp.DataStore
	({
        fields: FieldGrdBArang_InvLapInventarisSubKelompok
    });
    
    gridPanelLookUpBarang_InvLapInventarisSubKelompok =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_InvLapInventarisSubKelompok,
		height:120,
		width:448,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_InvLapInventarisSubKelompok.getAt(row).data.kd_inv);
					KdInv_InvLapInventarisSubKelompok = dsDataGrdBarang_InvLapInventarisSubKelompok.getAt(row).data.kd_inv;
					NamaSub_InvLapInventarisSubKelompok = dsDataGrdBarang_InvLapInventarisSubKelompok.getAt(row).data.nama_sub;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'inv_kd_inv',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvLapInventarisSubKelompok;
}

function dataGridLookUpBarangInvLapInventarisSubKelompok(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/lap_inventarissubkelompok/getGridLookUpBarang",
			params: {inv_kd_inv:inv_kd_inv},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_InvLapInventarisSubKelompok.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_InvLapInventarisSubKelompok.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_InvLapInventarisSubKelompok.add(recs);
					
					
					
					gridPanelLookUpBarang_InvLapInventarisSubKelompok.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
