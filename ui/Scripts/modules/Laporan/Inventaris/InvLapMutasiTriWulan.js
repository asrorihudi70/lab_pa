
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapMutasiTriwulan_Inv;
var selectNamaLapMutasiTriwulan_Inv;
var now = new Date();
var selectSetPerseorangan;
var frmLapMutasiTriwulan_Inv;
var varLapMutasiTriwulan_Inv= ShowFormLapMutasiTriwulan_Inv();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var nomorCmbMutasiTriwulanInv;
function ShowFormLapMutasiTriwulan_Inv()
{
    frmLapMutasiTriwulan_Inv= fnLapMutasiTriwulan_Inv();
    frmLapMutasiTriwulan_Inv.show();
};

function fnLapMutasiTriwulan_Inv()
{
    var winLapMutasiTriwulan_InvReport = new Ext.Window
    (
        {
            id: 'winLapMutasiTriwulan_InvReport',
            title: 'Laporan Mutasi Inventaris Triwulan (LMBT)',
            closeAction: 'destroy',
            width: 405,
            height: 190,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapMutasiTriwulan_Inv()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapMutasiTriwulan_Inv',
					handler: function()
					{
						var params={
							vtriwulan:nomorCmbMutasiTriwulanInv,
							namatriwulan:Ext.getCmp('cbo_LapTriwulanInv').getValue(),
							tanggal:now,
							tahun:Ext.getCmp('dtpTahunMutasiTriwulan_Inv').getValue().format("Y")
						};
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/inventaris/lap_mutasitriwulan/cetakmutasitriwulan");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();
						frmLapMutasiTriwulan_Inv.close();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapMutasiTriwulan_Inv',
					handler: function()
					{
						frmLapMutasiTriwulan_Inv.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapMutasiTriwulan_InvReport;
};


function ItemLapMutasiTriwulan_Inv()
{
    var PnlLapMutasiTriwulan_Inv = new Ext.Panel
    (
        {
            id: 'PnlLapMutasiTriwulan_Inv',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					xtype: 'fieldset',
					title: '',
					items: 
					[
						getItemLapMutasiTriwulan_Inv_Tahun(),
						getItemLapMutasiTriwulan_Inv_Triwulan(),
						
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 0 0 0' },
							style:{'margin-left':'30px','margin-top':'0px'},
							anchor: '94%',
							layoutConfig:
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							}
						}
					]
			}
            ]
        }
    );

    return PnlLapMutasiTriwulan_Inv;
};

function getItemLapMutasiTriwulan_Inv_Triwulan()
{
	
    var itemss = {
        layout: 'column',
        border: false,
        items: 
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width:  320,
				height: 45,
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Triwulan'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					ComboTriwulanInvOrderDietLap()
				]
			}
		]
    };
    return itemss;
};

function getItemLapMutasiTriwulan_Inv_Tahun()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width:  355,
				height: 35,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 0,
						xtype: 'label',
						text: 'Tahun'
					}, 
					{
						x: 90,
						y: 0,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTahunMutasiTriwulan_Inv',
						format: 'Y',
						value: now
					}, 
				]
			}
		]
    };
    return items;
};


function ComboTriwulanInvOrderDietLap()
{
	var dsvStoreCmbTriwulan = new Ext.data.ArrayStore({
		   fields: ['I (Satu)','II (Dua)', 'III (Tiga)', 'IV (Empat)'],
		});
    /*var Field_TriwulanInvOrderDiet = ['kd_Triwulan', 'Triwulan'];
    ds_TriwulanInvOrderDietLap = new WebApp.DataStore({fields: Field_TriwulanInvOrderDiet});
    ds_TriwulanInvOrderDietLap.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_Triwulan',
					Sortdir: 'ASC',
					target: 'ComboTriwulanInv',
					param: ''
				}
		}
	);*/
	
    var cbo_LapTriwulanInvOrderDiet = new Ext.form.ComboBox
    (
        {
			x: 100,
			y: 10,
            flex: 1,
			id: 'cbo_LapTriwulanInv',
			/*valueField: 'kd_Triwulan',
            //displayField: 'Triwulan',*/
			store: ['I (Satu)','II (Dua)', 'III (Tiga)', 'IV (Empat)'],
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					nomorCmbMutasiTriwulanInv=parseInt(c)+1;
					console.log(nomorCmbMutasiTriwulanInv);
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapTriwulanInvOrderDiet;
};

function ShowPesanWarningLapMutasiTriwulan_InvReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
