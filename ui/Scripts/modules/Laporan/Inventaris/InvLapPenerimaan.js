var print=true;
var DlgLapPenerimaanVendorPerObat={
	Dropdown:{
		vendor:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	getData:function(){
		var $this=this;
		
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/inventaris/lap_penerimaan/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.Dropdown.vendor).add(r.data.vendor);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		params['start_date']=timestimetodate($this.DateField.startDate.getValue());
		params['last_date']=timestimetodate($this.DateField.endDate.getValue());
		params['vendor']=$this.Dropdown.vendor.getValue();
		
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("target", "_blank");
		if(print == true){
			form.setAttribute("action", baseURL + "index.php/inventaris/lap_penerimaan/doPrintDirect");
		} else{
			form.setAttribute("action", baseURL + "index.php/inventaris/lap_penerimaan/doPrint");
		}
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "data");
		hiddenField.setAttribute("value", Ext.encode(params));
		form.appendChild(hiddenField);
		document.body.appendChild(form);
		form.submit();
		loadMask.hide();
		
		// $.ajax({
			// type: 'POST',
			// dataType:'JSON',
			// data:params,
			// url:baseURL + "index.php/gudang_farmasi/lap_penerimaanvendorperobat/doPrint",
			// success: function(r){
				// loadMask.hide();
				// if(r.result=='SUCCESS'){
					// $this.Window.main.close();
					// window.open(r.data, '_blank', 'location=0,resizable=1', false);
				// }else{
					// Ext.Msg.alert('Gagal',r.message);
				// }
			// },
			// error: function(jqXHR, exception) {
				// loadMask.hide();
				// Nci.ajax.ErrorMessage(jqXHR, exception);
			// }
		// });
	},
	init:function(){
		var $this=this;
		
		$this.Window.main=Q().window({
			title:'Laporan Penerimaan',
			fbar:[
				/* new Ext.Button({
					text:'Print',
					handler:function(){
						print=true;
						$this.doPrint();
					}
				}), */
				new Ext.Button({
					text:'Ok',
					handler:function(){
						print=false;
						$this.doPrint();
					}
				}), 
				new Ext.Button({
					text:'Batal',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().input({
							label:'Periode',
							width: 350,
							items:[
								$this.DateField.startDate=Q().datefield({
									width:100
								}),
								Q().display({value:'s/d',width:10}),
								$this.DateField.endDate=Q().datefield({
									width:100
								})
							]
						}),
						Q().input({
							label:'Vendor',
							items:[
								$this.Dropdown.vendor=Q().dropdown({
									width: 240,
									emptyText: 'Semua'
								})
							]
						})
					]
				})
			]
		}).show();
		$this.getData();
	}
};
DlgLapPenerimaanVendorPerObat.init();