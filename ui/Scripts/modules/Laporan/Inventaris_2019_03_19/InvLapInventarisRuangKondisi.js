
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapInventarisRuangKondisi;
var varInvLapInventarisRuangKondisi= ShowFormInvLapInventarisRuangKondisi();
var asc=0;
var IdRootKelompokBarang_pInvLapInventarisRuangKondisi='1000000000';
var KdInv_InvLapInventarisRuangKondisi;
var gridPanelLookUpBarang_InvLapInventarisRuangKondisi;



function ShowFormInvLapInventarisRuangKondisi()
{
    frmDlgInvLapInventarisRuangKondisi= fnDlgInvLapInventarisRuangKondisi();
    frmDlgInvLapInventarisRuangKondisi.show();
	GetStrTreeSetBarang_InvLapInventarisRuangKondisi();
};

function fnDlgInvLapInventarisRuangKondisi()
{
    var winInvLapInventarisRuangKondisiReport = new Ext.Window
    (
        {
            id: 'winInvLapInventarisRuangKondisiReport',
            title: 'Laporan Inventaris Ruang Kondisi',
            closeAction: 'destroy',
            width: 485,
            height: 540,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgInvLapInventarisRuangKondisi()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkInvLapInventarisRuangKondisi',
					handler: function()
					{
						if(Ext.get('cbUrutan_InvLapInventarisRuangKondisi').getValue()=='Nama barang'){
							var params={
								KdInv:KdInv_InvLapInventarisRuangKondisi,
								tglAwal:Ext.getCmp('dtpTglAwalInvLapStokPersediaanBarang').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirInvLapStokPersediaanBarang').getValue()
								
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/inventaris/lap_inventarisruangkondisi/cetakNamaBarang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgInvLapInventarisRuangKondisi.close();
						} else{
							var params={
								KdInv:KdInv_InvLapInventarisRuangKondisi,
								tglAwal:Ext.getCmp('dtpTglAwalInvLapStokPersediaanBarang').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirInvLapStokPersediaanBarang').getValue()
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/inventaris/lap_inventarisruangkondisi/cetakLokasiRuang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							//frmDlgInvLapInventarisRuangKondisi.close();
						}
						
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelInvLapInventarisRuangKondisi',
					handler: function()
					{
						frmDlgInvLapInventarisRuangKondisi.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winInvLapInventarisRuangKondisiReport;
};


function ItemDlgInvLapInventarisRuangKondisi()
{
    var PnlInvLapInventarisRuangKondisi = new Ext.Panel
    (
        {
            id: 'PnlInvLapInventarisRuangKondisi',
            fileUpload: true,
            layout: 'form',
            height: 525,
            anchor: '100%',
            bodyStyle: 'padding:10px',
            border: true,
            items:
            [
				getItemInvLapInventarisRuangKondisi_Tanggal(),
				getItemInvLapInventarisRuangKondisi_Batas(),
				getItemInvLapInventarisRuangKondisi_tree(),
				gridDataViewBarang_InvLapInventarisRuangKondisi(),
				getItemInvLapInventarisRuangKondisi_Batas(),
				getItemInvLapInventarisRuangKondisi_Pilihan()
            ]
        }
    );

    return PnlInvLapInventarisRuangKondisi;
};


function getItemInvLapInventarisRuangKondisi_tree()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
           itemsTreeListKelompokBarangGridDataView_InvLapInventarisRuangKondisi(),
		]
    };
    return items;
};



function GetStrTreeSetBarang_InvLapInventarisRuangKondisi()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and left(kd_inv,1)<>'8'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKelompokBarangGridDataView_InvLapInventarisRuangKondisi()
{	
		
	treeKlas_kelompokbarang_InvLapInventarisRuangKondisi= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:225,
			width:430,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_InvLapInventarisRuangKondisi=n.attributes
					if (strTreeCriteriKelompokBarang_InvLapInventarisRuangKondisi.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_InvLapInventarisRuangKondisi.leaf == false)
						{
							
						}
						else
						{
							console.log(n.attributes.id);
							dataGridLookUpBarangInvLapInventarisRuangKondisi(n.attributes.id);
							
							if(asc==0){
								asc=1;
								GetStrTreeSetBarang_InvLapInventarisRuangKondisi();
								rootTreeMasterBarang_InvLapInventarisRuangKondisi = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdRootKelompokBarang_pInvLapInventarisRuangKondisi,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeKlas_kelompokbarang_InvLapInventarisRuangKondisi.setRootNode(rootTreeMasterBarang_InvLapInventarisRuangKondisi);
						    } 
							
						};
					}
					else
					{
						
					};
				},
			}
		}
	);
	
	rootTreeMasterBarang_InvLapInventarisRuangKondisi = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdRootKelompokBarang_pInvLapInventarisRuangKondisi,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_InvLapInventarisRuangKondisi.setRootNode(rootTreeMasterBarang_InvLapInventarisRuangKondisi);  
	
    var pnlTreeFormDataWindowPopup_InvLapInventarisRuangKondisi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_InvLapInventarisRuangKondisi,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_InvLapInventarisRuangKondisi;
}

function getItemInvLapInventarisRuangKondisi_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  448,
				height: 50,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAwalInvLapStokPersediaanBarang',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 210,
						y: 5,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 235,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAkhirInvLapStokPersediaanBarang',
						format: 'd/M/Y',
						value: now,
						width: 100
					},
					{
						x: 10,
						y: 30,
						xtype: 'label',
						text: 'Sub-sub Kelompok Barang'
					}, 
				]
			}
		]
    };
    return items;
};

function getItemInvLapInventarisRuangKondisi_Pilihan()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  448,
				height: 35,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Urut berdasarkan'
					}, 
					{
						x: 100,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					comboUrutan_InvLapInventarisRuangKondisi(),
				]
			}
		]
    };
    return items;
};

function getItemInvLapInventarisRuangKondisi_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function comboUrutan_InvLapInventarisRuangKondisi()
{
    var cboUrutan_InvLapInventarisRuangKondisi = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 5,
                id:'cbUrutan_InvLapInventarisRuangKondisi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Nama barang'], [2, 'Lokasi/Ruang']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
					'select': function(a,b,c)
					{
						
					}
                }
            }
	);
	return cboUrutan_InvLapInventarisRuangKondisi;
};

function gridDataViewBarang_InvLapInventarisRuangKondisi()
{
    var FieldGrdBArang_InvLapInventarisRuangKondisi = [];
	
    dsDataGrdBarang_InvLapInventarisRuangKondisi= new WebApp.DataStore
	({
        fields: FieldGrdBArang_InvLapInventarisRuangKondisi
    });
    
    gridPanelLookUpBarang_InvLapInventarisRuangKondisi =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_InvLapInventarisRuangKondisi,
		height:120,
		width:448,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_InvLapInventarisRuangKondisi.getAt(row).data.kd_inv);
					KdInv_InvLapInventarisRuangKondisi = dsDataGrdBarang_InvLapInventarisRuangKondisi.getAt(row).data.kd_inv;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'inv_kd_inv',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvLapInventarisRuangKondisi;
}

function dataGridLookUpBarangInvLapInventarisRuangKondisi(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/lap_inventarisruangkondisi/getGridLookUpBarang",
			params: {inv_kd_inv:inv_kd_inv},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_InvLapInventarisRuangKondisi.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_InvLapInventarisRuangKondisi.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_InvLapInventarisRuangKondisi.add(recs);
					
					
					
					gridPanelLookUpBarang_InvLapInventarisRuangKondisi.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
