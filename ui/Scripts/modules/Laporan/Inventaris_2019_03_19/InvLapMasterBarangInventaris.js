
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgInvLapMasterBarang;
var varInvLapMasterBarang= ShowFormInvLapMasterBarang();
var asc=0;
var IdRootKelompokBarang_pInvLapMasterBarang='1000000000';
var KdInv_InvLapMasterBarang;
var gridPanelLookUpBarang_InvLapMasterBarang;
var jenis;


function ShowFormInvLapMasterBarang()
{
    frmDlgInvLapMasterBarang= fnDlgInvLapMasterBarang();
    frmDlgInvLapMasterBarang.show();
	GetStrTreeSetBarang_InvLapMasterBarang();
};

function fnDlgInvLapMasterBarang()
{
    var winInvLapMasterBarangReport = new Ext.Window
    (
        {
            id: 'winInvLapMasterBarangReport',
            title: 'Laporan Master Barang',
            closeAction: 'destroy',
            width: 450,
            height: 130,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgInvLapMasterBarang()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkInvLapMasterBarang',
					handler: function()
					{
						var params={
							gol:Ext.getCmp('cbGolBarangInvLapMasterBarang').getValue()
						} ;
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/inventaris/lap_masterbarang/cetakBarang");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();		
					//	frmDlgInvLapMasterBarang.close();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelInvLapMasterBarang',
					handler: function()
					{
						frmDlgInvLapMasterBarang.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
					
				}
			}

        }
    );

    return winInvLapMasterBarangReport;
};


function ItemDlgInvLapMasterBarang()
{
    var PnlInvLapMasterBarang = new Ext.Panel
    (
        {
            id: 'PnlInvLapMasterBarang',
            fileUpload: true,
            layout: 'form',
            height: '70',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				getItemInvLapMasterBarang_Pilihan()
            ]
        }
    );

    return PnlInvLapMasterBarang;
};


function getItemInvLapMasterBarang_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemInvLapMasterBarang_Pilihan()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  425,
				height: 50,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Golongan'
					}, 
					{
						x: 130,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					comboGolBarangInvLapMasterBarang()
				]
			}
		]
    };
    return items;
};


function comboGolBarangInvLapMasterBarang(){
	var Field_Vendor = ['kd', 'gol'];
    ds_GolBarangInvLapMasterBarang = new WebApp.DataStore({fields: Field_Vendor});
    ds_GolBarangInvLapMasterBarang.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd',
	        Sortdir: 'ASC',
	        target: 'ComboGolBarangInventaris',
	        param: ""
        }
    });
	var cboGolBarangInvLapMasterBarang = new Ext.form.ComboBox({
		x: 140,
		y: 10,
        id:'cbGolBarangInvLapMasterBarang',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        width: 200,
		readOnly:false,
		fieldLabel:'PBF ',
        store: ds_GolBarangInvLapMasterBarang,
        valueField: 'kd',
        displayField: 'gol',
        listeners:{
			'select': function(a,b,c){
				
			}
        }
	});
	return cboGolBarangInvLapMasterBarang;
};


function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
