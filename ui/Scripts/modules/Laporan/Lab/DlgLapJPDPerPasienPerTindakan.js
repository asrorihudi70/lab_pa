
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgDlgLapJPDPerPasienPerTindakan;
var varDlgLapJPDPerPasienPerTindakan= ShowFormDlgLapJPDPerPasienPerTindakan();
var asc=0;
var IdRootKelompokBarang_pDlgLapJPDPerPasienPerTindakan='1000000000';
var KdJPD_DlgLapJPDPerPasienPerTindakan;
var NamaSub_DlgLapJPDPerPasienPerTindakan;
var gridPanelLookUpBarang_DlgLapJPDPerPasienPerTindakan;
var sendDataArray = [];
var dsvComboDokterDlgLapJPDPerPasienPerTindakan;
var dsvComboSubKelompokPasienDlgLapJPDPerPasienPerTindakan;

function ShowFormDlgLapJPDPerPasienPerTindakan()
{
    frmDlgDlgLapJPDPerPasienPerTindakan= fnDlgDlgLapJPDPerPasienPerTindakan();
    frmDlgDlgLapJPDPerPasienPerTindakan.show();
	//GetStrpasienSetBarang_DlgLapJPDPerPasienPerTindakan();
};

function fnDlgDlgLapJPDPerPasienPerTindakan()
{
    var winDlgLapJPDPerPasienPerTindakanReport = new Ext.Window
    (
        {
            id: 'winDlgLapJPDPerPasienPerTindakanReport',
            title: 'Laporan Jasa Pelayanan Dokter Per Pasien Per Tindakan',
            closeAction: 'destroy',
            width: 370,
            height: 320,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgDlgLapJPDPerPasienPerTindakan()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkDlgLapJPDPerPasienPerTindakan',
					handler: function()
					{
							var params={
								KdJPD:KdJPD_DlgLapJPDPerPasienPerTindakan,
								namaSub:NamaSub_DlgLapJPDPerPasienPerTindakan,
								tglAwal:Ext.getCmp('dtpTglAwalJPDLapStokPersediaanBarang').getValue(),
								tahun:Ext.getCmp('dtpTglAwalJPDLapStokPersediaanBarang').getValue().format('Y'),
								sekarang:now
								
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/JPD/lap_JPDPerPasienPerTindakan/cetakBarang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
						
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelDlgLapJPDPerPasienPerTindakan',
					handler: function()
					{
						frmDlgDlgLapJPDPerPasienPerTindakan.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winDlgLapJPDPerPasienPerTindakanReport;
};


function ItemDlgDlgLapJPDPerPasienPerTindakan()
{
    var PnlDlgLapJPDPerPasienPerTindakan = new Ext.Panel
    (
        {
            id: 'PnlDlgLapJPDPerPasienPerTindakan',
            fileUpload: true,
            layout: 'form',
            height: 370,
            anchor: '100%',
            bodyStyle: 'padding:10px',
            border: true,
            items:
            [
				getItemDlgLapJPDPerPasienPerTindakan_pasien(),
				getItemDlgLapJPDPerPasienPerTindakan_Batas(),
				getItemDlgLapJPDPerPasienPerTindakan_Tanggal(),
				//getItemDlgLapJPDPerPasienPerTindakan_Batas(),
				//getItemDlgLapJPDPerPasienPerTindakan_Shift(),
            ]
        }
    );

    return PnlDlgLapJPDPerPasienPerTindakan;
};


function getItemDlgLapJPDPerPasienPerTindakan_pasien()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  335,
				height: 125,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Asal Pasien'
					}, 
					{
						x: 100,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					comboAsalPasienListView_DlgLapJPDPerPasienPerTindakan(),
					{
						x: 10,
						y: 35,
						xtype: 'label',
						text: 'Kelompok Pasien'
					}, 
					{
						x: 100,
						y: 35,
						xtype: 'label',
						text: ' : '
					},
					comboKelompokPasienListView_DlgLapJPDPerPasienPerTindakan(),
					comboSubKelompokPasienListView_DlgLapJPDPerPasienPerTindakan(),
					{
						x: 10,
						y: 95,
						xtype: 'label',
						text: 'Dokter'
					}, 
					{
						x: 100,
						y: 95,
						xtype: 'label',
						text: ' : '
					},
					comboDokterPasienListView_DlgLapJPDPerPasienPerTindakan()
				]
			}
		]
    };
    return items;
};



function GetStrpasienSetBarang_DlgLapJPDPerPasienPerTindakan()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and left(kd_JPD,1)<>'8'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrpasienSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function comboAsalPasienListView_DlgLapJPDPerPasienPerTindakan()
{	
    var cbo_LapJPDPerPasienPerTindakanLabAsalPasien = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 5,
            flex: 1,
			id: 'cbo_AsalPasienLapJPDPerPasienPerTindakanLab',
			/*valueField: 'kd_Triwulan',
            //displayField: 'Triwulan',*/
			store: ['Semua Pasien','Pasien Rawat Jalan', 'Pasien Rawat Inap', 'Pasien Gawat Darurat', 'Pasien UMUM'],
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					/*nomorCmbMutasiTriwulanInv=parseInt(c)+1;
					console.log(nomorCmbMutasiTriwulanInv);*/
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDPerPasienPerTindakanLabAsalPasien;
};

function comboKelompokPasienListView_DlgLapJPDPerPasienPerTindakan()
{	
    var cbo_LapJPDPerPasienPerTindakanLabKelompokPasien = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 35,
            flex: 1,
			id: 'cbo_KelompokPasienLapJPDPerPasienPerTindakanLab',
			/*valueField: 'kd_Triwulan',
            //displayField: 'Triwulan',*/
			store: ['Semua','Perorangan', 'Perusahaan', 'Asuransi'],
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					if (c===0)
					{
						Ext.getCmp('cbo_SubKelompokPasienLapJPDPerPasienPerTindakanLab').disable(true);
					}
					else
					{
						Ext.getCmp('cbo_SubKelompokPasienLapJPDPerPasienPerTindakanLab').enable(true);
						Ext.getCmp('cbo_SubKelompokPasienLapJPDPerPasienPerTindakanLab').setValue('');
						dsComboSubKelompokPasienDlgLapJPDPerPasienPerTindakan(c)
					}
					dsComboSubKelompokPasienDlgLapJPDPerPasienPerTindakan(c)
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDPerPasienPerTindakanLabKelompokPasien;
};
function dsComboSubKelompokPasienDlgLapJPDPerPasienPerTindakan(q)
{
	if (q===1)
	{
		dsvComboSubKelompokPasienDlgLapJPDPerPasienPerTindakan.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSubKelompokPasienDlgLapJasaPelayananDokterDetailPerorangan',
                    //param : 'kd_tarif=1'
                }			
            }
        );  
	}
	else if (q===2)
	{
		dsvComboSubKelompokPasienDlgLapJPDPerPasienPerTindakan.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSubKelompokPasienDlgLapJasaPelayananDokterDetailPerusahaan',
                    //param : 'kd_tarif=1'
                }			
            }
        );  
	}
	else if (q===3)
	{
		dsvComboSubKelompokPasienDlgLapJPDPerPasienPerTindakan.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSubKelompokPasienDlgLapJasaPelayananDokterDetailAsuransi',
                    //param : 'kd_tarif=1'
                }			
            }
        );  
	}
	 
    return dsvComboSubKelompokPasienDlgLapJPDPerPasienPerTindakan;
}
function comboSubKelompokPasienListView_DlgLapJPDPerPasienPerTindakan()
{	
	var Field =['kd_customer','customer'];
    dsvComboSubKelompokPasienDlgLapJPDPerPasienPerTindakan = new WebApp.DataStore({fields: Field});
	dsComboSubKelompokPasienDlgLapJPDPerPasienPerTindakan();
    var cbo_LapJPDPerPasienPerTindakanLabSubKelompok = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 65,
            flex: 1,
			id: 'cbo_SubKelompokPasienLapJPDPerPasienPerTindakanLab',
			valueField: 'kd_customer',
            displayField: 'customer',
			store: dsvComboSubKelompokPasienDlgLapJPDPerPasienPerTindakan,
			mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			disabled: true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					/*nomorCmbMutasiTriwulanInv=parseInt(c)+1;
					console.log(nomorCmbMutasiTriwulanInv);*/
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDPerPasienPerTindakanLabSubKelompok;
};
function dsComboDokterDlgLapJPDPerPasienPerTindakan()
{
	dsvComboDokterDlgLapJPDPerPasienPerTindakan.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboDokterDlgLapJasaPelayananDokterDetail',
                    //param : 'kd_tarif=1'
                }			
            }
        );   
    return dsvComboDokterDlgLapJPDPerPasienPerTindakan;
}
function comboDokterPasienListView_DlgLapJPDPerPasienPerTindakan()
{	
	var Field =['kd_dokter','nama'];
    dsvComboDokterDlgLapJPDPerPasienPerTindakan = new WebApp.DataStore({fields: Field});
	dsComboDokterDlgLapJPDPerPasienPerTindakan();
    var cbo_LapJPDPerPasienPerTindakanLabDokter = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 95,
            flex: 1,
			id: 'cbo_DokterPasienLapJPDPerPasienPerTindakanLab',
			valueField: 'kd_dokter',
            displayField: 'nama',
			store: dsvComboDokterDlgLapJPDPerPasienPerTindakan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					/*nomorCmbMutasiTriwulanInv=parseInt(c)+1;
					console.log(nomorCmbMutasiTriwulanInv);*/
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDPerPasienPerTindakanLabDokter;
};

function getItemDlgLapJPDPerPasienPerTindakan_Tanggal()
{
	radioTanggal = new Ext.form.RadioGroup({
	x: 10,
	y: 5,
	id :'radiosTanggalJPDPerPasienPerTindakan',
	columns  : 1,
	rows : 2,
	items: 
	[
		{
			
			id:'rbTanggalJPDPerPasienPerTindakan',
			name: 'rbLapJPD',
			boxLabel: 'Tanggal',
			checked: true, 
			//inputValue: "1"
		},
		{
			x: 10,
			y: 55,
			id:'rbBulanJPDPerPasienPerTindakan',
			name: 'rbLapJPD',
			boxLabel: 'Bulan',
			//inputValue: "1"
		},
	],
	listeners: {
		change : function()
		{ 
			if (Ext.getCmp('rbBulanJPDPerPasienPerTindakan').checked===true)
			{
				Ext.getCmp('dtpTglAwalJPDLapPerPasienPerTindakan').disable(true)
				Ext.getCmp('dtpTglAkhirJPDLapPerPasienPerTindakan').disable(true)
				Ext.getCmp('dtpBlnAwalJPDLapPerPasienPerTindakan').enable(true)
				Ext.getCmp('dtpBlnAkhirJPDLapPerPasienPerTindakan').enable(true)
			}
			else
			{
				Ext.getCmp('dtpTglAwalJPDLapPerPasienPerTindakan').enable(true)
				Ext.getCmp('dtpTglAkhirJPDLapPerPasienPerTindakan').enable(true)
				Ext.getCmp('dtpBlnAwalJPDLapPerPasienPerTindakan').disable(true)
				Ext.getCmp('dtpBlnAkhirJPDLapPerPasienPerTindakan').disable(true)
			}
		}
	}
		
   });
   
   
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  335,
				height: 100,
				anchor: '100% 100%',
				items: [
					radioTanggal,
					  {
							x: 80,
							y: 5,
							xtype: 'datefield',
							id: 'dtpTglAwalJPDLapPerPasienPerTindakan',
							format: 'd/M/Y',
							value: now
						}
					,
					{
						x: 190,
						y: 5,
						xtype: 'label',
						text: ' s / d '
					},
					{
						x: 220,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAkhirJPDLapPerPasienPerTindakan',
						format: 'd/M/Y',
						value: now
					}, 
					//radioBulan,
					{
							x: 80,
							y: 30,
							xtype: 'datefield',
							id: 'dtpBlnAwalJPDLapPerPasienPerTindakan',
							disabled: true,
							format: 'M/Y',
							value: now
						}
					,
					{
						x: 190,
						y: 30,
						xtype: 'label',
						text: ' s / d '
					},
					{
						x: 220,
						y: 30,
						xtype: 'datefield',
						id: 'dtpBlnAkhirJPDLapPerPasienPerTindakan',
						disabled: true,
						format: 'M/Y',
						value: now
					}, 
					{
					   xtype: 'checkbox',
					   id: 'CekSemuaShiftLapJPDPerPasienPerTindakan',
					   hideLabel:false,
					   boxLabel: 'Semua',
					   checked: true,
					   x: 10,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
								if(Ext.getCmp('CekSemuaShiftLapJPDPerPasienPerTindakan').getValue()===true)
								{
									Ext.getCmp('CekShift1LapJPDPerPasienPerTindakan').setValue(true);
									Ext.getCmp('CekShift1LapJPDPerPasienPerTindakan').disable(true);
									
									Ext.getCmp('CekShift2LapJPDPerPasienPerTindakan').setValue(true);
									Ext.getCmp('CekShift2LapJPDPerPasienPerTindakan').disable(true);
									
									Ext.getCmp('CekShift3LapJPDPerPasienPerTindakan').setValue(true)
									Ext.getCmp('CekShift3LapJPDPerPasienPerTindakan').disable(true);
								}
								else
								{
									Ext.getCmp('CekShift1LapJPDPerPasienPerTindakan').setValue(true);
									Ext.getCmp('CekShift1LapJPDPerPasienPerTindakan').enable(true);
									
									Ext.getCmp('CekShift2LapJPDPerPasienPerTindakan').setValue(false)
									Ext.getCmp('CekShift2LapJPDPerPasienPerTindakan').enable(true)
									
									Ext.getCmp('CekShift3LapJPDPerPasienPerTindakan').setValue(false)
									Ext.getCmp('CekShift3LapJPDPerPasienPerTindakan').enable(true)
								}	
							}
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift1LapJPDPerPasienPerTindakan',
					   hideLabel:false,
					   boxLabel: 'Shift 1',
					   checked: true,
					   disabled: true,
					   x: 65,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift2LapJPDPerPasienPerTindakan',
					   hideLabel:false,
					   boxLabel: 'Shift 2',
					   checked: true,
					   disabled: true,
					   x: 125,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift3LapJPDPerPasienPerTindakan',
					   hideLabel:false,
					   boxLabel: 'Shift 3',
					   checked: true,
					   disabled: true,
					   x: 180,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
				]
			}
		]
    };
    return items;
};

/*function getItemDlgLapJPDPerPasienPerTindakan_Shift()
{
	var items = {
        layout: 'column',
        border: false,
        items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				anchor: '100% 100%',
				items: [
				{
				   xtype: 'checkbox',
				   id: 'CekSemuaShiftLapJPDPerPasienPerTindakan',
				   hideLabel:false,
				   //checked: false,
				   x: 10,
				   y: 5,
				   listeners: 
				   {
						check: function()
						{
								
						 }
				   }
				}
			]
		}]
	}
}*/
function getItemDlgLapJPDPerPasienPerTindakan_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};



function gridDataViewBarang_DlgLapJPDPerPasienPerTindakan()
{
    var FieldGrdBArang_DlgLapJPDPerPasienPerTindakan = [];
	
    dsDataGrdBarang_DlgLapJPDPerPasienPerTindakan= new WebApp.DataStore
	({
        fields: FieldGrdBArang_DlgLapJPDPerPasienPerTindakan
    });
    
    gridPanelLookUpBarang_DlgLapJPDPerPasienPerTindakan =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_DlgLapJPDPerPasienPerTindakan,
		height:120,
		width:448,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_DlgLapJPDPerPasienPerTindakan.getAt(row).data.kd_JPD);
					KdJPD_DlgLapJPDPerPasienPerTindakan = dsDataGrdBarang_DlgLapJPDPerPasienPerTindakan.getAt(row).data.kd_JPD;
					NamaSub_DlgLapJPDPerPasienPerTindakan = dsDataGrdBarang_DlgLapJPDPerPasienPerTindakan.getAt(row).data.nama_sub;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'JPD_kd_JPD',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_JPD',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viJPDPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_DlgLapJPDPerPasienPerTindakan;
}

function dataGridLookUpBarangDlgLapJPDPerPasienPerTindakan(JPD_kd_JPD){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/JPD/lap_JPDPerPasienPerTindakan/getGridLookUpBarang",
			params: {JPD_kd_JPD:JPD_kd_JPD},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_DlgLapJPDPerPasienPerTindakan.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_DlgLapJPDPerPasienPerTindakan.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_DlgLapJPDPerPasienPerTindakan.add(recs);
					
					
					
					gridPanelLookUpBarang_DlgLapJPDPerPasienPerTindakan.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
