
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgDlgLapJasaPelayananDokterDetail;
var varDlgLapJasaPelayananDokterDetail= ShowFormDlgLapJasaPelayananDokterDetail();
var asc=0;
var IdRootKelompokBarang_pDlgLapJasaPelayananDokterDetail='1000000000';
var KdJPD_DlgLapJasaPelayananDokterDetail;
var NamaSub_DlgLapJasaPelayananDokterDetail;
var gridPanelLookUpBarang_DlgLapJasaPelayananDokterDetail;
var sendDataArray = [];
var dsvComboDokterDlgLapJasaPelayananDokterDetail;
var dsvComboSubKelompokPasienDlgLapJasaPelayananDokterDetail;
var sendDataArray = [];
function ShowFormDlgLapJasaPelayananDokterDetail()
{
    frmDlgDlgLapJasaPelayananDokterDetail= fnDlgDlgLapJasaPelayananDokterDetail();
    frmDlgDlgLapJasaPelayananDokterDetail.show();
	//GetStrpasienSetBarang_DlgLapJasaPelayananDokterDetail();
};

function fnDlgDlgLapJasaPelayananDokterDetail()
{
    var winDlgLapJasaPelayananDokterDetailReport = new Ext.Window
    (
        {
            id: 'winDlgLapJasaPelayananDokterDetailReport',
            title: 'Laporan Jasa Pelayanan Dokter Detail',
            closeAction: 'destroy',
            width: 370,
            height: 320,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgDlgLapJasaPelayananDokterDetail()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkDlgLapJasaPelayananDokterDetail',
					handler: function()
					{
						if (ValidasiReportJPDDetailLab() === 1)
                        {
							var params={
								KdJPD:KdJPD_DlgLapJasaPelayananDokterDetail,
								namaSub:NamaSub_DlgLapJasaPelayananDokterDetail,
								tglAwal:Ext.getCmp('dtpTglAwalJPDLapStokPersediaanBarang').getValue(),
								tahun:Ext.getCmp('dtpTglAwalJPDLapStokPersediaanBarang').getValue().format('Y'),
								sekarang:now
								
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/JPD/lap_JPDDetail/cetakBarang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();
						}
						
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelDlgLapJasaPelayananDokterDetail',
					handler: function()
					{
						frmDlgDlgLapJasaPelayananDokterDetail.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winDlgLapJasaPelayananDokterDetailReport;
};
function GetCriteriaJPDDetailLab()
{
	var strKriteria = '';
	
	if (Ext.getCmp('cboPilihanTRLab').getValue() !== ''){
		strKriteria = 'asal_pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanTRLab').getValue();
	}
	if (Ext.getCmp('cboUserRequestEntryTRLab').getValue() !== ''){
		strKriteria += '##@@##' + 'username';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryTRLab').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryTRLab').getValue();
	}
	
	if (Ext.getCmp('cboPilihanTRLabkelompokPasien').getValue() !== '')
	{
		if (Ext.get('cboPilihanTRLabkelompokPasien').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
			strKriteria += '##@@##' + 'NULL';
			
        } else if (Ext.get('cboPilihanTRLabkelompokPasien').getValue() === 'Perseorangan'){
			
			/* var selectSetPerseorangan2='';
            selectSetPerseorangan2 = '0000000001';
			strKriteria += '##@@##' + 'Perseorangan';
            strKriteria += '##@@##' + 'Umum';
            strKriteria += '##@@##' + selectSetPerseorangan2; */
			strKriteria += '##@@##' + 'Perseorangan';
            strKriteria += '##@@##' + Ext.get('cboPerseoranganTRLab').getValue();
            strKriteria += '##@@##' + Ext.getCmp('cboPerseoranganTRLab').getValue();
			
        } else if (Ext.get('cboPilihanTRLabkelompokPasien').getValue() === 'Perusahaan'){
            
			if(Ext.getCmp('cboPerusahaanRequestEntryTRLab').getValue() === '')
            {
                strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Perusahaan';
				strKriteria += '##@@##' + selectsetnamaperusahaan;
                strKriteria += '##@@##' + selectsetperusahaan;
            }
        } else {
            
			if(Ext.get('cboAsuransiTRLab').getValue() === '')
            {
                strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Asuransi';
				strKriteria += '##@@##' + selectsetnamaAsuransi;
                strKriteria += '##@@##' + selectSetAsuransi;
            }
            
        } 
	} 
	
	if (Ext.getCmp('radioasal').getValue() === true){
		strKriteria += '##@@##' + 'Tanggal';
		strKriteria += '##@@##' + Ext.get('dtpTglAwalFilterHasilLab').getValue();
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterHasilLab').getValue();
	} else {
		strKriteria += '##@@##' + 'Bulan';
		strKriteria += '##@@##' + Ext.get('dtpBulanAwalFilterHasilLab').getValue();
		strKriteria += '##@@##' + Ext.get('dtpBulanAkhirFilterHasilLab').getValue();
	}
	
	
	if (Ext.getCmp('Shift_All_TRLab').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_TRLab').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_TRLab').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_TRLab').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	}
	return strKriteria;
};
function ValidasiReportJPDDetailLab()
{
	var x=1;
	if(Ext.getCmp('cbo_AsalPasienLapJPDDetailLab').getValue() === ''){
		ShowPesanWarningLapJPDDetail_LabReport('Asal Pasien Belum Dipilih','Laporan Jasa Pelayanan Dokter Detail');
        x=0;
	}
	if(Ext.getCmp('cbo_KelompokPasienLapJPDDetailLab').getValue() === ''){
		ShowPesanWarningLapJPDDetail_LabReport('Kelompok Pasien Belum Dipilih','Laporan Jasa Pelayanan Dokter Detail');
        x=0;
	}
	/*if(Ext.get('cbo_SubKelompokPasienLapJPDDetailLab').getValue() === ''){
		ShowPesanWarningLapJPDDetail_LabReport('Sub Kelompok Pasien Belum Dipilih','Laporan Jasa Pelayanan Dokter Detail');
        x=0;
	}*/
	if(Ext.get('cbo_DokterPasienLapJPDDetailLab').getValue() === ''){
		ShowPesanWarningLapJPDDetail_LabReport('Dokter Belum Dipilih','Laporan Jasa Pelayanan Dokter Detail');
        x=0;
	}
	if(Ext.getCmp('rbTanggalJPDDetail').getValue() === false && Ext.getCmp('rbBulanJPDDetail').getValue() === false)
    {
        ShowPesanWarningLapJPDDetail_LabReport('Periode belum dipilih','Laporan Jasa Pelayanan Dokter Detail');
        x=0;
    }
	if(Ext.getCmp('CekSemuaShiftLapJPDDetail').getValue() === false && Ext.getCmp('CekShift1LapJPDDetail').getValue() === false && Ext.getCmp('CekShift2LapJPDDetail').getValue() === false && Ext.getCmp('CekShift3LapJPDDetail').getValue() === false){
		ShowPesanWarningLapJPDDetail_LabReport('Shift Belum Dipilih','Laporan Jasa Pelayanan Dokter Detail');
        x=0;
	}

    return x;
}

function ItemDlgDlgLapJasaPelayananDokterDetail()
{
    var PnlDlgLapJasaPelayananDokterDetail = new Ext.Panel
    (
        {
            id: 'PnlDlgLapJasaPelayananDokterDetail',
            fileUpload: true,
            layout: 'form',
            height: 370,
            anchor: '100%',
            bodyStyle: 'padding:10px',
            border: true,
            items:
            [
				getItemDlgLapJasaPelayananDokterDetail_pasien(),
				getItemDlgLapJasaPelayananDokterDetail_Batas(),
				getItemDlgLapJasaPelayananDokterDetail_Tanggal(),
				//getItemDlgLapJasaPelayananDokterDetail_Batas(),
				//getItemDlgLapJasaPelayananDokterDetail_Shift(),
            ]
        }
    );

    return PnlDlgLapJasaPelayananDokterDetail;
};


function getItemDlgLapJasaPelayananDokterDetail_pasien()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  335,
				height: 125,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Asal Pasien'
					}, 
					{
						x: 100,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					comboAsalPasienListView_DlgLapJasaPelayananDokterDetail(),
					{
						x: 10,
						y: 35,
						xtype: 'label',
						text: 'Kelompok Pasien'
					}, 
					{
						x: 100,
						y: 35,
						xtype: 'label',
						text: ' : '
					},
					comboKelompokPasienListView_DlgLapJasaPelayananDokterDetail(),
					comboSubKelompokPasienListView_DlgLapJasaPelayananDokterDetail(),
					{
						x: 10,
						y: 95,
						xtype: 'label',
						text: 'Dokter'
					}, 
					{
						x: 100,
						y: 95,
						xtype: 'label',
						text: ' : '
					},
					comboDokterPasienListView_DlgLapJasaPelayananDokterDetail()
				]
			}
		]
    };
    return items;
};



function GetStrpasienSetBarang_DlgLapJasaPelayananDokterDetail()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and left(kd_JPD,1)<>'8'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrpasienSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function comboAsalPasienListView_DlgLapJasaPelayananDokterDetail()
{	
    var cbo_LapJPDDetailLabAsalPasien = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 5,
            flex: 1,
			id: 'cbo_AsalPasienLapJPDDetailLab',
			/*valueField: 'kd_Triwulan',
            //displayField: 'Triwulan',*/
			store: ['Semua Pasien','Pasien Rawat Jalan', 'Pasien Rawat Inap', 'Pasien Gawat Darurat'],
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					/*nomorCmbMutasiTriwulanInv=parseInt(c)+1;
					console.log(nomorCmbMutasiTriwulanInv);*/
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDDetailLabAsalPasien;
};

function comboKelompokPasienListView_DlgLapJasaPelayananDokterDetail()
{	
    var cbo_LapJPDDetailLabKelompokPasien = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 35,
            flex: 1,
			id: 'cbo_KelompokPasienLapJPDDetailLab',
			/*valueField: 'kd_Triwulan',
            //displayField: 'Triwulan',*/
			store: ['Semua','Perorangan', 'Perusahaan', 'Asuransi'],
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					if (c===0)
					{
						Ext.getCmp('cbo_SubKelompokPasienLapJPDDetailLab').disable(true);
					}
					else
					{
						Ext.getCmp('cbo_SubKelompokPasienLapJPDDetailLab').enable(true);
						Ext.getCmp('cbo_SubKelompokPasienLapJPDDetailLab').setValue('');
						dsComboSubKelompokPasienDlgLapJasaPelayananDokterDetail(c)
					}
					dsComboSubKelompokPasienDlgLapJasaPelayananDokterDetail(c)
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDDetailLabKelompokPasien;
};
function dsComboSubKelompokPasienDlgLapJasaPelayananDokterDetail(q)
{
	if (q===1)
	{
		dsvComboSubKelompokPasienDlgLapJasaPelayananDokterDetail.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSubKelompokPasienDlgLapJasaPelayananDokterDetailPerorangan',
                    //param : 'kd_tarif=1'
                }			
            }
        );  
	}
	else if (q===2)
	{
		dsvComboSubKelompokPasienDlgLapJasaPelayananDokterDetail.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSubKelompokPasienDlgLapJasaPelayananDokterDetailPerusahaan',
                    //param : 'kd_tarif=1'
                }			
            }
        );  
	}
	else if (q===3)
	{
		dsvComboSubKelompokPasienDlgLapJasaPelayananDokterDetail.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSubKelompokPasienDlgLapJasaPelayananDokterDetailAsuransi',
                    //param : 'kd_tarif=1'
                }			
            }
        );  
	}
	 
    return dsvComboSubKelompokPasienDlgLapJasaPelayananDokterDetail;
}
function comboSubKelompokPasienListView_DlgLapJasaPelayananDokterDetail()
{	
	var Field =['kd_customer','customer'];
    dsvComboSubKelompokPasienDlgLapJasaPelayananDokterDetail = new WebApp.DataStore({fields: Field});
	dsComboSubKelompokPasienDlgLapJasaPelayananDokterDetail();
    var cbo_LapJPDDetailLabSubKelompok = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 65,
            flex: 1,
			id: 'cbo_SubKelompokPasienLapJPDDetailLab',
			valueField: 'kd_customer',
            displayField: 'customer',
			store: dsvComboSubKelompokPasienDlgLapJasaPelayananDokterDetail,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			disabled:true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					console.log(c);
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDDetailLabSubKelompok;
};
function dsComboDokterDlgLapJasaPelayananDokterDetail()
{
	dsvComboDokterDlgLapJasaPelayananDokterDetail.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboDokterDlgLapJasaPelayananDokterDetail',
                    //param : 'kd_tarif=1'
                }			
            }
        );   
    return dsvComboDokterDlgLapJasaPelayananDokterDetail;
}
function comboDokterPasienListView_DlgLapJasaPelayananDokterDetail()
{	
	var Field =['kd_dokter','nama'];
    dsvComboDokterDlgLapJasaPelayananDokterDetail = new WebApp.DataStore({fields: Field});
	dsComboDokterDlgLapJasaPelayananDokterDetail();
    var cbo_LapJPDDetailLabDokter = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 95,
            flex: 1,
			id: 'cbo_DokterPasienLapJPDDetailLab',
			valueField: 'kd_dokter',
            displayField: 'nama',
			store: dsvComboDokterDlgLapJasaPelayananDokterDetail,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					/*nomorCmbMutasiTriwulanInv=parseInt(c)+1;
					console.log(nomorCmbMutasiTriwulanInv);*/
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDDetailLabDokter;
};

function getItemDlgLapJasaPelayananDokterDetail_Tanggal()
{
	radioTanggal = new Ext.form.RadioGroup({
	x: 10,
	y: 5,
	id :'radiosTanggalJPDDetail',
	columns  : 1,
	rows : 2,
	items: 
	[
		{
			
			id:'rbTanggalJPDDetail',
			name: 'rbLapJPD',
			boxLabel: 'Tanggal',
			checked: true, 
			//inputValue: "1"
		},
		{
			x: 10,
			y: 55,
			id:'rbBulanJPDDetail',
			name: 'rbLapJPD',
			boxLabel: 'Bulan',
			//inputValue: "1"
		},
	],
	listeners: {
		change : function()
		{ 
			if (Ext.getCmp('rbBulanJPDDetail').checked===true)
			{
				Ext.getCmp('dtpTglAwalJPDLapDetail').disable(true)
				Ext.getCmp('dtpTglAkhirJPDLapDetail').disable(true)
				Ext.getCmp('dtpBlnAwalJPDLapDetail').enable(true)
				Ext.getCmp('dtpBlnAkhirJPDLapDetail').enable(true)
			}
			else
			{
				Ext.getCmp('dtpTglAwalJPDLapDetail').enable(true)
				Ext.getCmp('dtpTglAkhirJPDLapDetail').enable(true)
				Ext.getCmp('dtpBlnAwalJPDLapDetail').disable(true)
				Ext.getCmp('dtpBlnAkhirJPDLapDetail').disable(true)
			}
		}
	}
		
   });
   
   
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  335,
				height: 100,
				anchor: '100% 100%',
				items: [
					radioTanggal,
					  {
							x: 80,
							y: 5,
							xtype: 'datefield',
							id: 'dtpTglAwalJPDLapDetail',
							format: 'd/M/Y',
							value: now
						}
					,
					{
						x: 190,
						y: 5,
						xtype: 'label',
						text: ' s / d '
					},
					{
						x: 220,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAkhirJPDLapDetail',
						format: 'd/M/Y',
						value: now
					}, 
					//radioBulan,
					{
							x: 80,
							y: 30,
							xtype: 'datefield',
							id: 'dtpBlnAwalJPDLapDetail',
							disabled: true,
							format: 'M/Y',
							value: now
						}
					,
					{
						x: 190,
						y: 30,
						xtype: 'label',
						text: ' s / d '
					},
					{
						x: 220,
						y: 30,
						xtype: 'datefield',
						id: 'dtpBlnAkhirJPDLapDetail',
						disabled: true,
						format: 'M/Y',
						value: now
					}, 
					{
					   xtype: 'checkbox',
					   id: 'CekSemuaShiftLapJPDDetail',
					   hideLabel:false,
					   boxLabel: 'Semua',
					   checked: true,
					   x: 10,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
								if(Ext.getCmp('CekSemuaShiftLapJPDDetail').getValue()===true)
								{
									Ext.getCmp('CekShift1LapJPDDetail').setValue(true);
									Ext.getCmp('CekShift1LapJPDDetail').disable(true);
									
									Ext.getCmp('CekShift2LapJPDDetail').setValue(true);
									Ext.getCmp('CekShift2LapJPDDetail').disable(true);
									
									Ext.getCmp('CekShift3LapJPDDetail').setValue(true)
									Ext.getCmp('CekShift3LapJPDDetail').disable(true);
								}
								else
								{
									Ext.getCmp('CekShift1LapJPDDetail').setValue(false);
									Ext.getCmp('CekShift1LapJPDDetail').enable(true);
									
									Ext.getCmp('CekShift2LapJPDDetail').setValue(false)
									Ext.getCmp('CekShift2LapJPDDetail').enable(true)
									
									Ext.getCmp('CekShift3LapJPDDetail').setValue(false)
									Ext.getCmp('CekShift3LapJPDDetail').enable(true)
								}	
							}
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift1LapJPDDetail',
					   hideLabel:false,
					   boxLabel: 'Shift 1',
					   checked: true,
					   disabled: true,
					   x: 65,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
								if(Ext.getCmp('CekShift1LapJPDDetail').getValue()===true)
								{
									sendDataArray.push("1");
									console.log(sendDataArray);
								}
								else
								{
									
								}
							}
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift2LapJPDDetail',
					   hideLabel:false,
					   boxLabel: 'Shift 2',
					   checked: true,
					   disabled: true,
					   x: 125,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift3LapJPDDetail',
					   hideLabel:false,
					   boxLabel: 'Shift 3',
					   checked: true,
					   disabled: true,
					   x: 180,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
				]
			}
		]
    };
    return items;
};

/*function getItemDlgLapJasaPelayananDokterDetail_Shift()
{
	var items = {
        layout: 'column',
        border: false,
        items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				anchor: '100% 100%',
				items: [
				{
				   xtype: 'checkbox',
				   id: 'CekSemuaShiftLapJPDDetail',
				   hideLabel:false,
				   //checked: false,
				   x: 10,
				   y: 5,
				   listeners: 
				   {
						check: function()
						{
								
						 }
				   }
				}
			]
		}]
	}
}*/
function getItemDlgLapJasaPelayananDokterDetail_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};



function gridDataViewBarang_DlgLapJasaPelayananDokterDetail()
{
    var FieldGrdBArang_DlgLapJasaPelayananDokterDetail = [];
	
    dsDataGrdBarang_DlgLapJasaPelayananDokterDetail= new WebApp.DataStore
	({
        fields: FieldGrdBArang_DlgLapJasaPelayananDokterDetail
    });
    
    gridPanelLookUpBarang_DlgLapJasaPelayananDokterDetail =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_DlgLapJasaPelayananDokterDetail,
		height:120,
		width:448,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_DlgLapJasaPelayananDokterDetail.getAt(row).data.kd_JPD);
					KdJPD_DlgLapJasaPelayananDokterDetail = dsDataGrdBarang_DlgLapJasaPelayananDokterDetail.getAt(row).data.kd_JPD;
					NamaSub_DlgLapJasaPelayananDokterDetail = dsDataGrdBarang_DlgLapJasaPelayananDokterDetail.getAt(row).data.nama_sub;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'JPD_kd_JPD',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_JPD',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viJPDPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_DlgLapJasaPelayananDokterDetail;
}

function dataGridLookUpBarangDlgLapJasaPelayananDokterDetail(JPD_kd_JPD){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/JPD/lap_JPDDetail/getGridLookUpBarang",
			params: {JPD_kd_JPD:JPD_kd_JPD},
			failure: function(o)
			{
				ShowPesanWarningLapJPDDetail_LabReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_DlgLapJasaPelayananDokterDetail.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_DlgLapJasaPelayananDokterDetail.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_DlgLapJasaPelayananDokterDetail.add(recs);
					
					
					
					gridPanelLookUpBarang_DlgLapJasaPelayananDokterDetail.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapJPDDetail_LabReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function ShowPesanWarningLapJPDDetail_LabReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
