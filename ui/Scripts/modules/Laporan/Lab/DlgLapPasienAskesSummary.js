
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapPasienAskesSummary;
var selectNamaLapPasienAskesSummary;
var now = new Date();
var selectSetPerseorangan;
var frmLapPasienAskesSummary;
var varLapPasienAskesSummary= ShowFormLapLapPasienAskesSummary();
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var winLapPasienAskesSummaryReport;
var cboPilihanLapPasienAskesSummary;

var DataGridCaraBayar;
var DataGridMasterCaraBayar;
var Storedata1_CaraBayar;
var Storedata2_MasterCaraBayar;





function ShowFormLapLapPasienAskesSummary()
{
    frmLapPasienAskesSummary= fnLapPasienAskesSummary();
    frmLapPasienAskesSummary.show();
	loadDataComboAsalPasienLapPasienAskesSummary();
};


function fnLapPasienAskesSummary()
{
    winLapPasienAskesSummaryReport = new Ext.Window
    (
        {
            id: 'winLapPasienAskesSummaryReport',
            title: 'Laporan Pasien ASKES Detail',
            closeAction: 'destroy',
            width: 420,
            height: 290,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapPasienAskesSummary()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'Ok',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapPasienAskesSummary',
						handler: function()
						{
							if (ValidasiReportLapPasienAskesSummary() === 1)
							{								
								if(Ext.getCmp('radioasal').getValue() === true){
									periode='tanggal';
									tglAwal=Ext.getCmp('dtpTglAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
								} else{
									periode='bulan';
									tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilLab').getValue();
								}
								
								if (Ext.getCmp('Shift_All_LapPasienAskesSummary').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1_LapPasienAskesSummary').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2_LapPasienAskesSummary').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3_LapPasienAskesSummary').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
								
								var params={
									kd_asal:Ext.getCmp('cboPilihanLapPasienAskesSummary').getValue(),
									asal_pasien:Ext.get('cboPilihanLapPasienAskesSummary').getValue(),
									periode:periode,
									tglAwal:tglAwal,
									tglAkhir:tglAkhir,
									shift:shift,
									shift1:shift1,
									shift2:shift2,
									shift3:shift3,
								} 
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/lab/lap_laboratorium/cetakPasienAskesSummary");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapPasienAskesSummaryReport.close(); 
								
							};
							
							
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapPasienAskesSummary',
						handler: function()
						{
							winLapPasienAskesSummaryReport.close();
						}
					}
			]

        }
    );

    return winLapPasienAskesSummaryReport;
};


function ItemLapPasienAskesSummary()
{
    var PnlLapLapPasienAskesSummary = new Ext.Panel
    (
        {
            id: 'PnlLapLapPasienAskesSummary',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapPasienAskesSummary_Atas(),
                getItemLapLapPasienAskesSummary_Batas(),
                getItemLapLapPasienAskesSummary_Tengah(),
                getItemLapLapPasienAskesSummary_Batas(),
                getItemLapLapPasienAskesSummary_Bawah(),
              
            ]
        }
    );

    return PnlLapLapPasienAskesSummary;
};


function getKodeReportLapPasienAskesSummary()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLapPasienAskesSummary').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLapPasienAskesSummary').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLapPasienAskesSummary()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapPasienAskesSummary').getValue() === ''){
		ShowPesanWarningLapPasienAskesSummaryReport('Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapPasienAskesSummaryReport('Periode belum dipilih','Warning');
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapPasienAskesSummary').getValue() === false && Ext.getCmp('Shift_1_LapPasienAskesSummary').getValue() === false && Ext.getCmp('Shift_2_LapPasienAskesSummary').getValue() === false && Ext.getCmp('Shift_3_LapPasienAskesSummary').getValue() === false){
		ShowPesanWarningLapPasienAskesSummaryReport('Shift Belum Dipilih','Warning');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapPasienAskesSummaryReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapPasienAskesSummary_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  395,
            height: 45,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapPasienAskesSummary(),
            ]
        }]
    };
    return items;
};


function getItemLapLapPasienAskesSummary_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapPasienAskesSummary_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  395,
				height: 40,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
				
					{
						x: 60,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapPasienAskesSummary',
						id : 'Shift_All_LapPasienAskesSummary',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapPasienAskesSummary').setValue(true);
								Ext.getCmp('Shift_2_LapPasienAskesSummary').setValue(true);
								Ext.getCmp('Shift_3_LapPasienAskesSummary').setValue(true);
								Ext.getCmp('Shift_1_LapPasienAskesSummary').disable();
								Ext.getCmp('Shift_2_LapPasienAskesSummary').disable();
								Ext.getCmp('Shift_3_LapPasienAskesSummary').disable();
							}else{
								Ext.getCmp('Shift_1_LapPasienAskesSummary').setValue(false);
								Ext.getCmp('Shift_2_LapPasienAskesSummary').setValue(false);
								Ext.getCmp('Shift_3_LapPasienAskesSummary').setValue(false);
								Ext.getCmp('Shift_1_LapPasienAskesSummary').enable();
								Ext.getCmp('Shift_2_LapPasienAskesSummary').enable();
								Ext.getCmp('Shift_3_LapPasienAskesSummary').enable();
							}
						}
					},
					{
						x: 130,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapPasienAskesSummary',
						id : 'Shift_1_LapPasienAskesSummary'
					},
					{
						x: 200,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapPasienAskesSummary',
						id : 'Shift_2_LapPasienAskesSummary'
					},
					{
						x: 270,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapPasienAskesSummary',
						id : 'Shift_3_LapPasienAskesSummary'
					}
				]
			}
        ]
            
    };
    return items;
};

function getItemLapLapPasienAskesSummary_Tengah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  395,
				height: 120,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'radio',
						id:'radioasal',
						handler: function (field, value) 
						{
							if (value === true)
							{
								Ext.getCmp('radioasalBulan').setValue(false);
								Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
								Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
								Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
								Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
							}else
							{
								Ext.getCmp('radioasal').setValue(false);
								Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
								Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
								Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
								Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
							}
						}
					}, {
						x: 30,
						y: 10,
						xtype: 'label',
						text: ' Transaksi Pada Tanggal'
					}, 
					{
						x: 30,
						y: 30,
						xtype: 'datefield',
						id: 'dtpTglAwalFilterHasilLab',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 140,
						y: 30,
						xtype: 'label',
						text: ' s/d Tanggal'
					}, 
					{
						x: 205,
						y: 30,
						xtype: 'datefield',
						id: 'dtpTglAkhirFilterHasilLab',
						format: 'd/M/Y',
						value: now,
						width: 100
					},
					{
						x: 10,
						y: 65,
						xtype: 'radio',
						id:'radioasalBulan',
						handler: function (field, value) 
						{if (value === true)
							{
								Ext.getCmp('radioasal').setValue(false);
								Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
								Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
								Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
								Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
							}else
							{
								Ext.getCmp('radioasalBulan').setValue(false);
								Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
								Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
								Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
								Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
							}
						}
					}, 
					{
						x: 30,
						y: 65,
						xtype: 'label',
						text: ' Transaksi Pada Bulan'
					}, 
					{
						x: 30,
						y: 85,
						xtype: 'datefield',
						id: 'dtpBulanAwalFilterHasilLab',
						format: 'M/Y',
						value: now
					}, 
					{
						x: 140,
						y: 85,
						xtype: 'label',
						text: ' s/d Bulan'
					}, 
					{
						x: 205,
						y: 85,
						xtype: 'datefield',
						id: 'dtpBulanAkhirFilterHasilLab',
						format: 'M/Y',
						value: now,
						width: 100
					},
					
				]
			},
		]
    };
    return items;
};

var selectSetPilihan;

function loadDataComboAsalPasienLapPasienAskesSummary(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/lab/lap_laboratorium/getAsalPasien",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboPilihanLapPasienAskesSummary.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_AsalPasien_LapPasienAskesSummary.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_AsalPasien_LapPasienAskesSummary.add(recs);
				console.log(o);
			}
		}
	});
}


function mComboPilihanLapPasienAskesSummary()
{
	var Field = ['kd_asal','ket'];
    ds_AsalPasien_LapPasienAskesSummary = new WebApp.DataStore({fields: Field});
    cboPilihanLapPasienAskesSummary = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapPasienAskesSummary',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: ds_AsalPasien_LapPasienAskesSummary,
                valueField: 'kd_asal',
                displayField: 'ket',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapPasienAskesSummary;
};
