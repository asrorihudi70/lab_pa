
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgDlgLapPasienPerusahaanSummary;
var varDlgLapPasienPerusahaanSummary= ShowFormDlgLapPasienPerusahaanSummary();
var asc=0;
var IdRootKelompokBarang_pDlgLapPasienPerusahaanSummary='1000000000';
var KdPasienPerusahaan_DlgLapPasienPerusahaanSummary;
var NamaSub_DlgLapPasienPerusahaanSummary;
var gridPanelLookUpBarang_DlgLapPasienPerusahaanSummary;
var sendDataArray = [];
var dsvComboPerusahaanDlgLapPasienPerusahaanSummary;
var dsvComboSubKelompokPasienDlgLapPasienPerusahaanSummary;
var sendDataArray = [];
var namaCustomer;
var periode='tanggal';
var optionalPerusahaan=1;
function ShowFormDlgLapPasienPerusahaanSummary()
{
    frmDlgDlgLapPasienPerusahaanSummary= fnDlgDlgLapPasienPerusahaanSummary();
    frmDlgDlgLapPasienPerusahaanSummary.show();
	//GetStrpasienSetBarang_DlgLapPasienPerusahaanSummary();
};

function fnDlgDlgLapPasienPerusahaanSummary()
{
    var winDlgLapPasienPerusahaanSummaryReport = new Ext.Window
    (
        {
            id: 'winDlgLapPasienPerusahaanSummaryReport',
            title: 'Laporan Jasa Pelayanan Perusahaan Summary',
            closeAction: 'destroy',
            width: 370,
            height: 260,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgDlgLapPasienPerusahaanSummary()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkDlgLapPasienPerusahaanSummary',
					handler: function()
					{
						
						if (ValidasiReportPasienPerusahaanSummaryLab() === 1)
                        {
							var params=GetCriteriaPasienPerusahaanSummaryLab();
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/lab/lap_pasienperusahaansummary/cetaklaporan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();
						}
						
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelDlgLapPasienPerusahaanSummary',
					handler: function()
					{
						frmDlgDlgLapPasienPerusahaanSummary.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winDlgLapPasienPerusahaanSummaryReport;
};
function GetCriteriaPasienPerusahaanSummaryLab()
{
	
	var getTglAwal = Ext.getCmp('dtpTglAwalPasienPerusahaanLapSummary').getValue();
	var getTglAkhir = Ext.getCmp('dtpTglAkhirPasienPerusahaanLapSummary').getValue();
	var besokTglAwal=getTglAwal.add(Date.DAY, +1);
	var besokTglAkhir=getTglAkhir.add(Date.DAY, +1);
	var getThnBlnAwal =Ext.getCmp('dtpBlnAwalPasienPerusahaanLapSummary').getValue();
	var getThnBlnAkhir=Ext.getCmp('dtpBlnAkhirPasienPerusahaanLapSummary').getValue();
	var getTahunAwal = getThnBlnAwal.format("Y");
	var getTahunAkhir = getThnBlnAkhir.format("Y");
	var getBulanAwal = getThnBlnAwal.format("m");
	var getBulanAkhir = getThnBlnAkhir.format("m");
	var asalPasien;
	var kodeShift="";
	var namaShift="";
	var cekBoxPilihan;
	var kodeCustomer=Ext.getCmp('cbo_CmbPerusahaanLapPasienPerusahaanSummaryLab').getValue();
	
	//if (kodeCustomer!=='0000000001')
	if (Ext.getCmp('rbKriteriaSemuaPerusahaanPasienPerusahaanSummary').getValue() === true){
		cekBoxPilihan=1;
	}
	else
	{
		cekBoxPilihan=2;
	}
	if (Ext.getCmp('CekSemuaShiftLapPasienPerusahaanSummary').getValue() === true)
	{
		namaShift = "Semua Shift";
		kodeShift="1,2,3";
	}else{
		namaShift= "Shift ";
		if (Ext.getCmp('CekShift1LapPasienPerusahaanSummary').getValue() === true)
		{
			namaShift += "1";
			kodeShift += "1";
		}
		if (Ext.getCmp('CekShift2LapPasienPerusahaanSummary').getValue() === true)
		{
			if (kodeShift!=='')
			{
				kodeShift += ",";
			}
			if (namaShift!=='')
			{
				namaShift += ",2";
			}
			else
			{
				namaShift += "2";
			}
			kodeShift += "2";
		}
		if (Ext.getCmp('CekShift3LapPasienPerusahaanSummary').getValue() === true)
		{
			if (kodeShift!=='')
			{
				kodeShift += ",";
			}
			if (namaShift!=='')
			{
				namaShift += ",3";
			}
			else
			{
				namaShift += "3";
			}
			kodeShift += "3";
		}
		if (namaShift==="Shift 1,2,3")
			{
				namaShift="Semua Shift";
			}
	}
	
	console.log(Ext.getCmp('cbo_CmbPerusahaanLapPasienPerusahaanSummaryLab').getValue())
	console.log(namaCustomer);
	var	params =
	{
		tanggal:now,
		tglAwal:Ext.getCmp('dtpTglAwalPasienPerusahaanLapSummary').getValue(),
		tglAkhir:Ext.getCmp('dtpTglAkhirPasienPerusahaanLapSummary').getValue(),
		tglAwalBesok:besokTglAwal,
		tglAkhirBesok:besokTglAkhir,
		thnAwal:getTahunAwal,
		thnAkhir:getTahunAkhir,
		blnAwal:getBulanAwal,
		blnAkhir:getBulanAkhir,
		KdCust:Ext.getCmp('cbo_CmbPerusahaanLapPasienPerusahaanSummaryLab').getValue(),
		namaCust:namaCustomer,
		kdShift:kodeShift,
		nmShift:namaShift,
		periodeSummary:periode,
		optPerusahaan:optionalPerusahaan
	}
   
    return params;
};
function ValidasiReportPasienPerusahaanSummaryLab()
{
	var x=1;
	if(Ext.getCmp('CekSemuaShiftLapPasienPerusahaanSummary').getValue() === false && Ext.getCmp('CekShift1LapPasienPerusahaanSummary').getValue() === false && Ext.getCmp('CekShift2LapPasienPerusahaanSummary').getValue() === false && Ext.getCmp('CekShift3LapPasienPerusahaanSummary').getValue() === false){
		ShowPesanWarningLapPasienPerusahaanSummary_LabReport('Shift Belum Dipilih','Laporan Pasien Perusahaan Summary');
        x=0;
	}

    return x;
}

function ItemDlgDlgLapPasienPerusahaanSummary()
{
    var PnlDlgLapPasienPerusahaanSummary = new Ext.Panel
    (
        {
            id: 'PnlDlgLapPasienPerusahaanSummary',
            fileUpload: true,
            layout: 'form',
            height: 370,
            anchor: '100%',
            bodyStyle: 'padding:10px',
            border: true,
            items:
            [
				getItemDlgLapPasienPerusahaanSummary_pasien(),
				getItemDlgLapPasienPerusahaanSummary_Batas(),
				getItemDlgLapPasienPerusahaanSummary_Tanggal(),
				//getItemDlgLapPasienPerusahaanSummary_Batas(),
				//getItemDlgLapPasienPerusahaanSummary_Shift(),
            ]
        }
    );

    return PnlDlgLapPasienPerusahaanSummary;
};


function getItemDlgLapPasienPerusahaanSummary_pasien()
{
	radioKriteria = new Ext.form.RadioGroup({
	x: 10,
	y: 5,
	id :'radiosKriteriaPasienPerusahaanSummary',
	columns  : 2,
	//rows : 2,
	items: 
	[
		{
			
			id:'rbKriteriaSemuaPerusahaanPasienPerusahaanSummary',
			name: 'rbLapPasienPerusahaanPanelPerusahaan',
			boxLabel: 'Semua Perusahaan',
			checked: true, 
			//inputValue: "1"
		},
		{
			id:'rbKriteriaPerPerusahaanPasienPerusahaanSummary',
			name: 'rbLapPasienPerusahaanPanelPerusahaan',
			boxLabel: 'Per Perusahaan',
			//inputValue: "1"
		},
	],
	listeners: {
		change : function()
		{ 
			
			if (Ext.getCmp('rbKriteriaSemuaPerusahaanPasienPerusahaanSummary').getValue()===true)
			{
				Ext.getCmp('cbo_CmbPerusahaanLapPasienPerusahaanSummaryLab').setValue('');
				Ext.getCmp('cbo_CmbPerusahaanLapPasienPerusahaanSummaryLab').disable();
				optionalPerusahaan=1;
			}
			else
			{
				Ext.getCmp('cbo_CmbPerusahaanLapPasienPerusahaanSummaryLab').enable();
				optionalPerusahaan=0;
			}
		}
	}
		
   });
    var items = {
        layout: 'column',
        border: false,
        items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  335,
				height: 75,
				anchor: '100% 100%',
				items: [
				radioKriteria,
					
					{
						x: 10,
						y: 35,
						xtype: 'label',
						text: 'Perusahaan'
					}, 
					{
						x: 100,
						y: 35,
						xtype: 'label',
						text: ' : '
					},
					comboCmbPerusahaanListView_DlgLapPasienPerusahaanSummary()
				]
			}
		]
    };
    return items;
};



function GetStrpasienSetBarang_DlgLapPasienPerusahaanSummary()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and left(kd_PasienPerusahaan,1)<>'8'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrpasienSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function dsComboPerusahaanDlgLapPasienPerusahaanSummary()
{
	dsvComboPerusahaanDlgLapPasienPerusahaanSummary.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboPerusahaanDlgLapPasienPerusahaanSummary',
                    //param : 'kd_tarif=1'
                }			
            }
        );   
    return dsvComboPerusahaanDlgLapPasienPerusahaanSummary;
}
function comboCmbPerusahaanListView_DlgLapPasienPerusahaanSummary()
{	
	var Field =['kd_customer','customer'];
    dsvComboPerusahaanDlgLapPasienPerusahaanSummary = new WebApp.DataStore({fields: Field});
	dsComboPerusahaanDlgLapPasienPerusahaanSummary();
    var cbo_LapPasienPerusahaanSummaryLabPerusahaan = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 35,
            flex: 1,
			id: 'cbo_CmbPerusahaanLapPasienPerusahaanSummaryLab',
			valueField: 'kd_customer',
            displayField: 'customer',
			store: dsvComboPerusahaanDlgLapPasienPerusahaanSummary,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:205,
			disabled:true,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					namaCustomer=b.data.customer;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapPasienPerusahaanSummaryLabPerusahaan;
};

function getItemDlgLapPasienPerusahaanSummary_Tanggal()
{
	radioTanggal = new Ext.form.RadioGroup({
	x: 10,
	y: 5,
	id :'radiosTanggalPasienPerusahaanSummary',
	columns  : 1,
	rows : 2,
	items: 
	[
		{
			
			id:'rbTanggalPasienPerusahaanSummary',
			name: 'rbLapPasienPerusahaan',
			boxLabel: 'Tanggal',
			checked: true, 
			//inputValue: "1"
		},
		{
			x: 10,
			y: 55,
			id:'rbBulanPasienPerusahaanSummary',
			name: 'rbLapPasienPerusahaan',
			boxLabel: 'Bulan',
			//inputValue: "1"
		},
	],
	listeners: {
		change : function()
		{ 
			if (Ext.getCmp('rbBulanPasienPerusahaanSummary').checked===true)
			{
				Ext.getCmp('dtpTglAwalPasienPerusahaanLapSummary').disable(true)
				Ext.getCmp('dtpTglAkhirPasienPerusahaanLapSummary').disable(true)
				Ext.getCmp('dtpBlnAwalPasienPerusahaanLapSummary').enable(true)
				Ext.getCmp('dtpBlnAkhirPasienPerusahaanLapSummary').enable(true)
				periode='bulan';
			}
			else
			{
				Ext.getCmp('dtpTglAwalPasienPerusahaanLapSummary').enable(true)
				Ext.getCmp('dtpTglAkhirPasienPerusahaanLapSummary').enable(true)
				Ext.getCmp('dtpBlnAwalPasienPerusahaanLapSummary').disable(true)
				Ext.getCmp('dtpBlnAkhirPasienPerusahaanLapSummary').disable(true)
				periode='tanggal';
			}
		}
	}
		
   });
   
   
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  335,
				height: 100,
				anchor: '100% 100%',
				items: [
					radioTanggal,
					  {
							x: 80,
							y: 5,
							xtype: 'datefield',
							id: 'dtpTglAwalPasienPerusahaanLapSummary',
							format: 'd/M/Y',
							value: now
						}
					,
					{
						x: 190,
						y: 5,
						xtype: 'label',
						text: ' s / d '
					},
					{
						x: 220,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAkhirPasienPerusahaanLapSummary',
						format: 'd/M/Y',
						value: now
					}, 
					//radioBulan,
					{
							x: 80,
							y: 30,
							xtype: 'datefield',
							id: 'dtpBlnAwalPasienPerusahaanLapSummary',
							disabled: true,
							format: 'M/Y',
							value: now
						}
					,
					{
						x: 190,
						y: 30,
						xtype: 'label',
						text: ' s / d '
					},
					{
						x: 220,
						y: 30,
						xtype: 'datefield',
						id: 'dtpBlnAkhirPasienPerusahaanLapSummary',
						disabled: true,
						format: 'M/Y',
						value: now
					}, 
					{
					   xtype: 'checkbox',
					   id: 'CekSemuaShiftLapPasienPerusahaanSummary',
					   hideLabel:false,
					   boxLabel: 'Semua',
					   checked: true,
					   x: 10,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
								if(Ext.getCmp('CekSemuaShiftLapPasienPerusahaanSummary').getValue()===true)
								{
									Ext.getCmp('CekShift1LapPasienPerusahaanSummary').setValue(true);
									Ext.getCmp('CekShift1LapPasienPerusahaanSummary').disable(true);
									
									Ext.getCmp('CekShift2LapPasienPerusahaanSummary').setValue(true);
									Ext.getCmp('CekShift2LapPasienPerusahaanSummary').disable(true);
									
									Ext.getCmp('CekShift3LapPasienPerusahaanSummary').setValue(true)
									Ext.getCmp('CekShift3LapPasienPerusahaanSummary').disable(true);
								}
								else
								{
									Ext.getCmp('CekShift1LapPasienPerusahaanSummary').setValue(false);
									Ext.getCmp('CekShift1LapPasienPerusahaanSummary').enable(true);
									
									Ext.getCmp('CekShift2LapPasienPerusahaanSummary').setValue(false)
									Ext.getCmp('CekShift2LapPasienPerusahaanSummary').enable(true)
									
									Ext.getCmp('CekShift3LapPasienPerusahaanSummary').setValue(false)
									Ext.getCmp('CekShift3LapPasienPerusahaanSummary').enable(true)
								}	
							}
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift1LapPasienPerusahaanSummary',
					   hideLabel:false,
					   boxLabel: 'Shift 1',
					   checked: true,
					   disabled: true,
					   x: 65,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
								if(Ext.getCmp('CekShift1LapPasienPerusahaanSummary').getValue()===true)
								{
									sendDataArray.push("1");
									console.log(sendDataArray);
								}
								else
								{
									
								}
							}
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift2LapPasienPerusahaanSummary',
					   hideLabel:false,
					   boxLabel: 'Shift 2',
					   checked: true,
					   disabled: true,
					   x: 125,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift3LapPasienPerusahaanSummary',
					   hideLabel:false,
					   boxLabel: 'Shift 3',
					   checked: true,
					   disabled: true,
					   x: 180,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
				]
			}
		]
    };
    return items;
};

/*function getItemDlgLapPasienPerusahaanSummary_Shift()
{
	var items = {
        layout: 'column',
        border: false,
        items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				anchor: '100% 100%',
				items: [
				{
				   xtype: 'checkbox',
				   id: 'CekSemuaShiftLapPasienPerusahaanSummary',
				   hideLabel:false,
				   //checked: false,
				   x: 10,
				   y: 5,
				   listeners: 
				   {
						check: function()
						{
								
						 }
				   }
				}
			]
		}]
	}
}*/
function getItemDlgLapPasienPerusahaanSummary_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};



function gridDataViewBarang_DlgLapPasienPerusahaanSummary()
{
    var FieldGrdBArang_DlgLapPasienPerusahaanSummary = [];
	
    dsDataGrdBarang_DlgLapPasienPerusahaanSummary= new WebApp.DataStore
	({
        fields: FieldGrdBArang_DlgLapPasienPerusahaanSummary
    });
    
    gridPanelLookUpBarang_DlgLapPasienPerusahaanSummary =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_DlgLapPasienPerusahaanSummary,
		height:120,
		width:448,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_DlgLapPasienPerusahaanSummary.getAt(row).data.kd_PasienPerusahaan);
					KdPasienPerusahaan_DlgLapPasienPerusahaanSummary = dsDataGrdBarang_DlgLapPasienPerusahaanSummary.getAt(row).data.kd_PasienPerusahaan;
					NamaSub_DlgLapPasienPerusahaanSummary = dsDataGrdBarang_DlgLapPasienPerusahaanSummary.getAt(row).data.nama_sub;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'PasienPerusahaan_kd_PasienPerusahaan',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_PasienPerusahaan',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viPasienPerusahaanPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_DlgLapPasienPerusahaanSummary;
}

function dataGridLookUpBarangDlgLapPasienPerusahaanSummary(PasienPerusahaan_kd_PasienPerusahaan){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/PasienPerusahaan/lap_PasienPerusahaanSummary/getGridLookUpBarang",
			params: {PasienPerusahaan_kd_PasienPerusahaan:PasienPerusahaan_kd_PasienPerusahaan},
			failure: function(o)
			{
				ShowPesanWarningLapPasienPerusahaanSummary_LabReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_DlgLapPasienPerusahaanSummary.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_DlgLapPasienPerusahaanSummary.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_DlgLapPasienPerusahaanSummary.add(recs);
					
					
					
					gridPanelLookUpBarang_DlgLapPasienPerusahaanSummary.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapPasienPerusahaanSummary_LabReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function ShowPesanWarningLapPasienPerusahaanSummary_LabReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
