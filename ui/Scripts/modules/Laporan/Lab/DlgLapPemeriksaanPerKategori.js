
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapTRPerkomponenDetail;
var selectNamaLapTRPerkomponenDetail;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapTRPerkomponenDetail;
var varLapLapTRPerkomponenDetail= ShowFormLapLapTRPerkomponenDetail();
var selectSetUmum;
var customer;
var kdcustomer;
var tglAwal;
var tglAkhir;
var tipe;
var winLapTRPerkomponenDetailReport;

function ShowFormLapLapTRPerkomponenDetail()
{
    frmDlgLapTRPerkomponenDetail= fnDlgLapTRPerkomponenDetail();
    frmDlgLapTRPerkomponenDetail.show();
};

function fnDlgLapTRPerkomponenDetail()
{
    winLapTRPerkomponenDetailReport = new Ext.Window
    (
        {
            id: 'winLapTRPerkomponenDetailReport',
            title: 'Laporan Pemeriksaan Per Kategori',
            closeAction: 'destroy',
            width: 370,
            height: 210,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapTRPerkomponenDetail()],
            listeners:
			{
				activate: function()
				{
					Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').hide();
					Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').hide();
					Ext.getCmp('cboUmumLapTRPerkomponenDetail').show();
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'Ok',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapTRPerkomponenDetail',
						handler: function()
						{
							if (ValidasiReportLapTRPerkomponenDetail() === 1)
							{
								if (Ext.get('cboPilihanLapTRPerkomponenDetailkelompokPasien').getValue() === 'Semua')
								{
									tipe='Semua';
									customer='Semua';
									kdcustomer='Semua';
								} else if (Ext.get('cboPilihanLapTRPerkomponenDetailkelompokPasien').getValue() === 'Perseorangan'){
									customer=Ext.get('cboPerseoranganLapTRPerkomponenDetail').getValue();
									tipe='Perseorangan';
									kdcustomer=Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').getValue();
								} else if (Ext.get('cboPilihanLapTRPerkomponenDetailkelompokPasien').getValue() === 'Perusahaan'){
									customer=Ext.get('cboPerusahaanRequestEntryLapTRPerkomponenDetail').getValue();
									tipe='Perusahaan';
									kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').getValue();
								} else {
									customer=Ext.get('cboAsuransiLapTRPerkomponenDetail').getValue();
									tipe='Asuransi';
									kdcustomer=Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').getValue();
								} 
								
								var params={
									tipe:tipe,
									customer:customer,
									kdcustomer:kdcustomer,
									tglAwal:Ext.getCmp('dtpTglAwalFilterHasilLab').getValue(),
									tglAkhir:Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue(),
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/lab/lap_laboratorium/cetakPemeriksaanPerKategori");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapTRPerkomponenDetailReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapTRPerkomponenDetail',
						handler: function()
						{
							winLapTRPerkomponenDetailReport.close();
						}
					}
			]

        }
    );

    return winLapTRPerkomponenDetailReport;
};


function ItemDlgLapTRPerkomponenDetail()
{
    var PnlLapLapTRPerkomponenDetail = new Ext.Panel
    (
        {
            id: 'PnlLapLapTRPerkomponenDetail',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapTRPerkomponenDetail_Atas()
              
            ]
        }
    );

    return PnlLapLapTRPerkomponenDetail;
};



function ValidasiReportLapTRPerkomponenDetail()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapTRPerkomponenDetailkelompokPasien').getValue() === ''){
		ShowPesanWarningLapTRPerkomponenDetailReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapTRPerkomponenDetail').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapTRPerkomponenDetail').getValue() === '' &&  Ext.get('cboAsuransiLapTRPerkomponenDetail').getValue() === '' && Ext.get('cboUmumRegisLab').getValue() === '' ){
		ShowPesanWarningLapTRPerkomponenDetailReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapTRPerkomponenDetailReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapTRPerkomponenDetail_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
			{
                x: 10,
                y: 10,
                xtype: 'label',
                text: ' Periode'
            }, 
			{
                x: 60,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilLab',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 170,
                y: 30,
                xtype: 'label',
                text: ' s/d '
            }, 
			{
                x: 195,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTRPerkomponenDetailKelompokPasien(),
                mComboPerseoranganLapTRPerkomponenDetail(),
                mComboAsuransiLapTRPerkomponenDetail(),
                mComboPerusahaanLapTRPerkomponenDetail(),
                mComboUmumLapTRPerkomponenDetail()
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function mComboPilihanLapTRPerkomponenDetailKelompokPasien()
{
    var cboPilihanLapTRPerkomponenDetailkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanLapTRPerkomponenDetailkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapTRPerkomponenDetailkelompokPasien;
};

function mComboPerseoranganLapTRPerkomponenDetail()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapTRPerkomponenDetail = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapTRPerkomponenDetail.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapTRPerkomponenDetail = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganLapTRPerkomponenDetail',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapTRPerkomponenDetail,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLapTRPerkomponenDetail;
};

function mComboUmumLapTRPerkomponenDetail()
{
    var cboUmumLapTRPerkomponenDetail = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumLapTRPerkomponenDetail',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumLapTRPerkomponenDetail;
};

function mComboPerusahaanLapTRPerkomponenDetail()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapTRPerkomponenDetail = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryLapTRPerkomponenDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapTRPerkomponenDetail;
};

function mComboAsuransiLapTRPerkomponenDetail()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomerLab',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapTRPerkomponenDetail = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiLapTRPerkomponenDetail',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapTRPerkomponenDetail;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').show();
        Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenDetail').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').show();
        Ext.getCmp('cboUmumLapTRPerkomponenDetail').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenDetail').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenDetail').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenDetail').show();
   }
}
