var DlgLapPenerimaanJenisPenerimaan={
	ArrayStore:{
		unit1:Q().arraystore(),
		unit2:Q().arraystore()
	},
	CheckBox:{
		shift1:null,
		shift2:null,
		shift3:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	DropDown:{
		kelompok1:null,
		kelompok2:null,
		kelompok3:null
	},
	Grid:{
		unit1:null,
		unit2:null
	},
	Window:{
		main:null
	},
	doPrint:function(){
		var $this=this;
		loadMask.show();
		var params=[];
		params.push({name:'jenis_cust',value:$this.DropDown.kelompok2.getValue()});
		params.push({name:'kd_customer',value:$this.DropDown.kelompok3.getValue()});
		params.push({name:'start_date',value:Q($this.DateField.startDate).val()});
		params.push({name:'last_date',value:Q($this.DateField.endDate).val()});
		params.push({name:'shift1',value:$this.CheckBox.shift1.getValue()});
		params.push({name:'shift2',value:$this.CheckBox.shift2.getValue()});
		params.push({name:'shift3',value:$this.CheckBox.shift3.getValue()});
		for(var i=0; i<$this.ArrayStore.unit2.getRange().length ; i++){
			params.push({name:'kd_pay[]',value:$this.ArrayStore.unit2.getRange()[i].data.id});
		}
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			data:params,
			url:baseURL + "index.php/lab/lap_penerimaanjenispenerimaan/doPrint",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.close();
					window.open(r.data, '_blank', 'location=0,resizable=1', false);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	getSelect:function(kd){
		var $this=this;
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/lab/lap_penerimaanjenispenerimaan/getSelect",
			data:{cust:kd},
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.DropDown.kelompok3).reset();
					Q($this.DropDown.kelompok3).add({id:'',text:'Semua'});
					Q($this.DropDown.kelompok3).add(r.data);
					Q($this.DropDown.kelompok3).val('');
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	init:function(){
		var $this=this;
		$this.Window.main=Q().window({
			title:'Laporan Penerimaan Per Jenis Penerimaan',
			fbar:[
				new Ext.Button({
					text:'Ok',
					handler:function(){
						$this.doPrint();
					}
				}),
				new Ext.Button({
					text:'Close',
					handler:function(){
						$this.Window.main.close();
					}
				})
			],
			items:[
				Q().panel({
					items:[
						Q().fieldset({
							title:'Periode',
							width: 380,
							items:[
//								Q().input({
//									label:'Pengelompokan Laporan',
//									items:[
//										$this.DropDown.kelompok1=Q().dropdown({
//											width: 200
//										})
//									]
//								}),
								Q().input({
									label:'Periode',
									items:[
										$this.DateField.startDate=Q().datefield(),
										Q().display({value:'s/d'}),
										$this.DateField.endDate=Q().datefield()
									]
								}),
								Q().input({
									label:'Kelompok Pasien',
									items:[
										$this.DropDown.kelompok2=Q().dropdown({
											width: 200,
											data:[
												{id:'',text:'Semua'},
												{id:1,text:'Perorangan'},
												{id:2,text:'Perusahaan'},
												{id:3,text:'Ansuransi'}
											],
											select:function(a){
												if(a.getValue()!=''){
													$this.getSelect(a.getValue()-1);
												}else{
													Q($this.DropDown.kelompok3).reset();
													Q($this.DropDown.kelompok3).add({id:'',text:'Semua'});
												}
											}
										})
									]
								}),
								Q().input({
									separator:'',
									items:[
										$this.DropDown.kelompok3=Q().dropdown({
											width: 200,
											data:[
												{id:'',text:'Semua'}
											]
										})
									]
								}),
								Q().input({
									xWidth:100,
									separator:'',
									items:[
										Q().display({value:'Shift 1'}),
										$this.CheckBox.shift1=Q().checkbox({checked:true}),
										Q().display({value:'Shift 2'}),
										$this.CheckBox.shift2=Q().checkbox({checked:true}),
										Q().display({value:'Shift 3'}),
										$this.CheckBox.shift3=Q().checkbox({checked:true})
									]
								}),
								Q().input({
									label:'Cara Pembayaran',
									separator:':'
								}),
								{
									layout:'hbox',
									border: false,
									items:[
										$this.Grid.unit1=new Ext.grid.GridPanel({
								            ddGroup          : 'secondGridDDGroup',
								            store            : $this.ArrayStore.unit1,
								            autoScroll       : true,
								            columnLines      : true,
								            border           : true,
								            enableDragDrop   : true,
								            flex			: 1,
								            height           : 200,
								            stripeRows       : true,
								            trackMouseOver   : true,
								            colModel         : new Ext.grid.ColumnModel([
		                                        {
		                                                dataIndex: 'id',
		                                                sortable: true,
		                                                hidden : true
		                                        },{
		                                                header: 'Nama',
		                                                dataIndex: 'text',
		                                                sortable: true,
		                                                width: 50
		                                        }
		                                    ]),
		                                    listeners : {
							                    afterrender : function(comp) {
								                    var secondGridDropTargetEl = $this.Grid.unit1.getView().scroller.dom;
								                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							                            ddGroup    : 'firstGridDDGroup',
							                            notifyDrop : function(ddSource, e, data){
						                                    var records =  ddSource.dragData.selections;
						                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
						                                    $this.Grid.unit1.store.add(records);
						                                    $this.Grid.unit1.store.sort('text', 'ASC');
						                                    return true
							                            }
								                    });
							                    }
							                },
							                viewConfig:{
					                            forceFit: true
						                    }
								        }),
								        
								        $this.Grid.unit2=new Ext.grid.GridPanel({
								            ddGroup          : 'firstGridDDGroup',
								            store            : $this.ArrayStore.unit2,
								            autoScroll       : true,
								            columnLines      : true,
								            border           : true,
								            enableDragDrop   : true,
								            style:'margin-left:-1px;',
								            flex			: 1,
								            height           : 200,
								            stripeRows       : true,
								            trackMouseOver   : true,
								            colModel         : new Ext.grid.ColumnModel([
		                                        {
		                                                dataIndex: 'id',
		                                                hidden : true
		                                        },{
		                                                header: 'Nama',
		                                                dataIndex: 'text',
		                                                width: 50
		                                        }
		                                    ]),
		                                    listeners : {
							                    afterrender : function(comp) {
								                    var secondGridDropTargetEl = $this.Grid.unit2.getView().scroller.dom;
								                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							                            ddGroup    : 'secondGridDDGroup',
							                            notifyDrop : function(ddSource, e, data){
						                                    var records =  ddSource.dragData.selections;
						                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
						                                    $this.Grid.unit2.store.add(records);
						                                    $this.Grid.unit2.store.sort('text', 'ASC');
						                                    return true
							                            }
								                    });
							                    }
							                },
							                viewConfig:{
					                            forceFit: true
						                    }
								        })
									]
								}
							]
						})
					]
				})
			]
		});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/lab/lap_penerimaanjenispenerimaan/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					$this.Window.main.show();
					Q($this.ArrayStore.unit1).add(r.data.cust);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}
}
DlgLapPenerimaanJenisPenerimaan.init();