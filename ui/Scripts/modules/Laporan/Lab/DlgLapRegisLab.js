
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLabRegis;
var selectNamaLabRegis;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLabRegis;
var varLapLabRegis= ShowFormLapLabRegis();
var selectSetUmum;
var selectSetkelpas;

function ShowFormLapLabRegis()
{
    frmDlgLabRegis= fnDlgLabRegis();
    frmDlgLabRegis.show();
};

function fnDlgLabRegis()
{
    var winLabRegisReport = new Ext.Window
    (
        {
            id: 'winLabRegisReport',
            title: 'Laporan Regis Pasien Laboratorium',
            closeAction: 'destroy',
            width:400,
            height: 400,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLabRegis()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganRegisLab').hide();
                Ext.getCmp('cboAsuransiRegisLab').hide();
                Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
                Ext.getCmp('cboUmumRegisLab').show();
            }
        }

        }
    );

    return winLabRegisReport;
};


function ItemDlgLabRegis()
{
    var PnlLapLabRegis = new Ext.Panel
    (
        {
            id: 'PnlLapLabRegis',
            fileUpload: true,
            layout: 'form',
            height: '550',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLabRegis_Atas(),
                getItemLapLabRegis_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapLabRegis',
                            handler: function()
                            {
                               if (ValidasiReportLabRegis() === 1)
                               {
                                        //var tmppilihan = getKodeReportLabRegis();
                                        // var params = GetCriteriaLabRegis();
                                        var params = {
                                            asal_pasien : Ext.getCmp('cboPilihanLabRegis').getValue(),
                                            tgl_awal    : Ext.getCmp('dtpTglAwalFilterLapRegisLab').getValue(),
                                            tgl_akhir   : Ext.getCmp('dtpTglAkhirFilterLapRegisLab').getValue(),
                                            kelompok    : Ext.getCmp('cboPilihanLabRegiskelompokPasien').getValue(),
                                            kd_customer : Ext.getCmp('cboPerusahaanRequestEntryRegisLab').getValue(),
                                            all_shift   : Ext.getCmp('Shift_All_LabRegis').getValue(),
                                            shift_1     : Ext.getCmp('Shift_1_LabRegis').getValue(),
                                            shift_2     : Ext.getCmp('Shift_2_LabRegis').getValue(),
                                            shift_3     : Ext.getCmp('Shift_3_LabRegis').getValue(),
                                            type_file   : Ext.getCmp('format_print_LabRegis').getValue(),
                                        };
                                        console.log(params);
										//loadMask.show();
										var form = document.createElement("form");
										form.setAttribute("method", "post");
										form.setAttribute("target", "_blank");
										form.setAttribute("action", baseURL + "index.php/lab/lap_LABRegister/cetak");
										var hiddenField = document.createElement("input");
										hiddenField.setAttribute("type", "hidden");
										hiddenField.setAttribute("name", "data");
										hiddenField.setAttribute("value", Ext.encode(params));
										form.appendChild(hiddenField);
										document.body.appendChild(form);
										form.submit();	
                                        /*loadlaporanRadLab('0', 'LapRegisLab', criteria, function(){
											frmDlgLabRegis.close();
											loadMask.hide();
										});*/
								};
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapLabRegis',
                            handler: function()
                            {
                                    frmDlgLabRegis.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapLabRegis;
};

function GetCriteriaLabRegis()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalFilterLapRegisLab').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalFilterLapRegisLab').getValue();
	}
	if (Ext.get('dtpTglAkhirFilterLapRegisLab').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterLapRegisLab').getValue();
	}
	//[[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
	if(Ext.getCmp('cboPilihanLabRegis').getValue() !== '')
	{
		if (Ext.get('cboPilihanLabRegis').getValue()=== 'Semua'){
			strKriteria += '##@@##' + 'Pasien'
			strKriteria += '##@@##' + Ext.get('cboPilihanLabRegis').getValue()
		} else if (Ext.get('cboPilihanLabRegis').getValue()=== 'RWJ/IGD'){
			strKriteria += '##@@##' + 'Pasien'
			strKriteria += '##@@##' + Ext.get('cboPilihanLabRegis').getValue()
		} else if (Ext.get('cboPilihanLabRegis').getValue()=== 'RWI'){
			strKriteria += '##@@##' + 'Pasien'
			strKriteria += '##@@##' + Ext.get('cboPilihanLabRegis').getValue()
		} else {
			strKriteria += '##@@##' + 'Pasien'
			strKriteria += '##@@##' + Ext.get('cboPilihanLabRegis').getValue()
		} 
	}
       
	if (Ext.getCmp('cboPilihanLabRegiskelompokPasien').getValue() !== '')
	{
		if (Ext.get('cboPilihanLabRegiskelompokPasien').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
			strKriteria += '##@@##' + 'NULL';
        } else if (Ext.get('cboPilihanLabRegiskelompokPasien').getValue() === 'Perseorangan'){

			strKriteria += '##@@##' + 'Perseorangan';
            strKriteria += '##@@##' + Ext.get('cboPerseoranganRegisLab').getValue();
            strKriteria += '##@@##' + Ext.getCmp('cboPerseoranganRegisLab').getValue();
        } else if (Ext.get('cboPilihanLabRegiskelompokPasien').getValue() === 'Perusahaan'){
            
			if(Ext.getCmp('cboPerusahaanRequestEntryRegisLab').getValue() === '')
            {
                strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + 'NULL';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Perusahaan';
				strKriteria += '##@@##' + selectsetnamaperusahaan;
                strKriteria += '##@@##' + selectsetperusahaan;
            }
        } else {
            
			if(Ext.getCmp('cboAsuransiRegisLab').getValue() === '')
            {
                strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + 'NULL';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Asuransi';
				strKriteria += '##@@##' + selectsetnamaAsuransi;
                strKriteria += '##@@##' + selectSetAsuransi;
            }
            
        } 
	}   
	if (Ext.getCmp('Shift_All_LabRegis').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_LabRegis').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_LabRegis').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_LabRegis').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	}
	return strKriteria;
};

function getKodeReportLabRegis()
{   var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLabRegis').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLabRegis').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLabRegis()
{
    var x=1;
	if(Ext.getCmp('cboPilihanLabRegis').getValue() === ''){
		ShowPesanWarningLabRegisReport('Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
    /* if(Ext.get('dtpTglAwalFilterLapRegisLab').dom.value > Ext.get('dtpTglAkhirFilterLapRegisLab').dom.value)
    {
        ShowPesanWarningLabRegisReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    } */
	if(Ext.getCmp('cboPilihanLabRegiskelompokPasien').getValue() === ''){
		ShowPesanWarningLabRegisReport('Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	if(Ext.get('cboPerusahaanRequestEntryRegisLab').getValue() === '' &&  Ext.get('cboAsuransiRegisLab').getValue() === '' &&  Ext.get('cboPerseoranganRegisLab').getValue() === '' && Ext.get('cboUmumRegisLab').getValue() === ''){
		ShowPesanWarningLabRegisReport('Sub Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	if(Ext.getCmp('Shift_All_LabRegis').getValue() === false && Ext.getCmp('Shift_1_LabRegis').getValue() === false && Ext.getCmp('Shift_2_LabRegis').getValue() === false && Ext.getCmp('Shift_3_LabRegis').getValue() === false){
		ShowPesanWarningLabRegisReport(nmRequesterRequest,'Laporan Regis Laboratorium');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningLabRegisReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLabRegis_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLabRegis(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterLapRegisLab',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 40,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterLapRegisLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLabRegisKelompokPasien(),
                mComboUmumLabRegis(),
				mComboPerseoranganLabRegis(),
                mComboAsuransiLabRegis(),
                mComboPerusahaanLabRegis()
                
            ]
        }]
    };
    return items;
};

function getItemLapLabRegis_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {
                                        boxLabel: 'Semua',
                                        name: 'Shift_All_LabRegis',
                                        id : 'Shift_All_LabRegis',
                                        handler: function (field, value) {
                                            if (value === true){
                                                Ext.getCmp('Shift_1_LabRegis').setValue(true);
                                                Ext.getCmp('Shift_2_LabRegis').setValue(true);
                                                Ext.getCmp('Shift_3_LabRegis').setValue(true);
                                                Ext.getCmp('Shift_1_LabRegis').disable();
                                                Ext.getCmp('Shift_2_LabRegis').disable();
                                                Ext.getCmp('Shift_3_LabRegis').disable();
                                            }else{
                                                    Ext.getCmp('Shift_1_LabRegis').setValue(false);
                                                    Ext.getCmp('Shift_2_LabRegis').setValue(false);
                                                    Ext.getCmp('Shift_3_LabRegis').setValue(false);
                                                    Ext.getCmp('Shift_1_LabRegis').enable();
                                                    Ext.getCmp('Shift_2_LabRegis').enable();
                                                    Ext.getCmp('Shift_3_LabRegis').enable();
                                                }
                                        }
                                    },
                                    {
                                        boxLabel: 'Shift 1',
                                        name: 'Shift_1_LabRegis',
                                        id : 'Shift_1_LabRegis'
                                    },
                                    {
                                        boxLabel: 'Shift 2',
                                        name: 'Shift_2_LabRegis',
                                        id : 'Shift_2_LabRegis'
                                    },
                                    {
                                        boxLabel: 'Shift 3',
                                        name: 'Shift_3_LabRegis',
                                        id : 'Shift_3_LabRegis'
                                    }
                               ]
                        }
                    ]
            },
            {
                    xtype: 'fieldset',
                    title: 'Format print',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {
                                        boxLabel: 'Excel',
                                        name: 'format_print_LabRegis',
                                        id : 'format_print_LabRegis'
									},
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;

function mComboPilihanLabRegis()
{
    var cboPilihanLabRegis = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLabRegis',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLabRegis;
};

function mComboPilihanLabRegisKelompokPasien()
{
    var cboPilihanLabRegiskelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanLabRegiskelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLabRegiskelompokPasien;
};

function mComboPerseoranganLabRegis()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRegisLab = new WebApp.DataStore({fields: Field});
    dsPerseoranganRegisLab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganRegisLab = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganRegisLab',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: dsPerseoranganRegisLab,
				/* new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ), */
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRegisLab;
};

function mComboUmumLabRegis()
{
    var cboUmumRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumRegisLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumRegisLab;
};

function mComboPerusahaanLabRegis()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=1 order by CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryRegisLab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRegisLab;
};

function mComboAsuransiLabRegis()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomerLab',
                param: "jenis_cust=2 order by CUSTOMER"
            }
        }
    );
    var cboAsuransiRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiRegisLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiRegisLab;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRegisLab').show();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').show();
        Ext.getCmp('cboUmumRegisLab').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').show();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').show();
   }
}

function mCombounitLabRegis()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryLabRegis = new Ext.form.ComboBox
    (
        {
            id: 'cbounitRequestEntryLabRegis',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                }
                    
                }
        }
    )

    return cbounitRequestEntryLabRegis;
};
