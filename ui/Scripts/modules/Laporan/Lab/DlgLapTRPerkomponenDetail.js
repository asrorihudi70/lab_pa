
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapTRPerkomponenDetail;
var selectNamaLapTRPerkomponenDetail;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapTRPerkomponenDetail;
var varLapLapTRPerkomponenDetail= ShowFormLapLapTRPerkomponenDetail();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapTRPerkomponenDetail;
var customer;
var kdcustomer;
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var winLapTRPerkomponenDetailReport;

function ShowFormLapLapTRPerkomponenDetail()
{
    frmDlgLapTRPerkomponenDetail= fnDlgLapTRPerkomponenDetail();
    frmDlgLapTRPerkomponenDetail.show();
	loadDataComboUserLapTRPerkomponenDetail();
};

function fnDlgLapTRPerkomponenDetail()
{
    winLapTRPerkomponenDetailReport = new Ext.Window
    (
        {
            id: 'winLapTRPerkomponenDetailReport',
            title: 'Laporan Transaksi Perkomponen Detail',
            closeAction: 'destroy',
            width: 370,
            height: 410,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapTRPerkomponenDetail()],
            listeners:
			{
				activate: function()
				{
					Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').hide();
					Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').hide();
					Ext.getCmp('cboUmumLapTRPerkomponenDetail').show();
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'Ok',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapTRPerkomponenDetail',
						handler: function()
						{
							if (ValidasiReportLapTRPerkomponenDetail() === 1)
							{
								if (Ext.get('cboPilihanLapTRPerkomponenDetailkelompokPasien').getValue() === 'Semua')
								{
									tipe='Semua';
									customer='Semua';
									kdcustomer='Semua';
								} else if (Ext.get('cboPilihanLapTRPerkomponenDetailkelompokPasien').getValue() === 'Perseorangan'){
									customer=Ext.get('cboPerseoranganLapTRPerkomponenDetail').getValue();
									tipe='Perseorangan';
									kdcustomer=Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').getValue();
								} else if (Ext.get('cboPilihanLapTRPerkomponenDetailkelompokPasien').getValue() === 'Perusahaan'){
									customer=Ext.get('cboPerusahaanRequestEntryLapTRPerkomponenDetail').getValue();
									tipe='Perusahaan';
									kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').getValue();
								} else {
									customer=Ext.get('cboAsuransiLapTRPerkomponenDetail').getValue();
									tipe='Asuransi';
									kdcustomer=Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').getValue();
								} 
								
								if(Ext.getCmp('radioasal').getValue() === true){
									periode='tanggal';
									tglAwal=Ext.getCmp('dtpTglAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
								} else{
									periode='bulan';
									tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilLab').getValue();
								}
								
								if (Ext.getCmp('Shift_All_LapTRPerkomponenDetail').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1_LapTRPerkomponenDetail').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2_LapTRPerkomponenDetail').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3_LapTRPerkomponenDetail').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
								
								var params={
									asal_pasien:Ext.get('cboPilihanLapTRPerkomponenDetail').getValue(),
									user:Ext.getCmp('cboUserRequestEntryLapTRPerkomponenDetail').getValue(),
									tipe:tipe,
									customer:customer,
									kdcustomer:kdcustomer,
									periode:periode,
									tglAwal:tglAwal,
									tglAkhir:tglAkhir,
									shift:shift,
									shift1:shift1,
									shift2:shift2,
									shift3:shift3,
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/lab/lap_laboratorium/cetakTRPerkomponenDetail");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapTRPerkomponenDetailReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapTRPerkomponenDetail',
						handler: function()
						{
							winLapTRPerkomponenDetailReport.close();
						}
					}
			]

        }
    );

    return winLapTRPerkomponenDetailReport;
};


function ItemDlgLapTRPerkomponenDetail()
{
    var PnlLapLapTRPerkomponenDetail = new Ext.Panel
    (
        {
            id: 'PnlLapLapTRPerkomponenDetail',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapTRPerkomponenDetail_Atas(),
                getItemLapLapTRPerkomponenDetail_Batas(),
                getItemLapLapTRPerkomponenDetail_Bawah(),
                getItemLapLapTRPerkomponenDetail_Batas(),
                getItemLapLapTRPerkomponenDetail_Samping(),
              
            ]
        }
    );

    return PnlLapLapTRPerkomponenDetail;
};


function getKodeReportLapTRPerkomponenDetail()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLapTRPerkomponenDetail').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLapTRPerkomponenDetail').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLapTRPerkomponenDetail()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapTRPerkomponenDetail').getValue() === ''){
		ShowPesanWarningLapTRPerkomponenDetailReport('Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboUserRequestEntryLapTRPerkomponenDetail').getValue() === ''){
		ShowPesanWarningLapTRPerkomponenDetailReport('Operator Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboPilihanLapTRPerkomponenDetailkelompokPasien').getValue() === ''){
		ShowPesanWarningLapTRPerkomponenDetailReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapTRPerkomponenDetail').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapTRPerkomponenDetail').getValue() === '' &&  Ext.get('cboAsuransiLapTRPerkomponenDetail').getValue() === '' && Ext.get('cboUmumRegisLab').getValue() === '' ){
		ShowPesanWarningLapTRPerkomponenDetailReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
   /*  if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapTRPerkomponenDetailReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    } */
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapTRPerkomponenDetailReport('Periode belum dipilih','Warning');
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapTRPerkomponenDetail').getValue() === false && Ext.getCmp('Shift_1_LapTRPerkomponenDetail').getValue() === false && Ext.getCmp('Shift_2_LapTRPerkomponenDetail').getValue() === false && Ext.getCmp('Shift_3_LapTRPerkomponenDetail').getValue() === false){
		ShowPesanWarningLapTRPerkomponenDetailReport('Shift Belum Dipilih','Warning');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapTRPerkomponenDetailReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapTRPerkomponenDetail_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTRPerkomponenDetail(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboUserLapTRPerkomponenDetail(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTRPerkomponenDetailKelompokPasien(),
                mComboPerseoranganLapTRPerkomponenDetail(),
                mComboAsuransiLapTRPerkomponenDetail(),
                mComboPerusahaanLapTRPerkomponenDetail(),
                mComboUmumLapTRPerkomponenDetail()
            ]
        }]
    };
    return items;
};


function getItemLapLapTRPerkomponenDetail_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapTRPerkomponenDetail_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  345,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
				
					{
						x: 40,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapTRPerkomponenDetail',
						id : 'Shift_All_LapTRPerkomponenDetail',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapTRPerkomponenDetail').setValue(true);
								Ext.getCmp('Shift_2_LapTRPerkomponenDetail').setValue(true);
								Ext.getCmp('Shift_3_LapTRPerkomponenDetail').setValue(true);
								Ext.getCmp('Shift_1_LapTRPerkomponenDetail').disable();
								Ext.getCmp('Shift_2_LapTRPerkomponenDetail').disable();
								Ext.getCmp('Shift_3_LapTRPerkomponenDetail').disable();
							}else{
								Ext.getCmp('Shift_1_LapTRPerkomponenDetail').setValue(false);
								Ext.getCmp('Shift_2_LapTRPerkomponenDetail').setValue(false);
								Ext.getCmp('Shift_3_LapTRPerkomponenDetail').setValue(false);
								Ext.getCmp('Shift_1_LapTRPerkomponenDetail').enable();
								Ext.getCmp('Shift_2_LapTRPerkomponenDetail').enable();
								Ext.getCmp('Shift_3_LapTRPerkomponenDetail').enable();
							}
						}
					},
					{
						x: 110,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapTRPerkomponenDetail',
						id : 'Shift_1_LapTRPerkomponenDetail'
					},
					{
						x: 180,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapTRPerkomponenDetail',
						id : 'Shift_2_LapTRPerkomponenDetail'
					},
					{
						x: 250,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapTRPerkomponenDetail',
						id : 'Shift_3_LapTRPerkomponenDetail'
					}
				]
			}
        ]
            
    };
    return items;
};

function getItemLapLapTRPerkomponenDetail_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilLab',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilLab',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilLab',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function loadDataComboUserLapTRPerkomponenDetail(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/lab/lap_laboratorium/getUser",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUserRequestEntryLapTRPerkomponenDetail.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_User_LapTRPerkomponenDetail.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_User_LapTRPerkomponenDetail.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboPilihanLapTRPerkomponenDetail()
{
    var cboPilihanLapTRPerkomponenDetail = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapTRPerkomponenDetail',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ'],[3, 'RWI'],[4, 'IGD'], [5, 'Kunjungan Langsung']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapTRPerkomponenDetail;
};

function mComboPilihanLapTRPerkomponenDetailKelompokPasien()
{
    var cboPilihanLapTRPerkomponenDetailkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanLapTRPerkomponenDetailkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapTRPerkomponenDetailkelompokPasien;
};

function mComboPerseoranganLapTRPerkomponenDetail()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapTRPerkomponenDetail = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapTRPerkomponenDetail.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapTRPerkomponenDetail = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganLapTRPerkomponenDetail',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapTRPerkomponenDetail,
				/* new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ), */
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLapTRPerkomponenDetail;
};

function mComboUmumLapTRPerkomponenDetail()
{
    var cboUmumLapTRPerkomponenDetail = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumLapTRPerkomponenDetail',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumLapTRPerkomponenDetail;
};

function mComboPerusahaanLapTRPerkomponenDetail()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapTRPerkomponenDetail = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryLapTRPerkomponenDetail',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapTRPerkomponenDetail;
};

function mComboAsuransiLapTRPerkomponenDetail()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomerLab',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapTRPerkomponenDetail = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiLapTRPerkomponenDetail',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapTRPerkomponenDetail;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').show();
        Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenDetail').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').show();
        Ext.getCmp('cboUmumLapTRPerkomponenDetail').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenDetail').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenDetail').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenDetail').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenDetail').show();
   }
}

function mComboUserLapTRPerkomponenDetail()
{
    var Field = ['kd_user','full_name'];
    ds_User_LapTRPerkomponenDetail = new WebApp.DataStore({fields: Field});
    cboUserRequestEntryLapTRPerkomponenDetail = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cboUserRequestEntryLapTRPerkomponenDetail',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:'Semua',
            store: ds_User_LapTRPerkomponenDetail,
            valueField: 'kd_user',
            displayField: 'full_name',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.kd_user;
				} 
			}
        }
    )

    return cboUserRequestEntryLapTRPerkomponenDetail;
};
