var type_file=0;
var dsLABBatalTransaksi;
var selectLABBatalTransaksi;
var selectNamaLABBatalTransaksi;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgLABBatalTransaksi;
var varLapLABBatalTransaksi= ShowFormLapLABBatalTransaksi();
var dsLAB;

function ShowFormLapLABBatalTransaksi()
{
    frmDlgLABBatalTransaksi= fnDlgLABBatalTransaksi();
    frmDlgLABBatalTransaksi.show();
};

function fnDlgLABBatalTransaksi()
{
    var winLABBatalTransaksiReport = new Ext.Window
    (
        {
            id: 'winLABBatalTransaksiReport',
            title: 'Laporan Pembatalan Pembayaran',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLABBatalTransaksi()]

        }
    );

    return winLABBatalTransaksiReport;
};

function ItemDlgLABBatalTransaksi()
{
    var PnlLapLABBatalTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapLABBatalTransaksi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapLABBatalTransaksi_Periode(),
				//getItemLABBatalTransaksi_Shift(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapLABBatalTransaksi',
					handler: function()
					{
					   if (ValidasiReportLABBatalTransaksi() === 1)
					   {
						
							var params={
												tglAwal:Ext.getCmp('dtpTglAwalLapLABBatalTransaksi').getValue(),
												tglAkhir:Ext.getCmp('dtpTglAkhirLapLABBatalTransaksi').getValue(),
												type_file:type_file
									} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/lab/lap_laboratorium/cetak_laporan_batal_pembayaran");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgLABBatalTransaksi.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLapLABBatalTransaksi').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapLABBatalTransaksi').getValue()+'#aje#Laporan Batal Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionLAB/cetakLABBatalTransaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgLABBatalTransaksi.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapLABBatalTransaksi',
					handler: function()
					{
						frmDlgLABBatalTransaksi.close();
					}
				}
			
			]
        }
    );

    return PnlLapLABBatalTransaksi;
};


function ValidasiReportLABBatalTransaksi()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapLABBatalTransaksi').dom.value === '')
	{
		ShowPesanWarningLABBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapLABBatalTransaksi').getValue() === false && Ext.getCmp('Shift_1_LapLABBatalTransaksi').getValue() === false && Ext.getCmp('Shift_2_LapLABBatalTransaksi').getValue() === false && Ext.getCmp('Shift_3_LapLABBatalTransaksi').getValue() === false){
		ShowPesanWarningLABBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportLABBatalTransaksi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapLABBatalTransaksi').dom.value > Ext.get('dtpTglAkhirLapLABBatalTransaksi').dom.value)
    {
        ShowPesanWarningLABBatalTransaksiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningLABBatalTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapLABBatalTransaksi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapLABBatalTransaksi_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapLABBatalTransaksi',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					{
					   xtype: 'checkbox',
					   fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					}
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapLABBatalTransaksi',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


