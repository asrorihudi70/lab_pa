var type_file=0;
var dsLABBukaTransaksi;
var selectLABBukaTransaksi;
var selectNamaLABBukaTransaksi;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgLABBukaTransaksi;
var varLapLABBukaTransaksi= ShowFormLapLABBukaTransaksi();
var dsLAB;

function ShowFormLapLABBukaTransaksi()
{
    frmDlgLABBukaTransaksi= fnDlgLABBukaTransaksi();
    frmDlgLABBukaTransaksi.show();
};

function fnDlgLABBukaTransaksi()
{
    var winLABBukaTransaksiReport = new Ext.Window
    (
        {
            id: 'winLABBukaTransaksiReport',
            title: 'Laporan Histori Buka Transaksi',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLABBukaTransaksi()]

        }
    );

    return winLABBukaTransaksiReport;
};

function ItemDlgLABBukaTransaksi()
{
    var PnlLapLABBukaTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapLABBukaTransaksi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapLABBukaTransaksi_Periode(),
				//getItemLABBukaTransaksi_Shift(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapLABBukaTransaksi',
					handler: function()
					{
					   if (ValidasiReportLABBukaTransaksi() === 1)
					   {
						
							var params={
												tglAwal:Ext.getCmp('dtpTglAwalLapLABBukaTransaksi').getValue(),
												tglAkhir:Ext.getCmp('dtpTglAkhirLapLABBukaTransaksi').getValue(),
												type_file:type_file,
												
									} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/lab/lap_laboratorium/cetak_laporan_buka_transaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgLABBukaTransaksi.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLapLABBukaTransaksi').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapLABBukaTransaksi').getValue()+'#aje#Laporan Buka Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionLAB/cetakLABBukaTransaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgLABBukaTransaksi.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapLABBukaTransaksi',
					handler: function()
					{
						frmDlgLABBukaTransaksi.close();
					}
				}
			
			]
        }
    );

    return PnlLapLABBukaTransaksi;
};


function ValidasiReportLABBukaTransaksi()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapLABBukaTransaksi').dom.value === '')
	{
		ShowPesanWarningLABBukaTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapLABBukaTransaksi').getValue() === false && Ext.getCmp('Shift_1_LapLABBukaTransaksi').getValue() === false && Ext.getCmp('Shift_2_LapLABBukaTransaksi').getValue() === false && Ext.getCmp('Shift_3_LapLABBukaTransaksi').getValue() === false){
		ShowPesanWarningLABBukaTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportLABBukaTransaksi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapLABBukaTransaksi').dom.value > Ext.get('dtpTglAkhirLapLABBukaTransaksi').dom.value)
    {
        ShowPesanWarningLABBukaTransaksiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningLABBukaTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapLABBukaTransaksi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapLABBukaTransaksi_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapLABBukaTransaksi',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					{
					   xtype: 'checkbox',
					   fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					}
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapLABBukaTransaksi',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


