
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgDlgLapJasaPelayananDokterSummary;
var varDlgLapJasaPelayananDokterSummary= ShowFormDlgLapJasaPelayananDokterSummary();
var asc=0;
var IdRootKelompokBarang_pDlgLapJasaPelayananDokterSummary='1000000000';
var KdJPD_DlgLapJasaPelayananDokterSummary;
var NamaSub_DlgLapJasaPelayananDokterSummary;
var gridPanelLookUpBarang_DlgLapJasaPelayananDokterSummary;
var sendDataArray = [];
var dsvComboDokterDlgLapJasaPelayananDokterSummary;
var dsvComboSubKelompokPasienDlgLapJasaPelayananDokterSummary;
function ShowFormDlgLapJasaPelayananDokterSummary()
{
    frmDlgDlgLapJasaPelayananDokterSummary= fnDlgDlgLapJasaPelayananDokterSummary();
    frmDlgDlgLapJasaPelayananDokterSummary.show();
	//GetStrpasienSetBarang_DlgLapJasaPelayananDokterSummary();
};

function fnDlgDlgLapJasaPelayananDokterSummary()
{
    var winDlgLapJasaPelayananDokterSummaryReport = new Ext.Window
    (
        {
            id: 'winDlgLapJasaPelayananDokterSummaryReport',
            title: 'Laporan Jasa Pelayanan Dokter Summary',
            closeAction: 'destroy',
            width: 370,
            height: 320,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgDlgLapJasaPelayananDokterSummary()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkDlgLapJasaPelayananDokterSummary',
					handler: function()
					{
							var params={
								KdJPD:KdJPD_DlgLapJasaPelayananDokterSummary,
								namaSub:NamaSub_DlgLapJasaPelayananDokterSummary,
								tglAwal:Ext.getCmp('dtpTglAwalJPDLapStokPersediaanBarang').getValue(),
								tahun:Ext.getCmp('dtpTglAwalJPDLapStokPersediaanBarang').getValue().format('Y'),
								sekarang:now
								
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/JPD/lap_JPDSummary/cetakBarang");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
						
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelDlgLapJasaPelayananDokterSummary',
					handler: function()
					{
						frmDlgDlgLapJasaPelayananDokterSummary.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winDlgLapJasaPelayananDokterSummaryReport;
};


function ItemDlgDlgLapJasaPelayananDokterSummary()
{
    var PnlDlgLapJasaPelayananDokterSummary = new Ext.Panel
    (
        {
            id: 'PnlDlgLapJasaPelayananDokterSummary',
            fileUpload: true,
            layout: 'form',
            height: 370,
            anchor: '100%',
            bodyStyle: 'padding:10px',
            border: true,
            items:
            [
				getItemDlgLapJasaPelayananDokterSummary_pasien(),
				getItemDlgLapJasaPelayananDokterSummary_Batas(),
				getItemDlgLapJasaPelayananDokterSummary_Tanggal(),
				//getItemDlgLapJasaPelayananDokterSummary_Batas(),
				//getItemDlgLapJasaPelayananDokterSummary_Shift(),
            ]
        }
    );

    return PnlDlgLapJasaPelayananDokterSummary;
};


function getItemDlgLapJasaPelayananDokterSummary_pasien()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  335,
				height: 125,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						xtype: 'label',
						text: 'Asal Pasien'
					}, 
					{
						x: 100,
						y: 5,
						xtype: 'label',
						text: ' : '
					},
					comboAsalPasienListView_DlgLapJasaPelayananDokterSummary(),
					{
						x: 10,
						y: 35,
						xtype: 'label',
						text: 'Kelompok Pasien'
					}, 
					{
						x: 100,
						y: 35,
						xtype: 'label',
						text: ' : '
					},
					comboKelompokPasienListView_DlgLapJasaPelayananDokterSummary(),
					comboSubKelompokPasienListView_DlgLapJasaPelayananDokterSummary(),
					{
						x: 10,
						y: 95,
						xtype: 'label',
						text: 'Dokter'
					}, 
					{
						x: 100,
						y: 95,
						xtype: 'label',
						text: ' : '
					},
					comboDokterPasienListView_DlgLapJasaPelayananDokterSummary()
				]
			}
		]
    };
    return items;
};



function GetStrpasienSetBarang_DlgLapJasaPelayananDokterSummary()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and left(kd_JPD,1)<>'8'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrpasienSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function comboAsalPasienListView_DlgLapJasaPelayananDokterSummary()
{	
    var cbo_LapJPDSummaryLabAsalPasien = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 5,
            flex: 1,
			id: 'cbo_AsalPasienLapJPDSummaryLab',
			/*valueField: 'kd_Triwulan',
            //displayField: 'Triwulan',*/
			store: ['Semua Pasien','Pasien Rawat Jalan', 'Pasien Rawat Inap', 'Pasien Gawat Darurat', 'Pasien UMUM'],
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					/*nomorCmbMutasiTriwulanInv=parseInt(c)+1;
					console.log(nomorCmbMutasiTriwulanInv);*/
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDSummaryLabAsalPasien;
};

function comboKelompokPasienListView_DlgLapJasaPelayananDokterSummary()
{	
    var cbo_LapJPDSummaryLabKelompokPasien = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 35,
            flex: 1,
			id: 'cbo_KelompokPasienLapJPDSummaryLab',
			/*valueField: 'kd_Triwulan',
            //displayField: 'Triwulan',*/
			store: ['Semua','Perorangan', 'Perusahaan', 'Asuransi'],
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					if (c===0)
					{
						Ext.getCmp('cbo_SubKelompokPasienLapJPDSummaryLab').disable(true);
					}
					else
					{
						Ext.getCmp('cbo_SubKelompokPasienLapJPDSummaryLab').enable(true);
						Ext.getCmp('cbo_SubKelompokPasienLapJPDSummaryLab').setValue('');
						dsComboSubKelompokPasienDlgLapJasaPelayananDokterSummary(c)
					}
					dsComboSubKelompokPasienDlgLapJasaPelayananDokterSummary(c)
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDSummaryLabKelompokPasien;
};
function dsComboSubKelompokPasienDlgLapJasaPelayananDokterSummary(q)
{
	if (q===1)
	{
		dsvComboSubKelompokPasienDlgLapJasaPelayananDokterSummary.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSubKelompokPasienDlgLapJasaPelayananDokterDetailPerorangan',
                    //param : 'kd_tarif=1'
                }			
            }
        );  
	}
	else if (q===2)
	{
		dsvComboSubKelompokPasienDlgLapJasaPelayananDokterSummary.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSubKelompokPasienDlgLapJasaPelayananDokterDetailPerusahaan',
                    //param : 'kd_tarif=1'
                }			
            }
        );  
	}
	else if (q===3)
	{
		dsvComboSubKelompokPasienDlgLapJasaPelayananDokterSummary.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSubKelompokPasienDlgLapJasaPelayananDokterDetailAsuransi',
                    //param : 'kd_tarif=1'
                }			
            }
        );  
	}
	 
    return dsvComboSubKelompokPasienDlgLapJasaPelayananDokterSummary;
}
function comboSubKelompokPasienListView_DlgLapJasaPelayananDokterSummary()
{	
	var Field =['kd_customer','customer'];
    dsvComboSubKelompokPasienDlgLapJasaPelayananDokterSummary = new WebApp.DataStore({fields: Field});
	dsComboSubKelompokPasienDlgLapJasaPelayananDokterSummary();
    var cbo_LapJPDSummaryLabSubKelompok = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 65,
            flex: 1,
			id: 'cbo_SubKelompokPasienLapJPDSummaryLab',
			valueField: 'kd_customer',
            displayField: 'customer',
			store: dsvComboSubKelompokPasienDlgLapJasaPelayananDokterSummary,
			mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			disabled:true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					/*nomorCmbMutasiTriwulanInv=parseInt(c)+1;
					console.log(nomorCmbMutasiTriwulanInv);*/
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDSummaryLabSubKelompok;
};
function dsComboDokterDlgLapJasaPelayananDokterSummary()
{
	dsvComboDokterDlgLapJasaPelayananDokterSummary.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboDokterDlgLapJasaPelayananDokterDetail',
                    //param : 'kd_tarif=1'
                }			
            }
        );   
    return dsvComboDokterDlgLapJasaPelayananDokterSummary;
}
function comboDokterPasienListView_DlgLapJasaPelayananDokterSummary()
{	
	var Field =['kd_dokter','nama'];
    dsvComboDokterDlgLapJasaPelayananDokterSummary = new WebApp.DataStore({fields: Field});
	dsComboDokterDlgLapJasaPelayananDokterSummary();
    var cbo_LapJPDSummaryLabDokter = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 95,
            flex: 1,
			id: 'cbo_DokterPasienLapJPDSummaryLab',
			valueField: 'kd_dokter',
            displayField: 'nama',
			store: dsvComboDokterDlgLapJasaPelayananDokterSummary,
			mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:205,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					/*nomorCmbMutasiTriwulanInv=parseInt(c)+1;
					console.log(nomorCmbMutasiTriwulanInv);*/
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapJPDSummaryLabDokter;
};

function getItemDlgLapJasaPelayananDokterSummary_Tanggal()
{
	radioTanggal = new Ext.form.RadioGroup({
	x: 10,
	y: 5,
	id :'radiosTanggalJPDSummary',
	columns  : 1,
	rows : 2,
	items: 
	[
		{
			
			id:'rbTanggalJPDSummary',
			name: 'rbLapJPD',
			boxLabel: 'Tanggal',
			checked: true, 
			//inputValue: "1"
		},
		{
			x: 10,
			y: 55,
			id:'rbBulanJPDSummary',
			name: 'rbLapJPD',
			boxLabel: 'Bulan',
			//inputValue: "1"
		},
	],
	listeners: {
		change : function()
		{ 
			if (Ext.getCmp('rbBulanJPDSummary').checked===true)
			{
				Ext.getCmp('dtpTglAwalJPDLapSummary').disable(true)
				Ext.getCmp('dtpTglAkhirJPDLapSummary').disable(true)
				Ext.getCmp('dtpBlnAwalJPDLapSummary').enable(true)
				Ext.getCmp('dtpBlnAkhirJPDLapSummary').enable(true)
			}
			else
			{
				Ext.getCmp('dtpTglAwalJPDLapSummary').enable(true)
				Ext.getCmp('dtpTglAkhirJPDLapSummary').enable(true)
				Ext.getCmp('dtpBlnAwalJPDLapSummary').disable(true)
				Ext.getCmp('dtpBlnAkhirJPDLapSummary').disable(true)
			}
		}
	}
		
   });
   
   
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  335,
				height: 100,
				anchor: '100% 100%',
				items: [
					radioTanggal,
					  {
							x: 80,
							y: 5,
							xtype: 'datefield',
							id: 'dtpTglAwalJPDLapSummary',
							format: 'd/M/Y',
							value: now
						}
					,
					{
						x: 190,
						y: 5,
						xtype: 'label',
						text: ' s / d '
					},
					{
						x: 220,
						y: 5,
						xtype: 'datefield',
						id: 'dtpTglAkhirJPDLapSummary',
						format: 'd/M/Y',
						value: now
					}, 
					//radioBulan,
					{
							x: 80,
							y: 30,
							xtype: 'datefield',
							id: 'dtpBlnAwalJPDLapSummary',
							disabled: true,
							format: 'M/Y',
							value: now
						}
					,
					{
						x: 190,
						y: 30,
						xtype: 'label',
						text: ' s / d '
					},
					{
						x: 220,
						y: 30,
						xtype: 'datefield',
						id: 'dtpBlnAkhirJPDLapSummary',
						disabled: true,
						format: 'M/Y',
						value: now
					}, 
					{
					   xtype: 'checkbox',
					   id: 'CekSemuaShiftLapJPDSummary',
					   hideLabel:false,
					   boxLabel: 'Semua',
					   checked: true,
					   x: 10,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
								if(Ext.getCmp('CekSemuaShiftLapJPDSummary').getValue()===true)
								{
									Ext.getCmp('CekShift1LapJPDSummary').setValue(true);
									Ext.getCmp('CekShift1LapJPDSummary').disable(true);
									
									Ext.getCmp('CekShift2LapJPDSummary').setValue(true);
									Ext.getCmp('CekShift2LapJPDSummary').disable(true);
									
									Ext.getCmp('CekShift3LapJPDSummary').setValue(true)
									Ext.getCmp('CekShift3LapJPDSummary').disable(true);
								}
								else
								{
									Ext.getCmp('CekShift1LapJPDSummary').setValue(true);
									Ext.getCmp('CekShift1LapJPDSummary').enable(true);
									
									Ext.getCmp('CekShift2LapJPDSummary').setValue(false)
									Ext.getCmp('CekShift2LapJPDSummary').enable(true)
									
									Ext.getCmp('CekShift3LapJPDSummary').setValue(false)
									Ext.getCmp('CekShift3LapJPDSummary').enable(true)
								}	
							}
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift1LapJPDSummary',
					   hideLabel:false,
					   boxLabel: 'Shift 1',
					   checked: true,
					   disabled: true,
					   x: 65,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift2LapJPDSummary',
					   hideLabel:false,
					   boxLabel: 'Shift 2',
					   checked: true,
					   disabled: true,
					   x: 125,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift3LapJPDSummary',
					   hideLabel:false,
					   boxLabel: 'Shift 3',
					   checked: true,
					   disabled: true,
					   x: 180,
					   y: 65,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
				]
			}
		]
    };
    return items;
};

/*function getItemDlgLapJasaPelayananDokterSummary_Shift()
{
	var items = {
        layout: 'column',
        border: false,
        items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				anchor: '100% 100%',
				items: [
				{
				   xtype: 'checkbox',
				   id: 'CekSemuaShiftLapJPDSummary',
				   hideLabel:false,
				   //checked: false,
				   x: 10,
				   y: 5,
				   listeners: 
				   {
						check: function()
						{
								
						 }
				   }
				}
			]
		}]
	}
}*/
function getItemDlgLapJasaPelayananDokterSummary_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};



function gridDataViewBarang_DlgLapJasaPelayananDokterSummary()
{
    var FieldGrdBArang_DlgLapJasaPelayananDokterSummary = [];
	
    dsDataGrdBarang_DlgLapJasaPelayananDokterSummary= new WebApp.DataStore
	({
        fields: FieldGrdBArang_DlgLapJasaPelayananDokterSummary
    });
    
    gridPanelLookUpBarang_DlgLapJasaPelayananDokterSummary =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_DlgLapJasaPelayananDokterSummary,
		height:120,
		width:448,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_DlgLapJasaPelayananDokterSummary.getAt(row).data.kd_JPD);
					KdJPD_DlgLapJasaPelayananDokterSummary = dsDataGrdBarang_DlgLapJasaPelayananDokterSummary.getAt(row).data.kd_JPD;
					NamaSub_DlgLapJasaPelayananDokterSummary = dsDataGrdBarang_DlgLapJasaPelayananDokterSummary.getAt(row).data.nama_sub;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'JPD_kd_JPD',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_JPD',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viJPDPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_DlgLapJasaPelayananDokterSummary;
}

function dataGridLookUpBarangDlgLapJasaPelayananDokterSummary(JPD_kd_JPD){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/JPD/lap_JPDSummary/getGridLookUpBarang",
			params: {JPD_kd_JPD:JPD_kd_JPD},
			failure: function(o)
			{
				ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_DlgLapJasaPelayananDokterSummary.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_DlgLapJasaPelayananDokterSummary.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_DlgLapJasaPelayananDokterSummary.add(recs);
					
					
					
					gridPanelLookUpBarang_DlgLapJasaPelayananDokterSummary.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapRekapMakananPerRuang_GiziReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function ShowPesanWarningLapRekapMakananPerRuang_GiziReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
