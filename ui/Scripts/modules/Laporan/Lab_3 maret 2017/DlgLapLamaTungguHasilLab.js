
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLamaTungguHasilLab;
var selectNamaLamaTungguHasilLab;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLamaTungguHasilLab;
var varLapLamaTungguHasilLab= ShowFormLapLamaTungguHasilLab();
var selectSetUmum;
var selectSetkelpas;

function ShowFormLapLamaTungguHasilLab()
{
    frmDlgLamaTungguHasilLab= fnDlgLamaTungguHasilLab();
    frmDlgLamaTungguHasilLab.show();
};

function fnDlgLamaTungguHasilLab()
{
    var winLamaTungguHasilLabReport = new Ext.Window
    (
        {
            id: 'winLamaTungguHasilLabReport',
            title: 'Laporan Lama Tunggu Hasil Laboratorium',
            closeAction: 'destroy',
            width:400,
            height: 170,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLamaTungguHasilLab()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLamaTungguHasilLabReport;
};


function ItemDlgLamaTungguHasilLab()
{
    var PnlLapLamaTungguHasilLab = new Ext.Panel
    (
        {
            id: 'PnlLapLamaTungguHasilLab',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLamaTungguHasilLab_Atas(),
                getItemLapLamaTungguHasilLab_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapLamaTungguHasilLab',
                            handler: function()
                            {
                               if (ValidasiReportLamaTungguHasilLab() === 1)
                               {
                                        //var tmppilihan = getKodeReportLamaTungguHasilLab();
                                        var criteria = GetCriteriaLamaTungguHasilLab();
                                       
										loadMask.show();
                                        loadlaporanRadLab('0', 'LapLamaTungguHasilLab', criteria, function(){
											frmDlgLamaTungguHasilLab.close();
											loadMask.hide();
										});
								};
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapLamaTungguHasilLab',
                            handler: function()
                            {
                                    frmDlgLamaTungguHasilLab.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapLamaTungguHasilLab;
};

function GetCriteriaLamaTungguHasilLab()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalFilterLapRegisLab').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalFilterLapRegisLab').getValue();
	}
	if (Ext.get('dtpTglAkhirFilterLapRegisLab').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterLapRegisLab').getValue();
	}
	//[[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]   
	if (Ext.getCmp('Shift_All_LamaTungguHasilLab').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_LamaTungguHasilLab').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_LamaTungguHasilLab').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_LamaTungguHasilLab').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	}
	return strKriteria;
};

function getKodeReportLamaTungguHasilLab()
{   var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLamaTungguHasilLab').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLamaTungguHasilLab').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLamaTungguHasilLab()
{
    var x=1;
   /*  if(Ext.get('dtpTglAwalFilterLapRegisLab').dom.value > Ext.get('dtpTglAkhirFilterLapRegisLab').dom.value)
    {
        ShowPesanWarningLamaTungguHasilLabReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    } */
	if(Ext.getCmp('Shift_All_LamaTungguHasilLab').getValue() === false && Ext.getCmp('Shift_1_LamaTungguHasilLab').getValue() === false && Ext.getCmp('Shift_2_LamaTungguHasilLab').getValue() === false && Ext.getCmp('Shift_3_LamaTungguHasilLab').getValue() === false){
		ShowPesanWarningLamaTungguHasilLabReport(nmRequesterRequest,'Laporan Regis Laboratorium');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningLamaTungguHasilLabReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLamaTungguHasilLab_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 50,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterLapRegisLab',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 10,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterLapRegisLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            }
                
            ]
        }]
    };
    return items;
};

function getItemLapLamaTungguHasilLab_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {
										boxLabel: 'Semua',
										name: 'Shift_All_LamaTungguHasilLab',
										id : 'Shift_All_LamaTungguHasilLab',
										handler: function (field, value) {
											if (value === true){
												Ext.getCmp('Shift_1_LamaTungguHasilLab').setValue(true);
												Ext.getCmp('Shift_2_LamaTungguHasilLab').setValue(true);
												Ext.getCmp('Shift_3_LamaTungguHasilLab').setValue(true);
												Ext.getCmp('Shift_1_LamaTungguHasilLab').disable();
												Ext.getCmp('Shift_2_LamaTungguHasilLab').disable();
												Ext.getCmp('Shift_3_LamaTungguHasilLab').disable();
											}else{
													Ext.getCmp('Shift_1_LamaTungguHasilLab').setValue(false);
													Ext.getCmp('Shift_2_LamaTungguHasilLab').setValue(false);
													Ext.getCmp('Shift_3_LamaTungguHasilLab').setValue(false);
													Ext.getCmp('Shift_1_LamaTungguHasilLab').enable();
													Ext.getCmp('Shift_2_LamaTungguHasilLab').enable();
													Ext.getCmp('Shift_3_LamaTungguHasilLab').enable();
												}
										}
									},
                                    {
										boxLabel: 'Shift 1',
										name: 'Shift_1_LamaTungguHasilLab',
										id : 'Shift_1_LamaTungguHasilLab'
									},
                                    {
										boxLabel: 'Shift 2',
										name: 'Shift_2_LamaTungguHasilLab',
										id : 'Shift_2_LamaTungguHasilLab'
									},
                                    {
										boxLabel: 'Shift 3',
										name: 'Shift_3_LamaTungguHasilLab',
										id : 'Shift_3_LamaTungguHasilLab'
									}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};


