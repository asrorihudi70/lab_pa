
var tigaharilalu = new Date().add(Date.DAY, -3);
var now = new Date();
var frmDlgDlgLapPasienPerusahaanDetailDetail;
var varDlgLapPasienPerusahaanDetailDetail= ShowFormDlgLapPasienPerusahaanDetailDetail();
var asc=0;
var IdRootKelompokBarang_pDlgLapPasienPerusahaanDetailDetail='1000000000';
var KdPasienPerusahaan_DlgLapPasienPerusahaanDetailDetail;
var Nama_DlgLapPasienPerusahaanDetailDetail;
var gridPanelLookUpBarang_DlgLapPasienPerusahaanDetailDetail;
var sendDataArray = [];
var dsvComboDokterDlgLapPasienPerusahaanDetailDetail;
var dsvComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailDetail;
var sendDataArray = [];
var namaCustomer;
function ShowFormDlgLapPasienPerusahaanDetailDetail()
{
    frmDlgDlgLapPasienPerusahaanDetailDetail= fnDlgDlgLapPasienPerusahaanDetailDetail();
    frmDlgDlgLapPasienPerusahaanDetailDetail.show();
	//GetStrpasienSetBarang_DlgLapPasienPerusahaanDetailDetail();
};

function fnDlgDlgLapPasienPerusahaanDetailDetail()
{
    var winDlgLapPasienPerusahaanDetailDetailReport = new Ext.Window
    (
        {
            id: 'winDlgLapPasienPerusahaanDetailDetailReport',
            title: 'Laporan Tagihan Perusahaan',
            closeAction: 'destroy',
            width: 405,
            height: 270,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgDlgLapPasienPerusahaanDetailDetail()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkDlgLapPasienPerusahaanDetailDetail',
					handler: function()
					{
						if (ValidasiReportPasienPerusahaanDetailLab() === 1)
                        {
							var params=GetParamPasienPerusahaanDetailLab() ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/lab/lap_pasienperusahaandetail/cetaklaporan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();
						}
						
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelDlgLapPasienPerusahaanDetailDetail',
					handler: function()
					{
						frmDlgDlgLapPasienPerusahaanDetailDetail.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winDlgLapPasienPerusahaanDetailDetailReport;
};
function GetParamPasienPerusahaanDetailLab()
{
	var getTglAwal = Ext.getCmp('dtpTglAwalPasienPerusahaanDetail').getValue();
	var getTglAkhir = Ext.getCmp('dtpTglAkhirPasienPerusahaanDetail').getValue();
	var besokTglAwal=getTglAwal.add(Date.DAY, +1);
	var besokTglAkhir=getTglAkhir.add(Date.DAY, +1);
	var asalPasien;
	var kodeShift="";
	var namaShift="";
	var cekBoxPilihan;
	console.log(besokTglAwal);
	console.log(besokTglAkhir);
	if (Ext.getCmp('rbKriteriaPerusahaanPasienPerusahaanDetail').getValue() === true){
		cekBoxPilihan=1;
	}
	else
	{
		cekBoxPilihan=2;
	}
	if (Ext.getCmp('cbo_SemuaPasienLapPasienPerusahaanDetailLab').getValue() === 'Semua Pasien'){
		asalPasien="'03','08','07'"
	}
	else if(Ext.getCmp('cbo_SemuaPasienLapPasienPerusahaanDetailLab').getValue() === 'Pasien Rawat Jalan')
	{
		asalPasien="'07'"
	}
	else if(Ext.getCmp('cbo_SemuaPasienLapPasienPerusahaanDetailLab').getValue() === 'Pasien Rawat Inap')
	{
		asalPasien="'03"
	}
	else if(Ext.getCmp('cbo_SemuaPasienLapPasienPerusahaanDetailLab').getValue() === 'Pasien Gawat Darurat')
	{
		asalPasien="'08'"
	}
	if (Ext.getCmp('CekSemuaShiftLapPasienPerusahaanDetail').getValue() === true)
	{
		namaShift = "Semua Shift";
		kodeShift="1,2,3";
	}else{
		namaShift= "Shift ";
		if (Ext.getCmp('CekShift1LapPasienPerusahaanDetail').getValue() === true)
		{
			namaShift += "1";
			kodeShift += "1";
		}
		if (Ext.getCmp('CekShift2LapPasienPerusahaanDetail').getValue() === true)
		{
			if (kodeShift!=='')
			{
				kodeShift += ",";
			}
			if (namaShift!=='')
			{
				namaShift += ",2";
			}
			else
			{
				namaShift += "2";
			}
			kodeShift += "2";
		}
		if (Ext.getCmp('CekShift3LapPasienPerusahaanDetail').getValue() === true)
		{
			if (kodeShift!=='')
			{
				kodeShift += ",";
			}
			if (namaShift!=='')
			{
				namaShift += ",3";
			}
			else
			{
				namaShift += "3";
			}
			kodeShift += "3";
		}
		if (namaShift==="Shift 1,2,3")
			{
				namaShift="Semua Shift";
			}
	}
	
	console.log(Ext.getCmp('cbo_SemuaAsuransiPerusahaanLapPasienPerusahaanDetailLab').getValue())
	console.log(namaShift);
	var	params =
	{
		kdKasir:asalPasien,
		tanggal:now,
		tglAwal:Ext.getCmp('dtpTglAwalPasienPerusahaanDetail').getValue(),
		tglAkhir:Ext.getCmp('dtpTglAkhirPasienPerusahaanDetail').getValue(),
		tglAwalBesok:besokTglAwal,
		tglAkhirBesok:besokTglAkhir,
		KdCust:Ext.getCmp('cbo_SemuaAsuransiPerusahaanLapPasienPerusahaanDetailLab').getValue(),
		nmCust:namaCustomer,
		kdShift:kodeShift,
		nmShift:namaShift,
		jenisCust:cekBoxPilihan
	}
   
    return params;
};
function ValidasiReportPasienPerusahaanDetailLab()
{
	var x=1;
	if(Ext.getCmp('cbo_SemuaPasienLapPasienPerusahaanDetailLab').getValue() === ''){
		ShowPesanWarningLapPasienPerusahaanDetail_LabReport('Asal Pasien Belum Dipilih','Laporan Pasien Perusahaan Detail');
        x=0;
	}
	if(Ext.getCmp('cbo_SemuaAsuransiPerusahaanLapPasienPerusahaanDetailLab').getValue() === ''){
		ShowPesanWarningLapPasienPerusahaanDetail_LabReport('Perusahaan Belum Dipilih','Laporan Pasien Perusahaan Detail');
        x=0;
	}
	if(Ext.getCmp('CekSemuaShiftLapPasienPerusahaanDetail').getValue() === false && Ext.getCmp('CekShift1LapPasienPerusahaanDetail').getValue() === false && Ext.getCmp('CekShift2LapPasienPerusahaanDetail').getValue() === false && Ext.getCmp('CekShift3LapPasienPerusahaanDetail').getValue() === false){
		ShowPesanWarningLapPasienPerusahaanDetail_LabReport('Shift Belum Dipilih','Laporan Pasien Perusahaan Detail');
        x=0;
	}

    return x;
}

function ItemDlgDlgLapPasienPerusahaanDetailDetail()
{
    var PnlDlgLapPasienPerusahaanDetailDetail = new Ext.Panel
    (
        {
            id: 'PnlDlgLapPasienPerusahaanDetailDetail',
            fileUpload: true,
            layout: 'form',
            height: 390,
            anchor: '100%',
            bodyStyle: 'padding:10px',
            border: true,
            items:
            [
				getItemDlgLapPasienPerusahaanDetailDetail_pasien(),
				getItemDlgLapPasienPerusahaanDetailDetail_Batas(),
				getItemDlgLapPasienPerusahaanDetailDetail_Shift(),
				//getItemDlgLapPasienPerusahaanDetailDetail_Batas(),
				//getItemDlgLapPasienPerusahaanDetailDetail_Shift(),
            ]
        }
    );

    return PnlDlgLapPasienPerusahaanDetailDetail;
};


function getItemDlgLapPasienPerusahaanDetailDetail_pasien()
{
	radioKriteria = new Ext.form.RadioGroup({
	x: 10,
	y: 5,
	id :'radiosKriteriaPasienPerusahaanDetail',
	columns  : 2,
	//rows : 2,
	items: 
	[
		{
			
			id:'rbKriteriaPerusahaanPasienPerusahaanDetail',
			name: 'rbLapPasienPerusahaan',
			boxLabel: 'Perusahaan',
			checked: true, 
			//inputValue: "1"
		},
		{
			id:'rbKriteriaAsuransiPasienPerusahaanDetail',
			name: 'rbLapPasienPerusahaan',
			boxLabel: 'Asuransi',
			//inputValue: "1"
		},
	],
	listeners: {
		change : function()
		{ 
			Ext.getCmp('cbo_SemuaAsuransiPerusahaanLapPasienPerusahaanDetailLab').setValue('');
			if (Ext.getCmp('rbKriteriaPerusahaanPasienPerusahaanDetail').getValue()===true)
			{
				dsComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailDetail(1);
			}
			else
			{
				dsComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailDetail(2);
			}
		}
	}
		
   });
   
    var items = {
        layout: 'column',
        border: false,
        items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				title : 'Kriteria Laporan',
				width:  370,
				height: 155,
				anchor: '100% 100%',
				items: [
					radioKriteria, 
					{
						x: 10,
						y: 35,
						xtype: 'label',
						text: 'Asal Pasien'
					}, 
					{
						x: 100,
						y: 35,
						xtype: 'label',
						text: ' : '
					},
					comboSemuaPasienListView_DlgLapPasienPerusahaanDetailDetail(),
					{
						x: 10,
						y: 65,
						xtype: 'label',
						text: 'Range Tanggal'
					}, 
					{
						x: 100,
						y: 65,
						xtype: 'label',
						text: ' : '
					},
					{
							x: 115,
							y: 65,
							xtype: 'datefield',
							id: 'dtpTglAwalPasienPerusahaanDetail',
							format: 'd/M/Y',
							value: now
						}
					,
					{
						x: 225,
						y: 65,
						xtype: 'label',
						text: ' s / d '
					},
					{
						x: 255,
						y: 65,
						xtype: 'datefield',
						id: 'dtpTglAkhirPasienPerusahaanDetail',
						format: 'd/M/Y',
						value: now
					},
					{
						x: 10,
						y: 95,
						xtype: 'label',
						text: 'Perusahaan'
					}, 
					{
						x: 100,
						y: 95,
						xtype: 'label',
						text: ' : '
					},
					comboSemuaAsuransiPerusahaanListView_DlgLapPasienPerusahaanDetailDetail(),
				]
			}
		]
    };
    return items;
};



function GetStrpasienSetBarang_DlgLapPasienPerusahaanDetailDetail()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and left(kd_PasienPerusahaan,1)<>'8'"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrpasienSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function comboSemuaPasienListView_DlgLapPasienPerusahaanDetailDetail()
{	
    var cbo_LapPasienPerusahaanDetailLabSemuaPasien = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 35,
            flex: 1,
			id: 'cbo_SemuaPasienLapPasienPerusahaanDetailLab',
			/*valueField: 'kd_Triwulan',
            //displayField: 'Triwulan',*/
			store: ['Semua Pasien','Pasien Rawat Jalan', 'Pasien Rawat Inap', 'Pasien Gawat Darurat'],
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:240,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					/*nomorCmbMutasiTriwulanInv=parseInt(c)+1;
					console.log(nomorCmbMutasiTriwulanInv);*/
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapPasienPerusahaanDetailLabSemuaPasien;
};

function comboSemuaAsuransiPerusahaanListView_DlgLapPasienPerusahaanDetailDetail()
{	
	var Field =['kd_customer','customer'];
    dsvComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailDetail = new WebApp.DataStore({fields: Field});
	dsComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailDetail(1);
    var cbo_LapPasienPerusahaanDetailLabSemuaAsuransiPerusahaan = new Ext.form.ComboBox
    (
        {
			x: 115,
			y: 95,
            flex: 1,
			id: 'cbo_SemuaAsuransiPerusahaanLapPasienPerusahaanDetailLab',
			valueField: 'kd_customer',
            displayField: 'customer',
			store: dsvComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailDetail,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:240,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					console.log(b)
					namaCustomer=b.data.customer;
					console.log(a)
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_LapPasienPerusahaanDetailLabSemuaAsuransiPerusahaan;
};
function dsComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailDetail(q)
{
	if (q===1)
	{
		dsvComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailDetail.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailPerusahaan',
                    //param : 'kd_tarif=1'
                }			
            }
        );  
	}
	else if (q===2)
	{
		dsvComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailDetail.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailAsuransi',
                    //param : 'kd_tarif=1'
                }			
            }
        );  
	}
    return dsvComboSemuaAsuransiPerusahaanDlgLapPasienPerusahaanDetailDetail;
}
function getItemDlgLapPasienPerusahaanDetailDetail_Shift()
{    
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  370,
				height: 30,
				anchor: '100% 100%',
				items: [
					{
					   xtype: 'checkbox',
					   id: 'CekSemuaShiftLapPasienPerusahaanDetail',
					   hideLabel:false,
					   boxLabel: 'Semua',
					   checked: true,
					   x: 10,
					   y: 5,
					   listeners: 
					   {
							check: function()
							{
								if(Ext.getCmp('CekSemuaShiftLapPasienPerusahaanDetail').getValue()===true)
								{
									Ext.getCmp('CekShift1LapPasienPerusahaanDetail').setValue(true);
									Ext.getCmp('CekShift1LapPasienPerusahaanDetail').disable(true);
									
									Ext.getCmp('CekShift2LapPasienPerusahaanDetail').setValue(true);
									Ext.getCmp('CekShift2LapPasienPerusahaanDetail').disable(true);
									
									Ext.getCmp('CekShift3LapPasienPerusahaanDetail').setValue(true)
									Ext.getCmp('CekShift3LapPasienPerusahaanDetail').disable(true);
								}
								else
								{
									Ext.getCmp('CekShift1LapPasienPerusahaanDetail').setValue(false);
									Ext.getCmp('CekShift1LapPasienPerusahaanDetail').enable(true);
									
									Ext.getCmp('CekShift2LapPasienPerusahaanDetail').setValue(false)
									Ext.getCmp('CekShift2LapPasienPerusahaanDetail').enable(true)
									
									Ext.getCmp('CekShift3LapPasienPerusahaanDetail').setValue(false)
									Ext.getCmp('CekShift3LapPasienPerusahaanDetail').enable(true)
								}	
							}
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift1LapPasienPerusahaanDetail',
					   hideLabel:false,
					   boxLabel: 'Shift 1',
					   checked: true,
					   disabled: true,
					   x: 65,
					   y: 5,
					   listeners: 
					   {
							check: function()
							{
								if(Ext.getCmp('CekShift1LapPasienPerusahaanDetail').getValue()===true)
								{
									sendDataArray.push("1");
									console.log(sendDataArray);
								}
								else
								{
									
								}
							}
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift2LapPasienPerusahaanDetail',
					   hideLabel:false,
					   boxLabel: 'Shift 2',
					   checked: true,
					   disabled: true,
					   x: 125,
					   y: 5,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
					{
					   xtype: 'checkbox',
					   id: 'CekShift3LapPasienPerusahaanDetail',
					   hideLabel:false,
					   boxLabel: 'Shift 3',
					   checked: true,
					   disabled: true,
					   x: 180,
					   y: 5,
					   listeners: 
					   {
							check: function()
							{
									
							 }
					   }
					},
				]
			}
		]
    };
    return items;
};

/*function getItemDlgLapPasienPerusahaanDetailDetail_Shift()
{
	var items = {
        layout: 'column',
        border: false,
        items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				anchor: '100% 100%',
				items: [
				{
				   xtype: 'checkbox',
				   id: 'CekSemuaShiftLapPasienPerusahaanDetail',
				   hideLabel:false,
				   //checked: false,
				   x: 10,
				   y: 5,
				   listeners: 
				   {
						check: function()
						{
								
						 }
				   }
				}
			]
		}]
	}
}*/
function getItemDlgLapPasienPerusahaanDetailDetail_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};



function gridDataViewBarang_DlgLapPasienPerusahaanDetailDetail()
{
    var FieldGrdBArang_DlgLapPasienPerusahaanDetailDetail = [];
	
    dsDataGrdBarang_DlgLapPasienPerusahaanDetailDetail= new WebApp.DataStore
	({
        fields: FieldGrdBArang_DlgLapPasienPerusahaanDetailDetail
    });
    
    gridPanelLookUpBarang_DlgLapPasienPerusahaanDetailDetail =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_DlgLapPasienPerusahaanDetailDetail,
		height:120,
		width:448,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_DlgLapPasienPerusahaanDetailDetail.getAt(row).data.kd_PasienPerusahaan);
					KdPasienPerusahaan_DlgLapPasienPerusahaanDetailDetail = dsDataGrdBarang_DlgLapPasienPerusahaanDetailDetail.getAt(row).data.kd_PasienPerusahaan;
					Nama_DlgLapPasienPerusahaanDetailDetail = dsDataGrdBarang_DlgLapPasienPerusahaanDetailDetail.getAt(row).data.nama_;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'PasienPerusahaan_kd_PasienPerusahaan',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_PasienPerusahaan',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viPasienPerusahaanPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_DlgLapPasienPerusahaanDetailDetail;
}

function dataGridLookUpBarangDlgLapPasienPerusahaanDetailDetail(PasienPerusahaan_kd_PasienPerusahaan){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/PasienPerusahaan/lap_PasienPerusahaanDetail/getGridLookUpBarang",
			params: {PasienPerusahaan_kd_PasienPerusahaan:PasienPerusahaan_kd_PasienPerusahaan},
			failure: function(o)
			{
				ShowPesanWarningLapPasienPerusahaanDetail_LabReport('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_DlgLapPasienPerusahaanDetailDetail.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_DlgLapPasienPerusahaanDetailDetail.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_DlgLapPasienPerusahaanDetailDetail.add(recs);
					
					
					
					gridPanelLookUpBarang_DlgLapPasienPerusahaanDetailDetail.getView().refresh();
				}
				else 
				{
					ShowPesanWarningLapPasienPerusahaanDetail_LabReport('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function ShowPesanWarningLapPasienPerusahaanDetail_LabReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
