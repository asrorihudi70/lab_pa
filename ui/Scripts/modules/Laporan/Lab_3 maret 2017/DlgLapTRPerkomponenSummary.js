
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapTRPerkomponenSummary;
var selectNamaLapTRPerkomponenSummary;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapTRPerkomponenSummary;
var varLapLapTRPerkomponenSummary= ShowFormLapLapTRPerkomponenSummary();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapTRPerkomponenSummary;
var customer;
var kdcustomer;
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var winLapTRPerkomponenSummaryReport;

function ShowFormLapLapTRPerkomponenSummary()
{
    frmDlgLapTRPerkomponenSummary= fnDlgLapTRPerkomponenSummary();
    frmDlgLapTRPerkomponenSummary.show();
	loadDataComboUserLapTRPerkomponenSummary();
};

function fnDlgLapTRPerkomponenSummary()
{
    winLapTRPerkomponenSummaryReport = new Ext.Window
    (
        {
            id: 'winLapTRPerkomponenSummaryReport',
            title: 'Laporan Transaksi Perkomponen Summary',
            closeAction: 'destroy',
            width: 370,
            height: 410,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapTRPerkomponenSummary()],
            listeners:
			{
				activate: function()
				{
					Ext.getCmp('cboPerseoranganLapTRPerkomponenSummary').hide();
					Ext.getCmp('cboAsuransiLapTRPerkomponenSummary').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenSummary').hide();
					Ext.getCmp('cboUmumLapTRPerkomponenSummary').show();
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'Ok',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapTRPerkomponenSummary',
						handler: function()
						{
							if (ValidasiReportLapTRPerkomponenSummary() === 1)
							{
								if (Ext.get('cboPilihanLapTRPerkomponenSummarykelompokPasien').getValue() === 'Semua')
								{
									tipe='Semua';
									customer='Semua';
									kdcustomer='Semua';
								} else if (Ext.get('cboPilihanLapTRPerkomponenSummarykelompokPasien').getValue() === 'Perseorangan'){
									customer=Ext.get('cboPerseoranganLapTRPerkomponenSummary').getValue();
									tipe='Perseorangan';
									kdcustomer=Ext.getCmp('cboPerseoranganLapTRPerkomponenSummary').getValue();
								} else if (Ext.get('cboPilihanLapTRPerkomponenSummarykelompokPasien').getValue() === 'Perusahaan'){
									customer=Ext.get('cboPerusahaanRequestEntryLapTRPerkomponenSummary').getValue();
									tipe='Perusahaan';
									kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenSummary').getValue();
								} else {
									customer=Ext.get('cboAsuransiLapTRPerkomponenSummary').getValue();
									tipe='Asuransi';
									kdcustomer=Ext.getCmp('cboAsuransiLapTRPerkomponenSummary').getValue();
								} 
								
								if(Ext.getCmp('radioasal').getValue() === true){
									periode='tanggal';
									tglAwal=Ext.getCmp('dtpTglAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
								} else{
									periode='bulan';
									tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilLab').getValue();
								}
								
								if (Ext.getCmp('Shift_All_LapTRPerkomponenSummary').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1_LapTRPerkomponenSummary').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2_LapTRPerkomponenSummary').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3_LapTRPerkomponenSummary').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
								
								var params={
									asal_pasien:Ext.get('cboPilihanLapTRPerkomponenSummary').getValue(),
									user:Ext.getCmp('cboUserRequestEntryLapTRPerkomponenSummary').getValue(),
									tipe:tipe,
									customer:customer,
									kdcustomer:kdcustomer,
									periode:periode,
									tglAwal:tglAwal,
									tglAkhir:tglAkhir,
									shift:shift,
									shift1:shift1,
									shift2:shift2,
									shift3:shift3,
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/lab/lap_laboratorium/cetakTRPerkomponenSummary");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapTRPerkomponenSummaryReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapTRPerkomponenSummary',
						handler: function()
						{
							winLapTRPerkomponenSummaryReport.close();
						}
					}
			]

        }
    );

    return winLapTRPerkomponenSummaryReport;
};


function ItemDlgLapTRPerkomponenSummary()
{
    var PnlLapLapTRPerkomponenSummary = new Ext.Panel
    (
        {
            id: 'PnlLapLapTRPerkomponenSummary',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapTRPerkomponenSummary_Atas(),
                getItemLapLapTRPerkomponenSummary_Batas(),
                getItemLapLapTRPerkomponenSummary_Bawah(),
                getItemLapLapTRPerkomponenSummary_Batas(),
                getItemLapLapTRPerkomponenSummary_Samping(),
            ]
        }
    );

    return PnlLapLapTRPerkomponenSummary;
};



function ValidasiReportLapTRPerkomponenSummary()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapTRPerkomponenSummary').getValue() === ''){
		ShowPesanWarningLapTRPerkomponenSummaryReport('Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboUserRequestEntryLapTRPerkomponenSummary').getValue() === ''){
		ShowPesanWarningLapTRPerkomponenSummaryReport('Operator Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboPilihanLapTRPerkomponenSummarykelompokPasien').getValue() === ''){
		ShowPesanWarningLapTRPerkomponenSummaryReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapTRPerkomponenSummary').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapTRPerkomponenSummary').getValue() === '' &&  Ext.get('cboAsuransiLapTRPerkomponenSummary').getValue() === '' && Ext.get('cboUmumRegisLab').getValue() === '' ){
		ShowPesanWarningLapTRPerkomponenSummaryReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
   /*  if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapTRPerkomponenSummaryReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    } */
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapTRPerkomponenSummaryReport('Periode belum dipilih','Warning');
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapTRPerkomponenSummary').getValue() === false && Ext.getCmp('Shift_1_LapTRPerkomponenSummary').getValue() === false && Ext.getCmp('Shift_2_LapTRPerkomponenSummary').getValue() === false && Ext.getCmp('Shift_3_LapTRPerkomponenSummary').getValue() === false){
		ShowPesanWarningLapTRPerkomponenSummaryReport('Shift Belum Dipilih','Warning');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapTRPerkomponenSummaryReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapTRPerkomponenSummary_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTRPerkomponenSummary(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboUserLapTRPerkomponenSummary(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTRPerkomponenSummaryKelompokPasien(),
                mComboPerseoranganLapTRPerkomponenSummary(),
                mComboAsuransiLapTRPerkomponenSummary(),
                mComboPerusahaanLapTRPerkomponenSummary(),
                mComboUmumLapTRPerkomponenSummary()
            ]
        }]
    };
    return items;
};


function getItemLapLapTRPerkomponenSummary_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapTRPerkomponenSummary_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  345,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
				
					{
						x: 40,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapTRPerkomponenSummary',
						id : 'Shift_All_LapTRPerkomponenSummary',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapTRPerkomponenSummary').setValue(true);
								Ext.getCmp('Shift_2_LapTRPerkomponenSummary').setValue(true);
								Ext.getCmp('Shift_3_LapTRPerkomponenSummary').setValue(true);
								Ext.getCmp('Shift_1_LapTRPerkomponenSummary').disable();
								Ext.getCmp('Shift_2_LapTRPerkomponenSummary').disable();
								Ext.getCmp('Shift_3_LapTRPerkomponenSummary').disable();
							}else{
								Ext.getCmp('Shift_1_LapTRPerkomponenSummary').setValue(false);
								Ext.getCmp('Shift_2_LapTRPerkomponenSummary').setValue(false);
								Ext.getCmp('Shift_3_LapTRPerkomponenSummary').setValue(false);
								Ext.getCmp('Shift_1_LapTRPerkomponenSummary').enable();
								Ext.getCmp('Shift_2_LapTRPerkomponenSummary').enable();
								Ext.getCmp('Shift_3_LapTRPerkomponenSummary').enable();
							}
						}
					},
					{
						x: 110,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapTRPerkomponenSummary',
						id : 'Shift_1_LapTRPerkomponenSummary'
					},
					{
						x: 180,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapTRPerkomponenSummary',
						id : 'Shift_2_LapTRPerkomponenSummary'
					},
					{
						x: 250,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapTRPerkomponenSummary',
						id : 'Shift_3_LapTRPerkomponenSummary'
					}
				]
			}
        ]
            
    };
    return items;
};

function getItemLapLapTRPerkomponenSummary_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilLab',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilLab',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilLab',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function loadDataComboUserLapTRPerkomponenSummary(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/lab/lap_laboratorium/getUser",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUserRequestEntryLapTRPerkomponenSummary.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_User_LapTRPerkomponenSummary.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_User_LapTRPerkomponenSummary.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboPilihanLapTRPerkomponenSummary()
{
    var cboPilihanLapTRPerkomponenSummary = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapTRPerkomponenSummary',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapTRPerkomponenSummary;
};

function mComboPilihanLapTRPerkomponenSummaryKelompokPasien()
{
    var cboPilihanLapTRPerkomponenSummarykelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanLapTRPerkomponenSummarykelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapTRPerkomponenSummarykelompokPasien;
};

function mComboPerseoranganLapTRPerkomponenSummary()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapTRPerkomponenSummary = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapTRPerkomponenSummary.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapTRPerkomponenSummary = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganLapTRPerkomponenSummary',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapTRPerkomponenSummary,
				/* new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ), */
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLapTRPerkomponenSummary;
};

function mComboUmumLapTRPerkomponenSummary()
{
    var cboUmumLapTRPerkomponenSummary = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumLapTRPerkomponenSummary',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumLapTRPerkomponenSummary;
};

function mComboPerusahaanLapTRPerkomponenSummary()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapTRPerkomponenSummary = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryLapTRPerkomponenSummary',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapTRPerkomponenSummary;
};

function mComboAsuransiLapTRPerkomponenSummary()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomerLab',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapTRPerkomponenSummary = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiLapTRPerkomponenSummary',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapTRPerkomponenSummary;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapTRPerkomponenSummary').show();
        Ext.getCmp('cboAsuransiLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenSummary').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenSummary').show();
        Ext.getCmp('cboUmumLapTRPerkomponenSummary').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenSummary').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenSummary').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenSummary').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboAsuransiLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRPerkomponenSummary').hide();
        Ext.getCmp('cboUmumLapTRPerkomponenSummary').show();
   }
}

function mComboUserLapTRPerkomponenSummary()
{
    var Field = ['kd_user','full_name'];
    ds_User_LapTRPerkomponenSummary = new WebApp.DataStore({fields: Field});
    cboUserRequestEntryLapTRPerkomponenSummary = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cboUserRequestEntryLapTRPerkomponenSummary',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:'Semua',
            store: ds_User_LapTRPerkomponenSummary,
            valueField: 'kd_user',
            displayField: 'full_name',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.kd_user;
				} 
			}
        }
    )

    return cboUserRequestEntryLapTRPerkomponenSummary;
};
