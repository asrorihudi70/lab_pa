var type_file=0;
var dsLABPABatalTransaksi;
var selectLABPABatalTransaksi;
var selectNamaLABPABatalTransaksi;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgLABPABatalTransaksi;
var varLapLABPABatalTransaksi= ShowFormLapLABPABatalTransaksi();
var dsLABPA;

function ShowFormLapLABPABatalTransaksi()
{
    frmDlgLABPABatalTransaksi= fnDlgLABPABatalTransaksi();
    frmDlgLABPABatalTransaksi.show();
};

function fnDlgLABPABatalTransaksi()
{
    var winLABPABatalTransaksiReport = new Ext.Window
    (
        {
            id: 'winLABPABatalTransaksiReport',
            title: 'Laporan Pembatalan Detail Transaksi',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLABPABatalTransaksi()]

        }
    );

    return winLABPABatalTransaksiReport;
};

function ItemDlgLABPABatalTransaksi()
{
    var PnlLapLABPABatalTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapLABPABatalTransaksi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapLABPABatalTransaksi_Periode(),
				//getItemLABPABatalTransaksi_Shift(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLABPAel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapLABPABatalTransaksi',
					handler: function()
					{
					   if (ValidasiReportLABPABatalTransaksi() === 1)
					   {
						
							var params={
												tglAwal:Ext.getCmp('dtpTglAwalLapLABPABatalTransaksi').getValue(),
												tglAkhir:Ext.getCmp('dtpTglAkhirLapLABPABatalTransaksi').getValue(),
												type_file:type_file
									} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/lab/lap_laboratorium_pa/cetak_laporan_batal_detail_transaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgLABPABatalTransaksi.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLapLABPABatalTransaksi').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapLABPABatalTransaksi').getValue()+'#aje#Laporan Batal Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionLABPA/cetakLABPABatalTransaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgLABPABatalTransaksi.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLABPAel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapLABPABatalTransaksi',
					handler: function()
					{
						frmDlgLABPABatalTransaksi.close();
					}
				}
			
			]
        }
    );

    return PnlLapLABPABatalTransaksi;
};


function ValidasiReportLABPABatalTransaksi()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapLABPABatalTransaksi').dom.value === '')
	{
		ShowPesanWarningLABPABatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapLABPABatalTransaksi').getValue() === false && Ext.getCmp('Shift_1_LapLABPABatalTransaksi').getValue() === false && Ext.getCmp('Shift_2_LapLABPABatalTransaksi').getValue() === false && Ext.getCmp('Shift_3_LapLABPABatalTransaksi').getValue() === false){
		ShowPesanWarningLABPABatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportLABPABatalTransaksi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapLABPABatalTransaksi').dom.value > Ext.get('dtpTglAkhirLapLABPABatalTransaksi').dom.value)
    {
        ShowPesanWarningLABPABatalTransaksiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningLABPABatalTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapLABPABatalTransaksi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                LABPAelWidth: 85,
                LABPAelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapLABPABatalTransaksi_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				LABPAelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLABPAel: 'Periode ',
						id: 'dtpTglAwalLapLABPABatalTransaksi',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					{
					   xtype: 'checkbox',
					   fieldLABPAel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLABPAel:false,
					   boxLABPAel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					}
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				LABPAelWidth:1,
			    items:
				[
					{
						xtype: 'LABPAel',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				LABPAelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapLABPABatalTransaksi',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


