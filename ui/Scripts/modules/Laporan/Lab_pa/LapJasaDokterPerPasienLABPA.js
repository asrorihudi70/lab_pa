var fields = [
	{name: 'KD_UNIT', mapping : 'KD_UNIT'},
	{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];

var fieldsPayment = [
	{name: 'KD_PAY', mapping : 'KD_PAY'},
	{name: 'URAIAN', mapping : 'URAIAN'}
];

var cols = [
	{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
	{ header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
];

var colsPayment = [
	{ id : 'KD_PAY', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'KD_PAY',hidden : true},
	{ header: "Kode Pay", width: 50, sortable: true, dataIndex: 'URAIAN'}
];
var colsCustomer = [
	{ id : 'kd_customer', header: "Kode Customer", width: 160, sortable: true, dataIndex: 'kd_customer',hidden : true},
	{ header: "Customer", width: 50, sortable: true, dataIndex: 'customer'}
];

var dsDokterDokterPerpasien;
var secondGridStoreLapJasaDokPerPasien;
var selectSetPilihanDokter;
var firstGridPayment;
var dataSource_payment;
var selectSetUnit;
var type_file=0;
var dsDokterPelayaranPerawat;
var secondGridPayment;
var secondGridStoreLapCustomer;
var kelpas=-1;
var secondGridStoreLapJasaDokPerPasienPaymentLapJasaDokPerPasien;
var DlgLapLABPAJasaDokPerPasien={
	vars:{
		comboSelect:null
	},
	coba:function(){
		var $this=this;
		$this.DateField.startDate
	},
	ArrayStore:{
		dokter:new Ext.data.ArrayStore({fields:[]})
	},
	CheckboxGroup:{
		shift:null,
		tindakan:null,
	},
	ComboBox:{
		kelPasien1:null,
		jenisLaporan:null,
		asalPasien:null,
		combo1:null,
		combo2:null,
		combo3:null,
		combo0:null,
		poliklinik:null,
		dokter:null
	},
	DataStore:{
		combo2:null,
		combo3:null,
		poliklinik:null,
		combo1:null,
		gridcustomer:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	comboOnSelect:function(val){
		var $this=this;
		$this.ComboBox.combo0.hide();
		$this.ComboBox.combo1.hide();
		$this.ComboBox.combo2.hide();
		$this.ComboBox.combo3.hide();
		if(val==-1){
			$this.ComboBox.combo0.show();
			
		}else if(val==0){
			$this.ComboBox.combo1.show();
		}else if(val==1){
			$this.ComboBox.combo2.show();
		}else if(val==2){
			$this.ComboBox.combo3.show();
		}
	},
	initPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		var sendDataCustomer    	= "";
		var sendDataArraypayment = [];
		var sendDataArrayCustomer = []; 

		secondGridStoreLapJasaDokPerPasienPaymentLapJasaDokPerPasien.each(function(record){
			var recordArraypay= [record.get("KD_PAY")];
			sendDataArraypayment.push(recordArraypay);
		});
		secondGridStoreLapCustomer.each(function(record){
			var recordArray   = [record.get("kd_customer")];
			sendDataArrayCustomer.push(recordArray);
			sendDataCustomer 	+= "'"+recordArray+"',";
		});
		if (sendDataArraypayment.length === 0){  
			this.messageBox('Peringatan','Isi cara membayar dengan drag and drop','WARNING');
			loadMask.hide();
		}else{
			params['start_date'] = timestimetodate($this.DateField.startDate.getValue());
			params['last_date']  = timestimetodate($this.DateField.endDate.getValue());
			params['pasien']     = $this.ComboBox.kelPasien1.getValue();
			params['asal_pasien'] = $this.ComboBox.asalPasien.getValue();
			params['kd_dokter'] = $this.GetCriteriaLABProfesi();
			params['type_file'] = type_file;
			params['kd_profesi'] =Ext.getCmp('IDcboPilihanLABJenisProfesi').getValue();
			var pasien=parseInt($this.ComboBox.kelPasien1.getValue());
			params['tmp_kd_customer'] 	= sendDataCustomer;
		
			params['tmp_payment'] 	= sendDataArraypayment;
			var shift  = $this.CheckboxGroup.shift.items.items;
			var shifta = false;
			var tindakan_stat=false;
			for(var i=0;i<shift.length ; i++){
				params['shift'+i]=shift[i].checked;
				if(shift[i].checked==true)shifta=true;
			}
			//if(shifta==false){
			
			
				var form = document.createElement("form");
				form.setAttribute("method", "post");
				form.setAttribute("target", "_blank");
				form.setAttribute("action", baseURL + "index.php/lab_pa/lap_JasaDokterPerPasienLABPA/cetak");
				var hiddenField = document.createElement("input");
				hiddenField.setAttribute("type", "hidden");
				hiddenField.setAttribute("name", "data");
				hiddenField.setAttribute("value", Ext.encode(params));
				form.appendChild(hiddenField);
				document.body.appendChild(form);
				form.submit();
				loadMask.hide();
			
				
			//}
		} 
		
	},
	messageBox:function(modul, str, icon){
		Ext.MessageBox.show
		(
			{
				title: modul,
				msg:str,
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.icon,
				width:300
			}
		);
	},
	getDokter:function(){
		var $this=this;
		$this.ComboBox.dokter = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.ArrayStore.dokter,
            width: 200,
            value:'Semua',
            valueField: 'kd_dokter',
            displayField: 'nama',
            listeners:{
            	'select': function(a, b, c){
                               
            	}
            }
        });		    
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gawat_darurat/lap_penerimaan/getDokter",
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.dokter.loadData([],false);
					for(var i=0,iLen=r.data.length; i<iLen ;i++){
						$this.ArrayStore.dokter.add(new $this.ArrayStore.dokter.recordType(r.data[i]));
					}
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
		return $this.ComboBox.dokter;
	},
	getCombo0:function(){
		var $this=this;
		$this.ComboBox.combo0 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: new Ext.data.ArrayStore({
				id: 0,
				fields:['Id','displayText'],
				data: [[1, 'Semua']]
			}),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			disabled:true,
			listeners:{
				'select': function(a,b,c){
					selectSetUmum=b.data.displayText ;
				}
			}
		});
		return $this.ComboBox.combo0;
	},
	getCombo1:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo1 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo1.load({
    		params:{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 ORDER BY CUSTOMER '
			}
		});
    	$this.ComboBox.combo1 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hidden:true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: $this.DataStore.combo1,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
			listeners:{
				'select': function(a,b,c){
				}
			}
		});
		return $this.ComboBox.combo1;
	},
	getCombo2:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo2 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo2.load({
		    params:{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER '
			}
		});
   		$this.ComboBox.combo2 = new Ext.form.ComboBox({
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    hidden:true,
		    store: $this.DataStore.combo2,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			value:'Semua',
			width:150,
		    listeners:{
			    'select': function(a,b,c){
				}
			}
		});
    	return $this.ComboBox.combo2;
	},
	firstGridCustomer : function(){
		var $this=this;
		
		var dataSource_customer;
		var Field_poli_viDaftar = ['kd_customer','customer'];
		
		$this.DataStore.gridcustomer         = new WebApp.DataStore({fields: Field_poli_viDaftar});
		$this.loadcustomer(kelpas);
		
        firstGrid_customer = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : $this.DataStore.gridcustomer,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 120,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Customer',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'kd_customer',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Customer',
                                                    dataIndex: 'customer',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
			listeners : {
				afterrender : function(comp) {
					var secondGridDropTargetEl = firstGrid_customer.getView().scroller.dom;
					var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroup',
							notifyDrop : function(ddSource, e, data){
									var records =  ddSource.dragData.selections;
									Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
									firstGrid_customer.store.add(records);
									firstGrid_customer.store.sort('kd_customer', 'ASC');
									return true
							}
					});
				}
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid_customer;
	},
	seconGridCustomer : function(){
		
		var secondGridCust;
		secondGridStoreLapCustomer = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGridCust = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroup',
					store            : secondGridStoreLapCustomer,
					columns          : colsCustomer,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 120,
					stripeRows       : true,
					autoExpandColumn : 'kd_customer',
					title            : 'Customer yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGridCust.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroup',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGridCust.store.add(records);
											secondGridCust.store.sort('kd_customer', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGridCust;
	},
	loadcustomer:function(param){
		var $this=this;
		Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/function_laporan_PTP/getCustomerLaporanPenunjang",
				params: {kelpas :param},
				failure: function(o){
					var cst = Ext.decode(o.responseText);
				},	    
				success: function(o) {
					firstGrid_customer.store.removeAll();
					var cst = Ext.decode(o.responseText);

					for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
						var recs    = [],recType =  $this.DataStore.gridcustomer.recordType;
						var o=cst['listData'][i];
				
						recs.push(new recType(o));
						$this.DataStore.gridcustomer.add(recs);
						console.log(o);
					}
				}
			});
	},
	firstGridPayment : function(){
		
		var Field_payment = ['KD_PAY','URAIAN'];
		dataSource_payment         = new WebApp.DataStore({fields: Field_payment});
		dataSource_payment.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_PAY',
					Sortdir: 'ASC',
					target:'ViewListPayment',
					param: " "
				}
			}
		);
            firstGridPayment = new Ext.grid.GridPanel({
				ddGroup          : 'secondGridDDGroupPayment',
				store            : dataSource_payment,
				autoScroll       : true,
				columnLines      : true,
				border           : true,
				enableDragDrop   : true,
				height           : 120,
				stripeRows       : true,
				trackMouseOver   : true,
				title            : 'Payment',
				anchor           : '100% 100%',
				plugins          : [new Ext.ux.grid.FilterRow()],
				colModel         : new Ext.grid.ColumnModel
				(
					[
					new Ext.grid.RowNumberer(),
						{
							id: 'colKD_pay',
							header: 'Kode Pay',
							dataIndex: 'KD_PAY',
							sortable: true,
							hidden : true
						},
						{
							id: 'colKD_uraian',
							header: 'Uraian',
							dataIndex: 'URAIAN',
							sortable: true,
							width: 50
						}
					]
				),   
				listeners : {
					afterrender : function(comp) {
						var secondGridDropTargetEl = firstGridPayment.getView().scroller.dom;
						var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
								ddGroup    : 'firstGridDDGroupPayment',
								notifyDrop : function(ddSource, e, data){
										var records =  ddSource.dragData.selections;
										Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
										firstGridPayment.store.add(records);
										firstGridPayment.store.sort('KD_PAY', 'ASC');
										return true
								}
						});
					}
	            },
	                viewConfig: 
	                    {
	                            forceFit: true
	                    }
        	});
        return firstGridPayment;
	},
	seconGridPayment : function(){
		//var secondGridStoreLapJasaDokPerPasienPaymentLapJasaDokPerPasien;
		
		secondGridStoreLapJasaDokPerPasienPaymentLapJasaDokPerPasien = new Ext.data.JsonStore({
            fields : fieldsPayment,
            root   : 'records'
        });

        // create the destination Grid
            secondGridPayment = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroupPayment',
					store            : secondGridStoreLapJasaDokPerPasienPaymentLapJasaDokPerPasien,
					columns          : colsPayment,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 120,
					stripeRows       : true,
					autoExpandColumn : 'KD_PAY',
					title            : 'Payment yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGridPayment.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroupPayment',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGridPayment.store.add(records);
											secondGridPayment.store.sort('KD_PAY', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGridPayment;
	},
	getCombo3:function(){
		var $this=this;
		var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    	$this.DataStore.combo3 = new WebApp.DataStore({fields: Field_poli_viDaftar});

		$this.DataStore.combo3.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER "
            }
        });
    	$this.ComboBox.combo3 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:150,
			hidden:true,
			store: $this.DataStore.combo3,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
//			value: 0,
			listeners:{
				'select': function(a,b,c){
//					selectSetAsuransi=b.data.KD_CUSTOMER ;
//					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		});
		return $this.ComboBox.combo3;
	},
	getComboDokterLab: function(kdUnit)
	{
		dsDokterDokterPerpasien.load
		({
			params:
			{
				Skip: 0,
				Take: 1000,
				Sort: 'kd_dokter',
				Sortdir: 'ASC',
				target: 'ViewDokterPenunjang',
				param: "kd_unit = '"+kdUnit+"'"
			}
		});
		return dsDokterDokterPerpasien;
	},
	Combo_SelectLABProfesi: function(combo)
	{
		var $this=this;
	   var value = combo;
		Ext.Ajax.request({
			url: baseURL + "index.php/main/functionLAB/getUnitDefault",
			params: {text:''},
			failure: function (o){
				loadMask.hide();
				ShowPesanErrorPenJasLab('Gagal mendapatkan data Unit default', 'Laboratorium');
			},
			success: function (o){
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					selectSetUnit=cst.kd_unit;
					tmpnama_unit=cst.nama_unit;
					//Ext.getCmp('cbounitRequestEntryLABRekapJasaPelayananDokter').setValue(tmpnama_unit);
				} else{
					ShowPesanErrorPenJasLab('Gagal mendapatkan data Unit default', 'Laboratorium');
				}
			}
		});
	   if(value === "Dokter")
	   {    
			$this.getComboDokterLab(selectSetUnit);
			Ext.getCmp('cboDokterLABRekapJasaDokterPerpasien').show();
			Ext.getCmp('cboDokterLABRekapJasaPelayananPerawatPerPasien').hide();
	   }
	   else if(value === "Perawat")
	   {    
			//mComboDokterLABRekapJasaPelayananPerawat();
			Ext.getCmp('cboDokterLABRekapJasaPelayananPerawatPerPasien').show();
			Ext.getCmp('cboDokterLABRekapJasaDokterPerpasien').hide();
			
	   }
	   else
	   {
		   $this.getComboDokterLab(selectSetUnit);
			Ext.getCmp('cboDokterLABRekapJasaDokterPerpasien').show();
			Ext.getCmp('cboDokterLABRekapJasaPelayananPerawatPerPasien').hide();
	   }
	},
	mComboLABJenisProfesi: function()
	{
		var $this=this;
		var cboPilihanLABJenisProfesi = new Ext.form.ComboBox
		(
				{
					id:'IDcboPilihanLABJenisProfesi',
					typeAhead: true,
					triggerAction: 'all',
					lazyRender:true,
					mode: 'local',
					selectOnFocus:true,
					forceSelection: true,
					emptyText:'Silahkan Pilih...',
					fieldLabel: 'Pendaftaran Per Shift ',
					width: 240,
					value:1,
					store: new Ext.data.ArrayStore
					(
						{
								id: 0,
								fields:
								[
										'Id',
										'displayText'
								],
						data: [[1, 'Dokter'], [2, 'Perawat']]
						}
					),
					valueField: 'Id',
					displayField: 'displayText',
					listeners:
					{
						'select': function(a,b,c)
						{
								selectSetPilihanProfesi=b.data.displayText;
								$this.Combo_SelectLABProfesi(b.data.displayText);
						}
					}
				}
		);
		return cboPilihanLABJenisProfesi;
	},
	
	mComboDokterLABJasaDokterPerpasien: function()
	{
		var $this=this;
		var Field = ['KD_DOKTER','NAMA'];
		Ext.Ajax.request({
			url: baseURL + "index.php/main/functionLAB/getUnitDefault",
			params: {text:''},
			failure: function (o){
				loadMask.hide();
				ShowPesanErrorPenJasLab('Gagal mendapatkan data Unit default', 'Laboratorium');
			},
			success: function (o){
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					$this.getComboDokterLab(cst.kd_unit);
					//Ext.getCmp('cbounitRequestEntryLABRekapJasaPelayananDokter').setValue(tmpnama_unit);
				} else{
					ShowPesanErrorPenJasLab('Gagal mendapatkan data Unit default', 'Laboratorium');
				}
			}
		});
		dsDokterDokterPerpasien = new WebApp.DataStore({fields: Field});
		
		
		var cboPilihanLABRekapJasaDokterPerpasien = new Ext.form.ComboBox
		(
				{
			
					id:'cboDokterLABRekapJasaDokterPerpasien',
					typeAhead: true,
					triggerAction: 'all',
					lazyRender:true,
					mode: 'local',
					selectOnFocus:true,
					forceSelection: true,
					emptyText:'Silahkan Pilih...',
					width:240,
					store: dsDokterDokterPerpasien,
					valueField: 'KD_DOKTER',
					displayField: 'NAMA',
					value:'Semua',
					listeners:
					{
							'select': function(a,b,c)
							{
									selectSetPilihanDokter=b.data.displayText ;
							}
					}
				}
		);
		return cboPilihanLABRekapJasaDokterPerpasien;
	},
	mComboDokterLABJasaPerawatPerPasien: function()
	{
		var Field = ['KD_DOKTER','NAMA'];
		dsDokterPelayaranPerawat = new WebApp.DataStore({fields: Field});
		dsDokterPelayaranPerawat.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					//Sort: 'DEPT_ID',
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViewComboDokterLaporan',
					param: "WHERE dokter.jenis_dokter='1'"
				}
			}
		);
		var cboPilihanLABRekapJasaPelayananDokter = new Ext.form.ComboBox
		(
				{
					id:'cboDokterLABRekapJasaPelayananPerawatPerPasien',
					typeAhead: true,
					triggerAction: 'all',
					lazyRender:true,
					mode: 'local',
					selectOnFocus:true,
					forceSelection: true,
					emptyText:'Silahkan Pilih...',
					width:240,
					store: dsDokterPelayaranPerawat,
					valueField: 'KD_DOKTER',
					displayField: 'NAMA',
					value:'Semua',
					hidden:true,
					listeners:
					{
							'select': function(a,b,c)
							{
									selectSetPilihanDokter=b.data.displayText ;
							}
					}
				}
		);
		return cboPilihanLABRekapJasaPelayananDokter;
	},
	GetCriteriaLABProfesi: function()
	{
		var strKriteria = '';
		
		if (Ext.getCmp('IDcboPilihanLABJenisProfesi').getValue() !== '')
		{
			if (Ext.get('IDcboPilihanLABJenisProfesi').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
			else if (Ext.get('IDcboPilihanLABJenisProfesi').getValue() === 'Dokter'){ strKriteria = Ext.getCmp('cboDokterLABRekapJasaDokterPerpasien').getValue(); } 
			else if (Ext.get('IDcboPilihanLABJenisProfesi').getValue() === 'Perawat'){ strKriteria = Ext.getCmp('cboDokterLABRekapJasaPelayananPerawatPerPasien').getValue(); } 
		}else{
			strKriteria = 'Semua';
		}

		
		return strKriteria;
	},
	init:function(){
		var $this=this;
		$this.Window.main=new Ext.Window({
			width: 401,
			height:710,
			modal:true,
			title:'Laporan Jada Dokter Per Pasien LAB',
			layout:'fit',
			items:[
				new Ext.Panel({
					bodyStyle:'padding: 6px',
					layout:'form',
					border:false,
           			autoScroll: true,
					fbar:[
						new Ext.Button({
							text:'Ok',
							handler:function(){
								$this.initPrint()
							}
						}),
						new Ext.Button({
							text:'Batal',
							handler:function(){
								$this.Window.main.close();
							}
						})
					],
					items:[
						/* {
							// xtype : 'fieldset',
							// title : 'Kelompok pasien',
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							//height: 60,
							items:[
									{
										xtype : 'fieldset',
										title : 'Jenis Laporan Penerimaan',
										layout: 'column',
										border: true,
										bodyStyle:'padding: 2px 0px  7px 15px',
										width:390,
										height: 67,
										items:[
											$this.ComboBox.jenisLaporan=new Ext.form.ComboBox({
												triggerAction: 'all',
												lazyRender:true,
												mode: 'local',
												width: 160,
												selectOnFocus:true,
												forceSelection: true,
												emptyText:'Silahkan Pilih...',
												//fieldLabel: 'Pendaftaran Per Shift ',
												store: new Ext.data.ArrayStore({
													id: 0,
													fields:[
															'Id',
															'displayText'
													],
													data: [[0, 'Per Pasien'],[1, 'Per Jenis Penerimaan'], [2, 'Tunai Per Komponen Detail'], [3, 'Tunai Per Komponen Summary']]
												}),
												valueField: 'Id',
												displayField: 'displayText',
												value:0,
												listeners:{
													'select': function(a,b,c){
															
													}
												}
											}),
											
										]
									}
							]
						}, */
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:50,
							items:[
								{
									xtype : 'fieldset',
									title : 'Unit Asal Pasien',
									layout: 'column',
									width : 390,
									bodyStyle:'padding: 2px 0px  7px 15px',
									border: true,
									items:[
										$this.ComboBox.asalPasien=new Ext.form.ComboBox({
												triggerAction: 'all',
												lazyRender:true,
												mode: 'local',
												width: 150,
												selectOnFocus:true,
												forceSelection: true,
												emptyText:'Silahkan Pilih...',
												fieldLabel: 'Pendaftaran Per Shift ',
												store: new Ext.data.ArrayStore({
													id: 0,
													fields:[
															'Id',
															'displayText'
													],
													data: [[0, 'Semua'],[1, 'RWJ'],[2, 'RWI'], [3, 'IGD'],[4, 'Kunjungan Langsung']]
												}),
												valueField: 'Id',
												displayField: 'displayText',
												value:'Semua',
												listeners:{
													'select': function(a,b,c){
															$this.vars.comboSelect=b.data.displayText;
															$this.comboOnSelect(b.data.Id);
													}
												}
											}),
										
									]
								}
							]
						},
						
						{
							// xtype : 'fieldset',
							// title : 'Kelompok pasien',
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							//height: 60,
							items:[
									{
										xtype : 'fieldset',
										title : 'Kelompok pasien',
										layout: 'column',
										border: true,
										bodyStyle:'padding: 2px 0px  7px 15px',
										width:390,
										height: 170,
										items:[
										{
											layout: 'column',
											border: false,
											bodyStyle:'padding: 0px 0px  5px 0px',
											width:300,
											items:[
														$this.ComboBox.kelPasien1=new Ext.form.ComboBox({
														triggerAction: 'all',
														lazyRender:true,
														mode: 'local',
														width: 300,
														selectOnFocus:true,
														hidden:true,
														forceSelection: true,
														emptyText:'Silahkan Pilih...',
														fieldLabel: 'Pendaftaran Per Shift ',
														store: new Ext.data.ArrayStore({
															id: 0,
															fields:[
																	'Id',
																	'displayText'
															],
															data: [[0, 'Semua'],[1, 'Perseorangan'],[2, 'Perusahaan'], [3, 'Asuransi']]
														}),
														valueField: 'Id',
														displayField: 'displayText',
														value:'Semua',
														listeners:{
															'select': function(a,b,c){
																	$this.vars.comboSelect=b.data.displayText;
																	$this.comboOnSelect(b.data.Id);
															}
														}
													}),
											]
											
											/* {
												xtype:'displayfield',
												width: 10,
												value:'&nbsp;'
											}, */
											/* $this.getCombo0(),
											$this.getCombo1(),
											$this.getCombo2(),
											$this.getCombo3() */
										},{
												
												layout: 'column',
												border: false,
												width:340,
												items:[
													//grid customer
														{
															border:false,
															columnWidth:.47,
															//width :170,
															bodyStyle:'margin-top: 2px',
															items:[
																$this.firstGridCustomer(),
															]
														},
														{
															border:false,
															columnWidth :.03,
															bodyStyle:'margin-top: 2px',
														},
														{
															border:false,
															columnWidth:.47,
															//width :170,
															bodyStyle:'margin-top: 2px; align:right;',
															items:[
																$this.seconGridCustomer(),
															]
														}
												]
											}
										]
									}
							]
						},
						{
							// xtype : 'fieldset',
							// title : 'Kelompok pasien',
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							//height: 60,
							items:[
									
									{
										xtype : 'fieldset',
										title : 'Shift',
										layout:'column',
										border: true,
										bodyStyle:'padding: 2px 0px  7px 15px',
										width : 390,
										//height: 60,
										items:[
											$this.CheckboxGroup.shift=new Ext.form.CheckboxGroup({
												xtype: 'checkboxgroup',
												items: [
													{boxLabel: 'Semua',checked:true,name: 'Shift_All_LabRegis',id : 'Shift_All_LabRegis',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_LabRegis').setValue(true);Ext.getCmp('Shift_2_LabRegis').setValue(true);Ext.getCmp('Shift_3_LabRegis').setValue(true);Ext.getCmp('Shift_1_LabRegis').disable();Ext.getCmp('Shift_2_LabRegis').disable();Ext.getCmp('Shift_3_LabRegis').disable();}else{Ext.getCmp('Shift_1_LabRegis').setValue(false);Ext.getCmp('Shift_2_LabRegis').setValue(false);Ext.getCmp('Shift_3_LabRegis').setValue(false);Ext.getCmp('Shift_1_LabRegis').enable();Ext.getCmp('Shift_2_LabRegis').enable();Ext.getCmp('Shift_3_LabRegis').enable();}}},
													{boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1_LabRegis',id : 'Shift_1_LabRegis'},
													{boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2_LabRegis',id : 'Shift_2_LabRegis'},
													{boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3_LabRegis',id : 'Shift_3_LabRegis'}
												]
											})
										]
									}
							]
						},
						
						{
							xtype : 'fieldset',
							title : 'Payment',
							layout:'column',
							border:true,
							height: 160,
							bodyStyle:'margin-top: 2px',
							items:[
								{
									border:false,
									width :170,
									bodyStyle:'margin-top: 2px',
									items:[
										$this.firstGridPayment(),
									]
								},
								{
									border:false,
									width :10,
									bodyStyle:'margin-top: 2px',
								},
								{
									border:false,
									width :170,
									bodyStyle:'margin-top: 2px; align:right;',
									items:[
										$this.seconGridPayment(),
									]
								}
							]
						},
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:20,
							items:[
								{
								   xtype: 'checkbox',
								   id: 'CekLapPilihSemuaPaymentLapLAB',
								   hideLabel:false,
								   boxLabel: 'Pilih Semua',
								   checked: false,
								   listeners: 
								   {
										check: function()
										{
										   if(Ext.getCmp('CekLapPilihSemuaPaymentLapLAB').getValue()===true)
											{
												 firstGridPayment.getSelectionModel().selectAll();
											}
											else
											{
												firstGridPayment.getSelectionModel().clearSelections();
											}
										}
								   }
									
								},{
									border:false,
									width :130,
									bodyStyle:'margin-top: 2px',
								},{
									xtype:'button',
									text: 'Hapus Payment',
									//iconCls: 'refresh',
									anchor: '25%',
									hideLabel: false,
									handler: function(sm, row, rec)
									{
										secondGridPayment.store.removeAll();
										dataSource_payment.load
										(
											{
												params:
												{
													Skip: 0,
													Take: 1000,
													Sort: 'KD_PAY',
													Sortdir: 'ASC',
													target:'ViewListPayment',
													param: " "
												}
											}
										);
									}
									
								}]
						},
						
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:70,
							items:[
								{
									xtype : 'fieldset',
									title : 'Profesi',
									layout: 'column',
									width : 390,
									bodyStyle:'padding: 2px 0px  7px 15px',
									border: true,
									items:[
										$this.mComboLABJenisProfesi(),
										$this.mComboDokterLABJasaDokterPerpasien(),
										$this.mComboDokterLABJasaPerawatPerPasien(),
									]
								}
							]
						},
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:50,
							items:[
								{
									xtype : 'fieldset',
									title : 'Periode',
									layout: 'column',
									width : 390,
									bodyStyle:'padding: 2px 0px  7px 15px',
									border: true,
									items:[
										
										$this.DateField.startDate=new Ext.form.DateField({
											value: new Date(),
											format:'d/M/Y'
										}),
										{
											xtype:'displayfield',
											width: 30,
											value:'&nbsp;s/d&nbsp;'
										},
										$this.DateField.endDate=new Ext.form.DateField({
											value: new Date(),
											format:'d/M/Y'
										})
									]
								}
							]
						},
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:50,
							items:[
								{
									xtype: 'checkbox',
									   id: 'CekLapPilihTypeExcel',
									   hideLabel:false,
									   boxLabel: 'Excel',
									   checked: false,
									   listeners: 
									   {
											check: function()
											{
											   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
												{
													type_file=1;
												}
												else
												{
													type_file=0;
												}
											}
									   }
								}
							]
						   
						}
					]
				})
			]
		}).show();
	}
};
DlgLapLABPAJasaDokPerPasien.init();
