
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsTRLab;
var selectNamaTRLab;
var now = new Date();
var selectSetPerseorangan;
var frmDlgTRLab;
var varLapTRLab= ShowFormLapTRLab();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;

function ShowFormLapTRLab()
{
    frmDlgTRLab= fnDlgTRLab();
    frmDlgTRLab.show();
};

function fnDlgTRLab()
{
    var winTRLabReport = new Ext.Window
    (
        {
            id: 'winTRLabReport',
            title: 'Laporan Transaksi Laboratorium',
            closeAction: 'destroy',
            width: 370,
            height: 410,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgTRLab()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganTRLab').hide();
                Ext.getCmp('cboAsuransiTRLab').hide();
                Ext.getCmp('cboPerusahaanRequestEntryTRLab').hide();
                Ext.getCmp('cboUmumTRLab').show();
            }
        }

        }
    );

    return winTRLabReport;
};


function ItemDlgTRLab()
{
    var PnlLapTRLab = new Ext.Panel
    (
        {
            id: 'PnlLapTRLab',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapTRLab_Atas(),
                getItemLapTRLab_Batas(),
                getItemLapTRLab_Bawah(),
                getItemLapTRLab_Batas(),
                getItemLapTRLab_Samping(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapTRLab',
                            handler: function()
                            {
                                if (ValidasiReportTRLab() === 1)
                                {
                                        //var tmppilihan = getKodeReportTRLab();
                                       /*  var criteria = GetCriteriaTRLab();
                                        loadMask.show();
                                        loadlaporanRadLab('0', 'LapTransaksiLab', criteria, function(){
											frmDlgTRLab.close();
											loadMask.hide();
										}); */
										
										if (Ext.get('cboPilihanTRLabkelompokPasien').getValue() === 'Semua')
										{
											tipe='Semua';
											customer='Semua';
											kdcustomer='Semua';
										} else if (Ext.get('cboPilihanTRLabkelompokPasien').getValue() === 'Perseorangan'){
											customer=Ext.get('cboPerseoranganTRLab').getValue();
											tipe='Perseorangan';
											kdcustomer=Ext.getCmp('cboPerseoranganTRLab').getValue();
										} else if (Ext.get('cboPilihanTRLabkelompokPasien').getValue() === 'Perusahaan'){
											customer=Ext.get('cboPerusahaanRequestEntryTRLab').getValue();
											tipe='Perusahaan';
											kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryTRLab').getValue();
										} else {
											customer=Ext.get('cboAsuransiTRLab').getValue();
											tipe='Asuransi';
											kdcustomer=Ext.getCmp('cboAsuransiTRLab').getValue();
										} 
										
										if(Ext.getCmp('radioasal').getValue() === true){
											periode='tanggal';
											tglAwal=Ext.getCmp('dtpTglAwalFilterHasilLab').getValue();
											tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
										} else{
											periode='bulan';
											tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilLab').getValue();
											tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilLab').getValue();
										}
										
										if (Ext.getCmp('Shift_All_TRLab').getValue() === true){
											shift='All';
											shift1='false';
											shift2='false';
											shift3='false';
										}else{
											shift='';
											if (Ext.getCmp('Shift_1_TRLab').getValue() === true){
												shift1='true';
											} else{
												shift1='false';
											}
											if (Ext.getCmp('Shift_2_TRLab').getValue() === true){
												shift2='true';
											}else{
												shift2='false';
											}
											if (Ext.getCmp('Shift_3_TRLab').getValue() === true){
												shift3='true';
											}else{
												shift3='false';
											}
										}
										
										var params={
											asal_pasien:Ext.get('cboPilihanTRLab').getValue(),
											user:Ext.getCmp('cboUserRequestEntryTRLab').getValue(),
											tipe:tipe,
											customer:customer,
											kdcustomer:kdcustomer,
											periode:periode,
											tglAwal:tglAwal,
											tglAkhir:tglAkhir,
											shift:shift,
											shift1:shift1,
											shift2:shift2,
											shift3:shift3,
										} ;
										var form = document.createElement("form");
										form.setAttribute("method", "post");
										form.setAttribute("target", "_blank");
										form.setAttribute("action", baseURL + "index.php/lab_pa/lap_laboratorium_pa/LapTransaksiLab");
										var hiddenField = document.createElement("input");
										hiddenField.setAttribute("type", "hidden");
										hiddenField.setAttribute("name", "data");
										hiddenField.setAttribute("value", Ext.encode(params));
										form.appendChild(hiddenField);
										document.body.appendChild(form);
										form.submit();	
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapTRLab',
                            handler: function()
                            {
                                    frmDlgTRLab.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapTRLab;
};

function ValidasiReportTRLab()
{
	var x=1;
	if(Ext.getCmp('cboPilihanTRLab').getValue() === ''){
		ShowPesanWarningTRLabReport('Pasien Belum Dipilih','Laporan Transaksi Laboratorium');
        x=0;
	}
	if(Ext.getCmp('cboUserRequestEntryTRLab').getValue() === ''){
		ShowPesanWarningTRLabReport('Operator Belum Dipilih','Laporan Transaksi Laboratorium');
        x=0;
	}
	if(Ext.getCmp('cboPilihanTRLabkelompokPasien').getValue() === ''){
		ShowPesanWarningTRLabReport('Kelompok Pasien Belum Dipilih','Laporan Transaksi Laboratorium');
        x=0;
	}
	if(Ext.get('cboPerseoranganTRLab').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryTRLab').getValue() === '' &&  Ext.get('cboAsuransiTRLab').getValue() === '' && Ext.get('cboUmumRegisLab').getValue() === '' ){
		ShowPesanWarningTRLabReport('Sub Kelompok Pasien Belum Dipilih','Laporan Transaksi Laboratorium');
        x=0;
	}
    if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningTRLabReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Laporan Transaksi Laboratorium');
        x=0;
    }
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningTRLabReport('Periode belum dipilih','Laporan Transaksi Laboratorium');
        x=0;
    }
	if(Ext.getCmp('Shift_All_TRLab').getValue() === false && Ext.getCmp('Shift_1_TRLab').getValue() === false && Ext.getCmp('Shift_2_TRLab').getValue() === false && Ext.getCmp('Shift_3_TRLab').getValue() === false){
		ShowPesanWarningTRLabReport('Shift Belum Dipilih','Laporan Transaksi Laboratorium');
        x=0;
	}

    return x;
};

function ShowPesanWarningTRLabReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapTRLab_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanTRLab(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboUserTRLab(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanTRLabKelompokPasien(),
                mComboPerseoranganTRLab(),
                mComboAsuransiTRLab(),
                mComboPerusahaanTRLab(),
                mComboUmumTRLab()
            ]
        }]
    };
    return items;
};


function getItemLapTRLab_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapTRLab_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 50,
            anchor: '100% 100%',
            defaultType: 'checkbox',
            items: [
            
            {x: 40,y: 10,boxLabel: 'Semua',name: 'Shift_All_TRLab',id : 'Shift_All_TRLab',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_TRLab').setValue(true);Ext.getCmp('Shift_2_TRLab').setValue(true);Ext.getCmp('Shift_3_TRLab').setValue(true);Ext.getCmp('Shift_1_TRLab').disable();Ext.getCmp('Shift_2_TRLab').disable();Ext.getCmp('Shift_3_TRLab').disable();}else{Ext.getCmp('Shift_1_TRLab').setValue(false);Ext.getCmp('Shift_2_TRLab').setValue(false);Ext.getCmp('Shift_3_TRLab').setValue(false);Ext.getCmp('Shift_1_TRLab').enable();Ext.getCmp('Shift_2_TRLab').enable();Ext.getCmp('Shift_3_TRLab').enable();}}},
            {x: 110,y: 10,boxLabel: 'Shift 1',name: 'Shift_1_TRLab',id : 'Shift_1_TRLab'},
            {x: 180,y: 10,boxLabel: 'Shift 2',name: 'Shift_2_TRLab',id : 'Shift_2_TRLab'},
            {x: 250,y: 10,boxLabel: 'Shift 3',name: 'Shift_3_TRLab',id : 'Shift_3_TRLab'}
            ]
            }
        ]
            
    };
    return items;
};

function getItemLapTRLab_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilLab',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilLab',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilLab',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function mComboPilihanTRLab()
{
    var cboPilihanTRLab = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanTRLab',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanTRLab;
};

function mComboPilihanTRLabKelompokPasien()
{
    var cboPilihanTRLabkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanTRLabkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanTRLabkelompokPasien;
};

function mComboPerseoranganTRLab()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganTRLab = new WebApp.DataStore({fields: Field});
    dsPerseoranganTRLab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganTRLab = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganTRLab',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganTRLab,
				/* new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ), */
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganTRLab;
};

function mComboUmumTRLab()
{
    var cboUmumTRLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumTRLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumTRLab;
};

function mComboPerusahaanTRLab()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboCustomerLab',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryTRLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryTRLab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryTRLab;
};

function mComboAsuransiTRLab()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomerLab',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiTRLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiTRLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiTRLab;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganTRLab').show();
        Ext.getCmp('cboAsuransiTRLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRLab').hide();
        Ext.getCmp('cboUmumTRLab').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganTRLab').hide();
        Ext.getCmp('cboAsuransiTRLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRLab').show();
        Ext.getCmp('cboUmumTRLab').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganTRLab').hide();
        Ext.getCmp('cboAsuransiTRLab').show();
        Ext.getCmp('cboPerusahaanRequestEntryTRLab').hide();
        Ext.getCmp('cboUmumTRLab').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganTRLab').hide();
        Ext.getCmp('cboAsuransiTRLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRLab').hide();
        Ext.getCmp('cboUmumTRLab').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganTRLab').hide();
        Ext.getCmp('cboAsuransiTRLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRLab').hide();
        Ext.getCmp('cboUmumTRLab').show();
   }
}

function mComboUserTRLab()
{
    var Field = ['KD_USER','FULL_NAME'];
    ds_User_viDaftar = new WebApp.DataStore({fields: Field});

	ds_User_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: '',
                    Sortdir: '',
                    target:'ViewComboUser',
                    param: ""
                }
            }
        );

    var cboUserRequestEntryTRLab = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cboUserRequestEntryTRLab',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:selectSetuser,
            store: ds_User_viDaftar,
            valueField: 'KD_USER',
            displayField: 'FULL_NAME',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.KD_USER;
				} 
			}
        }
    )

    return cboUserRequestEntryTRLab;
};
