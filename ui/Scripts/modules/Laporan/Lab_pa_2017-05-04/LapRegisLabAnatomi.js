
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLabRegis;
var selectNamaLabRegis;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLabRegis;
var varLapLabRegis= ShowFormLapLabRegis();
var selectSetUmum;
var selectSetkelpas;
var type_file=0;
function ShowFormLapLabRegis()
{
    frmDlgLabRegis= fnDlgLabRegis();
    frmDlgLabRegis.show();
};

function fnDlgLabRegis()
{
    var winLabRegisReport = new Ext.Window
    (
        {
            id: 'winLabRegisReport',
            title: 'Laporan Regis Pasien Laboratorium',
            closeAction: 'destroy',
            width:400,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLabRegis()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganRegisLab').hide();
                Ext.getCmp('cboAsuransiRegisLab').hide();
                Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
                Ext.getCmp('cboUmumRegisLab').show();
            }
        }

        }
    );

    return winLabRegisReport;
};


function ItemDlgLabRegis()
{
    var PnlLapLabRegis = new Ext.Panel
    (
        {
            id: 'PnlLapLabRegis',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLabRegis_Atas(),
                getItemLapLabRegis_Bawah(),
				{
					   xtype: 'checkbox',
					   //fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
				},
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'OK',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapLabRegis',
                            handler: function()
                            {
                               if (ValidasiReportLabRegis() === 1)
                               {
								   /* //var tmppilihan = getKodeReportLabRegis();
									var criteria = GetCriteriaLabRegis();
								   
									loadMask.show();
									loadlaporanLabPA('0', 'LapRegisLab', criteria, function(){
										frmDlgLabRegis.close();
										loadMask.hide();
									}); */
									var kd_customer;
									if (Ext.get('cboPilihanLabRegiskelompokPasien').getValue() === 'Semua'){
										kd_customer='Semua';
									} else if (Ext.get('cboPilihanLabRegiskelompokPasien').getValue() === 'Perseorangan'){
										kd_customer= Ext.getCmp('cboPerseoranganRegisLab').getValue();
									} else if (Ext.get('cboPilihanLabRegiskelompokPasien').getValue() === 'Perusahaan'){
										kd_customer= Ext.getCmp('cboPerusahaanRequestEntryRegisLab').getValue();
									} else {
										kd_customer= Ext.getCmp('cboAsuransiRegisLab').getValue();
									}
										
									if (Ext.getCmp('Shift_All_LabRegis').getValue() === true){
										shift='All';
										shift1='false';
										shift2='false';
										shift3='false';
									}else{
										shift='';
										if (Ext.getCmp('Shift_1_LabRegis').getValue() === true){
											shift1='true';
										} else{
											shift1='false';
										}
										if (Ext.getCmp('Shift_2_LabRegis').getValue() === true){
											shift2='true';
										}else{
											shift2='false';
										}
										if (Ext.getCmp('Shift_3_LabRegis').getValue() === true){
											shift3='true';
										}else{
											shift3='false';
										}
									}
									
									
									var params={
										tglAwal: Ext.get('dtpTglAwalFilterLapRegisLab').getValue(),
										tglAkhir:Ext.get('dtpTglAkhirFilterLapRegisLab').getValue(),
										jenis_pasien:Ext.get('cboPilihanLabRegis').getValue(),
										kd_customer:kd_customer,
										shift:shift,
										shift1:shift1,
										shift2:shift2,
										shift3:shift3,
										type_file:type_file
									} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/lab_pa/lap_laboratorium_pa/LapRegisLab");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();

								};
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapLabRegis',
                            handler: function()
                            {
                                    frmDlgLabRegis.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapLabRegis;
};


function ValidasiReportLabRegis()
{
    var x=1;
	if(Ext.getCmp('cboPilihanLabRegis').getValue() === ''){
		ShowPesanWarningLabRegisReport('Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
    /* if(Ext.get('dtpTglAwalFilterLapRegisLab').dom.value > Ext.get('dtpTglAkhirFilterLapRegisLab').dom.value)
    {
        ShowPesanWarningLabRegisReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    } */
	if(Ext.getCmp('cboPilihanLabRegiskelompokPasien').getValue() === ''){
		ShowPesanWarningLabRegisReport('Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	if(Ext.get('cboPerusahaanRequestEntryRegisLab').getValue() === '' &&  Ext.get('cboAsuransiRegisLab').getValue() === '' &&  Ext.get('cboPerseoranganRegisLab').getValue() === '' && Ext.get('cboUmumRegisLab').getValue() === ''){
		ShowPesanWarningLabRegisReport('Sub Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	if(Ext.getCmp('Shift_All_LabRegis').getValue() === false && Ext.getCmp('Shift_1_LabRegis').getValue() === false && Ext.getCmp('Shift_2_LabRegis').getValue() === false && Ext.getCmp('Shift_3_LabRegis').getValue() === false){
		ShowPesanWarningLabRegisReport(nmRequesterRequest,'Laporan Regis Laboratorium');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningLabRegisReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLabRegis_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLabRegis(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterLapRegisLab',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 40,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterLapRegisLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLabRegisKelompokPasien(),
                mComboUmumLabRegis(),
				mComboPerseoranganLabRegis(),
                mComboAsuransiLabRegis(),
                mComboPerusahaanLabRegis()
                
            ]
        }]
    };
    return items;
};

function getItemLapLabRegis_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {
										boxLabel: 'Semua',
										name: 'Shift_All_LabRegis',
										id : 'Shift_All_LabRegis',
										handler: function (field, value) {
											if (value === true){
												Ext.getCmp('Shift_1_LabRegis').setValue(true);
												Ext.getCmp('Shift_2_LabRegis').setValue(true);
												Ext.getCmp('Shift_3_LabRegis').setValue(true);
												Ext.getCmp('Shift_1_LabRegis').disable();
												Ext.getCmp('Shift_2_LabRegis').disable();
												Ext.getCmp('Shift_3_LabRegis').disable();
											}else{
													Ext.getCmp('Shift_1_LabRegis').setValue(false);
													Ext.getCmp('Shift_2_LabRegis').setValue(false);
													Ext.getCmp('Shift_3_LabRegis').setValue(false);
													Ext.getCmp('Shift_1_LabRegis').enable();
													Ext.getCmp('Shift_2_LabRegis').enable();
													Ext.getCmp('Shift_3_LabRegis').enable();
												}
										}
									},
                                    {
										boxLabel: 'Shift 1',
										name: 'Shift_1_LabRegis',
										id : 'Shift_1_LabRegis'
									},
                                    {
										boxLabel: 'Shift 2',
										name: 'Shift_2_LabRegis',
										id : 'Shift_2_LabRegis'
									},
                                    {
										boxLabel: 'Shift 3',
										name: 'Shift_3_LabRegis',
										id : 'Shift_3_LabRegis'
									}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;

function mComboPilihanLabRegis()
{
    var cboPilihanLabRegis = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLabRegis',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLabRegis;
};

function mComboPilihanLabRegisKelompokPasien()
{
    var cboPilihanLabRegiskelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanLabRegiskelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLabRegiskelompokPasien;
};

function mComboPerseoranganLabRegis()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRegisLab = new WebApp.DataStore({fields: Field});
    dsPerseoranganRegisLab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganRegisLab = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganRegisLab',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: dsPerseoranganRegisLab,
				/* new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ), */
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRegisLab;
};

function mComboUmumLabRegis()
{
    var cboUmumRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumRegisLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumRegisLab;
};

function mComboPerusahaanLabRegis()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboCustomerLab',
			    param: 'jenis_cust=1 order by CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryRegisLab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRegisLab;
};

function mComboAsuransiLabRegis()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomerLab',
                param: "jenis_cust=2 order by CUSTOMER"
            }
        }
    );
    var cboAsuransiRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiRegisLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiRegisLab;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRegisLab').show();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').show();
        Ext.getCmp('cboUmumRegisLab').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').show();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').show();
   }
}

function mCombounitLabRegis()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryLabRegis = new Ext.form.ComboBox
    (
        {
            id: 'cbounitRequestEntryLabRegis',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                }
                    
                }
        }
    )

    return cbounitRequestEntryLabRegis;
};
