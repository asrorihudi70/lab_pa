
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapJumlahJenisPemeriksaan;
var selectNamaLapJumlahJenisPemeriksaan;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapJumlahJenisPemeriksaan;
var varLapJumlahJenisPemeriksaan= ShowFormLapJumlahJenisPemeriksaan();
var selectSetUmum;
var selectSetkelpas;

function ShowFormLapJumlahJenisPemeriksaan()
{
    frmDlgLapJumlahJenisPemeriksaan= fnDlgLapJumlahJenisPemeriksaan();
    frmDlgLapJumlahJenisPemeriksaan.show();
};

function fnDlgLapJumlahJenisPemeriksaan()
{
    var winLapJumlahJenisPemeriksaanReport = new Ext.Window
    (
        {
            id: 'winLapJumlahJenisPemeriksaanReport',
            title: 'Laporan Jumlah Jenis Pemeriksaan',
            closeAction: 'destroy',
            width:400,
            height: 170,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapJumlahJenisPemeriksaan()],
            listeners:
        {
            activate: function()
            {
               
            }
        }

        }
    );

    return winLapJumlahJenisPemeriksaanReport;
};


function ItemDlgLapJumlahJenisPemeriksaan()
{
    var PnlLapJumlahJenisPemeriksaan = new Ext.Panel
    (
        {
            id: 'PnlLapJumlahJenisPemeriksaan',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapJumlahJenisPemeriksaan_Atas(),
                getItemLapJumlahJenisPemeriksaan_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapJumlahJenisPemeriksaan',
                            handler: function()
                            {
								if (ValidasiReportLapJumlahJenisPemeriksaan() === 1)
								{
									if (Ext.getCmp('Shift_All_LapJumlahJenisPemeriksaan').getValue() === true){
										shift='All';
										shift1='false';
										shift2='false';
										shift3='false';
									}else{
										shift='';
										if (Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaan').getValue() === true){
											shift1='true';
										} else{
											shift1='false';
										}
										if (Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaan').getValue() === true){
											shift2='true';
										}else{
											shift2='false';
										}
										if (Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaan').getValue() === true){
											shift3='true';
										}else{
											shift3='false';
										}
									}
									
									var params={
										tglAwal:Ext.getCmp('dtpTglAwalFilterLapJumlahJenisPemeriksaan').getValue(),
										tglAkhir:Ext.getCmp('dtpTglAkhirFilterLapJumlahJenisPemeriksaan').getValue(),
										shift:shift,
										shift1:shift1,
										shift2:shift2,
										shift3:shift3,
									} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/lab_pa/lap_laboratorium_pa/LapJumlahJenisPemeriksaan");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
								};
                            }
                        },
                        {
                            xtype: 'button',
                            text:'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapJumlahJenisPemeriksaan',
                            handler: function()
                            {
                                    frmDlgLapJumlahJenisPemeriksaan.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapJumlahJenisPemeriksaan;
};

function ValidasiReportLapJumlahJenisPemeriksaan()
{
    var x=1;
    if(Ext.getCmp('dtpTglAwalFilterLapJumlahJenisPemeriksaan').getValue() > Ext.getCmp('dtpTglAkhirFilterLapJumlahJenisPemeriksaan').getValue())
    {
        ShowPesanWarningLapJumlahJenisPemeriksaanReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapJumlahJenisPemeriksaan').getValue() === false && Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaan').getValue() === false && Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaan').getValue() === false && Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaan').getValue() === false){
		ShowPesanWarningLapJumlahJenisPemeriksaanReport(nmRequesterRequest,'Laporan Regis Laboratorium');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningLapJumlahJenisPemeriksaanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapJumlahJenisPemeriksaan_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 50,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterLapJumlahJenisPemeriksaan',
                format: 'd-M-Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 10,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterLapJumlahJenisPemeriksaan',
                format: 'd-M-Y',
                value: now,
                width: 100
            }
                
            ]
        }]
    };
    return items;
};

function getItemLapJumlahJenisPemeriksaan_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {
										boxLabel: 'Semua',
										name: 'Shift_All_LapJumlahJenisPemeriksaan',
										id : 'Shift_All_LapJumlahJenisPemeriksaan',
										handler: function (field, value) {
											if (value === true){
												Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaan').setValue(true);
												Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaan').setValue(true);
												Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaan').setValue(true);
												Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaan').disable();
												Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaan').disable();
												Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaan').disable();
											}else{
													Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaan').setValue(false);
													Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaan').setValue(false);
													Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaan').setValue(false);
													Ext.getCmp('Shift_1_LapJumlahJenisPemeriksaan').enable();
													Ext.getCmp('Shift_2_LapJumlahJenisPemeriksaan').enable();
													Ext.getCmp('Shift_3_LapJumlahJenisPemeriksaan').enable();
												}
										}
									},
                                    {
										boxLabel: 'Shift 1',
										name: 'Shift_1_LapJumlahJenisPemeriksaan',
										id : 'Shift_1_LapJumlahJenisPemeriksaan'
									},
                                    {
										boxLabel: 'Shift 2',
										name: 'Shift_2_LapJumlahJenisPemeriksaan',
										id : 'Shift_2_LapJumlahJenisPemeriksaan'
									},
                                    {
										boxLabel: 'Shift 3',
										name: 'Shift_3_LapJumlahJenisPemeriksaan',
										id : 'Shift_3_LapJumlahJenisPemeriksaan'
									}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};


