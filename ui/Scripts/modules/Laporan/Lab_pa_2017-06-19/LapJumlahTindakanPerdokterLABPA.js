var type_file=0;
var dsLABPAJmlTindakanPerDok;
var selectLABPAJmlTindakanPerDok;
var selectNamaLABPAJmlTindakanPerDok;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgLABPAJmlTindakanPerDok;
var varLapLABPAJmlTindakanPerDok= ShowFormLapLABPAJmlTindakanPerDok();
var dsLABPA;

function ShowFormLapLABPAJmlTindakanPerDok()
{
    frmDlgLABPAJmlTindakanPerDok= fnDlgLABPAJmlTindakanPerDok();
    frmDlgLABPAJmlTindakanPerDok.show();
};

function fnDlgLABPAJmlTindakanPerDok()
{
    var winLABPAJmlTindakanPerDokReport = new Ext.Window
    (
        {
            id: 'winLABPAJmlTindakanPerDokReport',
            title: 'Laporan Jumlah Tindakan Per Dokter',
            closeAction: 'destroy',
            width:570,
            height: 193,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLABPAJmlTindakanPerDok()]

        }
    );

    return winLABPAJmlTindakanPerDokReport;
};

function ItemDlgLABPAJmlTindakanPerDok()
{
    var PnlLapLABPAJmlTindakanPerDok = new Ext.Panel
    (
        {
            id: 'PnlLapLABPAJmlTindakanPerDok',
            fileUpload: true,
            layout: 'form',
            width:560,
            height: 250,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapLABPAJmlTindakanPerDok_Periode(),
				//getItemLABPAJmlTindakanPerDok_Shift(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapLABPAJmlTindakanPerDok',
					handler: function()
					{
					   if (ValidasiReportLABPAJmlTindakanPerDok() === 1)
					   {
							
							var params={
												tglAwal:Ext.getCmp('dtpTglAwalLapLABPAJmlTindakanPerDok').getValue(),
												tglAkhir:Ext.getCmp('dtpTglAkhirLapLABPAJmlTindakanPerDok').getValue(),
												kdDokter:Ext.getCmp('cboDOKTER_LapJmlTindDok').getValue(),
												type_file:type_file
									} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/lab_pa/lap_laboratorium_pa/cetak_laporan_jumlah_tindakan_perdokter");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgLABPAJmlTindakanPerDok.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLapLABPAJmlTindakanPerDok').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapLABPAJmlTindakanPerDok').getValue()+'#aje#Laporan Batal Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionLABPA/cetakLABPAJmlTindakanPerDok");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgLABPAJmlTindakanPerDok.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapLABPAJmlTindakanPerDok',
					handler: function()
					{
						frmDlgLABPAJmlTindakanPerDok.close();
					}
				}
			
			]
        }
    );

    return PnlLapLABPAJmlTindakanPerDok;
};


function ValidasiReportLABPAJmlTindakanPerDok()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapLABPAJmlTindakanPerDok').dom.value === '')
	{
		ShowPesanWarningLABPAJmlTindakanPerDokReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapLABPAJmlTindakanPerDok').getValue() === false && Ext.getCmp('Shift_1_LapLABPAJmlTindakanPerDok').getValue() === false && Ext.getCmp('Shift_2_LapLABPAJmlTindakanPerDok').getValue() === false && Ext.getCmp('Shift_3_LapLABPAJmlTindakanPerDok').getValue() === false){
		ShowPesanWarningLABPAJmlTindakanPerDokReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportLABPAJmlTindakanPerDok()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapLABPAJmlTindakanPerDok').dom.value > Ext.get('dtpTglAkhirLapLABPAJmlTindakanPerDok').dom.value)
    {
        ShowPesanWarningLABPAJmlTindakanPerDokReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningLABPAJmlTindakanPerDokReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapLABPAJmlTindakanPerDok_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};
function getComboDokterLapJmlTindDok()
{
	dsdokter_LapJmlTindDok.load
	({
		params:
		{
			Skip: 0,
			Take: 1000,
			Sort: 'kd_dokter',
			Sortdir: 'ASC',
			target: 'ViewDokterPenunjang',
			param: "kd_unit in ('44')"
		}
	});
	return dsdokter_LapJmlTindDok;
}
function mComboDOKTERLapJmlTindDok()
{ 
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_LapJmlTindDok = new WebApp.DataStore({ fields: Field });
    getComboDokterLapJmlTindDok();
    var cboDOKTER_LapJmlTindDok = new Ext.form.ComboBox
	(
            {
                id: 'cboDOKTER_LapJmlTindDok',
				fieldLabel: 'Dokter ',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                //fieldLabel:  ' ',
                align: 'Right',
                width: 165,
				emptyText:'Pilih Dokter',
                store: dsdokter_LapJmlTindDok,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
				value:'Semua',
				editable: false,
				enableKeyEvents: true,
				
			listeners:
			{
				
				'select': function(a,b,c)
				{
					
					tmpkddoktertujuan=b.data.KD_DOKTER ;
				},
			}
            }
	);
	
    return cboDOKTER_LapJmlTindDok;
};
function getItemLapLABPAJmlTindakanPerDok_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:75,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapLABPAJmlTindakanPerDok',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},mComboDOKTERLapJmlTindDok(),
					{
					   xtype: 'checkbox',
					   fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					}
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapLABPAJmlTindakanPerDok',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


