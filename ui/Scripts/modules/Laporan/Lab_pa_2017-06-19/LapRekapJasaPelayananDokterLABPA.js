var type_file=0;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLABRekapJasaPelayananDokter;
var selectNamaLABRekapJasaPelayananDokter;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLABRekapJasaPelayananDokter;
var varLapLABRekapJasaPelayananDokter= ShowFormLapLABRekapJasaPelayananDokter();
var selectSetUmum;
var selectSetkelpas;

var unitasal;
var tmpnama_unit;
var selectSetPilihankelompokPasien;
var selectSetPilihanProfesi;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapLABRekapJasaPelayananDokter()
{
    frmDlgLABRekapJasaPelayananDokter= fnDlgLABRekapJasaPelayananDokter();
    frmDlgLABRekapJasaPelayananDokter.show();
};

function fnDlgLABRekapJasaPelayananDokter()
{
    var winLABRekapJasaPelayananDokterReport = new Ext.Window
    (
        {
            id: 'winLABRekapJasaPelayananDokterReport',
            title: 'Laporan Transaksi Jasa Pelayanan Dokter',
            closeAction: 'destroy',
            width:400,
            height: 360,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLABRekapJasaPelayananDokter()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganLAB').show();
                Ext.getCmp('cboAsuransiLAB').hide();
                Ext.getCmp('cboPerusahaanRequestEntryLAB').hide();
                Ext.getCmp('cboUmumLAB').hide();
            }
        }

        }
    );

    return winLABRekapJasaPelayananDokterReport;
};


function ItemDlgLABRekapJasaPelayananDokter()
{
    var PnlLapLABRekapJasaPelayananDokter = new Ext.Panel
    (
        {
            id: 'PnlLapLABRekapJasaPelayananDokter',
            fileUpload: true,
            layout: 'form',
			//width:400,
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLABRekapJasaPelayananDokter_Atas(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapLABRekapJasaPelayananDokter',
                            handler: function()
                            {
								if (Ext.getCmp('Shift_All').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
                            	//if (ValidasiReportLABPasienPerDokter() === 1)
							   	//{								
									var params={
						//id : 'cbPendaftaran_LABRekapJasaPelayananDokter'
						//id : 'cbTindakLAB_LABRekapJasaPelayananDokter'
						//$('.messageCheckbox:checked').val();
										shift:shift,
										shift1:shift1,
										shift2:shift2,
										shift3:shift3,
                                        kd_profesi:Ext.getCmp('IDcboPilihanLABJenisProfesi').getValue(),
										kd_poli:selectSetUnit,
										//pelayananPendaftaran:Ext.getCmp('cbPendaftaran_LABRekapJasaPelayananDokter').getValue(),
										//pelayananTindak:Ext.getCmp('cbTindakLAB_LABRekapJasaPelayananDokter').getValue(),
                                        //kd_dokter:Ext.getCmp('cboDokterLABRekapJasaPelayananDokter').getValue(),
										kd_dokter:GetCriteriaLABProfesi(),
										kd_kelompok:GetCriteriaLABPasienPerKelompok(),
										tglAwal:Ext.getCmp('dtpTglAwalLapLABRekapJasaPelayananDokter').getValue(),
										tglAkhir:Ext.getCmp('dtpTglAkhirLapLABRekapJasaPelayananDokter').getValue(),
										type_file:type_file,
										pasien: unitasal
									} 
									//alert(unitasal);
									//console.log(params);
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/lab_pa/lap_LABPARekapJasaPelayananDokter/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();		
									frmDlgLABPasienPerKelompok.close(); 
									
							   //};
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapLABRekapJasaPelayananDokter',
                            handler: function()
                            {
                                    frmDlgLABRekapJasaPelayananDokter.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapLABRekapJasaPelayananDokter;
};

function getItemLapLABRekapJasaPelayananDokter_Atas()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 280,
            anchor: '100% 100%',
            items: [
			{
				x: 10,
				y: 10,
				xtype: 'label',
				text: 'Unit Asal'
			}, {
				x: 110,
				y: 10,
				xtype: 'label',
				text: ' : '
			},
				mCombounitAsalLABRekapJasaPelayananDokter(),
			/* {
				xtype: 'checkboxgroup',
				width:210,
				x: 120,
				hidden:true,
				y: 10,
				items: 
				[
					{
						
						boxLabel: 'Pendaftaran',
						name: 'cbPendaftaran_LABRekapJasaPelayananDokter',
						id : 'cbPendaftaran_LABRekapJasaPelayananDokter'
					},
					{
						boxLabel: 'Tindak LAB',
						name: 'cbTindakLAB_LABRekapJasaPelayananDokter',
						id : 'cbTindakLAB_LABRekapJasaPelayananDokter'
					}
			   ]
			}, */
            //  ================================================================================== POLIKLINIK
			{
				x: 10,
				y: 40,
				xtype: 'label',
				text: 'Unit '
			}, {
				x: 110,
				y: 40,
				xtype: 'label',
				text: ' : '
			},
				mCombounitLABRekapJasaPelayananDokter(),
            //  ================================================================================== DOKTER
			{
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Bagian '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
                mComboLABJenisProfesi(),
				mComboDokterLABRekapJasaPelayananDokter(),
                mComboDokterLABRekapJasaPelayananPerawat(),

			{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, {
				x: 110,
				y: 130,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAwalLapLABRekapJasaPelayananDokter',
				format: 'd/M/Y',
                value: now
				//value: tigaharilalu
			}, {
				x: 230,
				y: 130,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 260,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapLABRekapJasaPelayananDokter',
				format: 'd/M/Y',
				value: now,
				width: 100
			},{
				x: 10,
				y: 160,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 160,
				xtype: 'label',
				text: ' : '
			},
				mComboLABPasienPerKelompok(),
				mComboLABPasienPerKelompokSEMUA(),
				mComboLABPasienPerKelompokPERORANGAN(),
				mComboLABPasienPerKelompokPERUSAHAAN(),
				mComboLABPasienPerKelompokASURANSI(),
			{
				x: 10,
				y: 220,
				xtype: 'label',
				text: 'Type File '
			}, {
				x: 110,
				y: 220,
				xtype: 'label',
				text: ' : '
			},
			{
				x: 120,
				y: 220,
			   xtype: 'checkbox',
			   id: 'CekLapPilihTypeExcel',
			   hideLabel:false,
			   boxLabel: 'Excel',
			   checked: false,
			   listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
						{
							type_file=1;
						}
						else
						{
							type_file=0;
						}
					}
			   }
			},
			{
				x: 10,
				y: 255,
				xtype: 'checkboxgroup',
				fieldLabel: 'Shift',
				columns: 5,
				vertical: false,
				items: [
					{
					boxLabel: 'Semua',checked:true,name: 'Shift_All',id : 'Shift_All',
					handler: function (field, value) 
					{
						if (value === true){
						Ext.getCmp('Shift_1').setValue(true);
						Ext.getCmp('Shift_2').setValue(true);
						Ext.getCmp('Shift_3').setValue(true);
						Ext.getCmp('Shift_1').disable();
						Ext.getCmp('Shift_2').disable();
						Ext.getCmp('Shift_3').disable();
					}else{
						Ext.getCmp('Shift_1').setValue(false);
						Ext.getCmp('Shift_2').setValue(false);
						Ext.getCmp('Shift_3').setValue(false);
						Ext.getCmp('Shift_1').enable();
						Ext.getCmp('Shift_2').enable();
						Ext.getCmp('Shift_3').enable();
					}
				}
				},
						{boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1',id : 'Shift_1'},
						{boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2',id : 'Shift_2'},
						{boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3',id : 'Shift_3'}
					]
			}
            ]
        }]
    };
    return items;
};

function ShowPesanWarningLABRekapJasaPelayananDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getComboDokterLab(kdUnit)
{
	dsDokterPelayaranDokter.load
	({
		params:
		{
			Skip: 0,
			Take: 1000,
			Sort: 'kd_dokter',
			Sortdir: 'ASC',
			target: 'ViewDokterPenunjang',
			param: "kd_unit = '"+kdUnit+"'"
		}
	});
	return dsDokterPelayaranDokter;
}
function mComboDokterLABRekapJasaPelayananDokter()
{
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    
    var cboPilihanLABRekapJasaPelayananDokter = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboDokterLABRekapJasaPelayananDokter',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanLABRekapJasaPelayananDokter;
};

function mComboDokterLABRekapJasaPelayananPerawat()
{
	var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokterLaporan',
				param: "WHERE dokter.jenis_dokter='1'"
			}
		}
	);
    var cboPilihanLABRekapJasaPelayananDokter = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboDokterLABRekapJasaPelayananPerawat',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:true,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLABRekapJasaPelayananDokter;
};

function getUnitDefault(){
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionLAB/getUnitDefault",
		params: {text:''},
		failure: function (o){
			loadMask.hide();
			ShowPesanErrorPenJasLab('Gagal mendapatkan data Unit default', 'Laboratorium');
		},
		success: function (o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				selectSetUnit=cst.kd_unit;
				tmpnama_unit=cst.nama_unit;
				getComboDokterLab(cst.kd_unit);
				Ext.getCmp('cbounitRequestEntryLABRekapJasaPelayananDokter').setValue(tmpnama_unit);
			} else{
				ShowPesanErrorPenJasLab('Gagal mendapatkan data Unit default', 'Laboratorium');
			}
		}
	});
}
function mCombounitLABRekapJasaPelayananDokter()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});
	getUnitDefault();
	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewCombounit_Konfigurasi',
                    param: " "
                }
            }
        );

    var cbounitRequestEntryLABRekapJasaPelayananDokter = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 40,
            id: 'cbounitRequestEntryLABRekapJasaPelayananDokter',
			width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryLABRekapJasaPelayananDokter;
};

function mCombounitAsalLABRekapJasaPelayananDokter()
{
    
    var cbounitAsalRequestEntryLABRekapJasaPelayananDokter = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 10,
            id: 'cbounitAsalRequestEntryLABRekapJasaPelayananDokter',
			width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: new Ext.data.ArrayStore({
							id: 0,
							fields:[
									'Id',
									'displayText'
							],
							data: [[0, 'Semua'],[1, 'RWJ'],[2, 'RWI'], [3, 'IGD'],[4, 'Kunjungan Langsung']]
						}),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
//            anchor: '95%',
			//value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						unitasal =b.data.Id; 		
						console.log(b.data);
					}
                    
                }
        }
    )

    return cbounitAsalRequestEntryLABRekapJasaPelayananDokter;
};


function mComboLABPasienPerKelompok()
{
    var cboPilihanLABRekapJasaPelayananDokterkelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 160,
                id:'cboPilihanLABPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectLABPasienPerKelompok(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanLABRekapJasaPelayananDokterkelompokPasien;
};

function mComboLABJenisProfesi()
{
    var cboPilihanLABJenisProfesi = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'IDcboPilihanLABJenisProfesi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Dokter'], [2, 'Perawat']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihanProfesi=b.data.displayText;
                            Combo_SelectLABProfesi(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLABJenisProfesi;
};

//LABPasienPerKelompok
function mComboLABPasienPerKelompokSEMUA()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganLAB = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboLABPasienPerKelompokSEMUA',
                typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				selectOnFocus:true,
				forceSelection: true,
				emptyText:'Silahkan Pilih...',
				valueField: 'Id',
	            displayField: 'displayText',
	            hidden:false,
				fieldLabel: '',
				width: 240,
				value:1,
				store: new Ext.data.ArrayStore
				(
						{
							id: 0,
							fields:
								[
										'Id',
										'displayText'
								],
							data: [[1, 'Semua']]
						}
				),
				listeners:
				{
					'select': function(a,b,c)
					{
						selectSetUmum=b.data.displayText ;
					}
	                                
	                            
				}
            }
	);
	return cboPerseoranganLAB;
};

function mComboLABPasienPerKelompokPERORANGAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganLAB = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboLABPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLAB;
};

function mComboLABPasienPerKelompokPERUSAHAAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='1' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganLAB = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboLABPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
				//value: selectsetperusahaan,
                listeners:
                {
				    'select': function(a,b,c)
					{
				        selectsetperusahaan = b.data.KD_CUSTOMER;
						selectsetnamaperusahaan = b.data.CUSTOMER;
					}
                }
            }
	);
	return cboPerseoranganLAB;
};

function mComboLABPasienPerKelompokASURANSI()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='2' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganLAB = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboLABPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
				valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
				    'select': function(a,b,c)
					{
						selectSetAsuransi=b.data.KD_CUSTOMER ;
						selectsetnamaAsuransi = b.data.CUSTOMER ;
					}
                }
            }
	);
	return cboPerseoranganLAB;
};


function Combo_SelectLABPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('IDmComboLABPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboLABPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('IDmComboLABPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboLABPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('IDmComboLABPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboLABPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboLABPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboLABPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboLABPasienPerKelompokSEMUA').show();
   }
};

function Combo_SelectLABProfesi(combo)
{
   var value = combo;

   if(value === "Dokter")
   {    
        Ext.getCmp('cboDokterLABRekapJasaPelayananDokter').show();
        Ext.getCmp('cboDokterLABRekapJasaPelayananPerawat').hide();
   }
   else if(value === "Perawat")
   {    
        Ext.getCmp('cboDokterLABRekapJasaPelayananPerawat').show();
        Ext.getCmp('cboDokterLABRekapJasaPelayananDokter').hide();
        
   }
   else
   {
        Ext.getCmp('cboDokterLABRekapJasaPelayananDokter').show();
        Ext.getCmp('cboDokterLABRekapJasaPelayananPerawat').hide();
   }
};


function GetCriteriaLABPasienPerKelompok()
{
    var strKriteria = '';
    
    if (Ext.getCmp('cboPilihanLABPasienPerKelompok').getValue() !== '')
    {
        if (Ext.get('cboPilihanLABPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
        else if (Ext.get('cboPilihanLABPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboLABPasienPerKelompokPERORANGAN').getValue(); } 
        else if (Ext.get('cboPilihanLABPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboLABPasienPerKelompokPERUSAHAAN').getValue(); } 
        else if (Ext.get('cboPilihanLABPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboLABPasienPerKelompokASURANSI').getValue(); }
    }else{
            strKriteria = 'Semua';
    }

    
    return strKriteria;
};

function GetCriteriaLABProfesi()
{
	var strKriteria = '';
	
	if (Ext.getCmp('IDcboPilihanLABJenisProfesi').getValue() !== '')
	{
		if (Ext.get('IDcboPilihanLABJenisProfesi').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
		else if (Ext.get('IDcboPilihanLABJenisProfesi').getValue() === 'Dokter'){ strKriteria = Ext.getCmp('cboDokterLABRekapJasaPelayananDokter').getValue(); } 
		else if (Ext.get('IDcboPilihanLABJenisProfesi').getValue() === 'Perawat'){ strKriteria = Ext.getCmp('cboDokterLABRekapJasaPelayananPerawat').getValue(); } 
	}else{
        strKriteria = 'Semua';
	}

	
	return strKriteria;
};

function ValidasiReportLABPasienPerDokter()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapLABPasienPerDokter').dom.value === '')
	{
		ShowPesanWarningLABPasienPerDokterReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
    return x;
};
