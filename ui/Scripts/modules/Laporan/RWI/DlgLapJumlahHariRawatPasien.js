
var dsRWIResponTime;
var selectRWIResponTime;
var selectNamaRWIResponTime;
var now = new Date();
var selectSetResponTime;
var frmDlgRWIResponTime;
var varLapRWIResponTime= ShowFormLapRWIResponTime();
var type_file=0;
function ShowFormLapRWIResponTime()
{
    frmDlgRWIResponTime= fnDlgRWIResponTime();
    frmDlgRWIResponTime.show();
};

function fnDlgRWIResponTime()
{
    var winRWIResponTimeReport = new Ext.Window
    (
        {
            id: 'winRWIResponTimeReport',
            title: 'Laporan Jumlah Hari Rawat Pasien',
            closeAction: 'destroy',
            width:400,
            height: 145,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWIResponTime()]

        }
    );

    return winRWIResponTimeReport;
};


function ItemDlgRWIResponTime()
{
    var PnlLapRWIResponTime = new Ext.Panel
    (
        {
            id: 'PnlLapRWIResponTime',
            fileUpload: true,
            layout: 'form',
            height: '80',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
               getItemLapRWIResponTime_Tanggal(), getItemLapRWIResponTime_Dept(),
                {
                    layout: 'absolute',
                    border: false,
                    width: 800,
                    height: 30,
                    items:
                    [
                        {
                            x: 175,
                            y: 1,
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWIResponTime',
                            handler: function(){
                                //if (ValidasiReportRWIResponTime() === 1){
									var kel_pas2;
									/*if(Ext.getCmp('cboPilihankelompokPasien').getValue() ==1){
										kel_pas2=0;
									}else{
										if(Ext.getCmp('cboPerseorangan').getValue() != ''){
											kel_pas2 = Ext.getCmp('cboPerseorangan').getValue();
										}
										if(Ext.getCmp('cboAsuransi').getValue() != ''){
											kel_pas2 = Ext.getCmp('cboAsuransi').getValue();
										}
										if(Ext.getCmp('cboPerusahaanRequestEntry').getValue() != ''){
											kel_pas2 = Ext.getCmp('cboPerusahaanRequestEntry').getValue();
										}
										
									}*/
                                    var params={
										tgl_awal:Ext.get('dtpTglAwalLapRWIResponTime').dom.value,
										tgl_akhir:Ext.get('dtpTglAkhirLapRWIResponTime').dom.value,
										//kel_pas:Ext.getCmp('cboPilihankelompokPasien').getValue(),
										//kel_pas2:kel_pas2,
										type_file:type_file
									} ;
                                    console.log(params);
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/rawat_inap/function_lap_RWI/cetak_lap_jumlahharirawat");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									loadMask.hide();
                                //}
                            }
                        },
                        {
                            x: 250,
                            y: 1,
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWIResponTime',
                            handler: function()
                            {
                                    frmDlgRWIResponTime.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWIResponTime;
};

function GetCriteriaRWIResponTime()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWIResponTime').dom.value !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWIResponTime').dom.value;
	};
        if (Ext.get('dtpTglAkhirLapRWIResponTime').dom.value !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRWIResponTime').dom.value;
	};
	if (Ext.getCmp('cboRujukanResponTimeSpesialRequestEntry').getValue() !== '' || Ext.getCmp('cboRujukanResponTimeSpesialRequestEntry').getValue() !== 'Pilih ResponTime...')
	{
		strKriteria += '##@@##' + 'ResponTime';
                strKriteria += '##@@##' + Ext.getCmp('cboRujukanResponTimeSpesialRequestEntry').getValue();
	};

	return strKriteria;
};


function ValidasiReportRWIResponTime()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWIResponTime').dom.value === '') || (Ext.get('dtpTglAkhirLapRWIResponTime').dom.value === '') || 
        (Ext.get('cboRujukanResponTimeSpesialRequestEntry').value === 'Silahkan Pilih...' || Ext.get('cboRujukanResponTimeSpesialRequestEntry').value === ' ' ))
    {
            if(Ext.get('dtpTglAwalLapRWIResponTime').dom.value === '')
            {
                    ShowPesanWarningRWIResponTimeReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            if(Ext.get('dtpTglAkhirLapRWIResponTime').dom.value === '')
            {
                    ShowPesanWarningRWIResponTimeReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.getCmp('cboRujukanResponTimeSpesialRequestEntry').getValue() === '' || Ext.getCmp('cboRujukanResponTimeSpesialRequestEntry').getValue() === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningRWIResponTimeReport(nmGetValidasiKosong('ResponTime'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRWIResponTime()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWIResponTime').dom.value > Ext.get('dtpTglAkhirLapRWIResponTime').dom.value)
    {
        ShowPesanWarningRWIResponTimeReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWIResponTimeReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWIResponTime_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.98,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 800,
                height: 35,
                anchor: '100% 100%',
                items:
                [
                    /*{
                        x: 5,
                        y: 1,
                        xtype: 'label',
                        text: 'Kelompok Pasien '
                    },
					{
                        x: 90,
                        y: 1,
                        xtype: 'label',
                        text: ':'
                    },*/
                    // mComboPilihanKelompokPasien(),
					// mComboPerseorangan(),
					// mComboAsuransi(),
					// mComboPerusahaan(),
					// mComboUmum(),
					{
                        x: 5,
                        y: 1,
                        xtype: 'label',
                        text: 'Tipe File'
                    },
					{
                        x: 90,
                        y: 1,
                        xtype: 'label',
                        text: ':'
                    },
					{
						x: 110,
                        y: 1,
						xtype: 'checkbox',
						id: 'CekLapPilihFile',
						hideLabel:false,
						boxLabel: 'Excel',
						checked: false,
						listeners: 
						{
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihFile').getValue()===true)
								{
									type_file =1;
								}
								else
								{
									type_file =0;
								   
								}
							}
						}
					}
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWIResponTime_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                columnWidth: .99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 800,
                height: 30,
                anchor: '100% 100%',
                items:
                [
                    {
                        x: 5,
                        y: 1,
                        xtype: 'label',
                        text: 'Tanggal '
                    },
					{
                        x: 90,
                        y: 1,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 110,
                        y: 1,
                        xtype: 'datefield',
                        id: 'dtpTglAwalLapRWIResponTime',
                        format: 'd-M-Y',
                        value:now
                    },
                    {
                        x: 220,
                        y: 1,
                        xtype: 'label',
                        text: 's/d'
                    },
                    {
                        x: 240,
                        y: 1,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirLapRWIResponTime',
                        format: 'd-M-Y',
                        value:now
                    },
					
                ]
            }
        ]
    }
    return items;
};


function mComboUnitLapRWIResponTime()
{
     var cboResponTime = new Ext.form.ComboBox
	(
		{
			id:'cboResponTime',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Wilayah ',
			width:80,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Prop'],[2, 'Kab'],[3, 'Kec'],[4, 'Kel']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetResponTime,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetResponTime=b.data.displayText ;
				}
			}
		}
	);
	return cboResponTime;
};

function mcomboResponTimeSpesial()
{
    var Field = ['no_ResponTime','ResponTime'];
    ds_ResponTimeSpesial_viDaftar = new WebApp.DataStore({fields: Field});

	ds_ResponTimeSpesial_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewSetupKelasSpesial',
                param: ""
            }
        }
    )

    var cboRujukanResponTimeSpesialRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanResponTimeSpesialRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih ResponTime...',
            fieldLabel: 'ResponTime ',
            align: 'Right',
            store: ds_ResponTimeSpesial_viDaftar,
            valueField: 'no_ResponTime',
            displayField: 'ResponTime',
            anchor: '95%',
            Width:'800',
            value: 'Semua',
            x: 95,
            y: 1,
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                },
                    'render': function(c)
                                {
                                    
                                }


		}
        }
    )

    return cboRujukanResponTimeSpesialRequestEntry;
}

function mComboPilihanKelompokPasien()
{
    var cboPilihankelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 110,
                y: 1,
                id:'cboPilihankelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihankelompokPasien;
};
function mComboPerseorangan()
{
    var cboPerseorangan = new Ext.form.ComboBox
	(
            {
                x: 110,
                y: 30,
                id:'cboPerseorangan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseorangan;
};

function mComboUmum()
{
    var cboUmum = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 30,
			id:'cboUmum',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			disabled:true,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmum;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1'
			}
		}
	);
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 30,
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: 'Semua',
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntry;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2"
            }
        }
    );
    var cboAsuransi = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 30,
			id:'cboAsuransi',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: 'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransi;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseorangan').show();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').show();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').show();
   }
   else
   {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').show();
   }
};
