var fields = [
    {name: 'KD_UNIT', mapping : 'KD_UNIT'},
    {name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];

var fieldsPayment = [
    {name: 'KD_PAY', mapping : 'KD_PAY'},
    {name: 'URAIAN', mapping : 'URAIAN'}
];

var cols = [
    { id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
    { header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
];

var colsPayment = [
    { id : 'KD_PAY', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'KD_PAY',hidden : true},
    { header: "Kode Pay", width: 50, sortable: true, dataIndex: 'URAIAN'}
];
var firstGrid;
var Field_order = ['columns'];
// $this.DataStore.list_order = new WebApp.DataStore({fields: Field_poli_viDaftar});
var secondGridStoreLapIGDTransaksiPerUnit;
var DlgLapIGDTransaksiPerUnit={
    vars:{
        comboSelect:null
    },
    coba:function(){
        var $this=this;
        $this.DateField.startDate
    },
    ArrayStore:{
        dokter:new Ext.data.ArrayStore({fields:[]})
    },
    CheckboxGroup:{
        shift:null,
        tindakan:null,
    },
    ComboBox:{
        kelPasien1:null,
        combo1:null,
        combo2:null,
        combo3:null,
        combo0:null,
        order:null,
        list_laporan:null,
        combooperator:null,
        poliklinik:null,
        dokter:null,
        jenislaporan:null,
        jenispasien:null,
    },
    DataStore:{
        combo2:null,
        combo3:null,
        list_laporan:null,
        list_order:new WebApp.DataStore({fields: Field_order}),
        combooperator:null,
        poliklinik:null,
        combo1:null,
        jenislaporan:null,
        dokter:null
    },
    DateField:{
        startDate:null,
        endDate:null
    },
    Window:{
        main:null
    },
    comboOnSelect:function(val){
        var $this=this;
        $this.ComboBox.combo0.hide();
        $this.ComboBox.combo1.hide();
        $this.ComboBox.combo2.hide();
        $this.ComboBox.combo3.hide();
        if(val==-1){
            $this.ComboBox.combo0.show();
            
        }else if(val==0){
            $this.ComboBox.combo1.show();
        }else if(val==1){
            $this.ComboBox.combo2.show();
        }else if(val==2){
            $this.ComboBox.combo3.show();
        }
    },
    initPrint:function(){
        var $this=this;
        loadMask.show();
        var params={};
        console.log($this.ComboBox.list_laporan.value);
        if ($this.ComboBox.list_laporan.value == undefined) {
            $this.messageBox("Informasi", "Laporan harap dipilih", "");    
        }else{
            params['laporan']    = $this.ComboBox.list_laporan.value;
            params['start_date'] = $this.DateField.startDate.value;
            params['end_date']   = $this.DateField.endDate.value;
            params['order_by']   = $this.ComboBox.order.value;

            console.log(params);
            var form             = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("target", "_blank");
            form.setAttribute("action", baseURL + "index.php/rawat_inap/function_lap_RWI/cetak_laporan_lain_lain");
            var hiddenField      = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "data");
            hiddenField.setAttribute("value", Ext.encode(params));
            form.appendChild(hiddenField);
            document.body.appendChild(form);
            form.submit();  
        }
        loadMask.hide();

    },
    messageBox:function(modul, str, icon){
        Ext.MessageBox.show
        (
            {
                title: modul,
                msg:str,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.icon,
                width:300
            }
        );
    },
    getDokter:function(){
        var $this=this;
        $this.ComboBox.dokter = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.ArrayStore.dokter,
            width: 200,
            value:'Semua',
            valueField: 'kd_dokter',
            displayField: 'nama',
            listeners:{
                'select': function(a, b, c){
                               
                }
            }
        });         
        $.ajax({
            type: 'POST',
            dataType:'JSON',
            url:baseURL + "index.php/gawat_darurat/lap_penerimaan/getDokter",
            success: function(r){
                loadMask.hide();
                if(r.processResult=='SUCCESS'){
                    $this.ArrayStore.dokter.loadData([],false);
                    for(var i=0,iLen=r.data.length; i<iLen ;i++){
                        $this.ArrayStore.dokter.add(new $this.ArrayStore.dokter.recordType(r.data[i]));
                    }
                }else{
                    Ext.Msg.alert('Gagal',r.processMessage);
                }
            },
            error: function(jqXHR, exception) {
                Nci.ajax.ErrorMessage(jqXHR, exception);
            }
        });
        return $this.ComboBox.dokter;
    },
    getPoliklinik:function(){
        var $this=this;
        var Field = ['KD_UNIT','NAMA_UNIT'];
        $this.DataStore.poliklinik = new WebApp.DataStore({fields: Field});
        $this.DataStore.poliklinik.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=3 and type_unit=false"
            }
        });
        $this.ComboBox.poliklinik = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.DataStore.poliklinik,
            width: 200,
            value:'Semua',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            listeners:{
                'select': function(a, b, c){
                               
                }
            }
        });         
        return $this.ComboBox.poliklinik;
    },
    getCombo0:function(){
        var $this=this;
        $this.ComboBox.combo0 = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            width:270,
            store: new Ext.data.ArrayStore({
                id: 0,
                fields:['Id','displayText'],
                data: [[1, 'Semua']]
            }),
            valueField: 'Id',
            displayField: 'displayText',
            value:1,
            disabled:true,
            listeners:{
                'select': function(a,b,c){
                    selectSetUmum=b.data.displayText ;
                }
            }
        });
        return $this.ComboBox.combo0;
    },
    getComboOrderBy:function(){
        var $this=this;
        $this.ComboBox.order = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            store: $this.DataStore.list_order,
            width:270,
            valueField: 'columns',
            displayField: 'columns',
            listeners:{
                'select': function(a,b,c){
                }
            }
        });
        return $this.ComboBox.order;
    },
    getCombo1:function(){
        var $this=this;
        var Field = ['KD_CUSTOMER','CUSTOMER'];
        $this.DataStore.combo1 = new WebApp.DataStore({fields: Field});
        $this.DataStore.combo1.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboLookupCustomer',
                param: 'jenis_cust=0 ORDER BY CUSTOMER '
            }
        });
        $this.ComboBox.combo1 = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            hidden:true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            width:270,
            store: $this.DataStore.combo1,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            value:'Semua',
            listeners:{
                'select': function(a,b,c){
                }
            }
        });
        return $this.ComboBox.combo1;
    },
    getCombo2:function(){
        var $this=this;
        var Field = ['KD_CUSTOMER','CUSTOMER'];
        $this.DataStore.combo2 = new WebApp.DataStore({fields: Field});
        $this.DataStore.combo2.load({
            params:{
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboLookupCustomer',
                param: 'jenis_cust=1 ORDER BY CUSTOMER '
            }
        });
        $this.ComboBox.combo2 = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: '',
            align: 'Right',
            hidden:true,
            store: $this.DataStore.combo2,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            value:'Semua',
            width:270,
            listeners:{
                'select': function(a,b,c){
                }
            }
        });
        return $this.ComboBox.combo2;
    },
    comboOperatorLaporanTransaksiPerUnitIGD:function(){
        var $this=this;
        var Field = ['KD_USER','FULL_NAME'];
        $this.DataStore.combooperator = new WebApp.DataStore({fields: Field});
        $this.DataStore.combooperator.load({
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'full_name',
                Sortdir: '',
                target:'ViewComboUser',
                param: ""
            }
        });
        $this.ComboBox.combooperator = new Ext.form.ComboBox
        (
            {
                // id: 'cboUserLaporanTransaksiPerUnitIGD',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'SEMUA',
                align: 'Right',
                value:'SEMUA',
                store: $this.DataStore.combooperator,
                valueField: 'KD_USER',
                displayField: 'FULL_NAME',
                width:380,
                listeners:
                {
                    'select': function(a, b, c)
                    {
                        //selectSetuser=b.data.KD_USER;
                    } 
                }
            }
        )

        return $this.ComboBox.combooperator;
    },
    loadDokter_TransaksiPerUnit:function(param){
        var $this=this;
        $this.DataStore.dokter = new WebApp.DataStore({fields: ['kd_dokter','nama']})
        if (param==='' || param===undefined) {
            param={
                text: '0',
            };
        }
        
        Ext.Ajax.request({
            url: baseURL + "index.php/main/cetaklaporanIGD/getDokter",
            params: {
                text:'',
            },
            failure: function(o){
                var cst = Ext.decode(o.responseText);
            },      
            success: function(o) {
                // $this.DataStore.dokter.removeAll();
                var cst = Ext.decode(o.responseText);

                for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                    var recs    = [],recType = $this.DataStore.dokter.recordType;
                    var o=cst['listData'][i];
            
                    recs.push(new recType(o));
                    $this.DataStore.dokter.add(recs);
                    console.log(o);
                }
            }
        });
        
    },

    ComboDokter_TransaksiPerUnit:function(){
        var $this=this;
        $this.DataStore.dokter = new WebApp.DataStore({fields: ['kd_dokter','nama']})
        $this.loadDokter_TransaksiPerUnit();
        $this.ComboBox.dokter = new Ext.form.ComboBox
        (
            {
                // id: '$this.ComboBox.dokter',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                selectOnFocus:true,
                store: $this.DataStore.dokter,
                valueField: 'kd_dokter',
                displayField: 'nama',
                emptyText:'SEMUA',
                width:380,
                listeners:
                {
                    'select': function(a, b, c)
                    {
                        
                    }
                }
            }
        )
        return $this.ComboBox.dokter;
    },
    seconGridPenerimaan : function(){
        
        var secondGrid;
        secondGridStoreLapIGDTransaksiPerUnit = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStoreLapIGDTransaksiPerUnit,
                    columns          : cols,
                    autoScroll       : true,
                    columnLines      : true,
                    border           : true,
                    enableDragDrop   : true,
                    enableDragDrop   : true,
                    height           : 150,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_UNIT',
                    title            : 'Unit yang dipilih',
                    listeners : {
                        afterrender : function(comp) {
                            var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                            var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                                    ddGroup    : 'secondGridDDGroup',
                                    notifyDrop : function(ddSource, e, data){
                                            var records =  ddSource.dragData.selections;
                                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                            secondGrid.store.add(records);
                                            secondGrid.store.sort('KD_UNIT', 'ASC');
                                            return true
                                    }
                            });
                        }
                    },
                viewConfig: 
                {
                        forceFit: true
                }
        });
        return secondGrid;
    },
    firstGridPenerimaan : function(){
        var dataSource_unit;
        var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
        dataSource_unit         = new WebApp.DataStore({fields: Field_poli_viDaftar});
        dataSource_unit.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: " parent = '1' and type_unit=false "
                }
            }
        );
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 150,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
            (
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colNRM_viDaftar',
                        header: 'No.Medrec',
                        dataIndex: 'KD_UNIT',
                        sortable: true,
                        hidden : true
                    },{
                        id: 'colNMPASIEN_viDaftar',
                        header: 'Nama',
                        dataIndex: 'NAMA_UNIT',
                        sortable: true,
                        width: 50
                    }
                ]
            ),   
            listeners : {
                afterrender : function(comp) {
                    var secondGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                }
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid;
    },
    firstGridPayment : function(){
        var firstGridPayment;
        var dataSource_payment;
        var Field_payment = ['KD_PAY','URAIAN'];
        dataSource_payment         = new WebApp.DataStore({fields: Field_payment});
        dataSource_payment.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'KD_PAY',
                    Sortdir: 'ASC',
                    target:'ViewListPayment',
                    param: " "
                }
            }
        );
            firstGridPayment = new Ext.grid.GridPanel({
                ddGroup          : 'secondGridDDGroupPayment',
                store            : dataSource_payment,
                autoScroll       : true,
                columnLines      : true,
                border           : true,
                enableDragDrop   : true,
                height           : 150,
                stripeRows       : true,
                trackMouseOver   : true,
                title            : 'Payment',
                anchor           : '100% 100%',
                plugins          : [new Ext.ux.grid.FilterRow()],
                colModel         : new Ext.grid.ColumnModel
                (
                    [
                    new Ext.grid.RowNumberer(),
                        {
                            id: 'colKD_pay',
                            header: 'Kode Pay',
                            dataIndex: 'KD_PAY',
                            sortable: true,
                            hidden : true
                        },
                        {
                            id: 'colKD_uraian',
                            header: 'Uraian',
                            dataIndex: 'URAIAN',
                            sortable: true,
                            width: 50
                        }
                    ]
                ),   
                listeners : {
                    afterrender : function(comp) {
                        var secondGridDropTargetEl = firstGridPayment.getView().scroller.dom;
                        var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                                ddGroup    : 'firstGridDDGroupPayment',
                                notifyDrop : function(ddSource, e, data){
                                        var records =  ddSource.dragData.selections;
                                        Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                        firstGridPayment.store.add(records);
                                        firstGridPayment.store.sort('KD_PAY', 'ASC');
                                        return true
                                }
                        });
                    }
                },
                    viewConfig: 
                        {
                                forceFit: true
                        }
            });
        return firstGridPayment;
    },
    getCombo3:function(){
        var $this=this;
        var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

        $this.DataStore.combo3 = new WebApp.DataStore({fields: Field_poli_viDaftar});

        $this.DataStore.combo3.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER "
            }
        });
        $this.ComboBox.combo3 = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: '',
            align: 'Right',
            width:270,
            hidden:true,
            store: $this.DataStore.combo3,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            value:'Semua',
//          value: 0,
            listeners:{
                'select': function(a,b,c){
//                  selectSetAsuransi=b.data.KD_CUSTOMER ;
//                  selectsetnamaAsuransi = b.data.CUSTOMER ;
                }
            }
        });
        return $this.ComboBox.combo3;
    },
    getComboListLaporan:function(){
        var $this=this;
        var Field_poli_viDaftar = ['baris_awal_isi_excel','deskripsi_sp','export_excel','jml_col','kd_sp','kolom_awal_isi_excel','nama_excel'];

        $this.DataStore.list_laporan = new WebApp.DataStore({fields: Field_poli_viDaftar});

        Ext.Ajax.request({
            //url: "./home.mvc/getModule",
            //url: baseURL + "index.php/main/getTrustee",
            url: baseURL + "index.php/main/functionRWI/list_laporan_lain_lain",
            params: {
                UserID: 'Admin',
            },
            failure: function (o)
            {
                var cst = Ext.decode(o.responseText);

            },
            success: function (o) {
                var cst  = Ext.decode(o.responseText);
                console.log(cst);
                //dsDataPerawatPenindak_KASIR_RWI.load(myData);
                for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                    var recs    = [],recType = $this.DataStore.list_laporan.recordType;
                    var o       = cst['data'][i];
                    recs.push(new recType(o));
                    $this.DataStore.list_laporan.add(recs);
                }
            }
        });
        $this.ComboBox.list_laporan = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Laporan...',
            fieldLabel: '',
            align: 'Right',
            width:400,
            store: $this.DataStore.list_laporan,
            valueField: 'kd_sp',
            displayField: 'deskripsi_sp',
//          value: 0,
            listeners:{
                'select': function(a,b,c){
                    Ext.Ajax.request({
                        url: baseURL + "index.php/main/functionRWI/list_order_by_lain_lain",
                        params: {
                            kd_sp: b.data.kd_sp,
                        },
                        failure: function (o)
                        {
                            var cst = Ext.decode(o.responseText);

                        },
                        success: function (o) {
                            var cst  = Ext.decode(o.responseText);
                            $this.DataStore.list_order.removeAll();
                            //dsDataPerawatPenindak_KASIR_RWI.load(myData);
                            for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                                var recs    = [],recType = $this.DataStore.list_order.recordType;
                                var o       = cst['data'][i];
                                recs.push(new recType(o));
                                $this.DataStore.list_order.add(recs);

                                // $this.ComboBox.order.getStore().add({text: recs, value: recs , Id:recs, displayText:recs});
                            }
                            // console.log($this.DataStore.list_order);
                        }
                    });
                }
            }
        });
        return $this.ComboBox.list_laporan;
    },
    init:function(){
        var $this=this;
        $this.Window.main=new Ext.Window({
            width: 600,
            height:300,
            modal:true,
            title:'Laporan Lain - Lain',
            layout:'fit',
            items:[
                new Ext.Panel({
                    bodyStyle:'padding: 5px 5px 5px 5px',
                    layout:'form',
                    border:false,
                    autoScroll       : true,
                    fbar:[
                        new Ext.Button({
                            text:'OK',
                            handler:function(){
                                $this.initPrint()
                            }
                        }),
                        new Ext.Button({
                            text:'Batal',
                            handler:function(){
                                $this.Window.main.close();
                            }
                        })
                    ],
                    items:[
                        {
                            xtype : 'fieldset',
                            title : 'UNIT',
                            hidden: true,
                            layout:'column',
                            border:true,
                            height: 200,
                            bodyStyle:'margin-top: 5px',
                            items:[
                                {
                                    border:false,
                                    columnWidth:.45,
                                    bodyStyle:'margin-top: 5px',
                                    items:[
                                        $this.firstGridPenerimaan(),
                                    ]
                                },
                                {
                                    border:false,
                                    columnWidth:.1,
                                    bodyStyle:'margin-top: 5px',
                                },
                                {
                                    border:false,
                                    columnWidth:.45,
                                    bodyStyle:'margin-top: 5px; align:right;',
                                    items:[
                                        $this.seconGridPenerimaan(),
                                    ]
                                },
                            ]
                        },
                        {
                            xtype : 'fieldset',
                            title : '',
                            hidden: true,
                            layout: 'column',
                            border: true,
                            items:[
                                {                                  
                                    xtype: 'checkbox',
                                    id: 'CekLapPilihSemuaIGDTransaksiPerUnit',
                                    hideLabel:false,
                                    boxLabel: 'Pilih Semua Unit',
                                    checked: false,
                                    listeners: 
                                    {
                                        check: function()
                                        {
                                           if(Ext.getCmp('CekLapPilihSemuaIGDTransaksiPerUnit').getValue()===true)
                                            {
                                                firstGrid.getSelectionModel().selectAll();
                                            }
                                            else
                                            {
                                                firstGrid.getSelectionModel().clearSelections();
                                            }
                                        }
                                   }
                                },
                            ]
                        },{
                            xtype : 'fieldset',
                            title : 'Daftar laporan',
                            layout: 'column',
                            border: true,
                            items:[
                                $this.getComboListLaporan(),
                            ]
                        },{
                            xtype : 'fieldset',
                            title : 'Periode',
                            layout: 'column',
                            border: true,
                            items:[
                                {
                                    xtype:'displayfield',
                                    width: 100,
                                    value:'Periode : '
                                },
                                $this.DateField.startDate=new Ext.form.DateField({
                                    value: new Date(),
                                    width: 175,
                                    format:'d/M/Y'
                                }),
                                {
                                    xtype:'displayfield',
                                    width: 30,
                                    value:'&nbsp;s/d&nbsp;'
                                },
                                $this.DateField.endDate=new Ext.form.DateField({
                                    value: new Date(),
                                    width: 175,
                                    format:'d/M/Y'
                                })
                            ]
                        },{
                            xtype : 'fieldset',
                            title : 'Order By',
                            layout: 'column',
                            border: true,
                            items:[
                                $this.getComboOrderBy(),
                            ]
                        },
                        /* {
                            xtype : 'fieldset',
                            title : 'Dokter',
                            layout: 'column',
                            border: true,
                            items:[
                                {
                                    xtype:'displayfield',
                                    width: 100,
                                    value:'Dokter : '
                                },
                                
                                $this.ComboDokter_TransaksiPerUnit(),
                            ]
                        }, */{
                            xtype : 'fieldset',
                            title : 'Kelompok pasien',
                            layout: 'column',
                            border: true,
                            hidden: true,
                            items:[
                                {
                                    xtype:'displayfield',
                                    width: 100,
                                    value:'Kelompok : '
                                },
                                $this.ComboBox.kelPasien1=new Ext.form.ComboBox({
                                    triggerAction: 'all',
                                    id: 'head_kel_pasien',
                                    lazyRender:true,
                                    mode: 'local',
                                    width: 100,
                                    selectOnFocus:true,
                                    forceSelection: true,
                                    emptyText:'Silahkan Pilih...',
                                    fieldLabel: 'Pendaftaran Per Shift ',
                                    store: new Ext.data.ArrayStore({
                                        id: 0,
                                        fields:[
                                                'Id',
                                                'displayText'
                                        ],
                                        data: [[-1, 'Semua'],[0, 'Perseorangan'],[1, 'Perusahaan'], [2, 'Asuransi']]
                                    }),
                                    valueField: 'Id',
                                    displayField: 'displayText',
                                    value:'Semua',
                                    listeners:{
                                        'select': function(a,b,c){
                                                $this.vars.comboSelect=b.data.displayText;
                                                $this.comboOnSelect(b.data.Id);
                                        }
                                    }
                                }),
                                {
                                    xtype:'displayfield',
                                    width: 10,
                                    value:'&nbsp;'
                                },
                                $this.getCombo0(),
                                $this.getCombo1(),
                                $this.getCombo2(),
                                $this.getCombo3(),
                            ]
                        },
                        /*{
                            xtype : 'fieldset',
                            title : 'Operator',
                            layout: 'column',
                            border: true,
                            items:[
                                {
                                    xtype:'displayfield',
                                    width: 100,
                                    value:'Operator : '
                                },
                                
                                $this.comboOperatorLaporanTransaksiPerUnitIGD(),
                            ]
                        },*/
                    ]
                })
            ]
        }).show();
    }
};
DlgLapIGDTransaksiPerUnit.init();
