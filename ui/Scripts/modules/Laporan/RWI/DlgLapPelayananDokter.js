
var dsRWIPelayananDokter;
var selectRWIPelayananDokter;
var selectNamaRWIPelayananDokter;
var now = new Date();
var selectSetPelayananDokter;
var frmDlgRWIPelayananDokter;
var varLapRWIPelayananDokter= ShowFormLapRWIPelayananDokter();
var type_file=0;
function ShowFormLapRWIPelayananDokter()
{
    frmDlgRWIPelayananDokter= fnDlgRWIPelayananDokter();
    frmDlgRWIPelayananDokter.show();
};
var kd_spesial='';
var kd_kelas='';
var kd_irna;
var kd_irna2;
var rd_tanggal = '1';
var pasien_pulang='false';
var secondGrid_bayar;
function fnDlgRWIPelayananDokter()
{
    
    var winPelayananDokter = new Ext.Window
    (
        {
            id: 'winPelayananDokter',
            title: 'Laporan Pelayanan Per Pasien Detail',
            closeAction: 'destroy',
            width:430,
            height: 580,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWIPelayananDokter()]

        }
    );

    return winPelayananDokter;
};


function ItemDlgRWIPelayananDokter()
{
    
    var PnlLapRWIPelayananDokter = new Ext.Panel
    (
        {
            id: 'PnlLapRWIPelayananDokter',
            fileUpload: true,
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px 5px 5px 5px',
            items:
            [
                {
                    layout: 'absolute',
                    border: false,
                    width: 420,
                    height: 160,
                    items: [kriteria_kamar_bayar()]
                },
                {
                    layout: 'absolute',
                    border: false,
                    width: 420,
                    height: 50,
                    items: [kriteria_pembayaran_sett()]
                },
                {
                    layout: 'absolute',
                    border: false,
                    width: 420,
                    height: 62,
                    items: [kriteria_tanggal_sett()]
                },
                {
                    layout: 'absolute',
                    border: false,
                    width: 450,
                    height: 120,
                    items: [dataGrid_viSummeryPasien()]
                },
                {
                    layout: 'absolute',
                    border: false,
                    width: 420,
                    height: 60,
                    bodyStyle: 'margin-top:5px; padding: 5px 5px 5px 5px;',
                    items: [kelompok_pasien()]
                },
                {
                    layout: 'absolute',
                    border: false,
                    width: 420,
                    height: 60,
                    bodyStyle: 'padding: 5px 5px 5px 5px',
                    items: [kriteria_dokter_sett()]
                },
                /*{
                    layout: 'absolute',
                    border: false,
                    width: 420,
                    height: 60,
                    bodyStyle: 'padding: 5px 5px 5px 5px',
                    items: [kriteria_laporan()]
                },*/
                /*{
                    layout: 'absolute',
                    border: true,
                    width: 765,
                    height: 35,
                    items: [kriteria_pilih_bawah()]
                },
                {
                     xtype: 'label',
                     html: '&nbsp;',
                     width:150,
                     height:2
                },*/
               {
                    layout: 'absolute',
                    border: false,
                    width: 760,
                    height: 30,
                    items:
                    [
                        
                        {
                            x: 200,
                            y: 0.25,
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWIPelayananDokter',
                            handler: function(){                                    
                                    loadMask.show();
                                    var kel_pas2;
                                    var nama_kel_pas;
                                    if(Ext.getCmp('cboPilihankelompokPasien').getValue() ==1){
                                        kel_pas2=0;
                                        nama_kel_pas='Semua';
                                    }
                                    if(Ext.getCmp('cboPilihankelompokPasien').getValue() ==2){
                                        kel_pas2 = Ext.getCmp('cboPerseorangan').getValue();
                                        
                                    }
                                    if(Ext.getCmp('cboPilihankelompokPasien').getValue() ==4){
                                        kel_pas2 = Ext.getCmp('cboAsuransi').getValue();
                                        
                                    }
                                    if(Ext.getCmp('cboPilihankelompokPasien').getValue() ==3){
                                        kel_pas2 = Ext.getCmp('cboPerusahaanRequestEntry').getValue();
                                    }

                                    var sendDataArrayBayar = [];
                                    var sendTextDataArrayBayar = "";
                                    secondGridStore_bayar.each(function(record){
                                        var recordArray = [record.get("kd_pay")];
                                        sendTextDataArrayBayar += "'"+recordArray+"',";
                                        // sendDataArrayBayar.push(recordArray);
                                    });

                                    var params={
                                        tgl_awal    :Ext.get('dtpTglAwalLapRWIPelayananDokter').dom.value,
                                        tgl_akhir   :Ext.get('dtpTglAkhirLapRWIPelayananDokter').dom.value,
                                        kel_pas     :Ext.getCmp('cboPilihankelompokPasien').getValue(),
                                        kel_pas2    :kel_pas2,
                                        unit        :kd_irna2,
                                        kd_dokter   :Ext.getCmp('cboPilihanDokter').getValue(),
                                        rd_tanggal  :rd_tanggal,
                                        type_file   :type_file,
                                    };
                                    params['kd_pay']            = sendTextDataArrayBayar.substring(0, sendTextDataArrayBayar.length-1);
                                    params['length_kd_pay']     = sendDataArrayBayar.length;
                                    console.log(params);
                                    var form = document.createElement("form");
                                    form.setAttribute("method", "post");
                                    form.setAttribute("target", "_blank");
                                    form.setAttribute("action", baseURL + "index.php/rawat_inap/function_lap_RWI/laporanCetakPelayananDokterDetail");
                                    var hiddenField = document.createElement("input");
                                    hiddenField.setAttribute("type", "hidden");
                                    hiddenField.setAttribute("name", "data");
                                    hiddenField.setAttribute("value", Ext.encode(params));
                                    form.appendChild(hiddenField);
                                    document.body.appendChild(form);
                                    form.submit();  
                                                /*
                                    Ext.Ajax.request({
                                        url: baseURL + "index.php/rawat_inap/control_buka_transaksi/cek_data_laporan_buka_trans",
                                        params: params,
                                        success: function(response) {
                                            var cst  = Ext.decode(response.responseText);
                                            if (cst.status === true) {
                                                var form = document.createElement("form");
                                                form.setAttribute("method", "post");
                                                form.setAttribute("target", "_blank");
                                                form.setAttribute("action", baseURL + "index.php/rawat_inap/control_buka_transaksi/laporan_buka_transaksi");
                                                var hiddenField = document.createElement("input");
                                                hiddenField.setAttribute("type", "hidden");
                                                hiddenField.setAttribute("name", "data");
                                                hiddenField.setAttribute("value", Ext.encode(params));
                                                form.appendChild(hiddenField);
                                                document.body.appendChild(form);
                                                form.submit();  
                                                // frmDlgRWIPelayananDokter.close();
                                                // loadMask.hide();
                                            }else{
                                                ShowPesanWarningPelayananDokter("Tidak ada data", "Information");
                                            }
                                        },
                                    });*/
                                    loadMask.hide();
                            }
                        },
                        {
                            x: 280,
                            y: 0.25,
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWIPelayananDokter',
                            handler: function()
                            {
                                    frmDlgRWIPelayananDokter.close();
                            }
                        }
                    ]
                }
                
            ]
        }
    );

    return PnlLapRWIPelayananDokter;
};

function GetCriteriaRWIPelayananDokter()
{
    var strKriteria = '';

    if (Ext.get('dtpTglAwalLapRWIPelayananDokter').dom.value !== '')
    {
        strKriteria = Ext.get('dtpTglAwalLapRWIPelayananDokter').dom.value;
    };
        if (Ext.get('dtpTglAkhirLapRWIPelayananDokter').dom.value !== '')
    {
        strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRWIPelayananDokter').dom.value;
    };
    if (Ext.getCmp('cboRujukanPelayananDokterSpesialRequestEntry').getValue() !== '' || Ext.getCmp('cboRujukanPelayananDokterSpesialRequestEntry').getValue() !== 'Pilih PelayananDokter...')
    {
        strKriteria += '##@@##' + 'PelayananDokter';
                strKriteria += '##@@##' + Ext.getCmp('cboRujukanPelayananDokterSpesialRequestEntry').getValue();
    };

    return strKriteria;
};


function ValidasiReportRWIPelayananDokter()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWIPelayananDokter').dom.value === '') || (Ext.get('dtpTglAkhirLapRWIPelayananDokter').dom.value === '') || 
        (Ext.get('cboRujukanPelayananDokterSpesialRequestEntry').value === 'Silahkan Pilih...' || Ext.get('cboRujukanPelayananDokterSpesialRequestEntry').value === ' ' ))
    {
            if(Ext.get('dtpTglAwalLapRWIPelayananDokter').dom.value === '')
            {
                    ShowPesanWarningPelayananDokter(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            if(Ext.get('dtpTglAkhirLapRWIPelayananDokter').dom.value === '')
            {
                    ShowPesanWarningPelayananDokter(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.getCmp('cboRujukanPelayananDokterSpesialRequestEntry').getValue() === '' || Ext.getCmp('cboRujukanPelayananDokterSpesialRequestEntry').getValue() === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningPelayananDokter(nmGetValidasiKosong('PelayananDokter'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRWIPelayananDokter()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWIPelayananDokter').dom.value > Ext.get('dtpTglAkhirLapRWIPelayananDokter').dom.value)
    {
        ShowPesanWarningPelayananDokter(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningPelayananDokter(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWIBukaTrans()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 460,
                height: 30,
                anchor: '100% 100%',
                items:
                [
                    
                    {
                        x: 400,
                        y: 1,
                        width:70,
                        xtype: 'checkbox',
                        id: 'CekLapPilihFile',
                        hideLabel:false,
                        boxLabel: 'Excel',
                        checked: false,
                        listeners: 
                        {
                            check: function()
                            {
                               if(Ext.getCmp('CekLapPilihFile').getValue()===true)
                                {
                                    type_file =1;
                                }
                                else
                                {
                                    type_file =0;
                                   
                                }
                            }
                        }
                    },
                ]
            }
        ]
    }
    return items;
};


function mComboUnitLapRWIPelayananDokter()
{
     var cboPelayananDokter = new Ext.form.ComboBox
    (
        {
            id:'cboPelayananDokter',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
            fieldLabel: 'Wilayah ',
            width:80,
                        anchor: '95%',
                        value:1,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[1, 'Prop'],[2, 'Kab'],[3, 'Kec'],[4, 'Kel']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetPelayananDokter,
            listeners:
            {
                'select': function(a,b,c)
                {
                                  selectSetPelayananDokter=b.data.displayText ;
                }
            }
        }
    );
    return cboPelayananDokter;
};

function mcomboPelayananDokterSpesial()
{
    var Field = ['no_PelayananDokter','PelayananDokter'];
    ds_PelayananDokterSpesial_viDaftar = new WebApp.DataStore({fields: Field});

    ds_PelayananDokterSpesial_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewSetupKelasSpesial',
                param: ""
            }
        }
    )

    var cboRujukanPelayananDokterSpesialRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanPelayananDokterSpesialRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih PelayananDokter...',
            fieldLabel: 'PelayananDokter ',
            align: 'Right',
            store: ds_PelayananDokterSpesial_viDaftar,
            valueField: 'no_PelayananDokter',
            displayField: 'PelayananDokter',
            anchor: '95%',
            Width:'800',
            value: 'Semua',
            x: 95,
            y: 1,
            listeners:
                {
                    'select': function(a, b, c)
                {
                                   
                                },
                    'render': function(c)
                                {
                                    
                                }


        }
        }
    )

    return cboRujukanPelayananDokterSpesialRequestEntry;
}

function mComboPilihanKelompokPasien()
{
    var cboPilihankelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 2,
                y: 1,
                id:'cboPilihankelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 120,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihankelompokPasien;
};
function mComboPerseorangan()
{
    var cboPerseorangan = new Ext.form.ComboBox
    (
            {
                x: 140,
                y: 1,
                id:'cboPerseorangan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:150,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
    );
    return cboPerseorangan;
};

function mComboUmum()
{
    var cboUmum = new Ext.form.ComboBox
    (
        {
            x: 140,
            y: 1,
            id:'cboUmum',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            width:150,
            disabled:true,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[1, 'Semua']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:1,
            listeners:
            {
                'select': function(a,b,c)
                {
                                  selectSetUmum=b.data.displayText ;
                }
                                
                            
            }
        }
    );
    return cboUmum;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboLookupCustomer',
                param: 'jenis_cust=1'
            }
        }
    );
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
    (
        {
            x: 140,
            y: 1,
            id: 'cboPerusahaanRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: '',
            align: 'Right',
            store: dsPerusahaanRequestEntry,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            width:150,
            value: 'Semua',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectsetperusahaan = b.data.KD_CUSTOMER;
                    selectsetnamaperusahaan = b.data.CUSTOMER;
                }
            }
        }
    );

    return cboPerusahaanRequestEntry;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2"
            }
        }
    );
    var cboAsuransi = new Ext.form.ComboBox
    (
        {
            x: 140,
            y: 1,
            id:'cboAsuransi',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: '',
            align: 'Right',
            width:150,
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            value: 'Semua',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetAsuransi=b.data.KD_CUSTOMER ;
                    selectsetnamaAsuransi=b.data.CUSTOMER ;
                }
            }
        }
    );
    return cboAsuransi;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseorangan').show();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').show();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').show();
   }
   else
   {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').show();
   }
};

function loadspesialisasi(){
     Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rawat_inap/function_lap_RWI/getSpesialisasi",
            params: {text:''},
            failure: function(o)
            {
                //ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    var recs=[],
                        recType=dataSource_spesialisasi.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        recs.push(new recType(cst.listData[i]));
                    }
                    dataSource_spesialisasi.add(recs);
                }
            }
        }
        
    )
}
function dataGrid_viSummeryPasien()
{
    var Field_poli_viDaftar = ['kd_unit','nama_unit'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
     Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rawat_inap/function_lap_RWI/getUnit",
            params: {text:''},
            failure: function(o)
            {
                //ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    var recs=[],
                        recType=dataSource_unit.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        recs.push(new recType(cst.listData[i]));
                    }
                    dataSource_unit.add(recs);
                }
            }
        }
        
    )
   
    // Generic fields array to use in both store defs.
    var fields = [
        {name: 'kd_unit', mapping : 'kd_unit'},
        {name: 'nama_unit', mapping : 'nama_unit'}
    ];

    
    // Column Model shortcut array
    var cols = [
        { id : 'kd_unit', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'kd_unit',hidden : true},
        {header: "Nama", width: 50, sortable: true, dataIndex: 'nama_unit'}
    ];
    
    var sm = new Ext.grid.CheckboxSelectionModel({
                dataIndex:'check',
                listeners: {
                    selectionchange: function(sm, selected, opts) {
                        var SelectedCheckbox=firstGrid.getSelectionModel();
                        // console.log(SelectedCheckbox);
                        kd_irna='';
                        kd_irna2='';
                        if(SelectedCheckbox.selections.length >0){
                            for(i=0;i<SelectedCheckbox.selections.length;i++){
                                kd_irna = SelectedCheckbox.selections.items[i].data.kd_unit;
                                kd_irna2 = kd_irna2.concat("'",kd_irna,"'",',');
                                
                            }
                            
                            // loadkamar(kd_irna2);
                        }else{
                            dataSource_kamar.removeAll();
                        }
                    }
                }
            });    
    firstGrid = new Ext.grid.GridPanel({
    store            : dataSource_unit,
    autoScroll       : true,
    columnLines      : true,
    border           : true,
    height           : 150,
    stripeRows       : true,
    title            : 'Nama',
    anchor           : '100% 100%',
    plugins          : [new Ext.ux.grid.FilterRow()],
    columns         :
                        [
                            sm,
                            new Ext.grid.RowNumberer(),
                            {
                                id: 'grid_kd_unit',
                                header: 'No.Medrec',
                                dataIndex: 'kd_unit',
                                sortable: true,
                                hidden : true
                            },
                            {
                                id: 'grid_nama_unit',
                                header: 'Nama',
                                dataIndex: 'nama_unit',
                                sortable: true,
                                width: 50
                            }
                        ],
    sm: sm, 
    viewConfig: 
    {
        forceFit: true
    }
    });

    var items =
    {
        layout:'column',
        border:false,
        bodyStyle: 'padding: 0px 0px 0px 0px',
        items:
        [
            // {
            //     xtype : 'fieldset',
            //     layout:'column',
            //     border:true,
            //     title:'Unit',
            //     width:420,
            //     height:165,
            //     items:[
                        {
                            columnWidth: .90,
                            layout: 'form',
                            border: false,
                            autoScroll: true,
                            bodyStyle: 'padding: 5px 5px 5px 5px',
                            items:
                            [ firstGrid]
                        },      
                // ]
            // },
            // {
            //     layout:'column',
            //     border:false,
            //     width:10,
            //     height:165,
            // },    
                            
            ]
    }
    return items;

}

function kelompok_pasien(){
    var items =
    {
        layout:'column',
            border:false,
            items:[
                    {   
                        xtype : 'fieldset',
                        layout:'absolute',
                        border:true,
                        title:'Kelompok Pasien',
                        height:65,
                        width:420,
                        padding: '5px 5px 5px 5px',
                        items:
                        [
                            mComboPilihanKelompokPasien(),
                            mComboPerseorangan(),
                            mComboAsuransi(),
                            mComboPerusahaan(),
                            mComboUmum(),
                            {
                                x: 300,
                                y: 1,
                                xtype: 'checkbox',
                                id: 'CekLapPilihFile',
                                hideLabel:false,
                                boxLabel: 'Excel',
                                checked: false,
                                listeners: 
                                {
                                    check: function()
                                    {
                                       if(Ext.getCmp('CekLapPilihFile').getValue()===true)
                                        {
                                            type_file =1;
                                        }
                                        else
                                        {
                                            type_file =0;
                                           
                                        }
                                    }
                                }
                            } 
                        ]
                    },
                    
            ]
        
    }
    return items;
}



function loadkamar(kd_irna2){
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rawat_inap/function_lap_RWI/getKamarRekap",
            params: {kd_irna2:kd_irna2},
            failure: function(o)
            {
                //ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                dataSource_kamar.removeAll();
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    var recs=[],
                        recType=dataSource_kamar.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        recs.push(new recType(cst.listData[i]));
                    }
                    dataSource_kamar.add(recs);
                }
            }
        }
        
    )
}

function loadbayar(){
     Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rawat_inap/function_lap_RWI/getPayment",
            params: {text:''},
            failure: function(o)
            {
                //ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    var recs=[],
                        recType=dataSource_bayar.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        recs.push(new recType(cst.listData[i]));
                    }
                    dataSource_bayar.add(recs);
                }
            }
        }
        
    )
}
function kriteria_kamar_bayar(){
    var Field_kamar_viDaftar = ['no_kamar','nama_kamar'];
    dataSource_kamar = new WebApp.DataStore({fields: Field_kamar_viDaftar});
    // loadkamar(kd_spesial,kd_kelas);
   
    // Generic fields array to use in both store defs.
    var fields = [
        {name: 'no_kamar', mapping : 'no_kamar'},
        {name: 'nama_kamar', mapping : 'nama_kamar'}
    ];

    
    // Column Model shortcut array
    var cols_kamar = [
        { id : 'no_kamar', header: "Kamar", width: 160, sortable: true, dataIndex: 'no_kamar',hidden : true},
        {header: "Kamar", width: 50, sortable: true, dataIndex: 'nama_kamar'}
    ];


    // declare the source Grid
    firstGrid_kamar = new Ext.grid.GridPanel({
    ddGroup          : 'secondGridDDGroup',
    store            : dataSource_kamar,
    autoScroll       : true,
    columnLines      : true,
    border           : true,
    enableDragDrop   : true,
    height           : 150,
    stripeRows       : true,
    trackMouseOver   : true,
    title            : 'Nama',
    anchor           : '100% 100%',
    plugins          : [new Ext.ux.grid.FilterRow()],
    colModel         : new Ext.grid.ColumnModel
                        (
                            [
                                new Ext.grid.RowNumberer(),
                                {
                                        id: 'colNRM_viDaftar',
                                        header: 'No.Medrec',
                                        dataIndex: 'no_kamar',
                                        sortable: true,
                                        hidden : true
                                },
                                {
                                        id: 'colNMPASIEN_viDaftar',
                                        header: 'Nama',
                                        dataIndex: 'nama_kamar',
                                        sortable: true,
                                        width: 50
                                }
                            ]
                        ),
    viewConfig: 
    {
        forceFit: true
    }
    });

    secondGridStore_kamar = new Ext.data.JsonStore({
        fields : fields,
        root   : 'records'
    });

    // create the destination Grid
    secondGrid_kamar = new Ext.grid.GridPanel({
        ddGroup          : 'firstGrid_kamarDDGroup',
        store            : secondGridStore_kamar,
        columns          : cols_kamar,
        enableDragDrop   : true,
        height           : 150,
        stripeRows       : true,
        autoExpandColumn : 'no_kamar',
        title            : 'Nama',
        listeners        : {
            afterrender : function(comp) {
            var secondGridDropTargetEl = secondGrid_kamar.getView().scroller.dom;
            var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                    ddGroup    : 'secondGridDDGroup',
                    notifyDrop : function(ddSource, e, data){
                            var records =  ddSource.dragData.selections;
                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                            secondGrid_kamar.store.add(records);
                            secondGrid_kamar.store.sort('no_kamar', 'ASC');
                            return true
                    }
            });
            }
        },
        viewConfig: 
        {
            forceFit: true
        }
    });
    /* grid pembayaran*/
    var Field_bayar_viDaftar = ['kd_pay','uraian'];
    dataSource_bayar = new WebApp.DataStore({fields: Field_bayar_viDaftar});
    loadbayar();
   
    
    // Generic fields array to use in both store defs.
    var fields_bayar = [
        {name: 'kd_pay', mapping : 'kd_pay'},
        {name: 'uraian', mapping : 'uraian'}
    ];

    
    // Column Model shortcut array
    var cols_bayar = [
        { id : 'kd_pay', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'kd_pay',hidden : true},
        {header: "Uraian", width: 50, sortable: true, dataIndex: 'uraian'}
    ];


    // declare the source Grid
    firstGrid_bayar = new Ext.grid.GridPanel({
    ddGroup          : 'secondGridDDGroup',
    store            : dataSource_bayar,
    autoScroll       : true,
    columnLines      : true,
    border           : true,
    enableDragDrop   : true,
    height           : 130,
    stripeRows       : true,
    trackMouseOver   : true,
    title            : 'Uraian',
    anchor           : '100% 100%',
    plugins          : [new Ext.ux.grid.FilterRow()],
    colModel         : new Ext.grid.ColumnModel
                        (
                            [
                                new Ext.grid.RowNumberer(),
                                {
                                        id: 'colNRM_viDaftar',
                                        header: 'No.Medrec',
                                        dataIndex: 'kd_pay',
                                        sortable: true,
                                        hidden : true
                                },
                                {
                                        id: 'colNMPASIEN_viDaftar',
                                        header: 'Uraian',
                                        dataIndex: 'uraian',
                                        sortable: true,
                                        width: 50
                                }
                            ]
                        ),
    viewConfig: 
    {
        forceFit: true
    }
    });

    secondGridStore_bayar = new Ext.data.JsonStore({
        fields : fields_bayar,
        root   : 'records'
    });

    // create the destination Grid
    secondGrid_bayar = new Ext.grid.GridPanel({
        ddGroup          : 'firstGrid_kamarDDGroup',
        store            : secondGridStore_bayar,
        columns          : cols_bayar,
        enableDragDrop   : true,
        height           : 130,
        stripeRows       : true,
        autoExpandColumn : 'kd_pay',
        title            : 'Uraian',
        listeners        : {
            afterrender : function(comp) {
            var secondGridDropTargetEl = secondGrid_bayar.getView().scroller.dom;
            var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                    ddGroup    : 'secondGridDDGroup',
                    notifyDrop : function(ddSource, e, data){
                            var records =  ddSource.dragData.selections;
                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                            secondGrid_bayar.store.add(records);
                            secondGrid_bayar.store.sort('kd_pay', 'ASC');
                            return true
                    }
            });
            }
        },
        viewConfig: 
        {
            forceFit: true
        }
    });
    
    var items =
    {
        layout:'column',
        border:false,
        items:
        [/*
            {
                xtype : 'fieldset',
                layout:'column',
                border:true,
                title:'Kelompok Kamar',
                width:360,
                items:[
                        {
                            columnWidth: .5,
                            layout: 'form',
                            border: false,
                            autoScroll: true,
                            bodyStyle: 'padding: 5px 5px 5px 5px',
                            items:
                            [ firstGrid_kamar]
                        },
                        {
                            columnWidth: .5,
                            layout: 'form',
                            border: false,
                            autoScroll: true,
                            bodyStyle: 'padding: 5px 5px 0px 0px',
                            items:
                            [secondGrid_kamar]
                        },
                ]
                
            },
            {
                layout:'column',
                border:false,
                width:10,
                height:180,
            },*/
            {
                xtype : 'fieldset',
                layout:'column',
                border:true,
                title:'Cara Pembayaran',
                bodyStyle: 'padding: 0px 0px 0px 6px',
                width:430,
                items:[
                        {
                            columnWidth: .5,
                            layout: 'form',
                            border: false,
                            autoScroll: true,
                            bodyStyle: 'padding: 5px 5px 5px 5px',
                            items:
                            [ firstGrid_bayar]
                        },
                        {
                            columnWidth: .5,
                            layout: 'form',
                            border: false,
                            autoScroll: true,
                            bodyStyle: 'padding: 5px 5px 0px 0px',
                            items:
                            [secondGrid_bayar]
                        },
                ]
                
            },
            
        ]
    }
    return items;

}

function kriteria_pilih_bawah(){
    var items =
    {
        layout: 'absolute',
        border:false,
        width:750,
        height: 30,
        items:
        [
            /*{
                x:10,
                y:7,
                xtype: 'checkbox',
                id: 'CekLapPilihSemuaKamar',
                hideLabel:false,
                boxLabel: 'Pilih Semua Kamar',
                checked: false,
                listeners: 
               {
                    check: function()
                    {
                       if(Ext.getCmp('CekLapPilihSemuaKamar').getValue()===true)
                        {
                             firstGrid_kamar.getSelectionModel().selectAll();
                             //first grid kamar
                        }
                        else
                        {
                             //first grid kamar
                            // firstGrid.getSelectionModel().clearSelections();
                        }
                    }
               }
            },
            
            {
                x:170,
                y:5,
                xtype: 'button',
                text: 'Reset Kamar',
                id: 'buttonresetkamar',
                handler: function()
                {
                    secondGridStore_kamar.removeAll();
                    loadkamar(kd_spesial,kd_kelas);
                    Ext.getCmp('CekLapPilihSemuaKamar').setValue(false);
                    // datarefresh_viInformasiUnitSpesial();
                }
            },*/
            
            {
                x:350,
                y:7,
                xtype: 'checkbox',
                id: 'CekLapPilihSemuaBayar',
                hideLabel:false,
                boxLabel: 'Pilih Semua Pembayaran',
                checked: false,
                listeners: 
               {
                    check: function()
                    {
                       if(Ext.getCmp('CekLapPilihSemuaBayar').getValue()===true)
                        {
                             firstGrid_bayar.getSelectionModel().selectAll();
                        }
                    }
               }
            },
            {
                x:550,
                y:5,
                xtype: 'button',
                text: 'Reset Pembayaran',
                id: 'buttonresetbayar',
                handler: function()
                {
                    secondGridStore_bayar.removeAll();
                    loadbayar();
                    Ext.getCmp('CekLapPilihSemuaBayar').setValue(false);
                }
            },
            /*{
                x:690,
                y:7,
                xtype: 'checkbox',
                id: 'CekLapPilihFile',
                hideLabel:false,
                boxLabel: 'Excel',
                checked: false,
                listeners: 
                {
                    check: function()
                    {
                       if(Ext.getCmp('CekLapPilihFile').getValue()===true)
                        {
                            type_file =1;
                        }
                        else
                        {
                            type_file =0;
                           
                        }
                    }
                }
            },*/
        ]
    }
    return items;
}

function kriteria_pilih_atas(){
    var items =
    {
        layout:'column',
        border:false,
        width:370,
        height:60,
        bodyStyle: 'padding: 5px 5px 5px 5px',
        items:
        [
            {
                xtype : 'fieldset',
                layout:'column',
                border:true,
                title:'Pilihan criter tanggal ',
                width:370,
                bodyStyle: 'padding: 5px 5px 5px 5px',
                height:60,
                items:[
                    {
                        xtype: 'radiogroup',
                        id: 'rbrujukan',
                        fieldLabel: '',
                        items: 
                        [
                            
                            {
                                boxLabel: 'Tgl Buka Trans',
                                name: 'rb_auto',
                                id: 'rd_tgl_buka',
                                checked: true,
                                inputValue: '1'
                            },
                            {
                                boxLabel: 'Tgl Masuk',
                                name: 'rb_auto',
                                id: 'rd_tgl_masuk',
                                inputValue: '2'
                            },
                            {
                                boxLabel: 'Tgl Keluar',
                                name: 'rb_auto',
                                id: 'rd_tgl_keluar',
                                inputValue: '3'
                            }
                        ],
                        listeners: {
                            change: function(field, newValue, oldValue) {
                                // console.log(newValue.inputValue);
                                rd_tanggal = newValue.inputValue;
                            }
                        }
                    } 
                ]
            },
        ]
    }
    return items;
}

function kriteria_laporan(){
    var items =
    {
        layout:'column',
        border:false,
        width:420,
        height:60,
        bodyStyle: 'padding: 5px 5px 5px 5px',
        items:
        [
            {
                xtype : 'fieldset',
                layout:'column',
                border:true,
                title:'Type File',
                width:420,
                bodyStyle: 'padding: 5px 5px 5px 5px',
                height:60,
                items:[
                    {
                        xtype: 'checkbox',
                        id: 'CekLapPilihFile',
                        hideLabel:false,
                        boxLabel: 'Excel',
                        checked: false,
                        listeners: 
                        {
                            check: function()
                            {
                               if(Ext.getCmp('CekLapPilihFile').getValue()===true)
                                {
                                    type_file =1;
                                }
                                else
                                {
                                    type_file =0;
                                   
                                }
                            }
                        }
                    } 
                ]
            },
        ]
    }
    return items;
}

function kriteria_dokter_sett(){
    var items =
    {
        layout:'column',
        border:false,
        width:420,
        height:70,
        bodyStyle: 'padding: 0px 0px 0px 0px',
        items:
        [
            {
                xtype : 'fieldset',
                layout:'column',
                border:true,
                title:'Dokter',
                width:420,
                bodyStyle: 'padding: 0px 0px 0px 0px',
                height:70,
                items:[
                    mComboDokter(),
                ]
            },
        ]
    }
    return items;
}

function kriteria_tanggal_sett(){
    var items =
    {
        layout:'column',
        border:false,
        width:420,
        height:75,
        bodyStyle: 'padding: 0px 0px 0px 0px',
        items:
        [
            {
                xtype : 'fieldset',
                layout:'column',
                border:true,
                title:'Periode',
                width:420,
                bodyStyle: 'padding: 5px 5px 5px 5px',
                height:75,
                items:[
                    {
                        width:120,
                        xtype: 'datefield',
                        id: 'dtpTglAwalLapRWIPelayananDokter',
                        format: 'd-M-Y',
                        value:now
                    },
                    {
                        width:110,
                        xtype: 'label',
                    },
                    {
                        width:120,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirLapRWIPelayananDokter',
                        format: 'd-M-Y',
                        value:now
                    },
                ]
            },
        ]
    }
    return items;
}

function kriteria_pembayaran_sett(){
    var items =
    {
        layout:'column',
        border:false,
        width:420,
        height:60,
        bodyStyle: 'padding: 0px 0px 0px 0px',
        items:
        [
            {
                xtype : 'fieldset',
                layout:'absolute',
                border:true,
                title:'Setting Pembayaran',
                width:420,
                bodyStyle: 'padding: 5px 5px 5px 5px',
                height:60,
                items:[
                    {
                        x   : 25,
                        y   : 1,
                        xtype: 'checkbox',
                        id: 'CekLapPilihSemuaBayar',
                        hideLabel:false,
                        boxLabel: 'Pilih Semua Pembayaran',
                        bodyStyle: 'margin-right: 25px',
                        checked: false,
                        listeners: 
                        {
                            check: function()
                            {
                               if(Ext.getCmp('CekLapPilihSemuaBayar').getValue()===true)
                                {
                                     firstGrid_bayar.getSelectionModel().selectAll();
                                }
                            }
                        }
                    },
                    {
                        x   : 240,
                        y   : 1,
                        xtype: 'button',
                        text: 'Reset Pembayaran',
                        id: 'buttonresetbayar',
                        handler: function()
                        {
                            secondGridStore_bayar.removeAll();
                            loadbayar();
                            Ext.getCmp('CekLapPilihSemuaBayar').setValue(false);
                        }
                    } 
                ]
            },
        ]
    }
    return items;
}
function mComboSpesialis()
{
    var Field = ['kd_unit','nama_unit'];
    dsSpesialisasi = new WebApp.DataStore({fields: Field});
    dsSpesialisasi.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target:'ViewSetupUnitSpesial',
                param: '1' //spesialisasi
            }
        }
    );
    var cboSpesialisasi = new Ext.form.ComboBox
    (
        {
            x: 5,
            y: 0.25,
            id: 'cboSpesialisasi',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'',
            fieldLabel: '',
            align: 'Right',
            store: dsSpesialisasi,
            valueField: 'kd_unit',
            displayField: 'nama_unit',
            width:150,
            value: 'Semua',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectsetspesialisasi = b.data.kd_unit;
                    kd_spesial = b.data.kd_unit;
                    load_datakelas(kd_spesial,kd_irna2);
                }
            }
        }
    );

    return cboSpesialisasi;
};

function load_datakelas(param,param2){
     Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rawat_inap/function_lap_RWI/getKelas",
            params: {kd_spesial:param,kd_unit:param2},
            failure: function(o)
            {
                //ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                dsKelas.removeAll();
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    var recs=[],
                        recType=dsKelas.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        recs.push(new recType(cst.listData[i]));
                    }
                    dsKelas.add(recs);
                }
            }
        }
        
    )
}
function mComboKelas()
{
    var Field = ['kd_unit','nama_unit'];
    dsKelas = new WebApp.DataStore({fields: Field});
   
    var cboKelas = new Ext.form.ComboBox
    (
        {
            x: 5,
            y: 0.25,
            id: 'cboKelas',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'',
            fieldLabel: '',
            align: 'Right',
            store: dsKelas,
            valueField: 'kd_unit',
            displayField: 'nama_unit',
            width:125,
            value: 'Semua',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectsetkelas= b.data.kd_unit;
                    selectsetkelas = b.data.nama_unit;
                    kd_kelas = b.data.kd_unit;
                    loadkamar(kd_spesial, kd_kelas);
                }
            }
        }
    );

    return cboKelas;
};

function mComboDokter()
{

   var Field = ['kd_dokter','nama'];
    ds_Dokter_viDaftar = new WebApp.DataStore({fields: Field});
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rawat_inap/function_lap_RWI/getDokter",
            params: {text:''},
            failure: function(o)
            {
                //ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    
                    var recs=[],
                        recType=ds_Dokter_viDaftar.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        recs.push(new recType(cst.listData[i]));
                    }
                    ds_Dokter_viDaftar.add(recs);
                }
            }
        }
        
    )
    
    var cboPilihanDokter = new Ext.form.ComboBox
    (
        {
            x: 5,
            y: 0.25,
            id: 'cboPilihanDokter',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Dokter ...',
            fieldLabel: 'Dokter ',
            align: 'Right',
            sorters: [{
                property: 'nama',
                direction: 'ASC'
            }],
            store: ds_Dokter_viDaftar,
            valueField: 'kd_dokter',
            displayField: 'nama',
            value:'Semua',
            width:360,
            tabIndex:29,
            listeners:
            {
                    'select': function(a, b, c)
                    {
                        //Dokterpilihanpasien=b.data.KD_UNIT;
                    },


            }
        }
    );
    return cboPilihanDokter;
};

function mComboKelasProduk()
{

   var Field = ['kd_klas','klasifikasi'];
    ds_kelas_produk = new WebApp.DataStore({fields: Field});

    
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rawat_inap/function_lap_RWI/get_kelas_produk",
            params: {text:''},
            failure: function(o)
            {
                //ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    
                    var recs=[],
                        recType=ds_kelas_produk.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        recs.push(new recType(cst.listData[i]));
                    }
                    ds_kelas_produk.add(recs);
                }
            }
        }
        
    )
      
    var cboKelasProduk = new Ext.form.ComboBox
    (
        {
            x: 5,
            y: 0.25,
            id: 'cboKelasProduk',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Dokter ...',
            fieldLabel: 'Dokter ',
            align: 'Right',
            sorters: [{
                property: 'klasifikasi',
                direction: 'ASC'
            }],
            store: ds_kelas_produk,
            valueField: 'kd_klas',
            displayField: 'klasifikasi',
            value:'Semua',
            width:150,
            tabIndex:29,
            listeners:
            {
                    'select': function(a, b, c)
                    {
                        //Dokterpilihanpasien=b.data.KD_UNIT;
                    },


            }
        }
    );
    return cboKelasProduk;
};

function mComboOperator()
{

   var Field = ['kd_user','user_names'];
    ds_operator = new WebApp.DataStore({fields: Field});

    
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rawat_inap/function_lap_RWI/getUser",
            params: {text:''},
            failure: function(o)
            {
                //ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    
                    var recs=[],
                        recType=ds_operator.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        recs.push(new recType(cst.listData[i]));
                    }
                    ds_operator.add(recs);
                }
            }
        }
        
    )
      
    var cboOperator = new Ext.form.ComboBox
    (
        {
            x: 5,
            y: 0.25,
            id: 'cboOperator',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Dokter ...',
            fieldLabel: 'Dokter ',
            align: 'Right',
            sorters: [{
                property: 'user_names',
                direction: 'ASC'
            }],
            store: ds_operator,
            valueField: 'kd_user',
            displayField: 'user_names',
            value:'Semua',
            width:160,
            tabIndex:29,
            listeners:
            {
                    'select': function(a, b, c)
                    {
                        //Dokterpilihanpasien=b.data.KD_UNIT;
                    },


            }
        }
    );
    return cboOperator;
};

