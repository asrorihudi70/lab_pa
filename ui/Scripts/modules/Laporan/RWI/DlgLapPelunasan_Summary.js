
var dsRWILapPelunasan_Summary;
var selectRWILapPelunasan_Summary;
var selectNamaRWILapPelunasan_Summary;
var now = new Date();
var selectSetResponTime;
var frmDlgRWILapPelunasan_Summary;
var varLapRWILapPelunasan_Summary= ShowFormLapRWILapPelunasan_Summary();
var type_file=0;
function ShowFormLapRWILapPelunasan_Summary()
{
    frmDlgRWILapPelunasan_Summary= fnDlgRWILapPelunasan_Summary();
    frmDlgRWILapPelunasan_Summary.show();
};

function fnDlgRWILapPelunasan_Summary()
{
    var winRWILapPelunasan_SummaryReport = new Ext.Window
    (
        {
            id: 'winRWILapPelunasan_SummaryReport',
            title: 'Laporan Pelunasan Pasien',
            closeAction: 'destroy',
            width:420,
            height: 420,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				
				ItemDlgRWILapPelunasan_Summary()
			],
			fbar:[
				{
					x: 175,
					y: 1,
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapRWILapPelunasan_Summary',
					handler: function(){
						//if (ValidasiReportRWILapPelunasan_Summary() === 1){
							var kel_pas2;
							if(Ext.getCmp('cboPilihankelompokPasienRWILapPelunasan_Summary').getValue() ==1){
								kel_pas2='Semua';
							}else{
								if(Ext.getCmp('cboPerseoranganRWILapPelunasan_Summary').getValue() != ''){
									kel_pas2 = Ext.getCmp('cboPerseoranganRWILapPelunasan_Summary').getValue();
								}
								if(Ext.getCmp('cboAsuransiRWILapPelunasan_Summary').getValue() != ''){
									kel_pas2 = Ext.getCmp('cboAsuransiRWILapPelunasan_Summary').getValue();
								}
								if(Ext.getCmp('cboPerusahaanRWILapPelunasan_Summary').getValue() != ''){
									kel_pas2 = Ext.getCmp('cboPerusahaanRWILapPelunasan_Summary').getValue();
								}
								
							}


							var SelectedCheckbox=firstGridUnitRS.getSelectionModel();
							kd_irna  = '';
							kd_irna2 = '';
							if(SelectedCheckbox.selections.length >0){
								for(i=0;i<SelectedCheckbox.selections.length;i++){
									kd_irna = SelectedCheckbox.selections.items[i].data.kd_unit;
									// kd_irna2 = kd_irna2.concat("'",kd_irna,"'",',');
									kd_irna2 += "'"+kd_irna+"',";
									
								}
								kd_irna2 = kd_irna2.substr(0, kd_irna2.length - 1);
								// loadkamar(kd_irna2);
							}else{
								dataSource_kamar.removeAll();
							}
							var sendTmpDataArrayKamar = "";

					        secondGridStore_kamar.each(function(record){
								var recordArray = [record.get("no_kamar")];
								// sendDataArrayUnit.push(recordArray);
								// console.log(record);
					            sendTmpDataArrayKamar += "'"+recordArray+"',";
					            /*var recordArrayNama   = [record.get("NAMA_UNIT")];
					            sendDataArrayNamaUnit.push(recordArrayNama);*/
					        });
					        sendTmpDataArrayKamar.substr(0, sendTmpDataArrayKamar.length - 1);
							var params={
								tgl_awal 	: Ext.get('dtpTglAwalLapRWILapPelunasan_Summary').dom.value,
								tgl_akhir 	: Ext.get('dtpTglAkhirLapRWILapPelunasan_Summary').dom.value,
								kel_pas 	: Ext.getCmp('cboPilihankelompokPasienRWILapPelunasan_Summary').getValue(),
								kd_irna 	: kd_irna2,
								kel_pas2 	: kel_pas2,
								type_file 	: type_file,
								no_kamar 	: sendTmpDataArrayKamar,
							};


							if(SelectedCheckbox.selections.length >0){
								console.log(params);
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/rawat_inap/function_lap_RWI/cetak_lap_pelunasan_summary");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();	
							}else{

							}
							loadMask.hide();
						//}
					}
				},
				{
					x: 250,
					y: 1,
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRWILapPelunasan_Summary',
					handler: function()
					{
							frmDlgRWILapPelunasan_Summary.close();
					}
				}
			]

        }
    );

    return winRWILapPelunasan_SummaryReport;
};

function getItemKelompokPasienRWILapPelunasan_Summary(){
	var items =
    {
			layout:'column',
			border:false,
			anchor: '100%',
			padding: '5px 5px 5px 0px',
			items:[
					{	
						xtype : 'fieldset',
						layout:'absolute',
						border:true,
						// title:'Kelompok Pasien',
						height:150,
						width:550,
						padding: '15px 15px 15px 15px',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Tanggal '
							},
							{
								x: 100,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 110,
								y: 10,
								xtype: 'datefield',
								id: 'dtpTglAwalLapRWILapPelunasan_Summary',
								format: 'd-M-Y',
								value:now
							},
							{
								x: 220,
								y: 10,
								xtype: 'label',
								text: 's/d'
							},
							{
								x: 240,
								y: 10,
								xtype: 'datefield',
								id: 'dtpTglAkhirLapRWILapPelunasan_Summary',
								format: 'd-M-Y',
								value:now
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Kelompok Pasien'
							},
							{
								x: 100,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							mComboPilihanKelompokPasienRWILapPelunasan_Summary(),
							mComboPerseoranganRWILapPelunasan_Summary(),
							mComboAsuransiRWILapPelunasan_Summary(),
							mComboPerusahaanRWILapPelunasan_Summary(),
							mComboUmumRWILapPelunasan_Summary(),
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Tipe File'
							},
							{
								x: 100,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 110,
								y: 100,
								xtype: 'checkbox',
								id: 'CekLapPilihFile',
								hideLabel:false,
								boxLabel: 'Excel',
								checked: false,
								listeners: 
								{
									check: function()
									{
									   if(Ext.getCmp('CekLapPilihFile').getValue()===true)
										{
											type_file =1;
										}
										else
										{
											type_file =0;
										   
										}
									}
								}
							}
						]
					},
					
			]
		
    }
    return items;
}

function mComboPilihanKelompokPasienRWILapPelunasan_Summary()
{
	var cbPilihankelompokPasienRWILapPelunasan_Summary = new Ext.form.ComboBox
	(
            {
                x: 110,
                y: 40,
                id:'cboPilihankelompokPasienRWILapPelunasan_Summary',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 130,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[0, 'Semua'], [1, 'Perseorangan'],[2, 'Perusahaan'], [3, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:0,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cbPilihankelompokPasienRWILapPelunasan_Summary;
};
function mComboPerseoranganRWILapPelunasan_Summary()
{
    var cbPerseoranganRWILapPelunasan_Summary = new Ext.form.ComboBox
	(
            {
                x: 110,
                y: 70,
                id:'cboPerseoranganRWILapPelunasan_Summary',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cbPerseoranganRWILapPelunasan_Summary;
};

function mComboUmumRWILapPelunasan_Summary()
{
    var cbUmumRWILapPelunasan_Summary = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 70,
			id:'cboUmumRWILapPelunasan_Summary',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			disabled:true,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cbUmumRWILapPelunasan_Summary;
};

function mComboPerusahaanRWILapPelunasan_Summary()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRWILapPelunasan_Summary = new WebApp.DataStore({fields: Field});
    dsPerusahaanRWILapPelunasan_Summary.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1'
			}
		}
	);
    var cbPerusahaanRWILapPelunasan_Summary = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 70,
		    id: 'cboPerusahaanRWILapPelunasan_Summary',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRWILapPelunasan_Summary,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: 'Semua',
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cbPerusahaanRWILapPelunasan_Summary;
};

function mComboAsuransiRWILapPelunasan_Summary()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'ASC',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2"
            }
        }
    );
    var cbAsuransiRWILapPelunasan_Summary = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 70,
			id:'cboAsuransiRWILapPelunasan_Summary',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: 'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cbAsuransiRWILapPelunasan_Summary;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRWILapPelunasan_Summary').show();
        Ext.getCmp('cboAsuransiRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboPerusahaanRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboUmumRWILapPelunasan_Summary').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboAsuransiRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboPerusahaanRWILapPelunasan_Summary').show();
        Ext.getCmp('cboUmumRWILapPelunasan_Summary').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboAsuransiRWILapPelunasan_Summary').show();
        Ext.getCmp('cboPerusahaanRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboUmumRWILapPelunasan_Summary').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboAsuransiRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboPerusahaanRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboUmumRWILapPelunasan_Summary').show();
		Ext.getCmp('cboPerseoranganRWILapPelunasan_Summary').setValue('Semua');
		Ext.getCmp('cboAsuransiRWILapPelunasan_Summary').setValue('Semua');
		Ext.getCmp('cboPerusahaanRWILapPelunasan_Summary').setValue('Semua');
   }
   else
   {
        Ext.getCmp('cboPerseoranganRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboAsuransiRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboPerusahaanRWILapPelunasan_Summary').hide();
        Ext.getCmp('cboUmumRWILapPelunasan_Summary').show();
   }
};

function gridUnitRWILapPelunasan_Summary(){
	var Field_poli_viDaftar = ['kd_unit','nama_unit'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/rawat_inap/function_lap_RWI/getUnit",
			params: {text:''},
			failure: function(o)
			{
				//ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dataSource_unit.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_unit.add(recs);
				}
			}
		}
		
	)
   
    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'kd_unit', mapping : 'kd_unit'},
		{name: 'nama_unit', mapping : 'nama_unit'}
	];

	
	// Column Model shortcut array
	var cols = [
		{ id : 'kd_unit', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'kd_unit',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'nama_unit'}
	];
	
	var sm = new Ext.grid.CheckboxSelectionModel({
				dataIndex:'check',
				listeners: {
					selectionchange: function(sm, selected, opts) {
						var SelectedCheckbox=firstGridUnitRS.getSelectionModel();
						kd_irna='';
						kd_irna2='';
						if(SelectedCheckbox.selections.length >0){
							for(i=0;i<SelectedCheckbox.selections.length;i++){
								kd_irna = SelectedCheckbox.selections.items[i].data.kd_unit;
								kd_irna2 = kd_irna2.concat("'",kd_irna,"'",',');
								
							}
							
							loadkamar(kd_irna2);
						}else{
							dataSource_kamar.removeAll();
						}
					}
				}
			});    
	firstGridUnitRS = new Ext.grid.GridPanel({
	store            : dataSource_unit,
	autoScroll       : true,
	columnLines      : true,
	border           : true,
	height           : 150,
	stripeRows       : true,
	title            : 'Unit RS',
	anchor           : '100% 100%',
	plugins          : [new Ext.ux.grid.FilterRow()],
	columns         :
		[
			sm,
			new Ext.grid.RowNumberer(),
			{
				id: 'grid_kd_unit',
				header: 'kd_unit',
				dataIndex: 'kd_unit',
				sortable: true,
				hidden : true
			},
			{
				id: 'grid_nama_unit',
				header: 'Nama Unit RS',
				dataIndex: 'nama_unit',
				sortable: true,
				width: 50
			}
		],
	sm: sm,	
	viewConfig: 
	{
		forceFit: true
	}
	});
	
	var Field_kamar_viDaftar = ['no_kamar','nama_kamar'];
    dataSource_kamar = new WebApp.DataStore({fields: Field_kamar_viDaftar});
    // loadkamar(kd_spesial,kd_kelas);
   
    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'no_kamar', mapping : 'no_kamar'},
		{name: 'nama_kamar', mapping : 'nama_kamar'}
	];

	
	// Column Model shortcut array
	var cols_kamar = [
		{ id : 'no_kamar', header: "Kamar", width: 160, sortable: true, dataIndex: 'no_kamar',hidden : true},
		{header: "Kamar", width: 50, sortable: true, dataIndex: 'nama_kamar'}
	];


	// declare the source Grid
	firstGrid_kamar = new Ext.grid.GridPanel({
	ddGroup          : 'secondGridDDGroup',
	store            : dataSource_kamar,
	autoScroll       : true,
	columnLines      : true,
	border           : true,
	enableDragDrop   : true,
	height           : 150,
	stripeRows       : true,
	trackMouseOver   : true,
	title            : 'Nama Kamar',
	anchor           : '100% 100%',
	plugins          : [new Ext.ux.grid.FilterRow()],
	colModel         : new Ext.grid.ColumnModel
						(
							[
								new Ext.grid.RowNumberer(),
								{
										id: 'colNRM_viDaftar',
										header: 'No.Medrec',
										dataIndex: 'no_kamar',
										sortable: true,
										hidden : true
								},
								{
										id: 'colNMPASIEN_viDaftar',
										header: 'Nama',
										dataIndex: 'nama_kamar',
										sortable: true,
										width: 50
								}
							]
						),
	viewConfig: 
	{
		forceFit: true
	}
	});

	secondGridStore_kamar = new Ext.data.JsonStore({
		fields : fields,
		root   : 'records'
	});

	// create the destination Grid
	secondGrid_kamar = new Ext.grid.GridPanel({
		ddGroup          : 'firstGrid_kamarDDGroup',
		store            : secondGridStore_kamar,
		columns          : cols_kamar,
		enableDragDrop   : true,
		height           : 150,
		stripeRows       : true,
		autoExpandColumn : 'no_kamar',
		title            : 'Nama kamar yang dipilih',
		listeners 		 : {
			afterrender : function(comp) {
			var secondGridDropTargetEl = secondGrid_kamar.getView().scroller.dom;
			var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'secondGridDDGroup',
					notifyDrop : function(ddSource, e, data){
							var records =  ddSource.dragData.selections;
							Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
							secondGrid_kamar.store.add(records);
							secondGrid_kamar.store.sort('no_kamar', 'ASC');
							return true
					}
			});
			}
		},
		viewConfig: 
		{
			forceFit: true
		}
	});
	var FrmTabs_viInformasiUnitRWILapPelunasan_Summary = new Ext.Panel
	(
		{
		    id: 'pilihunitLapRWILapPelunasan_Summary',
		    closable: true,
		    // region: 'center',
		    layout: 'column',
            height   : 165,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: true,
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    // iconCls: '',
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 5px 5px 5px 5px',
					anchor: '100% 100%',
					items:
					[
						firstGridUnitRS	
					]
				},/*{
					columnWidth: .35,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 5px 5px 5px 5px',
					anchor: '100% 100%',
					items:
					[
						// firstGrid_kamar
					]
				},
				{
					columnWidth: .35,
					layout: 'form',
					bodyStyle: 'padding: 5px 5px 5px 5px',
					border: false,
					anchor: '100% 100%',
					items:
					[
						// secondGrid_kamar
					]
				},*/
			]
		}
	)
	return FrmTabs_viInformasiUnitRWILapPelunasan_Summary;
}

function getItemPilihSemuaKamarRWILapPelunasan_Summary(){
	var items =
    {
			layout:'column',
			border:false,
			anchor: '100%',
            // bodyStyle: 'padding:5px',
			padding: '5px 5px 0px 0px',
			items:[
					{	
						xtype : 'fieldset',
						layout:'absolute',
						border:true,
						title:'',
						height:55,
						width:550,
						padding: '0px 0px 0px 15px',
						items:
						[
							{
								x:165,
								y:10,
								xtype: 'checkbox',
								id: 'CekLapPilihLapRWILapPelunasan_Summary',
								hideLabel:false,
								boxLabel: 'Pilih Semua Kamar',
								checked: false,
								listeners: 
								{
									check: function()
									{
									   if(Ext.getCmp('CekLapPilihLapRWILapPelunasan_Summary').getValue()===true)
										{
											firstGrid_kamar.getSelectionModel().selectAll();
										}
										else
										{
											firstGrid_kamar.getSelectionModel().clearSelections();
										}
									}
								}
							},
							{
								x:350,
								y:5,
								xtype: 'button',
								text: 'Reset Kamar',
								id: 'buttonresetkamarRWILapPelunasan_Summary',
								handler: function()
								{
									secondGridStore_kamar.removeAll();
									loadkamar(kd_irna2);
									Ext.getCmp('CekLapPilihLapRWILapPelunasan_Summary').setValue(false);
								}
							},
						]
					},
					
			]
		
    }
    return items;
}


function ItemDlgRWILapPelunasan_Summary()
{
    var PnlLapRWILapPelunasan_Summary = new Ext.Panel
    (
        {
            id: 'PnlLapRWILapPelunasan_Summary',
            fileUpload: true,
            layout: 'form',
            height: '80',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				gridUnitRWILapPelunasan_Summary(),
				// getItemPilihSemuaKamarRWILapPelunasan_Summary(),
				getItemKelompokPasienRWILapPelunasan_Summary(),
                {
                    layout: 'absolute',
                    border: false,
                    width: 800,
                    height: 30,
                    items:
                    [
                        
                    ]
                }
            ]
        }
    );

    return PnlLapRWILapPelunasan_Summary;
};


function ValidasiReportRWILapPelunasan_Summary()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWILapPelunasan_Summary').dom.value === '') || (Ext.get('dtpTglAkhirLapRWILapPelunasan_Summary').dom.value === '') || 
        (Ext.get('cboRujukanResponTimeSpesialRequestEntry').value === 'Silahkan Pilih...' || Ext.get('cboRujukanResponTimeSpesialRequestEntry').value === ' ' ))
    {
            if(Ext.get('dtpTglAwalLapRWILapPelunasan_Summary').dom.value === '')
            {
                    ShowPesanWarningRWILapPelunasan_SummaryReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            if(Ext.get('dtpTglAkhirLapRWILapPelunasan_Summary').dom.value === '')
            {
                    ShowPesanWarningRWILapPelunasan_SummaryReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.getCmp('cboRujukanResponTimeSpesialRequestEntry').getValue() === '' || Ext.getCmp('cboRujukanResponTimeSpesialRequestEntry').getValue() === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningRWILapPelunasan_SummaryReport(nmGetValidasiKosong('ResponTime'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRWILapPelunasan_Summary()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWILapPelunasan_Summary').dom.value > Ext.get('dtpTglAkhirLapRWILapPelunasan_Summary').dom.value)
    {
        ShowPesanWarningRWILapPelunasan_SummaryReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWILapPelunasan_SummaryReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function loadkamar(kd_irna2){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/rawat_inap/function_lap_RWI/getKamarRekap",
			params: {kd_irna2:kd_irna2},
			failure: function(o)
			{
				//ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				dataSource_kamar.removeAll();
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dataSource_kamar.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_kamar.add(recs);
				}
			}
		}
		
	)
}