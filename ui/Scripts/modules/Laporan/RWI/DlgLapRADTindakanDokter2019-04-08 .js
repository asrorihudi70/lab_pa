var type_file=0;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsIGDPelayananDokter;
var selectNamaIGDPelayananDokter;
var now = new Date();
var selectSetPerseorangan;
var frmDlgIGDPelayananDokter;
var varLapIGDPelayananDokter= ShowFormLapIGDPelayananDokter();
var selectSetUmum;
var selectSetkelpas;
var firstGrid;
var secondGrid;
var cmb_kd_customer_value = 'SEMUA';
var secondGridStore;
var dsDokterPelayaranDokter;
var dataSource_unitLapIGDPelayananDokter;
var DataStore_combouserLapIGDPelayananDokter;
var combouserLapIGDPelayananDokter;

var selectSetPilihankelompokPasien;
var selectSetPilihanProfesi = "Semua"; 
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;
var tmp_unit = "";

var dsDataDaftarPenangan_KASIR_KamarOperasi;

function ShowFormLapIGDPelayananDokter()
{
	var Field               = ['KD_DOKTER','NAMA'];
	dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
	dataStoreDokter('1');
    frmDlgIGDPelayananDokter= fnDlgIGDPelayananDokter();
    frmDlgIGDPelayananDokter.show();
};

function fnDlgIGDPelayananDokter()
{
    var winIGDPelayananDokterReport = new Ext.Window
    (
        {
            id: 'winIGDPelayananDokterReport',
            title: 'Lap. Kinerja pelaksana per tindakan',
            closeAction: 'destroy',
            width:400,
            height: 560,
            border: false,
            resizable:false,
            plain: true,
            // constrain: true,
            layout: 'form',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDPelayananDokter()],
            listeners:
			{
				activate: function()
				{
					/*Ext.getCmp('cboPerseoranganIGD').show();
					Ext.getCmp('cboAsuransiIGD').hide();
					Ext.getCmp('cboPerusahaanRequestEntryIGD').hide();
					Ext.getCmp('cboUmumIGD').hide();*/
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapIGDPelayananDokter',
					handler: function()
					{		
						// if(Ext.getCmp('cbPendaftaran_IGDPelayananDokter').getValue()=='' && Ext.getCmp('cbTindakIGD_IGDPelayananDokter').getValue()==''){
							// ShowPesanWarningIGDPelayananDokterReport('Pilih laporan pendaftaran atau tindakan WJ!','WARNING');
						// } else{
							var params={
								kd_profesi 			:Ext.getCmp('IDcboPilihanIGDJenisProfesi').getValue(),
								pelayananPendaftaran:Ext.getCmp('cbPendaftaran_IGDPelayananDokter').getValue(),
								pelayananTindak 	:Ext.getCmp('cbTindakIGD_IGDPelayananDokter').getValue(),
								kd_user 			:Ext.getCmp('combouserLapIGDPelayananDokter').getValue(),
								kd_dokter 			:Ext.getCmp('cboDokterIGDPelayananDokter').getValue(),
								kelompok 			:selectSetPilihankelompokPasien,
								profesi 			:selectSetPilihanProfesi,
								kd_kelompok 		:GetCriteriaIGDPasienPerKelompok(),
								kd_customer:cmb_kd_customer_value,
								tglAwal:Ext.getCmp('dtpTglAwalLapIGDPelayananDokter').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapIGDPelayananDokter').getValue(),
								JmlList:secondGridStore.getCount(),
								full_name:Ext.get('combouserLapIGDPelayananDokter').getValue(),
								type_file:type_file,
								detail:false
							} 
							var i=0;
							var tmp_kd_unit = "";
							for(i=0; i<secondGridStore.getCount(); i++){
								tmp_kd_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";		
							}
							tmp_kd_unit = tmp_kd_unit.substring(0, tmp_kd_unit.length - 1);
							params['kd_unit'] = tmp_kd_unit;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL+"index.php/rawat_inap/lap_rawat_inap/lap_RADTindakanDokter");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							console.log(params);
							form.submit();		
							// frmDlgIGDPasienPerKelompok.close(); 
						}
					// }
				},
				{
					xtype: 'button',
					text: 'Print',
					width: 70,
					hideLabel: true,
					hidden 	: true,
					id: 'btnPrintLapIGDPelayananDokter',
					handler: function()
					{		
						if(Ext.getCmp('cbPendaftaran_IGDPelayananDokter').getValue()=='' && Ext.getCmp('cbTindakIGD_IGDPelayananDokter').getValue()==''){
							ShowPesanWarningIGDPelayananDokterReport('Pilih laporan pendaftaran atau tindakan WJ!','WARNING');
						} else{
							var params={
								kd_profesi:Ext.getCmp('IDcboPilihanIGDJenisProfesi').getValue(),
								pelayananPendaftaran:Ext.getCmp('cbPendaftaran_IGDPelayananDokter').getValue(),
								pelayananTindak:Ext.getCmp('cbTindakIGD_IGDPelayananDokter').getValue(),
								kd_user:Ext.getCmp('combouserLapIGDPelayananDokter').getValue(),
								kd_dokter:GetCriteriaIGDProfesi(),
								kelompok 			:selectSetPilihankelompokPasien,
								profesi 			:selectSetPilihanProfesi,
								kd_kelompok:GetCriteriaIGDPasienPerKelompok(),
								kd_customer:cmb_kd_customer_value,
								tglAwal:Ext.getCmp('dtpTglAwalLapIGDPelayananDokter').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapIGDPelayananDokter').getValue(),
								JmlList:secondGridStore.getCount(),
								full_name:Ext.get('combouserLapIGDPelayananDokter').getValue(),
								type_file:type_file,
								detail:false
							} 
							var i=0;
							var tmp_kd_unit = "";
							for(i=0; i<secondGridStore.getCount(); i++){
								tmp_kd_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";		
							}
							tmp_kd_unit = tmp_kd_unit.substring(0, tmp_kd_unit.length - 1);
							params['kd_unit'] = tmp_kd_unit;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/gawat_darurat/lap_IGDTindakanDokter/cetakDirect");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							console.log(params);
							form.submit();		
							// frmDlgIGDPasienPerKelompok.close(); 
						}
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapIGDPelayananDokter',
					handler: function()
					{
							frmDlgIGDPelayananDokter.close();
					}
				}
			
			]

        }
    );

    return winIGDPelayananDokterReport;
};


function ItemDlgIGDPelayananDokter()
{
    var PnlLapIGDPelayananDokter = new Ext.Panel
    (
        {
            id: 'PnlLapIGDPelayananDokter',
            fileUpload: true,
            layout: 'form',
			//width:400,
            height: '590',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				gridPoliklinikLapIGDPelayananDokter(),
                getItemLapIGDPelayananDokter_Atas(),
                /* {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        
                    ]
                } */
            ]
        }
    );

    return PnlLapIGDPelayananDokter;
};

function gridPoliklinikLapIGDPelayananDokter(){
	var Field_poli_viDaftarLapIGDPelayananDokter = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unitLapIGDPelayananDokter = new WebApp.DataStore({fields: Field_poli_viDaftarLapIGDPelayananDokter});
    
    datarefresh_viInformasiUnitLapIGDPelayananDokter()

    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
	];


	// declare the source Grid
		firstGrid = new Ext.grid.GridPanel({
		ddGroup          : 'secondGridDDGroup',
		store            : dataSource_unitLapIGDPelayananDokter,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		enableDragDrop   : true,
		height           : 200,
		stripeRows       : true,
		trackMouseOver   : true,
		title            : 'Unit',
		anchor           : '100% 100%',
		plugins          : [new Ext.ux.grid.FilterRow()],
		colModel         : new Ext.grid.ColumnModel
		(
			[
					new Ext.grid.RowNumberer(),
					{
							id: 'colNRM_viDaftar',
							header: 'No.Medrec',
							dataIndex: 'KD_UNIT',
							sortable: true,
							hidden : true
					},
					{
							id: 'colNMPASIEN_viDaftar',
							header: 'Nama',
							dataIndex: 'NAMA_UNIT',
							sortable: true,
							width: 50
					}
			]
		),
		listeners : {
			afterrender : function(comp) {
				var firstGridDropTargetEl =  firstGrid.getView().scroller.dom;
				var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
						ddGroup    : 'firstGridDDGroup',
						notifyDrop : function(ddSource, e, data){
								var records =  ddSource.dragData.selections;
								Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
								firstGrid.store.add(records);
								firstGrid.store.sort('KD_UNIT', 'ASC');
								tmp_unit = "";

								for(i=0; i<secondGridStore.data.length; i++){
									// params['kd_unit'+i]=;		
									// console.log(secondGridStore.modified[i].data.KD_UNIT);
									tmp_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";

								}
								tmp_unit = tmp_unit.substr(0, tmp_unit.length-1);
								// console.log(tmp_unit);
								// dataStoreDokter(Ext.getCmp('IDcboPilihanIGDJenisProfesi').getValue(), tmp_unit);
								return true;
						}
				});
			}
		},
		viewConfig: 
			{
					forceFit: true
			}
	});

	secondGridStore = new Ext.data.JsonStore({
		fields : fields,
		root   : 'records'
	});

	// create the destination Grid
	secondGrid = new Ext.grid.GridPanel({
			ddGroup          : 'firstGridDDGroup',
			store            : secondGridStore,
			columns          : cols,
			enableDragDrop   : true,
			height           : 200,
			stripeRows       : true,
			autoExpandColumn : 'KD_UNIT',
			title            : 'Unit yang dipilih',
			listeners : {
			afterrender : function(comp) {
			var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
			var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'secondGridDDGroup',
					notifyDrop : function(ddSource, e, data){
							// Combo_SelectIGDProfesi(Ext.getCmp('IDcboPilihanIGDJenisProfesi').getValue());
							// Ext.getCmp('cboDokterIGDPelayananDokter').removeAll();
							// dsDokterPelayaran.removeAll();
							tmp_unit = "";
							var records =  ddSource.dragData.selections;
							Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
							secondGrid.store.add(records);
							secondGrid.store.sort('KD_UNIT', 'ASC');
							for(i=0; i<secondGridStore.data.length; i++){
								// params['kd_unit'+i]=;		
								// console.log(secondGridStore.modified[i].data.KD_UNIT);
								tmp_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";
							}
							tmp_unit = tmp_unit.substr(0, tmp_unit.length-1);
							// console.log(Ext.getCmp('IDcboPilihanIGDJenisProfesi').getValue());
							// dataStoreDokter(Ext.getCmp('IDcboPilihanIGDJenisProfesi').getValue(), tmp_unit);
							// mComboDokterIGDPelayananDokter(tmp_unit);
							// mComboDokterIGDPelayananPerawat(tmp_unit);
							return true;
					}
			});
			}
		},
		viewConfig: 
			{
					forceFit: true
			}
	});
	
	var FrmTabs_viInformasiUnitdokter = new Ext.Panel
	({
		    
		    closable: true,
		    region: 'center',
		    layout: 'column',
			height: 220,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[firstGrid
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid
						
					]
				},
				
				
			],

    })
    return FrmTabs_viInformasiUnitdokter;
}

function getItemLapIGDPelayananDokter_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 275,
            anchor: '100% 100%',
            items: [
			{
				x: 10,
				y: 0,
				xtype: 'checkbox',
				id: 'CekLapPilihSemuaIGDPelayananDokter',
				hideLabel:false,
				boxLabel: 'Pilih Semua Unit',
				checked: false,
				listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihSemuaIGDPelayananDokter').getValue()===true)
						{
							 firstGrid.getSelectionModel().selectAll();
						}
						else
						{
							firstGrid.getSelectionModel().clearSelections();
						}
					}
			   }
			},
			{
				x: 196,
				y: 0,
				xtype: 'checkbox',
				id: 'CekLapResetIGDPelayananDokter',
				hideLabel:false,
				boxLabel: 'Reset Unit',
				checked: false,
				listeners: 
			   {
					check: function()
					{
						if(Ext.getCmp('CekLapResetIGDPelayananDokter').getValue()===true)
						{
							secondGridStore.removeAll();
							tmp_unit = "";
							// dataStoreDokter(Ext.getCmp('IDcboPilihanIGDJenisProfesi').getValue(), tmp_unit);
							datarefresh_viInformasiUnitLapIGDPelayananDokter();
							Ext.getCmp('CekLapPilihSemuaIGDPelayananDokter').setValue(false);
						} else{
							
						}
					}
			   }
			},
			{
				xtype: 'checkboxgroup',
				width:210,
				x: 120,
				y: 40,
				hidden : true,
				items: 
				[
					{
						
						boxLabel: 'Pendaftaran',
						name: 'cbPendaftaran_IGDPelayananDokter',
						id : 'cbPendaftaran_IGDPelayananDokter'
					},
					{
						boxLabel: 'Tindak IGD',
						name: 'cbTindakIGD_IGDPelayananDokter',
						id : 'cbTindakIGD_IGDPelayananDokter'
					}
			   ]
			},
            //  ================================================================================== POLIKLINIK
			/* {
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Poliklinik '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
				mCombounitIGDPelayananDokter(), */
            //  ================================================================================== DOKTER
			{
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Bagian '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
                mComboIGDJenisProfesi(),
                // mComboDokterIGDPelayananPerawat(),
				mComboDokterIGDPelayananDokter(),

			{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, {
				x: 110,
				y: 130,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAwalLapIGDPelayananDokter',
				format: 'd/M/Y',
                value: now,
				//value: tigaharilalu
			}, {
				x: 230,
				y: 130,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 260,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapIGDPelayananDokter',
				format: 'd/M/Y',
				value: now,
				width: 100
			},{
				x: 10,
				y: 160,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 160,
				xtype: 'label',
				text: ' : '
			},
				mComboIGDPasienPerKelompok(),
				mComboIGDPasienPerKelompokSEMUA(),
				mComboIGDPasienPerKelompokPERORANGAN(),
				mComboIGDPasienPerKelompokPERUSAHAAN(),
				mComboIGDPasienPerKelompokASURANSI(),
			{
				x: 10,
				y: 220,
				xtype: 'label',
				text: 'Operator ',
				hidden : true,
			}, {
				x: 110,
				y: 220,
				xtype: 'label',
				text: ' : ',
				hidden : true,
			},
			ComboOperatorLapIGDPelayananDokter(),
			{
				x: 10,
				y: 250,
				xtype: 'label',
				text: 'Type File '
			}, {
				x: 110,
				y: 250,
				xtype: 'label',
				text: ' : '
			},
			{
				x: 120,
				y: 250,
				xtype: 'checkbox',
				id: 'CekLapPilihTypeExcel',
				hideLabel:false,
				boxLabel: 'Excel',
				checked: false,
				listeners: 
				{
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
						{
							type_file=1;
						}
						else
						{
							type_file=0;
						}
					}
			   }
			}
            ]
        },
		]
    };
    return items;
};

function ShowPesanWarningIGDPelayananDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function datarefresh_viInformasiUnitLapIGDPelayananDokter()
{
    dataSource_unitLapIGDPelayananDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "parent = ~1001~ and type_unit=false"
            }
        }
    )
    //alert("refersh")
}

function ComboOperatorLapIGDPelayananDokter(){
		var Field = ['kd_user','full_name'];
    	DataStore_combouserLapIGDPelayananDokter = new WebApp.DataStore({fields: Field});
		loaduserLapIGDPelayananDokter();
    	combouserLapIGDPelayananDokter = new Ext.form.ComboBox({
			typeAhead: true,
			x: 120,
			y: 220,
			hidden : true,
			id:'combouserLapIGDPelayananDokter',
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: DataStore_combouserLapIGDPelayananDokter,
			valueField: 'kd_user',
			displayField: 'full_name',
			value:'Semua',
			listeners:{
				'select': function(a,b,c){
				}
			}
		});
		return combouserLapIGDPelayananDokter;
}

function loaduserLapIGDPelayananDokter(){
	Ext.Ajax.request({
		url: baseURL + "index.php/rad/lap_RADTindakanDokter/getUser",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			DataStore_combouserLapIGDPelayananDokter.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  DataStore_combouserLapIGDPelayananDokter.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				DataStore_combouserLapIGDPelayananDokter.add(recs);
			}
		}
	});
}

function dataStoreDokter(level = null, kd_unit = null){
	// console.log(level);
	/*var criteriaUnit        = "";
	var criteriaDokter      = "";
	if (kd_unit == '') { kd_unit = null; }
	dsDokterPelayaranDokter.removeAll();
	if (level == "Dokter" || level == 1 ) {
		criteriaDokter = " jenis_dokter=~1~ ";
	}else{
		criteriaDokter = " jenis_dokter=~0~ ";
	}
	if (kd_unit != null) {
		criteriaUnit = " AND dokter_klinik.kd_unit in ("+kd_unit+") ";
	}*/
	// dsDokterPelayaranDokter.removeAll();
    /*dsDokterPelayaranDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewDokterPenunjangAll',
                param: " dp.kd_job = ~"+level+"~ and dp.kd_unit = ~71~ "
            }
        }
    );*/


	dsDokterPelayaranDokter.removeAll();
	// console.log(level);
	if (level == "Dokter" || level == 1 ) {
		criteriaDokter = 1;
	}else{
		criteriaDokter = 0;
	}

    dsDokterPelayaranDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewDataDokter',
                param: " jenis_dokter = ~"+criteriaDokter+"~"
            }
        }
    );
    
}

function mComboDokterIGDPelayananDokter()
{
	// console.log(dsDokterPelayaranDokter);
    var cboPilihanIGDPelayananDokter = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboDokterIGDPelayananDokter',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:false,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanIGDPelayananDokter;
};

function mComboDokterIGDPelayananPerawat(kd_unit = null)
{
    var cboPilihanIGDPelayananDokter = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboDokterIGDPelayananPerawat',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:false,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanIGDPelayananDokter;
};


function mCombounitIGDPelayananDokter()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewCombounit_Konfigurasi',
                    param: " "
                }
            }
        );

    var cbounitRequestEntryIGDPelayananDokter = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 70,
            id: 'cbounitRequestEntryIGDPelayananDokter',
			width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryIGDPelayananDokter;
};


function mComboIGDPasienPerKelompok()
{
    var cboPilihanIGDPelayananDokterkelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 160,
                id:'cboPilihanIGDPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectIGDPasienPerKelompok(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanIGDPelayananDokterkelompokPasien;
};

function mComboIGDJenisProfesi()
{
	dsDataDaftarPenangan_KASIR_KamarOperasi = new WebApp.DataStore({ fields: ['KD_JOB','KD_COMPONENT','LABEL','KD_JOB'] })
	loaddatastorePenangan();
    var cboPilihanIGDJenisProfesi = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'IDcboPilihanIGDJenisProfesi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: dsDataDaftarPenangan_KASIR_KamarOperasi,
                value 		: 'Semua',
                valueField 	: 'KD_JOB',
                displayField: 'LABEL',
                listeners:
                {
                    'select': function(a,b,c)
                    {
						tmp_unit = "";
						for(i=0; i<secondGridStore.data.length; i++){
							tmp_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";
						}
						Ext.getCmp('cboDokterIGDPelayananDokter').setValue('Semua');
						tmp_unit = tmp_unit.substr(0, tmp_unit.length-1);
                    	dataStoreDokter(b.data.KD_JOB);
                        selectSetPilihanProfesi=b.data.KD_JOB;
                        // Combo_SelectIGDProfesi(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanIGDJenisProfesi;
};

//IGDPasienPerKelompok
function mComboIGDPasienPerKelompokSEMUA()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganIGD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboIGDPasienPerKelompokSEMUA',
                typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				selectOnFocus:true,
				forceSelection: true,
				emptyText:'Silahkan Pilih...',
				valueField: 'Id',
	            displayField: 'displayText',
	            hidden:false,
				fieldLabel: '',
				width: 240,
				value:1,
				store: new Ext.data.ArrayStore
				(
						{
							id: 0,
							fields:
								[
										'Id',
										'displayText'
								],
							data: [[1, 'Semua']]
						}
				),
				listeners:
				{
					'select': function(a,b,c)
					{
						selectSetUmum=b.data.displayText ;
					}
	                                
	                            
				}
            }
	);
	return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokPERORANGAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganIGD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboIGDPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                          cmb_kd_customer_value=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokPERUSAHAAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='1' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganIGD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboIGDPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
				//value: selectsetperusahaan,
                listeners:
                {
				    'select': function(a,b,c)
					{
				        selectsetperusahaan = b.data.KD_CUSTOMER;
						selectsetnamaperusahaan = b.data.CUSTOMER;
						cmb_kd_customer_value = b.data.CUSTOMER;
					}
                }
            }
	);
	return cboPerseoranganIGD;
};

function mComboIGDPasienPerKelompokASURANSI()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='2' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganIGD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboIGDPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
				valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
				    'select': function(a,b,c)
					{
						selectSetAsuransi=b.data.KD_CUSTOMER ;
						selectsetnamaAsuransi = b.data.CUSTOMER ;
						cmb_kd_customer_value = b.data.CUSTOMER ;
					}
                }
            }
	);
	return cboPerseoranganIGD;
};


function Combo_SelectIGDPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboIGDPasienPerKelompokSEMUA').show();
   }
};

function Combo_SelectIGDProfesi(combo)
{
   var value = combo;

   if(value === "Dokter")
   {    
        Ext.getCmp('cboDokterIGDPelayananPerawat').show();
        Ext.getCmp('cboDokterIGDPelayananDokter').hide();
   }
   else if(value === "Perawat")
   {    
        Ext.getCmp('cboDokterIGDPelayananDokter').show();
        Ext.getCmp('cboDokterIGDPelayananPerawat').hide();
        
   }
   else
   {
        Ext.getCmp('cboDokterIGDPelayananPerawat').show();
        Ext.getCmp('cboDokterIGDPelayananDokter').hide();
   }
};


function GetCriteriaIGDPasienPerKelompok()
{
    var strKriteria = '';
    
    if (Ext.getCmp('cboPilihanIGDPasienPerKelompok').getValue() !== '')
    {
        if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
        else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokPERORANGAN').getValue(); } 
        else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokPERUSAHAAN').getValue(); } 
        else if (Ext.get('cboPilihanIGDPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboIGDPasienPerKelompokASURANSI').getValue(); }
    }else{
            strKriteria = 'Semua';
    }

    
    return strKriteria;
};

function GetCriteriaIGDProfesi()
{
	var strKriteria = '';
	
	if (Ext.getCmp('IDcboPilihanIGDJenisProfesi').getValue() !== '')
	{
		if (Ext.get('IDcboPilihanIGDJenisProfesi').getValue() === 'Semua') {
			strKriteria = 'SEMUA'; 
		} else if (Ext.get('IDcboPilihanIGDJenisProfesi').getValue() === 'Dokter'){ 
			strKriteria = Ext.getCmp('cboDokterIGDPelayananDokter').getValue(); 
		} else if (Ext.get('IDcboPilihanIGDJenisProfesi').getValue() === 'Perawat'){ 
			strKriteria = Ext.getCmp('cboDokterIGDPelayananDokter').getValue(); 
		} 
	}else{
        strKriteria = 'SEMUA';
	}

	
	return strKriteria;
};

function ValidasiReportIGDPasienPerDokter()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapIGDPasienPerDokter').dom.value === '')
	{
		ShowPesanWarningIGDPelayananDokterReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
    return x;
};


function loaddatastorePenangan(){
    Ext.Ajax.request({
        url: baseURL +  "index.php/general/control_cmb_dokter/getCmbDokterInapInt",
        params: {
            groups : 0,
        },
        success: function(response) {
            var cst  = Ext.decode(response.responseText);
			var recs   = [],recType = dsDataDaftarPenangan_KASIR_KamarOperasi.recordType;
			var data = {KD_JOB:"SEMUA", KD_COMPONENT:"SEMUA", LABEL:"Semua"};
			recs.push(new recType(data));
			dsDataDaftarPenangan_KASIR_KamarOperasi.add(recs);
            for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                var recs    = [],recType = dsDataDaftarPenangan_KASIR_KamarOperasi.recordType;
                var o       = cst['data'][i];
                recs.push(new recType(o));
                console.log(cst['data'][i]);
                dsDataDaftarPenangan_KASIR_KamarOperasi.add(recs);
            }
        },
        callback : function(){
            // Ext.getCmp('txtSearchDokter_KamarOperasi').focus();
        }
    });
}
