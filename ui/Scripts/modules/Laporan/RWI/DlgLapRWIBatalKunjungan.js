var fields = [
    {name: 'KD_UNIT', mapping : 'KD_UNIT'},
    {name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];

var fieldsPayment = [
    {name: 'KD_PAY', mapping : 'KD_PAY'},
    {name: 'URAIAN', mapping : 'URAIAN'}
];

var cols = [
    { id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
    { header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
];

var colsPayment = [
    { id : 'KD_PAY', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'KD_PAY',hidden : true},
    { header: "Kode Pay", width: 50, sortable: true, dataIndex: 'URAIAN'}
];

var secondGridStoreLapRWIBatalKunjungan;
var secondGridStoreLapRWIBatalKunjunganPaymentLapRWIBatalKunjungan;
var firstGrid;
var jenis_pay='';
var dataSource_payment;
var firstGridPayment;
var dataSource_unit;
var DlgLapCetakBatalKunjungan={
    vars:{
        comboSelect:null
    },
    coba:function(){
        var $this=this;
        $this.DateField.startDate
    },
    ArrayStore:{
        dokter:new Ext.data.ArrayStore({fields:[]})
    },
    CheckboxGroup:{
        shift:null,
        tindakan:null,
    },
    ComboBox:{
        kelPasien1:null,
        combo1:null,
        combo2:null,
        combo3:null,
        combo0:null,
        poliklinik:null,
        dokter:null
    },
    DataStore:{
        combo2:null,
        combo3:null,
        poliklinik:null,
        combo1:null
    },
    DateField:{
        startDate:null,
        endDate:null
    },
    Window:{
        main:null
    },
    comboOnSelect:function(val){
        var $this=this;
        $this.ComboBox.combo0.hide();
        $this.ComboBox.combo1.hide();
        $this.ComboBox.combo2.hide();
        $this.ComboBox.combo3.hide();
        if(val==-1){
            $this.ComboBox.combo0.show();
            jenis_pay='';
            $this.datapaymentfirstgrid(jenis_pay);
        }else if(val==0){
            $this.ComboBox.combo1.show();
            jenis_pay='jenis_pay=1';
            $this.datapaymentfirstgrid(jenis_pay);
        }else if(val==1){
            $this.ComboBox.combo2.show();
            jenis_pay='jenis_pay=3';
            $this.datapaymentfirstgrid(jenis_pay);
        }else if(val==2){
            $this.ComboBox.combo3.show();
            jenis_pay='jenis_pay=4';
            $this.datapaymentfirstgrid(jenis_pay);
        }
    },
    initPrint:function(){
        var $this=this;
        loadMask.show();
        var params={};
        var sendDataArrayUnit    = [];
        var sendDataArraypayment = [];
        var sendDataTmpDataUnit     = "";

        secondGridStoreLapRWIBatalKunjungan.each(function(record){
            var recordArray   = [record.get("KD_UNIT")];
            sendDataArrayUnit.push(recordArray);
            sendDataTmpDataUnit = sendDataTmpDataUnit+"'"+recordArray+"',";
        });

        /*secondGridStoreLapRWIBatalKunjunganPaymentLapRWIBatalKunjungan.each(function(record){
            var recordArraypay= [record.get("KD_PAY")];
            sendDataArraypayment.push(recordArraypay);
        });*/

        if (sendDataArrayUnit.length === 0)
        {  
            this.messageBox('Peringatan','Isi kriteria unit dengan drag and drop','WARNING');
            loadMask.hide();
        }else{
            params['type_file'] = Ext.getCmp('CetakExcelCetakBatalKunjungan').getValue();
            params['tgl_awal']  = timestimetodate($this.DateField.startDate.getValue());
            params['tgl_akhir'] = timestimetodate($this.DateField.endDate.getValue());
            params['tmp_kd_unit']   = sendDataTmpDataUnit;
            // params['tmp_payment']    = sendDataArraypayment;
            // var shift    =$this.CheckboxGroup.shift.items.items;
            // var tindakan =$this.CheckboxGroup.tindakan.items.items;
            /*var shifta=false;
            var tindakan_stat=false;
            for(var i=0;i<shift.length ; i++){
                params['shift'+i]=shift[i].checked;
                if(shift[i].checked==true)shifta=true;
            }
            for(var i=0;i<tindakan.length ; i++){
                params['tindakan'+i]=tindakan[i].checked;
                if(tindakan[i].checked==true)tindakan_stat=true;
            }*/
            // params['orderBy']     = $this.ComboBox.comboOrderBy.getValue();

                console.log(params);
                var form = document.createElement("form");
                form.setAttribute("method", "post");
                form.setAttribute("target", "_blank");
                form.setAttribute("action", baseURL + "index.php/rawat_inap/lap_RWIBatalKunjungan/cetak");
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", "data");
                hiddenField.setAttribute("value", Ext.encode(params));
                form.appendChild(hiddenField);
                document.body.appendChild(form);
                form.submit();
                loadMask.hide();
        } 
        
    },
	 PrintDirect:function(){
        var $this=this;
        loadMask.show();
        var params={};
        var sendDataArrayUnit    = [];
        var sendDataArraypayment = [];
        var sendDataTmpDataUnit     = "";

        secondGridStoreLapRWIBatalKunjungan.each(function(record){
            var recordArray   = [record.get("KD_UNIT")];
            sendDataArrayUnit.push(recordArray);
            sendDataTmpDataUnit = sendDataTmpDataUnit+"'"+recordArray+"',";
        });
        if (sendDataArrayUnit.length === 0)
        {  
            this.messageBox('Peringatan','Isi kriteria unit dengan drag and drop','WARNING');
            loadMask.hide();
        }else{
            params['type_file'] = Ext.getCmp('CetakExcelCetakBatalKunjungan').getValue();
            params['tgl_awal']  = timestimetodate($this.DateField.startDate.getValue());
            params['tgl_akhir'] = timestimetodate($this.DateField.endDate.getValue());
            params['tmp_kd_unit']   = sendDataTmpDataUnit;
			console.log(params);
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("target", "_blank");
			form.setAttribute("action", baseURL + "index.php/rawat_inap/lap_RWIBatalKunjungan/print_direct");
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", "data");
			hiddenField.setAttribute("value", Ext.encode(params));
			form.appendChild(hiddenField);
			document.body.appendChild(form);
			form.submit();
			loadMask.hide();
        } 
        
    },
    messageBox:function(modul, str, icon){
        Ext.MessageBox.show
        (
            {
                title: modul,
                msg:str,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.icon,
                width:300
            }
        );
    },
	loadUnit : function(){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/rawat_inap/lap_RWIBatalKunjungan/getUnit",
				params: {text:''},
				failure: function(o)
				{
					//ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					dataSource_unit.removeAll();
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						var recs=[],
							recType=dataSource_unit.recordType;
						for(var i=0; i<cst.listData.length; i++){
							recs.push(new recType(cst.listData[i]));
						}
						dataSource_unit.add(recs);
					}
				}
			}
			
		)
	},
    seconGridPenerimaan : function(){
        
        var secondGrid;
        secondGridStoreLapRWIBatalKunjungan = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStoreLapRWIBatalKunjungan,
                    columns          : cols,
                    autoScroll       : true,
                    columnLines      : true,
                    border           : true,
                    enableDragDrop   : true,
                    enableDragDrop   : true,
                    height           : 130,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_UNIT',
                    title            : 'Unit yang dipilih',
                    listeners : {
                        afterrender : function(comp) {
                            var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                            var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                                    ddGroup    : 'secondGridDDGroup',
                                    notifyDrop : function(ddSource, e, data){
                                            var records =  ddSource.dragData.selections;
                                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                            secondGrid.store.add(records);
                                            secondGrid.store.sort('KD_UNIT', 'ASC');
                                            return true
                                    }
                            });
                        }
                    },
                viewConfig: 
                {
                        forceFit: true
                }
        });
        return secondGrid;
    },
    firstGridPenerimaan : function(){
        var Field_poli_viDaftar = ['kd_unit','nama_unit'];
		dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
		var $this=this;
			$this.loadUnit();
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 130,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
			(
				[
						new Ext.grid.RowNumberer(),
						{
								id: 'colNRM_viDaftar',
								header: 'kd_unit',
								dataIndex: 'KD_UNIT',
								sortable: true,
								hidden : true
						},
						{
								id: 'colNMPASIEN_viDaftar',
								header: 'Nama',
								dataIndex: 'NAMA_UNIT',
								sortable: true,
								width: 50
						}
				]
			),   
            listeners : {
                afterrender : function(comp) {
                    var secondGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                }
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid;
    },
    init:function(){
        var $this=this;
        $this.Window.main=new Ext.Window({
            width: 600,
            height:400,
            modal:true,
            title:'Laporan History Batal Kunjungan',
            layout:'fit',
            items:[
                new Ext.Panel({
                    bodyStyle:'padding: 6px',
                    layout:'form',
                    border:false,
                    autoScroll: true,
                    fbar:[
                        new Ext.Button({
                            text:'Preview',
                            handler:function(){
                                $this.initPrint()
                            }
                        }),
						 new Ext.Button({
                            text:'Print',
                            handler:function(){
                                $this.PrintDirect()
                            }
                        }),
                        new Ext.Button({
                            text:'Batal',
                            handler:function(){
                                $this.Window.main.close();
                            }
                        })
                    ],
                    items:[
                        {
                            xtype : 'fieldset',
                            title : 'Unit RS',
                            layout:'column',
                            border:true,
                            height: 170,
                            items:[
                                {
                                    border:false,
                                    columnWidth:.45,
                                    items:[
                                        $this.firstGridPenerimaan(),
                                    ]
                                },
                                {
                                    border:false,
                                    columnWidth:.1,
                                    bodyStyle:'margin-top: 2px',
                                },
                                {
                                    border:false,
                                    columnWidth:.45,
                                    items:[
                                        $this.seconGridPenerimaan(),
                                    ]
                                }
                            ]
                        },{
                            layout: 'absolute',
                            border: true,
                            height:30,
                            items:[
                                {   
                                    x:10,
                                    y:4,
                                    xtype: 'checkbox',
                                    id: 'CekLapPilihSemuaCetakBatalKunjungan',
                                    hideLabel:false,
                                    boxLabel: 'Pilih Semua Unit',
                                    checked: false,
                                    listeners: 
                                    {
                                        check: function()
                                        {
                                           if(Ext.getCmp('CekLapPilihSemuaCetakBatalKunjungan').getValue()===true)
                                            {
                                                firstGrid.getSelectionModel().selectAll();
                                            }
                                            else
                                            {
                                                firstGrid.getSelectionModel().clearSelections();
                                            }
                                        }
                                   }
                                },
                                {
                                    border:false,
                                    columnWidth:.2,
                                    bodyStyle:'margin-top: 2px',
                                },
                                {
									x:315,
									y:4,
									xtype: 'button',
									text: 'Reset Unit',
									id: 'buttonresetUnitLapRWIBatalKunjungan',
									handler: function()
									{
										secondGridStoreLapRWIBatalKunjungan.removeAll();
										$this.loadUnit();
										Ext.getCmp('CekLapPilihSemuaCetakBatalKunjungan').setValue(false);
									}
								},
                            ]
                        },
                        {
                            layout: 'column',
                            border: false,
                            bodyStyle:'margin-top: 2px',
                            height:50,
                            items:[
                                {
                                    xtype : 'fieldset',
                                    title : 'Periode',
                                    layout: 'column',
                                    width : 320,
                                    bodyStyle:'padding: 2px 0px  7px 15px',
                                    border: true,
                                    items:[
                                        
                                        $this.DateField.startDate=new Ext.form.DateField({
                                            value: new Date(),
                                            format:'d/M/Y'
                                        }),
                                        {
                                            xtype:'displayfield',
                                            width: 30,
                                            value:'&nbsp;s/d&nbsp;'
                                        },
                                        $this.DateField.endDate=new Ext.form.DateField({
                                            value: new Date(),
                                            format:'d/M/Y'
                                        })
                                    ]
                                }
                            ]
                        },{
                            layout: 'absolute',
                            border: true,
                            height:30,
                            items:[
                                // {
                                    // border:false,
                                    // columnWidth:.2,
                                    // bodyStyle:'margin-top: 2px',
                                // },
                                {   
                                    x:10,
                                    y:4,
                                    xtype: 'checkbox',
                                    id: 'CetakExcelCetakBatalKunjungan',
                                    hideLabel:false,
                                    boxLabel: 'Cetak Excel',
                                    checked: false,
                                    listeners: 
                                    {
                                        
                                   }
                                },
                            ]
                        },
                    ]
                })
            ]
        }).show();
    }
};
DlgLapCetakBatalKunjungan.init();
