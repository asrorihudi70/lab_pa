
var dsRWIResponTime;
var selectRWIResponTime;
var selectNamaRWIResponTime;
var now = new Date();
var selectSetResponTime;
var frmDlgRWIResponTime;
var varLapRWICutOffPerbulan= ShowFormLapRWICutOffPerbulan();
var type_file=0;
function ShowFormLapRWICutOffPerbulan()
{
    frmDlgRWIResponTime= fnDlgRWIResponTime();
    frmDlgRWIResponTime.show();
};

function fnDlgRWIResponTime()
{
    var winRWIResponTimeReport = new Ext.Window
    (
        {
            id: 'winRWIResponTimeReport',
            title: 'Laporan  Rawat Inap Cut Off Perbulan',
            closeAction: 'destroy',
            width:400,
            height: 240,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWIResponTime()],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapRWICutOffPerbulan',
					handler: function(){
						//if (ValidasiReportRWIResponTime() === 1){
							var kd_customer;
							if(Ext.getCmp('cboPilihankelompokPasien').getValue() ==1){
								kd_customer='Semua';
							}else{
								if(Ext.get('cboPilihankelompokPasien').getValue() == 'Perseorangan'){
									kd_customer = Ext.getCmp('cboPerseorangan').getValue();
								}
								if(Ext.get('cboPilihankelompokPasien').getValue() == 'Asuransi'){
									kd_customer = Ext.getCmp('cboAsuransi').getValue();
								}
								if(Ext.get('cboPilihankelompokPasien').getValue() == 'Perusahaan'){
									kd_customer = Ext.getCmp('cboPerusahaanRequestEntry').getValue();
								}
								
							}
							var params={
								tgl_awal:Ext.get('dtpTglAwalLapRWICutOffPerbulan').dom.value,
								tgl_akhir:Ext.get('dtpTglAkhirLapRWICutOffPerbulan').dom.value,
                                kelpas:Ext.get('cboPilihankelompokPasien').getValue(),
								type_laporan:Ext.get('cboRumahSakit').getValue(),
								kd_customer:kd_customer,
							} ;
							console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_inap/function_lap_RWI/cetakLapRWICutOffPerBulan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();	
							loadMask.hide();
						//}
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRWICutOffPerbulan',
					handler: function()
					{
						frmDlgRWIResponTime.close();
					}
				}
				
			]
        }
    );

    return winRWIResponTimeReport;
};


function ItemDlgRWIResponTime()
{
    var PnlLapRWICutOffPerbulan = new Ext.Panel
    (
        {
            id: 'PnlLapRWICutOffPerbulan',
            fileUpload: true,
            layout: 'form',
            height: '80',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
				getItemLapRWICutOffPerbulan_Tanggal(), getItemLapRWICutOffPerbulan_Dept(),
                {
                    layout: 'absolute',
                    border: false,
                    width: 800,
                    height: 30,
                    items:
                    [
                        
                    ]
                }
            ]
        }
    );

    return PnlLapRWICutOffPerbulan;
};

function ValidasiTanggalReportRWIResponTime()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWICutOffPerbulan').dom.value > Ext.get('dtpTglAkhirLapRWICutOffPerbulan').dom.value)
    {
        ShowPesanWarningRWIResponTimeReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWIResponTimeReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWICutOffPerbulan_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.98,
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 10px 10px',
                border: false,
                width: 800,
                height: 100,
                anchor: '100% 100%',
                items:
                [
                    {
                        x: 5,
                        y: 1,
                        xtype: 'label',
                        text: 'Kelompok Pasien '
                    },
					{
                        x: 90,
                        y: 1,
                        xtype: 'label',
                        text: ':'
                    },
                    mComboPilihanKelompokPasien(),
                    mComboPerseorangan(),
                    mComboAsuransi(),
                    mComboPerusahaan(),
                    mComboUmum(),
                    {
                        x: 5,
                        y: 60,
                        xtype: 'label',
                        text: 'Tipe File'
                    },
					{
                        x: 90,
                        y: 60,
                        xtype: 'label',
                        text: ':'
                    },
                    mComboRumahSakit(),
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWICutOffPerbulan_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                columnWidth: .99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 800,
                height: 30,
                anchor: '100% 100%',
                items:
                [
                    {
                        x: 5,
                        y: 1,
                        xtype: 'label',
                        text: 'Tanggal '
                    },
					{
                        x: 90,
                        y: 1,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 110,
                        y: 1,
                        xtype: 'datefield',
                        id: 'dtpTglAwalLapRWICutOffPerbulan',
                        format: 'd-M-Y',
                        value:now
                    },
                    {
                        x: 220,
                        y: 1,
                        xtype: 'label',
                        text: 's/d'
                    },
                    {
                        x: 240,
                        y: 1,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirLapRWICutOffPerbulan',
                        format: 'd-M-Y',
                        value:now
                    },
					
                ]
            }
        ]
    }
    return items;
};


function mComboUnitLapRWICutOffPerbulan()
{
     var cboResponTime = new Ext.form.ComboBox
	(
		{
			id:'cboResponTime',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Wilayah ',
			width:80,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Prop'],[2, 'Kab'],[3, 'Kec'],[4, 'Kel']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetResponTime,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetResponTime=b.data.displayText ;
				}
			}
		}
	);
	return cboResponTime;
};

function mcomboResponTimeSpesial()
{
    var Field = ['no_ResponTime','ResponTime'];
    ds_ResponTimeSpesial_viDaftar = new WebApp.DataStore({fields: Field});

	ds_ResponTimeSpesial_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewSetupKelasSpesial',
                param: ""
            }
        }
    )

    var cboRujukanResponTimeSpesialRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanResponTimeSpesialRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih ResponTime...',
            fieldLabel: 'ResponTime ',
            align: 'Right',
            store: ds_ResponTimeSpesial_viDaftar,
            valueField: 'no_ResponTime',
            displayField: 'ResponTime',
            anchor: '95%',
            Width:'800',
            value: 'Semua',
            x: 95,
            y: 1,
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                },
                    'render': function(c)
                                {
                                    
                                }


		}
        }
    )

    return cboRujukanResponTimeSpesialRequestEntry;
}

function mComboPilihanKelompokPasien()
{
    var cboPilihankelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 110,
                y: 1,
                id:'cboPilihankelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihankelompokPasien;
};
function mComboPerseorangan()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0'
			}
		}
	);
    var cboPerseorangan = new Ext.form.ComboBox
	(
            {
                x: 110,
                y: 30,
                id:'cboPerseorangan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                value: 'Semua',
                width:200,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseorangan;
};

function mComboRumahSakit()
{

    var cboRumahSakit = new Ext.form.ComboBox
    (
        {
            x: 110,
            y: 60,
            id:'cboRumahSakit',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            width:200,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[0, 'Rumah Sakit'], [1, 'Pavilyun']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:0,
            listeners:
            {
                'select': function(a,b,c)
                {
                                  selectSetUmum=b.data.displayText ;
                }
                                
                            
            }
        }
    );
    return cboRumahSakit;
};

function mComboUmum()
{

    var cboUmum = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 30,
			id:'cboUmum',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			disabled:true,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmum;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1'
			}
		}
	);
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 30,
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: 'Semua',
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntry;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2"
            }
        }
    );
    var cboAsuransi = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 30,
			id:'cboAsuransi',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: 'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransi;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseorangan').show();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').show();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').show();
   }
   else
   {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').show();
   }
};
