
var dsRWILapPelunasan;
var selectRWILapPelunasan;
var selectNamaRWILapPelunasan;
var now = new Date();
var selectSetResponTime;
var frmDlgRWILapPelunasan;
var varLapRWILapPelunasan= ShowFormLapRWILapPelunasan();
var type_file=0;
function ShowFormLapRWILapPelunasan()
{
    frmDlgRWILapPelunasan= fnDlgRWILapPelunasan();
    frmDlgRWILapPelunasan.show();
};

function fnDlgRWILapPelunasan()
{
    var winRWILapPelunasanReport = new Ext.Window
    (
        {
            id: 'winRWILapPelunasanReport',
            title: 'Laporan  Rawat Inap Cut Off Perbulan',
            closeAction: 'destroy',
            width:560,
            height: 420,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [
                
                ItemDlgRWILapPelunasan()
            ],
            fbar:[
                {
                    x: 175,
                    y: 1,
                    xtype: 'button',
                    text: 'Ok',
                    width: 70,
                    hideLabel: true,
                    id: 'btnOkLapRWILapPelunasan',
                    handler: function(){
                        //if (ValidasiReportRWILapPelunasan() === 1){
                            var kel_pas2;
                            if(Ext.getCmp('cboPilihankelompokPasienRWILapPelunasan').getValue() ==1){
                                kel_pas2='Semua';
                            }else{
                                if (Ext.getCmp('cboPilihankelompokPasienRWILapPelunasan').getValue() == 2) {
                                    kel_pas2 = Ext.getCmp('cboPerseoranganRWILapPelunasan').getValue();
                                }else if(Ext.getCmp('cboPilihankelompokPasienRWILapPelunasan').getValue() == 3){
                                    kel_pas2 = Ext.getCmp('cboPerusahaanRWILapPelunasan').getValue();
                                }else{
                                    kel_pas2 = Ext.getCmp('cboAsuransiRWILapPelunasan').getValue();
                                }
                                
                            }
                            var sendTmpDataArrayKamar = "";

                            secondGridStore_kamar.each(function(record){
                                var recordArray = [record.get("no_kamar")];
                                // sendDataArrayUnit.push(recordArray);
                                // console.log(record);
                                sendTmpDataArrayKamar += "'"+recordArray+"',";
                                /*var recordArrayNama   = [record.get("NAMA_UNIT")];
                                sendDataArrayNamaUnit.push(recordArrayNama);*/
                            });

                            var tmp_kd_irna = "";
                            var SelectedCheckbox=firstGridUnitRS.getSelectionModel();
                            for(i=0;i<SelectedCheckbox.selections.length;i++){
                                // tmp_kd_irna = SelectedCheckbox.selections.items[i].data.kd_unit;
                                tmp_kd_irna += "'"+SelectedCheckbox.selections.items[i].data.kd_unit+"',";
                                
                            }
                            sendTmpDataArrayKamar = sendTmpDataArrayKamar.substr(0, sendTmpDataArrayKamar.length - 1);
                            tmp_kd_irna           = tmp_kd_irna.substr(0, tmp_kd_irna.length - 1);
                            var params = {
                                tgl_awal:Ext.get('dtpTglAwalLapRWILapPelunasan').dom.value,
                                tgl_akhir:Ext.get('dtpTglAkhirLapRWILapPelunasan').dom.value,
                                kel_pas:Ext.getCmp('cboPilihankelompokPasienRWILapPelunasan').getValue(),
                                kd_customer:kel_pas2,
                                type_file:type_file,
                                parent:tmp_kd_irna,
                                no_kamar:sendTmpDataArrayKamar,
                            } ;
                            console.log(params);
                            var form = document.createElement("form");
                            form.setAttribute("method", "post");
                            form.setAttribute("target", "_blank");
                            form.setAttribute("action", baseURL + "index.php/rawat_inap/function_lap_RWI/cetakLapRWICutOffPerBulan");
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", "data");
                            hiddenField.setAttribute("value", Ext.encode(params));
                            form.appendChild(hiddenField);
                            document.body.appendChild(form);
                            form.submit();  
                            loadMask.hide();
                        //}
                    }
                },
                {
                    x: 250,
                    y: 1,
                    xtype: 'button',
                    text: 'Cancel' ,
                    width: 70,
                    hideLabel: true,
                    id: 'btnCancelLapRWILapPelunasan',
                    handler: function()
                    {
                            frmDlgRWILapPelunasan.close();
                    }
                }
            ]

        }
    );

    return winRWILapPelunasanReport;
};

function getItemKelompokPasienRWILapPelunasan(){
    var items =
    {
            layout:'column',
            border:false,
            anchor: '100%',
            padding: '5px 5px 5px 0px',
            items:[
                    {   
                        xtype : 'fieldset',
                        layout:'absolute',
                        border:true,
                        // title:'Kelompok Pasien',
                        height:150,
                        width:550,
                        padding: '15px 15px 15px 15px',
                        items:
                        [
                            {
                                x: 10,
                                y: 10,
                                xtype: 'label',
                                text: 'Tanggal '
                            },
                            {
                                x: 100,
                                y: 10,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 110,
                                y: 10,
                                xtype: 'datefield',
                                id: 'dtpTglAwalLapRWILapPelunasan',
                                format: 'd-M-Y',
                                value:now
                            },
                            {
                                x: 220,
                                y: 10,
                                xtype: 'label',
                                text: 's/d'
                            },
                            {
                                x: 240,
                                y: 10,
                                xtype: 'datefield',
                                id: 'dtpTglAkhirLapRWILapPelunasan',
                                format: 'd-M-Y',
                                value:now
                            },
                            {
                                x: 10,
                                y: 40,
                                xtype: 'label',
                                text: 'Kelompok Pasien'
                            },
                            {
                                x: 100,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboPilihanKelompokPasienRWILapPelunasan(),
                            mComboPerseoranganRWILapPelunasan(),
                            mComboAsuransiRWILapPelunasan(),
                            mComboPerusahaanRWILapPelunasan(),
                            mComboUmumRWILapPelunasan(),
                            {
                                x: 10,
                                y: 100,
                                xtype: 'label',
                                text: 'Tipe File'
                            },
                            {
                                x: 100,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 110,
                                y: 100,
                                xtype: 'checkbox',
                                id: 'CekLapPilihFile',
                                hideLabel:false,
                                boxLabel: 'Excel',
                                checked: false,
                                listeners: 
                                {
                                    check: function()
                                    {
                                       if(Ext.getCmp('CekLapPilihFile').getValue()===true)
                                        {
                                            type_file =1;
                                        }
                                        else
                                        {
                                            type_file =0;
                                           
                                        }
                                    }
                                }
                            }
                        ]
                    },
                    
            ]
        
    }
    return items;
}

function mComboPilihanKelompokPasienRWILapPelunasan()
{
    var cbPilihankelompokPasienRWILapPelunasan = new Ext.form.ComboBox
    (
            {
                x: 110,
                y: 40,
                id:'cboPilihankelompokPasienRWILapPelunasan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 130,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
    );
    return cbPilihankelompokPasienRWILapPelunasan;
};
function mComboPerseoranganRWILapPelunasan()
{
    var cbPerseoranganRWILapPelunasan = new Ext.form.ComboBox
    (
            {
                x: 110,
                y: 70,
                id:'cboPerseoranganRWILapPelunasan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
    );
    return cbPerseoranganRWILapPelunasan;
};

function mComboUmumRWILapPelunasan()
{
    var cbUmumRWILapPelunasan = new Ext.form.ComboBox
    (
        {
            x: 110,
            y: 70,
            id:'cboUmumRWILapPelunasan',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            width:200,
            disabled:true,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[1, 'Semua']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:1,
            listeners:
            {
                'select': function(a,b,c)
                {
                                  selectSetUmum=b.data.displayText ;
                }
                                
                            
            }
        }
    );
    return cbUmumRWILapPelunasan;
};

function mComboPerusahaanRWILapPelunasan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRWILapPelunasan = new WebApp.DataStore({fields: Field});
    dsPerusahaanRWILapPelunasan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboLookupCustomer',
                param: 'jenis_cust=1'
            }
        }
    );
    var cbPerusahaanRWILapPelunasan = new Ext.form.ComboBox
    (
        {
            x: 110,
            y: 70,
            id: 'cboPerusahaanRWILapPelunasan',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: '',
            align: 'Right',
            store: dsPerusahaanRWILapPelunasan,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            width:200,
            value: 'Semua',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectsetperusahaan = b.data.KD_CUSTOMER;
                    selectsetnamaperusahaan = b.data.CUSTOMER;
                }
            }
        }
    );

    return cbPerusahaanRWILapPelunasan;
};

function mComboAsuransiRWILapPelunasan()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'ASC',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2"
            }
        }
    );
    var cbAsuransiRWILapPelunasan = new Ext.form.ComboBox
    (
        {
            x: 110,
            y: 70,
            id:'cboAsuransiRWILapPelunasan',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: '',
            align: 'Right',
            width:200,
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            value: 'Semua',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetAsuransi=b.data.KD_CUSTOMER ;
                    selectsetnamaAsuransi=b.data.CUSTOMER ;
                }
            }
        }
    );
    return cbAsuransiRWILapPelunasan;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRWILapPelunasan').show();
        Ext.getCmp('cboAsuransiRWILapPelunasan').hide();
        Ext.getCmp('cboPerusahaanRWILapPelunasan').hide();
        Ext.getCmp('cboUmumRWILapPelunasan').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRWILapPelunasan').hide();
        Ext.getCmp('cboAsuransiRWILapPelunasan').hide();
        Ext.getCmp('cboPerusahaanRWILapPelunasan').show();
        Ext.getCmp('cboUmumRWILapPelunasan').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRWILapPelunasan').hide();
        Ext.getCmp('cboAsuransiRWILapPelunasan').show();
        Ext.getCmp('cboPerusahaanRWILapPelunasan').hide();
        Ext.getCmp('cboUmumRWILapPelunasan').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRWILapPelunasan').hide();
        Ext.getCmp('cboAsuransiRWILapPelunasan').hide();
        Ext.getCmp('cboPerusahaanRWILapPelunasan').hide();
        Ext.getCmp('cboUmumRWILapPelunasan').show();
        Ext.getCmp('cboPerseoranganRWILapPelunasan').setValue('Semua');
        Ext.getCmp('cboAsuransiRWILapPelunasan').setValue('Semua');
        Ext.getCmp('cboPerusahaanRWILapPelunasan').setValue('Semua');
   }
   else
   {
        Ext.getCmp('cboPerseoranganRWILapPelunasan').hide();
        Ext.getCmp('cboAsuransiRWILapPelunasan').hide();
        Ext.getCmp('cboPerusahaanRWILapPelunasan').hide();
        Ext.getCmp('cboUmumRWILapPelunasan').show();
   }
};

function gridUnitRWILapPelunasan(){
    var Field_poli_viDaftar = ['kd_unit','nama_unit'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rawat_inap/function_lap_RWI/getUnit",
            params: {text:''},
            failure: function(o)
            {
                //ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    var recs=[],
                        recType=dataSource_unit.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        recs.push(new recType(cst.listData[i]));
                    }
                    dataSource_unit.add(recs);
                }
            }
        }
        
    )
   
    // Generic fields array to use in both store defs.
    var fields = [
        {name: 'kd_unit', mapping : 'kd_unit'},
        {name: 'nama_unit', mapping : 'nama_unit'}
    ];

    
    // Column Model shortcut array
    var cols = [
        { id : 'kd_unit', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'kd_unit',hidden : true},
        {header: "Nama", width: 50, sortable: true, dataIndex: 'nama_unit'}
    ];
    
    var sm = new Ext.grid.CheckboxSelectionModel({
                dataIndex:'check',
                listeners: {
                    selectionchange: function(sm, selected, opts) {
                        var SelectedCheckbox=firstGridUnitRS.getSelectionModel();
                        kd_irna='';
                        kd_irna2='';
                        if(SelectedCheckbox.selections.length >0){
                            for(i=0;i<SelectedCheckbox.selections.length;i++){
                                kd_irna = SelectedCheckbox.selections.items[i].data.kd_unit;
                                kd_irna2 = kd_irna2.concat("'",kd_irna,"'",',');
                                
                            }
                            
                            loadkamar(kd_irna2);
                        }else{
                            dataSource_kamar.removeAll();
                        }
                    }
                }
            });    
    firstGridUnitRS = new Ext.grid.GridPanel({
    store            : dataSource_unit,
    autoScroll       : true,
    columnLines      : true,
    border           : true,
    height           : 150,
    stripeRows       : true,
    title            : 'Unit RS',
    anchor           : '100% 100%',
    plugins          : [new Ext.ux.grid.FilterRow()],
    columns         :
        [
            sm,
            new Ext.grid.RowNumberer(),
            {
                id: 'grid_kd_unit',
                header: 'kd_unit',
                dataIndex: 'kd_unit',
                sortable: true,
                hidden : true
            },
            {
                id: 'grid_nama_unit',
                header: 'Nama Unit RS',
                dataIndex: 'nama_unit',
                sortable: true,
                width: 50
            }
        ],
    sm: sm, 
    viewConfig: 
    {
        forceFit: true
    }
    });
    
    var Field_kamar_viDaftar = ['no_kamar','nama_kamar'];
    dataSource_kamar = new WebApp.DataStore({fields: Field_kamar_viDaftar});
    // loadkamar(kd_spesial,kd_kelas);
   
    // Generic fields array to use in both store defs.
    var fields = [
        {name: 'no_kamar', mapping : 'no_kamar'},
        {name: 'nama_kamar', mapping : 'nama_kamar'}
    ];

    
    // Column Model shortcut array
    var cols_kamar = [
        { id : 'no_kamar', header: "Kamar", width: 160, sortable: true, dataIndex: 'no_kamar',hidden : true},
        {header: "Kamar", width: 50, sortable: true, dataIndex: 'nama_kamar'}
    ];


    // declare the source Grid
    firstGrid_kamar = new Ext.grid.GridPanel({
    ddGroup          : 'secondGridDDGroup',
    store            : dataSource_kamar,
    autoScroll       : true,
    columnLines      : true,
    border           : true,
    enableDragDrop   : true,
    height           : 150,
    stripeRows       : true,
    trackMouseOver   : true,
    title            : 'Nama Kamar',
    anchor           : '100% 100%',
    plugins          : [new Ext.ux.grid.FilterRow()],
    colModel         : new Ext.grid.ColumnModel
                        (
                            [
                                new Ext.grid.RowNumberer(),
                                {
                                        id: 'colNRM_viDaftar',
                                        header: 'No.Medrec',
                                        dataIndex: 'no_kamar',
                                        sortable: true,
                                        hidden : true
                                },
                                {
                                        id: 'colNMPASIEN_viDaftar',
                                        header: 'Nama',
                                        dataIndex: 'nama_kamar',
                                        sortable: true,
                                        width: 50
                                }
                            ]
                        ),
    viewConfig: 
    {
        forceFit: true
    }
    });

    secondGridStore_kamar = new Ext.data.JsonStore({
        fields : fields,
        root   : 'records'
    });

    // create the destination Grid
    secondGrid_kamar = new Ext.grid.GridPanel({
        ddGroup          : 'firstGrid_kamarDDGroup',
        store            : secondGridStore_kamar,
        columns          : cols_kamar,
        enableDragDrop   : true,
        height           : 150,
        stripeRows       : true,
        autoExpandColumn : 'no_kamar',
        title            : 'Nama kamar yang dipilih',
        listeners        : {
            afterrender : function(comp) {
            var secondGridDropTargetEl = secondGrid_kamar.getView().scroller.dom;
            var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                    ddGroup    : 'secondGridDDGroup',
                    notifyDrop : function(ddSource, e, data){
                            var records =  ddSource.dragData.selections;
                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                            secondGrid_kamar.store.add(records);
                            secondGrid_kamar.store.sort('no_kamar', 'ASC');
                            return true
                    }
            });
            }
        },
        viewConfig: 
        {
            forceFit: true
        }
    });
    var FrmTabs_viInformasiUnitRWILapPelunasan = new Ext.Panel
    (
        {
            id: 'pilihunitLapRWILapPelunasan',
            closable: true,
            // region: 'center',
            layout: 'column',
            height   : 165,
            itemCls: 'blacklabel',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: true,
            shadhow: true,
            margins: '0 5 5 0',
            anchor: '99%',
            // iconCls: '',
            items: 
            [
                {
                    columnWidth: .30,
                    layout: 'form',
                    border: false,
                    autoScroll: true,
                    bodyStyle: 'padding: 5px 5px 5px 5px',
                    anchor: '100% 100%',
                    items:
                    [
                        firstGridUnitRS 
                    ]
                },{
                    columnWidth: .35,
                    layout: 'form',
                    border: false,
                    autoScroll: true,
                    bodyStyle: 'padding: 5px 5px 5px 5px',
                    anchor: '100% 100%',
                    items:
                    [
                        firstGrid_kamar
                    ]
                },
                {
                    columnWidth: .35,
                    layout: 'form',
                    bodyStyle: 'padding: 5px 5px 5px 5px',
                    border: false,
                    anchor: '100% 100%',
                    items:
                    [
                        secondGrid_kamar
                    ]
                },
            ]
        }
    )
    return FrmTabs_viInformasiUnitRWILapPelunasan;
}

function getItemPilihSemuaKamarRWILapPelunasan(){
    var items =
    {
            layout:'column',
            border:false,
            anchor: '100%',
            // bodyStyle: 'padding:5px',
            padding: '5px 5px 0px 0px',
            items:[
                    {   
                        xtype : 'fieldset',
                        layout:'absolute',
                        border:true,
                        title:'',
                        height:55,
                        width:550,
                        padding: '0px 0px 0px 15px',
                        items:
                        [
                            {
                                x:165,
                                y:10,
                                xtype: 'checkbox',
                                id: 'CekLapPilihLapRWILapPelunasan',
                                hideLabel:false,
                                boxLabel: 'Pilih Semua Kamar',
                                checked: false,
                                listeners: 
                                {
                                    check: function()
                                    {
                                       if(Ext.getCmp('CekLapPilihLapRWILapPelunasan').getValue()===true)
                                        {
                                            firstGrid_kamar.getSelectionModel().selectAll();
                                        }
                                        else
                                        {
                                            firstGrid_kamar.getSelectionModel().clearSelections();
                                        }
                                    }
                                }
                            },
                            {
                                x:350,
                                y:5,
                                xtype: 'button',
                                text: 'Reset Kamar',
                                id: 'buttonresetkamarRWILapPelunasan',
                                handler: function()
                                {
                                    secondGridStore_kamar.removeAll();
                                    loadkamar(kd_irna2);
                                    Ext.getCmp('CekLapPilihLapRWILapPelunasan').setValue(false);
                                }
                            },
                        ]
                    },
                    
            ]
        
    }
    return items;
}


function ItemDlgRWILapPelunasan()
{
    var PnlLapRWILapPelunasan = new Ext.Panel
    (
        {
            id: 'PnlLapRWILapPelunasan',
            fileUpload: true,
            layout: 'form',
            height: '80',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                gridUnitRWILapPelunasan(),
                getItemPilihSemuaKamarRWILapPelunasan(),
                getItemKelompokPasienRWILapPelunasan(),
                {
                    layout: 'absolute',
                    border: false,
                    width: 800,
                    height: 30,
                    items:
                    [
                        
                    ]
                }
            ]
        }
    );

    return PnlLapRWILapPelunasan;
};


function ValidasiReportRWILapPelunasan()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWILapPelunasan').dom.value === '') || (Ext.get('dtpTglAkhirLapRWILapPelunasan').dom.value === '') || 
        (Ext.get('cboRujukanResponTimeSpesialRequestEntry').value === 'Silahkan Pilih...' || Ext.get('cboRujukanResponTimeSpesialRequestEntry').value === ' ' ))
    {
            if(Ext.get('dtpTglAwalLapRWILapPelunasan').dom.value === '')
            {
                    ShowPesanWarningRWILapPelunasanReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            if(Ext.get('dtpTglAkhirLapRWILapPelunasan').dom.value === '')
            {
                    ShowPesanWarningRWILapPelunasanReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.getCmp('cboRujukanResponTimeSpesialRequestEntry').getValue() === '' || Ext.getCmp('cboRujukanResponTimeSpesialRequestEntry').getValue() === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningRWILapPelunasanReport(nmGetValidasiKosong('ResponTime'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRWILapPelunasan()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWILapPelunasan').dom.value > Ext.get('dtpTglAkhirLapRWILapPelunasan').dom.value)
    {
        ShowPesanWarningRWILapPelunasanReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWILapPelunasanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function loadkamar(kd_irna2){
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rawat_inap/function_lap_RWI/getKamarRekap",
            params: {kd_irna2:kd_irna2},
            failure: function(o)
            {
                //ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                dataSource_kamar.removeAll();
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    var recs=[],
                        recType=dataSource_kamar.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        recs.push(new recType(cst.listData[i]));
                    }
                    dataSource_kamar.add(recs);
                }
            }
        }
        
    )
}