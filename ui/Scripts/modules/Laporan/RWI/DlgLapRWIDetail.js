
var dsIGDLaporandetail;
var selectNamaIGDLaporandetail;
var now = new Date();
var selectCount_viInformasiUnitdokter=50;
var icons_viInformasiUnitdokter="Gaji";
var dataSource_unit;
var selectSetJenisPasien;
var frmDlgIGDLaporandetail;
var secondGridStore;
var varLapIGDLaporandetail= ShowFormLapIGDLaporandetail();
var selectSetUmum;
var CurrentData_viDaftarIGD =
{
	data: Object,
	details: Array,
	row: 0
};

function ShowFormLapIGDLaporandetail()
{
    frmDlgIGDLaporandetail= fnDlgIGDLaporandetail();
    frmDlgIGDLaporandetail.show();
};

function fnDlgIGDLaporandetail()
{
    var winIGDLaporandetailReport = new Ext.Window
    (
        {
            id: 'winIGDLaporandetailReport',
            title: 'Laporan Inap',
            closeAction: 'destroy',
            width:620,
            height: 350,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [dataGrid_viInformasiUnit()],
            listeners:
					{
						activate: function()
						{

						}
					}

        }
    );

    return winIGDLaporandetailReport;
};


function ItemDlgIGDLaporandetail()
{
    var PnlLapIGDLaporandetail = new Ext.Panel
    (
        {
            id: 'PnlLapIGDLaporandetail',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '99%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                dataGrid_viInformasiUnit(),

            ]
        }
        
    );

    return PnlLapIGDLaporandetail;
};

function GetCriteriaIGDLaporandetail()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapIGDLaporandetail').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapIGDLaporandetail').getValue();
	};
        if (Ext.get('dtpTglAkhirLapIGDLaporandetail').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapIGDLaporandetail').getValue();
	};
    if (Ext.getCmp('cboRuanganRequestEntry').getValue() !== '' || Ext.getCmp('cboRuanganRequestEntry').getValue() !== 'Pilih Ruangan...')
	{
                strKriteria += '##@@##' + Ext.getCmp('cboRuanganRequestEntry').getValue();
	};
        if (Ext.getCmp('cboJenisPasien').getValue() !== '' || Ext.getCmp('cboJenisPasien').getValue() !== 'Silahkan Pilih...')
	{
                strKriteria += '##@@##' + Ext.getCmp('cboJenisPasien').getValue();
	};

	return strKriteria;
};

function ValidasiTanggalReportIGDLaporandetail()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapIGDLaporandetail').dom.value > Ext.get('dtpTglAkhirLapIGDLaporandetail').dom.value)
    {
        ShowPesanWarningIGDLaporandetailReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningIGDLaporandetailReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function mComboJenisPasien()
{
    var cboJenisPasien = new Ext.form.ComboBox
	(
		{
			id:'cboJenisPasien',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Jenis Pasien ',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua Pasien'],[2, 'Pasien Baru'],[3, 'Pasien Lama']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetJenisPasien=b.data.displayText ;
				}
			}
		}
	);
	return cboJenisPasien;
};

function dataGrid_viInformasiUnit(mod_id)
{
    var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    datarefresh_viInformasiUnit()

    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
	];


	// declare the source Grid
        var firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 200,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'KD_UNIT',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA_UNIT',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),
   
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });
        var secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStore,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 200,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_UNIT',
                    title            : 'Unit',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'secondGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid.store.add(records);
                                    secondGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

	
	var FrmTabs_viInformasiUnit = new Ext.Panel
        (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
            height       : 250,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .99,
					layout: 'form',
					border: true,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[mComboPoliklinik(),
					mComboKelasSpesialRequestEntry(),
					mComboRuanganRequestEntry()
						
					]
				},
				{
					columnWidth: .99,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[
						
                                              getItemPaneltgl_filter(),
                                       
                                     
                                            
                                                mComboJenisPasien()
                                       
					]
				},
                                
                                       
                                {
                                columnWidth: .99,
                                layout: 'hBox',
                                border: false,
                                defaults: { margins: '0 5 0 0' },
                                style:{'margin-left':'421px','margin-top':'30px'},
                                anchor: '100%',
                                layoutConfig:
                                {
                                    padding: '3',
                                    pack: 'end',
                                    align: 'middle'
                                },
                                items:
                                [
                                    {
                                        xtype: 'button',
                                        text: nmBtnOK,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnOkLapIGDLaporandetail',
                                        handler: function()
                                        {
                                            var sendDataArray = [];
                                            secondGridStore.each(function(record){
                                            var recordArray = [record.get("KD_UNIT")];
                                            sendDataArray.push(recordArray);
											
                                            });
									
											
                                        
											 var criteria = GetCriteriaIGDLaporandetail();
                                            // criteria += '##@@##' + sendDataArray;
                                             frmDlgIGDLaporandetail.close();
                                             loadlaporanRWI('0', 'rep030201', criteria);
										
                                        }
                                    },
                                    {
                                        xtype: 'button',
                                        text: nmBtnCancel ,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnCancelLapIGDLaporandetail',
                                        handler: function()
                                        {
                                                frmDlgIGDLaporandetail.close();
                                        }
                                    }
                                    ]
                                }
			],

		
    }
    )

    return FrmTabs_viInformasiUnit;
}

function getItemPaneltgl_filter() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .50,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTglAwalLapIGDLaporandetail',
					    name: 'dtpTglAwalLapIGDLaporandetail',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '99%',
					   
					}
				]
			},
			 {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 15},
			{
			    columnWidth: .50,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[ 
		
					{
					    xtype: 'datefield',
					   // fieldLabel: 'Tanggal ',
					    id: 'dtpTglAkhirLapIGDLaporandetail',
					    name: 'dtpTglAkhirLapIGDLaporandetail',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '90%',
						
					}
				]
			}
		]
	}
    return items;
};


function datarefresh_viInformasiUnit()
{
    dataSource_unit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )
    //alert("refersh")
}

function mComboPoliklinik()
{
    var Field = ['KD_SPESIAL','SPESIALISASI'];
    ds_Poli_viDaftarRWI = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftarRWI.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'kd_spesial',
                Sortdir: 'ASC',
                target:'ViewSetupSpesial',
                param: "kd_spesial <> 0"
            }
        }
    )

    var cboPoliklinikRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboPoliklinikRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Spesialisasi...',
            fieldLabel: 'Spesialisasi ',
            align: 'Right',
            store: ds_Poli_viDaftarRWI,
            valueField: 'KD_SPESIAL',
            displayField: 'SPESIALISASI',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
				alert(b.data.KD_SPESIAL);
                                   loaddatastorekelas(b.data.KD_SPESIAL);
                                   //loaddatastoreDokterSpesial(b.data.KD_SPESIAL);
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }
        	}
        }
    )

    return cboPoliklinikRequestEntry;
};


function loaddatastorekelas(KD_SPESIAL)
{
          dsKelasRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
                            Sort: 'kd_kelas',
			    Sortdir: 'ASC',
			    target: 'ViewComboKelasSpesial',
			    param: 'where kd_spesial =~'+ KD_SPESIAL+ '~'
			}
                    }
                )
};
function loaddatastoreruangan(KD_KELAS)
{
    var kd_spesial = Ext.getCmp('cboPoliklinikRequestEntry').getValue();
    dsRuanganRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
                            Sort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ViewComboRuanganre',
			    param: 'where kd_spesial =~'+ kd_spesial + '~'+' And '+'k.kd_kelas =~'+ KD_KELAS + '~'
			}
                    }
                )
    
}

function mComboRuanganRequestEntry()
{
    var Field = ['KD_UNIT','NAMA'];

    dsRuanganRequestEntry = new WebApp.DataStore({fields: Field});


    var cboRuanganRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboRuanganRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Ruangan...',
		    fieldLabel: 'Ruang / Unit ',
		    align: 'Right',
		    store: dsRuanganRequestEntry,
		    valueField: 'KD_UNIT',
		    displayField: 'NAMA',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   loaddatastorekamar(b.data.KD_UNIT)
								
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboRuanganRequestEntry;
};

function mComboKelasSpesialRequestEntry()
{
    var Field = ['KD_KELAS','KELAS'];

    dsKelasRequestEntry = new WebApp.DataStore({fields: Field});


    var cboKelasRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKelasRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Kelas...',
		    fieldLabel: 'Kelas ',
		    align: 'Right',
                     
//		    anchor:'60%',
		    store: dsKelasRequestEntry,
		    valueField: 'KD_KELAS',
		    displayField: 'KELAS',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   loaddatastoreruangan(b.data.KD_KELAS)
								   
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboKelasRequestEntry;
};


	
