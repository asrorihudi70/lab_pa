
var dsRWIKeluarMasuk;
var selectRWIKeluarMasuk;
var selectNamaRWIKeluarMasuk;
var now = new Date();
var selectSetKeluarMasuk;
var frmDlgRWIKeluarMasuk;
var varLapRWIKeluarMasuk= ShowFormLapRWIKeluarMasuk();
var tmprbRWI;
var tmprbDateRWI;
var type_file=0;
var jenis='Pasien Yang Sudah Keluar';
function ShowFormLapRWIKeluarMasuk()
{
    frmDlgRWIKeluarMasuk= fnDlgRWIKeluarMasuk();
    frmDlgRWIKeluarMasuk.show();
};

function fnDlgRWIKeluarMasuk()
{
    var winRWIKeluarMasukReport = new Ext.Window
    (
        {
            id: 'winRWIKeluarMasukReport',
            title: 'Laporan Rawat Inap Pasien Per Keluar Masuk',
            closeAction: 'destroy',
            width:400,
            height: 310,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWIKeluarMasuk()],
            listeners:
            {
                activate: function()
                {
                     Ext.getCmp('dtpBulanAwalLapRWIKeluarMasuk').disable();
                     Ext.getCmp('dtpBulanAkhirLapRWIKeluarMasuk').disable();
                     tmprbRWI = " and k.tgl_KELUAR IS NOT NULL ";
                     tmprbDateRWI = 'Tanggal';
                }
            }

        }
    );

    return winRWIKeluarMasukReport;
};


function ItemDlgRWIKeluarMasuk()
{
    var PnlLapRWIKeluarMasuk = new Ext.Panel
    (
        {
            id: 'PnlLapRWIKeluarMasuk',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWIKeluarMasuk_Dept(),
                getItemLapRWIKeluarMasuk_Tanggal(),
                {
                    layout: 'absolute',
                    border: false,
                    width: 800,
                    height: 30,
                    items:
                    [
                        {
                            x: 175,
                            y: 1,
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWIKeluarMasuk',
                            handler: function(){
                                var criteria = GetCriteriaRWIKeluarMasuk();
                                loadMask.show();
                                // loadlaporanRWI('0', 'rep030203', criteria,function(){
                                	// frmDlgRWIKeluarMasuk.close();
                                	// loadMask.hide();
                                // });
								var params={
									criteria:criteria,
									type_file:type_file,
									jenis:jenis
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/rawat_inap/function_lap_RWI/cetak_lap_keluar_masuk");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();	
								loadMask.hide();
                            }
                        },
                        {
                            x: 250,
                            y: 1,
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWIKeluarMasuk',
                            handler: function()
                            {
                                    frmDlgRWIKeluarMasuk.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWIKeluarMasuk;
};

function GetCriteriaRWIKeluarMasuk()
{
	var strKriteria = '';
        
        strKriteria = tmprbRWI;
            
        if (tmprbDateRWI === 'Tanggal')
            {
                strKriteria += '##@@##' + 'Tanggal';
                strKriteria += '##@@##' + Ext.get('dtpTglAwalLapRWIKeluarMasuk').dom.value;
                strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRWIKeluarMasuk').dom.value;
                
            }else if(tmprbDateRWI === 'Bulan')
            {
                strKriteria += '##@@##' + 'Bulan';
                strKriteria += '##@@##' + getnewformatdate(Ext.get('dtpBulanAwalLapRWIKeluarMasuk').dom.value);
                strKriteria += '##@@##' + getnewformatdate(Ext.get('dtpBulanAkhirLapRWIKeluarMasuk').dom.value);
            }

	return strKriteria;
};


function ValidasiReportRWIKeluarMasuk()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWIKeluarMasuk').dom.value === '') || (Ext.get('dtpTglAkhirLapRWIKeluarMasuk').dom.value === '') || 
        (Ext.get('cboRujukanKeluarMasukSpesialRequestEntry').value === 'Silahkan Pilih...' || Ext.get('cboRujukanKeluarMasukSpesialRequestEntry').value === ' ' ))
    {
            if(Ext.get('dtpTglAwalLapRWIKeluarMasuk').dom.value === '')
            {
                    ShowPesanWarningRWIKeluarMasukReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            if(Ext.get('dtpTglAkhirLapRWIKeluarMasuk').dom.value === '')
            {
                    ShowPesanWarningRWIKeluarMasukReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.getCmp('cboRujukanKeluarMasukSpesialRequestEntry').getValue() === '' || Ext.getCmp('cboRujukanKeluarMasukSpesialRequestEntry').getValue() === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningRWIKeluarMasukReport(nmGetValidasiKosong('KeluarMasuk'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRWIKeluarMasuk()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWIKeluarMasuk').dom.value > Ext.get('dtpTglAkhirLapRWIKeluarMasuk').dom.value)
    {
        ShowPesanWarningRWIKeluarMasukReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWIKeluarMasukReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWIKeluarMasuk_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.98,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 800,
                height: 80,
                anchor: '100% 100%',
                items:
                [
                    {   
                        x : 20,
                        y : 10,
                        xtype: 'radio',
                        boxLabel: 'Pasien Yang Sudah keluar',
                        name: 'rb_auto',
                        id: 'rb_spesialisasi',
                        checked: true,
                        inputValue: '1',
                        listeners: {
                          check : function(cb, value) {
                                if (value)
                                {
                                    tmprbRWI = " and k.tgl_KELUAR IS NOT NULL ";
									jenis='Pasien Yang Sudah Keluar';
                                }
                                else 
                                {
                                    
                                }
                           }
                     }
                    },
                    {
                        x : 20,
                        y : 30,
                        xtype: 'radio',
                        boxLabel: 'Pasien Yang Belum keluar',
                        name: 'rb_auto',
                        id: 'rb_kelas',
                        inputValue: '2',
                        listeners: {
                          check : function(cb, value) {
                                if (value)
                                {
                                    tmprbRWI = " and k.tgl_KELUAR IS NULL ";
									jenis="Pasien Yang Belum Keluar";
                                }
                                else 
                                {
                                    
                                }
                           }
                     }
                    },
                    {
                        x : 20,
                        y : 50,
                        xtype: 'radio',
                        boxLabel: 'Semua Pasien',
                        name: 'rb_auto',
                        id: 'rb_ruang',
                        inputValue: '3',
                        listeners: {
                          check : function(cb, value) {
                                if (value)
                                {
                                    tmprbRWI = " ";
									jenis="Semua Pasien ";
                                }
                                else 
                                {
                                    
                                }
                           }
                     }
                    }
                ]
            }
        ]
    }
    return items;
};

function getItemLapRWIKeluarMasuk_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                columnWidth: .99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 800,
                height: 150,
                anchor: '100% 100%',
                items:
                [
                    {
                        x : 20,
                        y : 20,
                        xtype: 'radio',
                        boxLabel: 'Daftar Pasien Masuk Pada Tanggal',
                        name: 'rb_dateawal',
                        id: 'rb_dateawal',
                        checked : true,
                        listeners: {
                          check : function(cb, value) {
                                if (value)
                                {
                                    tmprbDateRWI = 'Tanggal';
                                    Ext.getCmp('rb_Bulanawal').setValue(false);
                                    Ext.getCmp('dtpTglAwalLapRWIKeluarMasuk').enable();
                                    Ext.getCmp('dtpTglAkhirLapRWIKeluarMasuk').enable();
                                    Ext.getCmp('dtpBulanAkhirLapRWIKeluarMasuk').disable();
                                    Ext.getCmp('dtpBulanAkhirLapRWIKeluarMasuk').disable();
                                   
                                }
                                else 
                                {
                                    Ext.getCmp('rb_Bulanawal').setValue(true);
                                    Ext.getCmp('dtpTglAwalLapRWIKeluarMasuk').disable();
                                    Ext.getCmp('dtpTglAkhirLapRWIKeluarMasuk').disable();
                                    Ext.getCmp('dtpBulanAwalLapRWIKeluarMasuk').enable();
                                    Ext.getCmp('dtpBulanAwalLapRWIKeluarMasuk').enable();
                                }
                           }
                     }
                    },
                    {
                        x: 20,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpTglAwalLapRWIKeluarMasuk',
                        format: 'd-M-Y',
                        value:now
                    },
                    {
                        x: 130,
                        y: 40,
                        xtype: 'label',
                        text: 's/d Tanggal'
                    },
                    {
                        x: 195,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirLapRWIKeluarMasuk',
                        format: 'd-M-Y',
                        value:now
                    },
                    
                    {
                        x : 20,
                        y : 70,
                        xtype: 'radio',
                        boxLabel: 'Daftar Pasien Masuk Pada Tanggal',
                        name: 'rb_Bulanawal',
                        id: 'rb_Bulanawal',
                        listeners: {
                          check : function(cb, value) {
                                if (value)
                                {
                                    tmprbDateRWI = 'Bulan';
                                    Ext.getCmp('rb_dateawal').setValue(false);
                                    Ext.getCmp('dtpBulanAwalLapRWIKeluarMasuk').enable();
                                    Ext.getCmp('dtpBulanAkhirLapRWIKeluarMasuk').enable();
                                    Ext.getCmp('dtpTglAwalLapRWIKeluarMasuk').disable();
                                    Ext.getCmp('dtpTglAkhirLapRWIKeluarMasuk').disable();
                                }
                                else 
                                {
                                    Ext.getCmp('rb_dateawal').setValue(true);
                                    Ext.getCmp('dtpBulanAwalLapRWIKeluarMasuk').disable();
                                    Ext.getCmp('dtpBulanAkhirLapRWIKeluarMasuk').disable();
                                    Ext.getCmp('dtpTglAwalLapRWIKeluarMasuk').enable();
                                    Ext.getCmp('dtpTglAkhirLapRWIKeluarMasuk').enable();
                                }
                           }
                     }
                    },
                    {
                        x: 20,
                        y: 90,
                        xtype: 'datefield',
                        id: 'dtpBulanAwalLapRWIKeluarMasuk',
                        format: 'M-Y',
                        value:now
                    },
                    {
                        x: 130,
                        y: 90,
                        xtype: 'label',
                        text: 's/d Tanggal'
                    },
                    {
                        x: 195,
                        y: 90,
                        xtype: 'datefield',
                        id: 'dtpBulanAkhirLapRWIKeluarMasuk',
                        format: 'M-Y',
                        value:now
                    },
					{
                        x: 20,
                        y: 120,
                        xtype: 'label',
                        text: 'Tipe File'
                    },
					{
                        x: 130,
                        y: 120,
                        xtype: 'label',
                        text: ':'
                    },
					{
						x: 140,
                        y: 120,
						xtype: 'checkbox',
						id: 'CekLapPilihFile',
						hideLabel:false,
						boxLabel: 'Excel',
						checked: false,
						listeners: 
						{
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihFile').getValue()===true)
								{
									type_file =1;
								}
								else
								{
									type_file =0;
								   
								}
							}
						}
					}
                ]
            }
        ]
    }
    return items;
};

function mComboUnitLapRWIKeluarMasuk()
{
     var cboKeluarMasuk = new Ext.form.ComboBox
	(
		{
			id:'cboKeluarMasuk',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Wilayah ',
			width:80,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Prop'],[2, 'Kab'],[3, 'Kec'],[4, 'Kel']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetKeluarMasuk,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetKeluarMasuk=b.data.displayText ;
				}
			}
		}
	);
	return cboKeluarMasuk;
};

function mcomboKeluarMasukSpesial()
{
    var Field = ['no_kamar','kamar'];
    ds_KeluarMasukSpesial_viDaftar = new WebApp.DataStore({fields: Field});

	ds_KeluarMasukSpesial_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewSetupKelasSpesial',
                param: ""
            }
        }
    )

    var cboRujukanKeluarMasukSpesialRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanKeluarMasukSpesialRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih KeluarMasuk...',
            fieldLabel: 'KeluarMasuk ',
            align: 'Right',
            store: ds_KeluarMasukSpesial_viDaftar,
            valueField: 'no_kamar',
            displayField: 'kamar',
            anchor: '95%',
            Width:'800',
            x: 95,
            y: 1,
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                },
                    'render': function(c)
                                {
                                    
                                }


		}
        }
    )

    return cboRujukanKeluarMasukSpesialRequestEntry;
}