
var dsRWJPasienPerDokter;
var selectRWJPasienPerDokter;
var selectNamaRWJPasienPerDokter;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJPasienPerDokter;
var varLapRWJPasienPerDokter= ShowFormLapRWJPasienPerDokter();
var dsRWJ;
var dataSource_unit;
function ShowFormLapRWJPasienPerDokter()
{
    frmDlgRWJPasienPerDokter= fnDlgRWJPasienPerDokter();
    frmDlgRWJPasienPerDokter.show();
};

function fnDlgRWJPasienPerDokter()
{
    var winRWJPasienPerDokterReport = new Ext.Window
    (
        {
            id: 'winRWJPasienPerDokterReport',
            title: 'Laporan Per Dokter Per Diagnosa',
            closeAction: 'destroy',
            width:450,
            height: 460,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJPasienPerDokter()]
        }
    );

    return winRWJPasienPerDokterReport;
};

function ItemDlgRWJPasienPerDokter()
{
    var PnlLapRWJPasienPerDokter = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPasienPerDokter',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 460,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
				gridUnitLapRWJPasienPerDokter(),
                getItemLapRWJPasienPerDokter_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJPasienPerDokter',
					handler: function()
					{
					   if (ValidasiReportRWJPasienPerDokter() === 1)
					   {					
							var sendDataArray = [];
							secondGridStore.each(function(record){
								var recordArray = [record.get("KD_UNIT")];
								sendDataArray.push(recordArray);
							});
							if (sendDataArray.length === 0)
							{  
								ShowPesanWarningRWJPasienPerDokterReport('Isi kriteria unit dengan drag and drop','WARNING');
								loadMask.hide();
							} else{
								var params={
									// kd_poli:Ext.getCmp('cboUnitPilihanRwJPasien').getValue(),
									kd_dokter:Ext.getCmp('cboDokterPilihanRwJPasien').getValue(),
									tglAwal:Ext.getCmp('dtpTglAwalLapRWJPasienPerDokter').getValue(),
									tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJPasienPerDokter').getValue(),
									type_file:Ext.getCmp('CekLapPilihTypeExcel').getValue(),
									order_by:Ext.getCmp('cboOrderBy').getValue(),
									poliklinik:sendDataArray
								} 
								//console.log(params);
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/rawat_inap/lap_RWIPasienPerDokter/cetak");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								frmDlgRWJPasienPerDokter.close(); 

							}
							
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Print',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnPrintLapRWJPasienPerDokter',
					handler: function()
					{
					   if (ValidasiReportRWJPasienPerDokter() === 1)
					   {	
							var sendDataArray = [];
							secondGridStore.each(function(record){
							var recordArray = [record.get("KD_UNIT")];
							sendDataArray.push(recordArray);
							
							});
							if (sendDataArray.length === 0)
							{  
								ShowPesanWarningRWJPasienPerDokterReport('Isi kriteria unit dengan drag and drop','WARNING');
								loadMask.hide();
							} else{
								var params={
									// kd_poli:Ext.getCmp('cboUnitPilihanRwJPasien').getValue(),
									kd_dokter:Ext.getCmp('cboDokterPilihanRwJPasien').getValue(),
									tglAwal:Ext.getCmp('dtpTglAwalLapRWJPasienPerDokter').getValue(),
									tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJPasienPerDokter').getValue(),
									type_file:Ext.getCmp('CekLapPilihTypeExcel').getValue(),
									order_by:Ext.getCmp('cboOrderBy').getValue(),
									poliklinik:sendDataArray
								} 
								//console.log(params);
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_pasienperdokter_rwj/cetak_direct");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								frmDlgRWJPasienPerDokter.close(); 
							}
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJPasienPerDokter',
					handler: function()
					{
						frmDlgRWJPasienPerDokter.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJPasienPerDokter;
};


function ValidasiReportRWJPasienPerDokter()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJPasienPerDokter').dom.value === '')
	{
		ShowPesanWarningRWJPasienPerDokterReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
    return x;
};

function ValidasiTanggalReportRWJPasienPerDokter()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJPasienPerDokter').dom.value > Ext.get('dtpTglAkhirLapRWJPasienPerDokter').dom.value)
    {
        ShowPesanWarningRWJPasienPerDokterReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJPasienPerDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
function datarefresh_viInformasiUnit()
{
    /*dataSource_unit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnitB',
                param: "kd_bagian=1 and type_unit=false"
            }
        }
    )*/
    dataSource_unit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'nama_unit',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=1 and type_unit=false"
            }
        }
    )
    //alert("refersh")
}


function gridUnitLapRWJPasienPerDokter(){
	var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    datarefresh_viInformasiUnit()

    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
	];


	// declare the source Grid
	firstGrid = new Ext.grid.GridPanel({
		ddGroup          : 'secondGridDDGroup',
		store            : dataSource_unit,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		enableDragDrop   : true,
		height           : 200,
		stripeRows       : true,
		trackMouseOver   : true,
		title            : 'Poliklinik',
		anchor           : '100% 100%',
		plugins          : [new Ext.ux.grid.FilterRow()],
		colModel         : new Ext.grid.ColumnModel
					(
						[
							new Ext.grid.RowNumberer(),
							{
									id: 'colNRM_viDaftar',
									header: 'No.Medrec',
									dataIndex: 'KD_UNIT',
									sortable: true,
									hidden : true
							},
							{
									id: 'colNMPASIEN_viDaftar',
									header: 'Nama',
									dataIndex: 'NAMA_UNIT',
									sortable: true,
									width: 50
							}
						]
					),
		listeners : {
			afterrender : function(comp) {
			var firstGridDropTargetEl = firstGrid.getView().scroller.dom;
			var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
					ddGroup    : 'firstGridDDGroup',
					notifyDrop : function(ddSource, e, data){
							var records =  ddSource.dragData.selections;
							Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
							firstGrid.store.add(records);
							firstGrid.store.sort('KD_UNIT', 'ASC');
							return true
					}
			});
			}
		},

		viewConfig: 
			{
					forceFit: true
			}
	});

	secondGridStore = new Ext.data.JsonStore({
		fields : fields,
		root   : 'records'
	});

        // create the destination Grid
	secondGrid = new Ext.grid.GridPanel({
		ddGroup          : 'firstGridDDGroup',
		store            : secondGridStore,
		columns          : cols,
		enableDragDrop   : true,
		height           : 200,
		stripeRows       : true,
		autoExpandColumn : 'KD_UNIT',
		title            : 'Poliklinik',
		listeners : {
		afterrender : function(comp) {
		var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
		var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
				ddGroup    : 'secondGridDDGroup',
				notifyDrop : function(ddSource, e, data){
						var records =  ddSource.dragData.selections;
						Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
						secondGrid.store.add(records);
						secondGrid.store.sort('KD_UNIT', 'ASC');
						return true
				}
		});
		}
	},
	viewConfig: 
		{
				forceFit: true
		}
	});
	
	var FrmTabs_viInformasiUnit = new Ext.Panel
	(
		{
		    id: 'pilihunitLapRWJPasienPerDokter',
		    closable: true,
		    region: 'center',
		    layout: 'column',
            height   : 250,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    // iconCls: '',
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					anchor: '100% 100%',
					items:
					[firstGrid
						
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid
						
					]
				},
				{
                                        
					columnWidth: .60,
					layout: 'form',
					border: false,
					labelAlign: 'lfet',
					labelWidth: 5,
					items:
					[
						{
							xtype: 'checkbox',
							id: 'CekLapPilihLapRWJPasienPerDokter',
							hideLabel:false,
							boxLabel: 'Pilih Semua Unit',
							checked: false,
							listeners: 
							{
								check: function()
								{
								   if(Ext.getCmp('CekLapPilihLapRWJPasienPerDokter').getValue()===true)
									{
										 firstGrid.getSelectionModel().selectAll();
									}
									else
									{
										firstGrid.getSelectionModel().clearSelections();
									}
								}
							}
						},
					]
				}
			]
		}
	)
	return FrmTabs_viInformasiUnit;
}
function getItemLapRWJPasienPerDokter_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  '100%',
				height: 430,
				items:
				[
					{
						x: 10,
						y: 10,
						width: 170,
						xtype: 'label',
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJPasienPerDokter',
						format: 'd-M-Y',
						value:now,
						//anchor: '99%'
					},
					{
						x: 190,
						y: 10,
						width: 10,
						xtype: 'label',
						text : 's/d'
					},
					{
						x: 220,
						y: 10,
						width: 170,
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJPasienPerDokter',
						format: 'd-M-Y',
						value:now,
						//anchor: '100%'
					},
					// {
						// x: 10,
						// y: 40,
						// xtype: 'label',
						// text: 'Poli '
					// }, {
						// x: 190,
						// y: 40,
						// xtype: 'label',
						// text: ' : '
					// },
						// mComboUnitPilihanRwJPasienPerDokter(),
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Dokter '
					}, {
						x: 190,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
						mComboDokterPilihanRwJPasienPerDokter(),
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Order By '
					}, {
						x: 190,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
						mComboOrder(),
					{
						x: 10,
						y: 100,
						xtype: 'label',
						text: 'Type File '
					}, {
						x: 190,
						y: 100,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 220,
						y: 100,
						xtype: 'checkbox',
						id: 'CekLapPilihTypeExcel',
						hideLabel:false,
						boxLabel: 'Excel',
						checked: false,
						listeners: 
						{
						}
					}
				]
			},
     
        ]
    }
    return items;
};

function mComboUnitPilihanRwJPasienPerDokter()
{

   	var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});


	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnitB',
                param: "left(kd_unit, 1)='1' and type_unit=false"
            }
        }
    )

    var cboPilihanRwJTransaksi = new Ext.form.ComboBox
	(
			{
			x: 220,
			y: 40,
            id: 'cboUnitPilihanRwJPasien',
            ypeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Dokter ...',
            fieldLabel: 'Dokter ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            width:170,
            value:'Semua',
			tabIndex:29,
            listeners:
            {
                'select': function(a, b, c)
				{
						//Dokterpilihanpasien=b.data.KD_UNIT;
				},
			}
        }
	);
	return cboPilihanRwJTransaksi;
};

function mComboDokterPilihanRwJPasienPerDokter()
{

   var Field = ['KD_DOKTER','NAMA'];
    ds_Dokter_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Dokter_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA',
                Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: " WHERE left(dokter_klinik.kd_unit, 1)='2'"
            }
        }
    )
    var cboPilihanRwJTransaksi = new Ext.form.ComboBox
	(
			{
			x: 220,
			y: 40,
            id: 'cboDokterPilihanRwJPasien',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Dokter ...',
            fieldLabel: 'Dokter ',
            align: 'Right',
            sorters: [{
		        property: 'NAMA',
		        direction: 'ASC'
		    }],
            store: ds_Dokter_viDaftar,
            valueField: 'KD_DOKTER',
            displayField: 'NAMA',
            value:'Semua',
            width:170,
			tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
					{
						//Dokterpilihanpasien=b.data.KD_UNIT;
					},


		}
        }
	);
	return cboPilihanRwJTransaksi;
};

function mComboOrder()
{
    var cboOrderBy = new Ext.form.ComboBox
    (
        {
            x: 220,
            y: 70,
            id:'cboOrderBy',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: 'Order  ',
            width:170,
            value:1,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                    data: [[0, 'Medrec'], [1, 'Nama Pasien'], [2, 'Tanggal Masuk'], [3, 'Penjamin']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:'Medrec',
        }
    );
    return cboOrderBy;
};