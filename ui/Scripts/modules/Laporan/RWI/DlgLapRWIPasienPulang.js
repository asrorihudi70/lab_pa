var fields = [
    {name: 'KD_UNIT', mapping : 'KD_UNIT'},
    {name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];

var fieldsCustomer = [
    {name: 'KD_CUSTOMER', mapping : 'KD_CUSTOMER'},
    {name: 'CUSTOMER', mapping : 'CUSTOMER'}
];

var fieldsPayment = [
    {name: 'KD_PAY', mapping : 'KD_PAY'},
    {name: 'URAIAN', mapping : 'URAIAN'}
];

var cols = [
    { id : 'kd_unit', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'kd_unit',hidden : true},
    { header: "Nama", width: 50, sortable: true, dataIndex: 'nama_unit'}
];

var colsPayment = [
    { id : 'kd_pay', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'kd_pay',hidden : true},
    { header: "Uraian", width: 50, sortable: true, dataIndex: 'uraian'}
];

var colsCustomer = [
    { id : 'kd_customer', header: "Kode Customer", width: 160, sortable: true, dataIndex: 'kd_customer',hidden : true},
    { header: "Customer", width: 50, sortable: true, dataIndex: 'customer'}
];

var colsUnit = [
    { id : 'KD UNIT', header: "Kode unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
    { header: "NAMA UNIT", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
];

var colsKamar = [
    { id : 'NO_KAMAR', header: "No Kamar", width: 160, sortable: true, dataIndex: 'NO_KAMAR',hidden : true},
    { header: "NAMA KAMAR", width: 50, sortable: true, dataIndex: 'NAMA_KAMAR'}
];

var dataSource_Spesialisasi_LapRWI;
var dataSource_Kelas_LapRWI;
var dataNoDipilih_LapPasienPulang = "";
var dataUnitDipilih_LapPasienPulang = "";
var dataKelompokDipilih_LapPasienPulang = "";
var secondGridStoreLapPenerimaanKamar;
var secondGridStoreLapPenerimaan;
var secondGridStoreLapPenerimaanPaymentLapPenerimaan;
var secondGridStoreLapPenerimaanCustomer;
var kelpas=-1;
var firstGrid_customer;
var firstGrid_DetailUnit;
var DlgLapRWJPenerimaan={
    vars:{
        comboSelect:null
    },
    coba:function(){
        var $this=this;
        $this.DateField.startDate
    },
    ArrayStore:{
        dokter:new Ext.data.ArrayStore({fields:[]})
    },
    CheckboxGroup:{
        shift:null,
        tindakan:null,
        transfer:null
    },
    ComboBox:{
        cboSpesialisasi:null,
        cboKelas:null,
        kelPasien1:null,
        combo1:null,
        combo2:null,
        combo3:null,
        combo0:null,
        poliklinik:null,
        dokter:null,
        combouser:null,
        detailUnit:null,
        cboUrut:null,
    },
    DataStore:{
        dataSpesialisasi:null,
        dataKelas:null,
        combo2:null,
        combo3:null,
        poliklinik:null,
        combo1:null,
        combouser:null,
        gridcustomer:null,
        gridDetailUnit:null,
        gridDetailKamar:null,
        dataSource_unit:null,
    },
    DateField:{
        startDate:null,
        endDate:null
    },
    Window:{
        main:null
    },
    comboOnSelect:function(val){
        var $this=this;
        if(val==-1){
            kelpas =-1;
            $this.loadcustomer(kelpas);
            secondGridStoreLapPenerimaanCustomer.removeAll();
        }else if(val==0){
            kelpas =0;
            $this.loadcustomer(kelpas);
            secondGridStoreLapPenerimaanCustomer.removeAll();
        }else if(val==1){
            kelpas =1;
            $this.loadcustomer(kelpas);
            secondGridStoreLapPenerimaanCustomer.removeAll();
        }else if(val==2){
            kelpas =2;
            $this.loadcustomer(kelpas);
            secondGridStoreLapPenerimaanCustomer.removeAll();
        }
    },
    initPrint:function(){
        var $this=this;
        	/*console.log("UNIT         = "+dataNoDipilih_LapPasienPulang.substring(0, dataNoDipilih_LapPasienPulang.length-1));
        	console.log("SPESIALISASI = "+Ext.getCmp('cboSpesialisasi_LapPasienPulang').getValue());
        	console.log("Kelas        = "+Ext.getCmp('cboKelas').getValue());
        	console.log("Tanggal Awal = "+Ext.getCmp('textFormTanggal_Awal').getValue());
        	console.log("Tanggal Akhir= "+Ext.getCmp('textFormTanggal_Akhir').getValue());
        	console.log("Kelompok     = "+dataKelompokDipilih_LapPasienPulang.substring(0, dataKelompokDipilih_LapPasienPulang.length-1));*/

            for (var i = 0; i < secondGridStoreLapPenerimaan.data.length; i++) {
                dataUnitDipilih_LapPasienPulang += "'"+secondGridStoreLapPenerimaan.data.items[i].data.KD_UNIT+"',";
            }
            for (var i = 0; i < secondGridStoreLapPenerimaanKamar.data.length; i++) {
                dataNoDipilih_LapPasienPulang += "'"+secondGridStoreLapPenerimaanKamar.data.items[i].data.NO_KAMAR+"',";
            }
            
            for (var i = 0; i < secondGridStoreLapPenerimaanCustomer.data.length; i++) {
                dataKelompokDipilih_LapPasienPulang += "'"+secondGridStoreLapPenerimaanCustomer.data.items[i].data.kd_customer+"',";
            }
            
        	var params = {
                tmp_kd_unit   : dataUnitDipilih_LapPasienPulang.substring(0, dataUnitDipilih_LapPasienPulang.length-1),
        		tmp_no_kamar  : dataNoDipilih_LapPasienPulang.substring(0, dataNoDipilih_LapPasienPulang.length-1),
        		tmp_kd_spesial: Ext.getCmp('cboSpesialisasi_LapPasienPulang').getValue(),
        		kd_kelas      : Ext.getCmp('cboKelas').getValue(),
        		start_date    : Ext.getCmp('textFormTanggal_Awal').getValue(),
        		last_date     : Ext.getCmp('textFormTanggal_Akhir').getValue(),
                tmp_kd_customer: dataKelompokDipilih_LapPasienPulang.substring(0, dataKelompokDipilih_LapPasienPulang.length-1),
                type_file     : Ext.getCmp('CekLapPilihFile_LapPulangpasien').getValue(),
        		sorting_by    : Ext.getCmp('cboUrut_LapRWIPulang').getValue(),
        	};
            
            // console.log($this.DataStore.dataSource_unit);
            // console.log(secondGridStoreLapPenerimaanKamar.data.length);
            // console.log(params);

            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("target", "_blank");
            form.setAttribute("action", baseURL + "index.php/rawat_inap/function_rwi/laporan_pasien_pulang");
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "data");
            hiddenField.setAttribute("value", Ext.encode(params));
            form.appendChild(hiddenField);
            document.body.appendChild(form);
            form.submit();
            loadMask.hide();
         
        
    },
    messageBox:function(modul, str, icon){
        Ext.MessageBox.show
        (
            {
                title: modul,
                msg:str,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.icon,
                width:300
            }
        );
    },
    loaduser:function(){
        var $this=this;
        Ext.Ajax.request({
                url: baseURL + "index.php/rawat_jalan/function_laporan_PTP/getUser",
                params: '0',
                failure: function(o){
                    var cst = Ext.decode(o.responseText);
                },      
                success: function(o) {
                    $this.ComboBox.combouser.store.removeAll();
                    var cst = Ext.decode(o.responseText);

                    for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                        var recs    = [],recType =  $this.DataStore.combouser.recordType;
                        var o=cst['listData'][i];
                
                        recs.push(new recType(o));
                        $this.DataStore.combouser.add(recs);
                        // console.log(o);
                    }
                }
            });
    },
    getUser:function(){
        var $this=this;
        var Field = ['kd_user','full_name'];
        $this.DataStore.combouser = new WebApp.DataStore({fields: Field});
        $this.loaduser();
        $this.ComboBox.combouser = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            width:150,
            store: $this.DataStore.combouser,
            valueField: 'kd_user',
            displayField: 'full_name',
            value:'Semua',
            listeners:{
                'select': function(a,b,c){
                }
            }
        });
        return $this.ComboBox.combouser;
    },
    

    seconGridPenerimaan : function(){
        var secondGrid;
        var $this=this;
        secondGridStoreLapPenerimaan = new Ext.data.JsonStore({
            fields : colsUnit,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStoreLapPenerimaan,
                    columns          : colsUnit,
                    autoScroll       : true,
                    id               : 'IDSecondGridPenerimaan_LapPasienPulang',
                    columnLines      : true,
                    border           : true,
                    enableDragDrop   : true,
                    enableDragDrop   : true,
                    height           : 180,
                    stripeRows       : true,
                    autoExpandColumn : 'kd_unit',
                    title            : 'Unit yang dipilih',
                    listeners : {
                        afterrender : function(comp) {
                            var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                            var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                                    ddGroup    : 'secondGridDDGroup',
                                    notifyDrop : function(ddSource, e, data){
                                            var records =  ddSource.dragData.selections;
                                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                            secondGrid.store.add(records);

                                            console.log(secondGrid);
    										$this.DataStore.gridDetailKamar.removeAll();
                                            $this.loadDataUnit(secondGrid.store.data.items);
                                            // console.log(secondGrid.store.data.items);
                                            // console.log(secondGrid.store.data.items.length);
                                            //secondGrid.store.sort('kd_unit', 'ASC');
                                            return true
                                    }
                            });
                        }
                    },
                viewConfig: 
                {
                        forceFit: true
                }
        });
        return secondGrid;
    },
    firstGridPenerimaan : function(){
        var firstGrid;
        //var dataSource_unit;
        var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
        dataSource_unit         = new WebApp.DataStore({fields: Field_poli_viDaftar});
        
        Ext.Ajax.request({
            url: baseURL + "index.php/rawat_inap/function_rwi/getUnit",
            params: {parent : 1},
            failure: function(o){
                var cst = Ext.decode(o.responseText);
            },      
            success: function(o) {
                 firstGrid.store.removeAll();
                var cst = Ext.decode(o.responseText);

                for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                    var recs    = [],recType =  dataSource_unit.recordType;
                    var o=cst['listData'][i];
            
                    recs.push(new recType(o));
                    dataSource_unit.add(recs);
                    // console.log(o);
                }
            }
        });
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 180,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'KD_UNIT',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA_UNIT',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
            listeners : {
                afterrender : function(comp) {
                    var secondGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                }
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid;
    },
    //
    
    seconGridPenerimaanCustomer : function(){
        
        var secondGrid;
        secondGridStoreLapPenerimaanCustomer = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStoreLapPenerimaanCustomer,
                    columns          : colsCustomer,
                    autoScroll       : true,
                    columnLines      : true,
                    border           : true,
                    enableDragDrop   : true,
                    enableDragDrop   : true,
                    height           : 150,
                    stripeRows       : true,
                    autoExpandColumn : 'kd_customer',
                    title            : 'Customer yang dipilih',
            		anchor           : '100% 100%',
                    listeners : {
                        afterrender : function(comp) {
                            var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                            var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                                    ddGroup    : 'secondGridDDGroup',
                                    notifyDrop : function(ddSource, e, data){
                                            var records =  ddSource.dragData.selections;
                                            // dataKelompokDipilih_LapPasienPulang += "'"+records[0].data.kd_customer+"',";
                                            // console.log(dataKelompokDipilih_LapPasienPulang);
                                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                            secondGrid.store.add(records);
                                            secondGrid.store.sort('kd_customer', 'ASC');
                                            return true
                                    }
                            });
                        }
                    },
                viewConfig: 
                {
                        forceFit: true
                }
        });
        return secondGrid;
    },

    loadcustomer:function(param){
        var $this=this;
        Ext.Ajax.request({
                url: baseURL + "index.php/rawat_inap/function_rwi/getCustomer",
                params: {kelpas : ''},
                failure: function(o){
                    var cst = Ext.decode(o.responseText);
                },      
                success: function(o) {
                    firstGrid_customer.store.removeAll();
                    var cst = Ext.decode(o.responseText);

                    for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                        var recs    = [],recType =  $this.DataStore.gridcustomer.recordType;
                        var o=cst['listData'][i];
                
                        recs.push(new recType(o));
                        $this.DataStore.gridcustomer.add(recs);
                        // console.log(o);
                    }
                }
            });
    },
    firstGridPenerimaanCustomer : function(){
        var $this=this;
        
        var dataSource_customer;
        var Field_poli_viDaftar = ['kd_customer','customer'];
        
        $this.DataStore.gridcustomer         = new WebApp.DataStore({fields: Field_poli_viDaftar});
        $this.loadcustomer(kelpas);
        
        firstGrid_customer = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : $this.DataStore.gridcustomer,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 150,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Customer',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'kd_customer',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Customer',
                                                    dataIndex: 'customer',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
            listeners : {
                afterrender : function(comp) {
                    var secondGridDropTargetEl = firstGrid_customer.getView().scroller.dom;
                    var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid_customer.store.add(records);
                                    firstGrid_customer.store.sort('kd_customer', 'ASC');
                                    return true
                            }
                    });
                }
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid_customer;
    },
    
    firstGridPayment : function(){
        var firstGridPayment;
        var dataSource_payment;
        var Field_payment = ['kd_pay','uraian'];
        dataSource_payment         = new WebApp.DataStore({fields: Field_payment});
            Ext.Ajax.request({
                url: baseURL + "index.php/rawat_jalan/function_laporan_PTP/getPayment",
                params: '0',
                failure: function(o){
                    var cst = Ext.decode(o.responseText);
                },      
                success: function(o) {
                    firstGridPayment.store.removeAll();
                    var cst = Ext.decode(o.responseText);

                    for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                        var recs    = [],recType =  dataSource_payment.recordType;
                        var o=cst['listData'][i];
                
                        recs.push(new recType(o));
                        dataSource_payment.add(recs);
                        console.log(o);
                    }
                }
            });

            firstGridPayment = new Ext.grid.GridPanel({
                ddGroup          : 'secondGridDDGroupPayment',
                store            : dataSource_payment,
                autoScroll       : true,
                columnLines      : true,
                border           : true,
                enableDragDrop   : true,
                height           : 210,
                stripeRows       : true,
                trackMouseOver   : true,
                title            : 'Payment',
                anchor           : '100% 100%',
                plugins          : [new Ext.ux.grid.FilterRow()],
                colModel         : new Ext.grid.ColumnModel
                (
                    [
                    new Ext.grid.RowNumberer(),
                        {
                            id: 'colKD_pay',
                            header: 'Kode Pay',
                            dataIndex: 'kd_pay',
                            sortable: true,
                            hidden : true
                        },
                        {
                            id: 'colKD_uraian',
                            header: 'Uraian',
                            dataIndex: 'uraian',
                            sortable: true,
                            width: 50
                        }
                    ]
                ),   
                listeners : {
                    afterrender : function(comp) {
                        var secondGridDropTargetEl = firstGridPayment.getView().scroller.dom;
                        var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                                ddGroup    : 'firstGridDDGroupPayment',
                                notifyDrop : function(ddSource, e, data){
                                        var records =  ddSource.dragData.selections;
                                        Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                        firstGridPayment.store.add(records);
                                        firstGridPayment.store.sort('kd_pay', 'ASC');
                                        return true
                                }
                        });
                    }
                },
                    viewConfig: 
                        {
                                forceFit: true
                        }
            });
        return firstGridPayment;
    },
    seconGridPayment : function(){
        //var secondGridStoreLapPenerimaanPaymentLapPenerimaan;
        var secondGridPayment;
        secondGridStoreLapPenerimaanPaymentLapPenerimaan = new Ext.data.JsonStore({
            fields : fieldsPayment,
            root   : 'records'
        });

        // create the destination Grid
            secondGridPayment = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroupPayment',
                    store            : secondGridStoreLapPenerimaanPaymentLapPenerimaan,
                    columns          : colsPayment,
                    autoScroll       : true,
                    columnLines      : true,
                    border           : true,
                    enableDragDrop   : true,
                    enableDragDrop   : true,
                    height           : 210,
                    stripeRows       : true,
                    autoExpandColumn : 'kd_pay',
                    title            : 'Payment yang dipilih',
                    listeners : {
                        afterrender : function(comp) {
                            var secondGridDropTargetEl = secondGridPayment.getView().scroller.dom;
                            var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                                    ddGroup    : 'secondGridDDGroupPayment',
                                    notifyDrop : function(ddSource, e, data){
                                            var records =  ddSource.dragData.selections;
                                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                            secondGridPayment.store.add(records);
                                            secondGridPayment.store.sort('kd_pay', 'ASC');
                                            return true
                                    }
                            });
                        }
                    },
                viewConfig: 
                {
                        forceFit: true
                }
        });
        return secondGridPayment;
    },


    firstGridPenerimaanDetailKamar : function(){
        var $this=this;
        
        var dataSource_customer;
        var Field_poli_viDaftar = ['NO_KAMAR','NAMA_KAMAR'];
        
        $this.DataStore.gridDetailKamar         = new WebApp.DataStore({fields: Field_poli_viDaftar});
        // $this.loadcustomer(kelpas);
        
        firstGrid_DetailUnit = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : $this.DataStore.gridDetailKamar,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 150,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Kamar Tersedia',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNoKamar_viDaftar',
                                                    header: 'No Kamar',
                                                    dataIndex: 'NO_KAMAR',
                                                    sortable: true,
                                                    hidden : true
                                            },{
                                                    id: 'colNamaKamar_viDaftar',
                                                    header: 'Unit',
                                                    dataIndex: 'NAMA_KAMAR',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
            listeners : {
                afterrender : function(comp) {
                    var secondGridDropTargetEl = firstGrid_DetailUnit.getView().scroller.dom;
                    var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                var records =  ddSource.dragData.selections;
                                console.log(ddSource);
                                Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                firstGrid_DetailUnit.store.add(records);
                                firstGrid_DetailUnit.store.sort('NO_KAMAR', 'ASC');
                                return true
                            }
                    });
                }
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid_DetailUnit;
    },

    seconGridPenerimaanDetailKamar : function(){
        
        var secondGrid;
        secondGridStoreLapPenerimaanKamar = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStoreLapPenerimaanKamar,
                    columns          : colsKamar,
                    autoScroll       : true,
                    columnLines      : true,
                    border           : true,
                    enableDragDrop   : true,
                    enableDragDrop   : true,
                    height           : 150,
                    stripeRows       : true,
                    autoExpandColumn : 'kd_customer',
                    title            : 'Kamar yang dipilih',
            		anchor           : '100% 100%',
                    listeners : {
                        afterrender : function(comp) {
                            var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                            var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                                    ddGroup    : 'secondGridDDGroup',
                                    notifyDrop : function(ddSource, e, data){
                                            var records =  ddSource.dragData.selections;
                                            dataNoDipilih_LapPasienPulang += "'"+records[0].data.NO_KAMAR+"',";
                                            // console.log(dataNoDipilih_LapPasienPulang);
                                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                            secondGrid.store.add(records);
                                            secondGrid.store.sort('NO_KAMAR', 'ASC');
                                            return true
                                    }
                            });
                        }
                    },
                viewConfig: 
                {
                        forceFit: true
                }
        });
        return secondGrid;
    },

    loadDataUnit:function(params){
        var $this=this;
        var params_unit = "";
        for (var i = 0; i < params.length; i++) {
        	params_unit += "'"+params[i].data.NAMA_UNIT+"',";
        }
        params_unit = params_unit.substring(0, params_unit.length - 1);

        Ext.Ajax.request({
            url: baseURL + "index.php/rawat_inap/function_rwi/getDataUnit",
            params: {nama_unit : params_unit},
            failure: function(o){
                var cst = Ext.decode(o.responseText);
            },      
            success: function(o) {

				var fieldsDokterPenindak = [
					{name: 'NO_KAMAR', mapping : 'NO_KAMAR'},
					{name: 'NAMA_KAMAR', mapping : 'NAMA_KAMAR'}
				];
				var dsDataUnitPasienPulang = new WebApp.DataStore({ fields: fieldsDokterPenindak });
                var tmpKd_Parent = "";
                var cst = Ext.decode(o.responseText);
                    for(var i=0; i<cst.unit.length; i++){
                        var recs    = [],recType =  dsDataUnitPasienPulang.recordType;
                        var obj=cst.unit[i];

                        //dataUnitDipilih_LapPasienPulang += "'"+obj.
					// var o       = cst['data'][i];
						/*recs.push(new recType(obj));
						DataStorefirstGridStore.add(recs);*/
                		// console.log(obj);
                		recs.push(new recType(obj));
                		// console.log(recs);
                        $this.DataStore.gridDetailKamar.add(recs);
                        // console.log(o);
                    }
            }
        });
        // console.log(params_unit);

    },

    loadDetailUnit:function(param){
        var $this=this;
        Ext.Ajax.request({
                url: baseURL + "index.php/rawat_inap/function_rwi/getCustomer",
                params: {kelpas : ''},
                failure: function(o){
                    var cst = Ext.decode(o.responseText);
                },      
                success: function(o) {
                    firstGrid_DetailUnit.store.removeAll();
                    var cst = Ext.decode(o.responseText);

                    for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                        var recs    = [],recType =  $this.DataStore.gridcustomer.recordType;
                        var o=cst['listData'][i];
                
                        recs.push(new recType(o));
                        $this.DataStore.gridcustomer.add(recs);
                        // console.log(o);
                    }
                }
            });
    },

    /*
		LOAD DATA STORE SPESIALISASI
    */
    loadDataSpesialisasi : function(){
        var Field_Spesialisasi = ['KD_SPESIAL', 'SPESIALISASI'];
        dataSource_Spesialisasi_LapRWI         = new WebApp.DataStore({fields: Field_Spesialisasi});
	    dataSource_Spesialisasi_LapRWI.load
	    (
	            {
	                params:
	                        {
	                            Skip: 0,
	                            Take: 1000,
	                            Sort: 'spesialisasi',
	                            Sortdir: 'ASC',
	                            target: 'ViewSetupSpesial',
	                            param: 'kd_spesial <> 0'
	                        }
	            }
	    )
    },

    /*
		LOAD DATA STORE KELAS
    */
    loadDataKelas : function(params){
        var Field_Kelas = ['KD_KELAS', 'KELAS'];
        dataSource_Kelas_LapRWI         = new WebApp.DataStore({fields: Field_Kelas});
	    dataSource_Kelas_LapRWI.load
	    (
	        {
	            params:{
	                Skip: 0,
	                Take: 1000,
	                Sort: 'kelas',
	                Sortdir: 'ASC',
	                target: 'ViewComboKelasSpesial',
	                param: 'where kd_spesial =~' + params + '~',
	            }
	        }
	    )
    },

    init:function(){
        var $this=this;
        $this.loadDataSpesialisasi();
        $this.loadDataKelas(1);
        $this.Window.main=new Ext.Window({
            width: 460,
            height:550,
            modal:true,
            title:'Laporan Pasien Pulang',
            layout:'fit',
            items:[
                new Ext.Panel({
                    bodyStyle:'padding: 6px',
                    layout:'form',
                    border:false,
                    autoScroll: true,
                    fbar:[
                        new Ext.Button({
                            text:'Ok',
                            handler:function(){
                                $this.initPrint()
                            }
                        }),
                        new Ext.Button({
                            text:'Batal',
                            handler:function(){
                                $this.Window.main.close();
                            }
                        })
                    ],
                    items:[
                        {
                            layout:'column',
                            border:false,
                            width:460,
                            // height: 220,
                            bodyStyle:'padding: 2px 2px 2px 2px',
                            items:[
                                {
                                    xtype : 'fieldset',
                                    title : 'Unit',
                                    layout:'column',
                                    border:true,
                                    width:450,
                                    height: 150,
                                    bodyStyle:'padding: 2px 2px 2px 10px',
                                    items:[
                                        {
                                            border:false,
                                            columnWidth:.47,
                                            bodyStyle:'margin-top: 2px',
                                            items:[
                                                $this.firstGridPenerimaan(),
                                            ]
                                        },
                                        {
                                            border:false,
                                            columnWidth:.03,
                                            bodyStyle:'margin-top: 2px',
                                        },
                                        {
                                            border:false,
                                            columnWidth:.47,
                                            bodyStyle:'margin-top: 2px; align:right;',
                                            items:[
                                                $this.seconGridPenerimaan(),
                                            ]
                                        }
                                    ]
                                },{
                                    xtype : 'fieldset',
                                    title : 'Kamar',
                                    layout:'column',
                                    border:true,
                                    width:450,
                                    height: 230,
                                    bodyStyle:'padding: 2px 2px 2px 10px',
                                    items:[
                                        {
                                            border:false,
                                            columnWidth:.47,
                                            bodyStyle:'margin-top: 2px',
                                            items:[
                                                $this.firstGridPenerimaanDetailKamar(),
                                            ]
                                        },
                                        {
                                            border:false,
                                            columnWidth:.03,
                                            bodyStyle:'margin-top: 2px',
                                        },
                                        {
                                            border:false,
                                            columnWidth:.47,
                                            bodyStyle:'margin-top: 2px; align:right;',
                                            items:[
                                                $this.seconGridPenerimaanDetailKamar(),
                                            ]
                                        }
                                    ]
                                },{
                                    xtype : 'fieldset',
                                    title : 'Spesialisasi',
                                    layout: 'column',
                                    width : 450,
                                    height: 75,
                                    bodyStyle:'padding: 2px 2px 2px 10px',
                                    border: true,
                                    items:[
                                    	{
	                                        layout: 'column',
	                                        border: false,
                                   			bodyStyle:'padding: 2px 2px 2px 10px',
	                                        width:450,
	                                        items:[
	                                            $this.ComboBox.cboSpesialisasi=new Ext.form.ComboBox({
		                                            id: 'cboSpesialisasi_LapPasienPulang',
		                                            triggerAction: 'all',
		                                            lazyRender:true,
		                                            mode: 'local',
		                                            width: 400,
		                                            selectOnFocus:true,
		                                            forceSelection: true,
		                                            emptyText:'Silahkan Pilih...',
		                                            fieldLabel: 'Pendaftaran Per Shift ',
		                                            store: dataSource_Spesialisasi_LapRWI,
		                                            valueField: 'KD_SPESIAL',
		                                            displayField: 'SPESIALISASI',
		                                            value:'Semua',
		                                            listeners:{
		                                                'select': function(a,b,c){
		                                                	//console.log(b.data.KD_SPESIAL);
		                                                	$this.loadDataKelas(b.data.KD_SPESIAL)
		                                                }
		                                            }
                                               	}),
                                            ]
                                        }
                                    ]
                                },{
                                    xtype : 'fieldset',
                                    title : 'Kelas',
                                    layout: 'column',
                                    width : 450,
                                    height: 75,
                                    bodyStyle:'padding: 2px 2px 2px 10px',
                                    border: true,
                                    items:[
                                    	{
	                                        layout: 'column',
	                                        border: false,
                                   			bodyStyle:'padding: 2px 2px 2px 10px',
	                                        width:450,
	                                        items:[
	                                            $this.ComboBox.cboKelas=new Ext.form.ComboBox({
		                                            id: 'cboKelas',
		                                            triggerAction: 'all',
		                                            lazyRender:true,
		                                            mode: 'local',
		                                            width: 400,
		                                            selectOnFocus:true,
		                                            forceSelection: true,
		                                            emptyText:'Silahkan Pilih...',
		                                            fieldLabel: 'Pendaftaran Per Shift ',
		                                            store: dataSource_Kelas_LapRWI,
		                                            valueField: 'KD_KELAS',
		                                            displayField: 'KELAS',
		                                            value:'Semua',
		                                            listeners:{
		                                                'select': function(a,b,c){
		                                                }
		                                            }
                                               	}),
                                            ]
                                        }
                                    ]
                                },{
                                    xtype : 'fieldset',
                                    title : 'Tanggal Keluar',
                                    layout: 'column',
                                    width : 450,
                                    height: 75,
                                    bodyStyle:'padding: 2px 2px 2px 10px',
                                    border: true,
                                    items:[
                                        $this.DateField.startDate=new Ext.form.DateField({
		                                    id: 'textFormTanggal_Awal',
                                            value: new Date(),
                                            format:'d/M/Y',
                                            width 	: 150
                                        }),
                                        {
                                            xtype:'displayfield',
                                            width: 30,
                                            value:'&nbsp;s/d&nbsp;'
                                        },
                                        $this.DateField.endDate=new Ext.form.DateField({
		                                    id: 'textFormTanggal_Akhir',
                                            value 	: new Date(),
                                            format 	:'d/M/Y',
                                            width 	: 150
                                        })
                                    ]
                                },{
                                    xtype : 'fieldset',
                                    title : 'Customer',
                                    layout:'column',
                                    border:true,
                                    width:450,
                                    height: 230,
                                    bodyStyle:'padding: 2px 2px 2px 10px',
                                    items:[
                                        {
                                            border:false,
                                            columnWidth:.47,
                                            bodyStyle:'margin-top: 2px',
                                            items:[
                                                $this.firstGridPenerimaanCustomer(),
                                            ]
                                        },
                                        {
                                            border:false,
                                            columnWidth:.03,
                                            bodyStyle:'margin-top: 2px',
                                        },
                                        {
                                            border:false,
                                            columnWidth:.47,
                                            bodyStyle:'margin-top: 2px; align:right;',
                                            items:[
                                                $this.seconGridPenerimaanCustomer(),
                                            ]
                                        }
                                    ]
                                },{
                                    xtype : 'fieldset',
                                    title : 'Type File',
                                    layout: 'column',
                                    width : 450,
                                    height: 75,
                                    bodyStyle:'padding: 2px 2px 2px 10px',
                                    border: true,
                                    items:[
                                        {
                                            xtype: 'checkbox',
                                            id: 'CekLapPilihFile_LapPulangpasien',
                                            hideLabel:false,
                                            boxLabel: 'Excel',
                                            checked: false,
                                        }
                                    ]
                                },{
                                    xtype : 'fieldset',
                                    title : 'Order By',
                                    layout: 'column',
                                    width : 450,
                                    height: 75,
                                    bodyStyle:'padding: 2px 2px 2px 10px',
                                    border: true,
                                    items:[
                                        {
                                            layout: 'column',
                                            border: false,
                                            bodyStyle:'padding: 2px 2px 2px 10px',
                                            width:450,
                                            items:[
                                                $this.ComboBox.cboUrut=new Ext.form.ComboBox({
                                                    id: 'cboUrut_LapRWIPulang',
                                                    triggerAction: 'all',
                                                    lazyRender:true,
                                                    mode: 'local',
                                                    width: 400,
                                                    selectOnFocus:true,
                                                    forceSelection: true,
                                                    emptyText:'Silahkan Pilih...',
                                                    fieldLabel: 'Pendaftaran Per Shift ',
                                                    store: new Ext.data.ArrayStore
                                                    (
                                                        {
                                                            id: 0,
                                                            fields:
                                                            [
                                                                'id',
                                                                'displayText'
                                                            ],
                                                        data: [[1, 'No Medrec'],[2, 'Nama'],[3, 'Tanggal Masuk'],[4, 'Nama Kamar'],[5, 'Tanggal Keluar']]
                                                        }
                                                    ),
                                                    valueField: 'id',
                                                    displayField: 'displayText',
                                                    value: 1,
                                                }),
                                            ]
                                        }
                                    ]
                                },
                                
                            ]
                        },
                    ]
                })
            ]
        }).show();
    }
};
DlgLapRWJPenerimaan.init();
