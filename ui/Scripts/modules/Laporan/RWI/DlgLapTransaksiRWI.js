var polipilihanpasien;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRWITransaksi;
var selectNamaRWITransaksi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRWITransaksi;
var varLapRWITransaksi= ShowFormLapRWITransaksi();
var selectSetUmum;
var selectSetkelpas;
var winRWITransaksiReport;
var type_file=0;
function ShowFormLapRWITransaksi()
{
    frmDlgRWITransaksi= fnDlgRWITransaksi();
    //frmDlgRWITransaksi.show();
};

function fnDlgRWITransaksi()
{
     winRWITransaksiReport = new Ext.Window
    (
        {
            id: 'winRWITransaksiReport',
            title: 'Laporan ',
            closeAction: 'destroy',
            width:400,
            height: 290,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWITransaksi()],
            listeners:
			{
            activate: function()
				{
             
				}
			}

        }
    );

    winRWITransaksiReport.show();
	dataaddnew();
};
function dataaddnew()
{
				Ext.getCmp('cboPerseoranganRegisLab').hide();
                Ext.getCmp('cboAsuransiRegisLab').hide();
                Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
                Ext.getCmp('cboUmumRegisLab').show();
				Ext.getCmp('cboPilihanRWITransaksikelompokPasien').setValue('Semua');
				Ext.getCmp('cboUmumRegisLab').setValue('Semua');
				Ext.getCmp('cboPilihanRWITransaksi').setValue('Semua');
				
}

function ItemDlgRWITransaksi()
{
    var PnlLapRWITransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapRWITransaksi',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [	getItemLapRWITransaksi_tengah(),
                getItemLapRWITransaksi_Atas(),
                getItemLapRWITransaksi_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWITransaksi',
                            handler: function()
                            {
                               // if (ValidasiReportRWITransaksi() === 1)
                               // {
                                        //var tmppilihan = getKodeReportRWITransaksi();
                                        var criteria = GetCriteriaRWITransaksi();
                                        // winRWITransaksiReport.close();
                                       // loadlaporanRWI('0', 'rep030212', criteria);
									
									var params ={
												criteria:criteria,
												type_file:type_file
									} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/rawat_inap/function_lap_RWI/cetak_lap_transaksi");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
									loadMask.hide();
								// };
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWITransaksi',
                            handler: function()
                            {
                            winRWITransaksiReport.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWITransaksi;
};

function GetCriteriaRWITransaksi()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalFilterRWITransaksi').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalFilterRWITransaksi').getValue();
	}
	if (Ext.get('dtpTglAkhirFilterRWITransaksi').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterRWITransaksi').getValue();
	}

	if(Ext.getCmp('cboPilihanRWITransaksi').getValue() !== ''|| Ext.getCmp('cboPilihanRWITransaksi').getValue() === '')
	{
		if (Ext.get('cboPilihanRWITransaksi').getValue()=== ''){
			
			
		}  else {
		strKriteria += '##@@##' + polipilihanpasien
		} 
	}
       
	if (Ext.getCmp('cboPilihanRWITransaksikelompokPasien').getValue() !== '')
	{
		if (Ext.get('cboPilihanRWITransaksikelompokPasien').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
			 strKriteria += '##@@##' ;
        } 
		else if (Ext.get('cboPilihanRWITransaksikelompokPasien').getValue() === 'Perseorangan')
		{
			
			if(Ext.getCmp('cboperoranganRequestEntryRegisLab').getValue() === '')
            {
                strKriteria += '##@@##' + '0';
                strKriteria += '##@@##' + 'NULL';
				 strKriteria += '##@@##' ;
            }else{
                strKriteria += '##@@##' + '0';
                strKriteria += '##@@##' + selectsetperusahaan;
				 strKriteria += '##@@##';
            }
        } else if (Ext.get('cboPilihanRWITransaksikelompokPasien').getValue() === 'Perusahaan'){
            
			if(Ext.getCmp('cboPerusahaanRequestEntryRegisLab').getValue() === '')
            {
                strKriteria += '##@@##' + '1';
                strKriteria += '##@@##' + 'NULL';
				 strKriteria += '##@@##' ;
            }else{
                strKriteria += '##@@##' + '1';
                strKriteria += '##@@##' + selectsetperusahaan;
				strKriteria += '##@@##' ;
            }
        } else {
            
			if(Ext.getCmp('cboAsuransiRegisLab').getValue() === '')
            {
                strKriteria += '##@@##' + '2';
                strKriteria += '##@@##' + 'NULL';
				 strKriteria += '##@@##' ;
            }else{
                strKriteria += '##@@##' + '2';
                strKriteria += '##@@##' + selectSetAsuransi;
				strKriteria += '##@@##' ;
            }
            
        } 
	}   
	if (Ext.getCmp('Shift_All_RWITransaksi').getValue() === true)
	{
		
		strKriteria +=  '1';
		strKriteria += ',' + '2';
		strKriteria += ',' + '3,';
		strKriteria += '##@@##';
		strKriteria += '4';
	}else{
		if (Ext.getCmp('Shift_1_RWITransaksi').getValue() === true)
		{
		
			strKriteria += '1,';
		}
		if (Ext.getCmp('Shift_2_RWITransaksi').getValue() === true)
		{
			
			strKriteria += '2,';
		}
		if (Ext.getCmp('Shift_3_RWITransaksi').getValue() === true)
		{
			
			strKriteria += '3,';
			strKriteria += '##@@##';
			strKriteria += '4';
		}
	}
	if (Ext.getCmp('PendaftaranRWITransaksi').getValue() === true)
	{
	strKriteria += '##@@##'+'ya';
	} 
	if (Ext.getCmp('PendaftaranRWITransaksi').getValue() === false)
	{
	strKriteria += '##@@##'+'tidak';
	} 
	if (Ext.getCmp('TindakanRWITransaksi').getValue() === true)
	{
	strKriteria += '##@@##'+'ya';
	}
	if (Ext.getCmp('TindakanRWITransaksi').getValue() === false)
	{
	strKriteria += '##@@##'+'tidak';
	}
	return strKriteria;
};

function getKodeReportRWITransaksi()
{   var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanRWITransaksi').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanRWITransaksi').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportRWITransaksi()
{
    var x=1;
	
    if(Ext.get('dtpTglAwalFilterRWITransaksi').dom.value > Ext.get('dtpTglAkhirFilterRWITransaksi').dom.value)
    {
        ShowPesanWarningRWITransaksiReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }
	if(Ext.getCmp('cboPilihanRWITransaksikelompokPasien').getValue() === ''){
		ShowPesanWarningRWITransaksiReport('Kelompok Pasien Belum Dipilih','Laporan Transaksi');
        x=0;
	}

	if(Ext.getCmp('Shift_All_RWITransaksi').getValue() === false && Ext.getCmp('Shift_1_RWITransaksi').getValue() === false && Ext.getCmp('Shift_2_RWITransaksi').getValue() === false && Ext.getCmp('Shift_3_RWITransaksi').getValue() === false){
		ShowPesanWarningRWITransaksiReport(nmRequesterRequest,'Laporan Transaksi');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningRWITransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapRWITransaksi_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 130,
            anchor: '100% 100%',
            items: [
        /*     {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Poli '
            },  */
                mComboPilihanRWITransaksi(),
            {
                x: 10,
                y:  10,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y:  10,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y:  10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterRWITransaksi',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y:  10,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y:  10,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterRWITransaksi',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanRWITransaksiKelompokPasien(),
                mComboPerseoranganRWITransaksi(),
                mComboAsuransiRWITransaksi(),
                mComboPerusahaanRWITransaksi(),
                mComboUmumRWITransaksi(),
				mComboUmumLabRegis(),
			{
                x: 10,
                y: 100,
                xtype: 'label',
                text: 'Tipe File '
            }, {
                x: 110,
                y: 100,
                xtype: 'label',
                text: ' : '
            },{
				x: 120,
				y: 100,
				xtype: 'checkbox',
				id: 'CekLapPilihFile',
				hideLabel:false,
				boxLabel: 'Excel',
				checked: false,
				listeners: 
				{
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihFile').getValue()===true)
						{
							type_file =1;
						}
						else
						{
							type_file =0;
						   
						}
					}
				}
			}
            ]
        }]
    };
    return items;
};

function getItemLapRWITransaksi_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {boxLabel: 'Semua',name: 'Shift_All_RWITransaksi',id : 'Shift_All_RWITransaksi',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_RWITransaksi').setValue(true);Ext.getCmp('Shift_2_RWITransaksi').setValue(true);Ext.getCmp('Shift_3_RWITransaksi').setValue(true);Ext.getCmp('Shift_1_RWITransaksi').disable();Ext.getCmp('Shift_2_RWITransaksi').disable();Ext.getCmp('Shift_3_RWITransaksi').disable();}else{Ext.getCmp('Shift_1_RWITransaksi').setValue(false);Ext.getCmp('Shift_2_RWITransaksi').setValue(false);Ext.getCmp('Shift_3_RWITransaksi').setValue(false);Ext.getCmp('Shift_1_RWITransaksi').enable();Ext.getCmp('Shift_2_RWITransaksi').enable();Ext.getCmp('Shift_3_RWITransaksi').enable();}}},
                                    {boxLabel: 'Shift 1',name: 'Shift_1_RWITransaksi',id : 'Shift_1_RWITransaksi'},
                                    {boxLabel: 'Shift 2',name: 'Shift_2_RWITransaksi',id : 'Shift_2_RWITransaksi'},
                                    {boxLabel: 'Shift 3',name: 'Shift_3_RWITransaksi',id : 'Shift_3_RWITransaksi'}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};


function getItemLapRWITransaksi_tengah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: '',
                    autoHeight: true,
                    width: '373px',
					bodyStyle: 'padding: 3px 0px 0px 0px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
									{boxLabel: 'Pendaftaran',name: 'PendaftaranRWITransaksi',id : 'PendaftaranRWITransaksi'},
                                    {boxLabel: 'Tindakan RWI',name: 'TindakanRWITransaksi',id : 'TindakanRWITransaksi'}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};


var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;

function mComboPilihanRWITransaksi()
{

   var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ComboUnit_transakasi',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )
    var cboPilihanRWITransaksi = new Ext.form.ComboBox
	(
             {
			  x: 120,
                y: 10,
            id: 'cboPilihanRWITransaksi',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poli',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
             width:240,
			tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
				{
					
                     
						polipilihanpasien=b.data.KD_UNIT;
				},
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    {
										Ext.getCmp('cboDokterRequestEntry').focus();
										polipilihanpasien= c.value;
									}else if(e.getKey()==9)
									{
										polipilihanpasien= c.value;	
										   
									}
                                    }, c);
                                }


		}
        }
	);
	
	cboPilihanRWITransaksi.hide();
	return cboPilihanRWITransaksi;
};





function mComboPilihanRWITransaksiKelompokPasien()
{
    var cboPilihanRWITransaksikelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 40,
                id:'cboPilihanRWITransaksikelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPilihankelompokPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanRWITransaksikelompokPasien;
};

function mComboPerseoranganRWITransaksi()
{
    var cboPerseoranganRegisLab = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPerseoranganRegisLab',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRegisLab;
};

function mComboUmumRWITransaksi()
{
  var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by customer asc'
			}
		}
	);
    var cboperoranganRequestEntryRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
		    id: 'cboperoranganRequestEntryRegisLab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Customer...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboperoranganRequestEntryRegisLab;

};

function mComboPerusahaanRWITransaksi()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 order by customer asc'
			}
		}
	);
    var cboPerusahaanRequestEntryRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
		    id: 'cboPerusahaanRequestEntryRegisLab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRegisLab;
};

function mComboAsuransiRWITransaksi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 order by customer asc"
            }
        }
    );
    var cboAsuransiRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboAsuransiRegisLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiRegisLab;
};
function mComboUmumLabRegis()
{
    var cboUmumRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboUmumRegisLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetUmum,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumRegisLab;
};
function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboperoranganRequestEntryRegisLab').show();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboperoranganRequestEntryRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').show();
        Ext.getCmp('cboUmumRegisLab').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboperoranganRequestEntryRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').show();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').show();
   }
}

function mCombounitRWITransaksi()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryRWITransaksi = new Ext.form.ComboBox
    (
        {
            id: 'cbounitRequestEntryRWITransaksi',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                }
                    
                }
        }
    )

    return cbounitRequestEntryRWITransaksi;
};
