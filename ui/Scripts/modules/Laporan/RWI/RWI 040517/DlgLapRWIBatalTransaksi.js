
var dsRWIBatalTransaksi;
var selectRWIBatalTransaksi;
var selectNamaRWIBatalTransaksi;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWIBatalTransaksi;
var varLapRWIBatalTransaksi= ShowFormLapRWIBatalTransaksi();
var dsRWJ;
function ShowFormLapRWIBatalTransaksi()
{
    frmDlgRWIBatalTransaksi= fnDlgRWIBatalTransaksi();
    frmDlgRWIBatalTransaksi.show();
};

function fnDlgRWIBatalTransaksi()
{
    var winRWIBatalTransaksiReport = new Ext.Window
    (
        {
            id: 'winRWIBatalTransaksiReport',
            title: 'Laporan Pembatalan Transaksi',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWIBatalTransaksi()]

        }
    );

    return winRWIBatalTransaksiReport;
};

function ItemDlgRWIBatalTransaksi()
{
    var PnlLapRWIBatalTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapRWIBatalTransaksi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWIBatalTransaksi_Periode(),
				
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWIBatalTransaksi',
					handler: function()
					{
					   if (ValidasiReportRWIBatalTransaksi() === 1)
					   {
							/*if (Ext.getCmp('Shift_All_LapRWIBatalTransaksi').getValue() === true){
								shift='All';
								shift1='false';
								shift2='false';
								shift3='false';
							}else{
								shift='';
								if (Ext.getCmp('Shift_1_LapRWIBatalTransaksi').getValue() === true){
									shift1='true';
								} else{
									shift1='false';
								}
								if (Ext.getCmp('Shift_2_LapRWIBatalTransaksi').getValue() === true){
									shift2='true';
								}else{
									shift2='false';
								}
								if (Ext.getCmp('Shift_3_LapRWIBatalTransaksi').getValue() === true){
									shift3='true';
								}else{
									shift3='false';
								}
							}*/
							
							/*var params={
								tglAwal:Ext.getCmp('dtpTglAwalLapRWIBatalTransaksi').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAwalLapRWIBatalTransaksi').getValue(),
								
							} */
							
							var params=Ext.get('dtpTglAwalLapRWIBatalTransaksi').getValue()+'#aje#'
								+Ext.get('dtpTglAwalLapRWIBatalTransaksi').getValue()+'#aje#Laporan Batal Transaksi Rawat Inap#aje#05';
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionRWJ/cetakRWJBatalTransaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWIBatalTransaksi.close(); 
							
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWIBatalTransaksi',
					handler: function()
					{
						frmDlgRWIBatalTransaksi.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWIBatalTransaksi;
};


function ValidasiReportRWIBatalTransaksi()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWIBatalTransaksi').dom.value === '')
	{
		ShowPesanWarningRWIBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}

    return x;
};

function ValidasiTanggalReportRWIBatalTransaksi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWIBatalTransaksi').dom.value > Ext.get('dtpTglAkhirLapRWIBatalTransaksi').dom.value)
    {
        ShowPesanWarningRWIBatalTransaksiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWIBatalTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWIBatalTransaksi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWIBatalTransaksi_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWIBatalTransaksi',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWIBatalTransaksi',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};

function getItemRWIBatalTransaksi_Shift()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width:  395,
				height: 40,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
					{
						x: 0,
						y: 10,
						xtype:'label',
						text:'Shift :'
					},
					{
						x: 75,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapRWIBatalTransaksi',
						id : 'Shift_All_LapRWIBatalTransaksi',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapRWIBatalTransaksi').setValue(true);
								Ext.getCmp('Shift_2_LapRWIBatalTransaksi').setValue(true);
								Ext.getCmp('Shift_3_LapRWIBatalTransaksi').setValue(true);
								Ext.getCmp('Shift_1_LapRWIBatalTransaksi').disable();
								Ext.getCmp('Shift_2_LapRWIBatalTransaksi').disable();
								Ext.getCmp('Shift_3_LapRWIBatalTransaksi').disable();
							}else{
								Ext.getCmp('Shift_1_LapRWIBatalTransaksi').setValue(false);
								Ext.getCmp('Shift_2_LapRWIBatalTransaksi').setValue(false);
								Ext.getCmp('Shift_3_LapRWIBatalTransaksi').setValue(false);
								Ext.getCmp('Shift_1_LapRWIBatalTransaksi').enable();
								Ext.getCmp('Shift_2_LapRWIBatalTransaksi').enable();
								Ext.getCmp('Shift_3_LapRWIBatalTransaksi').enable();
							}
						}
					},
					{
						x: 145,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapRWIBatalTransaksi',
						id : 'Shift_1_LapRWIBatalTransaksi'
					},
					{
						x: 215,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapRWIBatalTransaksi',
						id : 'Shift_2_LapRWIBatalTransaksi'
					},
					{
						x: 285,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapRWIBatalTransaksi',
						id : 'Shift_3_LapRWIBatalTransaksi'
					}
				]
			}
     
        ]
    }
    return items;
};
