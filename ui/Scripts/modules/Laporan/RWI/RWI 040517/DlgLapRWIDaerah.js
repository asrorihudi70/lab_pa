
var dsRWIDaerah;
var selectRWIDaerah;
var selectNamaRWIDaerah;
var now = new Date();
var selectSetDaerah;
var frmDlgRWIDaerah;
var varLapRWIDaerah= ShowFormLapRWIDaerah();

function ShowFormLapRWIDaerah()
{
    frmDlgRWIDaerah= fnDlgRWIDaerah();
    frmDlgRWIDaerah.show();
};

function fnDlgRWIDaerah()
{
    var winRWIDaerahReport = new Ext.Window
    (
        {
            id: 'winRWIDaerahReport',
            title: 'Laporan Rawat Inap Pasien Per Daerah',
            closeAction: 'destroy',
            width:500,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWIDaerah()]

        }
    );

    return winRWIDaerahReport;
};


function ItemDlgRWIDaerah()
{
    var PnlLapRWIDaerah = new Ext.Panel
    (
        {
            id: 'PnlLapRWIDaerah',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWIDaerah_Tanggal(),getItemLapRWIDaerah_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWIDaerah',
                            handler: function(){
                                if (ValidasiReportRWIDaerah() === 1){
                                    var criteria = GetCriteriaRWIDaerah();
                                    loadMask.show();
                                    loadlaporanRWI('0', 'rep030205', criteria,function(){
                                    	frmDlgRWIDaerah.close();
                                    	loadMask.hide();
                                    });
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWIDaerah',
                            handler: function()
                            {
                                    frmDlgRWIDaerah.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWIDaerah;
};

function GetCriteriaRWIDaerah()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWIDaerah').dom.value !== '')
	{
		strKriteria = getnewformatdate(Ext.get('dtpTglAwalLapRWIDaerah').dom.value);
	};
	if (selectSetDaerah !== undefined)
	{
		strKriteria += '##@@##' + selectSetDaerah;
	};

	return strKriteria;
};


function ValidasiReportRWIDaerah()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWIDaerah').dom.value === '') || (Ext.get('cboDaerah').dom.value === '' || 
            Ext.get('cboDaerah').dom.value === 'Silahkan Pilih...'))
    {
            if(Ext.get('dtpTglAwalLapRWIDaerah').dom.value === '')
            {
                    ShowPesanWarningRWIDaerahReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('cboDaerah').dom.value === '' || Ext.get('cboDaerah').dom.value === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningRWIDaerahReport(nmGetValidasiKosong('Wilayah'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRWIDaerah()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWIDaerah').dom.value > Ext.get('dtpTglAkhirLapRWIDaerah').dom.value)
    {
        ShowPesanWarningRWIDaerahReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWIDaerahReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWIDaerah_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                    mComboUnitLapRWIDaerah()
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWIDaerah_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: 0.45,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 85,
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: 'Tanggal ',
                    id: 'dtpTglAwalLapRWIDaerah',
                    format: 'M-Y',
                    value:now,
                    width:80
                }
            ]
        }
        ]
    }
    return items;
};


function mComboUnitLapRWIDaerah()
{
     var cboDaerah = new Ext.form.ComboBox
	(
		{
			id:'cboDaerah',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Wilayah ',
			width:80,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Prop'],[2, 'Kab'],[3, 'Kec'],[4, 'Kel']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetDaerah=b.data.displayText ;
				}
			}
		}
	);
	return cboDaerah;
};