
var dsRWIPulang;
var selectNamaRWIPulang;
var now = new Date();
var frmDlgRWIPulang;
var varLapRWIPulang= ShowFormLapRWIPulang();
var tmprbRWI;
var tmprbDateRWI;
var selectSetPilihankelompokPasien;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;
var selectSetPerseorangan;
var selectSetUmum;
var selectSetUrutPulang;
var selectSetUnitRuangSpesial;
var selectSetUnitRuangSpesial2;
var selectSetKamarSpesial;
var selectSetKamarSpesial2;

function ShowFormLapRWIPulang()
{
    frmDlgRWIPulang= fnDlgRWIPulang();
    frmDlgRWIPulang.show();
};

function fnDlgRWIPulang()
{
    var winRWIPulangReport = new Ext.Window
    (
        {
            id: 'winRWIPulangReport',
            title: 'Laporan Rawat Inap Pasien Pulang',
            closeAction: 'destroy',
            width:500,
            height: 270,
            border: false,
            resizable:false,
            plain: true,
			constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWIPulang()],
            listeners:
            {
                activate: function()
                {
                     /* Ext.getCmp('dtpBulanAwalLapRWIPulang').disable();
                     Ext.getCmp('dtpBulanAkhirLapRWIPulang').disable();
                     tmprbRWI = " and k.tgl_KELUAR IS NOT NULL ";
                     tmprbDateRWI = 'Tanggal'; */
                }
            }

        }
    );

    return winRWIPulangReport;
};


function ItemDlgRWIPulang()
{
    var PnlLapRWIPulang = new Ext.Panel
    (
        {
            id: 'PnlLapRWIPulang',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWIPulang_Tanggal(),
                {
                    layout: 'absolute',
                    border: false,
                    width: 450,
                    height: 30,
                    items:
                    [
                        {
                            x: 285,
                            y: 1,
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWIPulang',
                            handler: function(){
								if (ValidasiReportRWIPulang() === 1){
                                    var criteria = GetCriteriaRWIPulang();
                                    loadMask.show();
                                    loadlaporanRWI('0', 'rep030208', criteria,function(){
                                    	frmDlgRWIPulang.close();
                                    	loadMask.hide();
                                    });
                                }
                            }
                        },
                        {
                            x: 360,
                            y: 1,
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWIPulang',
                            handler: function()
                            {
                                    frmDlgRWIPulang.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWIPulang;
};

function getItemLapRWIPulang_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                columnWidth:.98,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 500,
                height: 180,
                anchor: '100% 100%',
                items:
				[
					{
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Dari Tanggal'
                    },
					{
                        x: 100,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
					{
                        x: 110,
                        y: 10,
                        xtype: 'datefield',
                        id: 'dtpTglAwalLapRWIPulang',
                        format: 'd/M/Y',
                        value:now
                    },
					{
                        x: 220,
                        y: 10,
                        xtype: 'label',
                        text: 's/d'
                    },
					{
                        x: 245,
                        y: 10,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirLapRWIPulang',
                        format: 'd/M/Y',
                        value:now
                    },
					//----------------------------------------
					{
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Urut Per'
                    },
					{
                        x: 100,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
					mComboUrutLapRWIPulang(),
					//----------------------------------------
					{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Unit'
                    },
					{
                        x: 100,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },
					mcomboNamaUnitSpesial(),
					mcomboKamarSpesial(),
					//----------------------------------------
					{
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kelompok Pasien'
                    },
					{
                        x: 100,
                        y: 100,
                        xtype: 'label',
                        text: ':'
                    },
					mComboPilihanKelompokPasien(),
					mComboPerseorangan(),
					mComboAsuransi(),
					mComboPerusahaan(),
					mComboUmum()
				]
			}
        ]
    }
    return items;
};

function mComboUrutLapRWIPulang()
{
     var cboUrut = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 40,
			id:'cboUrut',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			width:153,
			value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'No Medrec'],[2, 'Nama'],[3, 'Tanggal Masuk'],[4, 'Nama Kamar']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUrutPulang=b.data.displayField ;
				}
			}
		}
	);
	return cboUrut;
};

function mcomboNamaUnitSpesial()
{
    var Field = ['kode','namaunit'];
    ds_UnitRuangSpesial_LapPulang = new WebApp.DataStore({fields: Field});

	ds_UnitRuangSpesial_LapPulang.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'kode',
                Sortdir: 'ASC',
                target:'ViewSetupUnitKamarSpesial',
                param: "1"
            }
        }
    )

    var cboUnitRuangSpesial = new Ext.form.ComboBox
    (
        {
			x: 110,
			y: 70,
            id: 'cboUnitRuangSpesial',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            store: ds_UnitRuangSpesial_LapPulang,
            valueField: 'kode',
            displayField: 'namaunit',
            anchor:'60%',
			value:'Semua',
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetUnitRuangSpesial=b.data.valueField ;
					selectSetUnitRuangSpesial=b.data.displayField ;
				    loadds_KamarSpesial_viDaftar(b.data.kode);					
				},
				'render': function(c)
				{
					
				}


			}
        }
    )

    return cboUnitRuangSpesial;
};

function loadds_KamarSpesial_viDaftar(kode){
	ds_KamarSpesial_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'No_Kamar',
                Sortdir: 'ASC',
                target:'ViewSetupUnitKamarSpesial',
                param: kode
            }
        }
    )
};

function mcomboKamarSpesial()
{
    var Field = ['no_kamar','nama_kamar'];
    ds_KamarSpesial_viDaftar = new WebApp.DataStore({fields: Field});

    var cboRujukanKamarSpesialRequestEntry = new Ext.form.ComboBox
    (
        {
			x: 270,
			y: 70,
            id: 'cboRujukanKamarSpesialRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            align: 'Right',
            store: ds_KamarSpesial_viDaftar,
            valueField: 'no_kamar',
            displayField: 'nama_kamar',
			value:'SEMUA',
            anchor: '99%',
            listeners:
			{
					'select': function(a, b, c)
					{
						selectSetKamarSpesial=b.data.valueField ;
						selectSetKamarSpesial2=b.data.displayField ;						
					},
					'render': function(c)
					{
						//selectSetKamarSpesial=c.data.valueField;
					}


			}
        }
    )

    return cboRujukanKamarSpesialRequestEntry;
};

function mComboPilihanKelompokPasien()
{
    var cboPilihankelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 110,
                y: 100,
                id:'cboPilihankelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihankelompokPasien;
};

function mComboPerseorangan()
{
    var cboPerseorangan = new Ext.form.ComboBox
	(
            {
                x: 110,
                y: 130,
                id:'cboPerseorangan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseorangan;
};

function mComboUmum()
{
    var cboUmum = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 130,
			id:'cboUmum',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmum;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1'
			}
		}
	);
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 130,
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: 'Semua',
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntry;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2"
            }
        }
    );
    var cboAsuransi = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 130,
			id:'cboAsuransi',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: 'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransi;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseorangan').show();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').show();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').show();
   }
   else
   {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').show();
   }
};

function GetCriteriaRWIPulang()
{
	var strKriteria = '';

	if (Ext.getCmp('dtpTglAwalLapRWIPulang').getValue() !== ''){
		strKriteria = 'Tanggal';
		strKriteria += '##@@##' + Ext.get('dtpTglAwalLapRWIPulang').getValue();
	}
	if (Ext.getCmp('dtpTglAkhirLapRWIPulang').getValue() !== ''){
		
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRWIPulang').getValue();
	}
	if (Ext.getCmp('cboUrut').getValue() !== ''){
		
		strKriteria += '##@@##' + 'urut';
		strKriteria += '##@@##' + Ext.get('cboUrut').getValue();
	}
	if (Ext.getCmp('cboUnitRuangSpesial').getValue() !== ''){
		
		strKriteria += '##@@##' + 'unit/ruang';
		strKriteria += '##@@##' + Ext.get('cboUnitRuangSpesial').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitRuangSpesial').getValue();
	}
	if (Ext.getCmp('cboRujukanKamarSpesialRequestEntry').getValue() !== ''){
		
		strKriteria += '##@@##' + 'kamar';
		strKriteria += '##@@##' + Ext.get('cboRujukanKamarSpesialRequestEntry').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboRujukanKamarSpesialRequestEntry').getValue();
	}
	if (Ext.getCmp('cboPilihankelompokPasien').getValue() !== '')
	{
		if (Ext.get('cboPilihankelompokPasien').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
			strKriteria += '##@@##' + 'NULL';
			
        } else if (Ext.get('cboPilihankelompokPasien').getValue() === 'Perseorangan'){
			
			var selectSetPerseorangan2='';
            selectSetPerseorangan2 = '0000000001';
			strKriteria += '##@@##' + 'Perseorangan';
            strKriteria += '##@@##' + 'Umum';
            strKriteria += '##@@##' + selectSetPerseorangan2;
			
        } else if (Ext.get('cboPilihankelompokPasien').getValue() === 'Perusahaan'){
            
			if(Ext.getCmp('cboPerusahaanRequestEntry').getValue() === '')
            {
                strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Perusahaan';
				strKriteria += '##@@##' + selectsetnamaperusahaan;
                strKriteria += '##@@##' + selectsetperusahaan;
            }
        } else {
            
			if(Ext.getCmp('cboAsuransi').getValue() === '')
            {
                strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Asuransi';
				strKriteria += '##@@##' + selectsetnamaAsuransi;
                strKriteria += '##@@##' + selectSetAsuransi;
            }
            
        }
	}
		
	

	return strKriteria;
};


function ValidasiReportRWIPulang()
{
    var x=1;

	/* if(Ext.getCmp('dtpTglAwalLapRWIPulang').getValue() <= Ext.getCmp('dtpTglAkhirLapRWIPulang').getValue()){
		
		ShowPesanWarningRWIPulangReport('Tanggal akhir tidak boleh lebih dari tanggal awal',nmTitleFormDlgReqCMRpt);
        x=0;
	}; */
	if(Ext.getCmp('dtpTglAwalLapRWIPulang').getValue() === '')
	{
		ShowPesanWarningRWIPulangReport('Tanggal awal',nmTitleFormDlgReqCMRpt);
		x=0;
	};
	if(Ext.getCmp('dtpTglAkhirLapRWIPulang').getValue() === '')
	{
		ShowPesanWarningRWIPulangReport('Tanggal akhir',nmTitleFormDlgReqCMRpt);
		x=0;
	};
	if(Ext.getCmp('cboUrut').getValue() === ''){
		
		ShowPesanWarningRWIPulangReport('Pilihan urut berdarakan belum di isi',nmTitleFormDlgReqCMRpt);
		x=0;
	}
	if(Ext.getCmp('cboUnitRuangSpesial').getValue() === ''){
		
		ShowPesanWarningRWIPulangReport('Unit belum dipilih',nmTitleFormDlgReqCMRpt);
		x=0;
	}
	if(Ext.getCmp('cboRujukanKamarSpesialRequestEntry').getValue() === ''){
		
		ShowPesanWarningRWIPulangReport('Kamar belum dipilih',nmTitleFormDlgReqCMRpt);
		x=0;
	}
	if(Ext.getCmp('cboPilihankelompokPasien').getValue() === ''){
		
		ShowPesanWarningRWIPulangReport('Kelompok pasien belum dipilih',nmTitleFormDlgReqCMRpt);
		x=0;
	}
	if(Ext.getCmp('cboPerseorangan').getValue() === '' &&  Ext.getCmp('cboPerusahaanRequestEntry').getValue() === '' &&  Ext.getCmp('cboAsuransi').getValue() === '' &&  Ext.getCmp('cboUmum').getValue() === ''){
		ShowPesanWarningRWIPulangReport('Sub kelompok pasien belum dipilih',nmTitleFormDlgReqCMRpt);
        x=0;
	}
	

    return x;
};

function ValidasiTanggalReportRWIPulang()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWIPulang').dom.value > Ext.get('dtpTglAkhirLapRWIPulang').dom.value)
    {
        ShowPesanWarningRWIPulangReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWIPulangReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

