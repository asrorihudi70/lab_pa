
var dsRWIKamar;
var selectRWIKamar;
var selectNamaRWIKamar;
var now = new Date();
var selectSetKamar;
var frmDlgRWIKamar;
var varLapRWIKamar= ShowFormLapRWIKamar();

function ShowFormLapRWIKamar()
{
    frmDlgRWIKamar= fnDlgRWIKamar();
    frmDlgRWIKamar.show();
};

function fnDlgRWIKamar()
{
    var winRWIKamarReport = new Ext.Window
    (
        {
            id: 'winRWIKamarReport',
            title: 'Laporan Rawat Inap Pasien Per Kamar',
            closeAction: 'destroy',
            width:400,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWIKamar()]

        }
    );

    return winRWIKamarReport;
};


function ItemDlgRWIKamar()
{
    var PnlLapRWIKamar = new Ext.Panel
    (
        {
            id: 'PnlLapRWIKamar',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWIKamar_Dept(),getItemLapRWIKamar_Tanggal(),
                {
                    layout: 'absolute',
                    border: false,
                    width: 800,
                    height: 30,
                    items:
                    [
                        {
                            x: 175,
                            y: 1,
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWIKamar',
                            handler: function(){
                                if (ValidasiReportRWIKamar() === 1){
                                    var criteria = GetCriteriaRWIKamar();
                                    loadMask.show();
                                    loadlaporanRWI('0', 'rep030204', criteria,function(){
                                    	frmDlgRWIKamar.close();
                                    	loadMask.hide();
                                    });
                                }
                            }
                        },
                        {
                            x: 250,
                            y: 1,
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWIKamar',
                            handler: function()
                            {
                                    frmDlgRWIKamar.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWIKamar;
};

function GetCriteriaRWIKamar()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWIKamar').dom.value !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWIKamar').dom.value;
	};
        if (Ext.get('dtpTglAkhirLapRWIKamar').dom.value !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRWIKamar').dom.value;
	};
	if (Ext.getCmp('cboRujukanKamarSpesialRequestEntry').getValue() !== '' || Ext.getCmp('cboRujukanKamarSpesialRequestEntry').getValue() !== 'Pilih Kamar...')
	{
		strKriteria += '##@@##' + 'Kamar';
                strKriteria += '##@@##' + Ext.getCmp('cboRujukanKamarSpesialRequestEntry').getValue();
	};

	return strKriteria;
};


function ValidasiReportRWIKamar()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWIKamar').dom.value === '') || (Ext.get('dtpTglAkhirLapRWIKamar').dom.value === '') || 
        (Ext.get('cboRujukanKamarSpesialRequestEntry').value === 'Silahkan Pilih...' || Ext.get('cboRujukanKamarSpesialRequestEntry').value === ' ' ))
    {
            if(Ext.get('dtpTglAwalLapRWIKamar').dom.value === '')
            {
                    ShowPesanWarningRWIKamarReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            if(Ext.get('dtpTglAkhirLapRWIKamar').dom.value === '')
            {
                    ShowPesanWarningRWIKamarReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.getCmp('cboRujukanKamarSpesialRequestEntry').getValue() === '' || Ext.getCmp('cboRujukanKamarSpesialRequestEntry').getValue() === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningRWIKamarReport(nmGetValidasiKosong('Kamar'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRWIKamar()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWIKamar').dom.value > Ext.get('dtpTglAkhirLapRWIKamar').dom.value)
    {
        ShowPesanWarningRWIKamarReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWIKamarReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWIKamar_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.98,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 800,
                height: 30,
                anchor: '100% 100%',
                items:
                [
                    {
                        x: 45,
                        y: 1,
                        xtype: 'label',
                        text: 'Kamar :'
                    },
                    mcomboKamarSpesial()
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWIKamar_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                columnWidth: .99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 800,
                height: 30,
                anchor: '100% 100%',
                items:
                [
                    {
                        x: 45,
                        y: 1,
                        xtype: 'label',
                        text: 'Tanggal :'
                    },
                    {
                        x: 95,
                        y: 1,
                        xtype: 'datefield',
                        id: 'dtpTglAwalLapRWIKamar',
                        format: 'd-M-Y',
                        value:now
                    },
                    {
                        x: 200,
                        y: 1,
                        xtype: 'label',
                        text: 's/d'
                    },
                    {
                        x: 220,
                        y: 1,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirLapRWIKamar',
                        format: 'd-M-Y',
                        value:now
                    }
                ]
            }
        ]
    }
    return items;
};


function mComboUnitLapRWIKamar()
{
     var cboKamar = new Ext.form.ComboBox
	(
		{
			id:'cboKamar',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Wilayah ',
			width:80,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Prop'],[2, 'Kab'],[3, 'Kec'],[4, 'Kel']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetKamar,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetKamar=b.data.displayText ;
				}
			}
		}
	);
	return cboKamar;
};

function mcomboKamarSpesial()
{
    var Field = ['no_kamar','kamar'];
    ds_KamarSpesial_viDaftar = new WebApp.DataStore({fields: Field});

	ds_KamarSpesial_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewSetupKelasSpesial',
                param: ""
            }
        }
    )

    var cboRujukanKamarSpesialRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanKamarSpesialRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Kamar...',
            fieldLabel: 'Kamar ',
            align: 'Right',
            store: ds_KamarSpesial_viDaftar,
            valueField: 'no_kamar',
            displayField: 'kamar',
            anchor: '95%',
            Width:'800',
            value: 'Semua',
            x: 95,
            y: 1,
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                },
                    'render': function(c)
                                {
                                    
                                }


		}
        }
    )

    return cboRujukanKamarSpesialRequestEntry;
}