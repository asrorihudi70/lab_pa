
var dsRWIPasien;
var selectNamaRWIPasien;
var now = new Date();
var frmDlgRWIPasien;
var varLapRWIPasien= ShowFormLapRWIPasien();
var tmprbRWI;
var tmprbDateRWI;
var selectSetPilihankelompokPasien;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;
var selectSetPerseorangan;
var selectSetUmum;
var selectSetUrutPasien;
var selectSetUnitRuangSpesial;
var selectSetKamarSpesial;

function ShowFormLapRWIPasien()
{
    frmDlgRWIPasien= fnDlgRWIPasien();
    frmDlgRWIPasien.show();
};

function fnDlgRWIPasien()
{
    var winRWIPasienReport = new Ext.Window
    (
        {
            id: 'winRWIPasienReport',
            title: 'Laporan Rawat Inap Pasien Pasien',
            closeAction: 'destroy',
            width:500,
            height: 230,
            border: false,
            resizable:false,
            plain: true,
			constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWIPasien()],
            listeners:
            {
                activate: function()
                {
                     /* Ext.getCmp('dtpBulanAwalLapRWIPasien').disable();
                     Ext.getCmp('dtpBulanAkhirLapRWIPasien').disable();
                     tmprbRWI = " and k.tgl_KELUAR IS NOT NULL ";
                     tmprbDateRWI = 'Tanggal'; */
                }
            }

        }
    );

    return winRWIPasienReport;
};


function ItemDlgRWIPasien()
{
    var PnlLapRWIPasien = new Ext.Panel
    (
        {
            id: 'PnlLapRWIPasien',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWIPasien_Tanggal(),
                {
                    layout: 'absolute',
                    border: false,
                    width: 450,
                    height: 30,
                    items:
                    [
                        {
                            x: 285,
                            y: 1,
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWIPasien',
                            handler: function()
                            {
								if (ValidasiReportRWIPasien() === 1)
								{
                                    var criteria = GetCriteriaRWIPasien();
                                    loadMask.show();
                                    loadlaporanRWI('0', 'rep030209', criteria,function(){
                                    	frmDlgRWIPasien.close();
                                    	loadMask.hide();
                                    });
                                };
                            }
                        },
                        {
                            x: 360,
                            y: 1,
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWIPasien',
                            handler: function()
                            {
                                    frmDlgRWIPasien.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWIPasien;
};

function getItemLapRWIPasien_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                columnWidth:.98,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 500,
                height: 145,
                anchor: '100% 100%',
                items:
				[
					{
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Urut Per'
                    },
					{
                        x: 100,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
					mComboUrutLapRWIPasien(),
					//----------------------------------------
					{
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Unit'
                    },
					{
                        x: 100,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
					mcomboNamaUnitSpesial(),
					mcomboKamarSpesial(),
					//----------------------------------------
					{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Kelompok Pasien'
                    },
					{
                        x: 100,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },
					mComboPilihanKelompokPasien(),
					mComboPerseorangan(),
					mComboAsuransi(),
					mComboPerusahaan(),
					mComboUmum()
				]
			}
        ]
    }
    return items;
};

function mComboUrutLapRWIPasien()
{
     var cboUrut = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 10,
			id:'cboUrut',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			width:153,
			value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'No Medrec'],[2, 'Nama'],[3, 'Tanggal Masuk'],[4, 'Nama Kamar']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUrutPasien=b.data.displayText ;
				}
			}
		}
	);
	return cboUrut;
};

function mcomboNamaUnitSpesial()
{
    var Field = ['kode','namaunit'];
    ds_UnitRuangSpesial_LapPasien = new WebApp.DataStore({fields: Field});

	ds_UnitRuangSpesial_LapPasien.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'kode',
                Sortdir: 'ASC',
                target:'ViewSetupUnitKamarSpesial',
                param: "1"
            }
        }
    )

    var cboUnitRuangSpesial = new Ext.form.ComboBox
    (
        {
			x: 110,
			y: 40,
            id: 'cboUnitRuangSpesial',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            store: ds_UnitRuangSpesial_LapPasien,
            valueField: 'kode',
            displayField: 'namaunit',
            anchor:'60%',
			value:'Semua',
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetUnitRuangSpesial=b.data.kode ;

				    loadds_KamarSpesial_viDaftar(b.data.kode);					
				},
				'render': function(c)
				{
					
				}


			}
        }
    )

    return cboUnitRuangSpesial;
};

function loadds_KamarSpesial_viDaftar(kode){
	ds_KamarSpesial_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'No_Kamar',
                Sortdir: 'ASC',
                target:'ViewSetupUnitKamarSpesial',
                param: kode
            }
        }
    )
};

function mcomboKamarSpesial()
{
    var Field = ['no_kamar','nama_kamar'];
    ds_KamarSpesial_viDaftar = new WebApp.DataStore({fields: Field});

    var cboRujukanKamarSpesialRequestEntry = new Ext.form.ComboBox
    (
        {
			x: 270,
			y: 40,
            id: 'cboRujukanKamarSpesialRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            align: 'Right',
            store: ds_KamarSpesial_viDaftar,
            valueField: 'no_kamar',
            displayField: 'nama_kamar',
			value:'SEMUA',
            anchor: '99%',
            listeners:
			{
					'select': function(a, b, c)
					{
						selectSetKamarSpesial=b.data.kode ;			   
					},
					'render': function(c)
					{
						//selectSetKamarSpesial=c.data.valueField;
					}


			}
        }
    )

    return cboRujukanKamarSpesialRequestEntry;
};

function mComboPilihanKelompokPasien()
{
    var cboPilihankelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 110,
                y: 70,
                id:'cboPilihankelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihankelompokPasien;
};

function mComboPerseorangan()
{
    var cboPerseorangan = new Ext.form.ComboBox
	(
            {
                x: 110,
                y: 100,
                id:'cboPerseorangan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:1,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseorangan;
};

function mComboUmum()
{
    var cboUmum = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 100,
			id:'cboUmum',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmum;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1'
			}
		}
	);
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 100,
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: 'Semua',
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntry;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2"
            }
        }
    );
    var cboAsuransi = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 100,
			id:'cboAsuransi',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: 'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransi;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseorangan').show();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').show();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').show();
   }
   else
   {
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboUmum').show();
   }
};

function GetCriteriaRWIPasien()
{
	var strKriteria = '';
	if (Ext.getCmp('cboUrut').getValue() !== ''){
		
		strKriteria ='urut';
		strKriteria += '##@@##' + Ext.get('cboUrut').getValue();
	}
	if (Ext.getCmp('cboUnitRuangSpesial').getValue() !== ''){
		
		strKriteria += '##@@##' + 'unit/ruang';
		strKriteria += '##@@##' + Ext.get('cboUnitRuangSpesial').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitRuangSpesial').getValue();
	}
	if (Ext.getCmp('cboRujukanKamarSpesialRequestEntry').getValue() !== ''){
		
		strKriteria += '##@@##' + 'kamar';
		strKriteria += '##@@##' + Ext.get('cboRujukanKamarSpesialRequestEntry').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboRujukanKamarSpesialRequestEntry').getValue();
	}
	if (Ext.getCmp('cboPilihankelompokPasien').getValue() !== '')
	{
		if (Ext.get('cboPilihankelompokPasien').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
			strKriteria += '##@@##' + 'NULL';
			
        } else if (Ext.get('cboPilihankelompokPasien').getValue() === 'Perseorangan'){
			
			var selectSetPerseorangan2='';
            selectSetPerseorangan2 = '0000000001';
			strKriteria += '##@@##' + 'Perseorangan';
            strKriteria += '##@@##' + 'Umum';
            strKriteria += '##@@##' + selectSetPerseorangan2;
			
        } else if (Ext.get('cboPilihankelompokPasien').getValue() === 'Perusahaan'){
            
			if(Ext.getCmp('cboPerusahaanRequestEntry').getValue() === '')
            {
                strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Perusahaan';
				strKriteria += '##@@##' + selectsetnamaperusahaan;
                strKriteria += '##@@##' + selectsetperusahaan;
            }
        } else {
            
			if(Ext.getCmp('cboAsuransi').getValue() === '')
            {
                strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Asuransi';
				strKriteria += '##@@##' + selectsetnamaAsuransi;
                strKriteria += '##@@##' + selectSetAsuransi;
            }
            
        }
	}
		
	

	return strKriteria;
};


function ValidasiReportRWIPasien()
{
    var x=1;
	
	if(Ext.getCmp('cboUrut').getValue() === ''){
		
		ShowPesanWarningRWIPasienReport('Pilihan urut berdarakan belum di isi',nmTitleFormDlgReqCMRpt);
		x=0;
	}
	if(Ext.getCmp('cboUnitRuangSpesial').getValue() === ''){
		
		ShowPesanWarningRWIPasienReport('Unit belum dipilih',nmTitleFormDlgReqCMRpt);
		x=0;
	}
	if(Ext.getCmp('cboRujukanKamarSpesialRequestEntry').getValue() === ''){
		
		ShowPesanWarningRWIPasienReport('Kamar belum dipilih',nmTitleFormDlgReqCMRpt);
		x=0;
	}
	if(Ext.getCmp('cboPilihankelompokPasien').getValue() === ''){
		
		ShowPesanWarningRWIPasienReport('Kelompok pasien belum dipilih',nmTitleFormDlgReqCMRpt);
		x=0;
	}
	if(Ext.getCmp('cboPerseorangan').getValue() === '' &&  Ext.getCmp('cboPerusahaanRequestEntry').getValue() === '' &&  Ext.getCmp('cboAsuransi').getValue() === '' &&  Ext.getCmp('cboUmum').getValue() === ''){
		ShowPesanWarningRWIPasienReport('Sub kelompok pasien belum dipilih',nmTitleFormDlgReqCMRpt);
        x=0;
	}
	

    return x;
};

function ValidasiTanggalReportRWIPasien()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWIPasien').dom.value > Ext.get('dtpTglAkhirLapRWIPasien').dom.value)
    {
        ShowPesanWarningRWIPasienReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWIPasienReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

