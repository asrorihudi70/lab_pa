
var tigaharilalu_rwi = new Date().add(Date.DAY, -3);


var now = new Date();
var selectSetPerseorangan;
var frmDlgPelayananDokter_RWI;
var varLapPelayananDokter_RWI= ShowFormLapPelayananDokter_RWI();
var selectSetUmum;
var selectSetkelpas;



var selectSetPilihankelompokPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapPelayananDokter_RWI()
{
    frmDlgPelayananDokter_RWI= fnDlgPelayananDokter_RWI();
    frmDlgPelayananDokter_RWI.show();
};

function fnDlgPelayananDokter_RWI()
{
    var winPelayananDokterReport_RWI = new Ext.Window
    (
        {
            id: 'winPelayananDokterReport_RWI',
            title: 'Laporan Transaksi Jasa Pelayanan Dokter Per Pasien',
            closeAction: 'destroy',
            width:400,
            height: 285,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [winPelayananDokterReport_RWI2()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganRWIDOKTER').show();
                Ext.getCmp('cboAsuransiRWIDOKTER').hide();
                Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
                Ext.getCmp('cboUmumRWIDOKTER').hide();
            }
        }

        }
    );

    return winPelayananDokterReport_RWI;
};


function winPelayananDokterReport_RWI2()
{
    var PnlLapPelayananDokter_RWI = new Ext.Panel
    (
        {
            id: 'PnlLapPelayananDokter_RWI',
            fileUpload: true,
            layout: 'form',
			//width:400,
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapPelayananDokter_Atas_RWI(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapPelayananDokter_RWI',
                            handler: function()
                            {
                               /* if (ValidasiReportPelayananDokter() === 1)
                               { */		loadMask.show();
                                        var criteria = GetCriteriaPelayananDokter();
                                        frmDlgPelayananDokter_RWI.close();
                                        loadlaporanRWI('0', 'rep030210', criteria, function(){
											loadMask.hide();
										});
								/* }; */
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapPelayananDokter_RWI',
                            handler: function()
                            {
                                    frmDlgPelayananDokter_RWI.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapPelayananDokter_RWI;
};

function getItemLapPelayananDokter_Atas_RWI()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 200,
            anchor: '100% 100%',
            items: [
			{
				xtype: 'checkboxgroup',
				width:210,
				x: 120,
				y: 10,
				items: 
				[
					{
						
						boxLabel: 'Pendaftaran',
						name: 'cbPendaftaran_PelayananDokter_RWI',
						id : 'cbPendaftaran_PelayananDokter_RWI'
					},
					{
						boxLabel: 'Tindak RWJ',
						name: 'cbTindakRWJ_PelayananDokter_RWI',
						id : 'cbTindakRWJ_PelayananDokter_RWI'
					}
			   ]
			},
			{
				x: 10,
				y: 40,
				xtype: 'label',
				text: 'Poliklinik '
			}, {
				x: 110,
				y: 40,
				xtype: 'label',
				text: ' : '
			},
				cbTindakRWJ_PelayananDokter_RWI(),
			{
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Dokter '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
			mComboDokterPelayananDokter(),
			{
				x: 10,
				y: 100,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, {
				x: 110,
				y: 100,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 100,
				xtype: 'datefield',
				id: 'dtpTglAwalLapPelayananDokter',
				format: 'd/M/Y',
				value: tigaharilalu_rwi
			}, {
				x: 230,
				y: 100,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 260,
				y: 100,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapPelayananDokter',
				format: 'd/M/Y',
				value: now,
				width: 100
			},{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 130,
				xtype: 'label',
				text: ' : '
			},
				mComboKelompokPasienPelayananDokter(),
				mComboUmumPelayananDokter(),
				mComboPerseoranganPelayananDokter(),
				mComboAsuransiPelayananDokter(),
				mComboPerusahaanPelayananDokter()
            ]
        }]
    };
    return items;
};

function GetCriteriaPelayananDokter()
{
	var strKriteria = '';
	
	if (Ext.getCmp('cbPendaftaran_PelayananDokter_RWI').getValue() === true && Ext.getCmp('cbTindakRWJ_PelayananDokter_RWI').getValue() === false){
		strKriteria ='autocas';
		strKriteria += '##@@##' + 1;
	}else if(Ext.getCmp('cbPendaftaran_PelayananDokter_RWI').getValue() === false && Ext.getCmp('cbTindakRWJ_PelayananDokter_RWI').getValue() === true) {
		strKriteria ='autocas';
		strKriteria += '##@@##' + 2;
	} else if(Ext.getCmp('cbPendaftaran_PelayananDokter_RWI').getValue() === true && Ext.getCmp('cbTindakRWJ_PelayananDokter_RWI').getValue() === true){
		strKriteria ='NoCheked';
		strKriteria += '##@@##' + 3;
	} else{
		strKriteria ='NoCheked';
		strKriteria += '##@@##' + 4;
	}
		
	if(Ext.getCmp('cbounitRequestEntryPelayananDokter_RWI').getValue() === '' || Ext.getCmp('cbounitRequestEntryPelayananDokter_RWI').getValue() === 'Semua'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + 'Semua';
	} else{
		strKriteria += '##@@##' + Ext.get('cbounitRequestEntryPelayananDokter_RWI').getValue();;
		strKriteria += '##@@##' + Ext.getCmp('cbounitRequestEntryPelayananDokter_RWI').getValue();
	}
	
	if(Ext.getCmp('cboDokterPelayananDokter').getValue() === '' || Ext.getCmp('cboDokterPelayananDokter').getValue() === 'Semua'){
		strKriteria += '##@@##' + 'Dokter';
		strKriteria += '##@@##' + 'Semua';
	} else{
		strKriteria += '##@@##' + Ext.get('cboDokterPelayananDokter').getValue();;
		strKriteria += '##@@##' + Ext.getCmp('cboDokterPelayananDokter').getValue();
	}
	
	if (Ext.get('dtpTglAwalLapPelayananDokter').getValue() !== ''){
		strKriteria += '##@@##' + Ext.get('dtpTglAwalLapPelayananDokter').getValue();
	}
	if (Ext.get('dtpTglAkhirLapPelayananDokter').getValue() !== ''){
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapPelayananDokter').getValue();
	}
	
	if (Ext.getCmp('cboPilihanPelayananDokterkelompokPasien').getValue() !== '')
	{
		if (Ext.get('cboPilihanPelayananDokterkelompokPasien').getValue() === 'Semua') {
            strKriteria += '##@@##' + 'Kelpas';
			strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
        } else if (Ext.get('cboPilihanPelayananDokterkelompokPasien').getValue() === 'Perseorangan'){

			strKriteria += '##@@##' + 'Perseorangan';
            strKriteria += '##@@##' + Ext.get('cboPerseoranganRWIDOKTER').getValue();
            strKriteria += '##@@##' + Ext.getCmp('cboPerseoranganRWIDOKTER').getValue();
        } else if (Ext.get('cboPilihanPelayananDokterkelompokPasien').getValue() === 'Perusahaan'){
            
			if(Ext.getCmp('cboPerusahaanRequestEntryRWJ').getValue() === '')
            {
				strKriteria += '##@@##' + 'Kelpas';
				strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Perusahaan';
				strKriteria += '##@@##' + Ext.get('cboPerusahaanRequestEntryRWJ').getValue();
                strKriteria += '##@@##' + Ext.getCmp('cboPerusahaanRequestEntryRWJ').getValue();
            }
        } else {
            
			if(Ext.getCmp('cboAsuransiRWIDOKTER').getValue() === '')
            {
				strKriteria += '##@@##' + 'Kelpas';
				strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Asuransi';
				strKriteria += '##@@##' + Ext.get('cboAsuransiRWIDOKTER').getValue();
                strKriteria += '##@@##' + Ext.getCmp('cboAsuransiRWIDOKTER').getValue();
            }
            
        }
	}  else{
            strKriteria += '##@@##' + 'Kelpas';
			strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
	}

	
	return strKriteria;
};

function ValidasiReportPelayananDokter()
{
    var x=1;
    if(Ext.getCmp('dtpTglAwalLapPelayananDokter').getValue() > Ext.getCmp('dtpTglAkhirLapPelayananDokter').getValue())
    {
        ShowPesanWarningPelayananDokterReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }
	if(Ext.getCmp('cboPilihanPelayananDokterkelompokPasien').getValue() === ''){
		ShowPesanWarningPelayananDokterReport('Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	if(Ext.getCmp('cboPerusahaanRequestEntryRWJ').getValue() === '' &&  Ext.getCmp('cboAsuransiRWIDOKTER').getValue() === '' &&  Ext.getCmp('cboPerseoranganRWIDOKTER').getValue() === ''){
		ShowPesanWarningPelayananDokterReport('Sub Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningPelayananDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function mComboDokterPelayananDokter()
{
	var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokterLap',
				param: "where d.nama not in('-') and left(dk.kd_unit,1)='2' order by d.nama"
			}
		}
	);
    var cboPilihanPelayananDokter = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboDokterPelayananDokter',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanPelayananDokter;
};

function mComboKelompokPasienPelayananDokter()
{
    var cboPilihanPelayananDokterkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 130,
                id:'cboPilihanPelayananDokterkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select_RWI_DOKTER(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanPelayananDokterkelompokPasien;
};

function mComboPerseoranganPelayananDokter()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganRWIDOKTER = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 160,
                id:'cboPerseoranganRWIDOKTER',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRWIDOKTER;
};

function mComboUmumPelayananDokter()
{
    var cboUmumRWIDOKTER = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 160,
			id:'cboUmumRWIDOKTER',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			valueField: 'Id',
            displayField: 'displayText',
            value:1,
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
									'Id',
									'displayText'
							],
					data: [[1, 'Umum']]
					}
			),
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumRWIDOKTER;
};

function mComboPerusahaanPelayananDokter()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1  order by CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 160,
		    id: 'cboPerusahaanRequestEntryRWJ',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRWJ;
};

function mComboAsuransiPelayananDokter()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2  order by CUSTOMER"
            }
        }
    );
    var cboAsuransiRWIDOKTER = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 160,
			id:'cboAsuransiRWIDOKTER',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiRWIDOKTER;
};

function Combo_Select_RWI_DOKTER(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRWIDOKTER').show();
        Ext.getCmp('cboAsuransiRWIDOKTER').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWIDOKTER').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRWIDOKTER').hide();
        Ext.getCmp('cboAsuransiRWIDOKTER').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').show();
        Ext.getCmp('cboUmumRWIDOKTER').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRWIDOKTER').hide();
        Ext.getCmp('cboAsuransiRWIDOKTER').show();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWIDOKTER').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRWIDOKTER').hide();
        Ext.getCmp('cboAsuransiRWIDOKTER').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWIDOKTER').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRWIDOKTER').hide();
        Ext.getCmp('cboAsuransiRWIDOKTER').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWIDOKTER').show();
   }
}

function cbTindakRWJ_PelayananDokter_RWI()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar_RWI = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar_RWI.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryPelayananDokter_RWI = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 40,
            id: 'cbounitRequestEntryPelayananDokter_RWI',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar_RWI,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryPelayananDokter_RWI;
};
