
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRadJasaPelayananDokter;
var selectNamaRadJasaPelayananDokter;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRadJasaPelayananDokter;
var varLapRadJasaPelayananDokter = ShowFormLapRadJasaPelayananDokter();
var selectSetUmum;
var selectSetkelpas;
var JenisLap;

function ShowFormLapRadJasaPelayananDokter()
{
    frmDlgRadJasaPelayananDokter = fnDlgRadJasaPelayananDokter();
    frmDlgRadJasaPelayananDokter.show();
}
;

function fnDlgRadJasaPelayananDokter()
{
    var winRadJasaPelayananDokterReport = new Ext.Window
            (
                    {
                        id: 'winRadJasaPelayananDokterReport',
                        title: 'Laporan Jasa Pelayanan Dokter',
                        closeAction: 'destroy',
                        width: 400,
                        height: 350,
                        border: false,
                        resizable: false,
                        constrain: true,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'icon_lapor',
                        modal: true,
                        items: [ItemDlgRadJasaPelayananDokter()],
                        listeners:
                                {
                                    activate: function ()
                                    {
                                        Ext.get('dtpTglAwalFilterHasilRad').show();
                                        Ext.get('dtpTglAkhirFilterHasilRad').show();
                                        Ext.get('dtpBlnAwalFilterHasilRad').hide();
                                        Ext.get('dtpBlnAkhirFilterHasilRad').hide();
                                        selectSetPilihan = 'Semua Pasien';
                                        Ext.getCmp('cboPilihanRadJasaPelayananDokter').setValue('Semua Pasien');
                                        selectSetPilihankelompokPasien = 'Semua';
                                        Ext.getCmp('cboPilihanRadJasaPelayananDokterkelompokPasien').setValue('Semua');
                                        Ext.getCmp('cboUmumJasaPelayananDokterRad').setValue('Semua');
                                        Ext.getCmp('cboPerseoranganJasaPelayananDokterRad').hide();
                                        Ext.getCmp('cboAsuransiJasaPelayananDokterRad').hide();
                                        Ext.getCmp('cboPerusahaanRequestEntryJasaPelayananDokterRad').hide();
                                        Ext.getCmp('cboUmumJasaPelayananDokterRad').show();
                                        Ext.getCmp('cboUmumJasaPelayananDokterRad').disable();
                                        Ext.getCmp('Shift_All_RadJasaPelayananDokter').setValue(true);
                                        Ext.getCmp('Shift_1_RadJasaPelayananDokter').setValue(true);
                                        Ext.getCmp('Shift_2_RadJasaPelayananDokter').setValue(true);
                                        Ext.getCmp('Shift_3_RadJasaPelayananDokter').setValue(true);
                                    }
                                }

                    }
            );

    return winRadJasaPelayananDokterReport;
}
;

function ItemDlgRadJasaPelayananDokter()
{
    var PnlLapRadJasaPelayananDokter = new Ext.Panel
            (
                    {
                        id: 'PnlLapRadJasaPelayananDokter',
                        fileUpload: true,
                        layout: 'form',
                        height: '500',
                        anchor: '100%',
                        bodyStyle: 'padding:5px',
                        border: true,
                        items:
                                [
                                    getItemLapRadJasaPelayananDokter_Atas(),
                                    getItemLapRadJasaPelayananDokter_Bawah(),
                                    {xtype: 'tbspacer', height: 10},
                                    getItemLapRadJasaPelayananDokter_NewBawah(),
                                    {
                                        layout: 'hBox',
                                        border: false,
                                        defaults: {margins: '0 5 0 0'},
                                        style: {'margin-left': '30px', 'margin-top': '5px'},
                                        anchor: '94%',
                                        layoutConfig:
                                                {
                                                    padding: '3',
                                                    pack: 'end',
                                                    align: 'middle'
                                                },
                                        items:
                                                [
                                                    {
                                                        xtype: 'button',
                                                        text: 'Ok',
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnOkLapRadJasaPelayananDokter',
                                                        handler: function ()
                                                        {

                                                            var criteria = GetCriteriaRadJasaPelayananDokter();
                                                            frmDlgRadJasaPelayananDokter.close();
                                                            loadlaporanRadLab('0', 'LapJasaPelayananDokterRad', criteria);
                                                        }
                                                    },
                                                    {
                                                        xtype: 'button',
                                                        text: 'Cancel',
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnCancelLapRadJasaPelayananDokter',
                                                        handler: function ()
                                                        {
                                                            frmDlgRadJasaPelayananDokter.close();
                                                        }
                                                    }
                                                ]
                                    }
                                ]
                    }
            );

    return PnlLapRadJasaPelayananDokter;
}
;

function GetCriteriaRadJasaPelayananDokter()
{
    var strKriteria = '';

    if (Ext.get('dtpTglAwalFilterHasilRad').getValue() !== '')
    {
        strKriteria = Ext.get('dtpTglAwalFilterHasilRad').getValue();
    }
    ;
    if (Ext.get('dtpTglAkhirFilterHasilRad').getValue() !== '')
    {
        strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterHasilRad').getValue();
    }
    ;

    if (Ext.get('cboPilihanRadJasaPelayananDokter').getValue() === 'Semua')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'NULL';
    } else if (Ext.get('cboPilihanRadJasaPelayananDokter').getValue() === 'RWJ/IGD')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'RWJ/IGD';
    } else if (Ext.get('cboPilihanRadJasaPelayananDokter').getValue() === 'RWI')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'RWI';
    } else
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'KL';
    }

    if (Ext.get('cboPilihanRadJasaPelayananDokterkelompokPasien').getValue() === 'Semua')
    {
        strKriteria += '##@@##' + 'Semua';
        strKriteria += '##@@##' + 'NULL';
    }
    if (Ext.get('cboPilihanRadJasaPelayananDokterkelompokPasien').getValue() === 'Perseorangan')
    {
        selectSetPerseorangan = '0000000001';
        strKriteria += '##@@##' + 'Umum';
        strKriteria += '##@@##' + selectSetPerseorangan;
    }
    if (Ext.get('cboPilihanRadJasaPelayananDokterkelompokPasien').getValue() === 'Perusahaan')
    {
        if (Ext.get('cboPerusahaanRequestEntryJasaPelayananDokterRad').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Perusahaan';
            strKriteria += '##@@##' + 'NULL';
        } else {
            strKriteria += '##@@##' + 'Perusahaan';
            strKriteria += '##@@##' + selectsetperusahaan;
        }
    }
    if (Ext.get('cboPilihanRadJasaPelayananDokterkelompokPasien').getValue() === 'Asuransi')
    {
        if (Ext.get('cboAsuransiJasaPelayananDokterRad').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Asuransi';
            strKriteria += '##@@##' + 'NULL';
        } else {
            strKriteria += '##@@##' + 'Asuransi';
            strKriteria += '##@@##' + selectSetAsuransi;
        }

    }
    ;
    if (Ext.getCmp('Shift_All_RadJasaPelayananDokter').getValue() === true)
    {
        strKriteria += '##@@##' + 'shift1';
        strKriteria += '##@@##' + 1;
        strKriteria += '##@@##' + 'shift2';
        strKriteria += '##@@##' + 2;
        strKriteria += '##@@##' + 'shift3';
        strKriteria += '##@@##' + 3;
    } else {
        if (Ext.getCmp('Shift_1_RadJasaPelayananDokter').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift1';
            strKriteria += '##@@##' + 1;
        }
        if (Ext.getCmp('Shift_2_RadJasaPelayananDokter').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift2';
            strKriteria += '##@@##' + 2;
        }
        if (Ext.getCmp('Shift_3_RadJasaPelayananDokter').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift3';
            strKriteria += '##@@##' + 3;
        }
    }
    return strKriteria;
}
;

function ValidasiTanggalReportRadJasaPelayananDokter()
{
    var x = 1;
    if (Ext.get('dtpTglAwalLapRadJasaPelayananDokter').dom.value > Ext.get('dtpTglAkhirLapRadJasaPelayananDokter').dom.value)
    {
        ShowPesanWarningRadJasaPelayananDokterReport(nmWarningDateDlgRpt, nmTitleFormDlgReqCMRpt);
        x = 0;
    }

    return x;
}
;

function ShowPesanWarningRadJasaPelayananDokterReport(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 300
                    }
            );
}
;

function getItemLapRadJasaPelayananDokter_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 375,
                height: 165,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Jenis Laporan '
                    }, {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'radiogroup',
                        id: 'rgjenislaporan',
                        boxMaxWidth: 300,
                        items: [
                            {
                                boxLabel: 'Detail',
                                name: 'rgjl',
                                inputValue: 0,
                                checked: true,
                                id: 'rbDetail'
                            },
                            {
                                boxLabel: 'Summary',
                                name: 'rgjl',
                                inputValue: 1,
                                id: 'rbSummary'
                            },
                            {
                                boxLabel: 'Per Pasien',
                                name: 'rgjl',
                                inputValue: 2,
                                id: 'rbPasien'
                            }
                        ],
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                if (Ext.getCmp('rbDetail').checked === true)
                                {
                                    JenisLap = 'Detail';
                                } else if (Ext.getCmp('rbSummary').checked === true) {
                                    JenisLap = 'Summary';
                                } else{
                                    JenisLap = 'Pasien';
                                }
                            }
                        }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Pasien '
                    }, {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboPilihanRadJasaPelayananDokter(),
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Kelompok pasien '
                    }, {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboPilihanRadJasaPelayananDokterKelompokPasien(),
                    mComboPerseoranganRadJasaPelayananDokter(),
                    mComboAsuransiRadJasaPelayananDokter(),
                    mComboPerusahaanRadJasaPelayananDokter(),
                    mComboUmumRadJasaPelayananDokter(),
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Dokter '
                    }, {
                        x: 110,
                        y: 130,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboDokterRadJasaPelayananDokter()
                ]
            }]
    };
    return items;
}
;

function getItemLapRadJasaPelayananDokter_Bawah()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                xtype: 'fieldset',
                                title: 'Shift',
                                autoHeight: true,
                                width: '373px',
                                defaultType: 'checkbox', // each item will be a checkbox
                                items:
                                        [
                                            {
                                                xtype: 'checkboxgroup',
                                                items: [
                                                    {boxLabel: 'Semua', name: 'Shift_All_RadJasaPelayananDokter', id: 'Shift_All_RadJasaPelayananDokter', handler: function (field, value) {
                                                            if (value === true) {
                                                                Ext.getCmp('Shift_1_RadJasaPelayananDokter').setValue(true);
                                                                Ext.getCmp('Shift_2_RadJasaPelayananDokter').setValue(true);
                                                                Ext.getCmp('Shift_3_RadJasaPelayananDokter').setValue(true);
                                                                Ext.getCmp('Shift_1_RadJasaPelayananDokter').disable();
                                                                Ext.getCmp('Shift_2_RadJasaPelayananDokter').disable();
                                                                Ext.getCmp('Shift_3_RadJasaPelayananDokter').disable();
                                                            } else {
                                                                Ext.getCmp('Shift_1_RadJasaPelayananDokter').setValue(false);
                                                                Ext.getCmp('Shift_2_RadJasaPelayananDokter').setValue(false);
                                                                Ext.getCmp('Shift_3_RadJasaPelayananDokter').setValue(false);
                                                                Ext.getCmp('Shift_1_RadJasaPelayananDokter').enable();
                                                                Ext.getCmp('Shift_2_RadJasaPelayananDokter').enable();
                                                                Ext.getCmp('Shift_3_RadJasaPelayananDokter').enable();
                                                            }
                                                        }},
                                                    {boxLabel: 'Shift 1', name: 'Shift_1_RadJasaPelayananDokter', id: 'Shift_1_RadJasaPelayananDokter'},
                                                    {boxLabel: 'Shift 2', name: 'Shift_2_RadJasaPelayananDokter', id: 'Shift_2_RadJasaPelayananDokter'},
                                                    {boxLabel: 'Shift 3', name: 'Shift_3_RadJasaPelayananDokter', id: 'Shift_3_RadJasaPelayananDokter'}
                                                ]
                                            }
                                        ]
                            }
                        ]

            };
    return items;
}
;

function getItemLapRadJasaPelayananDokter_NewBawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 375,
                height: 80,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Transaksi Pada '
                    }, {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'radiogroup',
                        id: 'rgTanggal',
                        boxMaxWidth: 150,
                        items: [
                            {
                                boxLabel: 'Tanggal',
                                name: 'rgTanggallap',
                                inputValue: 0,
                                checked: true,
                                id: 'rbTanggal'
                            },
                            {
                                boxLabel: 'Bulan',
                                name: 'rgTanggallap',
                                inputValue: 1,
                                id: 'rbBulan'
                            }
                        ],
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                if (Ext.getCmp('rbTanggal').checked === true)
                                {
                                    Ext.get('dtpTglAwalFilterHasilRad').show();
                                    Ext.get('dtpTglAkhirFilterHasilRad').show();
                                    Ext.get('dtpBlnAwalFilterHasilRad').hide();
                                    Ext.get('dtpBlnAkhirFilterHasilRad').hide();
                                } else {
                                    Ext.get('dtpTglAwalFilterHasilRad').hide();
                                    Ext.get('dtpTglAkhirFilterHasilRad').hide();
                                    Ext.get('dtpBlnAwalFilterHasilRad').show();
                                    Ext.get('dtpBlnAkhirFilterHasilRad').show();
                                }
                            }
                        }
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterHasilRad',
                        format: 'd/M/Y',
                        value: now
                    }, {
                        x: 235,
                        y: 45,
                        xtype: 'label',
                        text: ' s/d '
                    }, {
                        x: 260,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterHasilRad',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpBlnAwalFilterHasilRad',
                        format: 'M/Y',
                        value: now
                    }, {
                        x: 235,
                        y: 45,
                        xtype: 'label',
                        text: ' s/d '
                    }, {
                        x: 260,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpBlnAkhirFilterHasilRad',
                        format: 'M/Y',
                        value: now,
                        width: 100
                    },
                ]
            }]
    };
    return items;
}
;

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
function mComboPilihanRadJasaPelayananDokter()
{
    var cboPilihanRadJasaPelayananDokter = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboPilihanRadJasaPelayananDokter',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: 'Pendaftaran Per Shift ',
                        width: 240,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua Pasien'], [2, 'Pasien Rawat Jalan'], [3, 'Pasien Rawat Inap'], [4, 'Pasien Gawat Darurat'], [5, 'Pasien Umum']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetPilihan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPilihan = b.data.displayText;
                                    }
                                }
                    }
            );
    return cboPilihanRadJasaPelayananDokter;
}
;

function mComboPilihanRadJasaPelayananDokterKelompokPasien()
{
    var cboPilihanRadJasaPelayananDokterkelompokPasien = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboPilihanRadJasaPelayananDokterkelompokPasien',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: 'Pendaftaran Per Shift ',
                        width: 240,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua'], [2, 'Perseorangan'], [3, 'Perusahaan'], [4, 'Asuransi']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetPilihankelompokPasien,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPilihankelompokPasien = b.data.displayText;
                                        Combo_Select(b.data.displayText);
                                    }
                                }
                    }
            );
    return cboPilihanRadJasaPelayananDokterkelompokPasien;
}
;

function mComboPerseoranganRadJasaPelayananDokter()
{
    var Field = ['KD_CUSTOMER', 'CUSTOMER'];
    dsPerseoranganRequestEntryJasaPelayananDokterRad = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntryJasaPelayananDokterRad.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    //Sort: 'DEPT_ID',
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=0 order by CUSTOMER"
                                }
                    }
            );
    var cboPerseoranganJasaPelayananDokterRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboPerseoranganJasaPelayananDokterRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: '',
                        width: 240,
                        store: dsPerseoranganRequestEntryJasaPelayananDokterRad,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetPerseorangan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPerseorangan = b.data.displayText;
                                    }
                                }
                    }
            );
    return cboPerseoranganJasaPelayananDokterRad;
}
;

function mComboUmumRadJasaPelayananDokter()
{
    var cboUmumJasaPelayananDokterRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboUmumJasaPelayananDokterRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: '',
                        width: 240,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetUmum,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetUmum = b.data.displayText;
                                    }


                                }
                    }
            );
    return cboUmumJasaPelayananDokterRad;
}
;

function mComboDokterRadJasaPelayananDokter()
{
    var cboDokterJasaPelayananDokterRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 130,
                        id: 'cboDokterJasaPelayananDokterRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: '',
                        width: 240,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetUmum,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetDokter = b.data.displayText;
                                    }


                                }
                    }
            );
    return cboDokterJasaPelayananDokterRad;
}
;

function mComboPerusahaanRadJasaPelayananDokter()
{
    var Field = ['KD_CUSTOMER', 'CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    //Sort: 'DEPT_ID',
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=1 order by CUSTOMER"
                                }
                    }
            );
    var cboPerusahaanRequestEntryJasaPelayananDokterRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboPerusahaanRequestEntryJasaPelayananDokterRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Perusahaan...',
                        fieldLabel: '',
                        align: 'Right',
                        store: dsPerusahaanRequestEntry,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        width: 240,
                        value: selectsetperusahaan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectsetperusahaan = b.data.KD_CUSTOMER;
                                    }
                                }
                    }
            );

    return cboPerusahaanRequestEntryJasaPelayananDokterRad;
}
;

function mComboAsuransiRadJasaPelayananDokter()
{
    var Field_poli_viDaftar = ['KD_CUSTOMER', 'CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: '',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=2 order by CUSTOMER"
                                }
                    }
            );
    var cboAsuransiJasaPelayananDokterRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboAsuransiJasaPelayananDokterRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Asuransi...',
                        fieldLabel: '',
                        align: 'Right',
                        width: 240,
                        store: ds_customer_viDaftar,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetAsuransi,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetAsuransi = b.data.KD_CUSTOMER;
                                    }
                                }
                    }
            );
    return cboAsuransiJasaPelayananDokterRad;
}
;

function Combo_Select(combo)
{
    var value = combo;

    if (value === "Perseorangan")
    {
        Ext.getCmp('cboPerseoranganJasaPelayananDokterRad').show();
        Ext.getCmp('cboAsuransiJasaPelayananDokterRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryJasaPelayananDokterRad').hide();
        Ext.getCmp('cboUmumJasaPelayananDokterRad').hide();
    } else if (value === "Perusahaan")
    {
        Ext.getCmp('cboPerseoranganJasaPelayananDokterRad').hide();
        Ext.getCmp('cboAsuransiJasaPelayananDokterRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryJasaPelayananDokterRad').show();
        Ext.getCmp('cboUmumJasaPelayananDokterRad').hide();
    } else if (value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganJasaPelayananDokterRad').hide();
        Ext.getCmp('cboAsuransiJasaPelayananDokterRad').show();
        Ext.getCmp('cboPerusahaanRequestEntryJasaPelayananDokterRad').hide();
        Ext.getCmp('cboUmumJasaPelayananDokterRad').hide();
    } else if (value === "Semua")
    {
        Ext.getCmp('cboPerseoranganJasaPelayananDokterRad').hide();
        Ext.getCmp('cboAsuransiJasaPelayananDokterRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryJasaPelayananDokterRad').hide();
        Ext.getCmp('cboUmumJasaPelayananDokterRad').show();
        Ext.getCmp('cboUmumJasaPelayananDokterRad').disable();
    } else
    {
        Ext.getCmp('cboPerseoranganJasaPelayananDokterRad').hide();
        Ext.getCmp('cboAsuransiJasaPelayananDokterRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryJasaPelayananDokterRad').hide();
        Ext.getCmp('cboUmumJasaPelayananDokterRad').show();
    }
}

function mCombounitRadJasaPelayananDokter()
{
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

    ds_Poli_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'NAMA_UNIT',
                                    Sortdir: 'ASC',
                                    target: 'ViewSetupUnit',
                                    param: "kd_bagian=2 and type_unit=false"
                                }
                    }
            );

    var cbounitRequestEntryRadJasaPelayananDokter = new Ext.form.ComboBox
            (
                    {
                        id: 'cbounitRequestEntryRadJasaPelayananDokter',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih unit...',
                        fieldLabel: 'unit ',
                        align: 'Right',
                        store: ds_Poli_viDaftar,
                        valueField: 'KD_UNIT',
                        displayField: 'NAMA_UNIT',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                    }

                                }
                    }
            )

    return cbounitRequestEntryRadJasaPelayananDokter;
}
;
