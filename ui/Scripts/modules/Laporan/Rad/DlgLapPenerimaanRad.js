
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsDlgLapPengelompokanPenerimaanRad;
var selectNamaDlgLapPengelompokanPenerimaanRad;
var now = new Date();
var selectSetPerseorangan;
var frmDlgDlgLapPengelompokanPenerimaanRad;
var varLapDlgLapPengelompokanPenerimaanRad = ShowFormLapDlgLapPengelompokanPenerimaanRad();
var selectSetUmum;
var selectSetkelpas;
var dsRequestPengelompokanLaporanPenerimaanRad;
var secondGridStore;

function ShowFormLapDlgLapPengelompokanPenerimaanRad()
{
    frmDlgDlgLapPengelompokanPenerimaanRad = fnDlgDlgLapPengelompokanPenerimaanRad();
    frmDlgDlgLapPengelompokanPenerimaanRad.show();
}
;

function fnDlgDlgLapPengelompokanPenerimaanRad()
{
    var winDlgLapPengelompokanPenerimaanRadReport = new Ext.Window
            (
                    {
                        id: 'winDlgLapPengelompokanPenerimaanRadReport',
                        title: 'Laporan Penerimaan',
                        closeAction: 'destroy',
                        constrain: true,
                        width: 385, //385
                        height: 600,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'icon_lapor',
                        modal: true,
                        items: [ItemDlgDlgLapPengelompokanPenerimaanRad()],
                        listeners:
                                {
                                    activate: function ()
                                    {
                                        Ext.getCmp('cboPerseoranganDlgLapPengelompokanPenerimaanRad').hide();
                                        Ext.getCmp('cboAsuransiDlgLapPengelompokanPenerimaanRad').hide();
                                        Ext.getCmp('cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad').hide();
                                        Ext.getCmp('cboUmumDlgLapPengelompokanPenerimaanRad').show();
                                        Ext.getCmp('cboUmumDlgLapPengelompokanPenerimaanRad').disable();
                                        Ext.getCmp('Shift_All_DlgLapPengelompokanPenerimaanRad').setValue(true);
                                        Ext.getCmp('Shift_1_DlgLapPengelompokanPenerimaanRad').setValue(true);
                                        Ext.getCmp('Shift_2_DlgLapPengelompokanPenerimaanRad').setValue(true);
                                        Ext.getCmp('Shift_3_DlgLapPengelompokanPenerimaanRad').setValue(true);
                                    }
                                }

                    }
            );

    return winDlgLapPengelompokanPenerimaanRadReport;
}
;


function ItemDlgDlgLapPengelompokanPenerimaanRad()
{
    var PnlLapDlgLapPengelompokanPenerimaanRad = new Ext.Panel
            (
                    {
                        id: 'PnlLapDlgLapPengelompokanPenerimaanRad',
                        fileUpload: true,
                        layout: 'form',
                        height: '500',
                        anchor: '100%',
                        bodyStyle: 'padding:5px',
                        border: true,
                        items:
                                [
                                    getItemLapDlgLapPengelompokanPenerimaanRad_Atas(),
                                    {xtype: 'tbspacer', height: 10},
                                    getItemLapDlgLapPengelompokanPenerimaanRad_Samping(),
                                    {xtype: 'tbspacer', height: 10},
                                    getItemLapDlgLapPengelompokanPenerimaanRad_Bawah(),
                                    {
                                        layout: 'hBox',
                                        border: false,
                                        defaults: {margins: '0 5 0 0'},
                                        style: {'margin-left': '30px', 'margin-top': '5px'},
                                        anchor: '94%',
                                        layoutConfig:
                                                {
                                                    padding: '3',
                                                    pack: 'end',
                                                    align: 'middle'
                                                },
                                        items:
                                                [
                                                    {
                                                        xtype: 'button',
                                                        text: 'Ok',
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnOkLapDlgLapPengelompokanPenerimaanRad',
                                                        handler: function ()
                                                        {
                                                            var sendDataArray = [];
                                                            secondGridStore.each(function (record) {
                                                                var recordArray = [record.get("KODE")];
                                                                sendDataArray.push(recordArray);

                                                            });
                                                            var criteria = GetCriteriaDlgLapPengelompokanPenerimaanRad();
                                                            criteria += '##@@##' + sendDataArray;
                                                            //frmDlgDlgLapPengelompokanPenerimaanRad.close();
                                                            loadlaporanRadLab('0', 'LapPenerimaanRAD', criteria);

                                                        }
                                                    },
                                                    {
                                                        xtype: 'button',
                                                        text: 'Cancel',
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnCancelLapDlgLapPengelompokanPenerimaanRad',
                                                        handler: function ()
                                                        {
                                                            frmDlgDlgLapPengelompokanPenerimaanRad.close();
                                                        }
                                                    }
                                                ]
                                    }
                                ]
                    }
            );

    return PnlLapDlgLapPengelompokanPenerimaanRad;
}
;

function ValidasiTanggalReportDlgLapPengelompokanPenerimaanRad()
{
    var x = 1;
    if (Ext.get('dtpTglAwalLapDlgLapPengelompokanPenerimaanRad').dom.value > Ext.get('dtpTglAkhirLapDlgLapPengelompokanPenerimaanRad').dom.value)
    {
        ShowPesanWarningDlgLapPengelompokanPenerimaanRadReport(nmWarningDateDlgRpt, nmTitleFormDlgReqCMRpt);
        x = 0;
    }

    return x;
}
;

function ShowPesanWarningDlgLapPengelompokanPenerimaanRadReport(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 300
                    }
            );
}
;

function getItemLapDlgLapPengelompokanPenerimaanRad_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 360,
                height: 200,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Pengelompokan Laporan '
                    },
                    mComboPengelompokanLaporanLapPenerimaanRad(),
                    {
                        x: 10,
                        y: 60,
                        xtype: 'label',
                        text: 'Periode '
                    }, {
                        x: 10,
                        y: 80,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterDlgLapPengelompokanPenerimaanRad',
                        format: 'd/M/Y',
                        value: now
                    }, {
                        x: 120,
                        y: 80,
                        xtype: 'label',
                        text: ' s/d '
                    }, {
                        x: 145,
                        y: 80,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterDlgLapPengelompokanPenerimaanRad',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 10,
                        y: 110,
                        xtype: 'label',
                        text: 'Kelompok pasien '
                    },
                    mComboPengelompokanLaporanLapPenerimaanRadKelompokPasien(),
                    mComboPerseoranganDlgLapPengelompokanPenerimaanRad(),
                    mComboAsuransiDlgLapPengelompokanPenerimaanRad(),
                    mComboPerusahaanDlgLapPengelompokanPenerimaanRad(),
                    mComboUmumDlgLapPengelompokanPenerimaanRad()
                ]
            }]
    };
    return items;
}
;

function getItemLapDlgLapPengelompokanPenerimaanRad_Samping()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 10px 10px 10px 10px',
                                border: true,
                                width: 360,
                                height: 35,
                                anchor: '100% 100%',
                                defaultType: 'checkbox',
                                items: [
                                    {x: 40, y: 10, boxLabel: 'Semua', name: 'Shift_All_DlgLapPengelompokanPenerimaanRad', id: 'Shift_All_DlgLapPengelompokanPenerimaanRad', handler: function (field, value) {
                                            if (value === true) {
                                                Ext.getCmp('Shift_1_DlgLapPengelompokanPenerimaanRad').setValue(true);
                                                Ext.getCmp('Shift_2_DlgLapPengelompokanPenerimaanRad').setValue(true);
                                                Ext.getCmp('Shift_3_DlgLapPengelompokanPenerimaanRad').setValue(true);
                                                Ext.getCmp('Shift_1_DlgLapPengelompokanPenerimaanRad').disable();
                                                Ext.getCmp('Shift_2_DlgLapPengelompokanPenerimaanRad').disable();
                                                Ext.getCmp('Shift_3_DlgLapPengelompokanPenerimaanRad').disable();
                                            } else {
                                                Ext.getCmp('Shift_1_DlgLapPengelompokanPenerimaanRad').setValue(false);
                                                Ext.getCmp('Shift_2_DlgLapPengelompokanPenerimaanRad').setValue(false);
                                                Ext.getCmp('Shift_3_DlgLapPengelompokanPenerimaanRad').setValue(false);
                                                Ext.getCmp('Shift_1_DlgLapPengelompokanPenerimaanRad').enable();
                                                Ext.getCmp('Shift_2_DlgLapPengelompokanPenerimaanRad').enable();
                                                Ext.getCmp('Shift_3_DlgLapPengelompokanPenerimaanRad').enable();
                                            }
                                        }},
                                    {x: 110, y: 10, boxLabel: 'Shift 1', name: 'Shift_1_DlgLapPengelompokanPenerimaanRad', id: 'Shift_1_DlgLapPengelompokanPenerimaanRad'},
                                    {x: 180, y: 10, boxLabel: 'Shift 2', name: 'Shift_2_DlgLapPengelompokanPenerimaanRad', id: 'Shift_2_DlgLapPengelompokanPenerimaanRad'},
                                    {x: 250, y: 10, boxLabel: 'Shift 3', name: 'Shift_3_DlgLapPengelompokanPenerimaanRad', id: 'Shift_3_DlgLapPengelompokanPenerimaanRad'}
                                ]
                            }
                        ]

            };
    return items;
}
;

function getItemLapDlgLapPengelompokanPenerimaanRad_Bawah()
{
    var Field_poli_viDaftar = ['KODE', 'NAMA'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});

    datarefresh_viInformasiUnit()

    // Generic fields array to use in both store defs.
    var fields = [
        {name: 'KODE', mapping: 'KODE'},
        {name: 'NAMA', mapping: 'NAMA'}
    ];


    // Column Model shortcut array
    var cols = [
        {id: 'KODE', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KODE', hidden: true},
        {header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA'}
    ];


    // declare the source Grid
    var firstGrid = new Ext.grid.GridPanel({
        x: 10,
        y: 10,
        ddGroup: 'secondGridDDGroup',
        store: dataSource_unit,
        autoScroll: true,
        columnLines: true,
        border: true,
        enableDragDrop: true,
        height: 225,
        width: 160,
        stripeRows: true,
        trackMouseOver: true,
        title: 'Unit',
//        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        colModel: new Ext.grid.ColumnModel
                (
                        [
                            new Ext.grid.RowNumberer(),
                            {
                                id: 'colNRM_viDaftar',
                                header: 'No.Medrec',
                                dataIndex: 'KODE',
                                sortable: true,
                                hidden: true
                            },
                            {
                                id: 'colNMPASIEN_viDaftar',
                                header: 'Nama',
                                dataIndex: 'NAMA',
                                sortable: true,
                                width: 50
                            }
                        ]
                        ),
        viewConfig:
                {
                    forceFit: true
                }
    });

    secondGridStore = new Ext.data.JsonStore({
        fields: fields,
        root: 'records'
    });

    // create the destination Grid
    var secondGrid = new Ext.grid.GridPanel({
        x: 190,
        y: 10,
        ddGroup: 'firstGridDDGroup',
        store: secondGridStore,
        columns: cols,
        enableDragDrop: true,
        height: 225,
        width: 160,
        stripeRows: true,
        autoExpandColumn: 'KODE',
        title: 'Unit',
        listeners: {
            afterrender: function (comp) {
                var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                    ddGroup: 'secondGridDDGroup',
                    notifyDrop: function (ddSource, e, data) {
                        var records = ddSource.dragData.selections;
                        Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                        secondGrid.store.add(records);
                        secondGrid.store.sort('KODE', 'ASC');
                        return true
                    }
                });
            }
        },
        viewConfig:
                {
                    forceFit: true
                }
    });
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 10px 10px 10px 10px',
                                border: true,
                                width: 360,
                                height: 250,
                                anchor: '100% 100%',
                                defaultType: 'checkbox',
                                items: [
                                    firstGrid,
                                    secondGrid
                                ]
                            }
                        ]

            };
    return items;
}
;
var valuerbDlgLapPengelompokanPenerimaanRad;

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
function mComboPengelompokanLaporanLapPenerimaanRad()
{

    var cboPilihanDlgLapPengelompokanPenerimaanRad = new Ext.form.ComboBox
            (
                    {
                        x: 10,
                        y: 30,
                        id: 'cboPilihanDlgLapPengelompokanPenerimaanRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        width: 200,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [
                                                [1, 'Perjenis Penerimaan'], [2, 'PerPasien'], [3, 'Perjenis Komponen'],
                                                [4, 'Tunai Perkomponen Detail'], [5, 'Tunai Perkomponen Sumary'], [6, 'Administrasi NCI']
                                            ]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetPilihan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPilihan = b.data.displayText;
                                    }
                                }
                    }
            );
    return cboPilihanDlgLapPengelompokanPenerimaanRad;
}
;

function mComboPengelompokanLaporanLapPenerimaanRadKelompokPasien()
{
    var cboPilihanDlgLapPengelompokanPenerimaanRadkelompokPasien = new Ext.form.ComboBox
            (
                    {
                        x: 10,
                        y: 130,
                        id: 'cboPilihanDlgLapPengelompokanPenerimaanRadkelompokPasien',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: 'Pendaftaran Per Shift ',
                        width: 200,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua'], [2, 'Perseorangan'], [3, 'Perusahaan'], [4, 'Asuransi']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetPilihankelompokPasien,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPilihankelompokPasien = b.data.displayText;
                                        Combo_Select(b.data.displayText);
                                    }
                                }
                    }
            );
    return cboPilihanDlgLapPengelompokanPenerimaanRadkelompokPasien;
}
;

function mComboPerseoranganDlgLapPengelompokanPenerimaanRad()
{
    var Field = ['KD_CUSTOMER', 'CUSTOMER'];
    dsPerseoranganRequestEntryDlgLapPengelompokanPenerimaanRad = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntryDlgLapPengelompokanPenerimaanRad.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    //Sort: 'DEPT_ID',
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=0 order by CUSTOMER"
                                }
                    }
            );
    var cboPerseoranganDlgLapPengelompokanPenerimaanRad = new Ext.form.ComboBox
            (
                    {
                        x: 10,
                        y: 160,
                        id: 'cboPerseoranganDlgLapPengelompokanPenerimaanRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: '',
                        width: 200,
                        store: dsPerseoranganRequestEntryDlgLapPengelompokanPenerimaanRad,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetPerseorangan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPerseorangan = b.data.KD_CUSTOMER;
                                    }
                                }
                    }
            );
    return cboPerseoranganDlgLapPengelompokanPenerimaanRad;
}
;

function mComboUmumDlgLapPengelompokanPenerimaanRad()
{
    var cboUmumDlgLapPengelompokanPenerimaanRad = new Ext.form.ComboBox
            (
                    {
                        x: 10,
                        y: 160,
                        id: 'cboUmumDlgLapPengelompokanPenerimaanRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: '',
                        width: 200,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetUmum,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetUmum = b.data.displayText;
                                    }


                                }
                    }
            );
    return cboUmumDlgLapPengelompokanPenerimaanRad;
}
;

function mComboPerusahaanDlgLapPengelompokanPenerimaanRad()
{
    var Field = ['KD_CUSTOMER', 'CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    //Sort: 'DEPT_ID',
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=1 order by CUSTOMER"
                                }
                    }
            );
    var cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad = new Ext.form.ComboBox
            (
                    {
                        x: 10,
                        y: 160,
                        id: 'cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Perusahaan...',
                        fieldLabel: '',
                        align: 'Right',
                        store: dsPerusahaanRequestEntry,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        width: 200,
                        value: selectsetperusahaan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectsetperusahaan = b.data.KD_CUSTOMER;
                                    }
                                }
                    }
            );

    return cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad;
}
;



function mComboAsuransiDlgLapPengelompokanPenerimaanRad()
{
    var Field_poli_viDaftar = ['KD_CUSTOMER', 'CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: '',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=2 order by CUSTOMER"
                                }
                    }
            );
    var cboAsuransiDlgLapPengelompokanPenerimaanRad = new Ext.form.ComboBox
            (
                    {
                        x: 10,
                        y: 160,
                        id: 'cboAsuransiDlgLapPengelompokanPenerimaanRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Asuransi...',
                        fieldLabel: '',
                        align: 'Right',
                        width: 200,
                        store: ds_customer_viDaftar,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetAsuransi,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetAsuransi = b.data.KD_CUSTOMER;
                                    }
                                }
                    }
            );
    return cboAsuransiDlgLapPengelompokanPenerimaanRad;
}
;

function Combo_Select(combo)
{
    var value = combo;

    if (value === "Perseorangan")
    {
        Ext.getCmp('cboPerseoranganDlgLapPengelompokanPenerimaanRad').show();
        Ext.getCmp('cboAsuransiDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboUmumDlgLapPengelompokanPenerimaanRad').hide();
    } else if (value === "Perusahaan")
    {
        Ext.getCmp('cboPerseoranganDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboAsuransiDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad').show();
        Ext.getCmp('cboUmumDlgLapPengelompokanPenerimaanRad').hide();
    } else if (value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboAsuransiDlgLapPengelompokanPenerimaanRad').show();
        Ext.getCmp('cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboUmumDlgLapPengelompokanPenerimaanRad').hide();
    } else if (value === "Semua")
    {
        Ext.getCmp('cboPerseoranganDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboAsuransiDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboUmumDlgLapPengelompokanPenerimaanRad').show();
        Ext.getCmp('cboUmumDlgLapPengelompokanPenerimaanRad').setValue('Semua');
    } else
    {
        Ext.getCmp('cboPerseoranganDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboAsuransiDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad').hide();
        Ext.getCmp('cboUmumDlgLapPengelompokanPenerimaanRad').show();
    }
}

var selectuser;
function mComboUserDlgLapPengelompokanPenerimaanRad()
{
    var Field = ['KD_USER', 'FULL_NAME'];
    ds_User_viDaftar = new WebApp.DataStore({fields: Field});

    ds_User_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: '',
                                    target: 'ViewComboUser',
                                    param: ""
                                }
                    }
            );

    var cboUserRequestEntryDlgLapPengelompokanPenerimaanRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboUserRequestEntryDlgLapPengelompokanPenerimaanRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih User...',
                        align: 'Right',
                        store: ds_User_viDaftar,
                        valueField: 'KD_USER',
                        displayField: 'FULL_NAME',
                        width: 200,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectuser = b.data.kd_user;
                                    }
                                }
                    }
            )

    return cboUserRequestEntryDlgLapPengelompokanPenerimaanRad;
}
;

function datarefresh_viInformasiUnit()
{
    dataSource_unit.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelompokLaporan',
                                    param: ""
                                }
                    }
            )
    //alert("refersh")
}

function ValidasiReportDlgLapPengelompokanPenerimaanRad()
{
    if (Ext.get('cboPilihanDlgLapPengelompokanPenerimaanRad').getValue() === '')
    {

    }
}

function GetCriteriaDlgLapPengelompokanPenerimaanRad()
{
//    console.log(Ext.get('cboPilihanDlgLapPengelompokanPenerimaanRad').getValue());
    var strKriteria = '';
    strKriteria = Ext.get('dtpTglAwalFilterDlgLapPengelompokanPenerimaanRad').getValue();
    strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterDlgLapPengelompokanPenerimaanRad').getValue();

    if (Ext.get('cboPilihanDlgLapPengelompokanPenerimaanRad').getValue() !== 'Silahkan Pilih...')
    {
        strKriteria += '##@@##' + Ext.get('cboPilihanDlgLapPengelompokanPenerimaanRad').getValue();

    } else {
        ShowPesanWarningDlgLapPengelompokanPenerimaanRadReport('Data "Pengelompok Laporan" Tidak Boleh Kosong', 'Laporan Penerimaan');
    }



    if (Ext.get('cboPilihanDlgLapPengelompokanPenerimaanRadkelompokPasien').getValue() === 'Semua')
    {
        strKriteria += '##@@##' + 'Semua';
        strKriteria += '##@@##' + 'NULL';
        strKriteria += '##@@##' + '';
    }
    if (Ext.get('cboPilihanDlgLapPengelompokanPenerimaanRadkelompokPasien').getValue() === 'Perseorangan')
    {
        selectSetPerseorangan = '0000000001';
        strKriteria += '##@@##' + 'Umum';
        strKriteria += '##@@##' + selectSetPerseorangan;
        strKriteria += '##@@##' + Ext.get('cboPilihanDlgLapPengelompokanPenerimaanRadkelompokPasien').getValue();
    }
    if (Ext.get('cboPilihanDlgLapPengelompokanPenerimaanRadkelompokPasien').getValue() === 'Perusahaan')
    {
        if (Ext.get('cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Perusahaan';
            strKriteria += '##@@##' + 'NULL';
            strKriteria += '##@@##' + Ext.get('cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad').getValue();
        } else {
            strKriteria += '##@@##' + 'Perusahaan';
            strKriteria += '##@@##' + selectsetperusahaan;
            strKriteria += '##@@##' + Ext.get('cboPerusahaanRequestEntryDlgLapPengelompokanPenerimaanRad').getValue();
        }
    }
    if (Ext.get('cboPilihanDlgLapPengelompokanPenerimaanRadkelompokPasien').getValue() === 'Asuransi')
    {
        if (Ext.get('cboAsuransiDlgLapPengelompokanPenerimaanRad').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Asuransi';
            strKriteria += '##@@##' + 'NULL';
            strKriteria += '##@@##' + Ext.get('cboAsuransiDlgLapPengelompokanPenerimaanRad').getValue();
        } else {
            strKriteria += '##@@##' + 'Asuransi';
            strKriteria += '##@@##' + selectSetAsuransi;
            strKriteria += '##@@##' + Ext.get('cboAsuransiDlgLapPengelompokanPenerimaanRad').getValue();
        }

    }
    ;
    if (Ext.getCmp('Shift_All_DlgLapPengelompokanPenerimaanRad').getValue() === true)
    {
        strKriteria += '##@@##' + 'shift1';
        strKriteria += '##@@##' + 1;
        strKriteria += '##@@##' + 'shift2';
        strKriteria += '##@@##' + 2;
        strKriteria += '##@@##' + 'shift3';
        strKriteria += '##@@##' + 3;
    } else {
        if (Ext.getCmp('Shift_1_DlgLapPengelompokanPenerimaanRad').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift1';
            strKriteria += '##@@##' + 1;
        }
        if (Ext.getCmp('Shift_2_DlgLapPengelompokanPenerimaanRad').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift2';
            strKriteria += '##@@##' + 2;
        }
        if (Ext.getCmp('Shift_3_DlgLapPengelompokanPenerimaanRad').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift3';
            strKriteria += '##@@##' + 3;
        }


    }
    return strKriteria;
}
;

