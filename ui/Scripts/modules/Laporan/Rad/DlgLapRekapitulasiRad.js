
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRadRekapitulasi;
var selectNamaRadRekapitulasi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRadRekapitulasi;
var varLapRadRekapitulasi = ShowFormLapRadRekapitulasi();
var selectSetUmum;
var selectSetkelpas;
var JenisLap;
var ds_param_vikelpas;

function ShowFormLapRadRekapitulasi()
{
    frmDlgRadRekapitulasi = fnDlgRadRekapitulasi();
    frmDlgRadRekapitulasi.show();
}
;

function fnDlgRadRekapitulasi()
{
    var winRadRekapitulasiReport = new Ext.Window
            (
                    {
                        id: 'winRadRekapitulasiReport',
                        title: 'Laporan Transaksi Batal',
                        closeAction: 'destroy',
                        width: 400,
                        height: 150,
                        border: false,
                        resizable: false,
                        constrain: true,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'icon_lapor',
                        modal: true,
                        items: [ItemDlgRadRekapitulasi()],
                        listeners:
                                {
                                    activate: function ()
                                    {
                                    }
                                }

                    }
            );

    return winRadRekapitulasiReport;
}
;

function ItemDlgRadRekapitulasi()
{
    var PnlLapRadRekapitulasi = new Ext.Panel
            (
                    {
                        id: 'PnlLapRadRekapitulasi',
                        fileUpload: true,
                        layout: 'form',
                        height: '500',
                        anchor: '100%',
                        bodyStyle: 'padding:5px',
                        border: true,
                        items:
                                [
                                    getItemLapRadRekapitulasi_Bawah(),
                                    {
                                        layout: 'hBox',
                                        border: false,
                                        defaults: {margins: '0 5 0 0'},
                                        style: {'margin-left': '5px', 'margin-top': '5px'},
                                        anchor: '99%',
                                        layoutConfig:
                                                {
                                                    padding: '3',
                                                    pack: 'end',
                                                    align: 'middle'
                                                },
                                        items:
                                                [
                                                    {
                                                        xtype: 'button',
                                                        text: 'Ok',
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnOkLapRadRekapitulasi',
                                                        handler: function ()
                                                        {

                                                            var criteria = GetCriteriaRadRekapitulasi();
                                                            frmDlgRadRekapitulasi.close();
                                                            loadlaporanRadLab('0', 'LapRekapitulasiRad', criteria);
                                                        }
                                                    },
                                                    {
                                                        xtype: 'button',
                                                        text: 'Cancel',
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnCancelLapRadRekapitulasi',
                                                        handler: function ()
                                                        {
                                                            frmDlgRadRekapitulasi.close();
                                                        }
                                                    }
                                                ]
                                    }
                                ]
                    }
            );

    return PnlLapRadRekapitulasi;
}
;

function GetCriteriaRadRekapitulasi()
{
    var strKriteria = '';

    if (Ext.get('dtpTglAwalFilterHasilRad').getValue() !== '')
    {
        strKriteria = Ext.get('dtpTglAwalFilterHasilRad').getValue();
    }
    ;
    if (Ext.get('dtpTglAkhirFilterHasilRad').getValue() !== '')
    {
        strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterHasilRad').getValue();
    }
    ;

    if (Ext.get('cboPilihanRadRekapitulasi').getValue() === 'Semua')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'NULL';
    } else if (Ext.get('cboPilihanRadRekapitulasi').getValue() === 'RWJ/IGD')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'RWJ/IGD';
    } else if (Ext.get('cboPilihanRadRekapitulasi').getValue() === 'RWI')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'RWI';
    } else
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'KL';
    }

    if (Ext.get('cboPilihanRadRekapitulasikelompokPasien').getValue() === 'Semua')
    {
        strKriteria += '##@@##' + 'Semua';
        strKriteria += '##@@##' + 'NULL';
    }
    if (Ext.get('cboPilihanRadRekapitulasikelompokPasien').getValue() === 'Perseorangan')
    {
        selectSetPerseorangan = '0000000001';
        strKriteria += '##@@##' + 'Umum';
        strKriteria += '##@@##' + selectSetPerseorangan;
    }
    if (Ext.get('cboPilihanRadRekapitulasikelompokPasien').getValue() === 'Perusahaan')
    {
        if (Ext.get('cboPerusahaanRequestEntryRekapitulasiRad').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Perusahaan';
            strKriteria += '##@@##' + 'NULL';
        } else {
            strKriteria += '##@@##' + 'Perusahaan';
            strKriteria += '##@@##' + selectsetperusahaan;
        }
    }
    if (Ext.get('cboPilihanRadRekapitulasikelompokPasien').getValue() === 'Asuransi')
    {
        if (Ext.get('cboAsuransiRekapitulasiRad').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Asuransi';
            strKriteria += '##@@##' + 'NULL';
        } else {
            strKriteria += '##@@##' + 'Asuransi';
            strKriteria += '##@@##' + selectSetAsuransi;
        }

    }
    ;
    if (Ext.getCmp('Shift_All_RadRekapitulasi').getValue() === true)
    {
        strKriteria += '##@@##' + 'shift1';
        strKriteria += '##@@##' + 1;
        strKriteria += '##@@##' + 'shift2';
        strKriteria += '##@@##' + 2;
        strKriteria += '##@@##' + 'shift3';
        strKriteria += '##@@##' + 3;
    } else {
        if (Ext.getCmp('Shift_1_RadRekapitulasi').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift1';
            strKriteria += '##@@##' + 1;
        }
        if (Ext.getCmp('Shift_2_RadRekapitulasi').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift2';
            strKriteria += '##@@##' + 2;
        }
        if (Ext.getCmp('Shift_3_RadRekapitulasi').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift3';
            strKriteria += '##@@##' + 3;
        }
    }
    return strKriteria;
}
;

function ValidasiTanggalReportRadRekapitulasi()
{
    var x = 1;
    if (Ext.get('dtpTglAwalLapRadRekapitulasi').dom.value > Ext.get('dtpTglAkhirLapRadRekapitulasi').dom.value)
    {
        ShowPesanWarningRadRekapitulasiReport(nmWarningDateDlgRpt, nmTitleFormDlgReqCMRpt);
        x = 0;
    }

    return x;
}
;

function ShowPesanWarningRadRekapitulasiReport(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 300
                    }
            );
}
;

function getItemLapRadRekapitulasi_Bawah()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                xtype: 'fieldset',
                                title: 'Kriteria',
                                height : 100,
                                width: '373px',
                                layout: 'absolute',
                                
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Periode '
                                            }, {
                                                x: 110,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'datefield',
                                                id: 'dtpTglAwalFilterHasilRad',
                                                format: 'd/M/Y',
                                                value: now,
                                                height: 100,
                                            }, {
                                                 x: 235,
                                                 y: 15,
                                                xtype: 'label',
                                                text: ' s/d '
                                            }, {
                                                x: 260,
                                                y: 10,
                                                xtype: 'datefield',
                                                id: 'dtpTglAkhirFilterHasilRad',
                                                format: 'd/M/Y',
                                                value: now,
                                                width: 100
                                            },
                                            {
                                                x: 10,
                                                y: 40,
                                                xtype: 'label',
                                                text: 'Kel. Pasien '
                                            }, {
                                                x: 110,
                                                y: 40,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            mComboKelPasienRadRekapitulasi()
                                        ]
                            }
                        ]

            };
    return items;
}
;
var selectkelompokpasien;

function mComboKelPasienRadRekapitulasi()
{
    var Field = ['KODE', 'NAMA'];
    ds_param_vikelpas = new WebApp.DataStore({fields: Field});

    ds_param_vikelpas.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelompokPasien',
                                    param: ""
                                }
                    }
            );

    var cboKelPasienRequestEntryRadRekapitulasi = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboKelPasienRequestEntryRadRekapitulasi',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih KelPasien...',
                        fieldLabel: 'KelPasien ',
                        align: 'Right',
                        store: ds_param_vikelpas,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectkelompokpasien = '';
                                    }

                                }
                    }
            )

    return cboKelPasienRequestEntryRadRekapitulasi;
}
;
