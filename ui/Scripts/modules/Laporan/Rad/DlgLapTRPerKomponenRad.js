
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRadTRPerKomponen;
var selectNamaRadTRPerKomponen;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRadTRPerKomponen;
var varLapRadTRPerKomponen = ShowFormLapRadTRPerKomponen();
var selectSetUmum;
var selectSetkelpas;
var JenisLap;

function ShowFormLapRadTRPerKomponen()
{
    frmDlgRadTRPerKomponen = fnDlgRadTRPerKomponen();
    frmDlgRadTRPerKomponen.show();
}
;

function fnDlgRadTRPerKomponen()
{
    var winRadTRPerKomponenReport = new Ext.Window
            (
                    {
                        id: 'winRadTRPerKomponenReport',
                        title: 'Laporan Transaksi Per Komponen Pasien Radiologi',
                        closeAction: 'destroy',
                        width: 400,
                        height: 350,
                        border: false,
                        resizable: false,
                        constrain: true,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'icon_lapor',
                        modal: true,
                        items: [ItemDlgRadTRPerKomponen()],
                        listeners:
                                {
                                    activate: function ()
                                    {
//                                        Ext.getCmp('cboPerseoranganTRPerKomponenRad').show();
//                                        Ext.getCmp('cboAsuransiTRPerKomponenRad').hide();
//                                        Ext.getCmp('cboPerusahaanRequestEntryTRPerKomponenRad').hide();
//                                        Ext.getCmp('cboUmumTRPerKomponenRad').hide();
                                        Ext.get('dtpTglAwalFilterHasilRad').show();
                                        Ext.get('dtpTglAkhirFilterHasilRad').show();
                                        Ext.get('dtpBlnAwalFilterHasilRad').hide();
                                        Ext.get('dtpBlnAkhirFilterHasilRad').hide();
                                        selectSetPilihan = 'Semua Pasien';
                                        Ext.getCmp('cboPilihanRadTRPerKomponen').setValue('Semua Pasien');
                                        selectSetPilihankelompokPasien = 'Semua';
                                        Ext.getCmp('cboPilihanRadTRPerKomponenkelompokPasien').setValue('Semua');
                                        Ext.getCmp('cboUmumTRPerKomponenRad').setValue('Semua');
                                        Ext.getCmp('cboPerseoranganTRPerKomponenRad').hide();
                                        Ext.getCmp('cboAsuransiTRPerKomponenRad').hide();
                                        Ext.getCmp('cboPerusahaanRequestEntryTRPerKomponenRad').hide();
                                        Ext.getCmp('cboUmumTRPerKomponenRad').show();
                                        Ext.getCmp('cboUmumTRPerKomponenRad').disable();
                                        Ext.getCmp('Shift_All_RadTRPerKomponen').setValue(true);
                                        Ext.getCmp('Shift_1_RadTRPerKomponen').setValue(true);
                                        Ext.getCmp('Shift_2_RadTRPerKomponen').setValue(true);
                                        Ext.getCmp('Shift_3_RadTRPerKomponen').setValue(true);
                                    }
                                }

                    }
            );

    return winRadTRPerKomponenReport;
}
;

function ItemDlgRadTRPerKomponen()
{
    var PnlLapRadTRPerKomponen = new Ext.Panel
            (
                    {
                        id: 'PnlLapRadTRPerKomponen',
                        fileUpload: true,
                        layout: 'form',
                        height: '500',
                        anchor: '100%',
                        bodyStyle: 'padding:5px',
                        border: true,
                        items:
                                [
                                    getItemLapRadTRPerKomponen_Atas(),
                                    getItemLapRadTRPerKomponen_Bawah(),
                                    {xtype: 'tbspacer', height: 10},
                                    getItemLapRadTRPerKomponen_NewBawah(),
                                    {
                                        layout: 'hBox',
                                        border: false,
                                        defaults: {margins: '0 5 0 0'},
                                        style: {'margin-left': '30px', 'margin-top': '5px'},
                                        anchor: '94%',
                                        layoutConfig:
                                                {
                                                    padding: '3',
                                                    pack: 'end',
                                                    align: 'middle'
                                                },
                                        items:
                                                [
                                                    {
                                                        xtype: 'button',
                                                        text: 'Ok',
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnOkLapRadTRPerKomponen',
                                                        handler: function ()
                                                        {

                                                            var criteria = GetCriteriaRadTRPerKomponen();
//                                        frmDlgRadTRPerKomponen.close();
                                                            loadlaporanRadLab('0', 'LapTRPerKomponenRad', criteria);
                                                        }
                                                    },
                                                    {
                                                        xtype: 'button',
                                                        text: 'Cancel',
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnCancelLapRadTRPerKomponen',
                                                        handler: function ()
                                                        {
                                                            frmDlgRadTRPerKomponen.close();
                                                        }
                                                    }
                                                ]
                                    }
                                ]
                    }
            );

    return PnlLapRadTRPerKomponen;
}
;

function GetCriteriaRadTRPerKomponen()
{
    var strKriteria = '';

    if (Ext.get('dtpTglAwalFilterHasilRad').getValue() !== '')
    {
        strKriteria = Ext.get('dtpTglAwalFilterHasilRad').getValue();
    }
    ;
    if (Ext.get('dtpTglAkhirFilterHasilRad').getValue() !== '')
    {
        strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterHasilRad').getValue();
    }
    ;

    if (Ext.get('cboPilihanRadTRPerKomponen').getValue() === 'Semua')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'NULL';
    } else if (Ext.get('cboPilihanRadTRPerKomponen').getValue() === 'RWJ/IGD')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'RWJ/IGD';
    } else if (Ext.get('cboPilihanRadTRPerKomponen').getValue() === 'RWI')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'RWI';
    } else
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'KL';
    }

    if (Ext.get('cboPilihanRadTRPerKomponenkelompokPasien').getValue() === 'Semua')
    {
        strKriteria += '##@@##' + 'Semua';
        strKriteria += '##@@##' + 'NULL';
    }
    if (Ext.get('cboPilihanRadTRPerKomponenkelompokPasien').getValue() === 'Perseorangan')
    {
        selectSetPerseorangan = '0000000001';
        strKriteria += '##@@##' + 'Umum';
        strKriteria += '##@@##' + selectSetPerseorangan;
    }
    if (Ext.get('cboPilihanRadTRPerKomponenkelompokPasien').getValue() === 'Perusahaan')
    {
        if (Ext.get('cboPerusahaanRequestEntryTRPerKomponenRad').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Perusahaan';
            strKriteria += '##@@##' + 'NULL';
        } else {
            strKriteria += '##@@##' + 'Perusahaan';
            strKriteria += '##@@##' + selectsetperusahaan;
        }
    }
    if (Ext.get('cboPilihanRadTRPerKomponenkelompokPasien').getValue() === 'Asuransi')
    {
        if (Ext.get('cboAsuransiTRPerKomponenRad').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Asuransi';
            strKriteria += '##@@##' + 'NULL';
        } else {
            strKriteria += '##@@##' + 'Asuransi';
            strKriteria += '##@@##' + selectSetAsuransi;
        }

    }
    ;
    if (Ext.getCmp('Shift_All_RadTRPerKomponen').getValue() === true)
    {
        strKriteria += '##@@##' + 'shift1';
        strKriteria += '##@@##' + 1;
        strKriteria += '##@@##' + 'shift2';
        strKriteria += '##@@##' + 2;
        strKriteria += '##@@##' + 'shift3';
        strKriteria += '##@@##' + 3;
    } else {
        if (Ext.getCmp('Shift_1_RadTRPerKomponen').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift1';
            strKriteria += '##@@##' + 1;
        }
        if (Ext.getCmp('Shift_2_RadTRPerKomponen').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift2';
            strKriteria += '##@@##' + 2;
        }
        if (Ext.getCmp('Shift_3_RadTRPerKomponen').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift3';
            strKriteria += '##@@##' + 3;
        }
    }
    return strKriteria;
}
;

function ValidasiTanggalReportRadTRPerKomponen()
{
    var x = 1;
    if (Ext.get('dtpTglAwalLapRadTRPerKomponen').dom.value > Ext.get('dtpTglAkhirLapRadTRPerKomponen').dom.value)
    {
        ShowPesanWarningRadTRPerKomponenReport(nmWarningDateDlgRpt, nmTitleFormDlgReqCMRpt);
        x = 0;
    }

    return x;
}
;

function ShowPesanWarningRadTRPerKomponenReport(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 300
                    }
            );
}
;

function getItemLapRadTRPerKomponen_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 375,
                height: 135,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Jenis Laporan '
                    }, {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'radiogroup',
                        id: 'rgjenislaporan',
                        boxMaxWidth: 150,
                        items: [
                            {
                                boxLabel: 'Detail',
                                name: 'rgjl',
                                inputValue: 0,
                                checked: true,
                                id: 'rbDetail'
                            },
                            {
                                boxLabel: 'Summary',
                                name: 'rgjl',
                                inputValue: 1,
                                id: 'rbSummary'
                            }
                        ],
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                if (Ext.getCmp('rbDetail').checked === true)
                                {
                                    JenisLap = 'Detail';
                                } else {
                                    JenisLap = 'Summary';
                                }
                            }
                        }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Pasien '
                    }, {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboPilihanRadTRPerKomponen(),
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Kelompok pasien '
                    }, {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboPilihanRadTRPerKomponenKelompokPasien(),
                    mComboPerseoranganRadTRPerKomponen(),
                    mComboAsuransiRadTRPerKomponen(),
                    mComboPerusahaanRadTRPerKomponen(),
                    mComboUmumRadTRPerKomponen()
                ]
            }]
    };
    return items;
}
;

function getItemLapRadTRPerKomponen_Bawah()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                xtype: 'fieldset',
                                title: 'Shift',
                                autoHeight: true,
                                width: '373px',
                                defaultType: 'checkbox', // each item will be a checkbox
                                items:
                                        [
                                            {
                                                xtype: 'checkboxgroup',
                                                items: [
                                                    {boxLabel: 'Semua', name: 'Shift_All_RadTRPerKomponen', id: 'Shift_All_RadTRPerKomponen', handler: function (field, value) {
                                                            if (value === true) {
                                                                Ext.getCmp('Shift_1_RadTRPerKomponen').setValue(true);
                                                                Ext.getCmp('Shift_2_RadTRPerKomponen').setValue(true);
                                                                Ext.getCmp('Shift_3_RadTRPerKomponen').setValue(true);
                                                                Ext.getCmp('Shift_1_RadTRPerKomponen').disable();
                                                                Ext.getCmp('Shift_2_RadTRPerKomponen').disable();
                                                                Ext.getCmp('Shift_3_RadTRPerKomponen').disable();
                                                            } else {
                                                                Ext.getCmp('Shift_1_RadTRPerKomponen').setValue(false);
                                                                Ext.getCmp('Shift_2_RadTRPerKomponen').setValue(false);
                                                                Ext.getCmp('Shift_3_RadTRPerKomponen').setValue(false);
                                                                Ext.getCmp('Shift_1_RadTRPerKomponen').enable();
                                                                Ext.getCmp('Shift_2_RadTRPerKomponen').enable();
                                                                Ext.getCmp('Shift_3_RadTRPerKomponen').enable();
                                                            }
                                                        }},
                                                    {boxLabel: 'Shift 1', name: 'Shift_1_RadTRPerKomponen', id: 'Shift_1_RadTRPerKomponen'},
                                                    {boxLabel: 'Shift 2', name: 'Shift_2_RadTRPerKomponen', id: 'Shift_2_RadTRPerKomponen'},
                                                    {boxLabel: 'Shift 3', name: 'Shift_3_RadTRPerKomponen', id: 'Shift_3_RadTRPerKomponen'}
                                                ]
                                            }
                                        ]
                            }
                        ]

            };
    return items;
}
;

function getItemLapRadTRPerKomponen_NewBawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 375,
                height: 80,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Transaksi Pada '
                    }, {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'radiogroup',
                        id: 'rgTanggal',
                        boxMaxWidth: 150,
                        items: [
                            {
                                boxLabel: 'Tanggal',
                                name: 'rgTanggallap',
                                inputValue: 0,
                                checked: true,
                                id: 'rbTanggal'
                            },
                            {
                                boxLabel: 'Bulan',
                                name: 'rgTanggallap',
                                inputValue: 1,
                                id: 'rbBulan'
                            }
                        ],
                        listeners: {
                            change: function (field, newValue, oldValue) {
//                                tmpediting = 'true';
                                if (Ext.getCmp('rbTanggal').checked === true)
                                {
                                    Ext.get('dtpTglAwalFilterHasilRad').show();
                                    Ext.get('dtpTglAkhirFilterHasilRad').show();
                                    Ext.get('dtpBlnAwalFilterHasilRad').hide();
                                    Ext.get('dtpBlnAkhirFilterHasilRad').hide();
                                } else {
                                    Ext.get('dtpTglAwalFilterHasilRad').hide();
                                    Ext.get('dtpTglAkhirFilterHasilRad').hide();
                                    Ext.get('dtpBlnAwalFilterHasilRad').show();
                                    Ext.get('dtpBlnAkhirFilterHasilRad').show();
                                }
                            }
                        }
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterHasilRad',
                        format: 'd/M/Y',
                        value: now
                    }, {
                        x: 235,
                        y: 45,
                        xtype: 'label',
                        text: ' s/d '
                    }, {
                        x: 260,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterHasilRad',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpBlnAwalFilterHasilRad',
                        format: 'M/Y',
                        value: now
                    }, {
                        x: 235,
                        y: 45,
                        xtype: 'label',
                        text: ' s/d '
                    }, {
                        x: 260,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpBlnAkhirFilterHasilRad',
                        format: 'M/Y',
                        value: now,
                        width: 100
                    },
                ]
            }]
    };
    return items;
}
;

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
function mComboPilihanRadTRPerKomponen()
{
    var cboPilihanRadTRPerKomponen = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboPilihanRadTRPerKomponen',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: 'Pendaftaran Per Shift ',
                        width: 240,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua Pasien'], [2, 'Pasien Rawat Jalan'], [3, 'Pasien Rawat Inap'], [4, 'Pasien Gawat Darurat'], [5, 'Pasien Umum']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetPilihan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPilihan = b.data.displayText;
                                    }
                                }
                    }
            );
    return cboPilihanRadTRPerKomponen;
}
;

function mComboPilihanRadTRPerKomponenKelompokPasien()
{
    var cboPilihanRadTRPerKomponenkelompokPasien = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboPilihanRadTRPerKomponenkelompokPasien',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: 'Pendaftaran Per Shift ',
                        width: 240,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua'], [2, 'Perseorangan'], [3, 'Perusahaan'], [4, 'Asuransi']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetPilihankelompokPasien,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPilihankelompokPasien = b.data.displayText;
                                        Combo_Select(b.data.displayText);
                                    }
                                }
                    }
            );
    return cboPilihanRadTRPerKomponenkelompokPasien;
}
;

function mComboPerseoranganRadTRPerKomponen()
{
    var Field = ['KD_CUSTOMER', 'CUSTOMER'];
    dsPerseoranganRequestEntryTRPerKomponenRad = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntryTRPerKomponenRad.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    //Sort: 'DEPT_ID',
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=0 order by CUSTOMER"
                                }
                    }
            );
    var cboPerseoranganTRPerKomponenRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboPerseoranganTRPerKomponenRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: '',
                        width: 240,
                        store: dsPerseoranganRequestEntryTRPerKomponenRad,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetPerseorangan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPerseorangan = b.data.displayText;
                                    }
                                }
                    }
            );
    return cboPerseoranganTRPerKomponenRad;
}
;

function mComboUmumRadTRPerKomponen()
{
    var cboUmumTRPerKomponenRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboUmumTRPerKomponenRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: '',
                        width: 240,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetUmum,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetUmum = b.data.displayText;
                                    }


                                }
                    }
            );
    return cboUmumTRPerKomponenRad;
}
;

function mComboPerusahaanRadTRPerKomponen()
{
    var Field = ['KD_CUSTOMER', 'CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    //Sort: 'DEPT_ID',
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=1 order by CUSTOMER"
                                }
                    }
            );
    var cboPerusahaanRequestEntryTRPerKomponenRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboPerusahaanRequestEntryTRPerKomponenRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Perusahaan...',
                        fieldLabel: '',
                        align: 'Right',
                        store: dsPerusahaanRequestEntry,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        width: 240,
                        value: selectsetperusahaan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectsetperusahaan = b.data.KD_CUSTOMER;
                                    }
                                }
                    }
            );

    return cboPerusahaanRequestEntryTRPerKomponenRad;
}
;

function mComboAsuransiRadTRPerKomponen()
{
    var Field_poli_viDaftar = ['KD_CUSTOMER', 'CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: '',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=2 order by CUSTOMER"
                                }
                    }
            );
    var cboAsuransiTRPerKomponenRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboAsuransiTRPerKomponenRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Asuransi...',
                        fieldLabel: '',
                        align: 'Right',
                        width: 240,
                        store: ds_customer_viDaftar,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetAsuransi,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetAsuransi = b.data.KD_CUSTOMER;
                                    }
                                }
                    }
            );
    return cboAsuransiTRPerKomponenRad;
}
;

function Combo_Select(combo)
{
    var value = combo;

    if (value === "Perseorangan")
    {
        Ext.getCmp('cboPerseoranganTRPerKomponenRad').show();
        Ext.getCmp('cboAsuransiTRPerKomponenRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRPerKomponenRad').hide();
        Ext.getCmp('cboUmumTRPerKomponenRad').hide();
    } else if (value === "Perusahaan")
    {
        Ext.getCmp('cboPerseoranganTRPerKomponenRad').hide();
        Ext.getCmp('cboAsuransiTRPerKomponenRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRPerKomponenRad').show();
        Ext.getCmp('cboUmumTRPerKomponenRad').hide();
    } else if (value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganTRPerKomponenRad').hide();
        Ext.getCmp('cboAsuransiTRPerKomponenRad').show();
        Ext.getCmp('cboPerusahaanRequestEntryTRPerKomponenRad').hide();
        Ext.getCmp('cboUmumTRPerKomponenRad').hide();
    } else if (value === "Semua")
    {
        Ext.getCmp('cboPerseoranganTRPerKomponenRad').hide();
        Ext.getCmp('cboAsuransiTRPerKomponenRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRPerKomponenRad').hide();
        Ext.getCmp('cboUmumTRPerKomponenRad').show();
        Ext.getCmp('cboUmumTRPerKomponenRad').disable();
    } else
    {
        Ext.getCmp('cboPerseoranganTRPerKomponenRad').hide();
        Ext.getCmp('cboAsuransiTRPerKomponenRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRPerKomponenRad').hide();
        Ext.getCmp('cboUmumTRPerKomponenRad').show();
    }
}

function mCombounitRadTRPerKomponen()
{
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

    ds_Poli_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'NAMA_UNIT',
                                    Sortdir: 'ASC',
                                    target: 'ViewSetupUnit',
                                    param: "kd_bagian=2 and type_unit=false"
                                }
                    }
            );

    var cbounitRequestEntryRadTRPerKomponen = new Ext.form.ComboBox
            (
                    {
                        id: 'cbounitRequestEntryRadTRPerKomponen',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih unit...',
                        fieldLabel: 'unit ',
                        align: 'Right',
                        store: ds_Poli_viDaftar,
                        valueField: 'KD_UNIT',
                        displayField: 'NAMA_UNIT',
//            anchor: '95%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                    }

                                }
                    }
            )

    return cbounitRequestEntryRadTRPerKomponen;
}
;
