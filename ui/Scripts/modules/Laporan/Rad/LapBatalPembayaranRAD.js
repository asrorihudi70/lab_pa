var type_file=0;
var dsRADBatalTransaksi;
var selectRADBatalTransaksi;
var selectNamaRADBatalTransaksi;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRADBatalTransaksi;
var varLapRADBatalTransaksi= ShowFormLapRADBatalTransaksi();
var dsRAD;

function ShowFormLapRADBatalTransaksi()
{
    frmDlgRADBatalTransaksi= fnDlgRADBatalTransaksi();
    frmDlgRADBatalTransaksi.show();
};

function fnDlgRADBatalTransaksi()
{
    var winRADBatalTransaksiReport = new Ext.Window
    (
        {
            id: 'winRADBatalTransaksiReport',
            title: 'Laporan Pembatalan Pembayaran',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRADBatalTransaksi()]

        }
    );

    return winRADBatalTransaksiReport;
};

function ItemDlgRADBatalTransaksi()
{
    var PnlLapRADBatalTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapRADBatalTransaksi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRADBatalTransaksi_Periode(),
				//getItemRADBatalTransaksi_Shift(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRADBatalTransaksi',
					handler: function()
					{
					   if (ValidasiReportRADBatalTransaksi() === 1)
					   {
						
							var params={
												tglAwal:Ext.getCmp('dtpTglAwalLapRADBatalTransaksi').getValue(),
												tglAkhir:Ext.getCmp('dtpTglAkhirLapRADBatalTransaksi').getValue(),
												type_file:type_file
									} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rad/lap_radiologi/cetak_laporan_batal_pembayaran");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRADBatalTransaksi.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLapRADBatalTransaksi').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapRADBatalTransaksi').getValue()+'#aje#Laporan Batal Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionRAD/cetakRADBatalTransaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRADBatalTransaksi.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRADBatalTransaksi',
					handler: function()
					{
						frmDlgRADBatalTransaksi.close();
					}
				}
			
			]
        }
    );

    return PnlLapRADBatalTransaksi;
};


function ValidasiReportRADBatalTransaksi()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRADBatalTransaksi').dom.value === '')
	{
		ShowPesanWarningRADBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapRADBatalTransaksi').getValue() === false && Ext.getCmp('Shift_1_LapRADBatalTransaksi').getValue() === false && Ext.getCmp('Shift_2_LapRADBatalTransaksi').getValue() === false && Ext.getCmp('Shift_3_LapRADBatalTransaksi').getValue() === false){
		ShowPesanWarningRADBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportRADBatalTransaksi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRADBatalTransaksi').dom.value > Ext.get('dtpTglAkhirLapRADBatalTransaksi').dom.value)
    {
        ShowPesanWarningRADBatalTransaksiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRADBatalTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRADBatalTransaksi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRADBatalTransaksi_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRADBatalTransaksi',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					{
					   xtype: 'checkbox',
					   fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					}
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRADBatalTransaksi',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


