
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapPemakaianFilm;
var selectNamaLapPemakaianFilm;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapPemakaianFilm;
var varLapLapPemakaianFilm= ShowFormLapLapPemakaianFilm();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapPemakaianFilm;
var customer;
var kdcustomer;
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var winLapPemakaianFilmReport;

function ShowFormLapLapPemakaianFilm()
{
    frmDlgLapPemakaianFilm= fnDlgLapPemakaianFilm();
    frmDlgLapPemakaianFilm.show();
	//loadDataComboUserLapPemakaianFilm();
};

function fnDlgLapPemakaianFilm()
{
    winLapPemakaianFilmReport = new Ext.Window
    (
        {
            id: 'winLapPemakaianFilmReport',
            title: 'Laporan Pemakaian Film',
            closeAction: 'destroy',
            width: 370,
            height: 215,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapPemakaianFilm()],
            listeners:
			{
				activate: function()
				{
					/* Ext.getCmp('cboPerseoranganLapPemakaianFilm').hide();
					Ext.getCmp('cboAsuransiLapPemakaianFilm').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLapPemakaianFilm').hide();
					Ext.getCmp('cboUmumLapPemakaianFilm').show(); */
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'Ok',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapPemakaianFilm',
						handler: function()
						{
							if (ValidasiReportLapPemakaianFilm() === 1)
							{
								/* if (Ext.get('cboPilihanLapPemakaianFilmkelompokPasien').getValue() === 'Semua')
								{
									tipe='Semua';
									customer='Semua';
									kdcustomer='Semua';
								} else if (Ext.get('cboPilihanLapPemakaianFilmkelompokPasien').getValue() === 'Perseorangan'){
									customer=Ext.get('cboPerseoranganLapPemakaianFilm').getValue();
									tipe='Perseorangan';
									kdcustomer=Ext.getCmp('cboPerseoranganLapPemakaianFilm').getValue();
								} else if (Ext.get('cboPilihanLapPemakaianFilmkelompokPasien').getValue() === 'Perusahaan'){
									customer=Ext.get('cboPerusahaanRequestEntryLapPemakaianFilm').getValue();
									tipe='Perusahaan';
									kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapPemakaianFilm').getValue();
								} else {
									customer=Ext.get('cboAsuransiLapPemakaianFilm').getValue();
									tipe='Asuransi';
									kdcustomer=Ext.getCmp('cboAsuransiLapPemakaianFilm').getValue();
								}  */
								
								if(Ext.getCmp('radioasal').getValue() === true){
									periode='tanggal';
									tglAwal=Ext.getCmp('dtpTglAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
								} else{
									periode='bulan';
									tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilLab').getValue();
								}
								
								/* if (Ext.getCmp('Shift_All_LapPemakaianFilm').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1_LapPemakaianFilm').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2_LapPemakaianFilm').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3_LapPemakaianFilm').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								} */
								
								var params={
									//asal_pasien:Ext.get('cboPilihanLapPemakaianFilm').getValue(),
									//user:Ext.getCmp('cboUserRequestEntryLapPemakaianFilm').getValue(),
									//tipe:tipe,
									customer:customer,
									kdcustomer:kdcustomer,
									periode:periode,
									tglAwal:tglAwal,
									tglAkhir:tglAkhir,
									/* shift:shift,
									shift1:shift1,
									shift2:shift2,
									shift3:shift3, */
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/rad/lap_radiologi/cetakPemakaianFilm");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapPemakaianFilmReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapPemakaianFilm',
						handler: function()
						{
							winLapPemakaianFilmReport.close();
						}
					}
			]

        }
    );

    return winLapPemakaianFilmReport;
};


function ItemDlgLapPemakaianFilm()
{
    var PnlLapLapPemakaianFilm = new Ext.Panel
    (
        {
            id: 'PnlLapLapPemakaianFilm',
            fileUpload: true,
            layout: 'form',
            height: '300',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                //getItemLapLapPemakaianFilm_Atas(),
                getItemLapLapPemakaianFilm_Batas(),
                getItemLapLapPemakaianFilm_Bawah(),
                getItemLapLapPemakaianFilm_Batas(),
                //getItemLapLapPemakaianFilm_Samping(),
              
            ]
        }
    );

    return PnlLapLapPemakaianFilm;
};


function getKodeReportLapPemakaianFilm()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLapPemakaianFilm').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLapPemakaianFilm').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLapPemakaianFilm()
{
	var x=1;
	/* if(Ext.getCmp('cboPilihanLapPemakaianFilm').getValue() === ''){
		ShowPesanWarningLapPemakaianFilmReport('Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboUserRequestEntryLapPemakaianFilm').getValue() === ''){
		ShowPesanWarningLapPemakaianFilmReport('Operator Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboPilihanLapPemakaianFilmkelompokPasien').getValue() === ''){
		ShowPesanWarningLapPemakaianFilmReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapPemakaianFilm').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapPemakaianFilm').getValue() === '' &&  Ext.get('cboAsuransiLapPemakaianFilm').getValue() === '' && Ext.get('cboUmumRegisLab').getValue() === '' ){
		ShowPesanWarningLapPemakaianFilmReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	} */
   /*  if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapPemakaianFilmReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    } */
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapPemakaianFilmReport('Periode belum dipilih','Warning');
        x=0;
    }
	/* if(Ext.getCmp('Shift_All_LapPemakaianFilm').getValue() === false && Ext.getCmp('Shift_1_LapPemakaianFilm').getValue() === false && Ext.getCmp('Shift_2_LapPemakaianFilm').getValue() === false && Ext.getCmp('Shift_3_LapPemakaianFilm').getValue() === false){
		ShowPesanWarningLapPemakaianFilmReport('Shift Belum Dipilih','Warning');
        x=0;
	} */

    return x;
};

function ShowPesanWarningLapPemakaianFilmReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapPemakaianFilm_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapPemakaianFilm(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboUserLapPemakaianFilm(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapPemakaianFilmKelompokPasien(),
                mComboPerseoranganLapPemakaianFilm(),
                mComboAsuransiLapPemakaianFilm(),
                mComboPerusahaanLapPemakaianFilm(),
                mComboUmumLapPemakaianFilm()
            ]
        }]
    };
    return items;
};


function getItemLapLapPemakaianFilm_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapPemakaianFilm_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  345,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
				
					{
						x: 40,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapPemakaianFilm',
						id : 'Shift_All_LapPemakaianFilm',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapPemakaianFilm').setValue(true);
								Ext.getCmp('Shift_2_LapPemakaianFilm').setValue(true);
								Ext.getCmp('Shift_3_LapPemakaianFilm').setValue(true);
								Ext.getCmp('Shift_1_LapPemakaianFilm').disable();
								Ext.getCmp('Shift_2_LapPemakaianFilm').disable();
								Ext.getCmp('Shift_3_LapPemakaianFilm').disable();
							}else{
								Ext.getCmp('Shift_1_LapPemakaianFilm').setValue(false);
								Ext.getCmp('Shift_2_LapPemakaianFilm').setValue(false);
								Ext.getCmp('Shift_3_LapPemakaianFilm').setValue(false);
								Ext.getCmp('Shift_1_LapPemakaianFilm').enable();
								Ext.getCmp('Shift_2_LapPemakaianFilm').enable();
								Ext.getCmp('Shift_3_LapPemakaianFilm').enable();
							}
						}
					},
					{
						x: 110,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapPemakaianFilm',
						id : 'Shift_1_LapPemakaianFilm'
					},
					{
						x: 180,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapPemakaianFilm',
						id : 'Shift_2_LapPemakaianFilm'
					},
					{
						x: 250,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapPemakaianFilm',
						id : 'Shift_3_LapPemakaianFilm'
					}
				]
			}
        ]
            
    };
    return items;
};

function getItemLapLapPemakaianFilm_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilLab',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilLab',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilLab',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function loadDataComboUserLapPemakaianFilm(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/lab/lap_laboratorium/getUser",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUserRequestEntryLapPemakaianFilm.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_User_LapPemakaianFilm.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_User_LapPemakaianFilm.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboPilihanLapPemakaianFilm()
{
    var cboPilihanLapPemakaianFilm = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapPemakaianFilm',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ'],[3, 'RWI'], [4, 'IGD']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapPemakaianFilm;
};

function mComboPilihanLapPemakaianFilmKelompokPasien()
{
    var cboPilihanLapPemakaianFilmkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanLapPemakaianFilmkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapPemakaianFilmkelompokPasien;
};

function mComboPerseoranganLapPemakaianFilm()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapPemakaianFilm = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapPemakaianFilm.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapPemakaianFilm = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganLapPemakaianFilm',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapPemakaianFilm,
				/* new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ), */
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLapPemakaianFilm;
};

function mComboUmumLapPemakaianFilm()
{
    var cboUmumLapPemakaianFilm = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumLapPemakaianFilm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumLapPemakaianFilm;
};

function mComboPerusahaanLapPemakaianFilm()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapPemakaianFilm = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryLapPemakaianFilm',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapPemakaianFilm;
};

function mComboAsuransiLapPemakaianFilm()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomerLab',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapPemakaianFilm = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiLapPemakaianFilm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapPemakaianFilm;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapPemakaianFilm').show();
        Ext.getCmp('cboAsuransiLapPemakaianFilm').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapPemakaianFilm').hide();
        Ext.getCmp('cboUmumLapPemakaianFilm').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapPemakaianFilm').hide();
        Ext.getCmp('cboAsuransiLapPemakaianFilm').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapPemakaianFilm').show();
        Ext.getCmp('cboUmumLapPemakaianFilm').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapPemakaianFilm').hide();
        Ext.getCmp('cboAsuransiLapPemakaianFilm').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapPemakaianFilm').hide();
        Ext.getCmp('cboUmumLapPemakaianFilm').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapPemakaianFilm').hide();
        Ext.getCmp('cboAsuransiLapPemakaianFilm').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapPemakaianFilm').hide();
        Ext.getCmp('cboUmumLapPemakaianFilm').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapPemakaianFilm').hide();
        Ext.getCmp('cboAsuransiLapPemakaianFilm').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapPemakaianFilm').hide();
        Ext.getCmp('cboUmumLapPemakaianFilm').show();
   }
}

function mComboUserLapPemakaianFilm()
{
    var Field = ['kd_user','full_name'];
    ds_User_LapPemakaianFilm = new WebApp.DataStore({fields: Field});
    cboUserRequestEntryLapPemakaianFilm = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cboUserRequestEntryLapPemakaianFilm',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:'Semua',
            store: ds_User_LapPemakaianFilm,
            valueField: 'kd_user',
            displayField: 'full_name',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.kd_user;
				} 
			}
        }
    )

    return cboUserRequestEntryLapPemakaianFilm;
};
