var type_file=0;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRADRekapJasaPelayananDokter;
var selectNamaRADRekapJasaPelayananDokter;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRADRekapJasaPelayananDokter;
var varLapRADRekapJasaPelayananDokter= ShowFormLapRADRekapJasaPelayananDokter();
var selectSetUmum;
var selectSetkelpas;
var fields = [
	{name: 'KD_UNIT', mapping : 'KD_UNIT'},
	{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];
var colsCustomer = [
	{ id : 'kd_customer', header: "Kode Customer", width: 160, sortable: true, dataIndex: 'kd_customer',hidden : true},
	{ header: "Customer", width: 50, sortable: true, dataIndex: 'customer'}
];

var tmpnama_unit;
var selectSetPilihankelompokPasien;
var selectSetPilihanProfesi;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;
var dsGridCustomerRAD;
var secondGridStoreLapCustomerRAD;
var kelpas=-1;
function ShowFormLapRADRekapJasaPelayananDokter()
{
    frmDlgRADRekapJasaPelayananDokter= fnDlgRADRekapJasaPelayananDokter();
    frmDlgRADRekapJasaPelayananDokter.show();
};

function fnDlgRADRekapJasaPelayananDokter()
{
    var winRADRekapJasaPelayananDokterReport = new Ext.Window
    (
        {
            id: 'winRADRekapJasaPelayananDokterReport',
            title: 'Laporan Transaksi Jasa Pelayanan Dokter',
            closeAction: 'destroy',
            width:400,
            height: 460,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRADRekapJasaPelayananDokter()],
            listeners:
        {
            activate: function()
            {
                /* Ext.getCmp('cboPerseoranganRAD').show();
                Ext.getCmp('cboAsuransiRAD').hide();
                Ext.getCmp('cboPerusahaanRequestEntryRAD').hide();
                Ext.getCmp('cboUmumRAD').hide(); */
            }
        }

        }
    );

    return winRADRekapJasaPelayananDokterReport;
};


function ItemDlgRADRekapJasaPelayananDokter()
{
    var PnlLapRADRekapJasaPelayananDokter = new Ext.Panel
    (
        {
            id: 'PnlLapRADRekapJasaPelayananDokter',
            fileUpload: true,
            layout: 'form',
			//width:400,
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapRADRekapJasaPelayananDokter_Atas(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRADRekapJasaPelayananDokter',
                            handler: function()
                            {
								var sendDataCustomer    	= "";
								var sendDataArrayCustomer = []; 
								secondGridStoreLapCustomerRAD.each(function(record){
									var recordArray   = [record.get("kd_customer")];
									sendDataArrayCustomer.push(recordArray);
									sendDataCustomer 	+= "'"+recordArray+"',";
								});
								if (Ext.getCmp('Shift_All').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
                            	//if (ValidasiReportRADPasienPerDokter() === 1)
							   	//{								
									var params={
						//id : 'cbPendaftaran_RADRekapJasaPelayananDokter'
						//id : 'cbTindakRAD_RADRekapJasaPelayananDokter'
						//$('.messageCheckbox:checked').val();
										shift:shift,
										shift1:shift1,
										shift2:shift2,
										shift3:shift3,
                                        kd_profesi:Ext.getCmp('IDcboPilihanRADJenisProfesi').getValue(),
										kd_poli:selectSetUnit,
										pelayananPendaftaran:Ext.getCmp('cbPendaftaran_RADRekapJasaPelayananDokter').getValue(),
										pelayananTindak:Ext.getCmp('cbTindakRAD_RADRekapJasaPelayananDokter').getValue(),
                                        //kd_dokter:Ext.getCmp('cboDokterRADRekapJasaPelayananDokter').getValue(),
										kd_dokter:GetCriteriaRADProfesi(),
										//kd_kelompok:GetCriteriaRADPasienPerKelompok(),
										tmp_kd_customer: sendDataCustomer,
										tglAwal:Ext.getCmp('dtpTglAwalLapRADRekapJasaPelayananDokter').getValue(),
										tglAkhir:Ext.getCmp('dtpTglAkhirLapRADRekapJasaPelayananDokter').getValue(),
										pasien : Ext.getCmp('cbounitasalpasienRequestEntryRADRekapJasaPelayananDokter').getValue(),
										type_file:type_file
									} 
									//console.log(params);
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/rad/lap_RADRekapJasaPelayananDokter/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();		
									frmDlgRADPasienPerKelompok.close(); 
									
							   //};
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRADRekapJasaPelayananDokter',
                            handler: function()
                            {
                                    frmDlgRADRekapJasaPelayananDokter.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRADRekapJasaPelayananDokter;
};
  function firstGridCustomerRAD(){
		
		var dataSource_customer;
		var Field_poli_viDaftar = ['kd_customer','customer'];
		
		dsGridCustomerRAD         = new WebApp.DataStore({fields: Field_poli_viDaftar});
		loadcustomerRAD(kelpas);
		
        firstGrid_customer = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dsGridCustomerRAD,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 120,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Customer',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'kd_customer',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Customer',
                                                    dataIndex: 'customer',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
			listeners : {
				afterrender : function(comp) {
					var secondGridDropTargetEl = firstGrid_customer.getView().scroller.dom;
					var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroup',
							notifyDrop : function(ddSource, e, data){
									var records =  ddSource.dragData.selections;
									Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
									firstGrid_customer.store.add(records);
									firstGrid_customer.store.sort('kd_customer', 'ASC');
									return true
							}
					});
				}
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid_customer;
	}
	function seconGridCustomerRAD(){
		
		var secondGridCustRAD;
		secondGridStoreLapCustomerRAD = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGridCustRAD = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroup',
					store            : secondGridStoreLapCustomerRAD,
					columns          : colsCustomer,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 120,
					stripeRows       : true,
					autoExpandColumn : 'kd_customer',
					title            : 'Customer yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGridCustRAD.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroup',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGridCustRAD.store.add(records);
											secondGridCustRAD.store.sort('kd_customer', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGridCustRAD;
	}
	function loadcustomerRAD(param){
		Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/function_laporan_PTP/getCustomerLaporanPenunjang",
				params: {kelpas :param},
				failure: function(o){
					var cst = Ext.decode(o.responseText);
				},	    
				success: function(o) {
					firstGrid_customer.store.removeAll();
					var cst = Ext.decode(o.responseText);

					for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
						var recs    = [],recType =  dsGridCustomerRAD.recordType;
						var o=cst['listData'][i];
				
						recs.push(new recType(o));
						dsGridCustomerRAD.add(recs);
						console.log(o);
					}
				}
			});
	}
function getItemLapRADRekapJasaPelayananDokter_Atas()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 380,
            anchor: '100% 100%',
            items: [
			{
				xtype: 'checkboxgroup',
				width:210,
				x: 120,
				hidden:true,
				y: 10,
				items: 
				[
					{
						
						boxLabel: 'Pendaftaran',
						name: 'cbPendaftaran_RADRekapJasaPelayananDokter',
						id : 'cbPendaftaran_RADRekapJasaPelayananDokter'
					},
					{
						boxLabel: 'Tindak RAD',
						name: 'cbTindakRAD_RADRekapJasaPelayananDokter',
						id : 'cbTindakRAD_RADRekapJasaPelayananDokter'
					}
			   ]
			},{
				x: 10,
				y: 10,
				xtype: 'label',
				text: 'Unit Asal Pasien '
			}, {
				x: 110,
				y: 10,
				xtype: 'label',
				text: ' : '
			},
				mComboUnitAsalPasienRADRekapJasaPelayananDokter(),
            //  ================================================================================== POLIKLINIK
			{
				x: 10,
				y: 40,
				xtype: 'label',
				text: 'Poliklinik '
			}, {
				x: 110,
				y: 40,
				xtype: 'label',
				text: ' : '
			},
				mCombounitRADRekapJasaPelayananDokter(),
            //  ================================================================================== DOKTER
			{
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Bagian '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
                mComboRADJenisProfesi(),
				mComboDokterRADRekapJasaPelayananDokter(),
                mComboDokterRADRekapJasaPelayananPerawat(),

			{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, {
				x: 110,
				y: 130,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAwalLapRADRekapJasaPelayananDokter',
				format: 'd/M/Y',
                value: now
				//value: tigaharilalu
			}, {
				x: 230,
				y: 130,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 260,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapRADRekapJasaPelayananDokter',
				format: 'd/M/Y',
				value: now,
				width: 100
			},
			{
				// xtype : 'fieldset',
				// title : 'Kelompok pasien',
				x: 10,
				y: 160,
				layout: 'column',
				border: false,
				bodyStyle:'margin-top: 2px',
				//height: 60,
				items:[
						{
							xtype : 'fieldset',
							title : 'Kelompok pasien',
							layout: 'column',
							border: true,
							bodyStyle:'padding: 2px 0px  7px 15px',
							width:370,
							height: 170,
							items:[
							 {
									
									layout: 'column',
									border: false,
									width:340,
									items:[
										//grid customer
											{
												border:false,
												columnWidth:.47,
												//width :170,
												bodyStyle:'margin-top: 2px',
												items:[
													firstGridCustomerRAD(),
												]
											},
											{
												border:false,
												columnWidth :.03,
												bodyStyle:'margin-top: 2px',
											},
											{
												border:false,
												columnWidth:.47,
												//width :170,
												bodyStyle:'margin-top: 2px; align:right;',
												items:[
													seconGridCustomerRAD(),
												]
											} 
									]
								} 
							]
						} 
				]
			}, 
			/* {
				x: 10,
				y: 160,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 160,
				xtype: 'label',
				text: ' : '
			},
				mComboRADPasienPerKelompok(),
				mComboRADPasienPerKelompokSEMUA(),
				mComboRADPasienPerKelompokPERORANGAN(),
				mComboRADPasienPerKelompokPERUSAHAAN(),
				mComboRADPasienPerKelompokASURANSI(), */
			{
				x: 10,
				y: 320,
				xtype: 'label',
				text: 'Type File '
			}, {
				x: 110,
				y: 320,
				xtype: 'label',
				text: ' : '
			},
			{
				x: 120,
				y: 320,
			   xtype: 'checkbox',
			   id: 'CekLapPilihTypeExcel',
			   hideLabel:false,
			   boxLabel: 'Excel',
			   checked: false,
			   listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
						{
							type_file=1;
						}
						else
						{
							type_file=0;
						}
					}
			   }
			},
			{
				x: 10,
				y: 355,
				xtype: 'checkboxgroup',
				fieldLabel: 'Shift',
				columns: 5,
				vertical: false,
				items: [
					{
					boxLabel: 'Semua',checked:true,name: 'Shift_All',id : 'Shift_All',
					handler: function (field, value) 
					{
						if (value === true){
						Ext.getCmp('Shift_1').setValue(true);
						Ext.getCmp('Shift_2').setValue(true);
						Ext.getCmp('Shift_3').setValue(true);
						Ext.getCmp('Shift_1').disable();
						Ext.getCmp('Shift_2').disable();
						Ext.getCmp('Shift_3').disable();
					}else{
						Ext.getCmp('Shift_1').setValue(false);
						Ext.getCmp('Shift_2').setValue(false);
						Ext.getCmp('Shift_3').setValue(false);
						Ext.getCmp('Shift_1').enable();
						Ext.getCmp('Shift_2').enable();
						Ext.getCmp('Shift_3').enable();
					}
				}
				},
						{boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1',id : 'Shift_1'},
						{boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2',id : 'Shift_2'},
						{boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3',id : 'Shift_3'}
					]
			}
            ]
        }]
    };
    return items;
};

function ShowPesanWarningRADRekapJasaPelayananDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getComboDokterRad(kdUnit)
{
	dsDokterPelayaranDokter.load
	({
		params:
		{
			Skip: 0,
			Take: 1000,
			Sort: 'kd_dokter',
			Sortdir: 'ASC',
			target: 'ViewDokterPenunjang',
			param: "kd_unit = '"+kdUnit+"'"
		}
	});
	return dsDokterPelayaranDokter;
}
function mComboDokterRADRekapJasaPelayananDokter()
{
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    
    var cboPilihanRADRekapJasaPelayananDokter = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboDokterRADRekapJasaPelayananDokter',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanRADRekapJasaPelayananDokter;
};

function mComboDokterRADRekapJasaPelayananPerawat()
{
	var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokterLaporan',
				param: "WHERE dokter.jenis_dokter='1'"
			}
		}
	);
    var cboPilihanRADRekapJasaPelayananDokter = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboDokterRADRekapJasaPelayananPerawat',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:true,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanRADRekapJasaPelayananDokter;
};

function getUnitDefault(){
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRAD/getUnitDefault",
		params: {text:''},
		failure: function (o){
			loadMask.hide();
			ShowPesanErrorPenJasRad('Gagal mendapatkan data Unit default', 'Radiologi');
		},
		success: function (o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				selectSetUnit=cst.kd_unit;
				tmpnama_unit=cst.nama_unit;
				getComboDokterRad(cst.kd_unit);
				Ext.getCmp('cbounitRequestEntryRADRekapJasaPelayananDokter').setValue(tmpnama_unit);
			} else{
				ShowPesanErrorPenJasRad('Gagal mendapatkan data Unit default', 'Radiologi');
			}
		}
	});
}
function mComboUnitAsalPasienRADRekapJasaPelayananDokter()
{
    
    var cbounitasalpasienRequestEntryRADRekapJasaPelayananDokter = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 10,
            id: 'cbounitasalpasienRequestEntryRADRekapJasaPelayananDokter',
			width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: new Ext.data.ArrayStore({
					id: 0,
					fields:[
							'Id',
							'displayText'
					],
					data: [[0, 'Semua'],[1, 'RWJ'],[2, 'RWI'], [3, 'IGD'],[4, 'Kunjungan Langsung']]
				}),
			valueField: 'Id',
			displayField: 'displayText',
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						//selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitasalpasienRequestEntryRADRekapJasaPelayananDokter;
};
function mCombounitRADRekapJasaPelayananDokter()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});
	getUnitDefault();
	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewCombounit_Konfigurasi',
                    param: " "
                }
            }
        );

    var cbounitRequestEntryRADRekapJasaPelayananDokter = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 40,
            id: 'cbounitRequestEntryRADRekapJasaPelayananDokter',
			width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryRADRekapJasaPelayananDokter;
};


function mComboRADPasienPerKelompok()
{
    var cboPilihanRADRekapJasaPelayananDokterkelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 160,
                id:'cboPilihanRADPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectRADPasienPerKelompok(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanRADRekapJasaPelayananDokterkelompokPasien;
};

function mComboRADJenisProfesi()
{
    var cboPilihanRADJenisProfesi = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'IDcboPilihanRADJenisProfesi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Dokter'], [2, 'Perawat']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihanProfesi=b.data.displayText;
                            Combo_SelectRADProfesi(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanRADJenisProfesi;
};

//RADPasienPerKelompok
function mComboRADPasienPerKelompokSEMUA()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRAD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboRADPasienPerKelompokSEMUA',
                typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				selectOnFocus:true,
				forceSelection: true,
				emptyText:'Silahkan Pilih...',
				valueField: 'Id',
	            displayField: 'displayText',
	            hidden:false,
				fieldLabel: '',
				width: 240,
				value:1,
				store: new Ext.data.ArrayStore
				(
						{
							id: 0,
							fields:
								[
										'Id',
										'displayText'
								],
							data: [[1, 'Semua']]
						}
				),
				listeners:
				{
					'select': function(a,b,c)
					{
						selectSetUmum=b.data.displayText ;
					}
	                                
	                            
				}
            }
	);
	return cboPerseoranganRAD;
};

function mComboRADPasienPerKelompokPERORANGAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRAD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboRADPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRAD;
};

function mComboRADPasienPerKelompokPERUSAHAAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='1' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRAD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboRADPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
				//value: selectsetperusahaan,
                listeners:
                {
				    'select': function(a,b,c)
					{
				        selectsetperusahaan = b.data.KD_CUSTOMER;
						selectsetnamaperusahaan = b.data.CUSTOMER;
					}
                }
            }
	);
	return cboPerseoranganRAD;
};

function mComboRADPasienPerKelompokASURANSI()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='2' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRAD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboRADPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
				valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
				    'select': function(a,b,c)
					{
						selectSetAsuransi=b.data.KD_CUSTOMER ;
						selectsetnamaAsuransi = b.data.CUSTOMER ;
					}
                }
            }
	);
	return cboPerseoranganRAD;
};


function Combo_SelectRADPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboRADPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokSEMUA').show();
   }
};

function Combo_SelectRADProfesi(combo)
{
   var value = combo;

   if(value === "Dokter")
   {    
        Ext.getCmp('cboDokterRADRekapJasaPelayananDokter').show();
        Ext.getCmp('cboDokterRADRekapJasaPelayananPerawat').hide();
   }
   else if(value === "Perawat")
   {    
        Ext.getCmp('cboDokterRADRekapJasaPelayananPerawat').show();
        Ext.getCmp('cboDokterRADRekapJasaPelayananDokter').hide();
        
   }
   else
   {
        Ext.getCmp('cboDokterRADRekapJasaPelayananDokter').show();
        Ext.getCmp('cboDokterRADRekapJasaPelayananPerawat').hide();
   }
};


function GetCriteriaRADPasienPerKelompok()
{
    var strKriteria = '';
    
    if (Ext.getCmp('cboPilihanRADPasienPerKelompok').getValue() !== '')
    {
        if (Ext.get('cboPilihanRADPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
        else if (Ext.get('cboPilihanRADPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').getValue(); } 
        else if (Ext.get('cboPilihanRADPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').getValue(); } 
        else if (Ext.get('cboPilihanRADPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').getValue(); }
    }else{
            strKriteria = 'Semua';
    }

    
    return strKriteria;
};

function GetCriteriaRADProfesi()
{
	var strKriteria = '';
	
	if (Ext.getCmp('IDcboPilihanRADJenisProfesi').getValue() !== '')
	{
		if (Ext.get('IDcboPilihanRADJenisProfesi').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
		else if (Ext.get('IDcboPilihanRADJenisProfesi').getValue() === 'Dokter'){ strKriteria = Ext.getCmp('cboDokterRADRekapJasaPelayananDokter').getValue(); } 
		else if (Ext.get('IDcboPilihanRADJenisProfesi').getValue() === 'Perawat'){ strKriteria = Ext.getCmp('cboDokterRADRekapJasaPelayananPerawat').getValue(); } 
	}else{
        strKriteria = 'Semua';
	}

	
	return strKriteria;
};

function ValidasiReportRADPasienPerDokter()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRADPasienPerDokter').dom.value === '')
	{
		ShowPesanWarningRADPasienPerDokterReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
    return x;
};
