
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRadRegis;
var selectNamaRadRegis;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRadRegis;
var varLapRadRegis= ShowFormLapRadRegis();
var selectSetUmum;
var selectSetkelpas;

function ShowFormLapRadRegis()
{
    frmDlgRadRegis= fnDlgRadRegis();
    frmDlgRadRegis.show();
};

function fnDlgRadRegis()
{
    var winRadRegisReport = new Ext.Window
    (
        {
            id: 'winRadRegisReport',
            title: 'Laporan Regis Pasien Radiologi',
            closeAction: 'destroy',
            width:400,
            height: 280,
            border: false,
            resizable:false,
            constrain: true,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRadRegis()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganRegisRad').show();
                Ext.getCmp('cboAsuransiRegisRad').hide();
                Ext.getCmp('cboPerusahaanRequestEntryRegisRad').hide();
                Ext.getCmp('cboUmumRegisRad').hide();
            }
        }

        }
    );

    return winRadRegisReport;
};


function ItemDlgRadRegis()
{
    var PnlLapRadRegis = new Ext.Panel
    (
        {
            id: 'PnlLapRadRegis',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapRadRegis_Atas(),
                getItemLapRadRegis_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRadRegis',
                            handler: function()
                            {
                                
                                        var criteria = GetCriteriaRadRegis();
//                                        frmDlgRadRegis.close();
                                        loadlaporanRadLab('0', 'LapRegisRad', criteria);
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRadRegis',
                            handler: function()
                            {
                                    frmDlgRadRegis.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRadRegis;
};

function GetCriteriaRadRegis()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalFilterHasilRad').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalFilterHasilRad').getValue();
	};
        if (Ext.get('dtpTglAkhirFilterHasilRad').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterHasilRad').getValue();
	};
        
        if (Ext.get('cboPilihanRadRegis').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Unit';
            strKriteria += '##@@##' + 'NULL';
        }else if(Ext.get('cboPilihanRadRegis').getValue() === 'RWJ/IGD')
        {
            strKriteria += '##@@##' + 'Unit';
            strKriteria += '##@@##' + 'RWJ/IGD';
        }else if(Ext.get('cboPilihanRadRegis').getValue() === 'RWI')
        {   
            strKriteria += '##@@##' + 'Unit';
            strKriteria += '##@@##' + 'RWI';
        }else
        {
            strKriteria += '##@@##' + 'Unit';
            strKriteria += '##@@##' + 'KL';
        }
        
        if (Ext.get('cboPilihanRadRegiskelompokPasien').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
        }
        if (Ext.get('cboPilihanRadRegiskelompokPasien').getValue() === 'Perseorangan')
        {
            selectSetPerseorangan = '0000000001';
            strKriteria += '##@@##' + 'Umum';
            strKriteria += '##@@##' + selectSetPerseorangan;
        }
        if (Ext.get('cboPilihanRadRegiskelompokPasien').getValue() === 'Perusahaan')
        {
            if(Ext.get('cboPerusahaanRequestEntryRegisRad').getValue() === 'Semua')
            {
                strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + selectsetperusahaan;
            }
        }
        if (Ext.get('cboPilihanRadRegiskelompokPasien').getValue() === 'Asuransi')
        {
            if(Ext.get('cboAsuransiRegisRad').getValue() === 'Semua')
            {
                strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + selectSetAsuransi;
            }
            
        };
        if (Ext.getCmp('Shift_All_RadRegis').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift1';
            strKriteria += '##@@##' + 1;
            strKriteria += '##@@##' + 'shift2';
            strKriteria += '##@@##' + 2;
            strKriteria += '##@@##' + 'shift3';
            strKriteria += '##@@##' + 3;
        }else{
            if (Ext.getCmp('Shift_1_RadRegis').getValue() === true)
            {
                strKriteria += '##@@##' + 'shift1';
                strKriteria += '##@@##' + 1;
            }
            if (Ext.getCmp('Shift_2_RadRegis').getValue() === true)
            {
                strKriteria += '##@@##' + 'shift2';
                strKriteria += '##@@##' + 2;
            }
            if (Ext.getCmp('Shift_3_RadRegis').getValue() === true)
            {
                strKriteria += '##@@##' + 'shift3';
                strKriteria += '##@@##' + 3;
            }
        }
	return strKriteria;
};

function ValidasiTanggalReportRadRegis()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRadRegis').dom.value > Ext.get('dtpTglAkhirLapRadRegis').dom.value)
    {
        ShowPesanWarningRadRegisReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRadRegisReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapRadRegis_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanRadRegis(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilRad',
                format: 'd/M/Y',
                value: now
            }, {
                x: 230,
                y: 40,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilRad',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanRadRegisKelompokPasien(),
                mComboPerseoranganRadRegis(),
                mComboAsuransiRadRegis(),
                mComboPerusahaanRadRegis(),
                mComboUmumRadRegis()
            ]
        }]
    };
    return items;
};

function getItemLapRadRegis_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {boxLabel: 'Semua',name: 'Shift_All_RadRegis',id : 'Shift_All_RadRegis',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_RadRegis').setValue(true);Ext.getCmp('Shift_2_RadRegis').setValue(true);Ext.getCmp('Shift_3_RadRegis').setValue(true);Ext.getCmp('Shift_1_RadRegis').disable();Ext.getCmp('Shift_2_RadRegis').disable();Ext.getCmp('Shift_3_RadRegis').disable();}else{Ext.getCmp('Shift_1_RadRegis').setValue(false);Ext.getCmp('Shift_2_RadRegis').setValue(false);Ext.getCmp('Shift_3_RadRegis').setValue(false);Ext.getCmp('Shift_1_RadRegis').enable();Ext.getCmp('Shift_2_RadRegis').enable();Ext.getCmp('Shift_3_RadRegis').enable();}}},
                                    {boxLabel: 'Shift 1',name: 'Shift_1_RadRegis',id : 'Shift_1_RadRegis'},
                                    {boxLabel: 'Shift 2',name: 'Shift_2_RadRegis',id : 'Shift_2_RadRegis'},
                                    {boxLabel: 'Shift 3',name: 'Shift_3_RadRegis',id : 'Shift_3_RadRegis'}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
function mComboPilihanRadRegis()
{
    var cboPilihanRadRegis = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanRadRegis',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPilihan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanRadRegis;
};

function mComboPilihanRadRegisKelompokPasien()
{
    var cboPilihanRadRegiskelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanRadRegiskelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPilihankelompokPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanRadRegiskelompokPasien;
};

function mComboPerseoranganRadRegis()
{
    var cboPerseoranganRegisRad = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganRegisRad',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRegisRad;
};

function mComboUmumRadRegis()
{
    var cboUmumRegisRad = new Ext.form.ComboBox
	(
		{
                        x: 120,
                        y: 100,
			id:'cboUmumRegisRad',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetUmum,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumRegisRad;
};

function mComboPerusahaanRadRegis()
{
    var Field = ['kd_customer','customer'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: '',
			    Sortdir: 'ASC',
			    target:'ViewLookUpCustomer',
                            param: "1"
			}
		}
	);
    var cboPerusahaanRequestEntryRegisRad = new Ext.form.ComboBox
	(
		{
                    x: 120,
                    y: 100,
		    id: 'cboPerusahaanRequestEntryRegisRad',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'kd_customer',
		    displayField: 'customer',
                    width:240,
                    value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.kd_customer;
                                }
			}
                }
	);

    return cboPerusahaanRequestEntryRegisRad;
};

function mComboAsuransiRadRegis()
{
var Field_poli_viDaftar = ['kd_customer','customer'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewLookUpCustomer',
                param: "2"
            }
        }
    );
    var cboAsuransiRegisRad = new Ext.form.ComboBox
	(
		{
                    x: 120,
                    y: 100,
			id:'cboAsuransiRegisRad',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Asuransi...',
                        fieldLabel: '',
                        align: 'Right',
                        width:240,
			store: ds_customer_viDaftar,
			valueField: 'kd_customer',
                        displayField: 'customer',
                        value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.kd_customer ;
				}
			}
		}
	);
	return cboAsuransiRegisRad;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRegisRad').show();
        Ext.getCmp('cboAsuransiRegisRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisRad').hide();
        Ext.getCmp('cboUmumRegisRad').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRegisRad').hide();
        Ext.getCmp('cboAsuransiRegisRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisRad').show();
        Ext.getCmp('cboUmumRegisRad').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRegisRad').hide();
        Ext.getCmp('cboAsuransiRegisRad').show();
        Ext.getCmp('cboPerusahaanRequestEntryRegisRad').hide();
        Ext.getCmp('cboUmumRegisRad').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRegisRad').hide();
        Ext.getCmp('cboAsuransiRegisRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisRad').hide();
        Ext.getCmp('cboUmumRegisRad').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRegisRad').hide();
        Ext.getCmp('cboAsuransiRegisRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisRad').hide();
        Ext.getCmp('cboUmumRegisRad').show();
   }
}

function mCombounitRadRegis()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryRadRegis = new Ext.form.ComboBox
    (
        {
            id: 'cbounitRequestEntryRadRegis',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                }
                    
                }
        }
    )

    return cbounitRequestEntryRadRegis;
};
