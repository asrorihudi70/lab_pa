
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsTRRad;
var selectNamaTRRad;
var now = new Date();
var selectSetPerseorangan;
var frmDlgTRRad;
var varLapTRRad= ShowFormLapTRRad();
var selectSetUmum;
var selectSetkelpas;

function ShowFormLapTRRad()
{
    frmDlgTRRad= fnDlgTRRad();
    frmDlgTRRad.show();
};

function fnDlgTRRad()
{
    var winTRRadReport = new Ext.Window
    (
        {
            id: 'winTRRadReport',
            title: 'Laporan Transaksi Radiologi',
            closeAction: 'destroy',
            constrain: true,
            width: 370,
            height: 500,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgTRRad()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganTRRad').show();
                Ext.getCmp('cboAsuransiTRRad').hide();
                Ext.getCmp('cboPerusahaanRequestEntryTRRad').hide();
                Ext.getCmp('cboUmumTRRad').hide();
                Ext.getCmp('radioasal').setValue(true);
            }
        }

        }
    );

    return winTRRadReport;
};


function ItemDlgTRRad()
{
    var PnlLapTRRad = new Ext.Panel
    (
        {
            id: 'PnlLapTRRad',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapTRRad_Atas(),
                getItemLapTRRad_Batas(),
                getItemLapTRRad_Bawah(),
                getItemLapTRRad_Batas(),
                getItemLapTRRad_Samping(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapTRRad',
                            handler: function()
                            {
//                                if (ValidasiReportTRRad() === 1)
//                                {
                                        var criteria = GetCriteriaTRRad();
//                                        frmDlgTRRad.close();
                                        loadlaporanRadLab('0', 'LapTRRAD', criteria);
//                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapTRRad',
                            handler: function()
                            {
                                    frmDlgTRRad.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapTRRad;
};

function GetCriteriaTRRad()
{
	var strKriteria = '';
//        alert(valuerbTRRad);
        if (valuerbTRRad === 'Tanggal')
        {            
           strKriteria = Ext.get('dtpTglAwalFilterTRRad').getValue();
           strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterTRRad').getValue();
           
        }else
        {
           strKriteria = Ext.get('dtpBulanAwalFilterTRRad').getValue();
           strKriteria += '##@@##' + Ext.get('dtpBulanAkhirFilterTRRad').getValue();
        }
        if ( Ext.get('cboUserRequestEntryTRRad').getValue() !== '')
        {
            if(Ext.get('cboUserRequestEntryTRRad').getValue() !== 'Semua')
            {
                strKriteria += '##@@##' + 'User';
                strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryTRRad').getValue();
                strKriteria += '##@@##' + Ext.get('cboUserRequestEntryTRRad').getValue();
            }else
            {
                strKriteria += '##@@##' + 'User';
                strKriteria += '##@@##' + 'NULL';
                strKriteria += '##@@##' + Ext.get('cboUserRequestEntryTRRad').getValue();
            }
        }
        if (Ext.get('cboPilihanTRRad').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Unit';
            strKriteria += '##@@##' + 'NULL';
        }else if(Ext.get('cboPilihanTRRad').getValue() === 'RWJ/IGD')
        {
            strKriteria += '##@@##' + 'Unit';
            strKriteria += '##@@##' + 'RWJ/IGD';
        }else if(Ext.get('cboPilihanTRRad').getValue() === 'RWI')
        {   
            strKriteria += '##@@##' + 'Unit';
            strKriteria += '##@@##' + 'RWI';
        }else
        {
            strKriteria += '##@@##' + 'Unit';
            strKriteria += '##@@##' + 'KL';
        }
        
        if (Ext.get('cboPilihanTRRadkelompokPasien').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
            strKriteria += '##@@##' + Ext.get('cboPilihanTRRadkelompokPasien').getValue();
        }
        if (Ext.get('cboPilihanTRRadkelompokPasien').getValue() === 'Perseorangan')
        {
            selectSetPerseorangan = '0000000001';
            strKriteria += '##@@##' + 'Umum';
            strKriteria += '##@@##' + selectSetPerseorangan;
            strKriteria += '##@@##' + Ext.get('cboPilihanTRRadkelompokPasien').getValue();
        }
        if (Ext.get('cboPilihanTRRadkelompokPasien').getValue() === 'Perusahaan')
        {
            if(Ext.get('cboPerusahaanRequestEntryTRRad').getValue() === 'Semua')
            {
                strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + 'NULL';
                strKriteria += '##@@##' + Ext.get('cboPerusahaanRequestEntryTRRad').getValue();
            }else{
                strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + selectsetperusahaan;
                strKriteria += '##@@##' + Ext.get('cboPerusahaanRequestEntryTRRad').getValue();
            }
        }
        if (Ext.get('cboPilihanTRRadkelompokPasien').getValue() === 'Asuransi')
        {
            if(Ext.get('cboAsuransiTRRad').getValue() === 'Semua')
            {
                strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + 'NULL';
                strKriteria += '##@@##' + Ext.get('cboAsuransiTRRad').getValue();
            }else{
                strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + selectSetAsuransi;
                 strKriteria += '##@@##' + Ext.get('cboAsuransiTRRad').getValue();
            }
            
        };
        if (Ext.getCmp('Shift_All_TRRad').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift1';
            strKriteria += '##@@##' + 1;
            strKriteria += '##@@##' + 'shift2';
            strKriteria += '##@@##' + 2;
            strKriteria += '##@@##' + 'shift3';
            strKriteria += '##@@##' + 3;
        }else{
            if (Ext.getCmp('Shift_1_TRRad').getValue() === true)
            {
                strKriteria += '##@@##' + 'shift1';
                strKriteria += '##@@##' + 1;
            }
            if (Ext.getCmp('Shift_2_TRRad').getValue() === true)
            {
                strKriteria += '##@@##' + 'shift2';
                strKriteria += '##@@##' + 2;
            }
            if (Ext.getCmp('Shift_3_TRRad').getValue() === true)
            {
                strKriteria += '##@@##' + 'shift3';
                strKriteria += '##@@##' + 3;
            }
        }
	return strKriteria;
};

function ValidasiTanggalReportTRRad()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapTRRad').dom.value > Ext.get('dtpTglAkhirLapTRRad').dom.value)
    {
        ShowPesanWarningTRRadReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningTRRadReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapTRRad_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanTRRad(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboUserTRRad(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanTRRadKelompokPasien(),
                mComboPerseoranganTRRad(),
                mComboAsuransiTRRad(),
                mComboPerusahaanTRRad(),
                mComboUmumTRRad()
            ]
        }]
    };
    return items;
};


function getItemLapTRRad_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapTRRad_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 50,
            anchor: '100% 100%',
            defaultType: 'checkbox',
            items: [
            
            {x: 40,y: 10,boxLabel: 'Semua',name: 'Shift_All_TRRad',id : 'Shift_All_TRRad',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_TRRad').setValue(true);Ext.getCmp('Shift_2_TRRad').setValue(true);Ext.getCmp('Shift_3_TRRad').setValue(true);Ext.getCmp('Shift_1_TRRad').disable();Ext.getCmp('Shift_2_TRRad').disable();Ext.getCmp('Shift_3_TRRad').disable();}else{Ext.getCmp('Shift_1_TRRad').setValue(false);Ext.getCmp('Shift_2_TRRad').setValue(false);Ext.getCmp('Shift_3_TRRad').setValue(false);Ext.getCmp('Shift_1_TRRad').enable();Ext.getCmp('Shift_2_TRRad').enable();Ext.getCmp('Shift_3_TRRad').enable();}}},
            {x: 110,y: 10,boxLabel: 'Shift 1',name: 'Shift_1_TRRad',id : 'Shift_1_TRRad'},
            {x: 180,y: 10,boxLabel: 'Shift 2',name: 'Shift_2_TRRad',id : 'Shift_2_TRRad'},
            {x: 250,y: 10,boxLabel: 'Shift 3',name: 'Shift_3_TRRad',id : 'Shift_3_TRRad'}
            ]
            }
        ]
            
    };
    return items;
};
var valuerbTRRad;
function getItemLapTRRad_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {if (value === true)
                    {
                        valuerbTRRad = 'Tanggal';
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterTRRad').disable();
                        Ext.getCmp('dtpBulanAkhirFilterTRRad').disable();
                        Ext.getCmp('dtpTglAwalFilterTRRad').enable();
                        Ext.getCmp('dtpTglAkhirFilterTRRad').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterTRRad').disable();
                        Ext.getCmp('dtpTglAkhirFilterTRRad').disable();
                        Ext.getCmp('dtpBulanAwalFilterTRRad').enable();
                        Ext.getCmp('dtpBulanAkhirFilterTRRad').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, {
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterTRRad',
                format: 'd/M/Y',
                value: now
            }, {
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, {
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterTRRad',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        valuerbTRRad = 'Bulan';
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterTRRad').disable();
                        Ext.getCmp('dtpTglAkhirFilterTRRad').disable();
                        Ext.getCmp('dtpBulanAwalFilterTRRad').enable();
                        Ext.getCmp('dtpBulanAkhirFilterTRRad').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterTRRad').disable();
                        Ext.getCmp('dtpBulanAkhirFilterTRRad').disable();
                        Ext.getCmp('dtpTglAwalFilterTRRad').enable();
                        Ext.getCmp('dtpTglAkhirFilterTRRad').enable();
                    }
                }
            }, {
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, {
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterTRRad',
                format: 'M/Y',
                value: now
            }, {
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, {
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterTRRad',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
function mComboPilihanTRRad()
{
    var cboPilihanTRRad = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanTRRad',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPilihan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanTRRad;
};

function mComboPilihanTRRadKelompokPasien()
{
    var cboPilihanTRRadkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanTRRadkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPilihankelompokPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanTRRadkelompokPasien;
};

function mComboPerseoranganTRRad()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntryTRRad = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntryTRRad.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target:'ViewComboLookupCustomerRad',
				param: "jenis_cust=0 order by CUSTOMER"
			}
		}
	);
    var cboPerseoranganTRRad = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganTRRad',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width: 200,
                store: dsPerseoranganRequestEntryTRRad,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.KD_CUSTOMER ;
                        }
                }
            }
	);
	return cboPerseoranganTRRad;
};

function mComboUmumTRRad()
{
    var cboUmumTRRad = new Ext.form.ComboBox
	(
		{
                        x: 120,
                        y: 100,
			id:'cboUmumTRRad',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width: 200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetUmum,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumTRRad;
};

function mComboPerusahaanTRRad()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: '',
			    Sortdir: 'ASC',
			    target:'ViewComboLookupCustomerRad',
                            param: "jenis_cust=1 order by CUSTOMER"
			}
		}
	);
    var cboPerusahaanRequestEntryTRRad = new Ext.form.ComboBox
	(
		{
                    x: 120,
                    y: 100,
		    id: 'cboPerusahaanRequestEntryTRRad',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
                    width: 200,
                    value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
                                }
			}
                }
	);

    return cboPerusahaanRequestEntryTRRad;
};



function mComboAsuransiTRRad()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomerRad',
                param: "jenis_cust=2 order by CUSTOMER"
            }
        }
    );
    var cboAsuransiTRRad = new Ext.form.ComboBox
	(
		{
                    x: 120,
                    y: 100,
			id:'cboAsuransiTRRad',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Asuransi...',
                        fieldLabel: '',
                        align: 'Right',
                        width: 200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiTRRad;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganTRRad').show();
        Ext.getCmp('cboAsuransiTRRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRRad').hide();
        Ext.getCmp('cboUmumTRRad').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganTRRad').hide();
        Ext.getCmp('cboAsuransiTRRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRRad').show();
        Ext.getCmp('cboUmumTRRad').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganTRRad').hide();
        Ext.getCmp('cboAsuransiTRRad').show();
        Ext.getCmp('cboPerusahaanRequestEntryTRRad').hide();
        Ext.getCmp('cboUmumTRRad').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganTRRad').hide();
        Ext.getCmp('cboAsuransiTRRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRRad').hide();
        Ext.getCmp('cboUmumTRRad').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganTRRad').hide();
        Ext.getCmp('cboAsuransiTRRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTRRad').hide();
        Ext.getCmp('cboUmumTRRad').show();
   }
}

var selectuser;
function mComboUserTRRad()
{
    var Field = ['KD_USER','FULL_NAME'];
    ds_User_viDaftar = new WebApp.DataStore({fields: Field});

	ds_User_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: '',
                    Sortdir: '',
                    target:'ViewComboUser',
                    param: ""
                }
            }
        );

    var cboUserRequestEntryTRRad = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cboUserRequestEntryTRRad',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
            store: ds_User_viDaftar,
            valueField: 'KD_USER',
            displayField: 'FULL_NAME',
            width:200,
            listeners:
                {
                    'select': function(a, b, c)
					{
					   selectuser = b.data.kd_user;
					}                    
                }
        }
    )

    return cboUserRequestEntryTRRad;
};


