
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapTRSummary;
var selectNamaLapTRSummary;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapTRSummary;
var varLapLapTRSummary= ShowFormLapLapTRSummary();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapTRSummary;
var customer;
var kdcustomer;
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var winLapTRSummaryReport;

function ShowFormLapLapTRSummary()
{
    frmDlgLapTRSummary= fnDlgLapTRSummary();
    frmDlgLapTRSummary.show();
	loadDataComboUserLapTRSummary();
};

function fnDlgLapTRSummary()
{
    winLapTRSummaryReport = new Ext.Window
    (
        {
            id: 'winLapTRSummaryReport',
            title: 'Laporan Transaksi Detail',
            closeAction: 'destroy',
            width: 370,
            height: 410,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapTRSummary()],
            listeners:
			{
				activate: function()
				{
					Ext.getCmp('cboPerseoranganLapTRSummary').hide();
					Ext.getCmp('cboAsuransiLapTRSummary').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLapTRSummary').hide();
					Ext.getCmp('cboUmumLapTRSummary').show();
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'Ok',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapTRSummary',
						handler: function()
						{
							if (ValidasiReportLapTRSummary() === 1)
							{
								if (Ext.get('cboPilihanLapTRSummarykelompokPasien').getValue() === 'Semua')
								{
									tipe='Semua';
									customer='Semua';
									kdcustomer='Semua';
								} else if (Ext.get('cboPilihanLapTRSummarykelompokPasien').getValue() === 'Perseorangan'){
									customer=Ext.get('cboPerseoranganLapTRSummary').getValue();
									tipe='Perseorangan';
									kdcustomer=Ext.getCmp('cboPerseoranganLapTRSummary').getValue();
								} else if (Ext.get('cboPilihanLapTRSummarykelompokPasien').getValue() === 'Perusahaan'){
									customer=Ext.get('cboPerusahaanRequestEntryLapTRSummary').getValue();
									tipe='Perusahaan';
									kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapTRSummary').getValue();
								} else {
									customer=Ext.get('cboAsuransiLapTRSummary').getValue();
									tipe='Asuransi';
									kdcustomer=Ext.getCmp('cboAsuransiLapTRSummary').getValue();
								} 
								
								if(Ext.getCmp('radioasal').getValue() === true){
									periode='tanggal';
									tglAwal=Ext.getCmp('dtpTglAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
								} else{
									periode='bulan';
									tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilLab').getValue();
								}
								
								if (Ext.getCmp('Shift_All_LapTRSummary').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1_LapTRSummary').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2_LapTRSummary').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3_LapTRSummary').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
								
								var params={
									asal_pasien:Ext.get('cboPilihanLapTRSummary').getValue(),
									user:Ext.getCmp('cboUserRequestEntryLapTRSummary').getValue(),
									tipe:tipe,
									customer:customer,
									kdcustomer:kdcustomer,
									periode:periode,
									tglAwal:tglAwal,
									tglAkhir:tglAkhir,
									shift:shift,
									shift1:shift1,
									shift2:shift2,
									shift3:shift3,
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/rad/lap_radiologi/cetakTRSummary");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapTRSummaryReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapTRSummary',
						handler: function()
						{
							winLapTRSummaryReport.close();
						}
					}
			]

        }
    );

    return winLapTRSummaryReport;
};


function ItemDlgLapTRSummary()
{
    var PnlLapLapTRSummary = new Ext.Panel
    (
        {
            id: 'PnlLapLapTRSummary',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapTRSummary_Atas(),
                getItemLapLapTRSummary_Batas(),
                getItemLapLapTRSummary_Bawah(),
                getItemLapLapTRSummary_Batas(),
                getItemLapLapTRSummary_Samping(),
              
            ]
        }
    );

    return PnlLapLapTRSummary;
};


function getKodeReportLapTRSummary()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanLapTRSummary').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanLapTRSummary').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportLapTRSummary()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapTRSummary').getValue() === ''){
		ShowPesanWarningLapTRSummaryReport('Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboUserRequestEntryLapTRSummary').getValue() === ''){
		ShowPesanWarningLapTRSummaryReport('Operator Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.getCmp('cboPilihanLapTRSummarykelompokPasien').getValue() === ''){
		ShowPesanWarningLapTRSummaryReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapTRSummary').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapTRSummary').getValue() === '' &&  Ext.get('cboAsuransiLapTRSummary').getValue() === '' && Ext.get('cboUmumRegisLab').getValue() === '' ){
		ShowPesanWarningLapTRSummaryReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
   /*  if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapTRSummaryReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    } */
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapTRSummaryReport('Periode belum dipilih','Warning');
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapTRSummary').getValue() === false && Ext.getCmp('Shift_1_LapTRSummary').getValue() === false && Ext.getCmp('Shift_2_LapTRSummary').getValue() === false && Ext.getCmp('Shift_3_LapTRSummary').getValue() === false){
		ShowPesanWarningLapTRSummaryReport('Shift Belum Dipilih','Warning');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapTRSummaryReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapTRSummary_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTRSummary(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Operator '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboUserLapTRSummary(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapTRSummaryKelompokPasien(),
                mComboPerseoranganLapTRSummary(),
                mComboAsuransiLapTRSummary(),
                mComboPerusahaanLapTRSummary(),
                mComboUmumLapTRSummary()
            ]
        }]
    };
    return items;
};


function getItemLapLapTRSummary_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapTRSummary_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  345,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
				
					{
						x: 40,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapTRSummary',
						id : 'Shift_All_LapTRSummary',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapTRSummary').setValue(true);
								Ext.getCmp('Shift_2_LapTRSummary').setValue(true);
								Ext.getCmp('Shift_3_LapTRSummary').setValue(true);
								Ext.getCmp('Shift_1_LapTRSummary').disable();
								Ext.getCmp('Shift_2_LapTRSummary').disable();
								Ext.getCmp('Shift_3_LapTRSummary').disable();
							}else{
								Ext.getCmp('Shift_1_LapTRSummary').setValue(false);
								Ext.getCmp('Shift_2_LapTRSummary').setValue(false);
								Ext.getCmp('Shift_3_LapTRSummary').setValue(false);
								Ext.getCmp('Shift_1_LapTRSummary').enable();
								Ext.getCmp('Shift_2_LapTRSummary').enable();
								Ext.getCmp('Shift_3_LapTRSummary').enable();
							}
						}
					},
					{
						x: 110,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapTRSummary',
						id : 'Shift_1_LapTRSummary'
					},
					{
						x: 180,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapTRSummary',
						id : 'Shift_2_LapTRSummary'
					},
					{
						x: 250,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapTRSummary',
						id : 'Shift_3_LapTRSummary'
					}
				]
			}
        ]
            
    };
    return items;
};

function getItemLapLapTRSummary_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilLab',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilLab',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilLab',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function loadDataComboUserLapTRSummary(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/lab/lap_laboratorium/getUser",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUserRequestEntryLapTRSummary.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_User_LapTRSummary.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_User_LapTRSummary.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboPilihanLapTRSummary()
{
    var cboPilihanLapTRSummary = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapTRSummary',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ'],[3, 'RWI'], [4, 'IGD']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapTRSummary;
};

function mComboPilihanLapTRSummaryKelompokPasien()
{
    var cboPilihanLapTRSummarykelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanLapTRSummarykelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapTRSummarykelompokPasien;
};

function mComboPerseoranganLapTRSummary()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapTRSummary = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapTRSummary.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapTRSummary = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganLapTRSummary',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapTRSummary,
				/* new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ), */
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLapTRSummary;
};

function mComboUmumLapTRSummary()
{
    var cboUmumLapTRSummary = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumLapTRSummary',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumLapTRSummary;
};

function mComboPerusahaanLapTRSummary()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapTRSummary = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryLapTRSummary',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapTRSummary;
};

function mComboAsuransiLapTRSummary()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomerLab',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapTRSummary = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiLapTRSummary',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapTRSummary;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapTRSummary').show();
        Ext.getCmp('cboAsuransiLapTRSummary').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRSummary').hide();
        Ext.getCmp('cboUmumLapTRSummary').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapTRSummary').hide();
        Ext.getCmp('cboAsuransiLapTRSummary').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRSummary').show();
        Ext.getCmp('cboUmumLapTRSummary').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapTRSummary').hide();
        Ext.getCmp('cboAsuransiLapTRSummary').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRSummary').hide();
        Ext.getCmp('cboUmumLapTRSummary').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapTRSummary').hide();
        Ext.getCmp('cboAsuransiLapTRSummary').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRSummary').hide();
        Ext.getCmp('cboUmumLapTRSummary').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapTRSummary').hide();
        Ext.getCmp('cboAsuransiLapTRSummary').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapTRSummary').hide();
        Ext.getCmp('cboUmumLapTRSummary').show();
   }
}

function mComboUserLapTRSummary()
{
    var Field = ['kd_user','full_name'];
    ds_User_LapTRSummary = new WebApp.DataStore({fields: Field});
    cboUserRequestEntryLapTRSummary = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cboUserRequestEntryLapTRSummary',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:'Semua',
            store: ds_User_LapTRSummary,
            valueField: 'kd_user',
            displayField: 'full_name',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.kd_user;
				} 
			}
        }
    )

    return cboUserRequestEntryLapTRSummary;
};
