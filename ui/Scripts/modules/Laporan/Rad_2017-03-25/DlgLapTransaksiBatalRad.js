
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRadTransaksiBatal;
var selectNamaRadTransaksiBatal;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRadTransaksiBatal;
var varLapRadTransaksiBatal = ShowFormLapRadTransaksiBatal();
var selectSetUmum;
var selectSetkelpas;
var JenisLap;

function ShowFormLapRadTransaksiBatal()
{
    frmDlgRadTransaksiBatal = fnDlgRadTransaksiBatal();
    frmDlgRadTransaksiBatal.show();
}
;

function fnDlgRadTransaksiBatal()
{
    var winRadTransaksiBatalReport = new Ext.Window
            (
                    {
                        id: 'winRadTransaksiBatalReport',
                        title: 'Laporan Transaksi Batal',
                        closeAction: 'destroy',
                        width: 400,
                        height: 150,
                        border: false,
                        resizable: false,
                        constrain: true,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'icon_lapor',
                        modal: true,
                        items: [ItemDlgRadTransaksiBatal()],
                        listeners:
                                {
                                    activate: function ()
                                    {
                                    }
                                }

                    }
            );

    return winRadTransaksiBatalReport;
}
;

function ItemDlgRadTransaksiBatal()
{
    var PnlLapRadTransaksiBatal = new Ext.Panel
            (
                    {
                        id: 'PnlLapRadTransaksiBatal',
                        fileUpload: true,
                        layout: 'form',
                        height: '500',
                        anchor: '100%',
                        bodyStyle: 'padding:5px',
                        border: true,
                        items:
                                [
                                    getItemLapRadTransaksiBatal_Bawah(),
                                    {
                                        layout: 'hBox',
                                        border: false,
                                        defaults: {margins: '0 5 0 0'},
                                        style: {'margin-left': '5px', 'margin-top': '5px'},
                                        anchor: '99%',
                                        layoutConfig:
                                                {
                                                    padding: '3',
                                                    pack: 'end',
                                                    align: 'middle'
                                                },
                                        items:
                                                [
                                                    {
                                                        xtype: 'button',
                                                        text: 'Ok',
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnOkLapRadTransaksiBatal',
                                                        handler: function ()
                                                        {

                                                            var criteria = GetCriteriaRadTransaksiBatal();
                                                            frmDlgRadTransaksiBatal.close();
                                                            loadlaporanRadLab('0', 'LapTransaksiBatalRad', criteria);
                                                        }
                                                    },
                                                    {
                                                        xtype: 'button',
                                                        text: 'Cancel',
                                                        width: 70,
                                                        hideLabel: true,
                                                        id: 'btnCancelLapRadTransaksiBatal',
                                                        handler: function ()
                                                        {
                                                            frmDlgRadTransaksiBatal.close();
                                                        }
                                                    }
                                                ]
                                    }
                                ]
                    }
            );

    return PnlLapRadTransaksiBatal;
}
;

function GetCriteriaRadTransaksiBatal()
{
    var strKriteria = '';

    if (Ext.get('dtpTglAwalFilterHasilRad').getValue() !== '')
    {
        strKriteria = Ext.get('dtpTglAwalFilterHasilRad').getValue();
    }
    ;
    if (Ext.get('dtpTglAkhirFilterHasilRad').getValue() !== '')
    {
        strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterHasilRad').getValue();
    }
    ;

    if (Ext.get('cboPilihanRadTransaksiBatal').getValue() === 'Semua')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'NULL';
    } else if (Ext.get('cboPilihanRadTransaksiBatal').getValue() === 'RWJ/IGD')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'RWJ/IGD';
    } else if (Ext.get('cboPilihanRadTransaksiBatal').getValue() === 'RWI')
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'RWI';
    } else
    {
        strKriteria += '##@@##' + 'Unit';
        strKriteria += '##@@##' + 'KL';
    }

    if (Ext.get('cboPilihanRadTransaksiBatalkelompokPasien').getValue() === 'Semua')
    {
        strKriteria += '##@@##' + 'Semua';
        strKriteria += '##@@##' + 'NULL';
    }
    if (Ext.get('cboPilihanRadTransaksiBatalkelompokPasien').getValue() === 'Perseorangan')
    {
        selectSetPerseorangan = '0000000001';
        strKriteria += '##@@##' + 'Umum';
        strKriteria += '##@@##' + selectSetPerseorangan;
    }
    if (Ext.get('cboPilihanRadTransaksiBatalkelompokPasien').getValue() === 'Perusahaan')
    {
        if (Ext.get('cboPerusahaanRequestEntryTransaksiBatalRad').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Perusahaan';
            strKriteria += '##@@##' + 'NULL';
        } else {
            strKriteria += '##@@##' + 'Perusahaan';
            strKriteria += '##@@##' + selectsetperusahaan;
        }
    }
    if (Ext.get('cboPilihanRadTransaksiBatalkelompokPasien').getValue() === 'Asuransi')
    {
        if (Ext.get('cboAsuransiTransaksiBatalRad').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Asuransi';
            strKriteria += '##@@##' + 'NULL';
        } else {
            strKriteria += '##@@##' + 'Asuransi';
            strKriteria += '##@@##' + selectSetAsuransi;
        }

    }
    ;
    if (Ext.getCmp('Shift_All_RadTransaksiBatal').getValue() === true)
    {
        strKriteria += '##@@##' + 'shift1';
        strKriteria += '##@@##' + 1;
        strKriteria += '##@@##' + 'shift2';
        strKriteria += '##@@##' + 2;
        strKriteria += '##@@##' + 'shift3';
        strKriteria += '##@@##' + 3;
    } else {
        if (Ext.getCmp('Shift_1_RadTransaksiBatal').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift1';
            strKriteria += '##@@##' + 1;
        }
        if (Ext.getCmp('Shift_2_RadTransaksiBatal').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift2';
            strKriteria += '##@@##' + 2;
        }
        if (Ext.getCmp('Shift_3_RadTransaksiBatal').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift3';
            strKriteria += '##@@##' + 3;
        }
    }
    return strKriteria;
}
;

function ValidasiTanggalReportRadTransaksiBatal()
{
    var x = 1;
    if (Ext.get('dtpTglAwalLapRadTransaksiBatal').dom.value > Ext.get('dtpTglAkhirLapRadTransaksiBatal').dom.value)
    {
        ShowPesanWarningRadTransaksiBatalReport(nmWarningDateDlgRpt, nmTitleFormDlgReqCMRpt);
        x = 0;
    }

    return x;
}
;

function ShowPesanWarningRadTransaksiBatalReport(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 300
                    }
            );
}
;

function getItemLapRadTransaksiBatal_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 375,
                height: 165,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Jenis Laporan '
                    }, {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'radiogroup',
                        id: 'rgjenislaporan',
                        boxMaxWidth: 300,
                        items: [
                            {
                                boxLabel: 'Detail',
                                name: 'rgjl',
                                inputValue: 0,
                                checked: true,
                                id: 'rbDetail'
                            },
                            {
                                boxLabel: 'Summary',
                                name: 'rgjl',
                                inputValue: 1,
                                id: 'rbSummary'
                            },
                            {
                                boxLabel: 'Per Pasien',
                                name: 'rgjl',
                                inputValue: 2,
                                id: 'rbPasien'
                            }
                        ],
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                if (Ext.getCmp('rbDetail').checked === true)
                                {
                                    JenisLap = 'Detail';
                                } else if (Ext.getCmp('rbSummary').checked === true) {
                                    JenisLap = 'Summary';
                                } else{
                                    JenisLap = 'Pasien';
                                }
                            }
                        }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Pasien '
                    }, {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboPilihanRadTransaksiBatal(),
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Kelompok pasien '
                    }, {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboPilihanRadTransaksiBatalKelompokPasien(),
                    mComboPerseoranganRadTransaksiBatal(),
                    mComboAsuransiRadTransaksiBatal(),
                    mComboPerusahaanRadTransaksiBatal(),
                    mComboUmumRadTransaksiBatal(),
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Dokter '
                    }, {
                        x: 110,
                        y: 130,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboDokterRadTransaksiBatal()
                ]
            }]
    };
    return items;
}
;

function getItemLapRadTransaksiBatal_Bawah()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                xtype: 'fieldset',
                                title: 'Kriteria',
                                height : 100,
                                width: '373px',
                                layout: 'absolute',
                                
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Periode '
                                            }, {
                                                x: 110,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'datefield',
                                                id: 'dtpTglAwalFilterHasilRad',
                                                format: 'd/M/Y',
                                                value: now,
                                                height: 100,
                                            }, {
                                                 x: 235,
                                                 y: 15,
                                                xtype: 'label',
                                                text: ' s/d '
                                            }, {
                                                x: 260,
                                                y: 10,
                                                xtype: 'datefield',
                                                id: 'dtpTglAkhirFilterHasilRad',
                                                format: 'd/M/Y',
                                                value: now,
                                                width: 100
                                            },
                                        ]
                            }
                        ]

            };
    return items;
}
;

function getItemLapRadTransaksiBatal_NewBawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 375,
                height: 80,
                anchor: '100% 100%',
                items: [
                        {
                        x: 120,
                        y: 10,
                        xtype: 'radiogroup',
                        id: 'rgTanggal',
                        boxMaxWidth: 150,
                        items: [
                            {
                                boxLabel: 'Tanggal',
                                name: 'rgTanggallap',
                                inputValue: 0,
                                checked: true,
                                id: 'rbTanggal'
                            },
                            {
                                boxLabel: 'Bulan',
                                name: 'rgTanggallap',
                                inputValue: 1,
                                id: 'rbBulan'
                            }
                        ],
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                if (Ext.getCmp('rbTanggal').checked === true)
                                {
                                    Ext.get('dtpTglAwalFilterHasilRad').show();
                                    Ext.get('dtpTglAkhirFilterHasilRad').show();
                                    Ext.get('dtpBlnAwalFilterHasilRad').hide();
                                    Ext.get('dtpBlnAkhirFilterHasilRad').hide();
                                } else {
                                    Ext.get('dtpTglAwalFilterHasilRad').hide();
                                    Ext.get('dtpTglAkhirFilterHasilRad').hide();
                                    Ext.get('dtpBlnAwalFilterHasilRad').show();
                                    Ext.get('dtpBlnAkhirFilterHasilRad').show();
                                }
                            }
                        }
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterHasilRad',
                        format: 'd/M/Y',
                        value: now
                    }, {
                        x: 235,
                        y: 45,
                        xtype: 'label',
                        text: ' s/d '
                    }, {
                        x: 260,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterHasilRad',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpBlnAwalFilterHasilRad',
                        format: 'M/Y',
                        value: now
                    }, {
                        x: 235,
                        y: 45,
                        xtype: 'label',
                        text: ' s/d '
                    }, {
                        x: 260,
                        y: 40,
                        xtype: 'datefield',
                        id: 'dtpBlnAkhirFilterHasilRad',
                        format: 'M/Y',
                        value: now,
                        width: 100
                    },
                ]
            }
            ]
    };
    return items;
}
;

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
function mComboPilihanRadTransaksiBatal()
{
    var cboPilihanRadTransaksiBatal = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboPilihanRadTransaksiBatal',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: 'Pendaftaran Per Shift ',
                        width: 240,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua Pasien'], [2, 'Pasien Rawat Jalan'], [3, 'Pasien Rawat Inap'], [4, 'Pasien Gawat Darurat'], [5, 'Pasien Umum']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetPilihan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPilihan = b.data.displayText;
                                    }
                                }
                    }
            );
    return cboPilihanRadTransaksiBatal;
}
;

function mComboPilihanRadTransaksiBatalKelompokPasien()
{
    var cboPilihanRadTransaksiBatalkelompokPasien = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboPilihanRadTransaksiBatalkelompokPasien',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: 'Pendaftaran Per Shift ',
                        width: 240,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua'], [2, 'Perseorangan'], [3, 'Perusahaan'], [4, 'Asuransi']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetPilihankelompokPasien,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPilihankelompokPasien = b.data.displayText;
                                        Combo_Select(b.data.displayText);
                                    }
                                }
                    }
            );
    return cboPilihanRadTransaksiBatalkelompokPasien;
}
;

function mComboPerseoranganRadTransaksiBatal()
{
    var Field = ['KD_CUSTOMER', 'CUSTOMER'];
    dsPerseoranganRequestEntryTransaksiBatalRad = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntryTransaksiBatalRad.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    //Sort: 'DEPT_ID',
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=0 order by CUSTOMER"
                                }
                    }
            );
    var cboPerseoranganTransaksiBatalRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboPerseoranganTransaksiBatalRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: '',
                        width: 240,
                        store: dsPerseoranganRequestEntryTransaksiBatalRad,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetPerseorangan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetPerseorangan = b.data.displayText;
                                    }
                                }
                    }
            );
    return cboPerseoranganTransaksiBatalRad;
}
;

function mComboUmumRadTransaksiBatal()
{
    var cboUmumTransaksiBatalRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboUmumTransaksiBatalRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: '',
                        width: 240,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetUmum,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetUmum = b.data.displayText;
                                    }


                                }
                    }
            );
    return cboUmumTransaksiBatalRad;
}
;

function mComboDokterRadTransaksiBatal()
{
    var cboDokterTransaksiBatalRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 130,
                        id: 'cboDokterTransaksiBatalRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Silahkan Pilih...',
                        fieldLabel: '',
                        width: 240,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectSetUmum,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetDokter = b.data.displayText;
                                    }


                                }
                    }
            );
    return cboDokterTransaksiBatalRad;
}
;

function mComboPerusahaanRadTransaksiBatal()
{
    var Field = ['KD_CUSTOMER', 'CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    //Sort: 'DEPT_ID',
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=1 order by CUSTOMER"
                                }
                    }
            );
    var cboPerusahaanRequestEntryTransaksiBatalRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboPerusahaanRequestEntryTransaksiBatalRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Perusahaan...',
                        fieldLabel: '',
                        align: 'Right',
                        store: dsPerusahaanRequestEntry,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        width: 240,
                        value: selectsetperusahaan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectsetperusahaan = b.data.KD_CUSTOMER;
                                    }
                                }
                    }
            );

    return cboPerusahaanRequestEntryTransaksiBatalRad;
}
;

function mComboAsuransiRadTransaksiBatal()
{
    var Field_poli_viDaftar = ['KD_CUSTOMER', 'CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: '',
                                    target: 'ViewComboLookupCustomerRad',
                                    param: "jenis_cust=2 order by CUSTOMER"
                                }
                    }
            );
    var cboAsuransiTransaksiBatalRad = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboAsuransiTransaksiBatalRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Asuransi...',
                        fieldLabel: '',
                        align: 'Right',
                        width: 240,
                        store: ds_customer_viDaftar,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetAsuransi,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetAsuransi = b.data.KD_CUSTOMER;
                                    }
                                }
                    }
            );
    return cboAsuransiTransaksiBatalRad;
}
;

function Combo_Select(combo)
{
    var value = combo;

    if (value === "Perseorangan")
    {
        Ext.getCmp('cboPerseoranganTransaksiBatalRad').show();
        Ext.getCmp('cboAsuransiTransaksiBatalRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTransaksiBatalRad').hide();
        Ext.getCmp('cboUmumTransaksiBatalRad').hide();
    } else if (value === "Perusahaan")
    {
        Ext.getCmp('cboPerseoranganTransaksiBatalRad').hide();
        Ext.getCmp('cboAsuransiTransaksiBatalRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTransaksiBatalRad').show();
        Ext.getCmp('cboUmumTransaksiBatalRad').hide();
    } else if (value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganTransaksiBatalRad').hide();
        Ext.getCmp('cboAsuransiTransaksiBatalRad').show();
        Ext.getCmp('cboPerusahaanRequestEntryTransaksiBatalRad').hide();
        Ext.getCmp('cboUmumTransaksiBatalRad').hide();
    } else if (value === "Semua")
    {
        Ext.getCmp('cboPerseoranganTransaksiBatalRad').hide();
        Ext.getCmp('cboAsuransiTransaksiBatalRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTransaksiBatalRad').hide();
        Ext.getCmp('cboUmumTransaksiBatalRad').show();
        Ext.getCmp('cboUmumTransaksiBatalRad').disable();
    } else
    {
        Ext.getCmp('cboPerseoranganTransaksiBatalRad').hide();
        Ext.getCmp('cboAsuransiTransaksiBatalRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryTransaksiBatalRad').hide();
        Ext.getCmp('cboUmumTransaksiBatalRad').show();
    }
}

function mCombounitRadTransaksiBatal()
{
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

    ds_Poli_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'NAMA_UNIT',
                                    Sortdir: 'ASC',
                                    target: 'ViewSetupUnit',
                                    param: "kd_bagian=2 and type_unit=false"
                                }
                    }
            );

    var cbounitRequestEntryRadTransaksiBatal = new Ext.form.ComboBox
            (
                    {
                        id: 'cbounitRequestEntryRadTransaksiBatal',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih unit...',
                        fieldLabel: 'unit ',
                        align: 'Right',
                        store: ds_Poli_viDaftar,
                        valueField: 'KD_UNIT',
                        displayField: 'NAMA_UNIT',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                    }

                                }
                    }
            )

    return cbounitRequestEntryRadTransaksiBatal;
}
;
