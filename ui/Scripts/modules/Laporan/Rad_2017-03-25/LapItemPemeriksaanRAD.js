var fields = [
	{name: 'KD_UNIT', mapping : 'KD_UNIT'},
	{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];

var fieldsPayment = [
	{name: 'KD_CUSTOMER', mapping : 'KD_CUSTOMER'},
	{name: 'CUSTOMER', mapping : 'CUSTOMER'}
];

var cols = [
	{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
	{ header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
];

var colsPayment = [
	{ id : 'KD_CUSTOMER', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'KD_CUSTOMER',hidden : true},
	{ header: "Kode Pay", width: 50, sortable: true, dataIndex: 'CUSTOMER'}
];

var secondGridStoreLapItemPemeriksaan;
var secondGridStoreLapItemPemeriksaanGrid2;
var DlgLapRADItemPemeriksaan={
	vars:{
		comboSelect:null
	},
	coba:function(){
		var $this=this;
		$this.DateField.startDate
	},
	ArrayStore:{
		dokter:new Ext.data.ArrayStore({fields:[]})
	},
	CheckboxGroup:{
		shift:null,
		tindakan:null,
	},
	ComboBox:{
		kelPasien1:null,
		unitRad:null,
		asalPasien:null,
		combo1:null,
		combo2:null,
		combo3:null,
		combounitrad:null,
		combo0:null,
		poliklinik:null,
		dokter:null
	},
	DataStore:{
		combo2:null,
		combo3:null,
		combounitrad:null,
		poliklinik:null,
		combo1:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	comboOnSelect:function(val){
		var $this=this;
		$this.ComboBox.combo0.hide();
		$this.ComboBox.combo1.hide();
		$this.ComboBox.combo2.hide();
		$this.ComboBox.combo3.hide();
		if(val==-1){
			$this.ComboBox.combo0.show();
			
		}else if(val==0){
			$this.ComboBox.combo1.show();
		}else if(val==1){
			$this.ComboBox.combo2.show();
		}else if(val==2){
			$this.ComboBox.combo3.show();
		}
	},
	initPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		var sendDataArraypayment = [];

		secondGridStoreLapItemPemeriksaanGrid2.each(function(record){
			var recordArraypay= [record.get("KD_CUSTOMER")];
			sendDataArraypayment.push(recordArraypay);
		});

		if (sendDataArraypayment.length === 0){  
			this.messageBox('Peringatan','Isi cara membayar dengan drag and drop','WARNING');
			loadMask.hide();
		}else{
			params['start_date'] = timestimetodate($this.DateField.startDate.getValue());
			params['last_date']  = timestimetodate($this.DateField.endDate.getValue());
			//params['pasien']     = $this.ComboBox.kelPasien1.getValue();
			params['unit_rad'] = $this.ComboBox.unitRad.getValue();
			params['asal_pasien'] = $this.ComboBox.asalPasien.getValue();
			/* var pasien=parseInt($this.ComboBox.kelPasien1.getValue());
			if(pasien>=0){
				if(pasien==0){
					params['kd_customer']=$this.ComboBox.combo1.getValue();
				}else if(pasien==1){
					params['kd_customer']=$this.ComboBox.combo2.getValue();
				}else if(pasien==2){
					params['kd_customer']=$this.ComboBox.combo3.getValue();
				}
			} */
		
			params['tmp_payment'] 	= sendDataArraypayment;
			/* var shift  = $this.CheckboxGroup.shift.items.items;
			var shifta = false; */
			var tindakan_stat=false;
			//if(shifta==false){
				var form = document.createElement("form");
				form.setAttribute("method", "post");
				form.setAttribute("target", "_blank");
				form.setAttribute("action", baseURL + "index.php/rad/lap_RADItemPemeriksaan/cetak");
				var hiddenField = document.createElement("input");
				hiddenField.setAttribute("type", "hidden");
				hiddenField.setAttribute("name", "data");
				hiddenField.setAttribute("value", Ext.encode(params));
				form.appendChild(hiddenField);
				document.body.appendChild(form);
				form.submit();
				loadMask.hide();
			//}
		} 
		
	},
	messageBox:function(modul, str, icon){
		Ext.MessageBox.show
		(
			{
				title: modul,
				msg:str,
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.icon,
				width:300
			}
		);
	},
	getDokter:function(){
		var $this=this;
		$this.ComboBox.dokter = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.ArrayStore.dokter,
            width: 200,
            value:'Semua',
            valueField: 'kd_dokter',
            displayField: 'nama',
            listeners:{
            	'select': function(a, b, c){
                               
            	}
            }
        });		    
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gawat_darurat/lap_penerimaan/getDokter",
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.dokter.loadData([],false);
					for(var i=0,iLen=r.data.length; i<iLen ;i++){
						$this.ArrayStore.dokter.add(new $this.ArrayStore.dokter.recordType(r.data[i]));
					}
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
		return $this.ComboBox.dokter;
	},
	getCombo0:function(){
		var $this=this;
		$this.ComboBox.combo0 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: new Ext.data.ArrayStore({
				id: 0,
				fields:['Id','displayText'],
				data: [[1, 'Semua']]
			}),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			disabled:true,
			listeners:{
				'select': function(a,b,c){
					selectSetUmum=b.data.displayText ;
				}
			}
		});
		return $this.ComboBox.combo0;
	},
	getCombo1:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo1 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo1.load({
    		params:{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 ORDER BY CUSTOMER '
			}
		});
    	$this.ComboBox.combo1 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hidden:true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: $this.DataStore.combo1,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
			listeners:{
				'select': function(a,b,c){
				}
			}
		});
		return $this.ComboBox.combo1;
	},
	getCombo2:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo2 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo2.load({
		    params:{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER '
			}
		});
   		$this.ComboBox.combo2 = new Ext.form.ComboBox({
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    hidden:true,
		    store: $this.DataStore.combo2,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			value:'Semua',
			width:150,
		    listeners:{
			    'select': function(a,b,c){
				}
			}
		});
    	return $this.ComboBox.combo2;
	},
	
	firstGridPayment : function(){
		var firstGridPayment;
		var dataSource_payment;
		var Field_payment = ['KD_CUSTOMER','CUSTOMER'];
		dataSource_payment         = new WebApp.DataStore({fields: Field_payment});
		dataSource_payment.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_CUSTOMER',
					Sortdir: 'ASC',
					target:'ViewComboLookupCustomer',
					param: " jenis_cust in (0,1,2) ORDER BY CUSTOMER"
				}
			}
		);
            firstGridPayment = new Ext.grid.GridPanel({
				ddGroup          : 'secondGridDDGroupPayment',
				store            : dataSource_payment,
				autoScroll       : true,
				columnLines      : true,
				border           : true,
				enableDragDrop   : true,
				height           : 150,
				stripeRows       : true,
				trackMouseOver   : true,
				title            : 'Kelompok Pasien',
				anchor           : '100% 100%',
				plugins          : [new Ext.ux.grid.FilterRow()],
				colModel         : new Ext.grid.ColumnModel
				(
					[
					new Ext.grid.RowNumberer(),
						{
							id: 'colKD_cust',
							header: 'Kode Cust',
							dataIndex: 'KD_CUSTOMER',
							sortable: true,
							hidden : true
						},
						{
							id: 'colKD_customer',
							header: 'Customer',
							dataIndex: 'CUSTOMER',
							sortable: true,
							width: 50
						}
					]
				),   
				listeners : {
					afterrender : function(comp) {
						var secondGridDropTargetEl = firstGridPayment.getView().scroller.dom;
						var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
								ddGroup    : 'firstGridDDGroupPayment',
								notifyDrop : function(ddSource, e, data){
										var records =  ddSource.dragData.selections;
										Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
										firstGridPayment.store.add(records);
										firstGridPayment.store.sort('KD_CUSTOMER', 'ASC');
										return true
								}
						});
					}
	            },
	                viewConfig: 
	                    {
	                            forceFit: true
	                    }
        	});
        return firstGridPayment;
	},
	seconGridPayment : function(){
		//var secondGridStoreLapItemPemeriksaanGrid2;
		var secondGridPayment;
		secondGridStoreLapItemPemeriksaanGrid2 = new Ext.data.JsonStore({
            fields : fieldsPayment,
            root   : 'records'
        });

        // create the destination Grid
            secondGridPayment = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroupPayment',
					store            : secondGridStoreLapItemPemeriksaanGrid2,
					columns          : colsPayment,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 150,
					stripeRows       : true,
					autoExpandColumn : 'KD_CUSTOMER',
					title            : 'Pilihan Kelompok Pasien',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGridPayment.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroupPayment',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGridPayment.store.add(records);
											secondGridPayment.store.sort('KD_CUSTOMER', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGridPayment;
	},
	getCombo3:function(){
		var $this=this;
		var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    	$this.DataStore.combo3 = new WebApp.DataStore({fields: Field_poli_viDaftar});

		$this.DataStore.combo3.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER "
            }
        });
    	$this.ComboBox.combo3 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:150,
			hidden:true,
			store: $this.DataStore.combo3,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
//			value: 0,
			listeners:{
				'select': function(a,b,c){
//					selectSetAsuransi=b.data.KD_CUSTOMER ;
//					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		});
		return $this.ComboBox.combo3;
	},
	mComboUnitRad:function(){ 
		var Field = ['KD_UNIT','NAMA_UNIT'];
		$this.DataStore.combounitrad = new WebApp.DataStore({ fields: Field });
		$this.DataStore.combounitrad.load({
			params:{
				Skip: 0,
				Take: 1000,
				Sort: 'nama_unit',
				Sortdir: 'ASC',
				target: 'ViewSetupUnit',
				param: "parent = '5'"
			}
		});
		var cbounitrad_viPenJasRad = new Ext.form.ComboBox({
			id: 'cboUnitRad_viPenJasRad',
			x: 110,
			y: 190,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 130,
			emptyText:'Pilih Unit',
			store: $this.DataStore.combounitrad,
			valueField: 'KD_UNIT',
			displayField: 'NAMA_UNIT',
			//value:'All',
			editable: false,
			listeners:
				{
					'select': function(a,b,c)
					{
						tmpkd_unit = b.data.KD_UNIT;
						//getComboDokterRad(b.data.KD_UNIT);
					}
				}
		});
		return cbounitrad_viPenJasRad;
	},
	init:function(){
		var $this=this;
		$this.Window.main=new Ext.Window({
			width: 401,
			height:450,
			modal:true,
			title:'Laporan Item Pemeriksaan Radiologi',
			layout:'fit',
			items:[
				new Ext.Panel({
					bodyStyle:'padding: 6px',
					layout:'form',
					border:false,
           			autoScroll: true,
					fbar:[
						new Ext.Button({
							text:'Ok',
							handler:function(){
								$this.initPrint()
							}
						}),
						new Ext.Button({
							text:'Batal',
							handler:function(){
								$this.Window.main.close();
							}
						})
					],
					items:[
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:50,
							items:[
								{
									xtype : 'fieldset',
									title : 'Periode',
									layout: 'column',
									width : 390,
									bodyStyle:'padding: 2px 0px  7px 15px',
									border: true,
									items:[
										
										$this.DateField.startDate=new Ext.form.DateField({
											value: new Date(),
											format:'d/M/Y'
										}),
										{
											xtype:'displayfield',
											width: 30,
											value:'&nbsp;s/d&nbsp;'
										},
										$this.DateField.endDate=new Ext.form.DateField({
											value: new Date(),
											format:'d/M/Y'
										})
									]
								}
							]
						},
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:50,
							items:[
								{
									xtype : 'fieldset',
									title : 'Asal Pasien',
									layout: 'column',
									width : 390,
									bodyStyle:'padding: 2px 0px  7px 15px',
									border: true,
									items:[
										$this.ComboBox.asalPasien=new Ext.form.ComboBox({
												triggerAction: 'all',
												lazyRender:true,
												mode: 'local',
												width: 150,
												selectOnFocus:true,
												forceSelection: true,
												emptyText:'Silahkan Pilih...',
												fieldLabel: 'Pendaftaran Per Shift ',
												//store: dsunitrad_viPenJasRad,
												/* valueField: 'KD_UNIT',
												displayField: 'NAMA_UNIT', */
												store: new Ext.data.ArrayStore({
													id: 0,
													fields:[
															'Id',
															'displayText'
													],
													data: [[1, 'Semua'], [2, 'RWJ'],[3, 'RWI'], [4, 'IGD']]
												}),
												valueField: 'Id',
												displayField: 'displayText',
												value:'Semua',
												listeners:{
													'select': function(a,b,c){
															//$this.vars.comboSelect=b.data.displayText;
															//$this.comboOnSelect(b.data.Id);
													}
												}
											}),
										
									]
								}
							]
						},{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:50,
							items:[
								{
									xtype : 'fieldset',
									title : 'Unit Rad',
									layout: 'column',
									width : 390,
									bodyStyle:'padding: 2px 0px  7px 15px',
									border: true,
									items:[
										$this.ComboBox.unitRad=new Ext.form.ComboBox({
												triggerAction: 'all',
												lazyRender:true,
												mode: 'local',
												width: 150,
												selectOnFocus:true,
												forceSelection: true,
												emptyText:'Silahkan Pilih...',
												fieldLabel: 'Pendaftaran Per Shift ',
												//store: dsunitrad_viPenJasRad,
												/* valueField: 'KD_UNIT',
												displayField: 'NAMA_UNIT', */
												store: new Ext.data.ArrayStore({
													id: 0,
													fields:[
															'Id',
															'displayText'
													],
													data: [[-1, 'Semua'],[0, 'Radiologi IGD'],[1, 'Radiologi Pav'], [2, 'Radiologi Umum']]
												}),
												valueField: 'Id',
												displayField: 'displayText',
												value:'Semua',
												listeners:{
													'select': function(a,b,c){
															$this.vars.comboSelect=b.data.displayText;
															//$this.comboOnSelect(b.data.Id);
													}
												}
											}),
										
									]
								}
							]
						},
						{
							xtype : 'fieldset',
							title : 'Kelompok Pasien',
							layout:'column',
							border:true,
							height: 190,
							bodyStyle:'margin-top: 2px',
							items:[
								{
									border:false,
									width :170,
									bodyStyle:'margin-top: 2px',
									items:[
										$this.firstGridPayment(),
									]
								},
								{
									border:false,
									width :10,
									bodyStyle:'margin-top: 2px',
								},
								{
									border:false,
									width :170,
									bodyStyle:'margin-top: 2px; align:right;',
									items:[
										$this.seconGridPayment(),
									]
								}
							]
						},
					]
				})
			]
		}).show();
	}
};
DlgLapRADItemPemeriksaan.init();
