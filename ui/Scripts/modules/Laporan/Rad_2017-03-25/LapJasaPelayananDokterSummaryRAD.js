var type_file=0;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRADPelayananDokterSummary;
var selectNamaRADPelayananDokterSummary;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRADPelayananDokterSummary;
var varLapRADPelayananDokterSummary= ShowFormLapRADPelayananDokterSummary();
var selectSetUmum;
var selectSetkelpas;


var tmpnama_unit;
var selectSetPilihankelompokPasien;
var selectSetPilihanProfesi;
var selectSetPilihanDokterSummary;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapRADPelayananDokterSummary()
{
    frmDlgRADPelayananDokterSummary= fnDlgRADPelayananDokterSummary();
    frmDlgRADPelayananDokterSummary.show();
};

function fnDlgRADPelayananDokterSummary()
{
    var winRADPelayananDokterSummaryReport = new Ext.Window
    (
        {
            id: 'winRADPelayananDokterSummaryReport',
            title: 'Laporan Transaksi Jasa Pelayanan Dokter Summary',
            closeAction: 'destroy',
            width:400,
            height: 360,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRADPelayananDokterSummary()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganRAD').show();
                Ext.getCmp('cboAsuransiRAD').hide();
                Ext.getCmp('cboPerusahaanRequestEntryRAD').hide();
                Ext.getCmp('cboUmumRAD').hide();
            }
        }

        }
    );

    return winRADPelayananDokterSummaryReport;
};


function ItemDlgRADPelayananDokterSummary()
{
    var PnlLapRADPelayananDokterSummary = new Ext.Panel
    (
        {
            id: 'PnlLapRADPelayananDokterSummary',
            fileUpload: true,
            layout: 'form',
			//width:400,
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapRADPelayananDokterSummary_Atas(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRADPelayananDokterSummary',
                            handler: function()
                            {
								if (Ext.getCmp('Shift_All').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
                            	//if (ValidasiReportRADPasienPerDokterSummary() === 1)
							   	//{								
									var params={
						//id : 'cbPendaftaran_RADPelayananDokterSummary'
						//id : 'cbTindakRAD_RADPelayananDokterSummary'
						//$('.messageCheckbox:checked').val();
										shift:shift,
										shift1:shift1,
										shift2:shift2,
										shift3:shift3,
                                        kd_profesi:Ext.getCmp('IDcboPilihanRADJenisProfesi').getValue(),
										kd_poli:selectSetUnit,
										pelayananPendaftaran:Ext.getCmp('cbPendaftaran_RADPelayananDokterSummary').getValue(),
										pelayananTindak:Ext.getCmp('cbTindakRAD_RADPelayananDokterSummary').getValue(),
                                        //kd_DokterSummary:Ext.getCmp('cboDokterSummaryRADPelayananDokterSummary').getValue(),
										kd_DokterSummary:GetCriteriaRADProfesi(),
										kd_kelompok:GetCriteriaRADPasienPerKelompok(),
										tglAwal:Ext.getCmp('dtpTglAwalLapRADPelayananDokterSummary').getValue(),
										tglAkhir:Ext.getCmp('dtpTglAkhirLapRADPelayananDokterSummary').getValue(),
										type_file:type_file
									} 
									//console.log(params);
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/rad/lap_RADPelayananDokterSummary/cetak");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();		
									frmDlgRADPasienPerKelompok.close(); 
									
							   //};
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRADPelayananDokterSummary',
                            handler: function()
                            {
                                    frmDlgRADPelayananDokterSummary.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRADPelayananDokterSummary;
};

function getItemLapRADPelayananDokterSummary_Atas()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 280,
            anchor: '100% 100%',
            items: [
			{
				xtype: 'checkboxgroup',
				width:210,
				x: 120,
				hidden:true,
				y: 10,
				items: 
				[
					{
						
						boxLabel: 'Pendaftaran',
						name: 'cbPendaftaran_RADPelayananDokterSummary',
						id : 'cbPendaftaran_RADPelayananDokterSummary'
					},
					{
						boxLabel: 'Tindak RAD',
						name: 'cbTindakRAD_RADPelayananDokterSummary',
						id : 'cbTindakRAD_RADPelayananDokterSummary'
					}
			   ]
			},
            //  ================================================================================== POLIKLINIK
			{
				x: 10,
				y: 40,
				xtype: 'label',
				text: 'Poliklinik '
			}, {
				x: 110,
				y: 40,
				xtype: 'label',
				text: ' : '
			},
				mCombounitRADPelayananDokterSummary(),
            //  ================================================================================== DokterSummary
			{
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Bagian '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
                mComboRADJenisProfesi(),
				mComboDokterSummaryRADPelayananDokterSummary(),
                mComboDokterSummaryRADPelayananPerawat(),

			{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, {
				x: 110,
				y: 130,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAwalLapRADPelayananDokterSummary',
				format: 'd/M/Y',
                value: now
				//value: tigaharilalu
			}, {
				x: 230,
				y: 130,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 260,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapRADPelayananDokterSummary',
				format: 'd/M/Y',
				value: now,
				width: 100
			},{
				x: 10,
				y: 160,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 160,
				xtype: 'label',
				text: ' : '
			},
				mComboRADPasienPerKelompok(),
				mComboRADPasienPerKelompokSEMUA(),
				mComboRADPasienPerKelompokPERORANGAN(),
				mComboRADPasienPerKelompokPERUSAHAAN(),
				mComboRADPasienPerKelompokASURANSI(),
			{
				x: 10,
				y: 220,
				xtype: 'label',
				text: 'Type File '
			}, {
				x: 110,
				y: 220,
				xtype: 'label',
				text: ' : '
			},
			{
				x: 120,
				y: 220,
			   xtype: 'checkbox',
			   id: 'CekLapPilihTypeExcel',
			   hideLabel:false,
			   boxLabel: 'Excel',
			   checked: false,
			   listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
						{
							type_file=1;
						}
						else
						{
							type_file=0;
						}
					}
			   }
			},
			{
				x: 10,
				y: 255,
				xtype: 'checkboxgroup',
				fieldLabel: 'Shift',
				columns: 5,
				vertical: false,
				items: [
					{
					boxLabel: 'Semua',checked:true,name: 'Shift_All',id : 'Shift_All',
					handler: function (field, value) 
					{
						if (value === true){
						Ext.getCmp('Shift_1').setValue(true);
						Ext.getCmp('Shift_2').setValue(true);
						Ext.getCmp('Shift_3').setValue(true);
						Ext.getCmp('Shift_1').disable();
						Ext.getCmp('Shift_2').disable();
						Ext.getCmp('Shift_3').disable();
					}else{
						Ext.getCmp('Shift_1').setValue(false);
						Ext.getCmp('Shift_2').setValue(false);
						Ext.getCmp('Shift_3').setValue(false);
						Ext.getCmp('Shift_1').enable();
						Ext.getCmp('Shift_2').enable();
						Ext.getCmp('Shift_3').enable();
					}
				}
				},
						{boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1',id : 'Shift_1'},
						{boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2',id : 'Shift_2'},
						{boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3',id : 'Shift_3'}
					]
			}
            ]
        }]
    };
    return items;
};

function ShowPesanWarningRADPelayananDokterSummaryReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getComboDokterSummaryRAD(kdUnit)
{
	dsDokterSummaryPelayaranDokterSummary.load
	({
		params:
		{
			Skip: 0,
			Take: 1000,
			Sort: 'kd_DokterSummary',
			Sortdir: 'ASC',
			target: 'ViewDokterPenunjang',
			param: "kd_unit = '"+kdUnit+"'"
		}
	});
	return dsDokterSummaryPelayaranDokterSummary;
}
function mComboDokterSummaryRADPelayananDokterSummary()
{
    var Field = ['KD_Dokter','NAMA'];
    dsDokterSummaryPelayaranDokterSummary = new WebApp.DataStore({fields: Field});
    
    var cboPilihanRADPelayananDokterSummary = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboDokterSummaryRADPelayananDokterSummary',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterSummaryPelayaranDokterSummary,
                valueField: 'KD_Dokter',
                displayField: 'NAMA',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokterSummary=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanRADPelayananDokterSummary;
};

function mComboDokterSummaryRADPelayananPerawat()
{
	var Field = ['KD_Dokter','NAMA'];
    dsDokterSummaryPelayaranDokterSummary = new WebApp.DataStore({fields: Field});
    dsDokterSummaryPelayaranDokterSummary.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokterLaporan',
				param: "WHERE Dokter.jenis_Dokter='1'"
			}
		}
	);
    var cboPilihanRADPelayananDokterSummary = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboDokterSummaryRADPelayananPerawat',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterSummaryPelayaranDokterSummary,
                valueField: 'KD_Dokter',
                displayField: 'NAMA',
                value:'Semua',
                hidden:true,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokterSummary=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanRADPelayananDokterSummary;
};

function getUnitDefault(){
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRAD/getUnitDefault",
		params: {text:''},
		failure: function (o){
			loadMask.hide();
			ShowPesanErrorPenJasRAD('Gagal mendapatkan data Unit default', 'Radiologi');
		},
		success: function (o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				selectSetUnit=cst.kd_unit;
				tmpnama_unit=cst.nama_unit;
				getComboDokterSummaryRAD(cst.kd_unit);
				Ext.getCmp('cbounitRequestEntryRADPelayananDokterSummary').setValue(tmpnama_unit);
			} else{
				ShowPesanErrorPenJasRAD('Gagal mendapatkan data Unit default', 'Radiologi');
			}
		}
	});
}
function mCombounitRADPelayananDokterSummary()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});
	getUnitDefault();
	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewCombounit_Konfigurasi',
                    param: " "
                }
            }
        );

    var cbounitRequestEntryRADPelayananDokterSummary = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 40,
            id: 'cbounitRequestEntryRADPelayananDokterSummary',
			width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryRADPelayananDokterSummary;
};


function mComboRADPasienPerKelompok()
{
    var cboPilihanRADPelayananDokterSummarykelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 160,
                id:'cboPilihanRADPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectRADPasienPerKelompok(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanRADPelayananDokterSummarykelompokPasien;
};

function mComboRADJenisProfesi()
{
    var cboPilihanRADJenisProfesi = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'IDcboPilihanRADJenisProfesi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Dokter'], [2, 'Perawat']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihanProfesi=b.data.displayText;
                            Combo_SelectRADProfesi(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanRADJenisProfesi;
};

//RADPasienPerKelompok
function mComboRADPasienPerKelompokSEMUA()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRAD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboRADPasienPerKelompokSEMUA',
                typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				selectOnFocus:true,
				forceSelection: true,
				emptyText:'Silahkan Pilih...',
				valueField: 'Id',
	            displayField: 'displayText',
	            hidden:false,
				fieldLabel: '',
				width: 240,
				value:1,
				store: new Ext.data.ArrayStore
				(
						{
							id: 0,
							fields:
								[
										'Id',
										'displayText'
								],
							data: [[1, 'Semua']]
						}
				),
				listeners:
				{
					'select': function(a,b,c)
					{
						selectSetUmum=b.data.displayText ;
					}
	                                
	                            
				}
            }
	);
	return cboPerseoranganRAD;
};

function mComboRADPasienPerKelompokPERORANGAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRAD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboRADPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRAD;
};

function mComboRADPasienPerKelompokPERUSAHAAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='1' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRAD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboRADPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
				//value: selectsetperusahaan,
                listeners:
                {
				    'select': function(a,b,c)
					{
				        selectsetperusahaan = b.data.KD_CUSTOMER;
						selectsetnamaperusahaan = b.data.CUSTOMER;
					}
                }
            }
	);
	return cboPerseoranganRAD;
};

function mComboRADPasienPerKelompokASURANSI()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='2' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRAD = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 190,
                id:'IDmComboRADPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
				valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
				    'select': function(a,b,c)
					{
						selectSetAsuransi=b.data.KD_CUSTOMER ;
						selectsetnamaAsuransi = b.data.CUSTOMER ;
					}
                }
            }
	);
	return cboPerseoranganRAD;
};


function Combo_SelectRADPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboRADPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRADPasienPerKelompokSEMUA').show();
   }
};

function Combo_SelectRADProfesi(combo)
{
   var value = combo;

   if(value === "DokterSummary")
   {    
        Ext.getCmp('cboDokterSummaryRADPelayananDokterSummary').show();
        Ext.getCmp('cboDokterSummaryRADPelayananPerawat').hide();
   }
   else if(value === "Perawat")
   {    
        Ext.getCmp('cboDokterSummaryRADPelayananPerawat').show();
        Ext.getCmp('cboDokterSummaryRADPelayananDokterSummary').hide();
        
   }
   else
   {
        Ext.getCmp('cboDokterSummaryRADPelayananDokterSummary').show();
        Ext.getCmp('cboDokterSummaryRADPelayananPerawat').hide();
   }
};


function GetCriteriaRADPasienPerKelompok()
{
    var strKriteria = '';
    
    if (Ext.getCmp('cboPilihanRADPasienPerKelompok').getValue() !== '')
    {
        if (Ext.get('cboPilihanRADPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
        else if (Ext.get('cboPilihanRADPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboRADPasienPerKelompokPERORANGAN').getValue(); } 
        else if (Ext.get('cboPilihanRADPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboRADPasienPerKelompokPERUSAHAAN').getValue(); } 
        else if (Ext.get('cboPilihanRADPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboRADPasienPerKelompokASURANSI').getValue(); }
    }else{
            strKriteria = 'Semua';
    }

    
    return strKriteria;
};

function GetCriteriaRADProfesi()
{
	var strKriteria = '';
	
	if (Ext.getCmp('IDcboPilihanRADJenisProfesi').getValue() !== '')
	{
		if (Ext.get('IDcboPilihanRADJenisProfesi').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
		else if (Ext.get('IDcboPilihanRADJenisProfesi').getValue() === 'DokterSummary'){ strKriteria = Ext.getCmp('cboDokterSummaryRADPelayananDokterSummary').getValue(); } 
		else if (Ext.get('IDcboPilihanRADJenisProfesi').getValue() === 'Perawat'){ strKriteria = Ext.getCmp('cboDokterSummaryRADPelayananPerawat').getValue(); } 
	}else{
        strKriteria = 'Semua';
	}

	
	return strKriteria;
};

function ValidasiReportRADPasienPerDokterSummary()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRADPasienPerDokterSummary').dom.value === '')
	{
		ShowPesanWarningRADPasienPerDokterSummaryReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
    return x;
};
