var fields = [
	{name: 'KD_UNIT', mapping : 'KD_UNIT'},
	{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];

var fieldsPayment = [
	{name: 'KD_PAY', mapping : 'KD_PAY'},
	{name: 'URAIAN', mapping : 'URAIAN'}
];

var cols = [
	{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
	{ header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
];

var colsPayment = [
	{ id : 'KD_PAY', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'KD_PAY',hidden : true},
	{ header: "Kode Pay", width: 50, sortable: true, dataIndex: 'URAIAN'}
];
var colsCustomer = [
	{ id : 'kd_customer', header: "Kode Customer", width: 160, sortable: true, dataIndex: 'kd_customer',hidden : true},
	{ header: "Customer", width: 50, sortable: true, dataIndex: 'customer'}
];

var dataSource_payment;
var Field_payment = ['KD_PAY','URAIAN'];
dataSource_payment         = new WebApp.DataStore({fields: Field_payment});
var secondGridPayment;
var firstGridPayment;
var secondGridStoreLapPenerimaan;
var secondGridStoreLapPenerimaanPaymentLapPenerimaan;
var secondGridStoreLapPenerimaanCustomer;
var kelpas=-1;
var DlgLapRADPenerimaanTunaiKomponenDetail={
	vars:{
		comboSelect:null
	},
	coba:function(){
		var $this=this;
		$this.DateField.startDate
	},
	ArrayStore:{
		dokter:new Ext.data.ArrayStore({fields:[]})
	},
	CheckboxGroup:{
		shift:null,
		shift2:null,
		tindakan:null,
	},
	ComboBox:{
		jenisLaporan:null,
		kelPasien1:null,
		unitRad:null,
		asalPasien:null,
		combo1:null,
		combo2:null,
		combo3:null,
		combo0:null,
		poliklinik:null,
		dokter:null
	},
	DataStore:{
		combo2:null,
		combo3:null,
		poliklinik:null,
		combo1:null,
		gridcustomer:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	comboOnSelect:function(val){
		var $this=this;
		// $this.ComboBox.combo0.hide();
		// $this.ComboBox.combo1.hide();
		// $this.ComboBox.combo2.hide();
		// $this.ComboBox.combo3.hide();
		if(val==0){
			//$this.ComboBox.combo0.show();
			kelpas =-1;
			$this.loadcustomer(kelpas);
			secondGridStoreLapPenerimaanCustomer.removeAll();
			
		}else if(val==1){
			//$this.ComboBox.combo1.show();
			kelpas =0;
			$this.loadcustomer(kelpas);
			secondGridStoreLapPenerimaanCustomer.removeAll();
		}else if(val==2){
			//$this.ComboBox.combo2.show();
			kelpas =1;
			$this.loadcustomer(kelpas);
			secondGridStoreLapPenerimaanCustomer.removeAll();
		}else if(val==3){
			//$this.ComboBox.combo3.show();
			kelpas =2;
			$this.loadcustomer(kelpas);
			secondGridStoreLapPenerimaanCustomer.removeAll();
		}
	},
	initPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		var sendDataCustomer    	= "";
		var sendDataArraypayment = [];
		var sendDataArrayCustomer = []; 

		secondGridStoreLapPenerimaanPaymentLapPenerimaan.each(function(record){
			var recordArraypay= [record.get("KD_PAY")];
			sendDataArraypayment.push(recordArraypay);
		});
		secondGridStoreLapPenerimaanCustomer.each(function(record){
			var recordArray   = [record.get("kd_customer")];
			sendDataArrayCustomer.push(recordArray);
			sendDataCustomer 	+= "'"+recordArray+"',";
		});

		if (sendDataArraypayment.length === 0){  
			this.messageBox('Peringatan','Isi cara membayar dengan drag and drop','WARNING');
			loadMask.hide();
		}else{
			params['type_file'] = Ext.getCmp('type_file_rad_penerimaantunaikomponendetail').getValue();
			params['start_date'] = timestimetodate($this.DateField.startDate.getValue());
			params['last_date']  = timestimetodate($this.DateField.endDate.getValue());
			params['pasien']     = $this.ComboBox.kelPasien1.getValue();
			params['unit_rad'] = $this.ComboBox.unitRad.getValue();
			params['asal_pasien'] = $this.ComboBox.asalPasien.getValue();
			//params['type_file'] 	= Ext.getCmp('CekLapPilihTypeExcel_pp_RAD').getValue();
			var pasien=parseInt($this.ComboBox.kelPasien1.getValue());
			
			params['tmp_kd_customer'] 	= sendDataCustomer;
			
		
			params['tmp_payment'] 	= sendDataArraypayment;
			var shift  = $this.CheckboxGroup.shift.items.items;
			var shift2  = $this.CheckboxGroup.shift2.items.items;
			var shifta = false;
			var shiftb = false;
			var tindakan_stat=false;
			for(var i=0;i<shift.length ; i++){
				params['shift'+i]=shift[i].checked;
				if(shift[i].checked==true)shifta=true;
			}

			for(var i=0;i<shift2.length ; i++){
				params['shift2'+i]=shift2[i].checked;
				if(shift2[i].checked==true)shiftb=true;
			}
			//if(shifta==false){
			var jenis_lap=$this.ComboBox.jenisLaporan.getValue();
			if (jenis_lap === 0){
				//Laporan Penerimaan Per Pasien
				
				if (shifta == true && shiftb == false){
					loadMask.hide();
					Ext.Msg.alert('Gagal','Pilih shift periode akhir! ');
				}else if(shifta == false && shiftb == true){
					loadMask.hide();
					Ext.Msg.alert('Gagal','Pilih shift periode awal! ');
				}else{
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("target", "_blank");
					form.setAttribute("action", baseURL + "index.php/radiotherapy/lap_radiotherapy/cetak_detail_per_pasien");
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", "data");
					hiddenField.setAttribute("value", Ext.encode(params));
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();
					loadMask.hide();
				}
			}
			else  if (jenis_lap === 1){
				//Laporan Penerimaan Per Jenis Penerimaan
				var form = document.createElement("form");
				form.setAttribute("method", "post");
				form.setAttribute("target", "_blank");
				form.setAttribute("action", baseURL + "index.php/radiotherapy/lap_radiotherapy/cetak_detail_per_jenis");
				var hiddenField = document.createElement("input");
				hiddenField.setAttribute("type", "hidden");
				hiddenField.setAttribute("name", "data");
				hiddenField.setAttribute("value", Ext.encode(params));
				form.appendChild(hiddenField);
				document.body.appendChild(form);
				form.submit();
				loadMask.hide();
			}
			else  if (jenis_lap === 2){
				//Laporan Penerimaan Per Komponen Detail
				var form = document.createElement("form");
				form.setAttribute("method", "post");
				form.setAttribute("target", "_blank");
				form.setAttribute("action", baseURL + "index.php/rad/lap_RADPenerimaanTunaiPerKomponenDetail/cetak");
				var hiddenField = document.createElement("input");
				hiddenField.setAttribute("type", "hidden");
				hiddenField.setAttribute("name", "data");
				hiddenField.setAttribute("value", Ext.encode(params));
				form.appendChild(hiddenField);
				document.body.appendChild(form);
				form.submit();
				loadMask.hide();
			}
			else  if (jenis_lap === 3){
				//Laporan Penerimaan Per Komponen Summary
				var form = document.createElement("form");
				form.setAttribute("method", "post");
				form.setAttribute("target", "_blank");
				form.setAttribute("action", baseURL + "index.php/rad/lap_RADPenerimaanTunaiPerKomponenSummary/cetak");
				var hiddenField = document.createElement("input");
				hiddenField.setAttribute("type", "hidden");
				hiddenField.setAttribute("name", "data");
				hiddenField.setAttribute("value", Ext.encode(params));
				form.appendChild(hiddenField);
				document.body.appendChild(form);
				form.submit();
				loadMask.hide();
			}
				
			//}
		} 
		
	},
	messageBox:function(modul, str, icon){
		Ext.MessageBox.show
		(
			{
				title: modul,
				msg:str,
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.icon,
				width:300
			}
		);
	},
	getDokter:function(){
		var $this=this;
		$this.ComboBox.dokter = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.ArrayStore.dokter,
            width: 200,
            value:'Semua',
            valueField: 'kd_dokter',
            displayField: 'nama',
            listeners:{
            	'select': function(a, b, c){
                               
            	}
            }
        });		    
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gawat_darurat/lap_penerimaan/getDokter",
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.dokter.loadData([],false);
					for(var i=0,iLen=r.data.length; i<iLen ;i++){
						$this.ArrayStore.dokter.add(new $this.ArrayStore.dokter.recordType(r.data[i]));
					}
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
		return $this.ComboBox.dokter;
	},
	getCombo0:function(){
		var $this=this;
		$this.ComboBox.combo0 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: new Ext.data.ArrayStore({
				id: 0,
				fields:['Id','displayText'],
				data: [[1, 'Semua']]
			}),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			disabled:true,
			listeners:{
				'select': function(a,b,c){
					selectSetUmum=b.data.displayText ;
				}
			}
		});
		return $this.ComboBox.combo0;
	},
	getCombo1:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo1 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo1.load({
    		params:{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 ORDER BY CUSTOMER '
			}
		});
    	$this.ComboBox.combo1 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hidden:true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: $this.DataStore.combo1,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
			listeners:{
				'select': function(a,b,c){
				}
			}
		});
		return $this.ComboBox.combo1;
	},
	getCombo2:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo2 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo2.load({
		    params:{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER '
			}
		});
   		$this.ComboBox.combo2 = new Ext.form.ComboBox({
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    hidden:true,
		    store: $this.DataStore.combo2,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			value:'Semua',
			width:150,
		    listeners:{
			    'select': function(a,b,c){
				}
			}
		});
    	return $this.ComboBox.combo2;
	},
	firstGridPenerimaanCustomer : function(){
		var $this=this;
		
		var dataSource_customer;
		var Field_poli_viDaftar = ['kd_customer','customer'];
		
		$this.DataStore.gridcustomer         = new WebApp.DataStore({fields: Field_poli_viDaftar});
		$this.loadcustomer(kelpas);
		
        firstGrid_customer = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : $this.DataStore.gridcustomer,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 120,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Customer',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'kd_customer',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Customer',
                                                    dataIndex: 'customer',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
			listeners : {
				afterrender : function(comp) {
					var secondGridDropTargetEl = firstGrid_customer.getView().scroller.dom;
					var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroup',
							notifyDrop : function(ddSource, e, data){
									var records =  ddSource.dragData.selections;
									Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
									firstGrid_customer.store.add(records);
									firstGrid_customer.store.sort('kd_customer', 'ASC');
									return true
							}
					});
				}
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid_customer;
	},
	seconGridPenerimaanCustomer : function(){
		
		var secondGridCust;
		secondGridStoreLapPenerimaanCustomer = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGridCust = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroup',
					store            : secondGridStoreLapPenerimaanCustomer,
					columns          : colsCustomer,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 120,
					stripeRows       : true,
					autoExpandColumn : 'kd_customer',
					title            : 'Customer yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGridCust.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroup',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGridCust.store.add(records);
											secondGridCust.store.sort('kd_customer', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGridCust;
	},
	loadcustomer:function(param){
		var $this=this;
		Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/function_laporan_PTP/getCustomerLaporanPenunjang",
				params: {kelpas :param},
				failure: function(o){
					var cst = Ext.decode(o.responseText);
				},	    
				success: function(o) {
					firstGrid_customer.store.removeAll();
					var cst = Ext.decode(o.responseText);

					for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
						var recs    = [],recType =  $this.DataStore.gridcustomer.recordType;
						var o=cst['listData'][i];
				
						recs.push(new recType(o));
						$this.DataStore.gridcustomer.add(recs);
						console.log(o);
					}
				}
			});
	},
	firstGridPayment : function(){
		dataSource_payment.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_PAY',
					Sortdir: 'ASC',
					target:'ViewListPayment',
					param: " "
				}
			}
		);
            firstGridPayment = new Ext.grid.GridPanel({
				ddGroup          : 'secondGridDDGroupPayment',
				store            : dataSource_payment,
				autoScroll       : true,
				columnLines      : true,
				border           : true,
				enableDragDrop   : true,
				height           : 130,
				stripeRows       : true,
				trackMouseOver   : true,
				title            : 'Payment',
				anchor           : '100% 100%',
				plugins          : [new Ext.ux.grid.FilterRow()],
				colModel         : new Ext.grid.ColumnModel
				(
					[
					new Ext.grid.RowNumberer(),
						{
							id: 'colKD_pay',
							header: 'Kode Pay',
							dataIndex: 'KD_PAY',
							sortable: true,
							hidden : true
						},
						{
							id: 'colKD_uraian',
							header: 'Uraian',
							dataIndex: 'URAIAN',
							sortable: true,
							width: 50
						}
					]
				),   
				listeners : {
					afterrender : function(comp) {
						var secondGridDropTargetEl = firstGridPayment.getView().scroller.dom;
						var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
								ddGroup    : 'firstGridDDGroupPayment',
								notifyDrop : function(ddSource, e, data){
										var records =  ddSource.dragData.selections;
										Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
										firstGridPayment.store.add(records);
										firstGridPayment.store.sort('KD_PAY', 'ASC');
										return true
								}
						});
					}
	            },
	                viewConfig: 
	                    {
	                            forceFit: true
	                    }
        	});
        return firstGridPayment;
	},
	seconGridPayment : function(){
		//var secondGridStoreLapPenerimaanPaymentLapPenerimaan;
		secondGridStoreLapPenerimaanPaymentLapPenerimaan = new Ext.data.JsonStore({
            fields : fieldsPayment,
            root   : 'records'
        });

        // create the destination Grid
            secondGridPayment = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroupPayment',
					store            : secondGridStoreLapPenerimaanPaymentLapPenerimaan,
					columns          : colsPayment,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 130,
					stripeRows       : true,
					autoExpandColumn : 'KD_PAY',
					title            : 'Payment yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGridPayment.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroupPayment',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGridPayment.store.add(records);
											secondGridPayment.store.sort('KD_PAY', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGridPayment;
	},
	getCombo3:function(){
		var $this=this;
		var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    	$this.DataStore.combo3 = new WebApp.DataStore({fields: Field_poli_viDaftar});

		$this.DataStore.combo3.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER "
            }
        });
    	$this.ComboBox.combo3 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:150,
			hidden:true,
			store: $this.DataStore.combo3,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
//			value: 0,
			listeners:{
				'select': function(a,b,c){
//					selectSetAsuransi=b.data.KD_CUSTOMER ;
//					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		});
		return $this.ComboBox.combo3;
	},
	init:function(){
		var $this=this;
		$this.Window.main=new Ext.Window({
			width: 540,
			height:650,
			modal:true,
			title:'Laporan Penerimaan Radiotherapy',
			layout:'fit',
			items:[
				new Ext.Panel({
					bodyStyle:'padding: 6px',
					layout:'form',
					border:false,
           			autoScroll: true,
					fbar:[
						new Ext.Button({
							text:'Ok',
							handler:function(){
								$this.initPrint()
							}
						}),
						new Ext.Button({
							text:'Batal',
							handler:function(){
								$this.Window.main.close();
							}
						})
					],
					items:[
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:130,
							items:[
								{
									// xtype : 'fieldset',
									// title : 'Kelompok pasien',
									layout: 'column',
									border: false,
									bodyStyle:'margin-top: 2px',
									//height: 60,
									items:[
											{
												xtype : 'fieldset',
												title : 'Jenis Laporan Penerimaan',
												layout: 'column',
												border: true,
												bodyStyle:'padding: 2px 0px  7px 15px',
												width:530,
												height: 67,
												items:[
													$this.ComboBox.jenisLaporan=new Ext.form.ComboBox({
														triggerAction: 'all',
														lazyRender:true,
														mode: 'local',
														width: 330,
														selectOnFocus:true,
														forceSelection: true,
														emptyText:'Silahkan Pilih...',
														//fieldLabel: 'Pendaftaran Per Shift ',
														store: new Ext.data.ArrayStore({
															id: 0,
															fields:[
																	'Id',
																	'displayText'
															],
															data: [[0, 'Per Pasien'],[1, 'Per Jenis Penerimaan'],]
														}),
														valueField: 'Id',
														displayField: 'displayText',
														value:0,
														listeners:{
															'select': function(a,b,c){
																	
															}
														}
													}),{
														xtype:'displayfield',
														width: 30,
														value:'&nbsp;&nbsp;'
													},
												]
											},{
												xtype : 'fieldset',
												title : 'Periode',
												layout: 'column',
												width : 530,
												height:100,
												bodyStyle:'padding: 2px 0px  7px 15px',
												border: true,
												items:[
													{
														width:80,
														xtype: 'label',
														text:' Periode Awal : ',
													},
													$this.DateField.startDate=new Ext.form.DateField({
														value: new Date(),
														format:'d/M/Y',
														width:120,
													}),
													{
														xtype:'displayfield',
														width: 20,
														value:'&nbsp;&nbsp;'
													},
													$this.CheckboxGroup.shift=new Ext.form.CheckboxGroup({
														xtype: 'checkboxgroup',
														width:270,
														items: [
															{boxLabel: 'Semua',checked:false,name: 'Shift_All_LabRegis',id : 'Shift_All_LabRegis',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_LabRegis').setValue(true);Ext.getCmp('Shift_2_LabRegis').setValue(true);Ext.getCmp('Shift_3_LabRegis').setValue(true);Ext.getCmp('Shift_1_LabRegis').disable();Ext.getCmp('Shift_2_LabRegis').disable();Ext.getCmp('Shift_3_LabRegis').disable();}else{Ext.getCmp('Shift_1_LabRegis').setValue(false);Ext.getCmp('Shift_2_LabRegis').setValue(false);Ext.getCmp('Shift_3_LabRegis').setValue(false);Ext.getCmp('Shift_1_LabRegis').enable();Ext.getCmp('Shift_2_LabRegis').enable();Ext.getCmp('Shift_3_LabRegis').enable();}}},
															{boxLabel: 'Shift 1',checked:false,disabled:false,name: 'Shift_1_LabRegis',id : 'Shift_1_LabRegis'},
															{boxLabel: 'Shift 2',checked:false,disabled:false,name: 'Shift_2_LabRegis',id : 'Shift_2_LabRegis'},
															{boxLabel: 'Shift 3',checked:false,disabled:false,name: 'Shift_3_LabRegis',id : 'Shift_3_LabRegis'},
															// {boxLabel: 'Excel',checked:false,name: 'type_file_rad_penerimaantunaikomponendetail',id : 'type_file_rad_penerimaantunaikomponendetail'}
														]
													}),
													{				
														width:80,
														xtype: 'label',
														text:' Periode Akhir : ',
													},
													$this.DateField.endDate=new Ext.form.DateField({
														value: new Date(),
														format:'d/M/Y',
														width:120,
													}),
													{
														xtype:'displayfield',
														width: 20,
														value:'&nbsp;&nbsp;'
													},
													$this.CheckboxGroup.shift2=new Ext.form.CheckboxGroup({
														xtype: 'checkboxgroup',
														width:270,
														items: [
															{boxLabel: 'Semua',checked:false,name: 'Shift_All_LabRegis2',id : 'Shift_All_LabRegis2',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_LabRegis2').setValue(true);Ext.getCmp('Shift_2_LabRegis2').setValue(true);Ext.getCmp('Shift_3_LabRegis2').setValue(true);Ext.getCmp('Shift_1_LabRegis2').disable();Ext.getCmp('Shift_2_LabRegis2').disable();Ext.getCmp('Shift_3_LabRegis2').disable();}else{Ext.getCmp('Shift_1_LabRegis2').setValue(false);Ext.getCmp('Shift_2_LabRegis2').setValue(false);Ext.getCmp('Shift_3_LabRegis2').setValue(false);Ext.getCmp('Shift_1_LabRegis2').enable();Ext.getCmp('Shift_2_LabRegis2').enable();Ext.getCmp('Shift_3_LabRegis2').enable();}}},
															{boxLabel: 'Shift 1',checked:false,disabled:false,name: 'Shift_1_LabRegis2',id : 'Shift_1_LabRegis2'},
															{boxLabel: 'Shift 2',checked:false,disabled:false,name: 'Shift_2_LabRegis2',id : 'Shift_2_LabRegis2'},
															{boxLabel: 'Shift 3',checked:false,disabled:false,name: 'Shift_3_LabRegis2',id : 'Shift_3_LabRegis2'}
														]
													}),
												]
											}
									]
								},
							]
						},
						
						{
							// xtype : 'fieldset',
							// title : 'Kelompok pasien',
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 0px',
							//height: 60,
							items:[
									{
										xtype : 'fieldset',
										title : 'Kelompok pasien',
										layout: 'column',
										border: true,
										bodyStyle:'padding: 0px 0px  7px 10px',
										width:530,
										height: 200,
										items:[
										{
											layout: 'column',
											border: false,
											bodyStyle:'padding: 0px 0px  5px 0px',
											width:300,
											items:[
												$this.ComboBox.kelPasien1 = new Ext.form.ComboBox({
												triggerAction: 'all',
												lazyRender:true,
												mode: 'local',
												width: 300,
												selectOnFocus:true,
												hidden:true,
												forceSelection: true,
												emptyText:'Silahkan Pilih...',
												fieldLabel: 'Pendaftaran Per Shift ',
												store: new Ext.data.ArrayStore({
													id: 0,
													fields:[
															'Id',
															'displayText'
													],
													data: [[0, 'Semua'],[1, 'Perseorangan'],[2, 'Perusahaan'], [3, 'Asuransi']]
												}),
												valueField: 'Id',
												displayField: 'displayText',
												value:'Semua',
												listeners:{
													'select': function(a,b,c){
															$this.vars.comboSelect=b.data.displayText;
															$this.comboOnSelect(b.data.Id);
													}
												}
												}),
											]
											
											/* {
												xtype:'displayfield',
												width: 10,
												value:'&nbsp;'
											}, */
											/* $this.getCombo0(),
											$this.getCombo1(),
											$this.getCombo2(),
											$this.getCombo3() */
										},{
											layout: 'column',
											border: false,
											width:500,
											items:[
											//grid customer
												{
													border:false,
													columnWidth:.47,
													//width :170,
													bodyStyle:'margin-top: 2px',
													items:[
														$this.firstGridPenerimaanCustomer(),
													]
												},
												{
													border:false,
													columnWidth :.03,
													bodyStyle:'margin-top: 2px',
												},
												{
													border:false,
													columnWidth:.47,
													//width :170,
													bodyStyle:'margin-top: 2px; align:right;',
													items:[
														$this.seconGridPenerimaanCustomer(),
													]
												}
											]
										},{
											layout: 'column',
											border: false,
											width:500,
											items:[
											//grid customer
												{
													border:false,
													columnWidth:.47,
													//width :170,
													bodyStyle:'margin-top: 2px',
													items:[
														{
															id 			: 'CekCustomerPilihSemuaRAD',
															xtype 		: 'checkbox',
															boxLabel	: 'Pilih Semua Customer',
															anchor 		: '100% 100%',
															width 		: '100%',
															listeners :  {
																check: function()
																{
																	if(Ext.getCmp('CekCustomerPilihSemuaRAD').getValue() ===true)
																	{
																		firstGrid_customer.getSelectionModel().selectAll();
																	}
																	else
																	{
																		firstGrid_customer.getSelectionModel().clearSelections();
																	}
																}
															}
														}
													]
												},
												{
													border:false,
													columnWidth :.03,
													bodyStyle:'margin-top: 2px',
												},
												{
													border:false,
													columnWidth:.47,
													//width :170,
													bodyStyle:'margin-top: 2px; align:right;',
													items:[
														{
															xtype 		: 'button',
															text 		: 'Reset Pilihan',
															anchor 		: '100% 100%',
															width 		: '100%',
															handler 	: function(){
																$this.loadcustomer(kelpas);
																secondGridStoreLapPenerimaanCustomer.removeAll();
															}
														}
													]
												}
											]
										}
										]
									}
							]
						},
						/* {
							// xtype : 'fieldset',
							// title : 'Kelompok pasien',
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							//height: 60,
							items:[
									
									{
										xtype : 'fieldset',
										title : 'Shift',
										layout:'column',
										border: true,
										bodyStyle:'padding: 2px 0px  7px 15px',
										width : 390,
										//height: 60,
										items:[
											$this.CheckboxGroup.shift=new Ext.form.CheckboxGroup({
												xtype: 'checkboxgroup',
												items: [
													{boxLabel: 'Semua',checked:true,name: 'Shift_All_LabRegis',id : 'Shift_All_LabRegis',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_LabRegis').setValue(true);Ext.getCmp('Shift_2_LabRegis').setValue(true);Ext.getCmp('Shift_3_LabRegis').setValue(true);Ext.getCmp('Shift_1_LabRegis').disable();Ext.getCmp('Shift_2_LabRegis').disable();Ext.getCmp('Shift_3_LabRegis').disable();}else{Ext.getCmp('Shift_1_LabRegis').setValue(false);Ext.getCmp('Shift_2_LabRegis').setValue(false);Ext.getCmp('Shift_3_LabRegis').setValue(false);Ext.getCmp('Shift_1_LabRegis').enable();Ext.getCmp('Shift_2_LabRegis').enable();Ext.getCmp('Shift_3_LabRegis').enable();}}},
													{boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1_LabRegis',id : 'Shift_1_LabRegis'},
													{boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2_LabRegis',id : 'Shift_2_LabRegis'},
													{boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3_LabRegis',id : 'Shift_3_LabRegis'},
													{boxLabel: 'Excel',checked:false,name: 'type_file_rad_penerimaantunaikomponendetail',id : 'type_file_rad_penerimaantunaikomponendetail'}
												]
											})
										]
									}
							]
						}, */
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:50,
							items:[
								{
									xtype : 'fieldset',
									title : 'Asal Pasien & Unit Rad',
									layout: 'column',
									width : 530,
									bodyStyle:'padding: 2px 0px  7px 7px',
									border: true,
									items:[
										$this.ComboBox.asalPasien=new Ext.form.ComboBox({
												triggerAction: 'all',
												lazyRender:true,
												mode: 'local',
												width: 150,
												selectOnFocus:true,
												forceSelection: true,
												emptyText:'Silahkan Pilih...',
												fieldLabel: 'Pendaftaran Per Shift ',
												store: new Ext.data.ArrayStore({
													id: 0,
													fields:[
															'Id',
															'displayText'
													],
													data: [[0, 'Semua'],[1, 'RWJ'],[2, 'IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
												}),
												valueField: 'Id',
												displayField: 'displayText',
												value:'Semua',
												listeners:{
													'select': function(a,b,c){
															$this.vars.comboSelect=b.data.displayText;
															$this.comboOnSelect(b.data.Id);
													}
												}
											}),
											
											{
												xtype:'displayfield',
												width: 10,
												value:'&nbsp;'
											},
											$this.ComboBox.unitRad=new Ext.form.ComboBox({
												triggerAction: 'all',
												lazyRender:true,
												mode: 'local',
												width: 150,
												selectOnFocus:true,
												forceSelection: true,
												emptyText:'Silahkan Pilih...',
												fieldLabel: 'Pendaftaran Per Shift ',
												//store: dsunitrad_viPenJasRad,
												/* valueField: 'KD_UNIT',
												displayField: 'NAMA_UNIT', */
												store: new Ext.data.ArrayStore({
													id: 0,
													fields:[
															'Id',
															'displayText'
													],
													data: [[0, 'Semua'],[1, 'Radiologi IGD'],[2, 'Radiologi Pav'], [3, 'Radiologi Umum']]
												}),
												valueField: 'Id',
												displayField: 'displayText',
												value:'Semua',
												listeners:{
													'select': function(a,b,c){
															$this.vars.comboSelect=b.data.displayText;
															//$this.comboOnSelect(b.data.Id);
													}
												}
											}),

											{
												xtype:'displayfield',
												width: 100,
												value:'&nbsp;'
											},
											{
												xtype: 'checkbox',
												bodyStyle:'margin-left: 10px',
												fieldLbel: 'Type ',
												id: 'type_file_rad_penerimaantunaikomponendetail',
												hideLabel:false,
												boxLabel: 'Excel',
												checked: false,
												listeners: 
												{
												}
											}
									]
								}
							]
						},
						{
							xtype : 'fieldset',
							title : 'Payment',
							layout:'column',
							border:true,
							height: 190,
							bodyStyle:'margin-top: 0px',
							items:[
								{
									border:false,
									width :230,
									bodyStyle:'margin-top: 0px',
									items:[
										$this.firstGridPayment(),
									]
								},
								{
									border:false,
									width :20,
									bodyStyle:'margin-top: 2px',
								},
								{
									border:false,
									width :230,
									bodyStyle:'margin-top: 2px; align:right;',
									items:[
										$this.seconGridPayment(),
									]
								},{
											layout: 'column',
											border: false,
											width:500,
											items:[
											//grid customer
												{
													border:false,
													columnWidth:.47,
													//width :170,
													bodyStyle:'margin-top: 2px',
													items:[
														{
															id 			: 'CekPaymentPilihSemuaRAD',
															xtype 		: 'checkbox',
															boxLabel	: 'Pilih Semua Pembayaran',
															anchor 		: '100% 100%',
															width 		: '100%',
															listeners :  {
																check: function()
																{
																	if(Ext.getCmp('CekPaymentPilihSemuaRAD').getValue() ===true)
																	{
																		firstGridPayment.getSelectionModel().selectAll();
																	}
																	else
																	{
																		firstGridPayment.getSelectionModel().clearSelections();
																	}
																}
															}
														}
													]
												},
												{
													border:false,
													columnWidth :.03,
													bodyStyle:'margin-top: 2px',
												},
												{
													border:false,
													columnWidth:.47,
													//width :170,
													bodyStyle:'margin-top: 2px; align:right;',
													items:[
														{
															xtype 		: 'button',
															text 		: 'Reset Pilihan',
															anchor 		: '100% 100%',
															width 		: '100%',
															handler 	: function(){
																dataSource_payment.load
																(
																	{
																		params:
																		{
																			Skip: 0,
																			Take: 1000,
																			Sort: 'KD_PAY',
																			Sortdir: 'ASC',
																			target:'ViewListPayment',
																			param: " "
																		}
																	}
																);
																secondGridStoreLapPenerimaanPaymentLapPenerimaan.removeAll();
															}
														}
													]
												}
											]
										}
							]
						},
					]
				})
			]
		}).show();
	}
};
DlgLapRADPenerimaanTunaiKomponenDetail.init();
