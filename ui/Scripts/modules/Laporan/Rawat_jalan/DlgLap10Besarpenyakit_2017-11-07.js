var type_file=0;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRWJ10BesarPenyakit;
var selectNamaRWJ10BesarPenyakit;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRWJ10BesarPenyakit;
var varLapRWJ10BesarPenyakit= ShowFormLapRWJ10BesarPenyakit();
var selectSetUmum;
var selectSetkelpas;
var firstGrid;
var secondGrid;
var secondGridStore;
var dsDokterPelayaranDokter;
var dataSource_unitLapRWJ10BesarPenyakit;
var DataStore_combouserLapRWJ10BesarPenyakit;
var combouserLapRWJ10BesarPenyakit;

var selectSetPilihankelompokPasien;
var selectSetPilihanProfesi;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;
var tmp_unit = "";

function ShowFormLapRWJ10BesarPenyakit()
{
	var Field               = ['KD_DOKTER','NAMA'];
	dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
	dataStoreDokter("Dokter", null);
    frmDlgRWJ10BesarPenyakit= fnDlgRWJ10BesarPenyakit();
    frmDlgRWJ10BesarPenyakit.show();
};

function fnDlgRWJ10BesarPenyakit()
{
    var winRWJ10BesarPenyakitReport = new Ext.Window
    (
        {
            id: 'winRWJ10BesarPenyakitReport',
            title: 'Laporan 10 Besar Penyakit ',
            closeAction: 'destroy',
            width:400,
            height: 560,
            border: false,
            resizable:false,
            plain: true,
            // constrain: true,
            layout: 'form',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJ10BesarPenyakit()],
            listeners:
			{
				activate: function()
				{
					/*Ext.getCmp('cboPerseoranganRWJ').show();
					Ext.getCmp('cboAsuransiRWJ').hide();
					Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
					Ext.getCmp('cboUmumRWJ').hide();*/
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapRWJ10BesarPenyakit',
					handler: function()
					{		
						/* if(Ext.getCmp('cbPendaftaran_RWJ10BesarPenyakit').getValue()=='' && Ext.getCmp('cbTindakRWJ_RWJ10BesarPenyakit').getValue()==''){
							ShowPesanWarningRWJ10BesarPenyakitReport('Pilih laporan pendaftaran atau tindakan RWJ!','WARNING');
						} else{ */
							var params={
								//kd_profesi:Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue(),
								semua_pasien:Ext.getCmp('cbSemuaPasien_RWJ10BesarPenyakit').getValue(),
								pasien_baru:Ext.getCmp('cbPasienBaru_RWJ10BesarPenyakit').getValue(),
								pasien_lama:Ext.getCmp('cbPasienLama_RWJ10BesarPenyakit').getValue(),
								order_by:Ext.getCmp('combouserLapRWJ10BesarPenyakit').getValue(),
								//kd_dokter:GetCriteriaRWJProfesi(),
								kd_kelompok:GetCriteriaRWJPasienPerKelompok(),
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJ10BesarPenyakit').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJ10BesarPenyakit').getValue(),
								JmlList:secondGridStore.getCount(),
								full_name:Ext.get('combouserLapRWJ10BesarPenyakit').getValue(),
								type_file:type_file,
								detail:false
							} 
							var i=0;
							for(i=0; i<secondGridStore.getCount(); i++){
								params['kd_unit'+i]=secondGridStore.data.items[i].data.KD_UNIT;		

							}
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/main/cetaklaporanRWJ/laporan_sepuluhbesarpenyakit");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							console.log(params);
							form.submit();		
							// frmDlgRWJPasienPerKelompok.close(); 
						//}
					}
				},
				/* {
					xtype: 'button',
					text: 'Print',
					width: 70,
					hideLabel: true,
					id: 'btnPrintLapRWJ10BesarPenyakit',
					handler: function()
					{		
						if(Ext.getCmp('cbPendaftaran_RWJ10BesarPenyakit').getValue()=='' && Ext.getCmp('cbTindakRWJ_RWJ10BesarPenyakit').getValue()==''){
							ShowPesanWarningRWJ10BesarPenyakitReport('Pilih laporan pendaftaran atau tindakan WJ!','WARNING');
						} else{
							var params={
								kd_profesi:Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue(),
								pelayananPendaftaran:Ext.getCmp('cbPendaftaran_RWJ10BesarPenyakit').getValue(),
								pelayananTindak:Ext.getCmp('cbTindakRWJ_RWJ10BesarPenyakit').getValue(),
								kd_user:Ext.getCmp('combouserLapRWJ10BesarPenyakit').getValue(),
								kd_dokter:GetCriteriaRWJProfesi(),
								kd_kelompok:GetCriteriaRWJPasienPerKelompok(),
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJ10BesarPenyakit').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJ10BesarPenyakit').getValue(),
								JmlList:secondGridStore.getCount(),
								full_name:Ext.get('combouserLapRWJ10BesarPenyakit').getValue(),
								type_file:type_file,
								detail:false
							} 
							var i=0;
							for(i=0; i<secondGridStore.getCount(); i++){
								params['kd_unit'+i]=secondGridStore.data.items[i].data.KD_UNIT;		

							}
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_RWJTindakanDokter/cetakDirect");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							console.log(params);
							form.submit();		
							// frmDlgRWJPasienPerKelompok.close(); 
						}
					}
				}, */
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRWJ10BesarPenyakit',
					handler: function()
					{
							frmDlgRWJ10BesarPenyakit.close();
					}
				}
			
			]

        }
    );

    return winRWJ10BesarPenyakitReport;
};


function ItemDlgRWJ10BesarPenyakit()
{
    var PnlLapRWJ10BesarPenyakit = new Ext.Panel
    (
        {
            id: 'PnlLapRWJ10BesarPenyakit',
            fileUpload: true,
            layout: 'form',
			//width:400,
            height: '590',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				gridPoliklinikLapRWJ10BesarPenyakit(),
                getItemLapRWJ10BesarPenyakit_Atas(),
                /* {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        
                    ]
                } */
            ]
        }
    );

    return PnlLapRWJ10BesarPenyakit;
};

function gridPoliklinikLapRWJ10BesarPenyakit(){
	var Field_poli_viDaftarLapRWJ10BesarPenyakit = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unitLapRWJ10BesarPenyakit = new WebApp.DataStore({fields: Field_poli_viDaftarLapRWJ10BesarPenyakit});
    
    datarefresh_viInformasiUnitLapRWJ10BesarPenyakit()

    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
	];


	// declare the source Grid
		firstGrid = new Ext.grid.GridPanel({
		ddGroup          : 'secondGridDDGroup',
		store            : dataSource_unitLapRWJ10BesarPenyakit,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		enableDragDrop   : true,
		height           : 200,
		stripeRows       : true,
		trackMouseOver   : true,
		title            : 'Unit',
		anchor           : '100% 100%',
		plugins          : [new Ext.ux.grid.FilterRow()],
		colModel         : new Ext.grid.ColumnModel
		(
			[
					new Ext.grid.RowNumberer(),
					{
							id: 'colNRM_viDaftar',
							header: 'No.Medrec',
							dataIndex: 'KD_UNIT',
							sortable: true,
							hidden : true
					},
					{
							id: 'colNMPASIEN_viDaftar',
							header: 'Nama',
							dataIndex: 'NAMA_UNIT',
							sortable: true,
							width: 50
					}
			]
		),
		listeners : {
			afterrender : function(comp) {
				var firstGridDropTargetEl =  firstGrid.getView().scroller.dom;
				var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
						ddGroup    : 'firstGridDDGroup',
						notifyDrop : function(ddSource, e, data){
								var records =  ddSource.dragData.selections;
								Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
								firstGrid.store.add(records);
								firstGrid.store.sort('KD_UNIT', 'ASC');
								tmp_unit = "";

								for(i=0; i<secondGridStore.data.length; i++){
									// params['kd_unit'+i]=;		
									// console.log(secondGridStore.modified[i].data.KD_UNIT);
									tmp_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";

								}
								tmp_unit = tmp_unit.substr(0, tmp_unit.length-1);
								// console.log(tmp_unit);
								dataStoreDokter(Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue(), tmp_unit);
								return true;
						}
				});
			}
		},
		viewConfig: 
			{
					forceFit: true
			}
	});

	secondGridStore = new Ext.data.JsonStore({
		fields : fields,
		root   : 'records'
	});

	// create the destination Grid
	secondGrid = new Ext.grid.GridPanel({
			ddGroup          : 'firstGridDDGroup',
			store            : secondGridStore,
			columns          : cols,
			enableDragDrop   : true,
			height           : 200,
			stripeRows       : true,
			autoExpandColumn : 'KD_UNIT',
			title            : 'Unit',
			listeners : {
			afterrender : function(comp) {
			var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
			var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'secondGridDDGroup',
					notifyDrop : function(ddSource, e, data){
							// Combo_SelectRWJProfesi(Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue());
							// Ext.getCmp('cboDokterRWJ10BesarPenyakit').removeAll();
							// dsDokterPelayaran.removeAll();
							tmp_unit = "";
							var records =  ddSource.dragData.selections;
							Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
							secondGrid.store.add(records);
							secondGrid.store.sort('KD_UNIT', 'ASC');
							for(i=0; i<secondGridStore.data.length; i++){
								// params['kd_unit'+i]=;		
								// console.log(secondGridStore.modified[i].data.KD_UNIT);
								tmp_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";
							}
							tmp_unit = tmp_unit.substr(0, tmp_unit.length-1);
							// console.log(Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue());
//							dataStoreDokter(Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue(), tmp_unit);
							// mComboDokterRWJ10BesarPenyakit(tmp_unit);
							// mComboDokterRWJPelayananPerawat(tmp_unit);
							return true;
					}
			});
			}
		},
		viewConfig: 
			{
					forceFit: true
			}
	});
	
	var FrmTabs_viInformasiUnitdokter = new Ext.Panel
	({
		    
		    closable: true,
		    region: 'center',
		    layout: 'column',
			height: 220,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[firstGrid
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid
						
					]
				},
				
				
			],

    })
    return FrmTabs_viInformasiUnitdokter;
}

function getItemLapRWJ10BesarPenyakit_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 275,
            anchor: '100% 100%',
            items: [
			{
				x: 10,
				y: 0,
				xtype: 'checkbox',
				id: 'CekLapPilihSemuaRWJ10BesarPenyakit',
				hideLabel:false,
				boxLabel: 'Pilih Semua Unit',
				checked: false,
				listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihSemuaRWJ10BesarPenyakit').getValue()===true)
						{
							 firstGrid.getSelectionModel().selectAll();
						}
						else
						{
							firstGrid.getSelectionModel().clearSelections();
						}
					}
			   }
			},
			{
				x: 196,
				y: 0,
				xtype: 'checkbox',
				id: 'CekLapResetRWJ10BesarPenyakit',
				hideLabel:false,
				boxLabel: 'Reset Unit',
				checked: false,
				listeners: 
			   {
					check: function()
					{
						if(Ext.getCmp('CekLapResetRWJ10BesarPenyakit').getValue()===true)
						{
							secondGridStore.removeAll();
							tmp_unit = "";
							dataStoreDokter(Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue(), tmp_unit);
							datarefresh_viInformasiUnitLapRWJ10BesarPenyakit();
							Ext.getCmp('CekLapPilihSemuaRWJ10BesarPenyakit').setValue(false);
						} else{
							
						}
					}
			   }
			},
			{
                x: 100,
                y: 40,
                xtype: 'radio',
				boxLabel: 'Semua Pasien',
				checked: true,
                id:'cbSemuaPasien_RWJ10BesarPenyakit',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('cbPasienBaru_RWJ10BesarPenyakit').setValue(false);
						Ext.getCmp('cbPasienLama_RWJ10BesarPenyakit').setValue(false);
                    }else
                    {
                        Ext.getCmp('cbSemuaPasien_RWJ10BesarPenyakit').setValue(false);
                    }
                }
            },
			{
                x: 200,
                y: 40,
                xtype: 'radio',
				boxLabel: 'Pasien Baru',
                id:'cbPasienBaru_RWJ10BesarPenyakit',
                handler: function (field, value) 
                {if (value === true)
                    {
						Ext.getCmp('cbSemuaPasien_RWJ10BesarPenyakit').setValue(false);
						Ext.getCmp('cbPasienLama_RWJ10BesarPenyakit').setValue(false);
                    }else
                    {
                        Ext.getCmp('cbPasienBaru_RWJ10BesarPenyakit').setValue(false);
                    }
                }
            },
			{
                x: 300,
                y: 40,
                xtype: 'radio',
				boxLabel: 'Pasien Lama',
                id:'cbPasienLama_RWJ10BesarPenyakit',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('cbPasienBaru_RWJ10BesarPenyakit').setValue(false);
						Ext.getCmp('cbSemuaPasien_RWJ10BesarPenyakit').setValue(false);
                    }else
                    {
                        Ext.getCmp('cbPasienLama_RWJ10BesarPenyakit').setValue(false);
                    }
                }
            },
			/* {
				xtype: 'checkboxgroup',
				width:290,
				x: 100,
				y: 40,
				items: 
				[
					{
						
						boxLabel: 'Semua Pasien',
						name: 'cbSemuaPasien_RWJ10BesarPenyakit',
						id : 'cbSemuaPasien_RWJ10BesarPenyakit'
					},
					{
						boxLabel: 'Pasien Baru',
						name: 'cbPasienBaru_RWJ10BesarPenyakit',
						id : 'cbPasienBaru_RWJ10BesarPenyakit'
					},
					{
						boxLabel: 'Pasien Lama',
						name: 'cbPasienLama_RWJ10BesarPenyakit',
						id : 'cbPasienLama_RWJ10BesarPenyakit'
					}
			   ]
			}, */
            //  ================================================================================== POLIKLINIK
			/* {
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Poliklinik '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
				mCombounitRWJ10BesarPenyakit(), */
            //  ================================================================================== DOKTER

			{
				x: 10,
				y: 60,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, {
				x: 110,
				y: 60,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 60,
				xtype: 'datefield',
				id: 'dtpTglAwalLapRWJ10BesarPenyakit',
				format: 'd/M/Y',
                value: now,
				//value: tigaharilalu
			}, {
				x: 230,
				y: 60,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 260,
				y: 60,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapRWJ10BesarPenyakit',
				format: 'd/M/Y',
				value: now,
				width: 100
			},{
				x: 10,
				y: 90,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 90,
				xtype: 'label',
				text: ' : '
			},
				mComboRWJPasienPerKelompok(),
				mComboRWJPasienPerKelompokSEMUA(),
				mComboRWJPasienPerKelompokPERORANGAN(),
				mComboRWJPasienPerKelompokPERUSAHAAN(),
				mComboRWJPasienPerKelompokASURANSI(),
			{
				x: 10,
				y: 150,
				xtype: 'label',
				text: 'Order By '
			}, {
				x: 110,
				y: 150,
				xtype: 'label',
				text: ' : '
			},
			ComboOrderByLapRWJ10BesarPenyakit(),
			{
				x: 10,
				y: 180,
				xtype: 'label',
				text: 'Type File '
			}, {
				x: 110,
				y: 180,
				xtype: 'label',
				text: ' : '
			},
			{
				x: 120,
				y: 180,
				xtype: 'checkbox',
				id: 'CekLapPilihTypeExcel',
				hideLabel:false,
				boxLabel: 'Excel',
				checked: false,
				listeners: 
				{
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
						{
							type_file=1;
						}
						else
						{
							type_file=0;
						}
					}
			   }
			}
            ]
        },
		]
    };
    return items;
};

function ShowPesanWarningRWJ10BesarPenyakitReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function datarefresh_viInformasiUnitLapRWJ10BesarPenyakit()
{
    dataSource_unitLapRWJ10BesarPenyakit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnitB',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )
    //alert("refersh")
}

function ComboOrderByLapRWJ10BesarPenyakit(){
		var Field = ['kd_user','full_name'];
    	DataStore_combouserLapRWJ10BesarPenyakit = new WebApp.DataStore({fields: Field});
		//loaduserLapRWJ10BesarPenyakit();
    	combouserLapRWJ10BesarPenyakit = new Ext.form.ComboBox({
			typeAhead: true,
			x: 120,
			y: 150,
			id:'combouserLapRWJ10BesarPenyakit',
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'No Medrec'], [2, 'Tanggal Masuk']]
                    }
                ),
			valueField: 'Id',
            displayField: 'displayText',
			value:'No Medrec',
			listeners:{
				'select': function(a,b,c){
				}
			}
		});
		return combouserLapRWJ10BesarPenyakit;
}

function loaduserLapRWJ10BesarPenyakit(){
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/lap_RWJPelayananDokter/getUser",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			DataStore_combouserLapRWJ10BesarPenyakit.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  DataStore_combouserLapRWJ10BesarPenyakit.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				DataStore_combouserLapRWJ10BesarPenyakit.add(recs);
			}
		}
	});
}

function dataStoreDokter(level = null, kd_unit = null){
	var criteriaUnit        = "";
	var criteriaDokter      = "";
	if (kd_unit == '') { kd_unit = null; }
	dsDokterPelayaranDokter.removeAll();
	// console.log(level);
	if (level == "Dokter" || level == 1 ) {
		criteriaDokter = " dokter.jenis_dokter='1' ";
	}else{
		criteriaDokter = " left(dokter_klinik.kd_unit, 1)='2' AND dokter.jenis_dokter='0' ";
	}
	if (kd_unit != null) {
		criteriaUnit = " AND dokter_klinik.kd_unit in ("+kd_unit+") ";
	}
	// dsDokterPelayaranDokter.removeAll();
    dsDokterPelayaranDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: " WHERE "+criteriaDokter+criteriaUnit
            }
        }
    );
}

function mComboDokterRWJ10BesarPenyakit()
{
	// console.log(dsDokterPelayaranDokter);
    var cboPilihanRWJ10BesarPenyakit = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboDokterRWJ10BesarPenyakit',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:false,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanRWJ10BesarPenyakit;
};

function mComboDokterRWJPelayananPerawat(kd_unit = null)
{
    var cboPilihanRWJ10BesarPenyakit = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboDokterRWJPelayananPerawat',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:false,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanRWJ10BesarPenyakit;
};


function mCombounitRWJ10BesarPenyakit()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewCombounit_Konfigurasi',
                    param: " "
                }
            }
        );

    var cbounitRequestEntryRWJ10BesarPenyakit = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 70,
            id: 'cbounitRequestEntryRWJ10BesarPenyakit',
			width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryRWJ10BesarPenyakit;
};


function mComboRWJPasienPerKelompok()
{
    var cboPilihanRWJ10BesarPenyakitkelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 90,
                id:'cboPilihanRWJPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectRWJPasienPerKelompok(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanRWJ10BesarPenyakitkelompokPasien;
};

function mComboRWJJenisProfesi()
{
    var cboPilihanRWJJenisProfesi = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'IDcboPilihanRWJJenisProfesi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Dokter'], [2, 'Perawat']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
						tmp_unit = "";
						for(i=0; i<secondGridStore.data.length; i++){
							// params['kd_unit'+i]=;		
							// console.log(secondGridStore.modified[i].data.KD_UNIT);
							tmp_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";
						}
						tmp_unit = tmp_unit.substr(0, tmp_unit.length-1);
                    	dataStoreDokter(b.data.displayText, tmp_unit);
                        selectSetPilihanProfesi=b.data.displayText;
                        // Combo_SelectRWJProfesi(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanRWJJenisProfesi;
};

//RWJPasienPerKelompok
function mComboRWJPasienPerKelompokSEMUA()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 120,
                id:'IDmComboRWJPasienPerKelompokSEMUA',
                typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				selectOnFocus:true,
				forceSelection: true,
				emptyText:'Silahkan Pilih...',
				valueField: 'Id',
	            displayField: 'displayText',
	            hidden:false,
				fieldLabel: '',
				width: 240,
				value:1,
				store: new Ext.data.ArrayStore
				(
						{
							id: 0,
							fields:
								[
										'Id',
										'displayText'
								],
							data: [[1, 'Semua']]
						}
				),
				listeners:
				{
					'select': function(a,b,c)
					{
						selectSetUmum=b.data.displayText ;
					}
	                                
	                            
				}
            }
	);
	return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokPERORANGAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 120,
                id:'IDmComboRWJPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokPERUSAHAAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='1' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 120,
                id:'IDmComboRWJPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
				//value: selectsetperusahaan,
                listeners:
                {
				    'select': function(a,b,c)
					{
				        selectsetperusahaan = b.data.KD_CUSTOMER;
						selectsetnamaperusahaan = b.data.CUSTOMER;
					}
                }
            }
	);
	return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokASURANSI()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='2' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 120,
                id:'IDmComboRWJPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
				valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
				    'select': function(a,b,c)
					{
						selectSetAsuransi=b.data.KD_CUSTOMER ;
						selectsetnamaAsuransi = b.data.CUSTOMER ;
					}
                }
            }
	);
	return cboPerseoranganRWJ;
};


function Combo_SelectRWJPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').show();
   }
};

function Combo_SelectRWJProfesi(combo)
{
   var value = combo;

   if(value === "Dokter")
   {    
        Ext.getCmp('cboDokterRWJPelayananPerawat').show();
        Ext.getCmp('cboDokterRWJ10BesarPenyakit').hide();
   }
   else if(value === "Perawat")
   {    
        Ext.getCmp('cboDokterRWJ10BesarPenyakit').show();
        Ext.getCmp('cboDokterRWJPelayananPerawat').hide();
        
   }
   else
   {
        Ext.getCmp('cboDokterRWJPelayananPerawat').show();
        Ext.getCmp('cboDokterRWJ10BesarPenyakit').hide();
   }
};


function GetCriteriaRWJPasienPerKelompok()
{
    var strKriteria = '';
    
    if (Ext.getCmp('cboPilihanRWJPasienPerKelompok').getValue() !== '')
    {
        if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').getValue(); } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').getValue(); } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').getValue(); }
    }else{
            strKriteria = 'Semua';
    }

    
    return strKriteria;
};

function GetCriteriaRWJProfesi()
{
	var strKriteria = '';
	
	if (Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue() !== '')
	{
		if (Ext.get('IDcboPilihanRWJJenisProfesi').getValue() === 'Semua') {
			strKriteria = 'SEMUA'; 
		} else if (Ext.get('IDcboPilihanRWJJenisProfesi').getValue() === 'Dokter'){ 
			strKriteria = Ext.getCmp('cboDokterRWJ10BesarPenyakit').getValue(); 
		} else if (Ext.get('IDcboPilihanRWJJenisProfesi').getValue() === 'Perawat'){ 
			strKriteria = Ext.getCmp('cboDokterRWJ10BesarPenyakit').getValue(); 
		} 
	}else{
        strKriteria = 'SEMUA';
	}

	
	return strKriteria;
};

function ValidasiReportRWJPasienPerDokter()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJPasienPerDokter').dom.value === '')
	{
		ShowPesanWarningRWJ10BesarPenyakitReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
    return x;
};
