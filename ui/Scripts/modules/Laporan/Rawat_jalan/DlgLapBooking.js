var type_file=0; //type file 0 = pdf ; 1 = excel
var tanggal = new Date();
var now = tanggal.format('Y-m-d');
var selectCount_viInformasiUnitdokter=50;
var icons_viInformasiUnitdokter="Gaji";
var dataSource_unit;
var frmDlgRWJBooking;
var secondGridStore;
var varLapRWJBooking= ShowFormLapRWJBooking();
var selectSetUmum;
var firstGrid;
var secondGrid;
var CurrentData_viDaftarRWJ =
{
    data: Object,
    details: Array,
    row: 0
};


function ShowFormLapRWJBooking()
{
    frmDlgRWJBooking= fnDlgRWJBooking();
    frmDlgRWJBooking.show();
};

function fnDlgRWJBooking()
{
    var winRWJBookingReport = new Ext.Window
    (
        {
            id: 'winRWJBookingReport',
            title: 'Laporan Pasien Booking Online',
            closeAction: 'destroy',
            width:650,
            height: 460,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [dataGrid_viInformasiUnit()],
            listeners:
        {
            activate: function()
            {
                
            }
        }

        }
    );

    return winRWJBookingReport;
};


function ItemDlgRWJBooking()
{
    var PnlLapRWJBooking = new Ext.Panel
    (
        {
            id: 'PnlLapRWJBooking',
            fileUpload: true,
            layout: 'form',
            height: '640',
            anchor: '99%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                dataGrid_viInformasiUnit(),

            ]
        }
        
    );
    return PnlLapRWJBooking;
};

/*function ValidasiTanggalReportRWJBooking()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJBooking').dom.value > Ext.get('dtpTglAkhirLapRWJBooking').dom.value)
    {
        ShowPesanWarningRWJBookingReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};*/

function ShowPesanWarningRWJBookingReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function dataGrid_viInformasiUnit(mod_id)
{
    var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    datarefresh_viInformasiUnit()

    // Generic fields array to use in both store defs.
    var fields = [
        {name: 'KD_UNIT', mapping : 'KD_UNIT'},
        {name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
    ];


    // Column Model shortcut array
    var cols = [
        { id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
        {header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
    ];


    // declare the source Grid
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 200,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'KD_UNIT',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA_UNIT',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),
                listeners : {
                    afterrender : function(comp) {
                    var firstGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
   
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStore,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 200,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_UNIT',
                    title            : 'Unit',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'secondGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid.store.add(records);
                                    secondGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

    
    var FrmTabs_viInformasiUnit = new Ext.Panel
        (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'column',
            height       : 320,
            shadhow: true,
            margins: '0 5 5 0',
            anchor: '99%',
            iconCls: icons_viInformasiUnitdokter,
            items: 
            [
                {
                    columnWidth: .50,
                    layout: 'form',
                    border: false,
                    autoScroll: true,
                    bodyStyle: 'padding: 10px 10px 10px 10px',
                    anchor: '100% 100%',
                    items:
                    [firstGrid
                        
                        
                    ]
                },
                {
                    columnWidth: .50,
                    layout: 'form',
                    bodyStyle: 'padding: 10px 10px 10px 10px',
                    border: false,
                    anchor: '100% 100%',
                    items:
                    [secondGrid
                        
                    ]
                },
                                {
                                        
                                            columnWidth: .40,
                                            layout: 'form',
                                            border: false,
                                            labelAlign: 'right',
                                            labelWidth: 100,
                                            items:
                                            [
                                            {
                                                   xtype: 'checkbox',
                                                   id: 'CekLapPilihSemuaRWJDetail',
                                                   hideLabel:false,
                                                   boxLabel: 'Pilih Semua',
                                                   checked: false,
                                                   listeners: 
                                                   {
                                                        check: function()
                                                        {
                                                           if(Ext.getCmp('CekLapPilihSemuaRWJDetail').getValue()===true)
                                                            {
                                                                 firstGrid.getSelectionModel().selectAll();
                                                            }
                                                            else
                                                            {
                                                                firstGrid.getSelectionModel().clearSelections();
                                                            }
                                                        }
                                                   }
                                                },
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: 'Tgl Kunjungan',
                                                    id: 'dtpTglAwalLapRWJBooking',
                                                    format: 'd-M-Y',
                                                    value:now,
                                                    anchor: '95%'
                                                },  
                                            {  
                                                xtype: 'combo',
                                                fieldLabel: 'Order By',
                                                id: 'CboOrder',
                                                editable: false,
                                                store: new Ext.data.ArrayStore
                                                    (
                                                        {
                                                        id: 0,
                                                        fields:
                                                        [
                                                            'Id',
                                                            'displayText'
                                                        ],
                                                           data: [[0, 'Medrec'],[1, 'Nama Pasien'], [2, 'Tanggal'],]
                                                        }
                                                    ),
                                                displayField: 'displayText',
                                                mode: 'local',
                                                width: 100,
                                                forceSelection: true,
                                                triggerAction: 'all',
                                                // emptyText: 'Medrec',
                                                selectOnFocus: true,
                                                anchor: '95%',
                                                value:'Medrec',
                                                listeners:
                                                 {
                                                    'select': function(a, b, c)
                                                    {
                                                    },

                                                }
                                            },
                                            
                                            ]
                                        },
                                        {
                                            columnWidth: .30,
                                            layout: 'form',
                                            border: false,
                                            labelWidth: 30,
                                            items:
                                            [
                                                {
                                                fieldLabel: '',
                                                height:25,
                                                border:false,
                                                labelSeparator: '',
                                                boxLabel: 'Semua Data',
                                            },
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: 's/d ',
                                                    id: 'dtpTglAkhirLapRWJLaporandetail',
                                                    format: 'd-M-Y',
                                                    value:now,
                                                    anchor: '80%'
                                                }
                                            ]
                                        },                                      
                                {
                                columnWidth: .99,
                                layout: 'hBox',
                                border: false,
                                defaults: { margins: '0 2 0 0' },
                                style:{'margin-left':'398px','margin-top':'0px'},
                                anchor: '100%',
                                /* layoutConfig:
                                {
                                    padding: '3',
                                    pack: 'end',
                                    align: 'middle'
                                }, */
                                items:
                                [
                                    
                                    
                                    /*{
                                        xtype: 'button',
                                        text: 'Print',
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnOkLapRWJBooking',
                                        handler: function()
                                        {
                                            var sendDataArray = [];
                                            secondGridStore.each(function(record){
                                            var recordArray = [record.get("KD_UNIT")];
                                            sendDataArray.push(recordArray);
                                            
                                            });
                                    
                                            
                                           if (sendDataArray.length === 0)
                                            {
                                                   ShowPesanWarningRWJBookingReport('Isi kriteria unit dengan drag and drop','Laporan')
                                                 
                                            }else{
                                                var params={
                                                            
                                                            TglAwal         :Ext.getCmp('dtpTglAwalLapRWJBooking').getValue(), //split[0]
                                                            TglAkhir        :Ext.getCmp('dtpTglAkhirLapRWJLaporandetail').getValue(), //split[1]
                                                            JmlList         :secondGridStore.getCount(), //split[3]
                                                            Type_File       :type_file,
                                                            order_by        :Ext.getCmp('CboOrder').getValue() //split[6],
                                                } ;
                                                var i=0;
                                                for(i=0; i<secondGridStore.getCount(); i++){
                                                    params['kd_unit'+i]=secondGridStore.data.items[i].data.KD_UNIT;     
        
                                                }
                                                console.log(params);
                                                var form = document.createElement("form");
                                                form.setAttribute("method", "post");
                                                form.setAttribute("target", "_blank");
                                                // form.setAttribute("action", baseURL + "index.php/main/cetaklaporanRWJ/cetakDirectlaporanRWJ");
                                                form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_register_detail/cetak_direct");
                                                var hiddenField = document.createElement("input");
                                                hiddenField.setAttribute("type", "hidden");
                                                hiddenField.setAttribute("name", "data");
                                                hiddenField.setAttribute("value", Ext.encode(params));
                                                form.appendChild(hiddenField);
                                                document.body.appendChild(form);
                                                form.submit();  
                                                frmDlgRWJBooking.close();
                                             
                                            };
                                        }
                                    },*/
                                    {
                                        xtype: 'button',
                                        text: 'Preview',
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnPreviewLapRWJBooking',
                                        handler: function()
                                        {
                                            var sendDataArray = [];
                                            secondGridStore.each(function(record){
                                            var recordArray = [record.get("KD_UNIT")];
                                            sendDataArray.push(recordArray);
                                            
                                            });
                                    
                                            
                                           if (sendDataArray.length === 0)
                                            {
                                                   ShowPesanWarningRWJBookingReport('Isi kriteria unit dengan drag and drop','Laporan')
                                                 
                                            }else{
                                                
                                                var params={
                                                            
                                                            TglAwal:Ext.getCmp('dtpTglAwalLapRWJBooking').getValue(), //split[0]
                                                            TglAkhir:Ext.getCmp('dtpTglAkhirLapRWJLaporandetail').getValue(), //split[1]
                                                            JmlList:secondGridStore.getCount(), //split[3]
                                                            Type_File:type_file,
                                                            order_by        :Ext.getCmp('CboOrder').getValue(), //split[6]
                                                } ;
                                                var i=0;
                                                for(i=0; i<secondGridStore.getCount(); i++){
                                                    params['kd_unit'+i]=secondGridStore.data.items[i].data.KD_UNIT;     
        
                                                }
                                                console.log(params);
                                                var form = document.createElement("form");
                                                form.setAttribute("method", "post");
                                                form.setAttribute("target", "_blank");
                                                form.setAttribute("action", baseURL + "index.php/main/cetaklaporanRWJ/cetaklaporanRWJ_Booking");
                                                var hiddenField = document.createElement("input");
                                                hiddenField.setAttribute("type", "hidden");
                                                hiddenField.setAttribute("name", "data");
                                                hiddenField.setAttribute("value", Ext.encode(params));
                                                form.appendChild(hiddenField);
                                                document.body.appendChild(form);
                                                form.submit();  
                                                frmDlgRWJBooking.close();
                                             
                                            };
                                        }
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Cancel' ,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnCancelLapRWJBooking',
                                        handler: function()
                                        {
                                                frmDlgRWJBooking.close();
                                        }
                                    }
                                    ]
                                }
            ],

    }
    )
    // datarefresh_viInformasiUnitdokter();
    return FrmTabs_viInformasiUnit;
}

function DataInputKriteria()
{
    var FrmTabs_DataInputKriteria = new Ext.Panel
        (
        {
            id: FrmTabs_DataInputKriteria,
            closable: true,
            region: 'center',
            layout: 'column',
            height       : 100,
            title:  'Dokter Unit',
            itemCls: 'blacklabel',
            bodyStyle: 'padding:15px',
            border: false,
            shadhow: true,
            margins: '0 5 5 0',
            anchor: '100%',
            iconCls: icons_viInformasiUnitdokter,
            items: 
            [
                {
                    columnWidth: .50,
                    layout: 'form',
                    border: false,
                    autoScroll: true,
                    bodyStyle: 'padding: 10px 10px 10px 10px',
                    items:
                    []
                },
                {
                    columnWidth: .50,
                    layout: 'form',
                    bodyStyle: 'padding: 10px 10px 10px 10px',
                    border: false,
                    anchor: '100% 100%',
                    items:
                    []
                }
            ]
        
    }
    )
    
    return FrmTabs_DataInputKriteria;
}
        

function datarefresh_viInformasiUnit()
{
    dataSource_unit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnitB',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )
    //alert("refersh")
}



    

    
