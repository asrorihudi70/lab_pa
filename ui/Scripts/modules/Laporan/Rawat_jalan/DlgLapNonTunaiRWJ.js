var type_file=0;
var dsRWJTransKompDetRWJPAV;
var selectRWJTransKompDetRWJPAV;
var selectNamaRWJTransKompDetRWJPAV;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJTransKompDetRWJPAV;
var varLapRWJTransKompDetRWJPAV= ShowFormLapRWJTransKompDetRWJPAV();
var dsRWJ;

function ShowFormLapRWJTransKompDetRWJPAV()
{
    frmDlgRWJTransKompDetRWJPAV= fnDlgRWJTransKompDetRWJPAV();
    frmDlgRWJTransKompDetRWJPAV.show();
};

function fnDlgRWJTransKompDetRWJPAV()
{
    var winRWJTransKompDetRWJPAVReport = new Ext.Window
    (
        {
            id: 'winRWJTransKompDetRWJPAVReport',
            title: 'Laporan Non Tunai RWJ',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJTransKompDetRWJPAV()]

        }
    );

    return winRWJTransKompDetRWJPAVReport;
};

function ItemDlgRWJTransKompDetRWJPAV()
{
    var PnlLapRWJTransKompDetRWJPAV = new Ext.Panel
    (
        {
            id: 'PnlLapRWJTransKompDetRWJPAV',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJTransKompDetRWJPAV_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJTransKompDetRWJPAV',
					handler: function()
					{
					   if (ValidasiReportRWJTransKompDetRWJPAV() === 1)
					   {
							var params={
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJTransKompDetRWJPAV').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJTransKompDetRWJPAV').getValue(),
								type_file:type_file
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/main/cetaklaporanRWJ/cetakRWJNonTunai");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJTransKompDetRWJPAV.close(); 
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJTransKompDetRWJPAV',
					handler: function()
					{
						frmDlgRWJTransKompDetRWJPAV.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJTransKompDetRWJPAV;
};


function ValidasiReportRWJTransKompDetRWJPAV()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJTransKompDetRWJPAV').dom.value === '')
	{
		ShowPesanWarningRWJTransKompDetRWJPAVReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}

    return x;
};

function ValidasiTanggalReportRWJTransKompDetRWJPAV()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJTransKompDetRWJPAV').dom.value > Ext.get('dtpTglAkhirLapRWJTransKompDetRWJPAV').dom.value)
    {
        ShowPesanWarningRWJTransKompDetRWJPAVReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJTransKompDetRWJPAVReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJTransKompDetRWJPAV_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJTransKompDetRWJPAV_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJTransKompDetRWJPAV',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					{
					   xtype: 'checkbox',
					   fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					}
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJTransKompDetRWJPAV',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


