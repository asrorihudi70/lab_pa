var fields = [
	{name: 'KD_UNIT', mapping : 'KD_UNIT'},
	{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];

var fieldsCustomer = [
	{name: 'KD_CUSTOMER', mapping : 'KD_CUSTOMER'},
	{name: 'CUSTOMER', mapping : 'CUSTOMER'}
];

var fieldsPayment = [
	{name: 'KD_PAY', mapping : 'KD_PAY'},
	{name: 'URAIAN', mapping : 'URAIAN'}
];

var cols = [
	{ id : 'kd_unit', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'kd_unit',hidden : true},
	{ header: "Nama", width: 50, sortable: true, dataIndex: 'nama_unit'}
];

var colsPayment = [
	{ id : 'kd_pay', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'kd_pay',hidden : true},
	{ header: "Uraian", width: 50, sortable: true, dataIndex: 'uraian'}
];

var colsCustomer = [
	{ id : 'kd_customer', header: "Kode Customer", width: 160, sortable: true, dataIndex: 'kd_customer',hidden : true},
	{ header: "Customer", width: 50, sortable: true, dataIndex: 'customer'}
];

var secondGridStoreLapPenerimaan;
var secondGridStoreLapPenerimaanPaymentLapPenerimaan;
var secondGridStoreLapPenerimaanCustomer;
var kelpas=-1;
var firstGrid_customer;
var DlgLapRWJPenerimaan={
	vars:{
		comboSelect:null
	},
	coba:function(){
		var $this=this;
		$this.DateField.startDate
	},
	ArrayStore:{
		dokter:new Ext.data.ArrayStore({fields:[]})
	},
	CheckboxGroup:{
		shift:null,
		tindakan:null,
		transfer:null
	},
	ComboBox:{
		kelPasien1:null,
		combo1:null,
		combo2:null,
		combo3:null,
		combo0:null,
		poliklinik:null,
		dokter:null,
		combouser:null
	},
	DataStore:{
		combo2:null,
		combo3:null,
		poliklinik:null,
		combo1:null,
		combouser:null,
		gridcustomer:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	comboOnSelect:function(val){
		var $this=this;
		if(val==-1){
			kelpas =-1;
			$this.loadcustomer(kelpas);
			secondGridStoreLapPenerimaanCustomer.removeAll();
		}else if(val==0){
			kelpas =0;
			$this.loadcustomer(kelpas);
			secondGridStoreLapPenerimaanCustomer.removeAll();
		}else if(val==1){
			kelpas =1;
			$this.loadcustomer(kelpas);
			secondGridStoreLapPenerimaanCustomer.removeAll();
		}else if(val==2){
			kelpas =2;
			$this.loadcustomer(kelpas);
			secondGridStoreLapPenerimaanCustomer.removeAll();
		}
	},
	initPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		var sendDataArrayUnit    = [];
		var sendDataArraypayment = [];
		var sendDataArrayCustomer = []; 

		secondGridStoreLapPenerimaan.each(function(record){
			var recordArray   = [record.get("kd_unit")];
			sendDataArrayUnit.push(recordArray);
		});

		secondGridStoreLapPenerimaanPaymentLapPenerimaan.each(function(record){
			var recordArraypay= [record.get("kd_pay")];
			sendDataArraypayment.push(recordArraypay);
		});

		secondGridStoreLapPenerimaanCustomer.each(function(record){
			var recordArray   = [record.get("kd_customer")];
			sendDataArrayCustomer.push(recordArray);
		});
		
		if (sendDataArrayUnit.length === 0)
		{  
			this.messageBox('Peringatan','Isi kriteria unit dengan drag and drop','WARNING');
			loadMask.hide();
		}else if (sendDataArraypayment.length === 0){  
			this.messageBox('Peringatan','Isi cara membayar dengan drag and drop','WARNING');
			loadMask.hide();
		}else{
			params['start_date'] = timestimetodate($this.DateField.startDate.getValue());
			params['last_date']  = timestimetodate($this.DateField.endDate.getValue());
			params['pasien']     = $this.ComboBox.kelPasien1.getValue();
			
			params['tmp_kd_unit'] 	= sendDataArrayUnit;
			params['tmp_payment'] 	= sendDataArraypayment;
			var pasien=parseInt($this.ComboBox.kelPasien1.getValue());
			if(pasien>=0){
				params['tmp_kd_customer'] 	= sendDataArrayCustomer;
			}else{
				params['tmp_kd_customer'] 	= '';
			}
			
			params['kd_user'] =$this.ComboBox.combouser.getValue();
			params['type_file'] =Ext.getCmp('type_file').getValue();
			var shift    =$this.CheckboxGroup.shift.items.items;
			var tindakan =$this.CheckboxGroup.tindakan.items.items;
			var transfer =$this.CheckboxGroup.transfer.items.items;
			var shifta=false;
			var tindakan_stat=false;
			for(var i=0;i<shift.length ; i++){
				params['shift'+i]=shift[i].checked;
				if(shift[i].checked==true)shifta=true;
			}
			for(var i=0;i<tindakan.length ; i++){
				params['tindakan'+i]=tindakan[i].checked;
				if(tindakan[i].checked==true)tindakan_stat=true;
			}
			for(var i=0;i<transfer.length ; i++){
				params['transfer'+i]=transfer[i].checked;
				if(transfer[i].checked==true)transfer_stat=true;
			}
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("target", "_blank");
			form.setAttribute("action", baseURL + "index.php/rawat_jalan/function_laporan_PTP/laporan_summary");
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", "data");
			hiddenField.setAttribute("value", Ext.encode(params));
			form.appendChild(hiddenField);
			document.body.appendChild(form);
			form.submit();
			loadMask.hide();
			
		} 
		
	},
	messageBox:function(modul, str, icon){
		Ext.MessageBox.show
		(
			{
				title: modul,
				msg:str,
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.icon,
				width:300
			}
		);
	},
	loaduser:function(){
		var $this=this;
		Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/function_laporan_PTP/getUser",
				params: '0',
				failure: function(o){
					var cst = Ext.decode(o.responseText);
				},	    
				success: function(o) {
					$this.ComboBox.combouser.store.removeAll();
					var cst = Ext.decode(o.responseText);

					for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
						var recs    = [],recType =  $this.DataStore.combouser.recordType;
						var o=cst['listData'][i];
				
						recs.push(new recType(o));
						$this.DataStore.combouser.add(recs);
						console.log(o);
					}
				}
			});
	},
	getUser:function(){
		var $this=this;
		var Field = ['kd_user','full_name'];
    	$this.DataStore.combouser = new WebApp.DataStore({fields: Field});
		$this.loaduser();
    	$this.ComboBox.combouser = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: $this.DataStore.combouser,
			valueField: 'kd_user',
			displayField: 'full_name',
			value:'Semua',
			listeners:{
				'select': function(a,b,c){
				}
			}
		});
		return $this.ComboBox.combouser;
	},
	
	seconGridPenerimaan : function(){
		
		var secondGrid;
		secondGridStoreLapPenerimaan = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroup',
					store            : secondGridStoreLapPenerimaan,
					columns          : cols,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 180,
					stripeRows       : true,
					autoExpandColumn : 'kd_unit',
					title            : 'Unit yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroup',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGrid.store.add(records);
											secondGrid.store.sort('kd_unit', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGrid;
	},
	firstGridPenerimaan : function(){
		var firstGrid;
		var dataSource_unit;
		var Field_poli_viDaftar = ['kd_unit','nama_unit'];
		dataSource_unit         = new WebApp.DataStore({fields: Field_poli_viDaftar});
		
		Ext.Ajax.request({
			url: baseURL + "index.php/rawat_jalan/function_laporan_PTP/getUnit",
			params: {
				kd_bagian : '2',
			},
			failure: function(o){
				var cst = Ext.decode(o.responseText);
			},	    
			success: function(o) {
				 firstGrid.store.removeAll();
				var cst = Ext.decode(o.responseText);

				for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
					var recs    = [],recType =  dataSource_unit.recordType;
					var o=cst['listData'][i];
			
					recs.push(new recType(o));
					dataSource_unit.add(recs);
					console.log(o);
				}
			}
		});
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 180,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'kd_unit',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'nama_unit',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
			listeners : {
				afterrender : function(comp) {
					var secondGridDropTargetEl = firstGrid.getView().scroller.dom;
					var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroup',
							notifyDrop : function(ddSource, e, data){
									var records =  ddSource.dragData.selections;
									Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
									firstGrid.store.add(records);
									firstGrid.store.sort('kd_unit', 'ASC');
									return true
							}
					});
				}
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid;
	},
	//
	
	seconGridPenerimaanCustomer : function(){
		
		var secondGrid;
		secondGridStoreLapPenerimaanCustomer = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroup',
					store            : secondGridStoreLapPenerimaanCustomer,
					columns          : colsCustomer,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 150,
					stripeRows       : true,
					autoExpandColumn : 'kd_customer',
					title            : 'Customer yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroup',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGrid.store.add(records);
											secondGrid.store.sort('kd_customer', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGrid;
	},
	loadcustomer:function(param){
		var $this=this;
		Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/function_laporan_PTP/getCustomer",
				params: {kelpas :param},
				failure: function(o){
					var cst = Ext.decode(o.responseText);
				},	    
				success: function(o) {
					firstGrid_customer.store.removeAll();
					var cst = Ext.decode(o.responseText);

					for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
						var recs    = [],recType =  $this.DataStore.gridcustomer.recordType;
						var o=cst['listData'][i];
				
						recs.push(new recType(o));
						$this.DataStore.gridcustomer.add(recs);
						console.log(o);
					}
				}
			});
	},
	firstGridPenerimaanCustomer : function(){
		var $this=this;
		
		var dataSource_customer;
		var Field_poli_viDaftar = ['kd_customer','customer'];
		
		$this.DataStore.gridcustomer         = new WebApp.DataStore({fields: Field_poli_viDaftar});
		$this.loadcustomer(kelpas);
		
        firstGrid_customer = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : $this.DataStore.gridcustomer,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 150,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Customer',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'kd_customer',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Customer',
                                                    dataIndex: 'customer',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
			listeners : {
				afterrender : function(comp) {
					var secondGridDropTargetEl = firstGrid_customer.getView().scroller.dom;
					var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroup',
							notifyDrop : function(ddSource, e, data){
									var records =  ddSource.dragData.selections;
									Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
									firstGrid_customer.store.add(records);
									firstGrid_customer.store.sort('kd_customer', 'ASC');
									return true
							}
					});
				}
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid_customer;
	},
	
	firstGridPayment : function(){
		var firstGridPayment;
		var dataSource_payment;
		var Field_payment = ['kd_pay','uraian'];
		dataSource_payment         = new WebApp.DataStore({fields: Field_payment});
			Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/function_laporan_PTP/getPayment",
				params: '0',
				failure: function(o){
					var cst = Ext.decode(o.responseText);
				},	    
				success: function(o) {
					firstGridPayment.store.removeAll();
					var cst = Ext.decode(o.responseText);

					for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
						var recs    = [],recType =  dataSource_payment.recordType;
						var o=cst['listData'][i];
				
						recs.push(new recType(o));
						dataSource_payment.add(recs);
						console.log(o);
					}
				}
			});

            firstGridPayment = new Ext.grid.GridPanel({
				ddGroup          : 'secondGridDDGroupPayment',
				store            : dataSource_payment,
				autoScroll       : true,
				columnLines      : true,
				border           : true,
				enableDragDrop   : true,
				height           : 210,
				stripeRows       : true,
				trackMouseOver   : true,
				title            : 'Payment',
				anchor           : '100% 100%',
				plugins          : [new Ext.ux.grid.FilterRow()],
				colModel         : new Ext.grid.ColumnModel
				(
					[
					new Ext.grid.RowNumberer(),
						{
							id: 'colKD_pay',
							header: 'Kode Pay',
							dataIndex: 'kd_pay',
							sortable: true,
							hidden : true
						},
						{
							id: 'colKD_uraian',
							header: 'Uraian',
							dataIndex: 'uraian',
							sortable: true,
							width: 50
						}
					]
				),   
				listeners : {
					afterrender : function(comp) {
						var secondGridDropTargetEl = firstGridPayment.getView().scroller.dom;
						var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
								ddGroup    : 'firstGridDDGroupPayment',
								notifyDrop : function(ddSource, e, data){
										var records =  ddSource.dragData.selections;
										Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
										firstGridPayment.store.add(records);
										firstGridPayment.store.sort('kd_pay', 'ASC');
										return true
								}
						});
					}
	            },
	                viewConfig: 
	                    {
	                            forceFit: true
	                    }
        	});
        return firstGridPayment;
	},
	seconGridPayment : function(){
		//var secondGridStoreLapPenerimaanPaymentLapPenerimaan;
		var secondGridPayment;
		secondGridStoreLapPenerimaanPaymentLapPenerimaan = new Ext.data.JsonStore({
            fields : fieldsPayment,
            root   : 'records'
        });

        // create the destination Grid
            secondGridPayment = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroupPayment',
					store            : secondGridStoreLapPenerimaanPaymentLapPenerimaan,
					columns          : colsPayment,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 210,
					stripeRows       : true,
					autoExpandColumn : 'kd_pay',
					title            : 'Payment yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGridPayment.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroupPayment',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGridPayment.store.add(records);
											secondGridPayment.store.sort('kd_pay', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGridPayment;
	},
	init:function(){
		var $this=this;
		$this.Window.main=new Ext.Window({
			width: 775,
			height:550,
			modal:true,
			title:'Laporan Penerimaan Tunai Perkomponen Summary',
			layout:'fit',
			items:[
				new Ext.Panel({
					bodyStyle:'padding: 6px',
					layout:'form',
					border:false,
           			autoScroll: true,
					fbar:[
						new Ext.Button({
							text:'Ok',
							handler:function(){
								$this.initPrint()
							}
						}),
						new Ext.Button({
							text:'Batal',
							handler:function(){
								$this.Window.main.close();
							}
						})
					],
					items:[
						{
							layout:'column',
							border:false,
							width:750,
							height: 220,
							bodyStyle:'padding: 2px 2px 2px 2px',
							items:[
								{
									xtype : 'fieldset',
									title : 'Poliklinik',
									layout:'column',
									border:true,
									width:430,
									height: 230,
									bodyStyle:'padding: 2px 2px 2px 10px',
									items:[
										{
											border:false,
											columnWidth:.47,
											bodyStyle:'margin-top: 2px',
											items:[
												$this.firstGridPenerimaan(),
											]
										},
										{
											border:false,
											columnWidth:.03,
											bodyStyle:'margin-top: 2px',
										},
										{
											border:false,
											columnWidth:.47,
											bodyStyle:'margin-top: 2px; align:right;',
											items:[
												$this.seconGridPenerimaan(),
											]
										}
									]
								},{
									
									layout: 'column',
									border: false,
									width:20,
									height: 230
								},
								{
									xtype : 'fieldset',
									title : 'Kelompok pasien',
									layout: 'column',
									border: true,
									bodyStyle:'padding: 3px 5px  2px 10px',
									width:330,
									height: 230,
									items:[
											{
												layout: 'column',
												border: false,
												bodyStyle:'padding: 0px 0px  5px 0px',
												width:300,
												items:[
													$this.ComboBox.kelPasien1=new Ext.form.ComboBox({
													triggerAction: 'all',
													lazyRender:true,
													mode: 'local',
													width: 200,
													selectOnFocus:true,
													forceSelection: true,
													emptyText:'Silahkan Pilih...',
													fieldLabel: 'Pendaftaran Per Shift ',
													store: new Ext.data.ArrayStore({
														id: 0,
														fields:[
																'Id',
																'displayText'
														],
														data: [[-1, 'Semua'],[0, 'Perseorangan'],[1, 'Perusahaan'], [2, 'Asuransi']]
													}),
													valueField: 'Id',
													displayField: 'displayText',
													value:'Semua',
													listeners:{
														'select': function(a,b,c){
																$this.vars.comboSelect=b.data.displayText;
																$this.comboOnSelect(b.data.Id);
																
														}
													}
												}),
												{
													xtype:'displayfield',
													width: 10,
													value:'&nbsp;'
												}
												
														
												]
											},
											{
												
												layout: 'column',
												border: false,
												width:300,
												items:[
													//grid customer
														{
															border:false,
															columnWidth:.47,
															bodyStyle:'margin-top: 2px',
															items:[
																$this.firstGridPenerimaanCustomer(),
															]
														},
														{
															border:false,
															columnWidth:.03,
															bodyStyle:'margin-top: 2px',
														},
														{
															border:false,
															columnWidth:.47,
															bodyStyle:'margin-top: 2px; align:right;',
															items:[
																$this.seconGridPenerimaanCustomer(),
															]
														}
												]
											}
										
									]
								}
							]
						},
						{
							layout:'column',
							border:false,
							width:750,
							//height: 185,
							items:[
								{
									xtype : 'fieldset',
									layout:'column',
									border:false,
									width:330,
									//height: 200,
									bodyStyle:'padding: 2px 2px 2px 2px',
									items:[
										{
											xtype : 'fieldset',
											title : 'Tindakan',
											layout: 'column',
											width : 310,
											//height: 60,
											border: true,
											bodyStyle:'padding: 4px 0px  1px 10px',
											items:[
												$this.CheckboxGroup.tindakan=new Ext.form.CheckboxGroup({
													xtype: 'checkboxgroup',
													items: [
														{boxLabel: 'Pendaftaran',checked:false, name: 'tindakan_1',id : 'tindakan_1'},
														{boxLabel: 'Tindakan RWJ',checked:false, name: 'tindakan_2',id : 'tindakan_2'}
													]
												})
											]
										},
										{
											xtype : 'fieldset',
											title : 'Periode',
											layout: 'column',
											width : 310,
											bodyStyle:'padding: 2px 0px  7px 15px',
											border: true,
											items:[
												
												$this.DateField.startDate=new Ext.form.DateField({
													value: new Date(),
													format:'d/M/Y'
												}),
												{
													xtype:'displayfield',
													width: 30,
													value:'&nbsp;s/d&nbsp;'
												},
												$this.DateField.endDate=new Ext.form.DateField({
													value: new Date(),
													format:'d/M/Y'
												})
											]
										},
										{
											xtype : 'fieldset',
											//title : 'Operator',
											layout: 'column',
											width : 310,
											height: 70,
											
											border: false,
											items:[
												//$this.getUser(),
												{
													xtype : 'fieldset',
													title : 'Operator',
													layout: 'column',
													width : 200,
													bodyStyle:'padding: 2px 0px  7px 10px',
													border: true,
													items:[
														$this.getUser(),
													]
												},
												{
													
													layout: 'column',
													width : 27,
													border: false,
													height: 70
												},
												{
													xtype : 'fieldset',
													title : 'Type File',
													layout: 'column',
													width : 100,
													bodyStyle:'padding: 2px 0px  7px 5px',
													border: true,
													items:[
														{
															id: 'type_file',
															 xtype:'checkbox',
															 name: 'name',
															 labelSeparator: '',
															 hideLabel: true,
															 boxLabel: 'Excel',
															 fieldLabel: 'Excel' 
														},
														
													]
												},
											]
										},
										{
											xtype : 'fieldset',
											title : 'Shift',
											layout:'column',
											border: true,
											bodyStyle:'padding: 2px 0px  2px 10px',
											width : 310,
											//height: 60,
											items:[
												$this.CheckboxGroup.shift=new Ext.form.CheckboxGroup({
													xtype: 'checkboxgroup',
													items: [
														{boxLabel: 'Semua',checked:true,name: 'Shift_All_LabRegis',id : 'Shift_All_LabRegis',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_LabRegis').setValue(true);Ext.getCmp('Shift_2_LabRegis').setValue(true);Ext.getCmp('Shift_3_LabRegis').setValue(true);Ext.getCmp('Shift_1_LabRegis').disable();Ext.getCmp('Shift_2_LabRegis').disable();Ext.getCmp('Shift_3_LabRegis').disable();}else{Ext.getCmp('Shift_1_LabRegis').setValue(false);Ext.getCmp('Shift_2_LabRegis').setValue(false);Ext.getCmp('Shift_3_LabRegis').setValue(false);Ext.getCmp('Shift_1_LabRegis').enable();Ext.getCmp('Shift_2_LabRegis').enable();Ext.getCmp('Shift_3_LabRegis').enable();}}},
														{boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1_LabRegis',id : 'Shift_1_LabRegis'},
														{boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2_LabRegis',id : 'Shift_2_LabRegis'},
														{boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3_LabRegis',id : 'Shift_3_LabRegis'}
													]
												})
											]
										},
										{
											xtype : 'fieldset',
											title : 'Transfer',
											layout:'column',
											border: true,
											bodyStyle:'padding: 2px 0px  2px 10px',
											width : 310,
											//height: 60,
											items:[
												$this.CheckboxGroup.transfer=new Ext.form.CheckboxGroup({
													xtype: 'checkboxgroup',
													items: [
														{boxLabel: 'Laboratorium',name: 'Laboratorium',id : 'Laboratorium'},
														{boxLabel: 'Radiologi',name: 'Radiologi',id : 'Radiologi'},
														{boxLabel: 'Apotek',name: 'Apotek',id : 'Apotek'}
													]
												})
											]
										}
									]
								},{
									
									layout: 'column',
									border: false,
									width:5,
									height: 200
								},
								{
									xtype : 'fieldset',
									title : 'Pembayaran',
									layout: 'column',
									border: true,
									bodyStyle:'padding: 3px 0px  2px 10px',
									width:450,
									height: 260,
									items:[
										{
											border:false,
											columnWidth:.47,
											bodyStyle:'margin-top: 2px',
											items:[
												$this.firstGridPayment(),
											]
										},
										{
											border:false,
											columnWidth:.03,
											bodyStyle:'margin-top: 2px',
										},
										{
											border:false,
											columnWidth:.47,
											bodyStyle:'margin-top: 2px; align:right;',
											items:[
												$this.seconGridPayment(),
											]
										}
									]
								}	
							]
						},
					]
				})
			]
		}).show();
	}
};
DlgLapRWJPenerimaan.init();
