
var dsRWJBatalTransaksi;
var selectRWJBatalTransaksi;
var selectNamaRWJBatalTransaksi;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJBatalTransaksi;
var varLapRWJBatalTransaksi= ShowFormLapRWJBatalTransaksi();
var dsRWJ;

function ShowFormLapRWJBatalTransaksi()
{
    frmDlgRWJBatalTransaksi= fnDlgRWJBatalTransaksi();
    frmDlgRWJBatalTransaksi.show();
};

function fnDlgRWJBatalTransaksi()
{
    var winRWJBatalTransaksiReport = new Ext.Window
    (
        {
            id: 'winRWJBatalTransaksiReport',
            title: 'Laporan Pembatalan Transaksi',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJBatalTransaksi()]

        }
    );

    return winRWJBatalTransaksiReport;
};

function ItemDlgRWJBatalTransaksi()
{
    var PnlLapRWJBatalTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapRWJBatalTransaksi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJBatalTransaksi_Periode(),
				//getItemRWJBatalTransaksi_Shift(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJBatalTransaksi',
					handler: function()
					{
					   if (ValidasiReportRWJBatalTransaksi() === 1)
					   {
							/* if (Ext.getCmp('Shift_All_LapRWJBatalTransaksi').getValue() === true){
								shift='All';
								shift1='false';
								shift2='false';
								shift3='false';
							}else{
								shift='';
								if (Ext.getCmp('Shift_1_LapRWJBatalTransaksi').getValue() === true){
									shift1='true';
								} else{
									shift1='false';
								}
								if (Ext.getCmp('Shift_2_LapRWJBatalTransaksi').getValue() === true){
									shift2='true';
								}else{
									shift2='false';
								}
								if (Ext.getCmp('Shift_3_LapRWJBatalTransaksi').getValue() === true){
									shift3='true';
								}else{
									shift3='false';
								}
							} */
								

							var params=Ext.get('dtpTglAwalLapRWJBatalTransaksi').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapRWJBatalTransaksi').getValue()+'#aje#Laporan Batal Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionRWJ/cetakRWJBatalTransaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJBatalTransaksi.close(); 
							
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJBatalTransaksi',
					handler: function()
					{
						frmDlgRWJBatalTransaksi.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJBatalTransaksi;
};


function ValidasiReportRWJBatalTransaksi()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJBatalTransaksi').dom.value === '')
	{
		ShowPesanWarningRWJBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapRWJBatalTransaksi').getValue() === false && Ext.getCmp('Shift_1_LapRWJBatalTransaksi').getValue() === false && Ext.getCmp('Shift_2_LapRWJBatalTransaksi').getValue() === false && Ext.getCmp('Shift_3_LapRWJBatalTransaksi').getValue() === false){
		ShowPesanWarningRWJBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportRWJBatalTransaksi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJBatalTransaksi').dom.value > Ext.get('dtpTglAkhirLapRWJBatalTransaksi').dom.value)
    {
        ShowPesanWarningRWJBatalTransaksiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJBatalTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJBatalTransaksi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJBatalTransaksi_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJBatalTransaksi',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJBatalTransaksi',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


