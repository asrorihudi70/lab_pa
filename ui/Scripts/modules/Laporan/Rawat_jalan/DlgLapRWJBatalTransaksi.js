var type_file=0;
var dsRWJBatalTransaksi;
var selectRWJBatalTransaksi;
var selectNamaRWJBatalTransaksi;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJBatalTransaksi;
var varLapRWJBatalTransaksi= ShowFormLapRWJBatalTransaksi();
var dsRWJ;

function ShowFormLapRWJBatalTransaksi()
{
    frmDlgRWJBatalTransaksi= fnDlgRWJBatalTransaksi();
    frmDlgRWJBatalTransaksi.show();
};

function fnDlgRWJBatalTransaksi()
{
    var winRWJBatalTransaksiReport = new Ext.Window
    (
        {
            id: 'winRWJBatalTransaksiReport',
            title: 'Laporan Pembatalan Transaksi',
            closeAction: 'destroy',
            width:450,
            height: 180,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJBatalTransaksi()]

        }
    );

    return winRWJBatalTransaksiReport;
};

function ItemDlgRWJBatalTransaksi()
{
    var PnlLapRWJBatalTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapRWJBatalTransaksi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 180,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJBatalTransaksi_Periode(),
				//getItemRWJBatalTransaksi_Shift(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJBatalTransaksi',
					handler: function()
					{
					   if (ValidasiReportRWJBatalTransaksi() === 1)
					   {
						
							var params={
									tglAwal 	: Ext.getCmp('dtpTglAwalLapRWJBatalTransaksi').getValue(),
									tglAkhir 	: Ext.getCmp('dtpTglAkhirLapRWJBatalTransaksi').getValue(),
									type_file 	: type_file,
									order_by 	: Ext.getCmp('cboOrderBy').getValue(),
									} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/main/cetaklaporanRWJ/cetakRWJBatalTransaksi_rev");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							// frmDlgRWJBatalTransaksi.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLapRWJBatalTransaksi').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapRWJBatalTransaksi').getValue()+'#aje#Laporan Batal Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionRWJ/cetakRWJBatalTransaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJBatalTransaksi.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJBatalTransaksi',
					handler: function()
					{
						frmDlgRWJBatalTransaksi.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJBatalTransaksi;
};


function ValidasiReportRWJBatalTransaksi()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJBatalTransaksi').dom.value === '')
	{
		ShowPesanWarningRWJBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapRWJBatalTransaksi').getValue() === false && Ext.getCmp('Shift_1_LapRWJBatalTransaksi').getValue() === false && Ext.getCmp('Shift_2_LapRWJBatalTransaksi').getValue() === false && Ext.getCmp('Shift_3_LapRWJBatalTransaksi').getValue() === false){
		ShowPesanWarningRWJBatalTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportRWJBatalTransaksi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJBatalTransaksi').dom.value > Ext.get('dtpTglAkhirLapRWJBatalTransaksi').dom.value)
    {
        ShowPesanWarningRWJBatalTransaksiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJBatalTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJBatalTransaksi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJBatalTransaksi_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJBatalTransaksi',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					mComboOrder(),
					{
					   xtype: 'checkbox',
					   fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					},
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJBatalTransaksi',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};



function mComboOrder()
{
    var cboOrderBy = new Ext.form.ComboBox
	(
		{
			/*x: 120,
			y: 220,*/
			id:'cboOrderBy',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Order  ',
			// width:240,
			anchor:'99%',
			value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				    data: [[0, 'Medrec'],[1, 'Nama Pasien']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Medrec',
		}
	);
	return cboOrderBy;
};
