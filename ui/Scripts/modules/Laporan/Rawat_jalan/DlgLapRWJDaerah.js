var dsRWJDaerah;
var selectRWJDaerah;
var selectNamaRWJDaerah;
var now = new Date();
var selectSetDaerah;
var frmDlgRWJDaerah;
var selectSetkelpas;
var selectSetUmum;
var selectSetPerseorangan;
var varLapRWJDaerah= ShowFormLapRWJDaerah();

function ShowFormLapRWJDaerah()
{
    frmDlgRWJDaerah= fnDlgRWJDaerah();
    frmDlgRWJDaerah.show();
};

function fnDlgRWJDaerah()
{
    var winRWJDaerahReport = new Ext.Window
    (
        {
            id: 'winRWJDaerahReport',
            title: 'Laporan Rawat Jalan Pasien Per Daerah',
            closeAction: 'destroy',
            width:350,
            height: 221,
            border: false,
            resizable:false,
            plain: true,
            layout: 'column',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJDaerah()],
            listeners:
			{
				activate: function()
				{
					Ext.getCmp('cboPerseorangan').show();
					Ext.getCmp('cboPerseorangan').setValue('');
					Ext.getCmp('cboAsuransi').hide();
					Ext.getCmp('cboAsuransi').setValue('');
					Ext.getCmp('cboPerusahaanRequestEntry').hide();
					Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
					Ext.getCmp('cboUmum').hide();
					Ext.getCmp('cboUmum').setValue('');
					Ext.getCmp('cbperseorangan').hide();
					Ext.getCmp('cbperusahaan').hide();
					Ext.getCmp('cbasuransi').hide();

				}
			}

        }
    );

    return winRWJDaerahReport;
};


function ItemDlgRWJDaerah()
{
    var PnlLapRWJDaerah = new Ext.Panel
    (
        {
            id: 'PnlLapRWJDaerah',
            fileUpload: true,
            layout: 'form',
			width:350,
            height: '100',
            //anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                //getItemLapRWJDaerah_Tanggal(),
				getItemLapRWJDaerah_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'0px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWJDaerah',
                            handler: function()
                            {
                                if (ValidasiReportRWJDaerah() === 1)
                                {
                                        var criteria = GetCriteriaRWJDaerah();
                                        frmDlgRWJDaerah.close();
                                        console.log(criteria);
                                        loadlaporanRWJ('0', 'rep010203', criteria);
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWJDaerah',
                            handler: function()
                            {
                                    frmDlgRWJDaerah.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWJDaerah;
};

function GetCriteriaRWJDaerah()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJDaerah').dom.value !== '')
	{
		strKriteria = getnewformatdate(Ext.get('dtpTglAwalLapRWJDaerah').dom.value);
	};
	if (selectSetDaerah !== undefined)
	{
		strKriteria += '##@@##' + selectSetDaerah;
	};
	if (Ext.getCmp('kelPasien').getValue() === 'Umum')
	{
		selectSetPerseorangan = '0000000001';
		strKriteria += '##@@##' + 'Umum';
		strKriteria += '##@@##' + selectSetPerseorangan;
	}
	if (Ext.getCmp('kelPasien').getValue() === 'Perusahaan')
	{
		if(Ext.getCmp('cbperusahaan').getValue() === true)
		{
			strKriteria += '##@@##' + 'Perusahaan';
			strKriteria += '##@@##' + 'NULL';
		}else{
			strKriteria += '##@@##' + 'Perusahaan';
			strKriteria += '##@@##' + selectsetperusahaan;
		}
	}
	if (Ext.getCmp('kelPasien').getValue() === 'Asuransi')
	{
		if(Ext.getCmp('cbasuransi').getValue() === true)
		{
			strKriteria += '##@@##' + 'Asuransi';
			strKriteria += '##@@##' + 'NULL';
		}else{
			strKriteria += '##@@##' + 'Asuransi';
			strKriteria += '##@@##' + selectSetAsuransi;
		}
		
	}
	if (Ext.getCmp('kelPasien').getValue() === 'Semua' || Ext.getCmp('kelPasien').getValue() === '')
	{
		strKriteria += '##@@##' + 'NULL';
		strKriteria += '##@@##' + 'NULL';
		
	};

	return strKriteria;
};


function ValidasiReportRWJDaerah()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWJDaerah').dom.value === '') || (Ext.get('cboDaerah').dom.value === '' || 
            Ext.get('cboDaerah').dom.value === 'Silahkan Pilih...'))
    {
            if(Ext.get('dtpTglAwalLapRWJDaerah').dom.value === '')
            {
                    ShowPesanWarningRWJDaerahReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('cboDaerah').dom.value === '' || Ext.get('cboDaerah').dom.value === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningRWJDaerahReport(nmGetValidasiKosong('Wilayah'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRWJDaerah()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJDaerah').dom.value > Ext.get('dtpTglAkhirLapRWJDaerah').dom.value)
    {
        ShowPesanWarningRWJDaerahReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJDaerahReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJDaerah_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.0,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal ',
						id: 'dtpTglAwalLapRWJDaerah',
						format: 'M-Y',
						value:now,
						width:80
					},
                    mComboUnitLapRWJDaerah(),
					{  
						xtype: 'combo',
						fieldLabel: 'Kelompok Pasien',
						id: 'kelPasien',
						editable: false,
						store: new Ext.data.ArrayStore
							(
								{
								id: 0,
								fields:
								[
									'Id',
									'displayText'
								],
								   data: [[1, 'Semua'],[2, 'Umum'], [3, 'Perusahaan'], [4, 'Asuransi']]
								}
							),
						displayField: 'displayText',
						mode: 'local',
						width: 100,
						forceSelection: true,
						triggerAction: 'all',
						emptyText: 'Pilih Salah Satu...',
						selectOnFocus: true,
						anchor: '95%',
						value:selectSetkelpas,
						listeners:
						 {
								'select': function(a, b, c)
							{
							   Combo_Select(b.data.displayText);
							   selectSetkelpas=b.data.id;
							}

						}
					},
					{
						columnWidth: .0,
						layout: 'form',
						border: false,
						labelAlign: 'right',
						labelWidth: 150,
						defaultType: 'checkbox',

						items:
						[{
								fieldLabel: '',
								labelSeparator: '',
								boxLabel: 'Semua Data',
								name: 'cbperseorangan',
								id : 'cbperseorangan',
								handler: function (field, value) {
								   if (value === true)
								   {
									   Ext.getCmp('cboPerseorangan').setValue('Semua');
									   Ext.getCmp('cboPerseorangan').setDisabled(true);
								   }else{
									   Ext.getCmp('cboPerseorangan').setValue();
									   Ext.getCmp('cboPerseorangan').setDisabled(false);
								   }
								}
							}]
					},
					mComboPerseorangan(),
				{
					columnWidth: .0,
					layout: 'form',
					border: false,
					labelAlign: 'right',
					labelWidth: 150,
					defaultType: 'checkbox',

					items:
					[{
							fieldLabel: '',
							labelSeparator: '',
							boxLabel: 'Semua Data',
							name: 'cbperusahaan',
							id : 'cbperusahaan',
							handler: function (field, value) {
							   if (value === true)
							   {
								   Ext.getCmp('cboPerusahaanRequestEntry').setValue('Semua');
								   Ext.getCmp('cboPerusahaanRequestEntry').setDisabled(true);
							   }else{
								   Ext.getCmp('cboPerusahaanRequestEntry').setValue();
								   Ext.getCmp('cboPerusahaanRequestEntry').setDisabled(false);
							   }
							}
						}]
						},
				mComboPerusahaan(),
				{
					columnWidth: .0,
					layout: 'form',
					border: false,
					labelAlign: 'right',
					labelWidth: 150,
					defaultType: 'checkbox',

					items:
					[{
							fieldLabel: '',
							labelSeparator: '',
							boxLabel: 'Semua Data',
							name: 'cbasuransi',
							id : 'cbasuransi',
							handler: function (field, value) {
							   if (value === true)
							   {
								   Ext.getCmp('cboAsuransi').setValue('Semua');
								   Ext.getCmp('cboAsuransi').setDisabled(true);
							   }else{
								   Ext.getCmp('cboAsuransi').setValue();
								   Ext.getCmp('cboAsuransi').setDisabled(false);
							   }
							}
						}]
						},
				mComboAsuransi(),
				mComboUmum(),	
                ]
            }
        ]
    }
    return items;
};
function mComboPerseorangan()
{
    var cboPerseorangan = new Ext.form.ComboBox
	(
		{
			id:'cboPerseorangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:50,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Umum']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetPerseorangan,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetPerseorangan=b.data.displayText ;
								  console.log(selectSetPerseorangan);
				}
			}
		}
	);
	return cboPerseorangan;
};

function mComboUmum()
{
    var cboUmum = new Ext.form.ComboBox
	(
		{
			id:'cboUmum',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:50,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetUmum,
			listeners:
			{
				'select': function(a,b,c)
				{
					
                                  selectSetUmum=b.data.displayText ;
								  console.log(selectSetUmum);
				}
                                
                            
			}
		}
	);
	return cboUmum;
};
function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    ds_customer_viDaftar = new WebApp.DataStore({fields: Field});
    ref_combo_kelpas(1);
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
//		    anchor:'60%',
		    store: ds_customer_viDaftar,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
                    anchor: '95%',
                    value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
                                }
			}
                }
	);

    return cboPerusahaanRequestEntry;
};

var selectsetperusahaan;
var selectSetAsuransi;
function ref_combo_kelpas(jeniscus)
{
	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=~' + jeniscus + '~'
            }
        }
    );
	
	return ds_customer_viDaftar;
}
function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	
    var cboAsuransi = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransi',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Asuransi...',
                        fieldLabel: '',
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransi;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Umum")
   {    
		selectSetPerseorangan=undefined;
		selectsetperusahaan=undefined;
		selectSetAsuransi=undefined;
		selectSetUmum=undefined;
		Ext.getCmp('cboPerseorangan').setValue('');
		Ext.getCmp('cboAsuransi').setValue('');
		Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
		Ext.getCmp('cboUmum').setValue('');
        Ext.getCmp('cbperseorangan').enable();
        Ext.getCmp('cbperusahaan').disable();
        Ext.getCmp('cbasuransi').disable();
        Ext.getCmp('cbperseorangan').show();
        Ext.getCmp('cbperusahaan').hide();
        Ext.getCmp('cbasuransi').hide();
        Ext.getCmp('cbperseorangan').setValue(false);
        Ext.getCmp('cbperusahaan').setValue(false);
        Ext.getCmp('cbasuransi').setValue(false);
        
        Ext.getCmp('cboPerseorangan').show();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Perusahaan")
   {
		selectSetPerseorangan=undefined;
		selectsetperusahaan=undefined;
		selectSetAsuransi=undefined;
		selectSetUmum=undefined;
		Ext.getCmp('cboPerseorangan').setValue('');
		Ext.getCmp('cboAsuransi').setValue('');
		Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
		Ext.getCmp('cboUmum').setValue('');
        Ext.getCmp('cbperseorangan').disable();
        Ext.getCmp('cbperusahaan').enable();
        Ext.getCmp('cbasuransi').disable();
        Ext.getCmp('cbperseorangan').hide();
        Ext.getCmp('cbperusahaan').show();
        Ext.getCmp('cbasuransi').hide();
        Ext.getCmp('cbperseorangan').setValue(false);
        Ext.getCmp('cbperusahaan').setValue(false);
        Ext.getCmp('cbasuransi').setValue(false);
        
        
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
		ref_combo_kelpas(1);
   }
   else if(value === "Asuransi")
       {
			selectSetPerseorangan=undefined;
			selectsetperusahaan=undefined;
			selectSetAsuransi=undefined;
			selectSetUmum=undefined;
			Ext.getCmp('cboPerseorangan').setValue('');
			Ext.getCmp('cboAsuransi').setValue('');
			Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
			Ext.getCmp('cboUmum').setValue('');
            Ext.getCmp('cbperseorangan').disable();
            Ext.getCmp('cbperusahaan').disable();
            Ext.getCmp('cbasuransi').enable();
            Ext.getCmp('cbperseorangan').hide();
            Ext.getCmp('cbperusahaan').hide();
            Ext.getCmp('cbasuransi').show();
            Ext.getCmp('cbperseorangan').setValue(false);
            Ext.getCmp('cbperusahaan').setValue(false);
            Ext.getCmp('cbasuransi').setValue(false);
        
            Ext.getCmp('cboPerseorangan').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            Ext.getCmp('cboAsuransi').show();
			ref_combo_kelpas(2);
       }
       else if(value === "Semua")
       {
			selectSetPerseorangan=undefined;
			selectsetperusahaan=undefined;
			selectSetAsuransi=undefined;
			selectSetUmum=undefined;
			Ext.getCmp('cboPerseorangan').setValue('');
			Ext.getCmp('cboAsuransi').setValue('');
			Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
			Ext.getCmp('cboUmum').setValue('');
            Ext.getCmp('cboPerseorangan').hide();
            Ext.getCmp('cboAsuransi').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            
            Ext.getCmp('cbperseorangan').show();
            Ext.getCmp('cbperusahaan').hide();
            Ext.getCmp('cbasuransi').hide();
            Ext.getCmp('cbperseorangan').disable();
            Ext.getCmp('cbperusahaan').disable();
            Ext.getCmp('cbasuransi').disable();
            Ext.getCmp('cbperseorangan').setValue(true);
			//fnDlgRWJDaerah().height=300;
       }else
       {
			selectSetPerseorangan=undefined;
			selectsetperusahaan=undefined;
			selectSetAsuransi=undefined;
			selectSetUmum=undefined;
			Ext.getCmp('cboPerseorangan').setValue('');
			Ext.getCmp('cboAsuransi').setValue('');
			Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
			Ext.getCmp('cboUmum').setValue('');
			Ext.getCmp('cboPerseorangan').hide();
            Ext.getCmp('cboAsuransi').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            Ext.getCmp('cboUmum').hide();
       }
}
function getItemLapRWJDaerah_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: .80,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 55,
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: 'Tanggal ',
                    id: 'dtpTglAwalLapRWJDaerah',
                    format: 'M-Y',
                    value:now,
                    width:80
                }
            ]
        }
        ]
    }
    return items;
};


function mComboUnitLapRWJDaerah()
{
     var cboDaerah = new Ext.form.ComboBox
	(
		{
			id:'cboDaerah',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Wilayah ',
			width:80,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Prop'],[2, 'Kab'],[3, 'Kec'],[4, 'Kel']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetDaerah,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetDaerah=b.data.displayText ;
				}
			}
		}
	);
	return cboDaerah;
};