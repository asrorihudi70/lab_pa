var polipilihanpasien;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRwJDaftarPasienDokter;
var selectNamaRwJDaftarPasienDokter;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRwJDaftarPasienDokter;
var varLapRwJDaftarPasienDokter= ShowFormLapRwJDaftarPasienDokter();
var selectSetUmum;
var selectSetkelpas;
 var winRwJDaftarPasienDokterReport;

function ShowFormLapRwJDaftarPasienDokter()
{
    frmDlgRwJDaftarPasienDokter= fnDlgRwJDaftarPasienDokter();
    //frmDlgRwJDaftarPasienDokter.show();
};

function fnDlgRwJDaftarPasienDokter()
{
     winRwJDaftarPasienDokterReport = new Ext.Window
    (
        {
            id: 'winRwJDaftarPasienDokterReport',
            title: 'Laporan ',
            closeAction: 'destroy',
            width:400,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRwJDaftarPasienDokter()],
            listeners:
			{
            activate: function()
				{
             
				}
			}

        }
    );

    winRwJDaftarPasienDokterReport.show();
};


function ItemDlgRwJDaftarPasienDokter()
{
    var PnlLapRwJDaftarPasienDokter = new Ext.Panel
    (
        {
            id: 'PnlLapRwJDaftarPasienDokter',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [	

                getItemLapRwJDaftarPasienDokter_Atas(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRwJDaftarPasienDokter',
                            handler: function()
                            {
                               /*if (ValidasiReportRwJDaftarPasienDokter() === 1)
                               {*/
                                var params={
                                    kd_unit:Ext.getCmp('mComboRwjDaftarPoli').getValue(),
                                    kd_dokter:Ext.getCmp('mComboRwJDaftarDokter').getValue(),
                                    tgl_awal:Ext.getCmp('dtpTglAwalFilterRwJDaftarPasienDokter').getValue(),
                                    tgl_akhir:Ext.getCmp('dtpTglAkhirFilterRwJDaftarPasienDokter').getValue(),
                                } 
                                var form = document.createElement("form");
                                form.setAttribute("method", "post");
                                form.setAttribute("target", "_blank");
                                form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_PasienPerDokter/cetakLaporan");
                                var hiddenField = document.createElement("input");
                                hiddenField.setAttribute("type", "hidden");
                                hiddenField.setAttribute("name", "data");
                                hiddenField.setAttribute("value", Ext.encode(params));
                                form.appendChild(hiddenField);
                                document.body.appendChild(form);
                                form.submit();      
								//};
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRwJDaftarPasienDokter',
                            handler: function()
                            {
                            winRwJDaftarPasienDokterReport.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRwJDaftarPasienDokter;
};

/*function GetCriteriaRwJDaftarPasienDokter()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalFilterRwJDaftarPasienDokter').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalFilterRwJDaftarPasienDokter').getValue();
	}
	if (Ext.get('dtpTglAkhirFilterRwJDaftarPasienDokter').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterRwJDaftarPasienDokter').getValue();
	}

	return strKriteria;
};*/

function getKodeReportRwJDaftarPasienDokter()
{   var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanRwJDaftarPasienDokter').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanRwJDaftarPasienDokter').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

/*function ValidasiReportRwJDaftarPasienDokter()
{
    var x=1;
	
    if(Ext.get('dtpTglAwalFilterRwJDaftarPasienDokter').dom.value > Ext.get('dtpTglAkhirFilterRwJDaftarPasienDokter').dom.value)
    {
        ShowPesanWarningRwJDaftarPasienDokterReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }	

    return x;
};*/

function ShowPesanWarningRwJDaftarPasienDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapRwJDaftarPasienDokter_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterRwJDaftarPasienDokter',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 40,
                xtype: 'label',
                text: ' s/d '
            }, 
            {
                x: 260,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterRwJDaftarPasienDokter',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Poliklinik '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                ComboPoli(),
            {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Dokter '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                ComboDokter(),
            ]
        }]
    };
    return items;
};


var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;

function ComboPoli()
{
    var Field_poli = ['KD_UNIT', 'NAMA_UNIT'];
    ds_poli = new WebApp.DataStore({fields: Field_poli});
    ds_poli.load
    (
        {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: ""
                }
        }
    );
    
    var cbo_poliValue = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 10,
            id: 'mComboRwjDaftarPoli',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poliklinik ...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_poli,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            value: 'SEMUA',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
                {
                                   
                                }
                    
                }
        }
    )    
    return cbo_poliValue;
};

function ComboDokter()
{
    var Field_dokter = ['KD_DOKTER', 'NAMA'];
    ds_dokter = new WebApp.DataStore({fields: Field_dokter});
    ds_dokter.load
    (
        {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA',
                    Sortdir: 'ASC',
                    target:'ViewComboDokterLap',
                    param: ""
                }
        }
    );
    
    var cbo_dokterValue = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 70,
            id: 'mComboRwJDaftarDokter',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih dokter ...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_dokter,
            valueField: 'KD_DOKTER',
            displayField: 'NAMA',
            value: 'SEMUA',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
                {
                                   
                                }
                    
                }
        }
    )    
    return cbo_dokterValue;
};