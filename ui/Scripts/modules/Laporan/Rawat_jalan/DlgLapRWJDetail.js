var type_file=0; //type file 0 = pdf ; 1 = excel
var dsRWJPerLaporandetail;
var selectNamaRWJPerLaporandetail;
var tanggal = new Date();
var now = tanggal.format('Y-m-d');
var selectCount_viInformasiUnitdokter=50;
var icons_viInformasiUnitdokter="Gaji";
var dataSource_unit;
var selectSetJenisPasien='Semua Pasien';
var frmDlgRWJPerLaporandetail;
var ds_customer_viDaftar;
var secondGridStore;
var varLapRWJPerLaporandetail= ShowFormLapRWJPerLaporandetail();
var selectSetUmum;
var firstGrid;
var secondGrid;
var selectSetkelpas;
var selectSetPerseorangan;
var CurrentData_viDaftarRWJ =
{
	data: Object,
	details: Array,
	row: 0
};
var dsOperatorLapRWjDet;
var operator='000';
function ShowFormLapRWJPerLaporandetail()
{
    frmDlgRWJPerLaporandetail= fnDlgRWJPerLaporandetail();
    frmDlgRWJPerLaporandetail.show();
};

function fnDlgRWJPerLaporandetail()
{
    var winRWJPerLaporandetailReport = new Ext.Window
    (
        {
            id: 'winRWJPerLaporandetailReport',
            title: 'Laporan Rawat Jalan',
            closeAction: 'destroy',
            width:650,
            height: 460,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [dataGrid_viInformasiUnit()],
            listeners:
        {
            activate: function()
            {
				Ext.getCmp('cboPerseorangan').hide();
				Ext.getCmp('cboPerseorangan').setValue('');
                Ext.getCmp('cboAsuransi').hide();
                Ext.getCmp('cboAsuransi').setValue('');
                Ext.getCmp('cboPerusahaanRequestEntry').hide();
                Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
				Ext.getCmp('cboUmum').hide();
				Ext.getCmp('cboUmum').setValue('');
				Ext.getCmp('cbperseorangan').hide();
                Ext.getCmp('cbperusahaan').hide();
                Ext.getCmp('cbasuransi').hide();

            }
        }

        }
    );

    return winRWJPerLaporandetailReport;
};


function ItemDlgRWJPerLaporandetail()
{
    var PnlLapRWJPerLaporandetail = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPerLaporandetail',
            fileUpload: true,
            layout: 'form',
            height: '640',
            anchor: '99%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                dataGrid_viInformasiUnit(),

            ]
        }
        
    );
    return PnlLapRWJPerLaporandetail;
};

function GetCriteriaRWJPerLaporandetail()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJPerLaporandetail').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWJPerLaporandetail').getValue();
	};
        if (Ext.get('dtpTglAkhirLapRWJLaporandetail').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRWJLaporandetail').getValue();
	};
        if (Ext.get('cboJenisPasien').dom.value !== '' || Ext.get('cboJenisPasien').dom.value !== 'Silahkan Pilih...')
	{

				if (selectSetJenisPasien==='Semua Pasien')
				{
  
                strKriteria += '##@@##' + 'kosong';
				}
				else if (selectSetJenisPasien==='Pasien Baru')
				{
  
                strKriteria += '##@@##' + 'true';
				}else if (selectSetJenisPasien==='Pasien Lama')
				{
  
                strKriteria += '##@@##' + 'false';
				};
	};
	
	return strKriteria;
};

function ValidasiTanggalReportRWJPerLaporandetail()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJPerLaporandetail').dom.value > Ext.get('dtpTglAkhirLapRWJPerLaporandetail').dom.value)
    {
        ShowPesanWarningRWJPerLaporandetailReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJPerLaporandetailReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function mComboJenisPasien()
{
    var cboJenisPasien = new Ext.form.ComboBox
	(
		{
			id:'cboJenisPasien',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Jenis Pasien ',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua Pasien'],[2, 'Pasien Baru'],[3, 'Pasien Lama']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
//			value:selectSetJenisPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetJenisPasien=b.data.displayText ;
				}
			}
		}
	);
	return cboJenisPasien;
};

function dataGrid_viInformasiUnit(mod_id)
{
    var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    datarefresh_viInformasiUnit()

    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
	];


	// declare the source Grid
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 200,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'KD_UNIT',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA_UNIT',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),
				listeners : {
                    afterrender : function(comp) {
                    var firstGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
   
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStore,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 200,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_UNIT',
                    title            : 'Unit',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'secondGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid.store.add(records);
                                    secondGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

	
	var FrmTabs_viInformasiUnit = new Ext.Panel
        (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
            height       : 320,
			
//		    title:  'Pilih Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					anchor: '100% 100%',
					items:
					[firstGrid
						
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid
						
					]
				},
                                {
                                        
                                            columnWidth: .40,
                                            layout: 'form',
                                            border: false,
                                            labelAlign: 'right',
                                            labelWidth: 100,
                                            items:
                                            [
											{
												   xtype: 'checkbox',
												   id: 'CekLapPilihSemuaRWJDetail',
												   hideLabel:false,
												   boxLabel: 'Pilih Semua',
												   checked: false,
												   listeners: 
												   {
														check: function()
														{
														   if(Ext.getCmp('CekLapPilihSemuaRWJDetail').getValue()===true)
															{
																 firstGrid.getSelectionModel().selectAll();
															}
															else
															{
																firstGrid.getSelectionModel().clearSelections();
															}
														}
												   }
												},
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: 'Tanggal  ',
                                                    id: 'dtpTglAwalLapRWJPerLaporandetail',
                                                    format: 'd-M-Y',
                                                    value:now,
                                                    anchor: '95%'
                                                },
												{  
													xtype: 'combo',
													fieldLabel: 'Kelompok Pasien',
													id: 'kelPasien',
													editable: false,
													store: new Ext.data.ArrayStore
														(
															{
															id: 0,
															fields:
															[
																'Id',
																'displayText'
															],
															   data: [[1, 'Semua'],[2, 'Umum'], [3, 'Perusahaan'], [4, 'Asuransi']]
															}
														),
													displayField: 'displayText',
													mode: 'local',
													width: 100,
													forceSelection: true,
													triggerAction: 'all',
													emptyText: 'Pilih Salah Satu...',
													selectOnFocus: true,
													anchor: '95%',
													value:'Semua',
													listeners:
													 {
														'select': function(a, b, c)
														{
														   Combo_Select(b.data.displayText);
														   selectSetkelpas=b.data.id;
														},

													}
												},
												{
													columnWidth: .140,
													layout: 'form',
													border: false,
													labelAlign: 'right',
													labelWidth: 150,
													defaultType: 'checkbox',
													items:
													[{
															fieldLabel: '',
															labelSeparator: '',
															boxLabel: 'Semua Data',
															name: 'cbperseorangan',
															id : 'cbperseorangan',
															handler: function (field, value) {
															   if (value === true)
															   {
																   Ext.getCmp('cboPerseorangan').setValue('Semua');
																   Ext.getCmp('cboPerseorangan').setDisabled(true);
															   }else{
																   Ext.getCmp('cboPerseorangan').setValue();
																   Ext.getCmp('cboPerseorangan').setDisabled(false);
															   }
															}
														}]
												},
												mComboPerseorangan(),
											{
												columnWidth: .90,
												layout: 'form',
												border: false,
												labelAlign: 'right',
												labelWidth: 150,
												defaultType: 'checkbox',

												items:
												[{
														fieldLabel: '',
														labelSeparator: '',
														boxLabel: 'Semua Data',
														name: 'cbperusahaan',
														id : 'cbperusahaan',
														handler: function (field, value) {
														   if (value === true)
														   {
															   Ext.getCmp('cboPerusahaanRequestEntry').setValue('Semua');
															   Ext.getCmp('cboPerusahaanRequestEntry').setDisabled(true);
														   }else{
															   Ext.getCmp('cboPerusahaanRequestEntry').setValue();
															   Ext.getCmp('cboPerusahaanRequestEntry').setDisabled(false);
														   }
														}
													}]
													},
											mComboPerusahaan(),
											{
												columnWidth: .90,
												layout: 'form',
												border: false,
												labelAlign: 'right',
												labelWidth: 150,
												defaultType: 'checkbox',

												items:
												[{
														fieldLabel: '',
														labelSeparator: '',
														boxLabel: 'Semua Data',
														name: 'cbasuransi',
														id : 'cbasuransi',
														handler: function (field, value) {
														   if (value === true)
														   {
															   Ext.getCmp('cboAsuransi').setValue('Semua');
															   Ext.getCmp('cboAsuransi').setDisabled(true);
														   }else{
															   Ext.getCmp('cboAsuransi').setValue();
															   Ext.getCmp('cboAsuransi').setDisabled(false);
														   }
														}
													}]
													},
													
											mComboAsuransi(),
											mComboUmum(),	
											{  
												xtype: 'combo',
												fieldLabel: 'Order By',
												id: 'CboOrder',
												editable: false,
												store: new Ext.data.ArrayStore
													(
														{
														id: 0,
														fields:
														[
															'Id',
															'displayText'
														],
														   data: [[0, 'Medrec'],[1, 'Nama Pasien'], [2, 'Tanggal Masuk'],]
														}
													),
												displayField: 'displayText',
												mode: 'local',
												width: 100,
												forceSelection: true,
												triggerAction: 'all',
												// emptyText: 'Medrec',
												selectOnFocus: true,
												anchor: '95%',
												value:'Medrec',
												listeners:
												 {
													'select': function(a, b, c)
													{
													},

												}
											},
											mComboOperator(),	
                                            ]
                                        },
                                        {
                                            columnWidth: .30,
                                            layout: 'form',
                                            border: false,
                                            labelWidth: 30,
                                            items:
                                            [
												{
                                                fieldLabel: '',
												height:25,
												border:false,
												labelSeparator: '',
												boxLabel: 'Semua Data',
                                            },
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: 's/d ',
                                                    id: 'dtpTglAkhirLapRWJLaporandetail',
                                                    format: 'd-M-Y',
                                                    value:now,
                                                    anchor: '80%'
                                                }
                                            ]
                                        },
                                         {
                                            columnWidth: .30,
                                            layout: 'form',
                                            border: false,
                                            labelAlign: 'right',
                                            labelWidth: 75,
                                            items:
                                            [
												mComboJenisPasien(),
												{
												   xtype: 'checkbox',
												   fieldLabel: 'Type ',
												   id: 'CekLapPilihTypeExcel',
												   hideLabel:false,
												   boxLabel: 'Excel',
												   checked: false,
												   listeners: 
												   {
														check: function()
														{
														   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
															{
																type_file=1;
															}
															else
															{
																type_file=0;
															}
														}
												   }
												}
                                            ]

                                        }, 
										
										
                                       
                                {
                                columnWidth: .99,
                                layout: 'hBox',
                                border: false,
                                defaults: { margins: '0 2 0 0' },
                                style:{'margin-left':'398px','margin-top':'0px'},
                                anchor: '100%',
                                /* layoutConfig:
                                {
                                    padding: '3',
                                    pack: 'end',
                                    align: 'middle'
                                }, */
                                items:
                                [
                                    
									
                                    {
                                        xtype: 'button',
                                        text: 'Print',
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnOkLapRWJPerLaporandetail',
                                        handler: function()
                                        {
                                            var sendDataArray = [];
                                            secondGridStore.each(function(record){
                                            var recordArray = [record.get("KD_UNIT")];
                                            sendDataArray.push(recordArray);
											
                                            });
									
											
                                           if (sendDataArray.length === 0)
											{
                                                   ShowPesanWarningRWJPerLaporandetailReport('Isi kriteria unit dengan drag and drop','Laporan')
                                                 
										    }else{
											 
												var kel_pas_detail = "";
												var nama_kel_pas = "";
												if (Ext.getCmp('kelPasien').getValue() === 'Semua')
												{
													kel_pas_detail = 'NULL';
													nama_kel_pas = 'Semua Kelompok Pasien';
												}
												if(Ext.getCmp('kelPasien').getValue() == 'Umum')
												{
													selectSetPerseorangan = '0000000001';
													kel_pas_detail = selectSetPerseorangan;
													nama_kel_pas = 'Umum';
													
												}
												if(Ext.getCmp('kelPasien').getValue() == 'Perusahaan')
												{
													if(Ext.getCmp('cbperusahaan').getValue() === true)
													{
														kel_pas_detail = 'NULL';
														nama_kel_pas = 'Semua Perusahaan';
													}else{
														kel_pas_detail = selectsetperusahaan;
														nama_kel_pas = selectperusahaan;
													}
												}
												if(Ext.getCmp('kelPasien').getValue() == 'Asuransi')
												{
													if(Ext.getCmp('cbasuransi').getValue() === true)
													{
														kel_pas_detail = 'NULL';
														nama_kel_pas = 'Semua Asuransi';
													}else{
														kel_pas_detail = selectSetAsuransi;
														nama_kel_pas = selectAsuransi;
													}
												}
												
												var JenisPasien=""; //split[2]
												if(selectSetJenisPasien==='Semua Pasien'){
													JenisPasien = 'kosong';
												}else if (selectSetJenisPasien==='Pasien Baru'){	
													JenisPasien = 'true';
												}else if (selectSetJenisPasien==='Pasien Lama'){
													JenisPasien = 'false';
												}
												var params={
															JenisPasien 	:JenisPasien, //split[2]
															TglAwal 		:Ext.getCmp('dtpTglAwalLapRWJPerLaporandetail').getValue(), //split[0]
															TglAkhir 		:Ext.getCmp('dtpTglAkhirLapRWJLaporandetail').getValue(), //split[1]
															KelPasien 		:Ext.getCmp('kelPasien').getValue(), //split[4]
															KelPasien_d 	:kel_pas_detail,//split[5]
															JmlList 		:secondGridStore.getCount(), //split[3]
															Type_File 		:type_file,
															nama_kel_pas 	:nama_kel_pas, //split[6]
															order_by 		:Ext.getCmp('CboOrder').getValue(), //split[6]
															operator 		:operator,
												} ;
												var i=0;
												for(i=0; i<secondGridStore.getCount(); i++){
													params['kd_unit'+i]=secondGridStore.data.items[i].data.KD_UNIT;		
		
												}
												console.log(params);
												var form = document.createElement("form");
												form.setAttribute("method", "post");
												form.setAttribute("target", "_blank");
												// form.setAttribute("action", baseURL + "index.php/main/cetaklaporanRWJ/cetakDirectlaporanRWJ");
												form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_register_detail/cetak_direct");
												var hiddenField = document.createElement("input");
												hiddenField.setAttribute("type", "hidden");
												hiddenField.setAttribute("name", "data");
												hiddenField.setAttribute("value", Ext.encode(params));
												form.appendChild(hiddenField);
												document.body.appendChild(form);
												form.submit();	
												frmDlgRWJPerLaporandetail.close();
											 
											};
                                        }
                                    },
									{
                                        xtype: 'button',
                                        text: 'Preview',
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnPreviewLapRWJPerLaporandetail',
                                        handler: function()
                                        {
                                            var sendDataArray = [];
                                            secondGridStore.each(function(record){
                                            var recordArray = [record.get("KD_UNIT")];
                                            sendDataArray.push(recordArray);
											
                                            });
									
											
                                           if (sendDataArray.length === 0)
											{
                                                   ShowPesanWarningRWJPerLaporandetailReport('Isi kriteria unit dengan drag and drop','Laporan')
                                                 
										    }else{
									/*
											 var criteria = GetCriteriaRWJPerLaporandetail();
											
                                             criteria += '##@@##' + sendDataArray;
											if (Ext.getCmp('kelPasien').getValue() === 'Semua')
											{
												criteria += '##@@##' + 'Semua';
												criteria += '##@@##' + 'NULL';
												criteria += '##@@##' + 'Semua Kelompok Pasien';
											}
											if (Ext.getCmp('kelPasien').getValue() === 'Umum')
											{
												selectSetPerseorangan = '0000000001';
												criteria += '##@@##' + 'Umum';
												criteria += '##@@##' + selectSetPerseorangan;
												criteria += '##@@##' + 'Umum';
											}
											if (Ext.getCmp('kelPasien').getValue() === 'Perusahaan')
											{
												if(Ext.getCmp('cbperusahaan').getValue() === true)
												{
													criteria += '##@@##' + 'Perusahaan';
													criteria += '##@@##' + 'NULL';
													criteria += '##@@##' + 'Semua Perusahaan';
												}else{
													criteria += '##@@##' + 'Perusahaan';
													criteria += '##@@##' + selectsetperusahaan;
													criteria += '##@@##' + selectperusahaan;
												}
											}
											if (Ext.getCmp('kelPasien').getValue() === 'Asuransi')
											{
												if(Ext.getCmp('cbasuransi').getValue() === true)
												{

													criteria += '##@@##' + 'Asuransi';
													criteria += '##@@##' + 'NULL';
													criteria += '##@@##' + 'Semua Asuransi';
												}else{
													criteria += '##@@##' + 'Asuransi';
													criteria += '##@@##' + selectSetAsuransi;
													criteria += '##@@##' + selectAsuransi;
												}
												
											};
											 
                                             frmDlgRWJPerLaporandetail.close();
                                             loadlaporanRWJ('0', 'rep010201', criteria);
											 */
											 
											 
											var kel_pas_detail = "";
											var nama_kel_pas = "";
												if (Ext.getCmp('kelPasien').getValue() === 'Semua')
												{
													kel_pas_detail = 'NULL';
													nama_kel_pas = 'Semua Kelompok Pasien';
												}
												if(Ext.getCmp('kelPasien').getValue() == 'Umum')
												{
													selectSetPerseorangan = '0000000001';
													kel_pas_detail = selectSetPerseorangan;
													nama_kel_pas = 'Umum';
													
												}
												if(Ext.getCmp('kelPasien').getValue() == 'Perusahaan')
												{
													if(Ext.getCmp('cbperusahaan').getValue() === true)
													{
														kel_pas_detail = 'NULL';
														nama_kel_pas = 'Semua Perusahaan';
													}else{
														kel_pas_detail = selectsetperusahaan;
														nama_kel_pas = selectperusahaan;
													}
												}
												if(Ext.getCmp('kelPasien').getValue() == 'Asuransi')
												{
													if(Ext.getCmp('cbasuransi').getValue() === true)
													{
														kel_pas_detail = 'NULL';
														nama_kel_pas = 'Semua Asuransi';
													}else{
														kel_pas_detail = selectSetAsuransi;
														nama_kel_pas = selectAsuransi;
													}
												}
												
												var JenisPasien=""; //split[2]
												if(selectSetJenisPasien==='Semua Pasien'){
													JenisPasien = 'kosong';
												}else if (selectSetJenisPasien==='Pasien Baru'){	
													JenisPasien = 'true';
												}else if (selectSetJenisPasien==='Pasien Lama'){
													JenisPasien = 'false';
												}
												var params={
															JenisPasien:JenisPasien, //split[2]
															TglAwal:Ext.getCmp('dtpTglAwalLapRWJPerLaporandetail').getValue(), //split[0]
															TglAkhir:Ext.getCmp('dtpTglAkhirLapRWJLaporandetail').getValue(), //split[1]
															KelPasien:Ext.getCmp('kelPasien').getValue(), //split[4]
															KelPasien_d:kel_pas_detail,//split[5]
															JmlList:secondGridStore.getCount(), //split[3]
															Type_File:type_file,
															nama_kel_pas:nama_kel_pas, //split[6]
															order_by 		:Ext.getCmp('CboOrder').getValue(), //split[6]
															operator		:operator,
												} ;
												var i=0;
												for(i=0; i<secondGridStore.getCount(); i++){
													params['kd_unit'+i]=secondGridStore.data.items[i].data.KD_UNIT;		
		
												}
												console.log(params);
												var form = document.createElement("form");
												form.setAttribute("method", "post");
												form.setAttribute("target", "_blank");
												form.setAttribute("action", baseURL + "index.php/main/cetaklaporanRWJ/cetaklaporanRWJ_Regisdet");
												var hiddenField = document.createElement("input");
												hiddenField.setAttribute("type", "hidden");
												hiddenField.setAttribute("name", "data");
												hiddenField.setAttribute("value", Ext.encode(params));
												form.appendChild(hiddenField);
												document.body.appendChild(form);
												form.submit();	
												frmDlgRWJPerLaporandetail.close();
											 
											};
                                        }
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Cancel' ,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnCancelLapRWJPerLaporandetail',
                                        handler: function()
                                        {
                                                frmDlgRWJPerLaporandetail.close();
                                        }
                                    }
                                    ]
                                }
			],

    }
    )
	// datarefresh_viInformasiUnitdokter();
	return FrmTabs_viInformasiUnit;
}

function mComboPerseorangan(){
	var cboPerseorangan = new Ext.form.ComboBox({
		id:'cboPerseorangan',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		selectOnFocus:true,
		forceSelection: true,
		emptyText:'Silahkan Pilih...',
		fieldLabel: '',
		width:50,
		anchor: '95%',
		store: new Ext.data.ArrayStore
		(
			{
				id: 0,
				fields:
				[
					'Id',
					'displayText'
				],
			data: [[1, 'Umum']]
			}
		),
		valueField: 'Id',
		displayField: 'displayText',
		value:selectSetPerseorangan,
		listeners:
		{
			'select': function(a,b,c)
			{
								selectSetPerseorangan =b.data.displayText ;
								// $tmpTambah         = $Split[6];
								// console.log(selectSetPerseorangan);
			}
		}
	});
	return cboPerseorangan;
};

function mComboUmum()
{
    var cboUmum = new Ext.form.ComboBox
	(
		{
			id:'cboUmum',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:50,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetUmum,
			listeners:
			{
				'select': function(a,b,c)
				{
					
                                  selectSetUmum=b.data.displayText ;
								  console.log(selectSetUmum);
				}
                                
                            
			}
		}
	);
	return cboUmum;
};
function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    ds_customer_viDaftar = new WebApp.DataStore({fields: Field});
    ref_combo_kelpas(1);
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
//		    anchor:'60%',
		    store: ds_customer_viDaftar,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
                    anchor: '95%',
                    value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectperusahaan=b.data.CUSTOMER;
                                }
			}
                }
	);

    return cboPerusahaanRequestEntry;
};

var selectsetperusahaan;
var selectSetAsuransi;
var selectAsuransi;
var selectperusahaan;
function ref_combo_kelpas(jeniscus)
{
	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=~' + jeniscus + '~'
            }
        }
    );
	
	return ds_customer_viDaftar;
}
function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	
    var cboAsuransi = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransi',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Asuransi...',
                        fieldLabel: '',
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransi;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Umum")
   {    
		selectSetPerseorangan=undefined;
		selectsetperusahaan=undefined;
		selectSetAsuransi=undefined;
		selectSetUmum=undefined;
		selectAsuransi=undefined;
		selectperusahaan=undefined;
		Ext.getCmp('cboPerseorangan').setValue('');
		Ext.getCmp('cboAsuransi').setValue('');
		Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
		Ext.getCmp('cboUmum').setValue('');
        Ext.getCmp('cbperseorangan').enable();
        Ext.getCmp('cbperusahaan').disable();
        Ext.getCmp('cbasuransi').disable();
        Ext.getCmp('cbperseorangan').hide();
        Ext.getCmp('cbperusahaan').hide();
        Ext.getCmp('cbasuransi').hide();
        Ext.getCmp('cbperseorangan').setValue(false);
        Ext.getCmp('cbperusahaan').setValue(false);
        Ext.getCmp('cbasuransi').setValue(false);
        
        Ext.getCmp('cboPerseorangan').show();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Perusahaan")
   {
		selectSetPerseorangan=undefined;
		selectsetperusahaan=undefined;
		selectSetAsuransi=undefined;
		selectSetUmum=undefined;
		selectAsuransi=undefined;
		selectperusahaan=undefined;
		Ext.getCmp('cboPerseorangan').setValue('');
		Ext.getCmp('cboAsuransi').setValue('');
		Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
		Ext.getCmp('cboUmum').setValue('');
        Ext.getCmp('cbperseorangan').disable();
        Ext.getCmp('cbperusahaan').enable();
        Ext.getCmp('cbasuransi').disable();
        Ext.getCmp('cbperseorangan').hide();
        Ext.getCmp('cbperusahaan').show();
        Ext.getCmp('cbasuransi').hide();
        Ext.getCmp('cbperseorangan').setValue(false);
        Ext.getCmp('cbperusahaan').setValue(false);
        Ext.getCmp('cbasuransi').setValue(false);
        
        
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
		ref_combo_kelpas(1);
   }
   else if(value === "Asuransi")
       {
			selectSetPerseorangan=undefined;
			selectsetperusahaan=undefined;
			selectSetAsuransi=undefined;
			selectSetUmum=undefined;
			selectAsuransi=undefined;
			selectperusahaan=undefined;
			Ext.getCmp('cboPerseorangan').setValue('');
			Ext.getCmp('cboAsuransi').setValue('');
			Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
			Ext.getCmp('cboUmum').setValue('');
            Ext.getCmp('cbperseorangan').disable();
            Ext.getCmp('cbperusahaan').disable();
            Ext.getCmp('cbasuransi').enable();
            Ext.getCmp('cbperseorangan').hide();
            Ext.getCmp('cbperusahaan').hide();
            Ext.getCmp('cbasuransi').show();
            Ext.getCmp('cbperseorangan').setValue(false);
            Ext.getCmp('cbperusahaan').setValue(false);
            Ext.getCmp('cbasuransi').setValue(false);
        
            Ext.getCmp('cboPerseorangan').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            Ext.getCmp('cboAsuransi').show();
			ref_combo_kelpas(2);
       }
       else if(value === "Semua")
       {
			selectSetPerseorangan=undefined;
			selectsetperusahaan=undefined;
			selectSetAsuransi=undefined;
			selectSetUmum=undefined;
			selectAsuransi=undefined;
			selectperusahaan=undefined;
			Ext.getCmp('cboPerseorangan').setValue('');
			Ext.getCmp('cboAsuransi').setValue('');
			Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
			Ext.getCmp('cboUmum').setValue('');
			Ext.getCmp('cboPerseorangan').hide();
			Ext.getCmp('cboAsuransi').hide();
			Ext.getCmp('cboPerusahaanRequestEntry').hide();
            
			Ext.getCmp('cbperseorangan').hide();
			Ext.getCmp('cbperusahaan').hide();
			Ext.getCmp('cbasuransi').hide();
			Ext.getCmp('cbperseorangan').disable();
			Ext.getCmp('cbperusahaan').disable();
			Ext.getCmp('cbasuransi').disable();
			Ext.getCmp('cbperseorangan').setValue(true);
       }else
       {
			selectSetPerseorangan=undefined;
			selectsetperusahaan=undefined;
			selectSetAsuransi=undefined;
			selectSetUmum=undefined;
			selectAsuransi=undefined;
			selectperusahaan=undefined;
			Ext.getCmp('cboPerseorangan').setValue('');
			Ext.getCmp('cboAsuransi').setValue('');
			Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
			Ext.getCmp('cboUmum').setValue('');
			Ext.getCmp('cboPerseorangan').hide();
			Ext.getCmp('cboAsuransi').hide();
			Ext.getCmp('cboPerusahaanRequestEntry').hide();
            
			Ext.getCmp('cbperseorangan').hide();
			Ext.getCmp('cbperusahaan').hide();
			Ext.getCmp('cbasuransi').hide();
			Ext.getCmp('cbperseorangan').disable();
			Ext.getCmp('cbperusahaan').disable();
			Ext.getCmp('cbasuransi').disable();
			Ext.getCmp('cbperseorangan').setValue(true);
       }
}
function DataInputKriteria()
{
    var FrmTabs_DataInputKriteria = new Ext.Panel
        (
		{
		    id: FrmTabs_DataInputKriteria,
		    closable: true,
		    region: 'center',
		    layout: 'column',
            height       : 100,
		    title:  'Dokter Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[]
				}
			]
		
    }
    )
    
    return FrmTabs_DataInputKriteria;
}
        

function datarefresh_viInformasiUnit()
{
    dataSource_unit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnitB',
                param: "kd_bagian=2 and type='0'"
            }
        }
    )
    //alert("refersh")
}

function mComboOperator(){
	loadDataOperatorLaporanRWJDet();
      var Field = ['kd_user','user_names'];
      dsOperatorLapRWjDet = new WebApp.DataStore({fields: Field});
      var  cboOperatorLaporanRWJ = new Ext.form.ComboBox
	(
		{
		    id: 'cboOperatorLapRWJDet',
		    typeAhead: true,
		    triggerAction: 'all',
		    fieldLabel:'Operator',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsOperatorLapRWjDet,
		    valueField: 'kd_user',
		    displayField: 'user_names',
		    width:100,
		    anchor: '95%',
		    value:'Semua',
		    listeners:
			{
			    'select': function(a, b, c){
			    		operator=b.data.kd_user;
                 },				                       
			}
                }
	);
    return cboOperatorLaporanRWJ;
};
function loadDataOperatorLaporanRWJDet(){
	Ext.Ajax.request({
	url: baseURL + "index.php/rawat_jalan/lap_register_detail/get_data_operator",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsOperatorLapRWjDet.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsOperatorLapRWjDet.add(recs);
			}
				console.log(dsOperatorLapRWjDet);
		}
	});
}
    

	
