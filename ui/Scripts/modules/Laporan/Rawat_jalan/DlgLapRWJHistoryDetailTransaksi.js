var type_file=0;
var dsRWJHistoryDetailTransaksi;
var selectRWJHistoryDetailTransaksi;
var selectNamaRWJHistoryDetailTransaksi;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJHistoryDetailTransaksi;
var varLapRWJHistoryDetailTransaksi= ShowFormLapRWJHistoryDetailTransaksi();
var dsRWJ;

function ShowFormLapRWJHistoryDetailTransaksi()
{
    frmDlgRWJHistoryDetailTransaksi= fnDlgRWJHistoryDetailTransaksi();
    frmDlgRWJHistoryDetailTransaksi.show();
};

function fnDlgRWJHistoryDetailTransaksi()
{
    var winRWJHistoryDetailTransaksiReport = new Ext.Window
    (
        {
            id: 'winRWJHistoryDetailTransaksiReport',
            title: 'Laporan History Detail Transaksi',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJHistoryDetailTransaksi()]

        }
    );

    return winRWJHistoryDetailTransaksiReport;
};

function ItemDlgRWJHistoryDetailTransaksi()
{
    var PnlLapRWJHistoryDetailTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapRWJHistoryDetailTransaksi',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJHistoryDetailTransaksi_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnPreviewLapRWJHistoryDetailTransaksi',
					handler: function()
					{
						if (ValidasiReportRWJHistoryDetailTransaksi() === 1)
						{
							var params={
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJHistoryDetailTransaksi').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJHistoryDetailTransaksi').getValue(),
								type_file:type_file
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/main/cetaklaporanRWJ/cetakHistoryDetailTransaksi");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJHistoryDetailTransaksi.close(); 
						};
					}
				},
				{
					xtype: 'button',
					text: 'Print',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJHistoryDetailTransaksi',
					handler: function()
					{
						if (ValidasiReportRWJHistoryDetailTransaksi() === 1)
						{
							var params={
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJHistoryDetailTransaksi').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJHistoryDetailTransaksi').getValue(),
								type_file:type_file
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_histori_detail_transaksi/cetak_direct");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJHistoryDetailTransaksi.close(); 
						};
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJHistoryDetailTransaksi',
					handler: function()
					{
						frmDlgRWJHistoryDetailTransaksi.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJHistoryDetailTransaksi;
};


function ValidasiReportRWJHistoryDetailTransaksi()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJHistoryDetailTransaksi').dom.value === '')
	{
		ShowPesanWarningRWJHistoryDetailTransaksiReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}

    return x;
};

function ValidasiTanggalReportRWJHistoryDetailTransaksi()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJHistoryDetailTransaksi').dom.value > Ext.get('dtpTglAkhirLapRWJHistoryDetailTransaksi').dom.value)
    {
        ShowPesanWarningRWJHistoryDetailTransaksiReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJHistoryDetailTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJHistoryDetailTransaksi_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJHistoryDetailTransaksi_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJHistoryDetailTransaksi',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					{
					   xtype: 'checkbox',
					   fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					}
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJHistoryDetailTransaksi',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


