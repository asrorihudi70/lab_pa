var type_file=0;
var dsRWJHistoryPembayaran;
var selectRWJHistoryPembayaran;
var selectNamaRWJHistoryPembayaran;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJHistoryPembayaran;
var varLapRWJHistoryPembayaran= ShowFormLapRWJHistoryPembayaran();
var dsRWJ;

function ShowFormLapRWJHistoryPembayaran()
{
    frmDlgRWJHistoryPembayaran= fnDlgRWJHistoryPembayaran();
    frmDlgRWJHistoryPembayaran.show();
};

function fnDlgRWJHistoryPembayaran()
{
    var winRWJHistoryPembayaranReport = new Ext.Window
    (
        {
            id: 'winRWJHistoryPembayaranReport',
            title: 'Laporan History Pembayaran',
            closeAction: 'destroy',
            width:450,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJHistoryPembayaran()]

        }
    );

    return winRWJHistoryPembayaranReport;
};

function ItemDlgRWJHistoryPembayaran()
{
    var PnlLapRWJHistoryPembayaran = new Ext.Panel
    (
        {
            id: 'PnlLapRWJHistoryPembayaran',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJHistoryPembayaran_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJHistoryPembayaran',
					handler: function()
					{
					   if (ValidasiReportRWJHistoryPembayaran() === 1)
					   {
							var params={
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJHistoryPembayaran').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJHistoryPembayaran').getValue(),
								type_file:type_file
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/main/cetaklaporanRWJ/cetakRWJHistoryPembayaran");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJHistoryPembayaran.close(); 
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Print',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnPrintLapRWJHistoryPembayaran',
					handler: function()
					{
					   if (ValidasiReportRWJHistoryPembayaran() === 1)
					   {
							var params={
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJHistoryPembayaran').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJHistoryPembayaran').getValue(),
								type_file:type_file
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_history_pembayaran/cetak_direct");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJHistoryPembayaran.close(); 
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJHistoryPembayaran',
					handler: function()
					{
						frmDlgRWJHistoryPembayaran.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJHistoryPembayaran;
};


function ValidasiReportRWJHistoryPembayaran()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJHistoryPembayaran').dom.value === '')
	{
		ShowPesanWarningRWJHistoryPembayaranReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}

    return x;
};

function ValidasiTanggalReportRWJHistoryPembayaran()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJHistoryPembayaran').dom.value > Ext.get('dtpTglAkhirLapRWJHistoryPembayaran').dom.value)
    {
        ShowPesanWarningRWJHistoryPembayaranReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJHistoryPembayaranReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJHistoryPembayaran_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJHistoryPembayaran_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:70,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJHistoryPembayaran',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					{
					   xtype: 'checkbox',
					   fieldLabel: 'Type ',
					   id: 'CekLapPilihTypeExcel',
					   hideLabel:false,
					   boxLabel: 'Excel',
					   checked: false,
					   listeners: 
					   {
							check: function()
							{
							   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
								{
									type_file=1;
								}
								else
								{
									type_file=0;
								}
							}
					   }
					}
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJHistoryPembayaran',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


