
var dsRWJKIUP;
var selectNamaRWJKIUP;
var now = new Date();

var frmDlgRWJKIUP;
var varLapRWJKIUP= ShowFormLapRWJKIUP();

function ShowFormLapRWJKIUP()
{
    frmDlgRWJKIUP= fnDlgRWJKIUP();
    frmDlgRWJKIUP.show();
};

function fnDlgRWJKIUP()
{
    var winRWJKIUPReport = new Ext.Window
    (
        {
            id: 'winRWJKIUPReport',
            title: 'Laporan Rawat Jalan',
            closeAction: 'destroy',
            width:500,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJKIUP()]

        }
    );

    return winRWJKIUPReport;
};


function ItemDlgRWJKIUP()
{
    var PnlLapRWJKIUP = new Ext.Panel
    (
        {
            id: 'PnlLapRWJKIUP',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJKIUP_Tanggal(),
                getItemLapRWJKIUP_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWJKIUP',
                            handler: function()
                            {
                                if (ValidasiReportRWJKIUP() === 1)
                                {
                                    //if (ValidasiTanggalReportRWJKIUP() === 1)
                                    //{
                                        var criteria = GetCriteriaRWJKIUP();
                                        frmDlgRWJKIUP.close();
                                        loadlaporanRWJ('0', 'rep010205', criteria);
                                    //};
                                    //alert(tmp);
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWJKIUP',
                            handler: function()
                            {
                                    frmDlgRWJKIUP.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWJKIUP;
};

function GetCriteriaRWJKIUP()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJKIUP').dom.value !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWJKIUP').dom.value;
	};

	if (Ext.get('dtpTglAkhirLapRWJKIUP').dom.value !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRWJKIUP').dom.value;
	};
        if (Ext.getDom('rb_nama').checked === true)
        {
                strKriteria += '##@@##' + Ext.get('rb_nama').dom.value;
        }else
            {
                strKriteria += '##@@##' + Ext.get('rb_medrec').dom.value;
            }

	return strKriteria;
};


function ValidasiReportRWJKIUP()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWJKIUP').dom.value === '') || (Ext.get('dtpTglAkhirLapRWJKIUP').dom.value === ''))
    {
            if(Ext.get('dtpTglAwalLapRWJKIUP').dom.value === '')
            {
                    ShowPesanWarningRWJKIUPReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('dtpTglAkhirLapRWJKIUP').dom.value === '')
            {
                    ShowPesanWarningRWJKIUPReport(nmGetValidasiKosong(nmFinishDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
    };

    return x;
};

function ValidasiTanggalReportRWJKIUP()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJKIUP').dom.value > Ext.get('dtpTglAkhirLapRWJKIUP').dom.value)
    {
        ShowPesanWarningRWJKIUPReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJKIUPReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapRWJKIUP_Dept()
{
    var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: .90,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 150,
            items:
            [
                {
                    xtype: 'radiogroup',
                    id: 'rbrujukan',
                    fieldLabel: 'Urut Berdasarkan ',
                    items: [
                    {boxLabel: 'No. Medical Record',
                        name: 'rb_auto',
                        id: 'rb_medrec',
                        inputValue: 'K.Kd_Pasien'
                    },
                    {boxLabel: 'Nama Pasien',
                        name: 'rb_auto',
                        id: 'rb_nama',
                        inputValue: 'P.nama'
                    }
                ]
                }
            ]
        }
        ]
        
    }
    return items;
};

function getItemLapRWJKIUP_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: 0.45,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 85,
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: 'Tanggal ',
                    id: 'dtpTglAwalLapRWJKIUP',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        },
        {
            columnWidth: 0.48,
            layout: 'form',
            border: false,
                    labelWidth: 25,
            labelAlign: 'right',
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: 's/d ',
                    id: 'dtpTglAkhirLapRWJKIUP',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        }
        ]
    }
    return items;
};


function mComboUnitLapRWJKIUP()
{
    var dataSource_All = new Ext.data.Store({
	reader: new Ext.data.JsonReader
		({
			//fields: ['code',  'NoFaktur',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			fields: ['KD_UNIT', 'NAMA_UNIT']
		}),
	data:
		[
			{ KD_UNIT:'xxx', NAMA_UNIT:'Semua Unit'},
                        { KD_UNIT:'208', NAMA_UNIT:'Anak'},
                        { KD_UNIT:'209', NAMA_UNIT:'Bedah'},
                        { KD_UNIT:'215', NAMA_UNIT:'DM'},
                        { KD_UNIT:'202', NAMA_UNIT:'Gigi'},
                        { KD_UNIT:'203', NAMA_UNIT:'Gizi'},
                        { KD_UNIT:'206', NAMA_UNIT:'Interna'},
                        { KD_UNIT:'216', NAMA_UNIT:'Jantung'},
                        { KD_UNIT:'214', NAMA_UNIT:'Jiwa'},
                        { KD_UNIT:'207', NAMA_UNIT:'Kandungan'},
                        { KD_UNIT:'213', NAMA_UNIT:'KIA'},
                        { KD_UNIT:'212', NAMA_UNIT:'Kulit Kelamin'},
                        { KD_UNIT:'211', NAMA_UNIT:'Mata'},
                        { KD_UNIT:'204', NAMA_UNIT:'Syaraf'},
                        { KD_UNIT:'210', NAMA_UNIT:'THT'},
                        { KD_UNIT:'201', NAMA_UNIT:'Umum'},
                        { KD_UNIT:'205', NAMA_UNIT:'VCT'},
		]
	});
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    dsRWJKIUP = new WebApp.DataStore({ fields: Field });

    dsRWJKIUP.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    );
    
  var comboUnitLapRWJKIUP = new Ext.form.ComboBox
    (
        {
            id:'comboUnitLapRWJKIUP',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Poliklinik ',
            align:'Right',
            anchor:'99%',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store:dataSource_All,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectRWJKIUP=b.data.KD_UNIT ;
                    selectNamaRWJKIUP=b.data.NAMA_UNIT ;
                }
            }
        }
    );

//    dsRWJKIUP.load
//    (
//        {
//            params:
//            {
//                Skip: 0,
//                Take: 1000,
//                //Sort: 'DEPT_ID',
//                Sort: 'NAMA_UNIT',
//                Sortdir: 'ASC',
//                target:'ViewSetupUnit',
//                param: ''
//            }
//        }
//    );
    return comboUnitLapRWJKIUP;
};