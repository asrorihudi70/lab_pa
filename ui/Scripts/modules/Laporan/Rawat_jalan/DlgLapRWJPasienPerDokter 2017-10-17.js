
var dsRWJPasienPerDokter;
var selectRWJPasienPerDokter;
var selectNamaRWJPasienPerDokter;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJPasienPerDokter;
var varLapRWJPasienPerDokter= ShowFormLapRWJPasienPerDokter();
var dsRWJ;
function ShowFormLapRWJPasienPerDokter()
{
    frmDlgRWJPasienPerDokter= fnDlgRWJPasienPerDokter();
    frmDlgRWJPasienPerDokter.show();
};

function fnDlgRWJPasienPerDokter()
{
    var winRWJPasienPerDokterReport = new Ext.Window
    (
        {
            id: 'winRWJPasienPerDokterReport',
            title: 'Laporan Pasien per Dokter',
            closeAction: 'destroy',
            width:450,
            height: 260,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJPasienPerDokter()]
        }
    );

    return winRWJPasienPerDokterReport;
};

function ItemDlgRWJPasienPerDokter()
{
    var PnlLapRWJPasienPerDokter = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPasienPerDokter',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 300,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJPasienPerDokter_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJPasienPerDokter',
					handler: function()
					{
					   if (ValidasiReportRWJPasienPerDokter() === 1)
					   {								
							var params={
								kd_poli:Ext.getCmp('cboUnitPilihanRwJPasien').getValue(),
								kd_dokter:Ext.getCmp('cboDokterPilihanRwJPasien').getValue(),
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJPasienPerDokter').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJPasienPerDokter').getValue(),
								type_file:Ext.getCmp('CekLapPilihTypeExcel').getValue(),
								order_by:Ext.getCmp('cboOrderBy').getValue(),
							} 
							//console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_RWJPasienPerDokter/cetak");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJPasienPerDokter.close(); 
							
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Print',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnPrintLapRWJPasienPerDokter',
					handler: function()
					{
					   if (ValidasiReportRWJPasienPerDokter() === 1)
					   {								
							var params={
								kd_poli:Ext.getCmp('cboUnitPilihanRwJPasien').getValue(),
								kd_dokter:Ext.getCmp('cboDokterPilihanRwJPasien').getValue(),
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJPasienPerDokter').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJPasienPerDokter').getValue(),
								type_file:Ext.getCmp('CekLapPilihTypeExcel').getValue(),
								order_by:Ext.getCmp('cboOrderBy').getValue(),
							} 
							//console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_pasienperdokter_rwj/cetak_direct");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJPasienPerDokter.close(); 
							
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJPasienPerDokter',
					handler: function()
					{
						frmDlgRWJPasienPerDokter.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJPasienPerDokter;
};


function ValidasiReportRWJPasienPerDokter()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJPasienPerDokter').dom.value === '')
	{
		ShowPesanWarningRWJPasienPerDokterReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
    return x;
};

function ValidasiTanggalReportRWJPasienPerDokter()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJPasienPerDokter').dom.value > Ext.get('dtpTglAkhirLapRWJPasienPerDokter').dom.value)
    {
        ShowPesanWarningRWJPasienPerDokterReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJPasienPerDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};



function getItemLapRWJPasienPerDokter_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  '100%',
				height: 300,
				items:
				[
					{
						x: 10,
						y: 10,
						width: 170,
						xtype: 'label',
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJPasienPerDokter',
						format: 'd-M-Y',
						value:now,
						//anchor: '99%'
					},
					{
						x: 190,
						y: 10,
						width: 10,
						xtype: 'label',
						text : 's/d'
					},
					{
						x: 220,
						y: 10,
						width: 170,
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJPasienPerDokter',
						format: 'd-M-Y',
						value:now,
						//anchor: '100%'
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Poli '
					}, {
						x: 190,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
						mComboUnitPilihanRwJPasienPerDokter(),
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Dokter '
					}, {
						x: 190,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
						mComboDokterPilihanRwJPasienPerDokter(),
					{
						x: 10,
						y: 100,
						xtype: 'label',
						text: 'Order By '
					}, {
						x: 190,
						y: 100,
						xtype: 'label',
						text: ' : '
					},
						mComboOrder(),
					{
						x: 10,
						y: 130,
						xtype: 'label',
						text: 'Type File '
					}, {
						x: 190,
						y: 130,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 220,
						y: 130,
						xtype: 'checkbox',
						id: 'CekLapPilihTypeExcel',
						hideLabel:false,
						boxLabel: 'Excel',
						checked: false,
						listeners: 
						{
						}
					}
				]
			},
     
        ]
    }
    return items;
};

function mComboUnitPilihanRwJPasienPerDokter()
{

   	var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});


	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnitB',
                param: "left(kd_unit, 1)='2' and type_unit=false"
            }
        }
    )

    var cboPilihanRwJTransaksi = new Ext.form.ComboBox
	(
			{
			x: 220,
			y: 40,
            id: 'cboUnitPilihanRwJPasien',
            ypeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Dokter ...',
            fieldLabel: 'Dokter ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            width:170,
            value:'Semua',
			tabIndex:29,
            listeners:
            {
                'select': function(a, b, c)
				{
						//Dokterpilihanpasien=b.data.KD_UNIT;
				},
			}
        }
	);
	return cboPilihanRwJTransaksi;
};

function mComboDokterPilihanRwJPasienPerDokter()
{

   var Field = ['KD_DOKTER','NAMA'];
    ds_Dokter_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Dokter_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA',
                Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: " WHERE left(dokter_klinik.kd_unit, 1)='2'"
            }
        }
    )
    var cboPilihanRwJTransaksi = new Ext.form.ComboBox
	(
			{
			x: 220,
			y: 70,
            id: 'cboDokterPilihanRwJPasien',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Dokter ...',
            fieldLabel: 'Dokter ',
            align: 'Right',
            sorters: [{
		        property: 'NAMA',
		        direction: 'ASC'
		    }],
            store: ds_Dokter_viDaftar,
            valueField: 'KD_DOKTER',
            displayField: 'NAMA',
            value:'Semua',
            width:170,
			tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
					{
						//Dokterpilihanpasien=b.data.KD_UNIT;
					},


		}
        }
	);
	return cboPilihanRwJTransaksi;
};

function mComboOrder()
{
    var cboOrderBy = new Ext.form.ComboBox
    (
        {
            x: 220,
            y: 100,
            id:'cboOrderBy',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: 'Order  ',
            width:170,
            value:1,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                    data: [[0, 'Medrec'], [1, 'Nama Pasien'], [2, 'Tanggal Masuk'], [3, 'Penjamin']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:'Medrec',
        }
    );
    return cboOrderBy;
};