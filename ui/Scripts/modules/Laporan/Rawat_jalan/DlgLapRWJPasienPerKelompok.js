
var dsRWJPasienPerKelompok;
var selectRWJPasienPerKelompok;
var selectNamaRWJPasienPerKelompok;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJPasienPerKelompok;
var varLapRWJPasienPerKelompok= ShowFormLapRWJPasienPerKelompok();
var dsRWJ;


var selectSetPilihankelompokPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapRWJPasienPerKelompok()
{
    frmDlgRWJPasienPerKelompok= fnDlgRWJPasienPerKelompok();
    frmDlgRWJPasienPerKelompok.show();
};

function fnDlgRWJPasienPerKelompok()
{
    var winRWJPasienPerKelompokReport = new Ext.Window
    (
        {
            id: 'winRWJPasienPerKelompokReport',
            title: 'Laporan Pasien per Kelompok',
            closeAction: 'destroy',
            width:450,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJPasienPerKelompok()]
        }
    );

    return winRWJPasienPerKelompokReport;
};

function ItemDlgRWJPasienPerKelompok()
{
    var PnlLapRWJPasienPerKelompok = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPasienPerKelompok',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 280,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJPasienPerKelompok_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJPasienPerKelompok',
					handler: function()
					{
					   if (ValidasiReportRWJPasienPerKelompok() === 1)
					   {								
							var params={
								kd_poli:Ext.getCmp('cboUnitPilihanRwJPasien').getValue(),
								kd_kelompok:GetCriteriaRWJPasienPerKelompok(),
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJPasienPerKelompok').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJPasienPerKelompok').getValue(),
								type_file:Ext.getCmp('CekLapPilihTypeExcel').getValue(),
								order_by:Ext.getCmp('cboOrderBy').getValue(),
							} 
							//console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_RWJPasienPerKelompok/cetak");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJPasienPerKelompok.close(); 
							
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJPasienPerKelompok',
					handler: function()
					{
						frmDlgRWJPasienPerKelompok.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJPasienPerKelompok;
};


function ValidasiReportRWJPasienPerKelompok()
{
    var x=1;
	if(Ext.getCmp('dtpTglAwalLapRWJPasienPerKelompok').getValue() >Ext.getCmp('dtpTglAkhirLapRWJPasienPerKelompok').getValue())
    {
        ShowPesanWarningPelayananDokterReport('Tanggal akhir tidak boleh lebih cepat dari tanggal awal','Laporan Pasien per Kelompok');
        x=0;
    }
	

    return x;
};


function ShowPesanWarningRWJPasienPerKelompokReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};



function getItemLapRWJPasienPerKelompok_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  '100%',
				height: 300,
				items:
				[
					{
						x: 10,
						y: 10,
						width: 170,
						xtype: 'label',
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJPasienPerKelompok',
						format: 'd-M-Y',
						value:now,
						//anchor: '99%'
					},
					{
						x: 190,
						y: 10,
						width: 10,
						xtype: 'label',
						text : 's/d'
					},
					{
						x: 220,
						y: 10,
						width: 170,
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJPasienPerKelompok',
						format: 'd-M-Y',
						value:now,
						//anchor: '100%'
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Poli '
					}, {
						x: 190,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
						mComboUnitPilihanRwJPasienPerKelompok(),
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Kelompok '
					}, {
						x: 190,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
						mComboRWJPasienPerKelompok(),
					{
						x: 10,
						y: 100,
						xtype: 'label',
						text: 'Detail '
					}, {
						x: 190,
						y: 100,
						xtype: 'label',
						text: ' : '
					},
						mComboRWJPasienPerKelompokSEMUA(),
						mComboRWJPasienPerKelompokPERORANGAN(),
						mComboRWJPasienPerKelompokPERUSAHAAN(),
						mComboRWJPasienPerKelompokASURANSI(),
					{
						x: 10,
						y: 130,
						xtype: 'label',
						text: 'Order By '
					}, {
						x: 190,
						y: 130,
						xtype: 'label',
						text: ' : '
					},
					mComboOrder(),
					{
						x: 10,
						y: 160,
						xtype: 'label',
						text: 'Type File'
					}, {
						x: 190,
						y: 160,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 220,
						y: 160,
						xtype: 'checkbox',
						fieldLabel: 'Type ',
						id: 'CekLapPilihTypeExcel',
						hideLabel:false,
						boxLabel: 'Excel',
						checked: false,
						listeners: 
						{
						}
					},
				]
			},
     
        ]
    }
    return items;
};

function mComboUnitPilihanRwJPasienPerKelompok()
{
   var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnitB',
                param: "left(kd_unit, 1)='2' and type_unit=false"
            }
        }
    )
    var cboPilihanRwJTransaksi = new Ext.form.ComboBox
	(
			{
			x: 220,
			y: 40,
            id: 'cboUnitPilihanRwJPasien',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poliklinik ...',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            value: 'Semua',
            width:170,
			tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
				{
					polipilihanpasien=b.data.KD_UNIT;
				},


		}
        }
	);
	return cboPilihanRwJTransaksi;
};

function mComboRWJPasienPerKelompok()
{
    var cboPilihanPelayananDokterkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 220,
                y: 70,
                id:'cboPilihanRWJPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 170,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectRWJPasienPerKelompok(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanPelayananDokterkelompokPasien;
};

//RWJPasienPerKelompok
function mComboRWJPasienPerKelompokSEMUA()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
                x: 220,
                y: 100,
                id:'IDmComboRWJPasienPerKelompokSEMUA',
                typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				selectOnFocus:true,
				forceSelection: true,
				emptyText:'Silahkan Pilih...',
				valueField: 'Id',
	            displayField: 'displayText',
	            hidden:false,
				fieldLabel: '',
				width:170,
				value:1,
				store: new Ext.data.ArrayStore
				(
						{
							id: 0,
							fields:
								[
										'Id',
										'displayText'
								],
							data: [[1, 'Semua']]
						}
				),
				listeners:
				{
					'select': function(a,b,c)
					{
						selectSetUmum=b.data.displayText ;
					}
	                                
	                            
				}
            }
	);
	return cboPerseoranganRWJ;
};


function mComboOrder()
{
    var cboOrderBy = new Ext.form.ComboBox
	(
		{
			x: 220,
			y: 130,
			id:'cboOrderBy',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Order  ',
			width:170,
			value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				    data: [[0, 'Medrec'],[1, 'Nama Pasien'],[2, 'Tanggal Masuk']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Medrec',
		}
	);
	return cboOrderBy;
};


function mComboRWJPasienPerKelompokPERORANGAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
                x: 220,
                y: 100,
                id:'IDmComboRWJPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:170,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokPERUSAHAAN()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1  order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
                x: 220,
                y: 100,
                id:'IDmComboRWJPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width:170,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
				//value: selectsetperusahaan,
                listeners:
                {
				    'select': function(a,b,c)
					{
				        selectsetperusahaan = b.data.KD_CUSTOMER;
						selectsetnamaperusahaan = b.data.CUSTOMER;
					}
                }
            }
	);
	return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokASURANSI()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=2  order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
                x: 220,
                y: 100,
                id:'IDmComboRWJPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width:170,
                store: dsPerseoranganRequestEntry,
				valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
				    'select': function(a,b,c)
					{
						selectSetAsuransi=b.data.KD_CUSTOMER ;
						selectsetnamaAsuransi = b.data.CUSTOMER ;
					}
                }
            }
	);
	return cboPerseoranganRWJ;
};


function Combo_SelectRWJPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').show();
   }
};


function GetCriteriaRWJPasienPerKelompok()
{
	var strKriteria = '';
	
	if (Ext.getCmp('cboPilihanRWJPasienPerKelompok').getValue() !== '')
	{
		if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
		else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').getValue(); } 
		else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').getValue(); } 
		else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').getValue(); }
	}else{
            strKriteria = 'Semua';
	}

	
	return strKriteria;
};