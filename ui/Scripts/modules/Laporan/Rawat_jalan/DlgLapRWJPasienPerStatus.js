
var dsRWJPasienPerStatus;
var selectRWJPasienPerStatus;
var selectNamaRWJPasienPerStatus;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJPasienPerStatus;
var varLapRWJPasienPerStatus= ShowFormLapRWJPasienPerStatus();
var dsRWJ;


var selectSetPilihanStatusPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapRWJPasienPerStatus()
{
    frmDlgRWJPasienPerStatus= fnDlgRWJPasienPerStatus();
    frmDlgRWJPasienPerStatus.show();
};

function fnDlgRWJPasienPerStatus()
{
    var winRWJPasienPerStatusReport = new Ext.Window
    (
        {
            id: 'winRWJPasienPerStatusReport',
            title: 'Laporan Pasien per Status',
            closeAction: 'destroy',
            width:450,
            height: 230,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJPasienPerStatus()]
        }
    );

    return winRWJPasienPerStatusReport;
};

function ItemDlgRWJPasienPerStatus()
{
    var PnlLapRWJPasienPerStatus = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPasienPerStatus',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 230,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJPasienPerStatus_Periode(),
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJPasienPerStatus',
					handler: function()
					{
					   if (ValidasiReportRWJPasienPerStatus() === 1)
					   {								
							var params={
								kd_poli:Ext.getCmp('IDcboUnitPilihanRwJPasien').getValue(),
								kd_status:Ext.getCmp('IDmComboRWJPasienPerStatus').getValue(),
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJPasienPerStatus').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJPasienPerStatus').getValue(),
								order_by:Ext.getCmp('cboOrderBy').getValue(),
							} 
							//console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_RWJPasienPerStatus/cetak");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJPasienPerStatus.close(); 
							
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJPasienPerStatus',
					handler: function()
					{
						frmDlgRWJPasienPerStatus.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJPasienPerStatus;
};


function ValidasiReportRWJPasienPerStatus()
{
    var x=1;
	if(Ext.getCmp('dtpTglAwalLapRWJPasienPerStatus').getValue() >Ext.getCmp('dtpTglAkhirLapRWJPasienPerStatus').getValue())
    {
        ShowPesanWarningPelayananDokterReport('Tanggal akhir tidak boleh lebih cepat dari tanggal awal','Laporan Pasien per Status');
        x=0;
    }
	

    return x;
};


function ShowPesanWarningRWJPasienPerStatusReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};



function getItemLapRWJPasienPerStatus_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  '100%',
				height: 200,
				items:
				[
					{
						x: 10,
						y: 10,
						width: 170,
						xtype: 'label',
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJPasienPerStatus',
						format: 'd-M-Y',
						value:now,
						//anchor: '99%'
					},
					{
						x: 190,
						y: 10,
						width: 10,
						xtype: 'label',
						text : 's/d'
					},
					{
						x: 220,
						y: 10,
						width: 170,
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJPasienPerStatus',
						format: 'd-M-Y',
						value:now,
						//anchor: '100%'
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Poli '
					}, {
						x: 190,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
						mComboUnitPilihanRwJPasienPerStatus(),
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Status '
					}, {
						x: 190,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
						mComboRWJPasienPerStatus(),
					{
						x: 10,
						y: 100,
						xtype: 'label',
						text: 'Order '
					}, {
						x: 190,
						y: 100,
						xtype: 'label',
						text: ' : '
					},
						mComboOrder(),
					
				]
			},
     
        ]
    }
    return items;
};

function mComboUnitPilihanRwJPasienPerStatus()
{
   var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ComboUnit_transakasi',
                param: ""
            }
        }
    )
    var cboPilihanRwJTransaksi = new Ext.form.ComboBox
	(
			{
			x: 220,
			y: 40,
            id: 'IDcboUnitPilihanRwJPasien',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poliklinik ...',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            width:170,
			tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
				{
					polipilihanpasien=b.data.KD_UNIT;
				},


		}
        }
	);
	return cboPilihanRwJTransaksi;
};

function mComboRWJPasienPerStatus()
{
   var Field = ['ID_STATUS','STATUS'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'STATUS',
                Sortdir: 'ASC',
                target:'ViewComboLookupStatusPulang',
                param: ""
            }
        }
    )
    var cboPilihanRwJTransaksi = new Ext.form.ComboBox
	(
			{
			x: 220,
			y: 70,
            id: 'IDmComboRWJPasienPerStatus',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Status ...',
            fieldLabel: 'Status ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'ID_STATUS',
            displayField: 'STATUS',
            width:170,
			tabIndex:29,
			
            listeners:
                {
                'select': function(a, b, c)
				{
					/*polipilihanpasien=b.data.KD_UNIT;*/
				},


		}
        }
	);
	return cboPilihanRwJTransaksi;
};

function mComboOrder()
{
    var cboOrderBy = new Ext.form.ComboBox
    (
        {
            x: 220,
            y: 100,
            id:'cboOrderBy',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: 'Order  ',
            width:170,
            value:1,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                    data: [[0, 'Medrec'], [1, 'Nama Pasien'], [2, 'Tanggal Masuk'], [3, 'Penjamin']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:'Medrec',
        }
    );
    return cboOrderBy;
};