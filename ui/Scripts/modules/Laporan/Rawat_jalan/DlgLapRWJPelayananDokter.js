var type_file=0;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRWJPelayananDokter;
var selectNamaRWJPelayananDokter;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRWJPelayananDokter;
var varLapRWJPelayananDokter= ShowFormLapRWJPelayananDokter();
var selectSetUmum;
var selectSetkelpas;
var firstGrid;
var secondGrid;
var cmb_kd_customer_value = 'SEMUA';
var secondGridStore;
var dsDokterPelayaranDokter;
var dataSource_unitLapRWJPelayananDokter;
var DataStore_combouserLapRWJPelayananDokter;
var combouserLapRWJPelayananDokter;

var selectSetPilihankelompokPasien;
var selectSetPilihanProfesi = "Dokter"; 
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;
var tmp_unit = "";

function ShowFormLapRWJPelayananDokter()
{
    var Field               = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dataStoreDokter("Dokter", null);
    frmDlgRWJPelayananDokter= fnDlgRWJPelayananDokter();
    frmDlgRWJPelayananDokter.show();
};

function fnDlgRWJPelayananDokter()
{
    var winRWJPelayananDokterReport = new Ext.Window
    (
        {
            id: 'winRWJPelayananDokterReport',
            title: 'Laporan Kinerja Pelaksana Per Tindakan',
            closeAction: 'destroy',
            width:400,
            height: 525,
            border: false,
            resizable:false,
            plain: true,
            // constrain: true,
            layout: 'form',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJPelayananDokter()],
            listeners:
            {
                activate: function()
                {
                    /*Ext.getCmp('cboPerseoranganRWJ').show();
                    Ext.getCmp('cboAsuransiRWJ').hide();
                    Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
                    Ext.getCmp('cboUmumRWJ').hide();*/
                }
            },
            fbar:[
                {
                    xtype: 'button',
                    text: 'Preview',
                    width: 70,
                    hideLabel: true,
                    id: 'btnOkLapRWJPelayananDokter',
                    handler: function()
                    {       
                        // if(Ext.getCmp('cbPendaftaran_RWJPelayananDokter').getValue()=='' && Ext.getCmp('cbTindakRWJ_RWJPelayananDokter').getValue()==''){
                            // ShowPesanWarningRWJPelayananDokterReport('Pilih laporan pendaftaran atau tindakan WJ!','WARNING');
                        // } else{
                            var params={
                                kd_profesi          :Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue(),
                                pelayananPendaftaran:Ext.getCmp('cbPendaftaran_RWJPelayananDokter').getValue(),
                                pelayananTindak     :Ext.getCmp('cbTindakRWJ_RWJPelayananDokter').getValue(),
                                kd_user             :Ext.getCmp('combouserLapRWJPelayananDokter').getValue(),
                                kd_dokter           :GetCriteriaRWJProfesi(),
                                kelompok            :selectSetPilihankelompokPasien,
                                profesi             :selectSetPilihanProfesi,
                                kd_kelompok         :GetCriteriaRWJPasienPerKelompok(),
                                kd_customer:cmb_kd_customer_value,
                                tglAwal:Ext.getCmp('dtpTglAwalLapRWJPelayananDokter').getValue(),
                                tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJPelayananDokter').getValue(),
                                JmlList:secondGridStore.getCount(),
                                full_name:Ext.get('combouserLapRWJPelayananDokter').getValue(),
                                type_file:type_file,
                                detail:false
                            } 
                            var i=0;
                            var tmp_kd_unit = "";
                            for(i=0; i<secondGridStore.getCount(); i++){
                                tmp_kd_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";     
                            }
                            tmp_kd_unit = tmp_kd_unit.substring(0, tmp_kd_unit.length - 1);
                            params['kd_unit'] = tmp_kd_unit;
                            var form = document.createElement("form");
                            form.setAttribute("method", "post");
                            form.setAttribute("target", "_blank");
                            form.setAttribute("action", baseURL+"index.php/rawat_jalan/lap_RWJPelayananDokter/cetak");
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", "data");
                            hiddenField.setAttribute("value", Ext.encode(params));
                            form.appendChild(hiddenField);
                            document.body.appendChild(form);
                            console.log(params);
                            form.submit();      
                            // frmDlgRWJPasienPerKelompok.close(); 
                        }
                    // }
                },
                {
                    xtype: 'button',
                    text: 'Print',
                    width: 70,
                    hideLabel: true,
                    hidden  : true,
                    id: 'btnPrintLapRWJPelayananDokter',
                    handler: function()
                    {       
                        if(Ext.getCmp('cbPendaftaran_RWJPelayananDokter').getValue()=='' && Ext.getCmp('cbTindakRWJ_RWJPelayananDokter').getValue()==''){
                            ShowPesanWarningRWJPelayananDokterReport('Pilih laporan pendaftaran atau tindakan WJ!','WARNING');
                        } else{
                            var params={
                                kd_profesi:Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue(),
                                pelayananPendaftaran:Ext.getCmp('cbPendaftaran_RWJPelayananDokter').getValue(),
                                pelayananTindak:Ext.getCmp('cbTindakRWJ_RWJPelayananDokter').getValue(),
                                kd_user:Ext.getCmp('combouserLapRWJPelayananDokter').getValue(),
                                kd_dokter:GetCriteriaRWJProfesi(),
                                kelompok            :selectSetPilihankelompokPasien,
                                profesi             :selectSetPilihanProfesi,
                                kd_kelompok:GetCriteriaRWJPasienPerKelompok(),
                                kd_customer:cmb_kd_customer_value,
                                tglAwal:Ext.getCmp('dtpTglAwalLapRWJPelayananDokter').getValue(),
                                tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJPelayananDokter').getValue(),
                                JmlList:secondGridStore.getCount(),
                                full_name:Ext.get('combouserLapRWJPelayananDokter').getValue(),
                                type_file:type_file,
                                detail:false
                            } 
                            var i=0;
                            var tmp_kd_unit = "";
                            for(i=0; i<secondGridStore.getCount(); i++){
                                tmp_kd_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";     
                            }
                            tmp_kd_unit = tmp_kd_unit.substring(0, tmp_kd_unit.length - 1);
                            params['kd_unit'] = tmp_kd_unit;
                            var form = document.createElement("form");
                            form.setAttribute("method", "post");
                            form.setAttribute("target", "_blank");
                            form.setAttribute("action", baseURL + "index.php/gawat_darurat/lap_RWJTindakanDokter/cetakDirect");
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", "data");
                            hiddenField.setAttribute("value", Ext.encode(params));
                            form.appendChild(hiddenField);
                            document.body.appendChild(form);
                            console.log(params);
                            form.submit();      
                            // frmDlgRWJPasienPerKelompok.close(); 
                        }
                    }
                },
                {
                    xtype: 'button',
                    text: 'Cancel' ,
                    width: 70,
                    hideLabel: true,
                    id: 'btnCancelLapRWJPelayananDokter',
                    handler: function()
                    {
                            frmDlgRWJPelayananDokter.close();
                    }
                }
            
            ]

        }
    );

    return winRWJPelayananDokterReport;
};


function ItemDlgRWJPelayananDokter()
{
    var PnlLapRWJPelayananDokter = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPelayananDokter',
            fileUpload: true,
            layout: 'form',
            //width:400,
            height: '590',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                gridPoliklinikLapRWJPelayananDokter(),
                getItemLapRWJPelayananDokter_Atas(),
                /* {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        
                    ]
                } */
            ]
        }
    );

    return PnlLapRWJPelayananDokter;
};

function gridPoliklinikLapRWJPelayananDokter(){
    var Field_poli_viDaftarLapRWJPelayananDokter = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unitLapRWJPelayananDokter = new WebApp.DataStore({fields: Field_poli_viDaftarLapRWJPelayananDokter});
    
    datarefresh_viInformasiUnitLapRWJPelayananDokter()

    // Generic fields array to use in both store defs.
    var fields = [
        {name: 'KD_UNIT', mapping : 'KD_UNIT'},
        {name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
    ];


    // Column Model shortcut array
    var cols = [
        { id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
        {header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
    ];


    // declare the source Grid
        firstGrid = new Ext.grid.GridPanel({
        ddGroup          : 'secondGridDDGroup',
        store            : dataSource_unitLapRWJPelayananDokter,
        autoScroll       : true,
        columnLines      : true,
        border           : true,
        enableDragDrop   : true,
        height           : 200,
        stripeRows       : true,
        trackMouseOver   : true,
        title            : 'Unit',
        anchor           : '100% 100%',
        plugins          : [new Ext.ux.grid.FilterRow()],
        colModel         : new Ext.grid.ColumnModel
        (
            [
                    new Ext.grid.RowNumberer(),
                    {
                            id: 'colNRM_viDaftar',
                            header: 'No.Medrec',
                            dataIndex: 'KD_UNIT',
                            sortable: true,
                            hidden : true
                    },
                    {
                            id: 'colNMPASIEN_viDaftar',
                            header: 'Nama',
                            dataIndex: 'NAMA_UNIT',
                            sortable: true,
                            width: 50
                    }
            ]
        ),
        listeners : {
            afterrender : function(comp) {
                var firstGridDropTargetEl =  firstGrid.getView().scroller.dom;
                var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
                        ddGroup    : 'firstGridDDGroup',
                        notifyDrop : function(ddSource, e, data){
                                var records =  ddSource.dragData.selections;
                                Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                firstGrid.store.add(records);
                                firstGrid.store.sort('KD_UNIT', 'ASC');
                                tmp_unit = "";

                                for(i=0; i<secondGridStore.data.length; i++){
                                    // params['kd_unit'+i]=;        
                                    // console.log(secondGridStore.modified[i].data.KD_UNIT);
                                    tmp_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";

                                }
                                tmp_unit = tmp_unit.substr(0, tmp_unit.length-1);
                                // console.log(tmp_unit);
                                dataStoreDokter(Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue(), tmp_unit);
                                return true;
                        }
                });
            }
        },
        viewConfig: 
            {
                    forceFit: true
            }
    });

    secondGridStore = new Ext.data.JsonStore({
        fields : fields,
        root   : 'records'
    });

    // create the destination Grid
    secondGrid = new Ext.grid.GridPanel({
            ddGroup          : 'firstGridDDGroup',
            store            : secondGridStore,
            columns          : cols,
            enableDragDrop   : true,
            height           : 200,
            stripeRows       : true,
            autoExpandColumn : 'KD_UNIT',
            title            : 'Unit yang dipilih',
            listeners : {
            afterrender : function(comp) {
            var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
            var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                    ddGroup    : 'secondGridDDGroup',
                    notifyDrop : function(ddSource, e, data){
                            // Combo_SelectRWJProfesi(Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue());
                            // Ext.getCmp('cboDokterRWJPelayananDokter').removeAll();
                            // dsDokterPelayaran.removeAll();
                            tmp_unit = "";
                            var records =  ddSource.dragData.selections;
                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                            secondGrid.store.add(records);
                            secondGrid.store.sort('KD_UNIT', 'ASC');
                            for(i=0; i<secondGridStore.data.length; i++){
                                // params['kd_unit'+i]=;        
                                // console.log(secondGridStore.modified[i].data.KD_UNIT);
                                tmp_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";
                            }
                            tmp_unit = tmp_unit.substr(0, tmp_unit.length-1);
                            // console.log(Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue());
                            dataStoreDokter(Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue(), tmp_unit);
                            // mComboDokterRWJPelayananDokter(tmp_unit);
                            // mComboDokterRWJPelayananPerawat(tmp_unit);
                            return true;
                    }
            });
            }
        },
        viewConfig: 
            {
                    forceFit: true
            }
    });
    
    var FrmTabs_viInformasiUnitdokter = new Ext.Panel
    ({
            
            closable: true,
            region: 'center',
            layout: 'column',
            height: 220,
            itemCls: 'blacklabel',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            shadhow: true,
            margins: '0 5 5 0',
            anchor: '99%',
            items: 
            [
                {
                    columnWidth: .50,
                    layout: 'form',
                    border: false,
                    autoScroll: true,
                    bodyStyle: 'padding: 10px 10px 10px 10px',
                    items:
                    [firstGrid
                        
                    ]
                },
                {
                    columnWidth: .50,
                    layout: 'form',
                    bodyStyle: 'padding: 10px 10px 10px 10px',
                    border: false,
                    anchor: '100% 100%',
                    items:
                    [secondGrid
                        
                    ]
                },
                
                
            ],

    })
    return FrmTabs_viInformasiUnitdokter;
}

function getItemLapRWJPelayananDokter_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 275,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 0,
                xtype: 'checkbox',
                id: 'CekLapPilihSemuaRWJPelayananDokter',
                hideLabel:false,
                boxLabel: 'Pilih Semua Unit',
                checked: false,
                listeners: 
               {
                    check: function()
                    {
                       if(Ext.getCmp('CekLapPilihSemuaRWJPelayananDokter').getValue()===true)
                        {
                             firstGrid.getSelectionModel().selectAll();
                        }
                        else
                        {
                            firstGrid.getSelectionModel().clearSelections();
                        }
                    }
               }
            },
            {
                x: 196,
                y: 0,
                xtype: 'checkbox',
                id: 'CekLapResetRWJPelayananDokter',
                hideLabel:false,
                boxLabel: 'Reset Unit',
                checked: false,
                listeners: 
               {
                    check: function()
                    {
                        if(Ext.getCmp('CekLapResetRWJPelayananDokter').getValue()===true)
                        {
                            secondGridStore.removeAll();
                            tmp_unit = "";
                            dataStoreDokter(Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue(), tmp_unit);
                            datarefresh_viInformasiUnitLapRWJPelayananDokter();
                            Ext.getCmp('CekLapPilihSemuaRWJPelayananDokter').setValue(false);
                        } else{
                            
                        }
                    }
               }
            },
            {
                xtype: 'checkboxgroup',
                width:210,
                x: 120,
                y: 40,
                hidden : true,
                items: 
                [
                    {
                        
                        boxLabel: 'Pendaftaran',
                        name: 'cbPendaftaran_RWJPelayananDokter',
                        id : 'cbPendaftaran_RWJPelayananDokter'
                    },
                    {
                        boxLabel: 'Tindak RWJ',
                        name: 'cbTindakRWJ_RWJPelayananDokter',
                        id : 'cbTindakRWJ_RWJPelayananDokter'
                    }
               ]
            },
            //  ================================================================================== POLIKLINIK
            /* {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Poliklinik '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mCombounitRWJPelayananDokter(), */
            //  ================================================================================== DOKTER
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Bagian '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboRWJJenisProfesi(),
                // mComboDokterRWJPelayananPerawat(),
                mComboDokterRWJPelayananDokter(),

            {
                x: 10,
                y: 100,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 100,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 100,
                xtype: 'datefield',
                id: 'dtpTglAwalLapRWJPelayananDokter',
                format: 'd/M/Y',
                value: now,
                //value: tigaharilalu
            }, {
                x: 230,
                y: 100,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 100,
                xtype: 'datefield',
                id: 'dtpTglAkhirLapRWJPelayananDokter',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 130,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 130,
                xtype: 'label',
                text: ' : '
            },
                mComboRWJPasienPerKelompok(),
                mComboRWJPasienPerKelompokSEMUA(),
                mComboRWJPasienPerKelompokPERORANGAN(),
                mComboRWJPasienPerKelompokPERUSAHAAN(),
                mComboRWJPasienPerKelompokASURANSI(),
            {
                x: 10,
                y: 190,
                xtype: 'label',
                text: 'Operator ',
                hidden : true,
            }, {
                x: 110,
                y: 190,
                xtype: 'label',
                text: ' : ',
                hidden : true,
            },
            ComboOperatorLapRWJPelayananDokter(),
            {
                x: 10,
                y: 190,
                xtype: 'label',
                text: 'Type File '
            }, {
                x: 110,
                y: 190,
                xtype: 'label',
                text: ' : '
            },
            {
                x: 120,
                y: 190,
                xtype: 'checkbox',
                id: 'CekLapPilihTypeExcel',
                hideLabel:false,
                boxLabel: 'Excel',
                checked: false,
                listeners: 
                {
                    check: function()
                    {
                       if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
                        {
                            type_file=1;
                        }
                        else
                        {
                            type_file=0;
                        }
                    }
               }
            }
            ]
        },
        ]
    };
    return items;
};

function ShowPesanWarningRWJPelayananDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function datarefresh_viInformasiUnitLapRWJPelayananDokter()
{
    dataSource_unitLapRWJPelayananDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewComboUnitLaporan',
                param: "left(kd_unit, 1)='2'"
            }
        }
    )
    //alert("refersh")
}

function ComboOperatorLapRWJPelayananDokter(){
        var Field = ['kd_user','full_name'];
        DataStore_combouserLapRWJPelayananDokter = new WebApp.DataStore({fields: Field});
       /// loaduserLapRWJPelayananDokter();
        combouserLapRWJPelayananDokter = new Ext.form.ComboBox({
            typeAhead: true,
            x: 120,
            y: 190,
            hidden : true,
            id:'combouserLapRWJPelayananDokter',
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: '',
            width:150,
            store: DataStore_combouserLapRWJPelayananDokter,
            valueField: 'kd_user',
            displayField: 'full_name',
            value:'Semua',
            listeners:{
                'select': function(a,b,c){
                }
            }
        });
        return combouserLapRWJPelayananDokter;
}

function loaduserLapRWJPelayananDokter(){
    Ext.Ajax.request({
        url: baseURL + "index.php/gawat_darurat/lap_RWJPelayananDokter/getUser",
        params: '0',
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            DataStore_combouserLapRWJPelayananDokter.removeAll();
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType =  DataStore_combouserLapRWJPelayananDokter.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                DataStore_combouserLapRWJPelayananDokter.add(recs);
            }
        }
    });
}

function dataStoreDokter(level = null, kd_unit = null){
    var criteriaUnit        = "";
    var criteriaDokter      = "";
    if (kd_unit == '') { kd_unit = null; }
    dsDokterPelayaranDokter.removeAll();
    // console.log(level);
    if (level == "Dokter" || level == 1 ) {
        criteriaDokter = " dokter.jenis_dokter='1' ";
    }else{
        criteriaDokter = " left(dokter_klinik.kd_unit, 1)='3' AND dokter.jenis_dokter='0' ";
    }
    if (kd_unit != null) {
        criteriaUnit = " AND dokter_klinik.kd_unit in ("+kd_unit+") ";
    }
    // dsDokterPelayaranDokter.removeAll();
    dsDokterPelayaranDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: " WHERE "+criteriaDokter+criteriaUnit
            }
        }
    );
}

function mComboDokterRWJPelayananDokter()
{
    // console.log(dsDokterPelayaranDokter);
    var cboPilihanRWJPelayananDokter = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'cboDokterRWJPelayananDokter',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:false,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanRWJPelayananDokter;
};

function mComboDokterRWJPelayananPerawat(kd_unit = null)
{
    var cboPilihanRWJPelayananDokter = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboDokterRWJPelayananPerawat',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:false,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanRWJPelayananDokter;
};


function mCombounitRWJPelayananDokter()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

    ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewCombounit_Konfigurasi',
                    param: " "
                }
            }
        );

    var cbounitRequestEntryRWJPelayananDokter = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 70,
            id: 'cbounitRequestEntryRWJPelayananDokter',
            width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
                        selectSetUnit =b.data.displayField;                     
                    }
                    
                }
        }
    )

    return cbounitRequestEntryRWJPelayananDokter;
};


function mComboRWJPasienPerKelompok()
{
    var cboPilihanRWJPelayananDokterkelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 130,
                id:'cboPilihanRWJPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectRWJPasienPerKelompok(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanRWJPelayananDokterkelompokPasien;
};

function mComboRWJJenisProfesi()
{
    var cboPilihanRWJJenisProfesi = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 40,
                id:'IDcboPilihanRWJJenisProfesi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Dokter'], [2, 'Perawat']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        tmp_unit = "";
                        for(i=0; i<secondGridStore.data.length; i++){
                            // params['kd_unit'+i]=;        
                            // console.log(secondGridStore.modified[i].data.KD_UNIT);
                            tmp_unit += "'"+secondGridStore.data.items[i].data.KD_UNIT+"',";
                        }
                        tmp_unit = tmp_unit.substr(0, tmp_unit.length-1);
                        dataStoreDokter(b.data.displayText, tmp_unit);
                        selectSetPilihanProfesi=b.data.displayText;
                        // Combo_SelectRWJProfesi(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanRWJJenisProfesi;
};

//RWJPasienPerKelompok
function mComboRWJPasienPerKelompokSEMUA()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganRWJ = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 160,
                id:'IDmComboRWJPasienPerKelompokSEMUA',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                valueField: 'Id',
                displayField: 'displayText',
                hidden:false,
                fieldLabel: '',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                        {
                            id: 0,
                            fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                            data: [[1, 'Semua']]
                        }
                ),
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectSetUmum=b.data.displayText ;
                    }
                                    
                                
                }
            }
    );
    return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokPERORANGAN()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganRWJ = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 160,
                id:'IDmComboRWJPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                          cmb_kd_customer_value=b.data.displayText ;
                        }
                }
            }
    );
    return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokPERUSAHAAN()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='1' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganRWJ = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 160,
                id:'IDmComboRWJPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                //value: selectsetperusahaan,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectsetperusahaan = b.data.KD_CUSTOMER;
                        selectsetnamaperusahaan = b.data.CUSTOMER;
                        cmb_kd_customer_value = b.data.CUSTOMER;
                    }
                }
            }
    );
    return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokASURANSI()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='2' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganRWJ = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 160,
                id:'IDmComboRWJPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectSetAsuransi=b.data.KD_CUSTOMER ;
                        selectsetnamaAsuransi = b.data.CUSTOMER ;
                        cmb_kd_customer_value = b.data.CUSTOMER ;
                    }
                }
            }
    );
    return cboPerseoranganRWJ;
};


function Combo_SelectRWJPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').show();
   }
};

function Combo_SelectRWJProfesi(combo)
{
   var value = combo;

   if(value === "Dokter")
   {    
        Ext.getCmp('cboDokterRWJPelayananPerawat').show();
        Ext.getCmp('cboDokterRWJPelayananDokter').hide();
   }
   else if(value === "Perawat")
   {    
        Ext.getCmp('cboDokterRWJPelayananDokter').show();
        Ext.getCmp('cboDokterRWJPelayananPerawat').hide();
        
   }
   else
   {
        Ext.getCmp('cboDokterRWJPelayananPerawat').show();
        Ext.getCmp('cboDokterRWJPelayananDokter').hide();
   }
};


function GetCriteriaRWJPasienPerKelompok()
{
    var strKriteria = '';
    
    if (Ext.getCmp('cboPilihanRWJPasienPerKelompok').getValue() !== '')
    {
        if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').getValue(); } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').getValue(); } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').getValue(); }
    }else{
            strKriteria = 'Semua';
    }

    
    return strKriteria;
};

function GetCriteriaRWJProfesi()
{
    var strKriteria = '';
    
    if (Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue() !== '')
    {
        if (Ext.get('IDcboPilihanRWJJenisProfesi').getValue() === 'Semua') {
            strKriteria = 'SEMUA'; 
        } else if (Ext.get('IDcboPilihanRWJJenisProfesi').getValue() === 'Dokter'){ 
            strKriteria = Ext.getCmp('cboDokterRWJPelayananDokter').getValue(); 
        } else if (Ext.get('IDcboPilihanRWJJenisProfesi').getValue() === 'Perawat'){ 
            strKriteria = Ext.getCmp('cboDokterRWJPelayananDokter').getValue(); 
        } 
    }else{
        strKriteria = 'SEMUA';
    }

    
    return strKriteria;
};

function ValidasiReportRWJPasienPerDokter()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJPasienPerDokter').dom.value === '')
    {
        ShowPesanWarningRWJPelayananDokterReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
        x=0;
    }
    return x;
};
