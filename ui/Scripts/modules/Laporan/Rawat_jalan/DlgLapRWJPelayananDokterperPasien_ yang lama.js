var type_file=0;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsPelayananDokter;
var selectNamaPelayananDokter;
var now = new Date();
var selectSetPerseorangan;
var frmDlgPelayananDokter;
var varLapPelayananDokter= ShowFormLapPelayananDokter();
var selectSetUmum;
var selectSetkelpas;



var selectSetPilihankelompokPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapPelayananDokter()
{
    frmDlgPelayananDokter= fnDlgPelayananDokter();
    frmDlgPelayananDokter.show();
};

function fnDlgPelayananDokter()
{
    var winPelayananDokterReport = new Ext.Window
    ( 
        {
            id: 'winPelayananDokterReport',
            title: 'Laporan Transaksi Jasa Pelayanan Dokter Per Pasien',
            closeAction: 'destroy',
            width:400,
            height: 360,
            border: false,
            resizable:true,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgPelayananDokter()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganRWJ').show();
                Ext.getCmp('cboAsuransiRWJ').hide();
                Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
                Ext.getCmp('cboUmumRWJ').hide();
            }
        }

        }
    );

    return winPelayananDokterReport;
};


function ItemDlgPelayananDokter()
{
    var PnlLapPelayananDokter = new Ext.Panel
    (
        {
            id: 'PnlLapPelayananDokter',
            fileUpload: true,
            layout: 'form',
			//width:400,
            height: '530',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapPelayananDokter_Atas(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapPelayananDokter',
                            handler: function()
                            {
                               /* if (ValidasiReportPelayananDokter() === 1)
                               // { */		
						   //loadMask.show();
                                        // var criteria = GetCriteriaPelayananDokter();
                                        // frmDlgPelayananDokter.close();
                                        // loadlaporanRWJ('0', 'rep010211', criteria, function(){
											// loadMask.hide();
										// });
										
										var criteria = GetCriteriaPelayananDokter();
                                        var params ={
														criteria 	: criteria,
														type_file 	: type_file,
														order_by 	: Ext.getCmp('cboOrderBy').getValue(),
												} ;
										console.log(params);
										var form = document.createElement("form");
										form.setAttribute("method", "post");
										form.setAttribute("target", "_blank");
										form.setAttribute("action", baseURL + "index.php/main/cetaklaporanRWJ/rep010211_");
										// form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_jasa_pelayanan_dokter_perpasien/doprint");
										var hiddenField = document.createElement("input");
										hiddenField.setAttribute("type", "hidden");
										hiddenField.setAttribute("name", "data");
										hiddenField.setAttribute("value", Ext.encode(params));
										form.appendChild(hiddenField);
										document.body.appendChild(form);
										form.submit();	
										//frmDlgPelayananDokter.close();
								/* }; */
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapPelayananDokter',
                            handler: function()
                            {
                                    frmDlgPelayananDokter.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapPelayananDokter;
};

function getItemLapPelayananDokter_Atas()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 270,
            anchor: '100% 100%',
            items: [
			{
				xtype: 'checkboxgroup',
				width:210,
				x: 120,
				y: 10,
				items: 
				[
					{
						
						boxLabel: 'Pendaftaran',
						name: 'cbPendaftaran_PelayananDokter',
						id : 'cbPendaftaran_PelayananDokter'
					},
					{
						boxLabel: 'Tindak RWJ',
						name: 'cbTindakRWJ_PelayananDokter',
						id : 'cbTindakRWJ_PelayananDokter'
					}
			   ]
			},
			{
				x: 10,
				y: 40,
				xtype: 'label',
				text: 'Poliklinik '
			}, {
				x: 110,
				y: 40,
				xtype: 'label',
				text: ' : '
			},
				mCombounitPelayananDokter(),
			{
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Dokter '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
				mComboRWJJenisProfesi(),
				mComboDokterPelayananDokter(),
				mComboDokterRWJPelayananPerawat(),
			{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, {
				x: 110,
				y: 130,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAwalLapPelayananDokter',
				format: 'd/M/Y',
                //value: tigaharilalu
				value: now
			}, {
				x: 230,
				y: 130,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 260,
				y: 130,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapPelayananDokter',
				format: 'd/M/Y',
				value: now,
				width: 100
			},{
				x: 10,
				y: 160,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 160,
				xtype: 'label',
				text: ' : '
			},
				mComboKelompokPasienPelayananDokter(),
				mComboUmumPelayananDokter(),
				mComboPerseoranganPelayananDokter(),
				mComboAsuransiPelayananDokter(),
				mComboPerusahaanPelayananDokter(),
			{
				x: 10,
				y: 220,
				xtype: 'label',
				text: 'Order By '
			}, {
				x: 110,
				y: 220,
				xtype: 'label',
				text: ' : '
			},
				mComboOrder(),
			{
				x: 10,
				y: 250,
				xtype: 'label',
				text: 'Type File '
			}, {
				x: 110,
				y: 250,
				xtype: 'label',
				text: ' : '
			},
			{
				x: 120,
				y: 250,
			   xtype: 'checkbox',
			   id: 'CekLapPilihTypeExcel',
			   hideLabel:false,
			   boxLabel: 'Excel',
			   checked: false,
			   listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
						{
							type_file=1;
						}
						else
						{
							type_file=0;
						}
					}
			   }
			}
            ]
        }]
    };
    return items;
};

function GetCriteriaPelayananDokter()
{
	var strKriteria = '';
	
	if (Ext.getCmp('cbPendaftaran_PelayananDokter').getValue() === true && Ext.getCmp('cbTindakRWJ_PelayananDokter').getValue() === false){
		strKriteria ='autocas';
		strKriteria += '##@@##' + 1;
	}else if(Ext.getCmp('cbPendaftaran_PelayananDokter').getValue() === false && Ext.getCmp('cbTindakRWJ_PelayananDokter').getValue() === true) {
		strKriteria ='autocas';
		strKriteria += '##@@##' + 2;
	} else if(Ext.getCmp('cbPendaftaran_PelayananDokter').getValue() === true && Ext.getCmp('cbTindakRWJ_PelayananDokter').getValue() === true){
		strKriteria ='NoCheked';
		strKriteria += '##@@##' + 3;
	} else{
		strKriteria ='NoCheked';
		strKriteria += '##@@##' + 4;
	}
		
	if(Ext.getCmp('cbounitRequestEntryPelayananDokter').getValue() === '' || Ext.getCmp('cbounitRequestEntryPelayananDokter').getValue() === 'Semua'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + 'Semua';
	} else{
		strKriteria += '##@@##' + Ext.get('cbounitRequestEntryPelayananDokter').getValue();;
		strKriteria += '##@@##' + Ext.getCmp('cbounitRequestEntryPelayananDokter').getValue();
	}
	
	if(Ext.getCmp('cboDokterPelayananDokter').getValue() === '' || Ext.getCmp('cboDokterPelayananDokter').getValue() === 'Semua'){
		strKriteria += '##@@##' + 'Dokter';
		strKriteria += '##@@##' + 'Semua';
	} else{
		strKriteria += '##@@##' + Ext.get('cboDokterPelayananDokter').getValue();;
		strKriteria += '##@@##' + Ext.getCmp('cboDokterPelayananDokter').getValue();
	}
	
	if (Ext.get('dtpTglAwalLapPelayananDokter').getValue() !== ''){
		strKriteria += '##@@##' + Ext.get('dtpTglAwalLapPelayananDokter').getValue();
	}
	if (Ext.get('dtpTglAkhirLapPelayananDokter').getValue() !== ''){
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapPelayananDokter').getValue();
	}
	
	if (Ext.getCmp('cboPilihanPelayananDokterkelompokPasien').getValue() !== '')
	{
		if (Ext.get('cboPilihanPelayananDokterkelompokPasien').getValue() === 'Semua') {
            strKriteria += '##@@##' + 'Kelpas';
			strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
        } else if (Ext.get('cboPilihanPelayananDokterkelompokPasien').getValue() === 'Perseorangan'){

			strKriteria += '##@@##' + 'Perseorangan';
            strKriteria += '##@@##' + Ext.get('cboPerseoranganRWJ').getValue();
            strKriteria += '##@@##' + Ext.getCmp('cboPerseoranganRWJ').getValue();
        } else if (Ext.get('cboPilihanPelayananDokterkelompokPasien').getValue() === 'Perusahaan'){
            
			if(Ext.getCmp('cboPerusahaanRequestEntryRWJ').getValue() === '')
            {
				strKriteria += '##@@##' + 'Kelpas';
				strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Perusahaan';
				strKriteria += '##@@##' + Ext.get('cboPerusahaanRequestEntryRWJ').getValue();
                strKriteria += '##@@##' + Ext.getCmp('cboPerusahaanRequestEntryRWJ').getValue();
            }
        } else {
            
			if(Ext.getCmp('cboAsuransiRWJ').getValue() === '')
            {
				strKriteria += '##@@##' + 'Kelpas';
				strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Asuransi';
				strKriteria += '##@@##' + Ext.get('cboAsuransiRWJ').getValue();
                strKriteria += '##@@##' + Ext.getCmp('cboAsuransiRWJ').getValue();
            }
            
        }
	}  else{
            strKriteria += '##@@##' + 'Kelpas';
			strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
	}

	
	return strKriteria;
};

function ValidasiReportPelayananDokter()
{
    var x=1;
    if(Ext.getCmp('dtpTglAwalLapPelayananDokter').getValue() > Ext.getCmp('dtpTglAkhirLapPelayananDokter').getValue())
    {
        ShowPesanWarningPelayananDokterReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }
	if(Ext.getCmp('cboPilihanPelayananDokterkelompokPasien').getValue() === ''){
		ShowPesanWarningPelayananDokterReport('Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	if(Ext.getCmp('cboPerusahaanRequestEntryRWJ').getValue() === '' &&  Ext.getCmp('cboAsuransiRWJ').getValue() === '' &&  Ext.getCmp('cboPerseoranganRWJ').getValue() === ''){
		ShowPesanWarningPelayananDokterReport('Sub Kelompok Pasien Belum Dipilih','Laporan Regis Laboratorium');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningPelayananDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function mComboDokterPelayananDokter()
{
	var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                //param: "WHERE left(dokter_klinik.kd_unit, 1)='3' AND dokter.jenis_dokter='0'"
				param: "WHERE dokter.jenis_dokter='1'"
                //param: "WHERE left(dokter_klinik.kd_unit, 1)='3' AND dokter.jenis_dokter='0'"
			}
		}
	);
    var cboPilihanPelayananDokter = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboDokterPelayananDokter',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanPelayananDokter;
};

function mComboDokterRWJPelayananPerawat()
{
	var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokterLaporan',
				param: "WHERE left(dokter_klinik.kd_unit, 1)='3' AND dokter.jenis_dokter='0'"
				//param: "WHERE dokter.jenis_dokter='1'"
			}
		}
	);
    var cboPilihanRWJPelayananDokter = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboDokterRWJPelayananPerawat',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:true,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanRWJPelayananDokter;
};

function mComboRWJJenisProfesi()
{
    var cboPilihanRWJJenisProfesi = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'IDcboPilihanRWJJenisProfesi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Dokter'], [2, 'Perawat']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihanProfesi=b.data.displayText;
                            Combo_SelectRWJProfesi(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanRWJJenisProfesi;
};

function mComboKelompokPasienPelayananDokter()
{
    var cboPilihanPelayananDokterkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 160,
                id:'cboPilihanPelayananDokterkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanPelayananDokterkelompokPasien;
};

function mComboPerseoranganPelayananDokter()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 190,
                id:'cboPerseoranganRWJ',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRWJ;
};

function mComboOrder()
{
    var cboOrderBy = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 220,
			id:'cboOrderBy',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Order  ',
			width:240,
			value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				    data: [[0, 'Medrec'],[1, 'Nama Pasien']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Medrec',
		}
	);
	return cboOrderBy;
};

function mComboUmumPelayananDokter()
{
    var cboUmumRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 190,
			id:'cboUmumRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			valueField: 'Id',
            displayField: 'displayText',
            value:1,
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
									'Id',
									'displayText'
							],
					data: [[1, 'Umum']]
					}
			),
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumRWJ;
};

function mComboPerusahaanPelayananDokter()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='1' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerusahaanRequestEntryRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 190,
		    id: 'cboPerusahaanRequestEntryRWJ',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			//value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRWJ;
};

function mComboAsuransiPelayananDokter()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='2' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboAsuransiRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 190,
			id:'cboAsuransiRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			//value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiRWJ;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRWJ').show();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').show();
        Ext.getCmp('cboUmumRWJ').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').show();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').hide();
    }
   else if(value === "Semua")
   {
		Ext.getCmp('cboPerseoranganRWJ').show();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').hide();
        /* Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').show(); */
   }
   else
   {
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').show();
   }
}

function mCombounitPelayananDokter()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewCombounit_Konfigurasi',
                    param: " "
                }
            }
        );

    var cbounitRequestEntryPelayananDokter = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 40,
            id: 'cbounitRequestEntryPelayananDokter',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            width: 240,
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryPelayananDokter;
};


function Combo_SelectRWJProfesi(combo)
{
   var value = combo;
	console.log(value);
   if(value === "Dokter")
   {    
        Ext.getCmp('cboDokterPelayananDokter').show();
        Ext.getCmp('cboDokterRWJPelayananPerawat').hide();
   }
   else if(value === "Perawat")
   { 
        Ext.getCmp('cboDokterRWJPelayananPerawat').show();
        Ext.getCmp('cboDokterPelayananDokter').hide();
        
   }
   else
   {
        Ext.getCmp('cboDokterPelayananDokter').show();
        Ext.getCmp('cboDokterRWJPelayananPerawat').hide();
   }
};