var type_file=0;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRekapJasaDokter;
var selectNamaRekapJasaDokter;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRekapJasaDokter;
var varLapRekapJasaDokter= ShowFormLapRekapJasaDokter();
var selectSetUmum;
var selectSetkelpas;
var firstGrid;
var secondGrid;
var secondGridStore;
var dataSource_unitLapRekapJasaDokter;
var DataStore_combouserLapRekapJasaDokter;
var combouserLapRekapJasaDokter;
var jenis_profesi='Dokter';
var selectSetPilihankelompokPasien;
var selectSetPilihanProfesi;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;
var jenis_cust='Semua';
var nama_jenis_cust='Semua';
var type_file=0;
function ShowFormLapRekapJasaDokter()
{
    frmDlgRekapJasaDokter= fnDlgRekapJasaDokter();
    frmDlgRekapJasaDokter.show();
};
function fnDlgRekapJasaDokter()
{
    var winRekapJasaDokterReport = new Ext.Window
    (
        {
            id: 'winRekapJasaDokterReport',
            title: 'Laporan Rekap Jasa Pelayanan Dokter',
            closeAction: 'destroy',
            width:400,
            height: 580,
            border: false,
            resizable:false,
            plain: true,
            // constrain: true,
            layout: 'form',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRekapJasaDokter()],
            listeners:
			{
				activate: function()
				{
					// Ext.getCmp('cboPerseoranganRWJ').show();
					// Ext.getCmp('cboAsuransiRWJ').hide();
					// Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
					// Ext.getCmp('cboUmumRWJ').hide();
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapRekapJasaDokter',
					handler: function()
					{
						//if (ValidasiReportRWJPasienPerDokter() === 1)
						//{	
							var kd_dokter='';
							if(jenis_profesi == 'Dokter'){
								kd_dokter =   Ext.getCmp('cboPilihanRekapJasaDokter').getValue();
							}
							else if(jenis_profesi == 'Perawat'){
								kd_dokter =    Ext.getCmp('cboDokterRWJPelayananPerawat').getValue();
							}
							else if(jenis_profesi == 'Semua'){
								kd_dokter =    'Semua';
							}
							
							var tgl_awal;
							var tgl_akhir;
							var radio_asal;
							var radio_tgl;
							if(Ext.getCmp('radioasal').getValue() ==true){
								tgl_awal=Ext.get('dtpTglAwalFilterHasilLab').dom.value;
								tgl_akhir=Ext.get('dtpTglAkhirFilterHasilLab').dom.value;
								radio_tgl=1;
							}
							if(Ext.getCmp('radioasalBulan').getValue() ==true){
								tgl_awal=Ext.get('dtpBulanAwalFilterHasilLab').dom.value;
								tgl_akhir=Ext.get('dtpBulanAkhirFilterHasilLab').dom.value;
								radio_tgl=2;
							}
							
							var sendDataArrayUnit = [];
							secondGridStore.each(function(record){
								var recordArray = [record.get("KD_UNIT")];
								sendDataArrayUnit.push(recordArray);
							});
							var params={
								radio_tgl:radio_tgl,
								jenis_profesi:jenis_profesi,
								kd_dokter:kd_dokter,
								pendaftaran:Ext.getCmp('cbPendaftaran_RekapJasaDokter').getValue(),
								tindakan_rwj:Ext.getCmp('cbTindakRWJ_RekapJasaDokter').getValue(),
								kel_pas:Ext.getCmp('cboPilihanRWJPasienPerKelompok').getValue(),
								nama_kel_pas:Ext.get('cboPilihanRWJPasienPerKelompok').dom.value,
								jenis_cust:jenis_cust,
								nama_jenis_cust:nama_jenis_cust,
								operator:Ext.getCmp('combouserLapRekapJasaDokter').getValue(),
								nama_operator:Ext.get('combouserLapRekapJasaDokter').dom.value,
								tgl_awal:tgl_awal,
								tgl_akhir:tgl_akhir,
								type_file:type_file
							} 
							params['kd_unit'] 	= sendDataArrayUnit;
							//console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/function_laporan_per_poli_per_produk/cetak_lap_rekap_jasa_pel_dokter");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJPasienPerKelompok.close(); 
							
					   //};
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRekapJasaDokter',
					handler: function()
					{
							frmDlgRekapJasaDokter.close();
					}
				}
			
			]

        }
    );

    return winRekapJasaDokterReport;
};


function ItemDlgRekapJasaDokter()
{
    var PnlLapRekapJasaDokter = new Ext.Panel
    (
        {
            id: 'PnlLapRekapJasaDokter',
            fileUpload: true,
            layout: 'form',
			//width:400,
            height: '590',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				gridPoliklinikLapRekapJasaDokter(),
                getItemLapRekapJasaDokter_Atas(),
				getItemLapKriteriaTgl()
                /* {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        
                    ]
                } */
            ]
        }
    );

    return PnlLapRekapJasaDokter;
};

function gridPoliklinikLapRekapJasaDokter(){
	var Field_poli_viDaftarLapRekapJasaDokter = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unitLapRekapJasaDokter = new WebApp.DataStore({fields: Field_poli_viDaftarLapRekapJasaDokter});
    
    //datarefresh_viInformasiUnitLapRekapJasaDokter()

   // Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
	];
	
	Ext.Ajax.request({
			url: baseURL + "index.php/rawat_jalan/function_laporan_PTP/getUnitGridJasaDokter",
			params: {kd_bagian:'2'},
			failure: function(o){
				var cst = Ext.decode(o.responseText);
			},	    
			success: function(o) {
				 firstGrid.store.removeAll();
				var cst = Ext.decode(o.responseText);

				for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
					var recs    = [],recType =  dataSource_unitLapRekapJasaDokter.recordType;
					var o=cst['listData'][i];
			
					recs.push(new recType(o));
					dataSource_unitLapRekapJasaDokter.add(recs);
					console.log(o);
				}
			}
		});
	// declare the source Grid
		firstGrid = new Ext.grid.GridPanel({
		ddGroup          : 'secondGridDDGroup',
		store            : dataSource_unitLapRekapJasaDokter,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		enableDragDrop   : true,
		height           : 150,
		stripeRows       : true,
		trackMouseOver   : true,
		title            : 'Unit',
		anchor           : '100% 100%',
		plugins          : [new Ext.ux.grid.FilterRow()],
		colModel         : new Ext.grid.ColumnModel
		(
			[
					new Ext.grid.RowNumberer(),
					{
							id: 'colNRM_viDaftar',
							header: 'No.Medrec',
							dataIndex: 'KD_UNIT',
							sortable: true,
							hidden : true
					},
					{
							id: 'colNMPASIEN_viDaftar',
							header: 'Nama',
							dataIndex: 'NAMA_UNIT',
							sortable: true,
							width: 50
					}
			]
		),
		listeners : {
			afterrender : function(comp) {
				var firstGridDropTargetEl =  firstGrid.getView().scroller.dom;
				var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
						ddGroup    : 'firstGridDDGroup',
						notifyDrop : function(ddSource, e, data){
								var records =  ddSource.dragData.selections;
								Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
								firstGrid.store.add(records);
								firstGrid.store.sort('KD_UNIT', 'ASC');
								return true
						}
				});
			}
		},
		viewConfig: 
			{
					forceFit: true
			}
	});

	secondGridStore = new Ext.data.JsonStore({
		fields : fields,
		root   : 'records'
	});

	// create the destination Grid
	secondGrid = new Ext.grid.GridPanel({
			ddGroup          : 'firstGridDDGroup',
			store            : secondGridStore,
			columns          : cols,
			enableDragDrop   : true,
			height           : 150,
			stripeRows       : true,
			autoExpandColumn : 'KD_UNIT',
			title            : 'Unit',
			listeners : {
			afterrender : function(comp) {
			var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
			var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'secondGridDDGroup',
					notifyDrop : function(ddSource, e, data){
							var records =  ddSource.dragData.selections;
							Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
							secondGrid.store.add(records);
							secondGrid.store.sort('KD_UNIT', 'ASC');
							return true
					}
			});
			}
		},
		viewConfig: 
			{
					forceFit: true
			}
	});
	
	var FrmTabs_viInformasiUnitdokter = new Ext.Panel
	({
		    
		    closable: true,
		    region: 'center',
		    layout: 'column',
			height: 170,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: true,
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[firstGrid
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid
						
					]
				},
				
				
			],

    })
    return FrmTabs_viInformasiUnitdokter;
}

function getItemLapRekapJasaDokter_Atas()
{
    var items = {
        layout: 'column',
        border: false,
		bodyStyle: 'margin: 5px 0px 5px 0px',
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  370,
            height: 223,
            anchor: '100% 100%',
            items: [
			{
				x: 10,
				y: 5,
				xtype: 'checkbox',
				id: 'CekLapPilihSemuaRekapJasaDokter',
				hideLabel:false,
				boxLabel: 'Pilih Semua Unit',
				checked: false,
				listeners: 
			   {
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihSemuaRekapJasaDokter').getValue()===true)
						{
							 firstGrid.getSelectionModel().selectAll();
						}
						else
						{
							firstGrid.getSelectionModel().clearSelections();
						}
					}
			   }
			},
			{
				x: 196,
				y: 5,
				xtype: 'checkbox',
				id: 'CekLapResetRekapJasaDokter',
				hideLabel:false,
				boxLabel: 'Reset Unit',
				checked: false,
				listeners: 
			   {
					check: function()
					{
						if(Ext.getCmp('CekLapResetRekapJasaDokter').getValue()===true)
						{
							secondGridStore.removeAll();
							datarefresh_viInformasiUnitLapRekapJasaDokter();
							Ext.getCmp('CekLapPilihSemuaRekapJasaDokter').setValue(false);
						} else{
							
						}
					}
			   }
			},
			{
				xtype: 'checkboxgroup',
				width:210,
				x: 120,
				y: 30,
				items: 
				[
					{
						
						boxLabel: 'Pendaftaran',
						name: 'cbPendaftaran_RekapJasaDokter',
						id : 'cbPendaftaran_RekapJasaDokter'
					},
					{
						boxLabel: 'Tindak RWJ',
						name: 'cbTindakRWJ_RekapJasaDokter',
						id : 'cbTindakRWJ_RekapJasaDokter'
					}
			   ]
			},
           {
				x: 10,
				y: 55,
				xtype: 'label',
				text: 'Jenis '
			}, {
				x: 110,
				y: 55,
				xtype: 'label',
				text: ' : '
			},
                mComboRWJJenisProfesi(),
			{
				x: 10,
				y: 85,
				xtype: 'label',
				text:'Dokter'
			}, {
				x: 110,
				y: 85,
				xtype: 'label',
				text: ' : '
			},	
				mComboDokterRekapJasaDokter(),
                mComboDokterRWJPelayananPerawat(),
			{
				x: 10,
				y: 115,
				xtype: 'label',
				text: 'Operator '
			}, {
				x: 110,
				y: 115,
				xtype: 'label',
				text: ' : '
			},
			ComboOperatorLapRekapJasaDokter(),
			
			{
				x: 10,
				y: 145,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 145,
				xtype: 'label',
				text: ' : '
			},
				mComboRWJPasienPerKelompok(),
				mComboRWJPasienPerKelompokSEMUA(),
				mComboRWJPasienPerKelompokPERORANGAN(),
				mComboRWJPasienPerKelompokPERUSAHAAN(),
				mComboRWJPasienPerKelompokASURANSI(),
			
			{
				x: 10,
				y: 205,
				xtype: 'label',
				text: 'Type File '
			}, {
				x: 110,
				y: 205,
				xtype: 'label',
				text: ' : '
			},
			{
				x: 120,
				y: 205,
				xtype: 'checkbox',
				id: 'CekLapPilihTypeExcel',
				hideLabel:false,
				boxLabel: 'Excel',
				checked: false,
				listeners: 
				{
					check: function()
					{
					   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
						{
							type_file=1;
						}
						else
						{
							type_file=0;
						}
					}
			   }
			}
            ]
        },
		]
    };
    return items;
};

function ShowPesanWarningRekapJasaDokterReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function datarefresh_viInformasiUnitLapRekapJasaDokter()
{
    dataSource_unitLapRekapJasaDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnitB',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )
    //alert("refersh")
}

function ComboOperatorLapRekapJasaDokter(){
		var Field = ['kd_user','full_name'];
    	DataStore_combouserLapRekapJasaDokter = new WebApp.DataStore({fields: Field});
		loaduserLapRekapJasaDokter();
    	var combouserLapRekapJasaDokter = new Ext.form.ComboBox({
			id:'combouserLapRekapJasaDokter',
			typeAhead: true,
			x: 120,
			y: 115,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: DataStore_combouserLapRekapJasaDokter,
			valueField: 'kd_user',
			displayField: 'full_name',
			value:'Semua',
			listeners:{
				'select': function(a,b,c){
				}
			}
		});
		return combouserLapRekapJasaDokter;
}

function loaduserLapRekapJasaDokter(){
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/function_laporan_per_poli_per_produk/getUser",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			DataStore_combouserLapRekapJasaDokter.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  DataStore_combouserLapRekapJasaDokter.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				DataStore_combouserLapRekapJasaDokter.add(recs);
			}
		}
	});
}


function mComboDokterRekapJasaDokter()
{
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterDokter = new WebApp.DataStore({fields: Field});
	dsDokterDokter.removeAll();
    dsDokterDokter.load
    (
        {
            params:
            {
				Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokterLaporan',
				param: "WHERE dokter.jenis_dokter='1'"
               
            }
        }
    );
    var cboPilihanRekapJasaDokter = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 85,
                id:'cboPilihanRekapJasaDokter',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanRekapJasaDokter;
};

function mComboDokterRWJPelayananPerawat()
{
	var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
	dsDokterPelayaranDokter.removeAll();
    dsDokterPelayaranDokter.load
	(
		{
		    params:
			{
			    Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: "WHERE left(dokter_klinik.kd_unit, 1)='2' AND dokter.jenis_dokter='0'"
			}
		}
	);
    var cboDokterRWJPelayananPerawat = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 85,
                id:'cboDokterRWJPelayananPerawat',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                hidden:true,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
	);
	return cboDokterRWJPelayananPerawat;
};


function mCombounitRekapJasaDokter()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewCombounit_Konfigurasi',
                    param: " "
                }
            }
        );

    var cbounitRequestEntryRekapJasaDokter = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 70,
            id: 'cbounitRequestEntryRekapJasaDokter',
			width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryRekapJasaDokter;
};


function mComboRWJPasienPerKelompok()
{
    var cboPilihanRWJPasienPerKelompok = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 145,
                id:'cboPilihanRWJPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:'Semua',
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[-1, 'Semua'], [0, 'Perseorangan'],[1, 'Perusahaan'], [2, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectRWJPasienPerKelompok(b.data.displayText);
							
					}
                }
            }
    );
    return cboPilihanRWJPasienPerKelompok;
};

function mComboRWJJenisProfesi()
{
    var cboPilihanRWJJenisProfesi = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 55,
                id:'IDcboPilihanRWJJenisProfesi',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:'Dokter',
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Dokter'], [2, 'Perawat']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihanProfesi=b.data.displayText;
                            Combo_SelectRWJProfesi(b.data.displayText);
							jenis_profesi=b.data.displayText;
                    }
                }
            }
	);
	return cboPilihanRWJJenisProfesi;
};

//RWJPasienPerKelompok
function mComboRWJPasienPerKelompokSEMUA()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 175,
                id:'IDmComboRWJPasienPerKelompokSEMUA',
                typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				selectOnFocus:true,
				forceSelection: true,
				emptyText:'Silahkan Pilih...',
				valueField: 'Id',
	            displayField: 'displayText',
	            hidden:false,
				fieldLabel: '',
				width: 240,
				value:1,
				store: new Ext.data.ArrayStore
				(
						{
							id: 0,
							fields:
								[
										'Id',
										'displayText'
								],
							data: [[1, 'Semua']]
						}
				),
				listeners:
				{
					'select': function(a,b,c)
					{
						selectSetUmum=b.data.displayText ;
					}
	                                
	                            
				}
            }
	);
	return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokPERORANGAN()
{
	var Field = ['kd_customer','customer'];
    dsPerseoranganRequestEntry1 = new WebApp.DataStore({fields: Field});
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/rawat_jalan/function_laporan_per_poli_per_produk/getCustomer",
			params: {kelpas:'0'},
			failure: function(o)
			{
				//ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				dsPerseoranganRequestEntry1.removeAll();
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsPerseoranganRequestEntry1.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsPerseoranganRequestEntry1.add(recs);
				}
			}
		}
		
	)
    var IDmComboRWJPasienPerKelompokPERORANGAN = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 175,
                id:'IDmComboRWJPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry1,
                valueField: 'kd_customer',
				displayField: 'customer',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
						  jenis_cust=b.data.kd_customer;
						  nama_jenis_cust=b.data.customer;
                        }
                }
            }
	);
	return IDmComboRWJPasienPerKelompokPERORANGAN;
};

function mComboRWJPasienPerKelompokPERUSAHAAN()
{
	var Field = ['kd_customer','customer'];
    dsPerseoranganRequestEntry2 = new WebApp.DataStore({fields: Field});
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/rawat_jalan/function_laporan_per_poli_per_produk/getCustomer",
			params: {kelpas:'1'},
			failure: function(o)
			{
				//ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				dsPerseoranganRequestEntry2.removeAll();
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsPerseoranganRequestEntry2.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsPerseoranganRequestEntry2.add(recs);
				}
			}
		}
		
	)
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 175,
                id:'IDmComboRWJPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry2,
                valueField: 'kd_customer',
				displayField: 'customer',
				//value: selectsetperusahaan,
                listeners:
                {
				    'select': function(a,b,c)
					{
				        selectsetperusahaan = b.data.kd_customer;
						selectsetnamaperusahaan = b.data.customer;
						jenis_cust=b.data.kd_customer;
						nama_jenis_cust=b.data.customer;
					}
                }
            }
	);
	return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokASURANSI()
{
	var Field = ['kd_customer','customer'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/rawat_jalan/function_laporan_per_poli_per_produk/getCustomer",
			params: {kelpas:'2'},
			failure: function(o)
			{
				//ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				dsPerseoranganRequestEntry.removeAll();
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsPerseoranganRequestEntry.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsPerseoranganRequestEntry.add(recs);
				}
			}
		}
		
	)
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
				x: 120,
				y: 175,
                id:'IDmComboRWJPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
		    	emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
				valueField: 'kd_customer',
				displayField: 'customer',
                listeners:
                {
				    'select': function(a,b,c)
					{
						selectSetAsuransi=b.data.kd_customer ;
						selectsetnamaAsuransi = b.data.customer ;
						jenis_cust=b.data.kd_customer;
						nama_jenis_cust=b.data.customer;
					}
                }
            }
	);
	return cboPerseoranganRWJ;
};


function Combo_SelectRWJPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
		
		Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
		Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
		Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').show();
   }
};

function Combo_SelectRWJProfesi(combo)
{
   var value = combo;

   if(value === "Dokter")
   {    
        Ext.getCmp('cboPilihanRekapJasaDokter').show();
        Ext.getCmp('cboDokterRWJPelayananPerawat').hide();
		
   }
   else if(value === "Perawat")
   {    
        Ext.getCmp('cboDokterRWJPelayananPerawat').show();
         Ext.getCmp('cboPilihanRekapJasaDokter').hide();
		
        
   }
   else
   {
        Ext.getCmp('cboPilihanRekapJasaDokter').show();
        Ext.getCmp('cboDokterRWJPelayananPerawat').hide();
		
		
   }
};


function GetCriteriaRWJPasienPerKelompok()
{
    var strKriteria = '';
    
    if (Ext.getCmp('cboPilihanRWJPasienPerKelompok').getValue() !== '')
    {
        if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').getValue(); } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').getValue(); } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').getValue(); }
    }else{
            strKriteria = 'Semua';
    }

    
    return strKriteria;
};

function GetCriteriaRWJProfesi()
{
	var strKriteria = '';
	
	if (Ext.getCmp('IDcboPilihanRWJJenisProfesi').getValue() !== '')
	{
		if (Ext.get('IDcboPilihanRWJJenisProfesi').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
		else if (Ext.get('IDcboPilihanRWJJenisProfesi').getValue() === 'Dokter'){ strKriteria = Ext.getCmp('cboDokterRekapJasaDokter').getValue(); } 
		else if (Ext.get('IDcboPilihanRWJJenisProfesi').getValue() === 'Perawat'){ strKriteria = Ext.getCmp('cboDokterRWJPelayananPerawat').getValue(); } 
	}else{
        strKriteria = 'Semua';
	}

	
	return strKriteria;
};

function ValidasiReportRWJPasienPerDokter()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJPasienPerDokter').dom.value === '')
	{
		ShowPesanWarningRWJPasienPerDokterReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
    return x;
};


function getItemLapKriteriaTgl()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            border: true,
            width:  370,
            height: 105,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 5,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }
                }
            }, {
                x: 30,
                y: 5,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 25,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilLab',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 160,
                y: 25,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 235,
                y: 25,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 55,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 55,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 75,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilLab',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 160,
                y: 75,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 235,
                y: 75,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilLab',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};