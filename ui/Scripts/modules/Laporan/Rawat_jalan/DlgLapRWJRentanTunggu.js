
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapRWJRentanTunggu;
var selectNamaLapRWJRentanTunggu;
var now = new Date();
var selectSetPerseorangan;
var frmLapRWJRentanTunggu;
var varLapLapRWJRentanTunggu= ShowFormLapLapRWJRentanTunggu();
var tglAwal;
var tglAkhir;
var tipe;
var winLapRWJRentanTungguReport;
var cboPoliklinik_LapRWJRentanTunggu;






function ShowFormLapLapRWJRentanTunggu()
{
    frmLapRWJRentanTunggu= fnLapRWJRentanTunggu();
    frmLapRWJRentanTunggu.show();
	loadDataComboUnitFar_LapRWJRentanTunggu();
};


function fnLapRWJRentanTunggu()
{
    winLapRWJRentanTungguReport = new Ext.Window
    (
        {
            id: 'winLapRWJRentanTungguReport',
            title: 'Laporan Rentan Tunggu',
            closeAction: 'destroy',
            width: 420,
            height: 160,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemLapRWJRentanTunggu()],
            listeners:
			{
				activate: function()
				{
					
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'Ok',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapRWJRentanTunggu',
						handler: function()
						{
							
								var params={
									kd_unit:Ext.getCmp('cboPoliklinik_LapRWJRentanTunggu').getValue(),
									tglAwal:Ext.getCmp('dtpTglAwalFilter_RWJRentanTunggu').getValue(),
									tglAkhir:Ext.getCmp('dtpTglAkhirFilter_RWJRentanTunggu').getValue(),
								} 
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionRWJ/cetakRWJRentanTunggu");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapRWJRentanTungguReport.close(); 
							
							
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapRWJRentanTunggu',
						handler: function()
						{
							winLapRWJRentanTungguReport.close();
						}
					}
			]

        }
    );

    return winLapRWJRentanTungguReport;
};


function ItemLapRWJRentanTunggu()
{
    var PnlLapLapRWJRentanTunggu = new Ext.Panel
    (
        {
            id: 'PnlLapLapRWJRentanTunggu',
            fileUpload: true,
            layout: 'form',
            height: '150',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapRWJRentanTunggu_Atas(),
            ]
        }
    );

    return PnlLapLapRWJRentanTunggu;
};

function ValidasiReportLapRWJRentanTunggu()
{
	var x=1;
	if(Ext.getCmp('cboPoliklinik_LapRWJRentanTunggu').getValue() === ''){
		ShowPesanWarningLapRWJRentanTungguReport('Unit Far tidak boleh kosong','Warning');
        x=0;
	}
    return x;
};

function ShowPesanWarningLapRWJRentanTungguReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapRWJRentanTunggu_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  395,
            height: 75,
            anchor: '100% 100%',
            items: [
				{
					x: 10,
					y: 10,
					xtype: 'label',
					text: 'Poli '
				}, {
					x: 110,
					y: 10,
					xtype: 'label',
					text: ' : '
				},
				mComboPoliklinikLapRWJRentanTunggu(),
				
				{
					x: 10,
					y: 40,
					xtype: 'label',
					text: 'Periode '
				}, 
				{
					x: 110,
					y: 40,
					xtype: 'label',
					text: ' : '
				},
				{
					x: 120,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAwalFilter_RWJRentanTunggu',
					format: 'd/M/Y',
					value: now
				}, 
				{
					x: 225,
					y: 40,
					xtype: 'label',
					text: ' s/d Bulan'
				}, 
				{
					x: 280,
					y: 40,
					xtype: 'datefield',
					id: 'dtpTglAkhirFilter_RWJRentanTunggu',
					format: 'd/M/Y',
					value: now,
					width: 100
				},
            ]
        }]
    };
    return items;
};


function getItemLapLapRWJRentanTunggu_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

var selectSetPilihan;

function loadDataComboUnitFar_LapRWJRentanTunggu(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/getPoliklinik",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboPoliklinik_LapRWJRentanTunggu.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_Poliklinik_LapRWJRentanTunggu.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_Poliklinik_LapRWJRentanTunggu.add(recs);
				console.log(o);
			}
		}
	});
}


function mComboPoliklinikLapRWJRentanTunggu()
{
	var Field = ['kd_unit','nama_unit'];
    ds_Poliklinik_LapRWJRentanTunggu = new WebApp.DataStore({fields: Field});
    cboPoliklinik_LapRWJRentanTunggu = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPoliklinik_LapRWJRentanTunggu',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'SEMUA',
                width:200,
                store: ds_Poliklinik_LapRWJRentanTunggu,
                valueField: 'kd_unit',
                displayField: 'nama_unit',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPoliklinik_LapRWJRentanTunggu;
};
