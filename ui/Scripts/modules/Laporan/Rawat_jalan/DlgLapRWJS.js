
var dsRWJS;
var selectRWJS;
var selectNamaRWJS;
var now = new Date();

var frmDlgRWJS;
var varLapRWJS= ShowFormLapRWJS();

function ShowFormLapRWJS()
{
    frmDlgRWJS= fnDlgRWJS();
//    frmDlgRWJS.show();
    frmDlgRWJS.close();
    //ShowReport('0', 1010202, '');
    reportchart();
    
};

function fnDlgRWJS()
{
    var winRWJSReport = new Ext.Window
    (
        {
            id: 'winRWJSReport',
            title: 'Laporan Rawat Jalan Sumary',
            closeAction: 'destroy',
            width:500,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJS()]

        }
    );

    return winRWJSReport;
};


function ItemDlgRWJS()
{
    var PnlLapRWJS = new Ext.Panel
    (
        {
            id: 'PnlLapRWJS',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJS_Tanggal(), getItemLapRWJS_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWJS',
                            handler: function()
                            {
                                if (ValidasiReportRWJS() === 1)
                                {
                                    //if (ValidasiTanggalReportRWJS() === 1)
                                    //{
                                        //var criteria = GetCriteriaRWJS();
                                        frmDlgRWJS.close();
                                        reportchart();
                                        //ShowReport('0', 1010202, '');
//                                        window.open("http://localhost/Simrs/base/tmp/1421630699Tmp.pdf");
                                    //};
                                    //alert(tmp);
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWJS',
                            handler: function()
                            {
                                    frmDlgRWJS.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWJS;
};

function GetCriteriaRWJS()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJS').dom.value != '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWJS').dom.value;
	};

	if (Ext.get('dtpTglAkhirLapRWJS').dom.value != '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRWJS').dom.value;
                strKriteria += '##@@##' + 'group by k.kd_unit, u.nama_unit';
	};

//	if (selectRWJS != undefined)
//	{
//		strKriteria += '##@@##' + selectRWJS;
//		strKriteria += '##@@##' + selectNamaRWJS ;
//	};

	return strKriteria;
};


function ValidasiReportRWJS()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWJS').dom.value === '') || (Ext.get('dtpTglAkhirLapRWJS').dom.value === '') || (selectRWJS === undefined) || (Ext.get('comboUnitLapRWJS').dom.value === ''))
    {
            if(Ext.get('dtpTglAwalLapRWJS').dom.value === '')
            {
                    ShowPesanWarningRWJSReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('dtpTglAkhirLapRWJS').dom.value === '')
            {
                    ShowPesanWarningRWJSReport(nmGetValidasiKosong(nmFinishDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
//            else if(Ext.get('comboUnitLapRWJS').dom.value === '' )
//            {
//                    ShowPesanWarningRWJSReport(nmGetValidasiKosong(nmDeptDlgRpt),nmTitleFormDlgReqCMRpt);
//                    x=0;
//            };
    };

    return x;
};

function ValidasiTanggalReportRWJS()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJS').dom.value > Ext.get('dtpTglAkhirLapRWJS').dom.value)
    {
        ShowPesanWarningRWJSReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJSReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJS_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:1,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
//                    mComboUnitLapRWJS()
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJS_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: 0.45,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 85,
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmPeriodeDlgRpt + ' ',
                    id: 'dtpTglAwalLapRWJS',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        },
        {
            columnWidth: 0.48,
            layout: 'form',
            border: false,
                    labelWidth: 25,
            labelAlign: 'right',
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmSd + ' ',
                    id: 'dtpTglAkhirLapRWJS',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        }
        ]
    }
    return items;
};


function mComboUnitLapRWJS()
{
    var dataSource_All = new Ext.data.Store({
	reader: new Ext.data.JsonReader
		({
			//fields: ['code',  'NoFaktur',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			fields: ['KD_UNIT', 'NAMA_UNIT']
		}),
	data:
		[
			{ KD_UNIT:'xxx', NAMA_UNIT:'Semua Unit'},
                        { KD_UNIT:'208', NAMA_UNIT:'Anak'},
                        { KD_UNIT:'209', NAMA_UNIT:'Bedah'},
                        { KD_UNIT:'215', NAMA_UNIT:'DM'},
                        { KD_UNIT:'202', NAMA_UNIT:'Gigi'},
                        { KD_UNIT:'203', NAMA_UNIT:'Gizi'},
                        { KD_UNIT:'206', NAMA_UNIT:'Interna'},
                        { KD_UNIT:'216', NAMA_UNIT:'Jantung'},
                        { KD_UNIT:'214', NAMA_UNIT:'Jiwa'},
                        { KD_UNIT:'207', NAMA_UNIT:'Kandungan'},
                        { KD_UNIT:'213', NAMA_UNIT:'KIA'},
                        { KD_UNIT:'212', NAMA_UNIT:'Kulit Kelamin'},
                        { KD_UNIT:'211', NAMA_UNIT:'Mata'},
                        { KD_UNIT:'204', NAMA_UNIT:'Syaraf'},
                        { KD_UNIT:'210', NAMA_UNIT:'THT'},
                        { KD_UNIT:'201', NAMA_UNIT:'Umum'},
                        { KD_UNIT:'205', NAMA_UNIT:'VCT'},
		]
	});
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    dsRWJS = new WebApp.DataStore({ fields: Field });

    dsRWJS.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    );
    
  var comboUnitLapRWJS = new Ext.form.ComboBox
    (
        {
            id:'comboUnitLapRWJS',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Poliklinik ',
            align:'Right',
            anchor:'99%',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store:dataSource_All,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectRWJS=b.data.KD_UNIT ;
                    selectNamaRWJS=b.data.NAMA_UNIT ;
                }
            }
        }
    );

//    dsRWJS.load
//    (
//        {
//            params:
//            {
//                Skip: 0,
//                Take: 1000,
//                //Sort: 'DEPT_ID',
//                Sort: 'NAMA_UNIT',
//                Sortdir: 'ASC',
//                target:'ViewSetupUnit',
//                param: ''
//            }
//        }
//    );
    return comboUnitLapRWJS;
};

function generateData(){
    var data = [];
    for(var i = 0; i < 12; ++i){
        data.push([Date.monthNames[i], (Math.floor(Math.random() *  11) + 1) * 100]);
    }
    return data;
}


function reportchart()
{
    var store = new Ext.data.ArrayStore({
        fields: ['month', 'hits'],
        data: generateData()
    });

    new Ext.Panel({
        width: 700,
        height: 400,
        //renderTo: document.body,
        title: 'Column Chart with Reload - Hits per Month',
        tbar: [{
            text: 'Load new data set',
            handler: function(){
                store.loadData(generateData());
            }
        }],
        items: {
            xtype: 'columnchart',
            store: store,
            yField: 'hits',
	    url: 'C:/xampp/htdocs/belajarlaporan/image/charts.swf',
            xField: 'month',
            xAxis: new Ext.chart.CategoryAxis({
                title: 'Month'
            }),
            yAxis: new Ext.chart.NumericAxis({
                title: 'Hits'
            }),
            extraStyle: {
               xAxis: {
                    labelRotation: -90
                }
            }
        }
    });
}