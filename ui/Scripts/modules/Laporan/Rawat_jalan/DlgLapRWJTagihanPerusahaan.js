
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRWJTagihanPerusahaan;
var selectNamaRWJTagihanPerusahaan;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRWJTagihanPerusahaan;
var varLapRWJTagihanPerusahaan= ShowFormLapRWJTagihanPerusahaan();
var selectSetUmum;
var selectSetkelpas;



var selectSetPilihankelompokPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;

function ShowFormLapRWJTagihanPerusahaan()
{
    frmDlgRWJTagihanPerusahaan= fnDlgRWJTagihanPerusahaan();
    frmDlgRWJTagihanPerusahaan.show();
};

function fnDlgRWJTagihanPerusahaan()
{
    var winRWJTagihanPerusahaanReport = new Ext.Window
    (
        {
            id: 'winRWJTagihanPerusahaanReport',
            title: 'Laporan Transaksi Tagihan Perusahaan',
            closeAction: 'destroy',
            width:400,
            height: 250,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJTagihanPerusahaan()]
        }
    );

    return winRWJTagihanPerusahaanReport;
};


function ItemDlgRWJTagihanPerusahaan()
{
    var PnlLapRWJTagihanPerusahaan = new Ext.Panel
    (
        {
            id: 'PnlLapRWJTagihanPerusahaan',
            fileUpload: true,
            layout: 'form',
            width:400,
            height: 250,
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapRWJTagihanPerusahaan_Atas(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWJTagihanPerusahaan',
                            handler: function()
                            {
                                //if (ValidasiReportRWJPasienPerDokter() === 1)
                                //{                             
                                    var params={
                        //id : 'cbPendaftaran_RWJTagihanPerusahaan'
                        //id : 'cbTindakRWJ_RWJTagihanPerusahaan'
                        //$('.messageCheckbox:checked').val();
                                        kd_kelompok:GetCriteriaRWJPasienPerKelompok(),
                                        status:Ext.getCmp('cboPilihanRWJStatusPembayaran').getValue(),
                                        jenis_cust:Ext.getCmp('cboPilihanRWJPasienPerKelompok').getValue(),
                                        sort:Ext.getCmp('cboPilihanRWJUrutanSorting').getValue(),
                                        tglAwal:Ext.getCmp('dtpTglAwalLapRWJTagihanPerusahaan').getValue(),
                                        tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJTagihanPerusahaan').getValue(),
                                    } 
                                    //console.log(params);
                                    var form = document.createElement("form");
                                    form.setAttribute("method", "post");
                                    form.setAttribute("target", "_blank");
                                    form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_RWJTagihanPerusahaan/cetak");
                                    var hiddenField = document.createElement("input");
                                    hiddenField.setAttribute("type", "hidden");
                                    hiddenField.setAttribute("name", "data");
                                    hiddenField.setAttribute("value", Ext.encode(params));
                                    form.appendChild(hiddenField);
                                    document.body.appendChild(form);
                                    form.submit();      
                                    frmDlgRWJPasienPerKelompok.close(); 
                                    
                               //};
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWJTagihanPerusahaan',
                            handler: function()
                            {
                                    frmDlgRWJTagihanPerusahaan.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWJTagihanPerusahaan;
};

function getItemLapRWJTagihanPerusahaan_Atas()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 170,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalLapRWJTagihanPerusahaan',
                format: 'd/M/Y',
                value: now
                //value: tigaharilalu
            }, {
                x: 230,
                y: 10,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAkhirLapRWJTagihanPerusahaan',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboRWJPasienPerKelompok(),
                mComboRWJPasienPerKelompokSEMUA(),
                mComboRWJPasienPerKelompokPERORANGAN(),
                mComboRWJPasienPerKelompokPERUSAHAAN(),
                mComboRWJPasienPerKelompokASURANSI(),
            {
                x: 10,
                y: 100,
                xtype: 'label',
                text: 'Status Pembayaran '
            }, {
                x: 110,
                y: 100,
                xtype: 'label',
                text: ' : '
            },
                mComboRWJStatusPembayaran(),
            {
                x: 10,
                y: 130,
                xtype: 'label',
                text: 'Sorting Berdasarkan '
            }, {
                x: 110,
                y: 130,
                xtype: 'label',
                text: ' : '
            },
                mComboRWJUrutanSorting(),
            ]
        }]
    };
    return items;
};

function ShowPesanWarningRWJTagihanPerusahaanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function mComboDokterRWJTagihanPerusahaan()
{
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterPelayaranDokter = new WebApp.DataStore({fields: Field});
    dsDokterPelayaranDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboDokterLaporan',
                param: "left(p.kd_unit, 1)='2'"
            }
        }
    );
    var cboPilihanRWJTagihanPerusahaan = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'cboDokterRWJTagihanPerusahaan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                width:240,
                store: dsDokterPelayaranDokter,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihanDokter=b.data.displayText ;
                        }
                }
            }
    );
    return cboPilihanRWJTagihanPerusahaan;
};


function mCombounitRWJTagihanPerusahaan()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

    ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewComboUnitLaporan',
                    param: "left(kd_unit, 1)='2'"
                }
            }
        );

    var cbounitRequestEntryRWJTagihanPerusahaan = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cbounitRequestEntryRWJTagihanPerusahaan',
            width:240,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
                        selectSetUnit =b.data.displayField;                     
                    }
                    
                }
        }
    )

    return cbounitRequestEntryRWJTagihanPerusahaan;
};


function mComboRWJPasienPerKelompok()
{
    var cboPilihanRWJTagihanPerusahaankelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 40,
                id:'cboPilihanRWJPasienPerKelompok',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perusahaan'], [3, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_SelectRWJPasienPerKelompok(b.data.displayText);
                    }
                }
            }
    );
    return cboPilihanRWJTagihanPerusahaankelompokPasien;
};

function mComboRWJStatusPembayaran()
{
    var cboPilihanRWJTagihanPerusahaankelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 100,
                id:'cboPilihanRWJStatusPembayaran',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Status...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Lunas'], [3, 'Belum Lunas']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                    }
                }
            }
    );
    return cboPilihanRWJTagihanPerusahaankelompokPasien;
};

function mComboRWJUrutanSorting()
{
    var cboPilihanRWJTagihanPerusahaankelompokPasien = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 130,
                id:'cboPilihanRWJUrutanSorting',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Status...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'No Medic'], [2, 'Nama Pasien'], [3, 'Tanggal Transaksi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                    }
                }
            }
    );
    return cboPilihanRWJTagihanPerusahaankelompokPasien;
};

//RWJPasienPerKelompok
function mComboRWJPasienPerKelompokSEMUA()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboLookupCustomer',
                param: 'jenis_cust=0 order by CUSTOMER'
            }
        }
    );
    var cboPerseoranganRWJ = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'IDmComboRWJPasienPerKelompokSEMUA',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                valueField: 'Id',
                displayField: 'displayText',
                hidden:false,
                fieldLabel: '',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                        {
                            id: 0,
                            fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                            data: [[1, 'Semua']]
                        }
                ),
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectSetUmum=b.data.displayText ;
                    }
                                    
                                
                }
            }
    );
    return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokPERORANGAN()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='0' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganRWJ = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'IDmComboRWJPasienPerKelompokPERORANGAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
    );
    return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokPERUSAHAAN()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='1' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganRWJ = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'IDmComboRWJPasienPerKelompokPERUSAHAAN',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Pilih Perusahaan...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                //value: selectsetperusahaan,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectsetperusahaan = b.data.KD_CUSTOMER;
                        selectsetnamaperusahaan = b.data.CUSTOMER;
                    }
                }
            }
    );
    return cboPerseoranganRWJ;
};

function mComboRWJPasienPerKelompokASURANSI()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboCostumer',
                param: "WHERE kontraktor.jenis_cust='2' ORDER BY customer.customer ASC"
            }
        }
    );
    var cboPerseoranganRWJ = new Ext.form.ComboBox
    (
            {
                x: 120,
                y: 70,
                id:'IDmComboRWJPasienPerKelompokASURANSI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                hidden : true,
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Pilih Asuransi...',
                fieldLabel: '',
                width: 240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        selectSetAsuransi=b.data.KD_CUSTOMER ;
                        selectsetnamaAsuransi = b.data.CUSTOMER ;
                    }
                }
            }
    );
    return cboPerseoranganRWJ;
};


function Combo_SelectRWJPasienPerKelompok(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').show();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').show();
   }
   else
   {
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').hide();
        Ext.getCmp('IDmComboRWJPasienPerKelompokSEMUA').show();
   }
};


function GetCriteriaRWJPasienPerKelompok()
{
    var strKriteria = '';
    
    if (Ext.getCmp('cboPilihanRWJPasienPerKelompok').getValue() !== '')
    {
        if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Semua') { strKriteria = 'SEMUA'; } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Perseorangan'){ strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokPERORANGAN').getValue(); } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Perusahaan'){ strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokPERUSAHAAN').getValue(); } 
        else if (Ext.get('cboPilihanRWJPasienPerKelompok').getValue() === 'Asuransi') { strKriteria = Ext.getCmp('IDmComboRWJPasienPerKelompokASURANSI').getValue(); }
    }else{
            strKriteria = 'Semua';
    }

    
    return strKriteria;
};

function ValidasiReportRWJPasienPerDokter()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJPasienPerDokter').dom.value === '')
    {
        ShowPesanWarningRWJPasienPerDokterReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
        x=0;
    }
    return x;
};
