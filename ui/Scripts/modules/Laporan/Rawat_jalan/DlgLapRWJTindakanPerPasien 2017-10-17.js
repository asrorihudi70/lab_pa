
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRWJTindakanPerPasien;
var selectNamaRWJTindakanPerPasien;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRWJTindakanPerPasien;
var varLapRWJTindakanPerPasien= ShowFormLapRWJTindakanPerPasien();
var selectSetUmum;
var selectSetkelpas;



var selectSetPilihankelompokPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;
var type_file;
function ShowFormLapRWJTindakanPerPasien()
{
    frmDlgRWJTindakanPerPasien= fnDlgRWJTindakanPerPasien();
    frmDlgRWJTindakanPerPasien.show();
};

function fnDlgRWJTindakanPerPasien()
{
    var winRWJTindakanPerPasienReport = new Ext.Window
    (
        {
            id: 'winRWJTindakanPerPasienReport',
            title: 'Laporan Tindakan Per Pasien',
            closeAction: 'destroy',
            width:400,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJTindakanPerPasien()],
            listeners:
			{
				activate: function()
				{
					
				}
			}

        }
    );

    return winRWJTindakanPerPasienReport;
};


function ItemDlgRWJTindakanPerPasien()
{
    var PnlLapRWJTindakanPerPasien = new Ext.Panel
    (
        {
            id: 'PnlLapRWJTindakanPerPasien',
            layout: 'form',
            height: 420,
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapRWJTindakanPerPasien_Atas(),
            ],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					id: 'btnPreviewLapRWJTindakanPerPasien',
					handler: function()
					{
						var kd_customer;
						if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Perseorangan'){
							kd_customer=Ext.getCmp('cboPerseoranganRWJ').getValue();
						} else if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Perusahaan'){
							kd_customer=Ext.getCmp('cboPerusahaanRequestEntryRWJ').getValue();
						} else if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Asuransi'){
							kd_customer=Ext.getCmp('cboAsuransiRWJ').getValue();
						} else{
							kd_customer='SEMUA';
						}
						//alert(Ext.getCmp('CekExcel').getValue());
					  	var params={
							tglAwal:Ext.getCmp('dtpTglAwalLapRWJTindakanPerPasien').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJTindakanPerPasien').getValue(),
							kd_unit:Ext.getCmp('cbounitRequestEntryRWJTindakanPerPasien').getValue(),
							kd_customer:kd_customer,
							type_file:Ext.getCmp('CekExcel').getValue(),
							order_by:Ext.getCmp('cboOrderBy').getValue(),
							
						} 
						console.log(params);
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_tindakan_perpasien/preview");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();	
					}
				},
				{
					xtype: 'button',
					text: 'Print',
					width: 70,
					hideLabel: true,
					id: 'btnPrintLapRWJTindakanPerPasien',
					handler: function()
					{
						var kd_customer;
						if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Perseorangan'){
							kd_customer=Ext.getCmp('cboPerseoranganRWJ').getValue();
						} else if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Perusahaan'){
							kd_customer=Ext.getCmp('cboPerusahaanRequestEntryRWJ').getValue();
						} else if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Asuransi'){
							kd_customer=Ext.getCmp('cboAsuransiRWJ').getValue();
						} else{
							kd_customer='SEMUA';
						}
						//alert(Ext.getCmp('CekExcel').getValue());
					  	var params={
							tglAwal:Ext.getCmp('dtpTglAwalLapRWJTindakanPerPasien').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJTindakanPerPasien').getValue(),
							kd_unit:Ext.getCmp('cbounitRequestEntryRWJTindakanPerPasien').getValue(),
							kd_customer:kd_customer,
							type_file:Ext.getCmp('CekExcel').getValue(),
							order_by:Ext.getCmp('cboOrderBy').getValue()
							
						} 
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_tindakan_perpasien/doprint");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();	
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRWJTindakanPerPasien',
					handler: function()
					{
							frmDlgRWJTindakanPerPasien.close();
					}
				}
			]
        }
    );

    return PnlLapRWJTindakanPerPasien;
};

function getItemLapRWJTindakanPerPasien_Atas()
{
    var items = {
        layout: 'column',
        border: true,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 190,
            anchor: '100% 100%',
            items: [
			{
				x: 10,
				y: 10,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, 
			{
				x: 110,
				y: 10,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 10,
				xtype: 'datefield',
				id: 'dtpTglAwalLapRWJTindakanPerPasien',
				format: 'd/M/Y',
				value: now
			}, {
				x: 230,
				y: 10,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 260,
				y: 10,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapRWJTindakanPerPasien',
				format: 'd/M/Y',
				value: now,
				width: 100
			},
			{
				x: 10,
				y: 40,
				xtype: 'label',
				text: 'Poliklinik '
			}, {
				x: 110,
				y: 40,
				xtype: 'label',
				text: ' : '
			},
				mCombounitRWJTindakanPerPasien(),
			{
				x: 10,
				y: 70,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 70,
				xtype: 'label',
				text: ' : '
			},
				mComboKelompokPasienRWJTindakanPerPasien(),
				mComboUmumRWJTindakanPerPasien(),
				mComboPerseoranganRWJTindakanPerPasien(),
				mComboAsuransiRWJTindakanPerPasien(),
				mComboPerusahaanRWJTindakanPerPasien(),
			{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Order '
			}, {
				x: 110,
				y: 130,
				xtype: 'label',
				text: ' : '
			},
				mComboOrder(),
			{
				x: 10,
				y: 160,
				xtype: 'label',
				text: 'Excel '
			}, 
			{
				xtype: 'checkbox',
				id: 'CekExcel',
				hideLabel:false,
				//checked: false,
				x: 40,
				y: 160,
				listeners: 
				{
					
				}
			},
            ]
        }]
    };
    return items;
};


function ShowPesanWarningRWJTindakanPerPasienReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function mComboKelompokPasienRWJTindakanPerPasien()
{
    var cboPilihanRWJTindakanPerPasienkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanRWJTindakanPerPasienkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanRWJTindakanPerPasienkelompokPasien;
};

function mComboPerseoranganRWJTindakanPerPasien()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganRWJ',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,	
				hidden:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRWJ;
};

function mComboUmumRWJTindakanPerPasien()
{
    var cboUmumRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			valueField: 'Id',
            displayField: 'displayText',
            value:1,
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
									'Id',
									'displayText'
							],
					data: [[1, 'Semua']]
					}
			),
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumRWJ;
};

function mComboPerusahaanRWJTindakanPerPasien()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1  order by CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryRWJ',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
			hidden:true,
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRWJ;
};

function mComboAsuransiRWJTindakanPerPasien()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2  order by CUSTOMER"
            }
        }
    );
    var cboAsuransiRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			hidden:true,
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiRWJ;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRWJ').show();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').show();
        Ext.getCmp('cboUmumRWJ').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').show();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').show();
   }
}

function mCombounitRWJTindakanPerPasien()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnitB',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryRWJTindakanPerPasien = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 40,
            id: 'cbounitRequestEntryRWJTindakanPerPasien',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryRWJTindakanPerPasien;
};


function mComboOrder()
{
    var cboOrderBy = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 130,
            id:'cboOrderBy',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: 'Order  ',
            width:170,
            value:1,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                    data: [[0, 'Medrec'], [1, 'Nama Pasien'], [2, 'Tanggal Masuk'], [3, 'Penjamin']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:'Medrec',
        }
    );
    return cboOrderBy;
};