
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRWJTindakanPerPasien;
var selectNamaRWJTindakanPerPasien;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRWJTindakanPerPasien;
var varLapRWJTindakanPerPasien= ShowFormLapRWJTindakanPerPasien();
var selectSetUmum;
var selectSetkelpas;



var selectSetPilihankelompokPasien;
var selectSetPilihanDokter;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;
var selectSetUnit;
var type_file;
function ShowFormLapRWJTindakanPerPasien()
{
    frmDlgRWJTindakanPerPasien= fnDlgRWJTindakanPerPasien();
    frmDlgRWJTindakanPerPasien.show();
};

function fnDlgRWJTindakanPerPasien()
{
    var winRWJTindakanPerPasienReport = new Ext.Window
    (
        {
            id: 'winRWJTindakanPerPasienReport',
            title: 'Laporan Tindakan Per Pasien',
            closeAction: 'destroy',
            width:400,
            height: 530,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJTindakanPerPasien()],
            listeners:
			{
				activate: function()
				{
					
				}
			}

        }
    );

    return winRWJTindakanPerPasienReport;
};


function ItemDlgRWJTindakanPerPasien()
{
    var PnlLapRWJTindakanPerPasien = new Ext.Panel
    (
        {
            id: 'PnlLapRWJTindakanPerPasien',
            layout: 'form',
            height: 420,
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				gridUnitLapRWJPasienPerDokter(),
                getItemLapRWJTindakanPerPasien_Atas(),
            ],
			fbar:
			[
				{
					xtype: 'button',
					text: 'Preview',
					width: 70,
					hideLabel: true,
					id: 'btnPreviewLapRWJTindakanPerPasien',
					handler: function()
					{
						var sendDataArray = [];
						secondGridStore.each(function(record){
							var recordArray = [record.get("KD_UNIT")];
							sendDataArray.push(recordArray);
						});
						var kd_customer;
						if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Perseorangan'){
							kd_customer=Ext.getCmp('cboPerseoranganRWJ').getValue();
						} else if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Perusahaan'){
							kd_customer=Ext.getCmp('cboPerusahaanRequestEntryRWJ').getValue();
						} else if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Asuransi'){
							kd_customer=Ext.getCmp('cboAsuransiRWJ').getValue();
						} else{
							kd_customer='SEMUA';
						}
						//alert(Ext.getCmp('CekExcel').getValue());
						if (sendDataArray.length === 0)
						{  
							ShowPesanWarningRWJTindakanPerPasienReport('Isi kriteria unit dengan drag and drop','WARNING');
							loadMask.hide();
						} else{
							var params={
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJTindakanPerPasien').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJTindakanPerPasien').getValue(),
								// kd_unit:Ext.getCmp('cbounitRequestEntryRWJTindakanPerPasien').getValue(),
								kd_customer:kd_customer,
								type_file:Ext.getCmp('CekExcel').getValue(),
								order_by:Ext.getCmp('cboOrderBy').getValue(),
								poliklinik:sendDataArray
								
							} 
							console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_tindakan_perpasien/preview");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();	
						}
					}
				},
				{
					xtype: 'button',
					text: 'Print',
					width: 70,
					hideLabel: true,
					id: 'btnPrintLapRWJTindakanPerPasien',
					handler: function()
					{
						var sendDataArray = [];
						secondGridStore.each(function(record){
							var recordArray = [record.get("KD_UNIT")];
							sendDataArray.push(recordArray);
						});
						var kd_customer;
						if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Perseorangan'){
							kd_customer=Ext.getCmp('cboPerseoranganRWJ').getValue();
						} else if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Perusahaan'){
							kd_customer=Ext.getCmp('cboPerusahaanRequestEntryRWJ').getValue();
						} else if(Ext.get('cboPilihanRWJTindakanPerPasienkelompokPasien').getValue() == 'Asuransi'){
							kd_customer=Ext.getCmp('cboAsuransiRWJ').getValue();
						} else{
							kd_customer='SEMUA';
						}
						//alert(Ext.getCmp('CekExcel').getValue());
						if (sendDataArray.length === 0)
						{  
							ShowPesanWarningRWJTindakanPerPasienReport('Isi kriteria unit dengan drag and drop','WARNING');
							loadMask.hide();
						} else{
							var params={
								tglAwal:Ext.getCmp('dtpTglAwalLapRWJTindakanPerPasien').getValue(),
								tglAkhir:Ext.getCmp('dtpTglAkhirLapRWJTindakanPerPasien').getValue(),
								// kd_unit:Ext.getCmp('cbounitRequestEntryRWJTindakanPerPasien').getValue(),
								kd_customer:kd_customer,
								type_file:Ext.getCmp('CekExcel').getValue(),
								order_by:Ext.getCmp('cboOrderBy').getValue(),
								poliklinik:sendDataArray
								
							} 
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/lap_tindakan_perpasien/doprint");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();	
						}
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRWJTindakanPerPasien',
					handler: function()
					{
							frmDlgRWJTindakanPerPasien.close();
					}
				}
			]
        }
    );

    return PnlLapRWJTindakanPerPasien;
};

function getItemLapRWJTindakanPerPasien_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: false,
            width:  480,
            height: 190,
            anchor: '100% 100%',
            items: [
			{
				x: 10,
				y: 10,
				xtype: 'label',
				text: 'Periode Tanggal '
			}, 
			{
				x: 110,
				y: 10,
				xtype: 'label',
				text: ' : '
			}, {
				x: 120,
				y: 10,
				xtype: 'datefield',
				id: 'dtpTglAwalLapRWJTindakanPerPasien',
				format: 'd/M/Y',
				value: now
			}, {
				x: 230,
				y: 10,
				xtype: 'label',
				text: ' s/d '
			}, {
				x: 260,
				y: 10,
				xtype: 'datefield',
				id: 'dtpTglAkhirLapRWJTindakanPerPasien',
				format: 'd/M/Y',
				value: now,
				width: 100
			},
			// {
				// x: 10,
				// y: 40,
				// xtype: 'label',
				// text: 'Poliklinik '
			// }, {
				// x: 110,
				// y: 40,
				// xtype: 'label',
				// text: ' : '
			// },
				// mCombounitRWJTindakanPerPasien(),
			{
				x: 10,
				y: 40,
				xtype: 'label',
				text: 'Kelompok pasien '
			}, {
				x: 110,
				y: 40,
				xtype: 'label',
				text: ' : '
			},
				mComboKelompokPasienRWJTindakanPerPasien(),
				mComboUmumRWJTindakanPerPasien(),
				mComboPerseoranganRWJTindakanPerPasien(),
				mComboAsuransiRWJTindakanPerPasien(),
				mComboPerusahaanRWJTindakanPerPasien(),
			{
				x: 10,
				y: 100,
				xtype: 'label',
				text: 'Order '
			}, {
				x: 110,
				y: 100,
				xtype: 'label',
				text: ' : '
			},
				mComboOrder(),
			{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Excel '
			}, 
			{
				xtype: 'checkbox',
				id: 'CekExcel',
				hideLabel:false,
				//checked: false,
				x: 40,
				y: 130,
				listeners: 
				{
					
				}
			},
            ]
        }]
    };
    return items;
};
function gridUnitLapRWJPasienPerDokter(){
	var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    datarefresh_viInformasiUnit()

    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
	];


	// declare the source Grid
	firstGrid = new Ext.grid.GridPanel({
		ddGroup          : 'secondGridDDGroup',
		store            : dataSource_unit,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		enableDragDrop   : true,
		height           : 200,
		stripeRows       : true,
		trackMouseOver   : true,
		title            : 'Poliklinik',
		anchor           : '100% 100%',
		plugins          : [new Ext.ux.grid.FilterRow()],
		colModel         : new Ext.grid.ColumnModel
					(
						[
							new Ext.grid.RowNumberer(),
							{
									id: 'colNRM_viDaftar',
									header: 'No.Medrec',
									dataIndex: 'KD_UNIT',
									sortable: true,
									hidden : true
							},
							{
									id: 'colNMPASIEN_viDaftar',
									header: 'Nama',
									dataIndex: 'NAMA_UNIT',
									sortable: true,
									width: 50
							}
						]
					),
		listeners : {
			afterrender : function(comp) {
			var firstGridDropTargetEl = firstGrid.getView().scroller.dom;
			var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
					ddGroup    : 'firstGridDDGroup',
					notifyDrop : function(ddSource, e, data){
							var records =  ddSource.dragData.selections;
							Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
							firstGrid.store.add(records);
							firstGrid.store.sort('KD_UNIT', 'ASC');
							return true
					}
			});
			}
		},

		viewConfig: 
			{
					forceFit: true
			}
	});

	secondGridStore = new Ext.data.JsonStore({
		fields : fields,
		root   : 'records'
	});

        // create the destination Grid
	secondGrid = new Ext.grid.GridPanel({
		ddGroup          : 'firstGridDDGroup',
		store            : secondGridStore,
		columns          : cols,
		enableDragDrop   : true,
		height           : 200,
		stripeRows       : true,
		autoExpandColumn : 'KD_UNIT',
		title            : 'Poliklinik',
		listeners : {
		afterrender : function(comp) {
		var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
		var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
				ddGroup    : 'secondGridDDGroup',
				notifyDrop : function(ddSource, e, data){
						var records =  ddSource.dragData.selections;
						Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
						secondGrid.store.add(records);
						secondGrid.store.sort('KD_UNIT', 'ASC');
						return true
				}
		});
		}
	},
	viewConfig: 
		{
				forceFit: true
		}
	});
	
	var FrmTabs_viInformasiUnit = new Ext.Panel
	(
		{
		    id: 'pilihunitLapRWJTindakanPerPasien',
		    closable: true,
		    region: 'center',
		    layout: 'column',
            height   : 250,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    // iconCls: '',
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					anchor: '100% 100%',
					items:
					[firstGrid
						
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid
						
					]
				},
				{
                                        
					columnWidth: .60,
					layout: 'form',
					border: false,
					labelAlign: 'lfet',
					labelWidth: 5,
					items:
					[
						{
							xtype: 'checkbox',
							id: 'CekLapPilihLapRWJTindakanPerPasien',
							hideLabel:false,
							boxLabel: 'Pilih Semua Unit',
							checked: false,
							listeners: 
							{
								check: function()
								{
								   if(Ext.getCmp('CekLapPilihLapRWJTindakanPerPasien').getValue()===true)
									{
										 firstGrid.getSelectionModel().selectAll();
									}
									else
									{
										firstGrid.getSelectionModel().clearSelections();
									}
								}
							}
						},
					]
				}
			]
		}
	)
	return FrmTabs_viInformasiUnit;
}

function datarefresh_viInformasiUnit()
{
    dataSource_unit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnitB',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )
    //alert("refersh")
}


function ShowPesanWarningRWJTindakanPerPasienReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function mComboKelompokPasienRWJTindakanPerPasien()
{
    var cboPilihanRWJTindakanPerPasienkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 40,
                id:'cboPilihanRWJTindakanPerPasienkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanRWJTindakanPerPasienkelompokPasien;
};

function mComboPerseoranganRWJTindakanPerPasien()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerseoranganRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganRWJ = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPerseoranganRWJ',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,	
				hidden:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: dsPerseoranganRequestEntry,
                valueField: 'KD_CUSTOMER',
				displayField: 'CUSTOMER',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRWJ;
};

function mComboUmumRWJTindakanPerPasien()
{
    var cboUmumRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboUmumRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			valueField: 'Id',
            displayField: 'displayText',
            value:1,
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
									'Id',
									'displayText'
							],
					data: [[1, 'Semua']]
					}
			),
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumRWJ;
};

function mComboPerusahaanRWJTindakanPerPasien()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1  order by CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
		    id: 'cboPerusahaanRequestEntryRWJ',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
			hidden:true,
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRWJ;
};

function mComboAsuransiRWJTindakanPerPasien()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2  order by CUSTOMER"
            }
        }
    );
    var cboAsuransiRWJ = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboAsuransiRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			hidden:true,
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiRWJ;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRWJ').show();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').show();
        Ext.getCmp('cboUmumRWJ').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').show();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRWJ').hide();
        Ext.getCmp('cboAsuransiRWJ').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRWJ').hide();
        Ext.getCmp('cboUmumRWJ').show();
   }
}

function mCombounitRWJTindakanPerPasien()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnitB',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryRWJTindakanPerPasien = new Ext.form.ComboBox
    (
        {
			x: 120,
			y: 40,
            id: 'cbounitRequestEntryRWJTindakanPerPasien',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
			value: 'Semua',
            listeners:
                {
                    'select': function(a, b, c){
						selectSetUnit =b.data.displayField; 					
					}
                    
                }
        }
    )

    return cbounitRequestEntryRWJTindakanPerPasien;
};


function mComboOrder()
{
    var cboOrderBy = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 100,
            id:'cboOrderBy',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: 'Order  ',
            width:170,
            value:1,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                    data: [[0, 'Medrec'], [1, 'Nama Pasien'], [2, 'Tanggal Masuk'], [3, 'Penjamin']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:'Medrec',
        }
    );
    return cboOrderBy;
};