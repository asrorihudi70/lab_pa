var type_file=0;
var dsRWJ_IndexPekerjaan;
var selectRWJ_IndexPekerjaan;
var selectNamaRWJ_IndexPekerjaan;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJ_IndexPekerjaan;
var varLapRWJ_IndexPekerjaan= ShowFormLapRWJ_IndexPekerjaan();
var dsRWJ;

function ShowFormLapRWJ_IndexPekerjaan()
{
    frmDlgRWJ_IndexPekerjaan= fnDlgRWJ_IndexPekerjaan();
    frmDlgRWJ_IndexPekerjaan.show();
};

function fnDlgRWJ_IndexPekerjaan()
{
    var winRWJ_IndexPekerjaanReport = new Ext.Window
    (
        {
            id: 'winRWJ_IndexPekerjaanReport',
            title: 'Laporan Pembatalan Transaksi',
            closeAction: 'destroy',
            width:450,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJ_IndexPekerjaan()]

        }
    );

    return winRWJ_IndexPekerjaanReport;
};

function ItemDlgRWJ_IndexPekerjaan()
{
    var PnlLapRWJ_IndexPekerjaan = new Ext.Panel
    (
        {
            id: 'PnlLapRWJ_IndexPekerjaan',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
				mComboBagian(),
                getItemLapRWJ_IndexPekerjaan_Periode(),
				//getItemRWJ_IndexPekerjaan_Shift(),
				mComboPekerjaan(),
				{
				   xtype: 'checkbox',
				   fieldLabel: 'Type ',
				   id: 'CekLapPilihTypeExcel',
				   hideLabel:false,
				   boxLabel: 'Excel',
				   checked: false,
				   listeners: 
				   {
						check: function()
						{
						   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
							{
								type_file=1;
							}
							else
							{
								type_file=0;
							}
						}
				   }
				},
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJ_IndexPekerjaan',
					handler: function(){
					   if (ValidasiReportRWJ_IndexPekerjaan() === 1){
							var params={
								tgl_awal 	: Ext.getCmp('dtpTglAwalLapRWJ_IndexPekerjaan').getValue(),
								tgl_akhir 	: Ext.getCmp('dtpTglAkhirLapRWJ_IndexPekerjaan').getValue(),
								type_file 	: type_file,
								bagian 		: Ext.getCmp('cboBagian').getValue(),
								pekerjaan 	: Ext.getCmp('cboPekerjaan').getValue(),
							};
							console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/function_lap_RWJ/lap_index_pekerjaan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							// frmDlgRWJ_IndexPekerjaan.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLapRWJ_IndexPekerjaan').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapRWJ_IndexPekerjaan').getValue()+'#aje#Laporan Batal Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionRWJ/cetakRWJ_IndexPekerjaan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJ_IndexPekerjaan.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJ_IndexPekerjaan',
					handler: function()
					{
						frmDlgRWJ_IndexPekerjaan.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJ_IndexPekerjaan;
};


function ValidasiReportRWJ_IndexPekerjaan()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJ_IndexPekerjaan').dom.value === '')
	{
		ShowPesanWarningRWJ_IndexPekerjaanReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapRWJ_IndexPekerjaan').getValue() === false && Ext.getCmp('Shift_1_LapRWJ_IndexPekerjaan').getValue() === false && Ext.getCmp('Shift_2_LapRWJ_IndexPekerjaan').getValue() === false && Ext.getCmp('Shift_3_LapRWJ_IndexPekerjaan').getValue() === false){
		ShowPesanWarningRWJ_IndexPekerjaanReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportRWJ_IndexPekerjaan()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJ_IndexPekerjaan').dom.value > Ext.get('dtpTglAkhirLapRWJ_IndexPekerjaan').dom.value)
    {
        ShowPesanWarningRWJ_IndexPekerjaanReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJ_IndexPekerjaanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJ_IndexPekerjaan_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJ_IndexPekerjaan_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			// {
			// 	columnWidth: .49,
			// 	layout: 'form',
			// 	labelWidth:70,
			// 	border: false,
			// 	items:
			// 	[
			// 	]
			// },
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:100,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJ_IndexPekerjaan',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJ_IndexPekerjaan',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};



function mComboBagian()
{
    var cboBagian = new Ext.form.ComboBox
	(
		{
			/*x: 120,
			y: 220,*/
			id:'cboBagian',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Bagian ',
			// width:240,
			anchor:'99%',
			value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				    data: [[0, 'Rawat Inap'],[1, 'Rawat Jalan'],[2, 'Gawat Darurat']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
		}
	);
	return cboBagian;
};

function mComboPekerjaan()
{
	var Field = ['KD_PEKERJAAN','PEKERJAAN'];
    var dsPekerjaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPekerjaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboPekerjaan',
                param: ""
			}
		}
	);

    var cboOrderBy = new Ext.form.ComboBox
	(
		{
			id:'cboPekerjaan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Pekerjaan',
			anchor:'99%',
			store: dsPekerjaanRequestEntry,
			valueField: 'KD_PEKERJAAN',
			displayField: 'PEKERJAAN',
		}
	);
	return cboOrderBy;
};
