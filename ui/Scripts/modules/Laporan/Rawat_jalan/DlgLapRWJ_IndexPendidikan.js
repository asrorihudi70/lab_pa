var type_file=0;
var dsRWJ_IndexPendidikan;
var selectRWJ_IndexPendidikan;
var selectNamaRWJ_IndexPendidikan;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJ_IndexPendidikan;
var varLapRWJ_IndexPendidikan= ShowFormLapRWJ_IndexPendidikan();
var dsRWJ;

function ShowFormLapRWJ_IndexPendidikan()
{
    frmDlgRWJ_IndexPendidikan= fnDlgRWJ_IndexPendidikan();
    frmDlgRWJ_IndexPendidikan.show();
};

function fnDlgRWJ_IndexPendidikan()
{
    var winRWJ_IndexPendidikanReport = new Ext.Window
    (
        {
            id: 'winRWJ_IndexPendidikanReport',
            title: 'Laporan Index Pendidikan',
            closeAction: 'destroy',
            width:450,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJ_IndexPendidikan()]

        }
    );

    return winRWJ_IndexPendidikanReport;
};

function ItemDlgRWJ_IndexPendidikan()
{
    var PnlLapRWJ_IndexPendidikan = new Ext.Panel
    (
        {
            id: 'PnlLapRWJ_IndexPendidikan',
            fileUpload: true,
            layout: 'form',
            width:300,
            height: 200,//120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
				mComboBagian(),
                getItemLapRWJ_IndexPendidikan_Periode(),
				//getItemRWJ_IndexPendidikan_Shift(),
				mComboPendidikan(),
				{
				   xtype: 'checkbox',
				   fieldLabel: 'Type ',
				   id: 'CekLapPilihTypeExcel',
				   hideLabel:false,
				   boxLabel: 'Excel',
				   checked: false,
				   listeners: 
				   {
						check: function()
						{
						   if(Ext.getCmp('CekLapPilihTypeExcel').getValue()===true)
							{
								type_file=1;
							}
							else
							{
								type_file=0;
							}
						}
				   }
				},
            ],
			fbar:[
				{
					xtype: 'button',
					text: 'Ok',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkLapRWJ_IndexPendidikan',
					handler: function(){
					   if (ValidasiReportRWJ_IndexPendidikan() === 1){
							var params={
								tgl_awal 	: Ext.getCmp('dtpTglAwalLapRWJ_IndexPendidikan').getValue(),
								tgl_akhir 	: Ext.getCmp('dtpTglAkhirLapRWJ_IndexPendidikan').getValue(),
								type_file 	: type_file,
								bagian 		: Ext.getCmp('cboBagian').getValue(),
								pendidikan 	: Ext.getCmp('cboPendidikan').getValue(),
							};
							console.log(params);
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/function_lap_RWJ/lap_index_pendidikan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							// frmDlgRWJ_IndexPendidikan.close(); 
						
							
						/*
							var params=Ext.get('dtpTglAwalLapRWJ_IndexPendidikan').getValue()+'#aje#'
								+Ext.get('dtpTglAkhirLapRWJ_IndexPendidikan').getValue()+'#aje#Laporan Batal Transaksi Rawat Jalan#aje#01';
							
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionRWJ/cetakRWJ_IndexPendidikan");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value",params);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();		
							frmDlgRWJ_IndexPendidikan.close(); 
						*/
					   };
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJ_IndexPendidikan',
					handler: function()
					{
						frmDlgRWJ_IndexPendidikan.close();
					}
				}
			
			]
        }
    );

    return PnlLapRWJ_IndexPendidikan;
};


function ValidasiReportRWJ_IndexPendidikan()
{
    var x=1;
	if(Ext.get('dtpTglAwalLapRWJ_IndexPendidikan').dom.value === '')
	{
		ShowPesanWarningRWJ_IndexPendidikanReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;
	}
/* 	if(Ext.getCmp('Shift_All_LapRWJ_IndexPendidikan').getValue() === false && Ext.getCmp('Shift_1_LapRWJ_IndexPendidikan').getValue() === false && Ext.getCmp('Shift_2_LapRWJ_IndexPendidikan').getValue() === false && Ext.getCmp('Shift_3_LapRWJ_IndexPendidikan').getValue() === false){
		ShowPesanWarningRWJ_IndexPendidikanReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
		x=0;	
	} */

    return x;
};

function ValidasiTanggalReportRWJ_IndexPendidikan()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJ_IndexPendidikan').dom.value > Ext.get('dtpTglAkhirLapRWJ_IndexPendidikan').dom.value)
    {
        ShowPesanWarningRWJ_IndexPendidikanReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJ_IndexPendidikanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJ_IndexPendidikan_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJ_IndexPendidikan_Periode()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
			// {
			// 	columnWidth: .49,
			// 	layout: 'form',
			// 	labelWidth:70,
			// 	border: false,
			// 	items:
			// 	[
			// 	]
			// },
			{
				columnWidth: .49,
				layout: 'form',
				labelWidth:100,
				border: false,
				items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Periode ',
						id: 'dtpTglAwalLapRWJ_IndexPendidikan',
						format: 'd-M-Y',
						value:now,
						anchor: '99%'
					},
					
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'label',
						text : 's/d'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'datefield',
						id: 'dtpTglAkhirLapRWJ_IndexPendidikan',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};



function mComboBagian()
{
    var cboBagian = new Ext.form.ComboBox
	(
		{
			/*x: 120,
			y: 220,*/
			id:'cboBagian',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Bagian ',
			// width:240,
			anchor:'99%',
			value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				    data: [[0, 'Rawat Inap'],[1, 'Rawat Jalan'],[2, 'Gawat Darurat']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
		}
	);
	return cboBagian;
};

function mComboPendidikan()
{
	var Field = ['KD_PENDIDIKAN','PENDIDIKAN'];
    var dsPendidikanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPendidikanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
                target: 'ViewComboPendidikan',
                param: ""
			}
		}
	);

    var cboOrderBy = new Ext.form.ComboBox
	(
		{
			id:'cboPendidikan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'PENDIDIKAN',
			anchor:'99%',
			store: dsPendidikanRequestEntry,
			valueField: 'KD_PENDIDIKAN',
			displayField: 'PENDIDIKAN',
		}
	);
	return cboOrderBy;
};
