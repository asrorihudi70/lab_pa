var fields = [
	{name: 'KD_UNIT', mapping : 'KD_UNIT'},
	{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];

var fieldsPayment = [
	{name: 'KD_PAY', mapping : 'KD_PAY'},
	{name: 'URAIAN', mapping : 'URAIAN'}
];

var cols = [
	{ id : 'kd_unit', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'kd_unit',hidden : true},
	{ header: "Nama", width: 50, sortable: true, dataIndex: 'nama_unit'}
];

var colsPayment = [
	{ id : 'KD_PAY', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'KD_PAY',hidden : true},
	{ header: "Kode Pay", width: 50, sortable: true, dataIndex: 'URAIAN'}
];

var secondGridStoreLapPenerimaan;
var secondGridStoreLapPenerimaanPaymentLapPenerimaan;
var firstGrid;
var jenis_pay='';
var dataSource_payment;
var firstGridPayment;
var DlgLapRWJPenerimaan={
	vars:{
		comboSelect:null
	},
	coba:function(){
		var $this=this;
		$this.DateField.startDate
	},
	ArrayStore:{
		dokter:new Ext.data.ArrayStore({fields:[]})
	},
	CheckboxGroup:{
		shift:null,
		tindakan:null,
	},
	ComboBox:{
		kelPasien1:null,
		combo1:null,
		combo2:null,
		combo3:null,
		combo0:null,
		poliklinik:null,
		dokter:null,
		Status:null,
	},
	DataStore:{
		combo2:null,
		combo3:null,
		poliklinik:null,
		combo1:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	comboOnSelect:function(val){
		var $this=this;
		$this.ComboBox.combo0.hide();
		$this.ComboBox.combo1.hide();
		$this.ComboBox.combo2.hide();
		$this.ComboBox.combo3.hide();
		if(val==-1){
			$this.ComboBox.combo0.show();
			jenis_pay='';
			$this.datapaymentfirstgrid(jenis_pay);
		}else if(val==0){
			$this.ComboBox.combo1.show();
			jenis_pay='jenis_pay=1';
			$this.datapaymentfirstgrid(jenis_pay);
		}else if(val==1){
			$this.ComboBox.combo2.show();
			jenis_pay='jenis_pay=3';
			$this.datapaymentfirstgrid(jenis_pay);
		}else if(val==2){
			$this.ComboBox.combo3.show();
			jenis_pay='jenis_pay=4';
			$this.datapaymentfirstgrid(jenis_pay);
		}
	},
	initPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		var sendDataArrayUnit    = [];
		// var sendDataArrayUnitTmp = "";
		var sendDataArraypayment = [];

		secondGridStoreLapPenerimaan.each(function(record){
			var recordArray   = [record.get("kd_unit")];
			sendDataArrayUnit.push(recordArray);
			// sendDataArrayUnitTmp += "'"+recordArray+"',";
		});

		/*secondGridStoreLapPenerimaanPaymentLapPenerimaan.each(function(record){
			var recordArraypay= [record.get("KD_PAY")];
			sendDataArraypayment.push(recordArraypay);
		});*/

		if (sendDataArrayUnit.length === 0)
		{  
			this.messageBox('Peringatan','Isi kriteria unit dengan drag and drop','WARNING');
			loadMask.hide();
		}else{
			params['type_file'] = Ext.getCmp('CetakExcelRWJPenerimaan').getValue();
			params['start_date'] = timestimetodate($this.DateField.startDate.getValue());
			params['last_date']  = timestimetodate($this.DateField.endDate.getValue());
			// params['pasien']     = $this.ComboBox.kelPasien1.getValue();

			params['tmp_kd_unit'] 	= sendDataArrayUnit;
			params['status'] 		= $this.ComboBox.Status.getValue();
			// params['tmp_payment'] 	= sendDataArraypayment;
			// var shift    =$this.CheckboxGroup.shift.items.items;
			// var tindakan =$this.CheckboxGroup.tisndakan.items.items;
			// var tindakan_stat=false;
			// for(var i=0;i<tindakan.length ; i++){
			// 	params['tindakan'+i]=tindakan[i].checked;
			// 	if(tindakan[i].checked==true)tindakan_stat=true;
			// }
			params['orderBy']     = $this.ComboBox.comboOrderBy.getValue();
			// console.log(params);
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("target", "_blank");
			form.setAttribute("action", baseURL + "index.php/rawat_jalan/function_lap/laporan_poli_pasien_baru_lama");
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", "data");
			hiddenField.setAttribute("value", Ext.encode(params));
			form.appendChild(hiddenField);
			document.body.appendChild(form);
			form.submit();
			loadMask.hide();
		} 
		
	},
	DirectPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		var sendDataArrayUnit    = [];
		// var sendDataArrayUnitTmp = "";
		var sendDataArraypayment = [];

		secondGridStoreLapPenerimaan.each(function(record){
			var recordArray   = [record.get("kd_unit")];
			sendDataArrayUnit.push(recordArray);
			// sendDataArrayUnitTmp += "'"+recordArray+"',";
		});

		/*secondGridStoreLapPenerimaanPaymentLapPenerimaan.each(function(record){
			var recordArraypay= [record.get("KD_PAY")];
			sendDataArraypayment.push(recordArraypay);
		});*/

		if (sendDataArrayUnit.length === 0)
		{  
			this.messageBox('Peringatan','Isi kriteria unit dengan drag and drop','WARNING');
			loadMask.hide();
		}else{
			params['type_file'] = Ext.getCmp('CetakExcelRWJPenerimaan').getValue();
			params['start_date'] = timestimetodate($this.DateField.startDate.getValue());
			params['last_date']  = timestimetodate($this.DateField.endDate.getValue());
			// params['pasien']     = $this.ComboBox.kelPasien1.getValue();

			params['tmp_kd_unit'] 	= sendDataArrayUnit;
			params['status'] 		= $this.ComboBox.Status.getValue();
			// params['tmp_payment'] 	= sendDataArraypayment;
			// var shift    =$this.CheckboxGroup.shift.items.items;
			// var tindakan =$this.CheckboxGroup.tisndakan.items.items;
			// var tindakan_stat=false;
			// for(var i=0;i<tindakan.length ; i++){
			// 	params['tindakan'+i]=tindakan[i].checked;
			// 	if(tindakan[i].checked==true)tindakan_stat=true;
			// }
			params['orderBy']     = $this.ComboBox.comboOrderBy.getValue();
			// console.log(params);
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("target", "_blank");
			form.setAttribute("action", baseURL + "index.php/rawat_jalan/function_lap/laporan_poli_pasien_baru_lama_direct");
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", "data");
			hiddenField.setAttribute("value", Ext.encode(params));
			form.appendChild(hiddenField);
			document.body.appendChild(form);
			form.submit();
			loadMask.hide();
		} 
		
	},
	messageBox:function(modul, str, icon){
		Ext.MessageBox.show
		(
			{
				title: modul,
				msg:str,
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.icon,
				width:300
			}
		);
	},
	getDokter:function(){
		var $this=this;
		$this.ComboBox.dokter = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.ArrayStore.dokter,
            width: 200,
            value:'Semua',
            valueField: 'kd_dokter',
            displayField: 'nama',
            listeners:{
            	'select': function(a, b, c){
                               
            	}
            }
        });		    
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gawat_darurat/lap_penerimaan/getDokter",
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.dokter.loadData([],false);
					for(var i=0,iLen=r.data.length; i<iLen ;i++){
						$this.ArrayStore.dokter.add(new $this.ArrayStore.dokter.recordType(r.data[i]));
					}
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
		return $this.ComboBox.dokter;
	},
	getPoliklinik:function(){
		var $this=this;
		var Field = ['KD_UNIT','NAMA_UNIT'];
		$this.DataStore.poliklinik = new WebApp.DataStore({fields: Field});
		$this.DataStore.poliklinik.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnitB',
                param: "kd_bagian=2 and type_unit=false"
            }
        });
		$this.ComboBox.poliklinik = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.DataStore.poliklinik,
            width: 200,
            value:'Semua',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            listeners:{
            	'select': function(a, b, c){
                               
            	}
            }
        });		    
	    return $this.ComboBox.poliklinik;
	},
	getCombo0:function(){
		var $this=this;
		$this.ComboBox.combo0 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: new Ext.data.ArrayStore({
				id: 0,
				fields:['Id','displayText'],
				data: [[1, 'Semua']]
			}),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			disabled:true,
			listeners:{
				'select': function(a,b,c){
					selectSetUmum=b.data.displayText ;
				}
			}
		});
		return $this.ComboBox.combo0;
	},
	getComboOrderBy:function(){
		var $this=this;
		$this.ComboBox.comboOrderBy = new Ext.form.ComboBox({
			// x:300,
			// y:3,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Order By',
			width:'100%',
			store: new Ext.data.ArrayStore({
				id: 0,
				fields:['Id','displayText'],
				data: [[2, 'Kode Pasien'],[3, 'Nama Pasien'],[4, 'Tanggal'],[5, 'Status Pasien']]
			}),
			valueField: 'Id',
			displayField: 'displayText',
			value:2,
			listeners:{
				'select': function(a,b,c){
					selectSetUmum=b.data.displayText ;
				}
			}
		});
		return $this.ComboBox.comboOrderBy;
	},

	getComboStatus:function(){
		var $this=this;
		$this.ComboBox.Status = new Ext.form.ComboBox({
			// x:300,
			// y:3,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Status',
			bodyStyle:'margin: 5px 5px  5px 5px',
			store: new Ext.data.ArrayStore({
				id: 0,
				fields:['Id','displayText'],
				data: [[1, 'Semua'],[2, 'Baru'],[3, 'Lama']]
			}),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
		});
		return $this.ComboBox.Status;
	},

	getCombo1:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo1 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo1.load({
    		params:{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 ORDER BY CUSTOMER '
			}
		});
    	$this.ComboBox.combo1 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hidden:true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: $this.DataStore.combo1,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
			listeners:{
				'select': function(a,b,c){
				}
			}
		});
		return $this.ComboBox.combo1;
	},
	getCombo2:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo2 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo2.load({
		    params:{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER '
			}
		});
   		$this.ComboBox.combo2 = new Ext.form.ComboBox({
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    hidden:true,
		    store: $this.DataStore.combo2,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			value:'Semua',
			width:150,
		    listeners:{
			    'select': function(a,b,c){
				}
			}
		});
    	return $this.ComboBox.combo2;
	},
	seconGridPenerimaan : function(){
		
		var secondGrid;
		secondGridStoreLapPenerimaan = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroup',
					store            : secondGridStoreLapPenerimaan,
					columns          : cols,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 130,
					stripeRows       : true,
					autoExpandColumn : 'kd_unit',
					title            : 'Unit yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroup',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGrid.store.add(records);
											secondGrid.store.sort('kd_unit', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGrid;
	},
	firstGridPenerimaan : function(){
		var dataSource_unit;
		var Field_poli_viDaftar = ['kd_unit','nama_unit'];
		dataSource_unit         = new WebApp.DataStore({fields: Field_poli_viDaftar});
		Ext.Ajax.request({
			url: baseURL + "index.php/rawat_jalan/lap_penerimaan/get_unit",
			params: '0',
			failure: function(o){
				var cst = Ext.decode(o.responseText);
			},	    
			success: function(o) {
				 firstGrid.store.removeAll();
				var cst = Ext.decode(o.responseText);

				for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
					var recs    = [],recType =  dataSource_unit.recordType;
					var o=cst['listData'][i];
			
					recs.push(new recType(o));
					dataSource_unit.add(recs);
					console.log(o);
				}
			}
		});
		/* dataSource_unit.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'NAMA_UNIT',
					Sortdir: 'ASC',
					target:'ViewSetupUnitB',
					param: "kd_bagian='2' and type_unit=false "
				}
			}
		); */
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 130,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'kd_unit',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'nama_unit',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),   
			listeners : {
				afterrender : function(comp) {
					var secondGridDropTargetEl = firstGrid.getView().scroller.dom;
					var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroup',
							notifyDrop : function(ddSource, e, data){
									var records =  ddSource.dragData.selections;
									Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
									firstGrid.store.add(records);
									firstGrid.store.sort('kd_unit', 'ASC');
									return true
							}
					});
				}
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid;
	},
	datapaymentfirstgrid:function(jenis_pay){
		dataSource_payment.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_PAY',
					Sortdir: 'ASC',
					target:'ViewListPayment',
					param: jenis_pay
				}
			}
		);
	},
	firstGridPayment : function(){
		var $this=this;
		var Field_payment = ['KD_PAY','URAIAN'];
		dataSource_payment         = new WebApp.DataStore({fields: Field_payment});
		$this.datapaymentfirstgrid('');
		firstGridPayment = new Ext.grid.GridPanel({
			ddGroup          : 'secondGridDDGroupPayment',
			store            : dataSource_payment,
			autoScroll       : true,
			columnLines      : true,
			border           : true,
			enableDragDrop   : true,
			height           : 130,
			stripeRows       : true,
			trackMouseOver   : true,
			title            : 'Payment',
			anchor           : '100% 100%',
			plugins          : [new Ext.ux.grid.FilterRow()],
			colModel         : new Ext.grid.ColumnModel
			(
				[
				new Ext.grid.RowNumberer(),
					{
						id: 'colKD_pay',
						header: 'Kode Pay',
						dataIndex: 'KD_PAY',
						sortable: true,
						hidden : true
					},
					{
						id: 'colKD_uraian',
						header: 'Uraian',
						dataIndex: 'URAIAN',
						sortable: true,
						width: 50
					}
				]
			),   
			listeners : {
				afterrender : function(comp) {
					var secondGridDropTargetEl = firstGridPayment.getView().scroller.dom;
					var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroupPayment',
							notifyDrop : function(ddSource, e, data){
									var records =  ddSource.dragData.selections;
									Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
									firstGridPayment.store.add(records);
									firstGridPayment.store.sort('KD_PAY', 'ASC');
									return true
							}
					});
				}
			},
				viewConfig: 
					{
							forceFit: true
					}
		});
        return firstGridPayment;
	},
	seconGridPayment : function(){
		//var secondGridStoreLapPenerimaanPaymentLapPenerimaan;
		var secondGridPayment;
		secondGridStoreLapPenerimaanPaymentLapPenerimaan = new Ext.data.JsonStore({
            fields : fieldsPayment,
            root   : 'records'
        });

        // create the destination Grid
            secondGridPayment = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroupPayment',
					store            : secondGridStoreLapPenerimaanPaymentLapPenerimaan,
					columns          : colsPayment,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 130,
					stripeRows       : true,
					autoExpandColumn : 'KD_PAY',
					title            : 'Payment yang dipilih',
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGridPayment.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroupPayment',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGridPayment.store.add(records);
											secondGridPayment.store.sort('KD_PAY', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGridPayment;
	},
	getCombo3:function(){
		var $this=this;
		var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    	$this.DataStore.combo3 = new WebApp.DataStore({fields: Field_poli_viDaftar});

		$this.DataStore.combo3.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER "
            }
        });
    	$this.ComboBox.combo3 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:150,
			hidden:true,
			store: $this.DataStore.combo3,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
//			value: 0,
			listeners:{
				'select': function(a,b,c){
//					selectSetAsuransi=b.data.KD_CUSTOMER ;
//					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		});
		return $this.ComboBox.combo3;
	},
	init:function(){
		var $this=this;
		$this.Window.main=new Ext.Window({
			width: 600,
			height:400,
			modal:true,
			title:'Laporan Kunjungan Pasien Baru Lama',
			layout:'fit',
			items:[
				new Ext.Panel({
					bodyStyle:'padding: 6px',
					layout:'form',
					border:false,
           			autoScroll: true,
					fbar:[
						new Ext.Button({
							text:'Preview',
							handler:function(){
								$this.initPrint()
							}
						}),
						new Ext.Button({
							text:'Print',
							handler:function(){
								$this.DirectPrint()
							}
						}),
						new Ext.Button({
							text:'Batal',
							handler:function(){
								$this.Window.main.close();
							}
						})
					],
					items:[
						{
							xtype : 'fieldset',
							title : 'Poliklinik',
							layout:'column',
							border:true,
							height: 170,
							// bodyStyle:'margin-top: 2px',
							items:[
								{
									border:false,
									columnWidth:.45,
									// bodyStyle:'margin-top: 2px',
									items:[
										$this.firstGridPenerimaan(),
									]
								},
								{
									border:false,
									columnWidth:.1,
									bodyStyle:'margin-top: 2px',
								},
								{
									border:false,
									columnWidth:.45,
									// bodyStyle:'margin-top: 2px; align:right;',
									items:[
										$this.seconGridPenerimaan(),
									]
								}
							]
						},{
							// xtype : 'fieldset',
							// title : '',
							layout: 'absolute',
							border: true,
							height:30,
							items:[
								{	
									x:20,
									y:4,
									xtype: 'checkbox',
									id: 'CekLapPilihSemuaRWJPenerimaan',
									hideLabel:false,
									boxLabel: 'Pilih Semua Unit',
									checked: false,
									listeners: 
									{
										check: function()
										{
										   if(Ext.getCmp('CekLapPilihSemuaRWJPenerimaan').getValue()===true)
											{
												firstGrid.getSelectionModel().selectAll();
											}
											else
											{
												firstGrid.getSelectionModel().clearSelections();
											}
										}
								   }
								},
								{
									border:false,
									columnWidth:.2,
									bodyStyle:'margin-top: 2px',
								},
								{	
									x:400,
									y:4,
									xtype: 'checkbox',
									id: 'CetakExcelRWJPenerimaan',
									hideLabel:false,
									boxLabel: 'Cetak Excel',
									checked: false,
									listeners: 
									{
										
								   }
								},
							]
						},
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:60,
							items:[
								{
									xtype : 'fieldset',
									title : 'Status',
									layout: 'column',
									width : 260,
									//height: 60,
									border: true,
									bodyStyle:'padding: 2px 0px  7px 15px',
									items:[
									// DISINI
										$this.getComboStatus()
									]
								},{layout: 'column',width : 15, bodyStyle:'margin: 5px',border: false},{
									xtype : 'fieldset',
									title : 'Periode dan Status',
									layout: 'column',
									width : 320,
									bodyStyle:'padding: 2px 0px  7px 15px',
									border: true,
									items:[
										
										$this.DateField.startDate=new Ext.form.DateField({
											value: new Date(),
											width: '100%',
											format:'d/M/Y'
										}),
										{
											xtype:'displayfield',
											width: 30,
											value:'&nbsp;s/d&nbsp;'
										},
										$this.DateField.endDate=new Ext.form.DateField({
											value: new Date(),
											format:'d/M/Y'
										})
										
									]
								}
							]
						},

						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:60,
							items:[
								{
									xtype : 'fieldset',
									title : 'Order By',
									layout: 'column',
									width : 260,
									//height: 60,
									border: true,
									bodyStyle:'padding: 2px 0px  7px 15px',
									items:[
									// DISINI
										$this.getComboOrderBy()
									]
								},
							]
						},

						/*{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							items:[
									{
										xtype : 'fieldset',
										title : 'Kelompok pasien',
										layout: 'column',
										border: true,
										bodyStyle:'padding: 3px 0px  2px 10px',
										width:310,
										height: 67,
										items:[
											$this.ComboBox.kelPasien1=new Ext.form.ComboBox({
												triggerAction: 'all',
												lazyRender:true,
												mode: 'local',
												width: 100,
												selectOnFocus:true,
												forceSelection: true,
												emptyText:'Silahkan Pilih...',
												fieldLabel: 'Pendaftaran Per Shift ',
												store: new Ext.data.ArrayStore({
													id: 0,
													fields:[
															'Id',
															'displayText'
													],
													data: [[-1, 'Semua'],[0, 'Perseorangan'],[1, 'Perusahaan'], [2, 'Asuransi']]
												}),
												valueField: 'Id',
												displayField: 'displayText',
												value:'Semua',
												listeners:{
													'select': function(a,b,c){
															$this.vars.comboSelect=b.data.displayText;
															$this.comboOnSelect(b.data.Id);
													}
												}
											}),
											{
												xtype:'displayfield',
												width: 10,
												value:'&nbsp;'
											},
											$this.getCombo0(),
											$this.getCombo1(),
											$this.getCombo2(),
											$this.getCombo3()
										]
									},{layout: 'column',width : 15,
									bodyStyle:'margin: 5px',border: false},
									{
										xtype : 'fieldset',
										title : 'Shift',
										layout:'column',
										border: true,
										bodyStyle:'padding: 2px 0px  2px 10px',
										width : 270,
										//height: 60,
										items:[
											$this.CheckboxGroup.shift=new Ext.form.CheckboxGroup({
												xtype: 'checkboxgroup',
												items: [
													{boxLabel: 'Semua',checked:true,name: 'Shift_All_LabRegis',id : 'Shift_All_LabRegis',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_LabRegis').setValue(true);Ext.getCmp('Shift_2_LabRegis').setValue(true);Ext.getCmp('Shift_3_LabRegis').setValue(true);Ext.getCmp('Shift_1_LabRegis').disable();Ext.getCmp('Shift_2_LabRegis').disable();Ext.getCmp('Shift_3_LabRegis').disable();}else{Ext.getCmp('Shift_1_LabRegis').setValue(false);Ext.getCmp('Shift_2_LabRegis').setValue(false);Ext.getCmp('Shift_3_LabRegis').setValue(false);Ext.getCmp('Shift_1_LabRegis').enable();Ext.getCmp('Shift_2_LabRegis').enable();Ext.getCmp('Shift_3_LabRegis').enable();}}},
													{boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1_LabRegis',id : 'Shift_1_LabRegis'},
													{boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2_LabRegis',id : 'Shift_2_LabRegis'},
													{boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3_LabRegis',id : 'Shift_3_LabRegis'}
												]
											})
										]
									}
							]
						},*/
						
						/*{
							xtype : 'fieldset',
							title : 'Payment',
							layout:'column',
							border:true,
							height: 170,
							// bodyStyle:'margin-top: 2px',
							items:[
								{
									border:false,
									columnWidth:.45,
									// bodyStyle:'margin-top: 2px',
									items:[
										$this.firstGridPayment(),
									]
								},
								{
									border:false,
									columnWidth:.1,
									bodyStyle:'margin-top: 2px',
								},
								{
									border:false,
									columnWidth:.45,
									// bodyStyle:'margin-top: 2px; align:right;',
									items:[
										$this.seconGridPayment(),
									]
								}
							]
						},*/
						/*{
							layout: 'absolute',
							border: true,
							height:	30,
							items:[
								{								   
									x:20,
									y:3,
									xtype: 'checkbox',
									id: 'CekLapPilihSemuaPaymentRWJPenerimaan',
									hideLabel:false,
									boxLabel: 'Pilih Semua Payment',
									checked: false,
									listeners: 
									{
										check: function()
										{
										   if(Ext.getCmp('CekLapPilihSemuaPaymentRWJPenerimaan').getValue()===true)
											{
												firstGridPayment.getSelectionModel().selectAll();
											}
											else
											{
												firstGridPayment.getSelectionModel().clearSelections();
											}
										}
								   }
								},
								
							]
						}*/
					]
				})
			]
		}).show();
	}
};
DlgLapRWJPenerimaan.init();
