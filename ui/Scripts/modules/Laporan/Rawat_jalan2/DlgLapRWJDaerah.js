
var dsRWJDaerah;
var selectRWJDaerah;
var selectNamaRWJDaerah;
var now = new Date();
var selectSetDaerah;
var frmDlgRWJDaerah;
var varLapRWJDaerah= ShowFormLapRWJDaerah();

function ShowFormLapRWJDaerah()
{
    frmDlgRWJDaerah= fnDlgRWJDaerah();
    frmDlgRWJDaerah.show();
};

function fnDlgRWJDaerah()
{
    var winRWJDaerahReport = new Ext.Window
    (
        {
            id: 'winRWJDaerahReport',
            title: 'Laporan Rawat Jalan Pasien Per Daerah',
            closeAction: 'destroy',
            width:500,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJDaerah()]

        }
    );

    return winRWJDaerahReport;
};


function ItemDlgRWJDaerah()
{
    var PnlLapRWJDaerah = new Ext.Panel
    (
        {
            id: 'PnlLapRWJDaerah',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJDaerah_Tanggal(),getItemLapRWJDaerah_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWJDaerah',
                            handler: function()
                            {
                                if (ValidasiReportRWJDaerah() === 1)
                                {
                                        var criteria = GetCriteriaRWJDaerah();
                                        frmDlgRWJDaerah.close();
                                        loadlaporanRWJ('0', 'rep010203', criteria);
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWJDaerah',
                            handler: function()
                            {
                                    frmDlgRWJDaerah.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWJDaerah;
};

function GetCriteriaRWJDaerah()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJDaerah').dom.value !== '')
	{
		strKriteria = getnewformatdate(Ext.get('dtpTglAwalLapRWJDaerah').dom.value);
	};
	if (selectSetDaerah !== undefined)
	{
		strKriteria += '##@@##' + selectSetDaerah;
	};

	return strKriteria;
};


function ValidasiReportRWJDaerah()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWJDaerah').dom.value === '') || (Ext.get('cboDaerah').dom.value === '' || 
            Ext.get('cboDaerah').dom.value === 'Silahkan Pilih...'))
    {
            if(Ext.get('dtpTglAwalLapRWJDaerah').dom.value === '')
            {
                    ShowPesanWarningRWJDaerahReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('cboDaerah').dom.value === '' || Ext.get('cboDaerah').dom.value === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningRWJDaerahReport(nmGetValidasiKosong('Wilayah'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRWJDaerah()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJDaerah').dom.value > Ext.get('dtpTglAkhirLapRWJDaerah').dom.value)
    {
        ShowPesanWarningRWJDaerahReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJDaerahReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJDaerah_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                    mComboUnitLapRWJDaerah()
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJDaerah_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: 0.45,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 85,
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmPeriodeDlgRpt + ' ',
                    id: 'dtpTglAwalLapRWJDaerah',
                    format: 'M-Y',
                    value:now,
                    width:80
                }
            ]
        }
        ]
    }
    return items;
};


function mComboUnitLapRWJDaerah()
{
     var cboDaerah = new Ext.form.ComboBox
	(
		{
			id:'cboDaerah',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Wilayah ',
			width:80,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Prop'],[2, 'Kab'],[3, 'Kec'],[4, 'Kel']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetDaerah,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetDaerah=b.data.displayText ;
				}
			}
		}
	);
	return cboDaerah;
};