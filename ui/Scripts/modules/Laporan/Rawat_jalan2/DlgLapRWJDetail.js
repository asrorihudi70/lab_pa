
var dsRWJPerLaporandetail;
var selectNamaRWJPerLaporandetail;
var tanggal = new Date();
var now = tanggal.format('Y-m-d');
var selectCount_viInformasiUnitdokter=50;
var icons_viInformasiUnitdokter="Gaji";
var dataSource_unit;
var selectSetJenisPasien='Semua Pasien';
var frmDlgRWJPerLaporandetail;
var secondGridStore;
var varLapRWJPerLaporandetail= ShowFormLapRWJPerLaporandetail();
var selectSetUmum;
var firstGrid;
var secondGrid;

var CurrentData_viDaftarRWJ =
{
	data: Object,
	details: Array,
	row: 0
};

function ShowFormLapRWJPerLaporandetail()
{
    frmDlgRWJPerLaporandetail= fnDlgRWJPerLaporandetail();
    frmDlgRWJPerLaporandetail.show();
};

function fnDlgRWJPerLaporandetail()
{
    var winRWJPerLaporandetailReport = new Ext.Window
    (
        {
            id: 'winRWJPerLaporandetailReport',
            title: 'Laporan Rawat Jalan',
            closeAction: 'destroy',
            width:620,
            height: 350,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [dataGrid_viInformasiUnit()],
            listeners:
        {
            activate: function()
            {

            }
        }

        }
    );

    return winRWJPerLaporandetailReport;
};


function ItemDlgRWJPerLaporandetail()
{
    var PnlLapRWJPerLaporandetail = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPerLaporandetail',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '99%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                dataGrid_viInformasiUnit(),

            ]
        }
        
    );

    return PnlLapRWJPerLaporandetail;
};

function GetCriteriaRWJPerLaporandetail()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJPerLaporandetail').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWJPerLaporandetail').getValue();
	};
        if (Ext.get('dtpTglAkhirLapRWJLaporandetail').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRWJLaporandetail').getValue();
	};
        if (Ext.get('cboJenisPasien').dom.value !== '' || Ext.get('cboJenisPasien').dom.value !== 'Silahkan Pilih...')
	{

				if (selectSetJenisPasien==='Semua Pasien')
				{
  
                strKriteria += '##@@##' + 'kosong';
				}
				else if (selectSetJenisPasien==='Pasien Baru')
				{
  
                strKriteria += '##@@##' + 'true';
				}else if (selectSetJenisPasien==='Pasien Lama')
				{
  
                strKriteria += '##@@##' + 'false';
				};
	};
	return strKriteria;
};

function ValidasiTanggalReportRWJPerLaporandetail()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJPerLaporandetail').dom.value > Ext.get('dtpTglAkhirLapRWJPerLaporandetail').dom.value)
    {
        ShowPesanWarningRWJPerLaporandetailReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJPerLaporandetailReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function mComboJenisPasien()
{
    var cboJenisPasien = new Ext.form.ComboBox
	(
		{
			id:'cboJenisPasien',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Jenis Pasien ',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua Pasien'],[2, 'Pasien Baru'],[3, 'Pasien Lama']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
//			value:selectSetJenisPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetJenisPasien=b.data.displayText ;
				}
			}
		}
	);
	return cboJenisPasien;
};

function dataGrid_viInformasiUnit(mod_id)
{
    var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    datarefresh_viInformasiUnit()

    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
	];


	// declare the source Grid
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 200,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'KD_UNIT',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA_UNIT',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),
				listeners : {
                    afterrender : function(comp) {
                    var firstGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
   
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStore,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 200,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_UNIT',
                    title            : 'Unit',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'secondGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid.store.add(records);
                                    secondGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

	
	var FrmTabs_viInformasiUnit = new Ext.Panel
        (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
            height       : 250,
			
//		    title:  'Pilih Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[firstGrid
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid
						
					]
				},
                                {
                                        
                                            columnWidth: .30,
                                            layout: 'form',
                                            border: false,
                                            labelAlign: 'right',
                                            labelWidth: 77,
                                            items:
                                            [
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: nmPeriodeDlgRpt + ' ',
                                                    id: 'dtpTglAwalLapRWJPerLaporandetail',
                                                    format: 'd-M-Y',
                                                    value:now,
                                                    anchor: '95%'
                                                },

                                            ]
                                        },
                                        {
                                            columnWidth: .30,
                                            layout: 'form',
                                            border: false,
                                            labelWidth: 30,
                                            items:
                                            [
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: nmSd + ' ',
                                                    id: 'dtpTglAkhirLapRWJLaporandetail',
                                                    format: 'd-M-Y',
                                                    value:now,
                                                    anchor: '80%'
                                                }
                                            ]
                                        },
                                        {
                                            columnWidth: .40,
                                            layout: 'form',
                                            border: false,
                                            labelAlign: 'right',
                                            labelWidth: 75,
                                            items:
                                            [
                                            
                                                mComboJenisPasien()
                                            ]

                                        },
                                       
                                {
                                columnWidth: .99,
                                layout: 'hBox',
                                border: false,
                                defaults: { margins: '0 5 0 0' },
                                style:{'margin-left':'350px','margin-top':'30px'},
                                anchor: '100%',
                                layoutConfig:
                                {
                                    padding: '3',
                                    pack: 'end',
                                    align: 'middle'
                                },
                                items:
                                [
                                    {
                                       xtype: 'checkbox',
                                       id: 'CekLapPilihSemuaRWJDetail',
                                       hideLabel:false,
                                       boxLabel: 'Pilih Semua',
                                       checked: false,
                                       listeners: 
                                       {
                                            check: function()
                                            {
                                               if(Ext.getCmp('CekLapPilihSemuaRWJDetail').getValue()===true)
                                                {
                                                     firstGrid.getSelectionModel().selectAll();
                                                }
                                                else
                                                {
                                                    firstGrid.getSelectionModel().clearSelections();
                                                }
                                            }
                                       }
                                    },
                                    {
                                        xtype: 'button',
                                        text: nmBtnOK,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnOkLapRWJPerLaporandetail',
                                        handler: function()
                                        {
                                            var sendDataArray = [];
                                            secondGridStore.each(function(record){
                                            var recordArray = [record.get("KD_UNIT")];
                                            sendDataArray.push(recordArray);
											
                                            });
									
											
                                           if (sendDataArray.length === 0)
											{
                                                   ShowPesanWarningRWJPerLaporandetailReport('Isi kriteria unit dengan drag and drop','Laporan')
                                                 
										    }else{
											 var criteria = GetCriteriaRWJPerLaporandetail();
                                             criteria += '##@@##' + sendDataArray;
                                             frmDlgRWJPerLaporandetail.close();
                                             loadlaporanRWJ('0', 'rep010201', criteria);
											};
                                        }
                                    },
                                    {
                                        xtype: 'button',
                                        text: nmBtnCancel ,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnCancelLapRWJPerLaporandetail',
                                        handler: function()
                                        {
                                                frmDlgRWJPerLaporandetail.close();
                                        }
                                    }
                                    ]
                                }
			],

		
    }
    )
    
        
        
    
    
    // datarefresh_viInformasiUnitdokter();
    return FrmTabs_viInformasiUnit;
}

function DataInputKriteria()
{
    var FrmTabs_DataInputKriteria = new Ext.Panel
        (
		{
		    id: FrmTabs_DataInputKriteria,
		    closable: true,
		    region: 'center',
		    layout: 'column',
                    height       : 100,
		    title:  'Dokter Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[]
				}
			]
		
    }
    )
    
    return FrmTabs_DataInputKriteria;
}
        

function datarefresh_viInformasiUnit()
{
    dataSource_unit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )
    //alert("refersh")
}

    

	
