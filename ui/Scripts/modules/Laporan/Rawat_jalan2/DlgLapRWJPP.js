
var dsRWJPP;
var selectRWJPP;
var selectNamaRWJPP;
var now = new Date();

var frmDlgRWJPP;
var varLapRWJPP= ShowFormLapRWJPP();

function ShowFormLapRWJPP()
{
    frmDlgRWJPP= fnDlgRWJPP();
    frmDlgRWJPP.show();
};

function fnDlgRWJPP()
{
    var winRWJPPReport = new Ext.Window
    (
        {
            id: 'winRWJPPReport',
            title: 'Laporan Rawat Jalan Pasien Per Daerah',
            closeAction: 'destroy',
            width:500,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJPP()]

        }
    );

    return winRWJPPReport;
};


function ItemDlgRWJPP()
{
    var PnlLapRWJPP = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPP',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJPP_Tanggal(),getItemLapRWJPP_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWJPP',
                            handler: function()
                            {
                                if (ValidasiReportRWJPP() === 1)
                                {
                                    //if (ValidasiTanggalReportRWJPP() === 1)
                                    //{
                                        var criteria = GetCriteriaRWJPP();
                                        frmDlgRWJPP.close();
                                        ShowReport('0', 1010203, criteria);
//                                        window.open("http://localhost/Simrs/base/tmp/1421630699Tmp.pdf");
                                    //};
                                    //alert(tmp);
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWJPP',
                            handler: function()
                            {
                                    frmDlgRWJPP.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWJPP;
};

function GetCriteriaRWJPP()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJPP').dom.value != '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWJPP').dom.value;
	};

	if (Ext.get('dtpTglAkhirLapRWJPP').dom.value != '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRWJPP').dom.value;
	};

	if (selectRWJPP != undefined)
	{
		strKriteria += '##@@##' + selectRWJPP;
		strKriteria += '##@@##' + selectNamaRWJPP ;
	};

	return strKriteria;
};


function ValidasiReportRWJPP()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWJPP').dom.value === '') || (Ext.get('dtpTglAkhirLapRWJPP').dom.value === '') || (selectRWJPP === undefined) || (Ext.get('comboUnitLapRWJPP').dom.value === ''))
    {
            if(Ext.get('dtpTglAwalLapRWJPP').dom.value === '')
            {
                    ShowPesanWarningRWJPPReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('dtpTglAkhirLapRWJPP').dom.value === '')
            {
                    ShowPesanWarningRWJPPReport(nmGetValidasiKosong(nmFinishDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('comboUnitLapRWJPP').dom.value === '' )
            {
                    ShowPesanWarningRWJPPReport(nmGetValidasiKosong(nmDeptDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRWJPP()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJPP').dom.value > Ext.get('dtpTglAkhirLapRWJPP').dom.value)
    {
        ShowPesanWarningRWJPPReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJPPReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJPP_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:1,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                    mComboUnitLapRWJPP()
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJPP_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: 0.45,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 85,
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmPeriodeDlgRpt + ' ',
                    id: 'dtpTglAwalLapRWJPP',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        },
        {
            columnWidth: 0.48,
            layout: 'form',
            border: false,
                    labelWidth: 25,
            labelAlign: 'right',
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmSd + ' ',
                    id: 'dtpTglAkhirLapRWJPP',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        }
        ]
    }
    return items;
};


function mComboUnitLapRWJPP()
{
    var dataSource_All = new Ext.data.Store({
	reader: new Ext.data.JsonReader
		({
			//fields: ['code',  'NoFaktur',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			fields: ['KD_UNIT', 'NAMA_UNIT']
		}),
	data:
		[
			{ KD_UNIT:'xxx', NAMA_UNIT:'Semua Unit'},
                        { KD_UNIT:'208', NAMA_UNIT:'Anak'},
                        { KD_UNIT:'209', NAMA_UNIT:'Bedah'},
                        { KD_UNIT:'215', NAMA_UNIT:'DM'},
                        { KD_UNIT:'202', NAMA_UNIT:'Gigi'},
                        { KD_UNIT:'203', NAMA_UNIT:'Gizi'},
                        { KD_UNIT:'206', NAMA_UNIT:'Interna'},
                        { KD_UNIT:'216', NAMA_UNIT:'Jantung'},
                        { KD_UNIT:'214', NAMA_UNIT:'Jiwa'},
                        { KD_UNIT:'207', NAMA_UNIT:'Kandungan'},
                        { KD_UNIT:'213', NAMA_UNIT:'KIA'},
                        { KD_UNIT:'212', NAMA_UNIT:'Kulit Kelamin'},
                        { KD_UNIT:'211', NAMA_UNIT:'Mata'},
                        { KD_UNIT:'204', NAMA_UNIT:'Syaraf'},
                        { KD_UNIT:'210', NAMA_UNIT:'THT'},
                        { KD_UNIT:'201', NAMA_UNIT:'Umum'},
                        { KD_UNIT:'205', NAMA_UNIT:'VCT'},
		]
	});
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    dsRWJPP = new WebApp.DataStore({ fields: Field });

    dsRWJPP.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    );
    
  var comboUnitLapRWJPP = new Ext.form.ComboBox
    (
        {
            id:'comboUnitLapRWJPP',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Poliklinik ',
            align:'Right',
            anchor:'99%',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store:dataSource_All,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectRWJPP=b.data.KD_UNIT ;
                    selectNamaRWJPP=b.data.NAMA_UNIT ;
                }
            }
        }
    );

//    dsRWJPP.load
//    (
//        {
//            params:
//            {
//                Skip: 0,
//                Take: 1000,
//                //Sort: 'DEPT_ID',
//                Sort: 'NAMA_UNIT',
//                Sortdir: 'ASC',
//                target:'ViewSetupUnit',
//                param: ''
//            }
//        }
//    );
    return comboUnitLapRWJPP;
};