
var dsRWJPerRujukan;
var selectNamaRWJPerRujukan;
var now = new Date();
var selectCount_viInformasiUnitdokter=50;
var icons_viInformasiUnitdokter="Gaji";
var dataSource_unit;
var selectSetJenisPasien;
var frmDlgRWJPerRujukan;
var secondGridStore;
var varLapRWJPerRujukan= ShowFormLapRWJPerRujukan();
var selectSetUmum;
var firstGrid;
var secondGrid;

var CurrentData_viDaftarRWJ =
{
	data: Object,
	details: Array,
	row: 0
};

function ShowFormLapRWJPerRujukan()
{
    frmDlgRWJPerRujukan= fnDlgRWJPerRujukan();
    frmDlgRWJPerRujukan.show();
};

function fnDlgRWJPerRujukan()
{
    var winRWJPerRujukanReport = new Ext.Window
    (
        {
            id: 'winRWJPerRujukanReport',
            title: 'Laporan Rawat Jalan',
            closeAction: 'destroy',
            width:500,
            height: 500,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [dataGrid_viInformasiUnitdokter()],
            listeners:
        {
            activate: function()
            {
//                Ext.getCmp('cboJenisPasien').show();
//                Ext.getCmp('cboAsuransi').hide();
//                Ext.getCmp('cboPerusahaanRequestEntry').hide();
//                Ext.getCmp('cboUmum').hide();
//                Ext.getCmp('dtpTglAkhirLapRWJKIUP').hide();
            }
        }

        }
    );

    return winRWJPerRujukanReport;
};


function ItemDlgRWJPerRujukan()
{
    var PnlLapRWJPerRujukan = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPerRujukan',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '99%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                dataGrid_viInformasiUnitdokter()
            ]
        }
        
    );

    return PnlLapRWJPerRujukan;
};

function GetCriteriaRWJPerRujukan()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJPerRujukan').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWJPerRujukan').getValue();
	};
        if (Ext.get('dtpTglAkhirLapRWJRujukan').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRWJRujukan').getValue();
	};
        if (Ext.getCmp('cboRujukanRequestEntry').getValue() !== '' || Ext.getCmp('cboRujukanRequestEntry').getValue() !== 'Pilih Rujukan...')
	{
		strKriteria += '##@@##' + 'Rujukan';
                strKriteria += '##@@##' + Ext.getCmp('cboRujukanRequestEntry').getValue();
	};
        if (Ext.getCmp('cboRujukanDariRequestEntry').getValue() !== '' || Ext.getCmp('cboRujukanDariRequestEntry').getValue() !== 'Pilih Rujukan...')
	{
                strKriteria += '##@@##' + 'rujukandari';	
                strKriteria += '##@@##' + Ext.getCmp('cboRujukanDariRequestEntry').getValue();
	};
        if (Ext.getCmp('cboJenisPasien').getValue() !== '' || Ext.getCmp('cboJenisPasien').getValue() !== 'Silahkan Pilih...')
	{
                strKriteria += '##@@##' + 'Jenis';	
                strKriteria += '##@@##' + Ext.getCmp('cboJenisPasien').getValue();
	};
        
        
	return strKriteria;
};

function ValidasiTanggalReportRWJPerRujukan()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJPerRujukan').dom.value > Ext.get('dtpTglAkhirLapRWJPerRujukan').dom.value)
    {
        ShowPesanWarningRWJPerRujukanReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJPerRujukanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function mComboJenisPasien()
{
    var cboJenisPasien = new Ext.form.ComboBox
	(
		{
			id:'cboJenisPasien',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Jenis Pasien ',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua Pasien'],[2, 'Pasien Baru'],[3, 'Pasien Lama']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
//			value:selectSetJenisPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetJenisPasien=b.data.displayText ;
				}
			}
		}
	);
	return cboJenisPasien;
};

function mcomborujukandari()
{
    var Field = ['cara_penerimaan','penerimaan'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewComboRujukanDariUnion',
                param: ""
            }
        }
    )

    var cboRujukanDariRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanDariRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Rujukan...',
            fieldLabel: 'Rujukan Dari ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'cara_penerimaan',
            displayField: 'penerimaan',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   loaddatastorerujukan(b.data.cara_penerimaan);
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }


		}
        }
    )

    return cboRujukanDariRequestEntry;
}

function loaddatastorerujukan(cara_penerimaan)
{
          dsRujukanRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    Sort: 'rujukan',
			    Sortdir: 'ASC',
			    target: 'ViewComboRujukanUnion',
			    param: cara_penerimaan
			}
                    }
                )
}

function mComboRujukan()
{
    var Field = ['kd_rujukan','rujukan'];

    dsRujukanRequestEntry = new WebApp.DataStore({fields: Field});

	console.log(dsRujukanRequestEntry);
    var cboRujukanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboRujukanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Rujukan...',
		    fieldLabel: '',
		    align: 'Right',       
//		    anchor:'60%',
		    store: dsRujukanRequestEntry,
		    valueField: 'kd_rujukan',
		    displayField: 'rujukan',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboRujukanRequestEntry;
};

function dataGrid_viInformasiUnitdokter(mod_id)
{
    var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unit = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    datarefresh_viInformasiUnit()

    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
	];


	// declare the source Grid
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unit,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 200,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'KD_UNIT',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA_UNIT',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),
                    listeners : {
                    afterrender : function(comp) {
                    var firstGridDropTargetEl =  firstGrid.getView().scroller.dom;
                    var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStore,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 200,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_UNIT',
                    title            : 'Unit',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'secondGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid.store.add(records);
                                    secondGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

	
	var FrmTabs_viInformasiUnitdokter = new Ext.Panel
        (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
                    height       : 400,
//		    title:  'Pilih Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[firstGrid
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid
						
					]
				},
                                {
                                        
                                            columnWidth: .48,
                                            layout: 'form',
                                            border: false,
                                            labelAlign: 'right',
                                            labelWidth: 100,
                                            items:
                                            [
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: nmPeriodeDlgRpt + ' ',
                                                    id: 'dtpTglAwalLapRWJPerRujukan',
                                                    format: 'd/M/Y',
                                                    value:now,
                                                    anchor: '95%'
                                                },

                                            ]
                                        },
                                        {
                                            columnWidth: .32,
                                            layout: 'form',
                                            border: false,
                                            labelWidth: 30,
                                            items:
                                            [
                                                {
                                                    xtype: 'datefield',
                                                    fieldLabel: nmSd + ' ',
                                                    id: 'dtpTglAkhirLapRWJRujukan',
                                                    format: 'd/M/Y',
                                                    value:now,
                                                    anchor: '95%'
                                                }
                                            ]
                                        },
                                        {
                                            columnWidth: .48,
                                            layout: 'form',
                                            border: false,
                                            labelAlign: 'right',
                                            labelWidth: 100,
                                            items:
                                            [
                                                mcomborujukandari(),
                                                mComboJenisPasien()
                                            ]

                                        },
                                        {
                                            columnWidth: .32,
                                            layout: 'form',
                                            border: false,
                                            labelWidth: 30,
                                            items:
                                            [
                                                 mComboRujukan()
                                            ]

                                        
                                    },
                                {
                                columnWidth: .90,
                                layout: 'hBox',
                                border: false,
                                defaults: { margins: '0 5 0 0' },
                                style:{'margin-left':'30px','margin-top':'5px'},
                                anchor: '94%',
                                layoutConfig:
                                {
                                    padding: '3',
                                    pack: 'end',
                                    align: 'middle'
                                },
                                items:
                                [
                                    {
                                       xtype: 'checkbox',
                                       id: 'CekLapPilihSemuaRWJPerPerujuk',
                                       hideLabel:false,
                                       boxLabel: 'Pilih Semua',
                                       checked: false,
                                       listeners: 
                                       {
                                            check: function()
                                            {
                                               if(Ext.getCmp('CekLapPilihSemuaRWJPerPerujuk').getValue()===true)
                                                {
                                                     firstGrid.getSelectionModel().selectAll();
                                                }
                                                else
                                                {
                                                    firstGrid.getSelectionModel().clearSelections();
                                                }
                                            }
                                       }
                                    },
                                    {
                                        xtype: 'button',
                                        text: nmBtnOK,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnOkLapRWJPerRujukan',
                                        handler: function()
                                        {
                                            var sendDataArray = [];
                                            secondGridStore.each(function(record){
                                            var recordArray = [record.get("KD_UNIT")];
                                            sendDataArray.push(recordArray);
                                            });
											//console.log(ValidasiReportRWJPerRujukan());
                                            if (ValidasiReportRWJPerRujukan() === 1)
											{
                                                    var criteria = GetCriteriaRWJPerRujukan();
                                                    criteria += '##@@##' + sendDataArray;
                                                    frmDlgRWJPerRujukan.close();
                                                    loadlaporanRWJ('0', 'ref010208', criteria);
                                                    
                                          } 
                                        }
                                    },
                                    {
                                        xtype: 'button',
                                        text: nmBtnCancel ,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnCancelLapRWJPerRujukan',
                                        handler: function()
                                        {
                                                frmDlgRWJPerRujukan.close();
                                        }
                                    }
                                    ]
                                }
			],
//                        bbar    : [
//			'->', // Fill
//			{
//				text    : 'Reset',
//				handler : function() {
//					//refresh source grid
//					 datarefresh_viInformasiUnit()
//
//					//purge destination grid
//					secondGridStore.removeAll();
//				}
//			}
//		]
		
    }
    )
    
        
        
    
    
    // datarefresh_viInformasiUnitdokter();
    return FrmTabs_viInformasiUnitdokter;
}
function ValidasiReportRWJPerRujukan()
{
    var x=1;
    if (Ext.getCmp('cboRujukanDariRequestEntry').getValue() === '')
    {
		ShowPesanWarningRWJPerRujukanReport('Rujukan belum terisi', 'Laporan Rawat Jalan');
		x = 0;
    }
	/* else
	{
		x = 1;
	} */else if (Ext.getCmp('cboRujukanRequestEntry').getValue() === '')
    {
		ShowPesanWarningRWJPerRujukanReport('Sub Rujukan belum terisi', 'Laporan Rawat Jalan');
		x = 0;
    }
	else
	{
		x = 1;
	}
    return x;
}
function DataInputKriteria()
{
    var FrmTabs_DataInputKriteria = new Ext.Panel
        (
		{
		    id: FrmTabs_DataInputKriteria,
		    closable: true,
		    region: 'center',
		    layout: 'column',
                    height       : 100,
		    title:  'Dokter Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[]
				}
			]
		
    }
    )
    
    return FrmTabs_DataInputKriteria;
}
        

function datarefresh_viInformasiUnit()
{
    dataSource_unit.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )
    //alert("refersh")
}

    

	
