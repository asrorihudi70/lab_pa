
var dsRWJPerShift;
var selectNamaRWJPerShift;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRWJPerShift;
var varLapRWJPerShift= ShowFormLapRWJPerShift();
var selectSetUmum;
var selectSetkelpas;

function ShowFormLapRWJPerShift()
{
    frmDlgRWJPerShift= fnDlgRWJPerShift();
    frmDlgRWJPerShift.show();
};

function fnDlgRWJPerShift()
{
    var winRWJPerShiftReport = new Ext.Window
    (
        {
            id: 'winRWJPerShiftReport',
            title: 'Laporan Rawat Jalan',
            closeAction: 'destroy',
            width:500,
            height: 400,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJPerShift()],
            listeners:
        {
            activate: function()
            {
				Ext.getCmp('Shift_All').setValue(true);
                Ext.getCmp('cboPerseorangan').show();
                Ext.getCmp('cboAsuransi').hide();
                Ext.getCmp('cboPerusahaanRequestEntry').hide();
                Ext.getCmp('cboUmum').hide();
                Ext.getCmp('dtpTglAkhirLapRWJKIUP').hide();
                Ext.getCmp('cbounitRequestEntry').hide();
                Ext.getCmp('rb_pilihan1').setValue(true);
                Ext.getCmp('cbperseorangan').hide();
                Ext.getCmp('cbperusahaan').hide();
                Ext.getCmp('cbasuransi').hide();
            }
        }

        }
    );

    return winRWJPerShiftReport;
};


function ItemDlgRWJPerShift()
{
    var PnlLapRWJPerShift = new Ext.Panel
    (
        {
            id: 'PnlLapRWJPerShift',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWJPerShift_Dept(),
                getItemLapRWJPerShift_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRWJPerShift',
                            handler: function()
                            {
//                                if (ValidasiReportRWJPerShift() === 1)
//                                {
                                        var tmppilihan = getKodeReportRWJPerShift();
                                        var criteria = GetCriteriaRWJPerShift();
//                                        frmDlgRWJPerShift.close();
                                        loadlaporanRWJ('0', tmppilihan, criteria);
//                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRWJPerShift',
                            handler: function()
                            {
                                    frmDlgRWJPerShift.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWJPerShift;
};

function GetCriteriaRWJPerShift()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJPerShift').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWJPerShift').getValue();
	};
        if (Ext.getCmp('rb_pilihan1').getValue() === true)
        {
            strKriteria += '##@@##' + 'kd_unit';
            strKriteria += '##@@##' + 'NULL';
        }
        if (Ext.getCmp('rb_pilihan2').getValue() === true)
        {
            strKriteria += '##@@##' + 'kd_unit';
            strKriteria += '##@@##' + Ext.getCmp('cbounitRequestEntry').getValue();
        }
        if (Ext.getCmp('kelPasien').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
        }
        if (Ext.getCmp('kelPasien').getValue() === 'Umum')
        {
            selectSetPerseorangan = '0000000001';
            strKriteria += '##@@##' + 'Umum';
            strKriteria += '##@@##' + selectSetPerseorangan;
        }
        if (Ext.getCmp('kelPasien').getValue() === 'Perusahaan')
        {
            if(Ext.getCmp('cbperusahaan').getValue() === true)
            {
                strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Perusahaan';
                strKriteria += '##@@##' + selectsetperusahaan;
            }
        }
        if (Ext.getCmp('kelPasien').getValue() === 'Asuransi')
        {
            if(Ext.getCmp('cbasuransi').getValue() === true)
            {
                strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + 'NULL';
            }else{
                strKriteria += '##@@##' + 'Asuransi';
                strKriteria += '##@@##' + selectSetAsuransi;
            }
            
        };
        if (Ext.getCmp('Shift_All').getValue() === true)
        {
            strKriteria += '##@@##' + 'shift1';
            strKriteria += '##@@##' + 1;
            strKriteria += '##@@##' + 'shift2';
            strKriteria += '##@@##' + 2;
            strKriteria += '##@@##' + 'shift3';
            strKriteria += '##@@##' + 3;
        }else{
            if (Ext.getCmp('Shift_1').getValue() === true)
            {
                strKriteria += '##@@##' + 'shift1';
                strKriteria += '##@@##' + 1;
            }
            if (Ext.getCmp('Shift_2').getValue() === true)
            {
                strKriteria += '##@@##' + 'shift2';
                strKriteria += '##@@##' + 2;
            }
            if (Ext.getCmp('Shift_3').getValue() === true)
            {
                strKriteria += '##@@##' + 'shift3';
                strKriteria += '##@@##' + 3;
            }
        }
            
	return strKriteria;
};

function getKodeReportRWJPerShift()
{    var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihan').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihan').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiTanggalReportRWJPerShift()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJPerShift').dom.value > Ext.get('dtpTglAkhirLapRWJPerShift').dom.value)
    {
        ShowPesanWarningRWJPerShiftReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJPerShiftReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapRWJPerShift_Dept()
{
    var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: .90,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 150,
            items:
            [
                mComboPilihan(),
                {
                    xtype: 'datefield',
                    fieldLabel: nmPeriodeDlgRpt + ' ',
                    id: 'dtpTglAwalLapRWJPerShift',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                },
                {
                    xtype: 'radiogroup',
                    id: 'rbrujukan',
                    fieldLabel: 'Pilih Unit ',
                    items: [
                                {boxLabel: 'Semua Unit',
                                    name: 'rb_auto',
                                    id: 'rb_pilihan1',
                                    inputValue: 'K.Kd_Pasien',
                                    handler: function (field, value) {
                                    if (value === true)
                                    {
                                        Ext.getCmp('cbounitRequestEntry').hide();
                                    }else{
                                        Ext.getCmp('cbounitRequestEntry').show();
                                    }
                                 }
                                },
                                {boxLabel: 'Pilih Salah Satu',
                                    name: 'rb_auto',
                                    id: 'rb_pilihan2',
                                    inputValue: 'P.nama',
                                    handler: function (field, value) {
                                    if (value === true)
                                    {
                                        Ext.getCmp('cbounitRequestEntry').show();
                                    }else{
                                        Ext.getCmp('cbounitRequestEntry').hide();
                                    }
                                 }
                                }
                            ]
                },
                mCombounit(),
                {  
                    xtype: 'combo',
                    fieldLabel: 'Kelompok Pasien',
                    id: 'kelPasien',
                    editable: false,
                    store: new Ext.data.ArrayStore
                        (
                            {
                            id: 0,
                            fields:
                            [
                                'Id',
                                'displayText'
                            ],
                               data: [[1, 'Semua'],[2, 'Umum'], [3, 'Perusahaan'], [4, 'Asuransi']]
                            }
                        ),
                    displayField: 'displayText',
                    mode: 'local',
                    width: 100,
                    forceSelection: true,
                    triggerAction: 'all',
                    emptyText: 'Pilih Salah Satu...',
                    selectOnFocus: true,
                    anchor: '95%',
                    value:selectSetkelpas,
                    listeners:
                     {
                            'select': function(a, b, c)
                        {
                           Combo_Select(b.data.displayText);
                           selectSetkelpas=b.data.id;
                        }

                    }
                },
                {
                    columnWidth: .90,
                    layout: 'form',
                    border: false,
                    labelAlign: 'right',
                    labelWidth: 150,
                    defaultType: 'checkbox',

                    items:
                    [{
                            fieldLabel: '',
                            labelSeparator: '',
                            boxLabel: 'Semua Data',
                            name: 'cbperseorangan',
                            id : 'cbperseorangan',
                            handler: function (field, value) {
                               if (value === true)
                               {
                                   Ext.getCmp('cboPerseorangan').setValue('Semua');
                                   Ext.getCmp('cboPerseorangan').setDisabled(true);
                               }else{
                                   Ext.getCmp('cboPerseorangan').setValue();
                                   Ext.getCmp('cboPerseorangan').setDisabled(false);
                               }
                            }
                        }]
                        },
                mComboPerseorangan(),
                {
                    columnWidth: .90,
                    layout: 'form',
                    border: false,
                    labelAlign: 'right',
                    labelWidth: 150,
                    defaultType: 'checkbox',

                    items:
                    [{
                            fieldLabel: '',
                            labelSeparator: '',
                            boxLabel: 'Semua Data',
                            name: 'cbperusahaan',
                            id : 'cbperusahaan',
                            handler: function (field, value) {
                               if (value === true)
                               {
                                   Ext.getCmp('cboPerusahaanRequestEntry').setValue('Semua');
                                   Ext.getCmp('cboPerusahaanRequestEntry').setDisabled(true);
                               }else{
                                   Ext.getCmp('cboPerusahaanRequestEntry').setValue();
                                   Ext.getCmp('cboPerusahaanRequestEntry').setDisabled(false);
                               }
                            }
                        }]
                        },
                mComboPerusahaan(),
                {
                    columnWidth: .90,
                    layout: 'form',
                    border: false,
                    labelAlign: 'right',
                    labelWidth: 150,
                    defaultType: 'checkbox',

                    items:
                    [{
                            fieldLabel: '',
                            labelSeparator: '',
                            boxLabel: 'Semua Data',
                            name: 'cbasuransi',
                            id : 'cbasuransi',
                            handler: function (field, value) {
                               if (value === true)
                               {
                                   Ext.getCmp('cboAsuransi').setValue('Semua');
                                   Ext.getCmp('cboAsuransi').setDisabled(true);
                               }else{
                                   Ext.getCmp('cboAsuransi').setValue();
                                   Ext.getCmp('cboAsuransi').setDisabled(false);
                               }
                            }
                        }]
                        },
                mComboAsuransi(),
                mComboUmum(),
                {
                    columnWidth: 0.48,
                    layout: 'form',
                    border: false,
                    labelWidth: 25,
                    labelAlign: 'right',
                    items:
                    [
                        {
                            xtype: 'datefield',
                            fieldLabel: nmSd + ' ',
                            id: 'dtpTglAkhirLapRWJKIUP',
                            format: 'd/M/Y',
                            value:now,
                            width:105
                        }
                    ]
               }
            ]
        }
        ]
        
    };
    return items;
};

function getItemLapRWJPerShift_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: .90,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 150,
            defaultType: 'checkbox',
            
            items:
            [{
                    fieldLabel: 'Pilih Shift',
                    boxLabel: 'Shift 1',
                    name: 'Shift_1',
                    id : 'Shift_1'
                    
                }, {
                    fieldLabel: '',
                    labelSeparator: '',
                    boxLabel: 'Shift 2',
                    name: 'Shift_2',
                    id : 'Shift_2'
                }, {
                    fieldLabel: '',
                    labelSeparator: '',
                    boxLabel: 'Shift 3',
                    name: 'Shift_3',
                    id : 'Shift_3'
                },{
                    fieldLabel: '',
                    labelSeparator: '',
                    boxLabel: 'All Shift',
                    name: 'Shift_All',
                    id : 'Shift_All',
                    handler: function (field, value) {
						/* Ext.getCmp('Shift_All').setValue(true);
						Ext.getCmp('Shift_1').setValue(true);
                           Ext.getCmp('Shift_2').setValue(true);
                           Ext.getCmp('Shift_3').setValue(true);
                           Ext.getCmp('Shift_1').disable();
                           Ext.getCmp('Shift_2').disable();
                           Ext.getCmp('Shift_3').disable(); */
                       if (value === true)
                       {
                           Ext.getCmp('Shift_1').setValue(true);
                           Ext.getCmp('Shift_2').setValue(true);
                           Ext.getCmp('Shift_3').setValue(true);
                           Ext.getCmp('Shift_1').disable();
                           Ext.getCmp('Shift_2').disable();
                           Ext.getCmp('Shift_3').disable();
                       }else{
                           Ext.getCmp('Shift_1').setValue(false);
                           Ext.getCmp('Shift_2').setValue(false);
                           Ext.getCmp('Shift_3').setValue(false);
                           Ext.getCmp('Shift_1').enable();
                           Ext.getCmp('Shift_2').enable();
                           Ext.getCmp('Shift_3').enable();
                       }
                    }
                }]
            }
        ]
    };
    return items;
};

var selectSetPilihan;
function mComboPilihan()
{
    var cboPilihan = new Ext.form.ComboBox
	(
		{
			id:'cboPilihan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Pendaftaran Per Shift ',
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Detail'], [2, 'Sumary']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetPilihan=b.data.displayText ;
				}
			}
		}
	);
	return cboPilihan;
};

function mComboPerseorangan()
{
    var cboPerseorangan = new Ext.form.ComboBox
	(
		{
			id:'cboPerseorangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:50,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Umum']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetPerseorangan,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetPerseorangan=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseorangan;
};

function mComboUmum()
{
    var cboUmum = new Ext.form.ComboBox
	(
		{
			id:'cboUmum',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:50,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetUmum,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmum;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    ds_customer_viDaftar = new WebApp.DataStore({fields: Field});
    ref_combo_kelpas(1);
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
//		    anchor:'60%',
		    store: ds_customer_viDaftar,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
                    anchor: '95%',
                    value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
                                }
			}
                }
	);

    return cboPerusahaanRequestEntry;
};

var selectsetperusahaan;
var selectSetAsuransi;
function ref_combo_kelpas(jeniscus)
{
	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=~' + jeniscus + '~'
            }
        }
    );
	
	return ds_customer_viDaftar;
}
function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	
    var cboAsuransi = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransi',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Asuransi...',
                        fieldLabel: '',
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransi;
};

function mCombounit()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        )

    var cbounitRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cbounitRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                }
                    
                }
        }
    )

    return cbounitRequestEntry;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Umum")
   {    
        Ext.getCmp('cbperseorangan').enable();
        Ext.getCmp('cbperusahaan').disable();
        Ext.getCmp('cbasuransi').disable();
        Ext.getCmp('cbperseorangan').show();
        Ext.getCmp('cbperusahaan').hide();
        Ext.getCmp('cbasuransi').hide();
        Ext.getCmp('cbperseorangan').setValue(false);
        Ext.getCmp('cbperusahaan').setValue(false);
        Ext.getCmp('cbasuransi').setValue(false);
        
        Ext.getCmp('cboPerseorangan').show();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboUmum').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cbperseorangan').disable();
        Ext.getCmp('cbperusahaan').enable();
        Ext.getCmp('cbasuransi').disable();
        Ext.getCmp('cbperseorangan').hide();
        Ext.getCmp('cbperusahaan').show();
        Ext.getCmp('cbasuransi').hide();
        Ext.getCmp('cbperseorangan').setValue(false);
        Ext.getCmp('cbperusahaan').setValue(false);
        Ext.getCmp('cbasuransi').setValue(false);
        
        
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
		ref_combo_kelpas(1);
   }
   else if(value === "Asuransi")
       {
            Ext.getCmp('cbperseorangan').disable();
            Ext.getCmp('cbperusahaan').disable();
            Ext.getCmp('cbasuransi').enable();
            Ext.getCmp('cbperseorangan').hide();
            Ext.getCmp('cbperusahaan').hide();
            Ext.getCmp('cbasuransi').show();
            Ext.getCmp('cbperseorangan').setValue(false);
            Ext.getCmp('cbperusahaan').setValue(false);
            Ext.getCmp('cbasuransi').setValue(false);
        
            Ext.getCmp('cboPerseorangan').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            Ext.getCmp('cboAsuransi').show();
			ref_combo_kelpas(2);
       }
       else if(value === "Semua")
       {
            Ext.getCmp('cboPerseorangan').hide();
            Ext.getCmp('cboAsuransi').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            
            Ext.getCmp('cbperseorangan').show();
            Ext.getCmp('cbperusahaan').hide();
            Ext.getCmp('cbasuransi').hide();
            Ext.getCmp('cbperseorangan').disable();
            Ext.getCmp('cbperusahaan').disable();
            Ext.getCmp('cbasuransi').disable();
            Ext.getCmp('cbperseorangan').setValue(true);
       }else
       {
           Ext.getCmp('cboPerseorangan').hide();
            Ext.getCmp('cboAsuransi').hide();
            Ext.getCmp('cboPerusahaanRequestEntry').hide();
            Ext.getCmp('cboUmum').hide();
       }
}