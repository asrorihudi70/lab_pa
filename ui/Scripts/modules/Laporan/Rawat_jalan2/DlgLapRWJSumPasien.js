
var dsRWJSumPasien;
var selectRWJSumPasien;
var selectNamaRWJSumPasien;
var now = new Date();
var selectSetUNIT='Semua';
var frmDlgRWJSumPasien;
var varLapRWJSumPasien= ShowFormLapRWJSumPasien();
var dsRWJ;
var dataSource_unitSummary;
function ShowFormLapRWJSumPasien()
{
    frmDlgRWJSumPasien= fnDlgRWJSumPasien();
    frmDlgRWJSumPasien.show();
};

function fnDlgRWJSumPasien()
{
    var winRWJSumPasienReport = new Ext.Window
    (
        {
            id: 'winRWJSumPasienReport',
            title: 'Laporan Summary Pasien Rawat Jalan Per Hari',
            closeAction: 'destroy',
            width:620,
            height: 320,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWJSumPasien()]

        }
    );

    return winRWJSumPasienReport;
};

function datarefresh_viInformasiUnitSummary()
{
    dataSource_unitSummary.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )
    //alert("refersh")
}
function mComboUnitLapRWJ()
{


    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    dsRWJ = new WebApp.DataStore({ fields: Field });

    dsRWJ.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ComboUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    );
    
  var comboUnitLapRWJ = new Ext.form.ComboBox
    (
        {
            id:'comboUnitLapRWJ',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Poliklinik ',
            align:'Right',
            anchor:'100%',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store:dsRWJ,
			value: selectSetUNIT,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetUNIT=b.data.KD_UNIT ;
				//	alert(selectSetUNIT);
                    //selectSetSumPasien=b.data.NAMA_UNIT ;
                }
            }
        }
    );

    return comboUnitLapRWJ;
};

function ItemDlgRWJSumPasien()
{
	var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    dataSource_unitSummary = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    datarefresh_viInformasiUnitSummary()

    // Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
	];


	// declare the source Grid
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_unitSummary,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 200,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Unit',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNRM_viDaftar',
                                                    header: 'No.Medrec',
                                                    dataIndex: 'KD_UNIT',
                                                    sortable: true,
                                                    hidden : true
                                            },
                                            {
                                                    id: 'colNMPASIEN_viDaftar',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA_UNIT',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),
				listeners : {
                    afterrender : function(comp) {
                    var firstGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
   
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStore,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 200,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_UNIT',
                    title            : 'Unit',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'secondGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid.store.add(records);
                                    secondGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });

    var PnlLapRWJSumPasien = new Ext.Panel
    (
        {
            id: 'form_summary_rwj',
		    closable: true,
		    region: 'center',
		    layout: 'column',
            height       : 250,
			
//		    title:  'Pilih Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
            items:
            [
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[firstGrid
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid
						
					]
				},
                {
					columnWidth: .30,
					layout: 'form',
					border: false,
					labelAlign: 'right',
					labelWidth: 77,
				 
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'periode ',
							id: 'dtpTglAwalLapRWJSumPasien',
							format: 'd-M-Y',
							value:now,
							 anchor: '99%'
						},
					
						
					]
				},
				
				{
					columnWidth: .30,
					layout: 'form',
					border: false,
					labelWidth: 30,
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'to',
							id: 'dtpTglAkhirLapRWJSumPasien',
							format: 'd-M-Y',
							value:now,
							anchor: '100%'
						}
					]
				},//getItemLapRWJcborwj(),
				
				/* {
                                columnWidth: .99,
                                layout: 'hBox',
                                border: false,
                                defaults: { margins: '0 5 0 0' },
                                style:{'margin-left':'350px','margin-top':'30px'},
                                anchor: '100%',
                                layoutConfig:
                                {
                                    padding: '3',
                                    pack: 'end',
                                    align: 'middle'
                                },
                                items:
                                [
                                    {
                                       xtype: 'checkbox',
                                       id: 'CekLapPilihSemuaRWJDetail',
                                       hideLabel:false,
                                       boxLabel: 'Pilih Semua',
                                       checked: false,
                                       listeners: 
                                       {
                                            check: function()
                                            {
                                               if(Ext.getCmp('CekLapPilihSemuaRWJDetail').getValue()===true)
                                                {
                                                     firstGrid.getSelectionModel().selectAll();
                                                }
                                                else
                                                {
                                                    firstGrid.getSelectionModel().clearSelections();
                                                }
                                            }
                                       }
                                    },
                                    {
                                        xtype: 'button',
                                        text: nmBtnOK,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnOkLapRWJPerLaporandetail',
                                        handler: function()
                                        {
                                            var sendDataArray = [];
                                            secondGridStore.each(function(record){
                                            var recordArray = [record.get("KD_UNIT")];
                                            sendDataArray.push(recordArray);
											
                                            });
									
											
                                           if (sendDataArray.length === 0)
											{
                                                   ShowPesanWarningRWJPerLaporandetailReport('Isi kriteria unit dengan drag and drop','Laporan')
                                                 
										    }else{
											 var criteria = GetCriteriaRWJPerLaporandetail();
                                             criteria += '##@@##' + sendDataArray;
                                             frmDlgRWJPerLaporandetail.close();
                                             loadlaporanRWJ('0', 'rep010201', criteria);
											};
                                        }
                                    },
                                    {
                                        xtype: 'button',
                                        text: nmBtnCancel ,
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnCancelLapRWJPerLaporandetail',
                                        handler: function()
                                        {
                                                frmDlgRWJPerLaporandetail.close();
                                        }
                                    }
                                    ]
                                }, */
                {
					columnWidth: .99,
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnOkLapRWJSumPasien',
                            handler: function()
                            {
                               if (ValidasiReportRWJSumPasien() === 1)
                               {
								   var sendDataArray = [];
                                            secondGridStore.each(function(record){
                                            var recordArray = [record.get("KD_UNIT")];
                                            sendDataArray.push(recordArray);
											
                                            });
									
											
                                           if (sendDataArray.length === 0)
											{
                                                   ShowPesanWarningRWJSumPasienReport('Isi kriteria unit dengan drag and drop','Laporan')
                                                 
										    }else{
											 var criteria = GetCriteriaRWJSumPasien();
                                             criteria += '##@@##' + sendDataArray;
                                             frmDlgRWJSumPasien.close();
                                             loadlaporanRWJ('0', 'rep010202', criteria);
											};
                                        /* var criteria = GetCriteriaRWJSumPasien();
                                        frmDlgRWJSumPasien.close();
                                        loadlaporanRWJ('0', 'rep010202', criteria); */
                               };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
						 style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnCancelLapRWJSumPasien',
                            handler: function()
                            {
                                    frmDlgRWJSumPasien.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRWJSumPasien;
};

function GetCriteriaRWJSumPasien()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRWJSumPasien').dom.value !== '')
	{
		strKriteria = Ext.get('dtpTglAwalLapRWJSumPasien').dom.value;
	};
	if (Ext.get('dtpTglAkhirLapRWJSumPasien').dom.value !== '')
	{
		strKriteria += '##@@##'+Ext.get('dtpTglAkhirLapRWJSumPasien').dom.value;
	};

	return strKriteria;
};


function ValidasiReportRWJSumPasien()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRWJSumPasien').dom.value === ''))
    {
            if(Ext.get('dtpTglAwalLapRWJSumPasien').dom.value === '')
            {
                    ShowPesanWarningRWJSumPasienReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('comboUnitLapRWJ').dom.value === '' || Ext.get('comboUnitLapRWJ').dom.value === 'Silahkan Pilih...' )
            {
                    ShowPesanWarningRWJSumPasienReport(nmGetValidasiKosong('Unit'),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRWJSumPasien()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRWJSumPasien').dom.value > Ext.get('dtpTglAkhirLapRWJSumPasien').dom.value)
    {
        ShowPesanWarningRWJSumPasienReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRWJSumPasienReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRWJSumPasien_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:.50,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                 
                ]
            }
        ]
    }
    return items;
};


function getItemLapRWJSumPasien_Tanggal()
{
   var items =
    {
        layout: 'form',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .50,
			    layout: 'form',
				 labelWidth:70,
				  border: false,
				 
			    items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'periode ',
						id: 'dtpTglAwalLapRWJSumPasien',
						format: 'd-M-Y',
						value:now,
					     anchor: '99%'
					},
				
					
				]
			},
			
			{
			    columnWidth: .49,
			    layout: 'form',
			    border: false,
				labelWidth:20,
			    items:
				[
					{
						xtype: 'datefield',
						fieldLabel: 'to',
						id: 'dtpTglAkhirLapRWJSumPasien',
						format: 'd-M-Y',
						value:now,
					    anchor: '100%'
					}
				]
			}
     
        ]
    }
    return items;
};


