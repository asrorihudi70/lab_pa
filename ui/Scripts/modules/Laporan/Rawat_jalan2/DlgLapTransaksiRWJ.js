var polipilihanpasien;
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsRwJTransaksi;
var selectNamaRwJTransaksi;
var now = new Date();
var selectSetPerseorangan;
var frmDlgRwJTransaksi;
var varLapRwJTransaksi= ShowFormLapRwJTransaksi();
var selectSetUmum;
var selectSetkelpas;
 var winRwJTransaksiReport;

function ShowFormLapRwJTransaksi()
{
    frmDlgRwJTransaksi= fnDlgRwJTransaksi();
    //frmDlgRwJTransaksi.show();
};

function fnDlgRwJTransaksi()
{
     winRwJTransaksiReport = new Ext.Window
    (
        {
            id: 'winRwJTransaksiReport',
            title: 'Laporan ',
            closeAction: 'destroy',
            width:400,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRwJTransaksi()],
            listeners:
			{
            activate: function()
				{
             
				}
			}

        }
    );

    winRwJTransaksiReport.show();
	dataaddnew();
};
function dataaddnew()
{
				Ext.getCmp('cboPerseoranganRegisLab').hide();
                Ext.getCmp('cboAsuransiRegisLab').hide();
                Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
                Ext.getCmp('cboUmumRegisLab').show();
				Ext.getCmp('cboPilihanRwJTransaksikelompokPasien').setValue('Semua');
				Ext.getCmp('cboUmumRegisLab').setValue('Semua');
				Ext.getCmp('cboPilihanRwJTransaksi').setValue('Semua');
				
}

function ItemDlgRwJTransaksi()
{
    var PnlLapRwJTransaksi = new Ext.Panel
    (
        {
            id: 'PnlLapRwJTransaksi',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [	getItemLapRwJTransaksi_tengah(),
                getItemLapRwJTransaksi_Atas(),
                getItemLapRwJTransaksi_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRwJTransaksi',
                            handler: function()
                            {
                               if (ValidasiReportRwJTransaksi() === 1)
                               {
                                        //var tmppilihan = getKodeReportRwJTransaksi();
                                        var criteria = GetCriteriaRwJTransaksi();
                                        winRwJTransaksiReport.close();
                                       loadlaporanRWJ('0', 'rep010210', criteria);
								};
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRwJTransaksi',
                            handler: function()
                            {
                            winRwJTransaksiReport.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapRwJTransaksi;
};

function GetCriteriaRwJTransaksi()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalFilterRwJTransaksi').getValue() !== '')
	{
		strKriteria = Ext.get('dtpTglAwalFilterRwJTransaksi').getValue();
	}
	if (Ext.get('dtpTglAkhirFilterRwJTransaksi').getValue() !== '')
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterRwJTransaksi').getValue();
	}

	if(Ext.getCmp('cboPilihanRwJTransaksi').getValue() !== ''|| Ext.getCmp('cboPilihanRwJTransaksi').getValue() === '')
	{
		if (Ext.get('cboPilihanRwJTransaksi').getValue()=== ''){
			
			
		}  else {
		strKriteria += '##@@##' + polipilihanpasien
		} 
	}
       
	if (Ext.getCmp('cboPilihanRwJTransaksikelompokPasien').getValue() !== '')
	{
		if (Ext.get('cboPilihanRwJTransaksikelompokPasien').getValue() === 'Semua')
        {
            strKriteria += '##@@##' + 'Semua';
            strKriteria += '##@@##' + 'NULL';
			 strKriteria += '##@@##' ;
        } 
		else if (Ext.get('cboPilihanRwJTransaksikelompokPasien').getValue() === 'Perseorangan')
		{
			
			if(Ext.getCmp('cboperoranganRequestEntryRegisLab').getValue() === '')
            {
                strKriteria += '##@@##' + '0';
                strKriteria += '##@@##' + 'NULL';
				 strKriteria += '##@@##' ;
            }else{
                strKriteria += '##@@##' + '0';
                strKriteria += '##@@##' + selectsetperusahaan;
				 strKriteria += '##@@##';
            }
        } else if (Ext.get('cboPilihanRwJTransaksikelompokPasien').getValue() === 'Perusahaan'){
            
			if(Ext.getCmp('cboPerusahaanRequestEntryRegisLab').getValue() === '')
            {
                strKriteria += '##@@##' + '1';
                strKriteria += '##@@##' + 'NULL';
				 strKriteria += '##@@##' ;
            }else{
                strKriteria += '##@@##' + '1';
                strKriteria += '##@@##' + selectsetperusahaan;
				strKriteria += '##@@##' ;
            }
        } else {
            
			if(Ext.getCmp('cboAsuransiRegisLab').getValue() === '')
            {
                strKriteria += '##@@##' + '2';
                strKriteria += '##@@##' + 'NULL';
				 strKriteria += '##@@##' ;
            }else{
                strKriteria += '##@@##' + '2';
                strKriteria += '##@@##' + selectSetAsuransi;
				strKriteria += '##@@##' ;
            }
            
        } 
	}   
	if (Ext.getCmp('Shift_All_RwJTransaksi').getValue() === true)
	{
		
		strKriteria +=  '1';
		strKriteria += ',' + '2';
		strKriteria += ',' + '3,';
		strKriteria += '##@@##';
		strKriteria += '4';
	}else{
		if (Ext.getCmp('Shift_1_RwJTransaksi').getValue() === true)
		{
		
			strKriteria += '1,';
		}
		if (Ext.getCmp('Shift_2_RwJTransaksi').getValue() === true)
		{
			
			strKriteria += '2,';
		}
		if (Ext.getCmp('Shift_3_RwJTransaksi').getValue() === true)
		{
			
			strKriteria += '3,';
			strKriteria += '##@@##';
			strKriteria += '4';
		}
	}
	if (Ext.getCmp('PendaftaranRwJTransaksi').getValue() === true)
	{
	strKriteria += '##@@##'+'ya';
	} 
	if (Ext.getCmp('PendaftaranRwJTransaksi').getValue() === false)
	{
	strKriteria += '##@@##'+'tidak';
	} 
	if (Ext.getCmp('TindakanRwJTransaksi').getValue() === true)
	{
	strKriteria += '##@@##'+'ya';
	}
	if (Ext.getCmp('TindakanRwJTransaksi').getValue() === false)
	{
	strKriteria += '##@@##'+'tidak';
	}
	return strKriteria;
};

function getKodeReportRwJTransaksi()
{   var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanRwJTransaksi').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanRwJTransaksi').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportRwJTransaksi()
{
    var x=1;
	
    if(Ext.get('dtpTglAwalFilterRwJTransaksi').dom.value > Ext.get('dtpTglAkhirFilterRwJTransaksi').dom.value)
    {
        ShowPesanWarningRwJTransaksiReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    }
	if(Ext.getCmp('cboPilihanRwJTransaksikelompokPasien').getValue() === ''){
		ShowPesanWarningRwJTransaksiReport('Kelompok Pasien Belum Dipilih','Laporan Transaksi');
        x=0;
	}

	if(Ext.getCmp('Shift_All_RwJTransaksi').getValue() === false && Ext.getCmp('Shift_1_RwJTransaksi').getValue() === false && Ext.getCmp('Shift_2_RwJTransaksi').getValue() === false && Ext.getCmp('Shift_3_RwJTransaksi').getValue() === false){
		ShowPesanWarningRwJTransaksiReport(nmRequesterRequest,'Laporan Transaksi');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningRwJTransaksiReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapRwJTransaksi_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Poli '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanRwJTransaksi(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterRwJTransaksi',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 40,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterRwJTransaksi',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanRwJTransaksiKelompokPasien(),
                mComboPerseoranganRwJTransaksi(),
                mComboAsuransiRwJTransaksi(),
                mComboPerusahaanRwJTransaksi(),
                mComboUmumRwJTransaksi(),
				mComboUmumLabRegis()
            ]
        }]
    };
    return items;
};

function getItemLapRwJTransaksi_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {boxLabel: 'Semua',name: 'Shift_All_RwJTransaksi',id : 'Shift_All_RwJTransaksi',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_RwJTransaksi').setValue(true);Ext.getCmp('Shift_2_RwJTransaksi').setValue(true);Ext.getCmp('Shift_3_RwJTransaksi').setValue(true);Ext.getCmp('Shift_1_RwJTransaksi').disable();Ext.getCmp('Shift_2_RwJTransaksi').disable();Ext.getCmp('Shift_3_RwJTransaksi').disable();}else{Ext.getCmp('Shift_1_RwJTransaksi').setValue(false);Ext.getCmp('Shift_2_RwJTransaksi').setValue(false);Ext.getCmp('Shift_3_RwJTransaksi').setValue(false);Ext.getCmp('Shift_1_RwJTransaksi').enable();Ext.getCmp('Shift_2_RwJTransaksi').enable();Ext.getCmp('Shift_3_RwJTransaksi').enable();}}},
                                    {boxLabel: 'Shift 1',name: 'Shift_1_RwJTransaksi',id : 'Shift_1_RwJTransaksi'},
                                    {boxLabel: 'Shift 2',name: 'Shift_2_RwJTransaksi',id : 'Shift_2_RwJTransaksi'},
                                    {boxLabel: 'Shift 3',name: 'Shift_3_RwJTransaksi',id : 'Shift_3_RwJTransaksi'}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};


function getItemLapRwJTransaksi_tengah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: '',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
									{boxLabel: 'Pendaftaran',name: 'PendaftaranRwJTransaksi',id : 'PendaftaranRwJTransaksi'},
                                    {boxLabel: 'Tindakan RWJ',name: 'TindakanRwJTransaksi',id : 'TindakanRwJTransaksi'}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};


var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;

function mComboPilihanRwJTransaksi()
{

   var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ComboUnit_transakasi',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )
    var cboPilihanRwJTransaksi = new Ext.form.ComboBox
	(
             {
			  x: 120,
                y: 10,
            id: 'cboPilihanRwJTransaksi',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poli',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
             width:240,
			tabIndex:29,
            listeners:
                {
                    'select': function(a, b, c)
				{
					
                     
						polipilihanpasien=b.data.KD_UNIT;
				},
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    {
										Ext.getCmp('cboDokterRequestEntry').focus();
										polipilihanpasien= c.value;
									}else if(e.getKey()==9)
									{
										polipilihanpasien= c.value;	
										   
									}
                                    }, c);
                                }


		}
        }
	);
	return cboPilihanRwJTransaksi;
};





function mComboPilihanRwJTransaksiKelompokPasien()
{
    var cboPilihanRwJTransaksikelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanRwJTransaksikelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPilihankelompokPasien,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanRwJTransaksikelompokPasien;
};

function mComboPerseoranganRwJTransaksi()
{
    var cboPerseoranganRegisLab = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganRegisLab',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRegisLab;
};

function mComboUmumRwJTransaksi()
{
  var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 order by customer asc'
			}
		}
	);
    var cboperoranganRequestEntryRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboperoranganRequestEntryRegisLab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Customer...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboperoranganRequestEntryRegisLab;

};

function mComboPerusahaanRwJTransaksi()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 order by customer asc'
			}
		}
	);
    var cboPerusahaanRequestEntryRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryRegisLab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRegisLab;
};

function mComboAsuransiRwJTransaksi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 order by customer asc"
            }
        }
    );
    var cboAsuransiRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiRegisLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiRegisLab;
};
function mComboUmumLabRegis()
{
    var cboUmumRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumRegisLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetUmum,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumRegisLab;
};
function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboperoranganRequestEntryRegisLab').show();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboperoranganRequestEntryRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').show();
        Ext.getCmp('cboUmumRegisLab').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboperoranganRequestEntryRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').show();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').show();
   }
}

function mCombounitRwJTransaksi()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryRwJTransaksi = new Ext.form.ComboBox
    (
        {
            id: 'cbounitRequestEntryRwJTransaksi',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                }
                    
                }
        }
    )

    return cbounitRequestEntryRwJTransaksi;
};
