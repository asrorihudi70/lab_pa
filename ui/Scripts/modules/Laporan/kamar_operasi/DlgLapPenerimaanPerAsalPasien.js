var fields = [
	{name: 'KD_UNIT', mapping : 'KD_UNIT'},
	{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
];

var fieldsPayment = [
	{name: 'KD_PAY', mapping : 'KD_PAY'},
	{name: 'URAIAN', mapping : 'URAIAN'}
];

var cols = [
	{ id : 'KD_UNIT', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_UNIT',hidden : true},
	{ header: "Unit yang dipilih", width: 50, sortable: true, dataIndex: 'NAMA_UNIT'}
];

var colsPayment = [
	{ id : 'KD_PAY', header: "Kode Pay", width: 160, sortable: true, dataIndex: 'KD_PAY',hidden : true},
	{ header: "Payment yang dipilih", width: 50, sortable: true, dataIndex: 'URAIAN'}
];
var kodeCustomer;
var secondGridStoreLapPenerimaanPerAsalPasien;
var secondGridStoreLapPenerimaanPayment_LapPenerimaanPerAsalPasien;
var pilihanCetakan;
var dataSource_payment;
var firstGridPayment;
var DlgLapRWJPenerimaan={
	vars:{
		comboSelect:null,
		comboSelectAsalPasien:null,
		comboSelectAsalPasienRad:null,
	},
	coba:function(){
		var $this=this;
		$this.DateField.startDate
	},
	ArrayStore:{
		dokter:new Ext.data.ArrayStore({fields:[]})
	},
	CheckboxGroup:{
		shift:null,
		tindakan:null,
	},
	ComboBox:{
		kelPasien1:null,
		unitRad:null,
		asalPasien:null,
		combo1:null,
		combo2:null,
		combounitrad:null,
		combo3:null,
		combo0:null,
		poliklinik:null,
		dokter:null
	},
	DataStore:{
		combo2:null,
		combo3:null,
		poliklinik:null,
		combo1:null
	},
	DateField:{
		startDate:null,
		endDate:null
	},
	Window:{
		main:null
	},
	comboOnSelect:function(val){
		var $this=this;
		$this.ComboBox.combo0.hide();
		$this.ComboBox.combo1.hide();
		$this.ComboBox.combo2.hide();
		$this.ComboBox.combo3.hide();
		if(val==0){
			$this.ComboBox.combo0.show();
			
		}else if(val==1){
			$this.ComboBox.combo1.show();
		}else if(val==2){
			$this.ComboBox.combo2.show();
		}else if(val==3){
			$this.ComboBox.combo3.show();
		}
	},
	initPrint:function(){
		var $this=this;
		loadMask.show();
		var params={};
		var sendDataArraypayment = [];
		var sendDataArrayUnit = [];

		secondGridStoreLapPenerimaanPayment_LapPenerimaanPerAsalPasien.each(function(record){
			var recordArraypay= [record.get("KD_PAY")];
			sendDataArraypayment.push(recordArraypay);
		});
		secondGridStoreLapPenerimaanPerAsalPasien.each(function(record){
			var recordArraypay= [record.get("KD_UNIT")];
			sendDataArrayUnit.push(recordArraypay);
		});

		if (sendDataArraypayment.length === 0){  
			this.messageBox('Peringatan','Isi cara membayar dengan drag and drop.','WARNING');
			loadMask.hide();
		}else{
			if($this.ComboBox.asalPasien.getValue() != 4 && sendDataArrayUnit.length == 0){
				this.messageBox('Peringatan','Isi unit dengan drag and drop. Unit tidak boleh kosong, kecuali asal pasien adalah <b>Kunjungan Langsung</b>.','WARNING');
				loadMask.hide();
			} else{
				params['type_file'] = Ext.getCmp('type_file_rad_penerimaanperpasien').getValue();
				params['start_date'] = timestimetodate($this.DateField.startDate.getValue());
				params['last_date']  = timestimetodate($this.DateField.endDate.getValue());
				params['pasien']     = $this.ComboBox.kelPasien1.getValue();
				params['unit_rad'] = $this.ComboBox.unitRad.getValue();
				params['asal_pasien'] = $this.ComboBox.asalPasien.getValue();
				var pasien=parseInt($this.ComboBox.kelPasien1.getValue());
				if(pasien>=0){
					if(pasien==0){
						params['kd_customer']=kodeCustomer;
					}else if(pasien==1){
						params['kd_customer']=kodeCustomer;
					}else if(pasien==2){
						params['kd_customer']=kodeCustomer;
					}else if(pasien==3){
						params['kd_customer']=kodeCustomer;
					}else{
						params['kd_customer']='';
					}
				}
			
				params['tmp_payment'] 	= sendDataArraypayment;
				params['tmp_unit'] 	= sendDataArrayUnit;
				var shift  = $this.CheckboxGroup.shift.items.items;
				var shifta = false;
				var tindakan_stat=false;
				for(var i=0;i<shift.length ; i++){
					params['shift'+i]=shift[i].checked;
					if(shift[i].checked==true)shifta=true;
				}
				//if(shifta==false){
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("target", "_blank");
					if(pilihanCetakan == true){
						form.setAttribute("action", baseURL + "index.php/rad/lap_penerimaan/directPenerimaanPerAsalPasien");
					} else{					
						form.setAttribute("action", baseURL + "index.php/kamar_operasi/lap_kamar_operasi/cetakPenerimaanPerAsalPasien");
					}
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", "data");
					hiddenField.setAttribute("value", Ext.encode(params));
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();
					loadMask.hide();
				//}
			}
		} 
		
	},
	messageBox:function(modul, str, icon){
		Ext.MessageBox.show
		(
			{
				title: modul,
				msg:str,
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.icon,
				width:300
			}
		);
	},
	getDokter:function(){
		var $this=this;
		$this.ComboBox.dokter = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: $this.ArrayStore.dokter,
            width: 200,
            value:'Semua',
            valueField: 'kd_dokter',
            displayField: 'nama',
            listeners:{
            	'select': function(a, b, c){
                               
            	}
            }
        });		    
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gawat_darurat/lap_penerimaan/getDokter",
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.dokter.loadData([],false);
					for(var i=0,iLen=r.data.length; i<iLen ;i++){
						$this.ArrayStore.dokter.add(new $this.ArrayStore.dokter.recordType(r.data[i]));
					}
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
		return $this.ComboBox.dokter;
	},
	getCombo0:function(){
		var $this=this;
		$this.ComboBox.combo0 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: new Ext.data.ArrayStore({
				id: 0,
				fields:['Id','displayText'],
				data: [[1, 'Semua']]
			}),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			disabled:true,
			listeners:{
				'select': function(a,b,c){
					selectSetUmum=b.data.displayText ;
				}
			}
		});
		return $this.ComboBox.combo0;
	},
	getCombo1:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo1 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo1.load({
    		params:{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=0 ORDER BY CUSTOMER '
			}
		});
    	$this.ComboBox.combo1 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hidden:true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:150,
			store: $this.DataStore.combo1,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
			listeners:{
				'select': function(a,b,c){
					kodeCustomer=b.data.KD_CUSTOMER ;
				}
			}
		});
		return $this.ComboBox.combo1;
	},
	getCombo2:function(){
		var $this=this;
		var Field = ['KD_CUSTOMER','CUSTOMER'];
    	$this.DataStore.combo2 = new WebApp.DataStore({fields: Field});
    	$this.DataStore.combo2.load({
		    params:{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomer',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER '
			}
		});
   		$this.ComboBox.combo2 = new Ext.form.ComboBox({
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    hidden:true,
		    store: $this.DataStore.combo2,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			value:'Semua',
			width:150,
		    listeners:{
			    'select': function(a,b,c){
					kodeCustomer=b.data.KD_CUSTOMER ;
				}
			}
		});
    	return $this.ComboBox.combo2;
	},
	loadPayment: function(){
		dataSource_payment.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_PAY',
					Sortdir: 'ASC',
					target:'ViewListPayment',
					param: " "
				}
			}
		);
		return dataSource_payment;
	},
	firstGridPayment : function(){
		var $this = this;
		var Field_payment = ['KD_PAY','URAIAN'];
		dataSource_payment         = new WebApp.DataStore({fields: Field_payment});
		$this.loadPayment();
		firstGridPayment = new Ext.grid.GridPanel({
			ddGroup          : 'secondGridDDGroupPayment',
			store            : dataSource_payment,
			autoScroll       : true,
			columnLines      : true,
			border           : true,
			enableDragDrop   : true,
			height           : 135,
			stripeRows       : true,
			trackMouseOver   : true,
			// title            : 'Payment',
			anchor           : '100% 100%',
			plugins          : [new Ext.ux.grid.FilterRow()],
			tbar:[
				{
					xtype: 'buttongroup',
					columns: 6,
					defaults: {
						scale: 'small'
					},
					items: [
						{
							xtype: 'button',
							text: 'Pilih Semua Payment',
							iconCls: 'CheckedGreen',
							tooltip: 'Add Data',
							border:true,
							id: 'btnPilihSemuaPaymentLapPenerimaanPerAsalPasien',
							handler: function()
							{
								firstGridPayment.getSelectionModel().selectAll();	
							}
						}
					]
				}
			],
			colModel         : new Ext.grid.ColumnModel
			(
				[
				new Ext.grid.RowNumberer(),
					{
						id: 'colKD_pay',
						header: 'Kode Pay',
						dataIndex: 'KD_PAY',
						sortable: true,
						hidden : true
					},
					{
						id: 'colKD_uraian',
						header: 'Payment',
						dataIndex: 'URAIAN',
						sortable: true,
						width: 50
					}
				]
			),   
			listeners : {
				afterrender : function(comp) {
					var secondGridDropTargetEl = firstGridPayment.getView().scroller.dom;
					var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroupPayment',
							notifyDrop : function(ddSource, e, data){
									var records =  ddSource.dragData.selections;
									Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
									firstGridPayment.store.add(records);
									firstGridPayment.store.sort('KD_PAY', 'ASC');
									return true
							}
					});
				}
			},
				viewConfig: 
					{
							forceFit: true
					}
		});
        return firstGridPayment;
	},
	seconGridPayment : function(){
		var $this = this;
		var secondGridPayment;
		secondGridStoreLapPenerimaanPayment_LapPenerimaanPerAsalPasien = new Ext.data.JsonStore({
            fields : fieldsPayment,
            root   : 'records'
        });

        // create the destination Grid
            secondGridPayment = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroupPayment',
					store            : secondGridStoreLapPenerimaanPayment_LapPenerimaanPerAsalPasien,
					columns          : colsPayment,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 135,
					stripeRows       : true,
					autoExpandColumn : 'KD_PAY',
					// title            : 'Payment yang dipilih',
					tbar:[
						{
							xtype: 'buttongroup',
							columns: 6,
							defaults: {
								scale: 'small'
							},
							items: [
								{
									xtype: 'button',
									text: '&nbsp;Reset Payment',
									iconCls: 'reset',
									tooltip: 'Add Data',
									border:true,
									id: 'btnResetSemuaPaymentLapPenerimaanPerAsalPasien',
									handler: function()
									{
										secondGridStoreLapPenerimaanPayment_LapPenerimaanPerAsalPasien.removeAll();
										$this.loadPayment();
									}
								}
							]
						}
					],
					listeners : {
						afterrender : function(comp) {
							var secondGridDropTargetEl = secondGridPayment.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
									ddGroup    : 'secondGridDDGroupPayment',
									notifyDrop : function(ddSource, e, data){
											var records =  ddSource.dragData.selections;
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											secondGridPayment.store.add(records);
											secondGridPayment.store.sort('KD_PAY', 'ASC');
											return true
									}
							});
						}
                	},
                viewConfig: 
				{
						forceFit: true
				}
        });
        return secondGridPayment;
	},
	getCombo3:function(){
		var $this=this;
		var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    	$this.DataStore.combo3 = new WebApp.DataStore({fields: Field_poli_viDaftar});

		$this.DataStore.combo3.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomer',
                param: "jenis_cust=2 ORDER BY CUSTOMER "
            }
        });
    	$this.ComboBox.combo3 = new Ext.form.ComboBox({
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:150,
			hidden:true,
			store: $this.DataStore.combo3,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:'Semua',
//			value: 0,
			listeners:{
				'select': function(a,b,c){
					kodeCustomer=b.data.KD_CUSTOMER ;
//					selectSetAsuransi=b.data.KD_CUSTOMER ;
//					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		});
		return $this.ComboBox.combo3;
	},
	mComboUnitRad:function(){ 
		var $this = this;
		var Field = ['KD_UNIT','NAMA_UNIT'];
		$this.DataStore.combounitrad = new WebApp.DataStore({ fields: Field });
		$this.DataStore.combounitrad.load({
			params:{
				Skip: 0,
				Take: 1000,
				Sort: 'nama_unit',
				Sortdir: 'ASC',
				target: 'ViewSetupUnit',
				param: "parent = '5'"
			}
		});
		var cbounitrad_viPenJasRad = new Ext.form.ComboBox({
			id: 'cboUnitRad_viPenJasRad',
			x: 110,
			y: 190,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 130,
			emptyText:'Pilih Unit',
			store: $this.DataStore.combounitrad,
			valueField: 'KD_UNIT',
			displayField: 'NAMA_UNIT',
			//value:'All',
			editable: false,
			listeners:
				{
					'select': function(a,b,c)
					{
						tmpkd_unit = b.data.KD_UNIT;
						//getComboDokterRad(b.data.KD_UNIT);
					}
				}
		});
		return cbounitrad_viPenJasRad;
	},
	loadUnit : function(unit){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/rad/lap_penerimaan/getUnit",
				params: {unit:unit},
				failure: function(o)
				{
				},	
				success: function(o) 
				{
					dataSourceUnitLapPenerimaanPerAsalPasien.removeAll();
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						var recs=[],
							recType=dataSourceUnitLapPenerimaanPerAsalPasien.recordType;
						for(var i=0; i<cst.listData.length; i++){
							recs.push(new recType(cst.listData[i]));
						}
						dataSourceUnitLapPenerimaanPerAsalPasien.add(recs);
					}
				}
			}
			
		)
	},
	seconGridPenerimaan_LapPenerimaanPerAsalPasien : function(){
        var $this = this;
        var secondGrid;
        secondGridStoreLapPenerimaanPerAsalPasien = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
            secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStoreLapPenerimaanPerAsalPasien,
                    columns          : cols,
                    autoScroll       : true,
                    columnLines      : true,
                    border           : true,
                    enableDragDrop   : true,
                    enableDragDrop   : true,
                    height           : 135,
                    stripeRows       : true,
                    autoExpandColumn : 'KD_UNIT',
                    // title            : 'Unit yang dipilih',
					tbar:[
						{
							xtype: 'buttongroup',
							columns: 6,
							defaults: {
								scale: 'small'
							},
							items: [
								{
									xtype: 'button',
									text: '&nbsp;Reset Unit',
									iconCls: 'reset',
									tooltip: 'Add Data',
									border:true,
									id: 'btnResetSemuaUnitLapPenerimaanPerAsalPasien',
									handler: function()
									{
										secondGridStoreLapPenerimaanPerAsalPasien.removeAll();
										$this.loadUnit($this.vars.comboSelectAsalPasien);	
									}
								}
							]
						}
					],
                    listeners : {
                        afterrender : function(comp) {
                            var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                            var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                                    ddGroup    : 'secondGridDDGroup',
                                    notifyDrop : function(ddSource, e, data){
                                            var records =  ddSource.dragData.selections;
                                            Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                            secondGrid.store.add(records);
                                            secondGrid.store.sort('KD_UNIT', 'ASC');
                                            return true
                                    }
                            });
                        }
                    },
                viewConfig: 
                {
                        forceFit: true
                }
        });
        return secondGrid;
    },
    firstGridPenerimaan_LapPenerimaanPerAsalPasien : function(){
        var Field_poli_viDaftar = ['kd_unit','nama_unit'];
		dataSourceUnitLapPenerimaanPerAsalPasien = new WebApp.DataStore({fields: Field_poli_viDaftar});
		var $this=this;
			$this.loadUnit();
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSourceUnitLapPenerimaanPerAsalPasien,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 135,
            stripeRows       : true,
            trackMouseOver   : true,
            // title            : 'Unit',
            anchor           : '100% 100%',
			tbar:[
				{
					xtype: 'buttongroup',
					columns: 6,
					defaults: {
						scale: 'small'
					},
					items: [
						{
							xtype: 'button',
							text: 'Pilih Semua Unit',
							iconCls: 'CheckedGreen',
							tooltip: 'Add Data',
							border:true,
							id: 'btnPilihSemuaUnitLapPenerimaanPerAsalPasien',
							handler: function()
							{
								firstGrid.getSelectionModel().selectAll();	
							}
						}
					]
				}
			],
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
			(
				[
						new Ext.grid.RowNumberer(),
						{
								id: 'colNRM_viDaftar',
								header: 'kd_unit',
								dataIndex: 'KD_UNIT',
								sortable: true,
								hidden : true
						},
						{
								id: 'colNMPASIEN_viDaftar',
								header: 'Unit',
								dataIndex: 'NAMA_UNIT',
								sortable: true,
								width: 50
						}
				]
			),   
            listeners : {
                afterrender : function(comp) {
                    var secondGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('KD_UNIT', 'ASC');
                                    return true
                            }
                    });
                }
            },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
        return firstGrid;
    },
	init:function(){
		var $this=this;
		$this.Window.main=new Ext.Window({
			width: 451,
			height:640,
			modal:true,
			title:'Laporan Penerimaan Per Asal Pasien',
			layout:'fit',
			resizable:false,
			items:[
				new Ext.Panel({
					bodyStyle:'padding: 6px',
					layout:'form',
					border:false,
           			autoScroll: true,
					fbar:[
						new Ext.Button({
							text:'Preview',
							handler:function(){
								// hanya preview maka pilihanCetakan di isi false (artinya tidak dicetak direct)
								pilihanCetakan = false;
								$this.initPrint();
							}
						}),
						new Ext.Button({
							text:'Print',
							handler:function(){
								// Print maka pilihanCetakan di isi true (artinya dicetak direct)
								pilihanCetakan = true;
								$this.initPrint();
							}
						}),
						new Ext.Button({
							text:'Batal',
							handler:function(){
								$this.Window.main.close();
							}
						})
					],
					items:[
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							height:50,
							items:[
								{
									xtype : 'fieldset',
									layout: 'column',
									border: true,
									bodyStyle:'padding: 0px 0px  5px 5px',
									height:70,
									title : 'Asal Pasien',
									width:445,
									items:[
										{
											xtype : 'fieldset',
											layout: 'column',
											width : 445,
											bodyStyle:'padding: 0px 0px  5px 5px',
											border: false,
											items:[
												$this.ComboBox.asalPasien=new Ext.form.ComboBox({
													triggerAction: 'all',
													lazyRender:true,
													mode: 'local',
													width: 150,
													selectOnFocus:true,
													forceSelection: true,
													emptyText:'Silahkan Pilih...',
													fieldLabel: 'Pendaftaran Per Shift ',
													store: new Ext.data.ArrayStore({
														id: 0,
														fields:[
																'Id',
																'displayText'
														],
														data: [[0, 'Semua'],[1, 'RWJ'],[2, 'RWI'], [3, 'IGD'], [4, 'Kunjungan Langsung']]
													}),
													valueField: 'Id',
													displayField: 'displayText',
													value:'Semua',
													listeners:{
														'select': function(a,b,c){
																$this.vars.comboSelectAsalPasien=b.data.Id;
																secondGridStoreLapPenerimaanPerAsalPasien.removeAll();
																$this.loadUnit(b.data.displayText);
														}
													}
												}),
												{
													border:false,
													width :10,
													bodyStyle:'margin-top: 2px',
												},
												$this.ComboBox.unitRad=new Ext.form.ComboBox({
													triggerAction: 'all',
													lazyRender:true,
													mode: 'local',
													width: 150,
													selectOnFocus:true,
													forceSelection: true,
													emptyText:'Silahkan Pilih...',
													store: new Ext.data.ArrayStore({
														id: 0,
														fields:[
																'Id',
																'displayText'
														],
														data: [[0, 'Semua'],[1, 'Kamar Operasi IGD'],[2, 'Kamar Operasi Pav'], [3, 'Kamar Operasi Umum']]
													}),
													valueField: 'Id',
													displayField: 'displayText',
													value:'Semua',
													listeners:{
														'select': function(a,b,c){
																$this.vars.comboSelectAsalPasienRad=b.data.displayText;
														}
													}
												}),								
											]
										},
									]
								},
							]
						},
						{
							xtype : 'fieldset',
							title : 'Unit',
							layout:'column',
							border:true,
							bodyStyle:'padding: 0px 0px  5px 0px',
							height: 173,
							items:[
									{
										border:false,
										columnWidth:.50,
										
										items:[
											$this.firstGridPenerimaan_LapPenerimaanPerAsalPasien(),
										]
									},
									{
										border:false,
										width :10,
										bodyStyle:'margin-top: 2px',
									},
									{
										border:false,
										columnWidth:.50,
										items:[
											$this.seconGridPenerimaan_LapPenerimaanPerAsalPasien(),
										]
									}
								]
						},
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							items:[
									{
										xtype : 'fieldset',
										title : 'Perode',
										layout: 'column',
										border: true,
										bodyStyle:'padding: 2px 0px  7px 15px',
										width : 445,
										height: 67,
										items:[
											{
												xtype : 'fieldset',
												layout: 'column',
												border: false,
												bodyStyle:'padding: 0px 0px  5px 0px',
												width : 360,
												items:[
													$this.DateField.startDate=new Ext.form.DateField({
														value: new Date(),
														format:'d/M/Y'
													}),
													{
														xtype:'displayfield',
														width: 30,
														value:'&nbsp;&nbsp;s/d&nbsp;'
													},
													$this.DateField.endDate=new Ext.form.DateField({
														value: new Date(),
														format:'d/M/Y'
													})
												]
											}	
										]
									}
							]
						},
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							items:[
									{
										xtype : 'fieldset',
										title : 'Kelompok pasien',
										layout: 'column',
										border: true,
										bodyStyle:'padding: 2px 0px  7px 15px',
										width : 445,
										height: 67,
										items:[
											$this.ComboBox.kelPasien1=new Ext.form.ComboBox({
												triggerAction: 'all',
												lazyRender:true,
												mode: 'local',
												width: 100,
												selectOnFocus:true,
												forceSelection: true,
												fieldLabel: 'Pendaftaran Per Shift ',
												store: new Ext.data.ArrayStore({
													id: 0,
													fields:[
															'Id',
															'displayText'
													],
													data: [[0, 'Semua'],[1, 'Perseorangan'],[2, 'Perusahaan'], [3, 'Asuransi']]
												}),
												valueField: 'Id',
												displayField: 'displayText',
												value:'Semua',
												listeners:{
													'select': function(a,b,c){
															$this.vars.comboSelect=b.data.displayText;
															$this.comboOnSelect(b.data.Id);
															kodeCustomer='Semua';
													}
												}
											}),
											{
												xtype:'displayfield',
												width: 10,
												value:'&nbsp;'
											},
											$this.getCombo0(),
											$this.getCombo1(),
											$this.getCombo2(),
											$this.getCombo3()
										]
									}
							]
						},
						{
							layout: 'column',
							border: false,
							bodyStyle:'margin-top: 2px',
							items:[
									
									{
										xtype : 'fieldset',
										title : 'Shift',
										layout:'column',
										border: true,
										bodyStyle:'padding: 0px 0px 0px 10px',
										width : 445,
										items:[
											$this.CheckboxGroup.shift=new Ext.form.CheckboxGroup({
												xtype: 'checkboxgroup',
												items: [
													{boxLabel: 'Semua',checked:true,name: 'Shift_All_LabRegis',id : 'Shift_All_LabRegis',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1_LabRegis').setValue(true);Ext.getCmp('Shift_2_LabRegis').setValue(true);Ext.getCmp('Shift_3_LabRegis').setValue(true);Ext.getCmp('Shift_1_LabRegis').disable();Ext.getCmp('Shift_2_LabRegis').disable();Ext.getCmp('Shift_3_LabRegis').disable();}else{Ext.getCmp('Shift_1_LabRegis').setValue(false);Ext.getCmp('Shift_2_LabRegis').setValue(false);Ext.getCmp('Shift_3_LabRegis').setValue(false);Ext.getCmp('Shift_1_LabRegis').enable();Ext.getCmp('Shift_2_LabRegis').enable();Ext.getCmp('Shift_3_LabRegis').enable();}}},
													{boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1_LabRegis',id : 'Shift_1_LabRegis'},
													{boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2_LabRegis',id : 'Shift_2_LabRegis'},
													{boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3_LabRegis',id : 'Shift_3_LabRegis'},
													{boxLabel: 'Excel',checked:false,name: 'type_file_rad_penerimaanperpasien',id : 'type_file_rad_penerimaanperpasien'}
												]
											})
										]
									}
							]
						},
						{
							xtype : 'fieldset',
							title : 'Payment',
							layout:'column',
							border:true,
							height: 173,
							bodyStyle:'padding: 0px 0px  5px 0px',
							items:[
								{
									border:false,
									columnWidth:.50,
									items:[
										$this.firstGridPayment(),
									]
								},
								{
									border:false,
									width :10,
									bodyStyle:'margin-top: 2px',
								},
								{
									border:false,
									columnWidth:.50,
									items:[
										$this.seconGridPayment(),
									]
								}
							]
						},
					]
				})
			]
		}).show();
	}
};
DlgLapRWJPenerimaan.init();
