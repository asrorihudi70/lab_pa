
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapRegisRad;
var selectNamaLapRegisRad;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRegisRad;
var varLapLapRegisRad= ShowFormLapLapRegisRad();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var cboUserRequestEntryLapRegisRad;
var customer;
var kdcustomer;
var shift;
var shift1;
var shift2;
var shift3;
var periode;
var tglAwal;
var tglAkhir;
var tipe;
var winLapRegisRadReport;

function ShowFormLapLapRegisRad()
{
    frmDlgLapRegisRad= fnDlgLapRegisRad();
    frmDlgLapRegisRad.show();
	loadDataComboUserLapRegisRad();
};

function fnDlgLapRegisRad()
{
    winLapRegisRadReport = new Ext.Window
    (
        {
            id: 'winLapRegisRadReport',
            title: 'Laporan Regis Pasien Kamar Operasi',
            closeAction: 'destroy',
            width: 370,
            height: 390,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRegisRad()],
            listeners:
			{
				activate: function()
				{
					Ext.getCmp('cboPerseoranganLapRegisRad').hide();
					Ext.getCmp('cboAsuransiLapRegisRad').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLapRegisRad').hide();
					Ext.getCmp('cboUmumLapRegisRad').show();
				}
			},
			fbar:[
					{
						xtype: 'button',
						text: 'Ok',
						width: 70,
						hideLabel: true,
						id: 'btnOkLapLapRegisRad',
						handler: function()
						{
							if (ValidasiReportLapRegisRad() === 1)
							{
								if (Ext.get('cboPilihanLapRegisRadkelompokPasien').getValue() === 'Semua')
								{
									tipe='Semua';
									customer='Semua';
									kdcustomer='Semua';
								} else if (Ext.get('cboPilihanLapRegisRadkelompokPasien').getValue() === 'Perseorangan'){
									customer=Ext.get('cboPerseoranganLapRegisRad').getValue();
									tipe='Perseorangan';
									kdcustomer=Ext.getCmp('cboPerseoranganLapRegisRad').getValue();
								} else if (Ext.get('cboPilihanLapRegisRadkelompokPasien').getValue() === 'Perusahaan'){
									customer=Ext.get('cboPerusahaanRequestEntryLapRegisRad').getValue();
									tipe='Perusahaan';
									kdcustomer=Ext.getCmp('cboPerusahaanRequestEntryLapRegisRad').getValue();
								} else {
									customer=Ext.get('cboAsuransiLapRegisRad').getValue();
									tipe='Asuransi';
									kdcustomer=Ext.getCmp('cboAsuransiLapRegisRad').getValue();
								} 
								
								if(Ext.getCmp('radioasal').getValue() === true){
									periode='tanggal';
									tglAwal=Ext.getCmp('dtpTglAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpTglAkhirFilterHasilLab').getValue();
								} else{
									periode='bulan';
									tglAwal=Ext.getCmp('dtpBulanAwalFilterHasilLab').getValue();
									tglAkhir=Ext.getCmp('dtpBulanAkhirFilterHasilLab').getValue();
								}
								
								if (Ext.getCmp('Shift_All_LapRegisRad').getValue() === true){
									shift='All';
									shift1='false';
									shift2='false';
									shift3='false';
								}else{
									shift='';
									if (Ext.getCmp('Shift_1_LapRegisRad').getValue() === true){
										shift1='true';
									} else{
										shift1='false';
									}
									if (Ext.getCmp('Shift_2_LapRegisRad').getValue() === true){
										shift2='true';
									}else{
										shift2='false';
									}
									if (Ext.getCmp('Shift_3_LapRegisRad').getValue() === true){
										shift3='true';
									}else{
										shift3='false';
									}
								}
								
								var params={
									asal_pasien:Ext.get('cboPilihanLapRegisRad').getValue(),
									//user:Ext.getCmp('cboUserRequestEntryLapRegisRad').getValue(),
									tipe:tipe,
									customer:customer,
									kdcustomer:kdcustomer,
									periode:periode,
									tglAwal:tglAwal,
									tglAkhir:tglAkhir,
									shift:shift,
									shift1:shift1,
									shift2:shift2,
									shift3:shift3,
								} ;
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/kamar_operasi/lap_kamar_operasi/laporan_regis_pasien");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
								//winLapRegisRadReport.close();
							};
						}
					},
					{
						xtype: 'button',
						text: 'Cancel' ,
						width: 70,
						hideLabel: true,
						id: 'btnCancelLapLapRegisRad',
						handler: function()
						{
							winLapRegisRadReport.close();
						}
					}
			]

        }
    );

    return winLapRegisRadReport;
};


function ItemDlgLapRegisRad()
{
    var PnlLapLapRegisRad = new Ext.Panel
    (
        {
            id: 'PnlLapLapRegisRad',
            fileUpload: true,
            layout: 'form',
            height: '470',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapLapRegisRad_Atas(),
                getItemLapLapRegisRad_Batas(),
                getItemLapLapRegisRad_Bawah(),
                getItemLapLapRegisRad_Batas(),
                getItemLapLapRegisRad_Samping(),
            ]
        }
    );

    return PnlLapLapRegisRad;
};



function ValidasiReportLapRegisRad()
{
	var x=1;
	if(Ext.getCmp('cboPilihanLapRegisRad').getValue() === ''){
		ShowPesanWarningLapRegisRadReport('Pasien Belum Dipilih','Warning');
        x=0;
	}
	
	if(Ext.getCmp('cboPilihanLapRegisRadkelompokPasien').getValue() === ''){
		ShowPesanWarningLapRegisRadReport('Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboPerseoranganLapRegisRad').getValue() === '' &&  Ext.get('cboPerusahaanRequestEntryLapRegisRad').getValue() === '' &&  Ext.get('cboAsuransiLapRegisRad').getValue() === '' && Ext.get('cboUmumRegisLab').getValue() === '' ){
		ShowPesanWarningLapRegisRadReport('Sub Kelompok Pasien Belum Dipilih','Warning');
        x=0;
	}
   /*  if(Ext.get('dtpTglAwalFilterHasilLab').dom.value > Ext.get('dtpTglAkhirFilterHasilLab').dom.value)
    {
        ShowPesanWarningLapRegisRadReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    } */
	if(Ext.getCmp('radioasal').getValue() === false && Ext.getCmp('radioasalBulan').getValue() === false)
    {
        ShowPesanWarningLapRegisRadReport('Periode belum dipilih','Warning');
        x=0;
    }
	if(Ext.getCmp('Shift_All_LapRegisRad').getValue() === false && Ext.getCmp('Shift_1_LapRegisRad').getValue() === false && Ext.getCmp('Shift_2_LapRegisRad').getValue() === false && Ext.getCmp('Shift_3_LapRegisRad').getValue() === false){
		ShowPesanWarningLapRegisRadReport('Shift Belum Dipilih','Warning');
        x=0;
	}

    return x;
};

function ShowPesanWarningLapRegisRadReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapLapRegisRad_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 105,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Asal Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapRegisRad(),
           
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanLapRegisRadKelompokPasien(),
                mComboPerseoranganLapRegisRad(),
                mComboAsuransiLapRegisRad(),
                mComboPerusahaanLapRegisRad(),
                mComboUmumLapRegisRad()
            ]
        }]
    };
    return items;
};


function getItemLapLapRegisRad_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  345,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getItemLapLapRegisRad_Samping()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  345,
				height: 50,
				anchor: '100% 100%',
				defaultType: 'checkbox',
				items: 
				[
				
					{
						x: 40,
						y: 10,
						boxLabel: 'Semua',
						name: 'Shift_All_LapRegisRad',
						id : 'Shift_All_LapRegisRad',
						handler: function (field, value) {
							if (value === true){
								Ext.getCmp('Shift_1_LapRegisRad').setValue(true);
								Ext.getCmp('Shift_2_LapRegisRad').setValue(true);
								Ext.getCmp('Shift_3_LapRegisRad').setValue(true);
								Ext.getCmp('Shift_1_LapRegisRad').disable();
								Ext.getCmp('Shift_2_LapRegisRad').disable();
								Ext.getCmp('Shift_3_LapRegisRad').disable();
							}else{
								Ext.getCmp('Shift_1_LapRegisRad').setValue(false);
								Ext.getCmp('Shift_2_LapRegisRad').setValue(false);
								Ext.getCmp('Shift_3_LapRegisRad').setValue(false);
								Ext.getCmp('Shift_1_LapRegisRad').enable();
								Ext.getCmp('Shift_2_LapRegisRad').enable();
								Ext.getCmp('Shift_3_LapRegisRad').enable();
							}
						}
					},
					{
						x: 110,
						y: 10,
						boxLabel: 'Shift 1',
						name: 'Shift_1_LapRegisRad',
						id : 'Shift_1_LapRegisRad'
					},
					{
						x: 180,
						y: 10,
						boxLabel: 'Shift 2',
						name: 'Shift_2_LapRegisRad',
						id : 'Shift_2_LapRegisRad'
					},
					{
						x: 250,
						y: 10,
						boxLabel: 'Shift 3',
						name: 'Shift_3_LapRegisRad',
						id : 'Shift_3_LapRegisRad'
					}
				]
			}
        ]
            
    };
    return items;
};

function getItemLapLapRegisRad_Bawah()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  345,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'radio',
                id:'radioasal',
                handler: function (field, value) 
                {
					if (value === true)
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }
                }
            }, {
                x: 30,
                y: 10,
                xtype: 'label',
                text: ' Transaksi Pada Tanggal'
            }, 
			{
                x: 30,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilLab',
                format: 'd/M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 30,
                xtype: 'label',
                text: ' s/d Tanggal'
            }, 
			{
                x: 205,
                y: 30,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilLab',
                format: 'd/M/Y',
                value: now,
                width: 100
            },
            {
                x: 10,
                y: 80,
                xtype: 'radio',
                id:'radioasalBulan',
                handler: function (field, value) 
                {if (value === true)
                    {
                        Ext.getCmp('radioasal').setValue(false);
                        Ext.getCmp('dtpTglAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').enable();
                    }else
                    {
                        Ext.getCmp('radioasalBulan').setValue(false);
                        Ext.getCmp('dtpBulanAwalFilterHasilLab').disable();
                        Ext.getCmp('dtpBulanAkhirFilterHasilLab').disable();
                        Ext.getCmp('dtpTglAwalFilterHasilLab').enable();
                        Ext.getCmp('dtpTglAkhirFilterHasilLab').enable();
                    }
                }
            }, 
			{
                x: 30,
                y: 80,
                xtype: 'label',
                text: ' Transaksi Pada Bulan'
            }, 
			{
                x: 30,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAwalFilterHasilLab',
                format: 'M/Y',
                value: now
            }, 
			{
                x: 140,
                y: 100,
                xtype: 'label',
                text: ' s/d Bulan'
            }, 
			{
                x: 205,
                y: 100,
                xtype: 'datefield',
                id: 'dtpBulanAkhirFilterHasilLab',
                format: 'M/Y',
                value: now,
                width: 100
            }
            ]
        }]
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectSetAsuransi;
var selectsetnamaAsuransi;
var selectsetnamaperusahaan;

function loadDataComboUserLapRegisRad(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/lab/lap_laboratorium/getUser",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUserRequestEntryLapRegisRad.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_User_LapRegisRad.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_User_LapRegisRad.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboPilihanLapRegisRad()
{
    var cboPilihanLapRegisRad = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanLapRegisRad',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ'],[3, 'RWI'], [4, 'IGD']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanLapRegisRad;
};

function mComboPilihanLapRegisRadKelompokPasien()
{
    var cboPilihanLapRegisRadkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 40,
                id:'cboPilihanLapRegisRadkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 200,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanLapRegisRadkelompokPasien;
};

function mComboPerseoranganLapRegisRad()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganLapRegisRad = new WebApp.DataStore({fields: Field});
    dsPerseoranganLapRegisRad.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganLapRegisRad = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPerseoranganLapRegisRad',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:200,
                store: dsPerseoranganLapRegisRad,
				/* new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ), */
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganLapRegisRad;
};

function mComboUmumLapRegisRad()
{
    var cboUmumLapRegisRad = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboUmumLapRegisRad',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:200,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumLapRegisRad;
};

function mComboPerusahaanLapRegisRad()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=1 ORDER BY CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryLapRegisRad = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
		    id: 'cboPerusahaanRequestEntryLapRegisRad',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:200,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLapRegisRad;
};

function mComboAsuransiLapRegisRad()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomerLab',
                param: "jenis_cust=2 ORDER BY CUSTOMER"
            }
        }
    );
    var cboAsuransiLapRegisRad = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboAsuransiLapRegisRad',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:200,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi=b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiLapRegisRad;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganLapRegisRad').show();
        Ext.getCmp('cboAsuransiLapRegisRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapRegisRad').hide();
        Ext.getCmp('cboUmumLapRegisRad').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganLapRegisRad').hide();
        Ext.getCmp('cboAsuransiLapRegisRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapRegisRad').show();
        Ext.getCmp('cboUmumLapRegisRad').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganLapRegisRad').hide();
        Ext.getCmp('cboAsuransiLapRegisRad').show();
        Ext.getCmp('cboPerusahaanRequestEntryLapRegisRad').hide();
        Ext.getCmp('cboUmumLapRegisRad').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganLapRegisRad').hide();
        Ext.getCmp('cboAsuransiLapRegisRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapRegisRad').hide();
        Ext.getCmp('cboUmumLapRegisRad').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganLapRegisRad').hide();
        Ext.getCmp('cboAsuransiLapRegisRad').hide();
        Ext.getCmp('cboPerusahaanRequestEntryLapRegisRad').hide();
        Ext.getCmp('cboUmumLapRegisRad').show();
   }
}

function mComboUserLapRegisRad()
{
    var Field = ['kd_user','full_name'];
    ds_User_LapRegisRad = new WebApp.DataStore({fields: Field});
    cboUserRequestEntryLapRegisRad = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 40,
            id: 'cboUserRequestEntryLapRegisRad',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih User...',
            align: 'Right',
			value:'Semua',
            store: ds_User_LapRegisRad,
            valueField: 'kd_user',
            displayField: 'full_name',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectSetuser=b.data.kd_user;
				} 
			}
        }
    )

    return cboUserRequestEntryLapRegisRad;
};
