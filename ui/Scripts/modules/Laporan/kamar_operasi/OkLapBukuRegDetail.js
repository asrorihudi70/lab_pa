
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsOkRegisDet;
var selectNamaOkRegisDet;
var now = new Date();
var selectSetPerseorangan;
var frmDlgOkRegisDet;
var varLapOkRegisDet= ShowFormLapOkRegisDet();
var selectSetUmum;
var selectSetkelpas;
var strShift;
function ShowFormLapOkRegisDet()
{
    frmDlgOkRegisDet= fnDlgOkRegisDet();
    frmDlgOkRegisDet.show();
};

function fnDlgOkRegisDet()
{
    var winOkRegisDetReport = new Ext.Window
    (
        {
            id: 'winOkRegisDetReport',
            title: 'Laporan Buku Register Detail',
            closeAction: 'destroy',
            width:400,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgOkRegisDet()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganRegisLab').hide();
                Ext.getCmp('cboAsuransiRegisLab').hide();
                Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
                Ext.getCmp('cboUmumRegisLab').show();
            }
        }
        }
    );

    return winOkRegisDetReport;
};


function ItemDlgOkRegisDet()
{
    var PnlLapOkRegisDet = new Ext.Panel
    (
        {
            id: 'PnlLapOkRegisDet',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapOkRegisDet_Atas(),
                getItemLapOkRegisDet_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: 'OK',
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapOkRegisDet',
                            handler: function()
                            {
                               if (ValidasiReportOkRegisDet() === 1)
                               {
                                        //var tmppilihan = getKodeReportOkRegisDet();
                                       // var criteria = GetCriteriaOkRegisDet();
									   var strKriteria;
                                       if (Ext.get('dtpTglAwalFilterLapRegisOk').getValue() !== '')
										{
											strKriteria = Ext.get('dtpTglAwalFilterLapRegisOk').getValue();
										}
										if (Ext.get('dtpTglAkhirFilterLapRegisOk').getValue() !== '')
										{
											strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterLapRegisOk').getValue();
										}
										//[[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
										if(Ext.getCmp('cboPilihanOkRegisDet').getValue() !== '')
										{
											if (Ext.get('cboPilihanOkRegisDet').getValue()=== 'Semua'){
												strKriteria += '##@@##' + 'Pasien'
												strKriteria += '##@@##' + Ext.get('cboPilihanOkRegisDet').getValue()
											} else if (Ext.get('cboPilihanOkRegisDet').getValue()=== 'RWJ/IGD'){
												strKriteria += '##@@##' + 'Pasien'
												strKriteria += '##@@##' + Ext.get('cboPilihanOkRegisDet').getValue()
											} else if (Ext.get('cboPilihanOkRegisDet').getValue()=== 'RWI'){
												strKriteria += '##@@##' + 'Pasien'
												strKriteria += '##@@##' + Ext.get('cboPilihanOkRegisDet').getValue()
											} 
										}
										   
										if (Ext.getCmp('cboPilihanOkRegisDetkelompokPasien').getValue() !== '')
										{
											if (Ext.get('cboPilihanOkRegisDetkelompokPasien').getValue() === 'Semua')
											{
												strKriteria += '##@@##' + 'Semua';
												strKriteria += '##@@##' + 'NULL';
												strKriteria += '##@@##' + 'NULL';
											} else if (Ext.get('cboPilihanOkRegisDetkelompokPasien').getValue() === 'Perseorangan'){

												strKriteria += '##@@##' + 'Perseorangan';
												strKriteria += '##@@##' + Ext.get('cboPerseoranganRegisLab').getValue();
												strKriteria += '##@@##' + Ext.getCmp('cboPerseoranganRegisLab').getValue();
											} else if (Ext.get('cboPilihanOkRegisDetkelompokPasien').getValue() === 'Perusahaan'){
												
												if(Ext.getCmp('cboPerusahaanRequestEntryRegisLab').getValue() === '')
												{
													strKriteria += '##@@##' + 'Perusahaan';
													strKriteria += '##@@##' + 'NULL';
												}else{
													strKriteria += '##@@##' + 'Perusahaan';
													strKriteria += '##@@##' + selectsetnamaperusahaan;
													strKriteria += '##@@##' + selectsetperusahaan;
												}
											} else {
												
												if(Ext.getCmp('cboAsuransiRegisLab').getValue() === '')
												{
													strKriteria += '##@@##' + 'Asuransi';
													strKriteria += '##@@##' + 'NULL';
												}else{
													strKriteria += '##@@##' + 'Asuransi';
													strKriteria += '##@@##' + selectsetnamaAsuransi;
													strKriteria += '##@@##' + selectSetAsuransi;
												}
												
											} 
										}   
										if (Ext.getCmp('Shift_All_OkRegisDet').getValue() === true)
										{
											strKriteria += '##@@##' + 'shift1';
											strKriteria += '##@@##' + 1;
											strKriteria += '##@@##' + 'shift2';
											strKriteria += '##@@##' + 2;
											strKriteria += '##@@##' + 'shift3';
											strKriteria += '##@@##' + 3;
										}else{
											if (Ext.getCmp('Shift_1_OkRegisDet').getValue() === true)
											{
												strKriteria += '##@@##' + 'shift1';
												strKriteria += '##@@##' + 1;
											}
											if (Ext.getCmp('Shift_2_OkRegisDet').getValue() === true)
											{
												strKriteria += '##@@##' + 'shift2';
												strKriteria += '##@@##' + 2;
											}
											if (Ext.getCmp('Shift_3_OkRegisDet').getValue() === true)
											{
												strKriteria += '##@@##' + 'shift3';
												strKriteria += '##@@##' + 3;
											}
										}
										/* if (Ext.getCmp('Shift_All_OkRegisDet').getValue() === true)
										{
											
											strShift += 1;
											strShift += ','+2+',';
											strShift += 3;
										}else{
											if (Ext.getCmp('Shift_1_OkRegisDet').getValue() === true)
											{
												strShift += 1;
											}
											if (Ext.getCmp('Shift_2_OkRegisDet').getValue() === true)
											{
												strShift += ','+2+',';
											}
											if (Ext.getCmp('Shift_3_OkRegisDet').getValue() === true)
											{
												strShift += 3;
											}
										} */
										loadMask.show();
                                        var params={tglAwal:Ext.getCmp('dtpTglAwalFilterLapRegisOk').getValue(),
													tglAkhir:Ext.getCmp('dtpTglAkhirFilterLapRegisOk').getValue(),
													kriteria:strKriteria
										} ;
										var form = document.createElement("form");
										form.setAttribute("method", "post");
										form.setAttribute("target", "_blank");
										form.setAttribute("action", baseURL + "index.php/kamar_operasi/lap_BukuRegisterDetailOperasi/cetak");
										var hiddenField = document.createElement("input");
										hiddenField.setAttribute("type", "hidden");
										hiddenField.setAttribute("name", "data");
										hiddenField.setAttribute("value", Ext.encode(params));
										form.appendChild(hiddenField);
										document.body.appendChild(form);
										form.submit();	
										loadMask.hide();
										
								};
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Close' ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapOkRegisDet',
                            handler: function()
                            {
                                    frmDlgOkRegisDet.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapOkRegisDet;
};



function getKodeReportOkRegisDet()
{   var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanOkRegisDet').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanOkRegisDet').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportOkRegisDet()
{
    var x=1;
	if(Ext.getCmp('cboPilihanOkRegisDet').getValue() === ''){
		ShowPesanWarningOkRegisDetReport('Pasien Belum Dipilih','Laporan Buku Register Detail');
        x=0;
	}
    /* if(Ext.get('dtpTglAwalFilterLapRegisLab').dom.value > Ext.get('dtpTglAkhirFilterLapRegisLab').dom.value)
    {
        ShowPesanWarningOkRegisDetReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    } */
	if(Ext.getCmp('cboPilihanOkRegisDetkelompokPasien').getValue() === ''){
		ShowPesanWarningOkRegisDetReport('Kelompok Pasien Belum Dipilih','Laporan Buku Register Detail');
        x=0;
	}
	if(Ext.get('cboPerusahaanRequestEntryRegisLab').getValue() === '' &&  Ext.get('cboAsuransiRegisLab').getValue() === '' &&  Ext.get('cboPerseoranganRegisLab').getValue() === '' && Ext.get('cboUmumRegisLab').getValue() === ''){
		ShowPesanWarningOkRegisDetReport('Sub Kelompok Pasien Belum Dipilih','Laporan Buku Register Detail');
        x=0;
	}
	if(Ext.getCmp('Shift_All_OkRegisDet').getValue() === false && Ext.getCmp('Shift_1_OkRegisDet').getValue() === false && Ext.getCmp('Shift_2_OkRegisDet').getValue() === false && Ext.getCmp('Shift_3_OkRegisDet').getValue() === false){
		ShowPesanWarningOkRegisDetReport(nmRequesterRequest,'Laporan Buku Register Detail');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningOkRegisDetReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapOkRegisDet_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 135,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanOkRegisDet(),
            {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterLapRegisOk',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 40,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 40,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterLapRegisOk',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanOkRegisDetKelompokPasien(),
                mComboUmumOkRegisDet(),
				mComboPerseoranganOkRegisDet(),
                mComboAsuransiOkRegisDet(),
                mComboPerusahaanOkRegisDet()
                
            ]
        }]
    };
    return items;
};

function getItemLapOkRegisDet_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {
										boxLabel: 'Semua',
										name: 'Shift_All_OkRegisDet',
										id : 'Shift_All_OkRegisDet',
										checked:true,
										handler: function (field, value) {
											if (value === true){
												Ext.getCmp('Shift_1_OkRegisDet').setValue(true);
												Ext.getCmp('Shift_2_OkRegisDet').setValue(true);
												Ext.getCmp('Shift_3_OkRegisDet').setValue(true);
												Ext.getCmp('Shift_1_OkRegisDet').disable();
												Ext.getCmp('Shift_2_OkRegisDet').disable();
												Ext.getCmp('Shift_3_OkRegisDet').disable();
											}else{
													Ext.getCmp('Shift_1_OkRegisDet').setValue(false);
													Ext.getCmp('Shift_2_OkRegisDet').setValue(false);
													Ext.getCmp('Shift_3_OkRegisDet').setValue(false);
													Ext.getCmp('Shift_1_OkRegisDet').enable();
													Ext.getCmp('Shift_2_OkRegisDet').enable();
													Ext.getCmp('Shift_3_OkRegisDet').enable();
												}
										}
									},
                                    {
										boxLabel: 'Shift 1',
										name: 'Shift_1_OkRegisDet',
										id : 'Shift_1_OkRegisDet',
										checked:true,
										disabled:true,
										handler: function (field, value) {
											
										}
									},
                                    {
										boxLabel: 'Shift 2',
										name: 'Shift_2_OkRegisDet',
										id : 'Shift_2_OkRegisDet',
										checked:true,
										disabled:true,
										handler: function (field, value) {
											
										}
									},
                                    {
										boxLabel: 'Shift 3',
										name: 'Shift_3_OkRegisDet',
										id : 'Shift_3_OkRegisDet',
										checked:true,
										disabled:true,
										handler: function (field, value) {
											
										}
									}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;

function mComboPilihanOkRegisDet()
{
    var cboPilihanOkRegisDet = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanOkRegisDet',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanOkRegisDet;
};

function mComboPilihanOkRegisDetKelompokPasien()
{
    var cboPilihanOkRegisDetkelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPilihanOkRegisDetkelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanOkRegisDetkelompokPasien;
};

function mComboPerseoranganOkRegisDet()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganRegisLab = new WebApp.DataStore({fields: Field});
    dsPerseoranganRegisLab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganRegisLab = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 100,
                id:'cboPerseoranganRegisLab',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: dsPerseoranganRegisLab,
				/* new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ), */
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganRegisLab;
};

function mComboUmumOkRegisDet()
{
    var cboUmumRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboUmumRegisLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumRegisLab;
};

function mComboPerusahaanOkRegisDet()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=1 order by CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
		    id: 'cboPerusahaanRequestEntryRegisLab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRegisLab;
};

function mComboAsuransiOkRegisDet()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomerLab',
                param: "jenis_cust=2 order by CUSTOMER"
            }
        }
    );
    var cboAsuransiRegisLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'cboAsuransiRegisLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiRegisLab;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganRegisLab').show();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').show();
        Ext.getCmp('cboUmumRegisLab').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').show();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganRegisLab').hide();
        Ext.getCmp('cboAsuransiRegisLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryRegisLab').hide();
        Ext.getCmp('cboUmumRegisLab').show();
   }
}

function mCombounitOkRegisDet()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryOkRegisDet = new Ext.form.ComboBox
    (
        {
            id: 'cbounitRequestEntryOkRegisDet',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                }
                    
                }
        }
    )

    return cbounitRequestEntryOkRegisDet;
};
