
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapJadwalDokterOperasi_Ok;
var selectNamaLapJadwalDokterOperasi_Ok;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapJadwalDokterOperasi_Ok;
var varLapJadwalDokterOperasi_Ok= ShowFormLapJadwalDokterOperasi_Ok();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var jenis='1';

function ShowFormLapJadwalDokterOperasi_Ok()
{
    frmDlgLapJadwalDokterOperasi_Ok= fnDlgLapJadwalDokterOperasi_Ok();
    frmDlgLapJadwalDokterOperasi_Ok.show();
};

function fnDlgLapJadwalDokterOperasi_Ok()
{
    var winLapJadwalDokterOperasi_OkReport = new Ext.Window
    (
        {
            id: 'winLapJadwalDokterOperasi_OkReport',
            title: 'Laporan Jadwal Dokter Operasi',
            closeAction: 'destroy',
            width: 405,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapJadwalDokterOperasi_Ok()],
			fbar:
			[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkLapJadwalDokterOperasi_Ok',
					handler: function()
					{
						var params={
							tglAwal:Ext.getCmp('dtpTglAwalRekapPerDokOp_Ok').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirRekapPerDokOp_Ok').getValue(),
							kdUnit:"ok_default_kamar",
							kdRuang:Ext.getCmp('cbo_RuangLapJadwalPemakaianRuangan').getValue(),
							jenisCek:jenis
						};
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/kamar_operasi/lap_JadwalDokterOperasi/cetakJadwalDokterOperasi");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();
						frmDlgLapJadwalDokterOperasi_Ok.close();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapJadwalDokterOperasi_Ok',
					handler: function()
					{
							frmDlgLapJadwalDokterOperasi_Ok.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapJadwalDokterOperasi_OkReport;
};


function ItemDlgLapJadwalDokterOperasi_Ok()
{
    var PnlLapJadwalDokterOperasi_Ok = new Ext.Panel
    (
        {
            id: 'PnlLapJadwalDokterOperasi_Ok',
            fileUpload: true,
            layout: 'form',
            height: '450',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					xtype: 'fieldset',
					title: '',
					items: 
					[
						getItemLapJadwalDokterOperasi_Ok_KelPasien(),
						getItemLapJadwalDokterOperasi_Ok_Tanggal(),
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 0 0 0' },
							style:{'margin-left':'30px','margin-top':'0px'},
							anchor: '94%',
							layoutConfig:
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							}
						}
					]
			}
            ]
        }
    );

    return PnlLapJadwalDokterOperasi_Ok;
};

function GetCriteriaLapJadwalDokterOperasi_Ok()
{
	var strKriteria = '';
	
	/* strKriteria = '';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalRekapPerDokOp_Ok').getValue();
	strKriteria += '##@@##' + Ext.get('dtpTglAkhirRekapPerDokOp_Ok').getValue();
	
	if (Ext.get('cboPilihanLapJadwalDokterOperasi_Ok').getValue() !== '' || Ext.get('cboPilihanLapJadwalDokterOperasi_Ok').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'asal pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanLapJadwalDokterOperasi_Ok').getValue();
	}
	if (Ext.get('cboUnitFarLapJadwalDokterOperasi_Ok').getValue() !== '' || Ext.get('cboUnitFarLapJadwalDokterOperasi_Ok').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + Ext.get('cboUnitFarLapJadwalDokterOperasi_Ok').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitFarLapJadwalDokterOperasi_Ok').getValue();
	}
	if (Ext.get('cboUserRequestEntryLapJadwalDokterOperasi_Ok').getValue() !== ''|| Ext.get('cboUserRequestEntryLapJadwalDokterOperasi_Ok').getValue() !== 'Pilih User...'){
		strKriteria += '##@@##' + 'operator';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryLapJadwalDokterOperasi_Ok').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryLapJadwalDokterOperasi_Ok').getValue();
	}
	
	if (Ext.getCmp('Shift_All_LapJadwalDokterOperasi_Ok').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_LapJadwalDokterOperasi_Ok').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_LapJadwalDokterOperasi_Ok').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_LapJadwalDokterOperasi_Ok').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	} */
	
	
	return strKriteria;
};

function ValidasiReportLapJadwalDokterOperasi_Ok()
{
	var x=1;
	if(Ext.get('cboUnitFarLapJadwalDokterOperasi_Ok').getValue() === ''|| Ext.get('cboUnitFarLapJadwalDokterOperasi_Ok').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapJadwalDokterOperasi_OkReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapJadwalDokterOperasi_Ok').getValue() === '' || Ext.get('cboUserRequestEntryLapJadwalDokterOperasi_Ok').getValue() == 'Pilih User...'){
		ShowPesanWarningLapJadwalDokterOperasi_OkReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalRekapPerDokOp_Ok').dom.value > Ext.get('dtpTglAkhirRekapPerDokOp_Ok').dom.value)
    {
        ShowPesanWarningLapJadwalDokterOperasi_OkReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }

    return x;
};

function getItemLapJadwalDokterOperasi_Ok_KelPasien()
{
	radios = new Ext.form.RadioGroup({
	id :'radiosa',
	columns  :2,
	items: 
	[
		{
			id:'rbUmumJadwalPemakaian',
			name: 'rbKelompok',
			boxLabel: 'Rawat jalan / UGD', 
			inputValue: "1",
			checked: true,
		},
		{
			boxLabel: 'Rawat Inap',
			id:'rbPasienJadwalPemakaian', 
			name: 'rbKelompok',
			inputValue: "2"
		},
	],
	listeners: {
		change : function()
		{ 
			jenis= radios.getValue().inputValue;
		
		}
	}
		
   });
    var itemss = {
        layout: 'column',
        border: false,
        items: 
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  355,
				height: 60,
				items: [
					radios,
					{
						x: 10,
						y: 30,
						xtype: 'label',
						text: 'Ruang'
					}, 
					{
						x: 90,
						y: 30,
						xtype: 'label',
						text: ' : '
					},
					ComboRuangLapJadwalPemakaianRuangan(),
					{
                        xtype: 'checkbox',
                        id: 'CekSemuaRuangJadDokOp',
                        hideLabel:false,
                        //checked: false,
						x: 260,
						y: 30,
                        listeners: 
						{
							check: function()
							{
								if (Ext.getCmp('CekSemuaRuangJadDokOp').getValue()=== true)
								{
									Ext.getCmp('cbo_RuangLapJadwalPemakaianRuangan').setValue("");
									Ext.getCmp('cbo_RuangLapJadwalPemakaianRuangan').disable();
								}
								else
								{
									Ext.getCmp('cbo_RuangLapJadwalPemakaianRuangan').setValue("");
									Ext.getCmp('cbo_RuangLapJadwalPemakaianRuangan').enable();
								}
							}
						}
                    },
                    {
						xtype: 'tbtext',
						text: 'Semua ',  
						width: 190, 
						x:280,
						y:30
					},
				]
			}
		]
    };
    return itemss;
};

function getItemLapJadwalDokterOperasi_Ok_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  355,
				height: 30,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 0,
						xtype: 'label',
						text: 'Tanggal Operasi'
					}, 
					{
						x: 90,
						y: 0,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTglAwalRekapPerDokOp_Ok',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 0,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTglAkhirRekapPerDokOp_Ok',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};

function getItemLapJadwalDokterOperasi_Ok_RuangMakan()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Ruang Makan'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					
				]
			}
		]
    };
    return items;
};



function ComboRuangLapJadwalPemakaianRuangan()
{
    var Field_RuangJadwalPemakaianPasien = ['no_kamar', 'nama_kamar'];
    ds_RuangLapJadwalPemakaianRuangan = new WebApp.DataStore({fields: Field_RuangJadwalPemakaianPasien});
    ds_RuangLapJadwalPemakaianRuangan.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sortdir: 'ASC',
					target: 'vKamarOperasi',
					param: ""
				}
		}
	);
	
    var cbo_RuangLapJadwalPemakaianRuangan = new Ext.form.ComboBox
    (
        {
			x: 100,
			y: 30,
			id: 'cbo_RuangLapJadwalPemakaianRuangan',
			valueField: 'no_kamar',
            displayField: 'nama_kamar',
			store: ds_RuangLapJadwalPemakaianRuangan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_RuangLapJadwalPemakaianRuangan;
}

function ShowPesanWarningLapJadwalDokterOperasi_OkReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
