
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapJadwalDokterKamar_Ok;
var selectNamaLapJadwalDokterKamar_Ok;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapJadwalDokterKamar_Ok;
var varLapJadwalDokterKamar_Ok= ShowFormLapJadwalDokterKamar_Ok();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var jenis='1';

function ShowFormLapJadwalDokterKamar_Ok()
{
    frmDlgLapJadwalDokterKamar_Ok= fnDlgLapJadwalDokterKamar_Ok();
    frmDlgLapJadwalDokterKamar_Ok.show();
};

function fnDlgLapJadwalDokterKamar_Ok()
{
    var winLapJadwalDokterKamar_OkReport = new Ext.Window
    (
        {
            id: 'winLapJadwalDokterKamar_OkReport',
            title: 'Laporan Jadwal Pemakaian Kamar',
            closeAction: 'destroy',
            width: 405,
            height: 200,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapJadwalDokterKamar_Ok()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkLapJadwalDokterKamar_Ok',
					handler: function()
					{
						var params={
							tglAwal:Ext.getCmp('dtpTglAwalRekapPerKamarPakaiKamar_Ok').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirRekapPerKamarPakaiKamar_Ok').getValue(),
							kdUnit:"ok_default_kamar",
							kdRuang:Ext.getCmp('cbo_RuangLapJadwalDokterRuangan').getValue(),
							jenisCek:jenis
						};
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/kamar_operasi/lap_jadwalpemakaiankamar/cetakJadwalPemakaianKamar");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();
						frmDlgLapJadwalDokterKamar_Ok.close();
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapJadwalDokterKamar_Ok',
					handler: function()
					{
							frmDlgLapJadwalDokterKamar_Ok.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapJadwalDokterKamar_OkReport;
};


function ItemDlgLapJadwalDokterKamar_Ok()
{
    var PnlLapJadwalDokterKamar_Ok = new Ext.Panel
    (
        {
            id: 'PnlLapJadwalDokterKamar_Ok',
            fileUpload: true,
            layout: 'form',
            height: '450',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					xtype: 'fieldset',
					title: '',
					items: 
					[
						getItemLapJadwalDokterKamar_Ok_KelPasien(),
						getItemLapJadwalDokterKamar_Ok_Tanggal(),
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 0 0 0' },
							style:{'margin-left':'30px','margin-top':'0px'},
							anchor: '94%',
							layoutConfig:
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							}
						}
					]
			}
            ]
        }
    );

    return PnlLapJadwalDokterKamar_Ok;
};

function GetCriteriaLapJadwalDokterKamar_Ok()
{
	var strKriteria = '';
	
	/* strKriteria = '';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalRekapPerKamarPakaiKamar_Ok').getValue();
	strKriteria += '##@@##' + Ext.get('dtpTglAkhirRekapPerKamarPakaiKamar_Ok').getValue();
	
	if (Ext.get('cboPilihanLapJadwalDokterKamar_Ok').getValue() !== '' || Ext.get('cboPilihanLapJadwalDokterKamar_Ok').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'asal pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanLapJadwalDokterKamar_Ok').getValue();
	}
	if (Ext.get('cboUnitFarLapJadwalDokterKamar_Ok').getValue() !== '' || Ext.get('cboUnitFarLapJadwalDokterKamar_Ok').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + Ext.get('cboUnitFarLapJadwalDokterKamar_Ok').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitFarLapJadwalDokterKamar_Ok').getValue();
	}
	if (Ext.get('cboUserRequestEntryLapJadwalDokterKamar_Ok').getValue() !== ''|| Ext.get('cboUserRequestEntryLapJadwalDokterKamar_Ok').getValue() !== 'Pilih User...'){
		strKriteria += '##@@##' + 'operator';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryLapJadwalDokterKamar_Ok').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryLapJadwalDokterKamar_Ok').getValue();
	}
	
	if (Ext.getCmp('Shift_All_LapJadwalDokterKamar_Ok').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_LapJadwalDokterKamar_Ok').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_LapJadwalDokterKamar_Ok').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_LapJadwalDokterKamar_Ok').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	} */
	
	
	return strKriteria;
};

function ValidasiReportLapJadwalDokterKamar_Ok()
{
	var x=1;
	if(Ext.get('cboUnitFarLapJadwalDokterKamar_Ok').getValue() === ''|| Ext.get('cboUnitFarLapJadwalDokterKamar_Ok').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapJadwalDokterKamar_OkReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapJadwalDokterKamar_Ok').getValue() === '' || Ext.get('cboUserRequestEntryLapJadwalDokterKamar_Ok').getValue() == 'Pilih User...'){
		ShowPesanWarningLapJadwalDokterKamar_OkReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalRekapPerKamarPakaiKamar_Ok').dom.value > Ext.get('dtpTglAkhirRekapPerKamarPakaiKamar_Ok').dom.value)
    {
        ShowPesanWarningLapJadwalDokterKamar_OkReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }

    return x;
};

function getItemLapJadwalDokterKamar_Ok_KelPasien()
{
	radios = new Ext.form.RadioGroup({
	id :'radiosa',
	columns  :2,
	items: 
	[
		{
			id:'rbUmumJadwalDokter',
			name: 'rbKelompok',
			boxLabel: 'Rawat jalan / UGD', 
			inputValue: "1",
			checked: true,
		},
		{
			boxLabel: 'Rawat Inap',
			id:'rbPasienJadwalDokter', 
			name: 'rbKelompok',
			inputValue: "2"
		},
	],
	listeners: {
		change : function()
		{ 
			jenis= radios.getValue().inputValue;
		
		}
	}
		
   });
    var itemss = {
        layout: 'column',
        border: false,
        items: 
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  355,
				height: 60,
				items: [
					radios,
					{
						x: 10,
						y: 30,
						xtype: 'label',
						text: 'Ruang'
					}, 
					{
						x: 90,
						y: 30,
						xtype: 'label',
						text: ' : '
					},
					ComboRuangLapJadwalDokterRuangan(),
					{
                        xtype: 'checkbox',
                        id: 'CekSemuaRuangJadPemakaianOp',
                        hideLabel:false,
                        //checked: false,
						x: 260,
						y: 30,
                        listeners: 
						{
							check: function()
							{
								if (Ext.getCmp('CekSemuaRuangJadPemakaianOp').getValue()=== true)
								{
									Ext.getCmp('cbo_RuangLapJadwalDokterRuangan').setValue("");
									Ext.getCmp('cbo_RuangLapJadwalDokterRuangan').disable();
								}
								else
								{
									Ext.getCmp('cbo_RuangLapJadwalDokterRuangan').setValue("");
									Ext.getCmp('cbo_RuangLapJadwalDokterRuangan').enable();
								}
							}
						}
                    },
                    {
						xtype: 'tbtext',
						text: 'Semua ',  
						width: 190, 
						x:280,
						y:30
					},
				]
			}
		]
    };
    return itemss;
};

function getItemLapJadwalDokterKamar_Ok_Tanggal()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  355,
				height: 30,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 0,
						xtype: 'label',
						text: 'Tanggal Operasi'
					}, 
					{
						x: 90,
						y: 0,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTglAwalRekapPerKamarPakaiKamar_Ok',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 0,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTglAkhirRekapPerKamarPakaiKamar_Ok',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};

function getItemLapJadwalDokterKamar_Ok_RuangMakan()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Ruang Makan'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					
				]
			}
		]
    };
    return items;
};



function ComboRuangLapJadwalDokterRuangan()
{
    var Field_RuangJadwalDokterPasien = ['no_kamar', 'nama_kamar'];
    ds_RuangLapJadwalDokterRuangan = new WebApp.DataStore({fields: Field_RuangJadwalDokterPasien});
    ds_RuangLapJadwalDokterRuangan.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sortdir: 'ASC',
					target: 'vKamarOperasi',
					param: ""
				}
		}
	);
	
    var cbo_RuangLapJadwalDokterRuangan = new Ext.form.ComboBox
    (
        {
			x: 100,
			y: 30,
			id: 'cbo_RuangLapJadwalDokterRuangan',
			valueField: 'no_kamar',
            displayField: 'nama_kamar',
			store: ds_RuangLapJadwalDokterRuangan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			valueIndex:1,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_RuangLapJadwalDokterRuangan;
}

function ShowPesanWarningLapJadwalDokterKamar_OkReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
