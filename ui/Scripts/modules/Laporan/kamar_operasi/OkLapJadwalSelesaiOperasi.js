
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapJadwalSelesaiOperasi_Ok;
var selectNamaLapJadwalSelesaiOperasi_Ok;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapJadwalSelesaiOperasi_Ok;
var varLapJadwalSelesaiOperasi_Ok= ShowFormLapJadwalSelesaiOperasi_Ok();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var jenis='1';
var ds_RuangLapJadwalSelesaiIsiKelompokPasien;
function ShowFormLapJadwalSelesaiOperasi_Ok()
{
    frmDlgLapJadwalSelesaiOperasi_Ok= fnDlgLapJadwalSelesaiOperasi_Ok();
    frmDlgLapJadwalSelesaiOperasi_Ok.show();
};

function fnDlgLapJadwalSelesaiOperasi_Ok()
{
    var winLapJadwalSelesaiOperasi_OkReport = new Ext.Window
    (
        {
            id: 'winLapJadwalSelesaiOperasi_OkReport',
            title: 'Laporan Jadwal Selesai Operasi',
            closeAction: 'destroy',
            width: 405,
            height: 250,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapJadwalSelesaiOperasi_Ok()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkLapJadwalSelesaiOperasi_Ok',
					handler: function()
					{
						var params={
							tglAwal:Ext.getCmp('dtpTglAwalRekapPeruang_Ok').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirRekapPeruang_Ok').getValue(),
							kdUnit:"ok_default_kamar",
							kdRuang:Ext.getCmp('cbo_RuangLapJadwalSelesaiRuangan').getValue(),
							kdCust:Ext.getCmp('cbo_RuangLapJadwalSelesaiIsiKelompokPasien').getValue(),
							jenisCek:jenis
						};
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/kamar_operasi/lap_JadwalSelesaiOperasi/cetakJadwalSelesaiOperasi");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();
						frmDlgLapJadwalSelesaiOperasi_Ok.close();
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapJadwalSelesaiOperasi_Ok',
					handler: function()
					{
							frmDlgLapJadwalSelesaiOperasi_Ok.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapJadwalSelesaiOperasi_OkReport;
};


function ItemDlgLapJadwalSelesaiOperasi_Ok()
{
    var PnlLapJadwalSelesaiOperasi_Ok = new Ext.Panel
    (
        {
            id: 'PnlLapJadwalSelesaiOperasi_Ok',
            fileUpload: true,
            layout: 'form',
            height: '520',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
				{
					xtype: 'fieldset',
					title: '',
					items: 
					[
						getItemLapJadwalSelesaiOperasi_Ok_KelPasien(),
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 0 0 0' },
							style:{'margin-left':'30px','margin-top':'0px'},
							anchor: '94%',
							layoutConfig:
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							}
						}
					]
			}
            ]
        }
    );

    return PnlLapJadwalSelesaiOperasi_Ok;
};

function GetCriteriaLapJadwalSelesaiOperasi_Ok()
{
	var strKriteria = '';
	
	/* strKriteria = '';
	strKriteria += '##@@##' + Ext.get('dtpTglAwalRekapPeruang_Ok').getValue();
	strKriteria += '##@@##' + Ext.get('dtpTglAkhirRekapPeruang_Ok').getValue();
	
	if (Ext.get('cboPilihanLapJadwalSelesaiOperasi_Ok').getValue() !== '' || Ext.get('cboPilihanLapJadwalSelesaiOperasi_Ok').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'asal pasien';
		strKriteria += '##@@##' + Ext.get('cboPilihanLapJadwalSelesaiOperasi_Ok').getValue();
	}
	if (Ext.get('cboUnitFarLapJadwalSelesaiOperasi_Ok').getValue() !== '' || Ext.get('cboUnitFarLapJadwalSelesaiOperasi_Ok').getValue() !== 'Silahkan Pilih...'){
		strKriteria += '##@@##' + 'Unit';
		strKriteria += '##@@##' + Ext.get('cboUnitFarLapJadwalSelesaiOperasi_Ok').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUnitFarLapJadwalSelesaiOperasi_Ok').getValue();
	}
	if (Ext.get('cboUserRequestEntryLapJadwalSelesaiOperasi_Ok').getValue() !== ''|| Ext.get('cboUserRequestEntryLapJadwalSelesaiOperasi_Ok').getValue() !== 'Pilih User...'){
		strKriteria += '##@@##' + 'operator';
		strKriteria += '##@@##' + Ext.get('cboUserRequestEntryLapJadwalSelesaiOperasi_Ok').getValue();
		strKriteria += '##@@##' + Ext.getCmp('cboUserRequestEntryLapJadwalSelesaiOperasi_Ok').getValue();
	}
	
	if (Ext.getCmp('Shift_All_LapJadwalSelesaiOperasi_Ok').getValue() === true)
	{
		strKriteria += '##@@##' + 'shift1';
		strKriteria += '##@@##' + 1;
		strKriteria += '##@@##' + 'shift2';
		strKriteria += '##@@##' + 2;
		strKriteria += '##@@##' + 'shift3';
		strKriteria += '##@@##' + 3;
	}else{
		if (Ext.getCmp('Shift_1_LapJadwalSelesaiOperasi_Ok').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift1';
			strKriteria += '##@@##' + 1;
		}
		if (Ext.getCmp('Shift_2_LapJadwalSelesaiOperasi_Ok').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift2';
			strKriteria += '##@@##' + 2;
		}
		if (Ext.getCmp('Shift_3_LapJadwalSelesaiOperasi_Ok').getValue() === true)
		{
			strKriteria += '##@@##' + 'shift3';
			strKriteria += '##@@##' + 3;
		}
	} */
	
	
	return strKriteria;
};

function ValidasiReportLapJadwalSelesaiOperasi_Ok()
{
	var x=1;
	if(Ext.get('cboUnitFarLapJadwalSelesaiOperasi_Ok').getValue() === ''|| Ext.get('cboUnitFarLapJadwalSelesaiOperasi_Ok').getValue() == 'Silahkan Pilih...'){
		ShowPesanWarningLapJadwalSelesaiOperasi_OkReport('Unit Belum Dipilih','Warning');
        x=0;
	}
	if(Ext.get('cboUserRequestEntryLapJadwalSelesaiOperasi_Ok').getValue() === '' || Ext.get('cboUserRequestEntryLapJadwalSelesaiOperasi_Ok').getValue() == 'Pilih User...'){
		ShowPesanWarningLapJadwalSelesaiOperasi_OkReport('Operator Belum Dipilih','Warning');
        x=0;
	}
    if(Ext.get('dtpTglAwalRekapPeruang_Ok').dom.value > Ext.get('dtpTglAkhirRekapPeruang_Ok').dom.value)
    {
        ShowPesanWarningLapJadwalSelesaiOperasi_OkReport('Tanggal awal tidak boleh kurang dari tanggal akhir','Warning');
        x=0;
    }

    return x;
};

function getItemLapJadwalSelesaiOperasi_Ok_KelPasien()
{
/* 	radios = new Ext.form.RadioGroup({
	id :'radiosa',
	columns  :2,
	items: 
	[
		{
			id:'rbUmumJadwalSelesai',
			name: 'rbKelompok',
			boxLabel: 'Rawat jalan / UGD', 
			inputValue: "1",
			checked: true,
		},
		{
			boxLabel: 'Rawat Inap',
			id:'rbPasienJadwalSelesai', 
			name: 'rbKelompok',
			inputValue: "2"
		},
	],
	listeners: {
		change : function()
		{ 
			jenis= radios.getValue().inputValue;
		
		}
	} 
		
   });*/
    var itemss = {
        layout: 'column',
        border: false,
        items: 
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width:  355,
				height: 130,
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Ruang'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					ComboRuangLapJadwalSelesaiRuangan(),
					{
                        xtype: 'checkbox',
                        id: 'CekSemuaRuangJadSelesaiOp',
                        hideLabel:false,
                        //checked: false,
						x: 260,
						y: 10,
                        listeners: 
						{
							check: function()
							{
								if (Ext.getCmp('CekSemuaRuangJadSelesaiOp').getValue()=== true)
								{
									Ext.getCmp('cbo_RuangLapJadwalSelesaiRuangan').setValue("");
									Ext.getCmp('cbo_RuangLapJadwalSelesaiRuangan').disable();
								}
								else
								{
									Ext.getCmp('cbo_RuangLapJadwalSelesaiRuangan').setValue("");
									Ext.getCmp('cbo_RuangLapJadwalSelesaiRuangan').enable();
								}
							}
						}
                    },
                    {
						xtype: 'tbtext',
						text: 'Semua ',  
						width: 190, 
						x:280,
						y:10
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Tanggal Operasi'
					}, 
					{
						x: 90,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 40,
						xtype: 'datefield',
						id: 'dtpTglAwalRekapPeruang_Ok',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 40,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 40,
						xtype: 'datefield',
						id: 'dtpTglAkhirRekapPeruang_Ok',
						format: 'd/M/Y',
						value: now,
						width: 100
					},
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Kelompok Pasien',
					},
					{
						x: 90,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
					ComboLapJadwalSelesaiKelompokPasien(),
					ComboLapJadwalSelesaiIsiKelompokPasien()
				]
			}
		]
    };
    return itemss;
};


function ComboLapJadwalSelesaiKelompokPasien()
{
	
    var cbo_RuangLapJadwalSelesaiKelompokPasien = new Ext.form.ComboBox
    (
        {
			x: 100,
			y: 70,
			id: 'cbo_RuangLapJadwalSelesaiKelompokPasienan',
			valueField: 'Id',
			displayField: 'displayText',
			store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['Id', 'displayText'],
				data: [[0,'Semua',],[1, 'Perorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
			}),
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			value:0,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					if (c === 0)
					{
						Ext.getCmp('cbo_RuangLapJadwalSelesaiIsiKelompokPasien').setValue('Semua');
					}
					else{
						dsvRuangLapJadwalSelesaiIsiKelompokPasien(c-1);
						Ext.getCmp('cbo_RuangLapJadwalSelesaiIsiKelompokPasien').setValue('');
					}
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_RuangLapJadwalSelesaiKelompokPasien;
}
function dsvRuangLapJadwalSelesaiIsiKelompokPasien(jeniscust)
{
	ds_RuangLapJadwalSelesaiIsiKelompokPasien.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sortdir: 'ASC',
					target: 'vComboIsiKelompokPasienOK',
					param: 'kontraktor.jenis_cust='+jeniscust+' order by customer'
				}
		}
	);
}
function ComboLapJadwalSelesaiIsiKelompokPasien()
{
    var Field_RuangJadwalSelesaiPasien = ['kd_customer', 'customer'];
    ds_RuangLapJadwalSelesaiIsiKelompokPasien = new WebApp.DataStore({fields: Field_RuangJadwalSelesaiPasien});
    //dsvRuangLapJadwalSelesaiIsiKelompokPasien()
	
    var cbo_RuangLapJadwalSelesaiIsiKelompokPasien = new Ext.form.ComboBox
    (
        {
			x: 100,
			y: 100,
			id: 'cbo_RuangLapJadwalSelesaiIsiKelompokPasien',
			valueField: 'kd_customer',
            displayField: 'customer',
			store: ds_RuangLapJadwalSelesaiIsiKelompokPasien,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			value:'Semua',
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_RuangLapJadwalSelesaiIsiKelompokPasien;
}


function ComboRuangLapJadwalSelesaiRuangan()
{
    var Field_RuangJadwalSelesaiPasien = ['no_kamar', 'nama_kamar'];
    ds_RuangLapJadwalSelesaiRuangan = new WebApp.DataStore({fields: Field_RuangJadwalSelesaiPasien});
    ds_RuangLapJadwalSelesaiRuangan.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sortdir: 'ASC',
					target: 'vKamarOperasi',
					param: ""
				}
		}
	);
	
    var cbo_RuangLapJadwalSelesaiRuangan = new Ext.form.ComboBox
    (
        {
			x: 100,
			y: 10,
			id: 'cbo_RuangLapJadwalSelesaiRuangan',
			valueField: 'no_kamar',
            displayField: 'nama_kamar',
			store: ds_RuangLapJadwalSelesaiRuangan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_RuangLapJadwalSelesaiRuangan;
}

function ShowPesanWarningLapJadwalSelesaiOperasi_OkReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
