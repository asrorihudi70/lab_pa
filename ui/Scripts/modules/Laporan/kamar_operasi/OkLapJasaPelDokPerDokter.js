
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapJasaPelDokPerDokter;
var selectNamaLapJasaPelDokPerDokter;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapJasaPelDokPerDokter;
var varLapJasaPelDokPerDokter= ShowFormLapJasaPelDokPerDokter();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var jenis='1';
var kodeUnitLapPerDokter_Gz;
function ShowFormLapJasaPelDokPerDokter()
{
    frmDlgLapJasaPelDokPerDokter= fnDlgLapJasaPelDokPerDokter();
    frmDlgLapJasaPelDokPerDokter.show();
};
function dsLoadUnitLapJasaPelDokPerDokter(){
	dsLapJasaPelDokPerDokter.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vGzUnitLapPerDokter',
                    param : 'kd_bagian=1'
                }			
            }
        );   
    return dsLapJasaPelDokPerDokter;
	}

function fnDlgLapJasaPelDokPerDokter()
{
    var winLapJasaPelDokPerDokterReport = new Ext.Window
    (
        {
            id: 'winLapJasaPelDokPerDokterReport',
            title: 'Laporan Jasa Pelayanan Dokter Per Pasien',
            closeAction: 'destroy',
            width: 405,
            height: 160,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapJasaPelDokPerDokter()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkLapJasaPelDokPerDokter',
					handler: function()
					{	
					var params={
					//kdUnit:Ext.getCmp('CmbSetUnitLapJasaPelDokPerDokter').getValue(),
					tglAwal:Ext.getCmp('dtpTglAwalPerDokter_OK').getValue(),
					tglAkhir:Ext.getCmp('dtpTglAkhirPerDokter_OK').getValue(),
					} ;
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("target", "_blank");
					form.setAttribute("action", baseURL + "index.php/kamar_operasi/functionKamarOperasi/jasapelayanandokter");
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", "data");
					hiddenField.setAttribute("value", Ext.encode(params));
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();	
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapJasaPelDokPerDokter',
					handler: function()
					{
						frmDlgLapJasaPelDokPerDokter.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapJasaPelDokPerDokterReport;
};


function ItemDlgLapJasaPelDokPerDokter()
{
	radios = new Ext.form.RadioGroup({
	id :'radiosa',
	columns  :2,
	items: 
	[
		{
			id:'rbPdf',
			name: 'rbKelompok',
			boxLabel: 'PDF', 
			inputValue: "1",
			checked: true,
		},
		{
			boxLabel: 'Excel',
			id:'rbExl', 
			name: 'rbKelompok',
			inputValue: "2"
		},
	],
	listeners: {
		change : function()
		{ 
			jenis= radios.getValue().inputValue;
		
		}
	}
		
   });
    var PnlLapJasaPelDokPerDokter = new Ext.Panel
    (
        {
            id: 'PnlLapJasaPelDokPerDokter',
            fileUpload: true,
            layout: 'form',
            height: '250',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
		
				{
					
					xtype: 'fieldset',
					title: 'Periode',
					items: 
					[
						getItemLapJasaPelDokPerDokter_Tanggal(),
					]
			}
            ]
        }
    );

    return PnlLapJasaPelDokPerDokter;
};

function getItemLapJasaPelDokPerDokter_Tanggal()
{
	
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAwalPerDokter_OK',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 10,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAkhirPerDokter_OK',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};
function getItemLapJasaPelDokPerDokter_Combo()
{
    var itemss = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Unit/Ruang'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					comboUnit_ok_Combo()
				]
			}
		]
    };
    return itemss;
};


function ShowPesanWarningLapPerDokterMakananPerRuang_okReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
