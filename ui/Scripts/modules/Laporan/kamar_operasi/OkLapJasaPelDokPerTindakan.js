
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapJasaPelDokPertindakan;
var selectNamaLapJasaPelDokPertindakan;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapJasaPelDokPertindakan;
var varLapJasaPelDokPertindakan= ShowFormLapJasaPelDokPertindakan();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var jenis='1';
var kodeUnitLapRekap_Gz;
function ShowFormLapJasaPelDokPertindakan()
{
    frmDlgLapJasaPelDokPertindakan= fnDlgLapJasaPelDokPertindakan();
    frmDlgLapJasaPelDokPertindakan.show();
};
function dsLoadUnitLapJasaPelDokPertindakan(){
	dsLapJasaPelDokPertindakan.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vGzUnitLapRekap',
                    param : 'kd_bagian=1'
                }			
            }
        );   
    return dsLapJasaPelDokPertindakan;
	}

function fnDlgLapJasaPelDokPertindakan()
{
    var winLapJasaPelDokPertindakanReport = new Ext.Window
    (
        {
            id: 'winLapJasaPelDokPertindakanReport',
            title: 'Laporan Jasa Pelayanan Dokter Per Tindakan',
            closeAction: 'destroy',
            width: 405,
            height: 160,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapJasaPelDokPertindakan()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkLapJasaPelDokPertindakan',
					handler: function()
					{	
					var params={
					//kdUnit:Ext.getCmp('CmbSetUnitLapJasaPelDokPertindakan').getValue(),
					tglAwal:Ext.getCmp('dtpTglAwalRekap_OK').getValue(),
					tglAkhir:Ext.getCmp('dtpTglAkhirRekap_OK').getValue(),
					} ;
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("target", "_blank");
					form.setAttribute("action", baseURL + "index.php/kamar_operasi/functionKamarOperasi/jasapelayananperproduk");
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", "data");
					hiddenField.setAttribute("value", Ext.encode(params));
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();	
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapJasaPelDokPertindakan',
					handler: function()
					{
						frmDlgLapJasaPelDokPertindakan.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapJasaPelDokPertindakanReport;
};


function ItemDlgLapJasaPelDokPertindakan()
{
	radios = new Ext.form.RadioGroup({
	id :'radiosa',
	columns  :2,
	items: 
	[
		{
			id:'rbPdf',
			name: 'rbKelompok',
			boxLabel: 'PDF', 
			inputValue: "1",
			checked: true,
		},
		{
			boxLabel: 'Excel',
			id:'rbExl', 
			name: 'rbKelompok',
			inputValue: "2"
		},
	],
	listeners: {
		change : function()
		{ 
			jenis= radios.getValue().inputValue;
		
		}
	}
		
   });
    var PnlLapJasaPelDokPertindakan = new Ext.Panel
    (
        {
            id: 'PnlLapJasaPelDokPertindakan',
            fileUpload: true,
            layout: 'form',
            height: '250',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
		
				{
					
					xtype: 'fieldset',
					title: 'Periode',
					items: 
					[
						getItemLapJasaPelDokPertindakan_Tanggal(),
					]
			}
            ]
        }
    );

    return PnlLapJasaPelDokPertindakan;
};

function getItemLapJasaPelDokPertindakan_Tanggal()
{
	
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAwalRekap_OK',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 10,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAkhirRekap_OK',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};
function getItemLapJasaPelDokPertindakan_Combo()
{
    var itemss = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Unit/Ruang'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					comboUnit_ok_Combo()
				]
			}
		]
    };
    return itemss;
};


function ShowPesanWarningLapRekapMakananPerRuang_okReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
