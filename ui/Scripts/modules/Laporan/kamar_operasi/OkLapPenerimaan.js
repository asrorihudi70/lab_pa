
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsOkPenerimaan;
var selectNamaOkPenerimaan;
var now = new Date();
var selectSetPerseorangan;
var frmDlgOkPenerimaan;
var varLapOkPenerimaan= ShowFormLapOkPenerimaan();
var selectSetUmum;
var selectSetkelpas;
var strShift;
function ShowFormLapOkPenerimaan()
{
    frmDlgOkPenerimaan= fnDlgOkPenerimaan();
    frmDlgOkPenerimaan.show();
};

function fnDlgOkPenerimaan()
{
    var winOkPenerimaanReport = new Ext.Window
    (
        {
            id: 'winOkPenerimaanReport',
            title: 'Laporan Penerimaan ',
            closeAction: 'destroy',
            width:400,
            height: 220,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgOkPenerimaan()],
            listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboPerseoranganNerimaLab').hide();
                Ext.getCmp('cboAsuransiNerimaLab').hide();
                Ext.getCmp('cboPerusahaanRequestEntryNerimaLab').hide();
                Ext.getCmp('cboUmumNerimaLab').show();
            }
        }

        }
    );

    return winOkPenerimaanReport;
};


function ItemDlgOkPenerimaan()
{
    var PnlLapOkPenerimaan = new Ext.Panel
    (
        {
            id: 'PnlLapOkPenerimaan',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
                getItemLapOkPenerimaan_Atas(),
                getItemLapOkPenerimaan_Bawah(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapOkPenerimaan',
                            handler: function()
                            {
                               if (ValidasiReportOkPenerimaan() === 1)
                               {
                                        //var tmppilihan = getKodeReportOkPenerimaan();
                                       // var criteria = GetCriteriaOkPenerimaan();
									   var strKriteria;
                                       if (Ext.get('dtpTglAwalFilterLapNerimaOk').getValue() !== '')
										{
											strKriteria = Ext.get('dtpTglAwalFilterLapNerimaOk').getValue();
										}
										if (Ext.get('dtpTglAkhirFilterLapNerimaOk').getValue() !== '')
										{
											strKriteria += '##@@##' + Ext.get('dtpTglAkhirFilterLapNerimaOk').getValue();
										}
										//[[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI'], [4, 'Kunjungan Langsung']]
										if(Ext.getCmp('cboPilihanOkPenerimaan').getValue() !== '')
										{
											if (Ext.get('cboPilihanOkPenerimaan').getValue()=== 'Semua'){
												strKriteria += '##@@##' + 'Pasien'
												strKriteria += '##@@##' + Ext.get('cboPilihanOkPenerimaan').getValue()
											} else if (Ext.get('cboPilihanOkPenerimaan').getValue()=== 'RWJ/IGD'){
												strKriteria += '##@@##' + 'Pasien'
												strKriteria += '##@@##' + Ext.get('cboPilihanOkPenerimaan').getValue()
											} else if (Ext.get('cboPilihanOkPenerimaan').getValue()=== 'RWI'){
												strKriteria += '##@@##' + 'Pasien'
												strKriteria += '##@@##' + Ext.get('cboPilihanOkPenerimaan').getValue()
											} 
										}
										   
										if (Ext.getCmp('cboPilihanOkPenerimaankelompokPasien').getValue() !== '')
										{
											if (Ext.get('cboPilihanOkPenerimaankelompokPasien').getValue() === 'Semua')
											{
												strKriteria += '##@@##' + 'Semua';
												strKriteria += '##@@##' + 'NULL';
												strKriteria += '##@@##' + 'NULL';
											} else if (Ext.get('cboPilihanOkPenerimaankelompokPasien').getValue() === 'Perseorangan'){

												strKriteria += '##@@##' + 'Perseorangan';
												strKriteria += '##@@##' + Ext.get('cboPerseoranganNerimaLab').getValue();
												strKriteria += '##@@##' + Ext.getCmp('cboPerseoranganNerimaLab').getValue();
											} else if (Ext.get('cboPilihanOkPenerimaankelompokPasien').getValue() === 'Perusahaan'){
												
												if(Ext.getCmp('cboPerusahaanRequestEntryNerimaLab').getValue() === '')
												{
													strKriteria += '##@@##' + 'Perusahaan';
													strKriteria += '##@@##' + 'NULL';
												}else{
													strKriteria += '##@@##' + 'Perusahaan';
													strKriteria += '##@@##' + selectsetnamaperusahaan;
													strKriteria += '##@@##' + selectsetperusahaan;
												}
											} else {
												
												if(Ext.getCmp('cboAsuransiNerimaLab').getValue() === '')
												{
													strKriteria += '##@@##' + 'Asuransi';
													strKriteria += '##@@##' + 'NULL';
												}else{
													strKriteria += '##@@##' + 'Asuransi';
													strKriteria += '##@@##' + selectsetnamaAsuransi;
													strKriteria += '##@@##' + selectSetAsuransi;
												}
												
											} 
										}   
										if (Ext.getCmp('Shift_All_OkPenerimaan').getValue() === true)
										{
											strKriteria += '##@@##' + 'shift1';
											strKriteria += '##@@##' + 1;
											strKriteria += '##@@##' + 'shift2';
											strKriteria += '##@@##' + 2;
											strKriteria += '##@@##' + 'shift3';
											strKriteria += '##@@##' + 3;
										}else{
											if (Ext.getCmp('Shift_1_OkPenerimaan').getValue() === true)
											{
												strKriteria += '##@@##' + 'shift1';
												strKriteria += '##@@##' + 1;
											}
											if (Ext.getCmp('Shift_2_OkPenerimaan').getValue() === true)
											{
												strKriteria += '##@@##' + 'shift2';
												strKriteria += '##@@##' + 2;
											}
											if (Ext.getCmp('Shift_3_OkPenerimaan').getValue() === true)
											{
												strKriteria += '##@@##' + 'shift3';
												strKriteria += '##@@##' + 3;
											}
										}
										/* if (Ext.getCmp('Shift_All_OkPenerimaan').getValue() === true)
										{
											
											strShift += 1;
											strShift += ','+2+',';
											strShift += 3;
										}else{
											if (Ext.getCmp('Shift_1_OkPenerimaan').getValue() === true)
											{
												strShift += 1;
											}
											if (Ext.getCmp('Shift_2_OkPenerimaan').getValue() === true)
											{
												strShift += ','+2+',';
											}
											if (Ext.getCmp('Shift_3_OkPenerimaan').getValue() === true)
											{
												strShift += 3;
											}
										} */
										loadMask.show();
                                        var params={tglAwal:Ext.getCmp('dtpTglAwalFilterLapNerimaOk').getValue(),
													tglAkhir:Ext.getCmp('dtpTglAkhirFilterLapNerimaOk').getValue(),
													kriteria:strKriteria
										} ;
										var form = document.createElement("form");
										form.setAttribute("method", "post");
										form.setAttribute("target", "_blank");
										form.setAttribute("action", baseURL + "index.php/kamar_operasi/lap_PenerimaanOperasi/cetak");
										var hiddenField = document.createElement("input");
										hiddenField.setAttribute("type", "hidden");
										hiddenField.setAttribute("name", "data");
										hiddenField.setAttribute("value", Ext.encode(params));
										form.appendChild(hiddenField);
										document.body.appendChild(form);
										form.submit();	
										loadMask.hide();
										
								};
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapOkPenerimaan',
                            handler: function()
                            {
                                    frmDlgOkPenerimaan.close();
                            }
                        }
                    ]
                }
            ]
        }
    );

    return PnlLapOkPenerimaan;
};



function getKodeReportOkPenerimaan()
{   var tmppilihan = ' ';
    if (Ext.getCmp('cboPilihanOkPenerimaan').getValue() === 1)
    {
        tmppilihan = 'ref010206';
    }else if (Ext.getCmp('cboPilihanOkPenerimaan').getValue() === 2)
    {
        tmppilihan = 'ref010207';
    }
    return tmppilihan;
}

function ValidasiReportOkPenerimaan()
{
    var x=1;
	if(Ext.getCmp('cboPilihanOkPenerimaan').getValue() === ''){
		ShowPesanWarningOkPenerimaanReport('Pasien Belum Dipilih','Laporan Penerimaan ');
        x=0;
	}
    /* if(Ext.get('dtpTglAwalFilterLapNerimaLab').dom.value > Ext.get('dtpTglAkhirFilterLapNerimaLab').dom.value)
    {
        ShowPesanWarningOkPenerimaanReport('Tanggal awal tidak boleh kurang dari tanggal akhir',nmTitleFormDlgReqCMRpt);
        x=0;
    } */
	if(Ext.getCmp('cboPilihanOkPenerimaankelompokPasien').getValue() === ''){
		ShowPesanWarningOkPenerimaanReport('Kelompok Pasien Belum Dipilih','Laporan Penerimaan ');
        x=0;
	}
	if(Ext.get('cboPerusahaanRequestEntryNerimaLab').getValue() === '' &&  Ext.get('cboAsuransiNerimaLab').getValue() === '' &&  Ext.get('cboPerseoranganNerimaLab').getValue() === '' && Ext.get('cboUmumNerimaLab').getValue() === ''){
		ShowPesanWarningOkPenerimaanReport('Sub Kelompok Pasien Belum Dipilih','Laporan Penerimaan ');
        x=0;
	}
	if(Ext.getCmp('Shift_All_OkPenerimaan').getValue() === false && Ext.getCmp('Shift_1_OkPenerimaan').getValue() === false && Ext.getCmp('Shift_2_OkPenerimaan').getValue() === false && Ext.getCmp('Shift_3_OkPenerimaan').getValue() === false){
		ShowPesanWarningOkPenerimaanReport(nmRequesterRequest,'Laporan Penerimaan ');
        x=0;
	}
	

    return x;
};

function ShowPesanWarningOkPenerimaanReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function getItemLapOkPenerimaan_Atas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width:  375,
            height: 100,
            anchor: '100% 100%',
            items: [
            {
                x: 10,
                y: 10,
				hidden:true,
                xtype: 'label',
                text: 'Pasien '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
				hidden:true,
                text: ' : '
            },
                mComboPilihanOkPenerimaan(),
            {
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'Periode Tanggal '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterLapNerimaOk',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 10,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 10,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterLapNerimaOk',
                format: 'd/M/Y',
                value: now,
                width: 100
            },{
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Kelompok pasien '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            },
                mComboPilihanOkPenerimaanKelompokPasien(),
                mComboUmumOkPenerimaan(),
				mComboPerseoranganOkPenerimaan(),
                mComboAsuransiOkPenerimaan(),
                mComboPerusahaanOkPenerimaan()
                
            ]
        }]
    };
    return items;
};

function getItemLapOkPenerimaan_Bawah()
{
   var items = 
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                    xtype: 'fieldset',
                    title: 'Shift',
                    autoHeight: true,
                    width: '373px',
                    defaultType: 'checkbox', // each item will be a checkbox
                    items: 
                    [
                        {
                        xtype: 'checkboxgroup',
                        items: [
                                    {
										boxLabel: 'Semua',
										name: 'Shift_All_OkPenerimaan',
										id : 'Shift_All_OkPenerimaan',
										checked:true,
										handler: function (field, value) {
											if (value === true){
												Ext.getCmp('Shift_1_OkPenerimaan').setValue(true);
												Ext.getCmp('Shift_2_OkPenerimaan').setValue(true);
												Ext.getCmp('Shift_3_OkPenerimaan').setValue(true);
												Ext.getCmp('Shift_1_OkPenerimaan').disable();
												Ext.getCmp('Shift_2_OkPenerimaan').disable();
												Ext.getCmp('Shift_3_OkPenerimaan').disable();
											}else{
													Ext.getCmp('Shift_1_OkPenerimaan').setValue(false);
													Ext.getCmp('Shift_2_OkPenerimaan').setValue(false);
													Ext.getCmp('Shift_3_OkPenerimaan').setValue(false);
													Ext.getCmp('Shift_1_OkPenerimaan').enable();
													Ext.getCmp('Shift_2_OkPenerimaan').enable();
													Ext.getCmp('Shift_3_OkPenerimaan').enable();
												}
										}
									},
                                    {
										boxLabel: 'Shift 1',
										name: 'Shift_1_OkPenerimaan',
										id : 'Shift_1_OkPenerimaan',
										checked:true,
										disabled:true,
										handler: function (field, value) {
											
										}
									},
                                    {
										boxLabel: 'Shift 2',
										name: 'Shift_2_OkPenerimaan',
										id : 'Shift_2_OkPenerimaan',
										checked:true,
										disabled:true,
										handler: function (field, value) {
											
										}
									},
                                    {
										boxLabel: 'Shift 3',
										name: 'Shift_3_OkPenerimaan',
										id : 'Shift_3_OkPenerimaan',
										checked:true,
										disabled:true,
										handler: function (field, value) {
											
										}
									}
                               ]
                        }
                    ]
            }
        ]
            
    };
    return items;
};

var selectSetPilihankelompokPasien;
var selectSetPilihan;
var selectsetperusahaan;
var selectsetnamaperusahaan;
var selectSetAsuransi;
var selectSetnamaAsuransi;

function mComboPilihanOkPenerimaan()
{
    var cboPilihanOkPenerimaan = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 10,
                id:'cboPilihanOkPenerimaan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
				hidden:true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'RWJ/IGD'],[3, 'RWI']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                        'select': function(a,b,c)
                        {
                                selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPilihanOkPenerimaan;
};

function mComboPilihanOkPenerimaanKelompokPasien()
{
    var cboPilihanOkPenerimaankelompokPasien = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 40,
                id:'cboPilihanOkPenerimaankelompokPasien',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width: 240,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'Semua'], [2, 'Perseorangan'],[3, 'Perusahaan'], [4, 'Asuransi']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                value:'Semua',
                listeners:
                {
                    'select': function(a,b,c)
                    {
                            selectSetPilihankelompokPasien=b.data.displayText;
                            Combo_Select(b.data.displayText);
                    }
                }
            }
	);
	return cboPilihanOkPenerimaankelompokPasien;
};

function mComboPerseoranganOkPenerimaan()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerseoranganNerimaLab = new WebApp.DataStore({fields: Field});
    dsPerseoranganNerimaLab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=0 order by CUSTOMER'
			}
		}
	);
    var cboPerseoranganNerimaLab = new Ext.form.ComboBox
	(
            {
                x: 120,
                y: 70,
                id:'cboPerseoranganNerimaLab',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: '',
                width:240,
                store: dsPerseoranganNerimaLab,
				/* new Ext.data.ArrayStore
                (
                        {
                                id: 0,
                                fields:
                                [
                                        'Id',
                                        'displayText'
                                ],
                        data: [[1, 'Umum']]
                        }
                ), */
                valueField: 'KD_CUSTOMER',
                displayField: 'CUSTOMER',
                value:selectSetPerseorangan,
                listeners:
                {
                        'select': function(a,b,c)
                        {
                          selectSetPerseorangan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPerseoranganNerimaLab;
};

function mComboUmumOkPenerimaan()
{
    var cboUmumNerimaLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboUmumNerimaLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: '',
			width:240,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Semua',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetUmum=b.data.displayText ;
				}
                                
                            
			}
		}
	);
	return cboUmumNerimaLab;
};

function mComboPerusahaanOkPenerimaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboLookupCustomerLab',
			    param: 'jenis_cust=1 order by CUSTOMER'
			}
		}
	);
    var cboPerusahaanRequestEntryNerimaLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
		    id: 'cboPerusahaanRequestEntryNerimaLab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: '',
		    align: 'Right',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			width:240,
			value: selectsetperusahaan,
		    listeners:
			{
			    'select': function(a,b,c)
				{
			        selectsetperusahaan = b.data.KD_CUSTOMER;
					selectsetnamaperusahaan = b.data.CUSTOMER;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryNerimaLab;
};

function mComboAsuransiOkPenerimaan()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboLookupCustomerLab',
                param: "jenis_cust=2 order by CUSTOMER"
            }
        }
    );
    var cboAsuransiNerimaLab = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'cboAsuransiNerimaLab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: '',
			align: 'Right',
			width:240,
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value: selectSetAsuransi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.KD_CUSTOMER ;
					selectsetnamaAsuransi = b.data.CUSTOMER ;
				}
			}
		}
	);
	return cboAsuransiNerimaLab;
};

function Combo_Select(combo)
{
   var value = combo;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('cboPerseoranganNerimaLab').show();
        Ext.getCmp('cboAsuransiNerimaLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryNerimaLab').hide();
        Ext.getCmp('cboUmumNerimaLab').hide();
   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganNerimaLab').hide();
        Ext.getCmp('cboAsuransiNerimaLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryNerimaLab').show();
        Ext.getCmp('cboUmumNerimaLab').hide();
   }
   else if(value === "Asuransi")
    {
        Ext.getCmp('cboPerseoranganNerimaLab').hide();
        Ext.getCmp('cboAsuransiNerimaLab').show();
        Ext.getCmp('cboPerusahaanRequestEntryNerimaLab').hide();
        Ext.getCmp('cboUmumNerimaLab').hide();
    }
   else if(value === "Semua")
   {
        Ext.getCmp('cboPerseoranganNerimaLab').hide();
        Ext.getCmp('cboAsuransiNerimaLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryNerimaLab').hide();
        Ext.getCmp('cboUmumNerimaLab').show();
   }
   else
   {
        Ext.getCmp('cboPerseoranganNerimaLab').hide();
        Ext.getCmp('cboAsuransiNerimaLab').hide();
        Ext.getCmp('cboPerusahaanRequestEntryNerimaLab').hide();
        Ext.getCmp('cboUmumNerimaLab').show();
   }
}

function mCombounitOkPenerimaan()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
    ds_param_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'NAMA_UNIT',
                    Sortdir: 'ASC',
                    target:'ViewSetupUnit',
                    param: "kd_bagian=2 and type_unit=false"
                }
            }
        );

    var cbounitRequestEntryOkPenerimaan = new Ext.form.ComboBox
    (
        {
            id: 'cbounitRequestEntryOkPenerimaan',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih unit...',
            fieldLabel: 'unit ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
//            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   
                                }
                    
                }
        }
    )

    return cbounitRequestEntryOkPenerimaan;
};
