
var tigaharilalu = new Date().add(Date.DAY, -3);
var dsLapRekap_OK;
var selectNamaLapRekap_OK;
var now = new Date();
var selectSetPerseorangan;
var frmDlgLapRekap_OK;
var varLapRekap_OK= ShowFormLapRekap_OK();
var selectSetUmum;
var selectSetkelpas;
var selectSetuser;
var jenis='1';
var kodeUnitLapRekap_Gz;
function ShowFormLapRekap_OK()
{
    frmDlgLapRekap_OK= fnDlgLapRekap_OK();
    frmDlgLapRekap_OK.show();
};
function dsLoadUnitLapRekap_OK(){
	dsLapRekap_OK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vGzUnitLapRekap',
                    param : 'kd_bagian=1'
                }			
            }
        );   
    return dsLapRekap_OK;
	}

function fnDlgLapRekap_OK()
{
    var winLapRekap_OKReport = new Ext.Window
    (
        {
            id: 'winLapRekap_OKReport',
            title: 'Laporan Rekapitulasi Operasi',
            closeAction: 'destroy',
            width: 405,
            height: 230,
            border: false,
            resizable:false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgLapRekap_OK()],
			fbar:
			[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkLapRekap_OK',
					handler: function()
					{
						if (jenis==='1')
						{
							var params={
							//kdUnit:Ext.getCmp('CmbSetUnitLapRekap_OK').getValue(),
							tglAwal:Ext.getCmp('dtpTglAwalRekap_OK').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirRekap_OK').getValue(),
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/kamar_operasi/lap_RekapitulasiOperasi/cetakRekapok");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();	
						}
						else
						{
							var params={
							//kdUnit:Ext.getCmp('CmbSetUnitLapRekap_OK').getValue(),
							tglAwal:Ext.getCmp('dtpTglAwalRekap_OK').getValue(),
							tglAkhir:Ext.getCmp('dtpTglAkhirRekap_OK').getValue(),
							} ;
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("target", "_blank");
							form.setAttribute("action", baseURL + "index.php/kamar_operasi/lap_RekapitulasiOperasi/cetakRekapokExcel");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("type", "hidden");
							hiddenField.setAttribute("name", "data");
							hiddenField.setAttribute("value", Ext.encode(params));
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();
						}
							
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelLapRekap_OK',
					handler: function()
					{
						frmDlgLapRekap_OK.close();
					}
				}
			],
            listeners:
			{
				activate: function()
				{
				   
				}
			}

        }
    );

    return winLapRekap_OKReport;
};


function ItemDlgLapRekap_OK()
{
	radios = new Ext.form.RadioGroup({
	id :'radiosa',
	columns  :2,
	items: 
	[
		{
			id:'rbPdf',
			name: 'rbKelompok',
			boxLabel: 'PDF', 
			inputValue: "1",
			checked: true,
		},
		{
			boxLabel: 'Excel',
			id:'rbExl', 
			name: 'rbKelompok',
			inputValue: "2"
		},
	],
	listeners: {
		change : function()
		{ 
			jenis= radios.getValue().inputValue;
		
		}
	}
		
   });
    var PnlLapRekap_OK = new Ext.Panel
    (
        {
            id: 'PnlLapRekap_OK',
            fileUpload: true,
            layout: 'form',
            height: '500',
            anchor: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items:
            [
			{
					
					xtype: 'fieldset',
					title: 'Jenis Output',
					items: 
					[
						radios,
					]
			}
			,
				{
					
					xtype: 'fieldset',
					title: 'Periode',
					items: 
					[
						getItemLapRekap_OK_Tanggal(),
					]
			}
            ]
        }
    );

    return PnlLapRekap_OK;
};

function getItemLapRekap_OK_Tanggal()
{
	
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Tanggal'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 100,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAwalRekap_OK',
						format: 'd/M/Y',
						value: now
					}, 
					{
						x: 215,
						y: 10,
						xtype: 'label',
						text: ' s/d '
					}, 
					{
						x: 245,
						y: 10,
						xtype: 'datefield',
						id: 'dtpTglAkhirRekap_OK',
						format: 'd/M/Y',
						value: now,
						width: 100
					}
				]
			}
		]
    };
    return items;
};
function getItemLapRekap_OK_Combo()
{
    var itemss = {
        layout: 'column',
        border: false,
        items: [
            {
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width:  355,
				height: 45,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Unit/Ruang'
					}, 
					{
						x: 90,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					comboUnit_ok_Combo()
				]
			}
		]
    };
    return itemss;
};


function ShowPesanWarningLapRekapMakananPerRuang_okReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};
