﻿var dsAccountInterfaceList;
var AddNewAccountInterface = false;
var selectCountAccountInterface = 50;
var selectAccountAccountInterface;
var AccountInterfaceLookUps;
var rowSelectedAccountInterface;
var selectCboInterfaceSetInterface;
var selectUnitKerja_aci ;
CurrentPage.page = getPanelAccountInterface(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelAccountInterface(mod_id) {
    var Field = ['INT_CODE','DESCRIPTION','ACCOUNT','Name','AKUN','CODE','KD_UNIT_KERJA','NAMA_UNIT_KERJA'];
    dsAccountInterfaceList = new WebApp.DataStore({ fields: Field });
   
    RefreshDataAccountInterface();

    var grListAccountInterface = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListAccountInterface',
		    stripeRows: true,
		    store: dsAccountInterfaceList,
		    autoScroll: true,
		    columnLines: true,
		    border: false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
					        rowSelectedAccountInterface = dsAccountInterfaceList.getAt(row);
					    }
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'Account',
					    header: "Code On Hand ",
					    dataIndex: 'ACCOUNT',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'NamaAccount',
					    header: "Cash In Bank" ,
					    dataIndex: 'Name',
					    width: 100,
					    sortable: true
					},
                    {
                        id: 'Int_Code',
                        header: "Fixed Assets",
                        dataIndex: 'INT_CODE',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'Description',
					    header: "Sales ",
					    dataIndex: 'DESCRIPTION',
					    width: 100,
					    sortable: true
					},
					{
					    id: 'RetainedEarning',
					    header: "Retained Earning ",
					    dataIndex: 'DESCRIPTION',
					    width: 100,
					    sortable: true
					},
					{
					    id: 'CurrentEarning',
					    header: "Current Earning",
					    dataIndex: '',
					    width: 100,
					    sortable: true
					}
                ]
			),
		    tbar:
			[
				{
				    id: 'btnEditAccountInterface',
				    text: 'Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) {
				        if (rowSelectedAccountInterface != undefined) 
						{
				            AccountInterfaceLookUp(rowSelectedAccountInterface.data);
				        }
				        else 
						{
				            AccountInterfaceLookUp();
				        }
				    }
				}, '-'
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsAccountInterfaceList,
					pageSize: selectCountAccountInterface,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			)
		}
	);
    //END var grListAccountInterface -----------------------------------------------------------

    var FormAccountInterface = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Akun Interface',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupInterface',
		    items: [grListAccountInterface],
		    tbar:
			[
				'Kode : ', ' ',
				{
				    xtype: 'textfield',
				    fieldLabel: 'Kode : ',
				    id: 'txtKDAccountInterfaceFilter',
				    width: 80,
				    onInit: function() { }
				}, ' ', '-',
				'Maks.Data : ', ' ', mComboMaksDataAccountInterface(),
				' ', '->',
				{
				    id: 'btnRefreshAccountInterface',
				    tooltip: 'Refresh',
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{
				        RefreshDataAccountInterfaceFilter();
				    }
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{
					//Ext.getCmp('cboDESKRIPSI').store = getAccountInterface();
				}
			}
		}
	);
    //END var FormAccountInterface-----------------------------------------------------------------------


    RefreshDataAccountInterface();
	//RefreshDataAccountInterface();

    return FormAccountInterface
};
// end function get panel main data
///-----------------------------------------------------------------------------------------------///


function AccountInterfaceLookUp(rowdata) {
    var lebar = 350;//600;
    AccountInterfaceLookUps = new Ext.Window
    (
		{
		    id: 'AccountInterfaceLookUps',
		    title: 'Akun Interface',
		    closeAction: 'destroy',
		    y: 90,
		    width: lebar,
		    height: 250,
		    resizable: false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupInterface',
		    modal: true,
		    items: getFormEntryAccountInterface(lebar),
		    listeners:
            {
                activate: function() 
				{ 
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedAccountInterface=undefined;
					RefreshDataAccountInterface();
				}
            }
		}
	);
    //END var AccountInterfaceLookUps---------------------------------------------------------

    AccountInterfaceLookUps.show();
    if (rowdata == undefined) {
        AccountInterfaceAddNew();
    }
    else {
        AccountInterfaceInit(rowdata)
    }
};

//  END FUNCTION AccountInterfaceLookUp
///------------------------------------------------------------------///

function mcomboUnitKerja_accountinterface()
{
	var Field = ['KD_UNIT_KERJA', 'NAMA_UNIT_KERJA','UNITKERJA'];
	dsUnitKerja_aci = new WebApp.DataStore({ fields: Field });
	
  var comboUnitKerja_aci = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_aci',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Unit Kerja...',
			fieldLabel: 'Unit Kerja ',			
			align:'Right',
			anchor:'100%',
			store: dsUnitKerja_aci,
			valueField: 'KD_UNIT_KERJA',
			displayField: 'UNITKERJA',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					//selectAktivaLancar_kbs = "";
					//Ext.getCmp('comboAktivaLancar_kbs').setValue("");
				//	Ext.get('comboAktivaLancar_kbs').dom.value = "";
					
					selectUnitKerja_aci=b.data.KD_UNIT_KERJA ;
					
					/*dsAktivaLancarPenerimaanMhs.load
					(
						{
							params:
							{
								Skip: 0,
								Take: 1000,
								Sort: '',
								Sortdir: 'ASC',
								target: 'viCboAktivaLancar',
								param: kriteria
							}
						}
					);*/
					
				} 
			}
		}
	);
	
	dsUnitKerja_aci.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_UNIT_KERJA',
			    Sortdir: 'ASC',
			    target: 'viCboUnitKerja',
			    param: ''
			}
		}
	);
	
	return comboUnitKerja_aci;
} ;

function getFormEntryAccountInterface(lebar) {
    var pnlAccountInterface = new Ext.FormPanel
    (
		{
		    id: 'PanelAccountInterface',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 250,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupAccountInterface',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width: lebar - 36.5,
				    bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 'auto',
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth: .989,
						    layout: 'form',
						    id: 'PnlKiriAccountInterface',
						    labelWidth: 120,
						    border: false,
						    items:
							[
								
								{
									xtype: 'compositefield',
									fieldLabel: 'Code On Hand ',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtCOHAccountInterface',
											width: 120,
											readOnly: true
										}
										
									]
								},									
								{
									xtype: 'compositefield',
									fieldLabel: 'Cash In Bank ',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtCIBAccountInterface',
											width: 120,
											readOnly: true
										}
										
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Fixed Assets',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtFSAccountInterface ',
											width: 120,
											readOnly: true
										}
										
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Sales ',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtSalesAccountInterface ',
											width: 120,
											readOnly: true
										}
										
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Retained Earning ',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtREAccountInterface ',
											width: 120,
											readOnly: true
										}
										
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Current Earning ',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtCEAccountInterface ',
											width: 120,
											readOnly: true
										}
										
									]
								},								
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddAccountInterface',
				    text: 'Tambah',
				    tooltip: 'Tambah Record Baru ',
				    iconCls: 'add',
				    handler: function() { AccountInterfaceAddNew() }
				}, '-',
				{
				    id: 'btnSimpanAccountInterface',
				    text: 'Simpan',
				    tooltip: 'Simpan Data ',
				    iconCls: 'save',
				    handler: function() {
				        AccountInterfaceSave(false);
				        RefreshDataAccountInterface();
				    }
				}, '-',
				{
				    id: 'btnSimpanCloseAccountInterface',
				    text: 'Simpan & Keluar',
				    tooltip: 'Simpan dan Keluar',
				    iconCls: 'saveexit',
				    handler: function() {
				        var x = AccountInterfaceSave(true);
				        RefreshDataAccountInterface();
				        if (x === undefined) {
				            AccountInterfaceLookUps.close();
				        };
				    }
				},'-', '->', '-',
				{
				    id: 'btnPrintAccountInterface',
				    text: ' Cetak',
				    tooltip: 'Cetak',
				    iconCls: 'print',
				    handler: function() {
				        //ShowFormLapAccountUIs();						
						//ShowReport('','011107',Ext.getCmp('txtKode_AccountInterface').getValue())
						ShowReport('','011107',selectCboInterfaceSetInterface);
				    }
				}
			]
		}
	);
    // END VAR pnlAccountInterface ------------------------------------------------------------------------ 
    return pnlAccountInterface
};
//END FUNCTION getFormEntryAccountInterface
///----------------------------------------------------------------------------------------------///

function AccountInterfaceSave(mBol) {
    if (ValidasiEntryAccountInterface('Simpan Data') == 1) {
        if (AddNewAccountInterface == true) {
            Ext.Ajax.request
				(
					{
					    url: "./Datapool.mvc/CreateDataObj",
					    params: getParamAccountInterface(),
					    success: function(o) {
					        var cst = o.responseText;
					        if (cst == '{"success":true}') {
					            ShowPesanInfoAccountInterface('Data berhasil di simpan', 'Simpan Data');
					            RefreshDataAccountInterface();
								 if (mBol === false) 
								{
									Ext.getCmp('comboInterfaceSetInterface').setReadOnly(true);
									//Ext.get('txtKode_AccountInterface').dom.readOnly=true;
									Ext.get('comboAccountAccountInterface').dom.readOnly=true;
								}
								AddNewAccountInterface = false;
					        }
					        else if (cst == '{"pesan":0,"success":false}') {
					            ShowPesanWarningAccountInterface('Data tidak berhasil di simpan, data tersebut sudah ada', 'Simpan Data');
					        }
					        else {
					            ShowPesanErrorAccountInterface('Data tidak berhasil di simpan', 'Simpan Data');
					        }
					    }
					}
				)
        }
        else {
            Ext.Ajax.request
			(
				{
				    url: "./Datapool.mvc/UpdateDataObj",
				    params: getParamAccountInterface(),
				    success: function(o) {
				        var cst = o.responseText;
				        if (cst == '{"success":true}') 
						{
				            ShowPesanInfoAccountInterface('Data berhasil di edit', 'Edit Data');
				            RefreshDataAccountInterface();
				            //AccountInterfaceAddNew();
				        }
				        else if (cst == '{"pesan":0,"success":false}') {
				            ShowPesanWarningAccountInterface('Data tidak berhasil di edit, data tersebut belum ada', 'Edit Data');
				        }
				        else {
				            ShowPesanErrorAccountInterface('Data tidak berhasil di edit', 'Edit Data');
				        }
				    }
				}
			)
        }

    }
    else {
        if (mBol === true) {
            return false;
        };
    };
}; //END FUNCTION AccountInterfaceSave
///---------------------------------------------------------------------------------------///

function AccountInterfaceDelete() {
    if (ValidasiEntryAccountInterface('Hapus Data') == 1) {
        Ext.Ajax.request
		(
			{
			    url: "./Datapool.mvc/DeleteDataObj",
			    params: getParamAccountInterface(),
			    success: function(o) {
			        var cst = o.responseText;
			        if (cst == '{"success":true}') {
			            ShowPesanInfoAccountInterface('Data berhasil di hapus', 'Hapus Data');
			            RefreshDataAccountInterface();
			            AccountInterfaceAddNew();
			        }
			        else if (cst == '{"pesan":0,"success":false}') {
			            ShowPesanWarningAccountInterface('Data tidak berhasil di hapus, data tersebut belum ada', 'Hapus Data');
			        }
			        else {
			            ShowPesanErrorAccountInterface('Data tidak berhasil di hapus', 'Hapus Data');
			        }
			    }
			}
		)
    }
};

function ValidasiEntryAccountInterface(modul) {
    var x = 1;	
	
	if(Ext.getCmp('comboInterfaceSetInterface').getValue() == "" || selectCboInterfaceSetInterface === undefined)
	{
		ShowPesanWarningAccountInterface("Akun spesifik belum dipilih", "Akun Interface");
		x = 0;
	}
	
	if(Ext.getCmp('comboAccountAccountInterface').getValue() == "" || selectAccountAccountInterface === undefined)
	{
		ShowPesanWarningAccountInterface("Akun interface belum dipilih", "Akun Interface");
		x = 0;
	}
	
    return x;
};

function ShowPesanWarningAccountInterface(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorAccountInterface(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoAccountInterface(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO
		}
	);
};


//-----------------------------------------------------------------------------------------------------
function AccountInterfaceInit(rowdata) {
    AddNewAccountInterface = false;
	Ext.getCmp('comboInterfaceSetInterface').setValue(rowdata.CODE);
	Ext.getCmp('comboInterfaceSetInterface').setReadOnly(true);
	selectCboInterfaceSetInterface = rowdata.INT_CODE;
    Ext.getCmp('comboAccountAccountInterface').setValue(rowdata.AKUN);
	Ext.getCmp('comboAccountAccountInterface').setReadOnly(true);
    selectAccountAccountInterface = rowdata.ACCOUNT;
    Ext.get('txtDeskripsiAccountInterface').dom.value = rowdata.DESCRIPTION;
	
	Ext.get('comboUnitKerja_aci').dom.value = rowdata.NAMA_UNIT_KERJA;	
	selectUnitKerja_aci = rowdata.KD_UNIT_KERJA;
    //Ext.get('txtKode_AccountInterface').dom.value = rowdata.Int_Code;
    //Ext.get('txtAccountInterface').dom.value = rowdata.Account;
	//Ext.get('txtKode_AccountInterface').dom.readOnly=true;
	//Ext.get('txtAccountInterface').dom.readOnly=true;
};
///---------------------------------------------------------------------------------------///

function AccountInterfaceAddNew(pnlAccountInterface) {
    AddNewAccountInterface = true;
	
	Ext.getCmp('comboInterfaceSetInterface').reset();
	//Ext.getCmp('comboInterfaceSetInterface').setreadOnly(false);
	//Ext.get('comboInterfaceSetInterface').dom.readOnly=false;
	Ext.getCmp('comboInterfaceSetInterface').setReadOnly(false);
	selectCboInterfaceSetInterface = undefined;
    Ext.get('comboAccountAccountInterface').dom.value = '';
	//Ext.get('comboAccountAccountInterface').dom.readOnly=false;
	Ext.getCmp('comboAccountAccountInterface').setReadOnly(false);
    selectAccountAccountInterface = undefined;
    Ext.get('txtDeskripsiAccountInterface').dom.value = '';
    //Ext.get('txtAccountInterface').dom.value = '';
	//Ext.get('txtKode_AccountInterface').dom.readOnly=false;
    //Ext.get('txtKode_AccountInterface').dom.value = '';
	//Ext.get('txtAccountInterface').dom.readOnly=false;
};
///---------------------------------------------------------------------------------------///

function getParamAccountInterface() {
    var params =
	{
	    Table: 'viAkunUI',
	    Account: selectAccountAccountInterface,//Ext.getCmp('txtAccountInterface').getValue()
	    Int_Code: selectCboInterfaceSetInterface,//Ext.get('txtKode_AccountInterface').getValue(),
	    Description: Ext.get('txtDeskripsiAccountInterface').getValue(),
		 KD_UNIT_KERJA: Ext.getCmp('comboUnitKerja_aci').getValue()
	};
    return params
};


function RefreshDataAccountInterface() {
    dsAccountInterfaceList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: selectCountAccountInterface,
			    Sort: 'Kd_AccountInterface',
			    Sortdir: 'ASC',
			    target: 'viAkunUI',
			    param: ''
			}
		}
	);
    return dsAccountInterfaceList;
};


function RefreshDataAccountInterfaceFilter() {
    if (Ext.get('txtKDAccountInterfaceFilter').getValue() != '') {
        var strAccountInterface = Ext.get('txtKDAccountInterfaceFilter').getValue();
        dsAccountInterfaceList.load
		(
			{
			    params:
				{
				    Skip: 0,
				    Take: selectCountAccountInterface,
				    Sort: '',
				    Sortdir: 'ASC',
				    target: 'viAkunUI',
				    param: strAccountInterface
				}
			}
		);
    }
    else {
        RefreshDataAccountInterface();
    }
};

function mComboMaksDataAccountInterface() {
    var cboMaksDataAccountInterface = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataAccountInterface',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: 'Maks.Data ',
		    width: 60,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountAccountInterface,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountAccountInterface = b.data.displayText;
			        RefreshDataAccountInterface();
			    }
			}
		}
	);
    return cboMaksDataAccountInterface;
};