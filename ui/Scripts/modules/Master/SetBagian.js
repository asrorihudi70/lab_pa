/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

 var dsSetBagianList;
 var AddNewSetBagian;
 var selectCountSetBagian=50;
 var rowSelectedSetBagian;
 var SetBagianLookUps;

 CurrentPage.page = getPanelSetBagian(CurrentPage.id);
 mainPage.add(CurrentPage.page);
 mainPage.setActiveTab(CurrentPage.id);

 function getPanelSetBagian(mod_id)
 {
     var Field =['KD_BAGIAN','BAGIAN'];
     dsSetBagianList = new WebApp.DataStore({ fields: Field });
     RefreshDataSetBagian();

     var grListSetBagian = new Ext.grid.EditorGridPanel
     (
        {
            id: 'grListSetBagian',
            stripeRows: true,
            store: dsSetBagianList,
            autoScroll: true,
            columnLines: true,
            border: false,
            anchor: '100% 100%',
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                        {
                            rowselect: function(sm, row, rec)
                            {
                                rowSelectedSetBagian=undefined;
                                rowSelectedSetBagian = dsSetBagianList.getAt(row);
                            }
                        }
                }
            ),
            listeners:
		{
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelectedSetBagian = dsSetBagianList.getAt(ridx);
                                if (rowSelectedSetBagian != undefined)
                                 {
                                    SetBagianLookUp(rowSelectedSetBagian.data);
                                 }
                                else
                                 {
                                    SetBagianLookUp();
                                 };
			}
		},
           cm: new Ext.grid.ColumnModel
            (
		[
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetBagian',
                        header: 'Kode Bagian',
                        dataIndex: 'KD_BAGIAN',
                        sortable: true,
                        width: 100
                    },
                    {
			id: 'colSetBagian',
			header: 'Bagian',
			dataIndex: 'BAGIAN',
			width: 300,
			sortable: true
                    }
                ]
            ),
            bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetBagianList,
            pageSize: selectCountSetBagian,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetBagian',
				    text: 'Edit',
                                    iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec)
					{
						if (rowSelectedSetBagian != undefined)
						{
						    SetBagianLookUp(rowSelectedSetBagian.data);
						}
						else
						{
						    SetBagianLookUp();
						}
				    }
				},' ','-'
			]
		    ,viewConfig: { forceFit: true }
		}
	);
     var FormSetBagian = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Bagian',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupBagian',
		    items: [grListSetBagian],
		    tbar:
			[
				'Kode' + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Id : ',
					id: 'txtKDSetBagianFilter',
					width:80,
					onInit: function() { }
				}, ' ','-',
				'Bagian' + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Bagian : ',
					id: 'txtSetBagianFilter',
					anchor: '95%',
					onInit: function() { }
				}, ' ',
				{
				    id: 'btnRefreshSetBagian',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec)
					{
						RefreshDataSetBagianFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function()
				{
					//Ext.getCmp('cboDESKRIPSI').store = getSetBagian();
				}
			}
		}
	);
    //END var FormSetBagian--------------------------------------------------
     RefreshDataSetBagian();
    return FormSetBagian ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///

function SetBagianLookUp(rowdata)
{
    var lebar=600;
    SetBagianLookUps = new Ext.Window
    (
		{
		    id: 'SetBagianLookUps',
		    title: 'Bagian',
		    closeAction: 'destroy',
                    y:90,
		    width: lebar,
		    height: 150,
                    resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupBagian',
		    modal: true,
		    items: getFormEntrySetBagian(lebar),
		    listeners:
            {
                activate: function()
                {
                },
				afterShow: function()
				{
					this.activate();
				},
				deactivate: function()
				{
					rowSelectedSetBagian=undefined;
					RefreshDataSetBagian();
				}
            }
		}
	);


    SetBagianLookUps.show();
	if (rowdata == undefined)
	{
		SetBagianAddNew();
	}
	else
	{
		SetBagianInit(rowdata)
	}
};


function getFormEntrySetBagian(lebar)
{
    var pnlSetBagian = new Ext.FormPanel
    (
		{
		    id: 'PanelSetBagian',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupBagian',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
                                    bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetBagian',
                                                    labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdSatuan2 + ' ',
								    name: 'txtKode_SetBagian',
								    id: 'txtKode_SetBagian',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmSatuan + ' ',
								    name: 'txtNameSetBagian',
								    id: 'txtNameSetBagian',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetBagian',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',
				    handler: function()
					{
						SetBagianAddNew()
					}
				}, '-',
				{
				    id: 'btnSimpanSetBagian',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',
				    handler: function()
					{
						SetBagianSave(false);
						RefreshDataSetBagian();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetBagian',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function()
					{
						var x = SetBagianSave(true);
						RefreshDataSetBagian();
						if (x===undefined)
						{
							SetBagianLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetBagian',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function()
					{
							SetBagianDelete() ;
							RefreshDataSetBagian();
					}
				},'-','->','-',
				{
					id:'btnPrintSetBagian',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',
					handler: function()
					{
						//LoadReport(950002);
					}
				}
			]
		}
	);

    return pnlSetBagian
};


function SetBagianSave(mBol)
{
    if (ValidasiEntrySetBagian(nmHeaderSimpanData,false) == 1 )
    {
            if (AddNewSetBagian == true)
            {
                    Ext.Ajax.request
                    (
                            {
                                    url: WebAppUrl.UrlSaveData,
                                    params: getParamSetBagian(),
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoSetBagian(nmPesanSimpanSukses,nmHeaderSimpanData);
                                                    RefreshDataSetBagian();
                                                    if(mBol === false)
                                                    {
                                                            Ext.get('txtKode_SetBagian').dom.value=cst.kd_bagian;
                                                    };
                                                    AddNewSetBagian = false;

                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanWarningSetBagian(nmPesanSimpanGagal,nmHeaderSimpanData);
                                            }
                                            else
                                            {
                                                    ShowPesanErrorSetBagian(nmPesanSimpanError,nmHeaderSimpanData);
                                            };
                                    }
                            }
                    )
            }
            else
            {
                    Ext.Ajax.request
                     (
                            {
                                    url: WebAppUrl.UrlUpdateData,
                                    params: getParamSetBagian(),
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoSetBagian(nmPesanEditSukses,nmHeaderEditData);
                                                    RefreshDataSetBagian();
                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanWarningSetBagian(nmPesanEditGagal,nmHeaderEditData);
                                            }
                                            else
                                            {
                                                    ShowPesanErrorSetBagian(nmPesanEditError,nmHeaderEditData);
                                            };
                                    }
                            }
                    )
            };
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };

};//END FUNCTION SetBagianSave
///---------------------------------------------------------------------------------------///

function SetBagianDelete()
{
	if (ValidasiEntrySetBagian(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmSatuan) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn)
			   {
					if (btn =='yes')
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetBagian(),
								success: function(o)
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true)
									{
										ShowPesanInfoSetBagian(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetBagian();
										SetBagianAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetBagian(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetBagian(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetBagian(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetBagian').getValue() == '' || (Ext.get('txtNameSetBagian').getValue() == ''))
	{
		if (Ext.get('txtKode_SetBagian').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetBagian(nmGetValidasiKosong(nmKdSatuan),modul);
			x=0;
		}
		else if (Ext.get('txtNameSetBagian').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetBagian(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};

function ShowPesanWarningSetBagian(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetBagian(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetBagian(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetBagianInit(rowdata)
{
    AddNewSetBagian = false;
    Ext.get('txtKode_SetBagian').dom.value = rowdata.KD_BAGIAN;
    Ext.get('txtNameSetBagian').dom.value = rowdata.BAGIAN;
};
///---------------------------------------------------------------------------------------///

function SetBagianAddNew()
{
    AddNewSetBagian = true;
    Ext.get('txtKode_SetBagian').dom.value = '';
    Ext.get('txtNameSetBagian').dom.value = '';
    rowSelectedSetBagian   = undefined;
};
///---------------------------------------------------------------------------------------///

function getParamSetBagian()
{
    var params =
	{
            Table: 'ViewSetupBagian',
	    KdBagian: Ext.get('txtKode_SetBagian').getValue(),
	    Bagian: Ext.get('txtNameSetBagian').getValue()
	};
    return params
};

function RefreshDataSetBagian()
{
	dsSetBagianList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountSetBagian,
					Sort: 'kd_bagian',
					Sortdir: 'ASC',
					target:'ViewSetupBagian',
					param : ''
				}
			}
		);

	rowSelectedSetBagian = undefined;
	return dsSetBagianList;
};

function RefreshDataSetBagianFilter()
{
	var KataKunci='';
    if (Ext.get('txtKDSetBagianFilter').getValue() != '')
    {
		//KataKunci = ' where Bagian_ID like ~%' + Ext.get('txtKDSetBagianFilter').getValue() + '%~';
                KataKunci = ' kd_bagian like ~%' + Ext.get('txtKDSetBagianFilter').getValue() + '%~';
	};

    if (Ext.get('txtSetBagianFilter').getValue() != '')
    {
		if (KataKunci === '')
		{
			//KataKunci = ' where Bagian like  ~%' + Ext.get('txtSetBagianFilter').getValue() + '%~';
                        KataKunci = ' bagian like  ~%' + Ext.get('txtSetBagianFilter').getValue() + '%~';
		}
		else
		{
			//KataKunci += ' and  Bagian like  ~%' + Ext.get('txtSetBagianFilter').getValue() + '%~';
                        KataKunci += ' and bagian like  ~%' + Ext.get('txtSetBagianFilter').getValue() + '%~';
		};
	};

    if (KataKunci != undefined)
    {
		dsSetBagianList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountSetBagian,
					//Sort: 'Bagian_ID',
                                        Sort: 'kd_bagian',
					Sortdir: 'ASC',
					target:'ViewSetupBagian',
					param : KataKunci
				}
			}
		);
    }
	else
	{
		RefreshDataSetBagian();
	};
};

function mComboMaksDataSetBagian()
{
  var cboMaksDataSetBagian = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetBagian',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetBagian,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountSetBagian=b.data.displayText ;
					RefreshDataSetBagian();
				}
			}
		}
	);
	return cboMaksDataSetBagian;
};
 
