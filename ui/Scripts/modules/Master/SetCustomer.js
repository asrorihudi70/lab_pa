﻿
var dsSetCustomerList;
var AddNewSetCustomer;
var selectCountSD=50;
var rowSelectedSetCustomer;
var SetCustomerLookUps;
CurrentPage.page = getPanelSetCustomer(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetCustomer(mod_id) 
{
    var Field = [
		'Cust_Code',
		'Customer',
		'Contact',
		'Address',
		'City',
		'State',
		'Zip',
		'Country',
		'Phone1',
		'Phone2',
		'Fax',
		'Due_Day',
		'Account'
	];
    dsSetCustomerList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetCustomer();

    var grListSetCustomer = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetCustomer',
		    stripeRows: true,
		    store: dsSetCustomerList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetCustomer=undefined;
							rowSelectedSetCustomer = dsSetCustomerList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'Kd_SetCustomer',
                        header: "Code",                      
                        dataIndex: 'Cust_Code',
                        sortable: true,
                        width: 30
                    },
					{
					    id: 'Customer',
					    header: "Customer ",					   
					    dataIndex: 'Customer',
					    width: 200,
					    sortable: true
					}
                ]
			),
		    tbar:
			[
				{
				    id: 'btnEditSetCustomer',
				    text: '  Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetCustomer !== undefined)
						{
						    SetCustomerLookUp(rowSelectedSetCustomer.data);
						}
						else
						{
						    SetCustomerLookUp();
						}
				    }
				},' ','-'
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsSetCustomerList,
					pageSize: 50,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
		    viewConfig: { forceFit: true }
		}
	);
    //END var grListSetCustomer 


    var FormSetCustomer = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Customer',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'Customer',
		    items: [grListSetCustomer],
		    tbar:
			[
				'Code : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Code : ',
					id: 'txtKDSetCustomerFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				'Customer : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Customer : ',
					id: 'txtCustomerFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				'Maks.Data : ', ' ',mComboMaksDataSD(),
				' ','->',
				{
				    id: 'btnRefreshSetCustomer',				    
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetCustomerFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetCustomer();
				}
			}
		}
	);
    //END var FormSetCustomer--------------------------------------------------

	RefreshDataSetCustomer();
    return FormSetCustomer
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetCustomerLookUp(rowdata) 
{
	var lebar=600;
    SetCustomerLookUps = new Ext.Window   	
    (
		{
		    id: 'SetCustomerLookUps',
		    title: 'Customer Information',
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 370,//320,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'form',
		    iconCls: 'Customer',
		    modal: true,
		    items: getFormEntrySetCustomer(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetCustomer=undefined;
					RefreshDataSetCustomer();
				}
            }
		}
	);
    //END var SetCustomerLookUps------------------------------------------------------------------

    SetCustomerLookUps.show();
	if (rowdata === undefined)
	{
		SetCustomerAddNew();
	}
	else
	{
		SetCustomerInit(rowdata)
	}	
};

//  END FUNCTION SetCustomerLookUp
///------------------------------------------------------------------------------------------------------------///




function getFormEntrySetCustomer(lebar) 
{
    var pnlSetCustomer = new Ext.FormPanel
    (
		{
		    id: 'PanelSetCustomer',
		    fileUpload: true,
		    iconCls: 'SetupSetCustomer',
		    border: true,
		    items:
			[
				{
					xtype: 'fieldset',
					layout: 'form',
					style: 'margin:5px 5px 5px 5px',
					items:
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Code',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtKdCustSetCustomer',
									width: 120,
									readOnly: true
								}
								
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Name',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtNmCustSetCustomer',
									flex: 1
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Contact',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtContactCustSetCustomer',
									flex: 1
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Address',
							items:
							[
								{
									xtype: 'textarea',
									id: 'txtaAddressCustSetCustomer',
									flex: 1,
									height: 40,
									maxLength: 100
								}								
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'City',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtCityCustSetCustomer',
									flex: 1,
									width: 100
								},
								{
									xtype: 'label',
									forId: 'txtStateCustSetCustomer',
									text: 'State: ',
									style: 'margin: 3px 30px 0 10px',
									width: 50//40
								},
								{
									xtype: 'textfield',
									id: 'txtStateCustSetCustomer',
									flex: 1
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Zip Code',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtZipCustSetCustomer',
									width: 70,
									maxLength: 7
								},
								{
									xtype: 'label',
									forId: 'txtCountryCustSetCustomer',
									text: 'Country: ',
									style: 'margin: 3px 30px 0 10px',
									width: 80
								},
								{
									xtype: 'textfield',
									id: 'txtCountryCustSetCustomer',
									flex: 1
								}
							]
						},						
						{
							xtype: 'compositefield',
							fieldLabel: 'Phone 1',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtPhone1CustSetCustomer',
									flex: 1
								},
								{
									xtype: 'label',
									forId: 'txtPhone2CustSetCustomer',
									text: 'Phone 2: ',
									style: 'margin: 3px 30px 0 10px',
									width: 80
								},
								{
									xtype: 'textfield',
									id: 'txtPhone2CustSetCustomer',
									flex: 1
								}
							]
						},						
						{
							xtype: 'compositefield',
							fieldLabel: 'Kota',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtCityCustSetCustomer',
									flex: 1
								},
								{
									xtype: 'label',
									forId: 'txtStateCustSetCustomer',
									text: 'Provinsi: ',
									style: 'margin: 3px 30px 0 10px',
									width: 80
								},
								{
									xtype: 'textfield',
									id: 'txtStateCustSetCustomer',
									flex: 1
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Fax',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtFaxCustSetCustomer',
									flex: 1,
									width: 180
								},
								{
									xtype: 'label',
									forId: 'txtFaxCustSetCustomer',
									text: 'Due Day: ',
									style: 'margin: 3px 30px 0 10px',
									width: 80
								},
								{
									xtype: 'textfield',
									id: 'numDueDayCustSetCustomer',
									flex: 1,
									width: 70
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Account',							
							items:
							[								
								{
									xtype: 'textfield',
									id: 'txtCustAccSetCustomer',
									width: 150,									
									enableKeyEvents: true,
									listeners:
									{
										'specialkey': function()
										{
											if(Ext.EventObject.getKey() === 13)
											{
												var criteria = " WHERE Account LIKE '%" + this.getValue() + "' ";
												FormLookupAkunSingle(this.getId(),'',criteria);
											}
										}
									}
								}
							]
						}
					] 
				}				
			],			
		    tbar:
			[
				{
				    id: 'btnAddSetCustomer',
				    text: 'Tambah',
				    tooltip: 'Tambah Record Baru ',
				    iconCls: 'add',				   
				    handler: function() { SetCustomerAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetCustomer',
				    text: 'Simpan',
				    tooltip: 'Rekam Data ',
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetCustomerSave(false);
						RefreshDataSetCustomer();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetCustomer',
				    text: 'Simpan & Keluar',
				    tooltip: 'Simpan dan Keluar',
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetCustomerSave(true);
						RefreshDataSetCustomer();
						if (x===undefined)
						{
							SetCustomerLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetCustomer',
				    text: 'Hapus',
				    tooltip: 'Remove the selected item',
				    iconCls: 'remove',
				    handler: function() 
					{
							SetCustomerDelete() ;
							RefreshDataSetCustomer();					
					}
				},'-','->','-',
				{
					id:'btnPrintSD',
					text: ' Cetak',
					tooltip: 'Cetak',
					iconCls: 'print',					
					handler: function() 
					{
						ShowReport('011108');
					}
				}
			]
		}
	); 

    return pnlSetCustomer
};
//END FUNCTION getFormEntrySetCustomer
///------------------------------------------------------------------------------------------------------------///


function SetCustomerSave(mBol) 
{
	if (ValidasiEntrySetCustomer('Simpan Data') == 1 )
	{
		if (AddNewSetCustomer == true) 
		{
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/CreateDataObj",
					params: getParamSetCustomer(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSD('Data berhasil di simpan','Simpan Data');
							RefreshDataSetCustomer();
							if(mBol === false)
							{
								Ext.getCmp('txtKdCustSetCustomer').setValue(cst.kode);
								AddNewSetCustomer = false;
							};							
						}
						else if (cst.pesan == 0)
						{
							ShowPesanWarningSD('Data tidak berhasil di simpan, data tersebut sudah ada','Simpan Data');
						}
						else 
						{
							ShowPesanErrorSD('Data tidak berhasil di simpan','Simpan Data');
						}
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: "./Datapool.mvc/UpdateDataObj",
					params: getParamSetCustomer(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSD('Data berhasil di edit','Edit Data');
							RefreshDataSetCustomer();
							if(mBol === false)
							{
								Ext.getCmp('txtKdCustSetCustomer').setValue(cst.kode);
								AddNewSetCustomer = false;
							};
							
						}
						else if (cst.pesan == 0)
						{
							ShowPesanWarningSD('Data tidak berhasil di edit, data tersebut sudah ada','Edit Data');
						}
						else 
						{
							ShowPesanErrorSD('Data tidak berhasil di edit','Edit Data');
						}
					}
				}
			)
		}
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetCustomerSave
///---------------------------------------------------------------------------------------///

function SetCustomerDelete() 
{
	if (ValidasiEntrySetCustomer('Hapus Data') == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: "./Datapool.mvc/DeleteDataObj",
				params: getParamSetCustomer(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSD('Data berhasil di hapus','Hapus Data');
						RefreshDataSetCustomer();
						SetCustomerAddNew();
					}
					else if (cst.pesan == 0)
					{
						ShowPesanWarningSD('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else {
						ShowPesanErrorSD('Data tidak berhasil di hapus','Hapus Data');
					}
				}
			}
		)
	}
};


function ValidasiEntrySetCustomer(modul)
{
	var x = 1;

	if(AddNewSetCustomer === true)
	{
		if(Ext.getCmp('txtNmCustSetCustomer').getValue() == "")
		{
			ShowPesanWarningSD('Nama customer belum di isi',modul);
			x=0;
		}		
	}
	else
	{
		if(Ext.getCmp('txtKdCustSetCustomer').getValue() == "")
		{
			ShowPesanWarningSD('Code customer belum di isi',modul);
			x=0;
		}else if(Ext.getCmp('txtNmCustSetCustomer').getValue() == "")
			{
				ShowPesanWarningSD('Nama customer belum di isi',modul);
				x=0;
			}
	}
	
	if(Ext.getCmp('txtFaxCustSetCustomer').getValue() != "")
		{
			if(isNaN(Ext.getCmp('txtFaxCustSetCustomer').getValue()) === true)
			{
				ShowPesanWarningSD('No Fax harus numerik.',modul);
				x=0;
			}
		}
		
		if(Ext.getCmp('txtPhone1CustSetCustomer').getValue() != "")
		{
			if(isNaN(Ext.getCmp('txtPhone1CustSetCustomer').getValue()) === true)
			{
				ShowPesanWarningSD('No Telephone harus numerik.',modul);
				x=0;
			}
		}
		
		if(Ext.getCmp('txtPhone2CustSetCustomer').getValue() != "")
		{
			if(isNaN(Ext.getCmp('txtPhone2CustSetCustomer').getValue()) === true)
			{
				ShowPesanWarningSD('No Telephone harus numerik.',modul);
				x=0;
			}
		}
		
		if(Ext.getCmp('txtZipCustSetCustomer').getValue() != "")
		{
			if(isNaN(Ext.getCmp('txtZipCustSetCustomer').getValue()) === true)
			{
				ShowPesanWarningSD('Kode Pos harus numerik.',modul);
				x=0;
			}
		}
		
		if(Ext.getCmp('numDueDayCustSetCustomer').getValue() != "")
		{
			if(isNaN(Ext.getCmp('numDueDayCustSetCustomer').getValue()) === true)
			{
				ShowPesanWarningSD('Due day harus numerik.',modul);
				x=0;
			}
			
		}
		else
		
		{
		ShowPesanWarningSD('Due day harus diisi.',modul);
				x=0;
		}
	return x;
};

function ShowPesanWarningSD(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorSD(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoSD(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

//------------------------------------------------------------------------------------
function SetCustomerInit(rowdata) 
{
    AddNewSetCustomer = false;
    Ext.getCmp('txtKdCustSetCustomer').setValue(rowdata.Cust_Code);
	Ext.getCmp('txtNmCustSetCustomer').setValue(rowdata.Customer);
	Ext.getCmp('txtContactCustSetCustomer').setValue(rowdata.Contact);
	Ext.getCmp('txtFaxCustSetCustomer').setValue(rowdata.Fax);	
	Ext.getCmp('txtPhone1CustSetCustomer').setValue(rowdata.Phone1);
	Ext.getCmp('txtPhone2CustSetCustomer').setValue(rowdata.Phone2);
	Ext.getCmp('txtaAddressCustSetCustomer').setValue(rowdata.Address);	
	Ext.getCmp('txtZipCustSetCustomer').setValue(rowdata.Zip);	
	Ext.getCmp('txtCityCustSetCustomer').setValue(rowdata.City);	
	Ext.getCmp('txtStateCustSetCustomer').setValue(rowdata.State);	
	Ext.getCmp('txtCountryCustSetCustomer').setValue(rowdata.Country);	
	Ext.getCmp('numDueDayCustSetCustomer').setValue(rowdata.Due_Day);
	Ext.getCmp('txtCustAccSetCustomer').setValue(rowdata.Account);
};
///---------------------------------------------------------------------------------------///



function SetCustomerAddNew() 
{
    AddNewSetCustomer = true;   
	Ext.getCmp('txtKdCustSetCustomer').setValue('');
	Ext.getCmp('txtNmCustSetCustomer').setValue('');
	Ext.getCmp('txtContactCustSetCustomer').setValue('');
	Ext.getCmp('txtFaxCustSetCustomer').setValue('');	
	Ext.getCmp('txtPhone1CustSetCustomer').setValue('');
	Ext.getCmp('txtPhone2CustSetCustomer').setValue('');
	Ext.getCmp('txtaAddressCustSetCustomer').setValue('');	
	Ext.getCmp('txtZipCustSetCustomer').setValue('');	
	Ext.getCmp('txtCityCustSetCustomer').setValue('');	
	Ext.getCmp('txtStateCustSetCustomer').setValue('');	
	Ext.getCmp('txtCountryCustSetCustomer').setValue('');	
	Ext.getCmp('numDueDayCustSetCustomer').setValue('');
	Ext.getCmp('txtCustAccSetCustomer').setValue('');
	rowSelectedSetCustomer=undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetCustomer() 
{
    var params =
	{	
		Table: 'viCustomer',   
	    kd_cust: Ext.getCmp('txtKdCustSetCustomer').getValue(),
	    nama: Ext.getCmp('txtNmCustSetCustomer').getValue(),	
	    kontak: Ext.getCmp('txtContactCustSetCustomer').getValue(),	
	    fax: Ext.getCmp('txtFaxCustSetCustomer').getValue(),	
	    phone1: Ext.getCmp('txtPhone1CustSetCustomer').getValue(),	
	    phone2: Ext.getCmp('txtPhone2CustSetCustomer').getValue(),	
	    alamat: Ext.getCmp('txtaAddressCustSetCustomer').getValue(),	
	    kdpos: Ext.getCmp('txtZipCustSetCustomer').getValue(),	
	    kota: Ext.getCmp('txtCityCustSetCustomer').getValue(),	
	    prov: Ext.getCmp('txtStateCustSetCustomer').getValue(),	
	    negara: Ext.getCmp('txtCountryCustSetCustomer').getValue(),	
	    dueday: Ext.getCmp('numDueDayCustSetCustomer').getValue(),
	    akun: Ext.getCmp('txtCustAccSetCustomer').getValue()
	};
    return params
};


function RefreshDataSetCustomer()
{	
	dsSetCustomerList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSD, 
				Sort: 'Customer', 
				Sortdir: 'ASC', 
				target:'viCustomer',
				param: ''
			} 
		}
	);
	rowSelectedSetCustomer = undefined;
	return dsSetCustomerList;
};

function RefreshDataSetCustomerFilter() 
{   
	var KataKunci;
    if (Ext.getCmp('txtKDSetCustomerFilter').getValue() != '')
    { 
		KataKunci = "WHERE Cust_Code LIKE '%" + Ext.getCmp('txtKDSetCustomerFilter').getValue() + "%'"; 
	}
    if (Ext.getCmp('txtCustomerFilter').getValue() != '')
    { 
		if (KataKunci == undefined)
		{
			KataKunci = "WHERE Customer LIKE '%" + Ext.getCmp('txtCustomerFilter').getValue() + "%'";
		}
		else
		{
			KataKunci += "AND Customer LIKE '%" + Ext.getCmp('txtCustomerFilter').getValue() + "%'";
		}  
	}
        
    if (KataKunci != undefined) 
    {  
		dsSetCustomerList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSD, 
					Sort: 'Customer', 
					Sortdir: 'ASC', 
					target:'viCustomer',
					param: KataKunci
				}			
			}
		);        
    }
	else
	{
		RefreshDataSetCustomer();
	}
};



function mComboMaksDataSD()
{
  var cboMaksDataSD = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSD,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSD=b.data.displayText ;
					RefreshDataSetCustomer();
				} 
			}
		}
	);
	return cboMaksDataSD;
};
 



