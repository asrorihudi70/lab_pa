var dsSetKelasList;
var AddNewSetKelas;
var selectCountSetKelas=50;
var rowSelectedSetKelas;
var SetKelasLookUps;

 CurrentPage.page = getPanelSetKelas(CurrentPage.id);
 mainPage.add(CurrentPage.page);
 mainPage.setActiveTab(CurrentPage.id);

 function getPanelSetKelas(mod_id)
 {
     var Field =['KD_KELAS','KELAS','PARENT','ASKES'];
     dsSetKelasList = new WebApp.DataStore({ fields: Field });
     RefreshDataSetKelas();

     var grListSetKelas = new Ext.grid.EditorGridPanel
     (
        {
            id: 'grListSetKelas',
            stripeRows: true,
            store: dsSetKelasList,
            autoScroll: true,
            columnLines: true,
            border: false,
            anchor: '100% 100%',
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                        {
                            rowselect: function(sm, row, rec)
                            {
                                rowSelectedSetKelas=undefined;
                                rowSelectedSetKelas = dsSetKelasList.getAt(row);
                            }
                        }
                }
            ),
            listeners:
		{
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelectedSetKelas = dsSetKelasList.getAt(ridx);
                                if (rowSelectedSetKelas != undefined)
                                 {
                                    SetKelasLookUp(rowSelectedSetKelas.data);
                                 }
                                else
                                 {
                                    SetKelasLookUp();
                                 };
			}
		},
           cm: new Ext.grid.ColumnModel
            (
		[
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetKelas',
                        header: 'Kode Kelas',
                        dataIndex: 'KD_KELAS',
                        sortable: true,
                        width: 100
                    },
                    {
			id: 'colSetKelas',
			header: 'Kelas',
			dataIndex: 'KELAS',
			width: 100,
			sortable: true
                    },
                    {
                        id: 'colSetParent',
                        header: 'Parent',
                        dataIndex: 'PARENT',
                        width: 100,
                        sortable: true
                    },
                    {
                        id: 'colSetAskes',
                        header: 'Askes',
                        dataIndex: 'ASKES',
                        width: 100,
                        sortable: true
                    }
                ]
            ),
            bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetKelasList,
            pageSize: selectCountSetKelas,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetKelas',
				    text: 'Edit',
                                    iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec)
					{
						if (rowSelectedSetKelas != undefined)
						{
						    SetKelasLookUp(rowSelectedSetKelas.data);
						}
						else
						{
						    SetKelasLookUp();
						}
				    }
				},' ','-'
			]
		    ,viewConfig: { forceFit: true }
		}
	);
     var FormSetKelas = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Kelas',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupKelas',
		    items: [grListSetKelas],
		    tbar:
			[
				'Kelas' + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Kelas : ',
					id: 'txtSetKelasFilter',
					anchor: '95%',
					onInit: function() { }
				}, ' ', '-',
                                'Askes ' + ' : ', ' ',mComboStatusKelasRequestView()
                                 ,' ', '-',
				{
				    id: 'btnRefreshSetKelas',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec)
					{
						RefreshDataSetKelasFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function()
				{
					//Ext.getCmp('cboDESKRIPSI').store = getSetKelas();
				}
			}
		}
	);
    //END var FormSetKelas--------------------------------------------------
     RefreshDataSetKelas();
    return FormSetKelas ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///

function SetKelasLookUp(rowdata)
{
    var lebar=600;
    SetKelasLookUps = new Ext.Window
    (
		{
		    id: 'SetKelasLookUps',
		    title: 'Kelas',
		    closeAction: 'destroy',
                    y:90,
		    width: lebar,
		    height: 250,
                    resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupKelas',
		    modal: true,
		    items: getFormEntrySetKelas(lebar),
		    listeners:
            {
                activate: function()
                {
                },
				afterShow: function()
				{
					this.activate();
				},
				deactivate: function()
				{
					rowSelectedSetKelas=undefined;
					RefreshDataSetKelas();
				}
            }
		}
	);


    SetKelasLookUps.show();
	if (rowdata == undefined)
	{
		SetKelasAddNew();
	}
	else
	{
		SetKelasInit(rowdata)
	}
};


function getFormEntrySetKelas(lebar)
{
    var pnlSetKelas = new Ext.FormPanel
    (
		{
		    id: 'PanelSetKelas',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 250,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupKelas',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
                                    bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 150,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetKelas',
                                                    labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: 'Kode ',
								    name: 'txtKode_SetKelas',
								    id: 'txtKode_SetKelas',
								    anchor: '40%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Nama ',
								    name: 'txtNameSetKelas',
								    id: 'txtNameSetKelas',
								    anchor: '100%'
								},
                                                                {
								    xtype: 'textfield',
								    fieldLabel: 'Parent ',
								    name: 'txtParentSetKelas',
								    id: 'txtParentSetKelas',
								    anchor: '100%'
								},
                                                                  mComboAksesKelasRequestView()

                                                               
                                                                
								    
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetKelas',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',
				    handler: function()
					{
						SetKelasAddNew()
					}
				}, '-',
				{
				    id: 'btnSimpanSetKelas',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',
				    handler: function()
					{
						SetKelasSave(false);
						RefreshDataSetKelas();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetKelas',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function()
					{
						var x = SetKelasSave(true);
						RefreshDataSetKelas();
						if (x===undefined)
						{
							SetKelasLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetKelas',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function()
					{
							SetKelasDelete() ;
							RefreshDataSetKelas();
					}
				},'-','->','-',
				{
					id:'btnPrintSetKelas',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',
					handler: function()
					{
						//LoadReport(950002);
					}
				}
			]
		}
	);

    return pnlSetKelas
};


function SetKelasSave(mBol)
{
    if (ValidasiEntrySetKelas(nmHeaderSimpanData,false) == 1 )
    {
            if (AddNewSetKelas == true)
            {
                    Ext.Ajax.request
                    (
                            {
                                    url: WebAppUrl.UrlSaveData,
                                    params: getParamSetKelas(),
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoSetKelas(nmPesanSimpanSukses,nmHeaderSimpanData);
                                                    RefreshDataSetKelas();
                                                    if(mBol === false)
                                                    {
                                                            Ext.get('txtKode_SetKelas').dom.value=cst.KelasId;
                                                    };
                                                    AddNewSetKelas = false;

                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanWarningSetKelas(nmPesanSimpanGagal,nmHeaderSimpanData);
                                            }
                                            else
                                            {
                                                    ShowPesanErrorSetKelas(nmPesanSimpanError,nmHeaderSimpanData);
                                            };
                                    }
                            }
                    )
            }
            else
            {
                    Ext.Ajax.request
                     (
                            {
                                    url: WebAppUrl.UrlUpdateData,
                                    params: getParamSetKelas(),
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoSetKelas(nmPesanEditSukses,nmHeaderEditData);
                                                    RefreshDataSetKelas();
                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanWarningSetKelas(nmPesanEditGagal,nmHeaderEditData);
                                            }
                                            else
                                            {
                                                    ShowPesanErrorSetKelas(nmPesanEditError,nmHeaderEditData);
                                            };
                                    }
                            }
                    )
            };
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };

};//END FUNCTION SetKelasSave
///---------------------------------------------------------------------------------------///

function SetKelasDelete()
{
	if (ValidasiEntrySetKelas(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmSatuan) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn)
			   {
					if (btn =='yes')
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetKelas(),
								success: function(o)
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true)
									{
										ShowPesanInfoSetKelas(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetKelas();
										SetKelasAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetKelas(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetKelas(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetKelas(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetKelas').getValue() == '' || (Ext.get('txtNameSetKelas').getValue() == '' || (Ext.get('txtParentSetKelas').getValue() == '' || (Ext.get('cboDataSetKelasAskes').getValue() == '' ))))
	{
		if (Ext.get('txtKode_SetKelas').getValue() == '' && mBolHapus === true)
		{
			x=0;
                }
		else if (Ext.get('txtNameSetKelas').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetKelas(nmGetValidasiKosong('Nama Kelas'),modul);
			};
		}
                else if (Ext.get('txtParentSetKelas').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetKelas(nmGetValidasiKosong('Parent'),modul);
			};
		}
                else if (Ext.get('cboDataSetKelasAskes').getValue() === 'Please Select One')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetKelas(nmGetValidasiKosong('Askes'),modul);
			};
		};
	};


	return x;
};

function ShowPesanWarningSetKelas(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetKelas(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetKelas(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetKelasInit(rowdata)
{
    AddNewSetKelas = false;
    Ext.get('txtKode_SetKelas').dom.value = rowdata.KD_KELAS;
    Ext.get('txtNameSetKelas').dom.value = rowdata.KELAS;
    Ext.get('txtParentSetKelas').dom.value = rowdata.PARENT;
    Ext.get('cboDataSetKelasAskes').dom.value = rowdata.ASKES;
    if (Ext.get('cboDataSetKelasAskes').dom.value === 'f')
    {
        Ext.get('cboDataSetKelasAskes').dom.value = 'False'
    }else
        {
            Ext.get('cboDataSetKelasAskes').dom.value = 'True'
        }
};
///---------------------------------------------------------------------------------------///

function SetKelasAddNew()
{
    AddNewSetKelas = true;
    Ext.get('txtKode_SetKelas').dom.value = '';
    Ext.get('txtNameSetKelas').dom.value = '';
    Ext.get('txtParentSetKelas').dom.value = '';
    Ext.get('cboDataSetKelasAskes').dom.value = 'Please Select One';
    rowSelectedSetKelas   = undefined;
};
///---------------------------------------------------------------------------------------///

function getParamSetKelas()
{
    var params =
	{
            Table: 'ViewSetupKelas',
	    KdKelas: Ext.get('txtKode_SetKelas').getValue(),
	    Kelas: Ext.get('txtNameSetKelas').getValue(),
            Parent: Ext.get('txtParentSetKelas').getValue(),
	    Askes: Ext.get('cboDataSetKelasAskes').getValue()
	};
    return params
};

function RefreshDataSetKelas()
{
	dsSetKelasList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountSetKelas,
					Sort: 'kd_kelas',
					Sortdir: 'ASC',
					target:'ViewSetupKelas',
					param : ''
				}
			}
		);

	rowSelectedSetKelas = undefined;
	return dsSetKelasList;
};

function RefreshDataSetKelasFilter()
{
   var KataKunci='';
   if (Ext.get('txtSetKelasFilter').getValue() != '')
    {
		KataKunci = ' kelas like ~%' + Ext.get('txtSetKelasFilter').getValue() + '%~';
    };
    if (Ext.get('cboMaksDataSetKelas').getValue() != 'All')
        {
            if (KataKunci === '')
                {
                    KataKunci = ' askes = ~' + Ext.get('cboMaksDataSetKelas').getValue() + '~';
                }
                else
                    {
                    KataKunci += ' and askes = ~' + Ext.get('cboMaksDataSetKelas').getValue() + '~';
                    };
        };
    

   

    if (KataKunci != undefined)
    {
		dsSetKelasList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountSetKelas,
					//Sort: 'Kelas_ID',
                                        Sort: 'kd_kelas',
					Sortdir: 'ASC',
					target:'ViewSetupKelas',
					param : KataKunci
				}
			}
		);
    }
	else
	{
		RefreshDataSetKelas();
	};
};

function mComboStatusKelasRequestView()
{
  var cboMaksDataSetKelas = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetKelas',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'All'],[2, 'True'], [3, 'False']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'All',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountSetKelas=b.data.displayText ;
					RefreshDataSetKelas();
				}
			}
		}
	);
	return cboMaksDataSetKelas;
};

function mComboAksesKelasRequestView()
{
    var cboDataSetKelasAskes = new Ext.form.ComboBox
	(
		{
			id:'cboDataSetKelasAskes',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',
			width:150,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'True'], [2, 'False']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Please Select One',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountSetKelas=b.data.displayText ;
					RefreshDataSetKelas();
				}
			}
		}
	);
	return cboDataSetKelasAskes;
};