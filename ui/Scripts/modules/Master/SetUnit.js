/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

 var dsSetUnitList;
 var AddNewSetUnit;
 var selectCountSetUnit=50;
 var rowSelectedSetUnit;
 var SetUnitLookUps;
 var dsKelasUnitRequestEntry;
 var selectKelasUnitRequestEntry;
 var dsBagianUnitRequestEntry;
 var selectBagianUnitRequestEntry;

 CurrentPage.page = getPanelSetUnit(CurrentPage.id);
 mainPage.add(CurrentPage.page);
 mainPage.setActiveTab(CurrentPage.id);

 function getPanelSetUnit(mod_id)
 {
     var Field =['KD_UNIT','KD_BAGIAN','KD_KELAS','NAMA_UNIT','PARENT','TYPE_UNIT','AKTIF','SPESIAL'];
     dsSetUnitList = new WebApp.DataStore({fields: Field});
     RefreshDataSetUnit();

     var grListSetUnit = new Ext.grid.EditorGridPanel
     (
        {
            id: 'grListSetUnit',
            stripeRows: true,
            store: dsSetUnitList,
            autoScroll: true,
            columnLines: true,
            border: false,
            anchor: '100% 100%',
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                        {
                            rowselect: function(sm, row, rec)
                            {
                                rowSelectedSetUnit=undefined;
                                rowSelectedSetUnit = dsSetUnitList.getAt(row);
                            }
                        }
                }
            ),
            listeners:
		{
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelectedSetUnit = dsSetUnitList.getAt(ridx);
                                if (rowSelectedSetUnit != undefined)
                                 {
                                    SetUnitLookUp(rowSelectedSetUnit.data);
                                 }
                                else
                                 {
                                    SetUnitLookUp();
                                 };
			}
		},
           cm: new Ext.grid.ColumnModel
            (
		[
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetUnit',
                        header: 'Kode Unit',
                        dataIndex: 'KD_UNIT',
                        sortable: true,
                        width: 200
                    },
                    {
                        id: 'colKodeSetBagian',
                        header: 'Kode Bagian',
                        dataIndex: 'KD_BAGIAN',
                        sortable: true,
                        width: 200
                    },
                    {
                        id: 'colKodeSetKelas',
                        header: 'Kode Kelas',
                        dataIndex: 'KD_KELAS',
                        sortable: true,
                        width: 200
                    },
                     {
			id: 'colSetUnit',
			header: 'Unit',
			dataIndex: 'NAMA_UNIT',
			width: 300,
			sortable: true
                    },
                    {
			id: 'colSetParent',
			header: 'Parent',
			dataIndex: 'PARENT',
			width: 300,
			sortable: true
                    },
                    {
			id: 'colSetType',
			header: 'Type',
			dataIndex: 'TYPE_UNIT',
			width: 300,
			sortable: true
                    },
                    {
			id: 'colSetAktif',
			header: 'Aktif',
			dataIndex: 'AKTIF',
			width: 300,
			sortable: true
                    },
                    {
			id: 'colSetSpesial',
			header: 'Spesial',
			dataIndex: 'SPESIAL',
			width: 300,
			sortable: true
                    }
                ]
            ),
            bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetUnitList,
            pageSize: selectCountSetUnit,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetUnit',
				    text: 'Edit',
                                    iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec)
					{
						if (rowSelectedSetUnit != undefined)
						{
						    SetUnitLookUp(rowSelectedSetUnit.data);
						}
						else
						{
						    SetUnitLookUp();
						}
				    }
				},' ','-'
			]
		    ,viewConfig: {forceFit: true}
		}
	);
     var FormSetUnit = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupUnit',
		    items: [grListSetUnit],
		    tbar:
			[
				'Kode Unit' + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Id Unit : ',
					id: 'txtKDSetUnitFilter',
					width:80,
					onInit: function() { }
				}, ' ','-',
                                'Nama Unit' + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Nama Unit : ',
					id: 'txtSetUnitFilter',
					anchor: '95%',
					onInit: function() { }
				}, ' ',
				{
				    id: 'btnRefreshSetUnit',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec)
					{
						RefreshDataSetUnitFilter();
					}
				}
			],
		    listeners:
			{'afterrender': function()
				{
					//Ext.getCmp('cboDESKRIPSI').store = getSetUnit();
				}
			}
		}
	);
    //END var FormSetUnit--------------------------------------------------
     RefreshDataSetUnit();
    return FormSetUnit ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///

function SetUnitLookUp(rowdata)
{
    var lebar=600;
    SetUnitLookUps = new Ext.Window
    (
		{
		    id: 'SetUnitLookUps',
		    title: 'Unit',
		    closeAction: 'destroy',
                    y:90,
		    width: lebar,
		    height: 600,
                    resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupUnit',
		    modal: true,
		    items: getFormEntrySetUnit(lebar),
		    listeners:
            {
                activate: function()
                {
                },
				afterShow: function()
				{
					this.activate();
				},
				deactivate: function()
				{
					rowSelectedSetUnit=undefined;
					RefreshDataSetUnit();
				}
            }
		}
	);


    SetUnitLookUps.show();
	if (rowdata == undefined)
	{
		SetUnitAddNew();
	}
	else
	{
		SetUnitInit(rowdata)
	}
};


function getFormEntrySetUnit(lebar)
{
    var pnlSetUnit = new Ext.FormPanel
    (
		{
		    id: 'PanelSetUnit',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 600,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupUnit',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
                                    bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 500,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetUnit',
                                                    labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: 'Kode Unit ',
								    name: 'txtKode_SetUnit',
								    id: 'txtKode_SetUnit',
								    anchor: '40%',
									readOnly:true
								},
                                                                mComboBagianUnitRequestEntry(),
//								{
//								    xtype: 'textfield',
//								    fieldLabel: 'Kode Bagian ',
//								    name: 'txtKode_SetBagian',
//								    id: 'txtKode_SetBagian',
//								    anchor: '40%'
//								},
                                                                mComboKelasUnitRequestEntry(),
//                                                                {
//								    xtype: 'textfield',
//								    fieldLabel: 'Kode Kelas ',
//								    name: 'txtKode_SetKelas',
//								    id: 'txtKode_SetKelas',
//								    anchor: '40%'
//								},
                                                                {
								    xtype: 'textfield',
								    fieldLabel: 'Nama Unit ',
								    name: 'txtNameSetUnit',
								    id: 'txtNameSetUnit',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Parent ',
								    name: 'txtNameSetParent',
								    id: 'txtNameSetParent',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Type ',
								    name: 'txtNameSetType',
								    id: 'txtNameSetType',
								    anchor: '100%'
								},
                                                                {
								    xtype: 'textfield',
								    fieldLabel: 'Aktif ',
								    name: 'txtNameSetAktif',
								    id: 'txtNameSetAktif',
								    anchor: '100%'
                                                                },
								{
								    xtype: 'textfield',
								    fieldLabel: 'Spesial ',
								    name: 'txtNameSetSpesial',
								    id: 'txtNameSetSpesial',
								    anchor: '100%'
								},
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetUnit',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',
				    handler: function()
					{
						SetUnitAddNew()
					}
				}, '-',
				{
				    id: 'btnSimpanSetUnit',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',
				    handler: function()
					{
						SetUnitSave(false);
						RefreshDataSetUnit();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetUnit',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function()
					{
						var x = SetUnitSave(true);
						RefreshDataSetUnit();
						if (x===undefined)
						{
							SetUnitLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetUnit',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function()
					{
							SetUnitDelete() ;
							RefreshDataSetUnit();
					}
				},'-','->','-',
				{
					id:'btnPrintSetUnit',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',
					handler: function()
					{
						//LoadReport(950002);
					}
				}
			]
		}
	);

    return pnlSetUnit
};


function SetUnitSave(mBol)
{
    if (ValidasiEntrySetUnit(nmHeaderSimpanData,false) == 1 )
    {
            if (AddNewSetUnit == true)
            {
                    Ext.Ajax.request
                    (
                            {
                                    url: WebAppUrl.UrlSaveData,
                                    params: getParamSetUnit(),
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoSetUnit(nmPesanSimpanSukses,nmHeaderSimpanData);
                                                    RefreshDataSetUnit();
                                                    if(mBol === false)
                                                    {
                                                            Ext.get('txtKode_SetUnit').dom.value=cst.kd_Unit;
                                                    };
                                                    AddNewSetUnit = false;

                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanWarningSetUnit(nmPesanSimpanGagal,nmHeaderSimpanData);
                                            }
                                            else
                                            {
                                                    ShowPesanErrorSetUnit(nmPesanSimpanError,nmHeaderSimpanData);
                                            };
                                    }
                            }
                    )
            }
            else
            {
                    Ext.Ajax.request
                     (
                            {
                                    url: WebAppUrl.UrlUpdateData,
                                    params: getParamSetUnit(),
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoSetUnit(nmPesanEditSukses,nmHeaderEditData);
                                                    RefreshDataSetUnit();
                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanWarningSetUnit(nmPesanEditGagal,nmHeaderEditData);
                                            }
                                            else
                                            {
                                                    ShowPesanErrorSetUnit(nmPesanEditError,nmHeaderEditData);
                                            };
                                    }
                            }
                    )
            };
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };

};//END FUNCTION SetUnitSave
///---------------------------------------------------------------------------------------///

function SetUnitDelete()
{
	if (ValidasiEntrySetUnit(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmSatuan) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn)
			   {
					if (btn =='yes')
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetUnit(),
								success: function(o)
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true)
									{
										ShowPesanInfoSetUnit(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetUnit();
										SetUnitAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetUnit(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetUnit(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetUnit(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetUnit').getValue() == '' || (Ext.get('txtNameSetUnit').getValue() == ''))
	{
		if (Ext.get('txtKode_SetUnit').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetUnit(nmGetValidasiKosong(nmKdSatuan),modul);
			x=0;
		}
		else if (Ext.get('txtNameSetUnit').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetUnit(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};

function ShowPesanWarningSetUnit(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetUnit(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetUnit(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetUnitInit(rowdata)
{
    AddNewSetUnit = false;
    Ext.get('txtKode_SetUnit').dom.value = rowdata.KD_UNIT;
    Ext.get('cboBagianUnitRequestEntry').dom.value = rowdata.KD_BAGIAN;
    Ext.get('cboKelasUnitRequestEntry').dom.value = rowdata.KD_KELAS;
    Ext.get('txtNameSetUnit').dom.value = rowdata.NAMA_UNIT;
    Ext.get('txtNameSetParent').dom.value = rowdata.PARENT;
    Ext.get('txtNameSetType').dom.value = rowdata.TYPE_UNIT;
    Ext.get('txtNameSetAktif').dom.value = rowdata.AKTIF;
    Ext.get('txtNameSetSpesial').dom.value = rowdata.SPESIAL;
};
///---------------------------------------------------------------------------------------///

function SetUnitAddNew()
{
    AddNewSetUnit = true;
    Ext.get('txtKode_SetUnit').dom.value = '';
    Ext.get('cboBagianUnitRequestEntry').dom.value = '';
    Ext.get('cboKelasUnitRequestEntry').dom.value = '';
    Ext.get('txtNameSetUnit').dom.value = '';
    Ext.get('txtNameSetParent').dom.value = '';
    Ext.get('txtNameSetType').dom.value = '';
    Ext.get('txtNameSetAktif').dom.value = '';
    Ext.get('txtNameSetSpesial').dom.value = '';
    rowSelectedSetUnit   = undefined;
    selectBagianUnitRequestEntry = undefined;
    selectKelasUnitRequestEntry = undefined;

};
///---------------------------------------------------------------------------------------///

function getParamSetUnit()
{
    var params =
	{
            Table: 'ViewSetupUnit',
	    KdUnit:   Ext.get('txtKode_SetUnit').getValue(),
	    KdBagian: selectBagianUnitRequestEntry,
            KdKelas: selectKelasUnitRequestEntry ,
            NamaUnit: Ext.get('txtNameSetUnit').getValue(),
            Parent:   Ext.get('txtNameSetParent').getValue(),
            Type:     Ext.get('txtNameSetType').getValue(),
            Aktif:    Ext.get('txtNameSetAktif').getValue(),
            Spesial:  Ext.get('txtNameSetSpesial').getValue()
	};
    return params
};

function RefreshDataSetUnit()
{
	dsSetUnitList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountSetUnit,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target:'ViewSetupUnit',
					param : ''
				}
			}
		);

	rowSelectedSetUnit = undefined;
	return dsSetUnitList;
};

function RefreshDataSetUnitFilter()
{
	var KataKunci='';
    if (Ext.get('txtKDSetUnitFilter').getValue() != '')
    {
		//KataKunci = ' where Unit_ID like ~%' + Ext.get('txtKDSetUnitFilter').getValue() + '%~';
                KataKunci = ' kd_unit like ~%' + Ext.get('txtKDSetUnitFilter').getValue() + '%~';
	};

    if (Ext.get('txtSetUnitFilter').getValue() != '')
    {
		if (KataKunci === '')
		{
			//KataKunci = ' where Unit like  ~%' + Ext.get('txtSetUnitFilter').getValue() + '%~';
                        KataKunci = ' nama_unit like  ~%' + Ext.get('txtSetUnitFilter').getValue() + '%~';
		}
		else
		{
			//KataKunci += ' and  Unit like  ~%' + Ext.get('txtSetUnitFilter').getValue() + '%~';
                        KataKunci += ' and nama_unit like  ~%' + Ext.get('txtSetUnitFilter').getValue() + '%~';
		};
	};

    if (KataKunci != undefined)
    {
		dsSetUnitList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountSetUnit,
                                        Sort: 'kd_unit',
					Sortdir: 'ASC',
					target:'ViewSetupUnit',
					param : KataKunci
				}
			}
		);
    }
	else
	{
		RefreshDataSetUnit();
	};
};

function mComboMaksDataSetUnit()
{
  var cboMaksDataSetUnit = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetUnit',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetUnit,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountSetUnit=b.data.displayText ;
					RefreshDataSetUnit();
				}
			}
		}
	);
	return cboMaksDataSetUnit;
};

function mComboKelasUnitRequestEntry()
{
    var Field = ['KD_KELAS','KELAS'];

    dsKelasUnitRequestEntry = new WebApp.DataStore({fields: Field});
    dsKelasUnitRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_kelas',
			    Sortdir: 'ASC',
			    target: 'ViewComboKelas',
			    param: ''
			}
		}
	);

    var cboKelasUnitRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKelasUnitRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Please Select One',
		    fieldLabel: 'Kelas',
		    align: 'Right',
		    anchor:'60%',
		    store: dsKelasUnitRequestEntry,
		    valueField: 'KD_KELAS',
		    displayField: 'KELAS',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        selectKelasUnitRequestEntry = b.data.KD_KELAS;
			    }
			}
		}
	);

    return cboKelasUnitRequestEntry;
};

function mComboBagianUnitRequestEntry()
{
    var Field = ['KD_BAGIAN','BAGIAN'];

    dsBagianUnitRequestEntry = new WebApp.DataStore({fields: Field});
    dsBagianUnitRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_bagian',
			    Sortdir: 'ASC',
			    target: 'ViewComboBagian',
			    param: ''
			}
		}
	)

    var cboBagianUnitRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboBagianUnitRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Please Select One',
		    fieldLabel: 'Bagian ',
		    align: 'Right',
		    anchor:'60%',
		    store: dsBagianUnitRequestEntry,
		    valueField: 'KD_BAGIAN',
		    displayField: 'BAGIAN',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        selectBagianUnitRequestEntry = b.data.KD_BAGIAN;
			    }
			}
		}
	);

    return cboBagianUnitRequestEntry;
};
 
