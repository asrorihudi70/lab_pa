var dsUnitList;
var AddNewUnit = false;
var selectCountUnit = 50;
var selectAccountUnit;
var UnitLookUps;
var rowSelectedUnit;
var selectCboInterfaceSetInterface;
var selectUnitKerja_aci ;
CurrentPage.page = getPanelUnit(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelUnit(mod_id) {
    var Field = ['INT_CODE','DESCRIPTION','ACCOUNT','Name','AKUN','CODE','KD_UNIT_KERJA','NAMA_UNIT_KERJA'];
    dsUnitList = new WebApp.DataStore({ fields: Field });
   
    RefreshDataUnit();

    var grListUnit = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListUnit',
		    stripeRows: true,
		    store: dsUnitList,
		    autoScroll: true,
		    columnLines: true,
		    border: false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
					        rowSelectedUnit = dsUnitList.getAt(row);
					    }
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'KD_UNIT',
					    header: "Kode Unit ",
					    dataIndex: 'KD_UNIT',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'K',
					    header: "Cash In Bank" ,
					    dataIndex: 'Name',
					    width: 100,
					    sortable: true
					},
                    {
                        id: 'Int_Code',
                        header: "Fixed Assets",
                        dataIndex: 'INT_CODE',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'Description',
					    header: "Sales ",
					    dataIndex: 'DESCRIPTION',
					    width: 100,
					    sortable: true
					},
					{
					    id: 'RetainedEarning',
					    header: "Retained Earning ",
					    dataIndex: 'DESCRIPTION',
					    width: 100,
					    sortable: true
					},
					{
					    id: 'CurrentEarning',
					    header: "Current Earning",
					    dataIndex: '',
					    width: 100,
					    sortable: true
					}
                ]
			),
		    tbar:
			[
				{
				    id: 'btnEditUnit',
				    text: 'Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) {
				        if (rowSelectedUnit != undefined) 
						{
				            UnitLookUp(rowSelectedUnit.data);
				        }
				        else 
						{
				            UnitLookUp();
				        }
				    }
				}, '-'
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsUnitList,
					pageSize: selectCountUnit,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			)
		}
	);
    //END var grListUnit -----------------------------------------------------------

    var FormUnit = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Akun Interface',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupInterface',
		    items: [grListUnit],
		    tbar:
			[
				'Kode : ', ' ',
				{
				    xtype: 'textfield',
				    fieldLabel: 'Kode : ',
				    id: 'txtKDUnitFilter',
				    width: 80,
				    onInit: function() { }
				}, ' ', '-',
				'Maks.Data : ', ' ', mComboMaksDataUnit(),
				' ', '->',
				{
				    id: 'btnRefreshUnit',
				    tooltip: 'Refresh',
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{
				        RefreshDataUnitFilter();
				    }
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{
					//Ext.getCmp('cboDESKRIPSI').store = getUnit();
				}
			}
		}
	);
    //END var FormUnit-----------------------------------------------------------------------


    RefreshDataUnit();
	//RefreshDataUnit();

    return FormUnit
};
// end function get panel main data
///-----------------------------------------------------------------------------------------------///


function UnitLookUp(rowdata) {
    var lebar = 350;//600;
    UnitLookUps = new Ext.Window
    (
		{
		    id: 'UnitLookUps',
		    title: 'Akun Interface',
		    closeAction: 'destroy',
		    y: 90,
		    width: lebar,
		    height: 250,
		    resizable: false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupInterface',
		    modal: true,
		    items: getFormEntryUnit(lebar),
		    listeners:
            {
                activate: function() 
				{ 
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedUnit=undefined;
					RefreshDataUnit();
				}
            }
		}
	);
    //END var UnitLookUps---------------------------------------------------------

    UnitLookUps.show();
    if (rowdata == undefined) {
        UnitAddNew();
    }
    else {
        UnitInit(rowdata)
    }
};

//  END FUNCTION UnitLookUp
///------------------------------------------------------------------///

function mcomboUnitKerja_Unit()
{
	var Field = ['KD_UNIT_KERJA', 'NAMA_UNIT_KERJA','UNITKERJA'];
	dsUnitKerja_aci = new WebApp.DataStore({ fields: Field });
	
  var comboUnitKerja_aci = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_aci',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Unit Kerja...',
			fieldLabel: 'Unit Kerja ',			
			align:'Right',
			anchor:'100%',
			store: dsUnitKerja_aci,
			valueField: 'KD_UNIT_KERJA',
			displayField: 'UNITKERJA',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					//selectAktivaLancar_kbs = "";
					//Ext.getCmp('comboAktivaLancar_kbs').setValue("");
				//	Ext.get('comboAktivaLancar_kbs').dom.value = "";
					
					selectUnitKerja_aci=b.data.KD_UNIT_KERJA ;
					
					/*dsAktivaLancarPenerimaanMhs.load
					(
						{
							params:
							{
								Skip: 0,
								Take: 1000,
								Sort: '',
								Sortdir: 'ASC',
								target: 'viCboAktivaLancar',
								param: kriteria
							}
						}
					);*/
					
				} 
			}
		}
	);
	
	dsUnitKerja_aci.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_UNIT_KERJA',
			    Sortdir: 'ASC',
			    target: 'viCboUnitKerja',
			    param: ''
			}
		}
	);
	
	return comboUnitKerja_aci;
} ;

function getFormEntryUnit(lebar) {
    var pnlUnit = new Ext.FormPanel
    (
		{
		    id: 'PanelUnit',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 250,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupUnit',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width: lebar - 36.5,
				    bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 'auto',
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth: .989,
						    layout: 'form',
						    id: 'PnlKiriUnit',
						    labelWidth: 120,
						    border: false,
						    items:
							[
								
								{
									xtype: 'compositefield',
									fieldLabel: 'Code On Hand ',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtCOHUnit',
											width: 120,
											readOnly: true
										}
										
									]
								},									
								{
									xtype: 'compositefield',
									fieldLabel: 'Cash In Bank ',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtCIBUnit',
											width: 120,
											readOnly: true
										}
										
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Fixed Assets',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtFSUnit ',
											width: 120,
											readOnly: true
										}
										
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Sales ',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtSalesUnit ',
											width: 120,
											readOnly: true
										}
										
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Retained Earning ',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtREUnit ',
											width: 120,
											readOnly: true
										}
										
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Current Earning ',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtCEUnit ',
											width: 120,
											readOnly: true
										}
										
									]
								},								
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddUnit',
				    text: 'Tambah',
				    tooltip: 'Tambah Record Baru ',
				    iconCls: 'add',
				    handler: function() { UnitAddNew() }
				}, '-',
				{
				    id: 'btnSimpanUnit',
				    text: 'Simpan',
				    tooltip: 'Simpan Data ',
				    iconCls: 'save',
				    handler: function() {
				        UnitSave(false);
				        RefreshDataUnit();
				    }
				}, '-',
				{
				    id: 'btnSimpanCloseUnit',
				    text: 'Simpan & Keluar',
				    tooltip: 'Simpan dan Keluar',
				    iconCls: 'saveexit',
				    handler: function() {
				        var x = UnitSave(true);
				        RefreshDataUnit();
				        if (x === undefined) {
				            UnitLookUps.close();
				        };
				    }
				},'-', '->', '-',
				{
				    id: 'btnPrintUnit',
				    text: ' Cetak',
				    tooltip: 'Cetak',
				    iconCls: 'print',
				    handler: function() {
				        //ShowFormLapAccountUIs();						
						//ShowReport('','011107',Ext.getCmp('txtKode_Unit').getValue())
						ShowReport('','011107',selectCboInterfaceSetInterface);
				    }
				}
			]
		}
	);
    // END VAR pnlUnit ------------------------------------------------------------------------ 
    return pnlUnit
};
//END FUNCTION getFormEntryUnit
///----------------------------------------------------------------------------------------------///

function UnitSave(mBol) {
    if (ValidasiEntryUnit('Simpan Data') == 1) {
        if (AddNewUnit == true) {
            Ext.Ajax.request
				(
					{
					    url: "./Datapool.mvc/CreateDataObj",
					    params: getParamUnit(),
					    success: function(o) {
					        var cst = o.responseText;
					        if (cst == '{"success":true}') {
					            ShowPesanInfoUnit('Data berhasil di simpan', 'Simpan Data');
					            RefreshDataUnit();
								 if (mBol === false) 
								{
									Ext.getCmp('comboInterfaceSetInterface').setReadOnly(true);
									//Ext.get('txtKode_Unit').dom.readOnly=true;
									Ext.get('comboAccountUnit').dom.readOnly=true;
								}
								AddNewUnit = false;
					        }
					        else if (cst == '{"pesan":0,"success":false}') {
					            ShowPesanWarningUnit('Data tidak berhasil di simpan, data tersebut sudah ada', 'Simpan Data');
					        }
					        else {
					            ShowPesanErrorUnit('Data tidak berhasil di simpan', 'Simpan Data');
					        }
					    }
					}
				)
        }
        else {
            Ext.Ajax.request
			(
				{
				    url: "./Datapool.mvc/UpdateDataObj",
				    params: getParamUnit(),
				    success: function(o) {
				        var cst = o.responseText;
				        if (cst == '{"success":true}') 
						{
				            ShowPesanInfoUnit('Data berhasil di edit', 'Edit Data');
				            RefreshDataUnit();
				            //UnitAddNew();
				        }
				        else if (cst == '{"pesan":0,"success":false}') {
				            ShowPesanWarningUnit('Data tidak berhasil di edit, data tersebut belum ada', 'Edit Data');
				        }
				        else {
				            ShowPesanErrorUnit('Data tidak berhasil di edit', 'Edit Data');
				        }
				    }
				}
			)
        }

    }
    else {
        if (mBol === true) {
            return false;
        };
    };
}; //END FUNCTION UnitSave
///---------------------------------------------------------------------------------------///

function UnitDelete() {
    if (ValidasiEntryUnit('Hapus Data') == 1) {
        Ext.Ajax.request
		(
			{
			    url: "./Datapool.mvc/DeleteDataObj",
			    params: getParamUnit(),
			    success: function(o) {
			        var cst = o.responseText;
			        if (cst == '{"success":true}') {
			            ShowPesanInfoUnit('Data berhasil di hapus', 'Hapus Data');
			            RefreshDataUnit();
			            UnitAddNew();
			        }
			        else if (cst == '{"pesan":0,"success":false}') {
			            ShowPesanWarningUnit('Data tidak berhasil di hapus, data tersebut belum ada', 'Hapus Data');
			        }
			        else {
			            ShowPesanErrorUnit('Data tidak berhasil di hapus', 'Hapus Data');
			        }
			    }
			}
		)
    }
};

function ValidasiEntryUnit(modul) {
    var x = 1;	
	
	if(Ext.getCmp('comboInterfaceSetInterface').getValue() == "" || selectCboInterfaceSetInterface === undefined)
	{
		ShowPesanWarningUnit("Akun spesifik belum dipilih", "Akun Interface");
		x = 0;
	}
	
	if(Ext.getCmp('comboAccountUnit').getValue() == "" || selectAccountUnit === undefined)
	{
		ShowPesanWarningUnit("Akun interface belum dipilih", "Akun Interface");
		x = 0;
	}
	
    return x;
};

function ShowPesanWarningUnit(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorUnit(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoUnit(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO
		}
	);
};


//-----------------------------------------------------------------------------------------------------
function UnitInit(rowdata) {
    AddNewUnit = false;
	Ext.getCmp('comboInterfaceSetInterface').setValue(rowdata.CODE);
	Ext.getCmp('comboInterfaceSetInterface').setReadOnly(true);
	selectCboInterfaceSetInterface = rowdata.INT_CODE;
    Ext.getCmp('comboAccountUnit').setValue(rowdata.AKUN);
	Ext.getCmp('comboAccountUnit').setReadOnly(true);
    selectAccountUnit = rowdata.ACCOUNT;
    Ext.get('txtDeskripsiUnit').dom.value = rowdata.DESCRIPTION;
	
	Ext.get('comboUnitKerja_aci').dom.value = rowdata.NAMA_UNIT_KERJA;	
	selectUnitKerja_aci = rowdata.KD_UNIT_KERJA;
    //Ext.get('txtKode_Unit').dom.value = rowdata.Int_Code;
    //Ext.get('txtUnit').dom.value = rowdata.Account;
	//Ext.get('txtKode_Unit').dom.readOnly=true;
	//Ext.get('txtUnit').dom.readOnly=true;
};
///---------------------------------------------------------------------------------------///

function UnitAddNew(pnlUnit) {
    AddNewUnit = true;
	
	Ext.getCmp('comboInterfaceSetInterface').reset();
	//Ext.getCmp('comboInterfaceSetInterface').setreadOnly(false);
	//Ext.get('comboInterfaceSetInterface').dom.readOnly=false;
	Ext.getCmp('comboInterfaceSetInterface').setReadOnly(false);
	selectCboInterfaceSetInterface = undefined;
    Ext.get('comboAccountUnit').dom.value = '';
	//Ext.get('comboAccountUnit').dom.readOnly=false;
	Ext.getCmp('comboAccountUnit').setReadOnly(false);
    selectAccountUnit = undefined;
    Ext.get('txtDeskripsiUnit').dom.value = '';
    //Ext.get('txtUnit').dom.value = '';
	//Ext.get('txtKode_Unit').dom.readOnly=false;
    //Ext.get('txtKode_Unit').dom.value = '';
	//Ext.get('txtUnit').dom.readOnly=false;
};
///---------------------------------------------------------------------------------------///

function getParamUnit() {
    var params =
	{
	    Table: 'viAkunUI',
	    Account: selectAccountUnit,//Ext.getCmp('txtUnit').getValue()
	    Int_Code: selectCboInterfaceSetInterface,//Ext.get('txtKode_Unit').getValue(),
	    Description: Ext.get('txtDeskripsiUnit').getValue(),
		 KD_UNIT_KERJA: Ext.getCmp('comboUnitKerja_aci').getValue()
	};
    return params
};


function RefreshDataUnit() {
    dsUnitList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: selectCountUnit,
			    Sort: 'Kd_Unit',
			    Sortdir: 'ASC',
			    target: 'viAkunUI',
			    param: ''
			}
		}
	);
    return dsUnitList;
};


function RefreshDataUnitFilter() {
    if (Ext.get('txtKDUnitFilter').getValue() != '') {
        var strUnit = Ext.get('txtKDUnitFilter').getValue();
        dsUnitList.load
		(
			{
			    params:
				{
				    Skip: 0,
				    Take: selectCountUnit,
				    Sort: '',
				    Sortdir: 'ASC',
				    target: 'viAkunUI',
				    param: strUnit
				}
			}
		);
    }
    else {
        RefreshDataUnit();
    }
};

function mComboMaksDataUnit() {
    var cboMaksDataUnit = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataUnit',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: 'Maks.Data ',
		    width: 60,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountUnit,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountUnit = b.data.displayText;
			        RefreshDataUnit();
			    }
			}
		}
	);
    return cboMaksDataUnit;
};