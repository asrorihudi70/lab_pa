// Data Source ExtJS

/**
*   Nama File       : setDokter.js
*   Menu            : Setup
*   Model id        : 030722
*   Keterangan      : Untuk Pengaturan Setting Data Master Dokter
*   Di buat tanggal : 05 Juni 2014
*   Oleh            : SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var mod_name_viDokter="viDokter";
//var DfltFilter_KD_CUSTOMER_viDokter = strKD_CUST;
var DfltFilter_KD_USER_viDokter = strKdUser;
var DfltFilterBtn_viDokter = 0;
var dataSourceGrid_viDokter;
var selectCount_viDokter=50;
var NamaForm_viDokter="Setup Dokter";
var icons_viDokter="Jabatan";
var addNew_viDokter;
var rowSelectedGridDataView_viDokter;
var setLookUps_viDokter;

CurrentPage.page = dataGrid_viDokter(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// Start Project Setup Dokter

// --------------------------------------- # Start Function # ---------------------------------------

/**
*   Function : ShowPesanWarning_viDokter
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanWarning_viDokter(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width :250
        }
    )
}
// End Function ShowPesanWarning_viDokter # --------------

/**
*   Function : ShowPesanError_viDokter
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanError_viDokter(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width :250
        }
    )
}
// End Function ShowPesanError_viDokter # --------------

/**
*   Function : ShowPesanInfo_viDokter
*   
*   Sebuah fungsi untuk menampilkan pesan saat data berhasil
*/

function ShowPesanInfo_viDokter(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width :250
        }
    )
}
// End Function ShowPesanInfo_viDokter # --------------

/**
*   Function : getCriteriaFilterGridDataView_viDokter
*   
*   Sebuah fungsi untuk memfilter data yang diambil dari Net.
*/
function getCriteriaFilterGridDataView_viDokter()
{
    var strKriteriaGridDataView_viDokter = "";

    strKriteriaGridDataView_viDokter = " WHERE KD_CUSTOMER ='" + strKD_CUST + "'  AND  KD_USER = '" + strKdUser + "' ";

    if(DfltFilterBtn_viDokter == 1)
    {

        if(Ext.getCmp('TxtFilterGridDataView_KD_DOKTER_viDokter').getValue() != "")
        {
            strKriteriaGridDataView_viDokter += " AND RIGHT(KD_DOKTER,2) LIKE '%" + Ext.getCmp('TxtFilterGridDataView_KD_DOKTER_viDokter').getValue() + "%' ";        
        }

        if(Ext.getCmp('TxtFilterGridDataView_NAMA_viDokter').getValue() != "")
        {
            strKriteriaGridDataView_viDokter += " AND NAMA LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NAMA_viDokter').getValue() + "%' ";
        }
        else
        {
            strKriteriaGridDataView_viDokter += " AND NAMA LIKE '%%' ";            
        }

    }

    strKriteriaGridDataView_viDokter += " ORDER BY KD_DOKTER ASC ";

    return strKriteriaGridDataView_viDokter;
}
// End Function getCriteriaFilterGridDataView_viDokter # --------------

/**
*   Function : DataRefresh_viDokter
*   
*   Sebuah fungsi untuk mengambil data dari Net.
*   Digunakan pada View Grid Pertama, Filter Grid 
*/

function DataRefresh_viDokter(criteria)
{
    
    dataSourceGrid_viDokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCount_viDokter,
                Sort: 'KD_DOKTER',
                Sortdir: 'ASC',
                target:'Viview_viDokter',
                param: criteria
                
            }
        }
    );    
    return dataSourceGrid_viDokter;
}
// End Function DataRefresh_viDokter # --------------

/**
*   Function : dataaddnew_viDokter
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function dataaddnew_viDokter(rowdata)
{
    addNew_viDokter = true;

    //-------------- # textfield # --------------
    Ext.getCmp('TxtWindowPopup_KD_DOKTER_viDokter').setValue('');
    Ext.getCmp('TxtWindowPopup_KD_DOKTER_VIEW_viDokter').setValue('');
    Ext.getCmp('TxtWindowPopup_NAMA_viDokter').setValue('');
}
// End Function dataaddnew_viDokter # --------------

/**
*   Function : datainit_viDokter
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function datainit_viDokter(rowdata)
{
    addNew_viDokter = false;

    //-------------- # textfield # --------------
    Ext.get('TxtWindowPopup_KD_DOKTER_viDokter').dom.value=rowdata.KD_DOKTER;
    Ext.get('TxtWindowPopup_KD_DOKTER_VIEW_viDokter').dom.value=rowdata.KD_DOKTER_VIEW;
    Ext.get('TxtWindowPopup_NAMA_viDokter').dom.value=rowdata.NAMA;
}
// End Function datainit_viDokter # --------------

/**
*   Function : dataparam_viDokter
*   
*   Sebuah fungsi untuk mengirim balik isian ke Net.
*   Digunakan saat save, edit, delete
*/

function dataparam_viDokter()
{
    var params_viDokter =
    {
        //-------------- # modelist Net. # --------------
        Table: 'Viview_viDokter',

        //-------------- # textfield # --------------
        KD_DOKTER: Ext.get('TxtWindowPopup_KD_DOKTER_viDokter').getValue(),
        KD_DOKTER_VIEW: Ext.get('TxtWindowPopup_KD_DOKTER_VIEW_viDokter').getValue(),
        NAMA: Ext.get('TxtWindowPopup_NAMA_viDokter').getValue(),
        KD_CUSTOMER: strKD_CUST,
        KD_USER: strKdUser,
    }

   return params_viDokter
}
// End Function dataparam_viDokter # --------------

/**
*   Function : datasave_viDokter)
*   
*   Sebuah fungsi untuk menyimpan dan mengedit data entry 
*/

function datasave_viDokter(mBol)
{
    if (Ext.get('TxtWindowPopup_NAMA_viDokter').getValue()!='Nama Dokter')
    {
        if ( addNew_viDokter == true)
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/CreateDataObj",
                    params: dataparam_viDokter(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            Ext.get('TxtWindowPopup_KD_DOKTER_viDokter').dom.value=cst.KD_DOKTER;
                            Ext.get('TxtWindowPopup_KD_DOKTER_VIEW_viDokter').dom.value=cst.KD_DOKTER_VIEW;
                            Ext.get('TxtWindowPopup_NAMA_viDokter').dom.value=cst.NAMA;
                            ShowPesanInfo_viDokter('Data berhasil disimpan','Simpan Data');
                            addNew_viDokter = false;
                            DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viDokter('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                        else
                        {
                            ShowPesanError_viDokter('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                    }
                }
            )
        }
        else
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/UpdateDataObj",
                    params: dataparam_viDokter(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            Ext.get('TxtWindowPopup_KD_DOKTER_viDokter').dom.value=cst.KD_DOKTER;
                            Ext.get('TxtWindowPopup_KD_DOKTER_VIEW_viDokter').dom.value=cst.KD_DOKTER_VIEW;
                            Ext.get('TxtWindowPopup_NAMA_viDokter').dom.value=cst.NAMA;
                            ShowPesanInfo_viDokter('Data berhasil disimpan','Edit Data');
                            DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viDokter('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                        else
                        {
                            ShowPesanError_viDokter('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                    }
                }
            )
        }

    }
    else
    {
        ShowPesanWarning_viDokter('Nama Dokter belum terisi','Simpan Data');
        if(mBol === true)
        {
            return false;
        }
    }
}
// End Function datasave_viDokter) # --------------

/**
*   Function : datadelete_viDokter
*   
*   Sebuah fungsi untuk menghapus data entry 
*/

function datadelete_viDokter()
{
    /*if (ValidasiEntry_viDokter('Hapus Data',true) == 1 )
    {*/
        Ext.Msg.show
        (
            {
                title:'Hapus Data',
                msg: "Akan menghapus data?" ,
                buttons: Ext.MessageBox.YESNO,
                width:300,
                fn: function (btn)
                {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                url: "./Datapool.mvc/DeleteDataObj",
                                params: dataparam_viDokter(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());
                                        setLookUps_viDokter.close(); 
                                        ShowPesanInfo_viDokter('Data berhasil dihapus','Hapus Data');
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarning_viDokter('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                    else
                                    {
                                        ShowPesanError_viDokter('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                }
                            }
                        )
                    }
                }
            }
        )
    //}
}
// End Function datadelete_viDokter # --------------

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*   Function : dataGrid_viDokter
*   
*   Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viDokter(mod_id)
{   
    // Field kiriman dari Project Net.
    var FieldMaster =
    [
        'KD_DOKTER', 'KD_DOKTER_VIEW', 'KD_CUSTOMER', 'KD_USER', 'NAMA'
    ];
    // Deklarasi penamaan yang akan digunakan pada Grid
    dataSourceGrid_viDokter = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
    DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());
    // Grid Setup Dokter
    var grData_viDokter = new Ext.grid.EditorGridPanel
    (
    {
        xtype: 'editorgrid',
        title: '',
        store: dataSourceGrid_viDokter,
        autoScroll: true,
        columnLines: true,
        border:false,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
        (
        {
            singleSelect: true,
            listeners:
            {
                rowselect: function(sm, row, rec)
                {
                    rowSelectedGridDataView_viDokter = undefined;
                    rowSelectedGridDataView_viDokter = dataSourceGrid_viDokter.getAt(row);
                }
            }
        }
        ),
        listeners:
        {
            // Function saat ada event double klik maka akan muncul form view
            rowdblclick: function (sm, ridx, cidx)
            {
                rowSelectedGridDataView_viDokter = dataSourceGrid_viDokter.getAt(ridx);
                if (rowSelectedGridDataView_viDokter != undefined)
                {
                    setLookUpGridDataView_viDokter(rowSelectedGridDataView_viDokter.data);
                }
                else
                {
                    setLookUpGridDataView_viDokter();
                }
            }
        },
        /**
        *   Mengatur tampilan pada Grid Setup Dokter
        *   Terdiri dari : Judul, Isi dan Event
        *   Isi pada Grid di dapat dari pemangilan dari Net.
        *   dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSourceGrid_viDokter
        *   didapat dari Function DataRefresh_viDokter()
        */
        colModel: new Ext.grid.ColumnModel
        (
            [
                new Ext.grid.RowNumberer(), 
                {
                    header: 'Kd. Dokter',
                    dataIndex: 'KD_DOKTER_VIEW',
                    id: 'ColGridDataView_KD_DOKTER_viDokter',
                    sortable: true,
                    width: 100,
                    filter:
                    {
                        type: 'int'
                    }
                },
                //-------------- ## --------------
                {
                    header: 'Nama Dokter',
                    dataIndex: 'NAMA',
                    id: 'ColGridDataView_NAMA_viDokter',
                    sortable: true,
                    width: 250,
                    filter:
                    {
                        type: 'int'
                    }
                }
                //-------------- ## --------------
            ]
            ),
        tbar: {
            xtype: 'toolbar',
            id: 'ToolbarGridDataView_viDokter',
            items: 
            [
                {
                    xtype: 'button',
                    text: 'Edit Data',
                    iconCls: 'Edit_Tr',
                    tooltip: 'Edit Data',
                    id: 'BtnEditGridDataView_viDokter',
                    handler: function(sm, row, rec)
                    {
                        if (rowSelectedGridDataView_viDokter != undefined)
                        {
                            setLookUpGridDataView_viDokter(rowSelectedGridDataView_viDokter.data)
                        }
                        else
                        {
                            setLookUpGridDataView_viDokter();
                        }
                    }
                }
                //-------------- ## --------------
            ]
        },
        // End Tolbar ke Dua # --------------
        // Button Bar Pagging # --------------
        // Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
        bbar : bbar_paging(mod_name_viDokter, selectCount_viDokter, dataSourceGrid_viDokter),
        // End Button Bar Pagging # --------------
        viewConfig: {
            forceFit: false
        }
    }
    )
    
    // Kriteria filter pada Grid
    var FrmTabs_viDokter = new Ext.Panel
    (
    {
        id: mod_id,
        title: NamaForm_viDokter,
        region: 'center',
        closable: true,
        border: false,
        layout: 'form',
        iconCls: icons_viDokter,
        margins: '0 5 5 0',
        items: [grData_viDokter],
        tbar:
        [ 
            { 
                xtype: 'tbtext', 
                text: 'Kd. Dokter', 
                cls: 'left-label', 
                width: 80 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_KD_DOKTER_viDokter',
                width: 200,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viDokter = 1;
                            DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbspacer',
                width: 15,
                height: 25
            },
            //-------------- ## --------------
            { 
                xtype: 'tbtext', 
                text: 'Nama Dokter', 
                cls: 'left-label', 
                width: 65 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_NAMA_viDokter',
                width: 200,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viDokter = 1;
                            DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbfill' 
            },
            //-------------- ## --------------
            {
                xtype: 'button',
                id: 'btnRefreshFilter_viDokter',
                iconCls: 'refresh',
                handler: function()
                {
                    DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());
                }
            }
            //-------------- ## --------------
        ]
        
    }
    )
    DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());
    return FrmTabs_viDokter;
}

/**
*   Function : setLookUpGridDataView_viDokter
*   
*   Sebuah fungsi untuk menampilkan Form Edit saat ada event klik Edit Data atau Alt+F3
*/
function setLookUpGridDataView_viDokter(rowdata)
{
    var lebar = 345; // Lebar Form saat PopUp
    setLookUps_viDokter = new Ext.Window
    (
    {
        id: 'SetLookUps_viDokter',
        title: NamaForm_viDokter,
        closeAction: 'destroy',
        autoScroll:true,
        width: 335,
        height: 145,
        resizable:false,
        border: false,
        plain: true,
        layout: 'fit',
        iconCls: icons_viDokter,
        modal: true,
        items: getFormItemEntry_viDokter(lebar,rowdata),
        listeners:
        {
            activate: function()
            {
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelectedGridDataView_viDokter=undefined;
                DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());
            }
        }
    }
    );

    setLookUps_viDokter.show();
    if (rowdata == undefined)
    {
        dataaddnew_viDokter();
    }
    else
    {
        datainit_viDokter(rowdata)
    }
}

/**
*   Function : getFormItemEntry_viDokter
*   
*   Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*   Di pangil dari Function setLookUpGridDataView_viDokter
*/
function getFormItemEntry_viDokter(lebar,rowdata)
{
    var pnlFormDataBasic_viDokter = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            fileUpload: true,
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            labelWidth: 100,
            // Tombol pada tollbar Edit Setup Dokter
            tbar: {
                xtype: 'toolbar',
                items: 
                [
                    {
                        xtype: 'button',
                        text: 'Add',
                        id: 'btnTambahWindowPopup_viDokter',
                        iconCls: 'add',
                        handler: function()
                        {
                            dataaddnew_viDokter();
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save',
                        id: 'btnSimpanWindowPopup_viDokter',
                        iconCls: 'save',
                        handler: function()
                        {
                            datasave_viDokter(false);
                            DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save & Close',
                        id: 'btnSimpanKeluarWindowPopup_viDokter',
                        iconCls: 'saveexit',
                        handler: function()
                        {
                            var x = datasave_viDokter(true);
                            DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());
                            if (x===undefined)
                            {
                                setLookUps_viDokter.close();
                            }
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Delete',
                        id: 'btnHapusWindowPopup_viDokter',
                        iconCls: 'remove',
                        handler: function()
                        {
                            datadelete_viDokter();
                            DataRefresh_viDokter(getCriteriaFilterGridDataView_viDokter());
                        }
                    }
                    //-------------- ## --------------
                ]
            },
            //-------------- #items# --------------
            items:
            [
                // Isian Pada Edit Setup Dokter
                {
                    xtype: 'fieldset',
                    title: '',
                    width: 305,
                    height: 70,
                    items: 
                    [                                   
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kd. Dokter Asli',
                            id: 'TxtWindowPopup_KD_DOKTER_viDokter',
                            name: 'TxtWindowPopup_KD_DOKTER_viDokter',
                            emptyText: 'Kode Dokter',
                            readOnly: true,
                            hidden: true,
                        },
                        //-------------- ## --------------  
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kd. Dokter',
                            id: 'TxtWindowPopup_KD_DOKTER_VIEW_viDokter',
                            name: 'TxtWindowPopup_KD_DOKTER_VIEW_viDokter',
                            emptyText: 'Kode Dokter',
                            readOnly: true,
                            width: 100, 
                            flex: 1,                
                        },
                        //-------------- ## --------------  
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nama Dokter',
                            id: 'TxtWindowPopup_NAMA_viDokter',
                            name: 'TxtWindowPopup_NAMA_viDokter',
                            emptyText: 'Nama Dokter',
                            width: 177, 
                            flex: 1,                
                        }
                        //-------------- ## -------------- 
                    ]       
                }
            ],
            //-------------- #items# --------------
        }
    )
    return pnlFormDataBasic_viDokter;
}

// --------------------------------------- # End Form # ---------------------------------------

// End Project Setup Dokter # --------------