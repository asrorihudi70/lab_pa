// Data Source ExtJS

/**
*   Nama File       : setKelompok.js
*   Menu            : Setup
*   Model id        : 030725
*   Keterangan      : Untuk Pengaturan Setting Data Master Kelompok
*   Di buat tanggal : 05 Juni 2014
*   Oleh            : SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var mod_name_viKelompok="viKelompok";
var DfltFilter_KD_CUSTOMER_viKelompok = strKD_CUST;
var DfltFilterBtn_viKelompok = 0;
var dataSourceGrid_viKelompok;
var selectCount_viKelompok=50;
var NamaForm_viKelompok="Setup Kelompok";
var icons_viKelompok="Jabatan";
var addNew_viKelompok;
var rowSelectedGridDataView_viKelompok;
var setLookUps_viKelompok;

CurrentPage.page = dataGrid_viKelompok(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// Start Project Setup Kelompok

// --------------------------------------- # Start Function # ---------------------------------------

/**
*   Function : ShowPesanWarning_viKelompok
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanWarning_viKelompok(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width :250
        }
    )
}
// End Function ShowPesanWarning_viKelompok # --------------

/**
*   Function : ShowPesanError_viKelompok
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanError_viKelompok(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width :250
        }
    )
}
// End Function ShowPesanError_viKelompok # --------------

/**
*   Function : ShowPesanInfo_viKelompok
*   
*   Sebuah fungsi untuk menampilkan pesan saat data berhasil
*/

function ShowPesanInfo_viKelompok(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width :250
        }
    )
}
// End Function ShowPesanInfo_viKelompok # --------------

/**
*   Function : getCriteriaFilterGridDataView_viKelompok
*   
*   Sebuah fungsi untuk memfilter data yang diambil dari Net.
*/
function getCriteriaFilterGridDataView_viKelompok()
{
    var strKriteriaGridDataView_viKelompok = "";

    strKriteriaGridDataView_viKelompok = " WHERE KD_CUSTOMER ='" + strKD_CUST + "' ";

    if(DfltFilterBtn_viKelompok == 1)
    {

        if(Ext.getCmp('TxtFilterGridDataView_KD_KELOMPOK_viKelompok').getValue() != "")
        {
            strKriteriaGridDataView_viKelompok += " AND RIGHT(KD_KELOMPOK,2) = '" + Ext.getCmp('TxtFilterGridDataView_KD_KELOMPOK_viKelompok').getValue() + "' ";        
        }

        if(Ext.getCmp('TxtFilterGridDataView_KELOMPOK_viKelompok').getValue() != "")
        {
            strKriteriaGridDataView_viKelompok += " AND KELOMPOK LIKE '%" + Ext.getCmp('TxtFilterGridDataView_KELOMPOK_viKelompok').getValue() + "%' ";
        }
        else
        {
            strKriteriaGridDataView_viKelompok += " AND KELOMPOK LIKE '%%' ";            
        }

    }

    strKriteriaGridDataView_viKelompok += " ORDER BY KD_KELOMPOK ASC ";

    return strKriteriaGridDataView_viKelompok;
}
// End Function getCriteriaFilterGridDataView_viKelompok # --------------

/**
*   Function : DataRefresh_viKelompok
*   
*   Sebuah fungsi untuk mengambil data dari Net.
*   Digunakan pada View Grid Pertama, Filter Grid 
*/

function DataRefresh_viKelompok(criteria)
{
    
    dataSourceGrid_viKelompok.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCount_viKelompok,
                Sort: 'KD_KELOMPOK',
                Sortdir: 'ASC',
                target:'Viview_viKelompok',
                param: criteria
                
            }
        }
    );    
    return dataSourceGrid_viKelompok;
}
// End Function DataRefresh_viKelompok # --------------

/**
*   Function : dataaddnew_viKelompok
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function dataaddnew_viKelompok(rowdata)
{
    addNew_viKelompok = true;

    //-------------- # textfield # --------------
    Ext.getCmp('TxtWindowPopup_KD_KELOMPOK_viKelompok').setValue('');
    Ext.getCmp('TxtWindowPopup_KD_KELOMPOK_VIEW_viKelompok').setValue('');
    Ext.getCmp('TxtWindowPopup_KELOMPOK_viKelompok').setValue('');
}
// End Function dataaddnew_viKelompok # --------------

/**
*   Function : datainit_viKelompok
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function datainit_viKelompok(rowdata)
{
    addNew_viKelompok = false;

    //-------------- # textfield # --------------
    Ext.get('TxtWindowPopup_KD_KELOMPOK_viKelompok').dom.value=rowdata.KD_KELOMPOK;
    Ext.get('TxtWindowPopup_KD_KELOMPOK_VIEW_viKelompok').dom.value=rowdata.KD_KELOMPOK_VIEW;
    Ext.get('TxtWindowPopup_KELOMPOK_viKelompok').dom.value=rowdata.KELOMPOK;
}
// End Function datainit_viKelompok # --------------

/**
*   Function : dataparam_viKelompok
*   
*   Sebuah fungsi untuk mengirim balik isian ke Net.
*   Digunakan saat save, edit, delete
*/

function dataparam_viKelompok()
{
    var params_viKelompok =
    {
        //-------------- # modelist Net. # --------------
        Table: 'Viview_viKelompok',

        //-------------- # textfield # --------------
        KD_KELOMPOK: Ext.get('TxtWindowPopup_KD_KELOMPOK_viKelompok').getValue(),
        KD_KELOMPOK_VIEW: Ext.get('TxtWindowPopup_KD_KELOMPOK_VIEW_viKelompok').getValue(),
        KELOMPOK: Ext.get('TxtWindowPopup_KELOMPOK_viKelompok').getValue(),
        KD_CUSTOMER: strKD_CUST,
        KD_TYPE: 1,
    }

   return params_viKelompok
}
// End Function dataparam_viKelompok # --------------

/**
*   Function : datasave_viKelompok)
*   
*   Sebuah fungsi untuk menyimpan dan mengedit data entry 
*/

function datasave_viKelompok(mBol)
{
    if (Ext.get('TxtWindowPopup_KELOMPOK_viKelompok').getValue()!='Nama Kelompok')
    {
        if ( addNew_viKelompok == true)
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/CreateDataObj",
                    params: dataparam_viKelompok(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            Ext.get('TxtWindowPopup_KD_KELOMPOK_viKelompok').dom.value=cst.KD_KELOMPOK;
                            Ext.get('TxtWindowPopup_KD_KELOMPOK_VIEW_viKelompok').dom.value=cst.KD_KELOMPOK_VIEW;
                            Ext.get('TxtWindowPopup_KELOMPOK_viKelompok').dom.value=cst.KELOMPOK;
                            ShowPesanInfo_viKelompok('Data berhasil disimpan','Simpan Data');
                            addNew_viKelompok = false;
                            DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viKelompok('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                        else
                        {
                            ShowPesanError_viKelompok('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                    }
                }
            )
        }
        else
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/UpdateDataObj",
                    params: dataparam_viKelompok(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            Ext.get('TxtWindowPopup_KD_KELOMPOK_viKelompok').dom.value=cst.KD_KELOMPOK;
                            Ext.get('TxtWindowPopup_KD_KELOMPOK_VIEW_viKelompok').dom.value=cst.KD_KELOMPOK_VIEW;
                            Ext.get('TxtWindowPopup_KELOMPOK_viKelompok').dom.value=cst.KELOMPOK;
                            ShowPesanInfo_viKelompok('Data berhasil disimpan','Edit Data');
                            DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viKelompok('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                        else
                        {
                            ShowPesanError_viKelompok('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                    }
                }
            )
        }

    }
    else
    {
        ShowPesanWarning_viKelompok('Nama Kelompok belum terisi','Simpan Data');
        if(mBol === true)
        {
            return false;
        }
    }
}
// End Function datasave_viKelompok) # --------------

/**
*   Function : datadelete_viKelompok
*   
*   Sebuah fungsi untuk menghapus data entry 
*/

function datadelete_viKelompok()
{
    /*if (ValidasiEntry_viKelompok('Hapus Data',true) == 1 )
    {*/
        Ext.Msg.show
        (
            {
                title:'Hapus Data',
                msg: "Akan menghapus data?" ,
                buttons: Ext.MessageBox.YESNO,
                width:300,
                fn: function (btn)
                {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                url: "./Datapool.mvc/DeleteDataObj",
                                params: dataparam_viKelompok(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());
                                        setLookUps_viKelompok.close(); 
                                        ShowPesanInfo_viKelompok('Data berhasil dihapus','Hapus Data');
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarning_viKelompok('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                    else
                                    {
                                        ShowPesanError_viKelompok('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                }
                            }
                        )
                    }
                }
            }
        )
    //}
}
// End Function datadelete_viKelompok # --------------

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*   Function : dataGrid_viKelompok
*   
*   Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viKelompok(mod_id)
{   
    // Field kiriman dari Project Net.
    var FieldMaster =
    [
        'KD_KELOMPOK', 'KD_KELOMPOK_VIEW', 'KD_CUSTOMER', 'KD_TYPE', 'KELOMPOK'
    ];
    // Deklarasi penamaan yang akan digunakan pada Grid
    dataSourceGrid_viKelompok = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
    DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());
    // Grid Setup Kelompok
    var grData_viKelompok = new Ext.grid.EditorGridPanel
    (
    {
        xtype: 'editorgrid',
        title: '',
        store: dataSourceGrid_viKelompok,
        autoScroll: true,
        columnLines: true,
        border:false,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
        (
        {
            singleSelect: true,
            listeners:
            {
                rowselect: function(sm, row, rec)
                {
                    rowSelectedGridDataView_viKelompok = undefined;
                    rowSelectedGridDataView_viKelompok = dataSourceGrid_viKelompok.getAt(row);
                }
            }
        }
        ),
        listeners:
        {
            // Function saat ada event double klik maka akan muncul form view
            rowdblclick: function (sm, ridx, cidx)
            {
                rowSelectedGridDataView_viKelompok = dataSourceGrid_viKelompok.getAt(ridx);
                if (rowSelectedGridDataView_viKelompok != undefined)
                {
                    setLookUpGridDataView_viKelompok(rowSelectedGridDataView_viKelompok.data);
                }
                else
                {
                    setLookUpGridDataView_viKelompok();
                }
            }
        },
        /**
        *   Mengatur tampilan pada Grid Setup Kelompok
        *   Terdiri dari : Judul, Isi dan Event
        *   Isi pada Grid di dapat dari pemangilan dari Net.
        *   dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSourceGrid_viKelompok
        *   didapat dari Function DataRefresh_viKelompok()
        */
        colModel: new Ext.grid.ColumnModel
        (
            [
                new Ext.grid.RowNumberer(), 
                {
                    header: 'Kd. Kelompok',
                    dataIndex: 'KD_KELOMPOK_VIEW',
                    id: 'ColGridDataView_KD_KELOMPOK_viKelompok',
                    sortable: true,
                    width: 100,
                    filter:
                    {
                        type: 'int'
                    }
                },
                //-------------- ## --------------
                {
                    header: 'Kelompok',
                    dataIndex: 'KELOMPOK',
                    id: 'ColGridDataView_KELOMPOK_viKelompok',
                    sortable: true,
                    width: 250,
                    filter:
                    {
                        type: 'int'
                    }
                }
                //-------------- ## --------------
            ]
            ),
        tbar: {
            xtype: 'toolbar',
            id: 'ToolbarGridDataView_viKelompok',
            items: 
            [
                {
                    xtype: 'button',
                    text: 'Edit Data',
                    iconCls: 'Edit_Tr',
                    tooltip: 'Edit Data',
                    id: 'BtnEditGridDataView_viKelompok',
                    handler: function(sm, row, rec)
                    {
                        if (rowSelectedGridDataView_viKelompok != undefined)
                        {
                            setLookUpGridDataView_viKelompok(rowSelectedGridDataView_viKelompok.data)
                        }
                        else
                        {
                            setLookUpGridDataView_viKelompok();
                        }
                    }
                }
                //-------------- ## --------------
            ]
        },
        // End Tolbar ke Dua # --------------
        // Button Bar Pagging # --------------
        // Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
        bbar : bbar_paging(mod_name_viKelompok, selectCount_viKelompok, dataSourceGrid_viKelompok),
        // End Button Bar Pagging # --------------
        viewConfig: {
            forceFit: false
        }
    }
    )
    
    // Kriteria filter pada Grid
    var FrmTabs_viKelompok = new Ext.Panel
    (
    {
        id: mod_id,
        title: NamaForm_viKelompok,
        region: 'center',
        closable: true,
        border: false,
        layout: 'form',
        iconCls: icons_viKelompok,
        margins: '0 5 5 0',
        items: [grData_viKelompok],
        tbar:
        [ 
            { 
                xtype: 'tbtext', 
                text: 'Kd. Kelompok', 
                cls: 'left-label', 
                width: 80 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_KD_KELOMPOK_viKelompok',
                width: 200,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viKelompok = 1;
                            DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbspacer',
                width: 15,
                height: 25
            },
            //-------------- ## --------------
            { 
                xtype: 'tbtext', 
                text: 'Kelompok', 
                cls: 'left-label', 
                width: 65 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_KELOMPOK_viKelompok',
                width: 200,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viKelompok = 1;
                            DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbfill' 
            },
            //-------------- ## --------------
            {
                xtype: 'button',
                id: 'btnRefreshFilter_viKelompok',
                iconCls: 'refresh',
                handler: function()
                {
                    DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());
                }
            }
            //-------------- ## --------------
        ]
        
    }
    )
    DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());
    return FrmTabs_viKelompok;
}

/**
*   Function : setLookUpGridDataView_viKelompok
*   
*   Sebuah fungsi untuk menampilkan Form Edit saat ada event klik Edit Data atau Alt+F3
*/
function setLookUpGridDataView_viKelompok(rowdata)
{
    var lebar = 345; // Lebar Form saat PopUp
    setLookUps_viKelompok = new Ext.Window
    (
    {
        id: 'SetLookUps_viKelompok',
        title: NamaForm_viKelompok,
        closeAction: 'destroy',
        autoScroll:true,
        width: 335,
        height: 145,
        resizable:false,
        border: false,
        plain: true,
        layout: 'fit',
        iconCls: icons_viKelompok,
        modal: true,
        items: getFormItemEntry_viKelompok(lebar,rowdata),
        listeners:
        {
            activate: function()
            {
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelectedGridDataView_viKelompok=undefined;
                DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());
            }
        }
    }
    );

    setLookUps_viKelompok.show();
    if (rowdata == undefined)
    {
        dataaddnew_viKelompok();
    }
    else
    {
        datainit_viKelompok(rowdata)
    }
}

/**
*   Function : getFormItemEntry_viKelompok
*   
*   Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*   Di pangil dari Function setLookUpGridDataView_viKelompok
*/
function getFormItemEntry_viKelompok(lebar,rowdata)
{
    var pnlFormDataBasic_viKelompok = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            fileUpload: true,
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            labelWidth: 100,
            // Tombol pada tollbar Edit Setup Kelompok
            tbar: {
                xtype: 'toolbar',
                items: 
                [
                    {
                        xtype: 'button',
                        text: 'Add',
                        id: 'btnTambahWindowPopup_viKelompok',
                        iconCls: 'add',
                        handler: function()
                        {
                            dataaddnew_viKelompok();
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save',
                        id: 'btnSimpanWindowPopup_viKelompok',
                        iconCls: 'save',
                        handler: function()
                        {
                            datasave_viKelompok(false);
                            DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save & Close',
                        id: 'btnSimpanKeluarWindowPopup_viKelompok',
                        iconCls: 'saveexit',
                        handler: function()
                        {
                            var x = datasave_viKelompok(true);
                            DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());
                            if (x===undefined)
                            {
                                setLookUps_viKelompok.close();
                            }
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Delete',
                        id: 'btnHapusWindowPopup_viKelompok',
                        iconCls: 'remove',
                        handler: function()
                        {
                            datadelete_viKelompok();
                            DataRefresh_viKelompok(getCriteriaFilterGridDataView_viKelompok());
                        }
                    }
                    //-------------- ## --------------
                ]
            },
            //-------------- #items# --------------
            items:
            [
                // Isian Pada Edit Setup Kelompok
                {
                    xtype: 'fieldset',
                    title: '',
                    width: 305,
                    height: 70,
                    items: 
                    [                                   
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kd. Kelompok Asli',
                            id: 'TxtWindowPopup_KD_KELOMPOK_viKelompok',
                            name: 'TxtWindowPopup_KD_KELOMPOK_viKelompok',
                            emptyText: 'Kode Kelompok',
                            readOnly: true,
                            hidden: true,
                        },
                        //-------------- ## --------------  
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kd. Kelompok',
                            id: 'TxtWindowPopup_KD_KELOMPOK_VIEW_viKelompok',
                            name: 'TxtWindowPopup_KD_KELOMPOK_VIEW_viKelompok',
                            emptyText: 'Kode Kelompok',
                            readOnly: true,
                            width: 100, 
                            flex: 1,                
                        },
                        //-------------- ## --------------  
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kelompok',
                            id: 'TxtWindowPopup_KELOMPOK_viKelompok',
                            name: 'TxtWindowPopup_KELOMPOK_viKelompok',
                            emptyText: 'Nama Kelompok',
                            width: 177, 
                            flex: 1,                
                        }
                        //-------------- ## -------------- 
                    ]       
                }
            ],
            //-------------- #items# --------------
        }
    )
    return pnlFormDataBasic_viKelompok;
}

// --------------------------------------- # End Form # ---------------------------------------

// End Project Setup Kelompok # --------------