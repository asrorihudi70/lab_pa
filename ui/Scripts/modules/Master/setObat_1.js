// Data Source ExtJS

/**
*   Nama File       : setObat.js
*   Menu            : Setup
*   Model id        : 030721
*   Keterangan      : Untuk Pengaturan Setting Data Master Obat
*   Di buat tanggal : 05 Juni 2014
*   Oleh            : SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var mod_name_viObat="viDokter";
var DfltFilter_KD_CUSTOMER_viObat = strKD_CUST;
var DfltFilter_KD_USER_viObat = strKdUser;
var DfltFilterBtn_viObat = 0;
var dataSourceGrid_viObat;
var selectCount_viObat=50;
var NAMA_OBATForm_viObat="Setup Obat";
var icons_viObat="Jabatan";
var addNew_viObat;
var rowSelectedGridDataView_viObat;
var setLookUps_viObat;

CurrentPage.page = dataGrid_viObat(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// Start Project Setup Obat

// --------------------------------------- # Start Function # ---------------------------------------

/**
*   Function : ShowPesanWarning_viObat
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanWarning_viObat(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width :250
        }
    )
}
// End Function ShowPesanWarning_viObat # --------------

/**
*   Function : ShowPesanError_viObat
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanError_viObat(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width :250
        }
    )
}
// End Function ShowPesanError_viObat # --------------

/**
*   Function : ShowPesanInfo_viObat
*   
*   Sebuah fungsi untuk menampilkan pesan saat data berhasil
*/

function ShowPesanInfo_viObat(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width :250
        }
    )
}
// End Function ShowPesanInfo_viObat # --------------

/**
*   Function : getCriteriaFilterGridDataView_viObat
*   
*   Sebuah fungsi untuk memfilter data yang diambil dari Net.
*/
function getCriteriaFilterGridDataView_viObat()
{
    var strKriteriaGridDataView_viObat = "";

    strKriteriaGridDataView_viObat = " WHERE KD_CUSTOMER ='" + strKD_CUST + "'  ";

    if(DfltFilterBtn_viObat == 1)
    {

        if(Ext.getCmp('TxtFilterGridDataView_KD_OBAT_viObat').getValue() != "")
        {
            strKriteriaGridDataView_viObat += " AND RIGHT(KD_OBAT,8) LIKE '%" + Ext.getCmp('TxtFilterGridDataView_KD_OBAT_viObat').getValue() + "%' ";        
        }

        if(Ext.getCmp('TxtFilterGridDataView_NAMA_OBAT_viObat').getValue() != "")
        {
            strKriteriaGridDataView_viObat += " AND NAMA_OBAT LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NAMA_OBAT_viObat').getValue() + "%' ";
        }
        else
        {
            strKriteriaGridDataView_viObat += " AND NAMA_OBAT LIKE '%%' ";            
        }

    }

    strKriteriaGridDataView_viObat += " ORDER BY KD_OBAT ASC ";

    return strKriteriaGridDataView_viObat;
}
// End Function getCriteriaFilterGridDataView_viObat # --------------

/**
*   Function : DataRefresh_viObat
*   
*   Sebuah fungsi untuk mengambil data dari Net.
*   Digunakan pada View Grid Pertama, Filter Grid 
*/

function DataRefresh_viObat(criteria)
{
    
    dataSourceGrid_viObat.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCount_viObat,
                Sort: 'KD_OBAT',
                Sortdir: 'ASC',
                target:'viObat',
                param: criteria
                
            }
        }
    );    
    return dataSourceGrid_viObat;
}
// End Function DataRefresh_viObat # --------------

/**
*   Function : viCombo_SATUAN_viObat
*   
*   Sebuah fungsi untuk menampilkan combobox Satuan
*/

function viRefresh_Satuan_viObat()
{
    ds_Satuan_viObat.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_SATUAN',
                Sortdir: 'ASC',
                target:'viSATUAN',
                param: ""
            }            
        }
    )
}

function viCombo_SATUAN_viObat(lebar,Nama_ID)
{
    var Field_satuan_viObat = ['KD_SATUAN','SATUAN'];
    ds_Satuan_viObat = new WebApp.DataStore({fields: Field_satuan_viObat});
    
    viRefresh_Satuan_viObat();
    var cbo_Satuan_viObat = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Satuan',
            valueField: 'KD_SATUAN',
            displayField: 'SATUAN',
            store: ds_Satuan_viObat,
            emptyText: 'Satuan',
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
        }
    )
    
    return cbo_Satuan_viObat;
}
// End Function viCombo_SATUAN_viObat # --------------

/**
*   Function : dataaddnew_viObat
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function dataaddnew_viObat(rowdata)
{
    addNew_viObat = true;

    //-------------- # textfield # --------------
    Ext.getCmp('TxtWindowPopup_KD_OBAT_viObat').setValue('');
    Ext.getCmp('TxtWindowPopup_KD_OBAT_VIEW_viObat').setValue('');
    Ext.getCmp('TxtWindowPopup_NAMA_OBAT_viObat').setValue('');
    Ext.getCmp('TxtWindowPopup_HARGA_BELI_viObat').setValue('');
    Ext.getCmp('TxtWindowPopup_HARGA_JUAL_viObat').setValue('');
    Ext.getCmp('TxtWindowPopup_MIN_STOK_viObat').setValue('');
    Ext.getCmp('TxtWindowPopup_STOK_viObat').setValue('');

     //-------------- # combobox # --------------
    Ext.getCmp('CmboWindowPopup_SATUAN_viObat').setValue('');
}
// End Function dataaddnew_viObat # --------------

/**
*   Function : datainit_viObat
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function datainit_viObat(rowdata)
{
    addNew_viObat = false;

    //-------------- # textfield # --------------
    Ext.get('TxtWindowPopup_KD_OBAT_viObat').dom.value=rowdata.KD_OBAT;
    Ext.get('TxtWindowPopup_KD_OBAT_VIEW_viObat').dom.value=rowdata.KD_OBAT_VIEW;
    Ext.get('TxtWindowPopup_NAMA_OBAT_viObat').dom.value=rowdata.NAMA_OBAT;
    /*Ext.get('TxtWindowPopup_HARGA_BELI_viObat').dom.value=rowdata.HARGA_BELI;
    Ext.get('TxtWindowPopup_HARGA_JUAL_viObat').dom.value=rowdata.HARGA_JUAL;*/
    Ext.getCmp('TxtWindowPopup_HARGA_BELI_viObat').setValue(formatCurrency(rowdata.HARGA_BELI));
    Ext.getCmp('TxtWindowPopup_HARGA_JUAL_viObat').setValue(formatCurrency(rowdata.HARGA_JUAL));
    Ext.get('TxtWindowPopup_MIN_STOK_viObat').dom.value=rowdata.MIN_STOK;
    Ext.get('TxtWindowPopup_STOK_viObat').dom.value=rowdata.STOK;

     //-------------- # combobox # --------------
    Ext.getCmp('CmboWindowPopup_SATUAN_viObat').setValue(rowdata.KD_SATUAN);
    Ext.get('CmboWindowPopup_SATUAN_viObat').dom.value=rowdata.SATUAN;
}
// End Function datainit_viObat # --------------

/**
*   Function : dataparam_viObat
*   
*   Sebuah fungsi untuk mengirim balik isian ke Net.
*   Digunakan saat save, edit, delete
*/

function dataparam_viObat()
{
    var params_viObat =
    {
        //-------------- # modelist Net. # --------------
        Table: 'viObat',

        //-------------- # textfield # --------------
        KD_OBAT: Ext.get('TxtWindowPopup_KD_OBAT_viObat').getValue(),
        KD_OBAT_VIEW: Ext.get('TxtWindowPopup_KD_OBAT_VIEW_viObat').getValue(),
        NAMA_OBAT: Ext.get('TxtWindowPopup_NAMA_OBAT_viObat').getValue(),
        HARGA_BELI: NonFormatCurrency(Ext.get('TxtWindowPopup_HARGA_BELI_viObat').getValue()),
        HARGA_JUAL: NonFormatCurrency(Ext.get('TxtWindowPopup_HARGA_JUAL_viObat').getValue()),
        MIN_STOK: Ext.get('TxtWindowPopup_MIN_STOK_viObat').getValue(),
        STOK: Ext.get('TxtWindowPopup_STOK_viObat').getValue(),
        KD_SATUAN: Ext.getCmp('CmboWindowPopup_SATUAN_viObat').getValue(),
        SATUAN: Ext.get('CmboWindowPopup_SATUAN_viObat').dom.value,
        KD_CUSTOMER: strKD_CUST,
    }

   return params_viObat
}
// End Function dataparam_viObat # --------------

/**
*   Function : datasave_viObat)
*   
*   Sebuah fungsi untuk menyimpan dan mengedit data entry 
*/

function datasave_viObat(mBol)
{
    if ((Ext.get('TxtWindowPopup_NAMA_OBAT_viObat').getValue()!='Nama Obat') && (Ext.get('CmboWindowPopup_SATUAN_viObat').dom.value!='Satuan'))
    {
        if ( addNew_viObat == true)
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/CreateDataObj",
                    params: dataparam_viObat(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());
                            
                            //-------------- # textfield # --------------
                            Ext.get('TxtWindowPopup_KD_OBAT_viObat').dom.value=cst.KD_OBAT;
                            Ext.get('TxtWindowPopup_KD_OBAT_VIEW_viObat').dom.value=cst.KD_OBAT_VIEW;
                            Ext.get('TxtWindowPopup_NAMA_OBAT_viObat').dom.value=cst.NAMA_OBAT;
                            Ext.get('TxtWindowPopup_HARGA_BELI_viObat').dom.value=cst.HARGA_BELI;
                            Ext.get('TxtWindowPopup_HARGA_JUAL_viObat').dom.value=cst.HARGA_JUAL;
                            Ext.get('TxtWindowPopup_MIN_STOK_viObat').dom.value=cst.MIN_STOK;
                            Ext.get('TxtWindowPopup_STOK_viObat').dom.value=cst.STOK;

                            //-------------- # combobox # --------------
                            Ext.getCmp('CmboWindowPopup_SATUAN_viObat').setValue(cst.KD_SATUAN);
                            Ext.get('CmboWindowPopup_SATUAN_viObat').dom.value=cst.SATUAN;

                            ShowPesanInfo_viObat('Data berhasil disimpan','Simpan Data');
                            addNew_viObat = false;
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viObat('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                        else
                        {
                            ShowPesanError_viObat('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                    }
                }
            )
        }
        else
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/UpdateDataObj",
                    params: dataparam_viObat(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());

                            //-------------- # textfield # --------------
                            Ext.get('TxtWindowPopup_KD_OBAT_viObat').dom.value=cst.KD_OBAT;
                            Ext.get('TxtWindowPopup_KD_OBAT_VIEW_viObat').dom.value=cst.KD_OBAT_VIEW;
                            Ext.get('TxtWindowPopup_NAMA_OBAT_viObat').dom.value=cst.NAMA_OBAT;
                            Ext.get('TxtWindowPopup_HARGA_BELI_viObat').dom.value=cst.HARGA_BELI;
                            Ext.get('TxtWindowPopup_HARGA_JUAL_viObat').dom.value=cst.HARGA_JUAL;
                            Ext.get('TxtWindowPopup_MIN_STOK_viObat').dom.value=cst.MIN_STOK;
                            Ext.get('TxtWindowPopup_STOK_viObat').dom.value=cst.STOK;

                            //-------------- # combobox # --------------
                            Ext.getCmp('CmboWindowPopup_SATUAN_viObat').setValue(cst.KD_SATUAN);
                            Ext.get('CmboWindowPopup_SATUAN_viObat').dom.value=cst.SATUAN;

                            ShowPesanInfo_viObat('Data berhasil disimpan','Edit Data');
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viObat('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                        else
                        {
                            ShowPesanError_viObat('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                    }
                }
            )
        }

    }
    else
    {
        ShowPesanWarning_viObat('Nama Obat dan Satuan belum terisi','Simpan Data');
        if(mBol === true)
        {
            return false;
        }
    }
}
// End Function datasave_viObat) # --------------

/**
*   Function : datadelete_viObat
*   
*   Sebuah fungsi untuk menghapus data entry 
*/

function datadelete_viObat()
{
    /*if (ValidasiEntry_viObat('Hapus Data',true) == 1 )
    {*/
        Ext.Msg.show
        (
            {
                title:'Hapus Data',
                msg: "Akan menghapus data?" ,
                buttons: Ext.MessageBox.YESNO,
                width:300,
                fn: function (btn)
                {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                url: "./Datapool.mvc/DeleteDataObj",
                                params: dataparam_viObat(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());
                                        setLookUps_viObat.close(); 
                                        ShowPesanInfo_viObat('Data berhasil dihapus','Hapus Data');
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarning_viObat('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                    else
                                    {
                                        ShowPesanError_viObat('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                }
                            }
                        )
                    }
                }
            }
        )
    //}
}
// End Function datadelete_viObat # --------------

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*   Function : dataGrid_viObat
*   
*   Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viObat(mod_id)
{   
    // Field kiriman dari Project Net.
    var FieldMaster =
    [
        'KD_OBAT', 'KD_OBAT_VIEW', 'KD_CUSTOMER', 'NAMA_OBAT', 'MIN_STOK', 'STOK', 'HARGA_BELI', 'HARGA_JUAL', 'KD_SATUAN', 'SATUAN'
    ];
    // Deklarasi peNAMA_OBATan yang akan digunakan pada Grid
    dataSourceGrid_viObat = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
    DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());
    // Grid Setup Obat
    var grData_viObat = new Ext.grid.EditorGridPanel
    (
    {
        xtype: 'editorgrid',
        title: '',
        store: dataSourceGrid_viObat,
        autoScroll: true,
        columnLines: true,
        border:false,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
        (
        {
            singleSelect: true,
            listeners:
            {
                rowselect: function(sm, row, rec)
                {
                    rowSelectedGridDataView_viObat = undefined;
                    rowSelectedGridDataView_viObat = dataSourceGrid_viObat.getAt(row);
                }
            }
        }
        ),
        listeners:
        {
            // Function saat ada event double klik maka akan muncul form view
            rowdblclick: function (sm, ridx, cidx)
            {
                rowSelectedGridDataView_viObat = dataSourceGrid_viObat.getAt(ridx);
                if (rowSelectedGridDataView_viObat != undefined)
                {
                    setLookUpGridDataView_viObat(rowSelectedGridDataView_viObat.data);
                }
                else
                {
                    setLookUpGridDataView_viObat();
                }
            }
        },
        /**
        *   Mengatur tampilan pada Grid Setup Obat
        *   Terdiri dari : Judul, Isi dan Event
        *   Isi pada Grid di dapat dari pemangilan dari Net.
        *   dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSourceGrid_viObat
        *   didapat dari Function DataRefresh_viObat()
        */
        colModel: new Ext.grid.ColumnModel
        (
            [
                new Ext.grid.RowNumberer(), 
                {
                    header: 'Kd. Obat',
                    dataIndex: 'KD_OBAT_VIEW',
                    id: 'ColGridDataView_KD_OBAT_viObat',
                    sortable: true,
                    width: 100,
                    filter:
                    {
                        type: 'int'
                    }
                },
                //-------------- ## --------------
                {
                    header: 'Nama Obat',
                    dataIndex: 'NAMA_OBAT',
                    id: 'ColGridDataView_NAMA_OBAT_viObat',
                    sortable: true,
                    width: 250,
                    filter:
                    {
                        type: 'int'
                    }
                },
                //-------------- ## --------------
                {
                    header: 'Satuan',
                    dataIndex: 'SATUAN',
                    id: 'ColGridDataView_SATUAN_viObat',
                    sortable: true,
                    width: 100,
                    filter:
                    {
                        type: 'int'
                    }
                },
                //-------------- ## --------------
                {
                    header: 'Min. Stok',
                    dataIndex: 'MIN_STOK',
                    id: 'ColGridDataView_MIN_STOK_viObat',
                    sortable: true,
                    width: 100,
                    filter:
                    {
                        type: 'int'
                    },
                    align:'center',
                    renderer: function(v, params, record) 
                    {
                        return formatCurrency(record.data.MIN_STOK);
                    },
                },
                //-------------- ## --------------
                {
                    header: 'Stok',
                    dataIndex: 'STOK',
                    id: 'ColGridDataView_STOK_viObat',
                    sortable: true,
                    width: 100,
                    filter:
                    {
                        type: 'int'
                    },
                    align:'center',
                    renderer: function(v, params, record) 
                    {
                        return formatCurrency(record.data.STOK);
                    },
                },
                //-------------- ## --------------
                {
                    header: 'Harga',
                    dataIndex: 'HARGA_JUAL',
                    id: 'ColGridDataView_HARGA_JUAL_viObat',
                    sortable: true,
                    width: 100,
                    filter:
                    {
                        type: 'int'
                    },
                    align:'right',
                    renderer: function(v, params, record) 
                    {
                        return formatCurrency(record.data.HARGA_JUAL);
                    },
                }
                //-------------- ## --------------
            ]
            ),
        tbar: {
            xtype: 'toolbar',
            id: 'ToolbarGridDataView_viObat',
            items: 
            [
                {
                    xtype: 'button',
                    text: 'Edit Data',
                    iconCls: 'Edit_Tr',
                    tooltip: 'Edit Data',
                    id: 'BtnEditGridDataView_viObat',
                    handler: function(sm, row, rec)
                    {
                        if (rowSelectedGridDataView_viObat != undefined)
                        {
                            setLookUpGridDataView_viObat(rowSelectedGridDataView_viObat.data)
                        }
                        else
                        {
                            setLookUpGridDataView_viObat();
                        }
                    }
                }
                //-------------- ## --------------
            ]
        },
        // End Tolbar ke Dua # --------------
        // Button Bar Pagging # --------------
        // Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
        bbar : bbar_paging(mod_name_viObat, selectCount_viObat, dataSourceGrid_viObat),
        // End Button Bar Pagging # --------------
        viewConfig: {
            forceFit: false
        }
    }
    )
    
    // Kriteria filter pada Grid
    var FrmTabs_viObat = new Ext.Panel
    (
    {
        id: mod_id,
        title: NAMA_OBATForm_viObat,
        region: 'center',
        closable: true,
        border: false,
        layout: 'form',
        iconCls: icons_viObat,
        margins: '0 5 5 0',
        items: [grData_viObat],
        tbar:
        [ 
            { 
                xtype: 'tbtext', 
                text: 'Kd. Obat', 
                cls: 'left-label', 
                width: 80 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_KD_OBAT_viObat',
                width: 200,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viObat = 1;
                            DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbspacer',
                width: 15,
                height: 25
            },
            //-------------- ## --------------
            { 
                xtype: 'tbtext', 
                text: 'Nama Obat', 
                cls: 'left-label', 
                width: 65 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_NAMA_OBAT_viObat',
                width: 200,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viObat = 1;
                            DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbfill' 
            },
            //-------------- ## --------------
            {
                xtype: 'button',
                id: 'btnRefreshFilter_viObat',
                iconCls: 'refresh',
                handler: function()
                {
                    DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());
                }
            }
            //-------------- ## --------------
        ]
        
    }
    )
    DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());
    return FrmTabs_viObat;
}

/**
*   Function : setLookUpGridDataView_viObat
*   
*   Sebuah fungsi untuk menampilkan Form Edit saat ada event klik Edit Data atau Alt+F3
*/
function setLookUpGridDataView_viObat(rowdata)
{
    var lebar = 819; // Lebar Form saat PopUp
    setLookUps_viObat = new Ext.Window
    (
    {
        id: 'SetLookUps_viObat',
        title: NAMA_OBATForm_viObat,
        closeAction: 'destroy',
        autoScroll:true,
        width: 730,
        height: 200,
        resizable:false,
        border: false,
        plain: true,
        layout: 'fit',
        iconCls: icons_viObat,
        modal: true,
        items: getFormItemEntry_viObat(lebar,rowdata),
        listeners:
        {
            activate: function()
            {
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelectedGridDataView_viObat=undefined;
                DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());
            }
        }
    }
    );

    setLookUps_viObat.show();
    if (rowdata == undefined)
    {
        dataaddnew_viObat();
    }
    else
    {
        datainit_viObat(rowdata)
    }
}

/**
*   Function : getFormItemEntry_viObat
*   
*   Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*   Di pangil dari Function setLookUpGridDataView_viObat
*/
function getFormItemEntry_viObat(lebar,rowdata)
{
    var pnlFormDataBasic_viObat = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            fileUpload: true,
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            labelWidth: 100,
            // Tombol pada tollbar Edit Setup Obat
            tbar: {
                xtype: 'toolbar',
                items: 
                [
                    {
                        xtype: 'button',
                        text: 'Add',
                        id: 'btnTambahWindowPopup_viObat',
                        iconCls: 'add',
                        handler: function()
                        {
                            dataaddnew_viObat();
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save',
                        id: 'btnSimpanWindowPopup_viObat',
                        iconCls: 'save',
                        handler: function()
                        {
                            datasave_viObat(false);
                            DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save & Close',
                        id: 'btnSimpanKeluarWindowPopup_viObat',
                        iconCls: 'saveexit',
                        handler: function()
                        {
                            var x = datasave_viObat(true);
                            DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());
                            if (x===undefined)
                            {
                                setLookUps_viObat.close();
                            }
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Delete',
                        id: 'btnHapusWindowPopup_viObat',
                        iconCls: 'remove',
                        handler: function()
                        {
                            datadelete_viObat();
                            DataRefresh_viObat(getCriteriaFilterGridDataView_viObat());
                        }
                    }
                    //-------------- ## --------------
                ]
            },
            //-------------- #items# --------------
            items:
            [
                // Isian Pada Edit Setup Obat
                {
                    xtype: 'fieldset',
                    title: '',
                    width: 700,
                    height: 125,
                    items: 
                    [                                   
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kd. Obat Asli',
                            id: 'TxtWindowPopup_KD_OBAT_viObat',
                            name: 'TxtWindowPopup_KD_OBAT_viObat',
                            emptyText: 'Kode Obat',
                            readOnly: true,
                            hidden: true,
                            width: 200, 
                            flex: 1,                
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kd. Obat',
                            id: 'TxtWindowPopup_KD_OBAT_VIEW_viObat',
                            name: 'TxtWindowPopup_KD_OBAT_VIEW_viObat',
                            emptyText: 'Kode Obat',
                            readOnly: true,
                            width: 200, 
                            flex: 1,                
                        },
                        //-------------- ## --------------  
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nama Obat',
                            id: 'TxtWindowPopup_NAMA_OBAT_viObat',
                            name: 'TxtWindowPopup_NAMA_OBAT_viObat',
                            emptyText: 'Nama Obat',
                            width: 569, 
                            flex: 1,                
                        },
                        //-------------- ## --------------    
                        {
                            xtype: 'compositefield',
                            fieldLabel: 'Satuan',
                            anchor: '100%',
                            width: 250,
                            items: 
                            [
                                viCombo_SATUAN_viObat(150,"CmboWindowPopup_SATUAN_viObat"),
                                //-------------- ## --------------
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'Label',
                                    value: 'Harga Beli:',
                                    id: '',
                                    name: '',
                                    flex: 1,
                                    width: 80,
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Harga Beli',
                                    id: 'TxtWindowPopup_HARGA_BELI_viObat',
                                    name: 'TxtWindowPopup_HARGA_BELI_viObat',
                                    emptyText: '0',
                                    style:{'text-align':'right'},
                                    width: 120,
                                },
                                //-------------- ## --------------  
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'Label',
                                    value: 'Harga Jual:',
                                    id: '',
                                    name: '',
                                    flex: 1,
                                    width: 80,
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Harga Jual',
                                    id: 'TxtWindowPopup_HARGA_JUAL_viObat',
                                    name: 'TxtWindowPopup_HARGA_JUAL_viObat',
                                    emptyText: '0',
                                    style:{'text-align':'right'},
                                    width: 120,
                                }
                                //-------------- ## --------------              
                            ]
                        },
                        //-------------- ## --------------                  
                        {
                            xtype: 'compositefield',
                            fieldLabel: 'Min. Stok',
                            anchor: '100%',
                            width: 250,
                            items: 
                            [
                                {
                                xtype: 'textfield',
                                fieldLabel: 'Label',
                                id: 'TxtWindowPopup_MIN_STOK_viObat',
                                name: 'TxtWindowPopup_MIN_STOK_viObat',
                                emptyText: '0',
                                flex: 1,
                                width: 150,
                                },
                                //-------------- ## --------------
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'Label',
                                    value: 'Stok:',
                                    id: '',
                                    name: '',
                                    flex: 1,
                                    width: 80,
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Stok',
                                    id: 'TxtWindowPopup_STOK_viObat',
                                    name: 'TxtWindowPopup_STOK_viObat',
                                    emptyText: '0',
                                    readOnly: true,
                                    width: 120,
                                }
                                //-------------- ## --------------      
                            ]
                        }
                        //-------------- ## -------------- 
                    ]       
                }
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormDataBasic_viObat;
}

// --------------------------------------- # End Form # ---------------------------------------

// End Project Setup Obat # --------------