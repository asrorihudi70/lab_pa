// Data Source ExtJS

/**
*   Nama File       : setPekerjaan.js
*   Menu            : Setup
*   Model id        : 030726
*   Keterangan      : Untuk Pengaturan Setting Data Master Unit
*   Di buat tanggal : 9 Juni 2014
*   Oleh            : Eka - CAIN
*/

// Deklarasi Variabel pada ExtJS
var dsDataUnsur_viPekerjaan;
var dataSource_viPekerjaan;
var selectCount_viPekerjaan=50;
var NamaForm_viPekerjaan="Setup Pekerjaan";
var icons_viPekerjaan="Jabatan";
var addNew_viPekerjaan;
var rowSelected_viPekerjaan;
var rowSelected_viPekerjaanList;
var setLookUps_viPekerjaan;
var setLookUps_viPekerjaanList;
var now_viPekerjaan = new Date();

var BlnIsDetail;

var CurrentData_viPekerjaan =
{
    data: Object,
    details: Array,
    row: 0
};

CurrentPage.page = dataGrid_viPekerjaan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


// Start Project Setup Unit

/**
*   Function : dataGrid_viPekerjaan
*   
*   Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/
function dataGrid_viPekerjaan(mod_id)
{   
    // Field kiriman dari Project Net.
    var FieldMaster =
    [
        'KD_PEKERJAAN', 'KD_CUSTOMER', 'PEKERJAAN'
    ];
    // Deklarasi penamaan yang akan digunakan pada Grid
    dataSource_viPekerjaan = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
    datarefresh_viPekerjaan();
    // Grid Setup Unit
    var grData_viPekerjaan = new Ext.grid.EditorGridPanel
    (
    {
        xtype: 'editorgrid',
        title: '',
        store: dataSource_viPekerjaan,
        autoScroll: true,
        columnLines: true,
        border:false,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
        (
        {
            singleSelect: true,
            listeners:
            {
                rowselect: function(sm, row, rec)
                {
                    rowSelected_viPekerjaan = undefined;
                    rowSelected_viPekerjaan = dataSource_viPekerjaan.getAt(row);
                }
            }
        }
        ),
        listeners:
        {
            // Function saat ada event double klik maka akan muncul form view
            rowdblclick: function (sm, ridx, cidx)
            {
                rowSelected_viPekerjaan = dataSource_viPekerjaan.getAt(ridx);
                if (rowSelected_viPekerjaan != undefined)
                {
                    setLookUp_viPekerjaan(rowSelected_viPekerjaan.data);
                }
                else
                {
                    setLookUp_viPekerjaan();
                }
            }
        },
        /**
        *   Mengatur tampilan pada Grid Setup Unit
        *   Terdiri dari : Judul, Isi dan Event
        *   Isi pada Grid di dapat dari pemangilan dari Net.
        *   dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSource_viPekerjaan
        *   didapat dari Function datarefresh_viPekerjaan()
        */
        colModel: new Ext.grid.ColumnModel
        (
            [
                new Ext.grid.RowNumberer(), 
                {
                    id: 'colKdPekerjaan_viPekerjaan',
                    header: 'Kd. Pekerjaan',
                    dataIndex: 'KD_PEKERJAAN',
                    sortable: true,
                    width: 250,
                    filter:
                    {
                        type: 'int'
                    }
                },
                //-------------- ## --------------
                {
                    id: 'colNama_viPekerjaan',
                    header: 'Pekerjaan',
                    dataIndex: 'PEKERJAAN',
                    sortable: true,
                    width: 250,
                    filter:
                    {
                        type: 'int'
                    }
                }
                //-------------- ## --------------
            ]
            ),
        tbar: {
            xtype: 'toolbar',
            id: 'toolbar_viPekerjaan',
            items: 
            [
                {
                    xtype: 'button',
                    text: 'Edit Data',
                    iconCls: 'Edit_Tr',
                    tooltip: 'Edit Data',
                    id: 'btnEdit_viPekerjaan',
                    handler: function(sm, row, rec)
                    {
                        if (rowSelected_viPekerjaan != undefined)
                        {
                            setLookUp_viPekerjaan(rowSelected_viPekerjaan.data)
                        }
                        else
                        {
                            setLookUp_viPekerjaan();
                        }
                    }
                }
                //-------------- ## --------------
            ]
        },
        bbar:new WebApp.PaggingBar
            (
                {
                    displayInfo: true,
                    store: dataSource_viPekerjaan,
                    pageSize: selectCount_viPekerjaan,
                    displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
                    emptyMsg: "Tidak ada record&nbsp;&nbsp;"        
                }
            ),
        viewConfig: {
            forceFit: false
        }
    }
    )
    
    // Kriteria filter pada Grid
    var FrmTabs_viPekerjaan = new Ext.Panel
    (
    {
        id: mod_id,
        title: NamaForm_viPekerjaan,
        region: 'center',
        closable: true,
        border: false,
        layout: 'form',
        iconCls: icons_viPekerjaan,
        margins: '0 5 5 0',
        items: [grData_viPekerjaan],
        // tbar:
        // [ 
            // { 
                // xtype: 'tbtext', 
                // text: 'Kd. Unit : ', 
                // cls: 'left-label', 
                // width: 65 
            // },
            // {
                // xtype: 'textfield',
                // id: 'txtfilterkdpoli_viPekerjaan',
                // width: 200,
                // listeners:
                // { 
                    // 'specialkey' : function()
                    // {
                        // if (Ext.EventObject.getKey() === 13) 
                        // {
                            // DataRefreshFilter_viPekerjaan();                               
                        // }                       
                    // }
                // }
            // },
            //-------------- ## --------------
            // { 
                // xtype: 'tbtext', 
                // text: 'Unit : ', 
                // cls: 'left-label', 
                // width: 65 
            // },
            // {
                // xtype: 'textfield',
                // id: 'txtfilterpoli_viPekerjaan',
                // width: 200,
                // listeners:
                // { 
                    // 'specialkey' : function()
                    // {
                        // if (Ext.EventObject.getKey() === 13) 
                        // {
                            // DataRefreshFilter_viPekerjaan();                               
                        // }                       
                    // }
                // }
            // },
            //-------------- ## --------------
            // { 
                // xtype: 'tbfill' 
            // },
            //-------------- ## --------------
            // {
                // xtype: 'button',
                // id: 'btnRefreshFilter_viPekerjaan',
                // iconCls: 'refresh',
                // handler: function()
                // {
                    // DataRefreshFilter_viPekerjaan();
                // }
            // }
            //-------------- ## --------------
        // ]
        
    }
    )
    datarefresh_viPekerjaan();
    return FrmTabs_viPekerjaan;
}

/**
*   Function : setLookUp_viPekerjaan
*   
*   Sebuah fungsi untuk menampilkan Form Edit saat ada event klik Edit Data
*/
function setLookUp_viPekerjaan(rowdata)
{
    var lebar = 819; // Lebar Form saat PopUp
    setLookUps_viPekerjaan = new Ext.Window
    (
    {
        id: 'SetLookUps_viPekerjaan',
        title: NamaForm_viPekerjaan,
        closeAction: 'destroy',
        autoScroll:true,
        width: 530,
        height: 145,
        resizable:false,
        border: false,
        plain: true,
        layout: 'fit',
        iconCls: icons_viPekerjaan,
        modal: true,
        items: getFormItemEntry_viPekerjaan(lebar,rowdata),
        listeners:
        {
            activate: function()
            {
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viPekerjaan=undefined;
                datarefresh_viPekerjaan();
            }
        }
    }
    );

    setLookUps_viPekerjaan.show();
    if (rowdata == undefined)
    {
        dataaddnew_viPekerjaan();
    }
    else
    {
        datainit_viPekerjaan(rowdata)
    }
}

/**
*   Function : getFormItemEntry_viPekerjaan
*   
*   Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*   Di pangil dari Function setLookUp_viPekerjaan
*/
function getFormItemEntry_viPekerjaan(lebar,rowdata)
{
    var pnlFormDataBasic_viPekerjaan = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            fileUpload: true,
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            // Tombol pada tollbar Edit Setup Unit
            tbar: {
                xtype: 'toolbar',
                items: 
                [
                    {
                        xtype: 'button',
                        text: 'Add',
                        id: 'btnAdd_viPekerjaan',
                        iconCls: 'add',
                        handler: function()
                        {
                            dataaddnew_viPekerjaan();
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save',
                        id: 'btnSimpan_viPekerjaan',
                        iconCls: 'save',
                        handler: function()
                        {
                            datasave_viPekerjaan(false);
                            datarefresh_viPekerjaan();
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save & Close',
                        id: 'btnSimpanExit_viPekerjaan',
                        iconCls: 'saveexit',
                        handler: function()
                        {
                            var x = datasave_viPekerjaan(true);
                            datarefresh_viPekerjaan();
                            if (x===undefined)
                            {
                                setLookUps_viPekerjaan.close();
                            }
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Delete',
                        id: 'btnHapus_viPekerjaan',
                        iconCls: 'remove',
                        handler: function()
                        {
                            datadelete_viPekerjaan();
                            BlnIsDetail=false;
                            datarefresh_viPekerjaan();
                        }
                    }
                    //-------------- ## --------------
                ]
            },
            //-------------- #items# --------------
            items:
            [
                // Isian Pada Edit Setup Unit
                {
                    xtype: 'fieldset',
                    title: '',
                    width: 500,
                    height: 70,
                    items: 
                    [                                   
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kd. Pekerjaan',
                            id: 'txtKdPekerjaan_viPekerjaan',
                            name: 'txtKdPekerjaan_viPekerjaan',
                            width: 100,
                            readOnly: true, 
                            flex: 1,                
                        },
                        //-------------- ## --------------  
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Pekerjaan',
                            id: 'txtNmPekerjaan_viPekerjaan',
                            name: 'txtNmPekerjaan_viPekerjaan',
                            width: 200, 
                            flex: 1,                
                        }
                        //-------------- ## -------------- 
                    ]       
                }
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormDataBasic_viPekerjaan;
}
//------------------------------------------------------------------------------------
///----------------------------------------------------------------------------------------------///
function datasave_viPekerjaan(mBol)
{
    if (ValidasiEntry_viPekerjaan('Simpan Data',false) == 1 )
    {
        if (addNew_viPekerjaan == true)
        {
            Ext.Ajax.request
            (
            {
                url: "./Datapool.mvc/CreateDataObj",
                params: dataparam_viPekerjaan(),
                success: function(o)
                {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                        ShowPesanInfo_viPekerjaan('Data berhasil di simpan','Simpan Data');
                        datarefresh_viPekerjaan();
                        addNew_viPekerjaan = false;
                    }
                    else
                    {
                        ShowPesanError_viPekerjaan('Data tidak berhasil di simpan ' ,'Simpan Data');
                    }
                }
            }
            )
        }
        else
        {
            Ext.Ajax.request
            (
            {
                url: "./Datapool.mvc/UpdateDataObj",
                params: dataparam_viPekerjaan(),
                success: function(o)
                {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                        ShowPesanInfo_viPekerjaan('Data berhasil disimpan','Edit Data');
                        datarefresh_viPekerjaan();
                    }
                    else
                    {
                        ShowPesanError_viPekerjaan('Data tidak berhasil disimpan ' ,'Edit Data');
                    }
                }
            }
            )
        }
    }
    else
    {
        if(mBol === true)
        {
            return false;
        }
    }
    
}

function datadelete_viPekerjaan()
{
    if (ValidasiEntry_viPekerjaan('Hapus Data',true) == 1 )
    {
        Ext.Msg.show
        (
        {
            title:'Hapus Data',
             msg: "Akan menghapus data?" ,
            buttons: Ext.MessageBox.YESNO,
            width:300,
            fn: function (btn)
            {
                if (btn =='yes')
                {
                    Ext.Ajax.request
                    (
                    {
                        url: "./Datapool.mvc/DeleteDataObj",
                        params: dataparam_viPekerjaan(),
                        success: function(o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfo_viPekerjaan('Data berhasil dihapus','Hapus Data');
                                datarefresh_viPekerjaan();
                                dataaddnew_viPekerjaan();
                            }
                            else
                            {
                                ShowPesanError_viPekerjaan('Data tidak berhasil dihapus '  ,'Hapus Data');
                            }
                        }
                    }
                    )//end Ajax.request
                } // end if btn yes
            }// end fn
        }
        )//end msg.show
    }
}
///-----------------------------------------------------------------------------------------------------------------///

function ValidasiEntry_viPekerjaan(modul,mBolHapus)
{
    var x = 1;
	
	if (modul == 'Simpan Data') 
	{
		if (Ext.get('txtNmPekerjaan_viPekerjaan').getValue() == '' )
		{ 
			x=0;
		}
	} else if (modul == 'Hapus Data')
	{
		
	}
	
    

    return x;
}


function ShowPesanWarning_viPekerjaan(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width :250
        }
    )
}

function ShowPesanError_viPekerjaan(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width :250
        }
    )
}

function ShowPesanInfo_viPekerjaan(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width :250
        }
    )
}

/**
*   Function : getCriteriaFilterGridDataView_viPekerjaanSetup
*   
*   Sebuah fungsi untuk memfilter data yang diambil dari Net.
*/
function getCriteriaFilterGridDataView_viPekerjaanSetup()
{
    var strKriteriaGridDataView_viPekerjaanSetup = "";

    strKriteriaGridDataView_viPekerjaanSetup = " WHERE KD_CUSTOMER ='" + strKD_CUST + "' ";

    strKriteriaGridDataView_viPekerjaanSetup += " ORDER BY KD_PEKERJAAN ASC ";

    return strKriteriaGridDataView_viPekerjaanSetup;
}
// End Function getCriteriaFilterGridDataView_viPekerjaanSetup # --------------

function datarefresh_viPekerjaan()
{
    dataSource_viPekerjaan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCount_viPekerjaan,
                Sort: '',
                Sortdir: 'ASC',
                target: 'viPEKERJAANSetup',//'viviewPenilaianPegawai',
                param: getCriteriaFilterGridDataView_viPekerjaanSetup(),
            }
        }
    );
    return dataSource_viPekerjaan;
}


function datainit_viPekerjaan(rowdata)
{
    addNew_viPekerjaan = false;
    Ext.get('txtKdPekerjaan_viPekerjaan').dom.value=rowdata.KD_PEKERJAAN;
    Ext.get('txtNmPekerjaan_viPekerjaan').dom.value=rowdata.PEKERJAAN;
    
}


function dataaddnew_viPekerjaan()
{
    addNew_viPekerjaan = true;
	//GetLastKdPekerjaan();
    Ext.get('txtKdPekerjaan_viPekerjaan').dom.value='';
    Ext.get('txtNmPekerjaan_viPekerjaan').dom.value='';
    rowSelected_viPekerjaan   = undefined;
	//GetLastKdPekerjaan();
}
///---------------------------------------------------------------------------------------///

function dataparam_viPekerjaan()
{
    var params =
	{
		Table: 'viPEKERJAANSetup',
		KD_PEKERJAAN:Ext.get('txtKdPekerjaan_viPekerjaan').getValue(), 
		KD_CUSTOMER: strKD_CUST,
		PEKERJAAN:Ext.getCmp('txtNmPekerjaan_viPekerjaan').getValue()
	}
    return params
}


/*function GetLastKdPekerjaan()
{
	Ext.Ajax.request
	 (
		{
            url: "./Module.mvc/ExecProc",
            params: 
			{
                UserID: 'Admin',
                ModuleID: 'GetLastKdPekerjaan',
				Params:	strKD_CUST
            },
            success: function(o) 
			{
				
                var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{		
					Ext.get('txtKdPekerjaan_viPekerjaan').dom.value=cst.KD_PEKERJAAN;				
					//Ext.getCmp('txtKdPekerjaan_viPekerjaan').setValue(cst.KD_PEKERJAAN);
				}
				else
				{
					ShowPesanWarning_viDaftar('KD_PEKERJAAN tidak di temukan ','Informasi');
				};
            }

        }
	);
}*/




