// Data Source ExtJS

/**
*   Nama File       : setPendidikan.js
*   Menu            : Setup
*   Model id        : 030726
*   Keterangan      : Untuk Pengaturan Setting Data Master Pendidikan
*   Di buat tanggal : 05 Juni 2014
*   Oleh            : SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var mod_name_viPendidikan="viPendidikan";
var DfltFilterBtn_viPendidikan = 0;
var dataSourceGrid_viPendidikan;
var selectCount_viPendidikan=50;
var NamaForm_viPendidikan="Setup Pendidikan";
var icons_viPendidikan="Jabatan";
var addNew_viPendidikan;
var rowSelectedGridDataView_viPendidikan;
var setLookUps_viPendidikan;

CurrentPage.page = dataGrid_viPendidikan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// Start Project Setup Pendidikan

// --------------------------------------- # Start Function # ---------------------------------------

/**
*   Function : ShowPesanWarning_viPendidikan
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanWarning_viPendidikan(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width :250
        }
    )
}
// End Function ShowPesanWarning_viPendidikan # --------------

/**
*   Function : ShowPesanError_viPendidikan
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanError_viPendidikan(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width :250
        }
    )
}
// End Function ShowPesanError_viPendidikan # --------------

/**
*   Function : ShowPesanInfo_viPendidikan
*   
*   Sebuah fungsi untuk menampilkan pesan saat data berhasil
*/

function ShowPesanInfo_viPendidikan(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width :250
        }
    )
}
// End Function ShowPesanInfo_viPendidikan # --------------

/**
*   Function : getCriteriaFilterGridDataView_viPendidikan
*   
*   Sebuah fungsi untuk memfilter data yang diambil dari Net.
*/
function getCriteriaFilterGridDataView_viPendidikan()
{
    var strKriteriaGridDataView_viPendidikan = "";

    if(DfltFilterBtn_viPendidikan == 1)
    {

        if(Ext.getCmp('TxtFilterGridDataView_KD_PENDIDIKAN_viPendidikan').getValue() != "")
        {
            strKriteriaGridDataView_viPendidikan = " WHERE KD_PENDIDIKAN = '" + Ext.getCmp('TxtFilterGridDataView_KD_PENDIDIKAN_viPendidikan').getValue() + "' ";        
        }

        if(Ext.getCmp('TxtFilterGridDataView_PENDIDIKAN_viPendidikan').getValue() != "")
        {
            if(Ext.getCmp('TxtFilterGridDataView_KD_PENDIDIKAN_viPendidikan').getValue() != "")
            {
                strKriteriaGridDataView_viPendidikan += " AND Pendidikan LIKE '%" + Ext.getCmp('TxtFilterGridDataView_PENDIDIKAN_viPendidikan').getValue() + "%' ";
            }
            else
            {
                strKriteriaGridDataView_viPendidikan = " WHERE Pendidikan LIKE '%" + Ext.getCmp('TxtFilterGridDataView_PENDIDIKAN_viPendidikan').getValue() + "%' ";
            }
        }

    }

    strKriteriaGridDataView_viPendidikan += " ORDER BY KD_PENDIDIKAN ASC ";

    return strKriteriaGridDataView_viPendidikan;
}
// End Function getCriteriaFilterGridDataView_viPendidikan # --------------

/**
*   Function : DataRefresh_viPendidikan
*   
*   Sebuah fungsi untuk mengambil data dari Net.
*   Digunakan pada View Grid Pertama, Filter Grid 
*/

function DataRefresh_viPendidikan(criteria)
{
    
    dataSourceGrid_viPendidikan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCount_viPendidikan,
                Sort: 'KD_PENDIDIKAN',
                Sortdir: 'ASC',
                target:'viPendidikan',
                param: criteria
                
            }
        }
    );    
    return dataSourceGrid_viPendidikan;
}
// End Function DataRefresh_viPendidikan # --------------

/**
*   Function : dataaddnew_viPendidikan
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function dataaddnew_viPendidikan(rowdata)
{
    addNew_viPendidikan = true;

    //-------------- # textfield # --------------
    Ext.getCmp('TxtWindowPopup_KD_PENDIDIKAN_viPendidikan').setValue('');
    Ext.getCmp('TxtWindowPopup_PENDIDIKAN_viPendidikan').setValue('');
}
// End Function dataaddnew_viPendidikan # --------------

/**
*   Function : datainit_viPendidikan
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function datainit_viPendidikan(rowdata)
{
    addNew_viPendidikan = false;

    //-------------- # textfield # --------------
    Ext.get('TxtWindowPopup_KD_PENDIDIKAN_viPendidikan').dom.value=rowdata.KD_PENDIDIKAN;
    Ext.get('TxtWindowPopup_PENDIDIKAN_viPendidikan').dom.value=rowdata.PENDIDIKAN;
}
// End Function datainit_viPendidikan # --------------

/**
*   Function : dataparam_viPendidikan
*   
*   Sebuah fungsi untuk mengirim balik isian ke Net.
*   Digunakan saat save, edit, delete
*/

function dataparam_viPendidikan()
{
    var params_viPendidikan =
    {
        //-------------- # modelist Net. # --------------
        Table: 'viPendidikan',

        //-------------- # textfield # --------------
        KD_PENDIDIKAN: Ext.get('TxtWindowPopup_KD_PENDIDIKAN_viPendidikan').getValue(),
        PENDIDIKAN: Ext.get('TxtWindowPopup_PENDIDIKAN_viPendidikan').getValue(),
    }

   return params_viPendidikan
}
// End Function dataparam_viPendidikan # --------------

/**
*   Function : datasave_viPendidikan)
*   
*   Sebuah fungsi untuk menyimpan dan mengedit data entry 
*/

function datasave_viPendidikan(mBol)
{
    if (Ext.get('TxtWindowPopup_PENDIDIKAN_viPendidikan').getValue()!='Nama Pendidikan')
    {
        if ( addNew_viPendidikan == true)
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/CreateDataObj",
                    params: dataparam_viPendidikan(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            Ext.get('TxtWindowPopup_KD_PENDIDIKAN_viPendidikan').dom.value=cst.KD_PENDIDIKAN;
                            Ext.get('TxtWindowPopup_PENDIDIKAN_viPendidikan').dom.value=cst.PENDIDIKAN;
                            ShowPesanInfo_viPendidikan('Data berhasil disimpan','Simpan Data');
                            addNew_viPendidikan = false;
                            DataRefresh_viPendidikan(getCriteriaFilterGridDataView_viPendidikan());
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viPendidikan('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                        else
                        {
                            ShowPesanError_viPendidikan('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                    }
                }
            )
        }
        else
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/UpdateDataObj",
                    params: dataparam_viPendidikan(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            Ext.get('TxtWindowPopup_KD_PENDIDIKAN_viPendidikan').dom.value=cst.KD_PENDIDIKAN;
                            Ext.get('TxtWindowPopup_PENDIDIKAN_viPendidikan').dom.value=cst.PENDIDIKAN;
                            ShowPesanInfo_viPendidikan('Data berhasil disimpan','Edit Data');
                            DataRefresh_viPendidikan(getCriteriaFilterGridDataView_viPendidikan());
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viPendidikan('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                        else
                        {
                            ShowPesanError_viPendidikan('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                    }
                }
            )
        }

    }
    else
    {
        ShowPesanWarning_viPendidikan('Nama Pendidikan belum terisi','Simpan Data');
        if(mBol === true)
        {
            return false;
        }
    }
}
// End Function datasave_viPendidikan) # --------------

/**
*   Function : datadelete_viPendidikan
*   
*   Sebuah fungsi untuk menghapus data entry 
*/

function datadelete_viPendidikan()
{
    /*if (ValidasiEntry_viPendidikan('Hapus Data',true) == 1 )
    {*/
        Ext.Msg.show
        (
            {
                title:'Hapus Data',
                msg: "Akan menghapus data?" ,
                buttons: Ext.MessageBox.YESNO,
                width:300,
                fn: function (btn)
                {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                url: "./Datapool.mvc/DeleteDataObj",
                                params: dataparam_viPendidikan(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        DataRefresh_viPendidikan(getCriteriaFilterGridDataView_viPendidikan());
                                        setLookUps_viPendidikan.close(); 
                                        ShowPesanInfo_viPendidikan('Data berhasil dihapus','Hapus Data');
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarning_viPendidikan('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                    else
                                    {
                                        ShowPesanError_viPendidikan('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                }
                            }
                        )
                    }
                }
            }
        )
    //}
}
// End Function datadelete_viPendidikan # --------------

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*   Function : dataGrid_viPendidikan
*   
*   Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viPendidikan(mod_id)
{   
    // Field kiriman dari Project Net.
    var FieldMaster =
    [
        'KD_PENDIDIKAN', 'PENDIDIKAN'
    ];
    // Deklarasi penamaan yang akan digunakan pada Grid
    dataSourceGrid_viPendidikan = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
    DataRefresh_viPendidikan(getCriteriaFilterGridDataView_viPendidikan);
    // Grid Setup Pendidikan
    var grData_viPendidikan = new Ext.grid.EditorGridPanel
    (
    {
        xtype: 'editorgrid',
        title: '',
        store: dataSourceGrid_viPendidikan,
        autoScroll: true,
        columnLines: true,
        border:false,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
        (
        {
            singleSelect: true,
            listeners:
            {
                rowselect: function(sm, row, rec)
                {
                    rowSelectedGridDataView_viPendidikan = undefined;
                    rowSelectedGridDataView_viPendidikan = dataSourceGrid_viPendidikan.getAt(row);
                }
            }
        }
        ),
        listeners:
        {
            // Function saat ada event double klik maka akan muncul form view
            rowdblclick: function (sm, ridx, cidx)
            {
                rowSelectedGridDataView_viPendidikan = dataSourceGrid_viPendidikan.getAt(ridx);
                if (rowSelectedGridDataView_viPendidikan != undefined)
                {
                    setLookUpGridDataView_viPendidikan(rowSelectedGridDataView_viPendidikan.data);
                }
                else
                {
                    setLookUpGridDataView_viPendidikan();
                }
            }
        },
        /**
        *   Mengatur tampilan pada Grid Setup Pendidikan
        *   Terdiri dari : Judul, Isi dan Event
        *   Isi pada Grid di dapat dari pemangilan dari Net.
        *   dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSourceGrid_viPendidikan
        *   didapat dari Function DataRefresh_viPendidikan()
        */
        colModel: new Ext.grid.ColumnModel
        (
            [
                new Ext.grid.RowNumberer(), 
                {
                    header: 'Kd. Pendidikan',
                    dataIndex: 'KD_PENDIDIKAN',
                    id: 'ColGridDataView_KD_PENDIDIKAN_viPendidikan',
                    sortable: true,
                    width: 100,
                    filter:
                    {
                        type: 'int'
                    }
                },
                //-------------- ## --------------
                {
                    header: 'PENDIDIKAN',
                    dataIndex: 'PENDIDIKAN',
                    id: 'ColGridDataView_PENDIDIKAN_viPendidikan',
                    sortable: true,
                    width: 250,
                    filter:
                    {
                        type: 'int'
                    }
                }
                //-------------- ## --------------
            ]
            ),
        tbar: {
            xtype: 'toolbar',
            id: 'ToolbarGridDataView_viPendidikan',
            items: 
            [
                {
                    xtype: 'button',
                    text: 'Edit Data',
                    iconCls: 'Edit_Tr',
                    tooltip: 'Edit Data',
                    id: 'BtnEditGridDataView_viPendidikan',
                    handler: function(sm, row, rec)
                    {
                        if (rowSelectedGridDataView_viPendidikan != undefined)
                        {
                            setLookUpGridDataView_viPendidikan(rowSelectedGridDataView_viPendidikan.data)
                        }
                        else
                        {
                            setLookUpGridDataView_viPendidikan();
                        }
                    }
                }
                //-------------- ## --------------
            ]
        },
        // End Tolbar ke Dua # --------------
        // Button Bar Pagging # --------------
        // Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
        bbar : bbar_paging(mod_name_viPendidikan, selectCount_viPendidikan, dataSourceGrid_viPendidikan),
        // End Button Bar Pagging # --------------
        viewConfig: {
            forceFit: false
        }
    }
    )
    
    // Kriteria filter pada Grid
    var FrmTabs_viPendidikan = new Ext.Panel
    (
    {
        id: mod_id,
        title: NamaForm_viPendidikan,
        region: 'center',
        closable: true,
        border: false,
        layout: 'form',
        iconCls: icons_viPendidikan,
        margins: '0 5 5 0',
        items: [grData_viPendidikan],
        tbar:
        [ 
            { 
                xtype: 'tbtext', 
                text: 'Kd. Pendidikan', 
                cls: 'left-label', 
                width: 85 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_KD_PENDIDIKAN_viPendidikan',
                width: 200,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viPendidikan = 1;
                            DataRefresh_viPendidikan(getCriteriaFilterGridDataView_viPendidikan());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbspacer',
                width: 15,
                height: 25
            },
            //-------------- ## --------------
            { 
                xtype: 'tbtext', 
                text: 'Pendidikan', 
                cls: 'left-label', 
                width: 65 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_PENDIDIKAN_viPendidikan',
                width: 200,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viPendidikan = 1;
                            DataRefresh_viPendidikan(getCriteriaFilterGridDataView_viPendidikan());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbfill' 
            },
            //-------------- ## --------------
            {
                xtype: 'button',
                id: 'btnRefreshFilter_viPendidikan',
                iconCls: 'refresh',
                handler: function()
                {
                    DataRefresh_viPendidikan(getCriteriaFilterGridDataView_viPendidikan());
                }
            }
            //-------------- ## --------------
        ]
        
    }
    )
    DataRefresh_viPendidikan();
    return FrmTabs_viPendidikan;
}

/**
*   Function : setLookUpGridDataView_viPendidikan
*   
*   Sebuah fungsi untuk menampilkan Form Edit saat ada event klik Edit Data atau Alt+F3
*/
function setLookUpGridDataView_viPendidikan(rowdata)
{
    var lebar = 345; // Lebar Form saat PopUp
    setLookUps_viPendidikan = new Ext.Window
    (
    {
        id: 'SetLookUps_viPendidikan',
        title: NamaForm_viPendidikan,
        closeAction: 'destroy',
        autoScroll:true,
        width: 335,
        height: 145,
        resizable:false,
        border: false,
        plain: true,
        layout: 'fit',
        iconCls: icons_viPendidikan,
        modal: true,
        items: getFormItemEntry_viPendidikan(lebar,rowdata),
        listeners:
        {
            activate: function()
            {
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelectedGridDataView_viPendidikan=undefined;
                DataRefresh_viPendidikan(getCriteriaFilterGridDataView_viPendidikan);
            }
        }
    }
    );

    setLookUps_viPendidikan.show();
    if (rowdata == undefined)
    {
        dataaddnew_viPendidikan();
    }
    else
    {
        datainit_viPendidikan(rowdata)
    }
}

/**
*   Function : getFormItemEntry_viPendidikan
*   
*   Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*   Di pangil dari Function setLookUpGridDataView_viPendidikan
*/
function getFormItemEntry_viPendidikan(lebar,rowdata)
{
    var pnlFormDataBasic_viPendidikan = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            fileUpload: true,
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            labelWidth: 100,
            // Tombol pada tollbar Edit Setup Pendidikan
            tbar: {
                xtype: 'toolbar',
                items: 
                [
                    {
                        xtype: 'button',
                        text: 'Add',
                        id: 'btnTambahWindowPopup_viPendidikan',
                        iconCls: 'add',
                        handler: function()
                        {
                            dataaddnew_viPendidikan();
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save',
                        id: 'btnSimpanWindowPopup_viPendidikan',
                        iconCls: 'save',
                        handler: function()
                        {
                            datasave_viPendidikan(false);
                            DataRefresh_viPendidikan(getCriteriaFilterGridDataView_viPendidikan);
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save & Close',
                        id: 'btnSimpanKeluarWindowPopup_viPendidikan',
                        iconCls: 'saveexit',
                        handler: function()
                        {
                            var x = datasave_viPendidikan(true);
                            DataRefresh_viPendidikan();
                            if (x===undefined)
                            {
                                setLookUps_viPendidikan.close();
                            }
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Delete',
                        id: 'btnHapusWindowPopup_viPendidikan',
                        iconCls: 'remove',
                        handler: function()
                        {
                            datadelete_viPendidikan();
                            DataRefresh_viPendidikan();
                        }
                    }
                    //-------------- ## --------------
                ]
            },
            //-------------- #items# --------------
            items:
            [
                // Isian Pada Edit Setup Pendidikan
                {
                    xtype: 'fieldset',
                    title: '',
                    width: 305,
                    height: 70,
                    items: 
                    [                                   
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kd. Pendidikan',
                            id: 'TxtWindowPopup_KD_PENDIDIKAN_viPendidikan',
                            name: 'TxtWindowPopup_KD_PENDIDIKAN_viPendidikan',
                            emptyText: 'Kode Pendidikan',
                            readOnly: true,
                            width: 100, 
                            flex: 1,                
                        },
                        //-------------- ## --------------  
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Pendidikan',
                            id: 'TxtWindowPopup_PENDIDIKAN_viPendidikan',
                            name: 'TxtWindowPopup_PENDIDIKAN_viPendidikan',
                            emptyText: 'Nama Pendidikan',
                            width: 177, 
                            flex: 1,                
                        }
                        //-------------- ## -------------- 
                    ]       
                }
            ],
            //-------------- #items# --------------
        }
    )
    return pnlFormDataBasic_viPendidikan;
}

// --------------------------------------- # End Form # ---------------------------------------

// End Project Setup Pendidikan # --------------