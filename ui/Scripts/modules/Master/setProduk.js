// Data Source ExtJS

/**
*   Nama File       : setProduk.js
*   Menu            : Setup
*   Model id        : 030724
*   Keterangan      : Untuk Pengaturan Setting Data Master Produk
*   Di buat tanggal : 06 Juni 2014
*   Oleh            : SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var mod_name_viProduk="viProduk";
var DfltFilter_KD_UNIT_viProduk = strKD_CUST;
var DfltFilter_KD_UNIT_viProduk = gstrListUnitKerja;
var DfltFilterBtn_viProduk = 0;
var dataSourceGrid_viProduk;
var selectCount_viProduk=50;
var NamaForm_viProduk="Setup Produk";
var icons_viProduk="Jabatan";
var addNew_viProduk;
var rowSelectedGridDataView_viProduk;
var setLookUps_viProduk;

CurrentPage.page = dataGrid_viProduk(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// Start Project Setup Produk

// --------------------------------------- # Start Function # ---------------------------------------

/**
*   Function : ShowPesanWarning_viProduk
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanWarning_viProduk(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width :250
        }
    )
}
// End Function ShowPesanWarning_viProduk # --------------

/**
*   Function : ShowPesanError_viProduk
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanError_viProduk(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width :250
        }
    )
}
// End Function ShowPesanError_viProduk # --------------

/**
*   Function : ShowPesanInfo_viProduk
*   
*   Sebuah fungsi untuk menampilkan pesan saat data berhasil
*/

function ShowPesanInfo_viProduk(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width :250
        }
    )
}
// End Function ShowPesanInfo_viProduk # --------------

/**
*   Function : getCriteriaFilterGridDataView_viProduk
*   
*   Sebuah fungsi untuk memfilter data yang diambil dari Net.
*/
function getCriteriaFilterGridDataView_viProduk()
{
    var strKriteriaGridDataView_viProduk = "";

    strKriteriaGridDataView_viProduk = " WHERE P.KD_UNIT in " + DfltFilter_KD_UNIT_viProduk + " ";

    if(DfltFilterBtn_viProduk == 1)
    {
        
        if(Ext.getCmp('TxtFilterGridDataView_KD_PRODUK_viProduk').getValue() != "")
        {
            strKriteriaGridDataView_viProduk += " AND RIGHT(KD_PRODUK,6) LIKE '%" + Ext.getCmp('TxtFilterGridDataView_KD_PRODUK_viProduk').getValue() + "%' ";        
        }

        if(Ext.getCmp('TxtFilterGridDataView_DESKRIPSI_viProduk').getValue() != "")
        {
            strKriteriaGridDataView_viProduk += " AND DESKRIPSI LIKE '%" + Ext.getCmp('TxtFilterGridDataView_DESKRIPSI_viProduk').getValue() + "%' ";
        }
        else
        {
            strKriteriaGridDataView_viProduk += " AND DESKRIPSI LIKE '%%' ";            
        }

        if(Ext.getCmp('CboPoliFilter_viProduk').getValue() != "")
        {
            strKriteriaGridDataView_viProduk += " AND U.NAMA_UNIT LIKE '%" + Ext.get('CboPoliFilter_viProduk').dom.value + "%' ";
        }

    }

    strKriteriaGridDataView_viProduk += " ORDER BY KD_PRODUK ASC ";

    return strKriteriaGridDataView_viProduk;
}
// End Function getCriteriaFilterGridDataView_viProduk # --------------

/**
*   Function : DataRefresh_viProduk
*   
*   Sebuah fungsi untuk mengambil data dari Net.
*   Digunakan pada View Grid Pertama, Filter Grid 
*/

function DataRefresh_viProduk(criteria)
{
    
    dataSourceGrid_viProduk.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCount_viProduk,
                Sort: 'KD_PRODUK',
                Sortdir: 'ASC',
                target:'viProduk',
                param: criteria
                
            }
        }
    );    
    return dataSourceGrid_viProduk;
}
// End Function DataRefresh_viProduk # --------------

/**
*   Function : viCombo_POLI_viProduk
*   
*   Sebuah fungsi untuk menampilkan combobox Poli
*/

function viRefresh_Poli_viProduk()
{
    ds_Poli_viProduk.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_UNIT',
                Sortdir: 'ASC',
                target:'viUNIT',
                param: " Where KD_CUSTOMER='" + strKD_CUST + "' "
            }            
        }
    )
}

function viCombo_POLI_viProduk(lebar,Nama_ID)
{
    var Field_poli_viProduk = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viProduk = new WebApp.DataStore({fields: Field_poli_viProduk});
    
    viRefresh_Poli_viProduk();
    var cbo_Poli_viProduk = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Poli',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store: ds_Poli_viProduk,
            emptyText: 'Poli',
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
            { 
                'specialkey' : function()
                {
                    if (Ext.EventObject.getKey() === 13) 
                    {
                        DfltFilterBtn_viProduk = 1;
                        DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());                              
                    }                       
                }
            }
        }
    )
    
    return cbo_Poli_viProduk;
}
// End Function viCombo_POLI_viProduk # --------------

/**
*   Function : dataaddnew_viProduk
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function dataaddnew_viProduk(rowdata)
{
    addNew_viProduk = true;

    //-------------- # textfield # --------------
    Ext.getCmp('TxtWindowPopup_KD_PRODUK_viProduk').setValue('');
    Ext.getCmp('TxtWindowPopup_KD_PRODUK_VIEW_viProduk').setValue('');
    Ext.getCmp('TxtWindowPopup_DESKRIPSI_viProduk').setValue('');
    Ext.getCmp('TxtWindowPopup_TARIF_viProduk').setValue('');
    Ext.getCmp('CmboWindowPopup_POLI_viProduk').setValue('');
}
// End Function dataaddnew_viProduk # --------------

/**
*   Function : datainit_viProduk
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function datainit_viProduk(rowdata)
{
    addNew_viProduk = false;

    //-------------- # textfield # --------------
    Ext.get('TxtWindowPopup_KD_PRODUK_viProduk').dom.value=rowdata.KD_PRODUK;
    Ext.get('TxtWindowPopup_KD_PRODUK_VIEW_viProduk').dom.value=rowdata.KD_PRODUK_VIEW;
    Ext.get('TxtWindowPopup_DESKRIPSI_viProduk').dom.value=rowdata.DESKRIPSI;
    //Ext.get('TxtWindowPopup_TARIF_viProduk').dom.value=rowdata.TARIF;
    Ext.getCmp('TxtWindowPopup_TARIF_viProduk').setValue(formatCurrency(rowdata.TARIF));

    //-------------- # combobox # --------------
    Ext.getCmp('CmboWindowPopup_POLI_viProduk').setValue(rowdata.KD_UNIT);
    Ext.get('CmboWindowPopup_POLI_viProduk').dom.value=rowdata.NAMA_UNIT;
}
// End Function datainit_viProduk # --------------

/**
*   Function : dataparam_viProduk
*   
*   Sebuah fungsi untuk mengirim balik isian ke Net.
*   Digunakan saat save, edit, delete
*/

function dataparam_viProduk()
{
    var params_viProduk =
    {
        //-------------- # modelist Net. # --------------
        Table: 'viProduk',

        //-------------- # textfield # --------------
        KD_PRODUK: Ext.get('TxtWindowPopup_KD_PRODUK_viProduk').getValue(),
        KD_PRODUK_VIEW: Ext.get('TxtWindowPopup_KD_PRODUK_VIEW_viProduk').getValue(),
        DESKRIPSI: Ext.get('TxtWindowPopup_DESKRIPSI_viProduk').getValue(),
        TARIF: NonFormatCurrency(Ext.get('TxtWindowPopup_TARIF_viProduk').getValue()),
        KD_UNIT: Ext.getCmp('CmboWindowPopup_POLI_viProduk').getValue(),
        NAMA_UNIT: Ext.get('CmboWindowPopup_POLI_viProduk').dom.value,
    }

   return params_viProduk
}
// End Function dataparam_viProduk # --------------

/**
*   Function : datasave_viProduk)
*   
*   Sebuah fungsi untuk menyimpan dan mengedit data entry 
*/

function datasave_viProduk(mBol)
{
    if ((Ext.get('TxtWindowPopup_DESKRIPSI_viProduk').getValue()!='Nama Deskripsi') && (Ext.get('CmboWindowPopup_POLI_viProduk').dom.value!='Poli'))
    {
        if ( addNew_viProduk == true)
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/CreateDataObj",
                    params: dataparam_viProduk(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            Ext.get('TxtWindowPopup_KD_PRODUK_viProduk').dom.value=cst.KD_PRODUK;
                            Ext.get('TxtWindowPopup_KD_PRODUK_VIEW_viProduk').dom.value=cst.KD_PRODUK_VIEW;
                            Ext.get('TxtWindowPopup_DESKRIPSI_viProduk').dom.value=cst.DESKRIPSI;
                            Ext.get('TxtWindowPopup_TARIF_viProduk').dom.value=cst.TARIF;
                            Ext.getCmp('CmboWindowPopup_POLI_viProduk').setValue(cst.KD_UNIT);
                            Ext.get('CmboWindowPopup_POLI_viProduk').dom.value=cst.NAMA_UNIT;

                            ShowPesanInfo_viProduk('Data berhasil disimpan','Simpan Data');
                            addNew_viProduk = false;
                            DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viProduk('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                        else
                        {
                            ShowPesanError_viProduk('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                    }
                }
            )
        }
        else
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/UpdateDataObj",
                    params: dataparam_viProduk(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            Ext.get('TxtWindowPopup_KD_PRODUK_viProduk').dom.value=cst.KD_PRODUK;
                            Ext.get('TxtWindowPopup_KD_PRODUK_VIEW_viProduk').dom.value=cst.KD_PRODUK_VIEW;
                            Ext.get('TxtWindowPopup_DESKRIPSI_viProduk').dom.value=cst.DESKRIPSI;
                            Ext.get('TxtWindowPopup_TARIF_viProduk').dom.value=cst.TARIF;
                            Ext.getCmp('CmboWindowPopup_POLI_viProduk').setValue(cst.KD_UNIT);
                            Ext.get('CmboWindowPopup_POLI_viProduk').dom.value=cst.NAMA_UNIT;

                            ShowPesanInfo_viProduk('Data berhasil disimpan','Edit Data');
                            DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viProduk('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                        else
                        {
                            ShowPesanError_viProduk('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                    }
                }
            )
        }

    }
    else
    {
        ShowPesanWarning_viProduk('Nama Deskripsi atau Poli belum terisi','Simpan Data');
        if(mBol === true)
        {
            return false;
        }
    }
}
// End Function datasave_viProduk) # --------------

/**
*   Function : datadelete_viProduk
*   
*   Sebuah fungsi untuk menghapus data entry 
*/

function datadelete_viProduk()
{
    /*if (ValidasiEntry_viProduk('Hapus Data',true) == 1 )
    {*/
        Ext.Msg.show
        (
            {
                title:'Hapus Data',
                msg: "Akan menghapus data?" ,
                buttons: Ext.MessageBox.YESNO,
                width:300,
                fn: function (btn)
                {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                url: "./Datapool.mvc/DeleteDataObj",
                                params: dataparam_viProduk(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());
                                        setLookUps_viProduk.close(); 
                                        ShowPesanInfo_viProduk('Data berhasil dihapus','Hapus Data');
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarning_viProduk('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                    else
                                    {
                                        ShowPesanError_viProduk('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                }
                            }
                        )
                    }
                }
            }
        )
    //}
}
// End Function datadelete_viProduk # --------------

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*   Function : dataGrid_viProduk
*   
*   Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viProduk(mod_id)
{   
    // Field kiriman dari Project Net.
    var FieldMaster =
    [
        'KD_PRODUK', 'KD_PRODUK_VIEW', 'KD_UNIT', 'NAMA_UNIT', 'DESKRIPSI', 'TARIF'
    ];
    // Deklarasi penamaan yang akan digunakan pada Grid
    dataSourceGrid_viProduk = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
    DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());
    // Grid Setup Produk
    var grData_viProduk = new Ext.grid.EditorGridPanel
    (
    {
        xtype: 'editorgrid',
        title: '',
        store: dataSourceGrid_viProduk,
        autoScroll: true,
        columnLines: true,
        border:false,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
        (
        {
            singleSelect: true,
            listeners:
            {
                rowselect: function(sm, row, rec)
                {
                    rowSelectedGridDataView_viProduk = undefined;
                    rowSelectedGridDataView_viProduk = dataSourceGrid_viProduk.getAt(row);
                }
            }
        }
        ),
        listeners:
        {
            // Function saat ada event double klik maka akan muncul form view
            rowdblclick: function (sm, ridx, cidx)
            {
                rowSelectedGridDataView_viProduk = dataSourceGrid_viProduk.getAt(ridx);
                if (rowSelectedGridDataView_viProduk != undefined)
                {
                    setLookUpGridDataView_viProduk(rowSelectedGridDataView_viProduk.data);
                }
                else
                {
                    setLookUpGridDataView_viProduk();
                }
            }
        },
        /**
        *   Mengatur tampilan pada Grid Setup Produk
        *   Terdiri dari : Judul, Isi dan Event
        *   Isi pada Grid di dapat dari pemangilan dari Net.
        *   dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSourceGrid_viProduk
        *   didapat dari Function DataRefresh_viProduk()
        */
        colModel: new Ext.grid.ColumnModel
        (
            [
                new Ext.grid.RowNumberer(), 
                {
                    header: 'Kd. Produk',
                    dataIndex: 'KD_PRODUK_VIEW',
                    id: 'ColGridDataView_KD_PRODUK_viProduk',
                    sortable: true,
                    width: 100,
                    filter:
                    {
                        type: 'int'
                    }
                },
                //-------------- ## --------------
                {
                    header: 'Deskripsi',
                    dataIndex: 'DESKRIPSI',
                    id: 'ColGridDataView_DESKRIPSI_viProduk',
                    sortable: true,
                    width: 250,
                    filter:
                    {
                        type: 'int'
                    }
                },
                //-------------- ## --------------
                {
                    header: 'Tarif',
                    dataIndex: 'TARIF',
                    id: 'ColGridDataView_TARIF_viProduk',
                    sortable: true,
                    width: 100,
                    filter:
                    {
                        type: 'int'
                    },
                    align:'right',
                    renderer: function(v, params, record) 
                    {
                        return formatCurrency(record.data.TARIF);
                    },  
                },
                //-------------- ## --------------
                {
                    header: 'Poli',
                    dataIndex: 'NAMA_UNIT',
                    id: 'ColGridDataView_NAMA_UNIT_viProduk',
                    sortable: true,
                    width: 150,
                    filter:
                    {
                        type: 'int'
                    }
                }
                //-------------- ## --------------
            ]
            ),
        tbar: {
            xtype: 'toolbar',
            id: 'ToolbarGridDataView_viProduk',
            items: 
            [
                {
                    xtype: 'button',
                    text: 'Edit Data',
                    iconCls: 'Edit_Tr',
                    tooltip: 'Edit Data',
                    id: 'BtnEditGridDataView_viProduk',
                    handler: function(sm, row, rec)
                    {
                        if (rowSelectedGridDataView_viProduk != undefined)
                        {
                            setLookUpGridDataView_viProduk(rowSelectedGridDataView_viProduk.data)
                        }
                        else
                        {
                            setLookUpGridDataView_viProduk();
                        }
                    }
                }
                //-------------- ## --------------
            ]
        },
        // End Tolbar ke Dua # --------------
        // Button Bar Pagging # --------------
        // Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
        bbar : bbar_paging(mod_name_viProduk, selectCount_viProduk, dataSourceGrid_viProduk),
        // End Button Bar Pagging # --------------
        viewConfig: {
            forceFit: false
        }
    }
    )
    
    // Kriteria filter pada Grid
    var FrmTabs_viProduk = new Ext.Panel
    (
    {
        id: mod_id,
        title: NamaForm_viProduk,
        region: 'center',
        closable: true,
        border: false,
        layout: 'form',
        iconCls: icons_viProduk,
        margins: '0 5 5 0',
        items: [grData_viProduk],
        tbar:
        [ 
            { 
                xtype: 'tbtext', 
                text: 'Kd. Produk', 
                cls: 'left-label', 
                width: 80 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_KD_PRODUK_viProduk',
                emptyText: 'Kode Produk',
                width: 100,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viProduk = 1;
                            DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbspacer',
                width: 15,
                height: 25
            },
            //-------------- ## --------------
            { 
                xtype: 'tbtext', 
                text: 'Deskripsi', 
                cls: 'left-label', 
                width: 65 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_DESKRIPSI_viProduk',
                emptyText: 'Deskripsi',
                width: 200,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viProduk = 1;
                            DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbspacer',
                width: 15,
                height: 25
            },
            //-------------- ## --------------
            { 
                xtype: 'tbtext', 
                text: 'Poli ', 
                cls: 'left-label', 
                width: 45 
            },
            viCombo_POLI_viProduk(150,"CboPoliFilter_viProduk"),
            //-------------- ## --------------
            { 
                xtype: 'tbfill' 
            },
            //-------------- ## --------------
            {
                xtype: 'button',
                id: 'btnRefreshFilter_viProduk',
                iconCls: 'refresh',
                handler: function()
                {
                    DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());
                }
            }
            //-------------- ## --------------
        ]
        
    }
    )
    DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());
    return FrmTabs_viProduk;
}

/**
*   Function : setLookUpGridDataView_viProduk
*   
*   Sebuah fungsi untuk menampilkan Form Edit saat ada event klik Edit Data atau Alt+F3
*/
function setLookUpGridDataView_viProduk(rowdata)
{
    var lebar = 345; // Lebar Form saat PopUp
    setLookUps_viProduk = new Ext.Window
    (
    {
        id: 'SetLookUps_viProduk',
        title: NamaForm_viProduk,
        closeAction: 'destroy',
        autoScroll:true,
        width: 335,
        height: 200,
        resizable:false,
        border: false,
        plain: true,
        layout: 'fit',
        iconCls: icons_viProduk,
        modal: true,
        items: getFormItemEntry_viProduk(lebar,rowdata),
        listeners:
        {
            activate: function()
            {
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelectedGridDataView_viProduk=undefined;
                DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());
            }
        }
    }
    );

    setLookUps_viProduk.show();
    if (rowdata == undefined)
    {
        dataaddnew_viProduk();
    }
    else
    {
        datainit_viProduk(rowdata)
    }
}

/**
*   Function : getFormItemEntry_viProduk
*   
*   Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*   Di pangil dari Function setLookUpGridDataView_viProduk
*/
function getFormItemEntry_viProduk(lebar,rowdata)
{
    var pnlFormDataBasic_viProduk = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            fileUpload: true,
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            labelWidth: 100,
            // Tombol pada tollbar Edit Setup Produk
            tbar: {
                xtype: 'toolbar',
                items: 
                [
                    {
                        xtype: 'button',
                        text: 'Add',
                        id: 'btnTambahWindowPopup_viProduk',
                        iconCls: 'add',
                        handler: function()
                        {
                            dataaddnew_viProduk();
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save',
                        id: 'btnSimpanWindowPopup_viProduk',
                        iconCls: 'save',
                        handler: function()
                        {
                            datasave_viProduk(false);
                            DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save & Close',
                        id: 'btnSimpanKeluarWindowPopup_viProduk',
                        iconCls: 'saveexit',
                        handler: function()
                        {
                            var x = datasave_viProduk(true);
                            DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());
                            if (x===undefined)
                            {
                                setLookUps_viProduk.close();
                            }
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Delete',
                        id: 'btnHapusWindowPopup_viProduk',
                        iconCls: 'remove',
                        handler: function()
                        {
                            datadelete_viProduk();
                            DataRefresh_viProduk(getCriteriaFilterGridDataView_viProduk());
                        }
                    }
                    //-------------- ## --------------
                ]
            },
            //-------------- #items# --------------
            items:
            [
                // Isian Pada Edit Setup Produk
                {
                    xtype: 'fieldset',
                    title: '',
                    width: 305,
                    height: 125,
                    items: 
                    [                                   
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kd. Produk Asli',
                            id: 'TxtWindowPopup_KD_PRODUK_viProduk',
                            name: 'TxtWindowPopup_KD_PRODUK_viProduk',
                            emptyText: 'Kode Produk',
                            readOnly: true,
                            hidden: true,
                        },
                        //-------------- ## --------------  
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kd. Produk',
                            id: 'TxtWindowPopup_KD_PRODUK_VIEW_viProduk',
                            name: 'TxtWindowPopup_KD_PRODUK_VIEW_viProduk',
                            emptyText: 'Kode Produk',
                            readOnly: true,
                            width: 100, 
                            flex: 1,                
                        },
                        //-------------- ## --------------  
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Deskripsi',
                            id: 'TxtWindowPopup_DESKRIPSI_viProduk',
                            name: 'TxtWindowPopup_DESKRIPSI_viProduk',
                            emptyText: 'Nama Deskripsi',
                            width: 177, 
                            flex: 1,                
                        },
                        //-------------- ## -------------- 
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Tarif',
                            id: 'TxtWindowPopup_TARIF_viProduk',
                            name: 'TxtWindowPopup_TARIF_viProduk',
                            emptyText: '0',
                            style:{'text-align':'right'},
                            width: 177, 
                            flex: 1,              
                        },
                        //-------------- ## -------------- 
                        viCombo_POLI_viProduk(177,"CmboWindowPopup_POLI_viProduk"),
                        //-------------- ## -------------- 
                    ]       
                }
            ],
            //-------------- #items# --------------
        }
    )
    return pnlFormDataBasic_viProduk;
}

// --------------------------------------- # End Form # ---------------------------------------

// End Project Setup Produk # --------------