// Data Source ExtJS

/**
*   Nama File       : setSatuan.js
*   Menu            : Setup
*   Model id        : 030726
*   Keterangan      : Untuk Pengaturan Setting Data Master Satuan
*   Di buat tanggal : 05 Juni 2014
*   Oleh            : SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var mod_name_viSatuan="viSatuan";
var DfltFilterBtn_viSatuan = 0;
var dataSourceGrid_viSatuan;
var selectCount_viSatuan=50;
var NamaForm_viSatuan="Setup Satuan";
var icons_viSatuan="Jabatan";
var addNew_viSatuan;
var rowSelectedGridDataView_viSatuan;
var setLookUps_viSatuan;

CurrentPage.page = dataGrid_viSatuan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// Start Project Setup Satuan

// --------------------------------------- # Start Function # ---------------------------------------

/**
*   Function : ShowPesanWarning_viSatuan
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanWarning_viSatuan(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width :250
        }
    )
}
// End Function ShowPesanWarning_viSatuan # --------------

/**
*   Function : ShowPesanError_viSatuan
*   
*   Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
*/

function ShowPesanError_viSatuan(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width :250
        }
    )
}
// End Function ShowPesanError_viSatuan # --------------

/**
*   Function : ShowPesanInfo_viSatuan
*   
*   Sebuah fungsi untuk menampilkan pesan saat data berhasil
*/

function ShowPesanInfo_viSatuan(str,modul)
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg:str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width :250
        }
    )
}
// End Function ShowPesanInfo_viSatuan # --------------

/**
*   Function : getCriteriaFilterGridDataView_viSatuan
*   
*   Sebuah fungsi untuk memfilter data yang diambil dari Net.
*/
function getCriteriaFilterGridDataView_viSatuan()
{
    var strKriteriaGridDataView_viSatuan = "";

    if(DfltFilterBtn_viSatuan == 1)
    {

        if(Ext.getCmp('TxtFilterGridDataView_KD_SATUAN_viSatuan').getValue() != "")
        {
            strKriteriaGridDataView_viSatuan = " WHERE KD_SATUAN = '" + Ext.getCmp('TxtFilterGridDataView_KD_SATUAN_viSatuan').getValue() + "' ";        
        }

        if(Ext.getCmp('TxtFilterGridDataView_SATUAN_viSatuan').getValue() != "")
        {
            if(Ext.getCmp('TxtFilterGridDataView_KD_SATUAN_viSatuan').getValue() != "")
            {
                strKriteriaGridDataView_viSatuan += " AND SATUAN LIKE '%" + Ext.getCmp('TxtFilterGridDataView_SATUAN_viSatuan').getValue() + "%' ";
            }
            else
            {
                strKriteriaGridDataView_viSatuan = " WHERE SATUAN LIKE '%" + Ext.getCmp('TxtFilterGridDataView_SATUAN_viSatuan').getValue() + "%' ";
            }
        }

    }

    strKriteriaGridDataView_viSatuan += " ORDER BY KD_SATUAN ASC ";

    return strKriteriaGridDataView_viSatuan;
}
// End Function getCriteriaFilterGridDataView_viSatuan # --------------

/**
*   Function : DataRefresh_viSatuan
*   
*   Sebuah fungsi untuk mengambil data dari Net.
*   Digunakan pada View Grid Pertama, Filter Grid 
*/

function DataRefresh_viSatuan(criteria)
{
    
    dataSourceGrid_viSatuan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCount_viSatuan,
                Sort: 'KD_SATUAN',
                Sortdir: 'ASC',
                target:'Viview_viSatuan',
                param: criteria
                
            }
        }
    );    
    return dataSourceGrid_viSatuan;
}
// End Function DataRefresh_viSatuan # --------------

/**
*   Function : dataaddnew_viSatuan
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function dataaddnew_viSatuan(rowdata)
{
    addNew_viSatuan = true;

    //-------------- # textfield # --------------
    Ext.getCmp('TxtWindowPopup_KD_SATUAN_viSatuan').setValue('');
    Ext.getCmp('TxtWindowPopup_SATUAN_viSatuan').setValue('');
}
// End Function dataaddnew_viSatuan # --------------

/**
*   Function : datainit_viSatuan
*   
*   Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
*/

function datainit_viSatuan(rowdata)
{
    addNew_viSatuan = false;

    //-------------- # textfield # --------------
    Ext.get('TxtWindowPopup_KD_SATUAN_viSatuan').dom.value=rowdata.KD_SATUAN;
    Ext.get('TxtWindowPopup_SATUAN_viSatuan').dom.value=rowdata.SATUAN;
}
// End Function datainit_viSatuan # --------------

/**
*   Function : dataparam_viSatuan
*   
*   Sebuah fungsi untuk mengirim balik isian ke Net.
*   Digunakan saat save, edit, delete
*/

function dataparam_viSatuan()
{
    var params_viSatuan =
    {
        //-------------- # modelist Net. # --------------
        Table: 'Viview_viSatuan',

        //-------------- # textfield # --------------
        KD_SATUAN: Ext.get('TxtWindowPopup_KD_SATUAN_viSatuan').getValue(),
        SATUAN: Ext.get('TxtWindowPopup_SATUAN_viSatuan').getValue(),
    }

   return params_viSatuan
}
// End Function dataparam_viSatuan # --------------

/**
*   Function : datasave_viSatuan)
*   
*   Sebuah fungsi untuk menyimpan dan mengedit data entry 
*/

function datasave_viSatuan(mBol)
{
    if (Ext.get('TxtWindowPopup_SATUAN_viSatuan').getValue()!='Nama Satuan')
    {
        if ( addNew_viSatuan == true)
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/CreateDataObj",
                    params: dataparam_viSatuan(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            Ext.get('TxtWindowPopup_KD_SATUAN_viSatuan').dom.value=cst.KD_SATUAN;
                            Ext.get('TxtWindowPopup_SATUAN_viSatuan').dom.value=cst.SATUAN;
                            ShowPesanInfo_viSatuan('Data berhasil disimpan','Simpan Data');
                            addNew_viSatuan = false;
                            DataRefresh_viSatuan(getCriteriaFilterGridDataView_viSatuan());
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viSatuan('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                        else
                        {
                            ShowPesanError_viSatuan('Data tidak berhasil disimpan '  + cst.pesan,'Simpan Data');
                        }
                    }
                }
            )
        }
        else
        {
            Ext.Ajax.request
            (
                {
                    url: "./Datapool.mvc/UpdateDataObj",
                    params: dataparam_viSatuan(),
                    success: function(o)
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            Ext.get('TxtWindowPopup_KD_SATUAN_viSatuan').dom.value=cst.KD_SATUAN;
                            Ext.get('TxtWindowPopup_SATUAN_viSatuan').dom.value=cst.SATUAN;
                            ShowPesanInfo_viSatuan('Data berhasil disimpan','Edit Data');
                            DataRefresh_viSatuan(getCriteriaFilterGridDataView_viSatuan());
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarning_viSatuan('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                        else
                        {
                            ShowPesanError_viSatuan('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                        }
                    }
                }
            )
        }

    }
    else
    {
        ShowPesanWarning_viSatuan('Nama Satuan belum terisi','Simpan Data');
        if(mBol === true)
        {
            return false;
        }
    }
}
// End Function datasave_viSatuan) # --------------

/**
*   Function : datadelete_viSatuan
*   
*   Sebuah fungsi untuk menghapus data entry 
*/

function datadelete_viSatuan()
{
    /*if (ValidasiEntry_viSatuan('Hapus Data',true) == 1 )
    {*/
        Ext.Msg.show
        (
            {
                title:'Hapus Data',
                msg: "Akan menghapus data?" ,
                buttons: Ext.MessageBox.YESNO,
                width:300,
                fn: function (btn)
                {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                url: "./Datapool.mvc/DeleteDataObj",
                                params: dataparam_viSatuan(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        DataRefresh_viSatuan(getCriteriaFilterGridDataView_viSatuan());
                                        setLookUps_viSatuan.close(); 
                                        ShowPesanInfo_viSatuan('Data berhasil dihapus','Hapus Data');
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarning_viSatuan('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                    else
                                    {
                                        ShowPesanError_viSatuan('Data tidak berhasil dihapus '  + cst.pesan ,'Hapus Data');
                                    }
                                }
                            }
                        )
                    }
                }
            }
        )
    //}
}
// End Function datadelete_viSatuan # --------------

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*   Function : dataGrid_viSatuan
*   
*   Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viSatuan(mod_id)
{   
    // Field kiriman dari Project Net.
    var FieldMaster =
    [
        'KD_SATUAN', 'SATUAN'
    ];
    // Deklarasi penamaan yang akan digunakan pada Grid
    dataSourceGrid_viSatuan = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
    DataRefresh_viSatuan(getCriteriaFilterGridDataView_viSatuan);
    // Grid Setup Satuan
    var grData_viSatuan = new Ext.grid.EditorGridPanel
    (
    {
        xtype: 'editorgrid',
        title: '',
        store: dataSourceGrid_viSatuan,
        autoScroll: true,
        columnLines: true,
        border:false,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
        (
        {
            singleSelect: true,
            listeners:
            {
                rowselect: function(sm, row, rec)
                {
                    rowSelectedGridDataView_viSatuan = undefined;
                    rowSelectedGridDataView_viSatuan = dataSourceGrid_viSatuan.getAt(row);
                }
            }
        }
        ),
        listeners:
        {
            // Function saat ada event double klik maka akan muncul form view
            rowdblclick: function (sm, ridx, cidx)
            {
                rowSelectedGridDataView_viSatuan = dataSourceGrid_viSatuan.getAt(ridx);
                if (rowSelectedGridDataView_viSatuan != undefined)
                {
                    setLookUpGridDataView_viSatuan(rowSelectedGridDataView_viSatuan.data);
                }
                else
                {
                    setLookUpGridDataView_viSatuan();
                }
            }
        },
        /**
        *   Mengatur tampilan pada Grid Setup Satuan
        *   Terdiri dari : Judul, Isi dan Event
        *   Isi pada Grid di dapat dari pemangilan dari Net.
        *   dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSourceGrid_viSatuan
        *   didapat dari Function DataRefresh_viSatuan()
        */
        colModel: new Ext.grid.ColumnModel
        (
            [
                new Ext.grid.RowNumberer(), 
                {
                    header: 'Kd. Satuan',
                    dataIndex: 'KD_SATUAN',
                    id: 'ColGridDataView_KD_SATUAN_viSatuan',
                    sortable: true,
                    width: 100,
                    filter:
                    {
                        type: 'int'
                    }
                },
                //-------------- ## --------------
                {
                    header: 'Satuan',
                    dataIndex: 'SATUAN',
                    id: 'ColGridDataView_SATUAN_viSatuan',
                    sortable: true,
                    width: 250,
                    filter:
                    {
                        type: 'int'
                    }
                }
                //-------------- ## --------------
            ]
            ),
        tbar: {
            xtype: 'toolbar',
            id: 'ToolbarGridDataView_viSatuan',
            items: 
            [
                {
                    xtype: 'button',
                    text: 'Edit Data',
                    iconCls: 'Edit_Tr',
                    tooltip: 'Edit Data',
                    id: 'BtnEditGridDataView_viSatuan',
                    handler: function(sm, row, rec)
                    {
                        if (rowSelectedGridDataView_viSatuan != undefined)
                        {
                            setLookUpGridDataView_viSatuan(rowSelectedGridDataView_viSatuan.data)
                        }
                        else
                        {
                            setLookUpGridDataView_viSatuan();
                        }
                    }
                }
                //-------------- ## --------------
            ]
        },
        // End Tolbar ke Dua # --------------
        // Button Bar Pagging # --------------
        // Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
        bbar : bbar_paging(mod_name_viSatuan, selectCount_viSatuan, dataSourceGrid_viSatuan),
        // End Button Bar Pagging # --------------
        viewConfig: {
            forceFit: false
        }
    }
    )
    
    // Kriteria filter pada Grid
    var FrmTabs_viSatuan = new Ext.Panel
    (
    {
        id: mod_id,
        title: NamaForm_viSatuan,
        region: 'center',
        closable: true,
        border: false,
        layout: 'form',
        iconCls: icons_viSatuan,
        margins: '0 5 5 0',
        items: [grData_viSatuan],
        tbar:
        [ 
            { 
                xtype: 'tbtext', 
                text: 'Kd. Satuan', 
                cls: 'left-label', 
                width: 65 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_KD_SATUAN_viSatuan',
                width: 200,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viSatuan = 1;
                            DataRefresh_viSatuan(getCriteriaFilterGridDataView_viSatuan());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbspacer',
                width: 15,
                height: 25
            },
            //-------------- ## --------------
            { 
                xtype: 'tbtext', 
                text: 'Satuan', 
                cls: 'left-label', 
                width: 65 
            },
            {
                xtype: 'textfield',
                id: 'TxtFilterGridDataView_SATUAN_viSatuan',
                width: 200,
                listeners:
                { 
                    'specialkey' : function()
                    {
                        if (Ext.EventObject.getKey() === 13) 
                        {
                            DfltFilterBtn_viSatuan = 1;
                            DataRefresh_viSatuan(getCriteriaFilterGridDataView_viSatuan());                               
                        }                       
                    }
                }
            },
            //-------------- ## --------------
            { 
                xtype: 'tbfill' 
            },
            //-------------- ## --------------
            {
                xtype: 'button',
                id: 'btnRefreshFilter_viSatuan',
                iconCls: 'refresh',
                handler: function()
                {
                    DataRefresh_viSatuan(getCriteriaFilterGridDataView_viSatuan());
                }
            }
            //-------------- ## --------------
        ]
        
    }
    )
    DataRefresh_viSatuan();
    return FrmTabs_viSatuan;
}

/**
*   Function : setLookUpGridDataView_viSatuan
*   
*   Sebuah fungsi untuk menampilkan Form Edit saat ada event klik Edit Data atau Alt+F3
*/
function setLookUpGridDataView_viSatuan(rowdata)
{
    var lebar = 310; // Lebar Form saat PopUp
    setLookUps_viSatuan = new Ext.Window
    (
    {
        id: 'SetLookUps_viSatuan',
        title: NamaForm_viSatuan,
        closeAction: 'destroy',
        autoScroll:true,
        width: 300,
        height: 145,
        resizable:false,
        border: false,
        plain: true,
        layout: 'fit',
        iconCls: icons_viSatuan,
        modal: true,
        items: getFormItemEntry_viSatuan(lebar,rowdata),
        listeners:
        {
            activate: function()
            {
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelectedGridDataView_viSatuan=undefined;
                DataRefresh_viSatuan(getCriteriaFilterGridDataView_viSatuan);
            }
        }
    }
    );

    setLookUps_viSatuan.show();
    if (rowdata == undefined)
    {
        dataaddnew_viSatuan();
    }
    else
    {
        datainit_viSatuan(rowdata)
    }
}

/**
*   Function : getFormItemEntry_viSatuan
*   
*   Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*   Di pangil dari Function setLookUpGridDataView_viSatuan
*/
function getFormItemEntry_viSatuan(lebar,rowdata)
{
    var pnlFormDataBasic_viSatuan = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            fileUpload: true,
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            labelWidth: 65,
            // Tombol pada tollbar Edit Setup Satuan
            tbar: {
                xtype: 'toolbar',
                items: 
                [
                    {
                        xtype: 'button',
                        text: 'Add',
                        id: 'btnTambahWindowPopup_viSatuan',
                        iconCls: 'add',
                        handler: function()
                        {
                            dataaddnew_viSatuan();
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save',
                        id: 'btnSimpanWindowPopup_viSatuan',
                        iconCls: 'save',
                        handler: function()
                        {
                            datasave_viSatuan(false);
                            DataRefresh_viSatuan(getCriteriaFilterGridDataView_viSatuan);
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Save & Close',
                        id: 'btnSimpanKeluarWindowPopup_viSatuan',
                        iconCls: 'saveexit',
                        handler: function()
                        {
                            var x = datasave_viSatuan(true);
                            DataRefresh_viSatuan();
                            if (x===undefined)
                            {
                                setLookUps_viSatuan.close();
                            }
                        }
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'tbseparator'
                    },
                    //-------------- ## --------------
                    {
                        xtype: 'button',
                        text: 'Delete',
                        id: 'btnHapusWindowPopup_viSatuan',
                        iconCls: 'remove',
                        handler: function()
                        {
                            datadelete_viSatuan();
                            DataRefresh_viSatuan();
                        }
                    }
                    //-------------- ## --------------
                ]
            },
            //-------------- #items# --------------
            items:
            [
                // Isian Pada Edit Setup Satuan
                {
                    xtype: 'fieldset',
                    title: '',
                    width: 270,
                    height: 70,
                    items: 
                    [                                   
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kd. Satuan',
                            id: 'TxtWindowPopup_KD_SATUAN_viSatuan',
                            name: 'TxtWindowPopup_KD_SATUAN_viSatuan',
                            emptyText: 'Kode Satuan',
                            readOnly: true,
                            width: 100, 
                            flex: 1,                
                        },
                        //-------------- ## --------------  
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Satuan',
                            id: 'TxtWindowPopup_SATUAN_viSatuan',
                            name: 'TxtWindowPopup_SATUAN_viSatuan',
                            emptyText: 'Nama Satuan',
                            width: 177, 
                            flex: 1,                
                        }
                        //-------------- ## -------------- 
                    ]       
                }
            ],
            //-------------- #items# --------------
        }
    )
    return pnlFormDataBasic_viSatuan;
}

// --------------------------------------- # End Form # ---------------------------------------

// End Project Setup Satuan # --------------