﻿
var dsSetVendorList;
var AddNewSetVendor;
var selectCountSD=50;
var rowSelectedSetVendor;
var SetVendorLookUps;
CurrentPage.page = getPanelSetVendor(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetVendor(mod_id) 
{
    var Field = [
		'Vendor_Code',
		'Vendor',
		'Contact',
		'Address',
		'City',
		'State',
		'Zip',
		'Country',
		'Phone1',
		'Phone2',
		'Fax',
		'Due_Day',
		'Account'
	];
    dsSetVendorList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetVendor();

    var grListSetVendor = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetVendor',
		    stripeRows: true,
		    store: dsSetVendorList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetVendor=undefined;
							rowSelectedSetVendor = dsSetVendorList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'Kd_SetVendor',
                        header: "Code",                      
                        dataIndex: 'Vendor_Code',
                        sortable: true,
                        width: 30
                    },
					{
					    id: 'Vendor',
					    header: "Vendor ",					   
					    dataIndex: 'Vendor',
					    width: 200,
					    sortable: true
					}
                ]
			),
		    tbar:
			[
				{
				    id: 'btnEditSetVendor',
				    text: '  Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetVendor !== undefined)
						{
						    SetVendorLookUp(rowSelectedSetVendor.data);
						}
						else
						{
						    SetVendorLookUp();
						}
				    }
				},' ','-'
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsSetVendorList,
					pageSize: 50,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
		    viewConfig: { forceFit: true }
		}
	);
    //END var grListSetVendor 


    var FormSetVendor = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Vendor',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'Vendor',
		    items: [grListSetVendor],
		    tbar:
			[
				'Code : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Code : ',
					id: 'txtKDSetVendorFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				'Vendor : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Vendor : ',
					id: 'txtVendorFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				'Maks.Data : ', ' ',mComboMaksDataSD(),
				' ','->',
				{
				    id: 'btnRefreshSetVendor',				    
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetVendorFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetVendor();
				}
			}
		}
	);
    //END var FormSetVendor--------------------------------------------------

	RefreshDataSetVendor();
    return FormSetVendor
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetVendorLookUp(rowdata) 
{
	var lebar=600;
    SetVendorLookUps = new Ext.Window   	
    (
		{
		    id: 'SetVendorLookUps',
		    title: 'Vendor Information',
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 370,//320,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'form',
		    iconCls: 'Vendor',
		    modal: true,
		    items: getFormEntrySetVendor(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetVendor=undefined;
					RefreshDataSetVendor();
				}
            }
		}
	);
    //END var SetVendorLookUps------------------------------------------------------------------

    SetVendorLookUps.show();
	if (rowdata === undefined)
	{
		SetVendorAddNew();
	}
	else
	{
		SetVendorInit(rowdata)
	}	
};

//  END FUNCTION SetVendorLookUp
///------------------------------------------------------------------------------------------------------------///




function getFormEntrySetVendor(lebar) 
{
    var pnlSetVendor = new Ext.FormPanel
    (
		{
		    id: 'PanelSetVendor',
		    fileUpload: true,
		    iconCls: 'SetupSetVendor',
		    border: true,
		    items:
			[
				{
					xtype: 'fieldset',
					layout: 'form',
					style: 'margin:5px 5px 5px 5px',
					items:
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Code',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtKdVendorSetVendor',
									width: 120,
									readOnly: true
								}
								
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Name',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtNmVendorSetVendor',
									flex: 1
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Contact',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtContactVendorSetVendor',
									flex: 1
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Address',
							items:
							[
								{
									xtype: 'textarea',
									id: 'txtaAddressVendorSetVendor',
									flex: 1,
									height: 40,
									maxLength: 100
								}								
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'City',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtCityVendorSetVendor',
									flex: 1,
									width: 100
								},
								{
									xtype: 'label',
									forId: 'txtStateVendorSetVendor',
									text: 'State: ',
									style: 'margin: 3px 30px 0 10px',
									width: 50//40
								},
								{
									xtype: 'textfield',
									id: 'txtStateVendorSetVendor',
									flex: 1
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Zip Code',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtZipVendorSetVendor',
									width: 70,
									maxLength: 7
								},
								{
									xtype: 'label',
									forId: 'txtCountryVendorSetVendor',
									text: 'Country: ',
									style: 'margin: 3px 30px 0 10px',
									width: 80
								},
								{
									xtype: 'textfield',
									id: 'txtCountryVendorSetVendor',
									flex: 1
								}
							]
						},						
						{
							xtype: 'compositefield',
							fieldLabel: 'Phone 1',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtPhone1VendorSetVendor',
									flex: 1
								},
								{
									xtype: 'label',
									forId: 'txtPhone2VendorSetVendor',
									text: 'Phone 2: ',
									style: 'margin: 3px 30px 0 10px',
									width: 80
								},
								{
									xtype: 'textfield',
									id: 'txtPhone2VendorSetVendor',
									flex: 1
								}
							]
						},						
						{
							xtype: 'compositefield',
							fieldLabel: 'Kota',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtCityVendorSetVendor',
									flex: 1
								},
								{
									xtype: 'label',
									forId: 'txtStateVendorSetVendor',
									text: 'Provinsi: ',
									style: 'margin: 3px 30px 0 10px',
									width: 80
								},
								{
									xtype: 'textfield',
									id: 'txtStateVendorSetVendor',
									flex: 1
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Fax',
							items:
							[
								{
									xtype: 'textfield',
									id: 'txtFaxVendorSetVendor',
									flex: 1,
									width: 180
								},
								{
									xtype: 'label',
									forId: 'txtFaxVendorSetVendor',
									text: 'Due Day: ',
									style: 'margin: 3px 30px 0 10px',
									width: 80
								},
								{
									xtype: 'textfield',
									id: 'numDueDayVendorSetVendor',
									flex: 1,
									width: 70
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Account',							
							items:
							[								
								{
									xtype: 'textfield',
									id: 'txtVendorAccSetVendor',
									width: 150,									
									enableKeyEvents: true,
									listeners:
									{
										'specialkey': function()
										{
											if(Ext.EventObject.getKey() === 13)
											{
												var criteria = " WHERE Account LIKE '%" + this.getValue() + "' ";
												FormLookupAkunSingle(this.getId(),'',criteria);
											}
										}
									}
								}
							]
						}
					] 
				}				
			],			
		    tbar:
			[
				{
				    id: 'btnAddSetVendor',
				    text: 'Tambah',
				    tooltip: 'Tambah Record Baru ',
				    iconCls: 'add',				   
				    handler: function() { SetVendorAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetVendor',
				    text: 'Simpan',
				    tooltip: 'Rekam Data ',
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetVendorSave(false);
						RefreshDataSetVendor();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetVendor',
				    text: 'Simpan & Keluar',
				    tooltip: 'Simpan dan Keluar',
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetVendorSave(true);
						RefreshDataSetVendor();
						if (x===undefined)
						{
							SetVendorLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetVendor',
				    text: 'Hapus',
				    tooltip: 'Remove the selected item',
				    iconCls: 'remove',
				    handler: function() 
					{
							SetVendorDelete() ;
							RefreshDataSetVendor();					
					}
				},'-','->','-',
				{
					id:'btnPrintSD',
					text: ' Cetak',
					tooltip: 'Cetak',
					iconCls: 'print',					
					handler: function() 
					{
						ShowReport('011108');
					}
				}
			]
		}
	); 

    return pnlSetVendor
};
//END FUNCTION getFormEntrySetVendor
///------------------------------------------------------------------------------------------------------------///


function SetVendorSave(mBol) 
{
	if (ValidasiEntrySetVendor('Simpan Data') == 1 )
	{
		if (AddNewSetVendor == true) 
		{
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/CreateDataObj",
					params: getParamSetVendor(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSD('Data berhasil di simpan','Simpan Data');
							RefreshDataSetVendor();
							if(mBol === false)
							{
								Ext.getCmp('txtKdVendorSetVendor').setValue(cst.kode);
								AddNewSetVendor = false;
							};							
						}
						else if (cst.pesan == 0)
						{
							ShowPesanWarningSD('Data tidak berhasil di simpan, data tersebut sudah ada','Simpan Data');
						}
						else 
						{
							ShowPesanErrorSD('Data tidak berhasil di simpan','Simpan Data');
						}
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: "./Datapool.mvc/UpdateDataObj",
					params: getParamSetVendor(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSD('Data berhasil di edit','Edit Data');
							RefreshDataSetVendor();
							if(mBol === false)
							{
								Ext.getCmp('txtKdVendorSetVendor').setValue(cst.kode);
								AddNewSetVendor = false;
							};
							
						}
						else if (cst.pesan == 0)
						{
							ShowPesanWarningSD('Data tidak berhasil di edit, data tersebut sudah ada','Edit Data');
						}
						else 
						{
							ShowPesanErrorSD('Data tidak berhasil di edit','Edit Data');
						}
					}
				}
			)
		}
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetVendorSave
///---------------------------------------------------------------------------------------///

function SetVendorDelete() 
{
	if (ValidasiEntrySetVendor('Hapus Data') == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: "./Datapool.mvc/DeleteDataObj",
				params: getParamSetVendor(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSD('Data berhasil di hapus','Hapus Data');
						RefreshDataSetVendor();
						SetVendorAddNew();
					}
					else if (cst.pesan == 0)
					{
						ShowPesanWarningSD('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else {
						ShowPesanErrorSD('Data tidak berhasil di hapus','Hapus Data');
					}
				}
			}
		)
	}
};


function ValidasiEntrySetVendor(modul)
{
	var x = 1;

	if(AddNewSetVendor === true)
	{
		if(Ext.getCmp('txtNmVendorSetVendor').getValue() == "")
		{
			ShowPesanWarningSD('Nama Vendor belum di isi',modul);
			x=0;
		}		
	}
	else
	{
		if(Ext.getCmp('txtKdVendorSetVendor').getValue() == "")
		{
			ShowPesanWarningSD('Code Vendor belum di isi',modul);
			x=0;
		}else if(Ext.getCmp('txtNmVendorSetVendor').getValue() == "")
			{
				ShowPesanWarningSD('Nama Vendor belum di isi',modul);
				x=0;
			}
	}
	
	if(Ext.getCmp('txtFaxVendorSetVendor').getValue() != "")
		{
			if(isNaN(Ext.getCmp('txtFaxVendorSetVendor').getValue()) === true)
			{
				ShowPesanWarningSD('No Fax harus numerik.',modul);
				x=0;
			}
		}
		
		if(Ext.getCmp('txtPhone1VendorSetVendor').getValue() != "")
		{
			if(isNaN(Ext.getCmp('txtPhone1VendorSetVendor').getValue()) === true)
			{
				ShowPesanWarningSD('No Telephone harus numerik.',modul);
				x=0;
			}
		}
		
		if(Ext.getCmp('txtPhone2VendorSetVendor').getValue() != "")
		{
			if(isNaN(Ext.getCmp('txtPhone2VendorSetVendor').getValue()) === true)
			{
				ShowPesanWarningSD('No Telephone harus numerik.',modul);
				x=0;
			}
		}
		
		if(Ext.getCmp('txtZipVendorSetVendor').getValue() != "")
		{
			if(isNaN(Ext.getCmp('txtZipVendorSetVendor').getValue()) === true)
			{
				ShowPesanWarningSD('Kode Pos harus numerik.',modul);
				x=0;
			}
		}
		
		if(Ext.getCmp('numDueDayVendorSetVendor').getValue() != "")
		{
			if(isNaN(Ext.getCmp('numDueDayVendorSetVendor').getValue()) === true)
			{
				ShowPesanWarningSD('Due day harus numerik.',modul);
				x=0;
			}
			
		}
		else
		
		{
		ShowPesanWarningSD('Due day harus diisi.',modul);
				x=0;
		}
	return x;
};

function ShowPesanWarningSD(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorSD(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoSD(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

//------------------------------------------------------------------------------------
function SetVendorInit(rowdata) 
{
    AddNewSetVendor = false;
    Ext.getCmp('txtKdVendorSetVendor').setValue(rowdata.Vendor_Code);
	Ext.getCmp('txtNmVendorSetVendor').setValue(rowdata.Vendor);
	Ext.getCmp('txtContactVendorSetVendor').setValue(rowdata.Contact);
	Ext.getCmp('txtFaxVendorSetVendor').setValue(rowdata.Fax);	
	Ext.getCmp('txtPhone1VendorSetVendor').setValue(rowdata.Phone1);
	Ext.getCmp('txtPhone2VendorSetVendor').setValue(rowdata.Phone2);
	Ext.getCmp('txtaAddressVendorSetVendor').setValue(rowdata.Address);	
	Ext.getCmp('txtZipVendorSetVendor').setValue(rowdata.Zip);	
	Ext.getCmp('txtCityVendorSetVendor').setValue(rowdata.City);	
	Ext.getCmp('txtStateVendorSetVendor').setValue(rowdata.State);	
	Ext.getCmp('txtCountryVendorSetVendor').setValue(rowdata.Country);	
	Ext.getCmp('numDueDayVendorSetVendor').setValue(rowdata.Due_Day);
	Ext.getCmp('txtVendorAccSetVendor').setValue(rowdata.Account);
};
///---------------------------------------------------------------------------------------///



function SetVendorAddNew() 
{
    AddNewSetVendor = true;   
	Ext.getCmp('txtKdVendorSetVendor').setValue('');
	Ext.getCmp('txtNmVendorSetVendor').setValue('');
	Ext.getCmp('txtContactVendorSetVendor').setValue('');
	Ext.getCmp('txtFaxVendorSetVendor').setValue('');	
	Ext.getCmp('txtPhone1VendorSetVendor').setValue('');
	Ext.getCmp('txtPhone2VendorSetVendor').setValue('');
	Ext.getCmp('txtaAddressVendorSetVendor').setValue('');	
	Ext.getCmp('txtZipVendorSetVendor').setValue('');	
	Ext.getCmp('txtCityVendorSetVendor').setValue('');	
	Ext.getCmp('txtStateVendorSetVendor').setValue('');	
	Ext.getCmp('txtCountryVendorSetVendor').setValue('');	
	Ext.getCmp('numDueDayVendorSetVendor').setValue('');
	Ext.getCmp('txtVendorAccSetVendor').setValue('');
	rowSelectedSetVendor=undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetVendor() 
{
    var params =
	{	
		Table: 'viVendor',   
	    kd_Vendor: Ext.getCmp('txtKdVendorSetVendor').getValue(),
	    nama: Ext.getCmp('txtNmVendorSetVendor').getValue(),	
	    kontak: Ext.getCmp('txtContactVendorSetVendor').getValue(),	
	    fax: Ext.getCmp('txtFaxVendorSetVendor').getValue(),	
	    phone1: Ext.getCmp('txtPhone1VendorSetVendor').getValue(),	
	    phone2: Ext.getCmp('txtPhone2VendorSetVendor').getValue(),	
	    alamat: Ext.getCmp('txtaAddressVendorSetVendor').getValue(),	
	    kdpos: Ext.getCmp('txtZipVendorSetVendor').getValue(),	
	    kota: Ext.getCmp('txtCityVendorSetVendor').getValue(),	
	    prov: Ext.getCmp('txtStateVendorSetVendor').getValue(),	
	    negara: Ext.getCmp('txtCountryVendorSetVendor').getValue(),	
	    dueday: Ext.getCmp('numDueDayVendorSetVendor').getValue(),
	    akun: Ext.getCmp('txtVendorAccSetVendor').getValue()
	};
    return params
};


function RefreshDataSetVendor()
{	
	dsSetVendorList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSD, 
				Sort: 'Vendor', 
				Sortdir: 'ASC', 
				target:'viVendor',
				param: ''
			} 
		}
	);
	rowSelectedSetVendor = undefined;
	return dsSetVendorList;
};

function RefreshDataSetVendorFilter() 
{   
	var KataKunci;
    if (Ext.getCmp('txtKDSetVendorFilter').getValue() != '')
    { 
		KataKunci = "WHERE Vendor_Code LIKE '%" + Ext.getCmp('txtKDSetVendorFilter').getValue() + "%'"; 
	}
    if (Ext.getCmp('txtVendorFilter').getValue() != '')
    { 
		if (KataKunci == undefined)
		{
			KataKunci = "WHERE Vendor LIKE '%" + Ext.getCmp('txtVendorFilter').getValue() + "%'";
		}
		else
		{
			KataKunci += "AND Vendor LIKE '%" + Ext.getCmp('txtVendorFilter').getValue() + "%'";
		}  
	}
        
    if (KataKunci != undefined) 
    {  
		dsSetVendorList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSD, 
					Sort: 'Vendor', 
					Sortdir: 'ASC', 
					target:'viVendor',
					param: KataKunci
				}			
			}
		);        
    }
	else
	{
		RefreshDataSetVendor();
	}
};



function mComboMaksDataSD()
{
  var cboMaksDataSD = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSD,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSD=b.data.displayText ;
					RefreshDataSetVendor();
				} 
			}
		}
	);
	return cboMaksDataSD;
};
 



