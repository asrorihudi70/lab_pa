
var now = new Date();
var dsSpesialisasiAccount;
var ListHasilAccount;
var rowSelectedHasilAccount;
var dsKelasAccount;
var dsKamarAccount;
var dataSource_Account;
var dataSource_detAccount;
var dsPerawatAccount;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpkd_perawat;
var rowSelected_viAcc;
var selectGroup;
var selectLevel;
var radios;
var mod_name='Account';
var selectCountPagu=50;
var selectlevelfilter =undefined;
var selectgrupfilter =undefined
CurrentPage.page = getPanelAccount(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelAccount(mod_id) {
    var Field = ['ACCOUNT', 'NAME', 'TYPE', 'LEVELS', 'PARENT', 'GROUPS', 'SHOWFIELD'];

    dataSource_Account = new WebApp.DataStore({
        fields: Field
    });

    
    var gridListHasilAccount = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_Account,
        // anchor: '100% 73%',
        columnLines: false,
        autoScroll: true,
        border: true,
        sort: false,
        // autoHeight: true,
        layout: 'fit',
        region: 'center',
        height: 280,
		plugins: [new Ext.ux.grid.FilterRow()],
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {

                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viAcc = dataSource_Account.getAt(ridx);
                if (rowSelected_viAcc !== undefined)
                {
                    Ext.getCmp('TxtNumberAccount').setValue(rowSelected_viAcc.data.ACCOUNT);
                    Ext.getCmp('TxtNamaAccount').setValue(rowSelected_viAcc.data.NAME);
                    if (rowSelected_viAcc.data.TYPE === 'G')
                    {
                        radios.items.items[0].setValue(true);
                    } else
                    {
                        radios.items.items[1].setValue(true);
                    }
                    Ext.getCmp('cboLevel').setValue(rowSelected_viAcc.data.LEVELS);
                    Ext.getCmp('TxtParentAccount').setValue(rowSelected_viAcc.data.PARENT);
                    Ext.getCmp('cboGroup').setValue(rowSelected_viAcc.data.GROUPS);
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilAccount',
                        header: 'Number',
                        dataIndex: 'ACCOUNT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75,
						filter: {}
                    },
                    {
                        id: 'colMedrecViewHasilAccount',
                        header: 'Name',
                        dataIndex: 'NAME',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200,
						filter: {}
                    },
                    {
                        id: 'colNamaViewHasilAccount',
                        header: 'Type',
                        dataIndex: 'TYPE',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25,
						filter: {}
                    },
                    {
                        id: 'colAlamatViewHasilAccount',
                        header: 'Level',
                        dataIndex: 'LEVELS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25,
						filter: {}
                    },
                    {
                        id: 'colSpesialisasiViewHasilAccount',
                        header: 'Parent',
                        dataIndex: 'PARENT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50,
						filter: {}
                    },
                    {
                        id: 'colKelasViewHasilAccount',
                        header: 'Group',
                        dataIndex: 'GROUPS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 30,
						filter: {}
                    },
                    {
                        id: 'colKamarViewHasilAccount',
                        header: 'ShowField',
                        dataIndex: 'SHOWFIELD',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50,
						filter: {}
                    }
                ]
                ),
				bbar : bbar_paging(mod_name, selectCountPagu, dataSource_Account),
        viewConfig: {
            forceFit: true
        }
    });
	 var PanelGridAkun = new Ext.Panel
	(
		{
			id: 'PanelGridAkun',
			closable:true,
			region: 'center',
			layout: 'form',
			title: 'Pencarian',
			margins:'0 5 5 0',
			bodyStyle: 'padding:15px',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			//iconCls: 'Pagu',
			items: [gridListHasilAccount],
			tbar: 
			[
				//'Unit/Sub '+gstrSatker+' : ', ' ',mComboUnitKerjaPagu(),
				'Group '+' : ', ' ',mComboGroupAkunFilter(),' ','-',
				'Level : ', ' ',mComboLevelFilter(),' ','-',
				{ xtype: 'tbtext', text: 'Number : ', cls: 'left-label', width: 50 },
				{
					xtype: 'textfield',
					id: 'txtFilterNumber',
					width: 150,
					listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									load_Account();
								} 						
							}
						}
				},' ','-',
				{ xtype: 'tbtext', text: 'Name : ', cls: 'left-label', width: 50 },
				{
					xtype: 'textfield',
					id: 'txtFilterName',
					width: 150,
					listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									load_Account();
								} 						
							}
						}
				},
				' ','->', 
				{
					xtype: 'button',
					id:'btnRefreshPagu',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					handler: function(sm, row, rec) 
					{
						// RefreshDataPaguFilter();
						load_Account("");
					}
				}
			]
		}
	);
    var FormDepanAccount = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Account',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
		
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
			PanelGridAkun,
            // gridListHasilAccount
        ],
        tbar: {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'New',
                    iconCls: 'AddRow',
                    id: 'btnNew_Acc',
                    handler: function ()
                    {
                        cleardataaccount();
                    }
                },
                {
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'save',
                    id: 'btnSimpan_Acc',
                    handler: function ()
                    {
                        SavingDataAccount();
                    }
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    iconCls: 'remove',
                    id: 'btnHapus_Acc',
                    handler: function ()
                    {
                        if (Ext.getCmp('TxtNumberAccount').getValue() === "") {
                            ShowPesanWarningAccount('Silahkan Pilih Account yang akan di Hapus','Warning')
                        }else{
                            Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                if (button === 'yes') {
                                    loadMask.show();
                                    DeleteData();
                                }
                            });
                        }                        
                    }
                }
            ]
        },
        listeners: {
            'afterrender': function () {

            }
        }
    });
	load_Account("");
    return FormDepanAccount;
}
;

function mComboGroupAkunFilter(){
	
    var comboGroupAkunFIlter = new Ext.form.ComboBox
	(
		{
		    id: 'comboGroupAkunFIlter',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    // emptyText: 'Pilih '+gstrSatker+'...',
		    // emptyText: '',
		    // fieldLabel: gstrSatker,
		    fieldLabel: 'gstrSatker',
		    align: 'right',
		    width: 80,
		    // store: dsUGroupAkunFilter,
			store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Asset'], [2, 'Liability'], [3, 'Capital'], [4, 'Revenue'], [5, 'Expense']]
                                        }
                                ),
		    valueField: 'Id',
		    displayField: 'displayText',
			value:' ',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					
			        selectgrupfilter = b.data.Id;
					load_Account();
			    }
			}
		}
	);


	
    return comboGroupAkunFIlter;
}

function mComboLevelFilter()
{
    var comboGroupLevelFilter = new Ext.form.ComboBox
	(
		{
		    id: 'comboGroupLevelFilter',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    // emptyText: 'Pilih '+gstrSatker+'...',
		    // emptyText: '',
		    // fieldLabel: gstrSatker,
		    fieldLabel: 'gstrSatker',
		    align: 'right',
		    width: 50,
		    // store: dsUGroupAkunFilter,
			store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, '1'], [2, '2'], [3, '3'], [4, '4'], [5, '5'], [6, '6'], [7, '7'], [8, '8'], [9, '9']]
                                        }
                                ),
		    valueField: 'Id',
		    displayField: 'displayText',
			value:' ',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectlevelfilter = b.data.Id;
					load_Account();
			    }
			}
		}
	);
	
	return comboGroupLevelFilter;
};
function getPanelPencarianPasien() {
    radios = new Ext.form.RadioGroup({
        x: 120,
        y: 70,
        xtype: 'radiogroup',
        id: 'rgtype',
        boxMaxWidth: 150,
        items: [
            {
                boxLabel: 'General',
                name: 'rgtypeaccount',
                inputValue: '',
                checked: true,
                id: 'rgtypeaccountgeneral'
            },
            {
                boxLabel: 'Detail',
                name: 'rgtypeaccount',
                inputValue: '',
                id: 'rgtypeaccountdetail'
            }
        ],
        listeners: {
            change: function (field, newValue, oldValue) {
                tmpediting = 'true';
                if (Ext.getCmp('rgtypeaccountgeneral').checked === true)
                {
                    TypeProgram = 'G';
                } else {
                    TypeProgram = 'D';
                }
            }
        }
    
    });
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 140,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Number '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtNumberAccount',
                        id: 'TxtNumberAccount',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            load_Account();
                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Name '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtNamaAccount',
                        id: 'TxtNamaAccount',
                        width: 340,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            // load_Account("lower(name) like lower(~%"+Ext.getCmp('TxtNamaAccount').getValue()+"%~)");
											load_Account
										}
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Type '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    radios,
                    {
                        x: 293,
                        y: 70,
                        xtype: 'label',
                        text: 'Level '
                    },
                    {
                        x: 340,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboLevel(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Parent '
                    }, {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 100,
                        xtype: 'textfield',
                        name: 'TxtParentAccount',
                        id: 'TxtParentAccount',
                        width: 150,
                    },
                    {
                        x: 293,
                        y: 100,
                        xtype: 'label',
                        text: 'Group '
                    },
                    {
                        x: 340,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboGroup(),
                ]
            }
        ]
    };
    return items;
}
;

//combo dan load database

function SavingDataAccount()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/master_data/account/save",
                        params: paramsavingAccount(),
                        failure: function (o)
                        {
                            ShowPesanWarningAccount('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            load_Account();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoAccount('Proses Saving Berhasil', 'Save');
                                load_Account();
                            }
                            else
                            {
                                ShowPesanWarningAccount('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                load_Account();
                            }
                            ;
                        }
                    }
            );
}
;
function paramsavingAccount()
{
    var tmpTYPE = '';
    if (radios.items.items[0].getValue() == true) {
        tmpTYPE = 'G';
    }else{
        tmpTYPE = 'D';
    }
    var params =
            {                
                ACCOUNT  : Ext.getCmp('TxtNumberAccount').getValue(),
                NAME     : Ext.getCmp('TxtNamaAccount').getValue(),
                TYPE     : tmpTYPE,
                LEVELS   : Ext.getCmp('cboLevel').getValue(),
                PARENT   : Ext.getCmp('TxtParentAccount').getValue(),
                GROUPS   : Ext.getCmp('cboGroup').getValue()
            };
    return params;
}

function load_Account()
{
	var criteria = GetCriteriaGridUtama();
    dataSource_Account.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountPagu,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAccount',
                                    param: criteria
                                }
                    }
            );
    return dataSource_Account;
}

function GetCriteriaGridUtama(){
	var criteria = '';
	console.log(selectgrupfilter);
	console.log(selectlevelfilter);
	console.log(Ext.getCmp('txtFilterNumber').getValue());
	console.log(Ext.getCmp('txtFilterName').getValue());
	if (selectgrupfilter == undefined
			&& selectlevelfilter == undefined 
			&& (Ext.getCmp('txtFilterNumber').getValue() == ''|| Ext.getCmp('txtFilterNumber').getValue() == undefined )
			&& (Ext.getCmp('txtFilterName').getValue() == '' || Ext.getCmp('txtFilterName').getValue() == undefined )){
		criteria = " ";
	}else  {
		criteria = " where ";
	}
	
	var hitung_param =0;

	if (selectgrupfilter != undefined){
		criteria += " groups = '" + Ext.getCmp('comboGroupAkunFIlter').getValue() +  "' ";
		hitung_param++;
	}
	
	if (hitung_param > 0){
		if ( selectlevelfilter !== undefined){
			criteria += " and levels = '" + Ext.getCmp('comboGroupLevelFilter').getValue() + " '  ";
			hitung_param++;
		}
	}else{
		if (selectlevelfilter !== undefined){
			criteria += " levels = '" + Ext.getCmp('comboGroupLevelFilter').getValue() + " '  ";
			hitung_param++;
		}
	}
	
	
	if (hitung_param > 0){
		if ( (Ext.getCmp('txtFilterNumber').getValue() != undefined ) && (Ext.getCmp('txtFilterNumber').getValue() != '' )){
			criteria += " and account like '" + Ext.getCmp('txtFilterNumber').getValue() + "%'  ";
			hitung_param++;
		}
	}else{
		if ( (Ext.getCmp('txtFilterNumber').getValue() != undefined ) && (Ext.getCmp('txtFilterNumber').getValue() != '' )){
			criteria += "  account like '" + Ext.getCmp('txtFilterNumber').getValue() + "%'  ";
			hitung_param++;
		}
	}
	
	if (hitung_param > 0){
		if ((Ext.getCmp('txtFilterName').getValue() != undefined ) && (Ext.getCmp('txtFilterName').getValue() != '' ) ){
			criteria += " and upper(name) like upper ('" + Ext.getCmp('txtFilterName').getValue() + "%' ) ";
			hitung_param++;
		}
	}else{
		if ((Ext.getCmp('txtFilterName').getValue() != undefined ) && (Ext.getCmp('txtFilterName').getValue() != '' )  ){
			criteria += " upper(name) like upper ('" + Ext.getCmp('txtFilterName').getValue() + "%' ) ";
			hitung_param++;
		}
	}
		
	return criteria;
}

function ShowPesanWarningAccount(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoAccount(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                         url: baseURL + "index.php/master_data/account/delete",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningAccount('Hubungi Admin', 'Error');
                            load_Account("");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoAccount('Data berhasil di hapus', 'Information');
                                load_Account("");
                                cleardataaccount();
                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningAccount('Gagal menghapus data', 'Error');
                                load_Account("");
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                ACCOUNT  : Ext.getCmp('TxtNumberAccount').getValue()
            };
    return params;
}

function mComboGroup()
{
    var cboGroup = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 100,
                        id: 'cboGroup',
                        typeAhead: true,
                        editable: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        tabIndex: 5,
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Group',
                        fieldLabel: 'Group',
                        width: 100,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Asset'], [2, 'Liability'], [3, 'Capital'], [4, 'Revenue'], [5, 'Expense']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectGroup,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectGroup = b.data.Id;
                                    }
                                }
                    }
            );
    return cboGroup;
}
;

function mComboLevel()
{
    var cboLevel = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 70,
                        id: 'cboLevel',
                        editable: false,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        tabIndex: 5,
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: '',
                        width: 50,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, '1'], [2, '2'], [3, '3'], [4, '4'], [5, '5'], [6, '6'], [7, '7'], [8, '8'], [9, '9']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectLevel,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectLevel = b.data.Id;
                                    }
                                }
                    }
            );
    return cboLevel;
}
;

function cleardataaccount(){
    Ext.getCmp('TxtNumberAccount').setValue('');
    Ext.getCmp('TxtNamaAccount').setValue('');
    Ext.getCmp('cboLevel').setValue('');
    Ext.getCmp('TxtParentAccount').setValue('');
    Ext.getCmp('cboGroup').setValue('');
    Ext.getCmp('rgtype').setValue("rgtypeaccountgeneral", true);
}