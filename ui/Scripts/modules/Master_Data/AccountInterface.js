
var now = new Date();
var dsSpesialisasiAccountInterface;
var ListHasilAccountInterface;
var rowSelectedHasilAccountInterface;
var dsKelasAccountInterface;
var dsKamarAccountInterface;
var dataSource_AccountInterface;
var dataSource_detAccountInterface;
var dsPerawatAccountInterface;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpkd_perawat;
var rowSelected_viAcc_In;
var selectGroup;
var selectLevel;
var radios;
var dataSource_Data_interface;
var sBase = location.href.substr(0, location.href.lastIndexOf("/") + 1);
var IdRootKelompokBarang_pInvMasterBarang='1000000000';
var Account_Acc_interface_master_data;
var Produk_Acc_interface_master_data;
var Unit_Acc_interface_master_data;
var Account_Acc_interface_master_data_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','account','name'],data: []});
var Produk_Acc_interface_master_data_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','kode','name','kd_produk','deskripsi'],data: []});

CurrentPage.page = getPanelAccountInterface(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelAccountInterface(mod_id) {
	var Field = ['ACCOUNT', 'KODEPRODUK', 'NAMAPRODUK', 'UNIT', 'KDUNIT'];

    dataSource_AccountInterface = new WebApp.DataStore({
        fields: Field
    });

    load_AccountInterface("");
    var gridListHasilAccountInterface = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_AccountInterface,
        anchor: '100% 86%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 250,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {

                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viAcc = dataSource_Account.getAt(ridx);
                if (rowSelected_viAcc !== undefined)
                {
                    Ext.getCmp('TxtNumberAccount').setValue(rowSelected_viAcc.data.ACCOUNT);
                    Ext.getCmp('TxtNamaAccount').setValue(rowSelected_viAcc.data.NAME);
                    if (rowSelected_viAcc.data.TYPE === 'G')
                    {
                        radios.items.items[0].setValue(true);
                    } else
                    {
                        radios.items.items[1].setValue(true);
                    }
                    Ext.getCmp('cboLevel').setValue(rowSelected_viAcc.data.LEVELS);
                    Ext.getCmp('TxtParentAccount').setValue(rowSelected_viAcc.data.PARENT);
                    Ext.getCmp('cboGroup').setValue(rowSelected_viAcc.data.GROUPS);
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilAccount',
                        header: 'Account',
                        dataIndex: 'ACCOUNT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colMedrecViewHasilAccount',
                        header: 'Kode Unit',
                        dataIndex: 'KODEPRODUK',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colNamaViewHasilAccount',
                        header: 'Nama Unit',
                        dataIndex: 'NAMAPRODUK',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar : 
        	{
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    iconCls: 'add',
                    id: 'btnTambah_AI',
                    handler: function ()
                    {
                        HasilAccountInterfaceLookUp()
                    }
                },
                {
                    xtype: 'button',
                    text: 'Cek Produk belum Terdaftar',
                    iconCls: 'find',
                    id: 'btnCekProdukBlmTerdaftar_AI',
                    handler: function ()
                    {
                        LookUPDataBelumTerdaftar();
                        loadDataBelumProdukTerpakai("","","","");
                    }
                },
                {
                    xtype: 'button',
                    text: 'Produk > 1 Account',
                    iconCls: 'find',
                    id: 'btnCekProduklebihdari1_AI',
                    handler: function ()
                    {
                        LookUPLebih_Dari_1_acc_interface();
                        loadDataLebihDari1("","","","","");
                    }
                },
                {
                    xtype: 'button',
                    text: 'Account Yang Belum Terdaftar',
                    iconCls: 'find',
                    id: 'btnCekAccountBelumTerdaftar',
                    handler: function ()
                    {
                        LookUPACC_Belum_terdaftar_acc_interface();
                        loadACC_Belum_terdaftarDari1("","","","","");
                    }
                }
            ]
        },
    });

    var FormDepanAccountInterface = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Account Interface',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianAccountInterface(),
            gridListHasilAccountInterface
        ],
        listeners: {
            'afterrender': function () {
            }
        }
    });
    return FormDepanAccountInterface;
}
;
function getPanelPencarianAccountInterface() {
    var items = {
        layout: 'column',
        border: true,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 70,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Account '
                    },
                    {
                        x: 130,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 140,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtAccount_AccountInterface',
                        id: 'TxtAccount_AccountInterface',
                        width: 200,
                        listeners:
                                {

                                	'render': function(c) {
                                                  c.getEl().on('keyup', function() {
                                                    if (Ext.getCmp('TxtProduk_AccountInterface').getValue() != "") {
                                                		params = "WHERE a.Account like '%"+ Ext.getCmp('TxtAccount_AccountInterface').getValue() +"%' AND (cast(a.Kd_Produk as text) like '%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%' or lower(p.deskripsi) like lower('%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%'))";
                                                	}else{
                                                		params = "WHERE a.Account like '%"+ Ext.getCmp('TxtAccount_AccountInterface').getValue() +"%'";
                                                	}
                                                	
                                                    load_AccountInterface(params);
                                                  }, c);
                                    },
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            if (Ext.getCmp('TxtProduk_AccountInterface').getValue() != "") {
                                        		params = "WHERE a.Account like '%"+ Ext.getCmp('TxtAccount_AccountInterface').getValue() +"%' AND (cast(a.Kd_Produk as text) like '%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%' or lower(p.deskripsi) like lower('%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%'))";
                                        	}else{
                                        		params = "WHERE a.Account like '%"+ Ext.getCmp('TxtAccount_AccountInterface').getValue() +"%'";
                                        	}
                                        	
                                            load_AccountInterface(params);
                                        }

                                    }
                                }
                    },
                    {

                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Produk '
                    },
                    {
                        x: 130,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 140,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtProduk_AccountInterface',
                        id: 'TxtProduk_AccountInterface',
                        width: 200,
                        listeners:
                                {
                                	'render': function(c) {
                                                  c.getEl().on('keyup', function() {
                                                    if (Ext.getCmp('TxtAccount_AccountInterface').getValue() != "") {
                                        				params = "WHERE a.Account like '%"+ Ext.getCmp('TxtAccount_AccountInterface').getValue() +"%' AND (cast(a.Kd_Produk as text) like '%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%' or lower(p.deskripsi) like lower('%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%'))";
		                                        	}else{
		                                        		params = "WHERE (cast(a.Kd_Produk as text) like '%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%' or lower(p.deskripsi) like lower('%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%'))";
		                                        	}
		                                        	
		                                            load_AccountInterface(params);
                                                  }, c);
                                    },
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                        	if (Ext.getCmp('TxtAccount_AccountInterface').getValue() != "") {
                                        		params = "WHERE a.Account like '%"+ Ext.getCmp('TxtAccount_AccountInterface').getValue() +"%' AND (cast(a.Kd_Produk as text) like '%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%' or lower(p.deskripsi) like lower('%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%'))";
                                        	}else{
                                        		params = "WHERE (cast(a.Kd_Produk as text) like '%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%' or lower(p.deskripsi) like lower('%"+Ext.getCmp('TxtProduk_AccountInterface').getValue()+"%'))";
                                        	}
                                        	
                                            load_AccountInterface(params);
                                        }

                                    }
                                }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function HasilAccountInterfaceLookUp(rowdata) {
    FormLookUpdetailAccountInterface = new Ext.Window({
        id: 'gridHasilAccountInterface',
        title: 'AccountInterface Setup',
        closeAction: 'destroy',
        width: 800,
        height: 600,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupAccountInterface()],
        listeners: {
        }
    });
    FormLookUpdetailAccountInterface.show();
    if (rowdata === undefined) {
    } else {
    }

}
;
function formpopupAccountInterface() {
    
    var FrmTabs_popupAccountInterface = new Ext.Panel
            (
                    {
                        id: 'formpopupAccountInterface',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: false,
                        shadhow: true,
                        // anchor: '100%',
                        height : 600,
                        iconCls: '',
                        items:
                                [
                                // PanelAccountInterface(),
                                PanelInputDataProdukUnit()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_ACC_Interface',
                                                    handler: function ()
                                                    {
                                                        SavingData();
                                                        // console.log(dataSource_Account);
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Delete',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_viDaftar',
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Cetak',
                                                    iconCls: 'print',
                                                    id: 'btnCetak_viDaftar',
                                                    handler: function ()
                                                    {
                                                        var criteria = GetCriteriaAcc_In();
                                                        loadMask.show();
                                                        loadlaporanAskep('0', 'LapObservasiTindakan', criteria, function () {
                                                            loadMask.hide();
                                                        });
                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupAccountInterface;
}
;
function PanelAccountInterface() {
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDataAccountInterface',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height : 250,
                        border: true,
                        autoscroll: false,
                        items: [
                        {
                        	layout: 'column',
							border: false,
							items:
							[
	                            {
	                            	columnWidth:.40,
	                            	bodyStyle: 'padding: 10px 10px 10px 10px',
	                            	border: false,
	                            	height: 300,
	                            	items:
	                            	[
	                            		getItemPanelTreee_Acc_Interface()
	                            	]
	                            },
	                            {
	                            	columnWidth:.60,
	                            	layout: 'absolute',
	                            	bodyStyle: 'padding: 10px 10px 10px 10px',
	                            	border: false,
	                            	height: 300,
	                            	items:
	                            	[
	                            		getItemGridBarang_Acc_Interface()
	                            	]
	                            }
	                        ]
	                    }
                        ]
                    });
    return items;
}
;

function getItemPanelTreee_Acc_Interface() {
    var items =
	{
		layout: 'form',   
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:false,
		items:
		[
			itemsTreeListKelompokBarangGridDataView_Acc_Interface()
		
		]
	};
    return items;
};

function itemsTreeListKelompokBarangGridDataView_Acc_Interface()
{	
		
	treeKlas_kelompokbarang_Acc_Interface= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			border:false,
			height:200,
			width:200,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_Acc_Interface=n.attributes
					if (strTreeCriteriKelompokBarang_Acc_Interface.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_Acc_Interface.leaf == false)
						{
							
						}
						else
						{
                            GetStrTreeSet_Acc_Interface();
							// dataGridLookUpAcc_Interface(n.attributes.id);
       //                      console.log(n.attributes.id)
							// if(n.attributes.id=='1000000000'){
							// 	GetStrTreeSet_Acc_Interface();
							// 	rootTreeMasterBarang_InvMasterBarang = new Ext.tree.AsyncTreeNode
							// 	(
							// 		{
							// 			expanded: true,
							// 			text:'MASTER',
							// 			id:IdRootKelompokBarang_pInvMasterBarang,
							// 			children: StrTreeSetKelompokBarang,
							// 			autoScroll: true
							// 		}
							// 	) 
							// 	treeKlas_kelompokbarang_Acc_Interface.setRootNode(rootTreeMasterBarang_InvMasterBarang);
							// } 
							
						};
					}
					else
					{
						
					};
				},
				
				
			}
		}
	);
	
	rootTreeMasterBarang_InvMasterBarang = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdRootKelompokBarang_pInvMasterBarang,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_Acc_Interface.setRootNode(rootTreeMasterBarang_InvMasterBarang);  
	
    var pnlTreeFormDataWindowPopup_viInvKelompokBarang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_Acc_Interface,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viInvKelompokBarang;
}

function getItemGridBarang_Acc_Interface() 
{
    var items =
	{
	    layout: 'form',   
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 1px 1px 1px',
		border:false,
	    items:
		[
			{
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewBarang_Acc_Interface()
				]	
			}
			//-------------- ## --------------
		],
	};
    return items;
};

function gridDataViewBarang_Acc_Interface()
{
    var FieldGrdBArang_Acc_Interface = ['acc_name','account','name','parent'];
	
    dsDataGrdBarang_Acc_Interface= new WebApp.DataStore
	({
        fields: FieldGrdBArang_Acc_Interface
    });
    
    gridPanelLookUpBarang_InvMasterBarang =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_Acc_Interface,
        height: 220,//220,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataGrdBarang_Acc_Interface.getAt(row).data.kd_inv);
					// InvMasterBarang.vars.KodeInv = dsDataGrdBarang_Acc_Interface.getAt(row).data.kd_inv;
					// InvMasterBarang.vars.NamaSubInv = dsDataGrdBarang_Acc_Interface.getAt(row).data.nama_sub;
					// InvMasterBarang.vars.inv_kd_inv = dsDataGrdBarang_Acc_Interface.getAt(row).data.inv_kd_inv;
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'account',
				header: 'Account',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'account',
				header: 'Account',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'name',
				header: 'Nama Account',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvMasterBarang;
}
var tmpkodeaccount = '';
var tmpkodeproduk = '';
var gridListHasilAccount;
function PanelInputDataProdukUnit() {
	var Field = ['tag','kd_unit', 'nama_unit'];

    dataSource_Account = new WebApp.DataStore({
        fields: Field
    });


    gridListHasilAccount = new Ext.grid.EditorGridPanel({
    	x:0,
    	y:100,
        stripeRows: true,
        store: dataSource_Account,
        anchor: '100% 100%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 700,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                // rowSelected_viAcc = dataSource_Account.getAt(ridx);
                // if (rowSelected_viAcc !== undefined)
                // {
                //     Ext.getCmp('TxtNumberAccount').setValue(rowSelected_viAcc.data.ACCOUNT);
                //     Ext.getCmp('TxtNamaAccount').setValue(rowSelected_viAcc.data.NAME);
                //     if (rowSelected_viAcc.data.TYPE === 'G')
                //     {
                //         radios.items.items[0].setValue(true);
                //     } else
                //     {
                //         radios.items.items[1].setValue(true);
                //     }
                //     Ext.getCmp('cboLevel').setValue(rowSelected_viAcc.data.LEVELS);
                //     Ext.getCmp('TxtParentAccount').setValue(rowSelected_viAcc.data.PARENT);
                //     Ext.getCmp('cboGroup').setValue(rowSelected_viAcc.data.GROUPS);
                // } else
                // {
                //     //HasilAccountLookUp();
                // }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltagPilihData',
                        header: 'Tag',
                        dataIndex: 'tag',
                        width:20,
                        xtype:'checkcolumn',
                        listeners:{
                            'checkchange' : function(column, record, checked){
                                // alert('checked');
                            }
                        }
                    },
                    {
                        id: 'colKodedetailaccount',
                        header: 'Kode',
                        dataIndex: 'kd_unit',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colNamadetailaccount',
                        header: 'nama',
                        dataIndex: 'nama_unit',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        }
    });
    
    Account_Acc_interface_master_data= Nci.form.Combobox.autoComplete({
                x: 110,
                y: 10,
                store   : Account_Acc_interface_master_data_ds,
                keydown:function(text,e){
                            var nav=navigator.platform.match("Mac");
                            if (e.keyCode == 9 || e.keyCode == 13){
                                 // Ext.getCmp('TxtPopuPenerimaPenerimaan_Piutang').focus();
                            }
                },
                select  : function(a,b,c){
                                
                               tmpkodeaccount = b.data.account;
                },
                insert  : function(o){
                    return {
                        account         : o.account,
                        name            : o.name,
                        text            : '<table style="font-size: 11px;"><tr><td width="50">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
                    }
                },
                keypress:function(c,e){
                    if (e.keyCode == 13 || e.keyCode == 9){
                        // Ext.getCmp('TxtPopuPenerimaPenerimaan_Piutang').focus();
                    }
                },
                url: baseURL + "index.php/master_data/viewaccountinterface/accounts",
                valueField: 'name',
                displayField: 'text',
                listWidth: 300,
                width:200,
                enableKeyEvents:true,
                tabIndex: 1
            });
    Produk_Acc_interface_master_data= Nci.form.Combobox.autoComplete({
                x: 110,
                y: 40,
                store   : Produk_Acc_interface_master_data_ds,
                keydown:function(text,e){
                            var nav=navigator.platform.match("Mac");
                            if (e.keyCode == 9 || e.keyCode == 13){
                                 // Ext.getCmp('TxtPopuPenerimaPenerimaan_Piutang').focus();
                            }
                },
                select  : function(a,b,c){
                                
                                tmpkodeproduk = b.data.kd_produk;
                },
                insert  : function(o){
                    return {
                        kode            : o.kode,
                        name            : o.name,
                        kd_produk       : o.kd_produk,
                        deskripsi       : o.deskripsi,
                        text            : '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_produk+'</td><td width="200">'+o.name+'</td><td width="200">'+o.deskripsi+'</td></tr></table>'
                    }
                },
                keypress:function(c,e){
                    if (e.keyCode == 13 || e.keyCode == 9){
                        // Ext.getCmp('TxtPopuPenerimaPenerimaan_Piutang').focus();
                    }
                },
                url: baseURL + "index.php/master_data/viewaccountinterface/produk",
                valueField: 'name',
                displayField: 'text',
                listWidth: 300,
                width:200,
                enableKeyEvents:true,
                tabIndex: 1
            });
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelInputDataProdukUnit',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height : 600,
                        border: true,
                        autoscroll: false,
                        items: [
                        {
                        	layout: 'absolute',
                        	height : 530,
                        	width  : '100%',
							border : false,
							items:
							[
                                {
                                    x: 10,
                                    y: 10,
                                    xtype : 'label',
                                    text  : 'Account'
                                },
                                {
                                    x: 100,
                                    y: 10,
                                    xtype : 'label',
                                    text  : ':'
                                },
                                Account_Acc_interface_master_data,
	                            {
	                            	x: 10,
	                            	y: 40,
	                            	xtype : 'label',
	                            	text  : 'Produk'
	                            },
	                            {
	                            	x: 100,
	                            	y: 40,
	                            	xtype : 'label',
	                            	text  : ':'
	                            },
                                Produk_Acc_interface_master_data,
                                {
	                            	x: 10,
	                            	y: 70,
	                            	xtype : 'label',
	                            	text  : 'Unit'
	                            },
	                            {
	                            	x: 100,
	                            	y: 70,
	                            	xtype : 'label',
	                            	text  : ':'
	                            },
                                mCombodata_unit_ACC_Interface(),
                                gridListHasilAccount
	                        ]
	                    }
                        ]
                    });
    return items;
}
;

//combo dan load database

function dataGridLookUpAcc_Interface(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/master_data/viewaccountinterface/getGridLookUpBarang",
			params: {inv_kd_inv:''},
			failure: function(o)
			{
				ShowPesanErrorInvMasterBarang('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_Acc_Interface.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_Acc_Interface.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_Acc_Interface.add(recs);
					
					
					
					gridPanelLookUpBarang_InvMasterBarang.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvMasterBarang('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function GetStrTreeSet_Acc_Interface()
{
	 Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/master_data/prosesgetaccount/getdata",
			params: 
			{
				// UserID: strUser,
				// ModuleID: 'ProsesGetAccount',
				Params:	"ProsesGetAccount"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};

function datainit_AccountInterface(rowdata)
{
    console.log(rowdata.CUST_CODE)
    Ext.getCmp('TxtPopupCodeAccountInterface').setValue(rowdata.data.CUST_CODE);
    Ext.getCmp('TxtPopupNamaAccountInterface').setValue(rowdata.data.CUSTOMER);
    Ext.getCmp('TxtPopupContactAccountInterface').setValue(rowdata.data.CONTACT);
    Ext.getCmp('TxtPopupAddressAccountInterface').setValue(rowdata.data.ADDRESS);
    Ext.getCmp('TxtPopupCityAccountInterface').setValue(rowdata.data.CITY);
    Ext.getCmp('TxtPopupStateAccountInterface').setValue(rowdata.data.STATE);
    Ext.getCmp('TxtPopupZipAccountInterface').setValue(rowdata.data.ZIP);
    Ext.getCmp('TxtPopupCountryAccountInterface').setValue(rowdata.data.COUNTRY);
    Ext.getCmp('TxtPopupPhone1AccountInterface').setValue(rowdata.data.PHONE1);
    Ext.getCmp('TxtPopupPhone2AccountInterface').setValue(rowdata.data.PHONE2);
    Ext.getCmp('TxtPopupFaxAccountInterface').setValue(rowdata.data.FAX);
    Ext.getCmp('TxtPopupDueDayAccountInterface').setValue(rowdata.data.DUE_DAY);
    Ext.getCmp('TxtPopupAccountAccountInterface').setValue(rowdata.data.ACCOUNT);
//    caridatapasien();
}

function caridatapasien()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: {
                            Table: 'ViewAskepDetAcc_In',
                            query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);

                            if (cst.success === true)
                            {
                                if (cst.totalrecords === 0)
                                {
                                    Ext.getCmp('Tglpelaksanaan').setValue(now);
                                    Ext.getCmp('Jampelaksanaan').setValue('');
                                    tmpkd_perawat = '';
                                    Ext.getCmp('cboPerawatAccountInterface').setValue('');
                                    Ext.getCmp('txttekanandarah').setValue('');
                                    Ext.getCmp('txtPuke').setValue('');
                                    Ext.getCmp('txtCVP').setValue('');
                                    Ext.getCmp('txtNadi').setValue('');
                                    Ext.getCmp('txtKesadaran').setValue('');
                                    Ext.getCmp('txtSuhu').setValue('');
                                    Ext.getCmp('txtWSD/DRAIN').setValue('');
                                    Ext.getCmp('txtPerifer').setValue('');
                                    Ext.getCmp('txtOral').setValue('');
                                    Ext.getCmp('txtParental').setValue('');
                                    Ext.getCmp('txtMuntah').setValue('');
                                    Ext.getCmp('txtBAK').setValue('');
                                    Ext.getCmp('txtNGT').setValue('');
                                    Ext.getCmp('txtBAB').setValue('');
                                    Ext.getCmp('txtPendarahan').setValue('');
                                    Ext.getCmp('txttindakankeperawatan').setValue('');
                                } else
                                {
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmpjam = tmphasil.JAM_OBSERVASI.substring(0, 5);
                                    Ext.getCmp('Tglpelaksanaan').setValue(tmphasil.TGL_OBSERVASI);
                                    Ext.getCmp('Jampelaksanaan').setValue(tmpjam);
                                    tmpkd_perawat = tmphasil.KD_PERAWAT;
                                    Ext.getCmp('cboPerawatAccountInterface').setValue(tmphasil.NAMA_PERAWAT);
                                    Ext.getCmp('txttekanandarah').setValue(tmphasil.TEKANAN_DARAH);
                                    Ext.getCmp('txtPuke').setValue(tmphasil.DETAK_JANTUNG);
                                    Ext.getCmp('txtCVP').setValue(tmphasil.CVP);
                                    Ext.getCmp('txtNadi').setValue(tmphasil.NADI);
                                    Ext.getCmp('txtKesadaran').setValue(tmphasil.KESADARAN);
                                    Ext.getCmp('txtSuhu').setValue(tmphasil.SUHU);
                                    Ext.getCmp('txtWSD/DRAIN').setValue(tmphasil.WSD);
                                    Ext.getCmp('txtPerifer').setValue(tmphasil.PERIFER);
                                    Ext.getCmp('txtOral').setValue(tmphasil.ORAL);
                                    Ext.getCmp('txtParental').setValue(tmphasil.PARENTERAL);
                                    Ext.getCmp('txtMuntah').setValue(tmphasil.MUNTAH);
                                    Ext.getCmp('txtBAK').setValue(tmphasil.BAK);
                                    Ext.getCmp('txtNGT').setValue(tmphasil.NGT);
                                    Ext.getCmp('txtBAB').setValue(tmphasil.BAB);
                                    Ext.getCmp('txtPendarahan').setValue(tmphasil.PENDARAHAN);
                                    Ext.getCmp('txttindakankeperawatan').setValue(tmphasil.TINDAKAN_PERAWAT);
                                }
                            }
                        }
                    }
            );
}

function SavingData()
{
    var tmpjumlahceklis = 0;
    for (var L=0; L<dataSource_Account.getCount(); L++)
    {
        if (dataSource_Account.data.items[L].data.tag != '') {
           tmpjumlahceklis ++;
        }
        
    } 
    if (tmpjumlahceklis === 0) {
        ShowPesanWarning_acc_interface('Belum Ada Data Yang Di Pilih', 'Warning');
    }else{
         if (tmpkodeaccount === '') {
            ShowPesanWarning_acc_interface('Account Belum Di pilih', 'Warning');
        }else{
             if (tmpkodeproduk === '') {
                ShowPesanWarning_acc_interface('Produk Belum Di pilih', 'Warning');
                }else{
                    Ext.Ajax.request({   
                            url: baseURL + "index.php/master_data/viewaccountinterface/save",
                            params: paramsaving_ACC_Interface(),
                            failure: function (o)
                            {
                                // load_acc_interface(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_acc_interface').getValue(),Ext.getCmp('TxtTgl_voucher_acc_interface2').getValue());
                                ShowPesanWarning_acc_interface('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
                            },
                            success: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {                       
                                    ShowPesanInfo_acc_interface('Proses Simpan Berhasil', 'Save');
                                    loaddatadetailUnit(tmpkodeUnit,tmpkodeproduk);
                                }                 
                                else
                                {
                                   // 
                                    ShowPesanWarning_acc_interface('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
                                };
                            }
                        });
                }
            
        }        
    }
   
    
}
;
function paramsaving_ACC_Interface()
{
    var params =
            {
                account  : tmpkodeaccount,
                kdproduk : tmpkodeproduk
            };
               params['jumlah']=dataSource_Account.getCount();
               for (var L=0; L<dataSource_Account.getCount(); L++)
                {
                    if (dataSource_Account.data.items[L].data.tag != '') {
                        tmpceklis = 'true';
                    }else{
                        tmpceklis = 'false';
                    }
                    
                            params['kd_unit'+L]      = dataSource_Account.data.items[L].data.kd_unit;
                            params['tag'+L]          = tmpceklis;
                    
                }   
   return params;  
}

function mComboSpesialisasiAccountInterface()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiAccountInterface = new WebApp.DataStore({fields: Field});
    dsSpesialisasiAccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasi',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiAccountInterface = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboSpesialisasiAccountInterface',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local', forceSelection: true,
                        emptyText: 'Select a Spesialisasi...',
                        selectOnFocus: true,
                        fieldLabel: 'Propinsi',
                        align: 'Right',
                        store: dsSpesialisasiAccountInterface,
                        valueField: 'KD_SPESIAL',
                        displayField: 'SPESIALISASI',
                        anchor: '20%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKelasAccountInterface').setValue('');
                                        Ext.getCmp('cboKamarAccountInterface').setValue('');
                                        loaddatastoreKelasAccountInterface(b.data.KD_SPESIAL);
                                        var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                        load_AccountInterface(tmpkriteriaObservasi);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKelasAccountInterface').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboSpesialisasiAccountInterface;
}
;
function loaddatastoreKelasAccountInterface(kd_spesial)
{
    dsKelasAccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function mComboKelasAccountInterface()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasAccountInterface = new WebApp.DataStore({fields: Field});
    var cboKelasAccountInterface = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboKelasAccountInterface',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasAccountInterface,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        anchor: '20%',
                        listeners:
                                {'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKamarAccountInterface').setValue('');
                                        loaddatastoreKamarAccountInterface(b.data.kd_unit);
                                        var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                        load_AccountInterface(tmpkriteriaObservasi);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarAccountInterface').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboKelasAccountInterface;
}
;
function loaddatastoreKamarAccountInterface(kd_unit)
{
    dsKamarAccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiAccountInterface').getValue()
                                }
                    });
}

function mComboKamarAccountInterface()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarAccountInterface = new WebApp.DataStore({fields: Field});
    var cboKamarAccountInterface = new Ext.form.ComboBox
            (
                    {
                        x: 320,
                        y: 100,
                        id: 'cboKamarAccountInterface',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarAccountInterface,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        width: 120,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                        load_AccountInterface(tmpkriteriaObservasi);
                                    }
                                }
                    }
            );
    return cboKamarAccountInterface;
}
;
function load_AccountInterface(criteria)
{
    dataSource_AccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: 'order by deskripsi, nama_unit',
                                    Sortdir: 'ASC',
                                    target: 'ViewAccountInterface',
                                    param: criteria
                                }
                    }
            );
    return dataSource_AccountInterface;
}

function loadfilter_DetAccountInterface(rowdata)
{
    var criteria = '';
    if (rowdata !== undefined) {
        criteria = "kd_pasien_kunj = '" + rowdata.KD_PASIEN + "' and kd_unit_kunj = '" + rowdata.KD_UNIT +
                "' and urut_masuk_kunj = " + rowdata.URUT_MASUK + " and tgl_masuk_kunj = '" + rowdata.TGL_MASUK + "'";
    } else {
        criteria = "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
    }
    dataSource_detAccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepDetAcc_In',
                                    param: criteria
                                }
                    }
            );
    return dataSource_detAccountInterface;
}


function getCriteriaFilter_viDaftar()
{
    var strKriteria = " ng.AKHIR = 't' ";
    var tmpmedrec = Ext.getCmp('TxtCariMedrecAccountInterface').getValue();
    var tmpnama = Ext.getCmp('TxtCariNamaAccountInterfaceAccountInterface').getValue();
    var tmpspesialisasi = Ext.getCmp('cboSpesialisasiAccountInterface').getValue();
    var tmpkelas = Ext.getCmp('cboKelasAccountInterface').getValue();
    var tmpkamar = Ext.getCmp('cboKamarAccountInterface').getValue();
    var tmptglawal = Ext.get('dtpTanggalawalAccountInterface').getValue();
    var tmptglakhir = Ext.get('dtpTanggalakhirAccountInterface').getValue();
    var tmptambahan = Ext.getCmp('chkTglAccountInterface').getValue()

    if (tmpmedrec !== "")
    {
        strKriteria += " AND P.KD_PASIEN " + "ilike '%" + tmpmedrec + "%' ";
    }
    if (tmpnama !== "")
    {
        strKriteria += " AND P.NAMA " + "ilike '%" + tmpnama + "%' ";
    }
    if (tmpspesialisasi !== "")
    {
        if (tmpspesialisasi === '0')
        {
            strKriteria += "";
        } else
        {
            strKriteria += " AND NG.KD_SPESIAL = '" + tmpspesialisasi + "' ";
        }

    }
    if (tmpkelas !== "")
    {
        strKriteria += " AND ng.KD_UNIT_KAMAR='" + tmpkelas + "' ";
    }
    if (tmpkamar !== "")
    {
        strKriteria += " AND NG.NO_KAMAR= '" + tmpkamar + "' ";
    }
    if (tmptambahan === true)
    {
        strKriteria += " AND ng.Tgl_masuk Between '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
    }
    strKriteria += ' Limit 50 ';
    return strKriteria;
}

function mComboPerawat()
{
    var Field = ['KODE', 'NAMA'];
    dsPerawatAccountInterface = new WebApp.DataStore({fields: Field});
    dsPerawatAccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewPerawat',
                                    param: 'Aktif = 1 Order By nama_perawat'
                                }
                    }
            );
    var cboPerawatAccountInterface = new Ext.form.ComboBox
            (
                    {
                        x: 490,
                        y: 5,
                        id: 'cboPerawatAccountInterface',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Perawat...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsPerawatAccountInterface,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpkd_perawat = Ext.getCmp('cboPerawatAccountInterface').getValue();
                                    }
                                }
                    }
            );
    return cboPerawatAccountInterface;
}
;

function ShowPesanWarning_acc_interface(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfo_acc_interface(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarning_acc_interface('Hubungi Admin', 'Error');
                            loadfilter_DetAccountInterface();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfo_acc_interface('Data berhasil di hapus', 'Information');
                                caridatapasien();
                                loadfilter_DetAccountInterface();

                            } else
                            {
                                loadMask.hide();
                                ShowPesanWarning_acc_interface('Gagal menghapus data', 'Error');
                                caridatapasien();
                                loadfilter_DetAccountInterface();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepAcc_In',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
            };
    return params;
}

function mComboGroup()
{
    var cboGroup = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 100,
                        id: 'cboGroup',
                        typeAhead: true,
                        editable: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        tabIndex: 5,
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Group',
                        fieldLabel: 'Group',
                        width: 100,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Asset'], [2, 'Liability'], [3, 'Capital'], [4, 'Revenue'], [5, 'Expense']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectGroup,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectGroup = b.data.Id;
                                    }
                                }
                    }
            );
    return cboGroup;
}
;

function mComboLevel()
{
    var cboLevel = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 70,
                        id: 'cboLevel',
                        editable: false,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        tabIndex: 5,
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: '',
                        width: 50,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, '1'], [2, '2'], [3, '3'], [4, '4'], [5, '5'], [6, '6'], [7, '7'], [8, '8'], [9, '9']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectLevel,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectLevel = b.data.Id;
                                    }
                                }
                    }
            );
    return cboLevel;
}
;

function load_Data_interface(criteria)
{
    dataSource_Data_interface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAccountInterface',
                                    param: criteria
                                }
                    }
            );
    return dataSource_Data_interface;
}

var tmpkodeUnit;
function mCombodata_unit_ACC_Interface()
{
    var Field = ['kd_bagian', 'kd_unit', 'nama_unit'];

    ds_get_data_unit_ACC_Interface = new WebApp.DataStore({fields: Field});
    ds_get_data_unit_ACC_Interface.load
        (
            {
            params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'kd_bagian',
                        Sortdir: 'ASC',
                        target: 'ViewGetUnit',
                        param: "Parent = '0' AND Kd_Bagian < 10 AND Kd_Unit <> '0'"
                    }
            }
        );

    var cbodata_unit_ACC_Interface = new Ext.form.ComboBox
            (
                    {
                        x: 110,
                        y: 70,
                        id: 'cbodata_unit_ACC_Interface',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        align: 'Right',
                        editable: false,
                        width: 200,
                        store: ds_get_data_unit_ACC_Interface,
                        valueField: 'kd_bagian',
                        displayField: 'nama_unit',
                        selectOnFocus: true,
                        enableKeyEvents:true,
                        tabIndex: 3,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpkodeUnit = b.data.kd_bagian;
                                        // tmpkodeproduk
                                        // tmpkodeaccount
                                        loaddatadetailUnit(tmpkodeUnit,tmpkodeproduk);

                                    },
                                    keypress:function(c,e){
                                        if (e.keyCode == 13){
                                        
                                        } else if (e.keyCode == 9){
                                            
                                        }
                                    },

                                }
                    }
            );

    return cbodata_unit_ACC_Interface;
}
;

function loaddatadetailUnit(kd_unit,kd_produk){

    Ext.Ajax.request({
            url: baseURL + "index.php/master_data/viewaccountinterface/loaddatadetailunit",
            params: {
                kd_unit:kd_unit,
                kd_produk:kd_produk
            },
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarningPenerimaan_Piutang('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                dataSource_Account.removeAll();
                var recs=[],
                recType=dataSource_Account.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                dataSource_Account.add(recs);
                gridListHasilAccount.getView().refresh();
            }
        }
    });
}


function LookUPDataBelumTerdaftar(rowdata) {
    FormLookUPDataBelumTerdaftar = new Ext.Window({
        id: 'gridFormLookUPDataBelumTerdaftar',
        title: 'Produk Unit Yang Belum Masuk Account Interface',
        closeAction: 'destroy',
        width: 800,
        height: 600,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
                PanelPencarianLookupGridProdukBlmTerpakai(),
                // form_gridloaddataaccount_acc_interface()
                ],
        listeners: {
        }
    });
    FormLookUPDataBelumTerdaftar.show();

}
;
function PanelPencarianLookupGridProdukBlmTerpakai() {
    var fldgeneral = ['kd_produk', 'deskripsi', 'kd_unit',  'nama_unit'];
    ds_acc_interface_grid_general = new WebApp.DataStore({fields: fldgeneral});
    grid_general_acc_interface = new Ext.grid.EditorGridPanel({
        x:5,
        y:70,
        title: '',
        id: 'grid_general_acc_interface',
        stripeRows: true,
        store: ds_acc_interface_grid_general,
        border: true,
        frame: false,
        height: 490,
        width: 815,
        anchor: '100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
                }
            }
        }),
        cm: get_column_Blm_terdaftar_acc_interface(),
       viewConfig: {forceFit: true}
    });
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelPencarianLookupGridProdukBlmTerpakai',
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        width: '100%',
                        height: '100%',
                        iconCls: '',
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: '100%',
                                height: 600,
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'kode Produk '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtkodeProdukCariData',
                                        id: 'TxtkodeProdukCariData',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadDataBelumProdukTerpakai(Ext.getCmp('TxtkodeProdukCariData').getValue(),Ext.getCmp('TxtDeskripsiCariData').getValue(),Ext.getCmp('TxtUnitCariData').getValue(),Ext.getCmp('TxtNamaUnitCariData').getValue());
                                                }
                                            }
                                        }
                        
                                    },
                                    {
                                        x: 310,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Deskripsi '
                                    },
                                    {
                                        x: 370,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 380,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtDeskripsiCariData',
                                        id: 'TxtDeskripsiCariData',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadDataBelumProdukTerpakai(Ext.getCmp('TxtkodeProdukCariData').getValue(),Ext.getCmp('TxtDeskripsiCariData').getValue(),Ext.getCmp('TxtUnitCariData').getValue(),Ext.getCmp('TxtNamaUnitCariData').getValue());
                                                }
                                            }
                                        }
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Unit '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtUnitCariData',
                                        id: 'TxtUnitCariData',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadDataBelumProdukTerpakai(Ext.getCmp('TxtkodeProdukCariData').getValue(),Ext.getCmp('TxtDeskripsiCariData').getValue(),Ext.getCmp('TxtUnitCariData').getValue(),Ext.getCmp('TxtNamaUnitCariData').getValue());
                                                }
                                            }
                                        }
                                    },
                                    {
                                        x: 310,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Nama Unit '
                                    },
                                    {
                                        x: 370,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 380,
                                        y: 40,
                                        xtype: 'textfield',
                                        name : 'TxtNamaUnitCariData',
                                        id   : 'TxtNamaUnitCariData',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadDataBelumProdukTerpakai(Ext.getCmp('TxtkodeProdukCariData').getValue(),Ext.getCmp('TxtDeskripsiCariData').getValue(),Ext.getCmp('TxtUnitCariData').getValue(),Ext.getCmp('TxtNamaUnitCariData').getValue());
                                                }
                                            }
                                        }
                                    },
                                    grid_general_acc_interface
                                ]
                            }
                        ]
                    });
    return items;
}
;


var ds_acc_interface_grid_general;
var grid_general_acc_interface;
function form_gridloaddataaccount_acc_interface() {
    
    var Isi_form_gridloaddataaccount_acc_interface = new Ext.Panel
            (
                    {
                        id: 'form_gridloaddataaccount_acc_interface',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        width: 800,
                        height: 600,
                        iconCls: '',
                        items:
                                [
                                    grid_general_acc_interface                               
                                ]
                    }
            );
    return Isi_form_gridloaddataaccount_acc_interface;
}
;

function get_column_Blm_terdaftar_acc_interface() {
  return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        
        {
            id: Nci.getId(),
            header: 'Kode Produk',
            dataIndex: 'kd_produk',
            width: 100,
            hidden: false,
            menuDisabled: true
        }, {
            id: Nci.getId(),
            header: 'Deskripsi',
            dataIndex: 'deskripsi',
            sortable: true,
            width: 200
        }, {
            id: Nci.getId(),
            header: 'Kode Unit',
            dataIndex: 'kd_unit',
            width: 100,
            menuDisabled: true,
            hidden: false
        },
        {
            id: Nci.getId(),
            header: 'Nama Unit',
            dataIndex: 'nama_unit',
            width: 100,
            menuDisabled: true,
            hidden: false
        }
    ]);
}

function loadDataBelumProdukTerpakai(kd_produk,deskripsi,kd_unit,nama_unit){
    Ext.Ajax.request({
            url: baseURL + "index.php/master_data/viewaccountinterface/loaddatabelumterpakai",
            params: {
                kd_produk:kd_produk,
                deskripsi:deskripsi,
                kd_unit:kd_unit,
                nama_unit:nama_unit
            },
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarning_acc_interface('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
                if (cst.success === true && cst.totalrecords > '400'){
                	ShowPesanInfo_acc_interface('Data Lebih Dari 400 record, harap pilih kriteria pencarian lebih spesifik', 'info');
                    ds_acc_interface_grid_general.removeAll();
                    var recs=[],
                    recType=ds_acc_interface_grid_general.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    ds_acc_interface_grid_general.add(recs);
                    grid_general_acc_interface.getView().refresh();
                }else{
                    ds_acc_interface_grid_general.removeAll();
                    var recs=[],
                    recType=ds_acc_interface_grid_general.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    ds_acc_interface_grid_general.add(recs);
                    grid_general_acc_interface.getView().refresh();
                }
            }
        });
}

function LookUPLebih_Dari_1_acc_interface(rowdata) {
    FormLookUPLebih_Dari_1_acc_interface = new Ext.Window({
        id: 'gridFormLookUPLebih_Dari_1_acc_interface',
        title: 'Produk Unit Yang Masuk Lebih Dari Satu Account',
        closeAction: 'destroy',
        width: 800,
        height: 600,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
                PanelPencarianLookupGridLebih_Dari_1_acc_interface()
                ],
        listeners: {
        }
    });
    FormLookUPLebih_Dari_1_acc_interface.show();

}
;

var ds_acc_interface_grid_Lebih_Dari_1_acc_interface;
var grid_Lebih_Dari_1_acc_interface;
function PanelPencarianLookupGridLebih_Dari_1_acc_interface() {
    var fldgeneral = ['account','kd_produk', 'deskripsi', 'kd_unit',  'nama_unit'];
    ds_acc_interface_grid_Lebih_Dari_1_acc_interface = new WebApp.DataStore({fields: fldgeneral});
    grid_Lebih_Dari_1_acc_interface = new Ext.grid.EditorGridPanel({
        x:5,
        y:100,
        title: '',
        id: 'grid_Lebih_Dari_1_acc_interface',
        stripeRows: true,
        store: ds_acc_interface_grid_Lebih_Dari_1_acc_interface,
        border: true,
        frame: false,
        height: 460,
        width: 815,
        anchor: '100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
                }
            }
        }),
        cm: get_column_Lebih_Dari_1_acc_interface(),
       viewConfig: {forceFit: true}
    });
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelPencarianLookupGridLebih_Dari_1_acc_interface',
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        width: '100%',
                        height: '100%',
                        iconCls: '',
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: '100%',
                                height: 600,
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'kode Produk '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtkodeProdukCariDataLebihLebih',
                                        id: 'TxtkodeProdukCariDataLebih',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadDataLebihDari1(Ext.getCmp('TxtkodeProdukCariDataLebih').getValue(),Ext.getCmp('TxtDeskripsiCariDataLebih').getValue(),Ext.getCmp('TxtUnitCariDataLebih').getValue(),Ext.getCmp('TxtNamaUnitCariDataLebih').getValue(),Ext.getCmp('TxtNamaUnitCariDataLebih').getValue());
                                                }
                                            }
                                        }
                        
                                    },
                                    {
                                        x: 310,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Deskripsi '
                                    },
                                    {
                                        x: 370,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 380,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtDeskripsiCariDataLebih',
                                        id: 'TxtDeskripsiCariDataLebih',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadDataLebihDari1(Ext.getCmp('TxtkodeProdukCariDataLebih').getValue(),Ext.getCmp('TxtDeskripsiCariDataLebih').getValue(),Ext.getCmp('TxtUnitCariDataLebih').getValue(),Ext.getCmp('TxtNamaUnitCariDataLebih').getValue(),Ext.getCmp('TxtAccountCariDataLebih').getValue());
                                                }
                                            }
                                        }
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Unit '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtUnitCariDataLebih',
                                        id: 'TxtUnitCariDataLebih',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadDataLebihDari1(Ext.getCmp('TxtkodeProdukCariDataLebih').getValue(),Ext.getCmp('TxtDeskripsiCariDataLebih').getValue(),Ext.getCmp('TxtUnitCariDataLebih').getValue(),Ext.getCmp('TxtNamaUnitCariDataLebih').getValue(),Ext.getCmp('TxtAccountCariDataLebih').getValue());
                                                }
                                            }
                                        }
                                    },
                                    {
                                        x: 310,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Nama Unit '
                                    },
                                    {
                                        x: 370,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },  
                                    {
                                        x: 380,
                                        y: 40,
                                        xtype: 'textfield',
                                        name : 'TxtNamaUnitCariDataLebih',
                                        id   : 'TxtNamaUnitCariDataLebih',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadDataLebihDari1(Ext.getCmp('TxtkodeProdukCariDataLebih').getValue(),Ext.getCmp('TxtDeskripsiCariDataLebih').getValue(),Ext.getCmp('TxtUnitCariDataLebih').getValue(),Ext.getCmp('TxtNamaUnitCariDataLebih').getValue(),Ext.getCmp('TxtAccountCariDataLebih').getValue());
                                                }
                                            }
                                        }
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Account '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtAccountCariDataLebih',
                                        id: 'TxtAccountCariDataLebih',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadDataLebihDari1(Ext.getCmp('TxtkodeProdukCariDataLebih').getValue(),Ext.getCmp('TxtDeskripsiCariDataLebih').getValue(),Ext.getCmp('TxtUnitCariDataLebih').getValue(),Ext.getCmp('TxtNamaUnitCariDataLebih').getValue(),Ext.getCmp('TxtAccountCariDataLebih').getValue());
                                                }
                                            }
                                        }
                                    },
                                    grid_Lebih_Dari_1_acc_interface
                                ]
                            }
                        ]
                    });
    return items;
}
;



function form_gridloaddataaccount_acc_interface() {
    
    var Isi_form_gridloaddataaccount_acc_interface = new Ext.Panel
            (
                    {
                        id: 'form_gridloaddataaccount_acc_interface',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        width: 800,
                        height: 600,
                        iconCls: '',
                        items:
                                [
                                    grid_general_acc_interface                               
                                ]
                    }
            );
    return Isi_form_gridloaddataaccount_acc_interface;
}
;

function get_column_Lebih_Dari_1_acc_interface() {
  return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            id: Nci.getId(),
            header: 'Account',
            dataIndex: 'account',
            width: 100,
            hidden: false,
            menuDisabled: true
        },
        {
            id: Nci.getId(),
            header: 'Kode Produk',
            dataIndex: 'kd_produk',
            width: 100,
            hidden: false,
            menuDisabled: true
        }, {
            id: Nci.getId(),
            header: 'Deskripsi',
            dataIndex: 'deskripsi',
            sortable: true,
            width: 200
        }, {
            id: Nci.getId(),
            header: 'Kode Unit',
            dataIndex: 'kd_unit',
            width: 100,
            menuDisabled: true,
            hidden: false
        },
        {
            id: Nci.getId(),
            header: 'Nama Unit',
            dataIndex: 'nama_unit',
            width: 100,
            menuDisabled: true,
            hidden: false
        }
    ]);
}

function loadDataLebihDari1(kd_produk,deskripsi,kd_unit,nama_unit,account){
    Ext.Ajax.request({
            url: baseURL + "index.php/master_data/viewaccountinterface/loaddatalebihdari1",
            params: {
                kd_produk:kd_produk,
                deskripsi:deskripsi,
                kd_unit:kd_unit,
                nama_unit:nama_unit,
                account:account
            },
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarning_acc_interface('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
                if (cst.success === true && cst.totalrecords > '400'){
                	ShowPesanInfo_acc_interface('Data Lebih Dari 400 record, harap pilih kriteria pencarian lebih spesifik', 'info');
                    ds_acc_interface_grid_Lebih_Dari_1_acc_interface.removeAll();
                    var recs=[],
                    recType=ds_acc_interface_grid_Lebih_Dari_1_acc_interface.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    ds_acc_interface_grid_Lebih_Dari_1_acc_interface.add(recs);
                    grid_Lebih_Dari_1_acc_interface.getView().refresh();
                }else{
                    ds_acc_interface_grid_Lebih_Dari_1_acc_interface.removeAll();
                    var recs=[],
                    recType=ds_acc_interface_grid_Lebih_Dari_1_acc_interface.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    ds_acc_interface_grid_Lebih_Dari_1_acc_interface.add(recs);
                    grid_Lebih_Dari_1_acc_interface.getView().refresh();
                }
            }
        });
}

function LookUPACC_Belum_terdaftar_acc_interface(rowdata) {
    FormLookUPACC_Belum_terdaftar_acc_interface = new Ext.Window({
        id: 'gridFormLookUPACC_Belum_terdaftar_acc_interface',
        title: 'Account Yang Belum Terdaftar',
        closeAction: 'destroy',
        width: 800,
        height: 600,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
                PanelPencarianLookupGridACC_Belum_terdaftar_acc_interface()
                ],
        listeners: {
        }
    });
    FormLookUPACC_Belum_terdaftar_acc_interface.show();

}
;

var ds_acc_interface_grid_ACC_Belum_terdaftar_acc_interface;
var grid_ACC_Belum_terdaftar_acc_interface;
function PanelPencarianLookupGridACC_Belum_terdaftar_acc_interface() {
    var fldgeneral = ['account','kd_produk', 'deskripsi', 'kd_unit',  'nama_unit'];
    ds_acc_interface_grid_ACC_Belum_terdaftar_acc_interface = new WebApp.DataStore({fields: fldgeneral});
    grid_ACC_Belum_terdaftar_acc_interface = new Ext.grid.EditorGridPanel({
        x:5,
        y:100,
        title: '',
        id: 'grid_ACC_Belum_terdaftar_acc_interface',
        stripeRows: true,
        store: ds_acc_interface_grid_ACC_Belum_terdaftar_acc_interface,
        border: true,
        frame: false,
        height: 460,
        width: 815,
        anchor: '100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
                }
            }
        }),
        cm: get_column_ACC_Belum_terdaftar_acc_interface(),
       viewConfig: {forceFit: true}
    });
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelPencarianLookupGridACC_Belum_terdaftar_acc_interface',
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        width: '100%',
                        height: '100%',
                        iconCls: '',
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: '100%',
                                height: 600,
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'kode Produk '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtkodeProdukCariACC_Belum_terdaftar',
                                        id: 'TxtkodeProdukCariACC_Belum_terdaftar',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadACC_Belum_terdaftarDari1(Ext.getCmp('TxtkodeProdukCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtDeskripsiCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtUnitCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtNamaUnitCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtNamaUnitCariACC_Belum_terdaftar').getValue());
                                                }
                                            }
                                        }
                        
                                    },
                                    {
                                        x: 310,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Deskripsi '
                                    },
                                    {
                                        x: 370,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 380,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtDeskripsiCariACC_Belum_terdaftar',
                                        id: 'TxtDeskripsiCariACC_Belum_terdaftar',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadACC_Belum_terdaftarDari1(Ext.getCmp('TxtkodeProdukCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtDeskripsiCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtUnitCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtNamaUnitCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtAccountCariACC_Belum_terdaftar').getValue());
                                                }
                                            }
                                        }
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Unit '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtUnitCariACC_Belum_terdaftar',
                                        id: 'TxtUnitCariACC_Belum_terdaftar',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadACC_Belum_terdaftarDari1(Ext.getCmp('TxtkodeProdukCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtDeskripsiCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtUnitCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtNamaUnitCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtAccountCariACC_Belum_terdaftar').getValue());
                                                }
                                            }
                                        }
                                    },
                                    {
                                        x: 310,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Nama Unit '
                                    },
                                    {
                                        x: 370,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },  
                                    {
                                        x: 380,
                                        y: 40,
                                        xtype: 'textfield',
                                        name : 'TxtNamaUnitCariACC_Belum_terdaftar',
                                        id   : 'TxtNamaUnitCariACC_Belum_terdaftar',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadACC_Belum_terdaftarDari1(Ext.getCmp('TxtkodeProdukCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtDeskripsiCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtUnitCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtNamaUnitCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtAccountCariACC_Belum_terdaftar').getValue());
                                                }
                                            }
                                        }
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Account '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtAccountCariACC_Belum_terdaftar',
                                        id: 'TxtAccountCariACC_Belum_terdaftar',
                                        width: 180,
                                        listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                {
                                                    loadACC_Belum_terdaftarDari1(Ext.getCmp('TxtkodeProdukCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtDeskripsiCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtUnitCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtNamaUnitCariACC_Belum_terdaftar').getValue(),Ext.getCmp('TxtAccountCariACC_Belum_terdaftar').getValue());
                                                }
                                            }
                                        }
                                    },
                                    grid_ACC_Belum_terdaftar_acc_interface
                                ]
                            }
                        ]
                    });
    return items;
}
;



function form_gridloaddataaccount_acc_interface() {
    
    var Isi_form_gridloaddataaccount_acc_interface = new Ext.Panel
            (
                    {
                        id: 'form_gridloaddataaccount_acc_interface',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        width: 800,
                        height: 600,
                        iconCls: '',
                        items:
                                [
                                    grid_general_acc_interface                               
                                ]
                    }
            );
    return Isi_form_gridloaddataaccount_acc_interface;
}
;

function get_column_ACC_Belum_terdaftar_acc_interface() {
  return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            id: Nci.getId(),
            header: 'Account',
            dataIndex: 'account',
            width: 100,
            hidden: false,
            menuDisabled: true
        },
        {
            id: Nci.getId(),
            header: 'Kode Produk',
            dataIndex: 'kd_produk',
            width: 100,
            hidden: false,
            menuDisabled: true
        }, {
            id: Nci.getId(),
            header: 'Deskripsi',
            dataIndex: 'deskripsi',
            sortable: true,
            width: 200
        }, {
            id: Nci.getId(),
            header: 'Kode Unit',
            dataIndex: 'kd_unit',
            width: 100,
            menuDisabled: true,
            hidden: false
        },
        {
            id: Nci.getId(),
            header: 'Nama Unit',
            dataIndex: 'nama_unit',
            width: 100,
            menuDisabled: true,
            hidden: false
        }
    ]);
}

function loadACC_Belum_terdaftarDari1(kd_produk,deskripsi,kd_unit,nama_unit,account){
    Ext.Ajax.request({
            url: baseURL + "index.php/master_data/viewaccountinterface/loaddataaccountbelumterdaftar",
            params: {
                kd_produk:kd_produk,
                deskripsi:deskripsi,
                kd_unit:kd_unit,
                nama_unit:nama_unit,
                account:account
            },
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarning_acc_interface('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
                if (cst.success === true && cst.totalrecords > '400'){
                	ShowPesanInfo_acc_interface('Data Lebih Dari 400 record, harap pilih kriteria pencarian lebih spesifik', 'info');
                    ds_acc_interface_grid_ACC_Belum_terdaftar_acc_interface.removeAll();
                    var recs=[],
                    recType=ds_acc_interface_grid_ACC_Belum_terdaftar_acc_interface.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    ds_acc_interface_grid_ACC_Belum_terdaftar_acc_interface.add(recs);
                    grid_ACC_Belum_terdaftar_acc_interface.getView().refresh();
                }else{
                    ds_acc_interface_grid_ACC_Belum_terdaftar_acc_interface.removeAll();
                    var recs=[],
                    recType=ds_acc_interface_grid_ACC_Belum_terdaftar_acc_interface.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    ds_acc_interface_grid_ACC_Belum_terdaftar_acc_interface.add(recs);
                    grid_ACC_Belum_terdaftar_acc_interface.getView().refresh();
                }
            }
        });
}
