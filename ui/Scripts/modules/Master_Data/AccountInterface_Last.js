
var now = new Date();
var dsSpesialisasiAccountInterface;
var ListHasilAccountInterface;
var rowSelectedHasilAccountInterface;
var dsKelasAccountInterface;
var dsKamarAccountInterface;
var dataSource_AccountInterface;
var dataSource_detAccountInterface;
var dsPerawatAccountInterface;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpkd_perawat;
var rowSelected_viAcc_In;
var selectGroup;
var selectLevel;
var radios;
var dataSource_Data_interface;
var sBase = location.href.substr(0, location.href.lastIndexOf("/") + 1);

CurrentPage.page = getPanelAccountInterface(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelAccountInterface(mod_id) {
    var FormDepanAccountInterface = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Account Interface',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianAccountInterface()
        ],
        listeners: {
            'afterrender': function () {
               Ext.getCmp('TxtCashOnHandAccountInterface').setValue('111');
               Ext.getCmp('TxtCashInBankAccountInterface').setValue('112');
               Ext.getCmp('TxtFixedAssetsAccountInterface').setValue('13');
               Ext.getCmp('TxtSalesAccountInterface').setValue('4');
               Ext.getCmp('TxtRetainedEarningsAccountInterface').setValue('31101');
               Ext.getCmp('TxtCurrentEarningsAccountInterface').setValue('31101');
            }
        }
    });
    return FormDepanAccountInterface;
}
;
function getPanelPencarianAccountInterface() {
    var items = {
        layout: 'column',
        border: true,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 520,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Cash On Hand '
                    },
                    {
                        x: 130,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 140,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCashOnHandAccountInterface',
                        id: 'TxtCashOnHandAccountInterface',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            HasilAccountInterfaceLookUp("Groups = 1 And Type = 'G' ORDER BY Groups, Account");
                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Cash In Bank '
                    },
                    {
                        x: 130,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 140,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtCashInBankAccountInterface',
                        id: 'TxtCashInBankAccountInterface',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            HasilAccountInterfaceLookUp("Groups = 1 And Type = 'G' ORDER BY Groups, Account");
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Fixed Assets '
                    },
                    {
                        x: 130,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 140,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtFixedAssetsAccountInterface',
                        id: 'TxtFixedAssetsAccountInterface',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            HasilAccountInterfaceLookUp("Groups = 1 And Type = 'G' ORDER BY Groups, Account");
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Sales '
                    },
                    {
                        x: 130,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 140,
                        y: 100,
                        xtype: 'textfield',
                        name: 'TxtSalesAccountInterface',
                        id: 'TxtSalesAccountInterface',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            HasilAccountInterfaceLookUp("Groups = 4 And Type = 'G' ORDER BY Groups, Account");
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Retained Earnings '
                    },
                    {
                        x: 130,
                        y: 130,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 140,
                        y: 130,
                        xtype: 'textfield',
                        name: 'TxtRetainedEarningsAccountInterface',
                        id: 'TxtRetainedEarningsAccountInterface',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            HasilAccountInterfaceLookUp("Groups = 3 And Type = 'D' ORDER BY Groups, Account");
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 160,
                        xtype: 'label',
                        text: 'Current Earnings '
                    },
                    {
                        x: 130,
                        y: 160,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 140,
                        y: 160,
                        xtype: 'textfield',
                        name: 'TxtCurrentEarningsAccountInterface',
                        id: 'TxtCurrentEarningsAccountInterface',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            HasilAccountInterfaceLookUp("GGroups = 3 And Type = 'D' ORDER BY Groups, Account");
                                        }
                                    }
                                }
                    },
                    {
                        x: 140,
                        y: 190,
                        xtype: 'button',
                        text: 'Produk Interface',
                        id: 'btnProdukInterface_Acc_In',
                        handler: function ()
                        {
                        }
                    },
                    {
                        x: 10,
                        y: 220,
                        xtype: 'button',
                        width: 30,
                        id: 'btn1_Acc_In',
                        handler: function ()
                        {
                        }
                    },
                    {
                        x: 50,
                        y: 225,
                        xtype: 'label',
                        text: 'Produk Yang Belum Masuk Account Interface '
                    },
                    {
                        x: 10,
                        y: 250,
                        width: 30,
                        xtype: 'button',
                        id: 'btn2_Acc_In',
                        handler: function ()
                        {
                        }
                    },
                    {
                        x: 50,
                        y: 255,
                        xtype: 'label',
                        text: 'Produk Unit Yang Masuk Lebih Dari Satu Account '
                    },
                    {
                        x: 10,
                        y: 280,
                        width: 30,
                        xtype: 'button',
                        id: 'btn3_Acc_In',
                        handler: function ()
                        {
                        }
                    },
                    {
                        x: 50,
                        y: 285,
                        xtype: 'label',
                        text: 'Account yang belum terdaftar'
                    }
                ]
            }
        ]
    };
    return items;
}
;
function HasilAccountInterfaceLookUp(rowdata) {
    FormLookUpdetailAccountInterface = new Ext.Window({
        id: 'gridHasilAccountInterface',
        title: 'AccountInterface Setup',
        closeAction: 'destroy',
        width: 500,
        height: 350,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupAccountInterface()],
        listeners: {
        }
    });
    FormLookUpdetailAccountInterface.show();
    if (rowdata === undefined) {
        load_Account("ORDER BY Groups, Account");
    } else {
        load_Account(rowdata);
    }

}
;
function formpopupAccountInterface() {
    var Field = ['ACCOUNT', 'NAME', 'TYPE', 'LEVELS', 'PARENT', 'GROUPS', 'SHOWFIELD'];

    dataSource_Account = new WebApp.DataStore({
        fields: Field
    });


    var gridListHasilAccount = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_Account,
        anchor: '100% 100%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 150,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    //rowSelected_viAcc = dataSource_Account.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viAcc = dataSource_Account.getAt(ridx);
                if (rowSelected_viAcc !== undefined)
                {
                    Ext.getCmp('TxtNumberAccount').setValue(rowSelected_viAcc.data.ACCOUNT);
                    Ext.getCmp('TxtNamaAccount').setValue(rowSelected_viAcc.data.NAME);
                    if (rowSelected_viAcc.data.TYPE === 'G')
                    {
                        radios.items.items[0].setValue(true);
                    } else
                    {
                        radios.items.items[1].setValue(true);
                    }
                    Ext.getCmp('cboLevel').setValue(rowSelected_viAcc.data.LEVELS);
                    Ext.getCmp('TxtParentAccount').setValue(rowSelected_viAcc.data.PARENT);
                    Ext.getCmp('cboGroup').setValue(rowSelected_viAcc.data.GROUPS);
                } else
                {
                    //HasilAccountLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilAccount',
                        header: 'Number',
                        dataIndex: 'ACCOUNT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colMedrecViewHasilAccount',
                        header: 'Name',
                        dataIndex: 'NAME',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colNamaViewHasilAccount',
                        header: 'Type',
                        dataIndex: 'TYPE',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25
                    },
                    {
                        id: 'colAlamatViewHasilAccount',
                        header: 'Type',
                        dataIndex: 'LEVELS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25
                    },
                    {
                        id: 'colSpesialisasiViewHasilAccount',
                        header: 'Level',
                        dataIndex: 'PARENT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKelasViewHasilAccount',
                        header: 'Group',
                        dataIndex: 'GROUPS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25
                    },
                    {
                        id: 'colKamarViewHasilAccount',
                        header: 'ShowField',
                        dataIndex: 'SHOWFIELD',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        }
    });
    var FrmTabs_popupAccountInterface = new Ext.Panel
            (
                    {
                        id: 'formpopupAccountInterface',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: false,
                        shadhow: true,
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    gridListHasilAccount
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_viDaftar',
                                                    handler: function ()
                                                    {
                                                        SavingData()
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Delete',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_viDaftar',
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Cetak',
                                                    iconCls: 'print',
                                                    id: 'btnCetak_viDaftar',
                                                    handler: function ()
                                                    {
                                                        var criteria = GetCriteriaAcc_In();
                                                        loadMask.show();
                                                        loadlaporanAskep('0', 'LapObservasiTindakan', criteria, function () {
                                                            loadMask.hide();
                                                        });
                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupAccountInterface;
}
;
function PanelAccountInterface() {
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDataAccountInterface',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 500,
                                height: 300,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Code '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupCodeAccountInterface',
                                        id: 'TxtPopupCodeAccountInterface',
                                        width: 80,
                                        readOnly: false
                                    },
                                    {
                                        x: 210,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Name '
                                    },
                                    {
                                        x: 270,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 280,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupNamaAccountInterface',
                                        id: 'TxtPopupNamaAccountInterface',
                                        width: 200,
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Contact '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtPopupContactAccountInterface',
                                        id: 'TxtPopupContactAccountInterface',
                                        width: 360
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Address '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopupAddressAccountInterface',
                                        id: 'TxtPopupAddressAccountInterface',
                                        width: 360,
                                        readOnly: false
                                    },
                                    {
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'City '
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 100,
                                        xtype: 'textfield',
                                        name: 'TxtPopupCityAccountInterface',
                                        id: 'TxtPopupCityAccountInterface',
                                        width: 80,
                                        readOnly: false
                                    },
                                    {
                                        x: 210,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'State '
                                    },
                                    {
                                        x: 270,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 280,
                                        y: 100,
                                        xtype: 'textfield',
                                        name: 'TxtPopupStateAccountInterface',
                                        id: 'TxtPopupStateAccountInterface',
                                        width: 200,
                                        readOnly: false
                                    },
                                    {
                                        x: 10,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Zip '
                                    },
                                    {
                                        x: 110,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 130,
                                        xtype: 'textfield',
                                        name: 'TxtPopupZipAccountInterface',
                                        id: 'TxtPopupZipAccountInterface',
                                        width: 80,
                                        readOnly: false
                                    },
                                    {
                                        x: 210,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Country '
                                    },
                                    {
                                        x: 270,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 280,
                                        y: 130,
                                        xtype: 'textfield',
                                        name: 'TxtPopupCountryAccountInterface',
                                        id: 'TxtPopupCountryAccountInterface',
                                        width: 200,
                                        readOnly: false
                                    },
                                    {
                                        x: 10,
                                        y: 160,
                                        xtype: 'label',
                                        text: 'Phone 1 '
                                    },
                                    {
                                        x: 110,
                                        y: 160,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 160,
                                        xtype: 'textfield',
                                        name: 'TxtPopupPhone1AccountInterface',
                                        id: 'TxtPopupPhone1AccountInterface',
                                        width: 100,
                                        readOnly: false
                                    },
                                    {
                                        x: 230,
                                        y: 160,
                                        xtype: 'label',
                                        text: 'Phone 2 '
                                    },
                                    {
                                        x: 280,
                                        y: 160,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 290,
                                        y: 160,
                                        xtype: 'textfield',
                                        name: 'TxtPopupPhone2AccountInterface',
                                        id: 'TxtPopupPhone2AccountInterface',
                                        width: 100
                                    },
                                    {
                                        x: 10,
                                        y: 190,
                                        xtype: 'label',
                                        text: 'Fax '
                                    },
                                    {
                                        x: 110,
                                        y: 190,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 190,
                                        xtype: 'textfield',
                                        name: 'TxtPopupFaxAccountInterface',
                                        id: 'TxtPopupFaxAccountInterface',
                                        width: 100,
                                        readOnly: false
                                    },
                                    {
                                        x: 230,
                                        y: 190,
                                        xtype: 'label',
                                        text: 'Due Day '
                                    },
                                    {
                                        x: 280,
                                        y: 190,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 290,
                                        y: 190,
                                        xtype: 'numberfield',
                                        name: 'TxtPopupDueDayAccountInterface',
                                        id: 'TxtPopupDueDayAccountInterface',
                                        width: 100
                                    },
                                    {
                                        x: 10,
                                        y: 220,
                                        xtype: 'label',
                                        text: 'Account '
                                    },
                                    {
                                        x: 110,
                                        y: 220,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 220,
                                        xtype: 'numberfield',
                                        name: 'TxtPopupAccountAccountInterface',
                                        id: 'TxtPopupAccountAccountInterface',
                                        width: 100
                                    },
                                ]
                            }
                        ]
                    });
    return items;
}
;
function PanelDataAccountInterface()
{
    var Field = ['KD_PASIEN_KUNJ', 'KD_UNIT_KUNJ', 'URUT_MASUK_KUNJ', 'TGL_MASUK_KUNJ', 'TGL_OBSERVASI', 'JAM_OBSERVASI',
        'KD_PERAWAT', 'NAMA_PERAWAT', 'TEKANAN_DARAH', 'NADI', 'DETAK_JANTUNG', 'SUHU', 'CVP', 'WSD', 'KESADARAN', 'PERIFER',
        'ORAL', 'PARENTERAL', 'MUNTAH', 'NGT', 'BAB', 'BAK', 'PENDARAHAN', 'TINDAKAN_PERAWAT'];

    dataSource_detAccountInterface = new WebApp.DataStore({
        fields: Field
    });
    var gridListDataAccountInterface = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_detAccountInterface,
        anchor: '100% 100%',
        columnLines: false,
        autoScroll: true,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'form',
        region: 'center',
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglHasilRencana_Asuhan',
                        header: 'Tgl. Pelaksanaan',
                        dataIndex: 'TGL_OBSERVASI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'coljamHasilRencana_Asuhan',
                        header: 'Jam',
                        dataIndex: 'JAM_OBSERVASI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colPerawatHasilRencana_Asuhan',
                        header: 'Perawat',
                        dataIndex: 'NAMA_PERAWAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colTDHasilRencana_Asuhan',
                        header: 'TD',
                        dataIndex: 'TEKANAN_DARAH',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colNHasilRencana_Asuhan',
                        header: 'N',
                        dataIndex: 'NADI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colPHasilRencana_Asuhan',
                        header: 'P',
                        dataIndex: 'DETAK_JANTUNG',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colSHasilRencana_Asuhan',
                        header: 'S',
                        dataIndex: 'SUHU',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colCVPHasilRencana_Asuhan',
                        header: 'CVP',
                        dataIndex: 'CVP',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colWSDHasilRencana_Asuhan',
                        header: 'WSD/DRAIN',
                        dataIndex: 'WSD',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKESADARANHasilRencana_Asuhan',
                        header: 'Kesadaran',
                        dataIndex: 'KESADARAN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colPERIFERHasilRencana_Asuhan',
                        header: 'Perifer',
                        dataIndex: 'PERIFER',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colORALHasilRencana_Asuhan',
                        header: 'Oral',
                        dataIndex: 'ORAL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colPARENTERALHasilRencana_Asuhan',
                        header: 'Parental',
                        dataIndex: 'PARENTERAL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colMUNTAHHasilRencana_Asuhan',
                        header: 'Muntah',
                        dataIndex: 'MUNTAH',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colNGTHasilRencana_Asuhan',
                        header: 'NGT',
                        dataIndex: 'NGT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colBABHasilRencana_Asuhan',
                        header: 'BAB',
                        dataIndex: 'BAB',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colPENDARAHANHasilRencana_Asuhan',
                        header: 'Pendarahan',
                        dataIndex: 'PENDARAHAN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colTINDAKAN_PERAWATHasilRencana_Asuhan',
                        header: 'Tindakan Keperawatan',
                        dataIndex: 'TINDAKAN_PERAWAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 300
                    }
                ]
                ),
        viewConfig: {
            forceFit: false
        }
    });
    var PanelDataAccountInterface = new Ext.Panel
            (
                    {
                        title: "Data Rencana Asuhan",
                        id: 'PanelDataAccountInterface',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        height: 400,
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            gridListDataAccountInterface

                        ]
                    }
            );
    return PanelDataAccountInterface;
}
;

//combo dan load database

function datainit_AccountInterface(rowdata)
{
//    ACCOUNT: "1131"
//ADDRESS: "Banda Aceh"
//CITY: "Aceh"
//CONTACT: null
//COUNTRY: null
//CUSTOMER: "Umum"
//CUST_CODE: "0000000001"
//DUE_DAY: "45"
//FAX: null
//PHONE1: null
//PHONE2: null
//STATE: "Indonesia"
//ZIP: null
    console.log(rowdata.CUST_CODE)
    Ext.getCmp('TxtPopupCodeAccountInterface').setValue(rowdata.data.CUST_CODE);
    Ext.getCmp('TxtPopupNamaAccountInterface').setValue(rowdata.data.CUSTOMER);
    Ext.getCmp('TxtPopupContactAccountInterface').setValue(rowdata.data.CONTACT);
    Ext.getCmp('TxtPopupAddressAccountInterface').setValue(rowdata.data.ADDRESS);
    Ext.getCmp('TxtPopupCityAccountInterface').setValue(rowdata.data.CITY);
    Ext.getCmp('TxtPopupStateAccountInterface').setValue(rowdata.data.STATE);
    Ext.getCmp('TxtPopupZipAccountInterface').setValue(rowdata.data.ZIP);
    Ext.getCmp('TxtPopupCountryAccountInterface').setValue(rowdata.data.COUNTRY);
    Ext.getCmp('TxtPopupPhone1AccountInterface').setValue(rowdata.data.PHONE1);
    Ext.getCmp('TxtPopupPhone2AccountInterface').setValue(rowdata.data.PHONE2);
    Ext.getCmp('TxtPopupFaxAccountInterface').setValue(rowdata.data.FAX);
    Ext.getCmp('TxtPopupDueDayAccountInterface').setValue(rowdata.data.DUE_DAY);
    Ext.getCmp('TxtPopupAccountAccountInterface').setValue(rowdata.data.ACCOUNT);
//    caridatapasien();
}

function caridatapasien()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: {
                            Table: 'ViewAskepDetAcc_In',
                            query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);

                            if (cst.success === true)
                            {
                                if (cst.totalrecords === 0)
                                {
                                    Ext.getCmp('Tglpelaksanaan').setValue(now);
                                    Ext.getCmp('Jampelaksanaan').setValue('');
                                    tmpkd_perawat = '';
                                    Ext.getCmp('cboPerawatAccountInterface').setValue('');
                                    Ext.getCmp('txttekanandarah').setValue('');
                                    Ext.getCmp('txtPuke').setValue('');
                                    Ext.getCmp('txtCVP').setValue('');
                                    Ext.getCmp('txtNadi').setValue('');
                                    Ext.getCmp('txtKesadaran').setValue('');
                                    Ext.getCmp('txtSuhu').setValue('');
                                    Ext.getCmp('txtWSD/DRAIN').setValue('');
                                    Ext.getCmp('txtPerifer').setValue('');
                                    Ext.getCmp('txtOral').setValue('');
                                    Ext.getCmp('txtParental').setValue('');
                                    Ext.getCmp('txtMuntah').setValue('');
                                    Ext.getCmp('txtBAK').setValue('');
                                    Ext.getCmp('txtNGT').setValue('');
                                    Ext.getCmp('txtBAB').setValue('');
                                    Ext.getCmp('txtPendarahan').setValue('');
                                    Ext.getCmp('txttindakankeperawatan').setValue('');
                                } else
                                {
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmpjam = tmphasil.JAM_OBSERVASI.substring(0, 5);
                                    Ext.getCmp('Tglpelaksanaan').setValue(tmphasil.TGL_OBSERVASI);
                                    Ext.getCmp('Jampelaksanaan').setValue(tmpjam);
                                    tmpkd_perawat = tmphasil.KD_PERAWAT;
                                    Ext.getCmp('cboPerawatAccountInterface').setValue(tmphasil.NAMA_PERAWAT);
                                    Ext.getCmp('txttekanandarah').setValue(tmphasil.TEKANAN_DARAH);
                                    Ext.getCmp('txtPuke').setValue(tmphasil.DETAK_JANTUNG);
                                    Ext.getCmp('txtCVP').setValue(tmphasil.CVP);
                                    Ext.getCmp('txtNadi').setValue(tmphasil.NADI);
                                    Ext.getCmp('txtKesadaran').setValue(tmphasil.KESADARAN);
                                    Ext.getCmp('txtSuhu').setValue(tmphasil.SUHU);
                                    Ext.getCmp('txtWSD/DRAIN').setValue(tmphasil.WSD);
                                    Ext.getCmp('txtPerifer').setValue(tmphasil.PERIFER);
                                    Ext.getCmp('txtOral').setValue(tmphasil.ORAL);
                                    Ext.getCmp('txtParental').setValue(tmphasil.PARENTERAL);
                                    Ext.getCmp('txtMuntah').setValue(tmphasil.MUNTAH);
                                    Ext.getCmp('txtBAK').setValue(tmphasil.BAK);
                                    Ext.getCmp('txtNGT').setValue(tmphasil.NGT);
                                    Ext.getCmp('txtBAB').setValue(tmphasil.BAB);
                                    Ext.getCmp('txtPendarahan').setValue(tmphasil.PENDARAHAN);
                                    Ext.getCmp('txttindakankeperawatan').setValue(tmphasil.TINDAKAN_PERAWAT);
                                }
                            }
                        }
                    }
            );
}

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningObservasi('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            caridatapasien();
                            loadfilter_DetAccountInterface();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoObservasi('Proses Saving Berhasil', 'Save');
                                caridatapasien();
                                loadfilter_DetAccountInterface();
                            } else
                            {
                                ShowPesanWarningObservasi('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                caridatapasien();
                                loadfilter_DetAccountInterface();
                            }
                            ;
                        }
                    }
            );
}
;
function paramsaving()
{
    var params =
            {
                Table: 'ViewAskepAcc_In',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'",
                KD_PASIEN_KUNJ: tmpkdpasien,
                KD_UNIT_KUNJ: tmpkdunit,
                URUT_MASUK_KUNJ: tmpurutmasuk,
                TGL_MASUK_KUNJ: tmptglmasuk,
                TGL_OBSERVASI: Ext.get('Tglpelaksanaan').getValue(),
                JAM_OBSERVASI: Ext.get('Jampelaksanaan').getValue(),
                KD_PERAWAT: tmpkd_perawat,
                TEKANAN_DARAH: Ext.get('txttekanandarah').getValue(),
                NADI: Ext.get('txtNadi').getValue(),
                DETAK_JANTUNG: Ext.get('txtPuke').getValue(),
                SUHU: Ext.get('txtSuhu').getValue(),
                CVP: Ext.get('txtCVP').getValue(),
                WSD: Ext.get('txtWSD/DRAIN').getValue(),
                KESADARAN: Ext.get('txtKesadaran').getValue(),
                PERIFER: Ext.get('txtPerifer').getValue(),
                ORAL: Ext.get('txtOral').getValue(),
                PARENTERAL: Ext.get('txtParental').getValue(),
                MUNTAH: Ext.get('txtMuntah').getValue(),
                NGT: Ext.get('txtNGT').getValue(),
                BAB: Ext.get('txtBAB').getValue(),
                BAK: Ext.get('txtBAK').getValue(),
                PENDARAHAN: Ext.get('txtPendarahan').getValue(),
                TINDAKAN_PERAWAT: Ext.get('txttindakankeperawatan').getValue()
            };
    return params;
}

function mComboSpesialisasiAccountInterface()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiAccountInterface = new WebApp.DataStore({fields: Field});
    dsSpesialisasiAccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasi',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiAccountInterface = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboSpesialisasiAccountInterface',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local', forceSelection: true,
                        emptyText: 'Select a Spesialisasi...',
                        selectOnFocus: true,
                        fieldLabel: 'Propinsi',
                        align: 'Right',
                        store: dsSpesialisasiAccountInterface,
                        valueField: 'KD_SPESIAL',
                        displayField: 'SPESIALISASI',
                        anchor: '20%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKelasAccountInterface').setValue('');
                                        Ext.getCmp('cboKamarAccountInterface').setValue('');
                                        loaddatastoreKelasAccountInterface(b.data.KD_SPESIAL);
                                        var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                        load_AccountInterface(tmpkriteriaObservasi);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKelasAccountInterface').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboSpesialisasiAccountInterface;
}
;
function loaddatastoreKelasAccountInterface(kd_spesial)
{
    dsKelasAccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function mComboKelasAccountInterface()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasAccountInterface = new WebApp.DataStore({fields: Field});
    var cboKelasAccountInterface = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboKelasAccountInterface',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasAccountInterface,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        anchor: '20%',
                        listeners:
                                {'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKamarAccountInterface').setValue('');
                                        loaddatastoreKamarAccountInterface(b.data.kd_unit);
                                        var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                        load_AccountInterface(tmpkriteriaObservasi);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarAccountInterface').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboKelasAccountInterface;
}
;
function loaddatastoreKamarAccountInterface(kd_unit)
{
    dsKamarAccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiAccountInterface').getValue()
                                }
                    });
}

function mComboKamarAccountInterface()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarAccountInterface = new WebApp.DataStore({fields: Field});
    var cboKamarAccountInterface = new Ext.form.ComboBox
            (
                    {
                        x: 320,
                        y: 100,
                        id: 'cboKamarAccountInterface',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarAccountInterface,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        width: 120,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                        load_AccountInterface(tmpkriteriaObservasi);
                                    }
                                }
                    }
            );
    return cboKamarAccountInterface;
}
;
function load_AccountInterface(criteria)
{
    dataSource_AccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAccountInterface',
                                    param: criteria
                                }
                    }
            );
    return dataSource_AccountInterface;
}

function loadfilter_DetAccountInterface(rowdata)
{
    var criteria = '';
    if (rowdata !== undefined) {
        criteria = "kd_pasien_kunj = '" + rowdata.KD_PASIEN + "' and kd_unit_kunj = '" + rowdata.KD_UNIT +
                "' and urut_masuk_kunj = " + rowdata.URUT_MASUK + " and tgl_masuk_kunj = '" + rowdata.TGL_MASUK + "'";
    } else {
        criteria = "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
    }
    dataSource_detAccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepDetAcc_In',
                                    param: criteria
                                }
                    }
            );
    return dataSource_detAccountInterface;
}


function getCriteriaFilter_viDaftar()
{
    var strKriteria = " ng.AKHIR = 't' ";
    var tmpmedrec = Ext.getCmp('TxtCariMedrecAccountInterface').getValue();
    var tmpnama = Ext.getCmp('TxtCariNamaAccountInterfaceAccountInterface').getValue();
    var tmpspesialisasi = Ext.getCmp('cboSpesialisasiAccountInterface').getValue();
    var tmpkelas = Ext.getCmp('cboKelasAccountInterface').getValue();
    var tmpkamar = Ext.getCmp('cboKamarAccountInterface').getValue();
    var tmptglawal = Ext.get('dtpTanggalawalAccountInterface').getValue();
    var tmptglakhir = Ext.get('dtpTanggalakhirAccountInterface').getValue();
    var tmptambahan = Ext.getCmp('chkTglAccountInterface').getValue()

    if (tmpmedrec !== "")
    {
        strKriteria += " AND P.KD_PASIEN " + "ilike '%" + tmpmedrec + "%' ";
    }
    if (tmpnama !== "")
    {
        strKriteria += " AND P.NAMA " + "ilike '%" + tmpnama + "%' ";
    }
    if (tmpspesialisasi !== "")
    {
        if (tmpspesialisasi === '0')
        {
            strKriteria += "";
        } else
        {
            strKriteria += " AND NG.KD_SPESIAL = '" + tmpspesialisasi + "' ";
        }

    }
    if (tmpkelas !== "")
    {
        strKriteria += " AND ng.KD_UNIT_KAMAR='" + tmpkelas + "' ";
    }
    if (tmpkamar !== "")
    {
        strKriteria += " AND NG.NO_KAMAR= '" + tmpkamar + "' ";
    }
    if (tmptambahan === true)
    {
        strKriteria += " AND ng.Tgl_masuk Between '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
    }
    strKriteria += ' Limit 50 ';
    return strKriteria;
}

function mComboPerawat()
{
    var Field = ['KODE', 'NAMA'];
    dsPerawatAccountInterface = new WebApp.DataStore({fields: Field});
    dsPerawatAccountInterface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewPerawat',
                                    param: 'Aktif = 1 Order By nama_perawat'
                                }
                    }
            );
    var cboPerawatAccountInterface = new Ext.form.ComboBox
            (
                    {
                        x: 490,
                        y: 5,
                        id: 'cboPerawatAccountInterface',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Perawat...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsPerawatAccountInterface,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpkd_perawat = Ext.getCmp('cboPerawatAccountInterface').getValue();
                                    }
                                }
                    }
            );
    return cboPerawatAccountInterface;
}
;

function ShowPesanWarningObservasi(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoObservasi(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningObservasi('Hubungi Admin', 'Error');
                            loadfilter_DetAccountInterface();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoObservasi('Data berhasil di hapus', 'Information');
                                caridatapasien();
                                loadfilter_DetAccountInterface();

                            } else
                            {
                                loadMask.hide();
                                ShowPesanWarningObservasi('Gagal menghapus data', 'Error');
                                caridatapasien();
                                loadfilter_DetAccountInterface();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepAcc_In',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
            };
    return params;
}

function mComboGroup()
{
    var cboGroup = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 100,
                        id: 'cboGroup',
                        typeAhead: true,
                        editable: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        tabIndex: 5,
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Group',
                        fieldLabel: 'Group',
                        width: 100,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Asset'], [2, 'Liability'], [3, 'Capital'], [4, 'Revenue'], [5, 'Expense']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectGroup,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectGroup = b.data.Id;
                                    }
                                }
                    }
            );
    return cboGroup;
}
;

function mComboLevel()
{
    var cboLevel = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 70,
                        id: 'cboLevel',
                        editable: false,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        tabIndex: 5,
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: '',
                        width: 50,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, '1'], [2, '2'], [3, '3'], [4, '4'], [5, '5'], [6, '6'], [7, '7'], [8, '8'], [9, '9']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectLevel,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectLevel = b.data.Id;
                                    }
                                }
                    }
            );
    return cboLevel;
}
;

function load_Account(criteria)
{
    dataSource_Account.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAccount',
                                    param: criteria
                                }
                    }
            );
    return dataSource_Account;
}

function load_Data_interface(criteria)
{
    dataSource_Data_interface.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAccountInterface',
                                    param: criteria
                                }
                    }
            );
    return dataSource_Data_interface;
}