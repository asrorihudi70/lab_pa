
var now = new Date();
var dsSpesialisasicustomer;
var ListHasilcustomer;
var rowSelectedHasilcustomer;
var dsKelascustomer;
var dsKamarcustomer;
var dataSource_customer;
var dataSource_detcustomer;
var dsPerawatcustomer;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpkd_perawat;
var rowSelected_viCus;
var account_nci;
var radios;
var creteriaaa="";
var Account_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','account','name'],data: []});

var gridListHasilcustomer;
CurrentPage.page = getPanelcustomer(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelcustomer(mod_id) {
    var Field = ['kd_customer', 'customer', 'contact','account', 'alamat', 'kota', 'negara', 'kd_pos',
         'telepon1', 'telepon2', 'fax', 'DUE_DAY', 'account'];
    dataSource_customer = new WebApp.DataStore({
        fields: Field
    });

    load_customer("");
   gridListHasilcustomer = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_customer,
        anchor: '100% 80%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 150,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    //rowSelected_viCus = dataSource_customer.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viCus = dataSource_customer.getAt(ridx);
                console.log(rowSelected_viCus);
                if (rowSelected_viCus !== undefined)
                {
                    HasilcustomerLookUp(rowSelected_viCus);
                }
                else
                {
                    HasilcustomerLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colCodeViewHasilcustomer',
                        header: 'kode',
                        dataIndex: 'kd_customer',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colNameViewHasilcustomer',
                        header: 'Nama',
                        dataIndex: 'customer',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colcontactViewHasilcustomer',
                        header: 'Kontak',
                        dataIndex: 'contact',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colaccountViewHasilcustomer',
                        header: 'Akun',
                        dataIndex: 'account',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        tbar: {
            xtype: 'toolbar',
            items: [
						{
							xtype: 'button',
							text: 'Baru',
							iconCls: 'AddRow',
							id: 'btnNew_Cus',
							handler: function ()
							{
								HasilcustomerLookUp();
							}
						}           
				]
        },
        viewConfig: {
            forceFit: true
        }
    });
    var FormDepancustomer = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Master Customer',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencariancustomer(),
            gridListHasilcustomer
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });
    return FormDepancustomer;
}
;
function getPanelPencariancustomer() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 100,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCodecustomer',
                        id: 'TxtCodecustomer',
						width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtNamacustomer',
                        id: 'TxtNamacustomer',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'find',
                        id: 'btnCari_Cus',
                        handler: function ()
                        { 

						creteriaaa2();
						load_customer(creteriaaa);
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function creteriaaa2(){
	
	if (Ext.getCmp('TxtCodecustomer').getValue()!=="" && Ext.getCmp('TxtNamacustomer').getValue()==="" )
	{
	creteriaaa="lower(kd_customer) like lower('%"+ Ext.getCmp('TxtCodecustomer').getValue()+"%')";			
	}
	if (Ext.getCmp('TxtCodecustomer').getValue()==="" && Ext.getCmp('TxtNamacustomer').getValue()!=="" )
	{
	creteriaaa="lower(customer) like lower('"+ Ext.getCmp('TxtNamacustomer').getValue()+"%')";
		
	}
	if (Ext.getCmp('TxtCodecustomer').getValue()!=="" && Ext.getCmp('TxtNamacustomer').getValue()!=="" )
	{
	creteriaaa="lower(kd_customer) like lower('"+ Ext.getCmp('TxtCodecustomer').getValue()+"%') or lower(customer) like lower('"+ Ext.getCmp('TxtNamacustomer').getValue()+"%')";
		
	}
	if (Ext.getCmp('TxtCodecustomer').getValue()==="" && Ext.getCmp('TxtNamacustomer').getValue()==="" )
	{
	creteriaaa="";
	}

	return creteriaaa;
}
function HasilcustomerLookUp(rowdata) {
    FormLookUpdetailcustomer = new Ext.Window({
        id: 'gridHasilcustomer',
        title: 'Set Customer',
        closeAction: 'destroy',
        width: 500,
        height: 300,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupcustomer()],
        listeners: {
        }
    });
    FormLookUpdetailcustomer.show();
    if (rowdata === undefined||rowdata === "") {
	new_data();
    } else {
        datainit_customer(rowdata);
    }

}
;
function formpopupcustomer() {
    var FrmTabs_popupcustomer = new Ext.Panel
            (
                    {
                        id: 'formpopupcustomer',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: false,
                        shadhow: true,
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    Panelcustomer()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
												{
													xtype: 'button',
													text: 'Baru',
													iconCls: 'AddRow',
													id: 'btnNew_Cus2',
													handler: function ()
													{
														new_data();
													}
												},
                                                {
                                                    xtype: 'button',
                                                    text: 'Simpan',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_cus',
                                                    handler: function ()
                                                    {
                                                        SavingData()
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Hapus',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_cus',
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });
                                                    }
                                                }
                                                
                                            ]
                                }
                    }
            );
    return FrmTabs_popupcustomer;
}
;
function Panelcustomer() {
	 account_nci= Nci.form.Combobox.autoComplete({
		        x: 120,
                y: 70,
				store	: Account_ds,
				select	: function(a,b,c){
								account_nci.setValue(b.data.account);
								},
				insert	: function(o){
					return {
						account        :o.account,
						name 		   : o.name,
						text		   :  '<table style="font-size: 11px;"><tr><td width="50">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
					}
				},
				url: baseURL + "index.php/master_data/customer/accounts",
				valueField: 'account',
				displayField: 'text',
				listWidth: 300
			});
    
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDatacustomer',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 500,
                                height: 300,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Code '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupCodecustomer',
                                        id: 'TxtPopupCodecustomer',
                                        width: 80,
                                        readOnly: false,
										disabled:true,
                        
                                    },
                                    {
                                        x: 210,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Kontak '
                                    },
                                    {
                                        x: 270,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 280,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupcontactcustomer',
                                        id: 'TxtPopupcontactcustomer',
                                        width: 200,
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Nama '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtPopupNamacustomer',
                                        id: 'TxtPopupNamacustomer',
                                        width: 360
                                    },
                                    {
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Alamat '
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 100,
                                        xtype: 'textfield',
                                        name: 'TxtPopupalamatcustomer',
                                        id: 'TxtPopupalamatcustomer',
                                        width: 360,
                                        readOnly: false
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Akun '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    account_nci,
                                    {
                                        x: 10,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'negara '
                                    },
                                    {
                                        x: 110,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 130,
                                        xtype: 'textfield',
                                        name: 'TxtPopupnegaracustomer',
                                        id: 'TxtPopupnegaracustomer',
                                        width: 160,
                                        readOnly: false
                                    },
                                    {
                                        x: 10,
                                        y: 160,
                                        xtype: 'label',
                                        text: 'kd_pos '
                                    },
                                    {
                                        x: 110,
                                        y: 160,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 160,
                                        xtype: 'textfield',
                                        name: 'TxtPopupkd_poscustomer',
                                        id: 'TxtPopupkd_poscustomer',
                                        width: 80,
                                        readOnly: false
                                    },
                                    {
                                        x: 285,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Kota '
                                    },
                                    {
                                        x: 330,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 340,
                                        y: 130,
                                        xtype: 'textfield',
                                        name: 'TxtPopupkotacustomer',
                                        id: 'TxtPopupkotacustomer',
                                        width: 140,
                                        readOnly: false
                                    },
                                    {
                                        x: 230,
                                        y: 160,
                                        xtype: 'label',
                                        text: 'Telepon 1 '
                                    },
                                    {
                                        x: 280,
                                        y: 160,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 290,
                                        y: 160,
                                        xtype: 'textfield',
                                        name: 'TxtPopuptelepon1customer',
                                        id: 'TxtPopuptelepon1customer',
                                        width: 100,
                                        readOnly: false
                                    },
                                    {
                                        x: 230,
                                        y: 190,
                                        xtype: 'label',
                                        text: 'Telepon 2 '
                                    },
                                    {
                                        x: 280,
                                        y: 190,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 290,
                                        y: 190,
                                        xtype: 'textfield',
                                        name: 'TxtPopuptelepon2customer',
                                        id: 'TxtPopuptelepon2customer',
                                        width: 100
                                    },
                                    {
                                        x: 10,
                                        y: 190,
                                        xtype: 'label',
                                        text: 'Fax '
                                    },
                                    {
                                        x: 110,
                                        y: 190,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 190,
                                        xtype: 'textfield',
                                        name: 'TxtPopupFaxcustomer',
                                        id: 'TxtPopupFaxcustomer',
                                        width: 100,
                                        readOnly: false
                                    },
                                    
                                ]
                            }
                        ]
                    });
    return items;
}
;
function datainit_customer(rowdata)
{
   // console.log(rowdata.kd_customer)
    Ext.getCmp('TxtPopupCodecustomer').setValue(rowdata.data.kd_customer);
    Ext.getCmp('TxtPopupNamacustomer').setValue(rowdata.data.customer);
    Ext.getCmp('TxtPopupcontactcustomer').setValue(rowdata.data.contact);
    Ext.getCmp('TxtPopupalamatcustomer').setValue(rowdata.data.alamat);
    Ext.getCmp('TxtPopupkotacustomer').setValue(rowdata.data.kota);
    Ext.getCmp('TxtPopupnegaracustomer').setValue(rowdata.data.negara);
    Ext.getCmp('TxtPopupkd_poscustomer').setValue(rowdata.data.kd_pos);
    Ext.getCmp('TxtPopuptelepon1customer').setValue(rowdata.data.telepon1);
    Ext.getCmp('TxtPopuptelepon2customer').setValue(rowdata.data.telepon2);
    Ext.getCmp('TxtPopupFaxcustomer').setValue(rowdata.data.fax);
    account_nci.setValue(rowdata.data.account);
}

function new_data()
{
   // console.log(rowdata.kd_customer)
    Ext.getCmp('TxtPopupCodecustomer').setValue();
    Ext.getCmp('TxtPopupNamacustomer').setValue();
    Ext.getCmp('TxtPopupcontactcustomer').setValue();
    Ext.getCmp('TxtPopupalamatcustomer').setValue();
    Ext.getCmp('TxtPopupkotacustomer').setValue();
    Ext.getCmp('TxtPopupnegaracustomer').setValue();
    Ext.getCmp('TxtPopupkd_poscustomer').setValue();
    Ext.getCmp('TxtPopuptelepon1customer').setValue();
    Ext.getCmp('TxtPopuptelepon2customer').setValue();
    Ext.getCmp('TxtPopupFaxcustomer').setValue();
    account_nci.setValue();
}


function SavingData()
{
	if (Ext.getCmp('TxtPopupNamacustomer').getValue()==="")
	{
		 ShowPesanWarningCustomer('Nama Customer Belum diisi', 'Perhatian');

	}else{
		if (Ext.getCmp('TxtPopupcontactcustomer').getValue()==="")
		{
		ShowPesanWarningCustomer('kontak Belum diisi', 'Perhatian');	
		}else{
		if (Ext.getCmp('TxtPopuptelepon1customer').getValue()==="" && Ext.getCmp('TxtPopuptelepon2customer').getValue()==="")
		{
		ShowPesanWarningCustomer('Telepon Belum diisi', 'Perhatian');	
		}else{
		     Ext.Ajax.request
			(
					{	
						url: baseURL + "index.php/master_data/customer/save",
						params: paramsaving(),
						failure: function (o)
						{
							creteriaaa2();
							load_customer(creteriaaa);

							ShowPesanWarningCustomer('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
						},
						success: function (o)
						{
							creteriaaa2();
							load_customer(creteriaaa);

							var cst = Ext.decode(o.responseText);
							if (cst.success === true)
							{
								Ext.getCmp('TxtPopupCodecustomer').setValue(cst.kd_cus);
								ShowPesanInfoCustomer('Proses Saving Berhasil', 'Save');
							}
							else
							{
								ShowPesanWarningCustomer('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
							}
							;
						}
					}
			);
			
			}
			}
		
	}
}
;
function paramsaving()
{
    var params =
            {
            kd_cus:Ext.getCmp('TxtPopupCodecustomer').getValue(),
			customer:Ext.getCmp('TxtPopupNamacustomer').getValue(),
			alamat:Ext.getCmp('TxtPopupalamatcustomer').getValue(),
			kontak:Ext.getCmp('TxtPopupcontactcustomer').getValue(),
			kota:Ext.getCmp('TxtPopupkotacustomer').getValue(),
			negara:Ext.getCmp('TxtPopupnegaracustomer').getValue(),
			kode_pos:Ext.getCmp('TxtPopupkd_poscustomer').getValue(),
			tlp1:Ext.getCmp('TxtPopuptelepon1customer').getValue(),
			tlp2:Ext.getCmp('TxtPopuptelepon2customer').getValue(),
			fax:Ext.getCmp('TxtPopupFaxcustomer').getValue(),
			account:account_nci.getValue(),
			};
    return params;
}


function load_customer(criteria)
{
	//dataSource_customer
 Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/master_data/customer/select",
			params: {criteria:criteria},
			failure: function(o)
			{
				ShowPesanErrorRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_customer.removeAll();
					var recs=[],
						recType=dataSource_customer.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dataSource_customer.add(recs);
					
					gridListHasilcustomer.getView().refresh();
					
				
				} 
			}
		}
		
	)
}



function ShowPesanWarningCustomer(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoCustomer(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/master_data/customer/delete",
                        params: paramsaving(),
                        failure: function (o)
                        {
							creteriaaa2();
							load_customer(creteriaaa);
                            loadMask.hide();
                            ShowPesanWarningCustomer('Hubungi Admin', 'Error');
                          //  loadfilter_Detcustomer();
                        },
                        success: function (o)
                        { 	creteriaaa2();
							new_data();
							load_customer(creteriaaa);
							var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
								loadMask.hide();
                                ShowPesanInfoCustomer('Data berhasil di hapus', 'Information');
                                //loadfilter_Detcustomer();

                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningCustomer('Gagal menghapus data', 'Error');
                            }
                            ;
                        }
                    }

            );
}
;

