
var now = new Date();
var dsSpesialisasiGroup;
var ListHasilGroup;
var rowSelectedHasilGroup;
var dsKelasGroup;
var dsKamarGroup;
var dataSource_Group;
var dataSource_detGroup;
var dsPerawatGroup;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpkd_perawat;
var rowSelected_viGroup;
var selectGroup;
var selectLevel;
var radios;

CurrentPage.page = getPanelGroup(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelGroup(mod_id) {
    var Field = ['GROUPID', 'DANA_ID', 'GROUP_NAME', 'SUMBER_DANA'];

    dataSource_Group = new WebApp.DataStore({
        fields: Field
    });

    load_Group("");
    var gridListHasilGroup = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_Group,
        anchor: '100% 70%',
        columnLines: false,
        autoScroll: false,
        border: false,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 150,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    //rowSelected_viGroup = dataSource_Group.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viGroup = dataSource_Group.getAt(ridx);
                if (rowSelected_viGroup !== undefined)
                {
                    Ext.getCmp('TxtGroupIDGroup').setValue(rowSelected_viGroup.data.GROUPID);
                    Ext.getCmp('TxtGroupName').setValue(rowSelected_viGroup.data.GROUP_NAME);
                    Ext.getCmp('cboSumberDanaGroup').setValue(rowSelected_viGroup.data.SUMBER_DANA);
                }
                else
                {
                    //HasilGroupLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilGroup',
                        header: 'Group ID',
                        dataIndex: 'GROUPID',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colMedrecViewHasilGroup',
                        header: 'Group Name',
                        dataIndex: 'GROUP_NAME',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colNamaViewHasilGroup',
                        header: 'Sumber Dana',
                        dataIndex: 'SUMBER_DANA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        }
    });
    var FormDepanGroup = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Group',
        border: false,
        shadhow: true,
//        height: 100,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            gridListHasilGroup
        ],
        tbar: {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'New',
                    iconCls: 'New',
                    id: 'btnNew_Group',
                    handler: function ()
                    {
                        Ext.Ajax.request
                                (
                                        {
                                            url: baseURL + "index.php/main/CreateDataObj",
                                            params: paramGetID(),
                                            success: function (o)
                                            {
                                                var cst = Ext.decode(o.responseText);
                                                if (cst.success === true)
                                                {
                                                    Ext.getCmp('TxtGroupIDGroup').setValue(cst.ID);
                                                    Ext.getCmp('TxtGroupName').setValue('');
                                                    Ext.getCmp('cboSumberDanaGroup').setValue('');
                                                }
                                                ;
                                            }
                                        }
                                );
                    }
                },
                {
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'save',
                    id: 'btnSimpan_Group',
                    handler: function ()
                    {
                        SavingData();
                    }
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    iconCls: 'remove',
                    id: 'btnHapus_Group',
                    handler: function ()
                    {
                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                            if (button === 'yes') {
                                loadMask.show();
                                DeleteData();
                            }
                        });
                    }
                }
            ]
        },
        listeners: {
            'afterrender': function () {

            }
        }
    });
    return FormDepanGroup;
}
;
function getPanelPencarianPasien() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 100,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Group ID '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtGroupIDGroup',
                        id: 'TxtGroupIDGroup',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Group Name '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtGroupName',
                        id: 'TxtGroupName',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Sumber Dana '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSumberDana()
//                    {
//                        x: 120,
//                        y: 70,
//                    },
                ]
            }
        ]
    };
    return items;
}
;

//combo dan load database

function caridatapasien()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: {
                            Table: 'ViewAskepDetGroup',
                            query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);

                            if (cst.success === true)
                            {
                                if (cst.totalrecords === 0)
                                {
                                    Ext.getCmp('Tglpelaksanaan').setValue(now);
                                    Ext.getCmp('Jampelaksanaan').setValue('');
                                    tmpkd_perawat = '';
                                    Ext.getCmp('cboPerawatGroup').setValue('');
                                    Ext.getCmp('txttekanandarah').setValue('');
                                    Ext.getCmp('txtPuke').setValue('');
                                    Ext.getCmp('txtCVP').setValue('');
                                    Ext.getCmp('txtNadi').setValue('');
                                    Ext.getCmp('txtKesadaran').setValue('');
                                    Ext.getCmp('txtSuhu').setValue('');
                                    Ext.getCmp('txtWSD/DRAIN').setValue('');
                                    Ext.getCmp('txtPerifer').setValue('');
                                    Ext.getCmp('txtOral').setValue('');
                                    Ext.getCmp('txtParental').setValue('');
                                    Ext.getCmp('txtMuntah').setValue('');
                                    Ext.getCmp('txtBAK').setValue('');
                                    Ext.getCmp('txtNGT').setValue('');
                                    Ext.getCmp('txtBAB').setValue('');
                                    Ext.getCmp('txtPendarahan').setValue('');
                                    Ext.getCmp('txttindakankeperawatan').setValue('');
                                } else
                                {
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmpjam = tmphasil.JAM_OBSERVASI.substring(0, 5);
                                    Ext.getCmp('Tglpelaksanaan').setValue(tmphasil.TGL_OBSERVASI);
                                    Ext.getCmp('Jampelaksanaan').setValue(tmpjam);
                                    tmpkd_perawat = tmphasil.KD_PERAWAT;
                                    Ext.getCmp('cboPerawatGroup').setValue(tmphasil.NAMA_PERAWAT);
                                    Ext.getCmp('txttekanandarah').setValue(tmphasil.TEKANAN_DARAH);
                                    Ext.getCmp('txtPuke').setValue(tmphasil.DETAK_JANTUNG);
                                    Ext.getCmp('txtCVP').setValue(tmphasil.CVP);
                                    Ext.getCmp('txtNadi').setValue(tmphasil.NADI);
                                    Ext.getCmp('txtKesadaran').setValue(tmphasil.KESADARAN);
                                    Ext.getCmp('txtSuhu').setValue(tmphasil.SUHU);
                                    Ext.getCmp('txtWSD/DRAIN').setValue(tmphasil.WSD);
                                    Ext.getCmp('txtPerifer').setValue(tmphasil.PERIFER);
                                    Ext.getCmp('txtOral').setValue(tmphasil.ORAL);
                                    Ext.getCmp('txtParental').setValue(tmphasil.PARENTERAL);
                                    Ext.getCmp('txtMuntah').setValue(tmphasil.MUNTAH);
                                    Ext.getCmp('txtBAK').setValue(tmphasil.BAK);
                                    Ext.getCmp('txtNGT').setValue(tmphasil.NGT);
                                    Ext.getCmp('txtBAB').setValue(tmphasil.BAB);
                                    Ext.getCmp('txtPendarahan').setValue(tmphasil.PENDARAHAN);
                                    Ext.getCmp('txttindakankeperawatan').setValue(tmphasil.TINDAKAN_PERAWAT);
                                }
                            }
                        }
                    }
            );
}

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningGroup('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            load_Group("");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoGroup('Proses Saving Berhasil', 'Save');
                                load_Group("");
                            }
                            else
                            {
                                ShowPesanWarningGroup('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                load_Group("");
                            }
                            ;
                        }
                    }
            );
}
;
function paramsaving()
{
    var params =
            {
                Table: 'CrudGroup',
                IDGROUP: Ext.getCmp('TxtGroupIDGroup').getValue(),
                NAMA: Ext.getCmp('TxtGroupName').getValue(),
                IDSD: Ext.getCmp('cboSumberDanaGroup').getValue(),
            };
    return params;
}

function mComboSpesialisasiGroup()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiGroup = new WebApp.DataStore({fields: Field});
    dsSpesialisasiGroup.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasi',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiGroup = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboSpesialisasiGroup',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local', forceSelection: true,
                        emptyText: 'Select a Spesialisasi...',
                        selectOnFocus: true,
                        fieldLabel: 'Propinsi',
                        align: 'Right',
                        store: dsSpesialisasiGroup,
                        valueField: 'KD_SPESIAL',
                        displayField: 'SPESIALISASI',
                        anchor: '20%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKelasGroup').setValue('');
                                        Ext.getCmp('cboKamarGroup').setValue('');
                                        loaddatastoreKelasGroup(b.data.KD_SPESIAL);
                                        var tmpkriteriaGroup = getCriteriaFilter_viDaftar();
                                        load_Group(tmpkriteriaGroup);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKelasGroup').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboSpesialisasiGroup;
}
;
function loaddatastoreKelasGroup(kd_spesial)
{
    dsKelasGroup.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function mComboKelasGroup()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasGroup = new WebApp.DataStore({fields: Field});
    var cboKelasGroup = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboKelasGroup',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasGroup,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        anchor: '20%',
                        listeners:
                                {'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKamarGroup').setValue('');
                                        loaddatastoreKamarGroup(b.data.kd_unit);
                                        var tmpkriteriaGroup = getCriteriaFilter_viDaftar();
                                        load_Group(tmpkriteriaGroup);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarGroup').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboKelasGroup;
}
;
function loaddatastoreKamarGroup(kd_unit)
{
    dsKamarGroup.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiGroup').getValue()
                                }
                    });
}

function mComboKamarGroup()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarGroup = new WebApp.DataStore({fields: Field});
    var cboKamarGroup = new Ext.form.ComboBox
            (
                    {
                        x: 320,
                        y: 100,
                        id: 'cboKamarGroup',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarGroup,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        width: 120,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaGroup = getCriteriaFilter_viDaftar();
                                        load_Group(tmpkriteriaGroup);
                                    }
                                }
                    }
            );
    return cboKamarGroup;
}
;
function load_Group(criteria)
{
    dataSource_Group.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewGroup',
                                    param: criteria
                                }
                    }
            );
    return dataSource_Group;
}

function loadfilter_DetGroup(rowdata)
{
    var criteria = '';
    if (rowdata !== undefined) {
        criteria = "kd_pasien_kunj = '" + rowdata.KD_PASIEN + "' and kd_unit_kunj = '" + rowdata.KD_UNIT +
                "' and urut_masuk_kunj = " + rowdata.URUT_MASUK + " and tgl_masuk_kunj = '" + rowdata.TGL_MASUK + "'";
    } else {
        criteria = "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
    }
    dataSource_detGroup.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepDetGroup',
                                    param: criteria
                                }
                    }
            );
    return dataSource_detGroup;
}


function getCriteriaFilter_viDaftar()
{
    var strKriteria = " ng.AKHIR = 't' ";
    var tmpmedrec = Ext.getCmp('TxtCariMedrecGroup').getValue();
    var tmpnama = Ext.getCmp('TxtCariNamaPasienGroup').getValue();
    var tmpspesialisasi = Ext.getCmp('cboSpesialisasiGroup').getValue();
    var tmpkelas = Ext.getCmp('cboKelasGroup').getValue();
    var tmpkamar = Ext.getCmp('cboKamarGroup').getValue();
    var tmptglawal = Ext.get('dtpTanggalawalGroup').getValue();
    var tmptglakhir = Ext.get('dtpTanggalakhirGroup').getValue();
    var tmptambahan = Ext.getCmp('chkTglGroup').getValue()

    if (tmpmedrec !== "")
    {
        strKriteria += " AND P.KD_PASIEN " + "ilike '%" + tmpmedrec + "%' ";
    }
    if (tmpnama !== "")
    {
        strKriteria += " AND P.NAMA " + "ilike '%" + tmpnama + "%' ";
    }
    if (tmpspesialisasi !== "")
    {
        if (tmpspesialisasi === '0')
        {
            strKriteria += "";
        } else
        {
            strKriteria += " AND NG.KD_SPESIAL = '" + tmpspesialisasi + "' ";
        }

    }
    if (tmpkelas !== "")
    {
        strKriteria += " AND ng.KD_UNIT_KAMAR='" + tmpkelas + "' ";
    }
    if (tmpkamar !== "")
    {
        strKriteria += " AND NG.NO_KAMAR= '" + tmpkamar + "' ";
    }
    if (tmptambahan === true)
    {
        strKriteria += " AND ng.Tgl_masuk Between '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
    }
    strKriteria += ' Limit 50 ';
    return strKriteria;
}

function mComboSumberDana()
{
    var Field = ['ID', 'SUMBER'];
    dsPerawatGroup = new WebApp.DataStore({fields: Field});
    dsPerawatGroup.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'sumber_dana',
                                    Sortdir: 'ASC',
                                    target: 'ViewSumberDana',
                                    param: ''
                                }
                    }
            );
    var cboSumberDamaGroup = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboSumberDanaGroup',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Sumbe Dana...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsPerawatGroup,
                        valueField: 'ID',
                        displayField: 'SUMBER',
                        widht: 200,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
//                                        tmpkd_perawat = Ext.getCmp('cboPerawatGroup').getValue();
                                    }
                                }
                    }
            );
    return cboSumberDamaGroup;
}
;

function ShowPesanWarningGroup(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoGroup(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningGroup('Hubungi Admin', 'Error');
                            load_Group("");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoGroup('Data berhasil di hapus', 'Information');
                                load_Group("");
                                Ext.getCmp('TxtGroupIDGroup').setValue('');
                                Ext.getCmp('TxtGroupName').setValue('');
                                Ext.getCmp('cboSumberDanaGroup').setValue('');
                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningGroup('Gagal menghapus data', 'Error');
                                load_Group("");
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'CrudGroup',
                query: "group_id = '" + Ext.getCmp('TxtGroupIDGroup').getValue() + "'"
            };
    return params;
}

function paramGetID()
{
    var params =
            {
                Table: 'ViewGroup'
            };
    return params;
}