
var now = new Date();
var dsSpesialisasiMataAnggaran;
var ListHasilMataAnggaran;
var rowSelectedHasilMataAnggaran;
var dsKelasMataAnggaran;
var dsKamarMataAnggaran;
var dataSource_MataAnggaran;
var dataSource_detMataAnggaran;
var dsPerawatMataAnggaran;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpkd_perawat;
var rowSelected_viMA;
var rowSelected_viMA2;
var selectMA;
var selectLevel;
var radios;
var radios1;
var tmpSD = 1;
var tmptype;
var tmpdebit;
var tmpdana = '';
var tahunsekarang = now.getFullYear();
var Parent_MataAnggaran_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','ma_id','ma_name'],data: []});

CurrentPage.page = getPanelMataAnggaran(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelMataAnggaran(mod_id) {
    var Field = ['DANAID', 'MAID', 'MANAME', 'MATYPE', 'MALEVEL', 'MADANAPARENT', 'MAPARENT', 'GROUPID', 'DEBIT', 'YEAR', 'PLAN'];

    dataSource_MataAnggaran = new WebApp.DataStore({
        fields: Field
    });


    var gridListHasilMataAnggaran = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_MataAnggaran,
        anchor: '100% 50%',
        columnLines: false,
        autoScroll: false,
        border: false,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 150,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelected_viMA = dataSource_MataAnggaran.getAt(row);
                    if (rowSelected_viMA !== undefined)
                    {
                        tmpdana = rowSelected_viMA.data.DANAID;
                        Ext.getCmp('TxtNilaiPaguAnggaran').setValue(rowSelected_viMA.data.PLAN);
                        Ext.getCmp('TxtMaIDMataAnggaran').setValue(rowSelected_viMA.data.MAID);
                        Ext.getCmp('TxtNameMataAnggaran').setValue(rowSelected_viMA.data.MANAME);
                        Ext.getCmp('cboLevelMA').setValue(rowSelected_viMA.data.MALEVEL);
                        Ext.getCmp('TxtParentMataAnggaran').setValue(rowSelected_viMA.data.MAPARENT);
                        if (rowSelected_viMA.data.MATYPE === 'G')
                        {
                            radios.items.items[0].setValue(true);
                        } else
                        {
                            radios.items.items[1].setValue(true);
                        }

                        if (rowSelected_viMA.data.DEBIT === 't')
                        {
                            radios1.items.items[0].setValue(true);
                        } else
                        {
                            radios1.items.items[1].setValue(true);
                        }
                    }
                    else
                    {
                    }
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viMA = dataSource_MataAnggaran.getAt(ridx);
                if (rowSelected_viMA !== undefined)
                {
                    tmpdana = rowSelected_viMA.data.DANAID;
//                    Ext.getCmp('TxtNilaiPaguAnggaran').setValue(rowSelected_viMA.data.PLAN);
                    convertToRupiah(rowSelected_viMA.data.PLAN);
                    Ext.getCmp('TxtMaIDMataAnggaran').setValue(rowSelected_viMA.data.MAID);
                    Ext.getCmp('TxtNameMataAnggaran').setValue(rowSelected_viMA.data.MANAME);
                    Ext.getCmp('cboLevelMA').setValue(rowSelected_viMA.data.MALEVEL);
                    Ext.getCmp('TxtParentMataAnggaran').setValue(rowSelected_viMA.data.MAPARENT);
                    if (rowSelected_viMA.data.MATYPE === 'G')
                    {
                        radios.items.items[0].setValue(true);
                    } else
                    {
                        radios.items.items[1].setValue(true);
                    }

                    if (rowSelected_viMA.data.DEBIT === 't')
                    {
                        radios1.items.items[0].setValue(true);

                    } else
                    {
                        radios1.items.items[1].setValue(true);
                    }
                }
                else
                {
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilMataAnggaran',
                        header: 'Ma ID',
                        dataIndex: 'MAID',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colMedrecViewHasilMataAnggaran',
                        header: 'Name',
                        dataIndex: 'MANAME',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colNamaViewHasilMataAnggaran',
                        header: 'Type',
                        dataIndex: 'MATYPE',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25
                    },
                    {
                        id: 'colAlamatViewHasilMataAnggaran',
                        header: 'Type',
                        dataIndex: 'MALEVEL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25
                    },
                    {
                        id: 'colSpesialisasiViewHasilMataAnggaran',
                        header: 'Level',
                        dataIndex: 'MALEVEL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25
                    },
                    {
                        id: 'colKelasViewHasilMataAnggaran',
                        header: 'Dana Parent',
                        dataIndex: 'MADANAPARENT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKamarViewHasilMataAnggaran',
                        header: 'MA Parent',
                        dataIndex: 'MAPARENT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colNilaiPaguViewHasilMataAnggaran',
                        header: 'Nilai Pagu',
                        dataIndex: 'PLAN',
                        sortable: false,
                        align : 'right',
                        hideable: false,
                        menuDisabled: true,
                        width: 50,
                        renderer : function (v, params, record)
                        {
                        return formatCurrency(record.data.PLAN);
                        },
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        }
    });
    var FormDepanMataAnggaran = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'MataAnggaran',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            getPanelPencarianPasien2(),
            gridListHasilMataAnggaran
        ],
        tbar: {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'New',
                    iconCls: 'New',
                    id: 'btnNew_MA',
                    handler: function ()
                    {
                        Ext.getCmp('TxtMaIDMataAnggaran').setValue('');
                        Ext.getCmp('TxtNameMataAnggaran').setValue('');
                        Ext.getCmp('cboLevelMA').setValue('');
                        Ext.getCmp('TxtParentMataAnggaran').setValue('');
                        Ext.getCmp('TxtNilaiPaguAnggaran').setValue('');
                        radios.items.items[0].setValue(false);
                        radios.items.items[1].setValue(false);
                        radios1.items.items[0].setValue(false);
                        radios1.items.items[1].setValue(false);

                    }
                },
                {
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'save',
                    id: 'btnSimpan_MA',
                    handler: function ()
                    {
                        SavingData()
                    }
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    iconCls: 'remove',
                    id: 'btnHapus_MA',
                    handler: function ()
                    {
                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                            if (button === 'yes') {
                                loadMask.show();
                                DeleteData();
                            }
                        });
                    }
                }
            ]
        },
        listeners: {
            'afterrender': function () {
                Ext.getCmp('cboSumberDanaMA').setValue('');
                Ext.getCmp('TxtTahunMataAnggaran').setValue(tahunsekarang);
                Ext.getCmp('TxtMaIDMataAnggaran').disable();
                Ext.getCmp('TxtNameMataAnggaran').disable();
                Ext.getCmp('cboLevelMA').disable();
                Ext.getCmp('TxtParentMataAnggaran').disable();
                Ext.getCmp('TxtNilaiPaguAnggaran').disable();
                Ext.getCmp('rbtypeMA').disable();
                Ext.getCmp('rbtype2MA').disable();
            }
        }
    });
    return FormDepanMataAnggaran;
}
;
function getPanelPencarianPasien() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 1190,
                height: 50,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Sumber Dana '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSumberDana(),
                    {
                        x: 250,
                        y: 10,
                        xtype: 'label',
                        text: 'Tahun '
                    },
                    {
                        x: 300,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 310,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtTahunMataAnggaran',
                        id: 'TxtTahunMataAnggaran',
                        width: 50
                    },
                    {
                        x: 370,
                        y: 10,
                        xtype: 'button',
                        text: 'Load... ',
                        handler: function ()
                        {
                            // console.log(Ext.getCmp('cboSumberDanaMA').getValue())
                            if (Ext.getCmp('cboSumberDanaMA').getValue() != '') {
                                load_MataAnggaran("am.DANA_ID = '" + tmpSD + "'   and am.Years='" + Ext.getCmp('TxtTahunMataAnggaran').getValue() + "' and am.isdebit = 't' ORDER BY Group_ID, MA_ID ");
                                Ext.getCmp('TxtMaIDMataAnggaran').enable();
                                Ext.getCmp('TxtNameMataAnggaran').enable();
                                Ext.getCmp('cboLevelMA').enable();
                                Ext.getCmp('TxtNilaiPaguAnggaran').enable();
                                Ext.getCmp('rbtypeMA').enable();
                                Ext.getCmp('rbtype2MA').enable();
                                radios1.items.items[0].setValue(true);
                                radios.items.items[0].setValue(true);
                                Ext.getCmp('cboLevelMA').setValue('1');
                                Ext.getCmp('TxtParentMataAnggaran').disable();
                            }else{
                                ShowPesanWarningMataAnggaran('Pilih Sumber Dana Terlebih Dahulu','Warning');
                            }
                            
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function getPanelPencarianPasien2() {
    radios = new Ext.form.RadioGroup({
        x: 120,
        y: 70,
        xtype: 'radiogroup',
        id: 'rbtypeMA',
        columns: 2,
        boxMaxWidth: 150,
        items: [
            {boxLabel: 'General', name: 'communication', inputValue: 'G'},
            {boxLabel: 'Detail', name: 'communication', inputValue: 'D'}
        ],
        listeners: {
            change: function (obj, value) {
                tmptype = value.inputValue;
                if (tmptype === 'D') {
                    Ext.getCmp('TxtNilaiPaguAnggaran').enable();
                }else{
                    Ext.getCmp('TxtNilaiPaguAnggaran').disable();
                }
            }
        }
    });
    radios1 = new Ext.form.RadioGroup({
        x: 460,
        y: 70,
        xtype: 'radiogroup',
        id: 'rbtype2MA',
        columns: 2,
        boxMaxWidth: 200,
        items: [
            {boxLabel: 'Penerimaan', name: 'communication1', inputValue: 't'},
            {boxLabel: 'Pengeluaran', name: 'communication1', inputValue: 'f'}
        ],
        listeners: {
            change: function (obj, value) {
                tmpdebit = value.inputValue;
                load_MataAnggaran("am.DANA_ID = '" + tmpSD + "'   and am.Years='" + Ext.getCmp('TxtTahunMataAnggaran').getValue() + "' and am.isdebit = '" + tmpdebit + "' ORDER BY Group_ID, MA_ID ");
            }
        }
    });

    // Parent_Mata_Anggaran = Nci.form.Combobox.autoComplete({
    //             x: 120,
    //             y: 100,
    //             store   : Parent_MataAnggaran_ds,
    //             keydown:function(text,e){
    //                         var nav=navigator.platform.match("Mac");
    //                         if (e.keyCode == 9 || e.keyCode == 13){
    //                              // Ext.getCmp('TxtPopuPenerimaPenerimaan_Piutang').focus();
    //                         }
    //             },
    //             select  : function(a,b,c){
                                
    //                             // Ext.getCmp('TxtPopupNamaPenerimaan_Piutang').setValue(b.data.name);
    //             },
    //             insert  : function(o){
    //                 return {
    //                     ma_id           : o.ma_id,
    //                     ma_name         : o.ma_name,
    //                     text            : '<table style="font-size: 11px;"><tr><td width="50">'+o.ma_id+'</td><td width="200">'+o.ma_name+'</td></tr></table>'
    //                 }
    //             },
    //             keypress:function(c,e){
    //                 if (e.keyCode == 13 || e.keyCode == 9){
    //                     // Ext.getCmp('TxtPopuPenerimaPenerimaan_Piutang').focus();
    //                 }
    //             },
    //             url: baseURL + "index.php/master_data/Penerimaan_Piutang/accounts",
    //             valueField: 'ma_id',
    //             displayField: 'ma_id',
    //             listWidth: 300,
    //             width:100,
    //             enableKeyEvents:true,
    //             tabIndex: 1
    //         });
    var items = {
        layout: 'column',
        border: true,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 160,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Ma ID '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtMaIDMataAnggaran',
                        id: 'TxtMaIDMataAnggaran',
                        width: 150,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            HasilMataAnggaranLookUp();
                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Name '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtNameMataAnggaran',
                        id: 'TxtNameMataAnggaran',
                        width: 290,
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Type '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    radios,
                    {
                        x: 293,
                        y: 70,
                        xtype: 'label',
                        text: 'Level '
                    },
                    {
                        x: 340,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboLevel(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Parent '
                    }, {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 100,
                        xtype: 'textfield',
                        name: 'TxtParentMataAnggaran',
                        id: 'TxtParentMataAnggaran',
                        width: 150,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpparam = '';
                                            if (Ext.getCmp('TxtParentMataAnggaran').getValue() != '') {
                                                tmpparam = "ma_id = '"+Ext.getCmp('TxtParentMataAnggaran').getValue()+"' and";
                                            }else{
                                                tmpparam = "";
                                            }
                                            var tanggal = Ext.getCmp('TxtTahunMataAnggaran').getValue();
                                            HasilParent_Mata_AnggaranLookUp(tmpparam + " years = '" + tanggal + "' and isdebit = '" + tmpdebit + "' and ma_type = 'G' and ma_levels < " + Ext.getCmp('cboLevelMA').getValue());
                                        }

                                    }
                                }
                    },
                    {
                        x: 425,
                        y: 70,
                        xtype: 'label',
                        text: 'MA '
                    },
                    {
                        x: 450,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    radios1,
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Nilai Pagu '
                    }, {
                        x: 110,
                        y: 130,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 130,
                        xtype: 'textfield',
                        name: 'TxtNilaiPaguMataAnggaran',
                        id: 'TxtNilaiPaguAnggaran',
                        width: 150,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            convertToRupiah(Ext.getCmp('TxtNilaiPaguAnggaran').getValue());
                                        }

                                    },
                                    'blur': function ()
                                    {
                                        convertToRupiah(Ext.getCmp('TxtNilaiPaguAnggaran').getValue());
                                    }

                                }
                    },
                ]
            }
        ]
    };
    return items;
}
;

function HasilMataAnggaranLookUp(rowdata) {
    FormLookUpdetailMataAnggaran = new Ext.Window({
        id: 'gridHasilMataAnggaran',
        title: 'List Mata Anggaran',
        closeAction: 'destroy',
        width: 600,
        height: 400,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupMataAnggaran()],
        listeners: {
        }
    });
    FormLookUpdetailMataAnggaran.show();
    if (rowdata === undefined) {
        
        loadfilter_DetMataAnggaran("");
    }
    else {
        // loadfilter_DetMataAnggaran(rowdata);
    }

}
;
function formpopupMataAnggaran() {
    var FrmTabs_popupMataAnggaran = new Ext.Panel
            (
                    {
                        id: 'formpopupMataAnggaran',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: false,
                        shadhow: true,
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelDataMataAnggaran()
                                ]
                    }
            );
    return FrmTabs_popupMataAnggaran;
}
;
function PanelDataMataAnggaran()
{
    var Field = ['DANAID', 'MAID', 'MANAME', 'MALEVEL', 'MAPARENT', 'GROUPID','MATYPE'];

    dataSource_detMataAnggaran = new WebApp.DataStore({
        fields: Field
    });
    // loadfilter_DetMataAnggaran("")
    var gridListDataMataAnggaran = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_detMataAnggaran,
        anchor: '100% 70%',
        columnLines: false,
        autoScroll: true,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'form',
        region: 'center',
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viMA2 = dataSource_detMataAnggaran.getAt(ridx);
                if (rowSelected_viMA2 !== undefined)
                {
                	// console.log(rowSelected_viMA2);
                    Ext.getCmp('TxtMaIDMataAnggaran').setValue(rowSelected_viMA2.data.MAID);
                    Ext.getCmp('TxtNameMataAnggaran').setValue(rowSelected_viMA2.data.MANAME);
                    tmpdana = rowSelected_viMA2.data.DANAID;
                    	if (rowSelected_viMA2.data.MATYPE === 'G')
	                   {
	                       radios.items.items[0].setValue(true);
	                   } else
	                   {
	                       radios.items.items[1].setValue(true);
	                   }
	                   Ext.getCmp('cboLevelMA').setValue(rowSelected_viMA2.data.MALEVEL);
	                   Ext.getCmp('TxtParentMataAnggaran').setValue(rowSelected_viMA2.data.MAPARENT);
                       Ext.getCmp('TxtParentMataAnggaran').enable();
                       Ext.getCmp('TxtNilaiPaguAnggaran').disable();
                       FormLookUpdetailMataAnggaran.destroy();
                   
                }
                else
                {
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglHasilRencana_Asuhan',
                        header: 'Ma ID',
                        dataIndex: 'MAID',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'coljamHasilRencana_Asuhan',
                        header: 'Ma Name',
                        dataIndex: 'MANAME',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colPerawatHasilRencana_Asuhan',
                        header: 'Level',
                        dataIndex: 'MALEVEL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colTDHasilRencana_Asuhan',
                        header: 'Parent',
                        dataIndex: 'MAPARENT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colNHasilRencana_Asuhan',
                        header: 'Group',
                        dataIndex: 'GROUPID',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        }
    });
    var PanelDataMataAnggaran = new Ext.Panel
            (
                    {
                        title: "",
                        id: 'PanelDataMataAnggaran',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        height: 400,
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                        	PanelPencarianDataMA(),
                        	{
							  xtype: 'tbspacer',
							  height:10
							},
                            gridListDataMataAnggaran

                        ]
                    }
            );
    return PanelDataMataAnggaran;
}
;

function PanelPencarianDataMA() {

    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelPencarianDataMA',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 500,
                                height: 70,
                                // anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'MA ID '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtCariNoMA',
                                        id: 'TxtCariNoMA',
                                        width: 200,
                                        listeners:
                                        {
                                            'render': function(c) {
                                                  c.getEl().on('keyup', function() {
                                                    loadfilter_DetMataAnggaran(param = 'ma_id like ~%'+Ext.getCmp('TxtCariNoMA').getValue()+'%~');
                                                  }, c);
                                            }
                                            // 'specialkey': function ()
                                            // {
                                            //     if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                            //     {
                                            //         if (Ext.getCmp('TxtCariNoMA').getValue() != '') {
                                            //             if (Ext.getCmp('TxtCariNAMEMA').getValue() != '') {
                                            //                 param = 'ma_id = ~'+Ext.getCmp('TxtCariNoMA').getValue()+'~ AND lower(ma_name) like lower(~%'+Ext.getCmp('TxtCariNAMEMA').getValue()+'~)';
                                            //             }else{
                                            //                 param = 'ma_id = ~'+Ext.getCmp('TxtCariNoMA').getValue()+'~'
                                            //             }                                                        
                                            //         }
                                            //         loadfilter_DetMataAnggaran(param);
                                            //     }

                                            // }
                                        }                
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'MA NAME '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtCariNAMEMA',
                                        id: 'TxtCariNAMEMA',
                                        width: 200,
                                        listeners:
                                        {
                                            'render': function(c) {
                                                  c.getEl().on('keyup', function() {
                                                    loadfilter_DetMataAnggaran('lower(ma_name) like lower(~%'+Ext.getCmp('TxtCariNAMEMA').getValue()+'%~)');
                                                  }, c);
                                            }

                                            // 'specialkey': function ()
                                            // {
                                            //     if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                            //     {
                                            //         if (Ext.getCmp('TxtCariNAMEMA').getValue() != '') {
                                            //             if (Ext.getCmp('TxtCariNoMA').getValue() != '') {
                                            //                 param = 'ma_id = ~'+Ext.getCmp('TxtCariNoMA').getValue()+'~ AND lower(ma_name) like lower(~%'+Ext.getCmp('TxtCariNAMEMA').getValue()+'~)';
                                            //             }else{
                                            //                 param =  'lower(ma_name) like lower(~%'+Ext.getCmp('TxtCariNAMEMA').getValue()+'%~)';
                                            //             }                                                        
                                            //         }
                                            //         loadfilter_DetMataAnggaran(param);
                                            //     }

                                            // }
                                        }                       
                                    }
                                ]
                            }
                        ]
                    });
    return items;
}
;

function HasilParent_Mata_AnggaranLookUp(rowdata) {
    FormLookUpdetailParent_Mata_Anggaran = new Ext.Window({
        id: 'gridHasilParent_Mata_Anggaran',
        title: 'List Mata Anggaran',
        closeAction: 'destroy',
        width: 600,
        height: 400,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupParent_Mata_Anggaran()],
        listeners: {
        }
    });
    FormLookUpdetailParent_Mata_Anggaran.show();
    loadfilter_DetMataAnggaran(rowdata);

}
;
function formpopupParent_Mata_Anggaran() {
    var FrmTabs_popupParent_Mata_Anggaran = new Ext.Panel
            (
                    {
                        id: 'formpopupParent_Mata_Anggaran',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: false,
                        shadhow: true,
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelDataParent_Mata_Anggaran()
                                ]
                    }
            );
    return FrmTabs_popupParent_Mata_Anggaran;
}
;
function PanelDataParent_Mata_Anggaran()
{
    var Field = ['DANAID', 'MAID', 'MANAME', 'MALEVEL', 'MAPARENT', 'GROUPID','MATYPE'];

    dataSource_detMataAnggaran = new WebApp.DataStore({
        fields: Field
    });
    var gridListDataParent_Mata_Anggaran = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_detMataAnggaran,
        anchor: '100% 70%',
        columnLines: false,
        autoScroll: true,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'form',
        region: 'center',
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viMA2 = dataSource_detMataAnggaran.getAt(ridx);
                if (rowSelected_viMA2 !== undefined)
                {
                    // console.log(rowSelected_viMA2.data);
                       Ext.getCmp('TxtParentMataAnggaran').setValue(rowSelected_viMA2.data.MAID);
                       FormLookUpdetailParent_Mata_Anggaran.destroy();
                   
                }
                else
                {
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglHasilMata_Anggaran',
                        header: 'Ma ID',
                        dataIndex: 'MAID',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'coljamHasilMata_Anggaran',
                        header: 'Ma Name',
                        dataIndex: 'MANAME',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colPerawatHasilMata_Anggaran',
                        header: 'Level',
                        dataIndex: 'MALEVEL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colTDHasilMata_Anggaran',
                        header: 'Parent',
                        dataIndex: 'MAPARENT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colNHasilMata_Anggaran',
                        header: 'Group',
                        dataIndex: 'GROUPID',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        }
    });
    var PanelDataParent_Mata_Anggaran = new Ext.Panel
            (
                    {
                        title: "",
                        id: 'PanelDataParent_Mata_Anggaran',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        height: 400,
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            gridListDataParent_Mata_Anggaran

                        ]
                    }
            );
    return PanelDataParent_Mata_Anggaran;
}
;

//combo dan load database

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningMataAnggaran('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            load_MataAnggaran("am.DANA_ID = '" + tmpSD + "'   and am.Years='" + Ext.getCmp('TxtTahunMataAnggaran').getValue() + "' and am.isdebit = '" + tmpdebit + "' ORDER BY Group_ID, MA_ID ");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true && cst.pesan === '')
                            {
                                ShowPesanInfoMataAnggaran('Proses Saving Berhasil', 'Save');
                                load_MataAnggaran("am.DANA_ID = '" + tmpSD + "'   and am.Years='" + Ext.getCmp('TxtTahunMataAnggaran').getValue() + "' and am.isdebit = '" + tmpdebit + "' ORDER BY Group_ID, MA_ID ");
                            }else if(cst.success === true && cst.pesan != ''){
                                ShowPesanWarningMataAnggaran("MA Parent harus ber-type General 'G' dan berlevel diatasnya", "Gagal");
                                load_MataAnggaran("am.DANA_ID = '" + tmpSD + "'   and am.Years='" + Ext.getCmp('TxtTahunMataAnggaran').getValue() + "' and am.isdebit = '" + tmpdebit + "' ORDER BY Group_ID, MA_ID ");
                            }
                            else
                            {
                                ShowPesanWarningMataAnggaran('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                load_MataAnggaran("am.DANA_ID = '" + tmpSD + "'   and am.Years='" + Ext.getCmp('TxtTahunMataAnggaran').getValue() + "' and am.isdebit = '" + tmpdebit + "' ORDER BY Group_ID, MA_ID ");
                            }
                            ;
                        }
                    }
            );
}
;
function paramsaving()
{
    var params =
            {
                Table: 'ViewMataAnggaran',
                DANAID: tmpdana,
                MAID: Ext.getCmp('TxtMaIDMataAnggaran').getValue(),
                NAMEMA: Ext.getCmp('TxtNameMataAnggaran').getValue(),
                LVMA: Ext.getCmp('cboLevelMA').getValue(),
                PARENTMA: Ext.getCmp('TxtParentMataAnggaran').getValue(),
                TYPEMA: tmptype,
                DEBITMA: tmpdebit,
                GROUPID: tmpSD,
                TGL: Ext.getCmp('TxtTahunMataAnggaran').getValue(),
                PAGU: Ext.getCmp('TxtNilaiPaguAnggaran').getValue(),
            };
    return params;
}

function load_MataAnggaran(criteria)
{
    dataSource_MataAnggaran.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewMataAnggaran',
                                    param: criteria
                                }
                    }
            );
    return dataSource_MataAnggaran;
}

function loadfilter_DetMataAnggaran(criteria)
{
    dataSource_detMataAnggaran.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: 'Group_ID, MA_ID',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetMataAnggaran',
                                    param: criteria
                                }
                    }
            );
    return dataSource_detMataAnggaran;
}

function ShowPesanWarningMataAnggaran(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoMataAnggaran(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningMataAnggaran('Hubungi Admin', 'Error');
                            load_MataAnggaran("am.DANA_ID = '" + tmpSD + "'   and am.Years='" + Ext.getCmp('TxtTahunMataAnggaran').getValue() + "' and am.isdebit = '" + tmpdebit + "' ORDER BY Group_ID, MA_ID ");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true && cst.pesan == 0)
                            {
                                loadMask.hide();
                                ShowPesanInfoMataAnggaran('Data berhasil di hapus', 'Information');
								load_MataAnggaran("am.DANA_ID = '" + tmpSD + "'   and am.Years='" + Ext.getCmp('TxtTahunMataAnggaran').getValue() + "' and am.isdebit = '" + tmpdebit + "' ORDER BY Group_ID, MA_ID ");

                            }else if (cst.success === true && cst.pesan == 1) {
                                loadMask.hide();
                                ShowPesanInfoMataAnggaran('Data Tidak berhasil di hapus, karena Data Masih Mempunyai child / Turunan', 'Error');
                                load_MataAnggaran("am.DANA_ID = '" + tmpSD + "'   and am.Years='" + Ext.getCmp('TxtTahunMataAnggaran').getValue() + "' and am.isdebit = '" + tmpdebit + "' ORDER BY Group_ID, MA_ID ");

                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningMataAnggaran('Gagal menghapus data', 'Error');
                             	load_MataAnggaran("am.DANA_ID = '" + tmpSD + "'   and am.Years='" + Ext.getCmp('TxtTahunMataAnggaran').getValue() + "' and am.isdebit = '" + tmpdebit + "' ORDER BY Group_ID, MA_ID ");
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewMataAnggaran',
                DANAID: tmpdana,
                MAID: Ext.getCmp('TxtMaIDMataAnggaran').getValue(),
                TGL: Ext.getCmp('TxtTahunMataAnggaran').getValue(),
                LVMA: Ext.getCmp('cboLevelMA').getValue(),
                PARENTMA: Ext.getCmp('TxtParentMataAnggaran').getValue(),
                TYPEMA: tmptype,
                DEBITMA: tmpdebit,
                GROUPID: tmpSD,
                PAGU: Ext.getCmp('TxtNilaiPaguAnggaran').getValue(),
            }
    return params;
}

function mComboLevel()
{
    var cboLevel = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 70,
                        id: 'cboLevelMA',
                        editable: false,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        tabIndex: 5,
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: '',
                        width: 50,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, '1'], [2, '2'], [3, '3'], [4, '4'], [5, '5'], [6, '6'], [7, '7'], [8, '8'], [9, '9']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectLevel,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectLevel = b.data.Id;
                                        if (b.data.Id >= 2)
                                        {
                                            Ext.getCmp('TxtParentMataAnggaran').enable();
                                        } else
                                        {
                                            Ext.getCmp('TxtParentMataAnggaran').disable();
                                        }
                                    }
                                }
                    }
            );
    return cboLevel;
}
;

function mComboSumberDana()
{
    var Field = ['NAMA', 'KODE'];
    dsPerawatMA = new WebApp.DataStore({fields: Field});
    dsPerawatMA.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'sumber_dana',
                                    Sortdir: 'ASC',
                                    target: 'ViewListGroup',
                                    param: ''
                                }
                    }
            );
    var cboSumberDamaMA = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 10,
                        id: 'cboSumberDanaMA',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Sumbe Dana...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsPerawatMA,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        width: 120,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpSD = Ext.getCmp('cboSumberDanaMA').getValue();
                                    }
                                }
                    }
            );
    return cboSumberDamaMA;
}
;

function convertToRupiah(objek) {
    separator = ".";
    a = objek;

    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;
    j = 0;
    for (i = panjang; i > 0; i--) {
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
        }
    }
    objek = c;
    Ext.getCmp('TxtNilaiPaguAnggaran').setValue(objek)
} 