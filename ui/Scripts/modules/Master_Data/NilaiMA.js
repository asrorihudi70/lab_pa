
var now = new Date();
var dsSpesialisasiNilaiNilaiMA;
var ListHasilNilaiNilaiMA;
var rowSelectedHasilNilaiNilaiMA;
var dsKelasNilaiNilaiMA;
var dsKamarNilaiNilaiMA;
var dataSource_NilaiNilaiMA;
var dataSource_detNilaiNilaiMA;
var dsPerawatNilaiNilaiMA;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpkd_perawat;
var rowSelected_viNilaiMA;
var rowSelected_viNilaiMA2;
var selectNilaiMA;
var selectLevel;
var radios;
var radios1;
var tmpSD = 1;
var tmptype;
var tmpdebit;
var tmpdana = '';

CurrentPage.page = getPanelNilaiNilaiMA(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelNilaiNilaiMA(mod_id) {
    var Field = ['DANAID', 'NilaiMAID', 'NilaiMANAME', 'NilaiMATYPE', 'NilaiMALEVEL', 'NilaiMADANAPARENT', 'NilaiMAPARENT', 'GROUPID', 'DEBIT', 'YEAR'];

    dataSource_NilaiNilaiMA = new WebApp.DataStore({
        fields: Field
    });


    var gridListHasilNilaiNilaiMA = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_NilaiNilaiMA,
        anchor: '100% 50%',
        columnLines: false,
        autoScroll: false,
        border: false,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 150,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    //rowSelected_viNilaiMA = dataSource_NilaiNilaiMA.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viNilaiMA = dataSource_NilaiNilaiMA.getAt(ridx);
                if (rowSelected_viNilaiMA !== undefined)
                {
//                    Ext.getCmp('cboSumberDanaNilaiMA').setValue('Pemerintahan');
//                    Ext.getCmp('TxtTahunNilaiNilaiMA').setValue('2015');
                    Ext.getCmp('TxtMaIDNilaiNilaiMA').setValue(rowSelected_viNilaiMA.data.NilaiMAID);
                    Ext.getCmp('TxtNameNilaiNilaiMA').setValue(rowSelected_viNilaiMA.data.NilaiMANAME);
                    Ext.getCmp('cboLevelNilaiMA').setValue(rowSelected_viNilaiMA.data.NilaiMALEVEL);
                    Ext.getCmp('TxtParentNilaiNilaiMA').setValue(rowSelected_viNilaiMA.data.NilaiMAPARENT);
                    if (rowSelected_viNilaiMA.data.NilaiMATYPE === 'G')
                    {
                        radios.items.items[0].setValue(true);
                    } else
                    {
                        radios.items.items[1].setValue(true);
                    }

                    if (rowSelected_viNilaiMA.data.DEBIT === 't')
                    {
                        radios1.items.items[0].setValue(true);
                    } else
                    {
                        radios1.items.items[1].setValue(true);
                    }

//                    Ext.getCmp('rbtypeNilaiMA').setValue(rowSelected_viNilaiMA.data.NilaiMAID);
//                    Ext.getCmp('rbtype2NilaiMA').setValue(rowSelected_viNilaiMA.data.NilaiMAID);
                }
                else
                {
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilNilaiNilaiMA',
                        header: 'Ma ID',
                        dataIndex: 'DANAID',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colMedrecViewHasilNilaiNilaiMA',
                        header: 'Name',
                        dataIndex: 'NilaiMANAME',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colNamaViewHasilNilaiNilaiMA',
                        header: 'Type',
                        dataIndex: 'NilaiMATYPE',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25
                    },
                    {
                        id: 'colAlamatViewHasilNilaiNilaiMA',
                        header: 'Type',
                        dataIndex: 'NilaiMALEVEL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25
                    },
                    {
                        id: 'colSpesialisasiViewHasilNilaiNilaiMA',
                        header: 'Level',
                        dataIndex: 'NilaiMALEVEL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25
                    },
                    {
                        id: 'colKelasViewHasilNilaiNilaiMA',
                        header: 'Dana Parent',
                        dataIndex: 'NilaiMADANAPARENT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKamarViewHasilNilaiNilaiMA',
                        header: 'NilaiMA Parent',
                        dataIndex: 'NilaiMAPARENT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        }
    });
    var FormDepanNilaiNilaiMA = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'NilaiNilaiMA',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            getPanelPencarianPasien2(),
            gridListHasilNilaiNilaiMA
        ],
        tbar: {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'New',
                    iconCls: 'New',
                    id: 'btnNew_NilaiMA',
                    handler: function ()
                    {
                        Ext.getCmp('TxtMaIDNilaiNilaiMA').setValue('');
                        Ext.getCmp('TxtNameNilaiNilaiMA').setValue('');
                        Ext.getCmp('cboLevelNilaiMA').setValue('');
                        Ext.getCmp('TxtParentNilaiNilaiMA').setValue('');
                        radios.items.items[0].setValue(false);
                        radios.items.items[1].setValue(false);
                        radios1.items.items[0].setValue(false);
                        radios1.items.items[1].setValue(false);

                    }
                },
                {
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'save',
                    id: 'btnSimpan_NilaiMA',
                    handler: function ()
                    {
                        SavingData()
                    }
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    iconCls: 'remove',
                    id: 'btnHapus_NilaiMA',
                    handler: function ()
                    {
                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                            if (button === 'yes') {
                                loadMask.show();
                                DeleteData();
                            }
                        });
                    }
                }
            ]
        },
        listeners: {
            'afterrender': function () {
                Ext.getCmp('cboSumberDanaNilaiMA').setValue('Pemerintahan');
                Ext.getCmp('TxtTahunNilaiNilaiMA').setValue('2015');
//                Ext.getCmp('TxtMaIDNilaiNilaiMA').disable();
//                Ext.getCmp('TxtNameNilaiNilaiMA').disable();
//                Ext.getCmp('cboLevelNilaiMA').disable();
//                Ext.getCmp('TxtParentNilaiNilaiMA').disable();
//                Ext.getCmp('rbtypeNilaiMA').disable();
//                Ext.getCmp('rbtype2NilaiMA').disable();
            }
        }
    });
    return FormDepanNilaiNilaiMA;
}
;
function getPanelPencarianPasien() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 1190,
                height: 50,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Sumber Dana '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSumberDana(),
                    {
                        x: 250,
                        y: 10,
                        xtype: 'label',
                        text: 'Tahun '
                    },
                    {
                        x: 300,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 310,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtTahunNilaiNilaiMA',
                        id: 'TxtTahunNilaiNilaiMA',
                        width: 50
                    },
                    {
                        x: 370,
                        y: 10,
                        xtype: 'button',
                        text: 'Load... ',
                        handler: function ()
                        {
                            load_NilaiNilaiMA("DANA_ID = '" + tmpSD + "'   and Years='" + Ext.getCmp('TxtTahunNilaiNilaiMA').getValue() + "' and isdebit = '1' ORDER BY Group_ID, NilaiMA_ID ");
                            Ext.getCmp('TxtMaIDNilaiNilaiMA').enable();
                            Ext.getCmp('TxtNameNilaiNilaiMA').enable();
                            Ext.getCmp('cboLevelNilaiMA').enable();
                            Ext.getCmp('TxtParentNilaiNilaiMA').enable();
                            Ext.getCmp('rbtypeNilaiMA').enable();
                            Ext.getCmp('rbtype2NilaiMA').enable();
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function getPanelPencarianPasien2() {
    radios1 = new Ext.form.RadioGroup({
        x: 120,
        y: 10,
        xtype: 'radiogroup',
        id: 'rbtype2NilaiMA',
        columns: 2,
        boxMaxWidth: 200,
        items: [
            {boxLabel: 'Penerimaan', name: 'communication1', inputValue: 't'},
            {boxLabel: 'Pengeluaran', name: 'communication1', inputValue: 'f'}
        ],
        listeners: {
            change: function (obj, value) {
                tmpdebit = value.inputValue;
                load_NilaiNilaiMA("DANA_ID = '" + tmpSD + "'   and Years='" + Ext.getCmp('TxtTahunNilaiNilaiMA').getValue() + "' and isdebit = '" + tmpdebit + "' ORDER BY Group_ID, NilaiMA_ID ");
            }
        }
    });
    var items = {
        layout: 'column',
        border: true,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 200,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kelompok '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    radios1,
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Mata Anggaran '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                ]
            }
        ]
    };
    return items;
}
;
function HasilNilaiNilaiMALookUp(rowdata) {
    FormLookUpdetailNilaiNilaiMA = new Ext.Window({
        id: 'gridHasilNilaiNilaiMA',
        title: 'Observasi & Tindakan',
        closeAction: 'destroy',
        width: 1000,
        height: 650,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupNilaiNilaiMA()],
        listeners: {
        }
    });
    FormLookUpdetailNilaiNilaiMA.show();
    if (rowdata === undefined) {

    } else {
    }

}
;
function formpopupNilaiNilaiMA() {
    var FrmTabs_popupNilaiNilaiMA = new Ext.Panel
            (
                    {
                        id: 'formpopupNilaiNilaiMA',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: false,
                        shadhow: true,
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelDataNilaiNilaiMA()
                                ]
                    }
            );
    return FrmTabs_popupNilaiNilaiMA;
}
;
function PanelDataNilaiNilaiMA()
{
    var Field = ['DANAID', 'NilaiMAID', 'NilaiMANAME', 'NilaiMALEVEL', 'NilaiMAPARENT', 'GROUPID'];

    dataSource_detNilaiNilaiMA = new WebApp.DataStore({
        fields: Field
    });
    loadfilter_DetNilaiNilaiMA()
    var gridListDataNilaiNilaiMA = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_detNilaiNilaiMA,
        anchor: '100% 100%',
        columnLines: false,
        autoScroll: true,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'form',
        region: 'center',
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    //rowSelected_viNilaiMA = dataSource_NilaiNilaiMA.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viNilaiMA2 = dataSource_detNilaiNilaiMA.getAt(ridx);
                if (rowSelected_viNilaiMA2 !== undefined)
                {
//                    console.log(rowSelected_viNilaiMA2);
                    Ext.getCmp('TxtMaIDNilaiNilaiMA').setValue(rowSelected_viNilaiMA2.data.NilaiMAID);
                    Ext.getCmp('TxtNameNilaiNilaiMA').setValue(rowSelected_viNilaiMA2.data.NilaiMANAME);
                    tmpdana = rowSelected_viNilaiMA2.data.DANAID;
                    FormLookUpdetailNilaiNilaiMA.destroy();
//                    Ext.getCmp('TxtNumberNilaiNilaiMA').setValue(rowSelected_viNilaiMA.data.ACCOUNT);
//                    Ext.getCmp('TxtNamaNilaiNilaiMA').setValue(rowSelected_viNilaiMA.data.NAME);
//                    if (rowSelected_viNilaiMA.data.TYPE === 'G')
//                    {
//                        radios.items.items[0].setValue(true);
//                    } else
//                    {
//                        radios.items.items[1].setValue(true);
//                    }
//                    Ext.getCmp('cboLevel').setValue(rowSelected_viNilaiMA.data.LEVELS);
//                    Ext.getCmp('TxtParentNilaiNilaiMA').setValue(rowSelected_viNilaiMA.data.PARENT);
//                    Ext.getCmp('cboNilaiMA').setValue(rowSelected_viNilaiMA.data.GROUPS);
                }
                else
                {
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglHasilRencana_Asuhan',
                        header: 'Ma ID',
                        dataIndex: 'NilaiMAID',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'coljamHasilRencana_Asuhan',
                        header: 'Ma Name',
                        dataIndex: 'NilaiMANAME',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colPerawatHasilRencana_Asuhan',
                        header: 'Ma Level',
                        dataIndex: 'NilaiMALEVEL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colTDHasilRencana_Asuhan',
                        header: 'Ma Parent',
                        dataIndex: 'NilaiMAPARENT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colNHasilRencana_Asuhan',
                        header: 'Group ID',
                        dataIndex: 'GROUPID',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        viewConfig: {
            forceFit: false
        }
    });
    var PanelDataNilaiNilaiMA = new Ext.Panel
            (
                    {
                        title: "Data Rencana Asuhan",
                        id: 'PanelDataNilaiNilaiMA',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        height: 400,
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            gridListDataNilaiNilaiMA

                        ]
                    }
            );
    return PanelDataNilaiNilaiMA;
}
;

//combo dan load database

function datainit_NilaiNilaiMA(rowdata)
{
    Ext.getCmp('TxtPopupMedrec').setValue(rowdata.KD_PASIEN);
    Ext.getCmp('TxtPopupNamaPasien').setValue(rowdata.NANilaiMA);
    Ext.getCmp('TxtPopupAlamatPasien').setValue(rowdata.ALANilaiMAT);
    Ext.getCmp('TxtPopupSpesialisasi').setValue(rowdata.SPESIALISASI);
    Ext.getCmp('TxtPopupKelas').setValue(rowdata.KELAS);
    Ext.getCmp('TxtPopupkamarPasien').setValue(rowdata.NANilaiMA_KANilaiMAR);
    caridatapasien();
}

function caridatapasien()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: {
                            Table: 'ViewAskepDetNilaiMA',
                            query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);

                            if (cst.success === true)
                            {
                                if (cst.totalrecords === 0)
                                {
                                    Ext.getCmp('Tglpelaksanaan').setValue(now);
                                    Ext.getCmp('Jampelaksanaan').setValue('');
                                    tmpkd_perawat = '';
                                    Ext.getCmp('cboPerawatNilaiNilaiMA').setValue('');
                                    Ext.getCmp('txttekanandarah').setValue('');
                                    Ext.getCmp('txtPuke').setValue('');
                                    Ext.getCmp('txtCVP').setValue('');
                                    Ext.getCmp('txtNadi').setValue('');
                                    Ext.getCmp('txtKesadaran').setValue('');
                                    Ext.getCmp('txtSuhu').setValue('');
                                    Ext.getCmp('txtWSD/DRAIN').setValue('');
                                    Ext.getCmp('txtPerifer').setValue('');
                                    Ext.getCmp('txtOral').setValue('');
                                    Ext.getCmp('txtParental').setValue('');
                                    Ext.getCmp('txtMuntah').setValue('');
                                    Ext.getCmp('txtBAK').setValue('');
                                    Ext.getCmp('txtNGT').setValue('');
                                    Ext.getCmp('txtBAB').setValue('');
                                    Ext.getCmp('txtPendarahan').setValue('');
                                    Ext.getCmp('txttindakankeperawatan').setValue('');
                                } else
                                {
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmpjam = tmphasil.JAM_OBSERVASI.substring(0, 5);
                                    Ext.getCmp('Tglpelaksanaan').setValue(tmphasil.TGL_OBSERVASI);
                                    Ext.getCmp('Jampelaksanaan').setValue(tmpjam);
                                    tmpkd_perawat = tmphasil.KD_PERAWAT;
                                    Ext.getCmp('cboPerawatNilaiNilaiMA').setValue(tmphasil.NANilaiMA_PERAWAT);
                                    Ext.getCmp('txttekanandarah').setValue(tmphasil.TEKANAN_DARAH);
                                    Ext.getCmp('txtPuke').setValue(tmphasil.DETAK_JANTUNG);
                                    Ext.getCmp('txtCVP').setValue(tmphasil.CVP);
                                    Ext.getCmp('txtNadi').setValue(tmphasil.NADI);
                                    Ext.getCmp('txtKesadaran').setValue(tmphasil.KESADARAN);
                                    Ext.getCmp('txtSuhu').setValue(tmphasil.SUHU);
                                    Ext.getCmp('txtWSD/DRAIN').setValue(tmphasil.WSD);
                                    Ext.getCmp('txtPerifer').setValue(tmphasil.PERIFER);
                                    Ext.getCmp('txtOral').setValue(tmphasil.ORAL);
                                    Ext.getCmp('txtParental').setValue(tmphasil.PARENTERAL);
                                    Ext.getCmp('txtMuntah').setValue(tmphasil.MUNTAH);
                                    Ext.getCmp('txtBAK').setValue(tmphasil.BAK);
                                    Ext.getCmp('txtNGT').setValue(tmphasil.NGT);
                                    Ext.getCmp('txtBAB').setValue(tmphasil.BAB);
                                    Ext.getCmp('txtPendarahan').setValue(tmphasil.PENDARAHAN);
                                    Ext.getCmp('txttindakankeperawatan').setValue(tmphasil.TINDAKAN_PERAWAT);
                                }
                            }
                        }
                    }
            );
}

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningObservasi('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
//                            caridatapasien();
//                            loadfilter_DetNilaiNilaiMA();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoObservasi('Proses Saving Berhasil', 'Save');
//                                caridatapasien();
//                                loadfilter_DetNilaiNilaiMA();
                            }
                            else
                            {
                                ShowPesanWarningObservasi('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
//                                caridatapasien();
//                                loadfilter_DetNilaiNilaiMA();
                            }
                            ;
                        }
                    }
            );
}
;
function paramsaving()
{
    var params =
            {
                Table: 'ViewNilaiNilaiMA',
                DANAID: tmpdana,
                NilaiMAID: Ext.getCmp('TxtMaIDNilaiNilaiMA').getValue(),
                NAMENilaiMA: Ext.getCmp('TxtNameNilaiNilaiMA').getValue(),
                LVNilaiMA: Ext.getCmp('cboLevelNilaiMA').getValue(),
                PARENTNilaiMA: Ext.getCmp('TxtParentNilaiNilaiMA').getValue(),
                TYPENilaiMA: tmptype,
                DEBITNilaiMA: tmpdebit,
                GROUPID: tmpSD,
                TGL: Ext.getCmp('TxtTahunNilaiNilaiMA').getValue(),
            };
    return params;
}

function load_NilaiNilaiMA(criteria)
{
    dataSource_NilaiNilaiMA.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewNilaiNilaiMA',
                                    param: criteria
                                }
                    }
            );
    return dataSource_NilaiNilaiMA;
}

function loadfilter_DetNilaiNilaiMA()
{
    dataSource_detNilaiNilaiMA.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetNilaiNilaiMA',
                                    param: ''
                                }
                    }
            );
    return dataSource_detNilaiNilaiMA;
}


function getCriteriaFilter_viDaftar()
{
    var strKriteria = " ng.AKHIR = 't' ";
    var tmpmedrec = Ext.getCmp('TxtCariMedrecNilaiNilaiMA').getValue();
    var tmpnama = Ext.getCmp('TxtCariNamaPasienNilaiNilaiMA').getValue();
    var tmpspesialisasi = Ext.getCmp('cboSpesialisasiNilaiNilaiMA').getValue();
    var tmpkelas = Ext.getCmp('cboKelasNilaiNilaiMA').getValue();
    var tmpkamar = Ext.getCmp('cboKamarNilaiNilaiMA').getValue();
    var tmptglawal = Ext.get('dtpTanggalawalNilaiNilaiMA').getValue();
    var tmptglakhir = Ext.get('dtpTanggalakhirNilaiNilaiMA').getValue();
    var tmptambahan = Ext.getCmp('chkTglNilaiNilaiMA').getValue()

    if (tmpmedrec !== "")
    {
        strKriteria += " AND P.KD_PASIEN " + "ilike '%" + tmpmedrec + "%' ";
    }
    if (tmpnama !== "")
    {
        strKriteria += " AND P.NANilaiMA " + "ilike '%" + tmpnama + "%' ";
    }
    if (tmpspesialisasi !== "")
    {
        if (tmpspesialisasi === '0')
        {
            strKriteria += "";
        } else
        {
            strKriteria += " AND NG.KD_SPESIAL = '" + tmpspesialisasi + "' ";
        }

    }
    if (tmpkelas !== "")
    {
        strKriteria += " AND ng.KD_UNIT_KANilaiMAR='" + tmpkelas + "' ";
    }
    if (tmpkamar !== "")
    {
        strKriteria += " AND NG.NO_KANilaiMAR= '" + tmpkamar + "' ";
    }
    if (tmptambahan === true)
    {
        strKriteria += " AND ng.Tgl_masuk Between '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
    }
    strKriteria += ' Limit 50 ';
    return strKriteria;
}

function ShowPesanWarningObservasi(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoObservasi(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningObservasi('Hubungi Admin', 'Error');
                            loadfilter_DetNilaiNilaiMA();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoObservasi('Data berhasil di hapus', 'Information');
                                caridatapasien();
                                loadfilter_DetNilaiNilaiMA();

                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningObservasi('Gagal menghapus data', 'Error');
                                caridatapasien();
                                loadfilter_DetNilaiNilaiMA();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepNilaiMA',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
            };
    return params;
}

function mComboNilaiMA()
{
    var cboNilaiMA = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 100,
                        id: 'cboNilaiMA',
                        typeAhead: true,
                        editable: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        tabIndex: 5,
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih NilaiMA',
                        fieldLabel: 'NilaiMA',
                        width: 100,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Asset'], [2, 'Liability'], [3, 'Capital'], [4, 'Revenue'], [5, 'Expense']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectNilaiMA,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectNilaiMA = b.data.Id;
                                    }
                                }
                    }
            );
    return cboNilaiMA;
}
;

function mComboLevel()
{
    var cboLevel = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 70,
                        id: 'cboLevelNilaiMA',
                        editable: false,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        tabIndex: 5,
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: '',
                        width: 50,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, '1'], [2, '2'], [3, '3'], [4, '4'], [5, '5'], [6, '6'], [7, '7'], [8, '8'], [9, '9']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectLevel,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectLevel = b.data.Id;
                                    }
                                }
                    }
            );
    return cboLevel;
}
;

function mComboSumberDana()
{
    var Field = ['ID', 'SUMBER'];
    dsPerawatNilaiMA = new WebApp.DataStore({fields: Field});
    dsPerawatNilaiMA.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'sumber_dana',
                                    Sortdir: 'ASC',
                                    target: 'ViewMataAnggaran',
                                    param: ''
                                }
                    }
            );
    var cboSumberDamaNilaiMA = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 10,
                        id: 'cboSumberDanaNilaiMA',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Sumbe Dana...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsPerawatNilaiMA,
                        valueField: 'ID',
                        displayField: 'SUMBER',
                        width: 120,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpSD = Ext.getCmp('cboSumberDanaNilaiMA').getValue();
                                    }
                                }
                    }
            );
    return cboSumberDamaNilaiMA;
}
;