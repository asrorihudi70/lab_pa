
var dsSatuanList;
var AddNewSatuan;
var selectCountSD=50;
var rowSelectedSatuan;
var SatuanLookUps;
var NamaFormSatuan=CurrentPage.title;
CurrentPage.page = getPanelSatuan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSatuan(mod_id) 
{
	
    var Field = ['kd_satuan_sat', 'satuan_sat'];
    dsSatuanList = new WebApp.DataStore({ fields: Field });
	

    var grListSatuan = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSatuan',
		    stripeRows: true,
		    store: dsSatuanList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    plugins: [new Ext.ux.grid.FilterRow()],
		    selModel: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSatuan=undefined;
							rowSelectedSatuan = dsSatuanList.getAt(row);
						}
					}
				}
			),
		    colModel: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'Kd_Satuan',
                        header: "Kode",                      
                        dataIndex: 'kd_satuan_sat',
                        sortable: true,
                        width: 30,
                        filter: {}
                    },
					{
					    id: 'Satuan',
					    header:  " Satuan",					   
					    dataIndex: 'satuan_sat',
					    width: 200,
					    sortable: true,
					    filter: {}
					}
                ]
			),
			 listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSatuan = dsSatuanList.getAt(ridx);
					if (rowSelectedSatuan != undefined) 
					{
						SatuanLookUp(rowSelectedSatuan);
					}
					else 
					{
						SatuanLookUp();
					}
				}
				// End Function # --------------
			},
		    tbar:
			[
				{
				    id: 'btnEditSatuan',
				    text: '  Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSatuan !== undefined)
						{
						    SatuanLookUp(rowSelectedSatuan.data);
						}
						else
						{
						    SatuanLookUp();
						}
				    }
				},' ','-'
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsSatuanList,
					pageSize: selectCountSD,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
		    viewConfig: { forceFit: true }
		}
	);
    //END var grListSatuan 


    var FormSatuan = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Satuan',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupSatuan',
		    items: [grListSatuan],
		    tbar:
			[
				'Kode : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Kode : ',
					id: 'txtKDSatuanFilter',                   
					width:80,
					onInit: function() { },
					listeners: 
					{
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataSatuan();
							}
						}
					}
				}, ' ','-',
				'Satuan' + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel:'Satuan : ',
					id: 'txtSatuanFilter',                   
					anchor: '95%',
					onInit: function() { },
					listeners: 
					{
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataSatuan();
							}
						}
					}
				}, ' ','-',
				// 'Maks.Data : ', ' ',mComboMaksDataSD(),
				' ','->',
				{
				    id: 'btnRefreshSatuan',				    
				    xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSatuan();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSatuan();
				}
			}
		}
	);
    //END var FormSatuan--------------------------------------------------

	RefreshDataSatuan();
	
    return FormSatuan
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SatuanLookUp(rowdata) 
{
	var lebar=600;
    SatuanLookUps = new Ext.Window   	
    (
		{
		    id: 'SatuanLookUps',
		    title: 'Satuan',
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 152,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupSatuan',
		    modal: true,
		    items: getFormEntrySatuan(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSatuan=undefined;
					RefreshDataSatuan();
				}
            }
		}
	);
    //END var SatuanLookUps------------------------------------------------------------------

    SatuanLookUps.show();
	if (rowdata === undefined)
	{
		SatuanAddNew();
	}
	else
	{
		SatuanInit(rowdata)
	}	
};

//  END FUNCTION SatuanLookUp
///------------------------------------------------------------------------------------------------------------///




function getFormEntrySatuan(lebar) 
{
    var pnlSatuan = new Ext.FormPanel
    (
		{
		    id: 'PanelSatuan',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupSatuan',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-36.5,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSatuan',
							labelWidth:85,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: 'Kode ',
								    name: 'Kode_Satuan',
								    id: 'txtKode_Satuan',
									maxLength: 3,
								    anchor: '40%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Satuan',//'Satuan ',
								    name: 'txtSatuan',
								    id: 'txtSatuan',
									maxLength: 50,
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSatuan',
				    text: 'Tambah',
				    tooltip: 'Tambah Record Baru ',
				    iconCls: 'add',				   
				    handler: function() { SatuanAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSatuan',
				    text: 'Simpan',
				    tooltip: 'Rekam Data ',
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SatuanSave(false);
						RefreshDataSatuan();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSatuan',
				    text: 'Simpan & Keluar',
				    tooltip: 'Simpan dan Keluar',
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SatuanSave(true);
						RefreshDataSatuan();
						if (x===undefined)
						{
							SatuanLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSatuan',
				    text: 'Hapus',
				    tooltip: 'Remove the selected item',
				    iconCls: 'remove',
				    handler: function() 
					{
							SatuanDelete() ;
							RefreshDataSatuan();					
					}
				},'-','->','-',
				{
					id:'btnPrintSD',
					text: ' Cetak',
					tooltip: 'Cetak',
					iconCls: 'print',					
					handler: function() 
					{
						var criteria = "";
						if(Ext.getCmp('txtKode_Satuan').getValue() != "")
						{
							criteria += " WHERE KD_SATUAN_SAT=" + Ext.getCmp('txtKode_Satuan').getValue();
						}
						ShowReport("",'011104',criteria);
					}
				}
			]
		}
	); 

    return pnlSatuan
};
//END FUNCTION getFormEntrySatuan
///------------------------------------------------------------------------------------------------------------///


function SatuanSave(mBol) 
{
	if (ValidasiEntrySatuan('Simpan Data') == 1 )
	{
		// if (AddNewSatuan == true) 
		// {
			Ext.Ajax.request
			(
				{
					url: baseURL + "index.php/anggaran_module/functionMasterSatuan/saveSatuan",
					params: getParamSatuan(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							RefreshDataSatuan();
							Ext.Msg.show
							(
								{
								   title:'Simpan Data',
								   msg: cst.pesan,
								   buttons: Ext.MessageBox.OK,
								   fn: function (btn) 
								   {			
									   if (btn =='ok') 
										{
											if(mBol == true){
												SatuanLookUps.close();
											}
											
										} 
								   },
								   icon: Ext.MessageBox.INFO
								}
							);
							// ShowPesanInfoEntryKUPPA(cst.pesan,'Simpan Data');
						}
						else if (cst.success === false )
						{
							RefreshDataSatuan();
							Ext.Msg.show
							(
								{
								   title:'Simpan Data',
								   msg: cst.pesan,
								   buttons: Ext.MessageBox.OK,
								   fn: function (btn) 
								   {			
									   if (btn =='ok') 
										{
											if(mBol == true){
												SatuanLookUps.close();
											}
											
										} 
								   },
								   icon: Ext.MessageBox.INFO
								}
							);
						}
					}
				}
			)
		// }
		/* else 
		{
			Ext.Ajax.request
			 (
				{
					url: "./Datapool.mvc/UpdateDataObj",
					params: getParamSatuan(),
					success: function(o) 
					{
						var cst = o.responseText;
						if (cst == '{"success":true}') 
						{
							ShowPesanInfoSD('Data berhasil di edit','Edit Data');
							RefreshDataSatuan();
							if(mBol === false)
							{
								Ext.get('txtKode_Satuan').dom.readOnly=true;
							};
							
						}
						else if (cst == '{"pesan":0,"success":false}' )
						{
							ShowPesanWarningSD('Data tidak berhasil di edit, data tersebut belum ada','Edit Data');
						}
						else {
							ShowPesanErrorSD('Data tidak berhasil di edit','Edit Data');
						}
					}
				}
			)
		} */
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SatuanSave
///---------------------------------------------------------------------------------------///

function SatuanDelete() 
{
	if (ValidasiEntrySatuan('Hapus Data') == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran_module/functionMasterSatuan/deleteSatuan",
				params: getParamSatuan(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							RefreshDataSatuan();
							Ext.Msg.show
							(
								{
								   title:'Hapus Data',
								   msg: 'Data berhasil di hapus',
								   buttons: Ext.MessageBox.OK,
								   fn: function (btn) 
								   {			
									   if (btn =='ok') 
										{
											SatuanAddNew()
										} 
								   },
								   icon: Ext.MessageBox.INFO
								}
							);
							// ShowPesanInfoEntryKUPPA(cst.pesan,'Simpan Data');
						}else{
							Ext.Msg.show
							(
								{
								   title:'Hapus Data',
								   msg: 'Data tidak berhasil dihapus',
								   buttons: Ext.MessageBox.OK,
								   fn: function (btn) 
								   {			
									   if (btn =='ok') 
										{
											SatuanAddNew()
										} 
								   },
								   icon: Ext.MessageBox.INFO
								}
							);
						}
				}
			}
		)
	}
};


function ValidasiEntrySatuan(modul)
{
	var x = 1;
	if (Ext.get('txtKode_Satuan').getValue() == '' || Ext.get('txtSatuan').getValue() == '')
	{
		if (Ext.get('txtKode_Satuan').getValue() == '')
		{
			ShowPesanWarningSD('Kode '+ NamaFormSatuan +' belum di isi',modul);
			x=0;
		}
		else
		{
			ShowPesanWarningSD(NamaFormSatuan + ' belum di isi',modul);
			x=0;
		}
	}
	return x;
};

function ShowPesanWarningSD(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorSD(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoSD(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

//------------------------------------------------------------------------------------
function SatuanInit(rowdata) 
{
	// console.log(rowdata.data.kd_satuan_sat,rowdata.satuan_sat);
    AddNewSatuan = false;
    Ext.get('txtKode_Satuan').dom.value = rowdata.data.kd_satuan_sat;
	Ext.get('txtKode_Satuan').dom.readOnly=true;
    Ext.get('txtSatuan').dom.value = rowdata.data.satuan_sat;

};
///---------------------------------------------------------------------------------------///



function SatuanAddNew() 
{
    AddNewSatuan = true;   
	Ext.get('txtKode_Satuan').dom.value = '';
	Ext.get('txtKode_Satuan').dom.readOnly=false;
	Ext.get('txtSatuan').dom.value = '';
	rowSelectedSatuan=undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSatuan() 
{
    var params =
	{	
		Table: 'viSatuan',   
	    kd_satuan: Ext.get('txtKode_Satuan').getValue(),
	    satuan: Ext.get('txtSatuan').getValue()	
	};
    return params
};


function RefreshDataSatuan()
{	
	/* dsSatuanList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSD, 
				Sort: 'SATUAN_SAT', 
				Sortdir: 'ASC', 
				target:'viSatuan',
				param: ''
			} 
		}
	); */
	Ext.Ajax.request ({
			url: baseURL + "index.php/anggaran_module/functionMasterSatuan/getSatuan",
			params: {
				kd_satuan_sat	:Ext.getCmp('txtKDSatuanFilter').getValue(),
				satuan_sat		:Ext.getCmp('txtSatuanFilter').getValue()
			},
			failure: function(o)
			{
				// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
			},	
			success: function(o) 
			{   
				dsSatuanList.removeAll();
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) {
					var recs=[],
						recType=dsSatuanList.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));						
					}
					dsSatuanList.add(recs);
					// gridDTLTR_PaguGNRL.getView().refresh();
				} else {
					// ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
				};
			}
		});
	rowSelectedSatuan = undefined;
	return dsSatuanList;
};

function RefreshDataSatuanFilter() 
{   
	var KataKunci;
    if (Ext.get('txtKDSatuanFilter').getValue() != '')
    { 
		KataKunci = "WHERE KD_SATUAN_SAT like '%" + Ext.get('txtKDSatuanFilter').getValue()+ "%'"; 
	}
    if (Ext.get('txtSatuanFilter').getValue() != '')
    { 
		if (KataKunci == undefined)
		{
			KataKunci = "WHERE SATUAN_SAT like '%" + Ext.get('txtSatuanFilter').getValue() + "%'";
		}
		else
		{
			KataKunci += "AND SATUAN_SAT like '%" + Ext.get('txtSatuanFilter').getValue() + "%'";
		}  
	}
        
    if (KataKunci != undefined) 
    {  
		/* dsSatuanList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSD, 
					Sort: 'SATUAN_SAT', 
					Sortdir: 'ASC', 
					target:'viSatuan',
					param: KataKunci
				}			
			}
		);         */
		Ext.Ajax.request ({
			url: baseURL + "index.php/anggaran_module/functionMasterSatuan/getSatuan",
			params: {
				text:''
			},
			failure: function(o)
			{
				// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
			},	
			success: function(o) 
			{   
				dsSatuanList.removeAll();
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) {
					var recs=[],
						recType=dsSatuanList.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));						
					}
					dsSatuanList.add(recs);
					// gridDTLTR_PaguGNRL.getView().refresh();
				} else {
					// ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
				};
			}
		});
    }
	else
	{
		RefreshDataSatuan();
	}
};



// function mComboMaksDataSD()
// {
//   var cboMaksDataSD = new Ext.form.ComboBox
// 	(
// 		{
// 			id:'cboMaksDataSD',
// 			typeAhead: true,
// 			triggerAction: 'all',
// 			lazyRender:true,
// 			mode: 'local',
// 			emptyText:'',
// 			fieldLabel: 'Maks.Data ',			
// 			width:60,
// 			store: new Ext.data.ArrayStore
// 			(
// 				{
// 					id: 0,
// 					fields: 
// 					[
// 						'Id',
// 						'displayText'
// 					],
// 				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
// 				}
// 			),
// 			valueField: 'Id',
// 			displayField: 'displayText',
// 			value:selectCountSD,
// 			listeners:  
// 			{
// 				'select': function(a,b,c)
// 				{   
// 					selectCountSD=b.data.displayText ;
// 					RefreshDataSatuan();
// 				} 
// 			}
// 		}
// 	);
// 	return cboMaksDataSD;
// };