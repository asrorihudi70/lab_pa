
var now = new Date();
var dsSpesialisasiSumberDana;
var ListHasilSumberDana;
var rowSelectedHasilSumberDana;
var dsKelasSumberDana;
var dsKamarSumberDana;
var dataSource_SumberDana;
var dataSource_detSumberDana;
var dsPerawatSumberDana;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpkd_perawat;
var rowSelected_viSD;
var selectGroup;
var selectLevel;
var radios;

CurrentPage.page = getPanelSumberDana(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelSumberDana(mod_id) {
    var Field = ['ID', 'SUMBER', 'KET'];

    dataSource_SumberDana = new WebApp.DataStore({
        fields: Field
    });

    load_SumberDana("");
    var gridListHasilSumberDana = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_SumberDana,
        anchor: '100% 70%',
        columnLines: false,
        autoScroll: false,
        border: false,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 150,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {

                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viSD = dataSource_SumberDana.getAt(ridx);
                if (rowSelected_viSD !== undefined)
                {
                    Ext.getCmp('TxtDanaIDSumberDana').setValue(rowSelected_viSD.data.ID);
                    Ext.getCmp('TxtSumberDana').setValue(rowSelected_viSD.data.SUMBER);
                    Ext.getCmp('TxtKetSumberDana').setValue(rowSelected_viSD.data.KET);
                }
                else
                {
                    //HasilSumberDanaLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilSumberDana',
                        header: 'Dana ID',
                        dataIndex: 'ID',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colMedrecViewHasilSumberDana',
                        header: 'Sumber Dana',
                        dataIndex: 'SUMBER',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colNamaViewHasilSumberDana',
                        header: 'Keterangan',
                        dataIndex: 'KET',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        }
    });
    var FormDepanSumberDana = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Sumber Dana',
        border: false,
        shadhow: true,
//        height: 100,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            gridListHasilSumberDana
        ],
        tbar: {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'New',
                    iconCls: 'New',
                    id: 'btnNew_SD',
                    handler: function ()
                    {
                        Ext.getCmp('TxtDanaIDSumberDana').setValue('');
                        Ext.getCmp('TxtSumberDana').setValue('');
                        Ext.getCmp('TxtKetSumberDana').setValue('');
                    }
                },
                {
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'save',
                    id: 'btnSimpan_SD',
                    handler: function ()
                    {
                        SavingData();
                    }
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    iconCls: 'remove',
                    id: 'btnHapus_SD',
                    handler: function ()
                    {
                        if (Ext.getCmp('TxtDanaIDSumberDana').getValue() === '')
                        {
                            ShowPesanWarningObservasi('Silahkan Pilih Data yang ingin di hapus terlebih dahulu', 'Warning');
                        } else
                        {
                            Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                if (button === 'yes') {
                                    loadMask.show();
                                    DeleteData();
                                }
                            });
                        }
                    }
                }
            ]
        },
        listeners: {
            'afterrender': function () {

            }
        }
    });
    return FormDepanSumberDana;
}
;
function getPanelPencarianPasien() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 100,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Dana ID '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtDanaIDSumberDana',
                        id: 'TxtDanaIDSumberDana',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Sumber Dana '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtSumberDana',
                        id: 'TxtSumberDana',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Keterangan '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtKetSumberDana',
                        id: 'TxtKetSumberDana',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    },
                ]
            }
        ]
    };
    return items;
}
;
//combo dan load database

function datainit_SumberDana(rowdata)
{
    Ext.getCmp('TxtPopupMedrec').setValue(rowdata.KD_PASIEN);
    Ext.getCmp('TxtPopupNamaPasien').setValue(rowdata.NAMA);
    Ext.getCmp('TxtPopupAlamatPasien').setValue(rowdata.ALAMAT);
    Ext.getCmp('TxtPopupSpesialisasi').setValue(rowdata.SPESIALISASI);
    Ext.getCmp('TxtPopupKelas').setValue(rowdata.KELAS);
    Ext.getCmp('TxtPopupkamarPasien').setValue(rowdata.NAMA_KAMAR);
    caridatapasien();
}

function caridatapasien()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: {
                            Table: 'ViewAskepDetSD',
                            query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);

                            if (cst.success === true)
                            {
                                if (cst.totalrecords === 0)
                                {
                                    Ext.getCmp('Tglpelaksanaan').setValue(now);
                                    Ext.getCmp('Jampelaksanaan').setValue('');
                                    tmpkd_perawat = '';
                                    Ext.getCmp('cboPerawatSumberDana').setValue('');
                                    Ext.getCmp('txttekanandarah').setValue('');
                                    Ext.getCmp('txtPuke').setValue('');
                                    Ext.getCmp('txtCVP').setValue('');
                                    Ext.getCmp('txtNadi').setValue('');
                                    Ext.getCmp('txtKesadaran').setValue('');
                                    Ext.getCmp('txtSuhu').setValue('');
                                    Ext.getCmp('txtWSD/DRAIN').setValue('');
                                    Ext.getCmp('txtPerifer').setValue('');
                                    Ext.getCmp('txtOral').setValue('');
                                    Ext.getCmp('txtParental').setValue('');
                                    Ext.getCmp('txtMuntah').setValue('');
                                    Ext.getCmp('txtBAK').setValue('');
                                    Ext.getCmp('txtNGT').setValue('');
                                    Ext.getCmp('txtBAB').setValue('');
                                    Ext.getCmp('txtPendarahan').setValue('');
                                    Ext.getCmp('txttindakankeperawatan').setValue('');
                                } else
                                {
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmpjam = tmphasil.JAM_OBSERVASI.substring(0, 5);
                                    Ext.getCmp('Tglpelaksanaan').setValue(tmphasil.TGL_OBSERVASI);
                                    Ext.getCmp('Jampelaksanaan').setValue(tmpjam);
                                    tmpkd_perawat = tmphasil.KD_PERAWAT;
                                    Ext.getCmp('cboPerawatSumberDana').setValue(tmphasil.NAMA_PERAWAT);
                                    Ext.getCmp('txttekanandarah').setValue(tmphasil.TEKANAN_DARAH);
                                    Ext.getCmp('txtPuke').setValue(tmphasil.DETAK_JANTUNG);
                                    Ext.getCmp('txtCVP').setValue(tmphasil.CVP);
                                    Ext.getCmp('txtNadi').setValue(tmphasil.NADI);
                                    Ext.getCmp('txtKesadaran').setValue(tmphasil.KESADARAN);
                                    Ext.getCmp('txtSuhu').setValue(tmphasil.SUHU);
                                    Ext.getCmp('txtWSD/DRAIN').setValue(tmphasil.WSD);
                                    Ext.getCmp('txtPerifer').setValue(tmphasil.PERIFER);
                                    Ext.getCmp('txtOral').setValue(tmphasil.ORAL);
                                    Ext.getCmp('txtParental').setValue(tmphasil.PARENTERAL);
                                    Ext.getCmp('txtMuntah').setValue(tmphasil.MUNTAH);
                                    Ext.getCmp('txtBAK').setValue(tmphasil.BAK);
                                    Ext.getCmp('txtNGT').setValue(tmphasil.NGT);
                                    Ext.getCmp('txtBAB').setValue(tmphasil.BAB);
                                    Ext.getCmp('txtPendarahan').setValue(tmphasil.PENDARAHAN);
                                    Ext.getCmp('txttindakankeperawatan').setValue(tmphasil.TINDAKAN_PERAWAT);
                                }
                            }
                        }
                    }
            );
}

function SavingData()
{
    if (Ext.getCmp('TxtDanaIDSumberDana').getValue() === '')
    {
        ShowPesanWarningObservasi('Dana ID Tidak Boleh Kosong', 'Warning');
    }
    else if (Ext.getCmp('TxtSumberDana').getValue() === '')
    {
        ShowPesanWarningObservasi('Sumber Dana Tidak Boleh Kosong', 'Warning');
    }
    else if (Ext.getCmp('TxtKetSumberDana').getValue() === '')
    {
        ShowPesanWarningObservasi('Keterangan Tidak Boleh Kosong', 'Warning');
    } else
    {
        Ext.Ajax.request
                (
                        {
                            url: baseURL + "index.php/main/CreateDataObj",
                            params: paramsaving(),
                            failure: function (o)
                            {
                                ShowPesanWarningObservasi('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                load_SumberDana("");
                            },
                            success: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfoObservasi('Proses Saving Berhasil', 'Save');
                                    load_SumberDana("");
                                }
                                else
                                {
                                    ShowPesanWarningObservasi('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                    load_SumberDana("");
                                }
                                ;
                            }
                        }
                );
    }


}
;
function paramsaving()
{
    var params =
            {
                Table: 'ViewSumberDana',
                query: "dana_id = '" + Ext.getCmp('TxtDanaIDSumberDana').getValue() + "'",
                ID: Ext.getCmp('TxtDanaIDSumberDana').getValue(),
                SD: Ext.getCmp('TxtSumberDana').getValue(),
                KET: Ext.getCmp('TxtKetSumberDana').getValue()
            };
    return params;
}

function load_SumberDana(criteria)
{
    dataSource_SumberDana.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewSumberDana',
                                    param: criteria
                                }
                    }
            );
    return dataSource_SumberDana;
}

function ShowPesanWarningObservasi(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoObservasi(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{

    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningObservasi('Hubungi Admin', 'Error');
                            load_SumberDana("");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoObservasi('Data berhasil di hapus', 'Information');
                                load_SumberDana("");

                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningObservasi('Gagal menghapus data', 'Error');
                                load_SumberDana("");
                            }
                            ;
                        }
                    }

            );


}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewSumberDana',
                query: "dana_id = '" + Ext.getCmp('TxtDanaIDSumberDana').getValue() + "'"
            };
    return params;
}