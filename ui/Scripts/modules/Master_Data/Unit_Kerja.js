var now = new Date();
var dsSpesialisasiUnit_Kerja;
var ListHasilUnit_Kerja;
var rowSelectedHasilUnit_Kerja;
var dsKelasUnit_Kerja;
var dsKamarUnit_Kerja;
var dataSource_Unit_Kerja;
var dataSource_detUnit_Kerja;
var dsPerawatUnit_Kerja;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpkd_perawat;
var rowSelected_viAcc;
var selectGroup;
var selectLevel;
var induk_nci;
var radios_unit_kerja;

CurrentPage.page = getPanelUnit_Kerja(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
var Account_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','kd_unit_kerja','nama_unit_kerja'],data: []});

function getPanelUnit_Kerja(mod_id) {
    var Field = ['KODE', 'PARENT', 'JENIS', 'NAMA', 'STATUS'];

    dataSource_Unit_Kerja = new WebApp.DataStore({
        fields: Field
    });

    load_Unit_Kerja("");
    var gridListHasilUnit_Kerja = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_Unit_Kerja,
        anchor: '100% 60%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 150,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    //rowSelected_viAcc = dataSource_Unit_Kerja.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viAcc = dataSource_Unit_Kerja.getAt(ridx);
                // console.log(rowSelected_viCus);
                if (rowSelected_viAcc !== undefined)
                {
                    HasilUnitKerjaLookUp(rowSelected_viAcc);
                }
                else
                {
                    HasilUnitKerjaLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilUnit_Kerja',
                        header: 'Kode',
                        dataIndex: 'KODE',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colMedrecViewHasilUnit_Kerja',
                        header: 'Unit',
                        dataIndex: 'NAMA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colNamaViewHasilUnit_Kerja',
                        header: 'Induk',
                        dataIndex: 'PARENT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25
                    }
                ]
                ),

        tbar: {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'New',
                    iconCls: 'AddRow',
                    id: 'btnNew_unit',
                    handler: function ()
                    {
                        HasilUnitKerjaLookUp();
                    }
                }        
            ]
        },
        viewConfig: {
            forceFit: true
        }
    });
    var FormDepanUnit_Kerja = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Unit Kerja',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            gridListHasilUnit_Kerja
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });
    return FormDepanUnit_Kerja;
}
;
function getPanelPencarianPasien() {
    
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 100,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtNumberUnit_Kerja',
                        id: 'TxtNumberUnit_Kerja',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Unit Kerja '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtNamaUnit_Kerja',
                        id: 'TxtNamaUnit_Kerja',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }
                                    }
                                }
                    },
                    {
                        x:10,
                        y:70,
                        xtype: 'button',
                        text: 'Cari',
						iconCls: 'find',
                        id: 'btnNew_Cus',
						width: 50,
                        handler: function ()
                        {
							loadfilter_DetUnit_Kerja();
                            // HasilPlafonLookUp();
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;

function SavingDataUnitKerja()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsavingUnitKerja(),
                        failure: function (o)
                        {
                            ShowPesanWarningObservasi('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            // caridatapasien();
                             loadfilter_DetUnit_Kerja();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoObservasi('Proses Saving Berhasil', 'Save');
                                // caridatapasien();
                                loadfilter_DetUnit_Kerja();
                            }
                            else
                            {
                                ShowPesanWarningObservasi('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                // caridatapasien();
                                loadfilter_DetUnit_Kerja();
                            }
                            ;
                        }
                    }
            );
}
;
function paramsavingUnitKerja()
{
	console.log(Ext.getCmp('cbaktiftrue'));
    var params =
            {
                Table: 'ViewUnitKerja',
                KODE: Ext.get('TxtPopupCodeUnitKerja').getValue()+Ext.get('TxtPopupCodeUnitKerja_2').getValue(),
                PARENT: induk_nci.getValue(),
                JENIS: radios_unit_kerja.items.items[0].checked,
                NAMA: Ext.get('TxtPopupNamaUnitKerja').getValue(),
                AKTIF: Ext.getCmp('cbaktiftrue').checked
            };
    return params;
}

function load_Unit_Kerja(criteria)
{
    dataSource_Unit_Kerja.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewUnitKerja',
                                    param: criteria
                                }
                    }
            );
    return dataSource_Unit_Kerja;
}

function loadfilter_DetUnit_Kerja(rowdata)
{
    var criteria = '';
	if(Ext.getCmp('TxtNumberUnit_Kerja').getValue()!=undefined || Ext.getCmp('TxtNumberUnit_Kerja').getValue()!=""){
	  criteria = ' kd_unit_kerja like ~'+ Ext.getCmp('TxtNumberUnit_Kerja').getValue() +'%~ ';
	}if(Ext.getCmp('TxtNamaUnit_Kerja').getValue()!==undefined || Ext.getCmp('TxtNamaUnit_Kerja').getValue()!==""){
		if(criteria!==""){
			criteria+=' and nama_unit_kerja like ~'+ Ext.getCmp('TxtNamaUnit_Kerja').getValue() +'%~';
		}else{
			criteria='lower(nama_unit_kerja) like lower(~'+ Ext.getCmp('TxtNamaUnit_Kerja').getValue() +'%~)';
		}
	}
	dataSource_Unit_Kerja.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewUnitKerja',
                                    param: criteria
                                }
                    }
            );
    return dataSource_Unit_Kerja;
}


function mComboPerawat()
{
    var Field = ['KODE', 'NAMA'];
    dsPerawatUnit_Kerja = new WebApp.DataStore({fields: Field});
    dsPerawatUnit_Kerja.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewPerawat',
                                    param: 'Aktif = 1 Order By nama_perawat'
                                }
                    }
            );
    var cboPerawatUnit_Kerja = new Ext.form.ComboBox
            (
                    {
                        x: 490,
                        y: 5,
                        id: 'cboPerawatUnit_Kerja',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Perawat...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsPerawatUnit_Kerja,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpkd_perawat = Ext.getCmp('cboPerawatUnit_Kerja').getValue();
                                    }
                                }
                    }
            );
    return cboPerawatUnit_Kerja;
}
;

function ShowPesanWarningObservasi(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoObservasi(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/anggaran/ViewUnitKerja/delete",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningObservasi('Hubungi Admin', 'Error');
                            loadfilter_DetUnit_Kerja();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoObservasi('Data berhasil di hapus', 'Information');
                                //caridatapasien();
                                loadfilter_DetUnit_Kerja();

                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningObservasi('Gagal menghapus data', 'Error');
                                //caridatapasien();
                                loadfilter_DetUnit_Kerja();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
				id_del : Ext.getCmp('TxtPopupCodeUnitKerja').getValue(),
                //Table: 'ViewAskepAcc',
              //  query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
            };
    return params;
}

function mComboGroup()
{
    var cboGroup = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 100,
                        id: 'cboGroup',
                        typeAhead: true,
                        editable: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        tabIndex: 5,
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Group',
                        fieldLabel: 'Group',
                        width: 100,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Asset'], [2, 'Liability'], [3, 'Capital'], [4, 'Revenue'], [5, 'Expense']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectGroup,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectGroup = b.data.Id;
                                    }
                                }
                    }
            );
    return cboGroup;
}
;



function HasilUnitKerjaLookUp(rowdata) {
    FormLookUpdetailUnitKerja = new Ext.Window({
        id: 'gridHasilUnitKerja',
        title: 'Setup Unit Kerja ',
        closeAction: 'destroy',
        width: 380,
        height: 200,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupUnitKerja()],
        listeners: {
        }
    });
    FormLookUpdetailUnitKerja.show();
    if (rowdata === undefined) {
		new_data_uk();
    } else {
//        tmpkdpasien = rowdata.KD_PASIEN;
//        tmpkdunit = rowdata.KD_UNIT;
//        tmpurutmasuk = rowdata.URUT_MASUK;
//        tmptglmasuk = rowdata.TGL_MASUK;
        datainit_UnitKerja(rowdata);
//        loadfilter_DetUnitKerja(rowdata);
    }

}
;

function new_data_uk()
{
   // console.log(rowdata.kd_customer)
    Ext.getCmp('TxtPopupCodeUnitKerja').setValue();
    Ext.getCmp('TxtPopupCodeUnitKerja_2').setValue();
    Ext.getCmp('TxtPopupNamaUnitKerja').setValue();
	radios_unit_kerja.items.items[0].setValue(true);
	Ext.getCmp('cbaktiftrue').checked=true;	
    Ext.getCmp('cbaktiftrue').setValue([true]);
	induk_nci.setValue();
}
function datainit_UnitKerja(rowdata)
{
    Ext.getCmp('TxtPopupCodeUnitKerja').setValue(rowdata.data.KODE);
    Ext.getCmp('TxtPopupNamaUnitKerja').setValue(rowdata.data.NAMA);
	induk_nci.setValue(rowdata.data.PARENT);
    if (rowdata.data.STATUS == 't'){
        Ext.getCmp('cgaktif').setValue([true]);
    }else
    {
        Ext.getCmp('cgaktif').setValue([false]);
    }
    if (rowdata.data.JENIS == '1')
	{
	alert(rowdata.data.JENIS);
    radios_unit_kerja.items.items[0].setValue(true);
	 radios_unit_kerja.items.items[1].setValue(false);
    }else if(rowdata.data.JENIS === '2')
    {
	radios_unit_kerja.items.items[1].setValue(true);
    radios_unit_kerja.items.items[0].setValue(false);
    }
	Ext.get('TxtPopupCodeUnitKerja_2').setDisabled(true);
	
    //selectLevel = rowdata.data.STATUS;
}

function formpopupUnitKerja() {
    var FrmTabs_popupUnitKerja = new Ext.Panel
            (
                    {
                        id: 'formpopupUnitKerja',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: false,
                        shadhow: true,
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelUnitKerja()
                                ],
                        fbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
											    {
													xtype: 'button',
													text: 'New',
													iconCls: 'AddRow',
													id: 'btnNew_unit2',
													handler: function ()
													{
														new_data_uk();
													//	HasilUnitKerjaLookUp();
													}
												},
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_unit_kerja',
                                                    handler: function ()
                                                    {
                                                        SavingDataUnitKerja()
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Delete',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_unit_kerja',
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });
                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupUnitKerja;
}
;



function PanelUnitKerja() {
	  radios_unit_kerja = new Ext.form.RadioGroup({
        x: 120,
        y: 10,
        xtype: 'radiogroup',
        id: 'grupradio',
        boxMaxWidth: 150,
        items: [
            {
                boxLabel: 'Unit',
                name: 'rdunit_uk',
                inputValue: '',
                checked: true,
                id: 'rd_unit_uk'
            },
            {
                boxLabel: 'Sub Unit',
                name: 'rdunit_uk',
                inputValue: '',
                id: 'rd_subunit_uk'
            }
        ],
        listeners: {
            change: function (field, newValue, oldValue) {
				if(this.items.items[0].checked==true)
				{
					//induk_nci.setValue();
					
					
				}
				if(this.items.items[0].checked==false)
				{
					
				}

            }
        }
    
    });
  
	 induk_nci= Nci.form.Combobox.autoComplete({
		        x: 120,
                y: 40,
				store	: Account_ds,
				select	: function(a,b,c){
				induk_nci.setValue(b.data.kd_unit_kerja);
				Ext.getCmp('TxtPopupCodeUnitKerja').setValue(b.data.kd_unit_kerja);	
				},
				insert	: function(o){
					return {
						kd_unit_kerja        :o.kd_unit_kerja,
						nama_unit_kerja 	: o.nama_unit_kerja,
						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_unit_kerja+'</td><td width="200">'+o.nama_unit_kerja+'</td></tr></table>'
					}
				},
				url: baseURL + "index.php/anggaran/ViewUnitKerja/accounts",
				valueField: 'penyakit',
				displayField: 'text',
				listWidth: 250
			});
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDataUnitKerja',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 500,
                                height: 300,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Jenis Unit '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    radios_unit_kerja,
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text :  'Induk '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text : ' : '
                                    },
                                    induk_nci,
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'kode_unit '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopupCodeUnitKerja',
                                        id: 'TxtPopupCodeUnitKerja',
										disabled: true,
                                        width: 75
                                    },
									{
                                        x: 200,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopupCodeUnitKerja_2',
                                        id: 'TxtPopupCodeUnitKerja_2',
                                        width: 75
                                    },
                                    {
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Nama Unit '
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 100,
                                        xtype: 'textfield',
                                        name: 'TxtPopupNamaUnitKerja',
                                        id: 'TxtPopupNamaUnitKerja',
                                        width: 200
                                    },
                                    {
                                        x: 280,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Aktif '
                                    },
                                    {
                                        x: 310,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 340,
                                        y: 40,
                                        xtype: 'checkboxgroup',
                                        id: 'cgaktif',
                                        itemCls: 'x-check-group-alt',
                                        columns: 1,
                                        boxMaxWidth: 20,
                                        items: [
                                            {xtype: 'checkbox', boxLabel: '', name: 'cbaktiftrue', id: 'cbaktiftrue'}
                                        ],
                                        listeners: {
                                            change: function (checkbox, newValue, oldValue, eOpts) {
                                            }
                                        }
                                    }
                                ]
                            }
                        ]
                    });
    return items;
}
;

function mComboLevelUnit()
{
    var cboLevel = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboLevel',
                        editable: false,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        tabIndex: 5,
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: '',
                        width: 100,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Unit Kerja'], [2, 'Sub Unit Kerja'], [3, 'Bagian']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectLevel,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectLevel = b.data.Id;
                                    }
                                }
                    }
            );
    return cboLevel;
};