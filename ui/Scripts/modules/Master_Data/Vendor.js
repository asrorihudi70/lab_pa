
var now = new Date();
var dsSpesialisasivendor;
var ListHasilvendor;
var rowSelectedHasilvendor;
var dsKelasvendor;
var dsKamarvendor;
var dataSource_vendor;
var dataSource_detvendor;
var dsPerawatvendor;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpkd_perawat;
var rowSelected_viCus;
var account_nci;
var radios;
var creteriaaa_vendor="";
var Account_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','account','name'],data: []});

var gridListHasilvendor;
CurrentPage.page = getPanelvendor(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelvendor(mod_id) {
    var Field = ['kd_vendor', 'vendor', 'contact','account', 'alamat', 'kota', 'negara', 'kd_pos',
         'telepon1', 'telepon2', 'fax', 'DUE_DAY', 'account'];
    dataSource_vendor = new WebApp.DataStore({
        fields: Field
    });

    load_vendor("");
   gridListHasilvendor = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_vendor,
        anchor: '100% 80%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 150,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    //rowSelected_viCus = dataSource_vendor.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viCus = dataSource_vendor.getAt(ridx);
                console.log(rowSelected_viCus);
                if (rowSelected_viCus !== undefined)
                {
                    HasilvendorLookUp(rowSelected_viCus);
                }
                else
                {
                    HasilvendorLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colCodeViewHasilvendor',
                        header: 'kode',
                        dataIndex: 'kd_vendor',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colNameViewHasilvendor',
                        header: 'Nama',
                        dataIndex: 'vendor',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colcontactViewHasilvendor',
                        header: 'Kontak',
                        dataIndex: 'contact',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colaccountViewHasilvendor',
                        header: 'Akun',
                        dataIndex: 'account',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        tbar: {
            xtype: 'toolbar',
            items: [
						{
							xtype: 'button',
							text: 'Baru',
							iconCls: 'AddRow',
							id: 'btnNew_ven',
							handler: function ()
							{
								HasilvendorLookUp();
							}
						}           
				]
        },
        viewConfig: {
            forceFit: true
        }
    });
    var FormDepanvendor = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'vendor',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianvendor(),
            gridListHasilvendor
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });
    return FormDepanvendor;
}
;
function getPanelPencarianvendor() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 100,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCodevendor',
                        id: 'TxtCodevendor',
						width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtNamavendor',
                        id: 'TxtNamavendor',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'find',
                        id: 'btnCari_Cus',
                        handler: function ()
                        { 

						creteriaaa_vendor_vendor();
						load_vendor(creteriaaa_vendor);
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function creteriaaa_vendor_vendor(){
	
	if (Ext.getCmp('TxtCodevendor').getValue()!=="" && Ext.getCmp('TxtNamavendor').getValue()==="" )
	{
	creteriaaa_vendor="lower(kd_vendor) like lower('%"+ Ext.getCmp('TxtCodevendor').getValue()+"%')";			
	}
	if (Ext.getCmp('TxtCodevendor').getValue()==="" && Ext.getCmp('TxtNamavendor').getValue()!=="" )
	{
	creteriaaa_vendor="lower(vendor) like lower('"+ Ext.getCmp('TxtNamavendor').getValue()+"%')";
		
	}
	if (Ext.getCmp('TxtCodevendor').getValue()!=="" && Ext.getCmp('TxtNamavendor').getValue()!=="" )
	{
	creteriaaa_vendor="lower(kd_vendor) like lower('"+ Ext.getCmp('TxtCodevendor').getValue()+"%') or lower(vendor) like lower('"+ Ext.getCmp('TxtNamavendor').getValue()+"%')";
		
	}
	if (Ext.getCmp('TxtCodevendor').getValue()==="" && Ext.getCmp('TxtNamavendor').getValue()==="" )
	{
	creteriaaa_vendor="";
	}

	return creteriaaa_vendor;
}
function HasilvendorLookUp(rowdata) {
    FormLookUpdetailvendor = new Ext.Window({
        id: 'gridHasilvendor',
        title: 'vendor Setup',
        closeAction: 'destroy',
        width: 500,
        height: 300,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupvendor()],
        listeners: {
        }
    });
    FormLookUpdetailvendor.show();
    if (rowdata === undefined||rowdata === "") {
	new_data_vendor();
    } else {
        datainit_vendor(rowdata);
    }

}
;
function formpopupvendor() {
    var FrmTabs_popupvendor = new Ext.Panel
            (
                    {
                        id: 'formpopupvendor',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: false,
                        shadhow: true,
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    Panelvendor()
                                ],
                        fbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
												{
													xtype: 'button',
													text: 'Baru',
													iconCls: 'AddRow',
													id: 'btnNew_vend3',
													handler: function ()
													{
														new_data_vendor();
													}
												},
                                                {
                                                    xtype: 'button',
                                                    text: 'Simpan',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_vendor',
                                                    handler: function ()
                                                    {
                                                        SavingData_vendor()
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Hapus',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_ven',
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });
                                                    }
                                                }
                                                
                                            ]
                                }
                    }
            );
    return FrmTabs_popupvendor;
}
;
function Panelvendor() {
	 account_nci= Nci.form.Combobox.autoComplete({
		        x: 120,
                y: 70,
				store	: Account_ds,
				select	: function(a,b,c){
								account_nci.setValue(b.data.account);
								},
				insert	: function(o){
					return {
						account        :o.account,
						name 	: o.name,
						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
					}
				},
				url: baseURL + "index.php/master_data/vendor/accounts",
				valueField: 'penyakit',
				displayField: 'text',
				listWidth: 300
			});
    
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDatavendor',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 500,
                                height: 300,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Code '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupCodevendor',
                                        id: 'TxtPopupCodevendor',
                                        width: 80,
										disabled :true,
                                        readOnly: false
                                    },
                                    {
                                        x: 210,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Kontak '
                                    },
                                    {
                                        x: 270,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 280,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupcontactvendor',
                                        id: 'TxtPopupcontactvendor',
                                        width: 200,
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Nama '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtPopupNamavendor',
                                        id: 'TxtPopupNamavendor',
                                        width: 360
                                    },
                                    {
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Alamat '
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 100,
                                        xtype: 'textfield',
                                        name: 'TxtPopupalamatvendor',
                                        id: 'TxtPopupalamatvendor',
                                        width: 360,
                                        readOnly: false
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Akun '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    account_nci,
                                    {
                                        x: 10,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'negara '
                                    },
                                    {
                                        x: 110,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 130,
                                        xtype: 'textfield',
                                        name: 'TxtPopupnegaravendor',
                                        id: 'TxtPopupnegaravendor',
                                        width: 160,
                                        readOnly: false
                                    },
                                    {
                                        x: 10,
                                        y: 160,
                                        xtype: 'label',
                                        text: 'kd_pos '
                                    },
                                    {
                                        x: 110,
                                        y: 160,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 160,
                                        xtype: 'textfield',
                                        name: 'TxtPopupkd_posvendor',
                                        id: 'TxtPopupkd_posvendor',
                                        width: 80,
                                        readOnly: false
                                    },
                                    {
                                        x: 285,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Kota '
                                    },
                                    {
                                        x: 330,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 340,
                                        y: 130,
                                        xtype: 'textfield',
                                        name: 'TxtPopupkotavendor',
                                        id: 'TxtPopupkotavendor',
                                        width: 140,
                                        readOnly: false
                                    },
                                    {
                                        x: 230,
                                        y: 160,
                                        xtype: 'label',
                                        text: 'Telepon 1 '
                                    },
                                    {
                                        x: 280,
                                        y: 160,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 290,
                                        y: 160,
                                        xtype: 'textfield',
                                        name: 'TxtPopuptelepon1vendor',
                                        id: 'TxtPopuptelepon1vendor',
                                        width: 100,
                                        readOnly: false
                                    },
                                    {
                                        x: 230,
                                        y: 190,
                                        xtype: 'label',
                                        text: 'Telepon 2 '
                                    },
                                    {
                                        x: 280,
                                        y: 190,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 290,
                                        y: 190,
                                        xtype: 'textfield',
                                        name: 'TxtPopuptelepon2vendor',
                                        id: 'TxtPopuptelepon2vendor',
                                        width: 100
                                    },
                                    {
                                        x: 10,
                                        y: 190,
                                        xtype: 'label',
                                        text: 'Fax '
                                    },
                                    {
                                        x: 110,
                                        y: 190,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 190,
                                        xtype: 'textfield',
                                        name: 'TxtPopupFaxvendor',
                                        id: 'TxtPopupFaxvendor',
                                        width: 100,
                                        readOnly: false
                                    },
                                    
                                ]
                            }
                        ]
                    });
    return items;
}
;
function datainit_vendor(rowdata)
{
   // console.log(rowdata.kd_vendor)
    Ext.getCmp('TxtPopupCodevendor').setValue(rowdata.data.kd_vendor);
    Ext.getCmp('TxtPopupNamavendor').setValue(rowdata.data.vendor);
    Ext.getCmp('TxtPopupcontactvendor').setValue(rowdata.data.contact);
    Ext.getCmp('TxtPopupalamatvendor').setValue(rowdata.data.alamat);
    Ext.getCmp('TxtPopupkotavendor').setValue(rowdata.data.kota);
    Ext.getCmp('TxtPopupnegaravendor').setValue(rowdata.data.negara);
    Ext.getCmp('TxtPopupkd_posvendor').setValue(rowdata.data.kd_pos);
    Ext.getCmp('TxtPopuptelepon1vendor').setValue(rowdata.data.telepon1);
    Ext.getCmp('TxtPopuptelepon2vendor').setValue(rowdata.data.telepon2);
    Ext.getCmp('TxtPopupFaxvendor').setValue(rowdata.data.fax);
    account_nci.setValue(rowdata.data.account);
}

function new_data_vendor()
{
   // console.log(rowdata.kd_vendor)
    Ext.getCmp('TxtPopupCodevendor').setValue();
    Ext.getCmp('TxtPopupNamavendor').setValue();
    Ext.getCmp('TxtPopupcontactvendor').setValue();
    Ext.getCmp('TxtPopupalamatvendor').setValue();
    Ext.getCmp('TxtPopupkotavendor').setValue();
    Ext.getCmp('TxtPopupnegaravendor').setValue();
    Ext.getCmp('TxtPopupkd_posvendor').setValue();
    Ext.getCmp('TxtPopuptelepon1vendor').setValue();
    Ext.getCmp('TxtPopuptelepon2vendor').setValue();
    Ext.getCmp('TxtPopupFaxvendor').setValue();
    account_nci.setValue();
}


function SavingData_vendor()
{
	if (Ext.getCmp('TxtPopupNamavendor').getValue()==="")
	{
		 ShowPesanWarningvendor('Nama vendor Belum diisi', 'Perhatian');

	}else{
		if (Ext.getCmp('TxtPopupcontactvendor').getValue()==="")
		{
		ShowPesanWarningvendor('kontak Belum diisi', 'Perhatian');	
		}else{
		if (Ext.getCmp('TxtPopuptelepon1vendor').getValue()==="" && Ext.getCmp('TxtPopuptelepon2vendor').getValue()==="")
		{
		ShowPesanWarningvendor('Telepon Belum diisi', 'Perhatian');	
		}else{
		     Ext.Ajax.request
			(
					{	
						url: baseURL + "index.php/master_data/vendor/save",
						params: paramsaving(),
						failure: function (o)
						{	loadMask.hide();
							creteriaaa_vendor_vendor();
							load_vendor(creteriaaa_vendor);
							ShowPesanWarningvendor('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
						},
						success: function (o)
						{
							creteriaaa_vendor_vendor();
							load_vendor(creteriaaa_vendor);
							var cst = Ext.decode(o.responseText);
							if (cst.success === true)
							{
								Ext.getCmp('TxtPopupCodevendor').setValue(cst.kd_ven);
								ShowPesanInfovendor('Proses Saving Berhasil', 'Save');
							}
							else
							{
								ShowPesanWarningvendor('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
							}
							;
						}
					}
			);
			
			}
			}
		
	}
}
;
function paramsaving()
{
    var params =
            {
            kd_ven:Ext.getCmp('TxtPopupCodevendor').getValue(),
			vendor:Ext.getCmp('TxtPopupNamavendor').getValue(),
			alamat:Ext.getCmp('TxtPopupalamatvendor').getValue(),
			kontak:Ext.getCmp('TxtPopupcontactvendor').getValue(),
			kota:Ext.getCmp('TxtPopupkotavendor').getValue(),
			negara:Ext.getCmp('TxtPopupnegaravendor').getValue(),
			kode_pos:Ext.getCmp('TxtPopupkd_posvendor').getValue(),
			tlp1:Ext.getCmp('TxtPopuptelepon1vendor').getValue(),
			tlp2:Ext.getCmp('TxtPopuptelepon2vendor').getValue(),
			fax:Ext.getCmp('TxtPopupFaxvendor').getValue(),
			account:account_nci.getValue(),
			};
    return params;
}


function load_vendor(criteria)
{
	//dataSource_vendor
 Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/master_data/vendor/select",
			params: {criteria:criteria},
			failure: function(o)
			{	loadMask.hide();
				ShowPesanErrorRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_vendor.removeAll();
					var recs=[],
						recType=dataSource_vendor.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dataSource_vendor.add(recs);
					
					gridListHasilvendor.getView().refresh();
					
				
				} 
			}
		}
		
	)
}



function ShowPesanWarningvendor(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfovendor(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/master_data/vendor/delete",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningvendor('Hubungi Admin', 'Error');
							creteriaaa_vendor_vendor();
							load_vendor(creteriaaa_vendor);
							
						 //  loadfilter_Detvendor();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
								new_data_vendor();
                                loadMask.hide();
                                ShowPesanInfovendor('Data berhasil di hapus', 'Information');
                                creteriaaa_vendor_vendor();
								load_vendor(creteriaaa_vendor);

                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningvendor('Gagal menghapus data', 'Error');
                            }
                            ;
                        }
                    }

            );
}
;

