// Data Source ExtJS # --------------

/**
*	Nama File 		: TRAktifitas.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Aktifitas
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

var dataSource_viAktifitas;
var selectCount_viAktifitas=50;
var NamaForm_viAktifitas="Aktifitas";
var mod_name_viAktifitas="viAktifitas";
var now_viAktifitas= new Date();
var addNew_viAktifitas;
var rowSelected_viAktifitas;
var setLookUps_viAktifitas;
var mNoAktifitas_viAktifitas='';

var CurrentData_viAktifitas =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viAktifitas(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

/**
*	Function : dataGrid_viAktifitas
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viAktifitas(mod_id_viAktifitas)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viAktifitas = 
	[
		 'NO_Aktifitas', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_Aktifitas','JAM_Aktifitas', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viAktifitas = new WebApp.DataStore
	({
        fields: FieldMaster_viAktifitas
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viAktifitas = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viAktifitas,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viAktifitas = undefined;
							rowSelected_viAktifitas = dataSource_viAktifitas.getAt(row);
							CurrentData_viAktifitas
							CurrentData_viAktifitas.row = row;
							CurrentData_viAktifitas.data = rowSelected_viAktifitas.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viAktifitas = dataSource_viAktifitas.getAt(ridx);
					if (rowSelected_viAktifitas != undefined)
					{
						setLookUp_viAktifitas(rowSelected_viAktifitas.data);
					}
					else
					{
						setLookUp_viAktifitas();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[					
					{
						id: 'colTglRO_viAktifitas',
						header:'Tanggal',
						dataIndex: '',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viAktifitas',
						header: 'Aktifitas',
						dataIndex: '',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viAktifitas',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viAktifitas',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viAktifitas != undefined)
							{
								setLookUp_viAktifitas(rowSelected_viAktifitas.data)
							}
							else
							{								
								setLookUp_viAktifitas();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viAktifitas, selectCount_viAktifitas, dataSource_viAktifitas),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viAktifitas = new Ext.Panel
    (
		{
			title: NamaForm_viAktifitas,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viAktifitas,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viAktifitas],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viAktifitas,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viAktifitas',
							value: now_viAktifitas,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viAktifitas();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viAktifitas',
							value: now_viAktifitas,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viAktifitas();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Aktifitas : ', 
							style:{'text-align':'right'},
							width: 70,
							height: 25
						},		
						viComboAktifitas_Aktifitas(150, 'cboPbf_viAktifitasFilter'),						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viAktifitas',
							handler: function() 
							{					
								DfltFilterBtn_viAktifitas = 1;
								DataRefresh_viAktifitas(getCriteriaFilterGridDataView_viAktifitas());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viAktifitas;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viAktifitas # --------------

/**
*	Function : setLookUp_viAktifitas
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viAktifitas(rowdata)
{
    var lebar = 985;
    setLookUps_viAktifitas = new Ext.Window
    (
    {
        id: 'SetLookUps_viAktifitas',
		name: 'SetLookUps_viAktifitas',
        title: NamaForm_viAktifitas, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viAktifitas(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viAktifitas=undefined;
                //datarefresh_viAktifitas();
				mNoAktifitas_viAktifitas = '';
            }
        }
    }
    );

    setLookUps_viAktifitas.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viAktifitas();
		// Ext.getCmp('btnDelete_viAktifitas').disable();	
    }
    else
    {
        // datainit_viAktifitas(rowdata);
    }
}
// End Function setLookUpGridDataView_viAktifitas # --------------

/**
*	Function : getFormItemEntry_viAktifitas
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viAktifitas(lebar,rowdata)
{
    var pnlFormDataBasic_viAktifitas = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInput_viAktifitas(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viAktifitas(lebar),
				//-------------- ## --------------				
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viAktifitas',
						handler: function(){
							dataaddnew_viAktifitas();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viAktifitas',
						handler: function()
						{
							datasave_viAktifitas(addNew_viAktifitas);
							datarefresh_viAktifitas();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viAktifitas',
						handler: function()
						{
							var x = datasave_viAktifitas(addNew_viAktifitas);
							datarefresh_viAktifitas();
							if (x===undefined)
							{
								setLookUps_viAktifitas.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viAktifitas',
						handler: function()
						{
							datadelete_viAktifitas();
							datarefresh_viAktifitas();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viAktifitas;
}
// End Function getFormItemEntry_viAktifitas # --------------

/**
*	Function : getItemPanelInput_viAktifitas
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInput_viAktifitas(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[					
			{
				xtype: 'compositefield',
				fieldLabel: 'Bulan/Tahun',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[					
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viAktifitas',
						value: now_viAktifitas,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viAktifitas();								
								} 						
							}
						}
					},									                                        				
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Aktifitas',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					viComboAktifitas_Aktifitas(825, 'cboAktifitas_viAktifitas')					
				]
			},			
		]
	};
    return items;
};
// End Function getItemPanelInput_viAktifitas # --------------


/**
*	Function : getItemGridTransaksi_viAktifitas
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viAktifitas(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 455, //347,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viAktifitas()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viAktifitas # --------------

/**
*	Function : gridDataViewEdit_viAktifitas
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viAktifitas()
{
    
    chkSelected_viAktifitas = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viAktifitas',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viAktifitas = 
	[
	];
	
    dsDataGrdJab_viAktifitas= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viAktifitas
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viAktifitas,
        height: 450,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viAktifitas,			
			{			
				dataIndex: '',
				header: 'No',
				sortable: true,
				width: 50
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'KUNJUNGAN RAWAT DARURAT',
				sortable: true,
				width: 200,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Total Rujukan',
				sortable: true,
				width: 90,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'Total Non Rujukan',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'Tndk Lnjt Pel Dirawat',
				sortable: true,
				width: 120,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'Tndk Lnjt Pel Dirujuk',
				sortable: true,
				width: 120,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'Tndk Lnjt Pel Pulang',
				sortable: true,
				width: 120,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'Mati Di UGD',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'DOA',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
        ],
        plugins:chkSelected_viAktifitas,
    }    
    return items;
}
// End Function gridDataViewEdit_viAktifitas # --------------


function viComboAktifitas_Aktifitas(lebar,Nama_ID)
{
    var Field_Aktifitas = ['KD_Aktifitas', 'KD_CUSTOMER', 'Aktifitas'];
    dsAktifitas_Aktifitas = new WebApp.DataStore({fields: Field_Aktifitas});
    
	// viRefresh_Aktifitas();
	
    var cboAktifitas_Aktifitas = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Aktifitas',
			valueField: 'KD_Aktifitas',
            displayField: 'Aktifitas',
			emptyText:'KUNJUNGAN RAWAT DARURAT',
			store: dsAktifitas_Aktifitas,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboAktifitas_Aktifitas;
}