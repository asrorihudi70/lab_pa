// Data Source ExtJS # --------------

/**
*	Nama File 		: TRDataRS.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Data  RS
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

var dataSource_viDataRS;
var selectCount_viDataRS=50;
var NamaForm_viDataRS="Data RS";
var mod_name_viDataRS="viDataRS";
var now_viDataRS= new Date();
var addNew_viDataRS;
var rowSelected_viDataRS;
var setLookUps_viDataRS;
var mNoDataRS_viDataRS='';

var CurrentData_viDataRS =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viDataRS(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

/**
*	Function : dataGrid_viDataRS
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viDataRS(mod_id_viDataRS)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viDataRS = 
	[
		 'NO_DataRS', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_DataRS','JAM_DataRS', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viDataRS = new WebApp.DataStore
	({
        fields: FieldMaster_viDataRS
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viDataRS = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viDataRS,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viDataRS = undefined;
							rowSelected_viDataRS = dataSource_viDataRS.getAt(row);
							CurrentData_viDataRS
							CurrentData_viDataRS.row = row;
							CurrentData_viDataRS.data = rowSelected_viDataRS.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viDataRS = dataSource_viDataRS.getAt(ridx);
					if (rowSelected_viDataRS != undefined)
					{
						setLookUp_viDataRS(rowSelected_viDataRS.data);
					}
					else
					{
						setLookUp_viDataRS();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[					
					{
						id: 'colTglRO_viDataRS',
						header:'Tanggal',
						dataIndex: '',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viDataRS',
						header: 'DataRS',
						dataIndex: '',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viDataRS',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viDataRS',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viDataRS != undefined)
							{
								setLookUp_viDataRS(rowSelected_viDataRS.data)
							}
							else
							{								
								setLookUp_viDataRS();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viDataRS, selectCount_viDataRS, dataSource_viDataRS),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viDataRS = new Ext.Panel
    (
		{
			title: NamaForm_viDataRS,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viDataRS,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viDataRS],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viDataRS,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viDataRS',
							value: now_viDataRS,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viDataRS();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viDataRS',
							value: now_viDataRS,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viDataRS();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'DataRS : ', 
							style:{'text-align':'right'},
							width: 70,
							height: 25
						},		
						viComboDataRS_DataRS(150, 'cboPbf_viDataRSFilter'),						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viDataRS',
							handler: function() 
							{					
								DfltFilterBtn_viDataRS = 1;
								DataRefresh_viDataRS(getCriteriaFilterGridDataView_viDataRS());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viDataRS;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viDataRS # --------------

/**
*	Function : setLookUp_viDataRS
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viDataRS(rowdata)
{
    var lebar = 985;
    setLookUps_viDataRS = new Ext.Window
    (
    {
        id: 'SetLookUps_viDataRS',
		name: 'SetLookUps_viDataRS',
        title: NamaForm_viDataRS, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viDataRS(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viDataRS=undefined;
                //datarefresh_viDataRS();
				mNoDataRS_viDataRS = '';
            }
        }
    }
    );

    setLookUps_viDataRS.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viDataRS();
		// Ext.getCmp('btnDelete_viDataRS').disable();	
    }
    else
    {
        // datainit_viDataRS(rowdata);
    }
}
// End Function setLookUpGridDataView_viDataRS # --------------

/**
*	Function : getFormItemEntry_viDataRS
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viDataRS(lebar,rowdata)
{
    var pnlFormDataBasic_viDataRS = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInput_viDataRS(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viDataRS(lebar),
				//-------------- ## --------------				
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viDataRS',
						handler: function(){
							dataaddnew_viDataRS();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viDataRS',
						handler: function()
						{
							datasave_viDataRS(addNew_viDataRS);
							datarefresh_viDataRS();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viDataRS',
						handler: function()
						{
							var x = datasave_viDataRS(addNew_viDataRS);
							datarefresh_viDataRS();
							if (x===undefined)
							{
								setLookUps_viDataRS.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viDataRS',
						handler: function()
						{
							datadelete_viDataRS();
							datarefresh_viDataRS();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viDataRS;
}
// End Function getFormItemEntry_viDataRS # --------------

/**
*	Function : getItemPanelInput_viDataRS
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInput_viDataRS(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[					
			{
				xtype: 'compositefield',
				fieldLabel: 'Bulan/Tahun',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[					
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viDataRS',
						value: now_viDataRS,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viDataRS();								
								} 						
							}
						}
					},									                                        				
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Data Rumah Sakit',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					viComboDataRS_DataRS(825, 'cboDataRS_viDataRS')					
				]
			},			
		]
	};
    return items;
};
// End Function getItemPanelInput_viDataRS # --------------


/**
*	Function : getItemGridTransaksi_viDataRS
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viDataRS(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 455, //347,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viDataRS()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viDataRS # --------------

/**
*	Function : gridDataViewEdit_viDataRS
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viDataRS()
{
    
    chkSelected_viDataRS = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viDataRS',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viDataRS = 
	[
	];
	
    dsDataGrdJab_viDataRS= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viDataRS
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viDataRS,
        height: 450,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viDataRS,			
			{			
				dataIndex: '',
				header: 'No',
				sortable: true,
				width: 50
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'DATA DASAR RUMAH SAKIT',
				sortable: true,
				width: 200,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Keterangan',
				sortable: true,
				width: 200,
				renderer: function(v, params, record) 
				{
					
				}	
			},						
			//-------------- ## --------------			
        ],
        plugins:chkSelected_viDataRS,
    }    
    return items;
}
// End Function gridDataViewEdit_viDataRS # --------------


function viComboDataRS_DataRS(lebar,Nama_ID)
{
    var Field_DataRS = ['KD_DataRS', 'KD_CUSTOMER', 'DataRS'];
    dsDataRS_DataRS = new WebApp.DataStore({fields: Field_DataRS});
    
	// viRefresh_DataRS();
	
    var cboDataRS_DataRS = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'DataRS',
			valueField: 'KD_DataRS',
            displayField: 'DataRS',
			emptyText:'DATA DASAR RUMAH SAKIT',
			store: dsDataRS_DataRS,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboDataRS_DataRS;
}