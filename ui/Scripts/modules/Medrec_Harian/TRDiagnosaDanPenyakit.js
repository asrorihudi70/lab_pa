// Data Source ExtJS # --------------
// Deklarasi Variabel pada Diagnosa dan Kode Penyakit # --------------

var dataSource_viDiagnosaDanKdPenyakit;
var selectCount_viDiagnosaDanKdPenyakit=50;
var NamaForm_viDiagnosaDanKdPenyakit="Diagnosa dan Kode Penyakit";
var mod_name_viDiagnosaDanKdPenyakit="viDiagnosaDanKdPenyakit";
var now_viDiagnosaDanKdPenyakit= new Date();
var addNew_viDiagnosaDanKdPenyakit;
var rowSelected_viDiagnosaDanKdPenyakit;
var setLookUps_viDiagnosaDanKdPenyakit;
var mNoKunjungan_viDiagnosaDanKdPenyakit='';

var CurrentData_viDiagnosaDanKdPenyakit =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viDiagnosaDanKdPenyakit(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Rawat Jalan # --------------

// Start Project Kasir Rawat Jalan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viDiagnosaDanKdPenyakit
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viDiagnosaDanKdPenyakit(mod_id_viDiagnosaDanKdPenyakit)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viDiagnosaDanKdPenyakit = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viDiagnosaDanKdPenyakit = new WebApp.DataStore
	({
        fields: FieldMaster_viDiagnosaDanKdPenyakit
    });
    
    // Grid Kasir Rawat Jalan # --------------
	var GridDataView_viDiagnosaDanKdPenyakit = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viDiagnosaDanKdPenyakit,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viDiagnosaDanKdPenyakit = undefined;
							rowSelected_viDiagnosaDanKdPenyakit = dataSource_viDiagnosaDanKdPenyakit.getAt(row);
							CurrentData_viDiagnosaDanKdPenyakit
							CurrentData_viDiagnosaDanKdPenyakit.row = row;
							CurrentData_viDiagnosaDanKdPenyakit.data = rowSelected_viDiagnosaDanKdPenyakit.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viDiagnosaDanKdPenyakit = dataSource_viDiagnosaDanKdPenyakit.getAt(ridx);
					if (rowSelected_viDiagnosaDanKdPenyakit != undefined)
					{
						setLookUp_viDiagnosaDanKdPenyakit(rowSelected_viDiagnosaDanKdPenyakit.data);
					}
					else
					{
						setLookUp_viDiagnosaDanKdPenyakit();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir Rawat Jalan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_viDiagnosaDanKdPenyakit',
						header: 'No. Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 25,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMPASIEN_viDiagnosaDanKdPenyakit',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 45,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colALAMAT_viDiagnosaDanKdPenyakit',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					//-------------- ## --------------
					{
						id: 'colTglKunj_viDiagnosaDanKdPenyakit',
						header:'Tgl Masuk',
						dataIndex: ' ',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							//return ShowDate(record.data.TGL_OPERASI);
						}
					}

				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viDiagnosaDanKdPenyakit',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viDiagnosaDanKdPenyakit',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viDiagnosaDanKdPenyakit != undefined)
							{
								setLookUp_viDiagnosaDanKdPenyakit(rowSelected_viDiagnosaDanKdPenyakit.data)
							}
							else
							{								
								setLookUp_viDiagnosaDanKdPenyakit();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viDiagnosaDanKdPenyakit, selectCount_viDiagnosaDanKdPenyakit, dataSource_viDiagnosaDanKdPenyakit),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viDiagnosaDanKdPenyakit = new Ext.Panel
    (
		{
			title: NamaForm_viDiagnosaDanKdPenyakit,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viDiagnosaDanKdPenyakit,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viDiagnosaDanKdPenyakit],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viDiagnosaDanKdPenyakit,
		            columns: 8,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            
						{ 
							xtype: 'tbtext', 
							text: 'No. Medrec : ', 
							style:{'text-align':'left'},
							width: 75,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMedrec_viDiagnosaDanKdPenyakit',							
							emptyText: 'No. Medrec',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viDiagnosaDanKdPenyakit = 1;
										DataRefresh_viDiagnosaDanKdPenyakit(getCriteriaFilterGridDataView_viDiagnosaDanKdPenyakit());
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viDiagnosaDanKdPenyakit',
							emptyText: 'Nama',
							width: 150,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viDiagnosaDanKdPenyakit = 1;
										DataRefresh_viDiagnosaDanKdPenyakit(getCriteriaFilterGridDataView_viDiagnosaDanKdPenyakit());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Alamat : ', 
							style:{'text-align':'right'},
							width: 35,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_ALAMAT_viDiagnosaDanKdPenyakit',
							emptyText: 'Alamat',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viDiagnosaDanKdPenyakit = 1;
										DataRefresh_viDiagnosaDanKdPenyakit(getCriteriaFilterGridDataView_viDiagnosaDanKdPenyakit());								
									} 						
								}
							}
						},
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Masuk : ', 
							style:{'text-align':'left'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_viDiagnosaDanKdPenyakit',
							value: now_viDiagnosaDanKdPenyakit,
							format: 'd/M/Y',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viDiagnosaDanKdPenyakit();								
									} 						
								}
							}
						},						
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'center'},
							width: 50,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viDiagnosaDanKdPenyakit',
							value: now_viDiagnosaDanKdPenyakit,
							format: 'd/M/Y',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viDiagnosaDanKdPenyakit();								
									} 						
								}
							}
						},						
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},														
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:50,
							id: 'BtnFilterGridDataView_viDiagnosaDanKdPenyakit',
							handler: function() 
							{					
								DfltFilterBtn_viDiagnosaDanKdPenyakit = 1;
								DataRefresh_viDiagnosaDanKdPenyakit(getCriteriaFilterGridDataView_viDiagnosaDanKdPenyakit());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viDiagnosaDanKdPenyakit;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viDiagnosaDanKdPenyakit # --------------

/**
*	Function : setLookUp_viDiagnosaDanKdPenyakit
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viDiagnosaDanKdPenyakit(rowdata)
{
    var lebar = 860;
    setLookUps_viDiagnosaDanKdPenyakit = new Ext.Window
    (
    {
        id: 'SetLookUps_viDiagnosaDanKdPenyakit',
		name: 'SetLookUps_viDiagnosaDanKdPenyakit',
        title: NamaForm_viDiagnosaDanKdPenyakit, 
        closeAction: 'destroy',        
        width: 875,
        height: 650,//580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viDiagnosaDanKdPenyakit(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viDiagnosaDanKdPenyakit=undefined;
                //datarefresh_viDiagnosaDanKdPenyakit();
				mNoKunjungan_viDiagnosaDanKdPenyakit = '';
            }
        }
    }
    );

    setLookUps_viDiagnosaDanKdPenyakit.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viDiagnosaDanKdPenyakit();
		// Ext.getCmp('btnDelete_viDiagnosaDanKdPenyakit').disable();	
    }
    else
    {
        // datainit_viDiagnosaDanKdPenyakit(rowdata);
    }
}
// End Function setLookUpGridDataView_viDiagnosaDanKdPenyakit # --------------

/**
*	Function : getFormItemEntry_viDiagnosaDanKdPenyakit
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viDiagnosaDanKdPenyakit(lebar,rowdata)
{
    var pnlFormDataBasic_viDiagnosaDanKdPenyakit = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			// items:[getItemPanelInputBiodata_viDiagnosaDanKdPenyakit(lebar), getItemDataKunjungan_viDiagnosaDanKdPenyakit(lebar), getItemDataDetailDiagnosaDanKdPenyakit_viDiagnosaDanKdPenyakit(lebar)], ],
			items:[getItemPanelInputBiodata_viDiagnosaDanKdPenyakit(lebar), getItemDataKunjungan_viDiagnosaDanKdPenyakit(lebar), getItemDataDetailDiagnosaDanKdPenyakit_viDiagnosaDanKdPenyakit(lebar),getItemDataRiwayatPenyakit_viDiagnosaDanKdPenyakit(lebar)], 			
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viDiagnosaDanKdPenyakit',
						handler: function(){
							dataaddnew_viDiagnosaDanKdPenyakit();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viDiagnosaDanKdPenyakit',
						handler: function()
						{
							datasave_viDiagnosaDanKdPenyakit(addNew_viDiagnosaDanKdPenyakit);
							datarefresh_viDiagnosaDanKdPenyakit();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viDiagnosaDanKdPenyakit',
						handler: function()
						{
							var x = datasave_viDiagnosaDanKdPenyakit(addNew_viDiagnosaDanKdPenyakit);
							datarefresh_viDiagnosaDanKdPenyakit();
							if (x===undefined)
							{
								setLookUps_viDiagnosaDanKdPenyakit.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viDiagnosaDanKdPenyakit',
						handler: function()
						{
							datadelete_viDiagnosaDanKdPenyakit();
							datarefresh_viDiagnosaDanKdPenyakit();
							
						}
					},
					
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viDiagnosaDanKdPenyakit;
}
// End Function getFormItemEntry_viDiagnosaDanKdPenyakit # --------------

/**
*	Function : getItemPanelInputBiodata_viDiagnosaDanKdPenyakit
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viDiagnosaDanKdPenyakit(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[		
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Medrec',
				name: 'compNoMedrec_viDiagnosaDanKdPenyakit',
				id: 'compNoMedrec_viDiagnosaDanKdPenyakit',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 100,
						name: 'txtNoMedrec_viDiagnosaDanKdPenyakit',
						id: 'txtNoMedrec_viDiagnosaDanKdPenyakit',
						emptyText: 'No. Medrec',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 90,								
						value: 'Nama Pasien:',
						fieldLabel: 'Label',
					},															
					{
						xtype: 'textfield',					
						fieldLabel: 'Nama Pasien',								
						width: 512,
						name: 'txtNmPasien_viDiagnosaDanKdPenyakit',
						id: 'txtNmPasien_viDiagnosaDanKdPenyakit',
						emptyText: 'Nama Pasien',
					},										
				]
			},
			//-------------- ## --------------	
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
								xtype: 'displayfield',				
								width: 70,								
								value: 'Tgl. Lahir',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 50,								
								value: 'Thn',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 30,								
								value: 'Bln',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 30,								
								value: 'Hari',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 80,								
								value: 'Jenis Kelamin',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 65,								
								value: 'G. Darah',
							},				            
				        ]
				    },
		        ]
		    },
			//-------------- ## --------------
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhirEditData_viDiagnosaDanKdPenyakit',
		                        emptyText: '01',
		                        width: 20,
		                    },
		                    //-------------- ## -------------- 
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir2EditData_viDiagnosaDanKdPenyakit',
		                        emptyText: '01',
		                        width: 20,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtThnLhirEditData_viDiagnosaDanKdPenyakit',
		                        emptyText: '2014',
		                        width: 35,
		                    },
				            //-------------- ## --------------
							 {
		                        xtype: 'textfield',
		                        id: 'TxtThnLhir3EditData_viDiagnosaDanKdPenyakit',
		                        emptyText: '20',
		                        width: 20,
		                    },
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir3EditData_viDiagnosaDanKdPenyakit',
		                        emptyText: '01',
		                        width: 20,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir4EditData_viDiagnosaDanKdPenyakit',
		                        emptyText: '01',
		                        width: 20,
		                    },
				            //-------------- ## --------------
				            viComboJenisKelamin_DiagnosaDanPenyakit(80,'CmboJenisKelaminEditData_DiagnosaDanPenyakit'),
				            //-------------- ## --------------
				            viComboGolDarah_DiagnosaDanPenyakit(60,'CmboGlgnDrhEditData_DiagnosaDanPenyakit'),
				            //-------------- ## --------------
				            //-------------- ## --------------
							{
								xtype: 'displayfield',				
								width: 325,
								value: ' ',
							},
				            //-------------- ## --------------
							{
								xtype: 'displayfield',				
								width: 70,								
								value: 'Tgl. Masuk:',
							},
				            //-------------- ## --------------							
				            {
								xtype: 'textfield',
								flex: 1,
								width : 25,	
								name: 'txtTglMasukHr_viDiagnosaDanKdPenyakit',
								id: 'txtTglMasukHr_viDiagnosaDanKdPenyakit',
								emptyText: '01',
								listeners:
								{
									'specialkey': function() 
									{
										if (Ext.EventObject.getKey() === 13) 
										{
										};
									}
								}
							},
							//-------------- ## --------------
							{
								xtype: 'textfield',
								flex: 1,
								width : 25,	
								name: 'txtTglMasukBl_viDiagnosaDanKdPenyakit',
								id: 'txtTglMasukBl_viDiagnosaDanKdPenyakit',
								emptyText: '01',
								listeners:
								{
									'specialkey': function() 
									{
										if (Ext.EventObject.getKey() === 13) 
										{
										};
									}
								}
							},
							//-------------- ## --------------
							{
								xtype: 'textfield',
								flex: 1,
								width : 35,	
								name: 'txtTglMasukTh_viDiagnosaDanKdPenyakit',
								id: 'txtTglMasukTh_viDiagnosaDanKdPenyakit',
								emptyText: '2014',
								listeners:
								{
									'specialkey': function() 
									{
										if (Ext.EventObject.getKey() === 13) 
										{
										};
									}
								}
							},
							//-------------- ## --------------
							{ 
								xtype: 'tbspacer',
								height: 25,
							},
							//-------------- ## --------------
				        ]
				    },
		        ]
		    },			
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Alamat ',
				name: 'compAlamat_viDiagnosaDanKdPenyakit',
				id: 'compAlamat_viDiagnosaDanKdPenyakit',
				items: 
				[					
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 235,
						name: 'txtAlamat_viDiagnosaDanKdPenyakit',
						id: 'txtAlamat_viDiagnosaDanKdPenyakit',
						emptyText: 'Alamat'
					}
					//-------------- ## --------------
				]
			},
			// -------------- ## --------------				
			{
				xtype: 'compositefield',
				fieldLabel: 'Poliklinik ',
				name: 'compPoliklinik_viDiagnosaDanKdPenyakit',
				id: 'compPoliklinik_viDiagnosaDanKdPenyakit',
				items: 
				[					
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 235,
						name: 'txtPoliklinik_viDiagnosaDanKdPenyakit',
						id: 'txtPoliklinik_viDiagnosaDanKdPenyakit',
						emptyText: 'Poliklinik Anak'
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 295,								
						value: ' ',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 70,								
						value: 'Cara Keluar:',
					},
					viComboCaraKeluar_DiagnosaDanPenyakit(95,'CboCaraKeluar_DiagnosaDanPenyakit')
				]
			}
			// -------------- ## --------------				
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viDiagnosaDanKdPenyakit # --------------

/**
*	Function : getItemDataKunjungan_viDiagnosaDanKdPenyakit
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataKunjungan_viDiagnosaDanKdPenyakit(lebar) 
{
	var rdCatatan_viDiagnosaDanPenyakit = new Ext.form.RadioGroup
	({  
		fieldLabel: '',  
		columns: 3, 
		width: 450,
		border:false,
		items: 
		[  
			{
				boxLabel: 'Lain-lain', 
				width:40,
				id: 'rdLain_viDiagnosaDanPenyakit', 
				name: 'rdLain_viDiagnosaDanPenyakit', 
				inputValue: '1'
			},  
			//-------------- ## --------------
			{
				boxLabel: 'Neoplasma', 
				width:70,
				id: 'rdNeoplasma_viDiagnosaDanPenyakit', 
				name: 'rdNeoplasma_viDiagnosaDanPenyakit', 
				inputValue: '2'
			}, 
			//-------------- ## --------------
			{
				boxLabel: 'Kecelakaan/Keracunan', 
				width:400,
				id: 'rdKecelakaan_viDiagnosaDanPenyakit', 
				name: 'rdKecelakaan_viDiagnosaDanPenyakit', 
				inputValue: '3'
			} 
			//-------------- ## --------------
		]  
	});
	
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height: 235,//250,//250,//400,
	    items:
		[				
			{
				xtype: 'panel',
				// title: 'Data Diagnosa',
				border:false,	
				height: 390, //375,
				items: 
				[
					{
						xtype: 'tabpanel',
						activeTab: 0,		
						height: 220, //260,
						items: 
						[
							{
								xtype: 'panel',
								title: 'Data Diagnosa',				
								padding: '4px',
								items: 
								[										
									{
										xtype: 'compositefield',
										fieldLabel: 'Catatan Lain ',
										// name: 'compCatatan_viDiagnosaDanKdPenyakit',
										// id: 'compCatatan_viDiagnosaDanKdPenyakit',
										// anchor: '100%',
										width: 600,//500,
										items: 
										[
											{
												xtype: 'displayfield',				
												width: 90,								
												value: 'Catatan Lain :',
												fieldLabel: 'Label',
												id: 'lblCatLain_viDiagnosaDanKdPenyakit',
												name: 'lblCatLain_viDiagnosaDanKdPenyakit'
											},
											rdCatatan_viDiagnosaDanPenyakit,
										],										
									},
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									gridDataTrans_viDiagnosaDanKdPenyakit(),
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Morfologi Neoplasma ',
										name: 'compMorfologi_viDiagnosaDanKdPenyakit',
										id: 'compMorfologi_viDiagnosaDanKdPenyakit',
										items: 
										[
											{
												xtype: 'displayfield',				
												width: 200,								
												value: 'Morfologi Neoplasma :',
												fieldLabel: 'Label',
												id: 'lblMorfologi_viDiagnosaDanKdPenyakit',
												name: 'lblMorfologi_viDiagnosaDanKdPenyakit',
												style:{ 'text-align':'right'},
											},											
											{
												xtype: 'textfield',
												id: 'txtMorfologi_viDiagnosaDanKdPenyakit',
												name: 'txtMorfologi_viDiagnosaDanKdPenyakit',												
												width: 602,												
												emptyText:'Morfologi Neoplasma..',
												readOnly: false,
											},											
										],										
									},
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'SebabLuar ',
										name: 'compSebabLuar_viDiagnosaDanKdPenyakit',
										id: 'compSebabLuar_viDiagnosaDanKdPenyakit',
										items: 
										[
											{
												xtype: 'displayfield',				
												width: 200,								
												value: 'Sebab Luar Kecelakaan, Keracunan :',
												fieldLabel: 'Label',
												id: 'lblSebabLuar_viDiagnosaDanKdPenyakit',
												name: 'lblSebabLuar_viDiagnosaDanKdPenyakit'
											},
											{
												xtype: 'textfield',
												id: 'txtSebabLuar_viDiagnosaDanKdPenyakit',
												name: 'txtSebabLuar_viDiagnosaDanKdPenyakit',												
												width: 602,												
												emptyText:'Morfologi Neoplasma..',
												readOnly: false,
											},											
											
										]
									},
									{ 
										xtype: 'tbspacer',									
										height: 5
									},									
									{
										xtype: 'compositefield',
										fieldLabel: 'Kecelakaan ',
										name: 'compKecelakaan_viDiagnosaDanKdPenyakit',
										id: 'compKecelakaan_viDiagnosaDanKdPenyakit',
										items: 
										[
											{
												xtype: 'displayfield',				
												width: 70,								
												value: 'Kecelakaan :',
												fieldLabel: 'Label',
												id: 'lblKecelakaan_viDiagnosaDanKdPenyakit',
												name: 'lblKecelakaan_viDiagnosaDanKdPenyakit'
											},
											viComboKecelakaan_viDiagnosaDanKdPenyakit(733, 'cboKecelakaan_viDiagnosaDanKdPenyakit'),
										]
									},
									{ 
										xtype: 'tbspacer',									
										height: 5
									},									
									{
										xtype: 'compositefield',
										fieldLabel: 'Dokter ',
										name: 'compDokter_viDiagnosaDanKdPenyakit',
										id: 'compDokter_viDiagnosaDanKdPenyakit',
										items: 
										[
											{
												xtype: 'displayfield',				
												width: 70,								
												value: 'Dokter :',
												fieldLabel: 'Label',
												id: 'lblDokter_viDiagnosaDanKdPenyakit',
												name: 'lblDokter_viDiagnosaDanKdPenyakit'
											},
											{
												xtype: 'textfield',
												id: 'txtKdDokter_viDiagnosaDanKdPenyakit',
												name: 'txtKdDokter_viDiagnosaDanKdPenyakit',												
												width: 100,												
												emptyText:'',
												readOnly: true,
											},
											{
												xtype: 'textfield',
												id: 'txtNamaDokter_viDiagnosaDanKdPenyakit',
												name: 'txtNamaDokter_viDiagnosaDanKdPenyakit',												
												width: 627,												
												emptyText:'Nama Dokter..',
												readOnly: true,
											}
										]
									},
								]
							},														
						]
					},									
				]
			}
		]
	};
    return items;
};
// End Function getItemDataKunjungan_viDiagnosaDanKdPenyakit # --------------

function gridDataTrans_viDiagnosaDanKdPenyakit()
{
    
    var FieldGrdKasir_viDiagnosaDanKdPenyakit = 
	[
	];
	
    dsDataGrdJab_viDiagnosaDanKdPenyakit= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viDiagnosaDanKdPenyakit
    });  
	
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viDiagnosaDanKdPenyakit,
        height: 50,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'No. ICD',
				sortable: true,
				width: 100
			},			
			{
				dataIndex: '',
				header: 'Penyakit',
				sortable: true,
				width: 200
			},
			{
				dataIndex: '',
				header: 'Diagnosa',
				sortable: true,
				width: 100
			},
			{
				dataIndex: '',
				header: 'Kasus',
				sortable: true,
				width: 100
			},
        ]
    }    
    return items;
}

function gridDataRiwayatPenyakit_viDiagnosaDanKdPenyakit()
{
    
    var FieldGrdRiwayatPenyakit_viDiagnosaDanKdPenyakit = 
	[
	];
	
    dsDataGrdRiwayat_viDiagnosaDanKdPenyakit= new WebApp.DataStore
	({
        fields: FieldGrdRiwayatPenyakit_viDiagnosaDanKdPenyakit
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdRiwayat_viDiagnosaDanKdPenyakit,
        height: 35,//100,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: ' ',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: ' ',
				sortable: true,
				width: 500
			}        
		]
    }    
    return items;
}

function getItemDataDetailDiagnosaDanKdPenyakit_viDiagnosaDanKdPenyakit(lebar) 
{
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height: 120,//400,
	    items:
		[				
			{
				xtype: 'panel',
				title: 'Catatan',
				border:false,	
				height: 120,//375,
				items: 
				[
					{
						xtype: 'tabpanel',
						activeTab: 0,		
						height: 80,//200, //260,
						items: 
						[
							{
								xtype: 'panel',
								title: 'Anamnesis',				
								padding: '4px',
								items: 
								[																		
									{
										xtype: 'compositefield',
										fieldLabel: 'Catatan',
										name: 'compCatAnam_viDiagnosaDanKdPenyakit',
										id: 'compCatAnam_viDiagnosaDanKdPenyakit',
										items: 
										[											
											{
												xtype: 'textarea',						
												fieldLabel: 'Anamnesis',
												width: 805,
												height: 40,
												name: 'txtCatAnam_viDiagnosaDanKdPenyakit',
												id: 'txtCatAnam_viDiagnosaDanKdPenyakit'
											}					
										],										
									},									
								]
							},
							{
								xtype: 'panel',
								title: 'Diagnosa',				
								padding: '4px',
								items: 
								[
									{
										xtype: 'compositefield',
										fieldLabel: 'Catatan',
										name: 'compCatDiagnosa_viDiagnosaDanKdPenyakit',
										id: 'compCatDiagnosa_viDiagnosaDanKdPenyakit',
										items: 
										[											
											{
												xtype: 'textarea',						
												fieldLabel: 'Diagnosa',
												width: 805,
												height: 40,
												name: 'txtCatDiag_viDiagnosaDanKdPenyakit',
												id: 'txtCatDiag_viDiagnosaDanKdPenyakit'
											}					
										],										
									},
								]
							},							
							{
								xtype: 'panel',
								title: 'Tindakan',				
								padding: '4px',
								items: 
								[
									{
										xtype: 'compositefield',
										fieldLabel: 'Catatan',
										name: 'compCatTindakan_viDiagnosaDanKdPenyakit',
										id: 'compCatTindakan_viDiagnosaDanKdPenyakit',
										items: 
										[											
											{
												xtype: 'textarea',						
												fieldLabel: 'Tindakan',
												width: 805,
												height: 40,
												name: 'txtCatTindakan_viDiagnosaDanKdPenyakit',
												id: 'txtCatTindakan_viDiagnosaDanKdPenyakit'
											}					
										],										
									},
								]
							},
						]
					},									
				]
			}
		]
	};
    return items;
};

function getItemDataRiwayatPenyakit_viDiagnosaDanKdPenyakit(lebar) 
{
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height: 80,//400,
	    items:
		[				
			{
				xtype: 'panel',
				title: 'Riwayat Penyakit',
				border:false,	
				height: 100,//375,
				items: 
				[
					gridDataRiwayatPenyakit_viDiagnosaDanKdPenyakit()											
				]
			}
		]
	};
    return items;
};

/**
*   Function : FormSetLookupProduk_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan windows popup List Produk
*/

// function FormSetLookupProduk_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN) 
// {
    // vWinFormEntryLookupProduk_viDiagnosaDanKdPenyakit = new Ext.Window
    // (
        // {
            // id: 'FormGrdLookupProduk_viDiagnosaDanKdPenyakit',
            // title: 'List Produk',
            // closeAction: 'destroy',
            // closable:true,
            // width: 760,
            // height: 473,
            // border: false,
            // plain: true,
            // resizable:false,
            // constrainHeader : true,
            // layout: 'form',
            // iconCls: 'Edit_Tr',
			// //padding: '8px',
            // modal: true,                                   
            // items: 
            // [
                // 
                // //-------------- ## --------------
            // ],
            // tbar: 
			// {
				// xtype: 'toolbar',
				// items: 
				// [
					// {
						// xtype: 'button',
						// text: 'Add',
						// iconCls: 'add',
						// id: 'btnAddTreeProduk_viDiagnosaDanKdPenyakit',
						// handler: function()
						// {
						// }
					// },
					// //-------------- ## --------------
					// {
						// xtype: 'tbseparator'
					// },
					// //-------------- ## --------------
					// {
						// xtype: 'button',
						// text: 'Delete',
						// iconCls: 'remove',
						// id: 'btnDeleteTreeProduk_viDiagnosaDanKdPenyakit',
						// handler: function()
						// {
						// }
					// },
					// //-------------- ## --------------
				// ]
			// }
        // }
    // );
    // vWinFormEntryLookupProduk_viDiagnosaDanKdPenyakit.show();
    // mWindowGridLookupLookupProduk  = vWinFormEntryLookupProduk_viDiagnosaDanKdPenyakit; 
// }
// End Function FormSetLookupProduk_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getFormItemEntryProdukGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form produk
*/

/**
*   Function : FormSetLookupKonsultasi_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan windows popup Konsultasi
*/

function FormSetLookupKonsultasi_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryKonsultasi_viDiagnosaDanKdPenyakit = new Ext.Window
    (
        {
            id: 'FormGrdLookupKonsultasi_viDiagnosaDanKdPenyakit',
            title: 'Konsultasi',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupKonsultasi_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryKonsultasi_viDiagnosaDanKdPenyakit.show();
    mWindowGridLookupKonsultasi  = vWinFormEntryKonsultasi_viDiagnosaDanKdPenyakit; 
};

// End Function FormSetLookupKonsultasi_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getFormItemEntryLookupKonsultasi_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/
function getFormItemEntryLookupKonsultasi_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataKonsultasiWindowPopup_viDiagnosaDanKdPenyakit = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputKonsultasiDataView_viDiagnosaDanKdPenyakit(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataKonsultasiWindowPopup_viDiagnosaDanKdPenyakit;
}
// End Function getFormItemEntryLookupKonsultasi_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getItemPanelInputKonsultasiDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/

function viComboJenisTransaksi_viDiagnosaDanKdPenyakit(lebar,Nama_ID)
{
  var cbo_viComboJenisTransaksi_viDiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Transaksi..',
            fieldLabel: "Transaksi",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdTrnsksi',
                        'displayTextTrnsksi'
                    ],
                    data: [['all', "Semua"],[1, "Bayar"],[2, "Belum Bayar"]]
                }
            ),
            valueField: 'IdTrnsksi',
            displayField: 'displayTextTrnsksi',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisTransaksi_viDiagnosaDanKdPenyakit;
};
// End Function viComboJenisTransaksi_viDiagnosaDanKdPenyakit # --------------

function viComboKamarOperasi_viDiagnosaDanKdPenyakit(lebar,Nama_ID)
{
  var cbo_viComboKamarOperasi_viDiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Kamar Operasi..',
            fieldLabel: " ",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKamar',
                        'displayTextKamar'
                    ],
                    data: [[1, "Kamar 1"],[2, "Kamar 2"],[3, "Kamar 3"]]
                }
            ),
            valueField: 'IdKamar',
            displayField: 'displayTextKamar',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKamarOperasi_viDiagnosaDanKdPenyakit;
};

function viComboKecelakaan_viDiagnosaDanKdPenyakit(lebar,Nama_ID)
{
  var cbo_viComboKecelakaan_viDiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Non Traumatik',
            fieldLabel: "",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKecelakaan',
                        'IdKetKec'
                    ],
                    data: [[1, "Non Traumatik"]]
                }
            ),
            valueField: 'IdKecelakaan',
            displayField: 'IdKetKec',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKecelakaan_viDiagnosaDanKdPenyakit;
};

function getItemPanelInputKonsultasiDataView_viDiagnosaDanKdPenyakit(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Asal Unit',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viDiagnosaDanKdPenyakit(250,'CmboAsalUnit_viDiagnosaDanKdPenyakit'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Konsultasi ke',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viDiagnosaDanKdPenyakit(250,'CmboKonsultasike_viDiagnosaDanKdPenyakit'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viDiagnosaDanKdPenyakit(250,'CmboDokter_viDiagnosaDanKdPenyakit'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputKonsultasiDataView_viDiagnosaDanKdPenyakit # --------------

// ## END MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupGantiDokter_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupGantiDokter_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_viDiagnosaDanKdPenyakit = new Ext.Window
    (
        {
            id: 'FormGrdLookupGantiDokter_viDiagnosaDanKdPenyakit',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupGantiDokter_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_viDiagnosaDanKdPenyakit.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_viDiagnosaDanKdPenyakit; 
};

// End Function FormSetLookupGantiDokter_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getFormItemEntryLookupGantiDokter_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupGantiDokter_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataGantiDokterWindowPopup_viDiagnosaDanKdPenyakit = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputGantiDokterDataView_viDiagnosaDanKdPenyakit(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataGantiDokterWindowPopup_viDiagnosaDanKdPenyakit;
}
// End Function getFormItemEntryLookupGantiDokter_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getItemPanelInputGantiDokterDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputGantiDokterDataView_viDiagnosaDanKdPenyakit(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Tanggal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtTglGantiDokter_viDiagnosaDanKdPenyakit',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtBlnGantiDokter_viDiagnosaDanKdPenyakit',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtThnGantiDokter_viDiagnosaDanKdPenyakit',
                        emptyText: '2014',
                        width: 50,
                    },
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Asal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viDiagnosaDanKdPenyakit(250,'CmboDokterAsal_viDiagnosaDanKdPenyakit'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Ganti',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viDiagnosaDanKdPenyakit(250,'CmboDokterGanti_viDiagnosaDanKdPenyakit'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputGantiDokterDataView_viDiagnosaDanKdPenyakit # --------------

// ## END MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

// ## END MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

/**
*   Function : FormSetPembayaran_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan windows popup Pembayaran
*/

function FormSetPembayaran_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPembayaran_viDiagnosaDanKdPenyakit = new Ext.Window
    (
        {
            id: 'FormGrdPembayaran_viDiagnosaDanKdPenyakit',
            title: 'Pembayaran',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryGridDataView_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPembayaran_viDiagnosaDanKdPenyakit.show();
    mWindowGridLookupPembayaran  = vWinFormEntryPembayaran_viDiagnosaDanKdPenyakit; 
};

// End Function FormSetPembayaran_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getFormItemEntryGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function getFormItemEntryGridDataView_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataWindowPopup_viDiagnosaDanKdPenyakit = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataGridDataView_viDiagnosaDanKdPenyakit(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiGridDataView_viDiagnosaDanKdPenyakit(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnOkGridTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnCancelGridTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtBiayaGridTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
                            name: 'txtBiayaGridTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
							style:{'text-align':'right','margin-left':'150px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtPiutangGridTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
                            name: 'txtPiutangGridTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
							style:{'text-align':'right','margin-left':'146px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtTunaiGridTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
                            name: 'txtTunaiGridTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
							style:{'text-align':'right','margin-left':'142px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtDiscGridTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
                            name: 'txtDiscGridTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
							style:{'text-align':'right','margin-left':'138px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataWindowPopup_viDiagnosaDanKdPenyakit;
}
// End Function getFormItemEntryGridDataView_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getItemPanelInputBiodataGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemPanelInputBiodataGridDataView_viDiagnosaDanKdPenyakit(lebar) 
{
    var items =
    {
        title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
				xtype: 'textfield',
				fieldLabel: 'No. Transaksi',
				id: 'txtNoTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
				name: 'txtNoTransaksiPembayaranGridDataView_viDiagnosaDanKdPenyakit',
				emptyText: 'No. Transaksi',
				readOnly: true,
				flex: 1,
				width: 195,				
			},
			//-------------- ## --------------
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				id: 'txtNamaPasienPembayaranGridDataView_viDiagnosaDanKdPenyakit',
				name: 'txtNamaPasienPembayaranGridDataView_viDiagnosaDanKdPenyakit',
				emptyText: 'Nama Pasien',
				flex: 1,
				anchor: '100%',					
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran',
				anchor: '100%',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viDiagnosaDanKdPenyakit(195,'CmboPembayaranGridDataView_viDiagnosaDanKdPenyakit'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				anchor: '100%',
				labelSeparator: '',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viDiagnosaDanKdPenyakit(195,'CmboPembayaranDuaGridDataView_viDiagnosaDanKdPenyakit'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataGridDataView_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getItemGridTransaksiGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemGridTransaksiGridDataView_viDiagnosaDanKdPenyakit(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar-80,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridDataView_viDiagnosaDanKdPenyakit()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiGridDataView_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : gridDataViewEditGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridDataViewEditGridDataView_viDiagnosaDanKdPenyakit()
{
    
    var FieldGrdKasirGridDataView_viDiagnosaDanKdPenyakit = 
    [
    ];
    
    dsDataGrdJabGridDataView_viDiagnosaDanKdPenyakit= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viDiagnosaDanKdPenyakit
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viDiagnosaDanKdPenyakit,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kode',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Qty',
                sortable: true,
                width: 40
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Biaya',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Piutang',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tunai',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Disc',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridDataView_viDiagnosaDanKdPenyakit # --------------

// ## END MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------
function FormMoveRoom_viDiagnosaDanKdPenyakit() 
{
    vWinFormEntryMoveRoom_viDiagnosaDanKdPenyakit = new Ext.Window
    (
        {
            id: 'FormMoveRoom_viDiagnosaDanKdPenyakit',
            title: 'Move Room',
            closeAction: 'destroy',
            closable:true,
            width: 480,
            height: 345,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,            
            modal: true,                                   
            items: 
            [
				getFormMoveRoom_viDiagnosaDanKdPenyakit()
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryMoveRoom_viDiagnosaDanKdPenyakit.show();  
};

function getFormMoveRoom_viDiagnosaDanKdPenyakit()
{
	var pnlFormMoveRoom_viDiagnosaDanKdPenyakit = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'form',
            // padding: '8px',
            bodyStyle: 'padding:5px 5px 5px 5px;',
            items:
			[
				{
					xtype: 'panel',
					title: 'Asal',
					border:true,		
					id: 'panelAsalMoveRoom_viDiagnosaDanKdPenyakit',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Pasien',
							anchor: '100%',
							id: 'compPasienMoveRoom_viDiagnosaDanKdPenyakit',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Pasien:',
									id: 'labelPasienMoveRoom_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'KdPasien',														
									// width : 340,	
									width : 120,	
									name: 'txtKdPasienMoveRoom_viDiagnosaDanKdPenyakit',
									id: 'txtKdPasienMoveRoom_viDiagnosaDanKdPenyakit',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'NMPasien',														
									// width : 340,	
									width : 215,	
									name: 'txtNMPasienMoveRoom_viDiagnosaDanKdPenyakit',
									id: 'txtNMPasienMoveRoom_viDiagnosaDanKdPenyakit',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcKamarMoveRoom_viDiagnosaDanKdPenyakit',
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Kamar',
							anchor: '100%',
							id: 'compKamarMoveRoom_viDiagnosaDanKdPenyakit',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Kelas:',
									id: 'labelKelasMoveRoom_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'KelasPasien',														
									// width : 340,	
									width : 120,	
									name: 'txtKelasMoveRoom_viDiagnosaDanKdPenyakit',
									id: 'txtKelasMoveRoom_viDiagnosaDanKdPenyakit',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'displayfield',
									flex: 1,
									width: 50,
									name: '',
									id: 'labelKamarMoveRoom_viDiagnosaDanKdPenyakit',
									value: 'Kamar:',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'NMPasien',														
									width : 160,	
									name: 'txtKamarMoveRoom_viDiagnosaDanKdPenyakit',
									id: 'txtKamarMoveRoom_viDiagnosaDanKdPenyakit',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						}
					]
				},
				{ 
					xtype: 'tbspacer',					
					id: 'spcAsalMoveRoom_viDiagnosaDanKdPenyakit',					
					height: 5
				},
				{
					xtype: 'panel',
					title: 'Tujuan',
					border:true,		
					id: 'panelTujuanMoveRoom_viDiagnosaDanKdPenyakit',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Pasien',
							anchor: '100%',
							id: 'compSpesialisMoveRoom_viDiagnosaDanKdPenyakit',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Spesialis:',
									id: 'labelSpcMoveRoom_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								viComboSpescialis_viDiagnosaDanKdPenyakit(),
								{
									xtype: 'displayfield',
									flex: 1,
									width: 50,
									name: '',
									value: 'Kelas:',
									id: 'labelKlstujuanMoveRoom_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								viComboKelas_viDiagnosaDanKdPenyakit(),								
							]
						},
						{ 
							xtype: 'tbspacer',					
							id: 'spcRuangMoveRoom_viDiagnosaDanKdPenyakit',					
							height: 5
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Ruang',
							anchor: '100%',
							id: 'compRuangMoveRoom_viDiagnosaDanKdPenyakit',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Ruang:',
									id: 'labelRuangMoveRoom_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								viComboRuangan_viDiagnosaDanKdPenyakit()														
							]
						},
						{ 
							xtype: 'tbspacer',					
							id: 'spcKamarMoveRoom_viDiagnosaDanKdPenyakit',					
							height: 5
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Ruang',
							anchor: '100%',
							id: 'compKamarMoveRoom_viDiagnosaDanKdPenyakit',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Kamar:',
									id: 'labelKamarMoveRoom_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								viComboKamar_viDiagnosaDanKdPenyakit(),
								{
									xtype: 'displayfield',
									flex: 1,
									width: 50,
									name: '',
									value: 'Sisa Bed:',
									id: 'labelSisaBedMoveRoom_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									id: 'txtSisaBedMoveRoom_viDiagnosaDanKdPenyakit',
									name: 'txtSisaBedMoveRoom_viDiagnosaDanKdPenyakit',									
									width: 100,							
									readOnly: true
								}								
							]
						},
						{ 
							xtype: 'tbspacer',					
							id: 'spcTglMoveRoom_viDiagnosaDanKdPenyakit',					
							height: 5
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Ruang',
							anchor: '100%',
							id: 'compTglMoveRoom_viDiagnosaDanKdPenyakit',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Tanggal:',
									id: 'dsplTglMoveRoom_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'dateTglMoveRoom_viDiagnosaDanKdPenyakit',
									name: 'dateTglMoveRoom_viDiagnosaDanKdPenyakit',
									width: 160,
									format: 'd/M/Y',
								},
								{
									xtype: 'displayfield',
									flex: 1,
									width: 80,
									name: '',
									value: 'Jam Pindah:',
									id: 'labelJamPindahMoveRoom_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'date2TglMoveRoom_viDiagnosaDanKdPenyakit',
									name: 'date2TglMoveRoom_viDiagnosaDanKdPenyakit',
									width: 90								
								}
							]
						}							
					]
				},
				{ 
					xtype: 'tbspacer',					
					id: 'spcTujuanMoveRoom_viDiagnosaDanKdPenyakit',					
					height: 5
				},
				{
					xtype: 'panel',
					title: '',
					border:true,		
					id: 'panelAlasanMoveRoom_viDiagnosaDanKdPenyakit',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[					
						{
							xtype: 'compositefield',
							fieldLabel: 'Alasan',
							anchor: '100%',
							id: 'compAlasanMoveRoom_viDiagnosaDanKdPenyakit',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Tanggal:',
									id: 'dsplAlasanMoveRoom_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								viComboAlasanPindah_viDiagnosaDanKdPenyakit()
							]
						}							
					]
				},
				{
					xtype: 'panel',
					title: '',
					border:false,		
					id: 'panelButtonMoveRoom_viDiagnosaDanKdPenyakit',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[	
						{
							xtype: 'compositefield',
							fieldLabel: 'Alasan',
							anchor: '100%',
							id: 'compAlasanMoveRoom_viDiagnosaDanKdPenyakit',							
							items: 
							[					
								{
									xtype:'button',
									text:'Ok',
									width:70,
									style:{'margin-left':'300px'},
									hideLabel:true,
									id: 'ButtonOKMoveRoom_viDiagnosaDanKdPenyakit',
									handler:function()
									{
										
									}   
								},
								{
									xtype:'button',
									text:'Batal',
									width:70,
									style:{'margin-left':'300px'},
									hideLabel:true,
									id: 'ButtonBatalMoveRoom_viDiagnosaDanKdPenyakit',
									handler:function()
									{
										
									}   
								},
							]
						}
					]
				}
			]
		}
	)	
	return pnlFormMoveRoom_viDiagnosaDanKdPenyakit
}

function FormDeposit_viDiagnosaDanKdPenyakit() 
{
    vWinFormEntryDeposit_viDiagnosaDanKdPenyakit = new Ext.Window
    (
        {
            id: 'FormDeposit_viDiagnosaDanKdPenyakit',
            title: 'Deposit',
            closeAction: 'destroy',
            closable:true,
            width: 480,
            height: 325,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,            
            modal: true,                                   
            items: 
            [
				getFormDeposit_viDiagnosaDanKdPenyakit()
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryDeposit_viDiagnosaDanKdPenyakit.show();  
};

function Formcheckout_viDiagnosaDanKdPenyakit() 
{
    vWinFormEntrycheckout_viDiagnosaDanKdPenyakit = new Ext.Window
    (
        {
            id: 'Formcheckout_viDiagnosaDanKdPenyakit',
            title: 'Check Out',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 225,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,            
            modal: true,                                   
            items: 
            [
				getFormCheckout_viDiagnosaDanKdPenyakit()
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntrycheckout_viDiagnosaDanKdPenyakit.show();
    // mWindowGridLookupTransferPembayaran  = vWinFormEntrycheckout_viDiagnosaDanKdPenyakit; 
};

/**
*   Function : FormSetTransferPembayaran_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan windows popup Transfer ke Rawat Inap
*/

function FormSetTransferPembayaran_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryTransferPembayaran_viDiagnosaDanKdPenyakit = new Ext.Window
    (
        {
            id: 'FormGrdTrnsferPembayaran_viDiagnosaDanKdPenyakit',
            title: 'Pemindahan Tagihan Ke Unit Rawat Inap',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 450,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormTransferPembayaranItemEntryGridDataView_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryTransferPembayaran_viDiagnosaDanKdPenyakit.show();
    mWindowGridLookupTransferPembayaran  = vWinFormEntryTransferPembayaran_viDiagnosaDanKdPenyakit; 
};

// End Function FormSetTransferPembayaran_viDiagnosaDanKdPenyakit # --------------

function getFormDeposit_viDiagnosaDanKdPenyakit()
{
    var pnlFormDeposit_viDiagnosaDanKdPenyakit = new Ext.FormPanel
    (
        {
            title: 'Informasi Pembayaran',
            region: 'center',
            layout: 'form',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            items:
            //-------------- #items# --------------
            [			
				{
					xtype: 'compositefield',
					fieldLabel: 'Tanggal',
					anchor: '100%',
					width: 130,
					items: 
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Tanggal',
							id: 'DatePembayaran_viDiagnosaDanKdPenyakit',
							name: 'DatePembayaran_viDiagnosaDanKdPenyakit',
							width: 100,
							format: 'd/M/Y'
						},
						{
							xtype: 'displayfield',
							flex: 1,
							width: 70,
							name: '',
							value: 'No. Bukti:',
							fieldLabel: 'Label'
						},
						{
							xtype: 'textfield',
							flex: 1,
							fieldLabel: 'Label',														
							width : 165,	
							name: 'txtNobukti_viDiagnosaDanKdPenyakit',
							id: 'txtNobukti_viDiagnosaDanKdPenyakit',
							emptyText: 'No. Bukti',
							listeners:
							{
							}
						}
						//-------------- ## --------------				
					]
				},		
				{
					xtype: 'panel',
					title: 'Informasi Pembayar',
					border:false,	
					// height:375,
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Nama',
							anchor: '100%',
							id: 'compNamadeposit_viDiagnosaDanKdPenyakit',
							// width: 130,
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Nama:',
									id: 'labelNamadeposit_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'Nama',														
									width : 340,	
									name: 'txtNamadeposit_viDiagnosaDanKdPenyakit',
									id: 'txtNamadeposit_viDiagnosaDanKdPenyakit',
									emptyText: 'Nama',
									listeners:
									{
									}
								}
							]
						}
					]
				},
				{
					xtype: 'panel',
					title: 'Detail Pembayar',
					border:false,	
					// height:375,
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Nama',
							anchor: '100%',
							id: 'compuntukdeposit_viDiagnosaDanKdPenyakit',
							// width: 130,
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Untuk:',
									id: 'labeluntukdeposit_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'Untuk',														
									width : 340,	
									name: 'txtuntukdeposit_viDiagnosaDanKdPenyakit',
									id: 'txtuntukdeposit_viDiagnosaDanKdPenyakit',
									emptyText: 'Untuk',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcPasiendeposit_viDiagnosaDanKdPenyakit'
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Pasien',
							anchor: '100%',
							id: 'compPasiendeposit_viDiagnosaDanKdPenyakit',
							// width: 130,
							items: 
							[								
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Pasien:',
									id: 'labelPasiendeposit_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 120,	
									name: 'txtPasiendeposit_viDiagnosaDanKdPenyakit',
									id: 'txtPasiendeposit_viDiagnosaDanKdPenyakit',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 215,	
									name: 'txtNmPasiendeposit_viDiagnosaDanKdPenyakit',
									id: 'txtNmPasiendeposit_viDiagnosaDanKdPenyakit',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcKamardeposit_viDiagnosaDanKdPenyakit'
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Kelas',
							anchor: '100%',
							id: 'compKamardeposit_viDiagnosaDanKdPenyakit',
							// width: 130,
							items: 
							[								
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Kamar:',
									id: 'labelKamardeposit_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 120,	
									name: 'txtKelasdeposit_viDiagnosaDanKdPenyakit',
									id: 'txtKelasdeposit_viDiagnosaDanKdPenyakit',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 215,	
									name: 'txtKamardeposit_viDiagnosaDanKdPenyakit',
									id: 'txtKamardeposit_viDiagnosaDanKdPenyakit',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcTotaldeposit_viDiagnosaDanKdPenyakit'
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Total',
							anchor: '100%',
							id: 'compTotaldeposit_viDiagnosaDanKdPenyakit',
							// width: 130,
							items: 
							[								
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Jumlah:',
									id: 'labelTotaldeposit_viDiagnosaDanKdPenyakit',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 120,	
									name: 'txtTotaldeposit_viDiagnosaDanKdPenyakit',
									id: 'txtTotaldeposit_viDiagnosaDanKdPenyakit',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcDepositdeposit_viDiagnosaDanKdPenyakit'
						},
						{
							xtype: 'panel',
							title: '',				
							border:false,
							labelAlign: 'right',						
							items: 
							[
								{
									xtype: 'compositefield',
									fieldLabel: '',
									id: 'CompButtonDeposit_viDiagnosaDanKdPenyakit',
									items: 
									[								
										{
											xtype: 'button',
											text:'Ok',
											id: 'btnokDeposit_viDiagnosaDanKdPenyakit',
											name: 'btnokDeposit_viDiagnosaDanKdPenyakit',
											style:{'margin-left':'250px'},
											width: 60
										}
										,	
										{
											xtype: 'button',
											text:'Batal',
											id: 'btnbatalDeposit_viDiagnosaDanKdPenyakit',
											name: 'btnbatalDeposit_viDiagnosaDanKdPenyakit',
											style:{'margin-left':'250px'},
											width: 60
										},	
										{
											xtype: 'button',
											text:'Print',
											id: 'btnCetakDeposit_viDiagnosaDanKdPenyakit',
											name: 'btnbatalDeposit_viDiagnosaDanKdPenyakit',
											style:{'margin-left':'250px'},
											width: 60
										}

									]
								}
							]
						}
					]
				}			
			]
		}
	)
	return pnlFormDeposit_viDiagnosaDanKdPenyakit
}

function getFormCheckout_viDiagnosaDanKdPenyakit()
{
    var pnlFormCheckout_viDiagnosaDanKdPenyakit = new Ext.FormPanel
    (
        {
            title: 'Informasi Pasien Pulang',
            region: 'center',
            layout: 'form',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            items:
            //-------------- #items# --------------
            [
               {
					xtype: 'compositefield',
					fieldLabel: 'Pasien',
					id: 'compkdpasiencheckout_viDiagnosaDanKdPenyakit',
					items: 
                    [
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtkdpasiencheckout_viDiagnosaDanKdPenyakit',
                            name: 'txtkdpasiencheckout_viDiagnosaDanKdPenyakit',
                            width: 80,
                            readOnly: true,
                        },
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtnmpasiencheckout_viDiagnosaDanKdPenyakit',
                            name: 'txtnmpasiencheckout_viDiagnosaDanKdPenyakit',
                            width: 160,
                            readOnly: true,
                        }
					]
				},
               {
					xtype: 'compositefield',
					fieldLabel: 'Kelas',
					id: 'CompKelascheckout_viDiagnosaDanKdPenyakit',
					items: 
                    [
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtKelascheckout_viDiagnosaDanKdPenyakit',
                            name: 'txtKelascheckout_viDiagnosaDanKdPenyakit',
                            width: 100,
                            readOnly: true,
                        },
						{
							xtype: 'displayfield',
							flex: 1,
							width: 35,
							name: '',
							value: 'Kamar:',
							fieldLabel: 'Label'
						},
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtkamarcheckout_viDiagnosaDanKdPenyakit',
                            name: 'txtkamarcheckout_viDiagnosaDanKdPenyakit',
                            width: 100,
                            readOnly: true,
                        }
					]
				},
                {
					xtype: 'compositefield',
					fieldLabel: 'Tgl. Masuk',
					id: 'CompTglmasukcheckout_viDiagnosaDanKdPenyakit',
					items: 
                    [
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txttglmasukcheckout_viDiagnosaDanKdPenyakit',
                            name: 'txttglmasukcheckout_viDiagnosaDanKdPenyakit',
                            width: 100,
                            readOnly: true,
                        },
						{
							xtype: 'displayfield',
							flex: 1,
							width: 35,
							name: '',
							value: 'Jam:',
							id: 'dspljammasukcheckout_viDiagnosaDanKdPenyakit',
							fieldLabel: 'Label'
						},
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtjammasukcheckout_viDiagnosaDanKdPenyakit',
                            name: 'txtjammasukcheckout_viDiagnosaDanKdPenyakit',
                            width: 100,
                            readOnly: true,
                        }
					]
				},	
				{
					xtype: 'compositefield',
					fieldLabel: 'Tgl. Pulang',
					id: 'CompTglpulangcheckout_viDiagnosaDanKdPenyakit',
					items: 
                    [
						{
							xtype: 'datefield',
							id: 'dtfpulangcheckout_viDiagnosaDanKdPenyakit',
							value: now_viDiagnosaDanKdPenyakit,
							format: 'd/M/Y',
							width: 100
						},
						{
							xtype: 'displayfield',
							flex: 1,
							width: 35,
							name: '',
							value: 'Jam:',
							id: 'dspljammasukcheckout_viDiagnosaDanKdPenyakit',
							fieldLabel: 'Label'
						},
						{
							xtype: 'datefield',
							id: 'dtfjampulangcheckout_viDiagnosaDanKdPenyakit',
							// value: now_viDiagnosaDanKdPenyakit,
							// format: 'HH:MM',
							width: 100
						}
					]
				},
				viComboKeadaanPsn_viDiagnosaDanKdPenyakit(),						
				{
					xtype: 'panel',
					title: '',				
					border:false,
					labelAlign: 'right',
					// padding: '4px',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'CompButtoncheckout_viDiagnosaDanKdPenyakit',
							items: 
							[								
								{
									xtype: 'button',
									text:'Ok',
									id: 'btnok_viDiagnosaDanKdPenyakit',
									name: 'btnok_viDiagnosaDanKdPenyakit',
									style:{'margin-left':'210px'},
									width: 60
								}
								,	
								{
									xtype: 'button',
									text:'Batal',
									id: 'btnbatal_viDiagnosaDanKdPenyakit',
									name: 'btnbatal_viDiagnosaDanKdPenyakit',
									style:{'margin-left':'210px'},
									width: 75
								}
							]
						}
					]
				}
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormCheckout_viDiagnosaDanKdPenyakit;
}

function viComboKeadaanPsn_viDiagnosaDanKdPenyakit()
{
  var cbo_viComboKeadaanPsn_viDiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKeadaanPsn_viDiagnosaDanKdPenyakit",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Keadaan Pasien..',
            fieldLabel: "Keadaan Pasien",
            value:1,        
            width:245,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKeadaan',
                        'displayTextKeadaan'
                    ],
                    data: [[1, "Sembuh"],[2, "Sakit"]]
                }
            ),
            valueField: 'IdKeadaan',
            displayField: 'displayTextKeadaan',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKeadaanPsn_viDiagnosaDanKdPenyakit;
};
function viComboKamar_viDiagnosaDanKdPenyakit()
{
  var cbo_viComboKamar_viDiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKamar_viDiagnosaDanKdPenyakit",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Kamar..',
            fieldLabel: "Kamar",
            value:1,        
            width:180,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKamar',
                        'displayTextKamar'
                    ],
                    data: [[1, "011 VVIP"],[2, "012 VVIP"]]
                }
            ),
            valueField: 'IdKamar',
            displayField: 'displayTextKamar',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKamar_viDiagnosaDanKdPenyakit;
};

function viComboRuangan_viDiagnosaDanKdPenyakit()
{
  var cbo_viComboRuangan_viDiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboRuangan_viDiagnosaDanKdPenyakit",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Ruang..',
            fieldLabel: "Ruang",
            value:1,        
            width:340,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdRuang',
                        'displayTextRuang'
                    ],
                    data: [[1, "Kelas VVIP"],[2, "Kelas VVIP"]]
                }
            ),
            valueField: 'IdRuang',
            displayField: 'displayTextRuang',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboRuangan_viDiagnosaDanKdPenyakit;
};

function viComboSpescialis_viDiagnosaDanKdPenyakit()
{
  var cbo_viComboSpescialis_viDiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboSpescialis_viDiagnosaDanKdPenyakit",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Specialis..',
            fieldLabel: "Spesialis",
            value:1,        
            width:180,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdSpesialis',
                        'displayTextSpesialis'
                    ],
                    data: [[1, "Penyakit Dalam"],[2, "Jantung"]]
                }
            ),
            valueField: 'IdSpesialis',
            displayField: 'displayTextSpesialis',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboSpescialis_viDiagnosaDanKdPenyakit;
};

function viComboKelas_viDiagnosaDanKdPenyakit()
{
	var cbo_viComboKelas_viDiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKelas_viDiagnosaDanKdPenyakit",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Kelas..',
            fieldLabel: "Kelas",
            value:1,        
            width:100,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKelas',
                        'displayTextKelas'
                    ],
                    data: [[1, "VVIP"],[2, "VIP"]]
                }
            ),
            valueField: 'IdKelas',
            displayField: 'displayTextKelas',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKelas_viDiagnosaDanKdPenyakit;
};

function viComboAlasanPindah_viDiagnosaDanKdPenyakit()
{
  var ComboAlasanPindah_viDiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
    
        {
            id:"ComboAlasanPindah_viDiagnosaDanKdPenyakit",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Alasan..',
            fieldLabel: "Alasan",
            value:1,        
            width:340,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdAlasanPindah',
                        'displayTextAlasanPindah'
                    ],
                    data: [[1, "Tidak Nyaman"],[2, "Tidak Mampu Bayar"]]
                }
            ),
            valueField: 'IdAlasanPindah',
            displayField: 'displayTextAlasanPindah',
            listeners:  
            {
			
            }
        }
    );
    return ComboAlasanPindah_viDiagnosaDanKdPenyakit;
};

/**
*   Function : getFormTransferPembayaranItemEntryGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form transfer ke rawat inap
*/
function getFormTransferPembayaranItemEntryGridDataView_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN)
{
    var pnlFormTransferPembayaranDataWindowPopup_viDiagnosaDanKdPenyakit = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            items:
            //-------------- #items# --------------
            [
                {
                    xtype: 'fieldset',
                    title: 'Tagihan dipindahkan ke',
                    frame: false,
                    width: 350,
                    height: 165,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Transaksi',
                            id: 'txtNoTransaksiTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            name: 'txtNoTransaksiTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
							xtype: 'compositefield',
							fieldLabel: 'Tgl. Lahir',
							anchor: '100%',
							width: 130,
							items: 
							[
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'DateTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
									name: 'DateTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
									width: 130,
									format: 'd/M/Y',
								}
								//-------------- ## --------------				
							]
						},
						//-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Rekam Medis',
                            id: 'txtNoRekamMedisTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            name: 'txtNoRekamMedisTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nama Pasien',
                            id: 'txtNamaPasienTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            name: 'txtNamaPasienTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Unit Perawatan',
                            id: 'txtUnitPerawatanTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            name: 'txtUnitPerawatanTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: '',
                    frame: false,
                    width: 350,
                    height: 125,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jumlah biaya',
                            id: 'txtJumlhBiayaTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            name: 'txtJumlhBiayaTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'PAID',
                            id: 'txtPAIDTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            name: 'txtPAIDTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jmlh dipindahkan',
                            id: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            name: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Saldo Tagihan',
                            id: 'txtSaldoTagihanTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            name: 'txtSaldoTagihanTransaksiTransferPembayaran_viDiagnosaDanKdPenyakit',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: 'Alasan Transfer',
                    frame: false,
                    width: 350,
                    height: 62,
                    items: 
                    [
						{
				            xtype: 'combo',
				            fieldLabel: 'Alsn Transfer',
				            labelSeparator: '',
				        },
						//-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					width: 160,
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnOkTransferPembayaran_viDiagnosaDanKdPenyakit',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
						{
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnCancelTransferPembayaran_viDiagnosaDanKdPenyakit',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
					]
				},
                //-------------- ## --------------
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormTransferPembayaranDataWindowPopup_viDiagnosaDanKdPenyakit;
}
// End Function getFormTransferPembayaranItemEntryGridDataView_viDiagnosaDanKdPenyakit # --------------

// ## END MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

/**
*   Function : FormSetEditTarif_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan windows popup Edit Tarif
*/

function FormSetEditTarif_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryEditTarif_viDiagnosaDanKdPenyakit = new Ext.Window
    (
        {
            id: 'FormGrdEditTarif_viDiagnosaDanKdPenyakit',
            title: 'Edit Tarif',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryEditTarifGridDataView_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryEditTarif_viDiagnosaDanKdPenyakit.show();
    mWindowGridLookupEditTarif  = vWinFormEntryEditTarif_viDiagnosaDanKdPenyakit; 
};

// End Function FormSetEditTarif_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getFormItemEntryEditTarifGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/
function getFormItemEntryEditTarifGridDataView_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataEditTarifWindowPopup_viDiagnosaDanKdPenyakit = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukEditTarifGridDataView_viDiagnosaDanKdPenyakit(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiEditTarifGridDataView_viDiagnosaDanKdPenyakit(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiEditTarifGridDataView_viDiagnosaDanKdPenyakit',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiEditTarifGridDataView_viDiagnosaDanKdPenyakit',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viDiagnosaDanKdPenyakit',
                            name: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viDiagnosaDanKdPenyakit',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viDiagnosaDanKdPenyakit',
                            name: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viDiagnosaDanKdPenyakit',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataEditTarifWindowPopup_viDiagnosaDanKdPenyakit;
}
// End Function getFormItemEntryEditTarifGridDataView_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getItemPanelInputProdukEditTarifGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemPanelInputProdukEditTarifGridDataView_viDiagnosaDanKdPenyakit(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukEditTarifGridDataView_viDiagnosaDanKdPenyakit',
                name: 'txtProdukEditTarifGridDataView_viDiagnosaDanKdPenyakit',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukEditTarifGridDataView_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getItemGridTransaksiEditTarifGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemGridTransaksiEditTarifGridDataView_viDiagnosaDanKdPenyakit(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridEditTarifDataView_viDiagnosaDanKdPenyakit()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiEditTarifGridDataView_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : gridDataViewEditGridEditTarifDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function gridDataViewEditGridEditTarifDataView_viDiagnosaDanKdPenyakit()
{
    
    var FieldGrdKasirGridDataView_viDiagnosaDanKdPenyakit = 
    [
    ];
    
    dsDataGrdJabGridDataView_viDiagnosaDanKdPenyakit= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viDiagnosaDanKdPenyakit
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viDiagnosaDanKdPenyakit,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridEditTarifDataView_viDiagnosaDanKdPenyakit # --------------

// ## END MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

/**
*   Function : FormSetPostingManual_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan windows popup Posting Manual
*/

function FormSetPostingManual_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPostingManual_viDiagnosaDanKdPenyakit = new Ext.Window
    (
        {
            id: 'FormGrdPostingManual_viDiagnosaDanKdPenyakit',
            title: 'Posting Manual',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryPostingManualGridDataView_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPostingManual_viDiagnosaDanKdPenyakit.show();
    mWindowGridLookupPostingManual  = vWinFormEntryPostingManual_viDiagnosaDanKdPenyakit; 
};

// End Function FormSetPostingManual_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getFormItemEntryPostingManualGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/
function getFormItemEntryPostingManualGridDataView_viDiagnosaDanKdPenyakit(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataPostingManualWindowPopup_viDiagnosaDanKdPenyakit = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukPostingManualGridDataView_viDiagnosaDanKdPenyakit(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiPostingManualGridDataView_viDiagnosaDanKdPenyakit(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiPostingManualGridDataView_viDiagnosaDanKdPenyakit',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiPostingManualGridDataView_viDiagnosaDanKdPenyakit',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viDiagnosaDanKdPenyakit',
                            name: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viDiagnosaDanKdPenyakit',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viDiagnosaDanKdPenyakit',
                            name: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viDiagnosaDanKdPenyakit',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataPostingManualWindowPopup_viDiagnosaDanKdPenyakit;
}
// End Function getFormItemEntryPostingManualGridDataView_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getItemPanelInputProdukPostingManualGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemPanelInputProdukPostingManualGridDataView_viDiagnosaDanKdPenyakit(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukPostingManualGridDataView_viDiagnosaDanKdPenyakit',
                name: 'txtProdukPostingManualGridDataView_viDiagnosaDanKdPenyakit',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukPostingManualGridDataView_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : getItemGridTransaksiPostingManualGridDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemGridTransaksiPostingManualGridDataView_viDiagnosaDanKdPenyakit(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridPostingManualDataView_viDiagnosaDanKdPenyakit()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiPostingManualGridDataView_viDiagnosaDanKdPenyakit # --------------

/**
*   Function : gridDataViewEditGridPostingManualDataView_viDiagnosaDanKdPenyakit
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function gridDataViewEditGridPostingManualDataView_viDiagnosaDanKdPenyakit()
{
    
    var FieldGrdKasirGridDataView_viDiagnosaDanKdPenyakit = 
    [
    ];
    
    dsDataGrdJabGridDataView_viDiagnosaDanKdPenyakit= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viDiagnosaDanKdPenyakit
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viDiagnosaDanKdPenyakit,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}

function viComboJenisKelamin_DiagnosaDanPenyakit(lebar,Nama_ID)
{
    var Field_DiagnosaDanPenyakit = [' ', ' '];
    ds_DiagnosaDanKdPenyakit = new WebApp.DataStore({fields: Field_DiagnosaDanPenyakit});
    
	// viRefresh_Journal();
	
    var cboJK_DiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'DiagnosaDanKdPenyakit',
			valueField: 'DiagnosaDanKdPenyakit',
            displayField: 'DiagnosaDanKdPenyakit',
			emptyText:'Pria',
			store: ds_DiagnosaDanKdPenyakit,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboJK_DiagnosaDanKdPenyakit;
}

function viComboGolDarah_DiagnosaDanPenyakit(lebar,Nama_ID)
{
    var FieldGolDarah_DiagnosaDanPenyakit = [' ', ' '];
    dsGolDarah_DiagnosaDanKdPenyakit = new WebApp.DataStore({fields: FieldGolDarah_DiagnosaDanPenyakit});
    
	
    var cboGolDarah_DiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'DiagnosaDanKdPenyakit',
			valueField: 'DiagnosaDanKdPenyakit',
            displayField: 'DiagnosaDanKdPenyakit',
			emptyText:'A',
			store: dsGolDarah_DiagnosaDanKdPenyakit,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboGolDarah_DiagnosaDanKdPenyakit;
}

function viComboCaraKeluar_DiagnosaDanPenyakit(lebar,Nama_ID)
{
    var FieldCaraKeluar_DiagnosaDanPenyakit = [' ', ' '];
    dsCaraKeluar_DiagnosaDanKdPenyakit = new WebApp.DataStore({fields: FieldCaraKeluar_DiagnosaDanPenyakit});
    
	
    var cboCaraKeluar_DiagnosaDanKdPenyakit = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'DiagnosaDanKdPenyakit',
			valueField: 'DiagnosaDanKdPenyakit',
            displayField: 'DiagnosaDanKdPenyakit',
			emptyText:'Sembuh',
			store: dsCaraKeluar_DiagnosaDanKdPenyakit,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboCaraKeluar_DiagnosaDanKdPenyakit;
}