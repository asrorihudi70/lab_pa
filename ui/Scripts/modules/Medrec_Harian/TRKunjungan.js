// Data Source ExtJS # --------------

/**
*	Nama File 		: TRKunjungan.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Kunjungan
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

var dataSource_viKunjungan;
var selectCount_viKunjungan=50;
var NamaForm_viKunjungan="Kunjungan";
var mod_name_viKunjungan="viKunjungan";
var now_viKunjungan= new Date();
var addNew_viKunjungan;
var rowSelected_viKunjungan;
var setLookUps_viKunjungan;
var mNoKunjungan_viKunjungan='';

var CurrentData_viKunjungan =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viKunjungan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

/**
*	Function : dataGrid_viKunjungan
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viKunjungan(mod_id_viKunjungan)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viKunjungan = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viKunjungan = new WebApp.DataStore
	({
        fields: FieldMaster_viKunjungan
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viKunjungan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viKunjungan,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viKunjungan = undefined;
							rowSelected_viKunjungan = dataSource_viKunjungan.getAt(row);
							CurrentData_viKunjungan
							CurrentData_viKunjungan.row = row;
							CurrentData_viKunjungan.data = rowSelected_viKunjungan.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viKunjungan = dataSource_viKunjungan.getAt(ridx);
					if (rowSelected_viKunjungan != undefined)
					{
						setLookUp_viKunjungan(rowSelected_viKunjungan.data);
					}
					else
					{
						setLookUp_viKunjungan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[					
					{
						id: 'colTglRO_viKunjungan',
						header:'Tanggal',
						dataIndex: '',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viKunjungan',
						header: 'Aktifitas',
						dataIndex: '',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viKunjungan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viKunjungan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viKunjungan != undefined)
							{
								setLookUp_viKunjungan(rowSelected_viKunjungan.data)
							}
							else
							{								
								setLookUp_viKunjungan();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viKunjungan, selectCount_viKunjungan, dataSource_viKunjungan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viKunjungan = new Ext.Panel
    (
		{
			title: NamaForm_viKunjungan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viKunjungan,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viKunjungan],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viKunjungan,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viKunjungan',
							value: now_viKunjungan,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viKunjungan();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viKunjungan',
							value: now_viKunjungan,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viKunjungan();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Aktifitas : ', 
							style:{'text-align':'right'},
							width: 70,
							height: 25
						},		
						viCombo_Aktifitas(150, 'cboPbf_viKunjunganFilter'),						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viKunjungan',
							handler: function() 
							{					
								DfltFilterBtn_viKunjungan = 1;
								DataRefresh_viKunjungan(getCriteriaFilterGridDataView_viKunjungan());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viKunjungan;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viKunjungan # --------------

/**
*	Function : setLookUp_viKunjungan
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viKunjungan(rowdata)
{
    var lebar = 985;
    setLookUps_viKunjungan = new Ext.Window
    (
    {
        id: 'SetLookUps_viKunjungan',
		name: 'SetLookUps_viKunjungan',
        title: NamaForm_viKunjungan, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viKunjungan(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viKunjungan=undefined;
                //datarefresh_viKunjungan();
				mNoKunjungan_viKunjungan = '';
            }
        }
    }
    );

    setLookUps_viKunjungan.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viKunjungan();
		// Ext.getCmp('btnDelete_viKunjungan').disable();	
    }
    else
    {
        // datainit_viKunjungan(rowdata);
    }
}
// End Function setLookUpGridDataView_viKunjungan # --------------

/**
*	Function : getFormItemEntry_viKunjungan
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viKunjungan(lebar,rowdata)
{
    var pnlFormDataBasic_viKunjungan = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInput_viKunjungan(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viKunjungan(lebar),
				//-------------- ## --------------				
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viKunjungan',
						handler: function(){
							dataaddnew_viKunjungan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viKunjungan',
						handler: function()
						{
							datasave_viKunjungan(addNew_viKunjungan);
							datarefresh_viKunjungan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viKunjungan',
						handler: function()
						{
							var x = datasave_viKunjungan(addNew_viKunjungan);
							datarefresh_viKunjungan();
							if (x===undefined)
							{
								setLookUps_viKunjungan.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viKunjungan',
						handler: function()
						{
							datadelete_viKunjungan();
							datarefresh_viKunjungan();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viKunjungan;
}
// End Function getFormItemEntry_viKunjungan # --------------

/**
*	Function : getItemPanelInput_viKunjungan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInput_viKunjungan(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[					
			{
				xtype: 'compositefield',
				fieldLabel: 'Bulan/Tahun',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[					
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viKunjungan',
						value: now_viKunjungan,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viKunjungan();								
								} 						
							}
						}
					},									                                        				
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Aktifitas',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					viCombo_Aktifitas(825, 'cboAktifitas_viKunjungan')					
				]
			},			
		]
	};
    return items;
};
// End Function getItemPanelInput_viKunjungan # --------------


/**
*	Function : getItemGridTransaksi_viKunjungan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viKunjungan(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 455, //347,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viKunjungan()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viKunjungan # --------------

/**
*	Function : gridDataViewEdit_viKunjungan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viKunjungan()
{
    
    chkSelected_viKunjungan = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viKunjungan',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viKunjungan = 
	[
	];
	
    dsDataGrdJab_viKunjungan= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viKunjungan
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viKunjungan,
        height: 450,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viKunjungan,			
			{			
				dataIndex: '',
				header: 'No',
				sortable: true,
				width: 50
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'PENGUNJUNG RUMAH SAKIT',
				sortable: true,
				width: 300,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'JUMLAH',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
        ],
        plugins:chkSelected_viKunjungan,
    }    
    return items;
}
// End Function gridDataViewEdit_viKunjungan # --------------


function viCombo_Aktifitas(lebar,Nama_ID)
{
    var Field_Aktifitas = ['KD_Aktifitas', 'KD_CUSTOMER', 'Aktifitas'];
    ds_Aktifitas = new WebApp.DataStore({fields: Field_Aktifitas});
    
	// viRefresh_Aktifitas();
	
    var cbo_Aktifitas = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Aktifitas',
			valueField: 'KD_Aktifitas',
            displayField: 'Aktifitas',
			emptyText:'Aktifitas',
			store: ds_Aktifitas,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_Aktifitas;
}