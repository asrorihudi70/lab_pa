// Data Source ExtJS # --------------

/**
*	Nama File 		: TRMutasiKamar.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Mutasi Kamar
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

var dataSource_viMutasiKamar;
var selectCount_viMutasiKamar=50;
var NamaForm_viMutasiKamar="Mutasi Kamar";
var mod_name_viMutasiKamar="viMutasiKamar";
var now_viMutasiKamar= new Date();
var addNew_viMutasiKamar;
var rowSelected_viMutasiKamar;
var setLookUps_viMutasiKamar;
var mNoKunjungan_viMutasiKamar='';

var CurrentData_viMutasiKamar =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viMutasiKamar(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

/**
*	Function : dataGrid_viMutasiKamar
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viMutasiKamar(mod_id_viMutasiKamar)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viMutasiKamar = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viMutasiKamar = new WebApp.DataStore
	({
        fields: FieldMaster_viMutasiKamar
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viMutasiKamar = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viMutasiKamar,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viMutasiKamar = undefined;
							rowSelected_viMutasiKamar = dataSource_viMutasiKamar.getAt(row);
							CurrentData_viMutasiKamar
							CurrentData_viMutasiKamar.row = row;
							CurrentData_viMutasiKamar.data = rowSelected_viMutasiKamar.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viMutasiKamar = dataSource_viMutasiKamar.getAt(ridx);
					if (rowSelected_viMutasiKamar != undefined)
					{
						setLookUp_viMutasiKamar(rowSelected_viMutasiKamar.data);
					}
					else
					{
						setLookUp_viMutasiKamar();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[					
					{
						id: 'colTglRO_viMutasiKamar',
						header:'Tanggal',
						dataIndex: '',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viMutasiKamar',
						header: 'Spesialisasi',
						dataIndex: '',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viMutasiKamar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viMutasiKamar',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viMutasiKamar != undefined)
							{
								setLookUp_viMutasiKamar(rowSelected_viMutasiKamar.data)
							}
							else
							{								
								setLookUp_viMutasiKamar();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viMutasiKamar, selectCount_viMutasiKamar, dataSource_viMutasiKamar),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viMutasiKamar = new Ext.Panel
    (
		{
			title: NamaForm_viMutasiKamar,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viMutasiKamar,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viMutasiKamar],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viMutasiKamar,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viMutasiKamar',
							value: now_viMutasiKamar,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viMutasiKamar();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viMutasiKamar',
							value: now_viMutasiKamar,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viMutasiKamar();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Spesialisasi : ', 
							style:{'text-align':'right'},
							width: 70,
							height: 25
						},		
						viCombo_Spesialisasi(150, 'cboPbf_viMutasiKamarFilter'),						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viMutasiKamar',
							handler: function() 
							{					
								DfltFilterBtn_viMutasiKamar = 1;
								DataRefresh_viMutasiKamar(getCriteriaFilterGridDataView_viMutasiKamar());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viMutasiKamar;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viMutasiKamar # --------------

/**
*	Function : setLookUp_viMutasiKamar
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viMutasiKamar(rowdata)
{
    var lebar = 985;
    setLookUps_viMutasiKamar = new Ext.Window
    (
    {
        id: 'SetLookUps_viMutasiKamar',
		name: 'SetLookUps_viMutasiKamar',
        title: NamaForm_viMutasiKamar, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viMutasiKamar(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viMutasiKamar=undefined;
                //datarefresh_viMutasiKamar();
				mNoKunjungan_viMutasiKamar = '';
            }
        }
    }
    );

    setLookUps_viMutasiKamar.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viMutasiKamar();
		// Ext.getCmp('btnDelete_viMutasiKamar').disable();	
    }
    else
    {
        // datainit_viMutasiKamar(rowdata);
    }
}
// End Function setLookUpGridDataView_viMutasiKamar # --------------

/**
*	Function : getFormItemEntry_viMutasiKamar
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viMutasiKamar(lebar,rowdata)
{
    var pnlFormDataBasic_viMutasiKamar = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInput_viMutasiKamar(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viMutasiKamar(lebar),
				//-------------- ## --------------				
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viMutasiKamar',
						handler: function(){
							dataaddnew_viMutasiKamar();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viMutasiKamar',
						handler: function()
						{
							datasave_viMutasiKamar(addNew_viMutasiKamar);
							datarefresh_viMutasiKamar();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viMutasiKamar',
						handler: function()
						{
							var x = datasave_viMutasiKamar(addNew_viMutasiKamar);
							datarefresh_viMutasiKamar();
							if (x===undefined)
							{
								setLookUps_viMutasiKamar.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viMutasiKamar',
						handler: function()
						{
							datadelete_viMutasiKamar();
							datarefresh_viMutasiKamar();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viMutasiKamar;
}
// End Function getFormItemEntry_viMutasiKamar # --------------

/**
*	Function : getItemPanelInput_viMutasiKamar
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInput_viMutasiKamar(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[					
			{
				xtype: 'compositefield',
				fieldLabel: 'Bulan/Tahun',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[					
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viMutasiKamar',
						value: now_viMutasiKamar,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viMutasiKamar();								
								} 						
							}
						}
					},									                                        				
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Spesialisasi',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					viCombo_Spesialisasi(825, 'cboSpesialisasi_viMutasiKamar')					
				]
			},			
		]
	};
    return items;
};
// End Function getItemPanelInput_viMutasiKamar # --------------


/**
*	Function : getItemGridTransaksi_viMutasiKamar
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viMutasiKamar(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 455, //347,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viMutasiKamar()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viMutasiKamar # --------------

/**
*	Function : gridDataViewEdit_viMutasiKamar
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viMutasiKamar()
{
    
    chkSelected_viMutasiKamar = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viMutasiKamar',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viMutasiKamar = 
	[
	];
	
    dsDataGrdJab_viMutasiKamar= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viMutasiKamar
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viMutasiKamar,
        height: 450,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viMutasiKamar,			
			{			
				dataIndex: '',
				header: 'Kelas',
				sortable: true,
				width: 50
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'No. Kamar',
				sortable: true,
				width: 60,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kamar',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Awal',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Masuk',
				sortable: true,
				width: 45,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Pindahan',
				sortable: true,
				width: 60,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dipindah',
				sortable: true,
				width: 60,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Keluar Hidup',
				sortable: true,
				width: 75,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Keluar Mati<48',
				sortable: true,
				width: 90,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Keluar Mati>48',
				sortable: true,
				width: 90,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'Akhir',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'Lama Rawat',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'Hari Rawat',
				sortable: true,
				width: 70,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'Jumlah Tempat Tidur',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------			
        ],
        plugins:chkSelected_viMutasiKamar,
    }    
    return items;
}
// End Function gridDataViewEdit_viMutasiKamar # --------------


function viCombo_Spesialisasi(lebar,Nama_ID)
{
    var Field_Spesialisasi = ['KD_Spesialisasi', 'KD_CUSTOMER', 'Spesialisasi'];
    ds_Spesialisasi = new WebApp.DataStore({fields: Field_Spesialisasi});
    
	// viRefresh_Spesialisasi();
	
    var cbo_Spesialisasi = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Spesialisasi',
			valueField: 'KD_Spesialisasi',
            displayField: 'Spesialisasi',
			emptyText:'Spesialisasi',
			store: ds_Spesialisasi,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_Spesialisasi;
}