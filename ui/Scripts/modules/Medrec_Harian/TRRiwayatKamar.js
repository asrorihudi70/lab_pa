// Data Source ExtJS # --------------
// Deklarasi Variabel pada Diagnosa dan Kode Penyakit # --------------

var dataSource_viPasienKeluar;
var selectCount_viPasienKeluar=50;
var NamaForm_viPasienKeluar="Pasien Keluar";
var mod_name_viPasienKeluar="viPasienKeluar";
var now_viPasienKeluar= new Date();
var addNew_viPasienKeluar;
var rowSelected_viPasienKeluar;
var setLookUps_viPasienKeluar;
var mNoKunjungan_viPasienKeluar='';

var CurrentData_viPasienKeluar =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viPasienKeluar(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viPasienKeluar
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viPasienKeluar(mod_id_viPasienKeluar)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viPasienKeluar = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viPasienKeluar = new WebApp.DataStore
	({
        fields: FieldMaster_viPasienKeluar
    });
    
    // Grid Kasir Rawat Jalan # --------------
	var GridDataView_viPasienKeluar = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viPasienKeluar,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viPasienKeluar = undefined;
							rowSelected_viPasienKeluar = dataSource_viPasienKeluar.getAt(row);
							CurrentData_viPasienKeluar
							CurrentData_viPasienKeluar.row = row;
							CurrentData_viPasienKeluar.data = rowSelected_viPasienKeluar.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viPasienKeluar = dataSource_viPasienKeluar.getAt(ridx);
					if (rowSelected_viPasienKeluar != undefined)
					{
						setLookUp_viPasienKeluar(rowSelected_viPasienKeluar.data);
					}
					else
					{
						setLookUp_viPasienKeluar();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir Rawat Jalan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_viPasienKeluar',
						header: 'No. Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 25,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMPASIEN_viPasienKeluar',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 45,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colALAMAT_viPasienKeluar',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					//-------------- ## --------------
					{
						id: 'colTglKunj_viPasienKeluar',
						header:'Tgl Keluar',
						dataIndex: ' ',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							//return ShowDate(record.data.TGL_OPERASI);
						}
					}

				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viPasienKeluar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viPasienKeluar',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viPasienKeluar != undefined)
							{
								//setLookUp_viPasienKeluar(rowSelected_viPasienKeluar.data)
								FormSetLookupProduk_viKasirRwj('1','2')
							}
							else
							{								
								//setLookUp_viPasienKeluar();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viPasienKeluar, selectCount_viPasienKeluar, dataSource_viPasienKeluar),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viPasienKeluar = new Ext.Panel
    (
		{
			title: NamaForm_viPasienKeluar,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viPasienKeluar,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viPasienKeluar],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viPasienKeluar,
		            columns: 8,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            
						{ 
							xtype: 'tbtext', 
							text: 'No. Medrec : ', 
							style:{'text-align':'left'},
							width: 75,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMedrec_viPasienKeluar',							
							emptyText: 'No. Medrec',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viPasienKeluar = 1;
										DataRefresh_viPasienKeluar(getCriteriaFilterGridDataView_viPasienKeluar());
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viPasienKeluar',
							emptyText: 'Nama',
							width: 150,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viPasienKeluar = 1;
										DataRefresh_viPasienKeluar(getCriteriaFilterGridDataView_viPasienKeluar());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Alamat : ', 
							style:{'text-align':'right'},
							width: 35,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_ALAMAT_viPasienKeluar',
							emptyText: 'Alamat',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viPasienKeluar = 1;
										DataRefresh_viPasienKeluar(getCriteriaFilterGridDataView_viPasienKeluar());								
									} 						
								}
							}
						},
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Keluar : ', 
							style:{'text-align':'left'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_viPasienKeluar',
							value: now_viPasienKeluar,
							format: 'd/M/Y',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viPasienKeluar();								
									} 						
								}
							}
						},						
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'center'},
							width: 50,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viPasienKeluar',
							value: now_viPasienKeluar,
							format: 'd/M/Y',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viPasienKeluar();								
									} 						
								}
							}
						},						
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},														
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:50,
							id: 'BtnFilterGridDataView_viPasienKeluar',
							handler: function() 
							{					
								DfltFilterBtn_viPasienKeluar = 1;
								DataRefresh_viPasienKeluar(getCriteriaFilterGridDataView_viPasienKeluar());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viPasienKeluar;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viPasienKeluar # --------------

/**
*	Function : setLookUp_viPasienKeluar
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viPasienKeluar(rowdata)
{
    var lebar = 860;
    setLookUps_viPasienKeluar = new Ext.Window
    (
    {
        id: 'SetLookUps_viPasienKeluar',
		name: 'SetLookUps_viPasienKeluar',
        title: NamaForm_viPasienKeluar, 
        closeAction: 'destroy',        
        width: 875,
        height: 650,//580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viPasienKeluar(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viPasienKeluar=undefined;
                //datarefresh_viPasienKeluar();
				mNoKunjungan_viPasienKeluar = '';
            }
        }
    }
    );

    setLookUps_viPasienKeluar.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viPasienKeluar();
		// Ext.getCmp('btnDelete_viPasienKeluar').disable();	
    }
    else
    {
        // datainit_viPasienKeluar(rowdata);
    }
}
// End Function setLookUpGridDataView_viPasienKeluar # --------------

/**
*	Function : getFormItemEntry_viPasienKeluar
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viPasienKeluar(lebar,rowdata)
{
    var pnlFormDataBasic_viPasienKeluar = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------			
			items:[getItemPanelInputBiodata_viPasienKeluar(lebar), getItemDataKunjungan_viPasienKeluar(lebar), getItemDataDetailPasienKeluar_viPasienKeluar(lebar)], 			
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viPasienKeluar',
						handler: function(){
							dataaddnew_viPasienKeluar();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viPasienKeluar',
						handler: function()
						{
							datasave_viPasienKeluar(addNew_viPasienKeluar);
							datarefresh_viPasienKeluar();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viPasienKeluar',
						handler: function()
						{
							var x = datasave_viPasienKeluar(addNew_viPasienKeluar);
							datarefresh_viPasienKeluar();
							if (x===undefined)
							{
								setLookUps_viPasienKeluar.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viPasienKeluar',
						handler: function()
						{
							datadelete_viPasienKeluar();
							datarefresh_viPasienKeluar();
							
						}
					},
					
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viPasienKeluar;
}
// End Function getFormItemEntry_viPasienKeluar # --------------

/**
*	Function : getItemPanelInputBiodata_viPasienKeluar
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viPasienKeluar(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[		
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Medrec',
				name: 'compNoMedrec_viPasienKeluar',
				id: 'compNoMedrec_viPasienKeluar',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 100,
						name: 'txtNoMedrec_viPasienKeluar',
						id: 'txtNoMedrec_viPasienKeluar',
						emptyText: 'No. Medrec',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 90,								
						value: 'Nama Pasien:',
						fieldLabel: 'Label',
					},															
					{
						xtype: 'textfield',					
						fieldLabel: 'Nama Pasien',								
						width: 512,
						name: 'txtNmPasien_viPasienKeluar',
						id: 'txtNmPasien_viPasienKeluar',
						emptyText: 'Nama Pasien',
					},										
				]
			},
			//-------------- ## --------------	
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
								xtype: 'displayfield',				
								width: 86,								
								value: 'Tgl. Lahir',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 20,								
								value: 'Thn',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 20,								
								value: 'Bln',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 20,								
								value: 'Hari',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 585,//290,								
								value: 'Jenis Kelamin',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 65,								
								value: 'G. Darah',
							},				            
				        ]
				    },
		        ]
		    },
			//-------------- ## --------------
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhirEditData_viPasienKeluar',
		                        emptyText: '01',
		                        width: 20,
		                    },
		                    //-------------- ## -------------- 
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir2EditData_viPasienKeluar',
		                        emptyText: '01',
		                        width: 20,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtThnLhirEditData_viPasienKeluar',
		                        emptyText: '2014',
		                        width: 35,
		                    },
				            //-------------- ## --------------
							 {
		                        xtype: 'textfield',
		                        id: 'TxtThnLhir3EditData_viPasienKeluar',
		                        emptyText: '20',
		                        width: 20,
		                    },
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir3EditData_viPasienKeluar',
		                        emptyText: '01',
		                        width: 20,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir4EditData_viPasienKeluar',
		                        emptyText: '01',
		                        width: 20,
		                    },
				            //-------------- ## --------------
				            viComboJenisKelamin_PasienKeluar(585,'CmboJenisKelaminEditData_PasienKeluar'),
				            //-------------- ## --------------
				            viComboGolDarah_PasienKeluar(60,'CmboGlgnDrhEditData_PasienKeluar'),
							// //-------------- ## --------------
							{ 
								xtype: 'tbspacer',
								height: 25,
							},
							//-------------- ## --------------
				        ]
				    },
		        ]
		    },			
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Alamat ',
				name: 'compAlamat_viPasienKeluar',
				id: 'compAlamat_viPasienKeluar',
				items: 
				[					
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 710,
						name: 'txtAlamat_viPasienKeluar',
						id: 'txtAlamat_viPasienKeluar',
						emptyText: 'Alamat'
					},					
				]
			},						
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viPasienKeluar # --------------

/**
*	Function : getItemDataKunjungan_viPasienKeluar
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataKunjungan_viPasienKeluar(lebar) 
{		
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height: 160,
	    items:
		[				
			{
				xtype: 'panel',
				// title: 'Data Diagnosa',
				border:false,	
				height: 150,
				items: 
				[
					{
						xtype: 'compositefield',
						fieldLabel: 'Jenis Pasien',
						id: 'CompButtonJnsPx_viPasienKeluar',
						items: 
						[	
							{
								xtype: 'displayfield',
								flex: 1,
								width: 75,
								name: '',
								value: 'Jenis Pasien :',
								id: 'dsplJnsPasien_viPasienKeluar',
								style:{'text-align':'right'},
								fieldLabel: 'Label'
							},
							{
								xtype: 'button',
								text:'Pasien Umum',
								id: 'btnPXumum_viPasienKeluar',
								name: 'btnPXumum_viPasienKeluar',								
								width: 75
							},	
							{
								xtype: 'button',
								text:'Pasien Obstetri',
								id: 'btnPxObstetri_viPasienKeluar',
								name: 'btnPxObstetri_viPasienKeluar',								
								width: 85
							},
							{
								xtype: 'button',
								text:'Pasien Perinatal',
								id: 'btnPxPerinatal_viPasienKeluar',
								name: 'btnPxPerinatal_viPasienKeluar',								
								width: 75
							}
						]
					},
					{ 
						xtype: 'tbspacer',
						height: 5,
					},
					{
						xtype: 'compositefield',
						fieldLabel: 'Tanggal Masuk',
						anchor: '100%',
						width: 1200,
						items: 
						[
							{
								xtype: 'displayfield',
								flex: 1,
								width: 100,
								name: '',
								value: 'Tanggal Masuk :',
								id: 'dsplTglMasuk_viPasienKeluar',
								style:{'text-align':'right'},
								fieldLabel: 'Label'
							},
							{
		                        xtype: 'textfield',
		                        id: 'TxtTglMskEditData_viPasienKeluar',
		                        emptyText: '01',
		                        width: 20,
		                    },
		                    //-------------- ## -------------- 
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglMsk2EditData_viPasienKeluar',
		                        emptyText: '01',
		                        width: 20,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtThnMskEditData_viPasienKeluar',
		                        emptyText: '2014',
		                        width: 35,
		                    },
							{
								xtype: 'displayfield',
								flex: 1,
								width: 30,
								name: '',
								value: 'Jam :',
								id: 'dsplJam_viPasienKeluar',
								fieldLabel: 'Label'
							},
							{
		                        xtype: 'textfield',
		                        id: 'TxtJamEditData_viPasienKeluar',
		                        emptyText: '13:00',
		                        width: 40,
		                    },
							{
								xtype: 'displayfield',
								flex: 1,
								width: 90,
								name: '',
								value: 'Tanggal Keluar :',
								id: 'dsplTglKeluar_viPasienKeluar',
								fieldLabel: 'Label'
							},
							{
		                        xtype: 'textfield',
		                        id: 'TxtTglKlrEditData_viPasienKeluar',
		                        emptyText: '01',
		                        width: 20,
		                    },
		                    //-------------- ## -------------- 
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglKlr2EditData_viPasienKeluar',
		                        emptyText: '01',
		                        width: 20,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtThnKlrEditData_viPasienKeluar',
		                        emptyText: '2014',
		                        width: 35,
		                    },
							{
								xtype: 'displayfield',
								flex: 1,
								width: 30,
								name: '',
								value: 'Jam :',
								id: 'dsplJamKlr_viPasienKeluar',
								fieldLabel: 'Label'
							},
							{
		                        xtype: 'textfield',
		                        id: 'TxtJamKlrEditData_viPasienKeluar',
		                        emptyText: '13:00',
		                        width: 40,
		                    },
						]
					},
					{ 
						xtype: 'tbspacer',
						height: 5,
					},
					{
						xtype: 'compositefield',
						fieldLabel: 'Cara Penerimaan',
						anchor: '100%',
						width: 1200,
						items: 
						[
							{
								xtype: 'displayfield',
								flex: 1,
								width: 115,
								name: '',
								value: 'Cara Penerimaan :',
								id: 'dsplCaraTerima_viPasienKeluar',
								style:{'text-align':'right'},
								fieldLabel: 'Label'
							},
							viComboCaraPenerimaan_PasienKeluar(150, 'ComboCaraTerima_PasienKeluar'),							
							{
								xtype: 'displayfield',
								flex: 1,
								width: 100,
								name: '',
								value: 'Cara Keluar :',
								id: 'dsplCaraKeluar_viPasienKeluar',
								style:{'text-align':'right'},
								fieldLabel: 'Label'
							},
							viComboCaraKeluar_PasienKeluar(435, 'ComboCaraKeluar_PasienKeluar')
						]
					},
					{ 
						xtype: 'tbspacer',
						height: 5,
					},
					{
						xtype: 'compositefield',
						fieldLabel: 'Cara Masuk',
						anchor: '100%',
						width: 1200,
						items: 
						[
							{
								xtype: 'displayfield',
								flex: 1,
								width: 115,
								name: '',
								value: 'Cara Masuk :',
								id: 'dsplCaraMasuk_viPasienKeluar',
								style:{'text-align':'right'},
								fieldLabel: 'Label'
							},
							viComboCaraMasuk_PasienKeluar(150, 'ComboCaraMasuk_PasienKeluar'),							
							{
								xtype: 'displayfield',
								flex: 1,
								width: 100,
								name: '',
								value: 'Keadaan Akhir :',
								id: 'dsplKeadaanAkhir_viPasienKeluar',
								style:{'text-align':'right'},
								fieldLabel: 'Label'
							},
							viComboKeadaanAkhir_PasienKeluar(435, 'ComboKeadaanAkhir_PasienKeluar')
						]
					},					
					{ 
						xtype: 'tbspacer',
						height: 5,
					},
					{
						xtype: 'compositefield',
						fieldLabel: 'Bagian/Kelas/Kamar',
						anchor: '100%',
						width: 1200,
						items: 
						[
							{
								xtype: 'displayfield',
								flex: 1,
								width: 115,
								name: '',
								value: 'Bagian/Kelas/kamar :',
								id: 'dsplBagian_viPasienKeluar',
								style:{'text-align':'right'},
								fieldLabel: 'Label'
							},
							{
								xtype: 'textfield',					
								fieldLabel: '',								
								width: 694,
								name: 'txtBagian_viPasienKeluar',
								id: 'txtBagian_viPasienKeluar',
								emptyText: 'Bagian/Kelas/kamar'
							},						
						]
					},
						
				]
			}
		]
	};
    return items;
};
// End Function getItemDataKunjungan_viPasienKeluar # --------------

function getItemDataDetailPasienKeluar_viPasienKeluar(lebar) 
{
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height: 300,//225,
	    items:
		[				
			{
				xtype: 'panel',
				title: '',
				border:false,	
				height: 375,
				items: 
				[
					{
						xtype: 'tabpanel',
						activeTab: 0,		
						height: 280,
						items: 
						[							
							{
								xtype: 'panel',
								title: 'Umum',				
								padding: '4px',
								items: 
								[	
									{
										xtype: 'compositefield',
										fieldLabel: '',
										anchor: '100%',
										width: 1200,
										items: 
										[
											UmumDetailEditData_viPasienKeluar(),																				
											//-------------- ## --------------													
											UmumInfeksiEditData_viPasienKeluar(),
											//-------------- ## --------------	
										]
									},
									
									
								]
							},
							{
								xtype: 'panel',
								title: 'Obstetri',				
								padding: '4px',
								items: 
								[	
									{
										xtype: 'compositefield',
										fieldLabel: '',
										anchor: '100%',
										width: 1200,
										items: 
										[
											// UmumDetailEditData_viPasienKeluar(),																				
											// //-------------- ## --------------													
											// UmumInfeksiEditData_viPasienKeluar(),
											// //-------------- ## --------------	
										]
									},
									
									
								]
							},
						]
					},									
				]
			}
		]
	};
    return items;
};

function UmumInfeksiEditData_viPasienKeluar()
{	

	var itemsUmumInfeksiEditData_viPasienKeluar = 
	{
        xtype: 'panel',
        title: '',
        bodyStyle: 'padding: 5px 5px 5px 5px ;',
        width: 600,//500,
        height: 250,
		border:false,
        items: 
		[			
			gridUmumInfeksi_viPasienPulang()
        ]
    }
    return itemsUmumInfeksiEditData_viPasienKeluar;
};

function gridUmumInfeksi_viPasienPulang()
{
    
    var FieldUmumInfeksi_viPasienPulang = 
	[
	];
	
    dsUmumInfeksi_viPasienPulang= new WebApp.DataStore
	({
        fields: FieldUmumInfeksi_viPasienPulang
    });  
	
    var items = 
    {
        xtype: 'editorgrid',
        store: dsUmumInfeksi_viPasienPulang,
        height: 235,		
		width: 570,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Infeksi',
				sortable: true,
				width: 200
			},			
			{
				dataIndex: '',
				header: 'Penyebab',
				sortable: true,
				width: 150
			},
			{
				dataIndex: '',
				header: 'Tgl Infeksi',
				sortable: true,
				width: 100
			},			
        ]
    }    
    return items;
}

function UmumDetailEditData_viPasienKeluar()
{	

	var itemsUmumEditData_viPasienKeluar = 
	{
        xtype: 'panel',
        title: '',
        bodyStyle: 'padding: 5px 5px 5px 5px ;',
        width: 220,
        height: 250,
		border:false,
        items: 
		[			
			gridDataUmumTrans_viPasienKeluar()
        ]
    }
    return itemsUmumEditData_viPasienKeluar;
};

function gridDataUmumTrans_viPasienKeluar()
{
    
    var FieldGrdUmum_viPasienKeluar = 
	[
	];
	
    dsDataGrdUmum_viPasienKeluar= new WebApp.DataStore
	({
        fields: FieldGrdUmum_viPasienKeluar
    });  
	
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdUmum_viPasienKeluar,
        height: 235,		
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Imunisasi',
				sortable: true,
				width: 100
			},						
			{
				dataIndex: '',
				header: 'Tgl Imunisasi',
				sortable: true,
				width: 100
			},			
        ]
    }    
    return items;
}

function FormSetLookupKonsultasi_viPasienKeluar(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryKonsultasi_viPasienKeluar = new Ext.Window
    (
        {
            id: 'FormGrdLookupKonsultasi_viPasienKeluar',
            title: 'Konsultasi',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupKonsultasi_viPasienKeluar(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryKonsultasi_viPasienKeluar.show();
    mWindowGridLookupKonsultasi  = vWinFormEntryKonsultasi_viPasienKeluar; 
};

// End Function FormSetLookupKonsultasi_viPasienKeluar # --------------

/**
*   Function : getFormItemEntryLookupKonsultasi_viPasienKeluar
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/
function getFormItemEntryLookupKonsultasi_viPasienKeluar(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataKonsultasiWindowPopup_viPasienKeluar = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputKonsultasiDataView_viPasienKeluar(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataKonsultasiWindowPopup_viPasienKeluar;
}
// End Function getFormItemEntryLookupKonsultasi_viPasienKeluar # --------------

/**
*   Function : getItemPanelInputKonsultasiDataView_viPasienKeluar
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/

function viComboJenisTransaksi_viPasienKeluar(lebar,Nama_ID)
{
  var cbo_viComboJenisTransaksi_viPasienKeluar = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Transaksi..',
            fieldLabel: "Transaksi",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdTrnsksi',
                        'displayTextTrnsksi'
                    ],
                    data: [['all', "Semua"],[1, "Bayar"],[2, "Belum Bayar"]]
                }
            ),
            valueField: 'IdTrnsksi',
            displayField: 'displayTextTrnsksi',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisTransaksi_viPasienKeluar;
};
// End Function viComboJenisTransaksi_viPasienKeluar # --------------

function viComboKamarOperasi_viPasienKeluar(lebar,Nama_ID)
{
  var cbo_viComboKamarOperasi_viPasienKeluar = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Kamar Operasi..',
            fieldLabel: " ",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKamar',
                        'displayTextKamar'
                    ],
                    data: [[1, "Kamar 1"],[2, "Kamar 2"],[3, "Kamar 3"]]
                }
            ),
            valueField: 'IdKamar',
            displayField: 'displayTextKamar',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKamarOperasi_viPasienKeluar;
};

function getItemPanelInputKonsultasiDataView_viPasienKeluar(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Asal Unit',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPasienKeluar(250,'CmboAsalUnit_viPasienKeluar'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Konsultasi ke',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPasienKeluar(250,'CmboKonsultasike_viPasienKeluar'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPasienKeluar(250,'CmboDokter_viPasienKeluar'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}

function viComboDokter_PasienKeluar(lebar,Nama_ID)
{
    var FieldDokter_PasienKeluar = [' ', ' '];
    dsDokter_PasienKeluar = new WebApp.DataStore({fields: FieldDokter_PasienKeluar});    
		
    var cboDokter_PasienKeluar = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Dokter',
			valueField: 'Dokter',
            displayField: 'Dokter,',
			emptyText:'Dokter',
			store: dsDokter_PasienKeluar,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboDokter_PasienKeluar;
};

function viComboKeadaanAkhir_PasienKeluar(lebar,Nama_ID)
{
    var FieldKeadaanAkhir_PasienKeluar = [' ', ' '];
    dsKeadaanAkhir_PasienKeluar = new WebApp.DataStore({fields: FieldKeadaanAkhir_PasienKeluar});    
		
    var cboKeadaanAkhir_PasienKeluar = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'KeadaanAkhir',
			valueField: 'KeadaanAkhir',
            displayField: 'KeadaanAkhir,',
			emptyText:'Sembuh/Sehat',
			store: dsKeadaanAkhir_PasienKeluar,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboKeadaanAkhir_PasienKeluar;
};

function viComboCaraMasuk_PasienKeluar(lebar,Nama_ID)
{
    var FieldCaraMasuk_PasienKeluar = [' ', ' '];
    dsCaraMasuk_PasienKeluar = new WebApp.DataStore({fields: FieldCaraMasuk_PasienKeluar});    
		
    var cboCaraMasuk_PasienKeluar = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'CaraMasuk',
			valueField: 'CaraMasuk',
            displayField: 'CaraMasuk,',
			emptyText:'Datang Sendiri',
			store: dsCaraMasuk_PasienKeluar,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboCaraMasuk_PasienKeluar;
};

function viComboCaraPenerimaan_PasienKeluar(lebar,Nama_ID)
{
    var FieldCaraPenerimaan_PasienKeluar = [' ', ' '];
    dsCaraPenerimaan_PasienKeluar = new WebApp.DataStore({fields: FieldCaraPenerimaan_PasienKeluar});    
		
    var cboCaraPenerimaan_PasienKeluar = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'CaraPenerimaan',
			valueField: 'CaraPenerimaan',
            displayField: 'CaraPenerimaan',
			emptyText:'Melalui Gawat Darurat',
			store: dsCaraPenerimaan_PasienKeluar,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboCaraPenerimaan_PasienKeluar;
};

function viComboJenisKelamin_PasienKeluar(lebar,Nama_ID)
{
    var Field_PasienKeluar = [' ', ' '];
    ds_PasienKeluar = new WebApp.DataStore({fields: Field_PasienKeluar});
    
	// viRefresh_Journal();
	
    var cboJK_PasienKeluar = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'PasienKeluar',
			valueField: 'PasienKeluar',
            displayField: 'PasienKeluar',
			emptyText:'Pria',
			store: ds_PasienKeluar,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboJK_PasienKeluar;
}

function viComboGolDarah_PasienKeluar(lebar,Nama_ID)
{
    var FieldGolDarah_PasienKeluar = [' ', ' '];
    dsGolDarah_PasienKeluar = new WebApp.DataStore({fields: FieldGolDarah_PasienKeluar});
    
	
    var cboGolDarah_PasienKeluar = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'PasienKeluar',
			valueField: 'PasienKeluar',
            displayField: 'PasienKeluar',
			emptyText:'A',
			store: dsGolDarah_PasienKeluar,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboGolDarah_PasienKeluar;
}

function viComboCaraKeluar_PasienKeluar(lebar,Nama_ID)
{
    var FieldCaraKeluar_PasienKeluar = [' ', ' '];
    dsCaraKeluar_PasienKeluar = new WebApp.DataStore({fields: FieldCaraKeluar_PasienKeluar});
    
	
    var cboCaraKeluar_PasienKeluar = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'PasienKeluar',
			valueField: 'PasienKeluar',
            displayField: 'PasienKeluar',
			emptyText:'Diijinkan Pulang',
			store: dsCaraKeluar_PasienKeluar,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboCaraKeluar_PasienKeluar;
}

function viComboSebabKematian_PasienKeluar(lebar,Nama_ID)
{
    var FieldSebabKematian_PasienKeluar = [' ', ' '];
    dsSebabKematian_PasienKeluar = new WebApp.DataStore({fields: FieldSebabKematian_PasienKeluar});
    
	
    var cboSebabKematian_PasienKeluar = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'SebabKematian',
			valueField: 'SebabKematian',
            displayField: 'SebabKematian',
			emptyText:' ',
			store: dsSebabKematian_PasienKeluar,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboSebabKematian_PasienKeluar;
}

function FormSetLookupProduk_viKasirRwj(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryLookupProduk_viKasirRwj = new Ext.Window
    (
        {
            id: 'FormGrdLookupProduk_viKasirRwj',
            title: 'List Produk',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                // getFormItemEntryProdukGridDataView_viKasirRwj(subtotal,NO_KUNJUNGAN),
                //-------------- ## --------------
            ],
            tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAddTreeProduk_viKasirRwj',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDeleteTreeProduk_viKasirRwj',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
				]
			}
        }
    );
    vWinFormEntryLookupProduk_viKasirRwj.show();
    mWindowGridLookupLookupProduk  = vWinFormEntryLookupProduk_viKasirRwj; 
}
