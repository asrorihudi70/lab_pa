// Data Source ExtJS # --------------

/**
*	Nama File 		: TRTenagaRS.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Tenaga RS
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

var dataSource_viTenagaRS;
var selectCount_viTenagaRS=50;
var NamaForm_viTenagaRS="Tenaga RS";
var mod_name_viTenagaRS="viTenagaRS";
var now_viTenagaRS= new Date();
var addNew_viTenagaRS;
var rowSelected_viTenagaRS;
var setLookUps_viTenagaRS;
var mNoTenagaRS_viTenagaRS='';

var CurrentData_viTenagaRS =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viTenagaRS(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

/**
*	Function : dataGrid_viTenagaRS
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viTenagaRS(mod_id_viTenagaRS)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viTenagaRS = 
	[
		 'NO_TenagaRS', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_TenagaRS','JAM_TenagaRS', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viTenagaRS = new WebApp.DataStore
	({
        fields: FieldMaster_viTenagaRS
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viTenagaRS = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viTenagaRS,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viTenagaRS = undefined;
							rowSelected_viTenagaRS = dataSource_viTenagaRS.getAt(row);
							CurrentData_viTenagaRS
							CurrentData_viTenagaRS.row = row;
							CurrentData_viTenagaRS.data = rowSelected_viTenagaRS.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viTenagaRS = dataSource_viTenagaRS.getAt(ridx);
					if (rowSelected_viTenagaRS != undefined)
					{
						setLookUp_viTenagaRS(rowSelected_viTenagaRS.data);
					}
					else
					{
						setLookUp_viTenagaRS();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[					
					{
						id: 'colTglRO_viTenagaRS',
						header:'Tanggal',
						dataIndex: '',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viTenagaRS',
						header: 'Aktifitas',
						dataIndex: '',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viTenagaRS',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viTenagaRS',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viTenagaRS != undefined)
							{
								setLookUp_viTenagaRS(rowSelected_viTenagaRS.data)
							}
							else
							{								
								setLookUp_viTenagaRS();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viTenagaRS, selectCount_viTenagaRS, dataSource_viTenagaRS),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viTenagaRS = new Ext.Panel
    (
		{
			title: NamaForm_viTenagaRS,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viTenagaRS,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viTenagaRS],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viTenagaRS,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viTenagaRS',
							value: now_viTenagaRS,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viTenagaRS();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viTenagaRS',
							value: now_viTenagaRS,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viTenagaRS();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Aktifitas : ', 
							style:{'text-align':'right'},
							width: 70,
							height: 25
						},		
						viComboTenagaRS_Aktifitas(150, 'cboPbf_viTenagaRSFilter'),						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viTenagaRS',
							handler: function() 
							{					
								DfltFilterBtn_viTenagaRS = 1;
								DataRefresh_viTenagaRS(getCriteriaFilterGridDataView_viTenagaRS());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viTenagaRS;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viTenagaRS # --------------

/**
*	Function : setLookUp_viTenagaRS
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viTenagaRS(rowdata)
{
    var lebar = 985;
    setLookUps_viTenagaRS = new Ext.Window
    (
    {
        id: 'SetLookUps_viTenagaRS',
		name: 'SetLookUps_viTenagaRS',
        title: NamaForm_viTenagaRS, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viTenagaRS(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viTenagaRS=undefined;
                //datarefresh_viTenagaRS();
				mNoTenagaRS_viTenagaRS = '';
            }
        }
    }
    );

    setLookUps_viTenagaRS.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viTenagaRS();
		// Ext.getCmp('btnDelete_viTenagaRS').disable();	
    }
    else
    {
        // datainit_viTenagaRS(rowdata);
    }
}
// End Function setLookUpGridDataView_viTenagaRS # --------------

/**
*	Function : getFormItemEntry_viTenagaRS
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viTenagaRS(lebar,rowdata)
{
    var pnlFormDataBasic_viTenagaRS = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInput_viTenagaRS(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viTenagaRS(lebar),
				//-------------- ## --------------				
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viTenagaRS',
						handler: function(){
							dataaddnew_viTenagaRS();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viTenagaRS',
						handler: function()
						{
							datasave_viTenagaRS(addNew_viTenagaRS);
							datarefresh_viTenagaRS();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viTenagaRS',
						handler: function()
						{
							var x = datasave_viTenagaRS(addNew_viTenagaRS);
							datarefresh_viTenagaRS();
							if (x===undefined)
							{
								setLookUps_viTenagaRS.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viTenagaRS',
						handler: function()
						{
							datadelete_viTenagaRS();
							datarefresh_viTenagaRS();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viTenagaRS;
}
// End Function getFormItemEntry_viTenagaRS # --------------

/**
*	Function : getItemPanelInput_viTenagaRS
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInput_viTenagaRS(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[					
			{
				xtype: 'compositefield',
				fieldLabel: 'Bulan/Tahun',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[					
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viTenagaRS',
						value: now_viTenagaRS,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viTenagaRS();								
								} 						
							}
						}
					},									                                        				
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Aktifitas',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					viComboTenagaRS_Aktifitas(825, 'cboAktifitas_viTenagaRS')					
				]
			},			
		]
	};
    return items;
};
// End Function getItemPanelInput_viTenagaRS # --------------


/**
*	Function : getItemGridTransaksi_viTenagaRS
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viTenagaRS(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 455, //347,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viTenagaRS()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viTenagaRS # --------------

/**
*	Function : gridDataViewEdit_viTenagaRS
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viTenagaRS()
{
    
    chkSelected_viTenagaRS = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viTenagaRS',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viTenagaRS = 
	[
	];
	
    dsDataGrdJab_viTenagaRS= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viTenagaRS
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viTenagaRS,
        height: 450,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viTenagaRS,			
			{			
				dataIndex: '',
				header: 'No',
				sortable: true,
				width: 50
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'KETENAGAAN',
				sortable: true,
				width: 200,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'KEADAAN LAKI-LAKI',
				sortable: true,
				width: 120,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'KEADAAN PEREMPUAN',
				sortable: true,
				width: 140,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'KEBUTUHAN LAKI-LAKI',
				sortable: true,
				width: 140,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'KEBUTUHAN PEREMPUAN',
				sortable: true,
				width: 140,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'KEKURANGAN LAKI-LAKI',
				sortable: true,
				width: 140,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'KEKURANGAN PEREMPUAN',
				sortable: true,
				width: 150,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			//-------------- ## --------------			
        ],
        plugins:chkSelected_viTenagaRS,
    }    
    return items;
}
// End Function gridDataViewEdit_viTenagaRS # --------------


function viComboTenagaRS_Aktifitas(lebar,Nama_ID)
{
    var Field_Aktifitas = ['KD_Aktifitas', 'KD_CUSTOMER', 'Aktifitas'];
    dsTenagaRS_Aktifitas = new WebApp.DataStore({fields: Field_Aktifitas});
    
	// viRefresh_Aktifitas();
	
    var cboTenagaRS_Aktifitas = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Aktifitas',
			valueField: 'KD_Aktifitas',
            displayField: 'Aktifitas',
			emptyText:'KETENAGAAN',
			store: dsTenagaRS_Aktifitas,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboTenagaRS_Aktifitas;
}