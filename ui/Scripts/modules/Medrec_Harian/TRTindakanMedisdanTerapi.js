// Data Source ExtJS # --------------
// Deklarasi Variabel pada Diagnosa dan Kode Penyakit # --------------

var dataSource_viTindakanMedisdanTerapi;
var selectCount_viTindakanMedisdanTerapi=50;
var NamaForm_viTindakanMedisdanTerapi="Tindakan Medis dan Terapi";
var mod_name_viTindakanMedisdanTerapi="viTindakanMedisdanTerapi";
var now_viTindakanMedisdanTerapi= new Date();
var addNew_viTindakanMedisdanTerapi;
var rowSelected_viTindakanMedisdanTerapi;
var setLookUps_viTindakanMedisdanTerapi;
var mNoKunjungan_viTindakanMedisdanTerapi='';

var CurrentData_viTindakanMedisdanTerapi =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viTindakanMedisdanTerapi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viTindakanMedisdanTerapi
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viTindakanMedisdanTerapi(mod_id_viTindakanMedisdanTerapi)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viTindakanMedisdanTerapi = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viTindakanMedisdanTerapi = new WebApp.DataStore
	({
        fields: FieldMaster_viTindakanMedisdanTerapi
    });
    
    // Grid Kasir Rawat Jalan # --------------
	var GridDataView_viTindakanMedisdanTerapi = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viTindakanMedisdanTerapi,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viTindakanMedisdanTerapi = undefined;
							rowSelected_viTindakanMedisdanTerapi = dataSource_viTindakanMedisdanTerapi.getAt(row);
							CurrentData_viTindakanMedisdanTerapi
							CurrentData_viTindakanMedisdanTerapi.row = row;
							CurrentData_viTindakanMedisdanTerapi.data = rowSelected_viTindakanMedisdanTerapi.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viTindakanMedisdanTerapi = dataSource_viTindakanMedisdanTerapi.getAt(ridx);
					if (rowSelected_viTindakanMedisdanTerapi != undefined)
					{
						setLookUp_viTindakanMedisdanTerapi(rowSelected_viTindakanMedisdanTerapi.data);
					}
					else
					{
						setLookUp_viTindakanMedisdanTerapi();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir Rawat Jalan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_viTindakanMedisdanTerapi',
						header: 'No. Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 25,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMPASIEN_viTindakanMedisdanTerapi',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 45,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colALAMAT_viTindakanMedisdanTerapi',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					//-------------- ## --------------
					{
						id: 'colTglKunj_viTindakanMedisdanTerapi',
						header:'Tgl Masuk',
						dataIndex: ' ',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							//return ShowDate(record.data.TGL_OPERASI);
						}
					}

				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viTindakanMedisdanTerapi',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viTindakanMedisdanTerapi',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viTindakanMedisdanTerapi != undefined)
							{
								setLookUp_viTindakanMedisdanTerapi(rowSelected_viTindakanMedisdanTerapi.data)
							}
							else
							{								
								setLookUp_viTindakanMedisdanTerapi();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viTindakanMedisdanTerapi, selectCount_viTindakanMedisdanTerapi, dataSource_viTindakanMedisdanTerapi),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viTindakanMedisdanTerapi = new Ext.Panel
    (
		{
			title: NamaForm_viTindakanMedisdanTerapi,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viTindakanMedisdanTerapi,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viTindakanMedisdanTerapi],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viTindakanMedisdanTerapi,
		            columns: 8,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            
						{ 
							xtype: 'tbtext', 
							text: 'No. Medrec : ', 
							style:{'text-align':'left'},
							width: 75,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMedrec_viTindakanMedisdanTerapi',							
							emptyText: 'No. Medrec',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viTindakanMedisdanTerapi = 1;
										DataRefresh_viTindakanMedisdanTerapi(getCriteriaFilterGridDataView_viTindakanMedisdanTerapi());
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viTindakanMedisdanTerapi',
							emptyText: 'Nama',
							width: 150,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viTindakanMedisdanTerapi = 1;
										DataRefresh_viTindakanMedisdanTerapi(getCriteriaFilterGridDataView_viTindakanMedisdanTerapi());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Alamat : ', 
							style:{'text-align':'right'},
							width: 35,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_ALAMAT_viTindakanMedisdanTerapi',
							emptyText: 'Alamat',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viTindakanMedisdanTerapi = 1;
										DataRefresh_viTindakanMedisdanTerapi(getCriteriaFilterGridDataView_viTindakanMedisdanTerapi());								
									} 						
								}
							}
						},
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Masuk : ', 
							style:{'text-align':'left'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_viTindakanMedisdanTerapi',
							value: now_viTindakanMedisdanTerapi,
							format: 'd/M/Y',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viTindakanMedisdanTerapi();								
									} 						
								}
							}
						},						
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'center'},
							width: 50,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viTindakanMedisdanTerapi',
							value: now_viTindakanMedisdanTerapi,
							format: 'd/M/Y',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viTindakanMedisdanTerapi();								
									} 						
								}
							}
						},						
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},														
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:50,
							id: 'BtnFilterGridDataView_viTindakanMedisdanTerapi',
							handler: function() 
							{					
								DfltFilterBtn_viTindakanMedisdanTerapi = 1;
								DataRefresh_viTindakanMedisdanTerapi(getCriteriaFilterGridDataView_viTindakanMedisdanTerapi());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viTindakanMedisdanTerapi;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viTindakanMedisdanTerapi # --------------

/**
*	Function : setLookUp_viTindakanMedisdanTerapi
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viTindakanMedisdanTerapi(rowdata)
{
    var lebar = 860;
    setLookUps_viTindakanMedisdanTerapi = new Ext.Window
    (
    {
        id: 'SetLookUps_viTindakanMedisdanTerapi',
		name: 'SetLookUps_viTindakanMedisdanTerapi',
        title: NamaForm_viTindakanMedisdanTerapi, 
        closeAction: 'destroy',        
        width: 875,
        height: 650,//580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viTindakanMedisdanTerapi(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viTindakanMedisdanTerapi=undefined;
                //datarefresh_viTindakanMedisdanTerapi();
				mNoKunjungan_viTindakanMedisdanTerapi = '';
            }
        }
    }
    );

    setLookUps_viTindakanMedisdanTerapi.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viTindakanMedisdanTerapi();
		// Ext.getCmp('btnDelete_viTindakanMedisdanTerapi').disable();	
    }
    else
    {
        // datainit_viTindakanMedisdanTerapi(rowdata);
    }
}
// End Function setLookUpGridDataView_viTindakanMedisdanTerapi # --------------

/**
*	Function : getFormItemEntry_viTindakanMedisdanTerapi
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viTindakanMedisdanTerapi(lebar,rowdata)
{
    var pnlFormDataBasic_viTindakanMedisdanTerapi = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			// items:[getItemPanelInputBiodata_viTindakanMedisdanTerapi(lebar), getItemDataKunjungan_viTindakanMedisdanTerapi(lebar), getItemDataDetailTindakanMedisdanTerapi_viTindakanMedisdanTerapi(lebar)], ],
			items:[getItemPanelInputBiodata_viTindakanMedisdanTerapi(lebar), getItemDataKunjungan_viTindakanMedisdanTerapi(lebar), getItemDataDetailTindakanMedisdanTerapi_viTindakanMedisdanTerapi(lebar)], 			
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viTindakanMedisdanTerapi',
						handler: function(){
							dataaddnew_viTindakanMedisdanTerapi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viTindakanMedisdanTerapi',
						handler: function()
						{
							datasave_viTindakanMedisdanTerapi(addNew_viTindakanMedisdanTerapi);
							datarefresh_viTindakanMedisdanTerapi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viTindakanMedisdanTerapi',
						handler: function()
						{
							var x = datasave_viTindakanMedisdanTerapi(addNew_viTindakanMedisdanTerapi);
							datarefresh_viTindakanMedisdanTerapi();
							if (x===undefined)
							{
								setLookUps_viTindakanMedisdanTerapi.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viTindakanMedisdanTerapi',
						handler: function()
						{
							datadelete_viTindakanMedisdanTerapi();
							datarefresh_viTindakanMedisdanTerapi();
							
						}
					},
					
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viTindakanMedisdanTerapi;
}
// End Function getFormItemEntry_viTindakanMedisdanTerapi # --------------

/**
*	Function : getItemPanelInputBiodata_viTindakanMedisdanTerapi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viTindakanMedisdanTerapi(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[		
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Medrec',
				name: 'compNoMedrec_viTindakanMedisdanTerapi',
				id: 'compNoMedrec_viTindakanMedisdanTerapi',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 100,
						name: 'txtNoMedrec_viTindakanMedisdanTerapi',
						id: 'txtNoMedrec_viTindakanMedisdanTerapi',
						emptyText: 'No. Medrec',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 90,								
						value: 'Nama Pasien:',
						fieldLabel: 'Label',
					},															
					{
						xtype: 'textfield',					
						fieldLabel: 'Nama Pasien',								
						width: 512,
						name: 'txtNmPasien_viTindakanMedisdanTerapi',
						id: 'txtNmPasien_viTindakanMedisdanTerapi',
						emptyText: 'Nama Pasien',
					},										
				]
			},
			//-------------- ## --------------	
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
								xtype: 'displayfield',				
								width: 86,								
								value: 'Tgl. Lahir',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 20,								
								value: 'Thn',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 20,								
								value: 'Bln',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 20,								
								value: 'Hari',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 290,								
								value: 'Jenis Kelamin',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 65,								
								value: 'G. Darah',
							},				            
				        ]
				    },
		        ]
		    },
			//-------------- ## --------------
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhirEditData_viTindakanMedisdanTerapi',
		                        emptyText: '01',
		                        width: 20,
		                    },
		                    //-------------- ## -------------- 
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir2EditData_viTindakanMedisdanTerapi',
		                        emptyText: '01',
		                        width: 20,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtThnLhirEditData_viTindakanMedisdanTerapi',
		                        emptyText: '2014',
		                        width: 35,
		                    },
				            //-------------- ## --------------
							 {
		                        xtype: 'textfield',
		                        id: 'TxtThnLhir3EditData_viTindakanMedisdanTerapi',
		                        emptyText: '20',
		                        width: 20,
		                    },
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir3EditData_viTindakanMedisdanTerapi',
		                        emptyText: '01',
		                        width: 20,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir4EditData_viTindakanMedisdanTerapi',
		                        emptyText: '01',
		                        width: 20,
		                    },
				            //-------------- ## --------------
				            viComboJenisKelamin_TindakanMedisdanTerapi(290,'CmboJenisKelaminEditData_TindakanMedisdanTerapi'),
				            //-------------- ## --------------
				            viComboGolDarah_TindakanMedisdanTerapi(60,'CmboGlgnDrhEditData_TindakanMedisdanTerapi'),
				            //-------------- ## --------------
							{
								xtype: 'displayfield',				
								width: 50,								
								value: 'Poliklinik:',
							},
							{
								xtype: 'textfield',					
								fieldLabel: '',								
								width: 235,
								name: 'txtPoliklinik_viTindakanMedisdanTerapi',
								id: 'txtPoliklinik_viTindakanMedisdanTerapi',
								emptyText: 'Poliklinik Anak'
							},				    							
							// //-------------- ## --------------
							{ 
								xtype: 'tbspacer',
								height: 25,
							},
							//-------------- ## --------------
				        ]
				    },
		        ]
		    },			
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Alamat ',
				name: 'compAlamat_viTindakanMedisdanTerapi',
				id: 'compAlamat_viTindakanMedisdanTerapi',
				items: 
				[					
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 535,
						name: 'txtAlamat_viTindakanMedisdanTerapi',
						id: 'txtAlamat_viTindakanMedisdanTerapi',
						emptyText: 'Alamat'
					},
					//-------------- ## --------------					
					{
						xtype: 'displayfield',				
						width: 70,								
						value: 'Tgl. Masuk:',
					},
					//-------------- ## --------------							
					{
						xtype: 'textfield',
						flex: 1,
						width : 25,	
						name: 'txtTglMasukHr_viTindakanMedisdanTerapi',
						id: 'txtTglMasukHr_viTindakanMedisdanTerapi',
						emptyText: '01',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'textfield',
						flex: 1,
						width : 25,	
						name: 'txtTglMasukBl_viTindakanMedisdanTerapi',
						id: 'txtTglMasukBl_viTindakanMedisdanTerapi',
						emptyText: '01',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'textfield',
						flex: 1,
						width : 35,	
						name: 'txtTglMasukTh_viTindakanMedisdanTerapi',
						id: 'txtTglMasukTh_viTindakanMedisdanTerapi',
						emptyText: '2014',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
				]
			},			
			// -------------- ## --------------				
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viTindakanMedisdanTerapi # --------------

/**
*	Function : getItemDataKunjungan_viTindakanMedisdanTerapi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataKunjungan_viTindakanMedisdanTerapi(lebar) 
{		
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height: 160,
	    items:
		[				
			{
				xtype: 'panel',
				// title: 'Data Diagnosa',
				border:false,	
				height: 150,
				items: 
				[
					{
						xtype: 'tabpanel',
						activeTab: 0,		
						height: 140, 
						items: 
						[
							{
								xtype: 'panel',
								title: 'Tindakan Medis Yang Diberikan',				
								padding: '4px',
								items: 
								[	
									{
										xtype: 'compositefield',
										fieldLabel: '',
										anchor: '100%',
										width: 1200,
										items: 
										[
											TindakanMedisDetailEditData_viTindakanMedis(),																				
											//-------------- ## --------------													
											DokterMedisDetailEditData_viTindakanMedis(),
											//-------------- ## --------------	
										]
									},
									
									
								]
							},														
						]
					},									
				]
			}
		]
	};
    return items;
};
// End Function getItemDataKunjungan_viTindakanMedisdanTerapi # --------------

function gridDataTrans_viTindakanMedisdanTerapi()
{
    
    var FieldGrdKasir_viTindakanMedisdanTerapi = 
	[
	];
	
    dsDataGrdJab_viTindakanMedisdanTerapi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viTindakanMedisdanTerapi
    });  
	
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viTindakanMedisdanTerapi,
        height: 90,		
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kd T.Medis',
				sortable: true,
				width: 100
			},			
			{
				dataIndex: '',
				header: 'Tindakan Medis',
				sortable: true,
				width: 280
			},
			{
				dataIndex: '',
				header: 'Tgl Tindakan',
				sortable: true,
				width: 100
			},			
        ]
    }    
    return items;
}

function TindakanMedisDetailEditData_viTindakanMedis()
{	

	var itemsEditData_viTindakanMedis = 
	{
        xtype: 'panel',
        title: '',
        bodyStyle: 'padding: 5px 5px 5px 5px ;',
        width: 500,
        height: 100,
		border:false,
        items: 
		[			
			gridDataTrans_viTindakanMedisdanTerapi()
        ]
    }
    return itemsEditData_viTindakanMedis;
};

function DokterMedisDetailEditData_viTindakanMedis()
{	

	var itemsDokterEditData_viTindakanMedis = 
	{
        xtype: 'panel',
        title: '',
        bodyStyle: 'padding: 5px 5px 5px 5px ;',
        width: 300,//500,
        height: 100,
		border:false,
        items: 
		[			
			gridDataDokter_viTindakanMedisdanTerapi()
        ]
    }
    return itemsDokterEditData_viTindakanMedis;
};

function getItemDataDetailTindakanMedisdanTerapi_viTindakanMedisdanTerapi(lebar) 
{
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height: 300,//225,
	    items:
		[				
			{
				xtype: 'panel',
				title: '',
				border:false,	
				height: 375,
				items: 
				[
					{
						xtype: 'tabpanel',
						activeTab: 0,		
						height: 280,
						items: 
						[
							{
								xtype: 'panel',
								title: 'Anamnesis',				
								padding: '4px',
								items: 
								[	
									gridDataTerapiObat_viTindakanMedisdanTerapi(),	
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									gridDataTransfusi_viTindakanMedisdanTerapi(),		
								]
							},
							
						]
					},									
				]
			}
		]
	};
    return items;
};

function gridDataTerapiObat_viTindakanMedisdanTerapi()
{
    
    var FieldGrdTindakan_viTindakanMedisdanTerapi = 
	[
	];
	
    gridDataTerapiObat_viTindakanMedisdanTerapi= new WebApp.DataStore
	({
        fields: FieldGrdTindakan_viTindakanMedisdanTerapi
    });  
	
    var items = 
    {
        xtype: 'editorgrid',
        store: gridDataTerapiObat_viTindakanMedisdanTerapi,
        height: 140,		
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kd Obat',
				sortable: true,
				width: 100
			},			
			{
				dataIndex: '',
				header: 'Nama Obat',
				sortable: true,
				width: 300
			},
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 100
			},			
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 100
			},			
			{
				dataIndex: '',
				header: 'Tgl Pakai',
				sortable: true,
				width: 100
			},			
        ]
    }    
    return items;
}

function gridDataDokter_viTindakanMedisdanTerapi()
{
    
    var FieldGrdDokter_viTindakanMedisdanTerapi = 
	[
	];
	
    gridDataDokter_viTindakanMedisdanTerapi= new WebApp.DataStore
	({
        fields: FieldGrdDokter_viTindakanMedisdanTerapi
    });  
	
    var items = 
    {
        xtype: 'editorgrid',
        store: gridDataDokter_viTindakanMedisdanTerapi,
        height: 90,		
		width: 290,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'No.Dokter',
				sortable: true,
				width: 70
			},			
			{
				dataIndex: '',
				header: 'Nama Dokter',
				sortable: true,
				width: 150
			},
			{
				dataIndex: '',
				header: 'Tugas',
				sortable: true,
				width: 60
			},			
        ]
    }    
    return items;
}

function gridDataTransfusi_viTindakanMedisdanTerapi()
{
    
    var FieldGrdTransfusi_viTindakanMedisdanTerapi = 
	[
	];
	
    dsgridDataTransfusi_viTindakanMedisdanTerapi= new WebApp.DataStore
	({
        fields: FieldGrdTransfusi_viTindakanMedisdanTerapi
    });  
	
    var items = 
    {
        xtype: 'editorgrid',
        store: dsgridDataTransfusi_viTindakanMedisdanTerapi,
        height: 95,		
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Komponen Darah',
				sortable: true,
				width: 200
			},			
			{
				dataIndex: '',
				header: 'No Labu',
				sortable: true,
				width: 120
			},
			{
				dataIndex: '',
				header: 'cc',
				sortable: true,
				width: 100
			},			
			{
				dataIndex: '',
				header: 'Tgl Pakai',
				sortable: true,
				width: 100
			},
        ]
    }    
    return items;
}

/**
*   Function : getFormItemEntryProdukGridDataView_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan isian form produk
*/

/**
*   Function : FormSetLookupKonsultasi_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan windows popup Konsultasi
*/

function FormSetLookupKonsultasi_viTindakanMedisdanTerapi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryKonsultasi_viTindakanMedisdanTerapi = new Ext.Window
    (
        {
            id: 'FormGrdLookupKonsultasi_viTindakanMedisdanTerapi',
            title: 'Konsultasi',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupKonsultasi_viTindakanMedisdanTerapi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryKonsultasi_viTindakanMedisdanTerapi.show();
    mWindowGridLookupKonsultasi  = vWinFormEntryKonsultasi_viTindakanMedisdanTerapi; 
};

// End Function FormSetLookupKonsultasi_viTindakanMedisdanTerapi # --------------

/**
*   Function : getFormItemEntryLookupKonsultasi_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/
function getFormItemEntryLookupKonsultasi_viTindakanMedisdanTerapi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataKonsultasiWindowPopup_viTindakanMedisdanTerapi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputKonsultasiDataView_viTindakanMedisdanTerapi(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataKonsultasiWindowPopup_viTindakanMedisdanTerapi;
}
// End Function getFormItemEntryLookupKonsultasi_viTindakanMedisdanTerapi # --------------

/**
*   Function : getItemPanelInputKonsultasiDataView_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/

function viComboJenisTransaksi_viTindakanMedisdanTerapi(lebar,Nama_ID)
{
  var cbo_viComboJenisTransaksi_viTindakanMedisdanTerapi = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Transaksi..',
            fieldLabel: "Transaksi",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdTrnsksi',
                        'displayTextTrnsksi'
                    ],
                    data: [['all', "Semua"],[1, "Bayar"],[2, "Belum Bayar"]]
                }
            ),
            valueField: 'IdTrnsksi',
            displayField: 'displayTextTrnsksi',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisTransaksi_viTindakanMedisdanTerapi;
};
// End Function viComboJenisTransaksi_viTindakanMedisdanTerapi # --------------

function viComboKamarOperasi_viTindakanMedisdanTerapi(lebar,Nama_ID)
{
  var cbo_viComboKamarOperasi_viTindakanMedisdanTerapi = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Kamar Operasi..',
            fieldLabel: " ",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKamar',
                        'displayTextKamar'
                    ],
                    data: [[1, "Kamar 1"],[2, "Kamar 2"],[3, "Kamar 3"]]
                }
            ),
            valueField: 'IdKamar',
            displayField: 'displayTextKamar',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKamarOperasi_viTindakanMedisdanTerapi;
};

function getItemPanelInputKonsultasiDataView_viTindakanMedisdanTerapi(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Asal Unit',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viTindakanMedisdanTerapi(250,'CmboAsalUnit_viTindakanMedisdanTerapi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Konsultasi ke',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viTindakanMedisdanTerapi(250,'CmboKonsultasike_viTindakanMedisdanTerapi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viTindakanMedisdanTerapi(250,'CmboDokter_viTindakanMedisdanTerapi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputKonsultasiDataView_viTindakanMedisdanTerapi # --------------

// ## END MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupGantiDokter_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupGantiDokter_viTindakanMedisdanTerapi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_viTindakanMedisdanTerapi = new Ext.Window
    (
        {
            id: 'FormGrdLookupGantiDokter_viTindakanMedisdanTerapi',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupGantiDokter_viTindakanMedisdanTerapi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_viTindakanMedisdanTerapi.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_viTindakanMedisdanTerapi; 
};

// End Function FormSetLookupGantiDokter_viTindakanMedisdanTerapi # --------------

/**
*   Function : getFormItemEntryLookupGantiDokter_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupGantiDokter_viTindakanMedisdanTerapi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataGantiDokterWindowPopup_viTindakanMedisdanTerapi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputGantiDokterDataView_viTindakanMedisdanTerapi(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataGantiDokterWindowPopup_viTindakanMedisdanTerapi;
}
// End Function getFormItemEntryLookupGantiDokter_viTindakanMedisdanTerapi # --------------

/**
*   Function : getItemPanelInputGantiDokterDataView_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputGantiDokterDataView_viTindakanMedisdanTerapi(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Tanggal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtTglGantiDokter_viTindakanMedisdanTerapi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtBlnGantiDokter_viTindakanMedisdanTerapi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtThnGantiDokter_viTindakanMedisdanTerapi',
                        emptyText: '2014',
                        width: 50,
                    },
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Asal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viTindakanMedisdanTerapi(250,'CmboDokterAsal_viTindakanMedisdanTerapi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Ganti',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viTindakanMedisdanTerapi(250,'CmboDokterGanti_viTindakanMedisdanTerapi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputGantiDokterDataView_viTindakanMedisdanTerapi # --------------

// ## END MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

// ## END MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

/**
*   Function : FormSetPembayaran_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan windows popup Pembayaran
*/

function FormSetPembayaran_viTindakanMedisdanTerapi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPembayaran_viTindakanMedisdanTerapi = new Ext.Window
    (
        {
            id: 'FormGrdPembayaran_viTindakanMedisdanTerapi',
            title: 'Pembayaran',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryGridDataView_viTindakanMedisdanTerapi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPembayaran_viTindakanMedisdanTerapi.show();
    mWindowGridLookupPembayaran  = vWinFormEntryPembayaran_viTindakanMedisdanTerapi; 
};

// End Function FormSetPembayaran_viTindakanMedisdanTerapi # --------------

/**
*   Function : getFormItemEntryGridDataView_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function getFormItemEntryGridDataView_viTindakanMedisdanTerapi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataWindowPopup_viTindakanMedisdanTerapi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataGridDataView_viTindakanMedisdanTerapi(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiGridDataView_viTindakanMedisdanTerapi(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnOkGridTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnCancelGridTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtBiayaGridTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
                            name: 'txtBiayaGridTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
							style:{'text-align':'right','margin-left':'150px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtPiutangGridTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
                            name: 'txtPiutangGridTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
							style:{'text-align':'right','margin-left':'146px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtTunaiGridTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
                            name: 'txtTunaiGridTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
							style:{'text-align':'right','margin-left':'142px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtDiscGridTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
                            name: 'txtDiscGridTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
							style:{'text-align':'right','margin-left':'138px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataWindowPopup_viTindakanMedisdanTerapi;
}
// End Function getFormItemEntryGridDataView_viTindakanMedisdanTerapi # --------------

/**
*   Function : getItemPanelInputBiodataGridDataView_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemPanelInputBiodataGridDataView_viTindakanMedisdanTerapi(lebar) 
{
    var items =
    {
        title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
				xtype: 'textfield',
				fieldLabel: 'No. Transaksi',
				id: 'txtNoTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
				name: 'txtNoTransaksiPembayaranGridDataView_viTindakanMedisdanTerapi',
				emptyText: 'No. Transaksi',
				readOnly: true,
				flex: 1,
				width: 195,				
			},
			//-------------- ## --------------
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				id: 'txtNamaPasienPembayaranGridDataView_viTindakanMedisdanTerapi',
				name: 'txtNamaPasienPembayaranGridDataView_viTindakanMedisdanTerapi',
				emptyText: 'Nama Pasien',
				flex: 1,
				anchor: '100%',					
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran',
				anchor: '100%',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viTindakanMedisdanTerapi(195,'CmboPembayaranGridDataView_viTindakanMedisdanTerapi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				anchor: '100%',
				labelSeparator: '',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viTindakanMedisdanTerapi(195,'CmboPembayaranDuaGridDataView_viTindakanMedisdanTerapi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataGridDataView_viTindakanMedisdanTerapi # --------------

/**
*   Function : getItemGridTransaksiGridDataView_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemGridTransaksiGridDataView_viTindakanMedisdanTerapi(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar-80,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridDataView_viTindakanMedisdanTerapi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiGridDataView_viTindakanMedisdanTerapi # --------------

/**
*   Function : gridDataViewEditGridDataView_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridDataViewEditGridDataView_viTindakanMedisdanTerapi()
{
    
    var FieldGrdKasirGridDataView_viTindakanMedisdanTerapi = 
    [
    ];
    
    dsDataGrdJabGridDataView_viTindakanMedisdanTerapi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viTindakanMedisdanTerapi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viTindakanMedisdanTerapi,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kode',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Qty',
                sortable: true,
                width: 40
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Biaya',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Piutang',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tunai',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Disc',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridDataView_viTindakanMedisdanTerapi # --------------

// ## END MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------
function FormMoveRoom_viTindakanMedisdanTerapi() 
{
    vWinFormEntryMoveRoom_viTindakanMedisdanTerapi = new Ext.Window
    (
        {
            id: 'FormMoveRoom_viTindakanMedisdanTerapi',
            title: 'Move Room',
            closeAction: 'destroy',
            closable:true,
            width: 480,
            height: 345,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,            
            modal: true,                                   
            items: 
            [
				getFormMoveRoom_viTindakanMedisdanTerapi()
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryMoveRoom_viTindakanMedisdanTerapi.show();  
};

function getFormMoveRoom_viTindakanMedisdanTerapi()
{
	var pnlFormMoveRoom_viTindakanMedisdanTerapi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'form',
            // padding: '8px',
            bodyStyle: 'padding:5px 5px 5px 5px;',
            items:
			[
				{
					xtype: 'panel',
					title: 'Asal',
					border:true,		
					id: 'panelAsalMoveRoom_viTindakanMedisdanTerapi',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Pasien',
							anchor: '100%',
							id: 'compPasienMoveRoom_viTindakanMedisdanTerapi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Pasien:',
									id: 'labelPasienMoveRoom_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'KdPasien',														
									// width : 340,	
									width : 120,	
									name: 'txtKdPasienMoveRoom_viTindakanMedisdanTerapi',
									id: 'txtKdPasienMoveRoom_viTindakanMedisdanTerapi',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'NMPasien',														
									// width : 340,	
									width : 215,	
									name: 'txtNMPasienMoveRoom_viTindakanMedisdanTerapi',
									id: 'txtNMPasienMoveRoom_viTindakanMedisdanTerapi',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcKamarMoveRoom_viTindakanMedisdanTerapi',
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Kamar',
							anchor: '100%',
							id: 'compKamarMoveRoom_viTindakanMedisdanTerapi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Kelas:',
									id: 'labelKelasMoveRoom_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'KelasPasien',														
									// width : 340,	
									width : 120,	
									name: 'txtKelasMoveRoom_viTindakanMedisdanTerapi',
									id: 'txtKelasMoveRoom_viTindakanMedisdanTerapi',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'displayfield',
									flex: 1,
									width: 50,
									name: '',
									id: 'labelKamarMoveRoom_viTindakanMedisdanTerapi',
									value: 'Kamar:',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'NMPasien',														
									width : 160,	
									name: 'txtKamarMoveRoom_viTindakanMedisdanTerapi',
									id: 'txtKamarMoveRoom_viTindakanMedisdanTerapi',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						}
					]
				},
				{ 
					xtype: 'tbspacer',					
					id: 'spcAsalMoveRoom_viTindakanMedisdanTerapi',					
					height: 5
				},
				{
					xtype: 'panel',
					title: 'Tujuan',
					border:true,		
					id: 'panelTujuanMoveRoom_viTindakanMedisdanTerapi',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Pasien',
							anchor: '100%',
							id: 'compSpesialisMoveRoom_viTindakanMedisdanTerapi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Spesialis:',
									id: 'labelSpcMoveRoom_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								viComboSpescialis_viTindakanMedisdanTerapi(),
								{
									xtype: 'displayfield',
									flex: 1,
									width: 50,
									name: '',
									value: 'Kelas:',
									id: 'labelKlstujuanMoveRoom_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								viComboKelas_viTindakanMedisdanTerapi(),								
							]
						},
						{ 
							xtype: 'tbspacer',					
							id: 'spcRuangMoveRoom_viTindakanMedisdanTerapi',					
							height: 5
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Ruang',
							anchor: '100%',
							id: 'compRuangMoveRoom_viTindakanMedisdanTerapi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Ruang:',
									id: 'labelRuangMoveRoom_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								viComboRuangan_viTindakanMedisdanTerapi()														
							]
						},
						{ 
							xtype: 'tbspacer',					
							id: 'spcKamarMoveRoom_viTindakanMedisdanTerapi',					
							height: 5
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Ruang',
							anchor: '100%',
							id: 'compKamarMoveRoom_viTindakanMedisdanTerapi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Kamar:',
									id: 'labelKamarMoveRoom_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								viComboKamar_viTindakanMedisdanTerapi(),
								{
									xtype: 'displayfield',
									flex: 1,
									width: 50,
									name: '',
									value: 'Sisa Bed:',
									id: 'labelSisaBedMoveRoom_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									id: 'txtSisaBedMoveRoom_viTindakanMedisdanTerapi',
									name: 'txtSisaBedMoveRoom_viTindakanMedisdanTerapi',									
									width: 100,							
									readOnly: true
								}								
							]
						},
						{ 
							xtype: 'tbspacer',					
							id: 'spcTglMoveRoom_viTindakanMedisdanTerapi',					
							height: 5
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Ruang',
							anchor: '100%',
							id: 'compTglMoveRoom_viTindakanMedisdanTerapi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Tanggal:',
									id: 'dsplTglMoveRoom_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'dateTglMoveRoom_viTindakanMedisdanTerapi',
									name: 'dateTglMoveRoom_viTindakanMedisdanTerapi',
									width: 160,
									format: 'd/M/Y',
								},
								{
									xtype: 'displayfield',
									flex: 1,
									width: 80,
									name: '',
									value: 'Jam Pindah:',
									id: 'labelJamPindahMoveRoom_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'date2TglMoveRoom_viTindakanMedisdanTerapi',
									name: 'date2TglMoveRoom_viTindakanMedisdanTerapi',
									width: 90								
								}
							]
						}							
					]
				},
				{ 
					xtype: 'tbspacer',					
					id: 'spcTujuanMoveRoom_viTindakanMedisdanTerapi',					
					height: 5
				},
				{
					xtype: 'panel',
					title: '',
					border:true,		
					id: 'panelAlasanMoveRoom_viTindakanMedisdanTerapi',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[					
						{
							xtype: 'compositefield',
							fieldLabel: 'Alasan',
							anchor: '100%',
							id: 'compAlasanMoveRoom_viTindakanMedisdanTerapi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Tanggal:',
									id: 'dsplAlasanMoveRoom_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								viComboAlasanPindah_viTindakanMedisdanTerapi()
							]
						}							
					]
				},
				{
					xtype: 'panel',
					title: '',
					border:false,		
					id: 'panelButtonMoveRoom_viTindakanMedisdanTerapi',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[	
						{
							xtype: 'compositefield',
							fieldLabel: 'Alasan',
							anchor: '100%',
							id: 'compAlasanMoveRoom_viTindakanMedisdanTerapi',							
							items: 
							[					
								{
									xtype:'button',
									text:'Ok',
									width:70,
									style:{'margin-left':'300px'},
									hideLabel:true,
									id: 'ButtonOKMoveRoom_viTindakanMedisdanTerapi',
									handler:function()
									{
										
									}   
								},
								{
									xtype:'button',
									text:'Batal',
									width:70,
									style:{'margin-left':'300px'},
									hideLabel:true,
									id: 'ButtonBatalMoveRoom_viTindakanMedisdanTerapi',
									handler:function()
									{
										
									}   
								},
							]
						}
					]
				}
			]
		}
	)	
	return pnlFormMoveRoom_viTindakanMedisdanTerapi
}

function FormDeposit_viTindakanMedisdanTerapi() 
{
    vWinFormEntryDeposit_viTindakanMedisdanTerapi = new Ext.Window
    (
        {
            id: 'FormDeposit_viTindakanMedisdanTerapi',
            title: 'Deposit',
            closeAction: 'destroy',
            closable:true,
            width: 480,
            height: 325,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,            
            modal: true,                                   
            items: 
            [
				getFormDeposit_viTindakanMedisdanTerapi()
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryDeposit_viTindakanMedisdanTerapi.show();  
};

function Formcheckout_viTindakanMedisdanTerapi() 
{
    vWinFormEntrycheckout_viTindakanMedisdanTerapi = new Ext.Window
    (
        {
            id: 'Formcheckout_viTindakanMedisdanTerapi',
            title: 'Check Out',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 225,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,            
            modal: true,                                   
            items: 
            [
				getFormCheckout_viTindakanMedisdanTerapi()
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntrycheckout_viTindakanMedisdanTerapi.show();
    // mWindowGridLookupTransferPembayaran  = vWinFormEntrycheckout_viTindakanMedisdanTerapi; 
};

/**
*   Function : FormSetTransferPembayaran_viTindakanMedisdanTerapi
*   
*   Sebuah fungsi untuk menampilkan windows popup Transfer ke Rawat Inap
*/

function FormSetTransferPembayaran_viTindakanMedisdanTerapi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryTransferPembayaran_viTindakanMedisdanTerapi = new Ext.Window
    (
        {
            id: 'FormGrdTrnsferPembayaran_viTindakanMedisdanTerapi',
            title: 'Pemindahan Tagihan Ke Unit Rawat Inap',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 450,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormTransferPembayaranItemEntryGridDataView_viTindakanMedisdanTerapi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryTransferPembayaran_viTindakanMedisdanTerapi.show();
    mWindowGridLookupTransferPembayaran  = vWinFormEntryTransferPembayaran_viTindakanMedisdanTerapi; 
};

// End Function FormSetTransferPembayaran_viTindakanMedisdanTerapi # --------------

function getFormDeposit_viTindakanMedisdanTerapi()
{
    var pnlFormDeposit_viTindakanMedisdanTerapi = new Ext.FormPanel
    (
        {
            title: 'Informasi Pembayaran',
            region: 'center',
            layout: 'form',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            items:
            //-------------- #items# --------------
            [			
				{
					xtype: 'compositefield',
					fieldLabel: 'Tanggal',
					anchor: '100%',
					width: 130,
					items: 
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Tanggal',
							id: 'DatePembayaran_viTindakanMedisdanTerapi',
							name: 'DatePembayaran_viTindakanMedisdanTerapi',
							width: 100,
							format: 'd/M/Y'
						},
						{
							xtype: 'displayfield',
							flex: 1,
							width: 70,
							name: '',
							value: 'No. Bukti:',
							fieldLabel: 'Label'
						},
						{
							xtype: 'textfield',
							flex: 1,
							fieldLabel: 'Label',														
							width : 165,	
							name: 'txtNobukti_viTindakanMedisdanTerapi',
							id: 'txtNobukti_viTindakanMedisdanTerapi',
							emptyText: 'No. Bukti',
							listeners:
							{
							}
						}
						//-------------- ## --------------				
					]
				},		
				{
					xtype: 'panel',
					title: 'Informasi Pembayar',
					border:false,	
					// height:375,
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Nama',
							anchor: '100%',
							id: 'compNamadeposit_viTindakanMedisdanTerapi',
							// width: 130,
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Nama:',
									id: 'labelNamadeposit_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'Nama',														
									width : 340,	
									name: 'txtNamadeposit_viTindakanMedisdanTerapi',
									id: 'txtNamadeposit_viTindakanMedisdanTerapi',
									emptyText: 'Nama',
									listeners:
									{
									}
								}
							]
						}
					]
				},
				{
					xtype: 'panel',
					title: 'Detail Pembayar',
					border:false,	
					// height:375,
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Nama',
							anchor: '100%',
							id: 'compuntukdeposit_viTindakanMedisdanTerapi',
							// width: 130,
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Untuk:',
									id: 'labeluntukdeposit_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'Untuk',														
									width : 340,	
									name: 'txtuntukdeposit_viTindakanMedisdanTerapi',
									id: 'txtuntukdeposit_viTindakanMedisdanTerapi',
									emptyText: 'Untuk',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcPasiendeposit_viTindakanMedisdanTerapi'
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Pasien',
							anchor: '100%',
							id: 'compPasiendeposit_viTindakanMedisdanTerapi',
							// width: 130,
							items: 
							[								
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Pasien:',
									id: 'labelPasiendeposit_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 120,	
									name: 'txtPasiendeposit_viTindakanMedisdanTerapi',
									id: 'txtPasiendeposit_viTindakanMedisdanTerapi',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 215,	
									name: 'txtNmPasiendeposit_viTindakanMedisdanTerapi',
									id: 'txtNmPasiendeposit_viTindakanMedisdanTerapi',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcKamardeposit_viTindakanMedisdanTerapi'
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Kelas',
							anchor: '100%',
							id: 'compKamardeposit_viTindakanMedisdanTerapi',
							// width: 130,
							items: 
							[								
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Kamar:',
									id: 'labelKamardeposit_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 120,	
									name: 'txtKelasdeposit_viTindakanMedisdanTerapi',
									id: 'txtKelasdeposit_viTindakanMedisdanTerapi',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 215,	
									name: 'txtKamardeposit_viTindakanMedisdanTerapi',
									id: 'txtKamardeposit_viTindakanMedisdanTerapi',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcTotaldeposit_viTindakanMedisdanTerapi'
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Total',
							anchor: '100%',
							id: 'compTotaldeposit_viTindakanMedisdanTerapi',
							// width: 130,
							items: 
							[								
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Jumlah:',
									id: 'labelTotaldeposit_viTindakanMedisdanTerapi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 120,	
									name: 'txtTotaldeposit_viTindakanMedisdanTerapi',
									id: 'txtTotaldeposit_viTindakanMedisdanTerapi',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcDepositdeposit_viTindakanMedisdanTerapi'
						},
						{
							xtype: 'panel',
							title: '',				
							border:false,
							labelAlign: 'right',						
							items: 
							[
								{
									xtype: 'compositefield',
									fieldLabel: '',
									id: 'CompButtonDeposit_viTindakanMedisdanTerapi',
									items: 
									[								
										{
											xtype: 'button',
											text:'Ok',
											id: 'btnokDeposit_viTindakanMedisdanTerapi',
											name: 'btnokDeposit_viTindakanMedisdanTerapi',
											style:{'margin-left':'250px'},
											width: 60
										}
										,	
										{
											xtype: 'button',
											text:'Batal',
											id: 'btnbatalDeposit_viTindakanMedisdanTerapi',
											name: 'btnbatalDeposit_viTindakanMedisdanTerapi',
											style:{'margin-left':'250px'},
											width: 60
										},	
										{
											xtype: 'button',
											text:'Print',
											id: 'btnCetakDeposit_viTindakanMedisdanTerapi',
											name: 'btnbatalDeposit_viTindakanMedisdanTerapi',
											style:{'margin-left':'250px'},
											width: 60
										}

									]
								}
							]
						}
					]
				}			
			]
		}
	)
	return pnlFormDeposit_viTindakanMedisdanTerapi
}

function getFormCheckout_viTindakanMedisdanTerapi()
{
    var pnlFormCheckout_viTindakanMedisdanTerapi = new Ext.FormPanel
    (
        {
            title: 'Informasi Pasien Pulang',
            region: 'center',
            layout: 'form',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            items:
            //-------------- #items# --------------
            [
               {
					xtype: 'compositefield',
					fieldLabel: 'Pasien',
					id: 'compkdpasiencheckout_viTindakanMedisdanTerapi',
					items: 
                    [
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtkdpasiencheckout_viTindakanMedisdanTerapi',
                            name: 'txtkdpasiencheckout_viTindakanMedisdanTerapi',
                            width: 80,
                            readOnly: true,
                        },
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtnmpasiencheckout_viTindakanMedisdanTerapi',
                            name: 'txtnmpasiencheckout_viTindakanMedisdanTerapi',
                            width: 160,
                            readOnly: true,
                        }
					]
				},
               {
					xtype: 'compositefield',
					fieldLabel: 'Kelas',
					id: 'CompKelascheckout_viTindakanMedisdanTerapi',
					items: 
                    [
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtKelascheckout_viTindakanMedisdanTerapi',
                            name: 'txtKelascheckout_viTindakanMedisdanTerapi',
                            width: 100,
                            readOnly: true,
                        },
						{
							xtype: 'displayfield',
							flex: 1,
							width: 35,
							name: '',
							value: 'Kamar:',
							fieldLabel: 'Label'
						},
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtkamarcheckout_viTindakanMedisdanTerapi',
                            name: 'txtkamarcheckout_viTindakanMedisdanTerapi',
                            width: 100,
                            readOnly: true,
                        }
					]
				},
                {
					xtype: 'compositefield',
					fieldLabel: 'Tgl. Masuk',
					id: 'CompTglmasukcheckout_viTindakanMedisdanTerapi',
					items: 
                    [
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txttglmasukcheckout_viTindakanMedisdanTerapi',
                            name: 'txttglmasukcheckout_viTindakanMedisdanTerapi',
                            width: 100,
                            readOnly: true,
                        },
						{
							xtype: 'displayfield',
							flex: 1,
							width: 35,
							name: '',
							value: 'Jam:',
							id: 'dspljammasukcheckout_viTindakanMedisdanTerapi',
							fieldLabel: 'Label'
						},
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtjammasukcheckout_viTindakanMedisdanTerapi',
                            name: 'txtjammasukcheckout_viTindakanMedisdanTerapi',
                            width: 100,
                            readOnly: true,
                        }
					]
				},	
				{
					xtype: 'compositefield',
					fieldLabel: 'Tgl. Pulang',
					id: 'CompTglpulangcheckout_viTindakanMedisdanTerapi',
					items: 
                    [
						{
							xtype: 'datefield',
							id: 'dtfpulangcheckout_viTindakanMedisdanTerapi',
							value: now_viTindakanMedisdanTerapi,
							format: 'd/M/Y',
							width: 100
						},
						{
							xtype: 'displayfield',
							flex: 1,
							width: 35,
							name: '',
							value: 'Jam:',
							id: 'dspljammasukcheckout_viTindakanMedisdanTerapi',
							fieldLabel: 'Label'
						},
						{
							xtype: 'datefield',
							id: 'dtfjampulangcheckout_viTindakanMedisdanTerapi',
							// value: now_viTindakanMedisdanTerapi,
							// format: 'HH:MM',
							width: 100
						}
					]
				},
				viComboKeadaanPsn_viTindakanMedisdanTerapi(),						
				{
					xtype: 'panel',
					title: '',				
					border:false,
					labelAlign: 'right',
					// padding: '4px',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'CompButtoncheckout_viTindakanMedisdanTerapi',
							items: 
							[								
								{
									xtype: 'button',
									text:'Ok',
									id: 'btnok_viTindakanMedisdanTerapi',
									name: 'btnok_viTindakanMedisdanTerapi',
									style:{'margin-left':'210px'},
									width: 60
								}
								,	
								{
									xtype: 'button',
									text:'Batal',
									id: 'btnbatal_viTindakanMedisdanTerapi',
									name: 'btnbatal_viTindakanMedisdanTerapi',
									style:{'margin-left':'210px'},
									width: 75
								}
							]
						}
					]
				}
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormCheckout_viTindakanMedisdanTerapi;
}

function viComboKeadaanPsn_viTindakanMedisdanTerapi()
{
  var cbo_viComboKeadaanPsn_viTindakanMedisdanTerapi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKeadaanPsn_viTindakanMedisdanTerapi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Keadaan Pasien..',
            fieldLabel: "Keadaan Pasien",
            value:1,        
            width:245,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKeadaan',
                        'displayTextKeadaan'
                    ],
                    data: [[1, "Sembuh"],[2, "Sakit"]]
                }
            ),
            valueField: 'IdKeadaan',
            displayField: 'displayTextKeadaan',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKeadaanPsn_viTindakanMedisdanTerapi;
};
function viComboKamar_viTindakanMedisdanTerapi()
{
  var cbo_viComboKamar_viTindakanMedisdanTerapi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKamar_viTindakanMedisdanTerapi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Kamar..',
            fieldLabel: "Kamar",
            value:1,        
            width:180,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKamar',
                        'displayTextKamar'
                    ],
                    data: [[1, "011 VVIP"],[2, "012 VVIP"]]
                }
            ),
            valueField: 'IdKamar',
            displayField: 'displayTextKamar',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKamar_viTindakanMedisdanTerapi;
};

function viComboRuangan_viTindakanMedisdanTerapi()
{
  var cbo_viComboRuangan_viTindakanMedisdanTerapi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboRuangan_viTindakanMedisdanTerapi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Ruang..',
            fieldLabel: "Ruang",
            value:1,        
            width:340,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdRuang',
                        'displayTextRuang'
                    ],
                    data: [[1, "Kelas VVIP"],[2, "Kelas VVIP"]]
                }
            ),
            valueField: 'IdRuang',
            displayField: 'displayTextRuang',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboRuangan_viTindakanMedisdanTerapi;
};

function viComboSpescialis_viTindakanMedisdanTerapi()
{
  var cbo_viComboSpescialis_viTindakanMedisdanTerapi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboSpescialis_viTindakanMedisdanTerapi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Specialis..',
            fieldLabel: "Spesialis",
            value:1,        
            width:180,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdSpesialis',
                        'displayTextSpesialis'
                    ],
                    data: [[1, "Penyakit Dalam"],[2, "Jantung"]]
                }
            ),
            valueField: 'IdSpesialis',
            displayField: 'displayTextSpesialis',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboSpescialis_viTindakanMedisdanTerapi;
};

function viComboKelas_viTindakanMedisdanTerapi()
{
	var cbo_viComboKelas_viTindakanMedisdanTerapi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKelas_viTindakanMedisdanTerapi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Kelas..',
            fieldLabel: "Kelas",
            value:1,        
            width:100,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKelas',
                        'displayTextKelas'
                    ],
                    data: [[1, "VVIP"],[2, "VIP"]]
                }
            ),
            valueField: 'IdKelas',
            displayField: 'displayTextKelas',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKelas_viTindakanMedisdanTerapi;
};

function viComboAlasanPindah_viTindakanMedisdanTerapi()
{
  var ComboAlasanPindah_viTindakanMedisdanTerapi = new Ext.form.ComboBox
    (
    
        {
            id:"ComboAlasanPindah_viTindakanMedisdanTerapi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Alasan..',
            fieldLabel: "Alasan",
            value:1,        
            width:340,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdAlasanPindah',
                        'displayTextAlasanPindah'
                    ],
                    data: [[1, "Tidak Nyaman"],[2, "Tidak Mampu Bayar"]]
                }
            ),
            valueField: 'IdAlasanPindah',
            displayField: 'displayTextAlasanPindah',
            listeners:  
            {
			
            }
        }
    );
    return ComboAlasanPindah_viTindakanMedisdanTerapi;
};

function viComboJenisKelamin_TindakanMedisdanTerapi(lebar,Nama_ID)
{
    var Field_TindakanMedisdanTerapi = [' ', ' '];
    ds_TindakanMedisdanTerapi = new WebApp.DataStore({fields: Field_TindakanMedisdanTerapi});
    
	// viRefresh_Journal();
	
    var cboJK_TindakanMedisdanTerapi = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'TindakanMedisdanTerapi',
			valueField: 'TindakanMedisdanTerapi',
            displayField: 'TindakanMedisdanTerapi',
			emptyText:'Pria',
			store: ds_TindakanMedisdanTerapi,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboJK_TindakanMedisdanTerapi;
}

function viComboGolDarah_TindakanMedisdanTerapi(lebar,Nama_ID)
{
    var FieldGolDarah_TindakanMedisdanTerapi = [' ', ' '];
    dsGolDarah_TindakanMedisdanTerapi = new WebApp.DataStore({fields: FieldGolDarah_TindakanMedisdanTerapi});
    
	
    var cboGolDarah_TindakanMedisdanTerapi = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'TindakanMedisdanTerapi',
			valueField: 'TindakanMedisdanTerapi',
            displayField: 'TindakanMedisdanTerapi',
			emptyText:'A',
			store: dsGolDarah_TindakanMedisdanTerapi,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboGolDarah_TindakanMedisdanTerapi;
}

function viComboCaraKeluar_TindakanMedisdanTerapi(lebar,Nama_ID)
{
    var FieldCaraKeluar_TindakanMedisdanTerapi = [' ', ' '];
    dsCaraKeluar_TindakanMedisdanTerapi = new WebApp.DataStore({fields: FieldCaraKeluar_TindakanMedisdanTerapi});
    
	
    var cboCaraKeluar_TindakanMedisdanTerapi = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'TindakanMedisdanTerapi',
			valueField: 'TindakanMedisdanTerapi',
            displayField: 'TindakanMedisdanTerapi',
			emptyText:'Sembuh',
			store: dsCaraKeluar_TindakanMedisdanTerapi,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cboCaraKeluar_TindakanMedisdanTerapi;
}