var CurrentServiceCMApproveRequest =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPersonIntCMApproveRequest =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPersonExtCMApproveRequest =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPartCMApproveRequest =
{
    data: Object,
    details: Array,
    row: 0
};


// var select & value combo ==============================
var selectStatusCMApproveRequestEntry;
var selectCountApproveRequest = 50;
var selectStatusCMApproveRequestView=' 9999';
var valueStatusCMApproveRequestView=' All';
var valueStatusCMApproveRequestView2='(\' 9999\',\'1\',\'2\',\'7\')';
var DefaultStatusAcceptedCMApproveRequestView='6';
var DefaultStatusRejectedCMApproveRequestView='7';
var DefaultStatusApproveCMApproveRequestView='2';
var valueStatusCMApproveRequestEntry="('6','7')";
var selectStatusAssetApprove;
//=================================================

// var select grid ==============================
var rowSelectedApproveRequest;
var rowdataAsetInfoCMApproveRequest;
var cellSelectServiceCMApproveRequest;
var cellSelectPersonIntCMApproveRequest;
var cellSelectPersonExtCMApproveRequest;
var cellSelectPartCMApproveRequest;
//================================================


// var data store ===========================
var dsTRApproveRequestList;
var dsStatusCMApproveRequestView;
var dsDtlCraftPersonIntCMApproveRequest;
var dsDtlCraftPersonIntCMApproveRequestTemp;
var dsDtlCraftPersonExtCMApproveRequest;
var dsDtlCraftPersonExtCMApproveRequestTemp;
var dsDtlPartCMApproveRequest;
var dsDtlPartCMApproveRequestTemp;
var dsDtlServiceCMApproveRequest;
var dsLookVendorListCMApproveRequest;
//==========================================



//var component===========================
var TRApproveRequestLookUps;
var vTabPanelRepairCMApproveRequest;
var vTabPanelReqInfoCMApproveRequest;
//=========================================


var AddNewApproveRequest = true;
var varRowReqCMApproveRequest;
var AppIdCMApproveRequest;
var SchIdCMApproveRequest;
var IsExtRepairCMApproveRequest=false;
var NowApproveRequest = new Date();
var FocusCtrlCMApproveRequest;
var varTabRepairCMApproveRequest=1;
var varCatIdCMApproveRequest;
var varAssetIDApprove;
//var cboStatusAssetApprove;

	var mRecVendorCMApproveRequest = Ext.data.Record.create
	(
		[
		   {name: 'KD', mapping:'KD'},
		   {name: 'NAME', mapping:'NAME'}
		]
	);
	
	var mRecordServiceCMApproveRequest = Ext.data.Record.create
	(
		[
		   {name: 'SCH_CM_ID', mapping:'SCH_CM_ID'},
		   {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
		   {name: 'SERVICE_NAME', mapping:'SERVICE_NAME'},
		   {name: 'CATEGORY_ID', mapping:'CATEGORY_ID'},
		   {name: 'TAG', mapping:'TAG'}
		]
	)
	
	var mRecordPartCMApproveRequest = Ext.data.Record.create
	(
		[
		   {name: 'SCH_CM_ID', mapping:'SCH_CM_ID'},
		   {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
		   {name: 'PART_ID', mapping:'PART_ID'},
		   {name: 'PART_NAME', mapping:'PART_NAME'},
		   {name: 'QTY', mapping:'QTY'},
		   {name: 'UNIT_COST', mapping:'UNIT_COST'},
		   {name: 'TOT_COST', mapping:'TOT_COST'},
		   {name: 'DESC_SCH_PART', mapping:'DESC_SCH_PART'},
		   {name: 'TAG', mapping:'TAG'},
		   {name: 'ROW', mapping:'ROW'}
		]
	);
	
var mRecordPersonCMApproveRequest = Ext.data.Record.create
	(
		[
		   {name: 'SCH_CM_ID', mapping:'SCH_CM_ID'},
		   {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
		   {name: 'ROW_SCH', mapping:'ROW_SCH'},
		   {name: 'EMP_ID', mapping:'EMP_ID'},
		   {name: 'EMP_NAME', mapping:'EMP_NAME'},
		   {name: 'VENDOR_ID', mapping:'VENDOR_ID'},
		   {name: 'VENDOR', mapping:'VENDOR'},
		   {name: 'PERSON_NAME', mapping:'PERSON_NAME'},
		   {name: 'COST', mapping:'COST'},
		   {name: 'DESC_SCH_PERSON', mapping:'DESC_SCH_PERSON'},
		   {name: 'ROW', mapping:'ROW'}
		]
	);

CurrentPage.page = getPanelApproveRequest(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelApproveRequest(mod_id) 
{
    var Field =['APP_ID','APPROVER_ID','ASSET_MAINT_ID','STATUS_ID','PROBLEM','REQ_ID','ROW_REQ','VENDOR_ID','APP_DUE_DATE','APP_FINISH_DATE','IS_EXT_REPAIR','DESC_APP','COST','REQ_FINISH_DATE','DESC_STATUS','DESC_REQ','IMPACT','STATUS_NAME','APPROVER','ASSET_MAINT_NAME','VENDOR','ASSET_MAINT','REQ_DATE','REQUESTER_ID','REQUESTER','LOCATION_ID','LOCATION','DEPT_ID_REQUSTER','DEPT_NAME_REQUESTER','DEPT_ID_ASSET_MAINT','DEPT_NAME_ASSET','CATEGORY_ID','SCH_CM_ID','SCH_DUE_DATE','SCH_FINISH_DATE','STATUS_ASSET_ID','STATUS_ASSET']
	
    dsTRApproveRequestList = new WebApp.DataStore({ fields: Field });

    var grListTRApproveRequest = new Ext.grid.EditorGridPanel
	(
		{
		    stripeRows: true,
		    store: dsTRApproveRequestList,
		    anchor: '100% 91.9999%',
		    columnLines: true,
			autoScroll:true,
		    border: false,
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
					        rowSelectedApproveRequest = dsTRApproveRequestList.getAt(row);
					    }
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedApproveRequest = dsTRApproveRequestList.getAt(ridx);
					if (rowSelectedApproveRequest != undefined) 
					{
						ApproveRequestLookUp(rowSelectedApproveRequest.data);
					}
					else 
					{
						ShowPesanWarningCMApproveRequest(nmWarningSelectEditData,nmEditData)
					};
				}
			},
		    cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'colStatusImageApproveViewApprove',
					    header: nmStatusApproveCM,
					    dataIndex: 'STATUS_ID',
					    sortable: true,
					    width: 50,
						align:'center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store) 
						{
							 switch (value) 
							 {
								case '1':  
									 metaData.css = 'StatusHijau'; // open
									 break;
								 case '6':  
									 metaData.css = 'StatusKuning';
									 break;
								 case '2':   
									 metaData.css = 'StatusKuning';
									 break;
								 case '7':   
								 metaData.css = 'StatusMerah';
								 break;
							 }
								return '';     
						}
					},
					{
					    id: 'colTglApproveRequestViewApproveRequest',
					    header: nmRequestDateApproveCM,
					    dataIndex: 'REQ_DATE',
					    sortable: true,
					    width: 100,
                                            renderer: function(v, params, record)
                                            {
                                                return ShowDate(record.data.REQ_DATE);
                                            }
						//renderer: function(v, params, record)
						//{
					        //return ShowDate(record.data.REQ_DATE);
					    //}
					},
					{
					    header: nmRequesterApproveCM,
					    width: 100,
					    sortable: true,
					    dataIndex: 'REQUESTER',
					    id: 'colApproveRequesterViewApproveRequest'
					},
					{
					    id: 'colAssetMaintViewApproveRequest',
					    header: nmAssetIdNameApproveCM,
					    dataIndex: 'ASSET_MAINT',
					    sortable: true,
					    width: 250
					},
					{
					    id: 'colProblemViewApproveRequest',
					    header: nmProblemApproveCM,
					    dataIndex: 'PROBLEM',
					    width: 300
					},
					{
					    id: 'colApproverViewApproveRequest',
					    header: nmApproverApproveCM,
					    dataIndex: 'APPROVER',
					    width: 100
					},
					{
					    id: 'colDescStatusViewApproveRequest',
					    header: nmStatusDescApproveCM,
					    dataIndex: 'DESC_STATUS',
					    width: 300
					}
				]
			),
		    tbar:
			[
				{
				    id: 'btnEditApproveRequest',
				    text: nmEditData,
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedApproveRequest != undefined) 
						{
							ApproveRequestLookUp(rowSelectedApproveRequest.data);
						}
						else 
						{
							ShowPesanWarningCMApproveRequest(nmWarningSelectEditData,nmEditData)
						};
				    }
				}, ' ', '-',
				{
					xtype: 'checkbox',
					id: 'chkWithTglApproveRequest',
					hideLabel:true,
					checked: false,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilterApproveRequest').dom.disabled=false;
							Ext.get('dtpTglAkhirFilterApproveRequest').dom.disabled=false;
							
						}
						else
						{
							Ext.get('dtpTglAwalFilterApproveRequest').dom.disabled=true;
							Ext.get('dtpTglAkhirFilterApproveRequest').dom.disabled=true;
							
							
						};
					}
				}
				, ' ', '-', nmRequestDateApproveCM + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: 'Date From : ',
				    id: 'dtpTglAwalFilterApproveRequest',
				    format: 'd/M/Y',
				    value: NowApproveRequest,
				    width: 100,
				    onInit: function() { }
				}, ' ', nmSd + ' ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: 'to',
				    id: 'dtpTglAkhirFilterApproveRequest',
				    format: 'd/M/Y',
				    value: NowApproveRequest,
				    width: 100
				}
			]
		}
	);
	
	var LegendViewCMApproveRequest = new Ext.Panel
	(
		{
		    id: 'LegendViewCMApproveRequest',
		    region: 'center',
			border:false,
			bodyStyle: 'padding:0px 7px 0px 7px',
		    layout: 'column',
			frame:true,
			//height:32,
			anchor: '100% 8.0001%',
			autoScroll:false,
			items:
			[	
				{
					columnWidth: .033,
					layout: 'form',
					style:{'margin-top':'-1px'},
					//height:32,
					anchor: '100% 8.0001%',
					border: false,
					html: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
				},
				{
					columnWidth: .08,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style:{'margin-top':'1px'},
					border: false,
					html: " Open"
				},
				{
					columnWidth: .033,
					layout: 'form',
					style:{'margin-top':'-1px'},
					border: false,
					//height:32,
					anchor: '100% 8.0001%',
					html: '<img src="'+baseURL+'ui/images/icons/16x16/kuning.png" class="text-desc-legend"/>'
				},
				{
					columnWidth: .1,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style:{'margin-top':'1px'},
					border: false,
					html: " Approve"
				},
				{
					columnWidth: .033,
					layout: 'form',
					style:{'margin-top':'-1px'},
					border: false,
					//height:35,
					anchor: '100% 8.0001%',
					html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
				},
				{
					columnWidth: .1,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style:{'margin-top':'1px'},
					border: false,
					html: " Rejected"
				}
			]
		
		}
	)



    var FormTRApproveRequest = new Ext.Panel
	(
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormApproveCM,
		    border: false,
		    shadhow: true,
			autoScroll:false,
		    iconCls: 'ApproveRequest',
		    margins: '0 5 5 0',
		    items: [grListTRApproveRequest,LegendViewCMApproveRequest],
		    tbar:
			[
				nmStatusApproveCM + ' : ', ' ',mComboStatusCMApproveRequestView(),
				' ', '-',
					nmMaksData + ' : ', ' ', mComboMaksDataApproveRequest(),
					' ', '-',
				{
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    anchor: '25%',
				    handler: function(sm, row, rec) 
					{
				        RefreshDataApproveRequestFilter();
				    }
				}
			],
		    listeners:
			{
			    'afterrender': function() 
				{
			    }
			}
		}
	);
            
    RefreshDataApproveRequest();
    RefreshDataApproveRequest();
    
    return FormTRApproveRequest

};


function RefreshDataApproveRequest() 
{
    dsTRApproveRequestList.load
    (
            {
                    params:
                    {
                            Skip: 0,
                            Take: selectCountApproveRequest,
                            Sort: 'req_id',
                            Sortdir: 'ASC',
                            target:'ViewApproveCM',
                            param : ''
                    }
            }
    );
	
    rowSelectedApproveRequest = undefined;
    return dsTRApproveRequestList;
};

function RefreshDataApproveRequestFilter() 
{
    var KataKunci='';
	
    if (Ext.get('cboStatusCMApproveRequestView').getValue() != '' && selectStatusCMApproveRequestView != ' 9999')
    { 
        if (KataKunci === '')
        {
            if (selectStatusCMApproveRequestView === DefaultStatusApproveCMApproveRequestView)
            {
                //KataKunci = ' where status_id =  ~' + selectStatusCMApproveRequestView + '~ or status_id = ~' + DefaultStatusAcceptedCMApproveRequestView + '~';
                KataKunci = 'status_id = ~' + selectStatusCMApproveRequestView + '~ or status_id = ~' + DefaultStatusAcceptedCMApproveRequestView + '~';
            }
            else if (selectStatusCMApproveRequestView === DefaultStatusAcceptedCMApproveRequestView)
            {
                //KataKunci = ' where status_id =  ~' + selectStatusCMApproveRequestView + '~ or status_id = ~' + DefaultStatusApproveCMApproveRequestView + '~';
                KataKunci = 'status_id = ~' + selectStatusCMApproveRequestView + '~ or status_id = ~' + DefaultStatusApproveCMApproveRequestView + '~';
            }
            else
            {
                //KataKunci = ' where status_id =  ~' + selectStatusCMApproveRequestView + '~';
                KataKunci = 'status_id = ~' + selectStatusCMApproveRequestView + '~';
            };
        }
        else
        {
            if (selectStatusCMApproveRequestView === DefaultStatusApproveCMApproveRequestView )
            {
                KataKunci = ' and (status_id = ~' + selectStatusCMApproveRequestView + '~ or status_id = ~' + DefaultStatusAcceptedCMApproveRequestView + '~)';
            }
            else if (selectStatusCMApproveRequestView === DefaultStatusAcceptedCMApproveRequestView)
            {
                KataKunci = ' and status_id =  ~' + selectStatusCMApproveRequestView + '~ or status_id = ~' + DefaultStatusApproveCMApproveRequestView + '~';
            }
            else
            {
                KataKunci += ' and status_id = ~' + selectStatusCMApproveRequestView + '~';
            };
        };
    };
	
    if( Ext.get('chkWithTglApproveRequest').dom.checked === true )
    {
        KataKunci += '@^@' + Ext.get('dtpTglAwalFilterApproveRequest').getValue() + '&&'+ Ext.get('dtpTglAkhirFilterApproveRequest').getValue();
    };
        
    if (KataKunci != undefined) 
    {  
         dsTRApproveRequestList.load
        (
            {
                params:
                {
                    Skip: 0,
                    Take: selectCountApproveRequest,
                    Sort: 'req_id',
                    Sortdir: 'ASC',
                    target:'ViewApproveCM',
                    param : KataKunci
                }
            }
        );
    }
    else
    {
            RefreshDataApproveRequest();
    };

    return dsTRApproveRequestList;
};

function ApproveRequestLookUp(rowdata) 
{

    varRowReqCMApproveRequest='';
    AppIdCMApproveRequest='';
    SchIdCMApproveRequest='';
    IsExtRepairCMApproveRequest=false;
    varTabRepairCMApproveRequest=1;
    varCatIdCMApproveRequest='';
    TRApproveRequestLookUps='';
    cellSelectServiceCMApproveRequest='';
    cellSelectPersonIntCMApproveRequest='';
    cellSelectPersonExtCMApproveRequest='';
    cellSelectPartCMApproveRequest='';

    var lebar = 750;
    TRApproveRequestLookUps = new Ext.Window
    (
        {
            id: 'gridApproveRequest',
            title: nmTitleFormApproveCM,
            closeAction: 'destroy',
            width: lebar,
            height: 549,//559,
            border: false,
                resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'ApproveRequest',
            modal: true,
            items: getFormEntryTRApproveRequest(lebar),
            listeners: {
            activate: function()
                {
                    if (varBtnOkLookupEmp === true)
                        {
                                var j=dsDtlCraftPersonIntCMApproveRequest.getCount()-1;
                                 dsDtlCraftPersonIntCMApproveRequestTemp.insert(dsDtlCraftPersonIntCMApproveRequestTemp.getCount(),dsDtlCraftPersonIntCMApproveRequest.data.items[j]);
                                 GetPersonIntServiceCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
                                 varBtnOkLookupEmp=false;
                        };

                         // if (varBtnOkLookupVendor === true)
                        // {
                                        // TambahBarisPersonCMApproveRequest(dsDtlCraftPersonExtCMApproveRequest);
                                // // if(dsDtlCraftPersonExtCMApproveRequest.getCount() > 0 )
                                // // {
                                        // var j=dsDtlCraftPersonExtCMApproveRequest.getCount()-1;
                                // // }
                                // // else
                                // // {
                                        // // var j=dsDtlCraftPersonExtCMApproveRequest.getCount();
                                // // };

                                // GetRecordBaruPersonCMApproveRequest(dsDtlCraftPersonExtCMApproveRequest);
                                // dsDtlCraftPersonExtCMApproveRequest.data.items[j].data.PERSON_NAME=rowSelectedLookVendor.data.CONTACT1;
                                // dsDtlCraftPersonExtCMApproveRequestTemp.insert(dsDtlCraftPersonExtCMApproveRequestTemp.getCount(), 	dsDtlCraftPersonExtCMApproveRequest.data.items[j]);
                                // GetPersonExtServiceCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
                            // varBtnOkLookupVendor=false;
                        // };

                 if (varBtnOkLookupPart === true)
                {
                    var j=dsDtlPartCMApproveRequest.getCount()-1;
                     dsDtlPartCMApproveRequestTemp.insert(dsDtlPartCMApproveRequestTemp.getCount(), dsDtlPartCMApproveRequest.data.items[j]);
                     GetPartCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
                     CalcTotalPartCMApproveRequest('','',false)
                     varBtnOkLookupPart=false;
                };
            },
            afterShow: function()
                {
                        this.activate();
                },
                deactivate: function()
                {
                        rowSelectedApproveRequest=undefined;
                        RefreshDataApproveRequestFilter();
                }
            }
        }
    );

    TRApproveRequestLookUps.show();
    if (rowdata == undefined) 
	{
        ApproveRequestAddNew();
    }
    else 
	{
        TRApproveRequestInit(rowdata)
    }

};

function getFormEntryTRApproveRequest(lebar) 
{
    var pnlTRApproveRequest = new Ext.FormPanel
	(
		{
		    id: 'PanelTRApproveRequest',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    bodyStyle: 'padding:10px 10px 0px 10px',
			height:526,
		    anchor: '100%',
		    width: lebar,
		    border: false,
			//autoScroll:true,
		    items: [getItemPanelInputApproveRequest(lebar)],
		    tbar:
			[
				{
				    text: nmTambah,
				    id: 'btnTambahApproveRequest',
				    tooltip: nmTambah,
				    iconCls: 'add',
				    handler: function() 
					{
				        ApproveRequestAddNew();
				    }
				}, '-',
				{
				    text: nmSimpan,
				    id: 'btnSimpanApproveRequest',
				    tooltip: nmSimpan,
				    iconCls: 'save',
				    handler: function() 
					{
				        ApproveRequestSave(false);
				        RefreshDataApproveRequestFilter();
				    }
				}, '-',
				{
				    text: nmSimpanKeluar,
				    id: 'btnSimpanKeluarApproveRequest',
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{
						var x =  ApproveRequestSave(true);
				         RefreshDataApproveRequestFilter();
						if (x===undefined)
						{
							TRApproveRequestLookUps.close();
						};    
				    }
				}, '-',
				{
				    text: nmHapus,
				    id: 'btnHapusApproveRequest',
				    tooltip: nmHapus,
				    iconCls: 'remove',
					disabled:false,
				    handler: function() 
					{
				        ApproveRequestDelete();
				        RefreshDataApproveRequestFilter();
				    }
				}, '-',
				{
				    text: nmLookup,
				    id: 'btnLookupApproveRequest',
				    tooltip: nmLookup,
				    iconCls: 'find',
				    handler: function() 
					{
						if (FocusCtrlCMApproveRequest === 'colVendorRdo' && varTabRepairCMApproveRequest === 2 && cellSelectServiceCMApproveRequest != '' &&  cellSelectServiceCMApproveRequest != undefined)
						{	
							// var p = GetRecordBaruPersonCMApproveRequest(dsDtlCraftPersonExtCMApproveRequest);
							// FormLookupVendor(p,'','',true,dsDtlCraftPersonExtCMApproveRequest,true);
							 var criteria='';
							if (Ext.get('txtNamaVendorApproveRequest').dom.value != '')
							{
								//criteria = ' where VENDOR like ~%' + Ext.get('txtNamaVendorApproveRequest').dom.value + '%~';
                                                                criteria = 'vendor like ~%' + Ext.get('txtNamaVendorApproveRequest').dom.value + '%~';
							};
							FormLookupVendor(GetItemLookupVendorCMApproveRequest(),criteria);
						}
						else if (FocusCtrlCMApproveRequest === 'colEmpRdo' && varTabRepairCMApproveRequest === 2 && cellSelectServiceCMApproveRequest != '' &&  cellSelectServiceCMApproveRequest != undefined)
						{	
							// var p = GetRecordBaruPersonCMApproveRequest(dsDtlCraftPersonExtCMApproveRequest);
							// FormLookupVendor(p,'','',true,dsDtlCraftPersonExtCMApproveRequest,true);
							var criteria='';
							if (Ext.get('txtNamaVendorApproveRequest').dom.value != '')
							{
								//criteria = ' where VENDOR like ~%' + Ext.get('txtNamaVendorApproveRequest').dom.value + '%~';
                                                                criteria = 'vendor like ~%' + Ext.get('txtNamaVendorApproveRequest').dom.value + '%~';
							};
							FormLookupVendor(GetItemLookupVendorCMApproveRequest(),criteria);
						}
						else if (FocusCtrlCMApproveRequest === 'colEmp'&& cellSelectServiceCMApproveRequest != '' &&  cellSelectServiceCMApproveRequest != undefined)
						{
							var p = GetRecordBaruPersonCMApproveRequest(dsDtlCraftPersonIntCMApproveRequest);
							FormLookupEmployee('','','',true,p,dsDtlCraftPersonIntCMApproveRequest,true);
							FocusCtrlCMApproveRequest='';
						}
						else if ((FocusCtrlCMApproveRequest === 'colPart' || varTabRepairCMApproveRequest === 3 ) && FocusCtrlCMApproveRequest != 'txtPIC')
						{
							var p = GetRecordBaruPartCMApproveRequest();
							//var	str = ' where category_id = ~' + varCatIdCMApproveRequest + '~'
                                                        var	str = 'category_id = ~' + varCatIdCMApproveRequest + '~'
							var strList=GetStrListPartCMApproveRequest(true);
							if (strList !='')
							{
								//str += ' and PART_ID not in ( ' + strList + ') '
                                                                str += ' and part_id not in ( ' + strList + ') '
							};
							FormLookupPart(str,dsDtlPartCMApproveRequest,p,true,'',true);
							FocusCtrlCMApproveRequest='';
						}
						else if ((FocusCtrlCMApproveRequest === 'colService' || varTabRepairCMApproveRequest === 1 ) && FocusCtrlCMApproveRequest != 'txtPIC')
						{
							var p = GetRecordBaruServiceCMApproveRequest();
							var str = '';
							var strList = GetStrListServiceCMApproveRequest();
							
							if (strList !='')
							{
								//strList = ' and SERVICE_ID not in ( ' + strList + ') '
                                                                strList = ' and service_id not in ( ' + strList + ') '
							};
							//str = ' where category_id = ~' + varCatIdCMApproveRequest + '~';
                                                        str = 'category_id = ~' + varCatIdCMApproveRequest + '~';
							str += strList;
							
							FormLookupServiceCategory('','',str,true,p,dsDtlServiceCMApproveRequest,true);
							FocusCtrlCMApproveRequest='';
						}
						else if (FocusCtrlCMApproveRequest === 'txtPIC')
						{
							FormLookupEmployee('txtKdApproverApproveRequest','txtNamaApproverApproveRequest','');
							FocusCtrlCMApproveRequest='';
						};
				    }
				}, 
				'-', '->', '-',
				{
				    text: nmCetak,
					id:'btnCetakApproveRequest',
				    tooltip: nmCetak,
				    iconCls: 'print',
				    handler: function() {
				    var cKriteria;
				    //cKriteria = ' WHERE APP_ID=~' + AppIdCMApproveRequest + '~';
                                    cKriteria = 'app_id = ~' + AppIdCMApproveRequest + '~';
				    ShowReport('', '920008', cKriteria);
					}
				}
			]
		}
	); 
	
	


    var FormTRApproveRequest = new Ext.Panel
	(
		{
		    id: 'FormTRApproveRequest',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRApproveRequest]
		}
	);

    return FormTRApproveRequest
};
///---------------------------------------------------------------------------------------///


function RecordBaruApproveRequest()
{
    var p = new mRecordApproveRequest
    (
        {
           'MAK':'',
           'KD_ASSET_MAIN':'',
           'ASSET_MAIN_NAME':'',
           'KD_LOCATION':'',
           'LOCATION':'',
           'PROBLEM':'',
           'FINISH_DATE':'',
           'STATUS_REQ_CM':'',
           'DESCRIPTION_REQ':'',
           'LINE':''
        }
    );

    return p;
};

function TRApproveRequestInit(rowdata)
{

	// Request Information
        Ext.get('txtNoApproveRequest').dom.value=rowdata.REQ_ID;
	Ext.get('dtpTanggalApproveRequest').dom.value=ShowDate(rowdata.REQ_DATE);
	Ext.get('txtKdApproveRequesterApproveRequest').dom.value=rowdata.REQUESTER_ID;
	Ext.get('txtApproveRequesterApproveRequest').dom.value=rowdata.REQUESTER;
	Ext.get('txtDepartmentApproveRequest').dom.value=rowdata.DEPT_NAME_REQUESTER;
	Ext.get('cboStatusAssetApprove').dom.value=rowdata.STATUS_ASSET;
	selectStatusAssetApprove=rowdata.STATUS_ASSET_ID;
	
	varRowReqCMApproveRequest=rowdata.ROW_REQ;
	rowdataAsetInfoCMApproveRequest = rowdata;
	varCatIdCMApproveRequest=rowdata.CATEGORY_ID
	varAssetIDApprove=rowdata.ASSET_MAINT_ID;
	
	
	if (rowdata.APP_ID === null || rowdata.APP_ID === undefined || rowdata.APP_ID === '')
	{
		AddNewApproveRequest = true;
		Ext.get('rdoInternalApproveReq').dom.checked=true;
		IsExtRepairCMApproveRequest=false;
		AppIdCMApproveRequest='';
		SchIdCMApproveRequest='';
		DisabledVendorCMApproveRequest(true);
	}
	else
	{
		 AddNewApproveRequest=false;
		 SchIdCMApproveRequest=rowdata.SCH_CM_ID;
		 AppIdCMApproveRequest=rowdata.APP_ID;
		 LoadDataServiceCMApproveRequest(SchIdCMApproveRequest);
		 LoadDataPartCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
		 GetJumlahTotalPersonPartCMApproveRequest(false);
		 Ext.get('txtKdApproverApproveRequest').dom.value=rowdata.APPROVER_ID;
		 Ext.get('txtNamaApproverApproveRequest').dom.value=rowdata.APPROVER;
		 Ext.get('dtpPengerjaanAppRequest').dom.value=ShowDate(rowdata.APP_DUE_DATE);
		 Ext.get('dtpSelesaiAppRequest').dom.value=ShowDate(rowdata.APP_FINISH_DATE);
		 
		 
		 if(rowdata.IS_EXT_REPAIR === false)
		 {
			DisabledVendorCMApproveRequest(true);
			IsExtRepairCMApproveRequest=false;
			Ext.get('rdoInternalApproveReq').dom.checked=true;
			LoadDataEmployeeCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
		 }
		 else
		 {
			DisabledVendorCMApproveRequest(false);
			Ext.get('rdoExternalApproveReq').dom.checked=true;
			IsExtRepairCMApproveRequest=true;
			GetVendorCMApproveRequest();
			LoadDataVendorCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
		 };
		 Ext.get('txtCostRateApproveRequest').dom.value=formatCurrency(rowdata.COST);
		 
		 if (rowdata.DESC_APP != null)
		 {
			Ext.get('txtDeskripsiApproveRequest').dom.value=rowdata.DESC_APP;
		 }
		 else
		 {
			Ext.get('txtDeskripsiApproveRequest').dom.value='';
		 };
		 
		 
		 Ext.get('cboStatusCMApproveRequestEntry').dom.value=rowdata.STATUS_NAME;
		 selectStatusCMApproveRequestEntry=rowdata.STATUS_ID;
		  if (rowdata.DESC_STATUS != null)
		 {
			Ext.get('txtKetStatusApproveRequest').dom.value=rowdata.DESC_STATUS;
		 }
		 else
		 {
			Ext.get('txtKetStatusApproveRequest').dom.value='';
		 };
		 
	};
	
};

function mEnabledCMApproveRequest(mBol)
{
     Ext.get('btnLookupApproveRequest').dom.disabled=mBol;
     //document.getElementById('btnTambahBrsServiceCMApproveRequest').disabled=mBol;
     Ext.get('btnTambahBrsServiceCMApproveRequest').dom.disabled=mBol;
     Ext.get('btnHpsBrsServiceCMApproveRequest').dom.disabled=mBol;
     Ext.get('rdoInternalApproveReq').dom.disabled=mBol;
     Ext.get('rdoExternalApproveReq').dom.disabled=mBol;
     Ext.get('dtpPengerjaanAppRequest').dom.disabled=mBol;
     Ext.get('dtpSelesaiAppRequest').dom.disabled=mBol;
     Ext.get('txtCostRateApproveRequest').dom.disabled=mBol;
     Ext.get('txtDeskripsiApproveRequest').dom.disabled=mBol;

     if (varTabRepairCMApproveRequest === 2)
     {
         Ext.get('btnTambahBrsCraftPersonIntCMApproveRequest').dom.disabled=mBol;
         Ext.get('btnHpsBrsCraftPersonIntCMApproveRequest').dom.disabled=mBol;
         Ext.get('btnTambahBrsCraftPersonExtCMApproveRequest').dom.disabled=mBol;
         Ext.get('btnHpsBrsCraftPersonExtCMApproveRequest').dom.disabled=mBol;
     }
     else if (varTabRepairCMApproveRequest === 3)
     {
         Ext.get('btnTambahBrsPartCMApproveRequest').dom.disabled=mBol;
         Ext.get('btnHpsBrsPartCMApproveRequest').dom.disabled=mBol;
     };
};


function LoadDataServiceCMApproveRequest(SchId) 
{
    dsDtlServiceCMApproveRequest.load
    (
        {
            params:
                {
                    Skip: 0,
                    Take: 1000,
                    //Sort: 'SERVICE_ID',
                    Sort: 'service_id',
                    Sortdir: 'ASC',
                    target: 'ViewSchServiceCM',
                    //param: 'where sch_cm_id =~' + SchId + '~'
                    param: 'sch_cm_id = ~' + SchId + '~'
                }
        }
    );
    return dsDtlServiceCMApproveRequest;
};

function LoadDataEmployeeCMApproveRequestTemp(SchId,CatId) 
{

    dsDtlCraftPersonIntCMApproveRequestTemp.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'EMP_ID',
                //Sort: 'emp_id',
                Sort: 'row_sch',
                Sortdir: 'ASC',
                target: 'ViewSchPersonCM',
                //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~'
                param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
            }
        }
    );
    return dsDtlCraftPersonIntCMApproveRequestTemp;
};

function LoadDataEmployeeCMApproveRequest(SchId,CatId,ServId) 
{
	
    dsDtlCraftPersonIntCMApproveRequest.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'EMP_ID',
                //Sort: 'emp_id',
                Sort: 'row_sch',
                Sortdir: 'ASC',
                target: 'ViewSchPersonCM',
                //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~ and service_id = ~' + ServId + '~'
                param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~ and service_id = ~' + ServId + '~'
            }
        }
    );
    return dsDtlCraftPersonIntCMApproveRequest;
};

function LoadDataVendorCMApproveRequestTemp(SchId,CatId) 
{
    dsDtlCraftPersonExtCMApproveRequestTemp.load
    (
        {
            params:
                {
                    Skip: 0,
                    Take: 1000,
                    //Sort: 'VENDOR_ID',
                    //Sort: 'vendor_id',
                    Sort: 'row_sch',
                    Sortdir: 'ASC',
                    target: 'ViewSchPersonCM',
                    //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~'
                    param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
                }
        }
    );
    return dsDtlCraftPersonExtCMApproveRequestTemp;
};

function LoadDataVendorCMApproveRequest(SchId,CatId,ServId) 
{	
    dsDtlCraftPersonExtCMApproveRequest.load
    (
        {
            params:
                {
                    Skip: 0,
                    Take: 1000,
                    //Sort: 'VENDOR_ID',
                    //Sort: 'vendor_id',
                    Sort: 'row_sch',
                    Sortdir: 'ASC',
                    target: 'ViewSchPersonCM',
                    //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~ and service_id = ~' + ServId + '~'
                    param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~ and service_id = ~' + ServId + '~'
                }
        }
    );
    return dsDtlCraftPersonExtCMApproveRequest;
};

function LoadDataPartCMApproveRequestTemp(SchId,CatId) 
{
    dsDtlPartCMApproveRequestTemp.load
    (
        {
            params:
                {
                    Skip: 0,
                    Take: 1000,
                    //Sort: 'PART_ID',
                    Sort: 'part_id',
                    Sortdir: 'ASC',
                    target: 'ViewSchPartCM',
                    //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~'
                    param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
                }
        }
    );
    return dsDtlPartCMApproveRequestTemp;
};

function LoadDataPartCMApproveRequest(SchId,CatId,ServId) 
{	
    dsDtlPartCMApproveRequest.load
    (
        {
            params:
                {
                    Skip: 0,
                    Take: 1000,
                    //Sort: 'PART_ID',
                    Sort: 'part_id',
                    Sortdir: 'ASC',
                    target: 'ViewSchPartCM',
                    //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~ and service_id = ~' + ServId + '~'
                    param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~ and service_id = ~' + ServId + '~'
                }
        }
    );
    return dsDtlPartCMApproveRequest;
};

function DisabledVendorCMApproveRequest(mBol)
{
	//Ext.get('rdoInternalApproveReq').dom.checked=mBol;
	Ext.get('txtNamaVendorApproveRequest').dom.disabled=mBol;
	
	if (mBol === true)
	{
		Ext.get('txtNamaVendorApproveRequest').dom.value='';
		Ext.get('txtKdVendorApproveRequest').dom.value='';
	};
};

///---------------------------------------------------------------------------------------///
function ApproveRequestAddNew() 
{
    AddNewApproveRequest = true;
	AppIdCMApproveRequest='';
	SchIdCMApproveRequest='';
	dsDtlPartCMApproveRequest.removeAll();
	dsDtlPartCMApproveRequestTemp.removeAll();
	dsDtlCraftPersonIntCMApproveRequest.removeAll();
	dsDtlCraftPersonIntCMApproveRequestTemp.removeAll();
	dsDtlCraftPersonExtCMApproveRequest.removeAll();
	dsDtlCraftPersonExtCMApproveRequestTemp.removeAll();
	dsDtlServiceCMApproveRequest.removeAll();
	if (dsLookVendorListCMApproveRequest != undefined)
	{
		dsLookVendorListCMApproveRequest.removeAll();
	};
	
	if (varTabRepairCMApproveRequest === 2)
	{
		Ext.get('txtTotalPersonCostCMApproveRequest').dom.value=0;
	};
	
	if (varTabRepairCMApproveRequest === 3)
	{
		Ext.get('TotalPartCMApproveRequest').dom.value=0;
	};
	
	
	Ext.get('txtCostRateApproveRequest').dom.value=0;
	Ext.get('txtTotalServiceCMApproveRequest').dom.value=0;
	Ext.get('rdoInternalApproveReq').dom.checked = true
	IsExtRepairCMApproveRequest=false;
	varTabRepairCMApproveRequest=1;
	Ext.get('txtKdApproverApproveRequest').dom.value='';
	Ext.get('txtNamaApproverApproveRequest').dom.value='';
	Ext.get('txtDeskripsiApproveRequest').dom.value='';
	Ext.get('txtKetStatusApproveRequest').dom.value='';
	Ext.get('txtKdVendorApproveRequest').dom.value='';
	Ext.get('txtNamaVendorApproveRequest').dom.value='';


};
///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamApproveRequest() 
{
    var params =
	{
		Table:'ViewApproveCM',
		AppId:AppIdCMApproveRequest,
		SchId:SchIdCMApproveRequest,
		CatId:varCatIdCMApproveRequest,
		ReqId:Ext.get('txtNoApproveRequest').dom.value,
		RowReq:varRowReqCMApproveRequest,
		ApproverId:Ext.get('txtKdApproverApproveRequest').dom.value,
		DueDate:Ext.get('dtpPengerjaanAppRequest').dom.value,
		FinishDate:Ext.get('dtpSelesaiAppRequest').dom.value,
		DescApp:Ext.get('txtDeskripsiApproveRequest').dom.value,
		AssetID:varAssetIDApprove,
		StatusAsset:selectStatusAssetApprove,
		IsExt:GetIsExtApproveRequest(),
		Cost:GetNilaiCurrencyCMApproveRequest(Ext.get('txtCostRateApproveRequest').dom.value),
		StatusId:selectStatusCMApproveRequestEntry,
		DescStatus:Ext.get('txtKetStatusApproveRequest').dom.value,
		ListService:getArrDetailServiceCMApproveRequest(),
		ListPerson:getArrDetailPersonCMApproveRequest(),
		ListPart:getArrDetailPartCMApproveRequest(),
		JmlFieldService: mRecordServiceCMApproveRequest.prototype.fields.length-3,
		JmlFieldPerson: mRecordPersonCMApproveRequest.prototype.fields.length-2,
		JmlFieldPart: mRecordPartCMApproveRequest.prototype.fields.length-4,
		JmlListService:GetListCountDetailServiceCMApproveRequest(),
		JmlListPerson:GetListCountDetailPersonCMApproveRequest(),
		JmlListPart:GetListCountDetailPartCMApproveRequest(),
		Hapus:1,
		Service:0
	};
	
    return params
};

function getArrDetailServiceCMApproveRequest()
{
	var x='';
	for(var i = 0 ; i < dsDtlServiceCMApproveRequest.getCount();i++)
	{
		if (dsDtlServiceCMApproveRequest.data.items[i].data.SERVICE_ID != '' && dsDtlServiceCMApproveRequest.data.items[i].data.SERVICE_NAME != '' && dsDtlServiceCMApproveRequest.data.items[i].data.CATEGORY_ID != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'SERVICE_ID=' + dsDtlServiceCMApproveRequest.data.items[i].data.SERVICE_ID
			y += z + 'CATEGORY_ID='+ dsDtlServiceCMApproveRequest.data.items[i].data.CATEGORY_ID
			
			if (i === (dsDtlServiceCMApproveRequest.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
		dsDtlServiceCMApproveRequest.data.items[i].data.TAG='';
	}	
	return x;
};

function GetListCountDetailServiceCMApproveRequest()
{
	var x=0;
	for(var i = 0 ; i < dsDtlServiceCMApproveRequest.getCount();i++)
	{
		if (dsDtlServiceCMApproveRequest.data.items[i].data.SERVICE_ID != '' && dsDtlServiceCMApproveRequest.data.items[i].data.SERVICE_NAME  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getArrDetailPersonCMApproveRequest()
{
	var x='';
	var ds='';
	if (IsExtRepairCMApproveRequest === false)
	{
		ds=dsDtlCraftPersonIntCMApproveRequestTemp;
	}
	else
	{
		ds=dsDtlCraftPersonExtCMApproveRequestTemp; 
		for(var i = 0 ; i < ds.getCount();i++)
		{
			if(ds.data.items[i].data.PERSON_NAME === '')
			{
				ds.removeAt(i);
			};
		}
	};
	
	if(ds.getCount() > 0) 
	{
		for(var i = 0 ; i < ds.getCount();i++)
		{
			var  mDataKd='';
			var  mDataName='';
			var  mFlag='';
			
			if (IsExtRepairCMApproveRequest === false)  // internal
			{
				mDataKd = ds.data.items[i].data.EMP_ID;
				mDataName = ds.data.items[i].data.EMP_NAME;
				mFlag='xxx';
			}
			else
			{   
				mDataKd = ds.data.items[i].data.VENDOR_ID;  // external
				mDataName = ds.data.items[i].data.VENDOR;
				mFlag=ds.data.items[i].data.PERSON_NAME;
			};
			
			if (mDataKd != '' && mDataName != '' && mFlag != '')
			{
				var y='';
				var z='@@##$$@@';
				
				y = 'SERVICE_ID=' + ds.data.items[i].data.SERVICE_ID
				y += z + 'ROW_SCH='+ds.data.items[i].data.ROW_SCH
				y += z + 'EMP_ID='+ds.data.items[i].data.EMP_ID
				y += z + 'EMP_NAME='+ds.data.items[i].data.EMP_NAME
				y += z + 'VENDOR_ID='+ds.data.items[i].data.VENDOR_ID
				y += z + 'VENDOR='+ds.data.items[i].data.VENDOR
				
				if (IsExtRepairCMApproveRequest === false)  // internal
				{
					y += z + 'PERSON_NAME='+ ds.data.items[i].data.EMP_NAME
				}
				else
				{
					y += z + 'PERSON_NAME='+ ds.data.items[i].data.PERSON_NAME
				};
				
				y += z + 'COST='+ ds.data.items[i].data.COST
				y += z + 'DESC_SCH_PERSON='+ds.data.items[i].data.DESC_SCH_PERSON
				
				if (i === (ds.getCount()-1))
				{
					x += y 
				}
				else
				{
					x += y + '##[[]]##'
				};
			};
		}	
	}	
	else
	{
		if (IsExtRepairCMApproveRequest === true)  // external
		{
			if(Ext.get('txtKdVendorApproveRequest').dom.value != '' && dsDtlServiceCMApproveRequest.getCount() > 0)
			{
				var y='';
				var z='@@##$$@@';
				
				
				y = 'SERVICE_ID=' + dsDtlServiceCMApproveRequest.data.items[0].data.SERVICE_ID
				y += z + 'ROW_SCH=1'
				y += z + 'EMP_ID='+ ''
				y += z + 'EMP_NAME='+ ''
				y += z + 'VENDOR_ID='+ Ext.get('txtKdVendorApproveRequest').dom.value
				y += z + 'VENDOR='+ Ext.get('txtNamaVendorApproveRequest').dom.value
				if (rowSelectedLookVendor != undefined)
				{
					y += z + 'PERSON_NAME='+ rowSelectedLookVendor.data.CONTACT1
				}
				else
				{
					y += z + 'PERSON_NAME='+ ''
				};
				y += z + 'COST=0'
				y += z + 'DESC_SCH_PERSON='+ ''
				
				
				x += y 
				
			};
		};
	};
	
	return x;
};

function GetListCountDetailPersonCMApproveRequest()
{
	var x=0;
	
	if (IsExtRepairCMApproveRequest === false)
	{
		ds=dsDtlCraftPersonIntCMApproveRequestTemp;
	}
	else
	{
		ds=dsDtlCraftPersonExtCMApproveRequestTemp; 
	};
	
	if(ds.getCount() > 0)
	{
		for(var i = 0 ; i < ds.getCount();i++)
		{
		
			var  mDataKd='';
			var  mDataName='';
			var  mFlag='';
			
			if (IsExtRepairCMApproveRequest === false)  // internal
			{
				mDataKd = ds.data.items[i].data.EMP_ID;
				mDataName = ds.data.items[i].data.EMP_NAME;
				mFlag='xxx';
			}
			else
			{  
				mDataKd = ds.data.items[i].data.VENDOR_ID; // external
				mDataName = ds.data.items[i].data.VENDOR;
				mFlag = ds.data.items[i].data.PERSON_NAME;
			};
		
			if (mDataKd != '' && mDataName  != '' && mFlag !='')
			{
				x += 1;
			};
		}
	}
	else
	{
		if (IsExtRepairCMApproveRequest === true)  // external
		{
			if(Ext.get('txtKdVendorApproveRequest').dom.value != '' && dsDtlServiceCMApproveRequest.getCount() > 0)
			{
				x=1;
			};
		};
	};
	
	return x;
	
};

function getArrDetailPartCMApproveRequest()
{
	var x='';
	for(var i = 0 ; i < dsDtlPartCMApproveRequestTemp.getCount();i++)
	{
		if (dsDtlPartCMApproveRequestTemp.data.items[i].data.PART_ID != '' && dsDtlPartCMApproveRequestTemp.data.items[i].data.PART_NAME != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'SERVICE_ID=' + dsDtlPartCMApproveRequestTemp.data.items[i].data.SERVICE_ID
			y += z + 'PART_ID=' + dsDtlPartCMApproveRequestTemp.data.items[i].data.PART_ID
			y += z + 'QTY='+ dsDtlPartCMApproveRequestTemp.data.items[i].data.QTY
			y += z + 'UNIT_COST='+ dsDtlPartCMApproveRequestTemp.data.items[i].data.UNIT_COST
			y += z + 'TOT_COST='+ (dsDtlPartCMApproveRequestTemp.data.items[i].data.UNIT_COST * dsDtlPartCMApproveRequestTemp.data.items[i].data.QTY)
			y += z + 'DESC_SCH_PART='+dsDtlPartCMApproveRequestTemp.data.items[i].data.DESC_SCH_PART
			
			if (i === (dsDtlPartCMApproveRequestTemp.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
		dsDtlPartCMApproveRequestTemp.data.items[i].data.TAG='';
	}	
	return x;
};

function GetListCountDetailPartCMApproveRequest()
{
	var x=0;
	for(var i = 0 ; i < dsDtlPartCMApproveRequestTemp.getCount();i++)
	{
		if (dsDtlPartCMApproveRequestTemp.data.items[i].data.PART_ID != '' && dsDtlPartCMApproveRequestTemp.data.items[i].data.PART_NAME  != '')
		{
			x += 1;
		};
	}
	return x;
	
};


function GetIsExtApproveRequest()
{
	var x;
	if (Ext.get('rdoInternalApproveReq').dom.checked === true) 
	{
		x=0;
	}
	else
	{
		x=1;
	};
	
	return x;
};

function getItemPanelInputApproveRequest(lebar) 
{
	vTabPanelReqInfoCMApproveRequest = new Ext.TabPanel
	(
		{
		    id: 'vTabPanelReqInfoCMApproveRequest',
		    region: 'center',
		    margins: '7 7 7 7',
		    bodyStyle: 'padding:10px 10px 10px 10px',
		    activeTab: 0,
		    plain: true,
		    defferedRender: false,
		    frame: false,
		    border: true,
		    height: 122,
		    anchor: '100%',
		    items:
			[
			    {
			         title: nmTitleTabInfoReqApproveCM,
			         id: 'tabReqInfoCMApproveRequest ',
			         items:
			         [
						getItemPanelNoApproveRequest(lebar),getItemPanelNoApproveRequest2(lebar),
						getItemPanelDepartApproveRequest(lebar) 
					 ]
				},
				{
			         title: nmTitleTabInfoAsetApproveCM,
			         id: 'tabMainInfoCMApproveRequest ',
			         items:
			         [
						getItemPanelNoAssetApproveRequest(lebar),
						getItemPanelProblemApproveRequest(lebar)
					 ],
					 listeners: 
					{
					   activate: function()
					    {
							if (rowdataAsetInfoCMApproveRequest != undefined)
							{
								// Aset Information
								Ext.get('txtKdMainAssetApproveRequest').dom.value=rowdataAsetInfoCMApproveRequest.ASSET_MAINT_ID;
								Ext.get('txtNamaMainAsetApproveRequest').dom.value=rowdataAsetInfoCMApproveRequest.ASSET_MAINT_NAME;
								Ext.get('txtLocationApproveRequest').dom.value=rowdataAsetInfoCMApproveRequest.LOCATION;
								Ext.get('txtProblemApproveRequest').dom.value=rowdataAsetInfoCMApproveRequest.PROBLEM;
								Ext.get('dtpTglSelesaiApproveRequest').dom.value=ShowDate(rowdataAsetInfoCMApproveRequest.REQ_FINISH_DATE);
								Ext.get('DeptAssetApproveRequest').dom.value=rowdataAsetInfoCMApproveRequest.DEPT_NAME_ASSET;
							};
						}
					}
				}
			]
		}
	);

    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,//49,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
		border:false,
		height:734,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,//49,
				labelWidth:90,
			    layout: 'form',
			    border: false,
			    items:
				[
					vTabPanelReqInfoCMApproveRequest,
					{
						xtype:'fieldset',
						title: nmTitleTabApprovalApproveCM,	
						anchor:  '99.99%',
						height:'348px',
						items :
						[
							getItemPanelApproverApproveRequest(lebar),
							{
								xtype: 'textfield',
								fieldLabel: nmStatusDescApproveCM + ' ',
								name: 'txtKetStatusApproveRequest',
								id: 'txtKetStatusApproveRequest',
								anchor: '100%'
							},getItemPanelRadioVendorCMApproveRequest(lebar), 
							getItemTabPanelRepairCMApproveRequest(),
							getItemPanelTanggalAppApproveRequest(lebar),
							getItemPanelNotesApproveRequest(lebar) 
						]
					}
				]
			}
		]
	};
    return items;
};

function getItemPanelNotesApproveRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .593,
			    layout: 'form',
			    border: false,
			    items:
				[
					
					{
						xtype: 'textarea',
						fieldLabel: nmNoteApproveCM + ' ',
						name: 'txtDeskripsiApproveRequest',
						id: 'txtDeskripsiApproveRequest',
						scroll:true,
						anchor: '100% 83%'
					}
				]
			},
			{
			    columnWidth: .4,
			    layout: 'form',
			    border: false,
				labelWidth:105,
			    items:
				[ 
					mComboStatusAssetApprove()
				]
			}
		]
	}
    return items;
};

function getItemTabPanelRepairCMApproveRequest()
{
	var TotalServiceCMApproveRequest = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 666,
			border:false,
			labelAlign:'right',
			labelWidth:200,
			style: 
			{
				'margin-top': '3px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: nmTotalPersonPartApproveCM + ' ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalServiceCMApproveRequest',
					name:'txtTotalServiceCMApproveRequest',
					readOnly:true,
					width: 150
				}
			]
		}
	);
	
	var TotalPersonCostCMApproveRequest = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 666,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '3px',
				'margin-left': '461px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: nmTotalApproveCM + ' ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalPersonCostCMApproveRequest',
					name:'txtTotalPersonCostCMApproveRequest',
					readOnly:true,
					width: 150
				}
			]
		}
	);

	var TotalPartCMApproveRequest = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 666,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '3px',
				'margin-left': '461px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: nmTotalApproveCM + ' ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalPartCMApproveRequest',
					name:'txtTotalPartCMApproveRequest',
					readOnly:true,
					width: 150
				}
			]
		}
	);

    vTabPanelRepairCMApproveRequest = new Ext.TabPanel
	(
		{
		    id: 'vTabPanelRepairCMApproveRequest',
		    region: 'center',
		    margins: '5 5 5 5',
			style:
			{
			   'margin-top': '-7px'
			},
		    bodyStyle: 'padding:5px 5px 5px 5px',
		    activeTab: 0,
		    plain: true,
		    defferedRender: false,
		    frame: false,
		    border: true,
		    height: 182,//195,//215,
			width:742,
		    anchor: '100%',
		    items:
			[
				{
					 title: nmTitleTabServiceApproveCM,
					 id: 'tabServiceCMApproveRequest',
					 items:
					 [
						GetDTLServiceCMApproveRequestGrid(),TotalServiceCMApproveRequest
					 ],
					 listeners:
					{
						activate: function() 
						{
							FocusCtrlCMApproveRequest === 'colService'
							if(varTabRepairCMApproveRequest === 2 || varTabRepairCMApproveRequest===3)
							{
								if (selectStatusCMApproveRequestEntry === DefaultStatusRejectedCMApproveRequestView)
								{
									mEnabledCMApproveRequest(true);
								}
								else
								{
									mEnabledCMApproveRequest(false);
								};
							};
							varTabRepairCMApproveRequest=1;
						}
					}
				},
			    {
					 title: nmTitleTabPersonApproveCM,
					 id: 'tabPersonCMApproveRequest',
					 items:
					 [
						getItemPanelCardLayoutGridPersonCMApproveRequest(),TotalPersonCostCMApproveRequest
					 ],
			        listeners:
					{
						  activate: function() 
						  {
							if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest != undefined)
							{
								if (cellSelectServiceCMApproveRequest.data.SERVICE_ID === undefined || cellSelectServiceCMApproveRequest.data.SERVICE_ID === '')
								{
									vTabPanelRepairCMApproveRequest.setActiveTab('tabServiceCMApproveRequest');
									varTabRepairCMApproveRequest=1;
									alert(nmAlertTabService);
								}
								else
								{
									if (IsExtRepairCMApproveRequest === true)
									{
										Ext.getCmp('FormPanelCardLayoutGridPersonCMApproveRequest').getLayout().setActiveItem(1);	
										FocusCtrlCMApproveRequest = 'colVendor'
										GetPersonExtServiceCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
									}
									else
									{
										Ext.getCmp('FormPanelCardLayoutGridPersonCMApproveRequest').getLayout().setActiveItem(0);	
										FocusCtrlCMApproveRequest = 'colEmp'
										GetPersonIntServiceCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
									};
									CalcTotalPersonCMApproveRequest('',false);
									varTabRepairCMApproveRequest=2;
									if (selectStatusCMApproveRequestEntry === DefaultStatusRejectedCMApproveRequestView)
									{
										mEnabledCMApproveRequest(true);
									}
									else
									{
										mEnabledCMApproveRequest(false);
									};
								};
							}
							else
							{
								vTabPanelRepairCMApproveRequest.setActiveTab('tabServiceCMApproveRequest');
								varTabRepairCMApproveRequest=1;
								alert(nmAlertTabService);
							};
						  }
					}
				},
				{
					 title: nmTitleTabPartApproveCM,
					 id: 'tabPartCMApproveRequest',
					 items:
					 [
						GetDTLPartCMApproveRequestGrid(),TotalPartCMApproveRequest
					 ],
					 listeners:
					{
						activate: function() 
						{
							if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest != undefined)
							{
								if (cellSelectServiceCMApproveRequest.data.SERVICE_ID === undefined || cellSelectServiceCMApproveRequest.data.SERVICE_ID === '')
								{
									vTabPanelRepairCMApproveRequest.setActiveTab('tabServiceCMApproveRequest');
									varTabRepairCMApproveRequest=1;
									alert(nmAlertTabService);
								}
								else
								{
									FocusCtrlCMApproveRequest === 'colPart'
									varTabRepairCMApproveRequest=3;
									GetPartCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
									CalcTotalPartCMApproveRequest('','',false);
									if (selectStatusCMApproveRequestEntry === DefaultStatusRejectedCMApproveRequestView)
									{
										mEnabledCMApproveRequest(true);
									}
									else
									{
										mEnabledCMApproveRequest(false);
									};
								};	
							}
							else
							{
								vTabPanelRepairCMApproveRequest.setActiveTab('tabServiceCMApproveRequest');
								varTabRepairCMApproveRequest=1;
								alert(nmAlertTabService);
							};
						}
					}
				}
			]
		}
	)
		return vTabPanelRepairCMApproveRequest;
};

function getItemPanelCardLayoutGridPersonCMApproveRequest()
{
	 var FormPanelCardLayoutGridPersonCMApproveRequest = new Ext.Panel
	(
		{
		    id: 'FormPanelCardLayoutGridPersonCMApproveRequest',
		    trackResetOnLoad: true,
		    width: 680,//666,
			height: 120,
			layout: 'card',
			border:false,
			activeItem: 0,
			items: 
			[
				{
					xtype: 'panel',
					layout: 'form',
					width: 680,//666,
					height: 120,
					border:false,
					id: 'CardGridPersonCMApproveRequest1',
					items:
					[	 
						GetDTLCraftPersonIntCMApproveRequestGrid()
					]
				},
				{
					xtype: 'panel',
					layout: 'form',
					width: 680,
					height: 120,
					border:false,
					id: 'CardGridPersonCMApproveRequest2',
					items:
					[
						GetDTLCraftPersonExtCMApproveRequestGrid()
					]
				}
			]
		}
	);
	
	return FormPanelCardLayoutGridPersonCMApproveRequest;
};

function GetDTLCraftPersonIntCMApproveRequestGrid() 
{
    var fldDetail = ['SCH_CM_ID','SERVICE_ID','ROW_SCH','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_SCH_PERSON','VENDOR','EMP_NAME','ROW'];

    dsDtlCraftPersonIntCMApproveRequest = new WebApp.DataStore({ fields: fldDetail })
	dsDtlCraftPersonIntCMApproveRequestTemp = new WebApp.DataStore({ fields: fldDetail })

    var gridCraftPersonIntCMApproveRequest  = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlCraftPersonIntCMApproveRequest,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:679,
		    height:120,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectPersonIntCMApproveRequest = dsDtlCraftPersonIntCMApproveRequest.getAt(row);
					        CurrentPersonIntCMApproveRequest.row = row;
					        CurrentPersonIntCMApproveRequest.data = cellSelectPersonIntCMApproveRequest;
					    }
					}
				}
			),
				cm: CraftPersonIntCMApproveRequestDetailColumModel(),
				 tbar: 
			         [
				        {
					        id:'btnTambahBrsCraftPersonIntCMApproveRequest',
					        text: nmTambahBaris,
					        tooltip:nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						TambahBarisPersonCMApproveRequest(dsDtlCraftPersonIntCMApproveRequest,dsDtlCraftPersonIntCMApproveRequestTemp);
					        }
				        },'-',
				        {
					        id:'btnHpsBrsCraftPersonIntCMApproveRequest',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlCraftPersonIntCMApproveRequest.getCount() > 0 )
								{
									if (cellSelectPersonIntCMApproveRequest != undefined)
									{
										if(CurrentPersonIntCMApproveRequest != undefined)
										{
											HapusBarisPersonIntCMApproveRequest();
										};
									}
									else
									{
										ShowPesanWarningCMApproveRequest(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			            ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridCraftPersonIntCMApproveRequest;
};

function TambahBarisPersonCMApproveRequest(ds)
{
	if(IsExtRepairCMApproveRequest === true)
	{
		if (Ext.get('txtKdVendorApproveRequest').dom.value === '')
		{
			ShowPesanWarningCMApproveRequest(nmGetValidasiKosong(nmVendIDNameApproveCM), nmTambahBaris);
		}
		else
		{
			var p = GetRecordBaruPersonCMApproveRequest(ds);
			ds.insert(ds.getCount(), p);
			
			var j=dsDtlCraftPersonExtCMApproveRequest.getCount()-1;
			dsDtlCraftPersonExtCMApproveRequestTemp.insert(dsDtlCraftPersonExtCMApproveRequestTemp.getCount(),dsDtlCraftPersonExtCMApproveRequest.data.items[j]);
			GetPersonExtServiceCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
		};
	}
	else
	{
		var p = GetRecordBaruPersonCMApproveRequest(ds);
		ds.insert(ds.getCount(), p);
	};
};

function HapusBarisPersonIntCMApproveRequest()
{
	if (cellSelectPersonIntCMApproveRequest.data.EMP_ID != '' && cellSelectPersonIntCMApproveRequest.data.EMP_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' :'+ ' ' + (CurrentPersonIntCMApproveRequest.row + 1) + ' ' + nmOperatorDengan + ' ' + nmEmpIDApproveCM + ' : ' + ' ' + cellSelectPersonIntCMApproveRequest.data.EMP_ID + ' ' + nmOperatorAnd + ' ' + nmEmpNameApproveCM + ' : ' + cellSelectPersonIntCMApproveRequest.data.EMP_NAME,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlCraftPersonIntCMApproveRequest.data.items[CurrentPersonIntCMApproveRequest.row].data.ROW_SCH=== '' )
							{
								GetTotalRemoveCMApproveRequest(CurrentPersonIntCMApproveRequest,true);
								HapusRowTempCMApproveRequest(dsDtlCraftPersonIntCMApproveRequestTemp,CurrentPersonIntCMApproveRequest.data.data.ROW);
								dsDtlCraftPersonIntCMApproveRequest.removeAt(CurrentPersonIntCMApproveRequest.row);
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase(),
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPersonCMApproveRequest();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			if(CurrentPersonIntCMApproveRequest.data.data.COST != '' && CurrentPersonIntCMApproveRequest.data.data.COST != undefined)
			{
				GetTotalRemoveCMApproveRequest(CurrentPersonIntCMApproveRequest,true)
			};
			dsDtlCraftPersonIntCMApproveRequest.removeAt(CurrentPersonIntCMApproveRequest.row);
		};
};

function HapusBarisPersonExtCMApproveRequest()
{
	if (cellSelectPersonExtCMApproveRequest.data.VENDOR_ID != '' && cellSelectPersonExtCMApproveRequest.data.VENDOR != '' && cellSelectPersonExtCMApproveRequest.data.PERSON_NAME !='')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' :'+ ' ' + (CurrentPersonExtCMApproveRequest.row + 1) + ' ' + nmOperatorDengan + ' ' + nmPersonNameApproveCM + ' : '+ ' ' + cellSelectPersonExtCMApproveRequest.data.PERSON_NAME, 
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlCraftPersonExtCMApproveRequest.data.items[CurrentPersonExtCMApproveRequest.row].data.ROW_SCH === '' )
							{
								GetTotalRemoveCMApproveRequest(CurrentPersonExtCMApproveRequest,true)
								HapusRowTempCMApproveRequest(dsDtlCraftPersonExtCMApproveRequestTemp,CurrentPersonExtCMApproveRequest.data.data.ROW);
								dsDtlCraftPersonExtCMApproveRequest.removeAt(CurrentPersonExtCMApproveRequest.row);
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase(),
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPersonCMApproveRequest();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			if(CurrentPersonExtCMApproveRequest.data.data.COST != '' && CurrentPersonExtCMApproveRequest.data.data.COST != undefined)
			{
				GetTotalRemoveCMApproveRequest(CurrentPersonExtCMApproveRequest,true)
			};
			HapusRowTempCMApproveRequest(dsDtlCraftPersonExtCMApproveRequestTemp,CurrentPersonExtCMApproveRequest.data.data.ROW);
			dsDtlCraftPersonExtCMApproveRequest.removeAt(CurrentPersonExtCMApproveRequest.row);
		};
};

function GetTotalRemoveCMApproveRequest(current,mBolEmp)
{
	var a;
	var b=0;
	
	if(mBolEmp === true )
	{
		a='txtTotalPersonCostCMApproveRequest';
	}
	else
	{
		a='txtTotalPartCMApproveRequest';
	};
	
	var x=GetNilaiCurrencyCMApproveRequest(Ext.get(a).dom.value);
	var y=GetNilaiCurrencyCMApproveRequest(Ext.get('txtTotalServiceCMApproveRequest').dom.value);
	var z=GetNilaiCurrencyCMApproveRequest(Ext.get('txtCostRateApproveRequest').dom.value);
	
	if(mBolEmp === true)
	{
		b=current.data.data.COST;
	}
	else
	{
		b=current.data.data.UNIT_COST * current.data.data.QTY;
	};
	
	if (x > 0)
	{
		Ext.get(a).dom.value=formatCurrency(x-b);
	}
	else
	{
		Ext.get(a).dom.value=0;
	};
	
	if (y > 0)
	{
		Ext.get('txtTotalServiceCMApproveRequest').dom.value=formatCurrency(y-b);
	}
	else
	{
		Ext.get('txtTotalServiceCMApproveRequest').dom.value=0
	};
	
	if (z > 0)
	{
		Ext.get('txtCostRateApproveRequest').dom.value=formatCurrency(z-b);
	}
	else
	{
		Ext.get('txtCostRateApproveRequest').dom.value=0
	};
};

function DeleteDetailPersonCMApproveRequest()
{
	Ext.Ajax.request
	(
		{
			//url: "./Datapool.mvc/DeleteDataObj",
                        url: baseURL + "index.php/main/DeleteDataObj",
			params:  getParamDeleteDetailPersonCMApproveRequest(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoCMApproveRequest(nmPesanHapusSukses,nmHeaderHapusData);
					if (IsExtRepairCMApproveRequest === false)
					{
						GetTotalRemoveCMApproveRequest(CurrentPersonIntCMApproveRequest,true);
						//GetJumlahTotalPersonCMApproveRequest('where sch_cm_id  =~' + SchIdCMApproveRequest  + '~ and category_id = ~' + varCatIdCMApproveRequest  + '~' + ' and service_id= ~ ' + cellSelectServiceCMApproveRequest.data.SERVICE_ID + '~' + ' and EMP_ID <> ~~' + 'and EMP_ID is not null');
                                                GetJumlahTotalPersonCMApproveRequest('sch_cm_id = ~' + SchIdCMApproveRequest  + '~ and category_id = ~' + varCatIdCMApproveRequest  + '~' + ' and service_id = ~' + cellSelectServiceCMApproveRequest.data.SERVICE_ID + '~' + ' and emp_id <> ~~' + ' and emp_id is not null');
						HapusRowTempCMApproveRequest(dsDtlCraftPersonIntCMApproveRequestTemp,CurrentPersonIntCMApproveRequest.data.data.ROW);
						dsDtlCraftPersonIntCMApproveRequest.removeAt(CurrentPersonIntCMApproveRequest.row);
						cellSelectPersonIntCMApproveRequest=undefined;
						LoadDataEmployeeCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
						LoadDataEmployeeCMApproveRequest(SchIdCMApproveRequest,varCatIdCMApproveRequest,cellSelectServiceCMApproveRequest.data.SERVICE_ID);
					}
					else
					{
						GetTotalRemoveCMApproveRequest(CurrentPersonExtCMApproveRequest,true);
						//GetJumlahTotalPersonCMApproveRequest('where sch_cm_id  =~' + SchIdCMApproveRequest  + '~ and category_id = ~' + varCatIdCMApproveRequest  + '~' + ' and service_id= ~ ' + cellSelectServiceCMApproveRequest.data.SERVICE_ID + '~' + ' and VENDOR_ID <> ~~' + 'and VENDOR_ID is not null');
                                                GetJumlahTotalPersonCMApproveRequest('sch_cm_id = ~' + SchIdCMApproveRequest  + '~ and category_id = ~' + varCatIdCMApproveRequest  + '~' + ' and service_id= ~ ' + cellSelectServiceCMApproveRequest.data.SERVICE_ID + '~' + ' and vendor_id <> ~~' + ' and vendor_id is not null');
						HapusRowTempCMApproveRequest(dsDtlCraftPersonExtCMApproveRequestTemp,CurrentPersonExtCMApproveRequest.data.data.ROW);
						dsDtlCraftPersonExtCMApproveRequest.removeAt(CurrentPersonExtCMApproveRequest.row);
						cellSelectPersonExtCMApproveRequest=undefined;
						LoadDataVendorCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
						LoadDataVendorCMApproveRequest(SchIdCMApproveRequest,varCatIdCMApproveRequest,cellSelectServiceCMApproveRequest.data.SERVICE_ID);
					};
					
					AddNewCMApproveRequest = false;
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningCMApproveRequest(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else if (cst.success === false && cst.pesan===1)
				{
					ShowPesanWarningCMApproveRequest(nmPesanHapusGagal + ' , ' + nmKonfirmasiWO ,nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorCMApproveRequest(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailPersonCMApproveRequest()
{
	var x,y;
	var z=0;
	
	if (IsExtRepairCMApproveRequest === false)
	{
		x=CurrentPersonIntCMApproveRequest.data.data.SCH_CM_ID;
		y=CurrentPersonIntCMApproveRequest.data.data.ROW_SCH;
		z=CurrentPersonIntCMApproveRequest.data.data.COST;
	}
	else
	{
		x=CurrentPersonExtCMApproveRequest.data.data.SCH_CM_ID;
		y=CurrentPersonExtCMApproveRequest.data.data.ROW_SCH;
		z=CurrentPersonExtCMApproveRequest.data.data.COST;
	};
	
	var params =
	{	
		Table: 'ViewApproveCM',   
		AppId: AppIdCMApproveRequest,
		SchId:SchIdCMApproveRequest,
		CatId:varCatIdCMApproveRequest,
		ServiceId:cellSelectServiceCMApproveRequest.data.SERVICE_ID,
		RowSch: y,
		LastCost:z,
		Hapus:3
	};
	
    return params;
};

function CraftPersonIntCMApproveRequestDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdEmployeeCMApproveRequest',
			    header: nmEmpIDApproveCM,
			    dataIndex: 'EMP_ID',
			    width: 150,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdEmployeeCMApproveRequest',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
						            if (Ext.get('fieldcolKdEmployeeCMApproveRequest').dom.value != '')
									{
										//str = ' where EMP_ID like ~%' + Ext.get('fieldcolKdEmployeeCMApproveRequest').dom.value + '%~';
                                                                                str = 'emp_id like ~%' + Ext.get('fieldcolKdEmployeeCMApproveRequest').dom.value + '%~';
									};
									GetLookupPersonIntCMApproveRequest(str)    
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMApproveRequest='colEmp';
							}
						}
					}
				)
			},
			{
			    id: 'colEmployeeNameCMApproveRequest',
			    header: nmEmpNameApproveCM,
			    dataIndex: 'EMP_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolEmployeeNameCMApproveRequest',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
						            if (Ext.get('fieldcolEmployeeNameCMApproveRequest').dom.value != '')
									{
										//str = ' where EMP_NAME like ~%' + Ext.get('fieldcolEmployeeNameCMApproveRequest').dom.value + '%~';
                                                                                str = 'emp_name like ~%' + Ext.get('fieldcolEmployeeNameCMApproveRequest').dom.value + '%~';
									};
									GetLookupPersonIntCMApproveRequest(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMApproveRequest='colEmp';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colDescCraftPersonCMApproveRequest',
			    header: nmDescApproveCM,
			    width: 200,
			    dataIndex: 'DESC_SCH_PERSON',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescCraftPersonCMApproveRequest',
					    allowBlank: true,
					    width: 30
					}
				),
			    renderer: function(value, cell) 
				{
			       var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colCostCraftPersonCMApproveRequest',
			    header: nmCostApproveCM,
			    width: 150,
				align:'right',
			    dataIndex: 'COST',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostCraftPersonCMApproveRequest',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPersonCMApproveRequest(CurrentPersonIntCMApproveRequest.row,true);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.COST);
				}
			}
		]
	)

};

function GetLookupPersonIntCMApproveRequest(str)
{
	if (cellSelectPersonIntCMApproveRequest.data.ROW_SCH === '' || cellSelectPersonIntCMApproveRequest.data.ROW_SCH === undefined)
	{
		var p = new mRecordPersonCMApproveRequest
		(
			{
				'SCH_CM_ID':SchIdCMApproveRequest,
				'SERVICE_ID':cellSelectServiceCMApproveRequest.data.SERVICE_ID,
				'ROW_SCH':'',
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonIntCMApproveRequest.data.items[CurrentPersonIntCMApproveRequest.row].data.COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonIntCMApproveRequest.data.items[CurrentPersonIntCMApproveRequest.row].data.DESC_SCH_PERSON,
				'ROW': dsDtlCraftPersonIntCMApproveRequest.data.items[CurrentPersonIntCMApproveRequest.row].data.ROW
			}
		);
		FormLookupEmployee('','',str,true,p,dsDtlCraftPersonIntCMApproveRequest,false);
	}
	else
	{	
		var p = new mRecordPersonCMApproveRequest
		(
			{
				'SCH_CM_ID':SchIdCMApproveRequest,
				'SERVICE_ID':cellSelectServiceCMApproveRequest.data.SERVICE_ID,
				'ROW_SCH':dsDtlCraftPersonIntCMApproveRequest.data.items[CurrentPersonIntCMApproveRequest.row].data.ROW_SCH,
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonIntCMApproveRequest.data.items[CurrentPersonIntCMApproveRequest.row].data.COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonIntCMApproveRequest.data.items[CurrentPersonIntCMApproveRequest.row].data.DESC_SCH_PERSON,
				'ROW': dsDtlCraftPersonIntCMApproveRequest.data.items[CurrentPersonIntCMApproveRequest.row].data.ROW
			}
		);
		FormLookupEmployee('','',str,true,p,dsDtlCraftPersonIntCMApproveRequest,false);
	};
};

function GetDTLCraftPersonExtCMApproveRequestGrid() 
{
    var fldDetail = ['SCH_CM_ID','SERVICE_ID','ROW_SCH','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_SCH_PERSON','VENDOR','EMP_NAME','ROW'];

    dsDtlCraftPersonExtCMApproveRequest = new WebApp.DataStore({ fields: fldDetail })
	dsDtlCraftPersonExtCMApproveRequestTemp = new WebApp.DataStore({ fields: fldDetail })
	
    var gridCraftPersonExtCMApproveRequest  = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlCraftPersonExtCMApproveRequest,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:679,
		    height:92,//106,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectPersonExtCMApproveRequest = dsDtlCraftPersonExtCMApproveRequest.getAt(row);
					        CurrentPersonExtCMApproveRequest.row = row;
					        CurrentPersonExtCMApproveRequest.data = cellSelectPersonExtCMApproveRequest;
					    }
					}
				}
			),
				cm: CraftPersonExtCMApproveRequestDetailColumModel(),
				 tbar: 
			         [
				        {
					        id:'btnTambahBrsCraftPersonExtCMApproveRequest',
					        text: nmTambahBaris,
					        tooltip: nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						TambahBarisPersonCMApproveRequest(dsDtlCraftPersonExtCMApproveRequest);
					        }
				        },'-',
				        {
					        id:'btnHpsBrsCraftPersonExtCMApproveRequest',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlCraftPersonExtCMApproveRequest.getCount() > 0 )
								{
									if (cellSelectPersonExtCMApproveRequest != undefined)
									{
										if(CurrentPersonExtCMApproveRequest != undefined)
										{
											HapusBarisPersonExtCMApproveRequest();
										};
									}
									else
									{
										ShowPesanWarningCMApproveRequest(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			            ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridCraftPersonExtCMApproveRequest;
};

function CraftPersonExtCMApproveRequestDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdVendorCMApproveRequest',
			    header: nmVendIDApproveCM,
			    dataIndex: 'VENDOR_ID',
			    width: 80,
				hidden:true,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdVendorCMApproveRequest',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									if (Ext.get('fieldcolKdVendorCMApproveRequest').dom.value != '')
									{
										//str = ' where VENDOR_ID like ~%' + Ext.get('fieldcolKdVendorCMApproveRequest').dom.value + '%~';
                                                                                str = 'vendor_id like ~%' + Ext.get('fieldcolKdVendorCMApproveRequest').dom.value + '%~';
									};
						            GetLookupPersonExtCMApproveRequest(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMApproveRequest='colVendor';
							}
						}
					}
				)
			},
			{
			    id: 'colVendorNameCMApproveRequest',
			    header: nmVendNameApproveCM,
			    dataIndex: 'VENDOR',
				hidden:true,
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolVendorNameCMApproveRequest',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						           var str = '';
									if (Ext.get('fieldcolVendorNameCMApproveRequest').dom.value != '')
									{
										//str = ' where VENDOR like ~%' + Ext.get('fieldcolVendorNameCMApproveRequest').dom.value + '%~';
                                                                                str = 'vendor like ~%' + Ext.get('fieldcolVendorNameCMApproveRequest').dom.value + '%~';
									};
						            GetLookupPersonExtCMApproveRequest(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMApproveRequest='colVendor';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell) 
				{
			        var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colContactPersonCraftPersonCMApproveRequest',
			    header: nmPersonNameApproveCM,
			    width: 250,
			    dataIndex: 'PERSON_NAME',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolContactPersonCraftPersonCMApproveRequest',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90
					}
				)
			},
			{
			    id: 'colDescCraftPersonExtCMApproveRequest',
			    header: nmDescApproveCM,
			    width: 250,
			    dataIndex: 'DESC_SCH_PERSON',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescCraftPersonExtCMApproveRequest',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(value, cell) 
				{
			       var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colCostCraftPersonExtCMApproveRequest',
			    header: nmCostApproveCM,
			    width: 90,
			    dataIndex: 'COST',
				align:'right',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostCraftPersonExtCMApproveRequest',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPersonCMApproveRequest(CurrentPersonExtCMApproveRequest.row,true);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.COST);
				}
			}
		]
	)

};

function GetLookupPersonExtCMApproveRequest(str)
{
	if (cellSelectPersonExtCMApproveRequest.data.ROW_SCH === '' || cellSelectPersonExtCMApproveRequest.data.ROW_SCH === undefined)
	{
		var p = new mRecordPersonCMApproveRequest
		(
			{
				'SCH_CM_ID':SchIdCMApproveRequest,
				'SERVICE_ID':cellSelectServiceCMApproveRequest.data.SERVICE_ID,
				'ROW_SCH':'',
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonExtCMApproveRequest.data.items[CurrentPersonExtCMApproveRequest.row].data.COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonExtCMApproveRequest.data.items[CurrentPersonExtCMApproveRequest.row].data.DESC_SCH_PERSON,
				'ROW':dsDtlCraftPersonExtCMApproveRequest.data.items[CurrentPersonExtCMApproveRequest.row].data.ROW
			}
		);
		FormLookupVendor(p,str,'',true,dsDtlCraftPersonExtCMApproveRequest,false);
	}
	else
	{	
		var p = new mRecordPersonCMApproveRequest
		(
			{
				'SCH_CM_ID':SchIdCMApproveRequest,
				'SERVICE_ID':cellSelectServiceCMApproveRequest.data.SERVICE_ID,
				'ROW_SCH':dsDtlCraftPersonExtCMApproveRequest.data.items[CurrentPersonExtCMApproveRequest.row].data.ROW_SCH,
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonExtCMApproveRequest.data.items[CurrentPersonExtCMApproveRequest.row].data.COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonExtCMApproveRequest.data.items[CurrentPersonExtCMApproveRequest.row].data.DESC_SCH_PERSON,
				'ROW':dsDtlCraftPersonExtCMApproveRequest.data.items[CurrentPersonExtCMApproveRequest.row].data.ROW
			}
		);
		FormLookupVendor(p,str,'',true,dsDtlCraftPersonExtCMApproveRequest,false);
	};
};

function CalcTotalPersonCMApproveRequest(idx,mBolGrid)
{
	var ds;
	var col;
	if(IsExtRepairCMApproveRequest === false)
	{
		ds=dsDtlCraftPersonIntCMApproveRequest;
		col=Ext.get('fieldcolCostCraftPersonCMApproveRequest')
	}
	else
	{
		ds=dsDtlCraftPersonExtCMApproveRequest;
		col=Ext.get('fieldcolCostCraftPersonExtCMApproveRequest')
	};
	
    var total=Ext.num(0);
	for(var i=0;i < ds.getCount();i++)
	{
		if (i === idx)
		{
			if (col != null) 
			{
				if (Ext.num(col.dom.value) != null) 
				{
					total += (Ext.num(col.dom.value));
				};
			};
		}
		else
		{
			total += Ext.num(ds.data.items[i].data.COST);
		}
	}
	
	Ext.get('txtTotalPersonCostCMApproveRequest').dom.value = formatCurrency(total);	
	if (mBolGrid === true)
	{
		Ext.get('txtTotalServiceCMApproveRequest').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalCMApproveRequestAllService(true,false)));
		Ext.get('txtCostRateApproveRequest').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalCMApproveRequestAllService(true,false)));
	};
};

function CalcTotalCMApproveRequestAllService(mBol,mBolPart)
{
	var ds;
	
	if (mBol != true)
	{
		if(IsExtRepairCMApproveRequest === false)
		{
			ds=dsDtlCraftPersonIntCMApproveRequestTemp;
		}
		else
		{
			ds=dsDtlCraftPersonExtCMApproveRequestTemp;
		};
	}
	else
	{
		ds=dsDtlPartCMApproveRequestTemp;
	};
	
    var total=Ext.num(0);
	for(var i=0;i < ds.getCount();i++)
	{
		if (mBol != true)
		{
			total += Ext.num(ds.data.items[i].data.COST);
		}
		else
		{	
			//total += Ext.num(ds.data.items[i].data.TOT_COST);
			total += Ext.num(ds.data.items[i].data.UNIT_COST * ds.data.items[i].data.QTY);
		};
	}
	
	if (mBolPart === true)
	{
		ds=dsDtlPartCMApproveRequestTemp;
	}
	else
	{
		if(IsExtRepairCMApproveRequest === false)
		{
			ds=dsDtlCraftPersonIntCMApproveRequestTemp;
		}
		else
		{
			ds=dsDtlCraftPersonExtCMApproveRequestTemp;
		};
	};
	
	for(var i=0;i < ds.getCount();i++)
	{
		if (mBolPart === true)
		{
			if ( ds.data.items[i].data.SERVICE_ID != cellSelectServiceCMApproveRequest.data.SERVICE_ID)
			{
				//total += Ext.num(ds.data.items[i].data.TOT_COST);
				total += Ext.num(ds.data.items[i].data.UNIT_COST * ds.data.items[i].data.QTY);
			};
		}
		else
		{	
			if ( ds.data.items[i].data.SERVICE_ID != cellSelectServiceCMApproveRequest.data.SERVICE_ID)
			{
				total += Ext.num(ds.data.items[i].data.COST);
			};
		};
	};
	
	return total;
};

function GetDTLServiceCMApproveRequestGrid() 
{
    var fldDetail = ['SCH_CM_ID','SERVICE_ID','SERVICE_NAME','TAG','CATEGORY_ID'];

    dsDtlServiceCMApproveRequest = new WebApp.DataStore({ fields: fldDetail })

    var gridServiceCMApproveRequest  = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlServiceCMApproveRequest,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:679,//666,
		    height:120,//134,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectServiceCMApproveRequest = dsDtlServiceCMApproveRequest.getAt(row);
					        CurrentServiceCMApproveRequest.row = row;
					        CurrentServiceCMApproveRequest.data = cellSelectServiceCMApproveRequest;
					    }
					}
				}
			),
				cm: ServiceCMApproveRequestDetailColumModel(),
				 tbar: 
			         [
				        {
					        id:'btnTambahBrsServiceCMApproveRequest',
							name:'btnTambahBrsServiceCMApproveRequest',
					        text: nmTambahBaris,
					        tooltip: nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						TambahBarisServiceCMApproveRequest(dsDtlServiceCMApproveRequest);
					        }
				        },'-',
				        {
					        id:'btnHpsBrsServiceCMApproveRequest',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlServiceCMApproveRequest.getCount() > 0 )
								{
									if (cellSelectServiceCMApproveRequest != undefined)
									{
										if(CurrentServiceCMApproveRequest != undefined)
										{
											HapusBarisServiceCMApproveRequest();
										};
									}
									else
									{
										ShowPesanWarningCMApproveRequest(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			            ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridServiceCMApproveRequest;
};

function HapusBarisServiceCMApproveRequest()
{
	if (cellSelectServiceCMApproveRequest.data.SERVICE_ID != '' && cellSelectServiceCMApproveRequest.data.SERVICE_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' :'+ ' ' + (CurrentServiceCMApproveRequest.row + 1) + ' ' + nmOperatorDengan + ' ' + nmServIDApproveCM + ' : '+ ' ' + cellSelectServiceCMApproveRequest.data.SERVICE_ID + ' ' + nmOperatorAnd + ' ' + nmServNameApproveCM  + ' : ' + cellSelectServiceCMApproveRequest.data.SERVICE_NAME ,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlServiceCMApproveRequest.data.items[CurrentServiceCMApproveRequest.row].data.TAG === 1 )
							{
								deletePersonPartServiceRowDeleteCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
								dsDtlServiceCMApproveRequest.removeAt(CurrentServiceCMApproveRequest.row);
								cellSelectServiceCMApproveRequest=undefined;
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailServiceCMApproveRequest();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			dsDtlServiceCMApproveRequest.removeAt(CurrentServiceCMApproveRequest.row);
		};
};

function deletePersonPartServiceRowDeleteCMApproveRequest(service_id)
{
	var x=0;
	var y=0;
	if (service_id != '' && service_id != undefined)
	{
		if( IsExtRepairCMApproveRequest === true)
		{
			for (var i = 0; i < dsDtlCraftPersonExtCMApproveRequestTemp.getCount() ; i++) 
			{
				if(dsDtlCraftPersonExtCMApproveRequestTemp.data.items[i].data.SERVICE_ID === service_id)
				{
					x += dsDtlCraftPersonExtCMApproveRequestTemp.data.items[i].data.COST ;
					dsDtlCraftPersonExtCMApproveRequestTemp.removeAt(i);
				};
			};
		}
		else
		{
			for (var i = 0; i < dsDtlCraftPersonIntCMApproveRequestTemp.getCount() ; i++) 
			{
				if(dsDtlCraftPersonIntCMApproveRequestTemp.data.items[i].data.SERVICE_ID === service_id)
				{
					x += dsDtlCraftPersonIntCMApproveRequestTemp.data.items[i].data.COST ;
					dsDtlCraftPersonIntCMApproveRequestTemp.removeAt(i);
				};
			};
		};
		
		for (var i = 0; i < dsDtlPartCMApproveRequestTemp.getCount() ; i++) 
		{
			if(dsDtlPartCMApproveRequestTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				y += (dsDtlPartCMApproveRequestTemp.data.items[i].data.UNIT_COST * dsDtlPartCMApproveRequestTemp.data.items[i].data.QTY);
				dsDtlPartCMApproveRequestTemp.removeAt(i);
			};
		};
		
		var a=GetNilaiCurrencyCMApproveRequest(Ext.get('txtTotalServiceCMApproveRequest').dom.value);
		var b=GetNilaiCurrencyCMApproveRequest(Ext.get('txtCostRateApproveRequest').dom.value);
		
		if (a > 0)
		{
			Ext.get('txtTotalServiceCMApproveRequest').dom.value=formatCurrency(a-(x+y));
		}
		else
		{
			Ext.get('txtTotalServiceCMApproveRequest').dom.value=0
		};
		
		if (b > 0)
		{
			Ext.get('txtCostRateApproveRequest').dom.value=formatCurrency(b-(x+y));
		}
		else
		{
			Ext.get('txtCostRateApproveRequest').dom.value=0
		};
	};
};

function GetLookupServiceCMApproveRequest(str,idx,ds)
{
	if (AppIdCMApproveRequest === '' || AppIdCMApproveRequest === undefined)
	{
		var p = new mRecordServiceCMApproveRequest
		(
			{
				'SCH_CM_ID':'',
				'SERVICE_ID':'',
				'SERVICE_NAME':'',
				'CATEGORY_ID':varCatIdCMApproveRequest,
				'TAG':1
			}
		);
		FormLookupServiceCategory('','',str,true,p,ds,false);
	}
	else
	{	
		var p = new mRecordServiceCMApproveRequest
		(
			{
				'SCH_CM_ID':SchIdCMApproveRequest,
				'SERVICE_ID':'',
				'SERVICE_NAME':'',
				'CATEGORY_ID':varCatIdCMApproveRequest,
				'TAG':1
			}
		);
		FormLookupServiceCategory('','',str,true,p,ds,false);
	};
};

function TambahBarisServiceCMApproveRequest(ds)
{
	var p = GetRecordBaruServiceCMApproveRequest();
	ds.insert(ds.getCount(), p);
};

function DeleteDetailServiceCMApproveRequest()
{
	Ext.Ajax.request
	(
		{
			//url: "./Datapool.mvc/DeleteDataObj",
                        url: baseURL + "index.php/main/DeleteDataObj",
			params:  getParamDeleteDetailServiceCMApproveRequest(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoCMApproveRequest(nmPesanHapusSukses,nmHeaderHapusData);
					deletePersonPartServiceRowDeleteCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
					GetJumlahTotalPersonPartCMApproveRequest(true);
					dsDtlServiceCMApproveRequest.removeAt(CurrentServiceCMApproveRequest.row);
					cellSelectServiceCMApproveRequest=undefined;
					LoadDataServiceCMApproveRequest(SchIdCMApproveRequest);
					
					AddNewCMApproveRequest = false;
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningCMApproveRequest(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else if (cst.success === false && cst.pesan===1)
				{
					ShowPesanWarningCMApproveRequest(nmPesanHapusGagal + ' , ' + nmKonfirmasiWO,nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorCMApproveRequest(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailServiceCMApproveRequest()
{
	var params =
	{	
		Table: 'ViewApproveCM',   
	    AppId: AppIdCMApproveRequest,
		SchId:SchIdCMApproveRequest,
		CatId:varCatIdCMApproveRequest,
		ServiceId: cellSelectServiceCMApproveRequest.data.SERVICE_ID,
		Hapus:1,
		Service:1
	};
	
    return params;
};


function ServiceCMApproveRequestDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdServiceCMApproveRequest',
			    header: nmServIDApproveCM,
			    dataIndex: 'SERVICE_ID',
			    width: 150,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdServiceCMApproveRequest',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListServiceCMApproveRequest();
									if (strList !='')
									{
									    //strList = ' and SERVICE_ID not in ( ' + strList + ') '
                                                                            strList = ' and service_id not in ( ' + strList + ') '
									};
									
						            if (Ext.get('fieldcolKdServiceCMApproveRequest').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdCMApproveRequest + '~'
                                                                                str = 'category_id = ~' + varCatIdCMApproveRequest + '~'
										str += strList;
										//str += ' and SERVICE_ID like ~%' + Ext.get('fieldcolKdServiceCMApproveRequest').dom.value + '%~';
                                                                                str += ' and service_id like ~%' + Ext.get('fieldcolKdServiceCMApproveRequest').dom.value + '%~';
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdCMApproveRequest + '~';
                                                                                str = 'category_id = ~' + varCatIdCMApproveRequest + '~';
										str += strList;
									};
									GetLookupServiceCMApproveRequest(str,CurrentServiceCMApproveRequest.row,dsDtlServiceCMApproveRequest);  
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMApproveRequest='colService';
							}
						}
					}
				)
			},
			{
			    id: 'colServiceNameCMApproveRequest',
			    header: nmServNameApproveCM,
			    dataIndex: 'SERVICE_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolServiceNameCMApproveRequest',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListServiceCMApproveRequest();
									if (strList !='')
									{
									    //strList = ' and SERVICE_ID not in ( ' + strList + ') '
                                                                            strList = ' and service_id not in ( ' + strList + ') '
									};
									
						            if (Ext.get('fieldcolServiceNameCMApproveRequest').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdCMApproveRequest + '~'
                                                                                str = 'category_id = ~' + varCatIdCMApproveRequest + '~'
										str += strList;
										//str += ' and  SERVICE_NAME like ~%' + Ext.get('fieldcolServiceNameCMApproveRequest').dom.value + '%~';
                                                                                str += ' and service_name like ~%' + Ext.get('fieldcolServiceNameCMApproveRequest').dom.value + '%~';
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdCMApproveRequest + '~'
                                                                                str = 'category_id = ~' + varCatIdCMApproveRequest + '~'
										str += strList;
									};
									GetLookupServiceCMApproveRequest(str,CurrentServiceCMApproveRequest.row,dsDtlServiceCMApproveRequest);  
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMApproveRequest='colService';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			}
		]
	)
};

function GetDTLPartCMApproveRequestGrid() 
{
    var fldDetail = ['SCH_CM_ID','SERVICE_ID','PART_ID','QTY','UNIT_COST','TOT_COST','PART_NAME','PRICE','DESC_SCH_PART','TAG','ROW'];

    dsDtlPartCMApproveRequest = new WebApp.DataStore({ fields: fldDetail })
	dsDtlPartCMApproveRequestTemp = new WebApp.DataStore({ fields: fldDetail })

    var gridPartCMApproveRequest = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlPartCMApproveRequest,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:679,
		    height: 120,//150,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
						cellSelectPartCMApproveRequest = dsDtlPartCMApproveRequest.getAt(row);
					        CurrentPartCMApproveRequest.row = row;
					        CurrentPartCMApproveRequest.data = cellSelectPartCMApproveRequest;
					    }
					}
				}
			),
			cm: PartCMApproveRequestDetailColumModel(),
		    tbar:
			         [
				        {
				            id: 'btnTambahBrsPartCMApproveRequest',
				            text: nmTambahBaris,
				            tooltip: nmTambahBaris,
				            iconCls: 'AddRow',
				            handler: function() 
							{
								TambahBarisPartCMApproveRequest(dsDtlPartCMApproveRequest);
				            }
				        }, '-',
				        {
				            id: 'btnHpsBrsPartCMApproveRequest',
				            text: nmHapusBaris,
				            tooltip: nmHapusBaris,
				            iconCls: 'RemoveRow',
				            handler: function() 
							{
								if (dsDtlPartCMApproveRequest.getCount() > 0 )
								{
									if (cellSelectPartCMApproveRequest != undefined)
									{
										if(CurrentPartCMApproveRequest != undefined)
										{
											HapusBarisPartCMApproveRequest();
										};
									}
									else
									{
										ShowPesanWarningCMApproveRequest(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
				            }
				        }, ' ', '-'
			            ]
		    , viewConfig: { forceFit: true }
		}
	);

		return gridPartCMApproveRequest;
};

function HapusBarisPartCMApproveRequest()
{
	if (cellSelectPartCMApproveRequest.data.PART_ID != '' && cellSelectPartCMApproveRequest.data.PART_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' :'+ ' ' + (CurrentPartCMApproveRequest.row + 1) + ' ' + nmOperatorDengan + ' ' + nmPartNumApproveCM  + ' : ' + cellSelectPartCMApproveRequest.data.PART_ID + ' ' + nmOperatorAnd + ' ' + nmPartNameApproveCM  + ' : ' + cellSelectPartCMApproveRequest.data.PART_NAME ,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlPartCMApproveRequest.data.items[CurrentPartCMApproveRequest.row].data.TAG === 1 )
							{
								GetTotalRemoveCMApproveRequest(CurrentPartCMApproveRequest,false);
								HapusRowTempCMApproveRequest(dsDtlPartCMApproveRequestTemp,CurrentPartCMApproveRequest.data.data.ROW);
								dsDtlPartCMApproveRequest.removeAt(CurrentPartCMApproveRequest.row);
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPartCMApproveRequest();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			if(CurrentPartCMApproveRequest.data.data.UNIT_COST != '' && CurrentPartCMApproveRequest.data.data.UNIT_COST != undefined)
			{
				GetTotalRemoveCMApproveRequest(CurrentPartCMApproveRequest,false);
			};
			dsDtlPartCMApproveRequest.removeAt(CurrentPartCMApproveRequest.row);
		};
};

function DeleteDetailPartCMApproveRequest()
{
	Ext.Ajax.request
	(
		{
			//url: "./Datapool.mvc/DeleteDataObj",
                        url: baseURL + "index.php/main/DeleteDataObj",
			params:  getParamDeleteDetailPartCMApproveRequest(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					GetTotalRemoveCMApproveRequest(CurrentPartCMApproveRequest,false);
					GetJumlahTotalPartCMApproveRequest();
					HapusRowTempCMApproveRequest(dsDtlPartCMApproveRequestTemp,CurrentPartCMApproveRequest.data.data.ROW);
					dsDtlPartCMApproveRequest.removeAt(CurrentPartCMApproveRequest.row);
					cellSelectPartCMApproveRequest=undefined;
					LoadDataPartCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
					LoadDataPartCMApproveRequest(SchIdCMApproveRequest,varCatIdCMApproveRequest,cellSelectServiceCMApproveRequest.data.SERVICE_ID);
					AddNewCMApproveRequest = false;
					ShowPesanInfoCMApproveRequest(nmPesanHapusSukses,nmHeaderHapusData);
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningCMApproveRequest(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else if (cst.success === false && cst.pesan===1)
				{
					ShowPesanWarningCMApproveRequest(nmPesanHapusGagal + ' , This request had been work order',nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorCMApproveRequest(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailPartCMApproveRequest()
{
	
	var params =
	{	
		Table: 'ViewApproveCM',
		AppId: AppIdCMApproveRequest,		
                SchId:SchIdCMApproveRequest,
		CatId:varCatIdCMApproveRequest,
		ServiceId:cellSelectServiceCMApproveRequest.data.SERVICE_ID,
		PartId: CurrentPartCMApproveRequest.data.data.PART_ID,
		LastCost:CurrentPartCMApproveRequest.data.data.UNIT_COST * CurrentPartCMApproveRequest.data.data.QTY,
		Hapus:4
	};
	
    return params;
};

function TambahBarisPartCMApproveRequest(ds)
{
	var p = GetRecordBaruPartCMApproveRequest();
	ds.insert(ds.getCount(), p);
};

function PartCMApproveRequestDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdPartCMApproveRequest',
			    header: nmPartNumApproveCM,
			    dataIndex: 'PART_ID',
			    width: 140,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdPartCMApproveRequest',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListPartCMApproveRequest(false);
									
									if(Ext.get('fieldcolKdPartCMApproveRequest').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdCMApproveRequest + '~'
                                                                                str = 'category_id = ~' + varCatIdCMApproveRequest + '~'
										//str +=' and PART_ID like ~%' + Ext.get('fieldcolKdPartCMApproveRequest').dom.value + '%~';
                                                                                str +=' and part_id like ~%' + Ext.get('fieldcolKdPartCMApproveRequest').dom.value + '%~';
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdCMApproveRequest + '~'
                                                                                str = 'category_id = ~' + varCatIdCMApproveRequest + '~'
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									};
									GetLookupPartCMApproveRequest(str,CurrentPartCMApproveRequest.row,dsDtlPartCMApproveRequest);
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMApproveRequest='colPart';
							}
						}
					}
				)
			},
			{
			    id: 'colPartCMApproveRequest',
			    header: nmPartNameApproveCM,
			    dataIndex: 'PART_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolPartCMApproveRequest',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListPartCMApproveRequest(false);
									
									if(Ext.get('fieldcolPartCMApproveRequest').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdCMApproveRequest + '~'
                                                                                str = 'category_id = ~' + varCatIdCMApproveRequest + '~'
										//str += ' and PART_NAME like ~%' + Ext.get('fieldcolPartCMApproveRequest').dom.value + '%~';
                                                                                str += ' and part_name like ~%' + Ext.get('fieldcolPartCMApproveRequest').dom.value + '%~';
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdCMApproveRequest + '~'
                                                                                str = 'category_id = ~' + varCatIdCMApproveRequest + '~'
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									};
									GetLookupPartCMApproveRequest(str,CurrentPartCMApproveRequest.row,dsDtlPartCMApproveRequest);
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMApproveRequest='colPart';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell) 
				{
			        var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
			        return str;
			    }
			},
			{
			    id: 'colQtyPartCMApproveRequest',
			    header: nmQtyApproveCM,
				align:'center',
			    width: 50,
			    dataIndex: 'QTY',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolQtyPartCMApproveRequest',
					    allowBlank: false,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPartCMApproveRequest(CurrentPartCMApproveRequest.row,false,true);
								}; 
							}
						}
					}
				)
			},
			{
			    id: 'colCostPartCMApproveRequest',
			    header: nmUnitCostApproveCM,
			    width: 150,
				align:'right',
			    dataIndex: 'UNIT_COST',
				editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostPartCMApproveRequest',
					    allowBlank: false,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPartCMApproveRequest(CurrentPartCMApproveRequest.row,true,true);
								}; 
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.UNIT_COST);
				}	
			},
			{
			    id: 'colTotCostPartInternalCMApproveRequest',
			    header: nmTotCostApproveCM,
			    width: 150,
				align:'right',
			    dataIndex: 'TOT_COST',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.UNIT_COST * record.data.QTY);
				}	
			},
			{
			    id: 'colDescPartCMApproveRequest',
			    header: nmDescApproveCM,
			    width: 200,
			    dataIndex: 'DESC_SCH_PART',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescPartCMApproveRequest',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(v, params, record) //(value, cell) 
				{
			        // var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
			        // return str;
					return record.data.DESC_SCH_PART;
			    }
			}
		]
	)
};

function GetLookupPartCMApproveRequest(str,idx,ds)
{
	if (SchIdCMApproveRequest === '' || SchIdCMApproveRequest === undefined)
	{
		var p = new mRecordPartCMApproveRequest
		(
			{
				'SCH_CM_ID':'',
				'SERVICE_ID':cellSelectServiceCMApproveRequest.data.SERVICE_ID,
				'PART_ID':'',
				'PART_NAME':'', 
				'QTY':ds.data.items[idx].data.QTY,
				'UNIT_COST':ds.data.items[idx].data.UNIT_COST,
				'TOT_COST':ds.data.items[idx].data.UNIT_COST * ds.data.items[idx].data.QTY, //ds.data.items[idx].data.TOT_COST,
				'DESC_SCH_PART':ds.data.items[idx].data.DESC_SCH_PART,
				'TAG':1,
				'ROW': ds.data.items[idx].data.ROW
			}
		);
		FormLookupPart(str,ds,p,true,'',false);
	}
	else
	{	
		var p = new mRecordPartCMApproveRequest
		(
			{
				'SCH_CM_ID':SchIdCMApproveRequest,
				'PART_ID':'',
				'SERVICE_ID':cellSelectServiceCMApproveRequest.data.SERVICE_ID,
				'PART_NAME':'', 
				'QTY':ds.data.items[idx].data.QTY,
				'UNIT_COST':ds.data.items[idx].data.UNIT_COST,
				'TOT_COST': ds.data.items[idx].data.UNIT_COST * ds.data.items[idx].data.QTY,//ds.data.items[idx].data.TOT_COST,
				'DESC_SCH_PART':ds.data.items[idx].data.DESC_SCH_PART,
				'TAG':1,
				'ROW': ds.data.items[idx].data.ROW
			}
		);
		FormLookupPart(str,ds,p,false,idx,false);
	};
};


function CalcTotalPartCMApproveRequest(idx,mCost,mBolGrid)
{
	var total=Ext.num(0);
	for(var i=0;i < dsDtlPartCMApproveRequest.getCount();i++)
	{
		if (i === idx)
		{
			if (mCost===true)
			{
				if (Ext.get('fieldcolCostPartCMApproveRequest') != null) 
				{
					if (Ext.num(Ext.get('fieldcolCostPartCMApproveRequest').dom.value) != null) 
					{
						total += (Ext.num(Ext.get('fieldcolCostPartCMApproveRequest').dom.value) * Ext.num(dsDtlPartCMApproveRequest.data.items[i].data.QTY));
					};
			    };
			}
		    else
			{
			    if (Ext.get('fieldcolQtyPartCMApproveRequest') != null)
				{
			        if (Ext.num(Ext.get('fieldcolQtyPartCMApproveRequest').dom.value) != null) 
					{
			            total += (Ext.num(Ext.get('fieldcolQtyPartCMApproveRequest').dom.value) * Ext.num(dsDtlPartCMApproveRequest.data.items[i].data.UNIT_COST));
			        };
			    };
			}
		}
		else
		{
			total += Ext.num(dsDtlPartCMApproveRequest.data.items[i].data.UNIT_COST)*Ext.num(dsDtlPartCMApproveRequest.data.items[i].data.QTY);
		}
	}
	
	Ext.get('txtTotalPartCMApproveRequest').dom.value = formatCurrency(total);	
	if (mBolGrid === true)
	{
		Ext.get('txtTotalServiceCMApproveRequest').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalCMApproveRequestAllService(false,true)));
		Ext.get('txtCostRateApproveRequest').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalCMApproveRequestAllService(false,true)));
		
	};
};

function getItemPanelDateTargetApproveRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 400,
	    items:
		[
			{
			    columnWidth: .7,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmTargetDateApproveCM + ' ',
					    id: 'dtpTglSelesaiApproveRequest',
					    name: 'dtpTglSelesaiApproveRequest',
					    readOnly: true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'button',
					    text: nmBtnHistory,
					    width: 80,
					    hideLabel: true,
					    id: 'btnOkHistoryApproveRequest',
					    handler: function() 
					    {
							var criteria;
							//criteria='WHERE ASSET_MAINT_ID=~' + Ext.get('txtKdMainAssetApproveRequest').getValue() + '~'
                                                        criteria='asset_maint_id = ~' + Ext.get('txtKdMainAssetApproveRequest').getValue() + '~'
							FormLookupHistoryAsset(criteria,Ext.get('txtNamaMainAsetApproveRequest').dom.value);
					    }
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelRadioVendorCMApproveRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		style:{'margin-top':'-2px'},
		width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					getRadioVendorApproveRequest()
				]
			},
			{
			    columnWidth: .2,
			    layout: 'form',
			    border: false,
				labelWidth:58,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmVendIDNameApproveCM  + ' ',
					    name: 'txtKdVendorApproveRequest',
					    id: 'txtKdVendorApproveRequest',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
					    name: 'txtNamaVendorApproveRequest',
					    id: 'txtNamaVendorApproveRequest',
					    anchor: '99%',
					    listeners:
						{
						    'specialkey': function() {
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var criteria='';
									if (Ext.get('txtNamaVendorApproveRequest').dom.value != '')
									{
										//criteria = ' where VENDOR like ~%' + Ext.get('txtNamaVendorApproveRequest').dom.value + '%~';
                                                                                criteria = 'vendor like ~%' + Ext.get('txtNamaVendorApproveRequest').dom.value + '%~';
									};
						            FormLookupVendor(GetItemLookupVendorCMApproveRequest(),criteria);
						        };
						    }
						},
						'focus' : function()
						{
							FocusCtrlCMApproveRequest='colVendor';
						}
					}
				]
			},
			{
			    columnWidth: .15,
			    layout: 'form',
				id:'layoutBtnVendorCMApproveRequest',
				name:'layoutBtnVendorCMApproveRequest',
			    border: false,
			    items:
				[
					{
						xtype:'button',
						text:nmBtnVendorInfo,
						width:100,
						hideLabel:true,
						id: 'btnLookupVendorCMApproveRequest',
						handler:function() 
						{
							if(IsExtRepairCMApproveRequest === true)
							{
								if (rowSelectedLookVendor != undefined)
								{
									InfoVendorLookUp(rowSelectedLookVendor);
								}
								else
								{
									if(dsLookVendorListCMApproveRequest != undefined)
									{
										if(dsLookVendorListCMApproveRequest.getCount() > 0 )
										{
											InfoVendorLookUp(dsLookVendorListCMApproveRequest.data.items[0])
										};
									};
								};
							};
						}
					}
				]
			}
		]
	}
    return items;
};

function GetItemLookupVendorCMApproveRequest()
{
	var p = new mRecVendorCMApproveRequest
	(
		{
			KD:'txtKdVendorApproveRequest',
			NAME: 'txtNamaVendorApproveRequest'
		}
	);
	return p;
};


function getRadioVendorApproveRequest()
{
	var items=
	{
		xtype: 'radiogroup',
		fieldLabel: nmRepairApproveCM + ' ',
		itemCls: 'x-check-group-alt',
		anchor:'99%',
		columns: 2,
		items: 
		[        
			{
				boxLabel: nmRdoIntApproveCM, 
				name: 'rdoVendorApproveReq', 
				inputValue: '0', 
				id:'rdoInternalApproveReq',
				style: 
				{
					'margin-top': '3.5px'
				},
				handler: function() 
				{
					if(Ext.get('rdoInternalApproveReq').dom.checked===true)
					{
						DisabledVendorCMApproveRequest(true);
						IsExtRepairCMApproveRequest=false;
						if (varTabRepairCMApproveRequest=== 2)
						{
							Ext.getCmp('FormPanelCardLayoutGridPersonCMApproveRequest').getLayout().setActiveItem(0);
							if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest.data != undefined)
							{
								if (cellSelectServiceCMApproveRequest.data.SERVICE_ID != undefined && cellSelectServiceCMApproveRequest.data.SERVICE_ID !='')
								{
									GetPersonIntServiceCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
									CalcTotalPersonCMApproveRequest('',false);
								};
							};
							FocusCtrlCMApproveRequest = 'colEmpRdo';
						};
					}
					else
					{
						DisabledVendorCMApproveRequest(false);
						IsExtRepairCMApproveRequest=true;
						if (varTabRepairCMApproveRequest=== 2)
						{
							Ext.getCmp('FormPanelCardLayoutGridPersonCMApproveRequest').getLayout().setActiveItem(1);
							if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest.data != undefined)
							{
								if (cellSelectServiceCMApproveRequest.data.SERVICE_ID != undefined && cellSelectServiceCMApproveRequest.data.SERVICE_ID !='')
								{
									GetPersonExtServiceCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
									CalcTotalPersonCMApproveRequest('',false);
								};
							};
							FocusCtrlCMApproveRequest = 'colVendorRdo';
						};
						
						if (SchIdCMApproveRequest != '' && varCatIdCMApproveRequest != '')
						{
							GetVendorCMApproveRequest()
						};
					};
				}
           },
			 {
				boxLabel: nmRdoExtApproveCM, 
				name: 'rdoVendorApproveReq', 
				inputValue: '1', 
				id:'rdoExternalApproveReq',
				style: 
				{
				    'margin-top': '3.5px'
				},
				handler: function() 
				{
					if(Ext.get('rdoExternalApproveReq').dom.checked===true)
					{
						DisabledVendorCMApproveRequest(false);
						IsExtRepairCMApproveRequest=true;
						if (varTabRepairCMApproveRequest=== 2)
						{
							Ext.getCmp('FormPanelCardLayoutGridPersonCMApproveRequest').getLayout().setActiveItem(1);
							if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest.data != undefined)
							{
								if (cellSelectServiceCMApproveRequest.data.SERVICE_ID != undefined && cellSelectServiceCMApproveRequest.data.SERVICE_ID !='')
								{
									GetPersonExtServiceCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
									CalcTotalPersonCMApproveRequest('',false);
								};
							};
							FocusCtrlCMApproveRequest = 'colVendorRdo';
						};
						if (SchIdCMApproveRequest != '' && varCatIdCMApproveRequest != '')
						{
							GetVendorCMApproveRequest();
						};
					}
					else
					{
						DisabledVendorCMApproveRequest(true);
						IsExtRepairCMApproveRequest=false;
						if (varTabRepairCMApproveRequest=== 2)
						{
							Ext.getCmp('FormPanelCardLayoutGridPersonCMApproveRequest').getLayout().setActiveItem(0);
							if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest.data != undefined)
							{
								if (cellSelectServiceCMApproveRequest.data.SERVICE_ID != undefined && cellSelectServiceCMApproveRequest.data.SERVICE_ID !='')
								{
									GetPersonIntServiceCMApproveRequest(cellSelectServiceCMApproveRequest.data.SERVICE_ID);
									CalcTotalPersonCMApproveRequest('',false);
								};
							};
							FocusCtrlCMApproveRequest = 'colEmpRdo';
						};
					};
				}
			}
		]
	};
	
	return items;
};


function getItemPanelNoApproveRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .4,
			    layout: 'form',
				labelWidth:90,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmNoReqApproveCM + ' ',
					    name: 'txtNoApproveRequest',
					    id: 'txtNoApproveRequest',
						readOnly:true,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmRequestDateApproveCM + ' ',
					    id: 'dtpTanggalApproveRequest',
					    name: 'dtpTanggalApproveRequest',
						readOnly:true,
					    anchor: '60%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoApproveRequest2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoApproveRequest3(lebar) 
				]
			}
		]
	}
    return items;
};

function getItemPanelDepartApproveRequest(lebar) 
{
    var items =
    {
        layout: 'column',
        border: false,
        width: lebar - 56,
        items:
        [
        {
            columnWidth: 1,
            layout: 'form',
            border: false,
            items:
                [
                        {
                                xtype: 'textfield',
                                fieldLabel: nmDeptApproveCM + ' ',
                                name: 'txtDepartmentApproveRequest',
                                id: 'txtDepartmentApproveRequest',
                                readOnly:true,
                                anchor: '99.60%'
                        }
                ]
        }
        ]
    }
    return items;
};

function getItemPanelNoAssetApproveRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoAssetApproveRequest2(lebar) 
				]
			},
			{
			    columnWidth: .4,
			    layout: 'form',
			    border: false,
				labelWidth:70,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmLocApproveCM + ' ',
					    name: 'txtLocationApproveRequest',
					    id: 'txtLocationApproveRequest',
						readOnly:true,
					    anchor: '99.99%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelProblemApproveRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .48,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textarea',
						fieldLabel: nmProblemApproveCM + ' ',
						name: 'txtProblemApproveRequest',
						id: 'txtProblemApproveRequest',
						scroll:true,
						readOnly:true,
						anchor: '99.99%',
						height:50
				    }
				]
			},
			{
			    columnWidth: .52,
			    layout: 'form',
				labelWidth:80,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmDeptApproveCM + ' ',
					    id: 'DeptAssetApproveRequest',
					    name: 'DeptAssetApproveRequest',
					    readOnly: true,
					    anchor: '99.99%'
					},getItemPanelDateTargetApproveRequest(lebar) 
				]
			}
		]
	}
    return items;
};

function getItemPanelApproverApproveRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelApproverApproveRequest2(lebar) 
				]
			},
			{
			    columnWidth: .4,
			    layout: 'form',
			    border: false,
				labelWidth:70,
			    items:
				[ mComboStatusCMApproveRequestEntry() ]
			}
		]
	}
    return items;
};

function getItemPanelTanggalAppApproveRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
		style:{'margin-top': '3px'},
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						 xtype: 'datefield',
					    fieldLabel: nmStartDateApproveCM + ' ',
					    id: 'dtpPengerjaanAppRequest',
					    name: 'dtpPengerjaanAppRequest',
					    format: 'd/M/Y',
					    value: NowApproveRequest,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:70,
			    items:
				[
					{
						xtype: 'datefield',
					    fieldLabel: nmFinishDateApproveCM + ' ',
					    id: 'dtpSelesaiAppRequest',
					    name: 'dtpSelesaiAppRequest',
					    format: 'd/M/Y',
					    value: NowApproveRequest,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .4,
			    layout: 'form',
			    border: false,
				labelWidth:100,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmEstCostApproveCM + ' ',
						style: 
						{
							'font-weight': 'bold',
							'text-align':'right'
						},
						readOnly:true,
						name: 'txtCostRateApproveRequest',
						id: 'txtCostRateApproveRequest',
						anchor: '99%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoApproveRequest3(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmRequesterApproveCM + ' ',
					    name: 'txtKdApproveRequesterApproveRequest',
					    id: 'txtKdApproveRequesterApproveRequest',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .7,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
						readOnly:true,
					    name: 'txtApproveRequesterApproveRequest',
					    id: 'txtApproveRequesterApproveRequest',
					    anchor: '99.70%'
					}
				]
			}//,
			// {
			    // columnWidth: .3,
			    // layout: 'form',
			    // border: false,
			    // items:
				// [
					// {
						// xtype: 'textfield',
						// hideLabel:true,
						// fieldLabel: '',
						// name: 'txtDepartmentApproveRequest',
						// id: 'txtDepartmentApproveRequest',
						// readOnly:true,
						// anchor: '100%'
					// }
				// ]
			// }
		]
	}
    return items;
};

function getItemPanelNoAssetApproveRequest2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmAssetIdNameEntryApproveCM + ' ',
					    name: 'txtKdMainAssetApproveRequest',
					    id: 'txtKdMainAssetApproveRequest',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
						readOnly:true,
					    name: 'txtNamaMainAsetApproveRequest',
					    id: 'txtNamaMainAsetApproveRequest',
					    anchor: '50%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelApproverApproveRequest2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmApproverApproveCM + ' ',
					    name: 'txtKdApproverApproveRequest',
					    id: 'txtKdApproverApproveRequest',
						readOnly:true,
					    anchor: '98%'
						// listeners: 
						// { 
							// 'specialkey' : function()
							// {
								// if (Ext.EventObject.getKey() === 13) 
								// {
									// var criteria='';
									// if (Ext.get('txtKdApproverApproveRequest').dom.value != '')
									// {
										// criteria = ' where EMP_ID like ~%' + Ext.get('txtKdApproverApproveRequest').dom.value + '%~'; 
									// };
									// FormLookupEmployee('txtKdApproverApproveRequest','txtNamaApproverApproveRequest',criteria);
								// };
							// }
						// }
					}
				]
			},
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
					    name: 'txtNamaApproverApproveRequest',
					    id: 'txtNamaApproverApproveRequest',
					    anchor: '50%',
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var criteria='';
									if (Ext.get('txtNamaApproverApproveRequest').dom.value != '')
									{
										//criteria = ' where EMP_NAME like ~%' + Ext.get('txtNamaApproverApproveRequest').dom.value + '%~';
                                                                                criteria = 'emp_name like ~%' + Ext.get('txtNamaApproverApproveRequest').dom.value + '%~';
									};
									FormLookupEmployee('txtKdApproverApproveRequest','txtNamaApproverApproveRequest',criteria);
								};
							},
							'focus' : function()
							{
								FocusCtrlCMApproveRequest='txtPIC';
							}
						}
					}
				]
			}
		]
	}
    return items;
};


function mComboMaksDataApproveRequest() 
{
    var cboMaksDataApproveRequest = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataApproveRequest',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmMaksData + ' ',
		    width: 50,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountApproveRequest,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountApproveRequest = b.data.displayText;
			        RefreshDataApproveRequestFilter();
			    }
			}
		}
	);
    return cboMaksDataApproveRequest;
};


function ApproveRequestSave(mBol) 
{	
	if (ValidasiEntryCMApproveRequest(nmHeaderSimpanData,false) == 1 )
	{
		if (AppIdCMApproveRequest == '' || AppIdCMApproveRequest === undefined) 
		{
			Ext.Ajax.request
			(
				{
					//url: "./Datapool.mvc/CreateDataObj",
                                        url: baseURL + "index.php/main/CreateDataObj",
					params: getParamApproveRequest(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true && cst.App === true) 
						{
							ShowPesanInfoCMApproveRequest(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataApproveRequest();
							if(mBol === false)
							{
								AppIdCMApproveRequest=cst.ApproveId;
								SchIdCMApproveRequest=cst.SchId;
								LoadDataServiceCMApproveRequest(SchIdCMApproveRequest);
								GetJumlahTotalPersonPartCMApproveRequest();
								LoadDataPartCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
								if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest != undefined)
								{
									LoadDataPartCMApproveRequest(SchIdCMApproveRequest,varCatIdCMApproveRequest,cellSelectServiceCMApproveRequest.data.SERVICE_ID);
								};
								
								if (IsExtRepairCMApproveRequest === false)
								{
									LoadDataEmployeeCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
									if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest != undefined)
									{
										LoadDataEmployeeCMApproveRequest(SchIdCMApproveRequest,varCatIdCMApproveRequest,cellSelectServiceCMApproveRequest.data.SERVICE_ID);
									};
								}
								else
								{
									LoadDataVendorCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
									if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest != undefined)
									{
										LoadDataVendorCMApproveRequest(SchIdCMApproveRequest,varCatIdCMApproveRequest,cellSelectServiceCMApproveRequest.data.SERVICE_ID);
									};
								};
							};
							AddNewApproveRequest = false;
						}
						else if (cst.success === true && cst.App === false) 
						{
							ShowPesanInfoCMApproveRequest('This request succeesed rejected',nmHeaderSimpanData);
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningCMApproveRequest(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorCMApproveRequest(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
                                        url: baseURL + "index.php/main/CreateDataObj",
					params: getParamApproveRequest(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoCMApproveRequest(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataApproveRequest();
							if(mBol === false)
							{
								 LoadDataServiceCMApproveRequest(SchIdCMApproveRequest);
								 GetJumlahTotalPersonPartCMApproveRequest();
								 LoadDataPartCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
									if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest != undefined)
									{
										LoadDataPartCMApproveRequest(SchIdCMApproveRequest,varCatIdCMApproveRequest,cellSelectServiceCMApproveRequest.data.SERVICE_ID);
									};
								
									if (IsExtRepairCMApproveRequest === false)
									{
										LoadDataEmployeeCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
										if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest != undefined)
										{
											LoadDataEmployeeCMApproveRequest(SchIdCMApproveRequest,varCatIdCMApproveRequest,cellSelectServiceCMApproveRequest.data.SERVICE_ID);
										};
									}
									else
									{
										LoadDataVendorCMApproveRequestTemp(SchIdCMApproveRequest,varCatIdCMApproveRequest);
										if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest != undefined)
										{
											LoadDataVendorCMApproveRequest(SchIdCMApproveRequest,varCatIdCMApproveRequest,cellSelectServiceCMApproveRequest.data.SERVICE_ID);
										};
									};
							};
						}
						else if (cst.success === true && cst.App === false) 
						{
							ShowPesanInfoCMApproveRequest(nmAlertReqRejected,nmHeaderSimpanData);
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningCMApproveRequest(nmPesanEditGagal,nmHeaderEditData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningCMApproveRequest(nmPesanEditGagal + ' , ' + nmKonfirmasiWO,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorCMApproveRequest(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};


function ApproveRequestDelete()
{
    if ((AppIdCMApproveRequest != null) && (AppIdCMApproveRequest != '') && (AppIdCMApproveRequest != undefined) )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmTitleFormApproveCM) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								//url: "./Datapool.mvc/DeleteDataObj",
                                                                url: baseURL + "index.php/main/DeleteDataObj",
								params: getParamApproveRequest(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoCMApproveRequest(nmPesanHapusSukses,nmHeaderHapusData);
										ApproveRequestAddNew();
										RefreshDataApproveRequest();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningCMApproveRequest(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else if (cst.success === false && cst.pesan===1)
									{
										ShowPesanWarningCMApproveRequest(nmPesanHapusGagal + ' , ' + nmKonfirmasiWO ,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorCMApproveRequest(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
};

function ValidasiEntryCMApproveRequest(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtKdApproverApproveRequest').getValue() == '') || (Ext.get('txtNamaApproverApproveRequest').getValue() == '') || (Ext.get('cboStatusCMApproveRequestEntry').getValue() == '') || selectStatusCMApproveRequestEntry == '' || selectStatusCMApproveRequestEntry == undefined || selectStatusAssetApprove == '' || selectStatusAssetApprove == undefined || (Ext.get('cboStatusAssetApprove').getValue() == Ext.getCmp('cboStatusAssetApprove').emptyText))
	{
		if (Ext.get('txtKdApproverApproveRequest').getValue() == '') 
		{
			x = 0;
			if (mBolHapus != true)
			{
				ShowPesanWarningCMApproveRequest(nmGetValidasiKosong(nmAppIDApproveCM), modul);
			};
		}
		else if (Ext.get('txtNamaApproverApproveRequest').getValue() == '') 
		{
			x = 0;
			if (mBolHapus != true)
			{
				ShowPesanWarningCMApproveRequest(nmGetValidasiKosong(nmAppNameApproveCM), modul);
			};
		}
		else if (Ext.get('cboStatusCMApproveRequestEntry').getValue() == '' || selectStatusCMApproveRequestEntry == '' ||  selectStatusCMApproveRequestEntry == undefined) 
		{
			x = 0;
			if (mBolHapus != true)
			{
				ShowPesanWarningCMApproveRequest(nmGetValidasiKosong(nmStatusApproveCM), modul);
			};
		}
		else if (Ext.get('cboStatusAssetApprove').dom.value == '' || selectStatusAssetApprove == '' ||  selectStatusAssetApprove == undefined || Ext.get('cboStatusAssetApprove').dom.value == Ext.getCmp('cboStatusAssetApprove').emptyText) 
		{
			x = 0;
			if (mBolHapus != true)
			{
				ShowPesanWarningCMApproveRequest(nmGetValidasiKosong(nmStatusSetupAsset), modul);
			};
		};
	};
	return x;
};

function ShowPesanWarningCMApproveRequest(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorCMApproveRequest(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoCMApproveRequest(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};



function mComboStatusCMApproveRequestEntry() 
{
	var Field = ['STATUS_ID','STATUS_NAME'];
	
   var  dsStatusCMApproveRequestEntry = new WebApp.DataStore({ fields: Field });
    dsStatusCMApproveRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ID',
                            Sort: 'status_id',
			    Sortdir: 'ASC',
			    target: 'ViewComboStatusVw',
			    //param: 'where status_id in ' + valueStatusCMApproveRequestEntry
                            param: 'status_id in ' + valueStatusCMApproveRequestEntry
			}
		}
	);
	
    var cboStatusCMApproveRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboStatusCMApproveRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: nmPilihStatus,
		    fieldLabel: nmStatusApproveCM + ' ',
		    align: 'Right',
		    anchor:'99%',
		    store: dsStatusCMApproveRequestEntry,
		    valueField: 'STATUS_ID',
		    displayField: 'STATUS_NAME',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectStatusCMApproveRequestEntry = b.data.STATUS_ID;
					if (selectStatusCMApproveRequestEntry === DefaultStatusRejectedCMApproveRequestView)
					{
						mEnabledCMApproveRequest(true);
					}
					else
					{
						mEnabledCMApproveRequest(false);
					};
			    }
			}
		}
	);

    return cboStatusCMApproveRequestEntry;
};


function mComboStatusCMApproveRequestView() 
{
	var Field = ['STATUS_ID','STATUS_NAME'];
	
    dsStatusCMApproveRequestView = new WebApp.DataStore({ fields: Field });
    dsStatusCMApproveRequestView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ID',
                            Sort: 'status_id',
			    Sortdir: 'ASC',
			    target: 'ViewComboStatusVw',
			    //param: 'where status_id in ' + valueStatusCMApproveRequestView2
                            param: 'status_id in ' + valueStatusCMApproveRequestView2
			}
		}
	);
	
    var cboStatusCMApproveRequestView = new Ext.form.ComboBox
	(
		{
		    id: 'cboStatusCMApproveRequestView',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmStatusApproveCM + ' ',
		    align: 'Right',
		    anchor:'60%',
		    store: dsStatusCMApproveRequestView,
		    valueField: 'STATUS_ID',
		    displayField: 'STATUS_NAME',
			value:valueStatusCMApproveRequestView,
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectStatusCMApproveRequestView = b.data.STATUS_ID;
			    }
			}
		}
	);
	
    return cboStatusCMApproveRequestView;
};

function GetRecordBaruPartCMApproveRequest()
{
	var p = new mRecordPartCMApproveRequest
	(
		{
			SCH_CM_ID:'',
			SERVICE_ID: cellSelectServiceCMApproveRequest.data.SERVICE_ID,
			PART_ID :'',
		    PART_NAME :'',
		    QTY :1,
		    UNIT_COST :0,
			TOT_COST:0,
			DESC_SCH_PART:'',
			TAG:1,
			ROW:GetUrutRowCMApproveRequest(dsDtlPartCMApproveRequest)
		}
	);
	return p;
};

function GetRecordBaruPersonCMApproveRequest(ds)
{
    var p = new mRecordPersonCMApproveRequest
    (
        {
            SCH_CM_ID:'',
            SERVICE_ID:cellSelectServiceCMApproveRequest.data.SERVICE_ID,
            ROW_SCH :'',
            EMP_ID :'',
            EMP_NAME:'',
            VENDOR_ID :GetVendorTambahBarisCMApproveRequest(1),
            VENDOR:GetVendorTambahBarisCMApproveRequest(2),
            PERSON_NAME :'',
            COST:'',
            DESC_SCH_PERSON:'',
            ROW:GetUrutRowCMApproveRequest(ds)
        }
    );
    return p;
};

function GetVendorTambahBarisCMApproveRequest(x)
{
	var str='';
	if(IsExtRepairCMApproveRequest === true)
	{
		if(x=== 1)
		{
			str=Ext.get('txtKdVendorApproveRequest').dom.value;
		}
		else
		{
			str=Ext.get('txtNamaVendorApproveRequest').dom.value;
		};
	};
	return str;
};

function GetRecordBaruServiceCMApproveRequest()
{
    var p = new mRecordServiceCMApproveRequest
    (
        {
            SCH_CM_ID:'',
            SERVICE_ID:'',
            SERVICE_NAME :'',
            CATEGORY_ID:varCatIdCMApproveRequest,
            TAG:1
        }
    );
    return p;
};

function GetNilaiCurrencyCMApproveRequest(dblNilai)
{
    for (var i = 0; i < dblNilai.length; i++)
    {
        var y = dblNilai.substr(i, 1)
        if (y === '.')
        {
            dblNilai = dblNilai.replace('.', '');
        }
    };

    return dblNilai;
};

function GetPersonIntServiceCMApproveRequest(service_id)
{
	if (service_id != '' && service_id != undefined)
	{
		dsDtlCraftPersonIntCMApproveRequest.removeAll();
		for (var i = 0; i < dsDtlCraftPersonIntCMApproveRequestTemp.getCount() ; i++) 
		{
			if(dsDtlCraftPersonIntCMApproveRequestTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlCraftPersonIntCMApproveRequest.insert(dsDtlCraftPersonIntCMApproveRequest.getCount(), dsDtlCraftPersonIntCMApproveRequestTemp.data.items[i]);
			};
		};
	};
};

function GetPersonExtServiceCMApproveRequest(service_id)
{
	if (service_id != '' && service_id != undefined)
	{
		dsDtlCraftPersonExtCMApproveRequest.removeAll();
		for (var i = 0; i < dsDtlCraftPersonExtCMApproveRequestTemp.getCount() ; i++) 
		{
			if(dsDtlCraftPersonExtCMApproveRequestTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlCraftPersonExtCMApproveRequest.insert(dsDtlCraftPersonExtCMApproveRequest.getCount(), dsDtlCraftPersonExtCMApproveRequestTemp.data.items[i]);
			};
		};
	};
};

function GetPartCMApproveRequest(service_id)
{
	if (service_id != '' && service_id != undefined)
	{
		dsDtlPartCMApproveRequest.removeAll();
		for (var i = 0; i < dsDtlPartCMApproveRequestTemp.getCount() ; i++) 
		{
			if(dsDtlPartCMApproveRequestTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlPartCMApproveRequest.insert(dsDtlPartCMApproveRequest.getCount(), dsDtlPartCMApproveRequestTemp.data.items[i]);
			};
		};
	};
};

function HapusRowTempCMApproveRequest(ds,idx)
{
	if (cellSelectServiceCMApproveRequest != '' && cellSelectServiceCMApproveRequest.data != undefined)
	{
		if (cellSelectServiceCMApproveRequest.data.SERVICE_ID != undefined || cellSelectServiceCMApproveRequest.data.SERVICE_ID != '')
		{
			for (var i = 0; i < ds.getCount() ; i++) 
			{
				if(ds.data.items[i].data.SERVICE_ID === cellSelectServiceCMApproveRequest.data.SERVICE_ID && ds.data.items[i].data.ROW === idx)
				{
					ds.removeAt(i);
				};
			}
		};
	};
};

function GetStrListServiceCMApproveRequest()
{
	var str='';
	
	for (var i = 0; i < dsDtlServiceCMApproveRequest.getCount() ; i++) 
	{
		if(dsDtlServiceCMApproveRequest.getCount() === 1)
		{
			str +=  '\'' + dsDtlServiceCMApproveRequest.data.items[i].data.SERVICE_ID + '\'';
		}
		else
		{
			if (i === dsDtlServiceCMApproveRequest.getCount()-1 )
			{
				str +=  '\'' + dsDtlServiceCMApproveRequest.data.items[i].data.SERVICE_ID + '\'';
			}
			else
			{
				str +=  '\'' + dsDtlServiceCMApproveRequest.data.items[i].data.SERVICE_ID + '\'' + ',' ;
			};
		};
		// if (dsDtlServiceCMApproveRequest.data.items[i].data.SERVICE_ID != '')
		// {
			// if (i === dsDtlServiceCMApproveRequest.getCount()-2 || dsDtlServiceCMApproveRequest.getCount() === 1)
			// {
				// str +=  '\'' + dsDtlServiceCMApproveRequest.data.items[i].data.SERVICE_ID + '\'';
			// }
			// else
			// {
				// str +=  '\'' + dsDtlServiceCMApproveRequest.data.items[i].data.SERVICE_ID + '\'' + ',' ;
			// };
		//};
	}
	
	return str;
};

function GetStrListPartCMApproveRequest(mBolLookup)
{
	var str='';
	
	for (var i = 0; i < dsDtlPartCMApproveRequest.getCount() ; i++) 
	{
		// if (dsDtlPartCMApproveRequest.data.items[i].data.PART_ID != '')
		// {
			// if (mBolLookup === true)
			// {
				if(dsDtlPartCMApproveRequest.getCount() === 1)
				{
					str +=  '\'' + dsDtlPartCMApproveRequest.data.items[i].data.PART_ID + '\'';
				}
				else
				{
					if (i === dsDtlPartCMApproveRequest.getCount()-1 )
					{
						str +=  '\'' + dsDtlPartCMApproveRequest.data.items[i].data.PART_ID + '\'';
					}
					else
					{
						str +=  '\'' + dsDtlPartCMApproveRequest.data.items[i].data.PART_ID + '\'' + ',' ;
					};
				};
			// }
			// else
			// {
				// if (i === dsDtlPartCMApproveRequest.getCount()-2 || dsDtlPartCMApproveRequest.getCount() === 1)
				// {
					// str +=  '\'' + dsDtlPartCMApproveRequest.data.items[i].data.PART_ID + '\'';
				// }
				// else
				// {
					// str +=  '\'' + dsDtlPartCMApproveRequest.data.items[i].data.PART_ID + '\'' + ',' ;
				// };
			// };
		// };
	}
	
	return str;
};


function GetUrutRowCMApproveRequest(ds)
{
	var x=1;
	if (ds != undefined && ds.data != undefined )
	{
		if ( ds.data.length > 0 )
		{
			x = ds.data.items[ds.getCount()-1].data.ROW + 1;
		};
	};
	
	return x;
};

function GetJumlahTotalPersonPartCMApproveRequest(mBol)
{
     Ext.Ajax.request
     (
        {
            //url: "./Module.mvc/ExecProc",
            url: baseURL + "index.php/main/ExecProc",
            params:
            {
                    UserID: 'Admin',
                    ModuleID: 'ProsesGetJumlahWOTotalCostCM',
                    //Params:	'where sch_cm_id =~' + SchIdCMApproveRequest + '~ and category_id =~' + varCatIdCMApproveRequest + '~'
                    Params:	'sch_cm_id = ~' + SchIdCMApproveRequest + '~ and category_id = ~' + varCatIdCMApproveRequest + '~'
            },
            success: function(o)
            {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                            Ext.get('txtTotalServiceCMApproveRequest').dom.value=formatCurrency(cst.TotalCost);
                            if(mBol === true)
                            {
                                    Ext.get('txtCostRateApproveRequest').dom.value=formatCurrency(cst.TotalCost);
                            };
                    }
                    else
                    {
                            Ext.get('txtTotalServiceCMApproveRequest').dom.value=formatCurrency(0);
                            if(mBol === true)
                            {
                                    Ext.get('txtCostRateApproveRequest').dom.value=formatCurrency(0);
                            };
                    };
            }

        }
    );
};

function GetJumlahTotalPartCMApproveRequest()
{
     Ext.Ajax.request
     (
        {
            //url: "./Module.mvc/ExecProc",
            url: baseURL + "index.php/main/ExecProc",
            params:
            {
                    UserID: 'Admin',
                    ModuleID: 'ProsesGetJumlahApprovePartCM',
                    //Params:	'where sch_cm_id =~' + SchIdCMApproveRequest + '~ and category_id =~' + varCatIdCMApproveRequest + '~ and service_id =~' + cellSelectServiceCMApproveRequest.data.SERVICE_ID + '~'
                    Params:	'sch_cm_id = ~' + SchIdCMApproveRequest + '~ and category_id = ~' + varCatIdCMApproveRequest + '~ and service_id = ~' + cellSelectServiceCMApproveRequest.data.SERVICE_ID + '~'
            },
            success: function(o)
            {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                            Ext.get('txtTotalPartCMApproveRequest').dom.value=formatCurrency(cst.Jumlah);
                    }
                    else
                    {
                            Ext.get('txtTotalPartCMApproveRequest').dom.value=formatCurrency(0);
                    };
            }

        }
    );
};

function GetJumlahTotalPersonCMApproveRequest(str)
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahApprovePersonCM',
				Params:	str
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.get('txtTotalPersonCostCMApproveRequest').dom.value=formatCurrency(cst.Jumlah);
				}
				else
				{
					Ext.get('txtTotalPersonCostCMApproveRequest').dom.value=formatCurrency(0);
				};
			}

		}
	);
};

function GetVendorCMApproveRequest()
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetVendorCMApproveRequest',
				//Params:	'where sch_cm_id=~' + SchIdCMApproveRequest + '~ and category_id=~' + varCatIdCMApproveRequest + '~'
                                Params:	'sch_cm_id = ~' + SchIdCMApproveRequest + '~ and category_id = ~' + varCatIdCMApproveRequest + '~'
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.get('txtKdVendorApproveRequest').dom.value=cst.Id;
					Ext.get('txtNamaVendorApproveRequest').dom.value=cst.Nama;
					LoadVendorInfoDetailCMApproveRequest(cst.Id);
				}
				else
				{
					Ext.get('txtKdVendorApproveRequest').dom.value='';
					Ext.get('txtNamaVendorApproveRequest').dom.value='';
				};
			}

		}
	);
};

function LoadVendorInfoDetailCMApproveRequest(id)
{
	var fldDetail = ['VENDOR_ID','VENDOR','CONTACT1','CONTACT2','VEND_ADDRESS','VEND_CITY','VEND_PHONE1','VEND_PHONE2','VEND_POS_CODE','COUNTRY'];
	
	dsLookVendorListCMApproveRequest = new WebApp.DataStore({ fields: fldDetail });
	
	dsLookVendorListCMApproveRequest.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'VENDOR_ID',
                                Sort: 'vendor_id',
				Sortdir: 'ASC',
				target: 'LookupVendor',
				//param: 'where vendor_id=~' + id + '~'
                                param: 'vendor_id = ~' + id + '~'
			}
		}
	);
	return dsLookVendorListCMApproveRequest;
};


function mComboStatusAssetApprove()
{
	var Field = ['STATUS_ASSET_ID', 'STATUS_ASSET'];
	var dsStatusAssetApprove = new WebApp.DataStore({ fields: Field });

	dsStatusAssetApprove.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ASSET',
                            Sort: 'status_asset_id',
			    Sortdir: 'ASC',
			    target: 'viComboStatusAsset',
			    //param:' WHERE STATUS_ASSET_ID <> ~xxx~'
                            param:'status_asset_id <> ~xxx~'
			}
		}
	);
	
  var cboStatusAssetApprove = new Ext.form.ComboBox
	(
		{
			id:'cboStatusAssetApprove',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: nmPilihStatusSetupAset,
			fieldLabel: nmStatusAssetApprove + ' ',			
			anchor:'99%',
			store: dsStatusAssetApprove,
			valueField: 'STATUS_ASSET_ID',
			displayField: 'STATUS_ASSET',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectStatusAssetApprove=b.data.STATUS_ASSET_ID ;
				} 
			}
		}
	);
	
	dsStatusAssetApprove.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ASSET',
                            Sort: 'status_asset_id',
			    Sortdir: 'ASC',
			    target: 'viComboStatusAsset',
			     //param:' WHERE STATUS_ASSET_ID <> ~xxx~'
                             param:'status_asset_id <> ~xxx~'
			}
		}
	);
	
	return cboStatusAssetApprove;
};




	













