var CurrentTRRequest =
{
    data: Object,
    details: Array,
    row: 0
};


var mRecordRequest = Ext.data.Record.create
(
    [
       {name: 'ASSET_MAINT', mapping:'ASSET_MAINT'},
       {name: 'ASSET_MAINT_ID', mapping:'ASSET_MAINT_ID'},
       {name: 'ASSET_MAINT_NAME', mapping:'ASSET_MAINT_NAME'},
       {name: 'LOCATION_ID', mapping:'LOCATION_ID'},
       {name: 'LOCATION', mapping:'LOCATION'},
       {name: 'PROBLEM', mapping:'PROBLEM'},
       {name: 'REQ_FINISH_DATE', mapping:'REQ_FINISH_DATE'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'IMPACT', mapping:'IMPACT'},
       {name: 'ROW_REQ', mapping:'ROW_REQ'}
    ]
);

var selectDepartCMRequestEntry;
var selectStatusCMRequestView=' 9999';
var dsDepartCMRequestEntry;
var dsTRRequestList;
var dsDTLTRRequestList;
var dsStatusCMRequestView;
var AddNewRequest = true;
var selectCountRequest = 50;
var now = new Date();
var selectCMRequest;
var rowSelectedRequest;
var cellSelectedRequestDet;
var TRRequestLookUps;
var valueStatusCMRequestView='All';
var DefaultValueStatusCMRequestView='(\' 9999\',\'1\',\'6\',\'7\')';
var DefaultStatusAcceptedCMRequestView='6';
var DefaultStatusApproveCMRequestView='2';
var nowCMRequest = new Date();
var FocusCtrlCMRequest;

CurrentPage.page = getPanelRequest(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelRequest(mod_id) 
{
    var Field = ['STATUS_ID','STATUS_NAME','LOCATION_ID','ASSET_MAIN','LOCATION','REQ_DATE','REQ_ID','PROBLEM','EMP_ID','EMP_NAME','DEPT_ID','DEPT_NAME','IMPACT','REQ_FINISH_DATE','ASSET_MAINT_ID','ROW_REQ','DESC_REQ','DESC_STATUS'];
	
    dsTRRequestList = new WebApp.DataStore({ fields: Field });

    var grListTRRequest = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            store: dsTRRequestList,
            anchor: '100% 91.9999%',
            columnLines: true,
                autoScroll:true,
            border: false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedRequest = dsTRRequestList.getAt(row);
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedRequest = dsTRRequestList.getAt(ridx);
                    if (rowSelectedRequest != undefined)
                    {
                        RequestLookUp(rowSelectedRequest.data);
                    }
                    else
                    {
                        RequestLookUp();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colStatusImageRequestViewRequest',
                        header: nmStatusRequest,
                        dataIndex: 'STATUS_ID',
                        sortable: true,
                        width: 50,
                        align:'center',
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case '1':
                                         metaData.css = 'StatusHijau'; // open
                                         break;
                                 case '6':
                                         metaData.css = 'StatusKuning'; // accepted / approve
                                         break;
                                 case '7':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
                    {
                        id: 'colReqIdViewRequest',
                        header: nmRequestId,
                        dataIndex: 'REQ_ID',
                        sortable: true,
                        width: 100
                    },
                    {
                        id: 'colTglRequestViewRequest',
                        header: nmRequestDate,
                        dataIndex: 'REQ_DATE',
                        sortable: true,
                        width: 100,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.REQ_DATE);

                        }
                    },
                    {   //Nama Aset + Kode
                        id: 'colAssetMaintViewRequest',
                        header: nmAssetRequest,
                        dataIndex: 'ASSET_MAIN',
                        sortable: true,
                        width: 170
                    },
                    {
                        id: 'colLocationViewRequest',
                        header: nmLocationRequest,
                        dataIndex: 'LOCATION',
                        sortable: true,
                        width: 100
                    },
                    {
                        id: 'colProblemViewRequest',
                        header: nmProblemRequest,
                        dataIndex: 'PROBLEM',
                        width: 300
                    },
                    {
                        header: nmRequesterRequest,
                        width: 100,
                        sortable: true,
                        dataIndex: 'EMP_NAME',
                        id: 'colRequesterViewRequest'
                    },
                    {
                        id: 'colDeptViewRequest',
                        header: nmDeptRequest,
                        dataIndex: 'DEPT_NAME',
                        sortable: true,
                        width: 100
                    },
                    {
                        id: 'colImpactViewRequest',
                        header: nmImpactRequest,
                        dataIndex: 'IMPACT',
                        width: 100
                    },
                    {
                        id: 'colTglSelesaiViewRequest',
                        header: nmTargetDateRequest,
                        dataIndex: 'REQ_FINISH_DATE',
                        width: 100,
                        renderer: function(v, params, record)
                        {
                        //var d = record.data.REQ_FINISH_DATE;
                                //var value = Ext.isDate(d) ? ShowDate(d.format("Y-m-d")) : ShowDate(d);
                            return ShowDate(record.data.REQ_FINISH_DATE);
                        }
                    }
                ]
            ),
            tbar:
                [
                    {
                        id: 'btnEditRequest',
                        text: nmEditData,
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedRequest != undefined)
                            {
                                    RequestLookUp(rowSelectedRequest.data);
                            }
                            else
                            {
                                    RequestLookUp();
                            }
                        }
                    }, ' ', '-',
                    {
                        xtype: 'checkbox',
                        id: 'chkWithTglRequest',
                        hideLabel:true,
                        checked: false,
                        handler: function()
                        {
                            if (this.getValue()===true)
                            {
                                Ext.get('dtpTglAwalFilterRequest').dom.disabled=false;
                                Ext.get('dtpTglAkhirFilterRequest').dom.disabled=false;

                            }
                            else
                            {
                                Ext.get('dtpTglAwalFilterRequest').dom.disabled=true;
                                Ext.get('dtpTglAkhirFilterRequest').dom.disabled=true;
                            };
                        }
                    }
                    , ' ', '-', nmRequestDate + ' : ', ' ',
                    {
                        xtype: 'datefield',
                        fieldLabel: nmRequestDate + ' ',
                        id: 'dtpTglAwalFilterRequest',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        onInit: function() { }
                    }, ' ', '  ' + nmSd + ' ', ' ', {
                        xtype: 'datefield',
                        fieldLabel: nmSd + ' ',
                        id: 'dtpTglAkhirFilterRequest',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    }
                ]
            }
	);
	
	var LegendViewCMRequest = new Ext.Panel
	(
            {
            id: 'LegendViewCMRequest',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            //height:32,
            anchor: '100% 8.0001%',
            autoScroll:false,
            items:
            [
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .08,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Open"
                },
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    border: false,
                    //height:32,
                    anchor: '100% 8.0001%',
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/kuning.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Accepted"
                },
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    border: false,
                    //height:35,
                    anchor: '100% 8.0001%',
                    //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Rejected"
                }
            ]

        }
    )

    var FormTRRequest = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: nmTitleFormRequest,
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [grListTRRequest,LegendViewCMRequest],
            tbar:
            [
                nmRequestId + ' : ', ' ',
                {
                    xtype: 'textfield',
                    fieldLabel: nmRequestId + ' ',
                    id: 'txtReqIdViewRequestFilter',
                    width: 100,
                    onInit: function() { }
                },' ', '-',
                nmStatusRequest + ' : ', ' ',mComboStatusCMRequestView()
                ,' ', '-',
                nmMaksData + ' : ', ' ', mComboMaksDataRequest(),
                ' ', '-',
                {
                    text: nmRefresh,
                    tooltip: nmRefresh,
                    iconCls: 'refresh',
                    anchor: '25%',
                    handler: function(sm, row, rec)
                    {
                        rowSelectedRequest=undefined;
                        RefreshDataRequestFilter();
                    }
                }
            ],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	
   RefreshDataRequest();
   RefreshDataRequest();
	
   return FormTRRequest

};

function RequestLookUp(rowdata) 
{
    var lebar = 735;
    TRRequestLookUps = new Ext.Window
    (
        {
            id: 'gridRequest',
            title: nmTitleFormRequest,
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRRequest(lebar),
            listeners:
            {
                activate: function()
                {
                     if (varBtnOkLookupEmp === true)
                    {
                        selectDepartCMRequestEntry  = rowSelectedLookEmployee.data.DEPT_ID;
                        Ext.get('cboDepartCMRequestEntry').dom.value = rowSelectedLookEmployee.data.DEPT_NAME;
                        varBtnOkLookupEmp=false;
                    };
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
                    rowSelectedRequest=undefined;
                    RefreshDataRequestFilter();
                }
            }
        }
    );

    TRRequestLookUps.show();
    if (rowdata == undefined) 
	{
        RequestAddNew();
    }
    else 
	{
        TRRequestInit(rowdata)
    }

};

function getFormEntryTRRequest(lebar) 
{
    var pnlTRRequest = new Ext.FormPanel
    (
        {
            id: 'PanelTRRequest',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
                height:146,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputRequest(lebar)],
            tbar:
            [
                {
                    text: nmTambah,
                    id: 'btnTambahRequest',
                    tooltip: nmTambah,
                    iconCls: 'add',
                    handler: function()
                        {
                        RequestAddNew();
                    }
                }, '-',
                {
                    text: nmSimpan,
                    id: 'btnSimpanRequest',
                    tooltip: nmSimpan,
                    iconCls: 'save',
                    handler: function()
                        {
                        RequestSave(false);
                        RefreshDataRequestFilter();
                    }
                }, '-',
                {
                    text: nmSimpanKeluar,
                    id: 'btnSimpanKeluarRequest',
                    tooltip: nmSimpanKeluar,
                    iconCls: 'saveexit',
                    handler: function()
                        {
                                var x =  RequestSave(true);
                         RefreshDataRequestFilter();
                                if (x===undefined)
                                {
                                        TRRequestLookUps.close();
                                };
                    }
                }, '-',
                {
                    text: nmHapus,
                    id: 'btnHapusRequest',
                    tooltip: nmHapus,
                    iconCls: 'remove',
                    handler: function()
                        {
                        RequestDelete();
                        RefreshDataRequestFilter();
                    }
                }, '-',
                {
                    text: nmLookup,
                    id: 'btnLookupRequest',
                    tooltip: nmLookup,
                    iconCls: 'find',
                    handler: function()
                    {
                        if (FocusCtrlCMRequest === 'txtAset')
                        {
                            var p = RecordBaruRequest();
                            var str='';
                            if (selectDepartCMRequestEntry != undefined && selectDepartCMRequestEntry != '')
                            {
                                    //str = ' where dept_id =~' + selectDepartCMRequestEntry + '~';
                                    //str = "\"DEPT_ID\" = ~" + selectDepartCMRequestEntry + "~";
                                    str = 'dept_id = ~' + selectDepartCMRequestEntry + '~';
                            };
                            FormLookupAset(str, dsDTLTRRequestList, p, true, '', true,selectDepartCMRequestEntry,Ext.get('cboDepartCMRequestEntry').dom.value);
                        }
                        else if (FocusCtrlCMRequest === 'txtReq')
                        {
                            //FormLookupEmployee(x,y,criteria,mBolGrid,p,dsStore,mBolLookup)
                            FormLookupEmployee('txtKdRequesterRequest','txtRequesterRequest','');
                            //FormLookupEmployee('txtKdRequesterRequest','txtRequesterRequest','',true,p,dsDTLTRRequestList,true);
                        };
                    }
                },
                '-', '->', '-',
                {
                    text: nmCetak,
                    id:'btnCetakRequest',
                    tooltip: nmCetak,
                    iconCls: 'print',
                    handler: function() {
                        var cKriteria;
                        cKriteria = Ext.get('txtNoRequest').dom.value + '###1###';
                        cKriteria += Ext.get('dtpTanggalRequest').dom.value + '###2###';
                        cKriteria += Ext.get('txtRequesterRequest').dom.value + '###3###';
                        cKriteria += Ext.get('cboDepartCMRequestEntry').dom.value + '###4###';
                        ShowReport('', '920007', cKriteria);
                    }
                }
            ]
        }
    );

 var GDtabDetailRequest = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailRequest',
        region: 'center',
        activeTab: 0,
        anchor: '100% 69%',
        border:false,
        plain: true,
        defaults:
                {
                autoScroll: true
        },
                items: GetDTLTRRequestGrid(),
        tbar:
                [
                        {
                                id:'btnTambahBrsRequest',
                                text: nmTambahBaris,
                                tooltip: nmTambahBaris,
                                iconCls: 'AddRow',
                                handler: function()
                                {
                                        TambahBarisRequest();
                                }
                        },
                        '-',
                        {
                                id:'btnHpsBrsRequest',
                                text: nmHapusBaris,
                                tooltip: nmHapusBaris,
                                iconCls: 'RemoveRow',
                                handler: function()
                                {
                                        if (dsDTLTRRequestList.getCount() > 0 )
                                        {
                                                if (cellSelectedRequestDet != undefined)
                                                {
                                                        if(CurrentTRRequest != undefined)
                                                        {
                                                                HapusBarisRequest();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningRequest(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
                                                }
                                        }
                                }
                        },' ','-'
                ]
        }
    );
	
   
    var FormTRRequest = new Ext.Panel
	(
		{
		    id: 'FormTRRequest',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRRequest,GDtabDetailRequest]

		}
	);

    return FormTRRequest
};
///---------------------------------------------------------------------------------------///

function TambahBarisRequest()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRequest();
        dsDTLTRRequestList.insert(dsDTLTRRequestList.getCount(), p);
    };
};

function HapusBarisRequest()
{
    if ( cellSelectedRequestDet != undefined )
    {
        if (cellSelectedRequestDet.data.ASSET_MAINT != '' && cellSelectedRequestDet.data.ASSET_MAINT_ID != '')
        {
            Ext.Msg.show
            (
                {
                   title:nmHapusBaris,
                   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentTRRequest.row + 1) + ' ' + nmOperatorDengan + ' ' + nmAsetIDRequest + ' : ' + cellSelectedRequestDet.data.ASSET_MAINT_ID + ' ' + nmOperatorAnd + ' ' + nmAsetNameRequest + ' : ' + cellSelectedRequestDet.data.ASSET_MAINT_NAME ,
                   buttons: Ext.MessageBox.YESNO,
                   fn: function (btn)
                   {
                       if (btn =='yes')
                        {
                            if(dsDTLTRRequestList.data.items[CurrentTRRequest.row].data.ROW_REQ === '')
                            {
                                dsDTLTRRequestList.removeAt(CurrentTRRequest.row);
                            }
                            else
                            {
                                Ext.Msg.show
                                (
                                    {
                                       title:nmHapusBaris,
                                       msg: nmGetValidasiHapusBarisDatabase(),
                                       buttons: Ext.MessageBox.YESNO,
                                       fn: function (btn)
                                       {
                                            if (btn =='yes')
                                            {
                                                RequestDeleteDetail();
                                            };
                                       }
                                    }
                                )
                            };
                        };
                   },
                   icon: Ext.MessageBox.QUESTION
                }
            );
        }
        else
        {
            dsDTLTRRequestList.removeAt(CurrentTRRequest.row);
        };
    }
};

function RequestDeleteDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamRequestDeleteDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoRequest(nmPesanHapusSukses,nmHeaderHapusData);
                    dsDTLTRRequestList.removeAt(CurrentTRRequest.row);
                    cellSelectedRequestDet=undefined;
                    RefreshDataRequestDetail(Ext.get('txtNoRequest').dom.value);
                    AddNewRequest = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningRequest(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningRequest(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamRequestDeleteDetail()
{
    var params =
    {
        Table: 'ViewRequestCM',
        ReqId: CurrentTRRequest.data.data.REQ_ID,
        RowReq: CurrentTRRequest.data.data.ROW_REQ,
        Hapus:2
    };
	
    return params
};

function GetDTLTRRequestGrid() 
{
    var fldDetail = ['ASSET_MAINT_ID','ASSET_MAINT_NAME','ASSET_MAINT','LOCATION_ID','LOCATION','PROBLEM','DESC_REQ','REQ_FINISH_DATE','REQ_ID','ROW_REQ','STATUS_ID','IMPACT','DESC_STATUS'];
	
    dsDTLTRRequestList = new WebApp.DataStore({ fields: fldDetail })

    var gridDTLTRRequest = new Ext.grid.EditorGridPanel
    (
        {
            title: nmTitleDetailFormRequest,
            stripeRows: true,
            store: dsDTLTRRequestList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
                autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelectedRequestDet = dsDTLTRRequestList.getAt(row);
                            CurrentTRRequest.row = row;
                            CurrentTRRequest.data = cellSelectedRequestDet;
                            FocusCtrlCMRequest='txtAset';
                        }
                    }
                }
            ),
            cm: TRRequestDetailColumModel()
                //, viewConfig:{forceFit: true}
        }
    );

    return gridDTLTRRequest;
};

function TRRequestDetailColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'colKdNameAssetRequest',
                header: nmAssetRequest,
                dataIndex: 'ASSET_MAINT',
                width:170,
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldAsetNameRequest',
                        allowBlank: false,
                        enableKeyEvents : true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                if (Ext.EventObject.getKey() === 13)
                                {
                                    var str='';

                                    if(Ext.get('fieldAsetNameRequest').dom.value != '')
                                    {
                                        //str=' where ASSET_MAINT_NAME like ~%' + Ext.get('fieldAsetNameRequest').dom.value + '%~';
                                        //str="\"ASSET_MAINT_NAME\" like ~%" + Ext.get('fieldAsetNameRequest').dom.value + "%~";
                                        str="asset_maint_name like ~%" + Ext.get('fieldAsetNameRequest').dom.value + "%~";

                                        if (selectDepartCMRequestEntry != undefined && selectDepartCMRequestEntry != '')
                                        {
                                            //str += ' and dept_id =~' + selectDepartCMRequestEntry + '~';
                                            //str += " AND \"DEPT_ID\" = ~" + selectDepartCMRequestEntry + "~";
                                            str += " AND dept_id = ~" + selectDepartCMRequestEntry + "~";
                                        };
                                    }
                                    else
                                    {
                                        if (selectDepartCMRequestEntry != undefined && selectDepartCMRequestEntry != '')
                                        {
                                            //str = ' where dept_id =~' + selectDepartCMRequestEntry + '~';
                                            //str = "\"DEPT_ID\" = ~" + selectDepartCMRequestEntry + "~";
                                            str = "dept_id = ~" + selectDepartCMRequestEntry + "~";
                                        };
                                    };
									
                                    GetLookupAssetCMRequest(str);
                                };
                            },
                            'focus' : function()
                            {
                                FocusCtrlCMRequest='txtAset';
                            }
                        }
                    }
                )
            },
            {
                id: 'colKdAssetRequest',
                header: nmAssetRequest,
                dataIndex: 'ASSET_MAINT_ID',
                width:100,
                hidden:true
            },
            {
                id: 'colAsetNameRequest',
                header:nmAssetRequest,
                dataIndex: 'ASSET_MAINT_NAME',
                sortable: false,
                hidden:true,
                width: 200,
                renderer: function(value, cell)
                {
                    var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
                    return str;
                }
            },
            {
                id: 'colLocationRequest',
                header: nmLocationRequest,
                dataIndex: 'LOCATION',
                width:100
            },
            {
                id: 'colProblemRequest',
                header: nmProblemRequest,
                width:170,
                dataIndex: 'PROBLEM',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemRequest',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30
                    }
                ),
                renderer: function(value, cell)
                {
                    var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
                    return str;
                }
            },
            {
                id: 'colDeskripsiRequest',
                header: nmDeskRequest,
                width:150,
                dataIndex: 'DESC_REQ',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolDeskripsiRequest',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:90
                    }
                ),
                renderer: function(value, cell)
                {
                    var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
                    return str;
                }
            },
            {
               header: nmTargetDateRequest,
               dataIndex: 'REQ_FINISH_DATE',
               width: 95,
               renderer: function(v, params, record)
                    {
                        //var d = record.data.REQ_FINISH_DATE;
                        //var value = Ext.isDate(d) ? ShowDate(d.format("Y-m-d")) : ShowDate(d);
                        return ShowDate(record.data.REQ_FINISH_DATE);
                    },
               editor: new Ext.form.DateField({
                    format: 'd/M/Y',
                    minValue: '2010-01-01'
                    // disabledDays: [0, 6],
                    // disabledDaysText: 'Plants are not available on the weekends'
                })
            },
			// {
				// id: 'colTglSelesaiRequest',
				// header: "Target Date",
				// width:95,
				// dataIndex: 'REQ_FINISH_DATE', 
				// // editor: new Ext.form.DateField
				// // (
					// // {
						// // id:'fieldcolTglSelesaiRequest',
						// // allowBlank: true,
						// // enableKeyEvents : true,
						// // width:'100%'
					// // }
				// // ),
				// renderer: function(v, params, record) 
				// {
					// return ShowDate(record.data.REQ_FINISH_DATE);
				// },
				// editor: new Ext.form.DateField
				// (
					// {
						// format: 'm/d/y',
						// listener: 
						// {
							// change: function(field, vnew, vold)
							// {
								// field=vnew.format('ymd');							
							// }
						// }
					// }
				// )
			// },
            {
                id: 'colImpactRequest',
                header: nmImpactRequest,
                width:200,
                dataIndex: 'IMPACT',
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactRequest',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                ),
                renderer: function(value, cell)
                {
                    var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
                    return str;
                }
            }

        ]
    )
};

function GetLookupAssetCMRequest(str)
{
	if (AddNewRequest === true)
	{
		var p = new mRecordRequest
		(
			{
				'ASSET_MAINT':'',
				'ASSET_MAINT_ID':'',
				'ASSET_MAINT_NAME':'', 
				'LOCATION_ID':'', 
				'LOCATION':'',
				'PROBLEM':dsDTLTRRequestList.data.items[CurrentTRRequest.row].data.PROBLEM,
				'REQ_FINISH_DATE':dsDTLTRRequestList.data.items[CurrentTRRequest.row].data.REQ_FINISH_DATE,
				'DESC_REQ':dsDTLTRRequestList.data.items[CurrentTRRequest.row].data.DESC_REQ,
				'IMPACT':dsDTLTRRequestList.data.items[CurrentTRRequest.row].data.IMPACT,
				'ROW_REQ':''
			}
		);
		
		FormLookupAset(str,dsDTLTRRequestList,p,true,'',false,selectDepartCMRequestEntry,Ext.get('cboDepartCMRequestEntry').dom.value);
	}
	else
	{	
		var p = new mRecordRequest
		(
			{
				'ASSET_MAINT':'',
				'ASSET_MAINT_ID':'',
				'ASSET_MAINT_NAME':'', 
				'LOCATION_ID':'', 
				'LOCATION':'',
				'PROBLEM':dsDTLTRRequestList.data.items[CurrentTRRequest.row].data.PROBLEM,
				'REQ_FINISH_DATE':dsDTLTRRequestList.data.items[CurrentTRRequest.row].data.REQ_FINISH_DATE,
				'DESC_REQ':dsDTLTRRequestList.data.items[CurrentTRRequest.row].data.DESC_REQ,
				'IMPACT':dsDTLTRRequestList.data.items[CurrentTRRequest.row].data.IMPACT,
				'ROW_REQ':dsDTLTRRequestList.data.items[CurrentTRRequest.row].data.ROW_REQ
			}
		);
	
		FormLookupAset(str,dsDTLTRRequestList,p,false,CurrentTRRequest.row,false,selectDepartCMRequestEntry,Ext.get('cboDepartCMRequestEntry').dom.value);
	};
};

function RecordBaruRequest()
{
	var p = new mRecordRequest
	(
		{
			'ASSET_MAINT':'',
			'ASSET_MAINT_ID':'',
		    'ASSET_MAINT_NAME':'', 
		    'LOCATION_ID':'', 
		    'LOCATION':'',
		    'PROBLEM':'',
		    'REQ_FINISH_DATE':'', 
		    'DESC_REQ':'',
		    'IMPACT':'',
		    'ROW_REQ':''
		}
	);
	
	return p;
};

function TRRequestInit(rowdata)
{
    AddNewRequest = false;
	
	Ext.get('txtNoRequest').dom.value = rowdata.REQ_ID;
    Ext.get('dtpTanggalRequest').dom.value = ShowDate(rowdata.REQ_DATE);
	Ext.get('txtKdRequesterRequest').dom.value= rowdata.EMP_ID;
	Ext.get('txtRequesterRequest').dom.value = rowdata.EMP_NAME;
	selectDepartCMRequestEntry  = rowdata.DEPT_ID;
	Ext.get('cboDepartCMRequestEntry').dom.value = rowdata.DEPT_NAME;
	RefreshDataRequestDetail(rowdata.REQ_ID);
	
	if (rowdata.STATUS_ID != 1)
	{
		mEnabledRequestCM(true);
	}
	else
	{
		mEnabledRequestCM(false);
	};
};

function mEnabledRequestCM(mBol)
{
	 Ext.get('btnSimpanRequest').dom.disabled=mBol;
	 Ext.get('btnSimpanKeluarRequest').dom.disabled=mBol;
	 Ext.get('btnHapusRequest').dom.disabled=mBol;
	 Ext.get('btnLookupRequest').dom.disabled=mBol;
	 Ext.get('btnTambahBrsRequest').dom.disabled=mBol;
	 Ext.get('btnHpsBrsRequest').dom.disabled=mBol;
};


///---------------------------------------------------------------------------------------///
function RequestAddNew() 
{
    AddNewRequest = true;
	Ext.get('txtNoRequest').dom.value = '';
    Ext.get('dtpTanggalRequest').dom.value = nowCMRequest.format('d/M/Y');
	Ext.get('txtKdRequesterRequest').dom.value='';
	Ext.get('txtRequesterRequest').dom.value = '';
	selectDepartCMRequestEntry  = undefined;
	Ext.get('cboDepartCMRequestEntry').dom.value = '';
	rowSelectedRequest=undefined;
	dsDTLTRRequestList.removeAll();
	mEnabledRequestCM(false);

};

function RefreshDataRequestDetail(req_id) 
{
    var strKriteriaRequest='';
    //strKriteriaRequest = 'where req_id = ~' + req_id + '~'
    //strKriteriaRequest = "\"REQ_ID\" = ~" + req_id + "~";
    strKriteriaRequest = "req_id = ~" + req_id + "~";
   
    dsDTLTRRequestList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'req_date',
			    //Sort: 'REQ_DATE',
			    Sortdir: 'ASC',
			    target: 'ViewRequestCMDetail',
			    param: strKriteriaRequest
			}
		}
	);
    return dsDTLTRRequestList;
};

///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamRequest() 
{
    var params =
	{
		Table:'ViewRequestCM',
		ReqId: Ext.get('txtNoRequest').getValue(),
		EmpId: Ext.get('txtKdRequesterRequest').getValue(),
		DeptId:selectDepartCMRequestEntry,
		Tgl: Ext.get('dtpTanggalRequest').dom.value,
		List:getArrDetailRequest(),
		JmlField: mRecordRequest.prototype.fields.length-4,
		JmlList:GetListCountRequestDetail(),
		Hapus:1
	};
    return params
};

function GetListCountRequestDetail()
{
	
	var x=0;
	for(var i = 0 ; i < dsDTLTRRequestList.getCount();i++)
	{
		if (dsDTLTRRequestList.data.items[i].data.ASSET_MAINT_ID != '' || dsDTLTRRequestList.data.items[i].data.ASSET_MAINT_NAME  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getArrDetailRequest()
{
	var x='';
	for(var i = 0 ; i < dsDTLTRRequestList.getCount();i++)
	{
		if (dsDTLTRRequestList.data.items[i].data.ASSET_MAINT_ID != '' && dsDTLTRRequestList.data.items[i].data.ASSET_MAINT_NAME != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'ROW_REQ=' + dsDTLTRRequestList.data.items[i].data.ROW_REQ
			y += z + 'ASSET_MAINT_ID='+dsDTLTRRequestList.data.items[i].data.ASSET_MAINT_ID
			y += z + 'PROBLEM='+dsDTLTRRequestList.data.items[i].data.PROBLEM
			y += z + 'REQ_FINISH_DATE='+ ShowDate(dsDTLTRRequestList.data.items[i].data.REQ_FINISH_DATE)
			y += z + 'DESC_REQ='+dsDTLTRRequestList.data.items[i].data.DESC_REQ
			y += z + 'IMPACT='+dsDTLTRRequestList.data.items[i].data.IMPACT
			
			if (i === (dsDTLTRRequestList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getItemPanelInputRequest(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:98,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoRequest(lebar),getItemPanelNoRequest2(lebar),mComboDepartCMRequestEntry()  				
				]
			}
		]
	};
    return items;
};



function getItemPanelNoRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .45,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmRequestId + ' ',
					    name: 'txtNoRequest',
					    id: 'txtNoRequest',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .55,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: nmRequestDate + ' ',
					    id: 'dtpTanggalRequest',
					    name: 'dtpTanggalRequest',
					    format: 'd/M/Y',
					    value: now,
					    anchor: '70%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoRequest2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 45,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoRequest3() 
				]
			}
		]
	}
    return items;
};

function getItemPanelNoRequest3() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:100,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmRequesterRequest + ' ',
					    name: 'txtKdRequesterRequest',
					    id: 'txtKdRequesterRequest',
						readOnly:true,
					    anchor: '98%',
					    listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var criteria='';
									if (Ext.get('txtKdRequesterRequest').dom.value != '')
									{
										//criteria = ' where EMP_ID like ~%' + Ext.get('txtKdRequesterRequest').dom.value + '%~'; 
										//criteria = "\"EMP_ID\" like ~%" + Ext.get('txtKdRequesterRequest').dom.value + "%~";
                                                                                criteria = "emp_id like ~%" + Ext.get('txtKdRequesterRequest').dom.value + "%~";
									};									
									FormLookupEmployee('txtKdRequesterRequest','txtRequesterRequest',criteria);
									//sFormLookupEmployee('txtKdRequesterRequest','txtRequesterRequest',criteria,true,p,dsDTLTRRequestList,true);
								};
							}
						}
					}
				]
			},
			{
			    columnWidth: .7,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
					    name: 'txtRequesterRequest',
					    id: 'txtRequesterRequest',
					    anchor: '99.99%',
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var criteria='';
									if (Ext.get('txtRequesterRequest').dom.value != '')
									{
										//criteria = ' where EMP_NAME like ~%' + Ext.get('txtRequesterRequest').dom.value + '%~';
										//criteria = "\"EMP_NAME\" like ~%" + Ext.get('txtRequesterRequest').dom.value + "%~";
                                                                                criteria = "emp_name like ~%" + Ext.get('txtRequesterRequest').dom.value + "%~";
									};
									FormLookupEmployee('txtKdRequesterRequest','txtRequesterRequest',criteria);
									//FormLookupEmployee('txtKdRequesterRequest','txtRequesterRequest',criteria,true,p,dsDTLTRRequestList,true);

								};
							},
							'focus' : function()
							{
								FocusCtrlCMRequest='txtReq';
							}
						}
					}
				]
			}
		]
	}
    return items;
};

function RefreshDataRequest() 
{
    dsTRRequestList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountRequest,
                Sort: 'req_date',
                //Sort: 'REQ_DATE',
                Sortdir: 'ASC',
                target:'ViewRequestCM',
                param : ''
            }		
        }
    );
	
    rowSelectedRequest = undefined;
    return dsTRRequestList;
};

function mComboMaksDataRequest() 
{
    var cboMaksDataRequest = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataRequest',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmMaksData + ' ',
		    width: 50,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountRequest,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountRequest = b.data.displayText;
			        RefreshDataRequestFilter();
			    }
			}
		}
	);
    return cboMaksDataRequest;
};

function RefreshDataRequestFilter() 
{

	var KataKunci='';
    if (Ext.get('txtReqIdViewRequestFilter').getValue() != '')
    { 
		//KataKunci = ' where req_id like ~%' + Ext.get('txtReqIdViewRequestFilter').getValue() + '%~'; 
		//KataKunci = "\"REQ_ID\" like ~%" + Ext.get('txtReqIdViewRequestFilter').getValue() + "%~";
                KataKunci = "req_id like ~%" + Ext.get('txtReqIdViewRequestFilter').getValue() + "%~";
	};
	
    if (Ext.get('cboStatusCMRequestView').getValue() != '' && selectStatusCMRequestView != ' 9999')
    { 
		
		if (KataKunci === '')
		{
			if (selectStatusCMRequestView === DefaultStatusAcceptedCMRequestView )
			{
				//KataKunci = ' where status_id =  ~' + selectStatusCMRequestView + '~ or status_id = ~' + DefaultStatusApproveCMRequestView + '~';
				//KataKunci = "\"STATUS_ID\" = ~" + selectStatusCMRequestView + "~ OR \"STATUS_ID\" = ~" + DefaultStatusApproveCMRequestView + "~";
                                KataKunci = "status_id = ~" + selectStatusCMRequestView + "~ OR status_id = ~" + DefaultStatusApproveCMRequestView + "~";

			}
			else
			{
				//KataKunci = ' where status_id =  ~' + selectStatusCMRequestView + '~';
				//KataKunci = "\"STATUS_ID\" = ~" + selectStatusCMRequestView + "~";
                                KataKunci = "status_id = ~" + selectStatusCMRequestView + "~";
			};
		}
		else
		{
			if (selectStatusCMRequestView === DefaultStatusAcceptedCMRequestView )
			{
				//KataKunci = ' and (status_id =  ~' + selectStatusCMRequestView + '~ or status_id = ~' + DefaultStatusApproveCMRequestView + '~)';
				//KataKunci = " AND (\"STATUS_ID\" = ~" + selectStatusCMRequestView + "~ OR \"STATUS_ID\" = ~" + DefaultStatusApproveCMRequestView + "~)";
                                KataKunci = " AND (status_id = ~" + selectStatusCMRequestView + "~ OR status_id = ~" + DefaultStatusApproveCMRequestView + "~)";
			}
			else
			{
				//KataKunci += ' and  status_id =  ~' + selectStatusCMRequestView + '~';
				//KataKunci += " AND \"STATUS_ID\" = ~" + selectStatusCMRequestView + "~";
                                KataKunci += " AND status_id = ~" + selectStatusCMRequestView + "~";
			};
		};  
	};
	
	if( Ext.get('chkWithTglRequest').dom.checked === true )
	{
			KataKunci += '@^@' + Ext.get('dtpTglAwalFilterRequest').getValue() + '&&'+ Ext.get('dtpTglAkhirFilterRequest').getValue();
	};
        
    if (KataKunci != undefined) 
    {  
		dsTRRequestList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountRequest, 
					//Sort: 'req_id',
                                        Sort: 'req_date',
					//Sort: 'REQ_ID',
					Sortdir: 'ASC', 
					target:'ViewRequestCM',
					param : KataKunci
				}			
			}
		);   
    }
	else
	{
		RefreshDataRequest();
	};
    
	return dsTRRequestList;
};



function RequestSave(mBol) 
{	
	if (ValidasiEntryCMRequest(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewRequest == true) 
		{
			Ext.Ajax.request
			(
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",					
					params: getParamRequest(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRequest(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataRequest();
							if(mBol === false)
							{
								Ext.get('txtNoRequest').dom.value=cst.ReqId;
								RefreshDataRequestDetail(Ext.get('txtNoRequest').dom.value);
							};
							AddNewRequest = false;
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRequest(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningRequest(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorRequest(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",
					params: getParamRequest(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRequest(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataRequest();
							if(mBol === false)
							{
								RefreshDataRequestDetail(Ext.get('txtNoRequest').dom.value);
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRequest(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningRequest(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorRequest(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};

function ValidasiEntryCMRequest(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtNoRequest').getValue() == '') || (Ext.get('txtKdRequesterRequest').getValue() == '') || (Ext.get('txtRequesterRequest').getValue() == '') || (Ext.get('cboDepartCMRequestEntry').getValue() == '') || (Ext.get('dtpTanggalRequest').getValue() == '') || dsDTLTRRequestList.getCount() === 0 || (selectDepartCMRequestEntry === undefined ))
	{
		if (Ext.get('txtNoRequest').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('txtKdRequesterRequest').getValue() == '') 
		{
			ShowPesanWarningRequest(nmGetValidasiKosong(nmRequestId), modul);
			x = 0;
		}
		else if (Ext.get('txtRequesterRequest').getValue() == '') 
		{
			ShowPesanWarningRequest(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}
		else if (Ext.get('dtpTanggalRequest').getValue() == '') 
		{
			ShowPesanWarningRequest(nmGetValidasiKosong(nmRequestDate), modul);
			x = 0;
		}
		else if (Ext.get('cboDepartCMRequestEntry').getValue() == '' || selectDepartCMRequestEntry === undefined) 
		{
			ShowPesanWarningRequest(nmGetValidasiKosong(nmDeptRequest), modul);
			x = 0;
		}
		else if (dsDTLTRRequestList.getCount() === 0) 
		{
			ShowPesanWarningRequest(nmGetValidasiKosong(nmTitleDetailFormRequest),modul);
			x = 0;
		};
	};
	return x;
};


function ShowPesanWarningRequest(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorRequest(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoRequest(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function RequestDelete() 
{
   if (ValidasiEntryCMRequest(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                //url: "./Datapool.mvc/DeleteDataObj",
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamRequest(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoRequest(nmPesanHapusSukses,nmHeaderHapusData);
                                        RefreshDataRequest();
                                        RequestAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningRequest(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningRequest(nmPesanHapusGagal + ' , ' + nmKonfirmasiRejected ,nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorRequest(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};


function mComboDepartCMRequestEntry() 
{
    var Field = ['DEPT_ID','DEPT_NAME'];
	
    dsDepartCMRequestEntry = new WebApp.DataStore({ fields: Field });
    dsDepartCMRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'dept_id',
			    Sortdir: 'ASC',
			    target: 'ViewComboDept',
			    param: ''
			}
		}
	);

    var cboDepartCMRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDepartCMRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: nmPilihDept,
		    fieldLabel: nmDeptRequest + ' ',
		    align: 'Right',
		    anchor:'60%',
		    store: dsDepartCMRequestEntry,
		    valueField: 'DEPT_ID',
		    displayField: 'DEPT_NAME',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectDepartCMRequestEntry = b.data.DEPT_ID;
			    }
			}
		}
	);

    return cboDepartCMRequestEntry;
};



function mComboStatusCMRequestView() 
{
	var Field = ['STATUS_ID','STATUS_NAME'];
	
    dsStatusCMRequestView = new WebApp.DataStore({ fields: Field });
    dsStatusCMRequestView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ID',
                            Sort: 'status_id',
			    Sortdir: 'ASC',
			    target: 'ViewComboStatusVw',
			    //param: 'where status_id in ' + DefaultValueStatusCMRequestView
			    //param: "\"STATUS_ID\" IN ( ~" + DefaultValueStatusCMRequestView+"~ )"
			    //param: "\"STATUS_ID\" IN " + DefaultValueStatusCMRequestView //+"~ )"
                            param: "status_id IN " + DefaultValueStatusCMRequestView //+"~ )"
			}
		}
	);
	
    var cboStatusCMRequestView = new Ext.form.ComboBox
	(
		{
		    id: 'cboStatusCMRequestView',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmStatusRequest + ' ',
		    align: 'Right',
		    anchor:'60%',
		    store: dsStatusCMRequestView,
		    valueField: 'STATUS_ID',
		    displayField: 'STATUS_NAME',
			value:valueStatusCMRequestView,
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectStatusCMRequestView = b.data.STATUS_ID;
			    }
			}
		}
	);
	
    return cboStatusCMRequestView;
};

