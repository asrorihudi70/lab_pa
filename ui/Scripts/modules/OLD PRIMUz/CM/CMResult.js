var CurrentServiceCMResult=
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPersonIntCMResult=
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPersonExtCMResult=
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPartCMResult=
{
    data: Object,
    details: Array,
    row: 0
};



// var select & value combo ==============================
var selectStatusCMResultEntry;
var selectCategoryCMResultView=' 9999';
var valueCategoryCMResultView=' All';
var selectStatusAssetCMResult;
//========================================================

// var select grid ==============================
var rowSelectedCMResult;
var rowSelectedCMResultTemp;
var selectCountCMResult = 50;
var cellSelectServiceCMResult;
var cellSelectPersonIntCMResult;
var cellSelectPersonExtCMResult;
var cellSelectPartCMResult;
//================================================


// var data store ===========================
var dsTRCMResultList;
var dsDtlCraftPersonIntCMResult;
var dsDtlCraftPersonIntCMResultTemp;
var dsDtlCraftPersonExtCMResult;
var dsDtlCraftPersonExtCMResultTemp;
var dsDtlPartCMResult;
var dsDtlPartCMResultTemp;
var dsDtlServiceCMResult;
var dsLookVendorListCMResult;

//===========================================



//var component===========================
var TRCMResultLookUps;
var vTabPanelRepairCMResult ;
var vTabPanelRepairExternalCMResult ;
//=========================================



var NowCMResult = new Date();
var AddNewCMResult = true;
var IdCMWorkOrderCMResult;
var IdCMResult;
var IdAppCMResult;
var IsExtRepairCMResult;
var FocusCtrlCMResult;
var varTabRepairCMResult=1;
var varCatIdCMResult;
var SchIdCMResult;

var StrTreeComboCMResultView;
var rootTreeCMResultView;
var treeCMResultView;
var IdCategoryCMResultView=' 9999';


var mRecordServiceCMResult = Ext.data.Record.create
(
    [
       {name: 'RESULT_CM_ID', mapping:'RESULT_CM_ID'},
       {name: 'SCH_CM_ID', mapping:'SCH_CM_ID'},
       {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
       {name: 'SERVICE_NAME', mapping:'SERVICE_NAME'},
       {name: 'CATEGORY_ID', mapping:'CATEGORY_ID'},
       {name: 'TAG', mapping:'TAG'}
    ]
)

var mRecordPartCMResult = Ext.data.Record.create
(
    [
       {name: 'RESULT_CM_ID', mapping:'RESULT_CM_ID'},
       {name: 'SCH_CM_ID', mapping:'SCH_CM_ID'},
       {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
       {name: 'PART_ID', mapping:'PART_ID'},
       {name: 'PART_NAME', mapping:'PART_NAME'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'UNIT_COST', mapping:'UNIT_COST'},
       {name: 'TOT_COST', mapping:'TOT_COST'},
       {name: 'DESC_SCH_PART', mapping:'DESC_SCH_PART'},
       {name: 'TAG', mapping:'TAG'},
       {name: 'ROW', mapping:'ROW'}
    ]
);
	
var mRecordPersonCMResult = Ext.data.Record.create
(
    [
       {name: 'RESULT_CM_ID', mapping:'RESULT_CM_ID'},
       {name: 'SCH_CM_ID', mapping:'SCH_CM_ID'},
       {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
       {name: 'ROW_SCH', mapping:'ROW_SCH'},
       {name: 'EMP_ID', mapping:'EMP_ID'},
       {name: 'EMP_NAME', mapping:'EMP_NAME'},
       {name: 'VENDOR_ID', mapping:'VENDOR_ID'},
       {name: 'VENDOR', mapping:'VENDOR'},
       {name: 'PERSON_NAME', mapping:'PERSON_NAME'},
       {name: 'COST', mapping:'COST'},
       {name: 'REAL_COST', mapping:'REAL_COST'},
       {name: 'DESC_SCH_PERSON', mapping:'DESC_SCH_PERSON'},
       {name: 'ROW', mapping:'ROW'},
       {name: 'ROW_RSLT', mapping:'ROW_RSLT'}
    ]
);


CurrentPage.page = getPanelCMResult(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelCMResult(mod_id) 
{
    var Field =['RESULT_CM_ID','WO_CM_ID','FINISH_DATE','DIFF_FINISH_DATE','LAST_COST','DIFF_COST','DESC_RESULT_CM','WO_ID','PIC_ID','PIC_NAME','SCH_CM_ID','STATUS_ID','WO_CM_DATE','WO_CM_FINISH_DATE','DESC_WO_CM','ASSET_MAINT_ID','ASSET_MAINT_NAME','ASSET_MAINT','SCH_DUE_DATE','SCH_FINISH_DATE','IS_EXT_REPAIR','CATEGORY_ID','APP_ID','COST','APP_FINISH_DATE','REFERENCE',
	'STATUS_ASSET_ID','STATUS_ASSET'];
	
    dsTRCMResultList = new WebApp.DataStore({ fields: Field });
    GetStrTreeComboCMResultView();

    var chkInternalCMResult = new Ext.grid.CheckColumn
    (
        {
            id: 'chkInternalCMResult',
            header: nmRepairExtResultCM,
            align: 'center',
            disabled:true,
            dataIndex: 'IS_EXT_REPAIR',
            width: 70
        }
    );

    var grListTRCMResult = new Ext.grid.EditorGridPanel
	(
		{
		    stripeRows: true,
		    store: dsTRCMResultList,
		    anchor: '100% 91.9999%',
		    columnLines: true,
			autoScroll:true,
		    border: false,
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
					        rowSelectedCMResult = dsTRCMResultList.getAt(row);
					    }
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedCMResult = dsTRCMResultList.getAt(ridx);
					if (rowSelectedCMResult != undefined) 
					{
						CMResultLookUp(rowSelectedCMResult.data);
					}
					else 
					{
						ShowPesanWarningCMResult(nmWarningSelectEditData,nmEditData)
					};
				}
			},
		    cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'colWOImageCMResultView',
					    header: nmColResultCM,
					    dataIndex: 'RESULT_CM_ID',
					    sortable: true,
					    width: 50,
                                            align:'center',
                                            renderer: function(value, metaData, record, rowIndex, colIndex, store)
                                            {
                                                if ( value === null )
                                                {
                                                    metaData.css = 'BeforeResult';
                                                }
                                                else
                                                {
                                                    metaData.css = 'Result';
                                                };
                                                return '';
                                            }
					},
					{
					    id: 'colWOIdViewCMResult', 
					    header: nmColWOIDResultCM,
					    dataIndex: 'WO_ID',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'colWODateViewCMResult',
					    header: nmColWODateResultCM,
					    dataIndex: 'WO_CM_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.WO_CM_DATE);
					    }
					},
					{
					    id: 'colWODateViewCMResult', //+ Code
					    header: nmColAssetIdNameResultCM,
					    dataIndex: 'ASSET_MAINT',
					    sortable: true,
					    width: 250
					},
					{
					    id: 'colStartDateViewCMResult',
					    header: nmColSchStartDateResultCM,
					    dataIndex: 'SCH_DUE_DATE',
					    sortable: true,
					    width: 140,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.SCH_DUE_DATE);
					    }
					},
					{
					    id: 'colFinishDateViewCMResult',
					    header: nmColSchFinishDateResultCM,
					    dataIndex: 'SCH_FINISH_DATE',
					    sortable: true,
					    width: 140,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.SCH_FINISH_DATE);
					    }
					},
					{
					    id: 'colFinishResultDateViewCMResult',
					    header: nmColRsltFinishDateResultCM,
					    dataIndex: 'FINISH_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.FINISH_DATE);
					    }
					},
					{
					    id: 'colDescViewCMResult',
					    header: nmColRsltDescResultCM,
					    dataIndex: 'DESC_RESULT_CM',
					    width: 300
					},chkInternalCMResult
				]
			),
		    tbar:
			[
				{
				    id: 'btnEditCMResult',
				    text: nmEditData,
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedCMResult != undefined) 
						{
							CMResultLookUp(rowSelectedCMResult.data);
						}
						else 
						{
							ShowPesanWarningCMResult(nmWarningSelectEditData,nmEditData)
						}
				    }
				}, ' ', '-',
				{
					xtype: 'checkbox',
					id: 'chkWithTglCMResult',
					hideLabel:true,
					checked: false,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilterCMResult').dom.disabled=false;
							Ext.get('dtpTglAkhirFilterCMResult').dom.disabled=false;
							
						}
						else
						{
							Ext.get('dtpTglAwalFilterCMResult').dom.disabled=true;
							Ext.get('dtpTglAkhirFilterCMResult').dom.disabled=true;
							
							
						};
					}
				}
				, ' ', '-', nmWODateResultCM + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmWODateResultCM + ' : ',
				    id: 'dtpTglAwalFilterCMResult',
				    format: 'd/M/Y',
				    value: NowCMResult,
				    width: 100,
				    onInit: function() { }
				}, ' ', ' ' + nmSd + ' ', ' ', {
				    xtype: 'datefield',
				    fieldLabel:  nmSd + ' ',
				    id: 'dtpTglAkhirFilterCMResult',
				    format: 'd/M/Y',
				    value: NowCMResult,
				    width: 100
				}
			]
		}
	);
	
	var LegendViewCMResult = new Ext.Panel
	(
		{
		    id: 'LegendViewCMResult',
		    region: 'center',
			border:false,
			bodyStyle: 'padding:0px 7px 0px 7px',
		    layout: 'column',
			frame:true,
			//height:30,
			anchor: '100% 8.0001%',
			autoScroll:false,
			items:
			[	
				{
					columnWidth: .15,
					layout: 'form',
					//height:30,
					anchor: '100% 8.0001%',
					border: false,
					html: '<img src="'+baseURL+'ui/images/icons/16x16/Before.png" class="text-desc-legend"/>' + " " + " Work Order"
				},
				{
					columnWidth: .2,
					layout: 'form',
					border: false,
					//height:30,
					anchor: '100% 8.0001%',
					html: '<img src="'+baseURL+'ui/images/icons/16x16/After.png" class="text-desc-legend"/>' + " " + " Job Close"
				}
			]
		
		}
	)

    var FormTRCMResult = new Ext.Panel
	(
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormResultCM,
		    border: false,
		    shadhow: true,
			autoScroll:false,
		    iconCls: 'CMResult',
		    margins: '0 5 5 0',
		    items: [grListTRCMResult,LegendViewCMResult],
		    tbar:
			[
				nmCatResultCM + ' : ', ' ',mComboCategoryCMResultView(), 
				' ', '-',
					nmMaksData + ' : ', ' ', mComboMaksDataCMResult(),
					' ', '-',
				{
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    anchor: '25%',
				    handler: function(sm, row, rec) 
					{
				        RefreshDataCMResultFilter();
				    }
				}
			],
		    listeners:
			{
			    'afterrender': function() 
				{
			    }
			}
		}
	);
    RefreshDataCMResult();
    RefreshDataCMResult();
	
    return FormTRCMResult

};

function RefreshDataCMResult() 
{
    dsTRCMResultList.load
    (
        {
            params:
                {
                    Skip: 0,
                    Take: selectCountCMResult,
                    //Sort: 'Result_CM_ID',
                    Sort: 'result_cm_id',
                    Sortdir: 'ASC',
                    target: 'ViewResultCM',
                    param: ''
                }
        }
    );
    return dsTRCMResultList;
};

function RefreshDataCMResultFilter() 
{
    var KataKunci='';
	
	if (Ext.get('cboCategoryCMResultView').getValue() != '' && selectCategoryCMResultView != undefined)
    { 
		if (selectCategoryCMResultView.id != undefined)
		{
			if(selectCategoryCMResultView.id != ' 9999')
			{
				if (selectCategoryCMResultView.leaf === false)
				{
					//KataKunci =' where left(category_id,' + selectCategoryCMResultView.id.length + ')=' +  selectCategoryCMResultView.id
                                        //substring(am_request_cm.req_id, 7, 4)
                                        KataKunci =' substring( category_id,1,' + selectCategoryCMResultView.id.length + ') = ' +  selectCategoryCMResultView.id
				}
				else
				{
					//KataKunci =' WHERE Category_id=~'+ selectCategoryCMResultView.id + '~'
                                        KataKunci =' category_id = ~'+ selectCategoryCMResultView.id + '~'
				};
			};
		};
	};
	
    // if (Ext.get('cboCategoryCMResultView').getValue() != '' && selectCategoryCMResultView != undefined)
    // { 
		// if(selectCategoryCMResultView != ' 9999')
		// {
			// KataKunci = ' where category_id = ~' + selectCategoryCMResultView + '~';
		// };
	// };

	
	if( Ext.get('chkWithTglCMResult').dom.checked === true )
	{
			KataKunci += '@^@' + Ext.get('dtpTglAwalFilterCMResult').getValue() + '&&'+ Ext.get('dtpTglAkhirFilterCMResult').getValue();
	};
        
    if (KataKunci != undefined) 
    {  
		dsTRCMResultList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountCMResult,
					//Sort: 'Result_CM_ID',
                                        Sort: 'result_cm_id',
					Sortdir: 'ASC',
					target: 'ViewResultCM',
					param: KataKunci
				}
			}
		);
    }
	else
	{
		RefreshDataCMResult();
	};
    
	return dsTRCMResultList;
    
};



function CMResultLookUp(rowdata) 
{
    var lebar = 800; //735;
	
    IdCMWorkOrderCMResult='';
    IdCMResult='';
    IsExtRepairCMResult='';
    SchIdCMResult='';
    varTabRepairCMResult=1;
    varCatIdCMResult='';
    TRCMResultLookUps='';
    cellSelectServiceCMResult='';
    cellSelectPersonIntCMResult='';
    cellSelectPersonExtCMResult='';
    cellSelectPartCMResult='';

    GetStrTreeComboCMResultView();
    TRCMResultLookUps = new Ext.Window
    (
        {
            id: 'TRCMResultLookUps',
            title: nmTitleFormResultCM,
            closeAction: 'destroy',
            width: lebar,
            height: 560,//540,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'CMResult',
            modal: true,
            items: getFormEntryTRCMResult(lebar),
            listeners:
            {
                activate: function()
                {
                    if (varBtnOkLookupEmp === true)
                    {
                        var j=dsDtlCraftPersonIntCMResult.getCount()-1;
                         dsDtlCraftPersonIntCMResultTemp.insert(dsDtlCraftPersonIntCMResultTemp.getCount(),dsDtlCraftPersonIntCMResult.data.items[j]);
                         GetPersonIntServiceCMResult(cellSelectServiceCMResult.data.SERVICE_ID);
                         varBtnOkLookupEmp=false;
                    };
					
					 // if (varBtnOkLookupVendor === true)
					// {
						// var j=dsDtlCraftPersonExtCMResult.getCount()-1;
						 // dsDtlCraftPersonExtCMResultTemp.insert(dsDtlCraftPersonExtCMResultTemp.getCount(), dsDtlCraftPersonExtCMResult.data.items[j]);
						  // GetPersonExtServiceCMResult(cellSelectServiceCMResult.data.SERVICE_ID);
						 // varBtnOkLookupVendor=false;
					// };
					
					 if (varBtnOkLookupPart === true)
					{
						var j=dsDtlPartCMResult.getCount()-1;
						 dsDtlPartCMResultTemp.insert(dsDtlPartCMResultTemp.getCount(), dsDtlPartCMResult.data.items[j]);
						  GetPartCMResult(cellSelectServiceCMResult.data.SERVICE_ID);
						 CalcTotalPartCMResult('','',false)
						 varBtnOkLookupPart=false;
					};
			    },
			    afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedCMResult=undefined;
					RefreshDataCMResultFilter();
				}
			}
		}
	);

    TRCMResultLookUps.show();
    if (rowdata == undefined) 
	{
        //CMResultAddNew();
    }
    else 
	{
        TRCMResultInit(rowdata)
    }
};

function getFormEntryTRCMResult(lebar) 
{
    var pnlTRCMResult = new Ext.FormPanel
	(
		{
		    id: 'PanelTRCMResult',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    bodyStyle: 'padding:10px 10px 0px 10px',
                    height:538,//518,
		    anchor: '100%',
		    width: lebar,
		    border: false,
			//autoScroll:true,
		    items: [getItemPanelInputCMResult(lebar)],
		    tbar:
			[
				{
				    text: nmTambah,
				    id: 'btnTambahCMResult',
				    tooltip: nmTambah,
				    iconCls: 'add',
					disabled:true,
				    handler: function() 
					{
				        //CMResultAddNew();
				    }
				}, '-',
				{
				    text: nmSimpan,
				    id: 'btnSimpanCMResult',
				    tooltip: nmSimpan,
				    iconCls: 'save',
				    handler: function() 
					{
				        CMResultSave(false);
				        RefreshDataCMResultFilter();
				    }
				}, '-',
				{
				    text: nmSimpanKeluar,
				    id: 'btnSimpanKeluarCMResult',
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{
						var x =  CMResultSave(true);
				         RefreshDataCMResultFilter();
						if (x===undefined)
						{
							TRCMResultLookUps.close();
						};    
				    }
				}, '-',
				{
				    text: nmHapus,
				    id: 'btnHapusCMResult',
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
				        CMResultDelete();
				        RefreshDataCMResultFilter();
				    }
				}, '-',
				{
				    text: nmLookup,
				    id: 'btnLookupCMResult',
				    tooltip: nmLookup,
				    iconCls: 'find',
				    handler: function() 
					{
						if (FocusCtrlCMResult === 'colVendorRdo' && varTabRepairCMResult === 2 && cellSelectServiceCMResult != '' &&  cellSelectServiceCMResult != undefined)
						{	
							var p = GetRecordBaruPersonCMResult(dsDtlCraftPersonExtCMResult);
							FormLookupVendor(p,'','',true,dsDtlCraftPersonExtCMResult,true);
						}
						else if (FocusCtrlCMResult === 'colEmpRdo' && varTabRepairCMResult === 2 && cellSelectServiceCMResult != '' &&  cellSelectServiceCMResult != undefined)
						{	
							var p = GetRecordBaruPersonCMResult(dsDtlCraftPersonExtCMResult);
							FormLookupVendor(p,'','',true,dsDtlCraftPersonExtCMResult,true);
						}
						else if (FocusCtrlCMResult === 'colEmp')
						{
							var p = GetRecordBaruPersonCMResult(dsDtlCraftPersonIntCMResult);
							FormLookupEmployee('','','',true,p,dsDtlCraftPersonIntCMResult,true);
							FocusCtrlCMResult='';
						}
						else if ((FocusCtrlCMResult === 'colPart' || varTabRepairCMResult === 3 ) && FocusCtrlCMResult != 'txtPIC')
						{
							var p = GetRecordBaruPartCMResult();
							//var	str = ' where category_id = ~' + varCatIdCMResult + '~'
                                                        var	str = 'category_id = ~' + varCatIdCMResult + '~'
							var strList=GetStrListPartCMResult(true);
							if (strList !='')
							{
								//str += ' and PART_ID not in ( ' + strList + ') '
                                                                str += ' and part_id not in ( ' + strList + ') '
							};
							FormLookupPart(str,dsDtlPartCMResult,p,true,'',true);
							FocusCtrlCMResult='';
						}
						else if ((FocusCtrlCMResult === 'colService' || varTabRepairCMResult === 1 ) && FocusCtrlCMResult != 'txtPIC')
						{
							var p = GetRecordBaruServiceCMResult();
							var str = '';
							var strList = GetStrListServiceCMResult();
							
							if (strList !='')
							{
								//strList = ' and SERVICE_ID not in ( ' + strList + ') '
                                                                strList = ' and service_id not in ( ' + strList + ') '
							};
							//str = ' where category_id = ~' + varCatIdCMResult + '~';
                                                        str = 'category_id = ~' + varCatIdCMResult + '~';
							str += strList;
							
							FormLookupServiceCategory('','',str,true,p,dsDtlServiceCMResult,true);
							FocusCtrlCMResult='';
						};
				    }
				}, 
				'-', '->', '-',
				{
				    text: nmCetak,
					id:'btnCetakCMResult',
				    tooltip: nmCetak,
				    iconCls: 'print',
				    handler: function()
					{ 
						var cKriteria;
						cKriteria = Ext.get('txtKdMainAssetCMResult').dom.value + ' - ' + Ext.get('txtNamaMainAsetCMResult').dom.value + '###1###';
						cKriteria += Ext.get('txtNamaSupervisorCMResult').dom.value + '###2###';
						cKriteria += Ext.get('dtpStartCMResult').dom.value + '###3###';
						cKriteria += Ext.get('dtpFinishCMResult').dom.value + '###4###';
						cKriteria += IdCMResult + '###5###';
						cKriteria += Ext.get('dtpCMResult').dom.value + '###6###';
						cKriteria += Ext.get('dtpWOFinishCMResult').dom.value + '###7###';
						cKriteria += Ext.get('txtResultDescCMResult').dom.value + '###8###';
						cKriteria += Ext.get('dtpResultCMResult').dom.value + '###9###';
						cKriteria += GetNilaiCurrencyCMResult(Ext.get('txtLastCostCMResult').dom.value) + '###10###';
						ShowReport('', '920010', cKriteria);
					}
				}
			]
		}
	); 
	

    var FormTRCMResult = new Ext.Panel
	(
		{
		    id: 'FormTRCMResult',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRCMResult]
		}
	);

    return FormTRCMResult
};
///---------------------------------------------------------------------------------------///

function CetakCMResult() 
{
  
};

	
function GetStrListPartCMResult(mBolLookup)
{
	var str='';
	
	for (var i = 0; i < dsDtlPartCMResult.getCount() ; i++) 
	{
		// if (dsDtlPartCMResult.data.items[i].data.PART_ID != '')
		// {
			// if (mBolLookup === true)
			// {
				if(dsDtlPartCMResult.getCount() === 1)
				{
					str +=  '\'' + dsDtlPartCMResult.data.items[i].data.PART_ID + '\'';
				}
				else
				{
					if (i === dsDtlPartCMResult.getCount()-1 )
					{
						str +=  '\'' + dsDtlPartCMResult.data.items[i].data.PART_ID + '\'';
					}
					else
					{
						str +=  '\'' + dsDtlPartCMResult.data.items[i].data.PART_ID + '\'' + ',' ;
					};
				};
			// }
			// else
			// {
				// if (i === dsDtlPartCMResult.getCount()-2 || dsDtlPartCMResult.getCount() === 1)
				// {
					// str +=  '\'' + dsDtlPartCMResult.data.items[i].data.PART_ID + '\'';
				// }
				// else
				// {
					// str +=  '\'' + dsDtlPartCMResult.data.items[i].data.PART_ID + '\'' + ',' ;
				// };
			// };
		// };
	}
	
	return str;
};

function TRCMResultInit(rowdata)
{
    AddNewCMResult = false;
	rowSelectedCMResultTemp=rowSelectedCMResult;
    Ext.get('txtKdMainAssetCMResult').dom.value=rowdata.ASSET_MAINT_ID;
	Ext.get('txtNamaMainAsetCMResult').dom.value=rowdata.ASSET_MAINT_NAME;
	Ext.get('dtpStartCMResult').dom.value=ShowDate(rowdata.SCH_DUE_DATE);
	Ext.get('dtpFinishCMResult').dom.value=ShowDate(rowdata.SCH_FINISH_DATE);
	Ext.get('dtpCMResult').dom.value=ShowDate(rowdata.WO_CM_DATE);
	Ext.get('dtpWOFinishCMResult').dom.value=ShowDate(rowdata.WO_CM_FINISH_DATE);
	Ext.get('txtKdSupervisorCMResult').dom.value=rowdata.PIC_ID;
	Ext.get('txtNamaSupervisorCMResult').dom.value=rowdata.PIC_NAME;
	Ext.get('cboStatusAssetCMResult').dom.value=rowdata.STATUS_ASSET;
	selectStatusAssetCMResult=rowdata.STATUS_ASSET_ID;
	
	IsExtRepairCMResult = rowdata.IS_EXT_REPAIR;
	IdCMWorkOrderCMResult=rowdata.WO_ID;
	IdCMResult=rowdata.RESULT_CM_ID;
	IdAppCMResult=rowdata.APP_ID;
	SchIdCMResult=rowdata.SCH_CM_ID;
	varCatIdCMResult=rowdata.CATEGORY_ID;
	
	if (rowdata.DESC_WO_CM != null)
	{
		Ext.get('txtDeskripsiCMResult').dom.value=rowdata.DESC_WO_CM;
	}
	else
	{
		Ext.get('txtDeskripsiCMResult').dom.value='';
	};
	
	
	if (rowdata.RESULT_CM_ID != null)
	{
		IdCMResult=rowdata.RESULT_CM_ID;
		Ext.get('dtpResultCMResult').dom.value=ShowDate(rowdata.FINISH_DATE);
		Ext.get('txtLastCostCMResult').dom.value=formatCurrency(rowdata.LAST_COST);
		
		if (rowdata.DESC_RESULT_CM != null)
		{
			Ext.get('txtResultDescCMResult').dom.value=rowdata.DESC_RESULT_CM;
		}
		else
		{
			Ext.get('txtResultDescCMResult').dom.value='';
		};
		
		if (rowdata.REFERENCE != null)
		{
			Ext.get('txtReffCMResult').dom.value=rowdata.REFERENCE;
		}
		else
		{
			Ext.get('txtReffCMResult').dom.value='';
		};
		
		LoadDataServiceResultCMResult(IdCMResult,rowdata.CATEGORY_ID);
		LoadDataPartResultCMResult(IdCMResult,varCatIdCMResult);
		GetJumlahTotalResultPersonPartCMResult();
		if (rowdata.IS_EXT_REPAIR === false)
		{
			LoadDataEmployeeResultCMResult(IdCMResult,varCatIdCMResult)
		}
		else
		{
			LoadDataVendorResultCMResult(IdCMResult,varCatIdCMResult);
			GetVendorCMResult(false);
		};
		GetJumlahTotalPersonPartCMResult(false);
	}
	else
	{
		IdCMResult='';
		GetJumlahTotalPersonPartCMResult(true);
		Ext.get('dtpResultCMResult').dom.value=ShowDate(rowdata.WO_CM_FINISH_DATE);
		LoadDataServiceCMResult(rowdata.SCH_CM_ID);
		LoadDataPartCMResult(SchIdCMResult,varCatIdCMResult);
		if (rowdata.IS_EXT_REPAIR === false)
		{
			LoadDataEmployeeCMResult(SchIdCMResult,varCatIdCMResult)
		}
		else
		{
			LoadDataVendorCMResult(SchIdCMResult,varCatIdCMResult)
			GetVendorCMResult(true);
		};
	};
	
};

function LoadDataServiceCMResult(SchId) 
{
    dsDtlServiceCMResult.load
    (
        {
            params:
                {
                    Skip: 0,
                    Take: 1000,
                    //Sort: 'SERVICE_ID',
                    Sort: 'service_id',
                    Sortdir: 'ASC',
                    target: 'ViewSchServiceCM',
                    //param: 'where sch_cm_id =~' + SchId + '~'
                    param: 'sch_cm_id = ~' + SchId + '~'
                }
        }
    );
    return dsDtlServiceCMResult;
};

function LoadDataServiceResultCMResult(ResultId,CatId) 
{
    dsDtlServiceCMResult.load
    (
        {
            params:
                {
                    Skip: 0,
                    Take: 1000,
                    //Sort: 'SERVICE_ID',
                    Sort: 'service_id',
                    Sortdir: 'ASC',
                    target: 'ViewResultServiceCM',
                    //param: 'where result_cm_id =~' + ResultId + '~ and category_id= ~' + CatId + '~'
                    param: 'result_cm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~'
                }
        }
    );
    return dsDtlServiceCMResult;
};



function GetNilaiCurrencyCMResult(dblNilai)
{
	for (var i = 0; i < dblNilai.length; i++) 
	{
		var y = dblNilai.substr(i, 1)
		if (y === '.') 
		{
			dblNilai = dblNilai.replace('.', '');
		}
	};
	
	return dblNilai;
};

function LoadDataEmployeeCMResult(SchId,CatId) 
{
    dsDtlCraftPersonIntCMResultTemp.load
    (
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        //Sort: 'EMP_ID',
                        Sort: 'emp_id',
                        Sortdir: 'ASC',
                        target: 'ViewSchPersonCM',
                        //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~'
                        param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
                    }
            }
    );
    return dsDtlCraftPersonIntCMResultTemp;
};

function LoadDataEmployeeResultCMResult(ResultId,CatId) 
{
    dsDtlCraftPersonIntCMResultTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'EMP_ID',
                            Sort: 'row_rslt',
			    Sortdir: 'ASC',
			    target: 'ViewResultPersonCM',
			   //param: 'where result_cm_id =~' + ResultId + '~ and category_id= ~' + CatId + '~'
                           param: 'result_cm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlCraftPersonIntCMResultTemp;
};

function ReLoadDataEmployeeResultCMResult(ResultId,CatId,ServId) 
{
    dsDtlCraftPersonIntCMResult.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'EMP_ID',
                            Sort: 'row_rslt',
			    Sortdir: 'ASC',
			    target: 'ViewResultPersonCM',
			   //param: 'where result_cm_id =~' + ResultId + '~ and category_id= ~' + CatId + '~ and service_id = ~' + ServId + '~'
                           param: 'result_cm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~ and service_id = ~' + ServId + '~'
			}
		}
	);
    return dsDtlCraftPersonIntCMResult;
};

function LoadDataVendorCMResult(SchId,CatId) 
{
    dsDtlCraftPersonExtCMResultTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'VENDOR_ID',
                            Sort: 'vendor_id',
			    Sortdir: 'ASC',
			    target: 'ViewSchPersonCM',
			     //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~'
                             param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlCraftPersonExtCMResultTemp;
};

function LoadDataVendorResultCMResult(ResultId,CatId) 
{
    dsDtlCraftPersonExtCMResultTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'VENDOR_ID',
                            Sort: 'vendor_id',
			    Sortdir: 'ASC',
			    target: 'ViewResultPersonCM',
			   //param: 'where result_cm_id =~' + ResultId + '~ and category_id= ~' + CatId + '~'
                           param: 'result_cm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlCraftPersonExtCMResultTemp;
};

function ReLoadDataVendorResultCMResult(ResultId,CatId,ServId) 
{
    dsDtlCraftPersonExtCMResult.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'VENDOR_ID',
                            Sort: 'vendor_id',
			    Sortdir: 'ASC',
			    target: 'ViewResultPersonCM',
			   //param: 'where result_cm_id =~' + ResultId + '~ and category_id= ~' + CatId + '~ and service_id = ~' + ServId + '~'
                           param: 'result_cm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~ and service_id = ~' + ServId + '~'
			}
		}
	);
    return dsDtlCraftPersonExtCMResult;
};


function LoadDataPartCMResult(SchId,CatId) 
{
    dsDtlPartCMResultTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'PART_ID',
                            Sort: 'part_id',
			    Sortdir: 'ASC',
			    target: 'ViewSchPartCM',
			    //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~'
                            param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlPartCMResultTemp;
};

function LoadDataPartResultCMResult(ResultId,CatId) 
{
    dsDtlPartCMResultTemp.load
    (
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        //Sort: 'PART_ID',
                        Sort: 'part_id',
                        Sortdir: 'ASC',
                        target: 'ViewResultPartCM',
                       //param: 'where result_cm_id =~' + ResultId + '~ and category_id= ~' + CatId + '~'
                       param: 'result_cm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~'
                    }
            }
    );
    return dsDtlPartCMResultTemp;
};

function ReLoadDataPartResultCMResult(ResultId,CatId,ServId) 
{
    dsDtlPartCMResult.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'PART_ID',
                Sort: 'part_id',
                Sortdir: 'ASC',
                target: 'ViewResultPartCM',
               //param: 'where result_cm_id =~' + ResultId + '~ and category_id= ~' + CatId + '~ and service_id = ~' + ServId + '~'
               param: 'result_cm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~ and service_id = ~' + ServId + '~'
            }
        }
    );
    return dsDtlPartCMResult;
};

///---------------------------------------------------------------------------------------///
function CMResultAddNew() 
{
    AddNewCMResult = true; 
	
	IdCMResult='';
	Ext.get('dtpResultCMResult').dom.value=ShowDate(rowSelectedCMResultTemp.data.WO_CM_FINISH_DATE);
	GetJumlahTotalPersonPartCMResult(true);
	Ext.get('txtResultDescCMResult').dom.value='';
	Ext.get('txtReffCMResult').dom.value='';
	dsDtlServiceCMResult.removeAll();
	LoadDataServiceCMResult(SchIdCMResult);
	
	if (IsExtRepairCMResult === false)
	{
		dsDtlCraftPersonIntCMResult.removeAll();
		dsDtlCraftPersonIntCMResultTemp.removeAll();
		LoadDataEmployeeCMResult(SchIdCMResult,varCatIdCMResult)
	}
	else
	{
		dsDtlCraftPersonExtCMResult.removeAll();
		dsDtlCraftPersonExtCMResultTemp.removeAll();
		LoadDataVendorCMResult(SchIdCMResult,varCatIdCMResult)
	};
	
	dsDtlPartCMResult.removeAll();
	dsDtlPartCMResultTemp.removeAll();
	LoadDataPartCMResult(SchIdCMResult,varCatIdCMResult);
};
///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamCMResult() 
{
    var params =
    {
        Table:'ViewResultCM',
        ResultId: IdCMResult,
        WOId: IdCMWorkOrderCMResult,
        AppId:IdAppCMResult,
        CatId:varCatIdCMResult,
        FinishDate: Ext.get('dtpResultCMResult').dom.value,
        Cost: GetNilaiCurrencyCMResult(Ext.get('txtLastCostCMResult').dom.value),
        Ref:Ext.get('txtReffCMResult').dom.value,
        Desc:Ext.get('txtResultDescCMResult').dom.value,
        ListService:getArrDetailServiceCMResult(),
        AssetID:Ext.get('txtKdMainAssetCMResult').dom.value,
        StatusAsset:selectStatusAssetCMResult,
        ListPerson:getArrDetailPersonCMResult(),
        ListPart:getArrDetailPartCMResult(),
        JmlFieldService: mRecordServiceCMResult.prototype.fields.length-3,
        JmlFieldPerson: mRecordPersonCMResult.prototype.fields.length-3,
        JmlFieldPart: mRecordPartCMResult.prototype.fields.length-4,
        JmlListService:GetListCountDetailServiceCMResult(),
        JmlListPerson:GetListCountDetailPersonCMResult(),
        JmlListPart:GetListCountDetailPartCMResult(),
        Hapus:1,
        Service:0
    };
    return params;
};

function getArrDetailServiceCMResult()
{
    var x='';
    for(var i = 0 ; i < dsDtlServiceCMResult.getCount();i++)
    {
        if (dsDtlServiceCMResult.data.items[i].data.SERVICE_ID != '' && dsDtlServiceCMResult.data.items[i].data.SERVICE_NAME != '' && dsDtlServiceCMResult.data.items[i].data.CATEGORY_ID != '')
        {
            var y='';
            var z='@@##$$@@';

            y = 'RESULT_CM_ID=' + dsDtlServiceCMResult.data.items[i].data.RESULT_CM_ID
            y += z + 'SERVICE_ID=' + dsDtlServiceCMResult.data.items[i].data.SERVICE_ID
            y += z + 'CATEGORY_ID='+ dsDtlServiceCMResult.data.items[i].data.CATEGORY_ID

            if (i === (dsDtlServiceCMResult.getCount()-1))
            {
                    x += y
            }
            else
            {
                    x += y + '##[[]]##'
            };
        };
        dsDtlServiceCMResult.data.items[i].data.TAG='';
    }
    return x;
};

function GetListCountDetailServiceCMResult()
{
	var x=0;
	for(var i = 0 ; i < dsDtlServiceCMResult.getCount();i++)
	{
		if (dsDtlServiceCMResult.data.items[i].data.SERVICE_ID != '' && dsDtlServiceCMResult.data.items[i].data.SERVICE_NAME  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getArrDetailPersonCMResult()
{
	var x='';
	var ds='';
	if (IsExtRepairCMResult === false)
	{
		ds=dsDtlCraftPersonIntCMResultTemp;
	}
	else
	{
		ds=dsDtlCraftPersonExtCMResultTemp;
		for(var i = 0 ; i < ds.getCount();i++)
		{
			if(ds.data.items[i].data.PERSON_NAME === '')
			{
				ds.removeAt(i);
			};
		}
	};
	
	for(var i = 0 ; i < ds.getCount();i++)
	{
		var  mDataKd='';
		var  mDataName='';
		var  mFlag='';
		
		if (IsExtRepairCMResult === false)  // internal
		{
			mDataKd = ds.data.items[i].data.EMP_ID;
			mDataName = ds.data.items[i].data.EMP_NAME;
			mFlag='xxx';
		}
		else
		{   
			mDataKd = ds.data.items[i].data.VENDOR_ID;  // external
			mDataName = ds.data.items[i].data.VENDOR;
			mFlag=ds.data.items[i].data.PERSON_NAME;
		};
		
		if (mDataKd != '' && mDataName != '' && mFlag != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'RESULT_CM_ID=' + ds.data.items[i].data.RESULT_CM_ID
			y += z +'SERVICE_ID=' + ds.data.items[i].data.SERVICE_ID
			y += z + 'ROW_SCH='+ ds.data.items[i].data.ROW_SCH
			y += z + 'EMP_ID='+ ds.data.items[i].data.EMP_ID
			y += z + 'EMP_NAME='+ ds.data.items[i].data.EMP_NAME
			y += z + 'VENDOR_ID='+ ds.data.items[i].data.VENDOR_ID
			y += z + 'VENDOR='+ ds.data.items[i].data.VENDOR
			
			if (IsExtRepairCMResult === false)  // internal
			{
				y += z + 'PERSON_NAME='+ ds.data.items[i].data.EMP_NAME
			}
			else
			{
				y += z + 'PERSON_NAME='+ ds.data.items[i].data.PERSON_NAME
			};
			
			y += z + 'COST='+ ds.data.items[i].data.COST
			y += z + 'DESC_SCH_PERSON='+ds.data.items[i].data.DESC_SCH_PERSON
			y += z + 'ROW_RSLT='+ds.data.items[i].data.ROW_RSLT
			
			if (i === (ds.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};

function GetListCountDetailPersonCMResult()
{
	var x=0;
	
	if (IsExtRepairCMResult === false)
	{
		ds=dsDtlCraftPersonIntCMResultTemp;
	}
	else
	{
		ds=dsDtlCraftPersonExtCMResultTemp; 
	};
	
	for(var i = 0 ; i < ds.getCount();i++)
	{
	
		var  mDataKd='';
		var  mDataName='';
		var  mFlag='';
		
		if (IsExtRepairCMResult === false)  // internal
		{
			mDataKd = ds.data.items[i].data.EMP_ID;
			mDataName = ds.data.items[i].data.EMP_NAME;
			mFlag='xxx';
		}
		else
		{  
			mDataKd = ds.data.items[i].data.VENDOR_ID; // external
			mDataName = ds.data.items[i].data.VENDOR;
			mFlag = ds.data.items[i].data.PERSON_NAME;
		};
	
		if (mDataKd != '' && mDataName  != '' && mFlag !='')
		{
			x += 1;
		};
	}
	return x;
	
};

function getArrDetailPartCMResult()
{
	var x='';
	for(var i = 0 ; i < dsDtlPartCMResultTemp.getCount();i++)
	{
		if (dsDtlPartCMResultTemp.data.items[i].data.PART_ID != '' && dsDtlPartCMResultTemp.data.items[i].data.PART_NAME != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'RESULT_CM_ID=' +  dsDtlPartCMResultTemp.data.items[i].data.RESULT_CM_ID
			y += z + 'SERVICE_ID=' + dsDtlPartCMResultTemp.data.items[i].data.SERVICE_ID
			y += z + 'PART_ID=' + dsDtlPartCMResultTemp.data.items[i].data.PART_ID
			y += z + 'QTY='+ dsDtlPartCMResultTemp.data.items[i].data.QTY
			y += z + 'UNIT_COST='+ dsDtlPartCMResultTemp.data.items[i].data.UNIT_COST
			y += z + 'TOT_COST='+ (dsDtlPartCMResultTemp.data.items[i].data.UNIT_COST * dsDtlPartCMResultTemp.data.items[i].data.QTY)
			y += z + 'DESC_SCH_PART='+dsDtlPartCMResultTemp.data.items[i].data.DESC_SCH_PART
			
			if (i === (dsDtlPartCMResultTemp.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
		dsDtlPartCMResultTemp.data.items[i].data.TAG='';
	}	
	return x;
};

function GetListCountDetailPartCMResult()
{
	var x=0;
	for(var i = 0 ; i < dsDtlPartCMResultTemp.getCount();i++)
	{
		if (dsDtlPartCMResultTemp.data.items[i].data.PART_ID != '' && dsDtlPartCMResultTemp.data.items[i].data.PART_NAME  != '')
		{
			x += 1;
		};
	}
	return x;
	
};


function getItemPanelInputCMResult(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
		border:false,
		height:754,//734,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar-35,
				labelWidth:75,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype:'fieldset',
						title: nmTitleTabInfoSchResultCM,	
						anchor:  '99.99%',
						height:'70px',
						items :
						[
							getItemPanelNoAssetCMResult(lebar)
						]
					},
					{
						xtype:'fieldset',
						title: nmTitleTabInfoWOResultCM,
						anchor:  '99.99%',
						height:'280px',//260,
						items :
						[
							getItemPanelTanggalCMResult(lebar),
							getItemPanelVendorAppCMResult(lebar),
							getItemPanelCardLayoutCMResult(),
							getItemPanelDescCMResult()
						]
					},
					{
						xtype:'fieldset',
						title: nmTitleTabInfoRsltResultCM,	
						anchor:  '99.99%',
						labelWidth:140,
						height:'83px',
						items :
						[
							getItemPanelResultCMResult2(lebar),
							getItemPanelNotesCMResult(lebar)
						]
					}
				]
			}
		]
	};
    return items;
};


function getItemPanelNotesCMResult(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 70,
	    items:
		[
			{
			    columnWidth: .61,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textarea',
						fieldLabel: nmNoteResultCM + ' ',
						name: 'txtResultDescCMResult',
						id: 'txtResultDescCMResult',
						scroll:true,
						anchor: '100% 83%'
					}
				]
			},
			{
			    columnWidth: .39,
			    layout: 'form',
			    border: false,
				labelWidth:87,
			    items:
				[ 
					mComboStatusAssetCMResult()
				]
			}
		]
	}
    return items;
};

function mComboStatusAssetCMResult()
{
    var Field = ['STATUS_ASSET_ID', 'STATUS_ASSET'];
    var dsStatusAssetCMResult = new WebApp.DataStore({ fields: Field });

    dsStatusAssetCMResult.load
    (
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        //Sort: 'STATUS_ASSET',
                        Sort: 'status_asset',
                        Sortdir: 'ASC',
                        target: 'viComboStatusAsset',
                        //param:' WHERE STATUS_ASSET_ID <> ~xxx~'
                        param:'status_asset_id <> ~xxx~'
                    }
            }
    );
	
    var cboStatusAssetCMResult = new Ext.form.ComboBox
    (
            {
                    id:'cboStatusAssetCMResult',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    emptyText: nmPilihStatusSetupAset,
                    fieldLabel: nmStatusAssetApprove + ' ',
                    anchor:'99%',
                    store: dsStatusAssetCMResult,
                    valueField: 'STATUS_ASSET_ID',
                    displayField: 'STATUS_ASSET',
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectStatusAssetCMResult=b.data.STATUS_ASSET_ID ;
                            }
                    }
            }
    );
	
	dsStatusAssetCMResult.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ASSET',
                            Sort: 'status_asset',
			    Sortdir: 'ASC',
			    target: 'viComboStatusAsset',
			     //param:' WHERE STATUS_ASSET_ID <> ~xxx~'
                             param:'status_asset_id <> ~xxx~'
			}
		}
	);
	
	return cboStatusAssetCMResult;
};

function getItemPanelTanggalCMResult(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 70,
	    items:
		[
			{
			    columnWidth: .23,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmWODateResultCM + ' ',
						id: 'dtpCMResult',
						name: 'dtpCMResult',
						readOnly:true,
						anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .27,
			    layout: 'form',
			    border: false,
				labelWidth:100,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmWOFinishResultCM + ' ',
						id: 'dtpWOFinishCMResult',
						name: 'dtpWOFinishCMResult',
						readOnly:true,
						anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .2,
			    layout: 'form',
			    border: false,
				labelWidth:40,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmPICResultCM + ' ',
						id: 'txtKdSupervisorCMResult',
						name: 'txtKdSupervisorCMResult',
						readOnly:true,
						anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: '',
						hideLabel:true,
						id: 'txtNamaSupervisorCMResult',
						name: 'txtNamaSupervisorCMResult',
						readOnly:true,
						anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelResultCMResult2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 57,
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:140,
			    items:
				[
					{
					    xtype: 'datefield',
						fieldLabel: nmFinishDateResultCM + ' ',
						id: 'dtpResultCMResult',
						name: 'dtpResultCMResult',
						format: 'd/M/Y',
						value: NowCMResult,
						anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:80,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmFinalCostResultCM + ' ',
					    name: 'txtLastCostCMResult',
					    id: 'txtLastCostCMResult',
						style: 
						{
							'font-weight': 'bold',
							'text-align':'right'
						},
						readOnly:true,
					    anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:50,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmRefResultCM + ' ',
					    name: 'txtReffCMResult',
					    id: 'txtReffCMResult',
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelCardLayoutCMResult()
{
	 var FormPanelCardLayoutCMResult = new Ext.Panel
	(
		{
		    id: 'FormPanelCardLayoutCMResult',
		    trackResetOnLoad: true,
		    width: 742,
			height: 195,//175
			layout: 'card',
			border:false,
			activeItem: 0,
			items: 
			[
				{
					xtype: 'panel',
					layout: 'form',
					border:false,
					id: 'CardCMResult1',
					items:
					[	 
						getItemTabPanelRepairInternalCMResult()
					]
				}
			]
		}
	);
	
	return FormPanelCardLayoutCMResult;
};

function getItemTabPanelRepairInternalCMResult()
{

	var TotalServiceCMResult = new Ext.Panel
	(
		{
			frame: false,
			layout: 'column',
			width: 700,
			border:false,
			labelAlign:'right',
			labelWidth:210,
			style: 
			{
				'margin-top': '3px'
			},
			items: 
			[
				{
					columnWidth: .5,
					layout: 'form',
					border: false,
					labelWidth:210,
					items:
					{
						xtype: 'textfield',
						fieldLabel: nmTotalPersonPartEstResultCM + ' ',
						style: 
						{
							'font-weight': 'bold',
							'text-align':'right'
						},
						id:'txtTotalServiceCMResult',
						name:'txtTotalServiceCMResult',
						readOnly:true,
						width: 110
					}
				},
				{
					columnWidth: .5,
					layout: 'form',
					border: false,
					labelWidth:210,
					items:
					{
						xtype: 'textfield',
						fieldLabel: nmTotalPersonPartRealResultCM + ' ',
						style: 
						{
							'font-weight': 'bold',
							'text-align':'right'
						},
						id:'txtTotalServiceRealCMResult',
						name:'txtTotalServiceRealCMResult',
						readOnly:true,
						width: 110
					}
				}
			]
		}
	);
	
	var TotalPersonCMResult = new Ext.Panel
	(
		{
			frame: false,
			layout: 'column',
			width: 725,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '3px',
				'margin-left': '410px'//'318px'
			},
			items: 
			[
				{
					columnWidth: .42,//.37,
					layout: 'form',
					border: false,
					labelWidth:140,
					items:
					{
						xtype: 'textfield',
						fieldLabel: nmTotalResultCM + ' ',
						style: 
						{
							'font-weight': 'bold',
							'text-align':'right'
						},
						id:'txtTotalPersonCMResult',
						name:'txtTotalPersonCMResult',
						readOnly:true,
						width: 150//120
					}
				}//,
				// {
					// columnWidth: .3,
					// layout: 'form',
					// border: false,
					// visible:false,
					// items:
					// {
						// xtype: 'textfield',
						// fieldLabel: '',
						// hideLabel:true,
						// style: 
						// {
							// 'font-weight': 'bold',
							// 'text-align':'right'
						// },
						// id:'txtTotalRealCostPersonCMResult',
						// name:'txtTotalRealCostPersonCMResult',
						// visible:false,
						// readOnly:true,
						// width: 120
					// }
				// }
			]
		}
	);

	var TotalPartCMResult = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 725,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '3px',
				'margin-left': '500px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: nmTotalResultCM + ' ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalPartCMResult',
					name:'txtTotalPartCMResult',
					readOnly:true,
					width: 150
				}
			]
		}
	);
	
    vTabPanelRepairCMResult = new Ext.TabPanel
	(
		{
		    id: 'vTabPanelRepairCMResult',
		    region: 'center',
		    margins: '7 7 7 7',
			style:
			{
			   'margin-top': '5px'
			},
		    bodyStyle: 'padding:7px 7px 7px 7px',
		    activeTab: 0,
		    plain: true,
		    defferedRender: false,
		    frame: false,
		    border: true,
		    height: 180,//160,
			width:742,
		    anchor: '100%',
		    items:
			[
				{
					 title: nmTitleTabServiceResultCM,
					 id: 'tabServiceCMResult',
					 items:
					 [
						GetDTLServiceCMResultGrid(),TotalServiceCMResult
					 ],
					 listeners:
					{
						activate: function() 
						{
							FocusCtrlCMResult === 'colService'
							varTabRepairCMResult=1;
						}
					}
				},
			    {
					 title: nmTitleTabPersonResultCM,
					 id: 'tabPersonCMResult',
					 items:
					 [
						getItemPanelCardLayoutGridServiceCMResult(),TotalPersonCMResult
					 ],
			        listeners:
					{
						activate: function() 
						{
							if (cellSelectServiceCMResult != undefined)
							{
								if (cellSelectServiceCMResult != '' && cellSelectServiceCMResult.data != undefined)
								{
									if (cellSelectServiceCMResult.data.SERVICE_ID === undefined || cellSelectServiceCMResult.data.SERVICE_ID === '')
									{
										vTabPanelRepairCMResult.setActiveTab('tabServiceCMResult');
										varTabRepairCMResult=1;
										alert(nmAlertTabService);
									}
									else
									{
										if (IsExtRepairCMResult === true)
										{
											Ext.getCmp('FormPanelCardLayoutGridPersonCMResult').getLayout().setActiveItem(1);	
											FocusCtrlCMResult = 'colVendor'
											GetPersonExtServiceCMResult(cellSelectServiceCMResult.data.SERVICE_ID);
										}
										else
										{
											Ext.getCmp('FormPanelCardLayoutGridPersonCMResult').getLayout().setActiveItem(0);	
											FocusCtrlCMResult = 'colEmp'
											GetPersonIntServiceCMResult(cellSelectServiceCMResult.data.SERVICE_ID);
										};
										CalcTotalPersonCMResult('',false);
										//CalcTotalRealCostPersonCMResult('');
										varTabRepairCMResult=2;
									};
								}
								else
								{
									vTabPanelRepairCMResult.setActiveTab('tabServiceCMResult');
									varTabRepairCMResult=1;
									alert(nmAlertTabService);
								};
							}
							else
							{
								vTabPanelRepairCMResult.setActiveTab('tabServiceCMResult');
								varTabRepairCMResult=1;
								alert(nmAlertTabService);
							};
						}
					}
				},
				{
					 title: nmTitleTabPartResultCM,
					 id: 'tabWOPartInternalCMResult',
					 items:
					 [
						GetDTLPartInternalCMResultGrid(),TotalPartCMResult
					 ],
					 listeners:
					{
						activate: function() 
						{
							if (cellSelectServiceCMResult != undefined)
							{
								if (cellSelectServiceCMResult != '' && cellSelectServiceCMResult.data != undefined)
								{
									if (cellSelectServiceCMResult.data.SERVICE_ID === undefined || cellSelectServiceCMResult.data.SERVICE_ID === '')
									{
										vTabPanelRepairCMResult.setActiveTab('tabServiceCMResult');
										varTabRepairCMResult=1;
										alert(nmAlertTabService);
									}
									else
									{
										FocusCtrlCMResult === 'colPart'
										varTabRepairCMResult=3;
										GetPartCMResult(cellSelectServiceCMResult.data.SERVICE_ID);
										CalcTotalPartCMResult('','',false)
									};	
								}
								else
								{
									vTabPanelRepairCMResult.setActiveTab('tabServiceCMResult');
									varTabRepairCMResult=1;
									alert(nmAlertTabService);
								};
							}
							else
							{
								vTabPanelRepairCMResult.setActiveTab('tabServiceCMResult');
								varTabRepairCMResult=1;
								alert(nmAlertTabService);
							};
						}
					}
				}
			]
		}
	)

    return vTabPanelRepairCMResult;
};

function GetDTLServiceCMResultGrid() 
{
    var fldDetail = ['RESULT_CM_ID','SCH_CM_ID','SERVICE_ID','SERVICE_NAME'];

    dsDtlServiceCMResult = new WebApp.DataStore({ fields: fldDetail })

    var gridServiceCMResult  = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlServiceCMResult,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:726,
		    height:115,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectServiceCMResult = dsDtlServiceCMResult.getAt(row);
					        CurrentServiceCMResult.row = row;
					        CurrentServiceCMResult.data = cellSelectServiceCMResult;
					    }
					}
				}
			),
				cm: ServiceCMResultDetailColumModel(),
				 tbar: 
			         [
				        {
					        id:'btnTambahBrsServiceCMResult',
					        text: nmTambahBaris,
					        tooltip: nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						TambahBarisServiceCMResult(dsDtlServiceCMResult);
					        }
				        },'-',
				        {
					        id:'btnHpsBrsServiceCMResult',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlServiceCMResult.getCount() > 0 )
								{
									if (cellSelectServiceCMResult != undefined)
									{
										if(CurrentServiceCMResult != undefined)
										{
											HapusBarisServiceCMResult();
										};
									}
									else
									{
										ShowPesanWarningCMResult(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			            ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridServiceCMResult;
};

function HapusBarisServiceCMResult()
{
	if (cellSelectServiceCMResult.data.SERVICE_ID != '' && cellSelectServiceCMResult.data.SERVICE_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentServiceCMResult.row + 1) + ' ' + nmOperatorDengan + ' ' + nmServIDResultCM + ' : ' + cellSelectServiceCMResult.data.SERVICE_ID + ' ' + nmOperatorAnd + ' ' + nmServNameResultCM + ' : ' + cellSelectServiceCMResult.data.SERVICE_NAME ,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlServiceCMResult.data.items[CurrentServiceCMResult.row].data.TAG === 1 )
							{
								deletePersonPartServiceRowDeleteCMResult(cellSelectServiceCMResult.data.SERVICE_ID);
								dsDtlServiceCMResult.removeAt(CurrentServiceCMResult.row);
								cellSelectServiceCMResult=undefined;
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase(),
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailServiceCMResult();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			dsDtlServiceCMResult.removeAt(CurrentServiceCMResult.row);
		};
};

function deletePersonPartServiceRowDeleteCMResult(service_id)
{
	var x=0;
	var y=0;
	
	if (service_id != '' && service_id != undefined)
	{
		if( IsExtRepairCMResult === true)
		{
			for (var i = 0; i < dsDtlCraftPersonExtCMResultTemp.getCount() ; i++) 
			{
				if(dsDtlCraftPersonExtCMResultTemp.data.items[i].data.SERVICE_ID === service_id)
				{
					x += dsDtlCraftPersonExtCMResultTemp.data.items[i].data.COST ;
					dsDtlCraftPersonExtCMResultTemp.removeAt(i);
				};
			};
		}
		else
		{
			for (var i = 0; i < dsDtlCraftPersonIntCMResultTemp.getCount() ; i++) 
			{
				if(dsDtlCraftPersonIntCMResultTemp.data.items[i].data.SERVICE_ID === service_id)
				{
					x += dsDtlCraftPersonIntCMResultTemp.data.items[i].data.COST ;
					dsDtlCraftPersonIntCMResultTemp.removeAt(i);
				};
			};
		};
		
		for (var i = 0; i < dsDtlPartCMResultTemp.getCount() ; i++) 
		{
			if(dsDtlPartCMResultTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				y += (dsDtlPartCMResultTemp.data.items[i].data.UNIT_COST * dsDtlPartCMResultTemp.data.items[i].data.QTY);
				dsDtlPartCMResultTemp.removeAt(i);
			};
		};
		
		var a=GetNilaiCurrencyCMResult(Ext.get('txtTotalServiceRealCMResult').dom.value);
		var b=GetNilaiCurrencyCMResult(Ext.get('txtLastCostCMResult').dom.value);
		
		if (a > 0)
		{
			Ext.get('txtTotalServiceRealCMResult').dom.value=formatCurrency(a-(x+y));
		}
		else
		{
			Ext.get('txtTotalServiceRealCMResult').dom.value=0
		};
		
		if (b > 0)
		{
			Ext.get('txtLastCostCMResult').dom.value=formatCurrency(b-(x+y));
		}
		else
		{
			Ext.get('txtLastCostCMResult').dom.value=0
		};
	};
};

function GetLookupServiceCMResult(str,idx,ds)
{
    if (IdCMResult === '' || IdCMResult === undefined)
    {
            var p = new mRecordServiceCMResult
            (
                    {
                            'RESULT_CM_ID':'',
                            'SCH_CM_ID':'',
                            'SERVICE_ID':'',
                            'SERVICE_NAME':'',
                            'CATEGORY_ID':varCatIdCMResult,
                            'TAG':1
                    }
            );
            FormLookupServiceCategory('','',str,true,p,ds,false);
    }
    else
    {
            var p = new mRecordServiceCMResult
            (
                    {
                            'RESULT_CM_ID':IdCMResult,
                            'SCH_CM_ID':'',
                            'SERVICE_ID':'',
                            'SERVICE_NAME':'',
                            'CATEGORY_ID':varCatIdCMResult,
                            'TAG':1
                    }
            );
            FormLookupServiceCategory('','',str,true,p,ds,false);
    };
};

function TambahBarisServiceCMResult(ds)
{
	var p = GetRecordBaruServiceCMResult();
	ds.insert(ds.getCount(), p);
};

function DeleteDetailServiceCMResult()
{
	Ext.Ajax.request
	(
		{
			//url: "./Datapool.mvc/DeleteDataObj",
                        url: baseURL + "index.php/main/DeleteDataObj",
			params:  getParamDeleteDetailServiceCMResult(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoCMResult(nmPesanHapusSukses,nmHeaderHapusData);
					deletePersonPartServiceRowDeleteCMResult(cellSelectServiceCMResult.data.SERVICE_ID);
					GetJumlahTotalResultPersonPartCMResult();
						dsDtlServiceCMResult.removeAt(CurrentServiceCMResult.row);
						cellSelectServiceCMResult=undefined;
						LoadDataServiceResultCMResult(IdCMResult,varCatIdCMResult);
						//GetJumlahPartCMResult();
					
					AddNewCMResult = false;
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningCMResult(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorCMResult(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailServiceCMResult()
{
	var params =
	{	
		Table: 'ViewResultCM',   
	    ResultId: CurrentServiceCMResult.data.data.RESULT_CM_ID,
		CatId: varCatIdCMResult,
		ServiceId: CurrentServiceCMResult.data.data.SERVICE_ID,
		Hapus:1,
		Service:1
		
	};
	
    return params;
};


function ServiceCMResultDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdServiceCMResult',
			    header: nmServIDResultCM,
			    dataIndex: 'SERVICE_ID',
			    width: 150,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdServiceCMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListServiceCMResult();
									if (strList !='')
									{
									    //strList = ' and SERVICE_ID not in ( ' + strList + ') '
                                                                            strList = ' and service_id not in ( ' + strList + ' )';
									};
									
						            if (Ext.get('fieldcolKdServiceCMResult').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdCMResult + '~'
                                                                                str = 'category_id = ~' + varCatIdCMResult + '~';
										str += strList;
										//str += ' and SERVICE_ID like ~%' + Ext.get('fieldcolKdServiceCMResult').dom.value + '%~';
                                                                                str += ' and service_id like ~%' + Ext.get('fieldcolKdServiceCMResult').dom.value + '%~';
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdCMResult + '~';
                                                                                str = 'category_id = ~' + varCatIdCMResult + '~';
										str += strList;
									};
									GetLookupServiceCMResult(str,CurrentServiceCMResult.row,dsDtlServiceCMResult);  
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMResult='colService';
							}
						}
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						str = "<div style='color:blue;'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colServiceNameCMResult',
			    header: nmServNameResultCM,
			    dataIndex: 'SERVICE_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolServiceNameCMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListServiceCMResult();
									if (strList !='')
									{
									    //strList = ' and SERVICE_ID not in ( ' + strList + ') '
                                                                            strList = ' and service_id not in (' + strList + ') '
									};
									
						            if (Ext.get('fieldcolServiceNameCMResult').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdCMResult + '~'
                                                                                str = 'category_id = ~' + varCatIdCMResult + '~';
										str += strList;
										//str += ' and  SERVICE_NAME like ~%' + Ext.get('fieldcolServiceNameCMResult').dom.value + '%~';
                                                                                str += ' and service_name like ~%' + Ext.get('fieldcolServiceNameCMResult').dom.value + '%~';
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdCMResult + '~'
                                                                                str = 'category_id = ~' + varCatIdCMResult + '~'
										str += strList;
									};
									GetLookupServiceCMResult(str,CurrentServiceCMResult.row,dsDtlServiceCMResult);  
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMResult='colService';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			}
		]
	)
};



function getItemPanelCardLayoutGridServiceCMResult()
{

	 var FormPanelCardLayoutGridPersonCMResult = new Ext.Panel
	(
		{
		    id: 'FormPanelCardLayoutGridPersonCMResult',
		    trackResetOnLoad: true,
		    width: 725,
			height: 115,//150,
			layout: 'card',
			border:false,
			activeItem: 0,
			items: 
			[
				{
					xtype: 'panel',
					layout: 'form',
					width: 725,
					height: 150,
					border:false,
					id: 'CardGridServiceCMResult1',
					items:
					[	 
						GetDTLCraftPersonCMResultGrid()
					]
				},
				{
					xtype: 'panel',
					layout: 'form',
					width: 725,
					height: 150,
					border:false,
					id: 'CardGridServiceCMResult2',
					items:
					[
						GetDTLCraftPersonExtCMResultGrid()
					]
				}
			]
		}
	);
	
	return FormPanelCardLayoutGridPersonCMResult;
};



function GetDTLCraftPersonCMResultGrid() 
{
   var fldDetail = ['RESULT_CM_ID','SCH_CM_ID','SERVICE_ID','ROW_SCH','ROW_RSLT','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_SCH_PERSON','VENDOR','EMP_NAME','CATEGORY_ID','RESULT_PM_ID','ROW','REAL_COST'];

    dsDtlCraftPersonIntCMResult = new WebApp.DataStore({ fields: fldDetail })
	dsDtlCraftPersonIntCMResultTemp = new WebApp.DataStore({ fields: fldDetail })
	
    var gridCraftPersonCMResult = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlCraftPersonIntCMResult,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:725,
		    height:115,//120,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectPersonIntCMResult = dsDtlCraftPersonIntCMResult.getAt(row);
					        CurrentPersonIntCMResult.row = row;
					        CurrentPersonIntCMResult.data = cellSelectPersonIntCMResult;
					    }
					}
				}
			),
				cm: CraftPersonCMResultDetailColumModel(),
				tbar: 
			         [
				        {
					        id:'btnTambahBrsCraftPersonIntCMResult',
					        text: nmTambahBaris,
					        tooltip: nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						TambahBarisPersonIntCMResult();
					        }
				        },'-',
				        {
					        id:'btnHpsBrsCraftPersonIntCMResult',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlCraftPersonIntCMResult.getCount() > 0 )
								{
									if (cellSelectPersonIntCMResult != undefined)
									{
										if(CurrentPersonIntCMResult != undefined)
										{
											HapusBarisPersonIntCMResult();
										};
									}
									else
									{
										ShowPesanWarningCMResult(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			            ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridCraftPersonCMResult;
};

function TambahBarisPersonIntCMResult()
{
	var p = GetRecordBaruPersonCMResult(dsDtlCraftPersonIntCMResult);
	dsDtlCraftPersonIntCMResult.insert(dsDtlCraftPersonIntCMResult.getCount(), p);
};

function HapusBarisPersonIntCMResult()
{
	if (cellSelectPersonIntCMResult.data.EMP_ID != '' && cellSelectPersonIntCMResult.data.EMP_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentPersonIntCMResult.row + 1) + ' ' + nmOperatorDengan + ' ' + nmEmpIDResultCM + ' : ' + cellSelectPersonIntCMResult.data.EMP_ID + ' ' + nmOperatorAnd + ' ' + nmEmpNameResultCM + ' : ' + cellSelectPersonIntCMResult.data.EMP_NAME,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlCraftPersonIntCMResult.data.items[CurrentPersonIntCMResult.row].data.ROW_RSLT=== '' )
							{
								GetTotalRemoveCMResult(CurrentPersonIntCMResult,true);
								HapusRowTempCMResult(dsDtlCraftPersonIntCMResultTemp,CurrentPersonIntCMResult.data.data.ROW);
								dsDtlCraftPersonIntCMResult.removeAt(CurrentPersonIntCMResult.row);
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPersonCMResult();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			if(CurrentPersonIntCMResult.data.data.COST != '' && CurrentPersonIntCMResult.data.data.COST != undefined)
			{
				GetTotalRemoveCMResult(CurrentPersonIntCMResult,true)
			};
			dsDtlCraftPersonIntCMResult.removeAt(CurrentPersonIntCMResult.row);
		};
};

function DeleteDetailPersonCMResult()
{
	Ext.Ajax.request
	(
		{
			//url: "./Datapool.mvc/DeleteDataObj",
                        url: baseURL + "index.php/main/DeleteDataObj",
			params:  getParamDeleteDetailPersonCMResult(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoCMResult(nmPesanHapusSukses,nmHeaderHapusData);
					if (IsExtRepairCMResult === false)
					{
						GetTotalRemoveCMResult(CurrentPersonIntCMResult,true);
						//GetJumlahTotalResultPersonCMResult('where RESULT_CM_ID =~' + IdCMResult + '~ and category_id = ~' + varCatIdCMResult + '~' + ' and service_id= ~ ' + cellSelectServiceCMResult.data.SERVICE_ID + '~' + ' and EMP_ID <> ~~' + 'and EMP_ID is not null');
                                                GetJumlahTotalResultPersonCMResult('result_cm_id = ~' + IdCMResult + '~ and category_id = ~' + varCatIdCMResult + '~' + ' and service_id = ~' + cellSelectServiceCMResult.data.SERVICE_ID + '~' + ' and emp_id <> ~~' + ' and emp_id is not null');
						HapusRowTempCMResult(dsDtlCraftPersonIntCMResultTemp,CurrentPersonIntCMResult.data.data.ROW);
						dsDtlCraftPersonIntCMResult.removeAt(CurrentPersonIntCMResult.row);
						cellSelectPersonIntCMResult=undefined;
						LoadDataEmployeeResultCMResult(IdCMResult,varCatIdCMResult);
						ReLoadDataEmployeeResultCMResult(IdCMResult,varCatIdCMResult,cellSelectServiceCMResult.data.SERVICE_ID);
					}
					else
					{
						GetTotalRemoveCMResult(CurrentPersonExtCMResult,true);
						//GetJumlahTotalResultPersonCMResult('where RESULT_CM_ID =~' + IdCMResult + '~ and category_id = ~' + varCatIdCMResult + '~' + ' and service_id= ~ ' + cellSelectServiceCMResult.data.SERVICE_ID + '~' + ' and VENDOR_ID <> ~~' + 'and VENDOR_ID is not null');
                                                GetJumlahTotalResultPersonCMResult('result_cm_id = ~' + IdCMResult + '~ and category_id = ~' + varCatIdCMResult + '~' + ' and service_id = ~' + cellSelectServiceCMResult.data.SERVICE_ID + '~' + ' and vendor_id <> ~~' + ' and vendor_id is not null');
						dsDtlCraftPersonExtCMResult.removeAt(CurrentPersonExtCMResult.row);
						cellSelectPersonExtCMResult=undefined;
						HapusRowTempCMResult(dsDtlCraftPersonExtCMResultTemp,CurrentPersonExtCMResult.data.data.ROW);
						LoadDataVendorResultCMResult(IdCMResult,varCatIdCMResult);
						ReLoadDataVendorResultCMResult(IdCMResult,varCatIdCMResult,cellSelectServiceCMResult.data.SERVICE_ID);
					};
					
					AddNewCMResult = false;
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningCMResult(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorCMResult(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailPersonCMResult()
{
	var x,y,z;
	var w=0;
	
	if (IsExtRepairCMResult === false)
	{
		x=CurrentPersonIntCMResult.data.data.RESULT_CM_ID;
		y=CurrentPersonIntCMResult.data.data.SERVICE_ID;
		z=CurrentPersonIntCMResult.data.data.ROW_RSLT;
		w=CurrentPersonIntCMResult.data.data.COST;
	}
	else
	{
		x=CurrentPersonExtCMResult.data.data.RESULT_CM_ID;
		y=CurrentPersonExtCMResult.data.data.SERVICE_ID;
		z=CurrentPersonExtCMResult.data.data.ROW_RSLT;
		w=CurrentPersonExtCMResult.data.data.COST;
	};
	
	var params =
	{	
		Table: 'ViewResultCM',   
	    ResultId: x,
		ServiceId:y,
		RowResult:z,
		CatId: varCatIdCMResult,
		LastCost:w,
		Hapus:3
	};
	
    return params;
};

function GetDTLCraftPersonExtCMResultGrid() 
{
    var fldDetail = ['RESULT_CM_ID','SCH_CM_ID','SERVICE_ID','ROW_SCH','ROW_RSLT','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_SCH_PERSON','VENDOR','EMP_NAME','CATEGORY_ID','RESULT_PM_ID','ROW'];

    dsDtlCraftPersonExtCMResult = new WebApp.DataStore({ fields: fldDetail });
	dsDtlCraftPersonExtCMResultTemp = new WebApp.DataStore({ fields: fldDetail })

    var gridCraftPersonExtCMResult = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlCraftPersonExtCMResult,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:725,
		    height:87,//120
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectPersonExtCMResult = dsDtlCraftPersonExtCMResult.getAt(row);
					        CurrentPersonExtCMResult.row = row;
					        CurrentPersonExtCMResult.data = cellSelectPersonExtCMResult;
					    }
					}
				}
			),
				cm: CraftPersonExtCMResultDetailColumModel(),
				 tbar: 
			         [
				        {
					        id:'btnTambahBrsCraftPersonExtCMResult',
					        text: nmTambahBaris,
					        tooltip: nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						TambahBarisPersonExtCMResult();
					        }
				        },'-',
				        {
					        id:'btnHpsBrsCraftPersonExtCMResult',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlCraftPersonExtCMResult.getCount() > 0 )
								{
									if (cellSelectPersonExtCMResult != undefined)
									{
										if(CurrentPersonExtCMResult != undefined)
										{
											HapusBarisPersonExtCMResult();
										};
									}
									else
									{
										ShowPesanWarningCMResult(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			            ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridCraftPersonExtCMResult;
};

function TambahBarisPersonExtCMResult()
{
	if (Ext.get('txtKdVendorCMResult').dom.value === '')
	{
		ShowPesanWarningCMResult(nmGetValidasiKosong(nmVendIDNameResultCM), nmTambahBaris);
	}
	else
	{
		var p = GetRecordBaruPersonCMResult(dsDtlCraftPersonExtCMResult);
		dsDtlCraftPersonExtCMResult.insert(dsDtlCraftPersonExtCMResult.getCount(), p);
		
		var j=dsDtlCraftPersonExtCMResult.getCount()-1;
		dsDtlCraftPersonExtCMResultTemp.insert(dsDtlCraftPersonExtCMResultTemp.getCount(), dsDtlCraftPersonExtCMResult.data.items[j]);
		GetPersonExtServiceCMResult(cellSelectServiceCMResult.data.SERVICE_ID);
	};
};

function HapusBarisPersonExtCMResult()
{
	if (cellSelectPersonExtCMResult.data.VENDOR_ID != '' && cellSelectPersonExtCMResult.data.VENDOR != '' && cellSelectPersonExtCMResult.data.PERSON_NAME !='')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentPersonExtCMResult.row + 1) + ' ' + nmOperatorDengan + ' '+ nmPersonNameResultCM + ' : ' + cellSelectPersonExtCMResult.data.PERSON_NAME, 
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlCraftPersonExtCMResult.data.items[CurrentPersonExtCMResult.row].data.ROW_RSLT === '' )
							{
								GetTotalRemoveCMResult(CurrentPersonExtCMResult,true);
								HapusRowTempCMResult(dsDtlCraftPersonExtCMResultTemp,CurrentPersonExtCMResult.data.data.ROW);
								dsDtlCraftPersonExtCMResult.removeAt(CurrentPersonExtCMResult.row);
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase(),
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPersonCMResult();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			if(CurrentPersonExtCMResult.data.data.COST != '' && CurrentPersonExtCMResult.data.data.COST != undefined)
			{
				GetTotalRemoveCMResult(CurrentPersonExtCMResult,true)
			};
			HapusRowTempCMResult(dsDtlCraftPersonExtCMResultTemp,CurrentPersonExtCMResult.data.data.ROW);
			dsDtlCraftPersonExtCMResult.removeAt(CurrentPersonExtCMResult.row);
		};
};

function GetTotalRemoveCMResult(current,mBolEmp)
{
	var a;
	var b=0;
	
	if(mBolEmp === true )
	{
		a='txtTotalPersonCMResult';
	}
	else
	{
		a='txtTotalPartCMResult';
	};
	
	var x=GetNilaiCurrencyCMResult(Ext.get(a).dom.value);
	var y=GetNilaiCurrencyCMResult(Ext.get('txtTotalServiceRealCMResult').dom.value);
	var z=GetNilaiCurrencyCMResult(Ext.get('txtLastCostCMResult').dom.value);
	
	if(mBolEmp === true)
	{
		b=current.data.data.COST;
	}
	else
	{
		b=current.data.data.UNIT_COST * current.data.data.QTY;
	};
	
	if (x > 0)
	{
		Ext.get(a).dom.value=formatCurrency(x-b);
	}
	else
	{
		Ext.get(a).dom.value=0;
	};
	
	if (y > 0)
	{
		Ext.get('txtTotalServiceRealCMResult').dom.value=formatCurrency(y-b);
	}
	else
	{
		Ext.get('txtTotalServiceRealCMResult').dom.value=0
	};
	
	if (z > 0)
	{
		Ext.get('txtLastCostCMResult').dom.value=formatCurrency(z-b);
	}
	else
	{
		Ext.get('txtLastCostCMResult').dom.value=0
	};
};

function GetDTLPartInternalCMResultGrid() {
     var fldDetail = ['RESULT_CM_ID','SCH_CM_ID','SERVICE_ID','PART_ID','QTY','UNIT_COST','TOT_COST','PART_NAME','PRICE','DESC_SCH_PART','TAG','ROW'];

    dsDtlPartCMResult = new WebApp.DataStore({ fields: fldDetail });
	  dsDtlPartCMResultTemp = new WebApp.DataStore({ fields: fldDetail })

    var gridPartInternalCMResult = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlPartCMResult,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:725,
		    height: 115,//120,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
							cellSelectPartCMResult = dsDtlPartCMResult.getAt(row);
					        CurrentPartCMResult.row = row;
					        CurrentPartCMResult.data = cellSelectPartCMResult;
					    }
					}
				}
			),
			cm: PartInternalCMResultDetailColumModel(),
			tbar:
			         [
				        {
				            id: 'btnTambahBrsPartCMResult',
				            text: nmTambahBaris,
				            tooltip: nmTambahBaris,
				            iconCls: 'AddRow',
				            handler: function() 
							{
								TambahBarisPartCMResult();
				            }
				        }, '-',
				        {
				            id: 'btnHpsBrsPartCMResult',
				            text: nmHapusBaris,
				            tooltip: nmHapusBaris,
				            iconCls: 'RemoveRow',
				            handler: function() 
							{
								if (dsDtlPartCMResult.getCount() > 0 )
								{
									if (cellSelectPartCMResult != undefined)
									{
										if(CurrentPartCMResult != undefined)
										{
											HapusBarisPartCMResult();
										};
									}
									else
									{
										ShowPesanWarningCMResult(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
				            }
				        }, ' ', '-'
			            ]
		    , viewConfig: { forceFit: true }
		}
	);

		return gridPartInternalCMResult;
};

function TambahBarisPartCMResult()
{
	var p = GetRecordBaruPartCMResult();
	dsDtlPartCMResult.insert(dsDtlPartCMResult.getCount(), p);
};

function HapusBarisPartCMResult()
{
	if (cellSelectPartCMResult.data.PART_ID != '' && cellSelectPartCMResult.data.PART_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + ' ' + (CurrentPartCMResult.row + 1) + ' ' + nmOperatorDengan + ' ' + nmPartNumResultCM + ' : ' + cellSelectPartCMResult.data.PART_ID + ' ' + nmOperatorAnd + ' ' + nmPartNameResultCM + ' : ' + cellSelectPartCMResult.data.PART_NAME ,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlPartCMResult.data.items[CurrentPartCMResult.row].data.TAG === 1 || dsDtlPartCMResult.data.items[CurrentPartCMResult.row].data.RESULT_CM_ID === '')
							{
								GetTotalRemoveCMResult(CurrentPartCMResult,false);
								HapusRowTempCMResult(dsDtlPartCMResultTemp,CurrentPartCMResult.data.data.ROW);
								dsDtlPartCMResult.removeAt(CurrentPartCMResult.row);
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPartCMResult();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			if(CurrentPartCMResult.data.data.UNIT_COST != '' && CurrentPartCMResult.data.data.UNIT_COST != undefined)
			{
				GetTotalRemoveCMResult(CurrentPartCMResult,false);
			};
			dsDtlPartCMResult.removeAt(CurrentPartCMResult.row);
		};
};

function DeleteDetailPartCMResult()
{
	Ext.Ajax.request
	(
		{
			//url: "./Datapool.mvc/DeleteDataObj",
                        url: baseURL + "index.php/main/DeleteDataObj",
			params:  getParamDeleteDetailPartCMResult(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					GetJumlahTotalResultPartCMResult();
					GetTotalRemoveCMResult(CurrentPartCMResult,false);
					HapusRowTempCMResult(dsDtlPartCMResultTemp,CurrentPartCMResult.data.data.ROW);
					dsDtlPartCMResult.removeAt(CurrentPartCMResult.row);
					cellSelectPartCMResult=undefined;
					LoadDataPartResultCMResult(IdCMResult,varCatIdCMResult);
					ReLoadDataPartResultCMResult(IdCMResult,varCatIdCMResult,cellSelectServiceCMResult.data.SERVICE_ID);
					AddNewCMResult = false;
					ShowPesanInfoCMResult(nmPesanHapusSukses,nmHeaderHapusData);
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningCMResult(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorCMResult(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailPartCMResult()
{
	
	var params =
	{	
		Table: 'ViewResultCM',   
	    ResultId: CurrentPartCMResult.data.data.RESULT_CM_ID,
		ServiceId: CurrentPartCMResult.data.data.SERVICE_ID,
		PartId: CurrentPartCMResult.data.data.PART_ID,
		CatId: varCatIdCMResult,
		LastCost:(CurrentPartCMResult.data.data.UNIT_COST * CurrentPartCMResult.data.data.QTY),
		Hapus:4
	};
	
    return params;
};

function CraftPersonCMResultDetailColumModel() 
{
   return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdEmployeeCMResult',
			    header: nmEmpIDResultCM,
			    dataIndex: 'EMP_ID',
			    width: 150,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdEmployeeCMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
						            if (Ext.get('fieldcolKdEmployeeCMResult').dom.value != '')
									{
										//str = ' where EMP_ID like ~%' + Ext.get('fieldcolKdEmployeeCMResult').dom.value + '%~';
                                                                                str = 'emp_id like ~%' + Ext.get('fieldcolKdEmployeeCMResult').dom.value + '%~';
									};
									GetLookupPersonIntCMResult(str)    
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMResult='colEmp';
							}
						}
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						str = "<div style='color:blue;'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colEmployeeNameCMResult',
			    header: nmEmpNameResultCM,
			    dataIndex: 'EMP_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolEmployeeNameCMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
						            if (Ext.get('fieldcolEmployeeNameCMResult').dom.value != '')
									{
										//str = ' where EMP_NAME like ~%' + Ext.get('fieldcolEmployeeNameCMResult').dom.value + '%~';
                                                                                str = 'emp_name like ~%' + Ext.get('fieldcolEmployeeNameCMResult').dom.value + '%~';
									};
									GetLookupPersonIntCMResult(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMResult='colEmp';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell,record) 
				{
					var str='';
					if (value != undefined)
					{
						if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};
					};
			        return str;
			    }
			},
			{
			    id: 'colDescCraftPersonCMResult',
			    header: nmDescResultCM,
			    width: 200,
			    dataIndex: 'DESC_SCH_PERSON',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescCraftPersonCMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(value, cell,record) 
				{
			       var str='';
					if (value != undefined)
					{
						if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};
					};
			        return str;
			    }
			},
			{
			    id: 'colCostCraftPersonCMResult',
			    header: nmCostResultCM,
			    width: 150,
				align:'right',
			    dataIndex: 'COST',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostCraftPersonCMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPersonCMResult(CurrentPersonIntCMResult.row,true);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.COST) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.COST);
					};
				}
			},
			{
			    id: 'colRealCostCraftPersonCMResult',
			    header: nmRealCostResultCM,
			    width: 150,
				align:'right',
				hidden:true,
			    dataIndex: 'REAL_COST',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolRealCostCraftPersonCMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalRealCostPersonCMResult(CurrentPersonIntCMResult.row);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.REAL_COST) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.REAL_COST);
					};
				}
			}
		]
	)

};

function GetLookupPersonIntCMResult(str)
{
	if (cellSelectPersonIntCMResult.data.ROW_SCH === '' || cellSelectPersonIntCMResult.data.ROW_SCH === undefined)
	{
		var p = new mRecordPersonCMResult
		(
			{
				'RESULT_CM_ID':IdCMResult,
				'SCH_CM_ID':'',
				'SERVICE_ID':cellSelectServiceCMResult.data.SERVICE_ID,
				'ROW_SCH':'',
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonIntCMResult.data.items[CurrentPersonIntCMResult.row].data.COST,
				'REAL_COST':dsDtlCraftPersonIntCMResult.data.items[CurrentPersonIntCMResult.row].data.REAL_COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonIntCMResult.data.items[CurrentPersonIntCMResult.row].data.DESC_SCH_PERSON,
				'ROW': dsDtlCraftPersonIntCMResult.data.items[CurrentPersonIntCMResult.row].data.ROW,
				'ROW_RSLT':''
			}
		);
		FormLookupEmployee('','',str,true,p,dsDtlCraftPersonIntCMResult,false);
	}
	else
	{	
		var p = new mRecordPersonCMResult
		(
			{
				'RESULT_CM_ID':IdCMResult,
				'SCH_CM_ID':'',
				'SERVICE_ID':cellSelectServiceCMResult.data.SERVICE_ID,
				'ROW_SCH':dsDtlCraftPersonIntCMResult.data.items[CurrentPersonIntCMResult.row].data.ROW_SCH,
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonIntCMResult.data.items[CurrentPersonIntCMResult.row].data.COST,
				'REAL_COST':dsDtlCraftPersonIntCMResult.data.items[CurrentPersonIntCMResult.row].data.REAL_COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonIntCMResult.data.items[CurrentPersonIntCMResult.row].data.DESC_SCH_PERSON,
				'ROW': dsDtlCraftPersonIntCMResult.data.items[CurrentPersonIntCMResult.row].data.ROW,
				'ROW_RSLT':dsDtlCraftPersonIntCMResult.data.items[CurrentPersonIntCMResult.row].data.ROW_RSLT
			}
		);
		FormLookupEmployee('','',str,true,p,dsDtlCraftPersonIntCMResult,false);
	};
};

function CalcTotalPersonCMResult(idx,mBolGrid)
{
	var ds;
	var col;
	if(IsExtRepairCMResult === false)
	{
		ds=dsDtlCraftPersonIntCMResult;
		col=Ext.get('fieldcolCostCraftPersonCMResult')
	}
	else
	{
		ds=dsDtlCraftPersonExtCMResult;
		col=Ext.get('fieldcolCostCraftPersonExtCMResult')
	};
	
    var total=Ext.num(0);
	for(var i=0;i < ds.getCount();i++)
	{
		if (i === idx)
		{
			if (col != null) 
			{
				if (Ext.num(col.dom.value) != null) 
				{
					total += (Ext.num(col.dom.value));
				};
			};
		}
		else
		{
			total += Ext.num(ds.data.items[i].data.COST);
		}
	}
	
	Ext.get('txtTotalPersonCMResult').dom.value = formatCurrency(total);	
	if (mBolGrid === true)
	{
		Ext.get('txtTotalServiceRealCMResult').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalCMResultAllService(true,false)));
		Ext.get('txtLastCostCMResult').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalCMResultAllService(true,false)));
	};
};

function CalcTotalCMResultAllService(mBol,mBolPart)
{
	var ds;
	
	if (mBol != true)
	{
		if(IsExtRepairCMResult === false)
		{
			ds=dsDtlCraftPersonIntCMResultTemp;
		}
		else
		{
			ds=dsDtlCraftPersonExtCMResultTemp;
		};
	}
	else
	{
		ds=dsDtlPartCMResultTemp;
	};
	
    var total=Ext.num(0);
	for(var i=0;i < ds.getCount();i++)
	{
		if (mBol != true)
		{
			total += Ext.num(ds.data.items[i].data.COST);
		}
		else
		{	
			//total += Ext.num(ds.data.items[i].data.TOT_COST);
			total += Ext.num(ds.data.items[i].data.UNIT_COST * ds.data.items[i].data.QTY);
		};
	}
	
	if (mBolPart === true)
	{
		ds=dsDtlPartCMResultTemp;
	}
	else
	{
		if(IsExtRepairCMResult === false)
		{
			ds=dsDtlCraftPersonIntCMResultTemp;
		}
		else
		{
			ds=dsDtlCraftPersonExtCMResultTemp;
		};
	};
	
	for(var i=0;i < ds.getCount();i++)
	{
		if (mBolPart === true)
		{
			if ( ds.data.items[i].data.SERVICE_ID != cellSelectServiceCMResult.data.SERVICE_ID)
			{
				//total += Ext.num(ds.data.items[i].data.TOT_COST);
				total += Ext.num(ds.data.items[i].data.UNIT_COST * ds.data.items[i].data.QTY);
			};
		}
		else
		{	
			if ( ds.data.items[i].data.SERVICE_ID != cellSelectServiceCMResult.data.SERVICE_ID)
			{
				total += Ext.num(ds.data.items[i].data.COST);
			};
		};
	};
	
	return total;
};

function CalcTotalRealCostPersonCMResult(idx)
{
    var ds;
    var col;
    if(IsExtRepairCMResult === false)
    {
        ds=dsDtlCraftPersonIntCMResult;
        col=Ext.get('fieldcolRealCostCraftPersonCMResult')
    }
    else
    {
        ds=dsDtlCraftPersonExtCMResult;
        col=Ext.get('fieldcolRealCostCraftPersonExtCMResult')
    };
	
    var total=Ext.num(0);
    for(var i=0;i < ds.getCount();i++)
    {
        if (i === idx)
        {
            if (col != null)
            {
                if (Ext.num(col.dom.value) != null)
                {
                    total += (Ext.num(col.dom.value));
                };
            };
        }
        else
        {
            total += Ext.num(ds.data.items[i].data.REAL_COST);
        }
    }

    //Ext.get('txtTotalRealCostPersonCMResult').dom.value = formatCurrency(total);

};

function CraftPersonExtCMResultDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdVendorCMResult',
			    header: nmVendIDResultCM,
			    dataIndex: 'VENDOR_ID',
				hidden:true,
			    width: 80,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdVendorCMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									if (Ext.get('fieldcolKdVendorCMResult').dom.value != '')
									{
										//str = ' where VENDOR_ID like ~%' + Ext.get('fieldcolKdVendorCMResult').dom.value + '%~';
                                                                                str = 'vendor_id like ~%' + Ext.get('fieldcolKdVendorCMResult').dom.value + '%~';
									};
						            GetLookupPersonExtCMResult(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMResult='colVendor';
							}
						}
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						str = "<div style='color:blue;'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colVendorNameCMResult',
			    header: nmVendNameResultCM,
				hidden:true,
			    dataIndex: 'VENDOR',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolVendorNameCMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						           var str = '';
									if (Ext.get('fieldcolVendorNameCMResult').dom.value != '')
									{
										//str = ' where VENDOR like ~%' + Ext.get('fieldcolVendorNameCMResult').dom.value + '%~';
                                                                                str = 'vendor like ~%' + Ext.get('fieldcolVendorNameCMResult').dom.value + '%~';
									};
						            GetLookupPersonExtCMResult(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMResult='colVendor';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell, record) 
				{
			        var str='';
					if (value != undefined)
					{
						if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};

					};
			        return str;
			    }
			},
			{
			    id: 'colContactPersonCraftPersonCMResult',
			    header: nmPersonNameResultCM,
			    width: 250,
			    dataIndex: 'PERSON_NAME',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolContactPersonCraftPersonCMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colDescCraftPersonExtCMResult',
			    header: nmDescResultCM,
			    width: 250,
			    dataIndex: 'DESC_SCH_PERSON',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescCraftPersonExtCMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(value, cell, record) 
				{
			       var str='';
					if (value != undefined)
					{
						if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};

					};
			        return str;
			    }
			},
			{
			    id: 'colCostCraftPersonExtCMResult',
			    header: nmCostResultCM,
			    width: 90,
			    dataIndex: 'COST',
				align:'right',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostCraftPersonExtCMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPersonCMResult(CurrentPersonExtCMResult.row,true);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.COST) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.COST);
					};
				}
			},
			{
			    id: 'colRealCostCraftPersonExtCMResult',
			    header: nmRealCostResultCM,
			    width: 150,
				align:'right',
			    dataIndex: 'REAL_COST',
				hidden:true,
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolRealCostCraftPersonExtCMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalRealCostPersonCMResult(CurrentPersonIntCMResult.row);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.REAL_COST) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.REAL_COST);
					};
				}
			}
		]
	)

};

function GetLookupPersonExtCMResult(str)
{
	if (cellSelectPersonExtCMResult.data.ROW_SCH === '' || cellSelectPersonExtCMResult.data.ROW_SCH === undefined)
	{
		var p = new mRecordPersonCMResult
		(
			{
				'RESULT_CM_ID':IdCMResult,
				'SCH_CM_ID':'',
				'SERVICE_ID':cellSelectServiceCMResult.data.SERVICE_ID,
				'ROW_SCH':'',
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonExtCMResult.data.items[CurrentPersonExtCMResult.row].data.COST,
				'REAL_COST':dsDtlCraftPersonExtCMResult.data.items[CurrentPersonIntCMResult.row].data.REAL_COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonExtCMResult.data.items[CurrentPersonExtCMResult.row].data.DESC_SCH_PERSON,
				'ROW':dsDtlCraftPersonExtCMResult.data.items[CurrentPersonExtCMResult.row].data.ROW,
				'ROW_RSLT':''
			}
		);
		FormLookupVendor(p,str,'',true,dsDtlCraftPersonExtCMResult,false);
	}
	else
	{	
		var p = new mRecordPersonCMResult
		(
			{
				'RESULT_CM_ID':IdCMResult,
				'SCH_CM_ID':'',
				'SERVICE_ID':cellSelectServiceCMResult.data.SERVICE_ID,
				'ROW_SCH':dsDtlCraftPersonExtCMResult.data.items[CurrentPersonExtCMResult.row].data.ROW_SCH,
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonExtCMResult.data.items[CurrentPersonExtCMResult.row].data.COST,
				'REAL_COST':dsDtlCraftPersonExtCMResult.data.items[CurrentPersonIntCMResult.row].data.REAL_COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonExtCMResult.data.items[CurrentPersonExtCMResult.row].data.DESC_SCH_PERSON,
				'ROW':dsDtlCraftPersonExtCMResult.data.items[CurrentPersonExtCMResult.row].data.ROW,
				'ROW_RSLT':dsDtlCraftPersonExtCMResult.data.items[CurrentPersonExtCMResult.row].data.ROW_RSLT
			}
		);
		FormLookupVendor(p,str,'',true,dsDtlCraftPersonExtCMResult,false);
	};
};

function PartInternalCMResultDetailColumModel() 
{
     return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdPartCMResult',
			    header: nmPartNumResultCM,
			    dataIndex: 'PART_ID',
			    width: 140,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdPartCMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListPartCMResult(false);
									
									if(Ext.get('fieldcolKdPartCMResult').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdCMResult + '~'
                                                                                str = 'category_id = ~' + varCatIdCMResult + '~'
										//str +=' and PART_ID like ~%' + Ext.get('fieldcolKdPartCMResult').dom.value + '%~';
                                                                                str +=' and part_id like ~%' + Ext.get('fieldcolKdPartCMResult').dom.value + '%~';
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdCMResult + '~'
                                                                                str = 'category_id = ~' + varCatIdCMResult + '~'
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									};
									GetLookupPartCMResult(str,CurrentPartCMResult.row,dsDtlPartCMResult);
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMResult='colPart';
							}
						}
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						str = "<div style='color:blue;'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colPartCMResult',
			    header: nmPartNameResultCM,
			    dataIndex: 'PART_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolPartCMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListPartCMResult(false);
									if(Ext.get('fieldcolPartCMResult').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdCMResult + '~'
                                                                                str = 'category_id = ~' + varCatIdCMResult + '~'
										//str += ' and PART_NAME like ~%' + Ext.get('fieldcolPartCMResult').dom.value + '%~';
                                                                                str += ' and part_name like ~%' + Ext.get('fieldcolPartCMResult').dom.value + '%~';
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdCMResult + '~'
                                                                                str = 'category_id = ~' + varCatIdCMResult + '~'
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									};
									GetLookupPartCMResult(str,CurrentPartCMResult.row,dsDtlPartCMResult);
						        };
						    },
							'focus' : function()
							{
								FocusCtrlCMResult='colPart';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell,record) 
				{
					var str='';
					if( value != undefined)
					{
						if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};
					};
			        return str;
			    }
			},
			{
			    id: 'colQtyPartCMResult',
			    header: nmQtyResultCM,
				align:'center',
			    width: 50,
			    dataIndex: 'QTY',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolQtyPartCMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPartCMResult(CurrentPartCMResult.row,false,true);
								}; 
							}
						}
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						str = "<div style='color:blue;'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colCostPartCMResult',
			    header: nmUnitCostResultCM,
			    width: 150,
				align:'right',
			    dataIndex: 'UNIT_COST',
				editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostPartCMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPartCMResult(CurrentPartCMResult.row,true,true);
								}; 
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.UNIT_COST) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.UNIT_COST);
					};
				}	
			},
			{
			    id: 'colTotCostPartInternalCMResult',
			    header: nmTotCostResultCM,
			    width: 150,
				align:'right',
			    dataIndex: 'TOT_COST',
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.UNIT_COST * record.data.QTY) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.UNIT_COST * record.data.QTY);
					};
				}	
			},
			{
			    id: 'colDescPartCMResult',
			    header: nmDescResultCM,
			    width: 200,
			    dataIndex: 'DESC_SCH_PART',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescPartCMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(value, params, record) 
				{
					var str='';
					if (value != undefined)
					{
						if (record.data.SCH_CM_ID != undefined && record.data.SCH_CM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};
					};
					return str ;
			    }
			}
		]
	)
};

function GetLookupPartCMResult(str,idx,ds)
{
    if (IdCMResult === '' || IdCMResult === undefined)
    {
        var p = new mRecordPartCMResult
        (
            {
                'RESULT_CM_ID':IdCMResult,
                'SCH_CM_ID':'',
                'SERVICE_ID':cellSelectServiceCMResult.data.SERVICE_ID,
                'PART_ID':'',
                'PART_NAME':'',
                'QTY':ds.data.items[idx].data.QTY,
                'UNIT_COST':ds.data.items[idx].data.UNIT_COST,
                'TOT_COST':ds.data.items[idx].data.UNIT_COST * ds.data.items[idx].data.QTY,//ds.data.items[idx].data.TOT_COST,
                'DESC_SCH_PART':ds.data.items[idx].data.DESC_SCH_PART,
                'TAG':1,
                'ROW': ds.data.items[idx].data.ROW
            }
        );
        FormLookupPart(str,ds,p,true,'',false);
    }
    else
    {
        var p = new mRecordPartCMResult
        (
            {
                'RESULT_CM_ID':IdCMResult,
                'SCH_CM_ID':'',
                'PART_ID':'',
                'SERVICE_ID':cellSelectServiceCMResult.data.SERVICE_ID,
                'PART_NAME':'',
                'QTY':ds.data.items[idx].data.QTY,
                'UNIT_COST':ds.data.items[idx].data.UNIT_COST,
                'TOT_COST':ds.data.items[idx].data.UNIT_COST * ds.data.items[idx].data.QTY,//ds.data.items[idx].data.TOT_COST,
                'DESC_SCH_PART':ds.data.items[idx].data.DESC_SCH_PART,
                'TAG':1,
                'ROW': ds.data.items[idx].data.ROW
            }
        );
        FormLookupPart(str,ds,p,false,idx,false);
    };
};


function CalcTotalPartCMResult(idx,mCost,mBolGrid)
{
	var total=Ext.num(0);
	for(var i=0;i < dsDtlPartCMResult.getCount();i++)
	{
		if (i === idx)
		{
			if (mCost===true)
			{
				if (Ext.get('fieldcolCostPartCMResult') != null) 
				{
					if (Ext.num(Ext.get('fieldcolCostPartCMResult').dom.value) != null) 
					{
						total += (Ext.num(Ext.get('fieldcolCostPartCMResult').dom.value) * Ext.num(dsDtlPartCMResult.data.items[i].data.QTY));
						
					};
			    };
			}
		    else
			{
			    if (Ext.get('fieldcolQtyPartCMResult') != null)
				{
			        if (Ext.num(Ext.get('fieldcolQtyPartCMResult').dom.value) != null) 
					{
			            total += (Ext.num(Ext.get('fieldcolQtyPartCMResult').dom.value) * Ext.num(dsDtlPartCMResult.data.items[i].data.UNIT_COST));
			        };
			    };
			}
		}
		else
		{
			total += Ext.num(dsDtlPartCMResult.data.items[i].data.UNIT_COST)*Ext.num(dsDtlPartCMResult.data.items[i].data.QTY);
		}
	}

	
	Ext.get('txtTotalPartCMResult').dom.value = formatCurrency(total);	
	if (mBolGrid === true)
	{
		Ext.get('txtTotalServiceRealCMResult').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalCMResultAllService(false,true)));
		Ext.get('txtLastCostCMResult').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalCMResultAllService(false,true)));
		
	};
	
};

function getItemPanelDescCMResult() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    labelWidth: 75,
				height:30,
			    items:
				[
				    {
				        xtype: 'textarea',
				        fieldLabel: nmWONotesResultCM + ' ',
				        name: 'txtDeskripsiCMResult',
				        id: 'txtDeskripsiCMResult',
				        scroll: true,
						readOnly:true,
				        anchor: '100% 90%'
				    }
				]
			}
		]

    }
	return items ;
};


function getItemPanelNoAssetCMResult(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 50,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoAssetCMResult2(lebar) 
				]
			}
		]
	}
    return items;
};

function getItemPanelVendorAppCMResult(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 57,
	    items:
		[
			{
			    columnWidth: .25,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmVendIDNameResultCM + ' ',
					    name: 'txtKdVendorCMResult',
					    id: 'txtKdVendorCMResult',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
						readOnly: true,
					    name: 'txtNamaVendorCMResult',
					    id: 'txtNamaVendorCMResult',
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype:'button',
						text:nmBtnVendorInfo,
						width:90,
						hideLabel:true,
						id: 'btnLookupVendorCMResult',
						handler:function() 
						{
							if(IsExtRepairCMResult === true)
							{
								if(dsLookVendorListCMResult != undefined)
								{
									if(dsLookVendorListCMResult.getCount() > 0 )
									{
										InfoVendorLookUp(dsLookVendorListCMResult.data.items[0])
									};
								};
							};
						}
					}
				]
			}
		]
	}
    return items;
};



function getItemPanelNoAssetCMResult2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 60,
	    items:
		[
			{
			    columnWidth: .23,
			    layout: 'form',
			    border: false,
				labelWidth:75,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmAssetIdNameEntryResultCM + ' ',
					    name: 'txtKdMainAssetCMResult',
					    id: 'txtKdMainAssetCMResult',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .32,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
						readOnly:true,
					    name: 'txtNamaMainAsetCMResult',
					    id: 'txtNamaMainAsetCMResult',
					    anchor: '99.99%'
					}
				]
			},
			{
			    columnWidth: .225,
			    layout: 'form',
				widthLabel:85,
			    border: false,
			    items:
				[
					{
						 xtype: 'textfield',
					    fieldLabel: nmStartDateResultCM + ' ',
					    id: 'dtpStartCMResult',
					    name: 'dtpStartCMResult',
					    readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .225,
			    layout: 'form',
			    border: false,
				labelWidth:70,
			    items:
				[
					{
						 xtype: 'textfield',
					    fieldLabel: nmFinishDateResultCM + ' ',
					    id: 'dtpFinishCMResult',
					    name: 'dtpFinishCMResult',
					    readOnly: true,
					    anchor: '97%'
					}
				]
			}
		]
	}
    return items;
};


function mComboMaksDataCMResult() 
{
    var cboMaksDataCMResult = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataCMResult',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmMaksData + ' ',
		    width: 50,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountCMResult,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountCMResult = b.data.displayText;
			        RefreshDataCMResultFilter();
			    }
			}
		}
	);
    return cboMaksDataCMResult;
};


function CMResultSave(mBol) 
{	
	if (IdCMResult == '' || IdCMResult === undefined) 
	{
		Ext.Ajax.request
		(
			{
				//url: "./Datapool.mvc/CreateDataObj",
                                url: baseURL + "index.php/main/CreateDataObj",
				params: getParamCMResult(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoCMResult(nmPesanSimpanSukses,nmHeaderSimpanData);
						RefreshDataCMResult();
						if(mBol === false)
						{
							IdCMResult=cst.ResultId;
							Ext.get('txtReffCMResult').dom.value=cst.Ref;
							LoadDataServiceResultCMResult(IdCMResult,varCatIdCMResult);
							LoadDataPartResultCMResult(IdCMResult,varCatIdCMResult);
							if (cellSelectServiceCMResult != '' && cellSelectServiceCMResult != undefined)
							{
								ReLoadDataPartResultCMResult(IdCMResult,varCatIdCMResult,cellSelectServiceCMResult.data.SERVICE_ID);
							};
							GetJumlahTotalResultPersonPartCMResult();
							
							if (IsExtRepairCMResult === false)
							{
								LoadDataEmployeeResultCMResult(IdCMResult,varCatIdCMResult);
								if (cellSelectServiceCMResult != '' && cellSelectServiceCMResult != undefined)
								{
									ReLoadDataEmployeeResultCMResult(IdCMResult,varCatIdCMResult,cellSelectServiceCMResult.data.SERVICE_ID);
								};
							}
							else
							{
								LoadDataVendorResultCMResult(IdCMResult,varCatIdCMResult);
								if (cellSelectServiceCMResult != '' && cellSelectServiceCMResult != undefined)
								{
									ReLoadDataVendorResultCMResult(IdCMResult,varCatIdCMResult,cellSelectServiceCMResult.data.SERVICE_ID);
								};
							};
						};
						AddNewCMResult = false;
					}
					else if  (cst.success === false && cst.pesan===0)
					{
						ShowPesanWarningCMResult(nmPesanSimpanGagal,nmHeaderSimpanData);
					}
					else 
					{
						ShowPesanErrorCMResult(nmPesanSimpanError,nmHeaderSimpanData);
					};
				}
			}
		)
	}
	else 
	{
		Ext.Ajax.request
		 (
			{
				//url: "./Datapool.mvc/CreateDataObj",
                                url: baseURL + "index.php/main/CreateDataObj",
				params: getParamCMResult(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoCMResult(nmPesanSimpanSukses,nmHeaderSimpanData);
						RefreshDataCMResult();
						if(mBol === false)
						{
							Ext.get('txtReffCMResult').dom.value=cst.Ref;
							LoadDataServiceResultCMResult(IdCMResult,varCatIdCMResult);
							LoadDataPartResultCMResult(IdCMResult,varCatIdCMResult);
							if (cellSelectServiceCMResult != '' && cellSelectServiceCMResult != undefined)
							{
								ReLoadDataPartResultCMResult(IdCMResult,varCatIdCMResult,cellSelectServiceCMResult.data.SERVICE_ID);
							};
							GetJumlahTotalResultPersonPartCMResult();
							
							if (IsExtRepairCMResult === false)
							{
								LoadDataEmployeeResultCMResult(IdCMResult,varCatIdCMResult);
								if (cellSelectServiceCMResult != '' && cellSelectServiceCMResult != undefined)
								{
									ReLoadDataEmployeeResultCMResult(IdCMResult,varCatIdCMResult,cellSelectServiceCMResult.data.SERVICE_ID);
								};
							}
							else
							{
								LoadDataVendorResultCMResult(IdCMResult,varCatIdCMResult);
								if (cellSelectServiceCMResult != '' && cellSelectServiceCMResult != undefined)
								{
									ReLoadDataVendorResultCMResult(IdCMResult,varCatIdCMResult,cellSelectServiceCMResult.data.SERVICE_ID);
								};
							};
						};
					}
					else if  (cst.success === false && cst.pesan===0)
					{
						ShowPesanWarningCMResult(nmPesanSimpanGagal,nmHeaderSimpanData);
					}
					else 
					{
						ShowPesanErrorCMResult(nmPesanSimpanError,nmHeaderSimpanData);
					};
				}
			}
		)
	};
};


function ShowPesanWarningCMResult(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorCMResult(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoCMResult(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function CMResultDelete() 
{
    if ((IdCMResult != null) && (IdCMResult != '') && (IdCMResult != undefined) )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmTitleFormResultCM) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								//url: "./Datapool.mvc/DeleteDataObj",
                                                                url: baseURL + "index.php/main/DeleteDataObj",
								params: getParamCMResult(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoCMResult(nmPesanHapusSukses,nmHeaderHapusData);
										CMResultAddNew();
										RefreshDataCMResult();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningCMResult(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorCMResult(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
};

function mComboCategoryCMResultView() 
{
	var Field = ['CATEGORY_ID','CATEGORY_NAME'];
	
    var dsCategoryCMResultView = new WebApp.DataStore({ fields: Field });
	
	Ext.TreeComboCMResultView = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treeCMResultView = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreeCMResultView  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						id:IdCategoryCMResultView,
						children: StrTreeComboCMResultView,
						autoScroll: true
					}
				) 
				treeCMResultView.setRootNode(rootTreeCMResultView); 
				
				this.list = treeCMResultView
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboCMResultView.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treeCMResultView
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				if (node.attributes.id === IdCategoryCMResultView)
				{
					this.setRawValue(valueCategoryCMResultView);
				}
				else
				{
					this.setRawValue(node.attributes.text);
				};
				
				selectCategoryCMResultView = node.attributes;
				
				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategoryCMResultView
		}
	);
			
    var cboCategoryCMResultView = new Ext.TreeComboCMResultView
    (
        {
            id:'cboCategoryCMResultView',
            fieldLabel: nmCatResultCM + ' ',
            width:120,
            value:valueCategoryCMResultView
        }
    );

     return cboCategoryCMResultView;
  
};

function GetRecordBaruPartCMResult()
{
    var p = new mRecordPartCMResult
    (
        {
            RESULT_CM_ID:'',
            SCH_CM_ID:'',
            SERVICE_ID: cellSelectServiceCMResult.data.SERVICE_ID,
            PART_ID :'',
            PART_NAME :'',
            QTY :1,
            UNIT_COST :0,
            TOT_COST:0,
            DESC_SCH_PART:'',
            TAG:1,
            ROW:GetUrutRowCMResult(dsDtlPartCMResult)
        }
    );
    return p;
};

function GetRecordBaruPersonCMResult(ds)
{
	var p = new mRecordPersonCMResult
	(
		{
			RESULT_CM_ID:'',
			SCH_CM_ID:'',
			SERVICE_ID:cellSelectServiceCMResult.data.SERVICE_ID,
			ROW_SCH :'',
		    EMP_ID :'',
			EMP_NAME:'',
		    VENDOR_ID :GetVendorTambahBarisCMResult(1),
			VENDOR:GetVendorTambahBarisCMResult(2),
		    PERSON_NAME :'',
			COST:'',
			REAL_COST:'',
			DESC_SCH_PERSON:'',
			ROW:GetUrutRowCMResult(ds),
			ROW_RSLT :''
		}
	);
	return p;
};

function GetVendorTambahBarisCMResult(x)
{
	var str='';
	if(IsExtRepairCMResult === true)
	{
		if(x=== 1)
		{
			str=Ext.get('txtKdVendorCMResult').dom.value;
		}
		else
		{
			str=Ext.get('txtNamaVendorCMResult').dom.value;
		};
	};
	return str;
};

function GetRecordBaruServiceCMResult()
{
	var p = new mRecordServiceCMResult
	(
		{
			RESULT_CM_ID:'',
			SCH_CM_ID:'',
			SERVICE_ID:'',
			SERVICE_NAME :'',
			CATEGORY_ID:varCatIdCMResult,
			TAG:1
		}
	);
	return p;
};

function GetNilaiCurrencyCMResult(dblNilai)
{
	for (var i = 0; i < dblNilai.length; i++) 
	{
		var y = dblNilai.substr(i, 1)
		if (y === '.') 
		{
			dblNilai = dblNilai.replace('.', '');
		}
	};
	
	return dblNilai;
};

function GetPersonIntServiceCMResult(service_id)
{
	if (service_id != '' && service_id != undefined)
	{
		dsDtlCraftPersonIntCMResult.removeAll();
		for (var i = 0; i < dsDtlCraftPersonIntCMResultTemp.getCount() ; i++) 
		{
			if(dsDtlCraftPersonIntCMResultTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlCraftPersonIntCMResult.insert(dsDtlCraftPersonIntCMResult.getCount(), dsDtlCraftPersonIntCMResultTemp.data.items[i]);
			};
		};
	};
};

function GetPersonExtServiceCMResult(service_id)
{
	if (service_id != '' && service_id != undefined)
	{
		dsDtlCraftPersonExtCMResult.removeAll();
		for (var i = 0; i < dsDtlCraftPersonExtCMResultTemp.getCount() ; i++) 
		{
			if(dsDtlCraftPersonExtCMResultTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlCraftPersonExtCMResult.insert(dsDtlCraftPersonExtCMResult.getCount(), dsDtlCraftPersonExtCMResultTemp.data.items[i]);
			};
		};
	};
};

function GetPartCMResult(service_id)
{
	if (service_id != '' && service_id != undefined)
	{
		dsDtlPartCMResult.removeAll();
		for (var i = 0; i < dsDtlPartCMResultTemp.getCount() ; i++) 
		{
			if(dsDtlPartCMResultTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlPartCMResult.insert(dsDtlPartCMResult.getCount(), dsDtlPartCMResultTemp.data.items[i]);
			};
		};
	};	
};


function HapusRowTempCMResult(ds,idx)
{
	if (cellSelectServiceCMResult != '' && cellSelectServiceCMResult.data != undefined)
	{
		if (cellSelectServiceCMResult.data.SERVICE_ID != undefined || cellSelectServiceCMResult.data.SERVICE_ID != '')
		{
			for (var i = 0; i < ds.getCount() ; i++) 
			{
				if(ds.data.items[i].data.SERVICE_ID === cellSelectServiceCMResult.data.SERVICE_ID && ds.data.items[i].data.ROW === idx)
				{
					ds.removeAt(i);
				};
			}
		};
	};
};

function GetStrListServiceCMResult()
{
	var str='';
	
	for (var i = 0; i < dsDtlServiceCMResult.getCount() ; i++) 
	{
		if(dsDtlServiceCMResult.getCount() === 1)
		{
			str +=  '\'' + dsDtlServiceCMResult.data.items[i].data.SERVICE_ID + '\'';
		}
		else
		{
			if (i === dsDtlServiceCMResult.getCount()-1 )
			{
				str +=  '\'' + dsDtlServiceCMResult.data.items[i].data.SERVICE_ID + '\'';
			}
			else
			{
				str +=  '\'' + dsDtlServiceCMResult.data.items[i].data.SERVICE_ID + '\'' + ',' ;
			};
		};
		// if (dsDtlServiceCMResult.data.items[i].data.SERVICE_ID != '')
		// {
			// if (i === dsDtlServiceCMResult.getCount()-2 || dsDtlServiceCMResult.getCount() === 1)
			// {
				// str +=  '\'' + dsDtlServiceCMResult.data.items[i].data.SERVICE_ID + '\'';
			// }
			// else
			// {
				// str +=  '\'' + dsDtlServiceCMResult.data.items[i].data.SERVICE_ID + '\'' + ',' ;
			// };
		// };
	}
	
	return str;
};


function GetUrutRowCMResult(ds)
{
	var x=1;
	if (ds != undefined && ds.data != undefined )
	{
		if ( ds.data.length > 0 )
		{
			x = ds.data.items[ds.getCount()-1].data.ROW + 1;
		};
	};
	
	return x;
};

function GetJumlahTotalPersonPartCMResult(mBolAwal)
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahWOTotalCostCM',
				//Params:	'where sch_cm_id =~' + SchIdCMResult + '~ and category_id =~' + varCatIdCMResult + '~'
                                Params:	'sch_cm_id = ~' + SchIdCMResult + '~ and category_id = ~' + varCatIdCMResult + '~'
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.get('txtTotalServiceCMResult').dom.value=formatCurrency(cst.TotalCost);
					if (mBolAwal=== true)
					{
						Ext.get('txtTotalServiceRealCMResult').dom.value=formatCurrency(cst.TotalCost);
						Ext.get('txtLastCostCMResult').dom.value=formatCurrency(cst.TotalCost);
					};
				}
				else
				{
					Ext.get('txtTotalServiceCMResult').dom.value=formatCurrency(0);
					if (mBolAwal=== true)
					{
						Ext.get('txtTotalServiceRealCMResult').dom.value=formatCurrency(0);
						Ext.get('txtLastCostCMResult').dom.value=formatCurrency(0);
					};
				};
			}

		}
	);
};

function GetJumlahTotalResultPersonPartCMResult()
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahResultTotalCM',
				//Params:	'where result_cm_id =~' + IdCMResult + '~ and category_id =~' + varCatIdCMResult + '~'
                                //Params:	'result_cm_id = ~' + IdCMResult + '~ and category_id = ~' + varCatIdCMResult + '~'
                                Params:	'sch_cm_id = ~' + IdCMResult + '~ and category_id = ~' + varCatIdCMResult + '~'
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.get('txtTotalServiceRealCMResult').dom.value=formatCurrency(cst.TotalCost);
				}
				else
				{
					Ext.get('txtTotalServiceRealCMResult').dom.value=formatCurrency(0);
				};
			}

		}
	);
};

function GetJumlahTotalResultPersonCMResult(str)
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahResultPersonCM',
				Params:	str
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.get('txtTotalPersonCMResult').dom.value=formatCurrency(cst.Jumlah);
				}
				else
				{
					Ext.get('txtTotalPersonCMResult').dom.value=formatCurrency(0);
				};
			}

		}
	);
};

function GetJumlahTotalResultPartCMResult()
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahResultPartCM',
				//Params:	'where result_cm_id=~' + IdCMResult + '~ and category_id=~' + varCatIdCMResult + '~ and service_id =~' + cellSelectServiceCMResult.data.SERVICE_ID + '~'
                                Params:	'result_cm_id = ~' + IdCMResult + '~ and category_id = ~' + varCatIdCMResult + '~ and service_id = ~' + cellSelectServiceCMResult.data.SERVICE_ID + '~'
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.get('txtTotalPartCMResult').dom.value=formatCurrency(cst.Jumlah);
				}
				else
				{
					Ext.get('txtTotalPartCMResult').dom.value=formatCurrency(0);
				};
			}

		}
	);
};

function GetVendorCMResult(mBolAwal)
{
	var strModul;
	var strParam;
	
	if(mBolAwal === true)
	{
		strModul='ProsesGetVendorCMApproveRequest';
		//strParam='where sch_cm_id=~' + SchIdCMResult + '~ and category_id=~' + varCatIdCMResult + '~';
                strParam='sch_cm_id = ~' + SchIdCMResult + '~ and category_id = ~' + varCatIdCMResult + '~';
	}
	else
	{
		strModul='ProsesGetVendorCMResult';
		//strParam='where result_cm_id=~' + IdCMResult + '~ and category_id=~' + varCatIdCMResult + '~';
                strParam='result_cm_id = ~' + IdCMResult + '~ and category_id = ~' + varCatIdCMResult + '~';
	};
	
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: strModul,
				Params:	strParam
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.get('txtKdVendorCMResult').dom.value=cst.Id;
					Ext.get('txtNamaVendorCMResult').dom.value=cst.Nama;
					LoadVendorInfoDetailCMResult(cst.Id);
				}
				else
				{
					Ext.get('txtKdVendorCMResult').dom.value='';
					Ext.get('txtNamaVendorCMResult').dom.value='';
				};
			}

		}
	);
};

function LoadVendorInfoDetailCMResult(id)
{
	var fldDetail = ['VENDOR_ID','VENDOR','CONTACT1','CONTACT2','VEND_ADDRESS','VEND_CITY','VEND_PHONE1','VEND_PHONE2','VEND_POS_CODE','COUNTRY'];
	
	dsLookVendorListCMResult = new WebApp.DataStore({ fields: fldDetail });
	
	dsLookVendorListCMResult.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'VENDOR_ID',
                                Sort: 'vendor_id',
				Sortdir: 'ASC',
				target: 'LookupVendor',
				//param: 'where vendor_id = ~' + id + '~'
                                param: 'vendor_id = ~' + id + '~'
			}
		}
	);
	return dsLookVendorListCMResult;
};

function GetStrTreeComboCMResultView()
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetDataTreeComboSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeComboCMResultView= cst.arr;
			},
			failure:function()
			{
			    
			}
		}
	);
};









