var selectFilterSchViewScheduleCM=1;
var selectAssetViewScheduleCM;
var dsAssetViewScheduleCM;
var dsTRScheduleCMList;
var AddNewScheduleCM = true;
var selectCountScheduleCM = 50;
var now = new Date();
var rowSelectedScheduleCM;
var cellSelectedScheduleCMDet;
var TRScheduleCMLookUps;
var selectRepairViewScheduleCM;
var valueCategoryViewScheduleCM=' All';
var selectCategoryViewScheduleCM=' 9999';
var DefaultSelectCategoryViewScheduleCM=' 9999';
var varSchCMId;
var valueListStatusViewScheduleCM ="('4','5')";
var selectStatusViewScheduleCM ;
var dsStatusViewScheduleCM;
var StrTreeComboViewScheduleCM;
var rootTreeViewScheduleCM;
var treeViewScheduleCM;
var IdCategoryViewScheduleCM=' 9999';


CurrentPage.page = getPanelScheduleCM(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


function getPanelScheduleCM(mod_id) 
{
    var Field = ['Status','ASSET_MAINT','ASSET_MAINT_ID','ASSET_MAINT_NAME','PROBLEM','SCH_DUE_DATE','SCH_FINISH_DATE','SCH_CM_ID','APP_ID','IS_EXT_REPAIR','STATUS_ID','CATEGORY_ID','DESC_SCH_CM','EMP_ID'];

    dsTRScheduleCMList = new WebApp.DataStore({ fields: Field });
    GetStrTreeComboViewScheduleCM();
    var grListTRScheduleCM = new Ext.grid.EditorGridPanel
	(
		{
		    stripeRows: true,
		    store: dsTRScheduleCMList,
		    anchor: '100% 91.9999%',
		    columnLines: true,
		    border: false,
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
					        rowSelectedScheduleCM = dsTRScheduleCMList.getAt(row);
					    }
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedScheduleCM = dsTRScheduleCMList.getAt(ridx);
					if (rowSelectedScheduleCM != undefined) 
					{
						ScheduleCMLookUp(rowSelectedScheduleCM.data);
					}
					else 
					{
						ShowPesanWarningViewScheduleCM(nmWarningSelectEditData,nmEditData)
					};
				}
			},
		    cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'colStatusImageSchViewScheduleCM',
					    header: nmStatusScheduleCM,
					    dataIndex: 'Status',
					    sortable: true,
					    width: 50,
						align:'center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store) 
						{
							 switch (value) 
							 {
								 case 1:  
									 metaData.css = 'StatusHijau';
									 break;
								 case 2:   
									 metaData.css = 'StatusKuning';
									 break;
								 case 3:  
									metaData.css = 'StatusMerah';
								case 4:  
									metaData.css = 'StatusBiru';	
									 break;
							 }
								return '';     
						}
					},
					{
					    header: nmAssetScheduleCM,
					    width: 250,
					    sortable: true,
					    dataIndex: 'ASSET_MAINT',
					    id: 'colNamaAsetMaintViewScheduleCM'
					},
					{
					    id: 'colProblemViewScheduleCM',
					    header: nmProblemScheduleCM,
					    dataIndex: 'PROBLEM',
					    width: 400
					},
					{
						id: 'colTglMulaiViewScheduleCM',
						header: nmStartDateScheduleCM,
						dataIndex: 'SCH_DUE_DATE',
						width: 130,
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.SCH_DUE_DATE);
						}
					},
					{
					    id: 'colTglSelesaiViewScheduleCM',
					    header: nmFinishDateScheduleCM,
					    dataIndex: 'SCH_FINISH_DATE',
					    width: 130,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.SCH_FINISH_DATE);
					    }
					}
				]
			),
		    tbar:
			[
				{
				    id: 'btnEditScheduleCM',
				    text: nmEditData,
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						// new Ext.ux.Notification({
						// iconCls:	'x-icon-error',
						// title:	  'Ruh-row',
						// html:		'This is just a stub.  This is only a stub.  If this would have been a real functioning doo-dad, you never would have even seen this stub.',
						// autoDestroy: true,
						// hideDelay:  5000
					// }).show(document); 

						if (rowSelectedScheduleCM != undefined) 
						{
							ScheduleCMLookUp(rowSelectedScheduleCM.data);
						}
						else 
						{
							ShowPesanWarningViewScheduleCM('Please select row to edit','Edit Data')
						};
				    }
				}, ' ', '-',
				{
					xtype: 'checkbox',
					id: 'chkWithTglScheduleCM',
					hideLabel:true,
					checked: false,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilterScheduleCM').dom.disabled=false;
							Ext.get('dtpTglAkhirFilterScheduleCM').dom.disabled=false;
							
						}
						else
						{
							Ext.get('dtpTglAwalFilterScheduleCM').dom.disabled=true;
							Ext.get('dtpTglAkhirFilterScheduleCM').dom.disabled=true;
							
							
						};
					}
				}
				, ' ', '-', nmStartDateScheduleCM + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmStartDateScheduleCM + ' ',
				    id: 'dtpTglAwalFilterScheduleCM',
				    format: 'd/M/Y',
				    value: now,
				    width: 100,
				    onInit: function() { }
				}, ' ', ' ' + nmSd + ' ', ' ', {
				    xtype: 'datefield',
				    fieldLabel: nmSd + ' ',
				    id: 'dtpTglAkhirFilterScheduleCM',
				    format: 'd/M/Y',
				    value: now,
				    width: 100
				}
			]
		}
	);
	
	var LegendViewCMSchedule = new Ext.Panel
	(
		{
		    id: 'LegendViewCMSchedule',
		    region: 'center',
			border:false,
			bodyStyle: 'padding:0px 7px 0px 7px',
		    layout: 'column',
			frame:true,
			//height:32,
			anchor: '100% 8.0001%',
			autoScroll:false,
			items:
			[	
				{
					columnWidth: .033,
					layout: 'form',
					style:{'margin-top':'-1px'},
					//height:32,
					anchor: '100% 8.0001%',
					border: false,
					html: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
				},
				{
					columnWidth: .08,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style:{'margin-top':'1px'},
					border: false,
					html: " " + nmLegFutureScheduleCM
				},
				{
					columnWidth: .033,
					layout: 'form',
					style:{'margin-top':'-1px'},
					border: false,
					//height:32,
					anchor: '100% 8.0001%',
					html: '<img src="'+baseURL+'ui/images/icons/16x16/kuning.png" class="text-desc-legend"/>'
				},
				{
					columnWidth: .13,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style:{'margin-top':'1px'},
					border: false,
					html: " " + nmLegOnSchScheduleCM
				},
				{
					columnWidth: .033,
					layout: 'form',
					style:{'margin-top':'-1px'},
					border: false,
					//height:35,
					anchor: '100% 8.0001%',
					html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
				},
				{
					columnWidth: .1,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style:{'margin-top':'1px'},
					border: false,
					html: " " + nmLegLostScheduleCM
				},
				{
					columnWidth: .033,
					layout: 'form',
					style:{'margin-top':'-1px'},
					border: false,
					//height:35,
					anchor: '100% 8.0001%',
					html: '<img src="'+baseURL+'ui/images/icons/16x16/biru.png" class="text-desc-legend"/>'
				},
				{
					columnWidth: .13,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style:{'margin-top':'1px'},
					border: false,
					html: " " + nmLegCloseSchPM
				}
			]
		
		}
	)
	
	 var FormPanelCardLayoutScheduleCM = new Ext.Panel
	(
		{
		    id: 'FormPanelCardLayoutScheduleCM',
		    trackResetOnLoad: true,
		    width: 150,
			height: 20,
			layout: 'card',
			border:false,
			activeItem: 0,
			items: 
			[
				mComboCategoryViewScheduleCM(),mComboStatusViewScheduleCM(),mComboRepairViewScheduleCM()
			]
		}
	);

    var FormTRScheduleCM = new Ext.Panel
	(
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleScheduleCM,
		    border: false,
		    shadhow: true,
		    iconCls: 'ScheduleCM',
		    margins: '0 5 5 0',
		    items: [grListTRScheduleCM,LegendViewCMSchedule],
		    tbar:
			[
				nmFilterScheduleCM + ' : ',mComboFilterViewScheduleCM(),' ','-', ' ' ,FormPanelCardLayoutScheduleCM,
				' ', '-',
					nmMaksData + ' : ', ' ', mComboMaksDataScheduleCM(),
					' ', '-',
				{
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    anchor: '25%',
				    handler: function(sm, row, rec) 
					{
				        RefreshDataScheduleCMFilter();
				    }
				}
			],
		    listeners:
			{
			    'afterrender': function() 
				{
			    }
			}
		}
	);
   RefreshDataScheduleCM();
   RefreshDataScheduleCM();

	
    return FormTRScheduleCM

};

function ScheduleCMLookUp(rowdata) 
{
    var lebar = 600;
    GetStrTreeComboViewScheduleCM();
    TRScheduleCMLookUps = new Ext.Window
	(
		{
		    id: 'TRScheduleCMLookUps',
		    title: nmTitleScheduleCM,
		    closeAction: 'destroy',
		    width: lebar,
		    height: 257,
		    border: false,
			resizable:false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'ScheduleCM',
		    modal: true,
		    items: getFormEntryTRScheduleCM(lebar),
		    listeners:
			{
			    activate: function() 
				{
			    },
			    afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedScheduleCM=undefined;
					RefreshDataScheduleCMFilter();
				}
			}
		}
	);

    TRScheduleCMLookUps.show();
    if (rowdata == undefined) 
	{
        //ScheduleCMAddNew();
    }
    else 
	{
        TRScheduleCMInit(rowdata)
    };
};

function TRScheduleCMInit(rowdata)
{
	Ext.get('txtKdMainAssetScheduleCM').dom.value=rowdata.ASSET_MAINT_ID;
	Ext.get('txtNamaMainAsetScheduleCM').dom.value=rowdata.ASSET_MAINT_NAME;
	Ext.get('txtProblemScheduleCM').dom.value=rowdata.PROBLEM;
	Ext.get('dtpPengerjaanScheduleCMEntry').dom.value=ShowDate(rowdata.SCH_DUE_DATE);
	Ext.get('dtpSelesaiScheduleCMEntry').dom.value=ShowDate(rowdata.SCH_FINISH_DATE);
	varSchCMId=rowdata.SCH_CM_ID;
};

function RefreshDataScheduleCM() 
{
    dsTRScheduleCMList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: selectCountScheduleCM,
			    //Sort: 'SCH_CM_ID',
                            Sort: 'sch_cm_id',
			    Sortdir: 'ASC',
			    target: 'ViewScheduleCM',
			    param: ''
			}
		}
	);
    return dsTRScheduleCMList;
};

function RefreshDataScheduleCMFilter() 
{
    var KataKunci='';
	
	if (selectFilterSchViewScheduleCM === 3)
	{
		//KataKunci = ' where is_ext_repair =  ' + selectRepairViewScheduleCM ;
                KataKunci = 'is_ext_repair = ' + selectRepairViewScheduleCM ;
	}
	else if (selectFilterSchViewScheduleCM === 1)
	{
		if (Ext.get('cboCategoryViewScheduleCM').getValue() != '' && selectCategoryViewScheduleCM != undefined)
		{ 
			if (selectCategoryViewScheduleCM.id != undefined)
			{
				if(selectCategoryViewScheduleCM.id != DefaultSelectCategoryViewScheduleCM)
				{
					if (selectCategoryViewScheduleCM.leaf === false)
					{
						//KataKunci =' where left(category_id,' + selectCategoryViewScheduleCM.id.length + ')=' +  selectCategoryViewScheduleCM.id
                                                KataKunci ='substring( category_id,1,' + selectCategoryViewScheduleCM.id.length + ')=' +  selectCategoryViewScheduleCM.id
					}
					else
					{
						//KataKunci =' WHERE Category_id=~'+ selectCategoryViewScheduleCM.id + '~'
                                                KataKunci ='category_id = ~'+ selectCategoryViewScheduleCM.id + '~'
					};
				};
			};
		};
		// if (selectCategoryViewScheduleCM != DefaultSelectCategoryViewScheduleCM)
		// {
			// KataKunci = ' where category_id =  ~' + selectCategoryViewScheduleCM + '~';
		// };
	}
	else if (selectFilterSchViewScheduleCM === 2)
	{
		//KataKunci = ' where status_id =  ~' + selectStatusViewScheduleCM + '~';
                KataKunci = 'status_id = ~' + selectStatusViewScheduleCM + '~';
	};
	
	if( Ext.get('chkWithTglScheduleCM').dom.checked === true )
	{
			KataKunci += '@^@' + Ext.get('dtpTglAwalFilterScheduleCM').getValue() + '&&'+ Ext.get('dtpTglAkhirFilterScheduleCM').getValue();
	};
        
    if (KataKunci != undefined) 
    {  
			dsTRScheduleCMList.load
			(
				{
					params:
					{
						Skip: 0,
						Take: selectCountScheduleCM,
						//Sort: 'SCH_CM_ID',
                                                Sort: 'sch_cm_id',
						Sortdir: 'ASC',
						target: 'ViewScheduleCM',
						param:KataKunci
					}
				}
			);  
    }
	else
	{
		RefreshDataScheduleCM();
	};
    
	return dsTRScheduleCMList;
    
};

function getFormEntryTRScheduleCM(lebar) 
{
    var pnlTRScheduleCM = new Ext.FormPanel
	(
		{
		    id: 'PanelTRScheduleCM',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    bodyStyle: 'padding:10px 10px 0px 10px',
			height:518,
		    anchor: '100%',
		    width: lebar,
		    border: false,
		    items: [getItemPanelInputScheduleCM(lebar)],
		    tbar:
			[
				{
				    text: nmTambah,
				    id: 'btnTambahScheduleCM',
				    tooltip: nmTambah,
				    iconCls: 'add',
					disabled:true,
				    handler: function() 
					{
				    }
				}, '-',
				{
				    text: nmSimpan,
				    id: 'btnSimpanScheduleCM',
				    tooltip: nmSimpan,
				    iconCls: 'save',
				    handler: function() 
					{
				        ScheduleCMSave(false);
				        RefreshDataScheduleCMFilter();
				    }
				}, '-',
				{
				    text: nmSimpanKeluar,
				    id: 'btnSimpanKeluarScheduleCM',
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{
						var x =  ScheduleCMSave(true);
				         RefreshDataScheduleCMFilter();
						if (x===undefined)
						{
							varSchCMId=undefined;
							TRScheduleCMLookUps.close();
						}; 
				    }
				}, '-',
				{
				    text: nmHapus,
				    id: 'btnHapusScheduleCM',
				    tooltip: nmHapus,
				    iconCls: 'remove',
					disabled:true,
				    handler: function() 
					{
				    }
				}, '-',
				{
				    text: nmLookup,
				    id: 'btnLookupScheduleCM',
				    tooltip: nmLookup,
				    iconCls: 'find',
					disabled:true,
				    handler: function() 
					{
				    }
				}, 
				'-', '->', '-',
				{
				    text: nmCetak,
					id:'btnCetakScheduleCM',
				    tooltip: nmCetak,
					disabled:true,
				    iconCls: 'print',
				    handler: function()
					{ }
				}
			]
		}
	); 
	

    var FormTRScheduleCM = new Ext.Panel
	(
		{
		    id: 'FormTRScheduleCM',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRScheduleCM]
		}
	);

    return FormTRScheduleCM
};

function getItemPanelInputScheduleCM(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
		border:false,
		height:734,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar-35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype:'fieldset',
						title: nmTitleInfoAssetScheduleCM,	
						anchor:  '99.99%',
						height:'100px',
						items :
						[
							getItemPanelNoAssetScheduleCM(lebar),
							{
								xtype: 'textarea',
								fieldLabel: nmProblemScheduleCM + ' ',
								name: 'txtProblemScheduleCM',
								id: 'txtProblemScheduleCM',
								scroll:true,
								readOnly:true,
								anchor: '97.7% 60%'
				            }
						]
					},
					{
						xtype:'fieldset',
						title: nmTitleScheduleScheduleCM,	
						anchor:  '99.99%',
						height:'50px',
						items :
						[
							getItemPanelTanggalScheduleCMEntry(lebar)
						]
					}
				]
			}
		]
	}
	
	return items;
};

function getItemPanelNoAssetScheduleCM(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 70,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoAssetScheduleCM2(lebar) 
				]
			}
		]
	}
    return items;
};

function getItemPanelNoAssetScheduleCM2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 70,
	    items:
		[
			{
			    columnWidth: .4,
			    layout: 'form',
			    border: false,
				labelWidth:100,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmAssetScheduleCM + ' ',
					    name: 'txtKdMainAssetScheduleCM',
					    id: 'txtKdMainAssetScheduleCM',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
						readOnly:true,
					    name: 'txtNamaMainAsetScheduleCM',
					    id: 'txtNamaMainAsetScheduleCM',
					    anchor: '99.99%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelTanggalScheduleCMEntry(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 70,
	    items:
		[
			{
			    columnWidth: .5,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'datefield',
					    fieldLabel: nmStartDateScheduleCM + ' ',
					    id: 'dtpPengerjaanScheduleCMEntry',
					    name: 'dtpPengerjaanScheduleCMEntry',
					    format: 'd/M/Y',
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .5,
			    layout: 'form',
			    border: false,
				labelWidth:80,
			    items:
				[
					{
						xtype: 'datefield',
					    fieldLabel: nmFinishDateScheduleCM + ' ',
					    id: 'dtpSelesaiScheduleCMEntry',
					    name: 'dtpSelesaiScheduleCMEntry',
					    format: 'd/M/Y',
					    anchor: '98%'
					}
				]
			}
		]
	}
    return items;
};

function mComboMaksDataScheduleCM() 
{
    var cboMaksDataScheduleCM = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataScheduleCM',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmMaksData + ' ',
		    width: 50,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountScheduleCM,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountScheduleCM = b.data.displayText;
			        RefreshDataScheduleCMFilter();
			    }
			}
		}
	);
    return cboMaksDataScheduleCM;
};


function mComboFilterViewScheduleCM() 
{
    var cboFilterViewScheduleCM = new Ext.form.ComboBox
	(
		{
		    id: 'cboFilterViewScheduleCM',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmFilterScheduleCM + ' ',
		    width: 150,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 'Asset Category'], [2, 'Status'],[3, 'Repair']]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
			value:'Asset Category',
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectFilterSchViewScheduleCM = b.data.Id;
					if (selectFilterSchViewScheduleCM === 1)
					{
						Ext.getCmp('FormPanelCardLayoutScheduleCM').getLayout().setActiveItem(0);
						selectCategoryViewScheduleCM=DefaultSelectCategoryViewScheduleCM;
						Ext.get('cboCategoryViewScheduleCM').dom.value=valueCategoryViewScheduleCM;
					}
					else if (selectFilterSchViewScheduleCM === 2)
					{
						selectStatusViewScheduleCM=undefined;
						Ext.getCmp('FormPanelCardLayoutScheduleCM').getLayout().setActiveItem(1);
						Ext.get('cboStatusViewScheduleCM').dom.value='';
					}
					else if (selectFilterSchViewScheduleCM === 3)
					{
						selectRepairViewScheduleCM=undefined;
						Ext.getCmp('FormPanelCardLayoutScheduleCM').getLayout().setActiveItem(2);
						Ext.get('cboRepairViewScheduleCM').dom.value='';
					};
					   RefreshDataScheduleCM();
			    }
			}
		}
	);
    return cboFilterViewScheduleCM;
};


function mComboRepairViewScheduleCM() 
{
    var cboRepairViewScheduleCM = new Ext.form.ComboBox
	(
		{
		    id: 'cboRepairViewScheduleCM',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: '',
		    align: 'Right',
		    //anchor:'100%',
			width:150,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'KD_REPAIR',
						'REPAIR'
					],
				    data: [[0, 'Internal'], [1, 'External']]
				}
			),
		    valueField: 'KD_REPAIR',
		    displayField: 'REPAIR',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectRepairViewScheduleCM = b.data.KD_REPAIR;
			    }
			}
		}
	);

    return cboRepairViewScheduleCM;
};

function mComboCategoryViewScheduleCM() 
{
	var Field = ['CATEGORY_ID', 'CATEGORY_NAME'];
	dsCategoryViewScheduleCM = new WebApp.DataStore({ fields: Field });
	
	Ext.TreeComboViewScheduleCM = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treeViewScheduleCM = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreeViewScheduleCM  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						id:IdCategoryViewScheduleCM,
						children: StrTreeComboViewScheduleCM,
						autoScroll: true
					}
				) 
				treeViewScheduleCM.setRootNode(rootTreeViewScheduleCM); 
				
				this.list = treeViewScheduleCM
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboViewScheduleCM.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treeViewScheduleCM
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				if (node.attributes.id === IdCategoryViewScheduleCM)
				{
					this.setRawValue(valueCategoryViewScheduleCM);
				}
				else
				{
					this.setRawValue(node.attributes.text);
				};
				
				selectCategoryViewScheduleCM = node.attributes;
				
				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategoryViewScheduleCM
		}
	);
	
	
	
	var cboCategoryViewScheduleCM = new Ext.TreeComboViewScheduleCM
	(
		{
			id:'cboCategoryViewScheduleCM',
			fieldLabel: nmCatScheduleCM + ' ',
			width:150,
			value:valueCategoryViewScheduleCM
		}
	);
	
	// var Field = ['CATEGORY_ID','CATEGORY_NAME'];
	
    // var dsCategoryViewScheduleCM = new WebApp.DataStore({ fields: Field });
    // dsCategoryViewScheduleCM.load
	// (
		// {
		    // params:
			// {
			    // Skip: 0,
			    // Take: 1000,
			    // Sort: 'CATEGORY_ID',
			    // Sortdir: 'ASC',
			    // target: 'ViewComboCategoryVw',
			    // param: ''
			// }
		// }
	// );
	
    // var cboCategoryViewScheduleCM = new Ext.form.ComboBox
	// (
		// {
		    // id: 'cboCategoryViewScheduleCM',
		    // typeAhead: true,
		    // triggerAction: 'all',
		    // lazyRender: true,
		    // mode: 'local',
		    // emptyText: '',
		    // fieldLabel: '',
		    // align: 'Right',
		    // //anchor:'60%',
			// width:150,
		    // store: dsCategoryViewScheduleCM,
		    // valueField: 'CATEGORY_ID',
		    // displayField: 'CATEGORY_NAME',
			// value:valueCategoryViewScheduleCM,
		    // listeners:
			// {
			    // 'select': function(a, b, c) 
				// {
			        // selectCategoryViewScheduleCM = b.data.CATEGORY_ID;
			    // }
			// }
		// }
	// );
	
    return cboCategoryViewScheduleCM;
};

function mComboStatusViewScheduleCM() 
{
	var Field = ['STATUS_ID','STATUS_NAME'];
	
    dsStatusViewScheduleCM = new WebApp.DataStore({ fields: Field });
    dsStatusViewScheduleCM.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ID',
                            Sort: 'status_id',
			    Sortdir: 'ASC',
			    target: 'ViewComboStatusVw',
			    //param: 'where status_id in ' + valueListStatusViewScheduleCM
                            param: "status_id IN " + valueListStatusViewScheduleCM
			}
		}
	);
	
    var cboStatusViewScheduleCM = new Ext.form.ComboBox
	(
		{
		    id: 'cboStatusViewScheduleCM',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmStatusScheduleCM + ' ',
		    align: 'Right',
		    anchor:'60%',
		    store: dsStatusViewScheduleCM,
		    valueField: 'STATUS_ID',
		    displayField: 'STATUS_NAME',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectStatusViewScheduleCM = b.data.STATUS_ID;
			    }
			}
		}
	);
	
    return cboStatusViewScheduleCM;
};

function ShowPesanWarningViewScheduleCM(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorViewScheduleCM(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoViewScheduleCM(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ScheduleCMSave(mBol) 
{
	Ext.Ajax.request
	 (
		{
			//url: "./Datapool.mvc/UpdateDataObj",
                        url: baseURL + "index.php/main/UpdateDataObj",
			params: getParamScheduleCM(),
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoViewScheduleCM(nmPesanEditSukses,nmHeaderEditData);
					RefreshDataScheduleCM();
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningViewScheduleCM(nmPesanEditGagal,nmHeaderEditData);
				}
				else 
				{
					ShowPesanErrorViewScheduleCM(nmPesanEditError,nmHeaderEditData);
				};
			}
		}
	)
};

function getParamScheduleCM() 
{
    var params =
	{	
		Table: 'ViewScheduleCM',   
	    SchId: varSchCMId,
	    DueDate: Ext.get('dtpPengerjaanScheduleCMEntry').getValue(),
		FinishDate: Ext.get('dtpSelesaiScheduleCMEntry').getValue()
	};
    return params
};

function GetStrTreeComboViewScheduleCM()
 {
    Ext.Ajax.request
	 (
		{
		    //url: "./Module.mvc/ExecProc",
                    url: baseURL + "index.php/main/ExecProc",
		    params:
			{
			    UserID: 'Admin',
			    ModuleID: 'ProsesGetDataTreeComboSetCat',
			    Params: ''
			},
		    success: function(resp) {
		        var cst = Ext.decode(resp.responseText);
		        StrTreeComboViewScheduleCM = cst.arr;
		    },
		    failure: function() {

		    }
		}
	);
};










