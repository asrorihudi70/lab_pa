var CurrentServiceCMWorkOrder =
{
    data: Object,
    details: Array,
    row: 0
};

// var select & value combo ==============================
var selectStatusCMWorkOrderEntry;
var selectCountCMWorkOrder = 50;
var selectCategoryCMWorkOrderView=' 9999';
var selectRepairCMWorkOrderView=99;
var selectStatusViewCMWorkOrder;
var valueCategoryCMWorkOrderView=' All';
var valueRepairCMWorkOrderView=' All';
var valueListStatusViewCMWorkOrder ='(4,5)';
//=================================================

// var select grid ==============================
var rowSelectedCMWorkOrder;
var rowSelectedCMWorkOrderTemp;
var cellSelectServiceCMWorkOrder;
//================================================


// var data store ===========================
var dsTRCMWorkOrderList;
var dsDtlServiceCMWorkOrder;
var dsDtlCraftPersonExtCMWorkOrder;
var dsDtlCraftPersonIntCMWorkOrder;
var dsDtlCraftPersonExtCMWorkOrderTemp;
var dsDtlCraftPersonIntCMWorkOrderTemp;
var dsDtlPartCMWorkOrder;
var dsDtlPartCMWorkOrderTemp;
var dsStatusViewCMWorkOrder;
var dsLookVendorListCMWorkOrder;
//==========================================



//var component===========================
var TRCMWorkOrderLookUps;
var vTabPanelRepairInternalCMWorkOrder ;
var vTabPanelRepairExternalCMWorkOrder ;
//=========================================


var AddNewCMWorkOrder = true;
var IdCMWorkOrder;
var SchIdCMWorkOrder;
var varCatIdCMWorkOrder;
var IsExtRepairCMWorkOrder;
var NowCMWorkOrder = new Date();
var varTabPartCMWorkOrder=1;
var FocusCtrlCMWorkOrder;
var StrTreeComboCMWorkOrderView;
var rootTreeCMWorkOrderView;
var treeCMWorkOrderView;
var IdCategoryCMWorkOrderView=' 9999';

CurrentPage.page = getPanelCMWorkOrder(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelCMWorkOrder(mod_id) 
{
    var Field =['WO_CM_ID','PIC_ID_WO','PIC_NAME_WO','DESC_WO_CM','SCH_CM_ID','STATUS_ID','WO_CM_DATE','WO_CM_FINISH_DATE','SCH_DUE_DATE','SCH_FINISH_DATE','DESC_SCH_CM','ASSET_MAINT_ID','ASSET_MAINT_NAME','CATEGORY_ID','IS_EXT_REPAIR','ASSET_MAINT','PROBLEM','APP_ID','VENDOR_ID','ROW_REQ','REQ_ID','SchID'];
    dsTRCMWorkOrderList = new WebApp.DataStore({ fields: Field });
	GetStrTreeComboCMWorkOrderView();
	 var chkInternalCMWorkOrder = new Ext.grid.CheckColumn
	(
		{
			id: 'chkInternalCMWorkOrder',
			header: nmExternalWOCM,
			align: 'center',
			disabled:true,
			dataIndex: 'IS_EXT_REPAIR',
			width: 70
		}
	);

    var grListTRCMWorkOrder = new Ext.grid.EditorGridPanel
	(
		{
		    stripeRows: true,
		    store: dsTRCMWorkOrderList,
		    anchor: '100% 92%',
		    columnLines: true,
		    border: false,
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
					        rowSelectedCMWorkOrder = dsTRCMWorkOrderList.getAt(row);
					    }
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedCMWorkOrder = dsTRCMWorkOrderList.getAt(ridx);
					if (rowSelectedCMWorkOrder != undefined) 
					{
						CMWorkOrderLookUp(rowSelectedCMWorkOrder.data);
					}
					else 
					{
						ShowPesanWarningCMWorkOrder(nmWarningSelectEditData,nmEditData)
					};
				}
			},
		    cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'colWOImageCMWorkOrderView',
					    header: nmStatusWOCM,
					    dataIndex: 'WO_CM_ID',
					    sortable: true,
					    width: 50,
						align:'center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store) 
						{
							if ( value === null )
							{
								metaData.css = 'BeforeWO';
							}
							else
							{
								metaData.css = 'WO';
							};
								return '';     
						}
					},
					{
					    id: 'colWOIdViewCMWorkOrder', //+ Code
					    header: nmWOIDCM,
					    dataIndex: 'WO_CM_ID',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'colWODateViewCMWorkOrder',
					    header: nmWODateCM,
					    dataIndex: 'WO_CM_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
							if (record.data.WO_CM_DATE != null)
							{
								return ShowDate(record.data.WO_CM_DATE);
							}
							else
							{
								return '';
							}
					    }
					},
					{
					    id: 'colWODateViewCMWorkOrder', //+ Code
					    header: nmAsetWOCM,
					    dataIndex: 'ASSET_MAINT',
					    sortable: true,
					    width: 250
					},
					{
					    id: 'colStartDateViewCMWorkOrder',
					    header: nmStartDateWOCM,
					    dataIndex: 'SCH_DUE_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.SCH_DUE_DATE);
					    }
					},
					{
					    id: 'colFinishDateViewCMWorkOrder',
					    header: nmFinishDateWOCM,
					    dataIndex: 'SCH_FINISH_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.SCH_FINISH_DATE);
					    }
					},
					{
					    id: 'colDescViewCMWorkOrder',
					    header: nmDescWOCM,
					    dataIndex: 'DESCRIPTION',
					    width: 300
					},chkInternalCMWorkOrder
				]
			),
		    tbar:
			[
				{
				    id: 'btnEditCMWorkOrder',
				    text: nmEditData,
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedCMWorkOrder != undefined) 
						{
							CMWorkOrderLookUp(rowSelectedCMWorkOrder.data);
						}
						else 
						{
							ShowPesanWarningCMWorkOrder(nmWarningSelectEditData,nmEditData)
						}
				    }
				}, ' ', '-',
				{
					xtype: 'checkbox',
					id: 'chkWithTglCMWorkOrder',
					hideLabel:true,
					checked: false,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilterCMWorkOrder').dom.disabled=false;
							Ext.get('dtpTglAkhirFilterCMWorkOrder').dom.disabled=false;
							
						}
						else
						{
							Ext.get('dtpTglAwalFilterCMWorkOrder').dom.disabled=true;
							Ext.get('dtpTglAkhirFilterCMWorkOrder').dom.disabled=true;
							
							
						};
					}
				}
				, ' ', '-', nmWODateCM + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmWODateCM + ' ',
				    id: 'dtpTglAwalFilterCMWorkOrder',
				    format: 'd/M/Y',
				    value: NowCMWorkOrder,
				    width: 100,
				    onInit: function() { }
				}, ' ', ' ' + nmSd + ' ', ' ', {
				    xtype: 'datefield',
				    fieldLabel: nmSd + ' ',
				    id: 'dtpTglAkhirFilterCMWorkOrder',
				    format: 'd/M/Y',
				    value: NowCMWorkOrder,
				    width: 100
				}
			]
		}
	);
	
	var LegendViewCMWorkOrder = new Ext.Panel
	(
		{
		    id: 'LegendViewCMWorkOrder',
		    region: 'center',
			border:false,
			bodyStyle: 'padding:0px 7px 0px 7px',
		    layout: 'column',
			frame:true,
			//height:30,
			anchor: '100% 8%',
			autoScroll:false,
			items:
			[	
				{
					columnWidth: .15,
					layout: 'form',
					//height:30,
					anchor: '100% 8%',
					border: false,
					html: '<img src="'+baseURL+'ui/images/icons/16x16/Before.png" class="text-desc-legend"/>' + " " + " On Schedule"
				},
				{
					columnWidth: .2,
					layout: 'form',
					border: false,
					//height:30,
					anchor: '100% 8%',
					html: '<img src="'+baseURL+'ui/images/icons/16x16/After.png" class="text-desc-legend"/>' + " " + " Work Order"
				}
			]
		
		}
	)

    var FormTRCMWorkOrder = new Ext.Panel
	(
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleWOCM,
		    border: false,
		    shadhow: true,
		    iconCls: 'CMWorkOrder',
		    margins: '0 5 5 0',
		    items: [grListTRCMWorkOrder,LegendViewCMWorkOrder],
		    tbar:
			[
				nmCategoryWOCM + ' : ', ' ', mComboCategoryCMWorkOrderView() 
				
				,' ', '-',nmRepairWOCM + ' : ', ' ',
				mComboRepairCMWorkOrderView(),//' ', '-','Status : ', ' ',
				//mComboStatusViewCMWorkOrder(),
				' ', '-',' ',
					nmMaksData + ' : ', ' ', mComboMaksDataCMWorkOrder(),
					' ', '-',
				{
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    anchor: '25%',
				    handler: function(sm, row, rec) 
					{
				        RefreshDataCMWorkOrderFilter();
				    }
				}
			],
		    listeners:
			{
			    'afterrender': function() 
				{
			    }
			}
		}
	);
    RefreshDataCMWorkOrder() ;
    RefreshDataCMWorkOrder() ;
	
    return FormTRCMWorkOrder

};

function RefreshDataCMWorkOrder() 
{
    dsTRCMWorkOrderList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: selectCountCMWorkOrder,
			    //Sort: 'WO_CM_ID',
                            Sort: 'wo_cm_id',
			    Sortdir: 'ASC',
			    target: 'ViewWorkOrderCM',
			    param: ''
			}
		}
	);
    return dsTRCMWorkOrderList;
};

function CMWorkOrderLookUp(rowdata) 
{
    var lebar = 800; //735;
    
    IsExtRepairCMWorkOrder='';
	IdCMWorkOrder='';
	SchIdCMWorkOrder='';
	varCatIdCMWorkOrder='';
	rowSelectedCMWorkOrderTemp='';
	cellSelectServiceCMWorkOrder='';
	TRCMWorkOrderLookUps='';
	GetStrTreeComboCMWorkOrderView();				
    TRCMWorkOrderLookUps = new Ext.Window
	(
		{
		    id: 'gridCMWorkOrder',
		    title: nmTitleWOCM,
		    closeAction: 'destroy',
		    width: lebar,
		    height: 540,//530,
		    border: false,
			resizable:false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'CMWorkOrder',
		    modal: true,
		    items: getFormEntryTRCMWorkOrder(lebar),
		    listeners:
			{
			    activate: function() 
				{
			    },
			    afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedCMWorkOrder=undefined;
					RefreshDataCMWorkOrderFilter();
				}
			}
		}
	);

    TRCMWorkOrderLookUps.show();
    if (rowdata == undefined) 
	{
        //CMWorkOrderAddNew();
    }
    else 
	{
        TRCMWorkOrderInit(rowdata)
    }
};

function getFormEntryTRCMWorkOrder(lebar) 
{
    var pnlTRCMWorkOrder = new Ext.FormPanel
	(
		{
		    id: 'PanelTRCMWorkOrder',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    bodyStyle: 'padding:10px 10px 0px 10px',
			height:518,
		    anchor: '100%',
		    width: lebar,
		    border: false,
			//autoScroll:true,
		    items: [getItemPanelInputCMWorkOrder(lebar)],
		    tbar:
			[
				{
				    text: nmTambah,
				    id: 'btnTambahCMWorkOrder',
				    tooltip: nmTambah,
				    iconCls: 'add',
					disabled:true,
				    handler: function() 
					{
				        //CMWorkOrderAddNew();
				    }
				}, '-',
				{
				    text: nmSimpan,
				    id: 'btnSimpanCMWorkOrder',
				    tooltip: nmSimpan,
				    iconCls: 'save',
				    handler: function() 
					{
				        CMWorkOrderSave(false);
				       RefreshDataCMWorkOrderFilter();
					
				    }
				}, '-',
				{
				    text: nmSimpanKeluar,
				    id: 'btnSimpanKeluarCMWorkOrder',
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{
						var x =  CMWorkOrderSave(true);
				         RefreshDataCMWorkOrderFilter();
						if (x===undefined)
						{
							TRCMWorkOrderLookUps.close();
						};    
				    }
				}, '-',
				{
				    text: nmHapus,
				    id: 'btnHapusCMWorkOrder',
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
				        CMWorkOrderDelete();
				        RefreshDataCMWorkOrderFilter(false);
				    }
				}, '-',
				{
				    text: nmLookup,
				    id: 'btnLookupCMWorkOrder',
				    tooltip: nmLookup,
				    iconCls: 'find',
				    handler: function() 
					{
						FormLookupEmployee('txtKdSupervisorInternalCMWorkOrder','txtNamaSupervisorInternalCMWorkOrder','');
				    }
				}, 
				'-', '->', '-',
				{
				    text: nmCetak,
					id:'btnCetakCMWorkOrder',
				    tooltip: nmCetak,
				    iconCls: 'print',
				    handler: function() {
				    var cKriteria;
				    cKriteria = Ext.get('txtKdMainAssetCMWorkOrder').dom.value + ' - ' + Ext.get('txtNamaMainAsetCMWorkOrder').dom.value + '###1###';
				    cKriteria += Ext.get('txtProblemCMWorkOrder').dom.value + '###2###';
				    cKriteria += Ext.get('dtpStartCMWorkOrder').dom.value + '###3###';
				    cKriteria += Ext.get('dtpFinishCMWorkOrder').dom.value + '###4###';
				    cKriteria += IdCMWorkOrder + '###5###';
				    ShowReport('', '920009', cKriteria);
					}
				}
			]
		}
	); 
	
	


    var FormTRCMWorkOrder = new Ext.Panel
	(
		{
		    id: 'FormTRCMWorkOrder',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRCMWorkOrder]
		}
	);

    return FormTRCMWorkOrder
};



function TRCMWorkOrderInit(rowdata)
{
    AddNewCMWorkOrder = false;
	rowSelectedCMWorkOrderTemp = rowSelectedCMWorkOrder;
    Ext.get('txtKdMainAssetCMWorkOrder').dom.value=rowdata.ASSET_MAINT_ID;
	Ext.get('txtNamaMainAsetCMWorkOrder').dom.value=rowdata.ASSET_MAINT_NAME;
	Ext.get('txtProblemCMWorkOrder').dom.value=rowdata.PROBLEM;
	Ext.get('dtpStartCMWorkOrder').dom.value=ShowDate(rowdata.SCH_DUE_DATE);
	Ext.get('dtpFinishCMWorkOrder').dom.value=ShowDate(rowdata.SCH_FINISH_DATE);
	IsExtRepairCMWorkOrder = rowdata.IS_EXT_REPAIR;
	SchIdCMWorkOrder=rowdata.SchID;
	varCatIdCMWorkOrder=rowdata.CATEGORY_ID;
	LoadDataServiceCMWorkOrder(SchIdCMWorkOrder);
	LoadDataPartCMWorkOrder(SchIdCMWorkOrder,rowdata.CATEGORY_ID);
	GetJumlahTotalPersonPartCMWorkOrder();
	if (rowdata.IS_EXT_REPAIR === false)
	{
		LoadDataEmployeeCMWorkOrder(SchIdCMWorkOrder,rowdata.CATEGORY_ID);
		DisabledVendorCMWorkOrder(true);
	}
	else
	{
		LoadDataVendorCMWorkOrder(SchIdCMWorkOrder,rowdata.CATEGORY_ID);
		DisabledVendorCMWorkOrder(false);
		GetVendorCMWorkOrder();
	};
	
	if ( rowdata.WO_CM_ID != null)
	{
		IdCMWorkOrder=rowdata.WO_CM_ID;
		Ext.get('dtpCMWorkOrder').dom.value=ShowDate(rowdata.WO_CM_DATE);
		Ext.get('dtpFinishDateCMWorkOrder').dom.value=ShowDate(rowdata.WO_CM_FINISH_DATE);
		Ext.get('txtKdSupervisorInternalCMWorkOrder').dom.value=rowdata.PIC_ID_WO;
		Ext.get('txtNamaSupervisorInternalCMWorkOrder').dom.value=rowdata.PIC_NAME_WO;
		
		if (rowdata.DESC_WO_CM != null)
		{
			Ext.get('txtDeskripsiCMWorkOrder').dom.value=rowdata.DESC_WO_CM;
		}
		else
		{
			Ext.get('txtDeskripsiCMWorkOrder').dom.value='';
		};
	}
	else
	{
		IdCMWorkOrder='';
		Ext.get('dtpFinishDateCMWorkOrder').dom.value=ShowDate(rowdata.SCH_FINISH_DATE);
	};
};

function DisabledVendorCMWorkOrder(mBol)
{
	if (mBol === true)
	{
		Ext.get('txtNamaVendorCMWorkOrder').dom.value='';
		Ext.get('txtKdVendorCMWorkOrder').dom.value='';
	};
};

function LoadDataServiceCMWorkOrder(SchId) 
{
    dsDtlServiceCMWorkOrder.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_ID',
                            Sort: 'service_id',
			    Sortdir: 'ASC',
			    target: 'ViewSchServiceCM',
			    //param: 'where sch_cm_id =~' + SchId + '~'
                            param: 'sch_cm_id = ~' + SchId + '~'
			}
		}
	);
    return dsDtlServiceCMWorkOrder;
};

function LoadDataPartCMWorkOrder(SchId,CatId) 
{
    dsDtlPartCMWorkOrderTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'PART_ID',
                            Sort: 'part_id',
			    Sortdir: 'ASC',
			    target: 'ViewSchPartCM',
			    //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~'
                            param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlPartCMWorkOrderTemp;
};

function LoadDataEmployeeCMWorkOrder(SchId,CatId) 
{
    dsDtlCraftPersonIntCMWorkOrderTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'EMP_ID',
                            Sort: 'emp_id',
			    Sortdir: 'ASC',
			    target: 'ViewSchPersonCM',
			    //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~'
                            param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlCraftPersonIntCMWorkOrderTemp;
};

function LoadDataVendorCMWorkOrder(SchId,CatId) 
{
    dsDtlCraftPersonExtCMWorkOrderTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'VENDOR_ID',
                            Sort: 'vendor_id',
			    Sortdir: 'ASC',
			    target: 'ViewSchPersonCM',
			     //param: 'where sch_cm_id =~' + SchId + '~ and category_id=~' + CatId + '~'
                             param: 'sch_cm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlCraftPersonExtCMWorkOrderTemp;
};


function CMWorkOrderAddNew() 
{
    AddNewCMWorkOrder = true;
	
	IdCMWorkOrder='';
	Ext.get('dtpCMWorkOrder').dom.value=NowCMWorkOrder.format('d/M/Y');
	Ext.get('dtpFinishDateCMWorkOrder').dom.value = ShowDate(rowSelectedCMWorkOrderTemp.data.SCH_FINISH_DATE);
	Ext.get('txtKdSupervisorInternalCMWorkOrder').dom.value='';
	Ext.get('txtNamaSupervisorInternalCMWorkOrder').dom.value='';
	Ext.get('txtDeskripsiCMWorkOrder').dom.value='';
	
};


function getParamCMWorkOrder() 
{
    var params =
	{
            Table:'ViewWorkOrderCM',
            WOId: IdCMWorkOrder,
            EmpId: Ext.get('txtKdSupervisorInternalCMWorkOrder').getValue(),
            SchId:SchIdCMWorkOrder,
            WODate: Ext.get('dtpCMWorkOrder').dom.value,
            WOFinishDate: Ext.get('dtpFinishDateCMWorkOrder').dom.value,
            Notes:Ext.get('txtDeskripsiCMWorkOrder').dom.value,
            IsExt:IsExtRepairCMWorkOrder
	};
    return params;
	
};

function getItemPanelInputCMWorkOrder(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
		border:false,
		height:734,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar-35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype:'fieldset',
						title: nmTitleSchInfoWOCM,	
						anchor:  '99.99%',
						height:'115px',
						items :
						[
							getItemPanelNoAssetCMWorkOrder(lebar),
							getItemPanelTanggalAppCMWorkOrder(lebar)
						]
					},
					{
						xtype:'fieldset',
						title: nmTitleWOWOCM,
						anchor:  '99.99%',
						height:'326px',//312,//302,
						items :
						[
						    getItemPanelTanggalCMWorkOrder(lebar),
							getItemPanelRadioVendorCMWorkOrder(lebar),
							getItemPanelCardLayoutCMWorkOrder(),
							getItemPanelDescCMWorkOrder()
						]
					}
				]
			}
		]
	};
    return items;
};

function getItemPanelTanggalCMWorkOrder(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 70,
	    items:
		[
			{
			    columnWidth: .29,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'datefield',
						fieldLabel:nmWODateCM + ' ',
						id: 'dtpCMWorkOrder',
						name: 'dtpCMWorkOrder',
						format: 'd/M/Y',
						value: NowCMWorkOrder,
						anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .26,
			    layout: 'form',
			    border: false,
				labelWidth:80,
			    items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmFinishDateWOCM + ' ',
						id: 'dtpFinishDateCMWorkOrder',
						name: 'dtpFinishDateCMWorkOrder',
						format: 'd/M/Y',
						value: NowCMWorkOrder,
						anchor: '99%'

					}
				]
			},
			{
			    columnWidth: .2,
			    layout: 'form',
			    border: false,
				labelWidth:30,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmPICWOCM + ' ',
					    name: 'txtKdSupervisorInternalCMWorkOrder',
					    id: 'txtKdSupervisorInternalCMWorkOrder',
					    readOnly: true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .25,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
					    hideLabel: true,
					    name: 'txtNamaSupervisorInternalCMWorkOrder',
					    id: 'txtNamaSupervisorInternalCMWorkOrder',
					    anchor: '100%',
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var criteria='';
									if (Ext.get('txtNamaSupervisorInternalCMWorkOrder').dom.value != '')
									{
										//criteria = ' where EMP_NAME like ~%' + Ext.get('txtNamaSupervisorInternalCMWorkOrder').dom.value + '%~';
                                                                                criteria = ' emp_name like ~%' + Ext.get('txtNamaSupervisorInternalCMWorkOrder').dom.value + '%~';
									};
									FormLookupEmployee('txtKdSupervisorInternalCMWorkOrder','txtNamaSupervisorInternalCMWorkOrder',criteria);
								};
							},
							'focus' : function()
							{
								FocusCtrlCMWorkOrder='txtPIC';
							}
						}
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelCardLayoutCMWorkOrder()
{
	 var FormPanelCardLayoutCMWorkOrder = new Ext.Panel
	(
		{
		    id: 'FormPanelCardLayoutCMWorkOrder',
		    trackResetOnLoad: true,
		    width: 742,
			height: 210,//225,//205,
			layout: 'card',
			border:false,
			activeItem: 0,
			items: 
			[
				{
					xtype: 'panel',
					layout: 'form',
					border:false,
					id: 'CardCMWorkOrder1',
					items:
					[	 
						getItemTabPanelRepairInternalCMWorkOrder()
					]
				}
			]
		}
	);
	
	return FormPanelCardLayoutCMWorkOrder;
};

function getItemTabPanelRepairInternalCMWorkOrder()
{
	var TotalServiceCMWorkOrder = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 725,
			border:false,
			labelAlign:'right',
			labelWidth:175,
			style: 
			{
				'margin-top': '3.3px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: nmTotalPersonPartWOCM + ' ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalServiceCMWorkOrder',
					name:'txtTotalServiceCMWorkOrder',
					readOnly:true,
					width: 150
				}
			]
		}
	);


	var TotalPersonCMWorkOrder = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 725,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '3px',//'3.9px',
				'margin-left': '500px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: nmTotalWOCM + ' ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalPersonCMWorkOrder',
					name:'txtTotalPersonCMWorkOrder',
					readOnly:true,
					width: 150
				}
			]
		}
	);

	var TotalPartCMWorkOrder = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 725,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '3.9px',
				'margin-left': '500px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel:  nmTotalWOCM + ' ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalPartCMWorkOrder',
					name:'txtTotalPartCMWorkOrder',
					readOnly:true,
					width: 150
				}
			]
		}
	);

    vTabPanelRepairInternalCMWorkOrder = new Ext.TabPanel
	(
		{
		    id: 'vTabPanelRepairInternalCMWorkOrder',
		    region: 'center',
		    margins: '7 7 7 7',
			style:
			{
			   'margin-top': '5px'
			},
		    bodyStyle: 'padding:7px 7px 7px 7px',
		    activeTab: 0,
		    plain: true,
		    defferedRender: false,
		    frame: false,
		    border: true,
		    height: 200,//215,
			width:742,
		    anchor: '100%',
		    items:
			[
				{
					 title: nmTitleServiceWOCM,
					 id: 'tabServiceCMWorkOrder',
					 items:
					 [
						GetDTLServiceCMWorkOrderGrid(),TotalServiceCMWorkOrder
					 ],
			        listeners:
					{
						  activate: function() 
						  {
							varTabPartCMWorkOrder=1;
						  }
					}
				},
			    {
					 title: nmTitlePersonWOCM,
					 id: 'tabPersonCMWorkOrder',
					 items:
					 [
						getItemPanelCardLayoutGridServiceCMWorkOrder(),TotalPersonCMWorkOrder
					 ],
			        listeners:
					{
						  activate: function() 
						  {
							if (cellSelectServiceCMWorkOrder != '' && cellSelectServiceCMWorkOrder.data != undefined)
							{
								if (cellSelectServiceCMWorkOrder.data.SERVICE_ID === undefined || cellSelectServiceCMWorkOrder.data.SERVICE_ID === '')
								{
									vTabPanelRepairInternalCMWorkOrder.setActiveTab('tabServiceCMWorkOrder');
									varTabPartCMWorkOrder=1;
									alert(nmAlertTabService);
								}
								else
								{
									if (IsExtRepairCMWorkOrder === true)
									{
										Ext.getCmp('FormPanelCardLayoutGridServiceCMWorkOrder').getLayout().setActiveItem(1);	
										GetPersonExtServiceCMWorkOrder(cellSelectServiceCMWorkOrder.data.SERVICE_ID);
									}
									else
									{
										Ext.getCmp('FormPanelCardLayoutGridServiceCMWorkOrder').getLayout().setActiveItem(0);	
										GetPersonIntServiceCMWorkOrder(cellSelectServiceCMWorkOrder.data.SERVICE_ID);
									};
									CalcTotalPersonCMWorkOrder();
									varTabPartCMWorkOrder=2;
								};
							}
							else
							{
								vTabPanelRepairInternalCMWorkOrder.setActiveTab('tabServiceCMWorkOrder');
								varTabPartCMWorkOrder=1;
								alert(nmAlertTabService);
							};
						  }
					}
				},
				{
					 title: nmTitlePartWOCM,
					 id: 'tabWOPartCMWorkOrder',
					 items:
					 [
						GetDTLPartInternalCMWorkOrderGrid(),TotalPartCMWorkOrder
					 ],
					 listeners:
					{
						activate: function() 
						{
							if (cellSelectServiceCMWorkOrder != '' && cellSelectServiceCMWorkOrder.data != undefined)
							{
								if (cellSelectServiceCMWorkOrder.data.SERVICE_ID === undefined || cellSelectServiceCMWorkOrder.data.SERVICE_ID === '')
								{
									vTabPanelRepairInternalCMWorkOrder.setActiveTab('tabServiceCMWorkOrder');
									varTabPartCMWorkOrder=1;
									alert(nmAlertTabService);
								}
								else
								{
									varTabPartCMWorkOrder=3;
									GetPartCMWorkOrder(cellSelectServiceCMWorkOrder.data.SERVICE_ID);
								};	
							}
							else
							{
								vTabPanelRepairInternalCMWorkOrder.setActiveTab('tabServiceCMWorkOrder');
								varTabPartCMWorkOrder=1;
								alert(nmAlertTabService);
							};
						}
					}
				}
			]
		}
	)

		return vTabPanelRepairInternalCMWorkOrder;
};

function CalcTotalPersonCMWorkOrder()
{
	var ds;
	if(IsExtRepairCMWorkOrder === false)
	{
		ds=dsDtlCraftPersonIntCMWorkOrder;
	}
	else
	{
		ds=dsDtlCraftPersonExtCMWorkOrder;
	};
	
    var total=Ext.num(0);
	if (ds != undefined)
	{
		for(var i=0;i < ds.getCount();i++)
		{
			total += Ext.num(ds.data.items[i].data.COST);
		}
	};
	
	Ext.get('txtTotalPersonCMWorkOrder').dom.value = formatCurrency(total);	
	
};


function GetPersonExtServiceCMWorkOrder(service_id)
{
	if (service_id != '' && service_id != undefined)
	{
		dsDtlCraftPersonExtCMWorkOrder.removeAll();
		for (var i = 0; i < dsDtlCraftPersonExtCMWorkOrderTemp.getCount() ; i++) 
		{
			if(dsDtlCraftPersonExtCMWorkOrderTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlCraftPersonExtCMWorkOrder.insert(dsDtlCraftPersonExtCMWorkOrder.getCount(), dsDtlCraftPersonExtCMWorkOrderTemp.data.items[i]);
			};
		};
	};
};

function GetPersonIntServiceCMWorkOrder(service_id)
{
	if (service_id != '' && service_id != undefined)
	{
		dsDtlCraftPersonIntCMWorkOrder.removeAll();
		for (var i = 0; i < dsDtlCraftPersonIntCMWorkOrderTemp.getCount() ; i++) 
		{
			if(dsDtlCraftPersonIntCMWorkOrderTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlCraftPersonIntCMWorkOrder.insert(dsDtlCraftPersonIntCMWorkOrder.getCount(), dsDtlCraftPersonIntCMWorkOrderTemp.data.items[i]);
			};
		};
	};
};

function GetPartCMWorkOrder(service_id)
{
	 var total=Ext.num(0);
	if (service_id != '' && service_id != undefined)
	{
		dsDtlPartCMWorkOrder.removeAll();
		for (var i = 0; i < dsDtlPartCMWorkOrderTemp.getCount() ; i++) 
		{
			if(dsDtlPartCMWorkOrderTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlPartCMWorkOrder.insert(dsDtlPartCMWorkOrder.getCount(), dsDtlPartCMWorkOrderTemp.data.items[i]);
				total += Ext.num(dsDtlPartCMWorkOrder.data.items[i].data.TOT_COST);
			};
		};
	};
	
	Ext.get('txtTotalPartCMWorkOrder').dom.value = formatCurrency(total);	
};


function getItemPanelCardLayoutGridServiceCMWorkOrder()
{
	 var FormPanelCardLayoutGridServiceCMWorkOrder = new Ext.Panel
	(
		{
		    id: 'FormPanelCardLayoutGridServiceCMWorkOrder',
		    trackResetOnLoad: true,
		    width: 725,
			height: 135,//150,
			layout: 'card',
			border:false,
			activeItem: 0,
			items: 
			[
				{
					xtype: 'panel',
					layout: 'form',
					width: 725,
					height: 135,//150,
					border:false,
					id: 'CardGridServiceCMWorkOrder1',
					items:
					[	 
						GetDTLCraftPersonCMWorkOrderGrid()
					]
				},
				{
					xtype: 'panel',
					layout: 'form',
					width: 725,
					height: 135,//150,
					border:false,
					id: 'CardGridServiceCMWorkOrder2',
					items:
					[
						GetDTLCraftPersonExtCMWorkOrderGrid()
					]
				}
			]
		}
	);
	
	return FormPanelCardLayoutGridServiceCMWorkOrder;
};


function GetDTLServiceCMWorkOrderGrid() 
{
    var fldDetail = ['SCH_CM_ID','SERVICE_ID','SERVICE_NAME'];

    dsDtlServiceCMWorkOrder = new WebApp.DataStore({ fields: fldDetail })

    var gridServiceCMWorkOrder  = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlServiceCMWorkOrder,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:725,
		    height:135,//150,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectServiceCMWorkOrder = dsDtlServiceCMWorkOrder.getAt(row);
					        CurrentServiceCMWorkOrder.row = row;
					        CurrentServiceCMWorkOrder.data = cellSelectServiceCMWorkOrder;
					    }
					}
				}
			),
				cm: gridServiceCMWorkOrderDetailColumModel()
		    , viewConfig:{forceFit: true}
		}
	);

		return gridServiceCMWorkOrder;
};


function gridServiceCMWorkOrderDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdServiceCMWorkOrder',
			    header: nmServiceIDWOCM,
			    dataIndex: 'SERVICE_ID',
			    width:100
			},
			{
			    id: 'colServiceNameCCMWorkOrder',
			    header: nmServiceNameWOCM,
			    dataIndex: 'SERVICE_NAME',
			    sortable: false,
			    width: 200,
			    renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			}
		]
	)
};

function GetDTLCraftPersonCMWorkOrderGrid() 
{
    var fldDetail = ['SCH_CM_ID','SERVICE_ID','ROW_SCH','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_SCH_PERSON','VENDOR','EMP_NAME'];

    dsDtlCraftPersonIntCMWorkOrder = new WebApp.DataStore({ fields: fldDetail })
	dsDtlCraftPersonIntCMWorkOrderTemp = new WebApp.DataStore({ fields: fldDetail })

    var gridCraftPersonCMWorkOrder = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlCraftPersonIntCMWorkOrder,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:725,
		    height:135,//150,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					      
					    }
					}
				}
			),
				cm: CraftPersonCMWorkOrderDetailColumModel()
		    , viewConfig:{forceFit: true}
		}
	);

		return gridCraftPersonCMWorkOrder;
};

function GetDTLCraftPersonExtCMWorkOrderGrid() 
{
    var fldDetail = ['SCH_CM_ID','SERVICE_ID','ROW_SCH','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_SCH_PERSON','VENDOR','EMP_NAME'];

    dsDtlCraftPersonExtCMWorkOrder = new WebApp.DataStore({ fields: fldDetail })
	dsDtlCraftPersonExtCMWorkOrderTemp = new WebApp.DataStore({ fields: fldDetail })
	
    var gridCraftPersonExtCMWorkOrder = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlCraftPersonExtCMWorkOrder,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:725,
		    height:135,//122,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					    }
					}
				}
			),
				cm: CraftPersonExtCMWorkOrderDetailColumModel(),
		     viewConfig:{forceFit: true}
		}
	);

		return gridCraftPersonExtCMWorkOrder;
};

function GetDTLPartInternalCMWorkOrderGrid() 
{
    var fldDetail = ['SCH_CM_ID','SERVICE_ID','PART_ID','QTY','UNIT_COST','TOT_COST','PART_NAME','PRICE','DESC_SCH_PART'];

    dsDtlPartCMWorkOrder = new WebApp.DataStore({ fields: fldDetail })
	dsDtlPartCMWorkOrderTemp = new WebApp.DataStore({ fields: fldDetail })
	
    var gridPartInternalCMWorkOrder = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlPartCMWorkOrder,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:725,
		    height: 133,//148,//150,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
					    }
					}
				}
			),
			cm: PartInternalCMWorkOrderDetailColumModel(),
		    viewConfig: { forceFit: true }
		}
	);

		return gridPartInternalCMWorkOrder;
};


function CraftPersonCMWorkOrderDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdEmployeeCMWorkOrder',
			    header: nmEmpIDWOCM,
			    dataIndex: 'EMP_ID',
			    width: 150
			},
			{
			    id: 'colEmployeeNameCMWorkOrder',
			    header: nmEmpNameWOCM,
			    dataIndex: 'EMP_NAME',
			    sortable: false,
			    width: 200,
			    renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colDescCraftPersonCMWorkOrder',
			    header: nmDescWOCM,
			    width: 200,
			    dataIndex: 'DESC_SCH_PERSON',
			    renderer: function(value, cell) 
				{
			       var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colCostCraftPersonCMWorkOrder',
			    header: nmCostPersonWOCM,
			    width: 150,
				align:'right',
			    dataIndex: 'COST',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.COST);
				}
			}
		]
	)

};

function CraftPersonExtCMWorkOrderDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdVendorCMWorkOrder',
			    header: nmVendorIdWOCM,
			    dataIndex: 'VENDOR_ID',
				hidden:true,
			    width: 80
			},
			{
			    id: 'colVendorNameCMWorkOrder',
			    header: nmVendorNamaWOCM,
			    dataIndex: 'VENDOR',
			    sortable: false,
				hidden:true,
			    width: 200,
			    renderer: function(value, cell) 
				{
			        var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colContactPersonCraftPersonCMWorkOrder',
			    header: nmContactPersonWOCM,
			    width: 250,
			    dataIndex: 'PERSON_NAME'
			},
			{
			    id: 'colDescCraftPersonExtCMWorkOrder',
			    header: nmDescWOCM,
			    width: 250,
			    dataIndex: 'DESC_SCH_PERSON',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescCraftPersonExtCMWorkOrder',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(value, cell) 
				{
			       var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colCostCraftPersonExtCMWorkOrder',
			    header: nmCostPersonWOCM,
			    width: 90,
			    dataIndex: 'COST',
				align:'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.COST);
				}
			}
		]
	)

};


function PartInternalCMWorkOrderDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdPartInternalCMWorkOrder',
			    header: nmPartNumberWOCM,
			    dataIndex: 'PART_ID',
			    width: 140
			},
			{
			    id: 'colPartInternalCMWorkOrder',
			    header: nmPartName,
			    dataIndex: 'PART_NAME',
			    sortable: false,
			    width: 200,
			    renderer: function(value, cell) 
				{
			        var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
			        return str;
			    }
			},
			{
			    id: 'colQtyPartInternalCMWorkOrder',
			    header: nmQtyWOCM,
				align:'center',
			    width: 50,
			    dataIndex: 'QTY'
			},
			{
			    id: 'colCostPartInternalCMWorkOrder',
			    header: nmUnitCostWOCM,
			    width: 150,
				align:'right',
			    dataIndex: 'UNIT_COST',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.UNIT_COST);
				}	
			},
			{
			    id: 'colTotCostPartInternalCMWorkOrder',
			    header: nmTotCostWOCM,
			    width: 150,
				align:'right',
			    dataIndex: 'TOT_COST',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.UNIT_COST * record.data.QTY);
				}	
			},
			{
			    id: 'colDescPartInternalCMWorkOrder',
			    header: nmDescPartWOCM,
			    width: 200,
			    dataIndex: 'DESC_SCH_PART',
			    renderer: function(value, cell) 
				{
			        var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
			        return str;
			    }
			}
		]
	)
};

function getItemPanelDescCMWorkOrder() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:40,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    labelWidth: 100,
				height:40,
			    items:
				[
				    {
				        xtype: 'textarea',
				        fieldLabel: nmNotes + ' ',
				        name: 'txtDeskripsiCMWorkOrder',
				        id: 'txtDeskripsiCMWorkOrder',
				        scroll: true,
				        anchor:'100% 95%'// '100% 87%'
				    }
				]
			}
		]

    }
	return items ;
};


function getItemPanelSupervisorExternalCMWorkOrder() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    bodyStyle: 'padding:7px 0px 7px 0px',
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
			    labelWidth: 70,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmSupervisorWOCM + ' ',
					    name: 'txtKdSupervisorExternalCMWorkOrder',
					    id: 'txtKdSupervisorExternalCMWorkOrder',
					    readOnly: true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .65,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
					    hideLabel: true,
					    name: 'txtNamaSupervisorExternalCMWorkOrder',
					    id: 'txtNamaSupervisorExternalCMWorkOrder',
					    anchor: '100%',
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var criteria='';
									if (Ext.get('txtNamaSupervisorExternalCMWorkOrder').dom.value != '')
									{
										//criteria = ' where EMPLOYEE_NAME like ~%' + Ext.get('txtNamaSupervisorExternalCMWorkOrder').dom.value + '%~';
                                                                                criteria = ' employee_name like ~%' + Ext.get('txtNamaSupervisorExternalCMWorkOrder').dom.value + '%~';
									};
									FormLookupEmployee('txtKdSupervisorExternalCMWorkOrder','txtNamaSupervisorExternalCMWorkOrder',criteria);
								};
							}
						}
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoAssetCMWorkOrder(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 70,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoAssetCMWorkOrder2(lebar) 
				]
			}
		]
	}
    return items;
};


function getItemPanelTanggalAppCMWorkOrder(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 70,
	    items:
		[
			{
				columnWidth: .65,
				layout: 'form',
				border: false,
				items:
				[
					{
						xtype: 'textarea',
						fieldLabel: nmProblemWOCM + ' ',
						name: 'txtProblemCMWorkOrder',
						id: 'txtProblemCMWorkOrder',
						scroll:true,
						readOnly:true,
						anchor: '100% 91%'
					}
				]
			},
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:80,
			    items:
				[
					{
						 xtype: 'textfield',
					    fieldLabel: nmStartDateWOCM + ' ',
					    id: 'dtpStartCMWorkOrder',
					    name: 'dtpStartCMWorkOrder',
					    readOnly:true,
					    anchor: '85%'
					},
					{
						 xtype: 'textfield',
					    fieldLabel: nmFinishDate + ' ',
					    id: 'dtpFinishCMWorkOrder',
					    name: 'dtpFinishCMWorkOrder',
					    readOnly: true,
					    anchor: '85%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelRadioVendorCMWorkOrder(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		//height:30,
		width: lebar - 57,
	    items:
		[
			{
			    columnWidth: .25,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmVendorNamaWOCM + ' ',
					    name: 'txtKdVendorCMWorkOrder',
					    id: 'txtKdVendorCMWorkOrder',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
					    name: 'txtNamaVendorCMWorkOrder',
					    id: 'txtNamaVendorCMWorkOrder',
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .15,
			    layout: 'form',
				id:'layoutBtnVendorCMWorkOrder',
				name:'layoutBtnVendorCMWorkOrder',
			    border: false,
			    items:
				[
					{
						xtype:'button',
						text:nmBtnVendorInfo,
						width:90,
						hideLabel:true,
						id: 'btnLookupVendorCMWorkOrder',
						handler:function() 
						{
							if(IsExtRepairCMWorkOrder === true)
							{
								if(dsLookVendorListCMWorkOrder != undefined)
								{
									if(dsLookVendorListCMWorkOrder.getCount() > 0 )
									{
										InfoVendorLookUp(dsLookVendorListCMWorkOrder.data.items[0])
									};
								};
							};
						}
					}
				]
			}
		]
	}
    return items;
};



function getItemPanelNoAssetCMWorkOrder2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 70,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:100,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmAssetWOCM + ' ',
					    name: 'txtKdMainAssetCMWorkOrder',
					    id: 'txtKdMainAssetCMWorkOrder',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .7,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
						readOnly:true,
					    name: 'txtNamaMainAsetCMWorkOrder',
					    id: 'txtNamaMainAsetCMWorkOrder',
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


function mComboMaksDataCMWorkOrder() 
{
    var cboMaksDataCMWorkOrder = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataCMWorkOrder',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmMaksData + ' ',
		    width: 50,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountCMWorkOrder,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountCMWorkOrder = b.data.displayText;
			        RefreshDataCMWorkOrderFilter();
			    }
			}
		}
	);
    return cboMaksDataCMWorkOrder;
};

function RefreshDataCMWorkOrderFilter() 
{
    var KataKunci='';
	
	if (Ext.get('cboCategoryCMWorkOrderView').getValue() != '' && selectCategoryCMWorkOrderView != undefined)
    { 
		if (selectCategoryCMWorkOrderView.id != undefined)
		{
			if(selectCategoryCMWorkOrderView.id != ' 9999')
			{
				if (selectCategoryCMWorkOrderView.leaf === false)
				{
					//KataKunci =' where left(category_id,' + selectCategoryCMWorkOrderView.id.length + ')=' +  selectCategoryCMWorkOrderView.id
                                        KataKunci =' substring(category_id,1,' + selectCategoryCMWorkOrderView.id.length + ')=' +  selectCategoryCMWorkOrderView.id
				}
				else
				{
					//KataKunci =' WHERE Category_id=~'+ selectCategoryCMWorkOrderView.id + '~'
                                        KataKunci =' category_id = ~'+ selectCategoryCMWorkOrderView.id + '~'
				};
			};
		};
	};
	
    // if (Ext.get('cboCategoryCMWorkOrderView').getValue() != '' && selectCategoryCMWorkOrderView != undefined)
    // { 
		// if(selectCategoryCMWorkOrderView != ' 9999')
		// {
			// KataKunci = ' where category_id = ~' + selectCategoryCMWorkOrderView + '~';
		// };
	// };
	
    if (Ext.get('cboRepairCMWorkOrderView').getValue() != '' && selectRepairCMWorkOrderView != undefined)
    { 
		if(selectRepairCMWorkOrderView != 99)
		{
			if (KataKunci === '')
			{
				//KataKunci = ' where IS_EXT_REPAIR =  ' + selectRepairCMWorkOrderView ;
                                KataKunci = ' is_ext_repair = ' + selectRepairCMWorkOrderView ;
			}
			else
			{
				//KataKunci += ' and  IS_EXT_REPAIR =  ' + selectRepairCMWorkOrderView ;
                                KataKunci += ' and is_ext_repair = ' + selectRepairCMWorkOrderView ;
			}; 
		};
	};
	
	// if (Ext.get('cboStatusViewCMWorkOrder').getValue() != '' && selectStatusViewCMWorkOrder != undefined)
    // { 
		// if (KataKunci === '')
		// {
			// KataKunci = ' where status_id =  ~' + selectStatusViewCMWorkOrder + '~';
		// }
		// else
		// {
			// KataKunci += ' and  status_id =  ~' + selectStatusViewCMWorkOrder + '~';
		// }; 
	// };
	
	if( Ext.get('chkWithTglCMWorkOrder').dom.checked === true )
	{
			KataKunci += '@^@' + Ext.get('dtpTglAwalFilterCMWorkOrder').getValue() + '&&'+ Ext.get('dtpTglAkhirFilterCMWorkOrder').getValue();
	};
        
    if (KataKunci != undefined) 
    {  
		 dsTRCMWorkOrderList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountCMWorkOrder,
					//Sort: 'WO_CM_ID',
                                        Sort: 'wo_cm_id',
					Sortdir: 'ASC',
					target: 'ViewWorkOrderCM',
					param: KataKunci
				}
			}
		);
    }
	else
	{
		RefreshDataCMWorkOrder();
	};
    
	return dsTRCMWorkOrderList;
    
};



function CMWorkOrderSave(mBol) 
{	
	if (ValidasiEntryCMWorkOrder(nmHeaderSimpanData,false) == 1 )
	{
		if (IdCMWorkOrder == '' || IdCMWorkOrder === undefined) 
		{
			Ext.Ajax.request
			(
				{
					//url: "./Datapool.mvc/CreateDataObj",
                                        url: baseURL + "index.php/main/CreateDataObj",
					params: getParamCMWorkOrder(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoCMWorkOrder(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataCMWorkOrder();
							if(mBol === false)
							{
								IdCMWorkOrder=cst.WOId;
							};
							AddNewCMWorkOrder = false;
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningCMWorkOrder(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningCMWorkOrder(nmPesanSimpanGagal + ' , ' + nmKonfirmasiResult,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorCMWorkOrder(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
                                        url: baseURL + "index.php/main/CreateDataObj",
					params: getParamCMWorkOrder(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoCMWorkOrder(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataCMWorkOrder();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningCMWorkOrder(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningCMWorkOrder(nmPesanSimpanGagal + ' , ' + nmKonfirmasiResult,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorCMWorkOrder(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
		
};

function ValidasiEntryCMWorkOrder(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtKdSupervisorInternalCMWorkOrder').getValue() == '') || (Ext.get('txtNamaSupervisorInternalCMWorkOrder').getValue() == ''))
	{
		if (Ext.get('txtKdSupervisorInternalCMWorkOrder').getValue() == '') 
		{
			x = 0;
			if (mBolHapus != true)
			{
				ShowPesanWarningCMWorkOrder(nmGetValidasiKosong(nmEmpIDWOCM), modul);
			};
		}
		else if (Ext.get('txtNamaSupervisorInternalCMWorkOrder').getValue() == '') 
		{
			x = 0;
			if (mBolHapus != true)
			{
				ShowPesanWarningCMWorkOrder(nmGetValidasiKosong(nmEmpNameWOCM), modul);
			};
		}
	};
	return x;
};

function CMWorkOrderDelete() 
{
	if (ValidasiEntryCMWorkOrder(nmHeaderHapusData,true) == 1 )
	{
	   if (IdCMWorkOrder != null && IdCMWorkOrder != '' && IdCMWorkOrder != undefined  )
		{
			Ext.Msg.show
			(
				{
				   title:nmHeaderHapusData,
				   msg: nmGetValidasiHapus(nmTitleWOWOCM) ,
				   buttons: Ext.MessageBox.YESNO,
				   width:275,
				   fn: function (btn) 
				   {			
						if (btn =='yes') 
						{
							Ext.Ajax.request
							(
								{
									//url: "./Datapool.mvc/DeleteDataObj",
                                                                        url: baseURL + "index.php/main/DeleteDataObj",
									params: getParamCMWorkOrder(),
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) 
										{
											ShowPesanInfoCMWorkOrder(nmPesanHapusSukses,nmHeaderHapusData);
											CMWorkOrderAddNew();
											RefreshDataCMWorkOrder();
										}
										else if (cst.success === false && cst.pesan===0)
										{
											ShowPesanWarningCMWorkOrder(nmPesanHapusGagal,nmHeaderHapusData);
										}
										else if (cst.success === false && cst.pesan===1)
										{
											ShowPesanWarningCMWorkOrder(nmPesanHapusGagal + ' , ' + nmKonfirmasiResult,nmHeaderHapusData);
										}
										else if (cst.success === false && cst.pesan===1)
										{
											ShowPesanWarningCMWorkOrder(nmPesanHapusGagal + ' , ' + nmKonfirmasiResult,nmHeaderHapusData);
										}
										else 
										{
											ShowPesanErrorCMWorkOrder(nmPesanHapusError,nmHeaderHapusData);
										};
									}
								}
							)
						};
					}
				}
			)
		};
	};
	
};


function ShowPesanWarningCMWorkOrder(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorCMWorkOrder(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoCMWorkOrder(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function mComboStatusViewCMWorkOrder() 
{
	var Field = ['STATUS_ID','STATUS_NAME'];
	
    dsStatusViewCMWorkOrder = new WebApp.DataStore({ fields: Field });
    dsStatusViewCMWorkOrder.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ID',
                            Sort: 'status_id',
			    Sortdir: 'ASC',
			    target: 'ViewComboStatusVw',
			    //param: 'where status_id in ' + valueListStatusViewCMWorkOrder
                            param: 'status_id in ' + valueListStatusViewCMWorkOrder
			}
		}
	);
	
    var cboStatusViewCMWorkOrder = new Ext.form.ComboBox
	(
		{
		    id: 'cboStatusViewCMWorkOrder',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmStatusWOCM + ' ',
		    align: 'Right',
		    width:100,
		    store: dsStatusViewCMWorkOrder,
		    valueField: 'STATUS_ID',
		    displayField: 'STATUS_NAME',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectStatusViewCMWorkOrder = b.data.STATUS_ID;
			    }
			}
		}
	);
	
    return cboStatusViewCMWorkOrder;
};

function mComboRepairCMWorkOrderView() 
{
    var cboRepairCMWorkOrderView = new Ext.form.ComboBox
	(
		{
		    id: 'cboRepairCMWorkOrderView',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmRepairWOCM + ' ',
		    align: 'Right',
		    width:80,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'KD_REPAIR',
						'REPAIR'
					],
				    data: [[99, ' All'],[0, 'Internal'], [1, 'External']]
				}
			),
		    valueField: 'KD_REPAIR',
		    displayField: 'REPAIR',
			value: valueRepairCMWorkOrderView,
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectRepairCMWorkOrderView = b.data.KD_REPAIR;
			    }
			}
		}
	);

    return cboRepairCMWorkOrderView;
};

function mComboCategoryCMWorkOrderView() 
{
	var Field = ['CATEGORY_ID','CATEGORY_NAME'];
	var dsCategoryCMWorkOrderView = new WebApp.DataStore({ fields: Field });
	
	Ext.TreeComboCMWorkOrderView = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treeCMWorkOrderView = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreeCMWorkOrderView  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						id:IdCategoryCMWorkOrderView,
						children: StrTreeComboCMWorkOrderView,
						autoScroll: true
					}
				) 
				treeCMWorkOrderView.setRootNode(rootTreeCMWorkOrderView); 
				
				this.list = treeCMWorkOrderView
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboCMWorkOrderView.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treeCMWorkOrderView
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				if (node.attributes.id === IdCategoryCMWorkOrderView)
				{
					this.setRawValue(valueCategoryCMWorkOrderView);
				}
				else
				{
					this.setRawValue(node.attributes.text);
				};
				
				selectCategoryCMWorkOrderView = node.attributes;
				
				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategoryCMWorkOrderView
		}
	);
	
	
	
	var cboCategoryCMWorkOrderView = new Ext.TreeComboCMWorkOrderView
	(
		{
			id:'cboCategoryCMWorkOrderView',
			fieldLabel: nmCategoryWOCM + ' ',
			width:120,
			value:valueCategoryCMWorkOrderView
		}
	);
	
	
	return cboCategoryCMWorkOrderView;
 
};

function GetJumlahTotalPersonPartCMWorkOrder()
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahWOTotalCostCM', // cek
				//Params:	'where sch_cm_id =~' + SchIdCMWorkOrder + '~ and category_id =~' + rowSelectedCMWorkOrderTemp.data.CATEGORY_ID + '~'
                                Params:	'sch_cm_id = ~' + SchIdCMWorkOrder + '~ and category_id = ~' + rowSelectedCMWorkOrderTemp.data.CATEGORY_ID + '~'
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.get('txtTotalServiceCMWorkOrder').dom.value=formatCurrency(cst.TotalCost);
				}
				else
				{
					Ext.get('txtTotalServiceCMWorkOrder').dom.value=formatCurrency(0);
				};
			}

		}
	);
};

function GetVendorCMWorkOrder()
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetVendorCMApproveRequest', //cek
				//Params:	'where sch_cm_id=~' + SchIdCMWorkOrder + '~ and category_id=~' + varCatIdCMWorkOrder + '~'
                                Params:	'sch_cm_id = ~' + SchIdCMWorkOrder + '~ and category_id = ~' + varCatIdCMWorkOrder + '~'
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.get('txtKdVendorCMWorkOrder').dom.value=cst.Id;
					Ext.get('txtNamaVendorCMWorkOrder').dom.value=cst.Nama;
					LoadVendorInfoDetailCMWorkOrder(cst.Id);
				}
				else
				{
					Ext.get('txtKdVendorCMWorkOrder').dom.value='';
					Ext.get('txtNamaVendorCMWorkOrder').dom.value='';
				};
			}

		}
	);
};

function LoadVendorInfoDetailCMWorkOrder(id)
{
	var fldDetail = ['VENDOR_ID','VENDOR','CONTACT1','CONTACT2','VEND_ADDRESS','VEND_CITY','VEND_PHONE1','VEND_PHONE2','VEND_POS_CODE','COUNTRY'];
	
	dsLookVendorListCMWorkOrder = new WebApp.DataStore({ fields: fldDetail });
	
	dsLookVendorListCMWorkOrder.load
	(
            {
                params:
                {
                        Skip: 0,
                        Take: 1000,
                        //Sort: 'VENDOR_ID',
                        Sort: 'vendor_id',
                        Sortdir: 'ASC',
                        target: 'LookupVendor',
                        //param: 'where vendor_id=~' + id + '~'
                        param: 'vendor_id = ~' + id + '~'
                }
            }
	);
	return dsLookVendorListCMWorkOrder;
};

function GetStrTreeComboCMWorkOrderView()
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetDataTreeComboSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeComboCMWorkOrderView= cst.arr;
			},
			failure:function()
			{
			    
			}
		}
	);
};

















