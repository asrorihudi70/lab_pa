var dsDepartmentApproval;
var selectDeptLapApproval;
var selectNamaDeptLapApproval;
var now = new Date();

var frmDlgApproval;
var varLapApproval= ShowFormLapApproval();

function ShowFormLapApproval()
{
	frmDlgApproval= fnDlgApproval();
	frmDlgApproval.show();
};

function fnDlgApproval()
{  	
    var winApprovalReport = new Ext.Window
	(
		{ 
			id: 'winApprovalReport',
			title: nmTitleFormDlgApprovalRpt,
			closeAction: 'destroy',
			width:500,
			height: 143,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlgApproval()]
			
		}
	);
	
    return winApprovalReport; 
};


function ItemDlgApproval() 
{	
	var PnlLapApproval = new Ext.Panel
	(
		{ 
			id: 'PnlLapApproval',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemLapApproval_Tanggal(),getItemLapApproval_Dept(),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: nmBtnOK,
							width: 70,
							hideLabel: true,
							id: 'btnOkLapApproval',
							handler: function() 
							{
								if (ValidasiReportApproval() === 1)
								{
									//if (ValidasiTanggalReportApproval() === 1)
									//{
										var criteria = GetCriteriaApproval();
										frmDlgApproval.close();
										ShowReport('', 920002, criteria);
									//};
								};
							}
						},
						{
							xtype: 'button',
							text: nmBtnCancel ,
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapApproval',
							handler: function() 
							{
								frmDlgApproval.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapApproval;
};

function GetCriteriaApproval()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapApproval').dom.value != '') 
	{
		strKriteria = Ext.get('dtpTglAwalLapApproval').dom.value;
	};
	
	if (Ext.get('dtpTglAkhirLapApproval').dom.value != '') 
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapApproval').dom.value;
	};
	
	if (selectDeptLapApproval != undefined) 
	{
		strKriteria += '##@@##' + selectDeptLapApproval;
		strKriteria += '##@@##' + selectNamaDeptLapApproval ;
	};
	
	return strKriteria;
};


function ValidasiReportApproval()
{
	var x=1;
	
	if((Ext.get('dtpTglAwalLapApproval').dom.value === '') || (Ext.get('dtpTglAkhirLapApproval').dom.value === '') || (selectDeptLapApproval === undefined) || (Ext.get('comboDepartmentLapApproval').dom.value === ''))
	{
		if(Ext.get('dtpTglAwalLapApproval').dom.value === '')
		{
			ShowPesanWarningApprovalReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgApprovalRpt);
			x=0;
		}
		else if(Ext.get('dtpTglAkhirLapApproval').dom.value === '')
		{
		    ShowPesanWarningApprovalReport(nmGetValidasiKosong(nmFinishDateDlgRpt), nmTitleFormDlgApprovalRpt);
			x=0;
		}
		else if(Ext.get('comboDepartmentLapApproval').dom.value === '' )
		{
		    ShowPesanWarningApprovalReport(nmGetValidasiKosong(nmDeptDlgRpt), nmTitleFormDlgApprovalRpt);
			x=0;
		};
	};

	return x;
};

function ValidasiTanggalReportApproval()
{
	var x=1;
		if(Ext.get('dtpTglAwalLapApproval').dom.value > Ext.get('dtpTglAkhirLapApproval').dom.value)
		{
		    ShowPesanWarningApprovalReport(nmWarningDateDlgRpt, nmTitleFormDlgApprovalRpt);
			x=0;
		}
		
	return x;
};

function ShowPesanWarningApprovalReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:300
		}
	);
};


function getItemLapApproval_Dept()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 85,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mComboDepartmentLapApproval()
				]
			}
		]
	}
	return items;
};


function getItemLapApproval_Tanggal() 
{
   var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 0.45,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmPeriodeDlgRpt + ' ',
						id: 'dtpTglAwalLapApproval',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    },
			{
		        columnWidth: 0.48,
		        layout: 'form',
		        border: false,
				labelWidth: 25,
		        labelAlign: 'right',
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmSd + ' ',
						id: 'dtpTglAkhirLapApproval',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapApproval()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDepartmentApproval = new WebApp.DataStore({ fields: Field });

	dsDepartmentApproval.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
  var comboDepartmentLapApproval = new Ext.form.ComboBox
	(
		{
			id:'comboDepartmentLapApproval',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmDeptDlgRpt + ' ',			
			align:'Right',
			anchor:'99%',
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			store:dsDepartmentApproval,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptLapApproval=b.data.DEPT_ID ;
					selectNamaDeptLapApproval=b.data.DEPT_NAME ;
				} 
			}
		}
	);

    dsDepartmentApproval.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'DEPT_ID',
                                Sort: 'dept_id',
				Sortdir: 'ASC', 
				target:'ComboDepartment',
				param: ''
			} 
		}
	);
	return comboDepartmentLapApproval;
};



