var dsDeptLapCostMaintenance;
var selectDeptLapCostMaintenance = 'xxx';
var strUnitKerjaLapCostMaintenance = ' All';
var now = new Date();
var nmTitleFormDlgCostMaintenanceRpt='Cost Maintenance';

var frmDlgHistoryCostCum = fnDlgCostMaintenance();

frmDlgHistoryCostCum.show();


function fnDlgCostMaintenance() {
    var winLapCostMaintenance = new Ext.Window
	(
		{
		    id: 'winLapCostMaintenance',
		    title: nmTitleFormDlgCostMaintenanceRpt,
		    closeAction: 'destroy',
		    width: 500,
		    height: 143,
		    border: false,
		    resizable: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'icon_lapor',
		    modal: true,
		    items: [ItemDlgCostMaintenance()]

		}
	);

    return winLapCostMaintenance;
};


function ItemDlgCostMaintenance() {
    var PnlLapCostMaintenance = new Ext.Panel
	(
		{
		    id: 'PnlLapCostMaintenance',
		    fileUpload: true,
		    layout: 'form',
		    height: '100',
		    anchor: '100%',
		    bodyStyle: 'padding:15px',
		    border: true,
		    items:
			[
				getItemLapCostMaintenance_Tahun(), getItemLapCostMaintenance_Department(),
				{
				    layout: 'hBox',
				    border: false,
				    defaults: { margins: '0 5 0 0' },
				    style: { 'margin-left': '30px', 'margin-top': '5px' },
				    anchor: '94%',
				    layoutConfig:
					{
					    padding: '3',
					    pack: 'end',
					    align: 'middle'
					},
				    items:
					[					    
						{
						    xtype: 'button',
						    text: nmBtnOK,
						    width: 70,
						    hideLabel: true,
						    id: 'btnOkLapCostMaintenance',
						    handler: function() {
						        if (ValidasiReportCostMaintenance() === 1) {
						            var criteria = GetCriteriaCostMaintenance();
						            frmDlgHistoryCostCum.close();
						            ShowReport('', '940008', criteria);
						        };
						    }
						},
						{
						    xtype: 'button',
						    text: nmBtnCancel,
						    width: 70,
						    hideLabel: true,
						    id: 'btnCancelLapCostMaintenance',
						    handler: function() {
						        frmDlgHistoryCostCum.close();
						    }
						}
					]
				}
			]
		}
	);

    return PnlLapCostMaintenance;
};

function GetCriteriaCostMaintenance() {
    var strKriteria = '';
    var x = 0;

    if (selectDeptLapCostMaintenance != undefined) {
        strKriteria += selectDeptLapCostMaintenance + "##@@##";
        strKriteria += strUnitKerjaLapCostMaintenance + "##@@##";
        x = 2;
    };

    if (Ext.get('cboTahunLapCostMaintenance').dom.value != '') {
        strKriteria += Ext.get('cboTahunLapCostMaintenance').dom.value;
        x += 1;
    };

    strKriteria += '@#@' + x;
    return strKriteria;
};

function ValidasiReportCostMaintenance() {
    var x = 1;

    if ((Ext.get('cboTahunLapCostMaintenance').dom.value === '') || (selectDeptLapCostMaintenance === undefined) || (Ext.get('comboUnitKerjaLapCostMaintenance').dom.value === '')) {
        if (Ext.get('cboTahunLapCostMaintenance').dom.value === '') {
            ShowPesanWarningCostMaintenanceReport(nmGetValidasiKosong(nmTahunDlgRpt), nmTitleFormDlgCostMaintenanceRpt);
            x = 0;
        }
        else if (Ext.get('comboUnitKerjaLapCostMaintenance').dom.value === '') {
            ShowPesanWarningCostMaintenanceReport(nmGetValidasiKosong(nmDeptDlgRpt), nmTitleFormDlgCostMaintenanceRpt);
            x = 0;
        };
    };

    return x;
};

function ShowPesanWarningCostMaintenanceReport(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:300
		}
	);
};

function getItemLapCostMaintenance_Department() {
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    labelWidth: 85,
			    labelAlign: 'right',
			    border: false,
			    items:
				[
					mComboDepartmentLapCostMaintenance()
				]
			}
		]
	}
    return items;
};


function getItemLapCostMaintenance_Tahun() {
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 1,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[mComboTahunLapCostMaintenance()]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapCostMaintenance() {
    var Field = ['DEPT_ID', 'DEPT_NAME'];
    dsDeptLapCostMaintenance = new WebApp.DataStore({ fields: Field });

    dsDeptLapCostMaintenance.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
    var comboUnitKerjaLapCostMaintenance = new Ext.form.ComboBox
	(
		{
		    id: 'comboUnitKerjaLapCostMaintenance',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmDeptDlgRpt + ' ',
		    align: 'Right',
		    anchor: '99%',
		    valueField: 'DEPT_ID',
		    displayField: 'DEPT_NAME',
		    store: dsDeptLapCostMaintenance,
		    //value: strUnitKerjaLapCostMaintenance,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectDeptLapCostMaintenance = b.data.DEPT_ID;
			        strUnitKerjaLapCostMaintenance = b.data.DEPT_NAME;
			    }
			}
		}
	);

    dsDeptLapCostMaintenance.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
    return comboUnitKerjaLapCostMaintenance;
};

function mComboTahunLapCostMaintenance() {
    var cboTahunLapCostMaintenance = new Ext.form.ComboBox
	(
		{
		    id: 'cboTahunLapCostMaintenance',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmTahunDlgRpt + ' ',
		    width: 60,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, now.format('Y') + 2], [2, now.format('Y') + 1], [3, now.format('Y')], [4, now.format('Y') - 1], [5, now.format('Y') - 2]]
				}
			),
		    valueField: 'displayText',
		    displayField: 'displayText',
		    value: now.format('Y'),
		    listeners:
			{
			    'select': function(a, b, c) {
			        //selectCountRKAT=b.data.displayText ;
			    }
			}
		}
	);
    return cboTahunLapCostMaintenance;
};


