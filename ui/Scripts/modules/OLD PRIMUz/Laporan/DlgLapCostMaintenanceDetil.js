﻿
var dsDeptLapCostMaintenanceDetil;
var selectDeptLapCostMaintenanceDetil = 'xxx';
var strUnitKerjaLapCostMaintenanceDetil = ' All';
var now = new Date();
var nmTitleFormDlgCostMaintenanceDetilRpt='Cost Maintenance Detail';

var frmDlgHistoryCostCum = fnDlgCostMaintenanceDetil();

frmDlgHistoryCostCum.show();


function fnDlgCostMaintenanceDetil() {
    var winLapCostMaintenanceDetil = new Ext.Window
	(
		{
		    id: 'winLapCostMaintenanceDetil',
		    title: nmTitleFormDlgCostMaintenanceDetilRpt,
		    closeAction: 'destroy',
		    width: 500,
		    height: 143,
		    border: false,
		    resizable: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'icon_lapor',
		    modal: true,
		    items: [ItemDlgCostMaintenanceDetil()]

		}
	);

    return winLapCostMaintenanceDetil;
};


function ItemDlgCostMaintenanceDetil() {
    var PnlLapCostMaintenanceDetil = new Ext.Panel
	(
		{
		    id: 'PnlLapCostMaintenanceDetil',
		    fileUpload: true,
		    layout: 'form',
		    height: '100',
		    anchor: '100%',
		    bodyStyle: 'padding:15px',
		    border: true,
		    items:
			[
				getItemLapCostMaintenanceDetil_Tahun(), getItemLapCostMaintenanceDetil_Department(),
				{
				    layout: 'hBox',
				    border: false,
				    defaults: { margins: '0 5 0 0' },
				    style: { 'margin-left': '30px', 'margin-top': '5px' },
				    anchor: '94%',
				    layoutConfig:
					{
					    padding: '3',
					    pack: 'end',
					    align: 'middle'
					},
				    items:
					[					    
						{
						    xtype: 'button',
						    text: nmBtnOK,
						    width: 70,
						    hideLabel: true,
						    id: 'btnOkLapCostMaintenanceDetil',
						    handler: function() {
						        if (ValidasiReportCostMaintenanceDetil() === 1) {
						            var criteria = GetCriteriaCostMaintenanceDetil();
						            frmDlgHistoryCostCum.close();
						            ShowReport('', '940009', criteria);
						        };
						    }
						},
						{
						    xtype: 'button',
						    text: nmBtnCancel,
						    width: 70,
						    hideLabel: true,
						    id: 'btnCancelLapCostMaintenanceDetil',
						    handler: function() {
						        frmDlgHistoryCostCum.close();
						    }
						}
					]
				}
			]
		}
	);

    return PnlLapCostMaintenanceDetil;
};

function GetCriteriaCostMaintenanceDetil() {
    var strKriteria = '';
    var x = 0;

    if (selectDeptLapCostMaintenanceDetil != undefined) {
        strKriteria += selectDeptLapCostMaintenanceDetil + "##@@##";
        strKriteria += strUnitKerjaLapCostMaintenanceDetil + "##@@##";
        x = 2;
    };

    if (Ext.get('cboTahunLapCostMaintenanceDetil').dom.value != '') {
        strKriteria += Ext.get('cboTahunLapCostMaintenanceDetil').dom.value;
        x += 1;
    };

    strKriteria += '@#@' + x;
    return strKriteria;
};

function ValidasiReportCostMaintenanceDetil() {
    var x = 1;

    if ((Ext.get('cboTahunLapCostMaintenanceDetil').dom.value === '') || (selectDeptLapCostMaintenanceDetil === undefined) || (Ext.get('comboUnitKerjaLapCostMaintenanceDetil').dom.value === '')) {
        if (Ext.get('cboTahunLapCostMaintenanceDetil').dom.value === '') {
            ShowPesanWarningCostMaintenanceDetilReport(nmGetValidasiKosong(nmTahunDlgRpt), nmTitleFormDlgCostMaintenanceDetilRpt);
            x = 0;
        }
        else if (Ext.get('comboUnitKerjaLapCostMaintenanceDetil').dom.value === '') {
            ShowPesanWarningCostMaintenanceDetilReport(nmGetValidasiKosong(nmDeptDlgRpt), nmTitleFormDlgCostMaintenanceDetilRpt);
            x = 0;
        };
    };

    return x;
};

function ShowPesanWarningCostMaintenanceDetilReport(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:300
		}
	);
};

function getItemLapCostMaintenanceDetil_Department() {
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    labelWidth: 85,
			    labelAlign: 'right',
			    border: false,
			    items:
				[
					mComboDepartmentLapCostMaintenanceDetil()
				]
			}
		]
	}
    return items;
};


function getItemLapCostMaintenanceDetil_Tahun() {
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 1,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[mComboTahunLapCostMaintenanceDetil()]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapCostMaintenanceDetil() {
    var Field = ['DEPT_ID', 'DEPT_NAME'];
    dsDeptLapCostMaintenanceDetil = new WebApp.DataStore({ fields: Field });

    dsDeptLapCostMaintenanceDetil.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
    var comboUnitKerjaLapCostMaintenanceDetil = new Ext.form.ComboBox
	(
		{
		    id: 'comboUnitKerjaLapCostMaintenanceDetil',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmDeptDlgRpt + ' ',
		    align: 'Right',
		    anchor: '99%',
		    valueField: 'DEPT_ID',
		    displayField: 'DEPT_NAME',
		    store: dsDeptLapCostMaintenanceDetil,
		    //value: strUnitKerjaLapCostMaintenanceDetil,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectDeptLapCostMaintenanceDetil = b.data.DEPT_ID;
			        strUnitKerjaLapCostMaintenanceDetil = b.data.DEPT_NAME;
			    }
			}
		}
	);

    dsDeptLapCostMaintenanceDetil.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
    return comboUnitKerjaLapCostMaintenanceDetil;
};

function mComboTahunLapCostMaintenanceDetil() {
    var cboTahunLapCostMaintenanceDetil = new Ext.form.ComboBox
	(
		{
		    id: 'cboTahunLapCostMaintenanceDetil',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmTahunDlgRpt + ' ',
		    width: 60,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, now.format('Y') + 2], [2, now.format('Y') + 1], [3, now.format('Y')], [4, now.format('Y') - 1], [5, now.format('Y') - 2]]
				}
			),
		    valueField: 'displayText',
		    displayField: 'displayText',
		    value: now.format('Y'),
		    listeners:
			{
			    'select': function(a, b, c) {
			        //selectCountRKAT=b.data.displayText ;
			    }
			}
		}
	);
    return cboTahunLapCostMaintenanceDetil;
};


