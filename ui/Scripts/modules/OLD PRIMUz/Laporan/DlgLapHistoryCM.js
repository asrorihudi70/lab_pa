﻿
var dsDepartmentHistoryCM;
var selectDeptLapHistoryCM;
var selectNamaDeptLapHistoryCM;
var now = new Date();

var frmDlgHistoryCM;
var varLapHistoryCM= ShowFormLapHistoryCM();

function ShowFormLapHistoryCM()
{
	frmDlgHistoryCM= fnDlgHistoryCM();
	frmDlgHistoryCM.show();
};

function fnDlgHistoryCM()
{  	
    var winHistoryCMReport = new Ext.Window
	(
		{ 
			id: 'winHistoryCMReport',
			title: nmTitleFormDlgHistoryCMRpt,
			closeAction: 'destroy',
			width:500,
			height: 143,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlgHistoryCM()]
			
		}
	);
	
    return winHistoryCMReport; 
};


function ItemDlgHistoryCM() 
{	
	var PnlLapHistoryCM = new Ext.Panel
	(
		{ 
			id: 'PnlLapHistoryCM',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemLapHistoryCM_Tanggal(),getItemLapHistoryCM_Dept(),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: nmBtnOK,
							width: 70,
							hideLabel: true,
							id: 'btnOkLapHistoryCM',
							handler: function() 
							{
								if (ValidasiReportHistoryCM() === 1)
								{
									//if (ValidasiTanggalReportHistoryCM() === 1)
									//{
										var criteria = GetCriteriaHistoryCM();
										frmDlgHistoryCM.close();
										ShowReport('', 920006, criteria);
									//};
								};
							}
						},
						{
							xtype: 'button',
							text: nmBtnCancel ,
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapHistoryCM',
							handler: function() 
							{
								frmDlgHistoryCM.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapHistoryCM;
};

function GetCriteriaHistoryCM()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapHistoryCM').dom.value != '') 
	{
		strKriteria = Ext.get('dtpTglAwalLapHistoryCM').dom.value;
	};
	
	if (Ext.get('dtpTglAkhirLapHistoryCM').dom.value != '') 
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapHistoryCM').dom.value;
	};
	
	if (selectDeptLapHistoryCM != undefined) 
	{
		strKriteria += '##@@##' + selectDeptLapHistoryCM;
		strKriteria += '##@@##' + selectNamaDeptLapHistoryCM ;
	};
	
	return strKriteria;
};


function ValidasiReportHistoryCM()
{
	var x=1;
	
	if((Ext.get('dtpTglAwalLapHistoryCM').dom.value === '') || (Ext.get('dtpTglAkhirLapHistoryCM').dom.value === '') || (selectDeptLapHistoryCM === undefined) || (Ext.get('comboDepartmentLapHistoryCM').dom.value === ''))
	{
		if(Ext.get('dtpTglAwalLapHistoryCM').dom.value === '')
		{
			ShowPesanWarningHistoryCMReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgHistoryCMRpt);
			x=0;
		}
		else if(Ext.get('dtpTglAkhirLapHistoryCM').dom.value === '')
		{
		    ShowPesanWarningHistoryCMReport(nmGetValidasiKosong(nmFinishDateDlgRpt), nmTitleFormDlgHistoryCMRpt);
			x=0;
		}
		else if(Ext.get('comboDepartmentLapHistoryCM').dom.value === '' )
		{
		    ShowPesanWarningHistoryCMReport(nmGetValidasiKosong(nmDeptDlgRpt), nmTitleFormDlgHistoryCMRpt);
			x=0;
		};
	};

	return x;
};

function ValidasiTanggalReportHistoryCM()
{
	var x=1;
		if(Ext.get('dtpTglAwalLapHistoryCM').dom.value > Ext.get('dtpTglAkhirLapHistoryCM').dom.value)
		{
		    ShowPesanWarningHistoryCMReport(nmWarningDateDlgRpt, nmTitleFormDlgHistoryCMRpt);
			x=0;
		}
		
	return x;
};

function ShowPesanWarningHistoryCMReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:300
		}
	);
};


function getItemLapHistoryCM_Dept()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 85,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mComboDepartmentLapHistoryCM()
				]
			}
		]
	}
	return items;
};


function getItemLapHistoryCM_Tanggal() 
{
   var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 0.45,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmPeriodeDlgRpt + ' ',
						id: 'dtpTglAwalLapHistoryCM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    },
			{
		        columnWidth: 0.48,
		        layout: 'form',
		        border: false,
				labelWidth: 25,
		        labelAlign: 'right',
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmSd + ' ',
						id: 'dtpTglAkhirLapHistoryCM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapHistoryCM()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDepartmentHistoryCM = new WebApp.DataStore({ fields: Field });

	dsDepartmentHistoryCM.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
  var comboDepartmentLapHistoryCM = new Ext.form.ComboBox
	(
		{
			id:'comboDepartmentLapHistoryCM',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmDeptDlgRpt + ' ',			
			align:'Right',
			anchor:'99%',
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			store:dsDepartmentHistoryCM,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptLapHistoryCM=b.data.DEPT_ID ;
					selectNamaDeptLapHistoryCM=b.data.DEPT_NAME ;
				} 
			}
		}
	);

    dsDepartmentHistoryCM.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'DEPT_ID',
                                Sort: 'dept_id',
				Sortdir: 'ASC', 
				target:'ComboDepartment',
				param: ''
			} 
		}
	);
	return comboDepartmentLapHistoryCM;
};



