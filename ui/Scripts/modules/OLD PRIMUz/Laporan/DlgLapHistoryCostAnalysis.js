﻿
var dsDeptLapHistoryCostAnalysis;
var selectDeptLapHistoryCostAnalysis = 'xxx';
var strUnitKerjaLapHistoryCostAnalysis = ' All';
var now = new Date();

var frmDlgHistoryCostCum = fnDlgHistoryCostAnalysis();

frmDlgHistoryCostCum.show();


function fnDlgHistoryCostAnalysis() {
    var winLapHistoryCostAnalysis = new Ext.Window
	(
		{
		    id: 'winLapHistoryCostAnalysis',
		    title: nmTitleFormDlgHistoryCostRpt,
		    closeAction: 'destroy',
		    width: 500,
		    height: 143,
		    border: false,
		    resizable: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'icon_lapor',
		    modal: true,
		    items: [ItemDlgHistoryCostAnalysis()]

		}
	);

    return winLapHistoryCostAnalysis;
};


function ItemDlgHistoryCostAnalysis() {
    var PnlLapHistoryCostAnalysis = new Ext.Panel
	(
		{
		    id: 'PnlLapHistoryCostAnalysis',
		    fileUpload: true,
		    layout: 'form',
		    height: '100',
		    anchor: '100%',
		    bodyStyle: 'padding:15px',
		    border: true,
		    items:
			[
				getItemLapHistoryCostAnalysis_Tahun(), getItemLapHistoryCostAnalysis_Department(),
				{
				    layout: 'hBox',
				    border: false,
				    defaults: { margins: '0 5 0 0' },
				    style: { 'margin-left': '30px', 'margin-top': '5px' },
				    anchor: '94%',
				    layoutConfig:
					{
					    padding: '3',
					    pack: 'end',
					    align: 'middle'
					},
				    items:
					[					    
						{
						    xtype: 'button',
						    text: nmBtnOK,
						    width: 70,
						    hideLabel: true,
						    id: 'btnOkLapHistoryCostAnalysis',
						    handler: function() {
						        if (ValidasiReportHistoryCostAnalysis() === 1) {
						            var criteria = GetCriteriaHistoryCostAnalysis();
						            frmDlgHistoryCostCum.close();
						            ShowReport('', '940005', criteria);
						        };
						    }
						},
						{
						    xtype: 'button',
						    text: nmBtnCancel,
						    width: 70,
						    hideLabel: true,
						    id: 'btnCancelLapHistoryCostAnalysis',
						    handler: function() {
						        frmDlgHistoryCostCum.close();
						    }
						}
					]
				}
			]
		}
	);

    return PnlLapHistoryCostAnalysis;
};

function GetCriteriaHistoryCostAnalysis() {
    var strKriteria = '';
    var x = 0;

    if (selectDeptLapHistoryCostAnalysis != undefined) {
        strKriteria += selectDeptLapHistoryCostAnalysis + "##@@##";
        strKriteria += strUnitKerjaLapHistoryCostAnalysis + "##@@##";
        x = 2;
    };

    if (Ext.get('cboTahunLapHistoryCostAnalysis').dom.value != '') {
        strKriteria += Ext.get('cboTahunLapHistoryCostAnalysis').dom.value;
        x += 1;
    };

    strKriteria += '@#@' + x;
    return strKriteria;
};

function ValidasiReportHistoryCostAnalysis() {
    var x = 1;

    if ((Ext.get('cboTahunLapHistoryCostAnalysis').dom.value === '') || (selectDeptLapHistoryCostAnalysis === undefined) || (Ext.get('comboUnitKerjaLapHistoryCostAnalysis').dom.value === '')) {
        if (Ext.get('cboTahunLapHistoryCostAnalysis').dom.value === '') {
            ShowPesanWarningHistoryCostAnalysisReport(nmGetValidasiKosong(nmTahunDlgRpt), nmTitleFormDlgHistoryCostRpt);
            x = 0;
        }
        else if (Ext.get('comboUnitKerjaLapHistoryCostAnalysis').dom.value === '') {
            ShowPesanWarningHistoryCostAnalysisReport(nmGetValidasiKosong(nmDeptDlgRpt), nmTitleFormDlgHistoryCostRpt);
            x = 0;
        };
    };

    return x;
};

function ShowPesanWarningHistoryCostAnalysisReport(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:300
		}
	);
};

function getItemLapHistoryCostAnalysis_Department() {
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    labelWidth: 85,
			    labelAlign: 'right',
			    border: false,
			    items:
				[
					mComboDepartmentLapHistoryCostAnalysis()
				]
			}
		]
	}
    return items;
};


function getItemLapHistoryCostAnalysis_Tahun() {
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 1,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[mComboTahunLapHistoryCostAnalysis()]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapHistoryCostAnalysis() {
    var Field = ['DEPT_ID', 'DEPT_NAME'];
    dsDeptLapHistoryCostAnalysis = new WebApp.DataStore({ fields: Field });

    dsDeptLapHistoryCostAnalysis.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
    var comboUnitKerjaLapHistoryCostAnalysis = new Ext.form.ComboBox
	(
		{
		    id: 'comboUnitKerjaLapHistoryCostAnalysis',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmDeptDlgRpt + ' ',
		    align: 'Right',
		    anchor: '99%',
		    valueField: 'DEPT_ID',
		    displayField: 'DEPT_NAME',
		    store: dsDeptLapHistoryCostAnalysis,
		    //value: strUnitKerjaLapHistoryCostAnalysis,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectDeptLapHistoryCostAnalysis = b.data.DEPT_ID;
			        strUnitKerjaLapHistoryCostAnalysis = b.data.DEPT_NAME;
			    }
			}
		}
	);

    dsDeptLapHistoryCostAnalysis.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
    return comboUnitKerjaLapHistoryCostAnalysis;
};

function mComboTahunLapHistoryCostAnalysis() {
    var cboTahunLapHistoryCostAnalysis = new Ext.form.ComboBox
	(
		{
		    id: 'cboTahunLapHistoryCostAnalysis',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmTahunDlgRpt + ' ',
		    width: 60,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, now.format('Y') + 2], [2, now.format('Y') + 1], [3, now.format('Y')], [4, now.format('Y') - 1], [5, now.format('Y') - 2]]
				}
			),
		    valueField: 'displayText',
		    displayField: 'displayText',
		    value: now.format('Y'),
		    listeners:
			{
			    'select': function(a, b, c) {
			        //selectCountRKAT=b.data.displayText ;
			    }
			}
		}
	);
    return cboTahunLapHistoryCostAnalysis;
};


