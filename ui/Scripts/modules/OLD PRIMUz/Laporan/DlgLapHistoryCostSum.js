﻿
var dsDeptLapHistoryCostSum;
var selectDeptLapHistoryCostSum = 'xxx';
var strUnitKerjaLapHistoryCostSum = ' All';
var now = new Date();

var frmDlgHistoryCostCum = fnDlgHistoryCostSum();

frmDlgHistoryCostCum.show();


function fnDlgHistoryCostSum() {
    var winLapHistoryCostSum = new Ext.Window
	(
		{
		    id: 'winLapHistoryCostSum',
		    title: nmTitleFormDlgHistoryCostSumRpt,
		    closeAction: 'destroy',
		    width: 500,
		    height: 143,
		    border: false,
		    resizable: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'icon_lapor',
		    modal: true,
		    items: [ItemDlgHistoryCostSum()]

		}
	);

    return winLapHistoryCostSum;
};


function ItemDlgHistoryCostSum() {
    var PnlLapHIstoryCostSum = new Ext.Panel
	(
		{
		    id: 'PnlLapHIstoryCostSum',
		    fileUpload: true,
		    layout: 'form',
		    height: '100',
		    anchor: '100%',
		    bodyStyle: 'padding:15px',
		    border: true,
		    items:
			[
				getItemLapHIstoryCostSum_Tahun(), getItemLapHistoryCostSum_Department(),
				{
				    layout: 'hBox',
				    border: false,
				    defaults: { margins: '0 5 0 0' },
				    style: { 'margin-left': '30px', 'margin-top': '5px' },
				    anchor: '94%',
				    layoutConfig:
					{
					    padding: '3',
					    pack: 'end',
					    align: 'middle'
					},
				    items:
					[					    
						{
						    xtype: 'button',
						    text: nmBtnOK,
						    width: 70,
						    hideLabel: true,
						    id: 'btnOkLapHistoryCostSum',
						    handler: function() {
						        if (ValidasiReportHistoryCostSum() === 1) {
						            var criteria = GetCriteriaHistoryCostSum();
						            frmDlgHistoryCostCum.close();
						            ShowReport('', '940007', criteria);
						        };
						    }
						},
						{
						    xtype: 'button',
						    text: nmBtnCancel,
						    width: 70,
						    hideLabel: true,
						    id: 'btnCancelLapHistoryCostSum',
						    handler: function() {
						        frmDlgHistoryCostCum.close();
						    }
						}
					]
				}
			]
		}
	);

    return PnlLapHIstoryCostSum;
};

function GetCriteriaHistoryCostSum() {
    var strKriteria = '';
    var x = 0;

    if (selectDeptLapHistoryCostSum != undefined) {
        strKriteria += selectDeptLapHistoryCostSum + "##@@##";
        strKriteria += strUnitKerjaLapHistoryCostSum + "##@@##";
        x = 2;
    };

    if (Ext.get('cboTahunLapHistoryCostSum').dom.value != '') {
        strKriteria += Ext.get('cboTahunLapHistoryCostSum').dom.value;
        x += 1;
    };

    strKriteria += '@#@' + x;
    return strKriteria;
};

function ValidasiReportHistoryCostSum() {
    var x = 1;

    if ((Ext.get('cboTahunLapHistoryCostSum').dom.value === '') || (selectDeptLapHistoryCostSum === undefined) || (Ext.get('comboUnitKerjaLapHistoryCostSum').dom.value === '')) {
        if (Ext.get('cboTahunLapHistoryCostSum').dom.value === '') {
            ShowPesanWarningHistoryCostSumReport(nmGetValidasiKosong(nmTahunDlgRpt), nmTitleFormDlgHistoryCostSumRpt);
            x = 0;
        }
        else if (Ext.get('comboUnitKerjaLapHistoryCostSum').dom.value === '') {
            ShowPesanWarningHistoryCostSumReport(nmGetValidasiKosong(nmDeptDlgRpt), nmTitleFormDlgHistoryCostSumRpt);
            x = 0;
        };
    };

    return x;
};

function ShowPesanWarningHistoryCostSumReport(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:300
		}
	);
};

function getItemLapHistoryCostSum_Department() {
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    labelWidth: 85,
			    labelAlign: 'right',
			    border: false,
			    items:
				[
					mComboDepartmentLapHistoryCostSum()
				]
			}
		]
	}
    return items;
};


function getItemLapHIstoryCostSum_Tahun() {
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 1,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[mComboTahunLapHistoryCostSum()]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapHistoryCostSum() {
    var Field = ['DEPT_ID', 'DEPT_NAME'];
    dsDeptLapHistoryCostSum = new WebApp.DataStore({ fields: Field });

    dsDeptLapHistoryCostSum.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
    var comboUnitKerjaLapHistoryCostSum = new Ext.form.ComboBox
	(
		{
		    id: 'comboUnitKerjaLapHistoryCostSum',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmDeptDlgRpt + ' ',
		    align: 'Right',
		    anchor: '99%',
		    valueField: 'DEPT_ID',
		    displayField: 'DEPT_NAME',
		    store: dsDeptLapHistoryCostSum,
		    //value: strUnitKerjaLapHistoryCostSum,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectDeptLapHistoryCostSum = b.data.DEPT_ID;
			        strUnitKerjaLapHistoryCostSum = b.data.DEPT_NAME;
			    }
			}
		}
	);

    dsDeptLapHistoryCostSum.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
    return comboUnitKerjaLapHistoryCostSum;
};

function mComboTahunLapHistoryCostSum() {
    var cboTahunLapHistoryCostSum = new Ext.form.ComboBox
	(
		{
		    id: 'cboTahunLapHistoryCostSum',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmTahunDlgRpt + ' ',
		    width: 60,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, now.format('Y') + 2], [2, now.format('Y') + 1], [3, now.format('Y')], [4, now.format('Y') - 1], [5, now.format('Y') - 2]]
				}
			),
		    valueField: 'displayText',
		    displayField: 'displayText',
		    value: now.format('Y'),
		    listeners:
			{
			    'select': function(a, b, c) {
			        //selectCountRKAT=b.data.displayText ;
			    }
			}
		}
	);
    return cboTahunLapHistoryCostSum;
};


