﻿
var dsDepartmentHistoryPM;
var selectDeptLapHistoryPM;
var selectNamaDeptLapHistoryPM;
var now = new Date();

var frmDlgHistoryPM;
var varLapHistoryPM= ShowFormLapHistoryPM();

function ShowFormLapHistoryPM()
{
	frmDlgHistoryPM= fnDlgHistoryPM();
	frmDlgHistoryPM.show();
};

function fnDlgHistoryPM()
{  	
    var winHistoryPMReport = new Ext.Window
	(
		{ 
			id: 'winHistoryPMReport',
			title: nmTitleFormDlgHistoryPMRpt,
			closeAction: 'destroy',
			width:500,
			height: 143,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlgHistoryPM()]
			
		}
	);
	
    return winHistoryPMReport; 
};


function ItemDlgHistoryPM() 
{	
	var PnlLapHistoryPM = new Ext.Panel
	(
		{ 
			id: 'PnlLapHistoryPM',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemLapHistoryPM_Tanggal(),getItemLapHistoryPM_Dept(),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: nmBtnOK,
							width: 70,
							hideLabel: true,
							id: 'btnOkLapHistoryPM',
							handler: function() 
							{
								if (ValidasiReportHistoryPM() === 1)
								{
									//if (ValidasiTanggalReportHistoryPM() === 1)
									//{
										var criteria = GetCriteriaHistoryPM();
										frmDlgHistoryPM.close();
										ShowReport('', 910004, criteria);
									//};
								};
							}
						},
						{
							xtype: 'button',
							text: nmBtnCancel,
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapHistoryPM',
							handler: function() 
							{
								frmDlgHistoryPM.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapHistoryPM;
};

function GetCriteriaHistoryPM()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapHistoryPM').dom.value != '') 
	{
		strKriteria = Ext.get('dtpTglAwalLapHistoryPM').dom.value;
	};
	
	if (Ext.get('dtpTglAkhirLapHistoryPM').dom.value != '') 
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapHistoryPM').dom.value;
	};
	
	if (selectDeptLapHistoryPM != undefined) 
	{
		strKriteria += '##@@##' + selectDeptLapHistoryPM;
		strKriteria += '##@@##' + selectNamaDeptLapHistoryPM ;
	};
	
	return strKriteria;
};


function ValidasiReportHistoryPM()
{
	var x=1;
	
	if((Ext.get('dtpTglAwalLapHistoryPM').dom.value === '') || (Ext.get('dtpTglAkhirLapHistoryPM').dom.value === '') || (selectDeptLapHistoryPM === undefined) || (Ext.get('comboDepartmentLapHistoryPM').dom.value === ''))
	{
		if(Ext.get('dtpTglAwalLapHistoryPM').dom.value === '')
		{
			ShowPesanWarningHistoryPMReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgHistoryPMRpt);
			x=0;
		}
		else if(Ext.get('dtpTglAkhirLapHistoryPM').dom.value === '')
		{
		    ShowPesanWarningHistoryPMReport(nmGetValidasiKosong(nmFinishDateDlgRpt), nmTitleFormDlgHistoryPMRpt);
			x=0;
		}
		else if(Ext.get('comboDepartmentLapHistoryPM').dom.value === '' )
		{
		    ShowPesanWarningHistoryPMReport(nmGetValidasiKosong(nmDeptDlgRpt), nmTitleFormDlgHistoryPMRpt);
			x=0;
		};
	};

	return x;
};

function ValidasiTanggalReportHistoryPM()
{
	var x=1;
		if(Ext.get('dtpTglAwalLapHistoryPM').dom.value > Ext.get('dtpTglAkhirLapHistoryPM').dom.value)
		{
		    ShowPesanWarningHistoryPMReport(nmWarningDateDlgRpt, nmTitleFormDlgHistoryPMRpt);
			x=0;
		}
		
	return x;
};

function ShowPesanWarningHistoryPMReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:300
		}
	);
};


function getItemLapHistoryPM_Dept()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 85,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mComboDepartmentLapHistoryPM()
				]
			}
		]
	}
	return items;
};


function getItemLapHistoryPM_Tanggal() 
{
   var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 0.45,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmPeriodeDlgRpt + ' ',
						id: 'dtpTglAwalLapHistoryPM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    },
			{
		        columnWidth: 0.48,
		        layout: 'form',
		        border: false,
				labelWidth: 25,
		        labelAlign: 'right',
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmSd + ' ',
						id: 'dtpTglAkhirLapHistoryPM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapHistoryPM()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDepartmentHistoryPM = new WebApp.DataStore({ fields: Field });

	dsDepartmentHistoryPM.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
  var comboDepartmentLapHistoryPM = new Ext.form.ComboBox
	(
		{
			id:'comboDepartmentLapHistoryPM',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmDeptDlgRpt + ' ',			
			align:'Right',
			anchor:'99%',
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			store:dsDepartmentHistoryPM,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptLapHistoryPM=b.data.DEPT_ID ;
					selectNamaDeptLapHistoryPM=b.data.DEPT_NAME ;
				} 
			}
		}
	);

    dsDepartmentHistoryPM.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'DEPT_ID',
                                Sort: 'dept_id',
				Sortdir: 'ASC', 
				target:'ComboDepartment',
				param: ''
			} 
		}
	);
	return comboDepartmentLapHistoryPM;
};



