﻿
var dsDepartmentHistoryParts;
var selectDeptLapHistoryParts;
var selectNamaDeptLapHistoryParts;
var now = new Date();

var frmDlgHistoryParts;
var varLapHistoryParts= ShowFormLapHistoryParts();

function ShowFormLapHistoryParts()
{
	frmDlgHistoryParts= fnDlgHistoryParts();
	frmDlgHistoryParts.show();
};

function fnDlgHistoryParts()
{  	
    var winHistoryPartsReport = new Ext.Window
	(
		{ 
			id: 'winHistoryPartsReport',
			title: nmTitleFormDlgHistoryPartRpt,
			closeAction: 'destroy',
			width:500,
			height: 143,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlgHistoryParts()]
			
		}
	);
	
    return winHistoryPartsReport; 
};


function ItemDlgHistoryParts() 
{	
	var PnlLapHistoryParts = new Ext.Panel
	(
		{ 
			id: 'PnlLapHistoryParts',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemLapHistoryParts_Tanggal(),getItemLapHistoryParts_Dept(),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: nmBtnOK,
							width: 70,
							hideLabel: true,
							id: 'btnOkLapHistoryParts',
							handler: function() 
							{
								if (ValidasiReportHistoryParts() === 1)
								{
									//if (ValidasiTanggalReportHistoryParts() === 1)
									//{
										var criteria = GetCriteriaHistoryParts();
										frmDlgHistoryParts.close();
										ShowReport('', 940006, criteria);
									//};
								};
							}
						},
						{
							xtype: 'button',
							text: nmBtnCancel,
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapHistoryParts',
							handler: function() 
							{
								frmDlgHistoryParts.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapHistoryParts;
};

function GetCriteriaHistoryParts()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapHistoryParts').dom.value != '') 
	{
		strKriteria = Ext.get('dtpTglAwalLapHistoryParts').dom.value;
	};
	
	if (Ext.get('dtpTglAkhirLapHistoryParts').dom.value != '') 
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapHistoryParts').dom.value;
	};
	
	if (selectDeptLapHistoryParts != undefined) 
	{
		strKriteria += '##@@##' + selectDeptLapHistoryParts;
		strKriteria += '##@@##' + selectNamaDeptLapHistoryParts ;
	};
	
	return strKriteria;
};


function ValidasiReportHistoryParts()
{
	var x=1;
	
	if((Ext.get('dtpTglAwalLapHistoryParts').dom.value === '') || (Ext.get('dtpTglAkhirLapHistoryParts').dom.value === '') || (selectDeptLapHistoryParts === undefined) || (Ext.get('comboDepartmentLapHistoryParts').dom.value === ''))
	{
		if(Ext.get('dtpTglAwalLapHistoryParts').dom.value === '')
		{
			ShowPesanWarningHistoryPartsReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgHistoryPartRpt);
			x=0;
		}
		else if(Ext.get('dtpTglAkhirLapHistoryParts').dom.value === '')
		{
			ShowPesanWarningHistoryPartsReport(nmGetValidasiKosong(nmFinishDateDlgRpt),nmTitleFormDlgHistoryPartRpt);
			x=0;
		}
		else if(Ext.get('comboDepartmentLapHistoryParts').dom.value === '' )
		{
			ShowPesanWarningHistoryPartsReport(nmGetValidasiKosong(nmDeptDlgRpt),nmTitleFormDlgHistoryPartRpt);
			x=0;
		};
	};

	return x;
};

function ValidasiTanggalReportHistoryParts()
{
	var x=1;
		if(Ext.get('dtpTglAwalLapHistoryParts').dom.value > Ext.get('dtpTglAkhirLapHistoryParts').dom.value)
		{
			ShowPesanWarningHistoryPartsReport(nmWarningDateDlgRpt,nmTitleFormDlgHistoryPartRpt);
			x=0;
		}
		
	return x;
};

function ShowPesanWarningHistoryPartsReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:300
		}
	);
};


function getItemLapHistoryParts_Dept()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 85,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mComboDepartmentLapHistoryParts()
				]
			}
		]
	}
	return items;
};


function getItemLapHistoryParts_Tanggal() 
{
   var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 0.45,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmPeriodeDlgRpt + ' ',
						id: 'dtpTglAwalLapHistoryParts',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    },
			{
		        columnWidth: 0.48,
		        layout: 'form',
		        border: false,
				labelWidth: 25,
		        labelAlign: 'right',
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmSd + ' ',
						id: 'dtpTglAkhirLapHistoryParts',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapHistoryParts()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDepartmentHistoryParts = new WebApp.DataStore({ fields: Field });

	dsDepartmentHistoryParts.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
  var comboDepartmentLapHistoryParts = new Ext.form.ComboBox
	(
		{
			id:'comboDepartmentLapHistoryParts',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmDeptDlgRpt + ' ',			
			align:'Right',
			anchor:'99%',
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			store:dsDepartmentHistoryParts,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptLapHistoryParts=b.data.DEPT_ID ;
					selectNamaDeptLapHistoryParts=b.data.DEPT_NAME ;
				} 
			}
		}
	);

    dsDepartmentHistoryParts.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'DEPT_ID',
                                Sort: 'dept_id',
				Sortdir: 'ASC', 
				target:'ComboDepartment',
				param: ''
			} 
		}
	);
	return comboDepartmentLapHistoryParts;
};



