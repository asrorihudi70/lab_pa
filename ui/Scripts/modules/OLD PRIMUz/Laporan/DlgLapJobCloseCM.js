﻿
var dsDepartmentJobCloseCM;
var selectDeptLapJobCloseCM;
var selectNamaDeptLapJobCloseCM;
var now = new Date();

var frmDlgJobCloseCM;
var varLapJobCloseCM= ShowFormLapJobCloseCM();

function ShowFormLapJobCloseCM()
{
	frmDlgJobCloseCM= fnDlgJobCloseCM();
	frmDlgJobCloseCM.show();
};

function fnDlgJobCloseCM()
{  	
    var winJobCloseCMReport = new Ext.Window
	(
		{ 
			id: 'winJobCloseCMReport',
			title: nmTitleFormDlgResultCMRpt,
			closeAction: 'destroy',
			width:500,
			height: 143,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlgJobCloseCM()]
			
		}
	);
	
    return winJobCloseCMReport; 
};


function ItemDlgJobCloseCM() 
{	
	var PnlLapJobCloseCM = new Ext.Panel
	(
		{ 
			id: 'PnlLapJobCloseCM',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemLapJobCloseCM_Tanggal(),getItemLapJobCloseCM_Dept(),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: nmBtnOK,
							width: 70,
							hideLabel: true,
							id: 'btnOkLapJobCloseCM',
							handler: function() 
							{
								if (ValidasiReportJobCloseCM() === 1)
								{
									//if (ValidasiTanggalReportJobCloseCM() === 1)
									//{
										var criteria = GetCriteriaJobCloseCM();
										frmDlgJobCloseCM.close();
										ShowReport('', 920005, criteria);
									//};
								};
							}
						},
						{
							xtype: 'button',
							text: nmBtnCancel,
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapJobCloseCM',
							handler: function() 
							{
								frmDlgJobCloseCM.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapJobCloseCM;
};

function GetCriteriaJobCloseCM()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapJobCloseCM').dom.value != '') 
	{
		strKriteria = Ext.get('dtpTglAwalLapJobCloseCM').dom.value;
	};
	
	if (Ext.get('dtpTglAkhirLapJobCloseCM').dom.value != '') 
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapJobCloseCM').dom.value;
	};
	
	if (selectDeptLapJobCloseCM != undefined) 
	{
		strKriteria += '##@@##' + selectDeptLapJobCloseCM;
		strKriteria += '##@@##' + selectNamaDeptLapJobCloseCM ;
	};
	
	return strKriteria;
};


function ValidasiReportJobCloseCM()
{
	var x=1;
	
	if((Ext.get('dtpTglAwalLapJobCloseCM').dom.value === '') || (Ext.get('dtpTglAkhirLapJobCloseCM').dom.value === '') || (selectDeptLapJobCloseCM === undefined) || (Ext.get('comboDepartmentLapJobCloseCM').dom.value === ''))
	{
		if(Ext.get('dtpTglAwalLapJobCloseCM').dom.value === '')
		{
			ShowPesanWarningJobCloseCMReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgResultCMRpt);
			x=0;
		}
		else if(Ext.get('dtpTglAkhirLapJobCloseCM').dom.value === '')
		{
		    ShowPesanWarningJobCloseCMReport(nmGetValidasiKosong(nmFinishDateDlgRpt), nmTitleFormDlgResultCMRpt);
			x=0;
		}
		else if(Ext.get('comboDepartmentLapJobCloseCM').dom.value === '' )
		{
		    ShowPesanWarningJobCloseCMReport(nmGetValidasiKosong(nmDeptDlgRpt), nmTitleFormDlgResultCMRpt);
			x=0;
		};
	};

	return x;
};

function ValidasiTanggalReportJobCloseCM()
{
	var x=1;
		if(Ext.get('dtpTglAwalLapJobCloseCM').dom.value > Ext.get('dtpTglAkhirLapJobCloseCM').dom.value)
		{
		    ShowPesanWarningJobCloseCMReport(nmWarningDateDlgRpt, nmTitleFormDlgResultCMRpt);
			x=0;
		}
		
	return x;
};

function ShowPesanWarningJobCloseCMReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:300
		}
	);
};


function getItemLapJobCloseCM_Dept()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 85,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mComboDepartmentLapJobCloseCM()
				]
			}
		]
	}
	return items;
};


function getItemLapJobCloseCM_Tanggal() 
{
   var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 0.45,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmPeriodeDlgRpt + ' ',
						id: 'dtpTglAwalLapJobCloseCM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    },
			{
		        columnWidth: 0.48,
		        layout: 'form',
		        border: false,
				labelWidth: 25,
		        labelAlign: 'right',
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmSd + ' ',
						id: 'dtpTglAkhirLapJobCloseCM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapJobCloseCM()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDepartmentJobCloseCM = new WebApp.DataStore({ fields: Field });

	dsDepartmentJobCloseCM.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
  var comboDepartmentLapJobCloseCM = new Ext.form.ComboBox
	(
		{
			id:'comboDepartmentLapJobCloseCM',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmDeptDlgRpt + ' ',			
			align:'Right',
			anchor:'99%',
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			store:dsDepartmentJobCloseCM,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptLapJobCloseCM=b.data.DEPT_ID ;
					selectNamaDeptLapJobCloseCM=b.data.DEPT_NAME ;
				} 
			}
		}
	);

    dsDepartmentJobCloseCM.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'DEPT_ID',
                                Sort: 'dept_id',
				Sortdir: 'ASC', 
				target:'ComboDepartment',
				param: ''
			} 
		}
	);
	return comboDepartmentLapJobCloseCM;
};



