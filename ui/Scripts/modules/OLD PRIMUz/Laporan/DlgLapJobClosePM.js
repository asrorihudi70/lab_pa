﻿
var dsDepartmentJobClosePM;
var selectDeptLapJobClosePM;
var selectNamaDeptLapJobClosePM;
var now = new Date();

var frmDlgJobClosePM;
var varLapJobClosePM= ShowFormLapJobClosePM();

function ShowFormLapJobClosePM()
{
	frmDlgJobClosePM= fnDlgJobClosePM();
	frmDlgJobClosePM.show();
};

function fnDlgJobClosePM()
{  	
    var winJobClosePMReport = new Ext.Window
	(
		{ 
			id: 'winJobClosePMReport',
			title: nmTitleFormDlgResultPMRpt,
			closeAction: 'destroy',
			width:500,
			height: 143,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlgJobClosePM()]
			
		}
	);
	
    return winJobClosePMReport; 
};


function ItemDlgJobClosePM() 
{	
	var PnlLapJobClosePM = new Ext.Panel
	(
		{ 
			id: 'PnlLapJobClosePM',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemLapJobClosePM_Tanggal(),getItemLapJobClosePM_Dept(),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: nmBtnOK,
							width: 70,
							hideLabel: true,
							id: 'btnOkLapJobClosePM',
							handler: function() 
							{
								if (ValidasiReportJobClosePM() === 1)
								{
									//if (ValidasiTanggalReportJobClosePM() === 1)
									//{
										var criteria = GetCriteriaJobClosePM();
										frmDlgJobClosePM.close();
										ShowReport('', 910006, criteria);
									//};
								};
							}
						},
						{
							xtype: 'button',
							text: nmBtnCancel,
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapJobClosePM',
							handler: function() 
							{
								frmDlgJobClosePM.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapJobClosePM;
};

function GetCriteriaJobClosePM()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapJobClosePM').dom.value != '') 
	{
		strKriteria = Ext.get('dtpTglAwalLapJobClosePM').dom.value;
	};
	
	if (Ext.get('dtpTglAkhirLapJobClosePM').dom.value != '') 
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapJobClosePM').dom.value;
	};
	
	if (selectDeptLapJobClosePM != undefined) 
	{
		strKriteria += '##@@##' + selectDeptLapJobClosePM;
		strKriteria += '##@@##' + selectNamaDeptLapJobClosePM ;
	};
	
	return strKriteria;
};


function ValidasiReportJobClosePM()
{
	var x=1;
	
	if((Ext.get('dtpTglAwalLapJobClosePM').dom.value === '') || (Ext.get('dtpTglAkhirLapJobClosePM').dom.value === '') || (selectDeptLapJobClosePM === undefined) || (Ext.get('comboDepartmentLapJobClosePM').dom.value === ''))
	{
		if(Ext.get('dtpTglAwalLapJobClosePM').dom.value === '')
		{
			ShowPesanWarningJobClosePMReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgResultPMRpt);
			x=0;
		}
		else if(Ext.get('dtpTglAkhirLapJobClosePM').dom.value === '')
		{
		    ShowPesanWarningJobClosePMReport(nmGetValidasiKosong(nmFinishDateDlgRpt), nmTitleFormDlgResultPMRpt);
			x=0;
		}
		else if(Ext.get('comboDepartmentLapJobClosePM').dom.value === '' )
		{
		    ShowPesanWarningJobClosePMReport(nmGetValidasiKosong(nmDeptDlgRpt), nmTitleFormDlgResultPMRpt);
			x=0;
		};
	};

	return x;
};

function ValidasiTanggalReportJobClosePM()
{
	var x=1;
		if(Ext.get('dtpTglAwalLapJobClosePM').dom.value > Ext.get('dtpTglAkhirLapJobClosePM').dom.value)
		{
		    ShowPesanWarningJobClosePMReport(nmWarningDateDlgRpt, nmTitleFormDlgResultPMRpt);
			x=0;
		}
		
	return x;
};

function ShowPesanWarningJobClosePMReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:300
		}
	);
};


function getItemLapJobClosePM_Dept()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 85,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mComboDepartmentLapJobClosePM()
				]
			}
		]
	}
	return items;
};


function getItemLapJobClosePM_Tanggal() 
{
   var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 0.45,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmPeriodeDlgRpt + ' ',
						id: 'dtpTglAwalLapJobClosePM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    },
			{
		        columnWidth: 0.48,
		        layout: 'form',
		        border: false,
				labelWidth: 25,
		        labelAlign: 'right',
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmSd + ' ',
						id: 'dtpTglAkhirLapJobClosePM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapJobClosePM()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDepartmentJobClosePM = new WebApp.DataStore({ fields: Field });

	dsDepartmentJobClosePM.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
  var comboDepartmentLapJobClosePM = new Ext.form.ComboBox
	(
		{
			id:'comboDepartmentLapJobClosePM',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmDeptDlgRpt + ' ',			
			align:'Right',
			anchor:'99%',
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			store:dsDepartmentJobClosePM,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptLapJobClosePM=b.data.DEPT_ID ;
					selectNamaDeptLapJobClosePM=b.data.DEPT_NAME ;
				} 
			}
		}
	);

    dsDepartmentJobClosePM.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'DEPT_ID',
                                Sort: 'dept_id',
				Sortdir: 'ASC', 
				target:'ComboDepartment',
				param: ''
			} 
		}
	);
	return comboDepartmentLapJobClosePM;
};



