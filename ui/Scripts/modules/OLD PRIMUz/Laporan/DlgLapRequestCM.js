
var dsDepartmentRequestCM;
var selectDeptLapRequestCM;
var selectNamaDeptLapRequestCM;
var now = new Date();

var frmDlgRequestCM;
var varLapRequestCM= ShowFormLapRequestCM();

function ShowFormLapRequestCM()
{
    frmDlgRequestCM= fnDlgRequestCM();
    frmDlgRequestCM.show();
};

function fnDlgRequestCM()
{  	
    var winRequestCMReport = new Ext.Window
    (
        {
            id: 'winRequestCMReport',
            title: nmTitleFormDlgReqCMRpt,
            closeAction: 'destroy',
            width:500,
            height: 143,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRequestCM()]

        }
    );
	
    return winRequestCMReport; 
};


function ItemDlgRequestCM() 
{	
    var PnlLapRequestCM = new Ext.Panel
    (
        {
            id: 'PnlLapRequestCM',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRequestCM_Tanggal(),getItemLapRequestCM_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
                            id: 'btnOkLapRequestCM',
                            handler: function()
                            {
                                if (ValidasiReportRequestCM() === 1)
                                {
                                    //if (ValidasiTanggalReportRequestCM() === 1)
                                    //{
                                        var criteria = GetCriteriaRequestCM();
                                        frmDlgRequestCM.close();
                                        ShowReport('', 920001, criteria);
                                    //};
                                };
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
                            id: 'btnCancelLapRequestCM',
                            handler: function()
                            {
                                    frmDlgRequestCM.close();
                            }
                        }
                    ]
                }
            ]
        }
    );
 
    return PnlLapRequestCM;
};

function GetCriteriaRequestCM()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapRequestCM').dom.value != '') 
	{
		strKriteria = Ext.get('dtpTglAwalLapRequestCM').dom.value;
	};
	
	if (Ext.get('dtpTglAkhirLapRequestCM').dom.value != '') 
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapRequestCM').dom.value;
	};
	
	if (selectDeptLapRequestCM != undefined) 
	{
		strKriteria += '##@@##' + selectDeptLapRequestCM;
		strKriteria += '##@@##' + selectNamaDeptLapRequestCM ;
	};
	
	return strKriteria;
};


function ValidasiReportRequestCM()
{
    var x=1;

    if((Ext.get('dtpTglAwalLapRequestCM').dom.value === '') || (Ext.get('dtpTglAkhirLapRequestCM').dom.value === '') || (selectDeptLapRequestCM === undefined) || (Ext.get('comboDepartmentLapRequestCM').dom.value === ''))
    {
            if(Ext.get('dtpTglAwalLapRequestCM').dom.value === '')
            {
                    ShowPesanWarningRequestCMReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('dtpTglAkhirLapRequestCM').dom.value === '')
            {
                    ShowPesanWarningRequestCMReport(nmGetValidasiKosong(nmFinishDateDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            }
            else if(Ext.get('comboDepartmentLapRequestCM').dom.value === '' )
            {
                    ShowPesanWarningRequestCMReport(nmGetValidasiKosong(nmDeptDlgRpt),nmTitleFormDlgReqCMRpt);
                    x=0;
            };
    };

    return x;
};

function ValidasiTanggalReportRequestCM()
{
    var x=1;
    if(Ext.get('dtpTglAwalLapRequestCM').dom.value > Ext.get('dtpTglAkhirLapRequestCM').dom.value)
    {
        ShowPesanWarningRequestCMReport(nmWarningDateDlgRpt,nmTitleFormDlgReqCMRpt);
        x=0;
    }

    return x;
};

function ShowPesanWarningRequestCMReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};


function getItemLapRequestCM_Dept()
{
    var items =
    {
        layout:'column',
        border:false,
        items:
        [
            {
                columnWidth:1,
                layout: 'form',
                labelWidth: 85,
                labelAlign:'right',
                border:false,
                items:
                [
                    mComboDepartmentLapRequestCM()
                ]
            }
        ]
    }
    return items;
};


function getItemLapRequestCM_Tanggal() 
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
        {
            columnWidth: 0.45,
            layout: 'form',
            border: false,
            labelAlign: 'right',
            labelWidth: 85,
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmPeriodeDlgRpt + ' ',
                    id: 'dtpTglAwalLapRequestCM',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        },
        {
            columnWidth: 0.48,
            layout: 'form',
            border: false,
                    labelWidth: 25,
            labelAlign: 'right',
            items:
            [
                {
                    xtype: 'datefield',
                    fieldLabel: nmSd + ' ',
                    id: 'dtpTglAkhirLapRequestCM',
                    format: 'd/M/Y',
                    value:now,
                    width:105
                }
            ]
        }
        ]
    }
    return items;
};


function mComboDepartmentLapRequestCM()
{
    var Field = ['DEPT_ID', 'DEPT_NAME'];
    dsDepartmentRequestCM = new WebApp.DataStore({ fields: Field });

    dsDepartmentRequestCM.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_NAME',
                Sort: 'dept_name',
                Sortdir: 'ASC',
                target: 'ComboDepartment',
                param: ''
            }
        }
    );

  var comboDepartmentLapRequestCM = new Ext.form.ComboBox
    (
        {
            id:'comboDepartmentLapRequestCM',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: nmDeptDlgRpt + ' ',
            align:'Right',
            anchor:'99%',
            valueField: 'DEPT_ID',
            displayField: 'DEPT_NAME',
            store:dsDepartmentRequestCM,
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectDeptLapRequestCM=b.data.DEPT_ID ;
                    selectNamaDeptLapRequestCM=b.data.DEPT_NAME ;
                }
            }
        }
    );

    dsDepartmentRequestCM.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: 'dept_id',
                Sortdir: 'ASC',
                target:'ComboDepartment',
                param: ''
            }
        }
    );
    return comboDepartmentLapRequestCM;
};



