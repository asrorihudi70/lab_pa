﻿
var dsDepartmentSchCM;
var selectDeptLapSchCM;
var selectNamaDeptLapSchCM;
var now = new Date();

var frmDlgScheduleCM;
var varLapScheduleCM= ShowFormLapScheduleCM();

function ShowFormLapScheduleCM()
{
	frmDlgScheduleCM= fnDlgScheduleCM();
	frmDlgScheduleCM.show();
};

function fnDlgScheduleCM()
{  	
    var winScheduleCMReport = new Ext.Window
	(
		{ 
			id: 'winScheduleCMReport',
			title: nmTitleFormDlgSchCMRpt,
			closeAction: 'destroy',
			width:500,
			height: 143,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlgScheduleCM()]
			
		}
	);
	
    return winScheduleCMReport; 
};


function ItemDlgScheduleCM() 
{	
	var PnlLapScheduleCM = new Ext.Panel
	(
		{ 
			id: 'PnlLapScheduleCM',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemLapScheduleCM_Tanggal(),getItemLapScheduleCM_Dept(),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: nmBtnOK,
							width: 70,
							hideLabel: true,
							id: 'btnOkLapScheduleCM',
							handler: function() 
							{
								if (ValidasiReportScheduleCM() === 1)
								{
									//if (ValidasiTanggalReportScheduleCM() === 1)
									//{
										var criteria = GetCriteriaScheduleCM();
										frmDlgScheduleCM.close();
										ShowReport('', 920003, criteria);
									//};
								};
							}
						},
						{
							xtype: 'button',
							text: nmBtnCancel,
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapScheduleCM',
							handler: function() 
							{
								frmDlgScheduleCM.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapScheduleCM;
};

function GetCriteriaScheduleCM()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapScheduleCM').dom.value != '') 
	{
		strKriteria = Ext.get('dtpTglAwalLapScheduleCM').dom.value;
	};
	
	if (Ext.get('dtpTglAkhirLapScheduleCM').dom.value != '') 
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapScheduleCM').dom.value;
	};
	
	if (selectDeptLapSchCM != undefined) 
	{
		strKriteria += '##@@##' + selectDeptLapSchCM;
		strKriteria += '##@@##' + selectNamaDeptLapSchCM ;
	};
	
	return strKriteria;
};


function ValidasiReportScheduleCM()
{
	var x=1;
	
	if((Ext.get('dtpTglAwalLapScheduleCM').dom.value === '') || (Ext.get('dtpTglAkhirLapScheduleCM').dom.value === '') || (selectDeptLapSchCM === undefined) || (Ext.get('comboDepartmentLapScheduleCM').dom.value === ''))
	{
		if(Ext.get('dtpTglAwalLapScheduleCM').dom.value === '')
		{
			ShowPesanWarningScheduleCMReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgSchCMRpt);
			x=0;
		}
		else if(Ext.get('dtpTglAkhirLapScheduleCM').dom.value === '')
		{
			ShowPesanWarningScheduleCMReport(nmGetValidasiKosong(nmFinishDateDlgRpt),nmTitleFormDlgSchCMRpt);
			x=0;
		}
		else if(Ext.get('comboDepartmentLapScheduleCM').dom.value === '' )
		{
			ShowPesanWarningScheduleCMReport(nmGetValidasiKosong(nmDeptDlgRpt),nmTitleFormDlgSchCMRpt);
			x=0;
		};
	};

	return x;
};

function ValidasiTanggalReportScheduleCM()
{
	var x=1;
		if(Ext.get('dtpTglAwalLapScheduleCM').dom.value > Ext.get('dtpTglAkhirLapScheduleCM').dom.value)
		{
			ShowPesanWarningScheduleCMReport(nmWarningDateDlgRpt,nmTitleFormDlgSchCMRpt);
			x=0;
		}
		
	return x;
};

function ShowPesanWarningScheduleCMReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:300
		}
	);
};


function getItemLapScheduleCM_Dept()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 85,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mComboDepartmentLapScheduleCM()
				]
			}
		]
	}
	return items;
};


function getItemLapScheduleCM_Tanggal() 
{
   var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 0.45,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmPeriodeDlgRpt + ' ',
						id: 'dtpTglAwalLapScheduleCM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    },
			{
		        columnWidth: 0.48,
		        layout: 'form',
		        border: false,
				labelWidth: 25,
		        labelAlign: 'right',
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmSd + ' ',
						id: 'dtpTglAkhirLapScheduleCM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapScheduleCM()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDepartmentSchCM = new WebApp.DataStore({ fields: Field });

	dsDepartmentSchCM.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
  var comboDepartmentLapScheduleCM = new Ext.form.ComboBox
	(
		{
			id:'comboDepartmentLapScheduleCM',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmDeptDlgRpt + ' ',			
			align:'Right',
			anchor:'99%',
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			store:dsDepartmentSchCM,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptLapSchCM=b.data.DEPT_ID ;
					selectNamaDeptLapSchCM=b.data.DEPT_NAME ;
				} 
			}
		}
	);

    dsDepartmentSchCM.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'DEPT_ID',
                                Sort: 'dept_id',
				Sortdir: 'ASC', 
				target:'ComboDepartment',
				param: ''
			} 
		}
	);
	return comboDepartmentLapScheduleCM;
};



