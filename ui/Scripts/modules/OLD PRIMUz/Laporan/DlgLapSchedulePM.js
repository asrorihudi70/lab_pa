﻿
var dsDepartmentSchedulePM;
var selectDeptLapSchedulePM;
var selectNamaDeptLapSchedulePM;
var now = new Date();

var frmDlgSchedulePM;
var varLapSchedulePM= ShowFormLapSchedulePM();

function ShowFormLapSchedulePM()
{
	frmDlgSchedulePM= fnDlgSchedulePM();
	frmDlgSchedulePM.show();
};

function fnDlgSchedulePM()
{  	
    var winSchedulePMReport = new Ext.Window
	(
		{ 
			id: 'winSchedulePMReport',
			title: nmTitleFormDlgSchPMRpt,
			closeAction: 'destroy',
			width:500,
			height: 143,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlgSchedulePM()]
			
		}
	);
	
    return winSchedulePMReport; 
};


function ItemDlgSchedulePM() 
{	
    var PnlLapSchedulePM = new Ext.Panel
    (
        {
            id: 'PnlLapSchedulePM',
            fileUpload: true,
            layout: 'form',
            height: '100',
            anchor: '100%',
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapSchedulePM_Tanggal(),getItemLapSchedulePM_Dept(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '94%',
                    layoutConfig:
                    {
                            padding: '3',
                            pack: 'end',
                            align: 'middle'
                    },
                    items:
                    [
                            {
                                    xtype: 'button',
                                    text: nmBtnOK,
                                    width: 70,
                                    hideLabel: true,
                                    id: 'btnOkLapSchedulePM',
                                    handler: function()
                                    {
                                            if (ValidasiReportSchedulePM() === 1)
                                            {
                                                    //if (ValidasiTanggalReportSchedulePM() === 1)
                                                    //{
                                                            var criteria = GetCriteriaSchedulePM();
                                                            frmDlgSchedulePM.close();
                                                            ShowReport('', 910002, criteria);
                                                    //};
                                            };
                                    }
                            },
                            {
                                    xtype: 'button',
                                    text: nmBtnCancel,
                                    width: 70,
                                    hideLabel: true,
                                    id: 'btnCancelLapSchedulePM',
                                    handler: function()
                                    {
                                            frmDlgSchedulePM.close();
                                    }
                            }
                    ]
                }
            ]
        }
    );
 
    return PnlLapSchedulePM;
};

function GetCriteriaSchedulePM()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapSchedulePM').dom.value != '') 
	{
		strKriteria = Ext.get('dtpTglAwalLapSchedulePM').dom.value;
	};
	
	if (Ext.get('dtpTglAkhirLapSchedulePM').dom.value != '') 
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapSchedulePM').dom.value;
	};
	
	if (selectDeptLapSchedulePM != undefined) 
	{
		strKriteria += '##@@##' + selectDeptLapSchedulePM;
		strKriteria += '##@@##' + selectNamaDeptLapSchedulePM ;
	};
	
	return strKriteria;
};


function ValidasiReportSchedulePM()
{
	var x=1;
	
	if((Ext.get('dtpTglAwalLapSchedulePM').dom.value === '') || (Ext.get('dtpTglAkhirLapSchedulePM').dom.value === '') || (selectDeptLapSchedulePM === undefined) || (Ext.get('comboDepartmentLapSchedulePM').dom.value === ''))
	{
		if(Ext.get('dtpTglAwalLapSchedulePM').dom.value === '')
		{
			ShowPesanWarningSchedulePMReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgSchPMRpt);
			x=0;
		}
		else if(Ext.get('dtpTglAkhirLapSchedulePM').dom.value === '')
		{
			ShowPesanWarningSchedulePMReport(nmGetValidasiKosong(nmFinishDateDlgRpt),nmTitleFormDlgSchPMRpt);
			x=0;
		}
		else if(Ext.get('comboDepartmentLapSchedulePM').dom.value === '' )
		{
			ShowPesanWarningSchedulePMReport(nmGetValidasiKosong(nmDeptDlgRpt),nmTitleFormDlgSchPMRpt);
			x=0;
		};
	};

	return x;
};

function ValidasiTanggalReportSchedulePM()
{
	var x=1;
		if(Ext.get('dtpTglAwalLapSchedulePM').dom.value > Ext.get('dtpTglAkhirLapSchedulePM').dom.value)
		{
			ShowPesanWarningSchedulePMReport(nmWarningDateDlgRpt,nmTitleFormDlgSchPMRpt);
			x=0;
		}
		
	return x;
};

function ShowPesanWarningSchedulePMReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:300
		}
	);
};


function getItemLapSchedulePM_Dept()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 85,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mComboDepartmentLapSchedulePM()
				]
			}
		]
	}
	return items;
};


function getItemLapSchedulePM_Tanggal() 
{
   var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 0.45,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmPeriodeDlgRpt + ' ',
						id: 'dtpTglAwalLapSchedulePM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    },
			{
		        columnWidth: 0.48,
		        layout: 'form',
		        border: false,
				labelWidth: 25,
		        labelAlign: 'right',
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmSd + ' ',
						id: 'dtpTglAkhirLapSchedulePM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapSchedulePM()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDepartmentSchedulePM = new WebApp.DataStore({ fields: Field });

	dsDepartmentSchedulePM.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
  var comboDepartmentLapSchedulePM = new Ext.form.ComboBox
	(
		{
			id:'comboDepartmentLapSchedulePM',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmDeptDlgRpt + ' ',			
			align:'Right',
			anchor:'99%',
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			store:dsDepartmentSchedulePM,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptLapSchedulePM=b.data.DEPT_ID ;
					selectNamaDeptLapSchedulePM=b.data.DEPT_NAME ;
				} 
			}
		}
	);

    dsDepartmentSchedulePM.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'DEPT_ID',
                                Sort: 'dept_id',
				Sortdir: 'ASC', 
				target:'ComboDepartment',
				param: ''
			} 
		}
	);
	return comboDepartmentLapSchedulePM;
};



