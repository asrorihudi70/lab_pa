﻿
var dsDepartmentWorkOrderCM;
var selectDeptLapWorkOrderCM;
var selectNamaDeptLapWorkOrderCM;
var now = new Date();

var frmDlgWorkOrderCM;
var varLapWorkOrderCM= ShowFormLapWorkOrderCM();

function ShowFormLapWorkOrderCM()
{
	frmDlgWorkOrderCM= fnDlgWorkOrderCM();
	frmDlgWorkOrderCM.show();
};

function fnDlgWorkOrderCM()
{  	
    var winWorkOrderCMReport = new Ext.Window
	(
		{ 
			id: 'winWorkOrderCMReport',
			title: nmTitleFormDlgWOCMRpt,
			closeAction: 'destroy',
			width:500,
			height: 143,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlgWorkOrderCM()]
			
		}
	);
	
    return winWorkOrderCMReport; 
};


function ItemDlgWorkOrderCM() 
{	
	var PnlLapWorkOrderCM = new Ext.Panel
	(
		{ 
			id: 'PnlLapWorkOrderCM',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemLapWorkOrderCM_Tanggal(),getItemLapWorkOrderCM_Dept(),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: nmBtnOK,
							width: 70,
							hideLabel: true,
							id: 'btnOkLapWorkOrderCM',
							handler: function() 
							{
								if (ValidasiReportWorkOrderCM() === 1)
								{
									//if (ValidasiTanggalReportWorkOrderCM() === 1)
									//{
										var criteria = GetCriteriaWorkOrderCM();
										frmDlgWorkOrderCM.close();
										ShowReport('', 920004, criteria);
									//};
								};
							}
						},
						{
							xtype: 'button',
							text: nmBtnCancel,
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapWorkOrderCM',
							handler: function() 
							{
								frmDlgWorkOrderCM.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapWorkOrderCM;
};

function GetCriteriaWorkOrderCM()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapWorkOrderCM').dom.value != '') 
	{
		strKriteria = Ext.get('dtpTglAwalLapWorkOrderCM').dom.value;
	};
	
	if (Ext.get('dtpTglAkhirLapWorkOrderCM').dom.value != '') 
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapWorkOrderCM').dom.value;
	};
	
	if (selectDeptLapWorkOrderCM != undefined) 
	{
		strKriteria += '##@@##' + selectDeptLapWorkOrderCM;
		strKriteria += '##@@##' + selectNamaDeptLapWorkOrderCM ;
	};
	
	return strKriteria;
};


function ValidasiReportWorkOrderCM()
{
	var x=1;
	
	if((Ext.get('dtpTglAwalLapWorkOrderCM').dom.value === '') || (Ext.get('dtpTglAkhirLapWorkOrderCM').dom.value === '') || (selectDeptLapWorkOrderCM === undefined) || (Ext.get('comboDepartmentLapWorkOrderCM').dom.value === ''))
	{
		if(Ext.get('dtpTglAwalLapWorkOrderCM').dom.value === '')
		{
			ShowPesanWarningWorkOrderCMReport(nmGetValidasiKosong(nmStartDateDlgRpt),nmTitleFormDlgWOCMRpt);
			x=0;
		}
		else if(Ext.get('dtpTglAkhirLapWorkOrderCM').dom.value === '')
		{
		    ShowPesanWarningWorkOrderCMReport(nmGetValidasiKosong(nmFinishDateDlgRpt), nmTitleFormDlgWOCMRpt);
			x=0;
		}
		else if(Ext.get('comboDepartmentLapWorkOrderCM').dom.value === '' )
		{
		    ShowPesanWarningWorkOrderCMReport(nmGetValidasiKosong(nmDeptDlgRpt),nmTitleFormDlgWOCMRpt);
			x=0;
		};
	};

	return x;
};

function ValidasiTanggalReportWorkOrderCM()
{
	var x=1;
		if(Ext.get('dtpTglAwalLapWorkOrderCM').dom.value > Ext.get('dtpTglAkhirLapWorkOrderCM').dom.value)
		{
		    ShowPesanWarningWorkOrderCMReport(nmWarningDateDlgRpt,nmTitleFormDlgWOCMRpt);
			x=0;
		}
		
	return x;
};

function ShowPesanWarningWorkOrderCMReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:300
		}
	);
};


function getItemLapWorkOrderCM_Dept()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 85,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mComboDepartmentLapWorkOrderCM()
				]
			}
		]
	}
	return items;
};


function getItemLapWorkOrderCM_Tanggal() 
{
   var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 0.45,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmPeriodeDlgRpt + ' ',
						id: 'dtpTglAwalLapWorkOrderCM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    },
			{
		        columnWidth: 0.48,
		        layout: 'form',
		        border: false,
				labelWidth: 25,
		        labelAlign: 'right',
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmSd + ' ',
						id: 'dtpTglAkhirLapWorkOrderCM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapWorkOrderCM()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDepartmentWorkOrderCM = new WebApp.DataStore({ fields: Field });

	dsDepartmentWorkOrderCM.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
  var comboDepartmentLapWorkOrderCM = new Ext.form.ComboBox
	(
		{
			id:'comboDepartmentLapWorkOrderCM',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmDeptDlgRpt + ' ',			
			align:'Right',
			anchor:'99%',
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			store:dsDepartmentWorkOrderCM,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptLapWorkOrderCM=b.data.DEPT_ID ;
					selectNamaDeptLapWorkOrderCM=b.data.DEPT_NAME ;
				} 
			}
		}
	);

    dsDepartmentWorkOrderCM.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'DEPT_ID',
                                Sort: 'dept_id',
				Sortdir: 'ASC', 
				target:'ComboDepartment',
				param: ''
			} 
		}
	);
	return comboDepartmentLapWorkOrderCM;
};



