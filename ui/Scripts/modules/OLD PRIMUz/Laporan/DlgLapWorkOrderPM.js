﻿
var dsDepartmentWorkOrderPM;
var selectDeptLapWorkOrderPM;
var selectNamaDeptLapWorkOrderPM;
var now = new Date();

var frmDlgWorkOrderPM;
var varLapWorkOrderPM= ShowFormLapWorkOrderPM();

function ShowFormLapWorkOrderPM()
{
	frmDlgWorkOrderPM= fnDlgWorkOrderPM();
	frmDlgWorkOrderPM.show();
};

function fnDlgWorkOrderPM()
{  	
    var winWorkOrderPMReport = new Ext.Window
	(
		{ 
			id: 'winWorkOrderPMReport',
			title: nmTitleFormDlgWOPMRpt,
			closeAction: 'destroy',
			width:500,
			height: 143,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'icon_lapor',
			modal: true,
			items: [ItemDlgWorkOrderPM()]
			
		}
	);
	
    return winWorkOrderPMReport; 
};


function ItemDlgWorkOrderPM() 
{	
	var PnlLapWorkOrderPM = new Ext.Panel
	(
		{ 
			id: 'PnlLapWorkOrderPM',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemLapWorkOrderPM_Tanggal(),getItemLapWorkOrderPM_Dept(),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: nmBtnOK,
							width: 70,
							hideLabel: true,
							id: 'btnOkLapWorkOrderPM',
							handler: function() 
							{
								if (ValidasiReportWorkOrderPM() === 1)
								{
									//if (ValidasiTanggalReportWorkOrderPM() === 1)
									//{
										var criteria = GetCriteriaWorkOrderPM();
										frmDlgWorkOrderPM.close();
										ShowReport('', 910003, criteria);
									//};
								};
							}
						},
						{
							xtype: 'button',
							text: nmBtnCancel,
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapWorkOrderPM',
							handler: function() 
							{
								frmDlgWorkOrderPM.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapWorkOrderPM;
};

function GetCriteriaWorkOrderPM()
{
	var strKriteria = '';

	if (Ext.get('dtpTglAwalLapWorkOrderPM').dom.value != '') 
	{
		strKriteria = Ext.get('dtpTglAwalLapWorkOrderPM').dom.value;
	};
	
	if (Ext.get('dtpTglAkhirLapWorkOrderPM').dom.value != '') 
	{
		strKriteria += '##@@##' + Ext.get('dtpTglAkhirLapWorkOrderPM').dom.value;
	};
	
	if (selectDeptLapWorkOrderPM != undefined) 
	{
		strKriteria += '##@@##' + selectDeptLapWorkOrderPM;
		strKriteria += '##@@##' + selectNamaDeptLapWorkOrderPM ;
	};
	
	return strKriteria;
};


function ValidasiReportWorkOrderPM()
{
	var x=1;
	
	if((Ext.get('dtpTglAwalLapWorkOrderPM').dom.value === '') || (Ext.get('dtpTglAkhirLapWorkOrderPM').dom.value === '') || (selectDeptLapWorkOrderPM === undefined) || (Ext.get('comboDepartmentLapWorkOrderPM').dom.value === ''))
	{
		if(Ext.get('dtpTglAwalLapWorkOrderPM').dom.value === '')
		{
		    ShowPesanWarningWorkOrderPMReport(nmGetValidasiKosong(nmStartDateDlgRpt), nmTitleFormDlgWOPMRpt);
			x=0;
		}
		else if(Ext.get('dtpTglAkhirLapWorkOrderPM').dom.value === '')
		{
		    ShowPesanWarningWorkOrderPMReport(nmGetValidasiKosong(nmFinishDateDlgRpt), nmTitleFormDlgWOPMRpt);
			x=0;
		}
		else if(Ext.get('comboDepartmentLapWorkOrderPM').dom.value === '' )
		{
		    ShowPesanWarningWorkOrderPMReport(nmGetValidasiKosong(nmDeptDlgRpt), nmTitleFormDlgWOPMRpt);
			x=0;
		};
	};

	return x;
};

function ValidasiTanggalReportWorkOrderPM()
{
	var x=1;
		if(Ext.get('dtpTglAwalLapWorkOrderPM').dom.value > Ext.get('dtpTglAkhirLapWorkOrderPM').dom.value)
		{
		    ShowPesanWarningWorkOrderPMReport(nmWarningDateDlgRpt, nmTitleFormDlgWOPMRpt);
			x=0;
		}
		
	return x;
};

function ShowPesanWarningWorkOrderPMReport(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:300
		}
	);
};


function getItemLapWorkOrderPM_Dept()
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 85,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					mComboDepartmentLapWorkOrderPM()
				]
			}
		]
	}
	return items;
};


function getItemLapWorkOrderPM_Tanggal() 
{
   var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		    {
		        columnWidth: 0.45,
		        layout: 'form',
		        border: false,
		        labelAlign: 'right',
		        labelWidth: 85,
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmPeriodeDlgRpt + ' ',
						id: 'dtpTglAwalLapWorkOrderPM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    },
			{
		        columnWidth: 0.48,
		        layout: 'form',
		        border: false,
				labelWidth: 25,
		        labelAlign: 'right',
		        items:
				[
					{
						xtype: 'datefield',
						fieldLabel: nmSd + ' ',
						id: 'dtpTglAkhirLapWorkOrderPM',
						format: 'd/M/Y',
						value:now,
						width:105
					}
				]
		    }
		]
	}
    return items;
};


function mComboDepartmentLapWorkOrderPM()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDepartmentWorkOrderPM = new WebApp.DataStore({ fields: Field });

	dsDepartmentWorkOrderPM.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    param: ''
			}
		}
	);
  var comboDepartmentLapWorkOrderPM = new Ext.form.ComboBox
	(
		{
			id:'comboDepartmentLapWorkOrderPM',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmDeptDlgRpt+ ' ',			
			align:'Right',
			anchor:'99%',
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			store:dsDepartmentWorkOrderPM,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptLapWorkOrderPM=b.data.DEPT_ID ;
					selectNamaDeptLapWorkOrderPM=b.data.DEPT_NAME ;
				} 
			}
		}
	);

    dsDepartmentWorkOrderPM.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'DEPT_ID',
                                Sort: 'dept_id',
				Sortdir: 'ASC', 
				target:'ComboDepartment',
				param: ''
			} 
		}
	);
	return comboDepartmentLapWorkOrderPM;
};



