var dsDashboardList;
var dsDashboardPerformSch;
var dsDashboardPerformRslt;
var selectCategoryDashboardView=' 9999';
var valueCategoryDashboardView=' All';
var nowDash = new Date();
var dsDashboardWOList;
var dsDashboardCostList;
var strCriteriaCatDashBoard='';


CurrentPage.page = getPanelDashboard(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

//Ext.chart.Chart.CHART_URL = './resources/open-flash-chart.swf';
Ext.chart.Chart.CHART_URL = WebAppUrl.UrlChartSWF ; //baseURL + 'ui/resources/charts.swf';
//Ext.chart.Chart.CHART_URL = 'lib/ext-3.0-rc3/resources/charts.swf';

function getPanelDashboard(mod_id) 
{
	//RefreshDataDashboardWO(true);
	
    var FormDashboard = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormDashboard,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    //anchor: '100%',
		    iconCls: 'SetupLocation',
			autoScroll:false,
		    items: [getPanelDashboardBaris1()],
			tbar:
			[
				nmPeriodDashboard + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmPeriodDashboard + ' : ',
				    id: 'dtpTglAwalDashboard',
				    format: 'd/M/Y',
				    value: nowDash,
				    width: 130,
				    onInit: function() { }
				}, ' ', ' ' + nmSd + ' ', ' ', 
				{
				    xtype: 'datefield',
				    fieldLabel: nmSd + ' ',
				    id: 'dtpTglAkhirDashboard',
				    format: 'd/M/Y',
				    value: nowDash,
				    width: 130
				},' ', '-',
				{
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    anchor: '25%',
				    handler: function(sm, row, rec) 
					{
						RefreshDataDashboardWO(false);
						RefreshDataDashboardCategory(false);
						refreshDataDashboardCostMaintCat(false);
						RefreshDataDashboardPerformSch(false);
						//RefreshDataDashboardPerformRslt(false);
				    }
				}
			]
		}
	);

	RefreshDataDashboardCategory(true);
	RefreshDataDashboardCategory(true);
	RefreshDataDashboardPerformSch(true);
	//RefreshDataDashboardPerformRslt(true);
	
    return FormDashboard ;
};

function getPanelDashboardBaris1() 
{
	
    var PanelDashboardBaris1 = new Ext.Panel
    (
		{
		    id: 'PanelDashboardBaris1',
		    region: 'center',
		    layout: 'form',
		    title: '',
		    itemCls: 'blacklabel',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
			autoScroll:false,
		    anchor: '100% 100%',
		    iconCls: '',
		    items: 
			[
				getPanelDashboardCostMaintCat(),getPanelDashboardPerformance()
			]
		}
	);

    return PanelDashboardBaris1 ;
};

function refreshDataDashboardCostMaintCat(mBol)
{
var cKriteria;
	if (mBol===true)
	{
		//cKriteria='where year(FINISH_DATE) = ' + nowDash.format('Y')
                cKriteria=' date_part(~year~, finish_date) = ' + nowDash.format('Y')
	}
	else
	{
		//cKriteria='where FINISH_DATE Between ~ ' + Ext.get('dtpTglAwalDashboard').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirDashboard').dom.value + '~'
                cKriteria='finish_date Between ~' + Ext.get('dtpTglAwalDashboard').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirDashboard').dom.value + '~'
	};
	
	 dsDashboardCostList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 2000,
			    //Sort: 'BULAN',
                            Sort: 'bulan',
			    Sortdir: 'ASC',
			    target: 'ViewDashboardCost',
			    param: cKriteria
			}
		}
	);
};

function getPanelDashboardCostMaintCat() 
{
	
	
	var fldDetail = ['BULAN','JML','JMLAWAL'];

	dsDashboardCostList = new WebApp.DataStore({ fields: fldDetail })
	
	refreshDataDashboardCostMaintCat(true);

	var PanelDashboardCostMaint= new Ext.Panel
    (
		{
		    id: 'PanelDashboardCostMaint',
		    region: 'center',
		    layout: 'form',
		    title: nmTitlePnlCostMaintDashboard,
		    itemCls: 'blacklabel',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    anchor: '100%',
		    iconCls: '',
			height:190,
		    items:
			
				{
					xtype: 'columnchart',
					id:'myChartCost',
					height:170,
					url:WebAppUrl.UrlChartSWF,
					store: dsDashboardCostList,
					xField: 'BULAN',
					extraStyle: 
					{
						 legend: 
						 {
							display: 'bottom',
							font:
							{
								family: 'Tahoma',
								size: 9
							}
						 }
					},
					series: 
					[
						{
							yField: 'JML',
							displayName: nmCostDashboard,
							style: 
							{
								color:'#AB63F6'
							}
						},
						{
							yField: 'JMLAWAL',
							displayName: nmCostRateDashboard
						}
					]
				}
			
			// {
				// xtype: 'columnchart',
				// store: dsDashboardCostList,
				// anchor: '100%',
				// url:'./resources/charts.swf',
				// xField: 'BULAN',
				// yAxis: new Ext.chart.NumericAxis
				// (
					// {
						// displayName: nmCostDashboard//,
						// //labelRenderer : Ext.util.Format.numberRenderer('0,0')
					// }
				// ),
				// tipRenderer : function(chart, record, index, series)
				// {
						// return ' ' + nmTooltipCostDashboard + ' ' + record.data.BULAN + ' = ' + record.data.JML;
				// },
				// extraStyle:
				// {
					// legend:
					// {
						// display: 'right',
						// padding: 5,
						// font:
						// {
							// family: 'Tahoma',
							// size: 8
						// }
					// }
				// },
				// chartStyle: 
				// {
					// padding: 10,
					// animationEnabled: true,
					// font: {
						// name: 'Tahoma',
						// color: 0x444444,
						// size: 11
					// },
					// dataTip: {
						// padding: 5,
						// border: {
							// color: 0x99bbe8,
							// size:1
						// },
						// background: {
							// color: 0xDAE7F6,
							// alpha: .9
						// },
						// font: {
							// name: 'Tahoma',
							// color: 0x15428B,
							// size: 10,
							// bold: true
						// }
					// },
					// xAxis: {
						// color: 0x69aBc8,
						// majorTicks: {color: 0x69aBc8, length: 4},
						// minorTicks: {color: 0x69aBc8, length: 2},
						// majorGridLines: {size: 1, color: 0xeeeeee}
					// },
					// yAxis: {
						// color: 0x69aBc8,
						// majorTicks: {color: 0x69aBc8, length: 4},
						// minorTicks: {color: 0x69aBc8, length: 2},
						// majorGridLines: {size: 1, color: 0xdfe8f6}
					// }
				// },
				// series: 
				// [
					// {
						// type: 'column',
						// displayName: nmCostDashboard,
						// yField: 'JML',
						// style: 
						// {
							// image:'bar.gif',
							// mode: 'stretch',
							// color:0x99BBE8
						// }
					// },
					// {
						// type:'line',
						// displayName: nmCostRateDashboard,
						// yField: 'JMLAWAL',
						// style: 
						// {
							// color: 0x15428B
						// }
					// }
				// ]
			// }
		}
	);
	
	
	var PanelDashboardCat= new Ext.Panel
    (
		{
		    id: 'PanelDashboardCat',
			name:'PanelDashboardCat',
		    region: 'center',
		    layout: 'form',
		    title: nmTitlePnlPerformDashboard,
		    itemCls: 'blacklabel',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
			height:190,
		    iconCls: '',
			items:
			[getDashboardPieChartPerformSch()
				// {
					// columnWidth: .5,
					// layout: 'form',
					// border: false,
					// height:190,
					// bodyStyle: 'padding:3px 3px 6px 6px',
					// items:
					// [	
						// // {
							// // xtype:'label',
							// // fieldLabel:'Schedule ',
							// // labelStyle:
							// // {
								// // 'font-weight': 'bold',
								// // 'color':'blue'
							// // }
						// // },
						// getDashboardPieChartPerformSch()
					// ]
				// },
				// {
					// columnWidth: .5,
					// layout: 'form',
					// bodyStyle: 'padding:3px 6px 6px 3px',
					// border: false,
					// height:190,
					// items:
					// [
						// {
							// xtype:'label',
							// fieldLabel:'Job Close ',
							// style:
							// {
								// 'font-weight': 'bold',
								// 'color':'blue'
							// }
						// },
						// getDashboardPieChartPerformRslt()
					// ]
				// }
			]
		}
    )
	
    var items =
	{
	    layout: 'column',
	    border: false,
		anchor: '100% 50%',
	    items:
		[
			{
			    columnWidth: .5,
			    layout: 'form',
			    border: false,
				bodyStyle: 'padding:6px 3px 3px 6px',
			    items:
				[
				    PanelDashboardCostMaint
				]
			},
			{
			    columnWidth: .5,
			    layout: 'form',
				bodyStyle: 'padding:6px 6px 3px 3px',
			    border: false,
			    items:
				[
				    PanelDashboardCat
				]
			}
		]

    }
	return items ;
};

function RefreshDataDashboardPerformSch(mBol) 
{
	var cKriteria;
	if (mBol===true)
	{
		//cKriteria='where year(Due_Date) = ' + nowDash.format('Y')
                cKriteria='date_part(~year~, due_date) = ' + nowDash.format('Y')
	}
	else
	{
		//cKriteria='where Due_Date Between ~ ' + Ext.get('dtpTglAwalDashboard').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirDashboard').dom.value + '~'
                cKriteria='due_date Between ~' + Ext.get('dtpTglAwalDashboard').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirDashboard').dom.value + '~'
	};
	
    dsDashboardPerformSch.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 2000,
			    //Sort: 'CATEGORY_NAME',
                            Sort: 'category_name',
			    Sortdir: 'ASC',
			    target: 'ViewDashboardPerform',
			    param: cKriteria
			}
		}
	);
    return dsDashboardPerformSch;
};

function RefreshDataDashboardPerformRslt(mBol) 
{
	var cKriteria;
	if (mBol===true)
	{
		//cKriteria='where year(Due_Date) = ' + nowDash.format('Y')
                cKriteria='date_part(~year~, due_date) = ' + nowDash.format('Y')
	}
	else
	{
		//cKriteria='where Due_Date Between ~ ' + Ext.get('dtpTglAwalDashboard').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirDashboard').dom.value + '~'
                cKriteria='due_date Between ~' + Ext.get('dtpTglAwalDashboard').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirDashboard').dom.value + '~'
	};
	
    dsDashboardPerformRslt.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 2000,
			    //Sort: 'CATEGORY_NAME',
                            Sort: 'category_name',
			    Sortdir: 'ASC',
			    target: 'ViewDashboardPerform',
			    param: cKriteria
			}
		}
	);
    return dsDashboardPerformRslt;
};

function getPanelDashboardPerformance() 
{
	var PanelDashboardPerformance1= new Ext.Panel
    (
		{
		    id: 'PanelDashboardPerformance1',
		    region: 'center',
		    layout: 'form',
		    title: nmTitlePnlMaintDashboard,
		    itemCls: 'blacklabel',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    //anchor: '100%',
		    iconCls: '',
			autoScroll:false,
			height:215,//195,
		    items:
			[
				getDashboardPieChart()
			]
		}
	);
	
	var PanelDashboardPerformance2= new Ext.Panel
    (
		{
		    id: 'PanelDashboardPerformance2',
		    region: 'center',
		    layout: 'form',
		    title: nmTitlePnlCatDashboard,
		    itemCls: 'blacklabel',
			autoScroll:false,
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
			bodyStyle: 'padding:7px 7px 7px 7px',
		    anchor: '100%',
			labelWidth:50,
			height:190,
		    iconCls: '',
		    items: [GetDTLDashboardCategory()]
		}
	);

    var items =
	{
	    layout: 'form',
		anchor: '100% 50%',
		bodyStyle: 'padding:3px 6px 6px 6px',
	    border: false,
		height:180,
	    items:
		[PanelDashboardPerformance2
			// {
			    // columnWidth: .4,
			    // layout: 'form',
			    // border: false,
				// height:230,
				// bodyStyle: 'padding:3px 3px 6px 6px',
			    // items:
				// [
				    // PanelDashboardPerformance1
				// ]
			// },
			// {
			    // columnWidth: .6,
			    // layout: 'form',
				// bodyStyle: 'padding:3px 6px 6px 3px',
			    // border: false,
				// height:230,
				// labelWidth:50,
			    // items:
				// [
				    // PanelDashboardPerformance2
				// ]
			// }
		]

    }
	return items ;
};


function getDashboardPieChart()
{
	// var x = swfobject.embedSWF
	// (
      // "open-flash-chart.swf", "myChart", "498", "230",
      // "9.0.0", "expressInstall.swf",
      // {"data-file":"./resources/open-flash-chart.php?load=chart"}
    // );
	
	var fldDetail = ['CATEGORY_NAME','CATEGORY_ID','JML'];

    dsDashboardWOList = new WebApp.DataStore({ fields: fldDetail })
	RefreshDataDashboardWO(true);
	var PanelDashboardPieChart= new Ext.Panel
	(
		{
			id: 'PanelDashboardPieChart',
			name:'PanelDashboardPieChart',
			//width: 320,
			height: 180,//320,
			align:'center',
			border:false,
			items:
			{
				store: dsDashboardWOList,
				anchor: '100%',
				xtype: 'piechart',
				id:'chartDashboardWO',
				name:'chartDashboardWO',
				url:WebAppUrl.UrlChartSWF,
				//url: './resources/open-flash-chart.swf',
				//dataURL : './resources/data.json?ofc=chart',
				dataField: 'JML',
				categoryField: 'CATEGORY_NAME',
				style: 
				{
					'margin-top': '10px',
					'margin-left':'15px'
					
				},
				chartStyle: 
				{
					dataTip: 
					{
						padding: 5,
						border: 
						{
							color: 0x99bbe8,
							size:1
						},
						background: 
						{
							color: 0xDAE7F6,
							alpha: .9
						}
					}
				},
				extraStyle:
				{
					legend:
					{
						display: 'right',
						padding: 7,
						font:
						{
							family: 'Tahoma',
							size: 9
						}
					}
				},
				series:
				[
					{
						style:
						{
							colors:['#0000BB','#0000FF','FFAA3C','#00BB00','#00FF00','#BB0000', '#FF0000' ]
						}
					}
				]
			}
		}
	);
	
	return PanelDashboardPieChart;
};

function getDashboardPieChartPerformSch()
{
	var fldDetail = ['GROUPS','JUMLAH'];

    dsDashboardPerformSch = new WebApp.DataStore({ fields: fldDetail })
	RefreshDataDashboardPerformSch(true);
	var PanelDashboardPieChartPerformSch= new Ext.Panel
	(
		{
			id: 'PanelDashboardPieChartPerformSch',
			name:'PanelDashboardPieChartPerformSch',
			height: 150,
			align:'center',
			border:false,
			items:
			{
				store: dsDashboardPerformSch,
				anchor: '100%',
				xtype: 'piechart',
				id:'chartDashboardPerformSch',
				name:'chartDashboardPerformSch',
				url:WebAppUrl.UrlChartSWF,
				dataField: 'JUMLAH',
				categoryField: 'GROUPS',
				style: 
				{
					'margin-top': '0px',
					'margin-left':'5px'
					
				},
				extraStyle:
				{
					legend:
					{
						display: 'right',
						padding: 7,
						font:
						{
							family: 'Tahoma',
							size: 9
						}
					}
				},
				series:
				[
					{
						style:
						{
							colors:['#00FF00','#FFFF00', '#FF0000','#FFAA3C','#0000FF']//'#FF0000'
							//#00FF00=hijau  FF0000=merah  AB63F6=ungu  0000FF=biru   FFAA3C=orange  58EEFC=cyan  FFFF00=kuning 
							//00BB00=hijau tua
							//colors:['#0000BB','#0000FF','FFAA3C','#00BB00','#00FF00','#BB0000', '#FF0000' ]
						}
					}
				]
				
			}
		}
	);
    RefreshDataDashboardPerformSch(true);
	return PanelDashboardPieChartPerformSch;
};


function getDashboardPieChartPerformRslt()
{
	var fldDetail = ['CATEGORY_NAME','CATEGORY_ID','SCHEDULE','RESULT'];

    dsDashboardPerformRslt = new WebApp.DataStore({ fields: fldDetail })
	RefreshDataDashboardPerformRslt(true);
	var PanelDashboardPieChartPerformRslt= new Ext.Panel
	(
		{
			id: 'PanelDashboardPieChartPerformRslt',
			name:'PanelDashboardPieChartPerformRslt',
			height: 150,
			align:'center',
			border:false,
			items:
			{
				store: dsDashboardPerformRslt,
				anchor: '100%',
				xtype: 'piechart',
				id:'chartDashboardPerformRslt',
				name:'chartDashboardPerformRslt',
				url:WebAppUrl.UrlChartSWF,
				dataField: 'RESULT',
				categoryField: 'CATEGORY_NAME',
				style: 
				{
					'margin-top': '-10px',
					'margin-left':'5px'
					
				},
				extraStyle:
				{
					legend:
					{
						display: 'bottom',
						padding: 7,
						font:
						{
							family: 'Tahoma',
							size: 9
						}
					}
				},
				series:
				[
					{
						style:
						{
							colors:['#00FF00', '#FF0000' , '#0000FF','#0000BB','FFAA3C','#00BB00','#BB0000']
						}
					}
				]
			}
		}
	);
	RefreshDataDashboardPerformRslt(true);
	return PanelDashboardPieChartPerformRslt;
};

function GetDTLDashboardCategory() 
{
    var fldDetail = ['CATEGORY_ID','CATEGORY_NAME','REQUEST','ONSCHEDULE','OFFSCHEDULE','WO','CLOSE'];

    dsDashboardList = new WebApp.DataStore({ fields: fldDetail })

    var gridDashboardCategory = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDashboardList,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:400,
			autoScroll:true,
		    height:150,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					    }
					}
				}
			),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'colCatAssetDashboard', 
					    header: nmColCatDashboard,
					    dataIndex: 'CATEGORY_NAME',
					    sortable: true,
					    width: 75
					},
					{
					    id: 'colRequestDashboard', 
					    header: nmColReqDashboard,
					    dataIndex: '',
						align:'center',
					    sortable: true,
					    width: 30,
						renderer:renderValueRequestDashBoard
					},
					{
					    id: 'colOnScheduleDashboard', 
					    header: nmColSchDashboard,
					    dataIndex: '',
						align:'center',
					    sortable: true,
					    width: 30,
						renderer:renderValueOnScheduleDashBoard
					},
					{
					    id: 'colOFFScheduleDashboard', 
					    header: nmColOffSchDashboard,
					    dataIndex: '',
						align:'center',
					    sortable: true,
					    width: 30,
						renderer:renderValueOffScheduleDashBoard
					},
					{
					    id: 'colWODashboard', 
					    header: nmColWODashboard,
					    dataIndex: '',
						align:'center',
					    sortable: true,
					    width: 30,
						renderer:renderValueWODashBoard
					},
					{
					    id: 'colCloseDashboard', 
					    header: nmColCloseDashboard,
					    dataIndex: '',
						align:'center',
					    sortable: true,
					    width: 30,
						renderer:renderValueResultDashBoard
					}
				]
			), viewConfig:{forceFit: true}
		}
	);

		return gridDashboardCategory;
};

function renderValueRequestDashBoard(value, p, record)  //Request
{
	var str='';
	
		if (strCriteriaCatDashBoard === '')
		{
			//str = ' where category_id =~' + record.data.CATEGORY_ID + '~';
                        str = ' category_id = ~' + record.data.CATEGORY_ID + '~';
		}
		else
		{
			str += strCriteriaCatDashBoard + ' and category_id = ~' + record.data.CATEGORY_ID + '~';
		};
		
		
		
        return String.format(
			'<a href="#" onclick="LoadModuleDashboard(400011,\'' + str + '\')">{1}</a>',
			value, record.data.REQUEST, record.data.CATEGORY_ID, record.data.REQUEST);
};

function renderValueOnScheduleDashBoard(value, p, record)  // on Schedule
{
	var str=''
	if (strCriteriaCatDashBoard === '')
	{
		//str = ' where category_id =~' + record.data.CATEGORY_ID + '~ and status <> 3';
                str = ' category_id = ~' + record.data.CATEGORY_ID + '~ and status <> 3';
	}
	else
	{
		str += strCriteriaCatDashBoard + ' and category_id = ~' + record.data.CATEGORY_ID + '~ and status <> 3';
	};
	
        return String.format(
               '<a href="#" onclick="LoadModuleDashboard(400012,\'' + str + '\')">{1}</a>',
                value, record.data.OnSCHEDULE, record.data.CATEGORY_NAME, record.data.OnSCHEDULE);
};

function renderValueOffScheduleDashBoard(value, p, record)  // on Schedule
{
		var str=''
		if (strCriteriaCatDashBoard === '')
		{
			//str = ' where category_id =~' + record.data.CATEGORY_ID + '~ and status = 3';
                        str = ' category_id = ~' + record.data.CATEGORY_ID + '~ and status = 3';
		}
		else
		{
			str += strCriteriaCatDashBoard + ' and category_id = ~' + record.data.CATEGORY_ID + '~ and status = 3';
		};
		
        return String.format(
               '<a href="#" onclick="LoadModuleDashboard(400012,\'' + str + '\')">{1}</a>',
                value, record.data.OffSCHEDULE, record.data.CATEGORY_NAME, record.data.OffSCHEDULE);
};

function renderValueWODashBoard(value, p, record)   //Work Order
{
		var str='';
		if (strCriteriaCatDashBoard === '')
		{
			//str = ' where category_id =~' + record.data.CATEGORY_ID + '~';
                        str = ' category_id = ~' + record.data.CATEGORY_ID + '~';
		}
		else
		{
			str += strCriteriaCatDashBoard + ' and category_id = ~' + record.data.CATEGORY_ID + '~';
		};
		
        return String.format(
			'<a href="#" onclick="LoadModuleDashboard(400013,\'' + str + '\')">{1}</a>',
			value, record.data.WO, record.data.CATEGORY_NAME, record.data.WO);
};

function renderValueResultDashBoard(value, p, record)  //Close
{
		var str='';
	
		if (strCriteriaCatDashBoard === '')
		{
			//str = ' where category_id =~' + record.data.CATEGORY_ID + '~';
                        str = ' category_id = ~' + record.data.CATEGORY_ID + '~';
		}
		else
		{
			str += strCriteriaCatDashBoard + ' and category_id = ~' + record.data.CATEGORY_ID + '~';
		};
		
        return String.format(

			'<a href="#" onclick="LoadModuleDashboard(400014,\'' + str + '\')">{1}</a>',
			value, record.data.CLOSE, record.data.CATEGORY_NAME, record.data.CLOSE);
				
};






function RefreshDataDashboardCategory(mBol) 
{
	var cKriteria;
	if (mBol===true)
	{
		//cKriteria='where year(Due_Date) = ' + nowDash.format('Y')
                cKriteria='date_part(~year~, due_date) = ' + nowDash.format('Y')
	}
	else
	{
		//cKriteria='where Due_Date Between ~ ' + Ext.get('dtpTglAwalDashboard').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirDashboard').dom.value + '~'
                cKriteria='due_date Between ~' + Ext.get('dtpTglAwalDashboard').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirDashboard').dom.value + '~'
	};
	
	strCriteriaCatDashBoard=cKriteria;
	
    dsDashboardList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 2000,
			    //Sort: 'CATEGORY_NAME',
                            Sort: 'category_name',
			    Sortdir: 'ASC',
			    target: 'ViewDashboardCategory',
			    param: cKriteria
			}
		}
	);
    return dsDashboardList;
};

function RefreshDataDashboardWO(mBol) 
{
	var cKriteria;
	if (mBol===true)
	{
		//cKriteria='where year(Due_Date) = ' + nowDash.format('Y')
                cKriteria='date_part(~year~, due_date) = ' + nowDash.format('Y')
	}
	else
	{
		//cKriteria='where Due_Date Between ~ ' + Ext.get('dtpTglAwalDashboard').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirDashboard').dom.value + '~'
                cKriteria='due_date Between ~' + Ext.get('dtpTglAwalDashboard').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirDashboard').dom.value + '~'
	};
	
	
    dsDashboardWOList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 2000,
			    //Sort: 'CATEGORY_NAME',
                            Sort: 'category_name',
			    Sortdir: 'ASC',
			    target: 'ViewDashboardWO',
			    param: cKriteria
			}
		}
	);
    return dsDashboardWOList;
};

function mComboCategoryDashboardView() 
{
	var Field = ['CATEGORY_ID','CATEGORY_NAME'];
	
    var dsCategoryDashboardView = new WebApp.DataStore({ fields: Field });
    dsCategoryDashboardView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'CATEGORY_ID',
                            Sort: 'category_id',
			    Sortdir: 'ASC',
			    target: 'ViewComboCategoryVw',
			    param: ''
			}
		}
	);
	
    var cboCategoryDashboardView = new Ext.form.ComboBox
	(
		{
		    id: 'cboCategoryDashboardView',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
			labelWidth:50,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmTitlePnlCatDashboard + ' ',
		    align: 'Right',
		    //anchor:'60%',
			width:130,
		    store: dsCategoryDashboardView,
		    valueField: 'CATEGORY_ID',
		    displayField: 'CATEGORY_NAME',
			value:valueCategoryDashboardView,
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectCategoryDashboardView = b.data.CATEGORY_ID;
			    }
			}
		}
	);
	
    return cboCategoryDashboardView;
};













