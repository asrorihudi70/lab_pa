var dsSetEarlyWarningList;
var AddNewSetEarlyWarning;
var selectCountSetEarlyWarning=50;
var rowSelectedSetEarlyWarning;
var nowEarlyWarning = new Date();
var mTempCriteria;

CurrentPage.page = getPanelSetEarlyWarning(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetEarlyWarning(mod_id) 
{
	
    var Field =['CATEGORY_ID','CATEGORY_NAME','BEFORE','TARGET','AFTER'];
    dsSetEarlyWarningList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetEarlyWarning(true);

    var grListSetEarlyWarning = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetEarlyWarning',
		    stripeRows: true,
		    store: dsSetEarlyWarningList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.CellSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						cellselect: function(sm, row, rec) 
					    {
					    }
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),                   
					{
					    id: 'colCatEarlyWar',
					    header: nmColCatEarly,					   
					    dataIndex: 'CATEGORY_NAME',
					    width: 300,
					    sortable: true
					},
					{
					    id: 'colBefore',
					    header: nmColBeforeEarly,
						align:'center',
					    dataIndex: 'BEFORE',
					    width: 150,
					    sortable: true,
						renderer:renderValueBefore
					},
					{
					    id: 'colTarget',
					    header: nmColTargetEarly,
						align:'center',
					    dataIndex: 'TARGET',
					    width: 150,
					    sortable: true,
						renderer:renderValueTarget
					},
					{
					    id: 'colAfter',
					    header: nmColAfterEarly,
						align:'center',
					    dataIndex: 'AFTER',
					    width: 150,
					    sortable: true,
						renderer:renderValueAfter
					}
                ]
			)
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetEarlyWarning = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormEarly,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupEarlyWarning',
		    items: [grListSetEarlyWarning],
		    tbar:
			[
				nmPeriodDashboard + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmPeriodDashboard + ' : ',
				    id: 'dtpTglAwalEarlyWarning',
				    format: 'd/M/Y',
				    value: nowEarlyWarning,
				    width: 130,
				    onInit: function() { }
				}, ' ', ' ' + nmSd + ' ', ' ', 
				{
				    xtype: 'datefield',
				    fieldLabel: nmSd + ' ',
				    id: 'dtpTglAkhirEarlyWarning',
				    format: 'd/M/Y',
				    value: nowEarlyWarning,
				    width: 130
				},' ', '-',				
				nmMaksData + ' : ', ' ',mComboMaksDataSetEarlyWarning(),
				' ','-',
				{
				    id: 'btnRefreshSetEarlyWarning',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetEarlyWarningFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetEarlyWarning();
				}
			}
		}
	);
    //END var FormSetEarlyWarning--------------------------------------------------

	RefreshDataSetEarlyWarning(true);
    return FormSetEarlyWarning ;
};

function renderValueBefore(value, p, record)  // Before
{
		var tmpCriteria=mTempCriteria;
		//tmpCriteria += ' AND CATEGORY_ID=~' + record.data.CATEGORY_ID + '~' ;
                tmpCriteria += ' AND catgory_id = ~' + record.data.CATEGORY_ID + '~' ;
        return String.format(
               '<a href="#" onclick="LoadModuleDashboard(400008,\'' + tmpCriteria + '\')">{1}</a>',
                value, record.data.BEFORE, record.data.CATEGORY_ID, record.data.BEFORE);
};

function renderValueTarget(value, p, record)  // Target
{
		var tmpCriteria=mTempCriteria;
		//tmpCriteria += ' AND CATEGORY_ID=~' + record.data.CATEGORY_ID + '~' ;
                tmpCriteria += ' AND category_id = ~' + record.data.CATEGORY_ID + '~' ;
        return String.format(
               '<a href="#" onclick="LoadModuleDashboard(400009,\'' + tmpCriteria + '\')">{1}</a>',
                value, record.data.TARGET, record.data.CATEGORY_ID, record.data.TARGET);
};

function renderValueAfter(value, p, record)  // After
{
		var tmpCriteria=mTempCriteria;
		//tmpCriteria += ' AND CATEGORY_ID=~' + record.data.CATEGORY_ID + '~' ;
                tmpCriteria += ' AND category_id = ~' + record.data.CATEGORY_ID + '~' ;
		return String.format(
			   '<a href="#" onclick="LoadModuleDashboard(400010,\'' + tmpCriteria + '\')">{1}</a>',
				value, record.data.AFTER, record.data.CATEGORY_ID, record.data.AFTER);

        
//        return String.format(
//               '<a href="#" onclick="LoadModuleDashboard(400010,' + record.data.CATEGORY_ID + ')">{1}</a>',
//                value, record.data.AFTER, record.data.CATEGORY_ID, record.data.AFTER);
};


function RefreshDataSetEarlyWarning(mBol)
{	
	var cKriteria;
	if (mBol===true)
	{
		//cKriteria='where year(Due_Date) = ' + nowEarlyWarning.format('Y')
                cKriteria=' date_part(~year~, due_date) = ' + nowEarlyWarning.format('Y')
	}
	else
	{
		//cKriteria='where Due_Date Between ~ ' + Ext.get('dtpTglAwalEarlyWarning').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirEarlyWarning').dom.value + '~'
                cKriteria='due_date Between ~' + Ext.get('dtpTglAwalEarlyWarning').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirEarlyWarning').dom.value + '~'
	};
	
	mTempCriteria=cKriteria;
	
	dsSetEarlyWarningList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetEarlyWarning, 
					//Sort: 'Due_Date',
                                        Sort: 'due_date',
					Sortdir: 'ASC', 
					target:'viEarlyWarning',
					param : cKriteria
				}			
			}
		);       
	
	rowSelectedSetEarlyWarning = undefined;
	return dsSetEarlyWarningList;
};

function RefreshDataSetEarlyWarningFilter() 
{   
	RefreshDataSetEarlyWarning(false);
};



function mComboMaksDataSetEarlyWarning()
{
  var cboMaksDataSetEarlyWarning = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetEarlyWarning',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetEarlyWarning,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetEarlyWarning=b.data.displayText ;
					RefreshDataSetEarlyWarning(false);
				} 
			}
		}
	);
	return cboMaksDataSetEarlyWarning;
};
 



