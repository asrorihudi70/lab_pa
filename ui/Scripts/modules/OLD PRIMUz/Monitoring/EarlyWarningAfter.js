﻿
var dsSetEarlyWarningAfterList;
var AddNewSetEarlyWarningAfter;
var selectCountSetEarlyWarningAfter=50;
var rowSelectedSetEarlyWarningAfter;

CurrentPage.page = getPanelSetEarlyWarningAfter(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetEarlyWarningAfter(mod_id) 
{
	
    var Field =['Status','PATH','ID','CATEGORY_ID','CATEGORY_NAME','ASSET_MAINT_ID','ASSET_MAINT_NAME','STATUS_ID','DUE_DATE','SERVICE_PM_NAME','LOCATION','DEPT_NAME','LOCATION_ID','DEPT_ID'];
    dsSetEarlyWarningAfterList = new WebApp.DataStore({ fields: Field });
	//alert(CurrentPage.criteria);
	RefreshDataSetEarlyWarningAfter(true);

    var grListSetEarlyWarningAfter = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetEarlyWarningAfter',
		    stripeRows: true,
		    store: dsSetEarlyWarningAfterList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetEarlyWarningAfter=undefined;
							rowSelectedSetEarlyWarningAfter = dsSetEarlyWarningAfterList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
					    id: 'colStatusImageEWBefore',
					    header: '',
					    //dataIndex: 'Status',
						dataIndex: 'PATH',
					    sortable: true,
					    width: 50,
						align:'center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store) 
						{						   
							return '<img src="' + value + '" class="text-desc-legend"/>'							 							
						}
					},
					{
					    id: 'colAssetName',
					    header: nmColAssetEarly,					   
					    dataIndex: 'ASSET_MAINT_NAME',
					    width: 300,
					    sortable: true
					},
					{
					    id: 'colServiceName',
					    header: nmColServiceEarly,					   
					    dataIndex: 'SERVICE_PM_NAME',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colLocationName',
					    header: nmColLocEarly,					   
					    dataIndex: 'LOCATION',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDeptName',
					    header: nmColDeptEarly,					   
					    dataIndex: 'DEPT_NAME',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDueDate',
					    header: nmColDueDateEarly,					   
					    dataIndex: 'DUE_DATE',
					    width: 100,
					    sortable: true,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.DUE_DATE);
					    }
					}
                ]
			)
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetEarlyWarningAfter = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormAfterEarly,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupEarlyWarningAfter',
		    items: [grListSetEarlyWarningAfter],
		    tbar:
			[
				// nmKdLokasi2 + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'Id : ',
					// id: 'txtKDSetEarlyWarningAfterFilter',                   
					// width:80,
					// onInit: function() { }
				// }, ' ','-',
				// nmLokasi + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'EarlyWarningAfter : ',
					// id: 'txtSetEarlyWarningAfterFilter',                   
					// anchor: '95%',
					// onInit: function() { }
				// }, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetEarlyWarningAfter(),
				' ','-',
				{
				    id: 'btnRefreshSetEarlyWarningAfter',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetEarlyWarningAfterFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetEarlyWarningAfter();
				}
			}
		}
	);
    //END var FormSetEarlyWarningAfter--------------------------------------------------

	RefreshDataSetEarlyWarningAfter(true);
    return FormSetEarlyWarningAfter ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function RefreshDataSetEarlyWarningAfter(mBol)
{	
	//var cKriteria = CurrentPage.criteria + ' AND Status=~3~' ;
        var cKriteria = CurrentPage.criteria + ' AND status = ~3~' ;
	
	dsSetEarlyWarningAfterList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetEarlyWarningAfter, 
					//Sort: 'Due_Date',
                                        Sort: 'due_date',
					Sortdir: 'ASC', 
					target:'viMonitoringEarlyWarningDetail',
					param : cKriteria
				}			
			}
		);       
	
	rowSelectedSetEarlyWarningAfter = undefined;
	return dsSetEarlyWarningAfterList;
};

function RefreshDataSetEarlyWarningAfterFilter() 
{   
	RefreshDataSetEarlyWarningAfter(false);
};



function mComboMaksDataSetEarlyWarningAfter()
{
  var cboMaksDataSetEarlyWarningAfter = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetEarlyWarningAfter',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetEarlyWarningAfter,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetEarlyWarningAfter=b.data.displayText ;
					RefreshDataSetEarlyWarningAfter(false);
				} 
			}
		}
	);
	return cboMaksDataSetEarlyWarningAfter;
};
 



