﻿
var dsSetEarlyWarningTargetList;
var AddNewSetEarlyWarningTarget;
var selectCountSetEarlyWarningTarget=50;
var rowSelectedSetEarlyWarningTarget;

CurrentPage.page = getPanelSetEarlyWarningTarget(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetEarlyWarningTarget(mod_id) 
{
	
    var Field =['Status','PATH','ID','CATEGORY_ID','CATEGORY_NAME','ASSET_MAINT_ID','ASSET_MAINT_NAME','STATUS_ID','DUE_DATE','SERVICE_PM_NAME','LOCATION','DEPT_NAME','LOCATION_ID','DEPT_ID'];
    dsSetEarlyWarningTargetList = new WebApp.DataStore({ fields: Field });
	//alert(CurrentPage.criteria);
	RefreshDataSetEarlyWarningTarget();

    var grListSetEarlyWarningTarget = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetEarlyWarningTarget',
		    stripeRows: true,
		    store: dsSetEarlyWarningTargetList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetEarlyWarningTarget=undefined;
							rowSelectedSetEarlyWarningTarget = dsSetEarlyWarningTargetList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
					    id: 'colStatusImageEWBefore',
					    header: '',
					    //dataIndex: 'Status',
						dataIndex: 'PATH',
					    sortable: true,
					    width: 50,
						align:'center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store) 
						{						   
							return '<img src="' + value + '" class="text-desc-legend"/>'							 							
						}
					},
					{
					    id: 'colAssetName',
					    header: nmColAssetEarly,					   
					    dataIndex: 'ASSET_MAINT_NAME',
					    width: 300,
					    sortable: true
					},
					{
					    id: 'colServiceName',
					    header: nmColServiceEarly,					   
					    dataIndex: 'SERVICE_PM_NAME',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colLocationName',
					    header: nmColLocEarly,					   
					    dataIndex: 'LOCATION',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDeptName',
					    header: nmColDeptEarly,					   
					    dataIndex: 'DEPT_NAME',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDueDate',
					    header: nmColDueDateEarly,					   
					    dataIndex: 'DUE_DATE',
					    width: 100,
					    sortable: true,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.DUE_DATE);
					    }
					}
                ]
			)
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetEarlyWarningTarget = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormTargetEarly,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupEarlyWarningTarget',
		    items: [grListSetEarlyWarningTarget],
		    tbar:
			[
				// nmKdLokasi2 + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'Id : ',
					// id: 'txtKDSetEarlyWarningTargetFilter',                   
					// width:80,
					// onInit: function() { }
				// }, ' ','-',
				// nmLokasi + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'EarlyWarningTarget : ',
					// id: 'txtSetEarlyWarningTargetFilter',                   
					// anchor: '95%',
					// onInit: function() { }
				// }, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetEarlyWarningTarget(),
				' ','-',
				{
				    id: 'btnRefreshSetEarlyWarningTarget',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetEarlyWarningTargetFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetEarlyWarningTarget();
				}
			}
		}
	);
    //END var FormSetEarlyWarningTarget--------------------------------------------------

	RefreshDataSetEarlyWarningTarget();
    return FormSetEarlyWarningTarget ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function RefreshDataSetEarlyWarningTarget()
{	
	//var cKriteria = CurrentPage.criteria + ' AND Status=~2~' ;
        var cKriteria = CurrentPage.criteria + ' AND status = ~2~' ;	
	
	dsSetEarlyWarningTargetList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetEarlyWarningTarget, 
					//Sort: 'Due_Date',
                                        Sort: 'due_date',
					Sortdir: 'ASC', 
					target:'viMonitoringEarlyWarningDetail',
					param : cKriteria
				}			
			}
		);       
	
	rowSelectedSetEarlyWarningTarget = undefined;
	return dsSetEarlyWarningTargetList;
};

function RefreshDataSetEarlyWarningTargetFilter() 
{   
	RefreshDataSetEarlyWarningTarget();
};



function mComboMaksDataSetEarlyWarningTarget()
{
  var cboMaksDataSetEarlyWarningTarget = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetEarlyWarningTarget',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetEarlyWarningTarget,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetEarlyWarningTarget=b.data.displayText ;
					RefreshDataSetEarlyWarningTarget();
				} 
			}
		}
	);
	return cboMaksDataSetEarlyWarningTarget;
};
 



