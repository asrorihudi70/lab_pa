var dsSetMaintenanceList;
var AddNewSetMaintenance;
var selectCountSetMaintenance=50;
var rowSelectedSetMaintenance;
var selectFilterMaintenance=1;
var mTempCriteriaMaintenance;
var nowMaintenance = new Date();

CurrentPage.page = getPanelSetMaintenance(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetMaintenance(mod_id) 
{
	
    var Field =['CATEGORY_ID','CATEGORY_NAME','REQUEST','SCHEDULE','WO','RESULT'];
    dsSetMaintenanceList = new WebApp.DataStore({ fields: Field });

    var grListSetMaintenance = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetMaintenance',
		    stripeRows: true,
		    store: dsSetMaintenanceList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.CellSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						cellselect: function(sm, row, rec) 
					    {
					    }
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),                   
					{
					    id: 'colCatEarlyWar',
					    header: nmColCatMaintMon,					   
					    dataIndex: 'CATEGORY_NAME',
					    width: 300,
					    sortable: true
					},
					{
					    id: 'colRequest',
					    header: nmColReqMaintMon,					   
					    dataIndex: 'REQUEST',
						align:'center',
					    width: 150,
					    sortable: true,
						renderer:renderValueRequest
					},
					{
					    id: 'colSchedule',
					    header: nmColSchMaintMon,					   
					    dataIndex: 'SCHEDULE',
						align:'center',
					    width: 150,
					    sortable: true,
						renderer:renderValueSchedule
					},
					{
					    id: 'colWO',
					    header: nmColWOMaintMon,					   
					    dataIndex: 'WO',
						align:'center',
					    width: 150,
					    sortable: true,
						renderer:renderValueWO
					},
					{
					    id: 'colResult',
					    header: nmColResultMaintMon,					   
					    dataIndex: 'RESULT',
						align:'center',
					    width: 150,
					    sortable: true,
						renderer:renderValueResult
					}
                ]
			)
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetMaintenance = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormMaintMon,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupMaintenance',
		    items: [grListSetMaintenance],
		    tbar:
			[
				//nmFilterMaintMon + ' : ', ' ',mComboFilterMaintenance(),' ','-',				
				nmPeriodDashboard + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmPeriodDashboard + ' : ',
				    id: 'dtpTglMaintenance',
				    format: 'd/M/Y',
				    value: nowMaintenance,
				    width: 130,
				    onInit: function() { }
				}, ' ', ' ' + nmSd + ' ', ' ', 
				{
				    xtype: 'datefield',
				    fieldLabel: nmSd + ' ',
				    id: 'dtpTglAkhirMaintenance',
				    format: 'd/M/Y',
				    value: nowMaintenance,
				    width: 130
				},' ', '-',			
				nmMaksData + ' : ', ' ',mComboMaksDataSetMaintenance(),
				' ','-',
				{
				    id: 'btnRefreshSetMaintenance',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetMaintenanceFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetMaintenance();
				}
			}
		}
	);
    //END var FormSetMaintenance--------------------------------------------------

	RefreshDataSetMaintenance(true);
    return FormSetMaintenance ;
};

function renderValueRequest(value, p, record)  // Request
{
		var TmpCriteriaM=mTempCriteriaMaintenance
		//TmpCriteriaM += ' AND CATEGORY_ID=~' + record.data.CATEGORY_ID + '~' ;
                TmpCriteriaM += ' AND category_id = ~' + record.data.CATEGORY_ID + '~' ;
        return String.format(
               '<a href="#" onclick="LoadModuleDashboard(400011,\'' + TmpCriteriaM + '\')">{1}</a>',
                value, record.data.REQUEST, record.data.CATEGORY_ID, record.data.REQUEST);
};

function renderValueSchedule(value, p, record)  // Schedule
{
		var TmpCriteriaM=mTempCriteriaMaintenance
		//TmpCriteriaM += ' AND CATEGORY_ID=~' + record.data.CATEGORY_ID + '~' ;
                TmpCriteriaM += ' AND category_id = ~' + record.data.CATEGORY_ID + '~' ;
        return String.format(
               '<a href="#" onclick="LoadModuleDashboard(400012,\'' + TmpCriteriaM + '\')">{1}</a>',
                value, record.data.SCHEDULE, record.data.CATEGORY_ID, record.data.SCHEDULE);
};

function renderValueWO(value, p, record)  // WO
{
		var TmpCriteriaM=mTempCriteriaMaintenance
		//TmpCriteriaM += ' AND CATEGORY_ID=~' + record.data.CATEGORY_ID + '~' ;
                TmpCriteriaM += ' AND category_id = ~' + record.data.CATEGORY_ID + '~' ;
        return String.format(
               '<a href="#" onclick="LoadModuleDashboard(400013,\'' + TmpCriteriaM + '\')">{1}</a>',
                value, record.data.WO, record.data.CATEGORY_ID, record.data.WO);
};

function renderValueResult(value, p, record)  // Result
{
		var TmpCriteriaM=mTempCriteriaMaintenance
		//TmpCriteriaM += ' AND CATEGORY_ID=~' + record.data.CATEGORY_ID + '~' ;
                TmpCriteriaM += ' AND category_id = ~' + record.data.CATEGORY_ID + '~' ;
        return String.format(
               '<a href="#" onclick="LoadModuleDashboard(400014,\'' + TmpCriteriaM + '\')">{1}</a>',
                value, record.data.RESULT, record.data.CATEGORY_ID, record.data.RESULT);
};


function RefreshDataSetMaintenance(mBol)
{	
	var cKriteria;
	if (mBol===true)
	{
		//cKriteria='where year(Due_Date) = ' + nowMaintenance.format('Y')
                cKriteria=' date_part(~year~, due_date) = ' + nowMaintenance.format('Y')
	}
	else
	{
		//cKriteria='where Due_Date Between ~ ' + Ext.get('dtpTglMaintenance').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirMaintenance').dom.value + '~'
                cKriteria='due_date Between ~' + Ext.get('dtpTglMaintenance').dom.value + '~ AND ~' + Ext.get('dtpTglAkhirMaintenance').dom.value + '~'
	};
	
	mTempCriteriaMaintenance=cKriteria;
	
	dsSetMaintenanceList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetMaintenance, 
					//Sort: 'Due_Date',
                                        Sort: 'due_date',
					Sortdir: 'ASC', 
					target:'viMaintenance',
					param : cKriteria
				}			
			}
		);       
	
	rowSelectedSetMaintenance = undefined;
	return dsSetMaintenanceList;
};

function RefreshDataSetMaintenanceFilter() 
{   
	RefreshDataSetMaintenance(false);
};



function mComboMaksDataSetMaintenance()
{
  var cboMaksDataSetMaintenance = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetMaintenance',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData +' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetMaintenance,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetMaintenance=b.data.displayText ;
					RefreshDataSetMaintenance(false);
				} 
			}
		}
	);
	return cboMaksDataSetMaintenance;
};
 


function mComboFilterMaintenance()
{
  var cboFilterMaintenance = new Ext.form.ComboBox
	(
		{
			id:'cboFilterMaintenance',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmFilterMaintMon+ ' ',			
			width:130,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 'This Year'], [2, 'This Month'],[3, 'Today']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectFilterMaintenance,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectFilterMaintenance=b.data.Id ;
					RefreshDataSetMaintenance(selectFilterMaintenance);
				} 
			}
		}
	);
	return cboFilterMaintenance;
};
