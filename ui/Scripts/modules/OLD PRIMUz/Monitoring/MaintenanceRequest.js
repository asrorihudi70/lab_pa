﻿
var dsSetMaintenanceRequestList;
var AddNewSetMaintenanceRequest;
var selectCountSetMaintenanceRequest=50;
var rowSelectedSetMaintenanceRequest;

CurrentPage.page = getPanelSetMaintenanceRequest(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetMaintenanceRequest(mod_id) 
{
	
    var Field = ['STATUS_ID','STATUS_NAME','LOCATION_ID','ASSET_MAIN','LOCATION','DUE_DATE','REQ_ID','PROBLEM','EMP_ID','EMP_NAME','DEPT_ID','DEPT_NAME','IMPACT','REQ_FINISH_DATE','ASSET_MAINT_ID','ROW_REQ','DESC_REQ','DESC_STATUS'];
    dsSetMaintenanceRequestList = new WebApp.DataStore({ fields: Field });
	//alert(CurrentPage.criteria);
	RefreshDataSetMaintenanceRequest();

    var grListSetMaintenanceRequest = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetMaintenanceRequest',
		    stripeRows: true,
		    store: dsSetMaintenanceRequestList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetMaintenanceRequest=undefined;
							rowSelectedSetMaintenanceRequest = dsSetMaintenanceRequestList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
					    id: 'colStatusImageRequestViewRequest',
					    header: nmColStatusMaintReqMon,
					    dataIndex: 'STATUS_ID',
					    sortable: true,
					    width: 70,
						align:'center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store) 
						{
							 switch (value) 
							 {
								 case '1':  
									 metaData.css = 'StatusHijau'; // open
									 break;
								 case '6':   
									 metaData.css = 'StatusKuning'; // accepted / approve
									 break;
								 case '7':  
									metaData.css = 'StatusMerah'; // rejected
									
									 break;
							 }
								return '';     
						}
					},
					{
					    id: 'colReqIdViewRequest',
					    header: nmColReqIDMaintReqMon,
					    dataIndex: 'REQ_ID',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'colTglRequestViewRequest',
					    header: nmColDateMaintReqMon,
					    dataIndex: 'DUE_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.DUE_DATE);
					    }
					},
					{   //Nama Aset + Kode
					    id: 'colAssetMaintViewRequest',
					    header: nmColAssetMaintReqMon,
					    dataIndex: 'ASSET_MAIN',
					    sortable: true,
					    width: 170
					},
					// {
					    // id: 'colLocationViewRequest',
					    // header: 'Location',
					    // dataIndex: 'LOCATION',
					    // sortable: true,
					    // width: 100
					// },
					{
					    id: 'colProblemViewRequest',
					    header: nmColProblemMaintReqMon,
					    dataIndex: 'PROBLEM',
					    width: 300
					},
					{
					    header: nmColRequesterMaintReqMon,
					    width: 100,
					    sortable: true,
					    dataIndex: 'EMP_NAME',
					    id: 'colRequesterViewRequest'
					},
					// {
					    // id: 'colDeptViewRequest',
					    // header: 'Department',
					    // dataIndex: 'DEPT_NAME',
					    // sortable: true,
					    // width: 100
					// },
					// {
					    // id: 'colImpactViewRequest',
					    // header: "Impact",
					    // dataIndex: 'IMPACT',
					    // width: 100
					// },
					{
					    id: 'colTglSelesaiViewRequest',
					    header: nmColTargetDateMaintReqMon,
					    dataIndex: 'REQ_FINISH_DATE',
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.REQ_FINISH_DATE);
					    }
					}
                ]
			)
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetMaintenanceRequest = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormMaintReqMon,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupMaintenanceRequest',
		    items: [grListSetMaintenanceRequest],
		    tbar:
			[
				// nmKdLokasi2 + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'Id : ',
					// id: 'txtKDSetMaintenanceRequestFilter',                   
					// width:80,
					// onInit: function() { }
				// }, ' ','-',
				// nmLokasi + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'MaintenanceRequest : ',
					// id: 'txtSetMaintenanceRequestFilter',                   
					// anchor: '95%',
					// onInit: function() { }
				// }, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetMaintenanceRequest(),
				' ','-',
				{
				    id: 'btnRefreshSetMaintenanceRequest',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetMaintenanceRequestFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetMaintenanceRequest();
				}
			}
		}
	);
    //END var FormSetMaintenanceRequest--------------------------------------------------

	RefreshDataSetMaintenanceRequest();
    return FormSetMaintenanceRequest ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function RefreshDataSetMaintenanceRequest()
{	
	dsSetMaintenanceRequestList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetMaintenanceRequest, 
					//Sort: 'Due_Date',
                                        Sort: 'due_date', 
					Sortdir: 'ASC', 
					target:'viMonitoringMaintenanceRequest',
					param : CurrentPage.criteria
				}			
			}
		);       
	
	rowSelectedSetMaintenanceRequest = undefined;
	return dsSetMaintenanceRequestList;
};

function RefreshDataSetMaintenanceRequestFilter() 
{   
	RefreshDataSetMaintenanceRequest();
};



function mComboMaksDataSetMaintenanceRequest()
{
  var cboMaksDataSetMaintenanceRequest = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetMaintenanceRequest',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetMaintenanceRequest,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetMaintenanceRequest=b.data.displayText ;
					RefreshDataSetMaintenanceRequest();
				} 
			}
		}
	);
	return cboMaksDataSetMaintenanceRequest;
};
 



