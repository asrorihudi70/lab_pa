﻿
var dsSetMaintenanceResultList;
var AddNewSetMaintenanceResult;
var selectCountSetMaintenanceResult=50;
var rowSelectedSetMaintenanceResult;

CurrentPage.page = getPanelSetMaintenanceResult(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetMaintenanceResult(mod_id) 
{
	
    var Field =['CATEGORY_ID', 'CATEGORY_NAME', 'DUE_DATE', 'RESULT_CM_ID', 'SERVICE_ID', 'Asset', 'SERVICE_NAME', 'Cost'];
    dsSetMaintenanceResultList = new WebApp.DataStore({ fields: Field });
	//alert(CurrentPage.criteria);
	RefreshDataSetMaintenanceResult();

    var grListSetMaintenanceResult = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetMaintenanceResult',
		    stripeRows: true,
		    store: dsSetMaintenanceResultList,
			// store: new Ext.data.GroupingStore
					// ({
						// reader: reader,
						// data: dsSetMaintenanceResultList,
						// sortInfo:{field: 'WO_CM_DATE', direction: "ASC"},
						// groupField:'WO_CM_ID'
					// }),

			autoScroll: true,
		    columnLines: true,
			border:false,			
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetMaintenanceResult=undefined;
							rowSelectedSetMaintenanceResult = dsSetMaintenanceResultList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),                   
					
					{
					    id: 'colAssetName',
					    header: nmColAssetMaintRsltMon,					   
					    dataIndex: 'Asset',
					    width: 250,
					    sortable: true
					},
					{
					    id: 'colFinishDate',
					    header: nmColFinishDateMaintRsltMon,					   
					    dataIndex: 'DUE_DATE',
					    width: 90,
					    sortable: true,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.DUE_DATE);
					    }
					},		
					{
					    id: 'colServiceName',
					    header: nmColServiceMaintRsltMon,					   
					    dataIndex: 'SERVICE_NAME',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colCost',
					    header: nmColCostMaintRsltMon,					   
					    dataIndex: 'Cost',
					    width: 80,
					    sortable: true,
						renderer: function(v, params, record) 
						{
					        return formatCurrency(record.data.Cost);
					    }
					}		
                ]
			)
			 // view: new Ext.grid.GroupingView({
            // forceFit:true,
            // groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
        // }),

		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetMaintenanceResult = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormMaintRsltMon,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupMaintenanceResult',
		    items: [grListSetMaintenanceResult],
		    tbar:
			[
				// nmKdLokasi2 + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'Id : ',
					// id: 'txtKDSetMaintenanceResultFilter',                   
					// width:80,
					// onInit: function() { }
				// }, ' ','-',
				// nmLokasi + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'MaintenanceResult : ',
					// id: 'txtSetMaintenanceResultFilter',                   
					// anchor: '95%',
					// onInit: function() { }
				// }, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetMaintenanceResult(),
				' ','-',
				{
				    id: 'btnRefreshSetMaintenanceResult',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetMaintenanceResultFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetMaintenanceResult();
				}
			}
		}
	);
    //END var FormSetMaintenanceResult--------------------------------------------------

	RefreshDataSetMaintenanceResult();
    return FormSetMaintenanceResult ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function RefreshDataSetMaintenanceResult()
{	
	dsSetMaintenanceResultList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetMaintenanceResult, 
					//Sort: 'Due_Date',
                                        Sort: 'due_date',
					Sortdir: 'ASC', 
					target:'viMonitoringMaintenanceResult',
					param : CurrentPage.criteria
				}			
			}
		);       
	
	rowSelectedSetMaintenanceResult = undefined;
	return dsSetMaintenanceResultList;
};

function RefreshDataSetMaintenanceResultFilter() 
{   
	RefreshDataSetMaintenanceResult();
};



function mComboMaksDataSetMaintenanceResult()
{
  var cboMaksDataSetMaintenanceResult = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetMaintenanceResult',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetMaintenanceResult,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetMaintenanceResult=b.data.displayText ;
					RefreshDataSetMaintenanceResult();
				} 
			}
		}
	);
	return cboMaksDataSetMaintenanceResult;
};
 



