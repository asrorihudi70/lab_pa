﻿
var dsSetMaintenanceScheduleList;
var AddNewSetMaintenanceSchedule;
var selectCountSetMaintenanceSchedule=50;
var rowSelectedSetMaintenanceSchedule;

CurrentPage.page = getPanelSetMaintenanceSchedule(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetMaintenanceSchedule(mod_id) 
{
	
    var Field =['Status','PATH','ID','CATEGORY_ID','CATEGORY_NAME','ASSET_MAINT_ID','ASSET_MAINT_NAME','STATUS_ID','DUE_DATE','SERVICE_PM_NAME','LOCATION','DEPT_NAME','LOCATION_ID','DEPT_ID'];
    dsSetMaintenanceScheduleList = new WebApp.DataStore({ fields: Field });
	//alert(CurrentPage.criteria);
	RefreshDataSetMaintenanceSchedule();

    var grListSetMaintenanceSchedule = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetMaintenanceSchedule',
		    stripeRows: true,
		    store: dsSetMaintenanceScheduleList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetMaintenanceSchedule=undefined;
							rowSelectedSetMaintenanceSchedule = dsSetMaintenanceScheduleList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
					    id: 'colStatusImageEWBefore',
					    header: '',
					    //dataIndex: 'Status',
						dataIndex: 'PATH',
					    sortable: true,
					    width: 50,
						align:'center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store) 
						{						   
							return '<img src="' + value + '" class="text-desc-legend"/>'							 							
						}
					},
					{
					    id: 'colAssetName',
					    header: nmColAssetMaintSchMon,					   
					    dataIndex: 'ASSET_MAINT_NAME',
					    width: 300,
					    sortable: true
					},
					{
					    id: 'colServiceName',
					    header: nmColServiceMaintSchMon,					   
					    dataIndex: 'SERVICE_PM_NAME',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colLocationName',
					    header: nmColLocMaintSchMon,					   
					    dataIndex: 'LOCATION',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDeptName',
					    header: nmColDeptMaintSchMon,					   
					    dataIndex: 'DEPT_NAME',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDueDate',
					    header: nmColDueDateMaintSchMon,					   
					    dataIndex: 'DUE_DATE',
					    width: 100,
					    sortable: true,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.DUE_DATE);
					    }
					}
                ]
			)
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetMaintenanceSchedule = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormMaintSchMon,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupMaintenanceSchedule',
		    items: [grListSetMaintenanceSchedule],
		    tbar:
			[
				// nmKdLokasi2 + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'Id : ',
					// id: 'txtKDSetMaintenanceScheduleFilter',                   
					// width:80,
					// onInit: function() { }
				// }, ' ','-',
				// nmLokasi + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'MaintenanceSchedule : ',
					// id: 'txtSetMaintenanceScheduleFilter',                   
					// anchor: '95%',
					// onInit: function() { }
				// }, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetMaintenanceSchedule(),
				' ','-',
				{
				    id: 'btnRefreshSetMaintenanceSchedule',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetMaintenanceScheduleFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetMaintenanceSchedule();
				}
			}
		}
	);
    //END var FormSetMaintenanceSchedule--------------------------------------------------

	RefreshDataSetMaintenanceSchedule();
    return FormSetMaintenanceSchedule ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function RefreshDataSetMaintenanceSchedule()
{	
	dsSetMaintenanceScheduleList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetMaintenanceSchedule, 
					//Sort: 'Due_Date',
                                        Sort: 'due_date',
					Sortdir: 'ASC', 
					target:'viMonitoringMaintenanceSchedule',
					param : CurrentPage.criteria
				}			
			}
		);       
	
	rowSelectedSetMaintenanceSchedule = undefined;
	return dsSetMaintenanceScheduleList;
};

function RefreshDataSetMaintenanceScheduleFilter() 
{   
	RefreshDataSetMaintenanceSchedule();
};



function mComboMaksDataSetMaintenanceSchedule()
{
  var cboMaksDataSetMaintenanceSchedule = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetMaintenanceSchedule',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetMaintenanceSchedule,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetMaintenanceSchedule=b.data.displayText ;
					RefreshDataSetMaintenanceSchedule();
				} 
			}
		}
	);
	return cboMaksDataSetMaintenanceSchedule;
};
 



