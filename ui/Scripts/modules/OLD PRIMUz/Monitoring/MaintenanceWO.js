﻿
var dsSetMaintenanceWOList;
var AddNewSetMaintenanceWO;
var selectCountSetMaintenanceWO=50;
var rowSelectedSetMaintenanceWO;

CurrentPage.page = getPanelSetMaintenanceWO(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetMaintenanceWO(mod_id) 
{
	
    var Field =['CATEGORY_ID', 'CATEGORY_NAME', 'DUE_DATE', 'WO_CM_ID', 'Asset', 'SERVICE_NAME', 'WO_CM_FINISH_DATE', 'DESC_WO_CM','Jenis'];
    dsSetMaintenanceWOList = new WebApp.DataStore({ fields: Field });
	//alert(CurrentPage.criteria);
	RefreshDataSetMaintenanceWO();

    var grListSetMaintenanceWO = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetMaintenanceWO',
		    stripeRows: true,
		    store: dsSetMaintenanceWOList,
			// store: new Ext.data.GroupingStore
					// ({
						// reader: reader,
						// data: dsSetMaintenanceWOList,
						// sortInfo:{field: 'WO_CM_DATE', direction: "ASC"},
						// groupField:'WO_CM_ID'
					// }),
			autoScroll: true,
		    columnLines: true,
			border:false,			
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetMaintenanceWO=undefined;
							rowSelectedSetMaintenanceWO = dsSetMaintenanceWOList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                   
					{
					    id: 'colKdWO',
					    header: nmColWOIDMaintWOMon,					   
					    dataIndex: 'WO_CM_ID',
					    width: 90,
					    sortable: true
					},
					{
					    id: 'colAssetName',
					    header: nmColAssetMaintWOMon,					   
					    dataIndex: 'Asset',
					    width: 250,
					    sortable: true
					},
					{
					    id: 'colDueDate',
					    header: nmColStartDateMaintWOMon,					   
					    dataIndex: 'DUE_DATE',
					    width: 90,
					    sortable: true,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.DUE_DATE);
					    }
					},
					{
					    id: 'colFinishDate',
					    header: nmColFinishDateMaintWOMon,					   
					    dataIndex: 'WO_CM_FINISH_DATE',
					    width: 90,
					    sortable: true,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.WO_CM_FINISH_DATE);
					    }
					},					
					{
					    id: 'colJenis',
					    header: nmColJenisMaintWOMon,					   
					    dataIndex: 'Jenis',
					    width: 80,
					    sortable: true
					},					
					{
					    id: 'colServiceName',
					    header: nmColServiceMaintWOMon,					   
					    dataIndex: 'SERVICE_NAME',
					    width: 150,
					    sortable: true
					}
                ]
			)
			 // view: new Ext.grid.GroupingView({
            // forceFit:true,
            // groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
        // }),
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetMaintenanceWO = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormMaintWOMon,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupMaintenanceWO',
		    items: [grListSetMaintenanceWO],
		    tbar:
			[
				// nmKdLokasi2 + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'Id : ',
					// id: 'txtKDSetMaintenanceWOFilter',                   
					// width:80,
					// onInit: function() { }
				// }, ' ','-',
				// nmLokasi + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'MaintenanceWO : ',
					// id: 'txtSetMaintenanceWOFilter',                   
					// anchor: '95%',
					// onInit: function() { }
				// }, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetMaintenanceWO(),
				' ','-',
				{
				    id: 'btnRefreshSetMaintenanceWO',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetMaintenanceWOFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetMaintenanceWO();
				}
			}
		}
	);
    //END var FormSetMaintenanceWO--------------------------------------------------

	RefreshDataSetMaintenanceWO();
    return FormSetMaintenanceWO ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function RefreshDataSetMaintenanceWO()
{	
	dsSetMaintenanceWOList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetMaintenanceWO, 
					//Sort: 'Due_Date',
                                        Sort: 'due_date',
					Sortdir: 'ASC', 
					target:'viMonitoringMaintenanceWO',
					param : CurrentPage.criteria
				}			
			}
		);       
	
	rowSelectedSetMaintenanceWO = undefined;
	return dsSetMaintenanceWOList;
};

function RefreshDataSetMaintenanceWOFilter() 
{   
	RefreshDataSetMaintenanceWO();
};



function mComboMaksDataSetMaintenanceWO()
{
  var cboMaksDataSetMaintenanceWO = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetMaintenanceWO',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetMaintenanceWO,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetMaintenanceWO=b.data.displayText ;
					RefreshDataSetMaintenanceWO();
				} 
			}
		}
	);
	return cboMaksDataSetMaintenanceWO;
};
 



