﻿
var dsSetOnCloseList;
var AddNewSetOnClose;
var selectCountSetOnClose=50;
var rowSelectedSetOnClose;

CurrentPage.page = getPanelSetOnClose(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetOnClose(mod_id) 
{
	
    var Field =['ID','CATEGORY_ID','CATEGORY_NAME','ASSET_MAINT_ID','ASSET_MAINT_NAME','STATUS_ID','DUE_DATE','SERVICE_PM_NAME','LOCATION','DEPT_NAME','LOCATION_ID','DEPT_ID'];
    dsSetOnCloseList = new WebApp.DataStore({ fields: Field });
	//alert(CurrentPage.criteria);
	RefreshDataSetOnClose();

    var grListSetOnClose = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetOnClose',
		    stripeRows: true,
		    store: dsSetOnCloseList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetOnClose=undefined;
							rowSelectedSetOnClose = dsSetOnCloseList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    // {
                        // id: 'colIdAsset',
                        // header: 'Id',                      
                        // dataIndex: 'ASSET_MAINT_ID',
                        // sortable: true,
                        // width: 100
                    // },
					{
					    id: 'colAssetName',
					    header: nmColAssetOnCloseMon,					   
					    dataIndex: 'ASSET_MAINT_NAME',
					    width: 300,
					    sortable: true
					},
					{
					    id: 'colServiceName',
					    header: nmColServiceOnCloseMon,					   
					    dataIndex: 'SERVICE_PM_NAME',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colLocationName',
					    header: nmColLocOnCloseMon,					   
					    dataIndex: 'LOCATION',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDeptName',
					    header: nmColDepOnCloseMon,					   
					    dataIndex: 'DEPT_NAME',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDueDate',
					    header: nmColDueDateOnCloseMon,					   
					    dataIndex: 'DUE_DATE',
					    width: 100,
					    sortable: true,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.DUE_DATE);
					    }
					}
                ]
			)
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetOnClose = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormOnCloseMon,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupOnClose',
		    items: [grListSetOnClose],
		    tbar:
			[
				// nmKdLokasi2 + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'Id : ',
					// id: 'txtKDSetOnCloseFilter',                   
					// width:80,
					// onInit: function() { }
				// }, ' ','-',
				// nmLokasi + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'OnClose : ',
					// id: 'txtSetOnCloseFilter',                   
					// anchor: '95%',
					// onInit: function() { }
				// }, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetOnClose(),
				' ','-',
				{
				    id: 'btnRefreshSetOnClose',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetOnCloseFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetOnClose();
				}
			}
		}
	);
    //END var FormSetOnClose--------------------------------------------------

	RefreshDataSetOnClose();
    return FormSetOnClose ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function RefreshDataSetOnClose()
{	
	dsSetOnCloseList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetOnClose, 
					//Sort: 'Due_Date',
                                        Sort: 'due_date',
					Sortdir: 'ASC', 
					target:'ViewMonitoring',
					//param : 'WHERE STATUS_ID=~3~ AND CATEGORY_ID=~' + CurrentPage.criteria + '~' + ' AND year(Due_Date) = ' + nowDash.format('Y')
                                        param : 'status_id = ~3~ AND category_id = ~' + CurrentPage.criteria + '~' + ' AND date_part(~year~, due_date) = ' + nowDash.format('Y')
				}			
			}
		);       
	
	rowSelectedSetOnClose = undefined;
	return dsSetOnCloseList;
};

function RefreshDataSetOnCloseFilter() 
{   
	RefreshDataSetOnClose();
};



function mComboMaksDataSetOnClose()
{
  var cboMaksDataSetOnClose = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetOnClose',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetOnClose,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetOnClose=b.data.displayText ;
					RefreshDataSetOnClose();
				} 
			}
		}
	);
	return cboMaksDataSetOnClose;
};
 



