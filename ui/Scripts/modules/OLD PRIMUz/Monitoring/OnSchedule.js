var dsSetOnScheduleList;
var AddNewSetOnSchedule;
var selectCountSetOnSchedule=50;
var rowSelectedSetOnSchedule;

CurrentPage.page = getPanelSetOnSchedule(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetOnSchedule(mod_id) 
{
	
    var Field =['ID','CATEGORY_ID','CATEGORY_NAME','ASSET_MAINT_ID','ASSET_MAINT_NAME','STATUS_ID','DUE_DATE','SERVICE_PM_NAME','LOCATION','DEPT_NAME','LOCATION_ID','DEPT_ID'];
    dsSetOnScheduleList = new WebApp.DataStore({ fields: Field });
	//alert(CurrentPage.criteria);
	RefreshDataSetOnSchedule();

    var grListSetOnSchedule = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetOnSchedule',
		    stripeRows: true,
		    store: dsSetOnScheduleList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetOnSchedule=undefined;
							rowSelectedSetOnSchedule = dsSetOnScheduleList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    // {
                        // id: 'colIdAsset',
                        // header: 'Id',                      
                        // dataIndex: 'ASSET_MAINT_ID',
                        // sortable: true,
                        // width: 100
                    // },
					{
					    id: 'colAssetName',
					    header: nmColAssetOnSchMon,					   
					    dataIndex: 'ASSET_MAINT_NAME',
					    width: 300,
					    sortable: true
					},
					{
					    id: 'colServiceName',
					    header: nmColServiceOnSchMon,					   
					    dataIndex: 'SERVICE_PM_NAME',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colLocationName',
					    header: nmColLocOnSchMon,					   
					    dataIndex: 'LOCATION',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDeptName',
					    header: nmColDepOnSchMon,					   
					    dataIndex: 'DEPT_NAME',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDueDate',
					    header: nmColDueDatOnSchMon,					   
					    dataIndex: 'DUE_DATE',
					    width: 100,
					    sortable: true,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.DUE_DATE);
					    }
					}
                ]
			)
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetOnSchedule = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormOnSchMon,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupOnSchedule',
		    items: [grListSetOnSchedule],
		    tbar:
			[
				// nmKdLokasi2 + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'Id : ',
					// id: 'txtKDSetOnScheduleFilter',                   
					// width:80,
					// onInit: function() { }
				// }, ' ','-',
				// nmLokasi + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'OnSchedule : ',
					// id: 'txtSetOnScheduleFilter',                   
					// anchor: '95%',
					// onInit: function() { }
				// }, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetOnSchedule(),
				' ','-',
				{
				    id: 'btnRefreshSetOnSchedule',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetOnScheduleFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetOnSchedule();
				}
			}
		}
	);
    //END var FormSetOnSchedule--------------------------------------------------

	RefreshDataSetOnSchedule();
    return FormSetOnSchedule ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function RefreshDataSetOnSchedule()
{	
	dsSetOnScheduleList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetOnSchedule, 
					//Sort: 'Due_Date',
                                        Sort: 'due_date',
					Sortdir: 'ASC', 
					target:'ViewMonitoring',
					//param : 'WHERE year(Due_Date) = ' + nowDash.format('Y') + ' AND STATUS_ID=~4~ AND CATEGORY_ID=~' + CurrentPage.criteria + '~'
                                        param : 'date_part(~year~, due_date) = ' + nowDash.format('Y') + ' AND status_id = ~4~ AND category_id = ~' + CurrentPage.criteria + '~'
				}			
			}
		);       
	
	rowSelectedSetOnSchedule = undefined;
	return dsSetOnScheduleList;
};

function RefreshDataSetOnScheduleFilter() 
{   
	RefreshDataSetOnSchedule();
};



function mComboMaksDataSetOnSchedule()
{
  var cboMaksDataSetOnSchedule = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetOnSchedule',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetOnSchedule,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetOnSchedule=b.data.displayText ;
					RefreshDataSetOnSchedule();
				} 
			}
		}
	);
	return cboMaksDataSetOnSchedule;
};
 



