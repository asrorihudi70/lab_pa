var dsSetOnWorkOrderList;
var AddNewSetOnWorkOrder;
var selectCountSetOnWorkOrder=50;
var rowSelectedSetOnWorkOrder;

CurrentPage.page = getPanelSetOnWorkOrder(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetOnWorkOrder(mod_id) 
{
	
    var Field =['ID','CATEGORY_ID','CATEGORY_NAME','ASSET_MAINT_ID','ASSET_MAINT_NAME','STATUS_ID','DUE_DATE','SERVICE_PM_NAME','LOCATION','DEPT_NAME','LOCATION_ID','DEPT_ID'];
    dsSetOnWorkOrderList = new WebApp.DataStore({ fields: Field });
	//alert(CurrentPage.criteria);
	RefreshDataSetOnWorkOrder();

    var grListSetOnWorkOrder = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetOnWorkOrder',
		    stripeRows: true,
		    store: dsSetOnWorkOrderList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetOnWorkOrder=undefined;
							rowSelectedSetOnWorkOrder = dsSetOnWorkOrderList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    // {
                        // id: 'colIdAsset',
                        // header: 'Id',                      
                        // dataIndex: 'ASSET_MAINT_ID',
                        // sortable: true,
                        // width: 100
                    // },
					{
					    id: 'colAssetName',
					    header: nmColAssetOnWOMon,					   
					    dataIndex: 'ASSET_MAINT_NAME',
					    width: 300,
					    sortable: true
					},
					{
					    id: 'colServiceName',
					    header: nmColServiceOnWOMon,					   
					    dataIndex: 'SERVICE_PM_NAME',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colLocationName',
					    header: nmColLocOnWOMon,					   
					    dataIndex: 'LOCATION',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDeptName',
					    header: nmColDepOnWOMon,					   
					    dataIndex: 'DEPT_NAME',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colDueDate',
					    header: nmColDueDatOnWOMon,					   
					    dataIndex: 'DUE_DATE',
					    width: 100,
					    sortable: true,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.DUE_DATE);
					    }
					}
                ]
			)
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetOnWorkOrder = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormOnWOMon,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupOnWorkOrder',
		    items: [grListSetOnWorkOrder],
		    tbar:
			[
				// nmKdLokasi2 + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'Id : ',
					// id: 'txtKDSetOnWorkOrderFilter',                   
					// width:80,
					// onInit: function() { }
				// }, ' ','-',
				// nmLokasi + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: 'OnWorkOrder : ',
					// id: 'txtSetOnWorkOrderFilter',                   
					// anchor: '95%',
					// onInit: function() { }
				// }, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetOnWorkOrder(),
				' ','-',
				{
				    id: 'btnRefreshSetOnWorkOrder',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetOnWorkOrderFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetOnWorkOrder();
				}
			}
		}
	);
    //END var FormSetOnWorkOrder--------------------------------------------------

	RefreshDataSetOnWorkOrder();
    return FormSetOnWorkOrder ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function RefreshDataSetOnWorkOrder()
{	
	dsSetOnWorkOrderList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetOnWorkOrder, 
					//Sort: 'Due_Date',
                                        Sort: 'due_date',
					Sortdir: 'ASC', 
					target:'ViewMonitoring',
					//param : 'WHERE STATUS_ID=~5~ AND CATEGORY_ID=~' + CurrentPage.criteria + '~' + ' AND year(Due_Date) = ' + nowDash.format('Y')
                                        param : 'status_id = ~5~ AND category_id = ~' + CurrentPage.criteria + '~' + ' AND date_part(~year~, due_date) = ' + nowDash.format('Y')
				}			
			}
		);       
	
	rowSelectedSetOnWorkOrder = undefined;
	return dsSetOnWorkOrderList;
};

function RefreshDataSetOnWorkOrderFilter() 
{   
	RefreshDataSetOnWorkOrder();
};



function mComboMaksDataSetOnWorkOrder()
{
  var cboMaksDataSetOnWorkOrder = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetOnWorkOrder',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetOnWorkOrder,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetOnWorkOrder=b.data.displayText ;
					RefreshDataSetOnWorkOrder();
				} 
			}
		}
	);
	return cboMaksDataSetOnWorkOrder;
};
 



