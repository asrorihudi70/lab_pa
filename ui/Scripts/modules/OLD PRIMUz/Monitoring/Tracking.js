var varRepairExtTracking='External';
var varRepairIntTracking='Internal';
var varStatusOpenTracking='1';
var dsTrackingList;
var dsTrackingListReqInfo;
var dsTrackingListAppInfo;
var dsTrackingListWOInfo;
var dsTrackingListCloseInfo;
var selectCountTracking = 50;
var now = new Date();
var selectTracking;
var rowSelectedTracking;
var rowSelectedTrackingReqInfo;
var rowSelectedTrackingAppInfo;
var rowSelectedTrackingWOInfo;
var rowSelectedTrackingCloseInfo;
var cellSelectedTrackingDet;
var TrackingLookUps;
var nowTracking = new Date();
var FocusCtrlTracking;
var selectCategoryTrackingView=' 9999';
var valueCategoryTrackingView=' All';
var StrTreeComboTrackingView;
var rootTreeTrackingView;
var treeTrackingView;
var IdCategoryTrackingView=' 9999';



CurrentPage.page = getPanelTracking(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelTracking(mod_id) 
{
    var Field = ['STATUS_ID','STATUS_NAME','LOCATION_ID','ASSET_MAIN','LOCATION','REQ_DATE','REQ_ID','PROBLEM','EMP_ID','EMP_NAME','DEPT_ID','DEPT_NAME','IMPACT','REQ_FINISH_DATE','ASSET_MAINT_ID','ROW_REQ','DESC_REQ','DESC_STATUS','CATEGORY_ID','CATEGORY_NAME'];
	
	var FieldReq=['REQ_ID','ROW_REQ','ASSET_MAINT_ID','STATUS_ID','PROBLEM','REQ_FINISH_DATE','DESC_REQ','IMPACT','DESC_STATUS','LOCATION','CATEGORY_ID','ASSET_MAINT_NAME','REQ_DATE','EMP_ID','DEPT_ID_REQ','DEPT_NAME_REQ','DEPT_ID_ASSET','DEPT_NAME_ASSET','EMP_NAME','STATUS_NAME'];
	
	var FieldApp=['APP_ID','EMP_ID','REQ_ID','ROW_REQ','APP_DUE_DATE','APP_FINISH_DATE','DESC_APP','IS_EXT_REPAIR','COST','DESC_STATUS','EMP_NAME','STATUS_ID','STATUS_NAME'];
	
	var FieldWO=['WO_CM_ID','EMP_ID','SCH_CM_ID','STATUS_ID','WO_CM_DATE','WO_CM_FINISH_DATE','DESC_WO_CM','EMP_NAME','REQ_ID','ROW_REQ','STATUS_NAME'];
	
	var FieldClose=['RESULT_CM_ID','WO_CM_ID','FINISH_DATE','DIFF_FINISH_DATE','LAST_COST','DIFF_COST','DESC_RESULT_CM','REFERENCE','REQ_ID','ROW_REQ'];
	
    dsTrackingList = new WebApp.DataStore({ fields: Field });
	dsTrackingListReqInfo=new WebApp.DataStore({ fields: FieldReq });
	dsTrackingListAppInfo=new WebApp.DataStore({ fields: FieldApp });
	dsTrackingListWOInfo=new WebApp.DataStore({ fields: FieldWO });
	dsTrackingListCloseInfo=new WebApp.DataStore({ fields: FieldClose });
	
	GetStrTreeComboTrackingView();
    var grListTracking = new Ext.grid.EditorGridPanel
	(
		{
		    stripeRows: true,
		    store: dsTrackingList,
		    anchor: '100% 100%',
		    columnLines: true,
			autoScroll:true,
		    border: false,
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
					        rowSelectedTracking = dsTrackingList.getAt(row);
//							var str =' where req_id=~' + rowSelectedTracking.data.REQ_ID + '~'
//								str += ' and row_req =' + rowSelectedTracking.data.ROW_REQ
//								str += ' and asset_maint_id =~' + rowSelectedTracking.data.ASSET_MAINT_ID + '~'
                                                        var str = ' req_id = ~' + rowSelectedTracking.data.REQ_ID + '~'
								str += ' and row_req = ' + rowSelectedTracking.data.ROW_REQ
								str += ' and asset_maint_id = ~' + rowSelectedTracking.data.ASSET_MAINT_ID + '~'
                                                                
							RefreshDataTrackingReqInfo(str);
							
//							var str2 =' where req_id=~' + rowSelectedTracking.data.REQ_ID + '~'
//								str2 += ' and row_req =' + rowSelectedTracking.data.ROW_REQ

							var str2 =' req_id = ~' + rowSelectedTracking.data.REQ_ID + '~'
								str2 += ' and row_req = ' + rowSelectedTracking.data.ROW_REQ

							RefreshDataTrackingAppInfo(str2);
							RefreshDataTrackingWOInfo(str2);
							RefreshDataTrackingCloseInfo(str2);
					    }
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					if (dsTrackingListReqInfo.getCount() > 0)
					{
						rowSelectedTrackingReqInfo=dsTrackingListReqInfo.data.items[0].data
					}
					else
					{
						rowSelectedTrackingReqInfo = undefined;
					};
					
					if (dsTrackingListAppInfo.getCount() > 0)
					{
						rowSelectedTrackingAppInfo=dsTrackingListAppInfo.data.items[0].data
					}
					else
					{
						rowSelectedTrackingAppInfo = undefined;
					};
					
					if (dsTrackingListWOInfo.getCount() > 0)
					{
						rowSelectedTrackingWOInfo=dsTrackingListWOInfo.data.items[0].data
					}
					else
					{
						rowSelectedTrackingWOInfo = undefined;
					};
					
					if (dsTrackingListCloseInfo.getCount() > 0)
					{
						rowSelectedTrackingCloseInfo=dsTrackingListCloseInfo.data.items[0].data
					}
					else
					{
						rowSelectedTrackingCloseInfo = undefined;
					};
					
					TrackingLookUp(rowSelectedTrackingReqInfo,rowSelectedTrackingAppInfo,rowSelectedTrackingWOInfo,rowSelectedTrackingCloseInfo);
				}
			},
		    cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'colReqIdViewTracking',
					    header: nmReqIDTracking,
					    dataIndex: 'REQ_ID',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'colTglTrackingViewTracking',
					    header: nmReqDateTracking,
					    dataIndex: 'REQ_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.REQ_DATE);
					    }
					},
					{   //Nama Aset + Kode
					    id: 'colAssetMaintViewTracking',
					    header: nmAssetTracking,
					    dataIndex: 'ASSET_MAIN',
					    sortable: true,
					    width: 170
					},
					{
					    id: 'colLocationViewTracking',
					    header: nmLocationTracking,
					    dataIndex: 'LOCATION',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'colProblemViewTracking',
					    header: nmProblemTracking,
					    dataIndex: 'PROBLEM',
					    width: 300
					},
					{
					    header: nmRequesterTracking,
					    width: 100,
					    sortable: true,
					    dataIndex: 'EMP_NAME',
					    id: 'colTrackingerViewTracking'
					},
					{
					    id: 'colDeptViewTracking',
					    header: nmDeptTracking,
					    dataIndex: 'DEPT_NAME',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'colImpactViewTracking',
					    header: nmImpactTracking,
					    dataIndex: 'IMPACT',
					    width: 100
					},
					{
					    id: 'colTglSelesaiViewTracking',
					    header: nmTargetDateRequest,
					    dataIndex: 'REQ_FINISH_DATE',
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.REQ_FINISH_DATE);
					    }
					}
				]
			),
		    tbar:
			[
				nmLocationTracking + ' : ', ' ',
				{
				    xtype: 'textfield',
				    fieldLabel: nmLocationTracking + ' ',
				    id: 'txtLocViewTrackingFilter',
				    width: 100
				},' ', '-',
				nmDeptTracking + ' : ', ' ',
				{
				    xtype: 'textfield',
				    fieldLabel: nmDeptTracking + ' ',
				    id: 'txtDeptViewTrackingFilter',
				    width: 100
				},
				' ', '-',
				{
					xtype: 'checkbox',
					id: 'chkWithTglTracking',
					hideLabel:true,
					checked: false,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilterTracking').dom.disabled=false;
							Ext.get('dtpTglAkhirFilterTracking').dom.disabled=false;	
							
						}
						else
						{
							Ext.get('dtpTglAwalFilterTracking').dom.disabled=true;
							Ext.get('dtpTglAkhirFilterTracking').dom.disabled=true;
						};
					}
				}
				, ' ', '-', nmReqDateTracking + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmReqDateTracking + ' ',
				    id: 'dtpTglAwalFilterTracking',
				    format: 'd/M/Y',
				    value: now,
				    width: 100,
				    onInit: function() { }
				}, ' ', '  ' + nmSd + ' ', ' ', 
				{
				    xtype: 'datefield',
				    fieldLabel: nmSd + ' ',
				    id: 'dtpTglAkhirFilterTracking',
				    format: 'd/M/Y',
				    value: now,
				    width: 100
				}
			]
		}
	);
	

    var FormTracking = new Ext.Panel
	(
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormTracking,
		    border: false,
		    shadhow: true,
			autoScroll:false,
		    iconCls: 'Tracking',
		    margins: '0 5 5 0',
		    items: [grListTracking],
		    tbar:
			[
				nmReqIDTracking + ' : ', ' ',
				{
				    xtype: 'textfield',
				    fieldLabel: nmReqIDTracking + ' ',
				    id: 'txtReqIdViewTrackingFilter',
				    width: 100,
				    onInit: function() { }
				},' ', '-',
				nmCategoryTracking + ' : ', ' ',mComboCategoryTrackingView() 
				,' ', '-',
					nmMaksData + ' : ', ' ', mComboMaksDataTracking(),
					' ', '-',
				{
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    anchor: '25%',
				    handler: function(sm, row, rec) 
					{
						rowSelectedTracking=undefined;
				        RefreshDataTrackingFilter();
				    }
				}
			],
		    listeners:
			{
			    'afterrender': function() 
				{
			    }
			}
		}
	);
	
   RefreshDataTracking();
   RefreshDataTracking();
	
    return FormTracking

};


function TrackingLookUp(rowdataReq,rowdataApp,rowdataWO,rowdataClose) 
{
    var lebar = 800;//735;
	GetStrTreeComboTrackingView();
    TrackingLookUps = new Ext.Window
	(
		{
		    id: 'gridTracking',
		    title: nmTitleFormTracking,
		    closeAction: 'destroy',
		    width: lebar,
		    height: 520,
		    border: false,
			resizable:false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'Tracking',
		    modal: true,
		    items: [getFormEntryTracking(lebar)],
		    listeners:
			{
			    activate: function() 
				{
			    },
			    afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedTracking=undefined;
					RefreshDataTrackingFilter();
				}
			}
		}
	);

    TrackingLookUps.show();
	TrackingInit(rowdataReq,rowdataApp,rowdataWO,rowdataClose)
	TrackingAddNew(rowdataReq,rowdataApp,rowdataWO,rowdataClose);
};

function getFormEntryTracking(lebar) 
{
    var pnlTracking = new Ext.FormPanel
	(
		{
		    id: 'PanelTracking',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    bodyStyle: 'padding:10px 10px 10px 10px',
			height:546,
		    anchor: '100%',
		    width: lebar,
		    border: false,
		    items: [getItemPanelInputTracking(lebar)]//,
		    // tbar:
			// [
				// '->', '-',
				// {
				    // text: nmCetak,
					// id:'btnCetakTracking',
				    // tooltip: nmCetak,
				    // iconCls: 'print',
				    // handler: function() 
					// {
						// var cKriteria;
						// cKriteria = Ext.get('txtNoTracking').dom.value + '###1###';
						// cKriteria += Ext.get('dtpTanggalTracking').dom.value + '###2###';
						// cKriteria += Ext.get('txtTrackingerTracking').dom.value + '###3###';
						// cKriteria += Ext.get('cboDepartTrackingEntry').dom.value + '###4###';
						// ShowReport('', '920007', cKriteria);	
					// }
				// }
			// ]
		}
	); 

 
   
    var FormTracking = new Ext.Panel
	(
		{
		    id: 'FormTracking',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTracking]

		}
	);

    return FormTracking
};

function TrackingInit(rowdataReqInfo,rowdataAppInfo,rowdataWOInfo,rowdataCloseInfo)
{
   if(rowdataReqInfo != undefined)
   {
		Ext.get('txtNoRequestTracking').dom.value = rowdataReqInfo.REQ_ID;
		Ext.get('txtRequestDateTracking').dom.value = ShowDate(rowdataReqInfo.REQ_DATE);
		Ext.get('txtKdRequesterTracking').dom.value= rowdataReqInfo.EMP_ID;
		Ext.get('txtNamaRequesterTracking').dom.value = rowdataReqInfo.EMP_NAME;
		Ext.get('txtDeptRequesTracking').dom.value = rowdataReqInfo.DEPT_NAME_REQ;
		
		if (rowdataAppInfo === undefined && rowdataReqInfo.STATUS_ID != varStatusOpenTracking)
		{
			Ext.get('txtStatusApproveAppTracking').dom.value= rowdataReqInfo.STATUS_NAME;
			Ext.get('txtKetStatusApproveAppTracking').dom.value = rowdataReqInfo.DESC_STATUS;
		};
    };
	
	 if(rowdataAppInfo != undefined)
	{
		Ext.get('txtKdApproverAppTracking').dom.value = rowdataAppInfo.EMP_ID;
		Ext.get('txtNamaApproverAppTracking').dom.value = rowdataAppInfo.EMP_NAME;
		Ext.get('txtStatusApproveAppTracking').dom.value= rowdataAppInfo.STATUS_NAME;
		Ext.get('txtKetStatusApproveAppTracking').dom.value = rowdataAppInfo.DESC_STATUS;
		Ext.get('txtStartDateAppTracking').dom.value = ShowDate(rowdataAppInfo.APP_DUE_DATE);
		Ext.get('txtFinishDateAppTracking').dom.value = ShowDate(rowdataAppInfo.APP_FINISH_DATE);
		
		if(rowdataAppInfo.IS_EXT_REPAIR === true)
		{
			Ext.get('txtRepairAppTracking').dom.value = varRepairExtTracking;
		}
		else
		{
			Ext.get('txtRepairAppTracking').dom.value = varRepairIntTracking;
		};
		
		Ext.get('txtDeskripsiAppTracking').dom.value = rowdataAppInfo.DESC_APP;
    };
	
	 if(rowdataWOInfo != undefined)
	{
		Ext.get('txtDateWOTracking').dom.value = ShowDate(rowdataWOInfo.WO_CM_DATE);
		Ext.get('txtFinishDateWOTracking').dom.value = ShowDate(rowdataWOInfo.WO_CM_FINISH_DATE);
		Ext.get('txtKdSupervisorWOTracking').dom.value= rowdataWOInfo.EMP_ID;
		Ext.get('txtNamaSupervisorWOTracking').dom.value = rowdataWOInfo.EMP_NAME;
		Ext.get('txtDeskripsiWOTracking').dom.value = rowdataWOInfo.DESC_WO_CM;
    };
	
	 if(rowdataCloseInfo != undefined)
	{
		Ext.get('txtFinishDateCloseTracking').dom.value = ShowDate(rowdataCloseInfo.FINISH_DATE);
		Ext.get('txtNoteCloseTracking').dom.value = rowdataCloseInfo.DESC_RESULT_CM;
    };
};

///---------------------------------------------------------------------------------------///
function TrackingAddNew(rowdataReqInfo,rowdataAppInfo,rowdataWOInfo,rowdataCloseInfo) 
{
 
	if(rowdataReqInfo === undefined)
	{
		Ext.get('txtNoRequestTracking').dom.value = '';
		Ext.get('txtRequestDateTracking').dom.value = '';
		Ext.get('txtKdRequesterTracking').dom.value= '';
		Ext.get('txtNamaRequesterTracking').dom.value = '';
		Ext.get('txtDeptRequesTracking').dom.value = '';
		
		Ext.get('txtStatusApproveAppTracking').dom.value= '';
		Ext.get('txtKetStatusApproveAppTracking').dom.value = '';
    };

	 if(rowdataAppInfo === undefined)
	{
		Ext.get('txtKdApproverAppTracking').dom.value = '';
		Ext.get('txtNamaApproverAppTracking').dom.value = '';
		Ext.get('txtStartDateAppTracking').dom.value = '';
		Ext.get('txtFinishDateAppTracking').dom.value = '';
		Ext.get('txtRepairAppTracking').dom.value = '';
		Ext.get('txtRepairAppTracking').dom.value = '';
		Ext.get('txtDeskripsiAppTracking').dom.value = '';
    };

	 if(rowdataWOInfo === undefined)
	{
		Ext.get('txtDateWOTracking').dom.value = '';
		Ext.get('txtFinishDateWOTracking').dom.value = '';
		Ext.get('txtKdSupervisorWOTracking').dom.value= '';
		Ext.get('txtNamaSupervisorWOTracking').dom.value = '';
		Ext.get('txtDeskripsiWOTracking').dom.value = '';
    };
	
	 if(rowdataCloseInfo === undefined)
	{
		Ext.get('txtFinishDateCloseTracking').dom.value = '';
		Ext.get('txtNoteCloseTracking').dom.value = '';
    };
	
};



function getItemPanelInputTracking(lebar) 
{

	var vTabPanelReqInfoTracking = new Ext.TabPanel
	(
		{
		    id: 'vTabPanelReqInfoTracking',
		    region: 'center',
		    margins: '7 7 7 7',
		    bodyStyle: 'padding:10px 10px 10px 10px',
		    activeTab: 0,
		    plain: true,
		    defferedRender: false,
		    frame: false,
		    border: true,
		    height: 122,
		    anchor: '100%',
		    items:
			[
			    {
			         title: nmTitleTabReqInfoTracking,
			         id: 'tabReqInfoTracking',
			         items:
			         [
						getItemPanelNoRequestTracking(lebar),getItemPanelNoRequestTracking2(lebar),
						getItemPanelDepartTrackingRequest(lebar) 
					 ]
				},
				{
			         title: nmTitleTabAsetInfoTracking,
			         id: 'tabMainInfoTRacking',
			         items:
			         [
						getItemPanelNoAssetTrackingRequest(lebar),
						getItemPanelProblemTrackingRequest(lebar)
					 ],
					 listeners: 
					{
					   activate: function()
					    {
							if (rowSelectedTrackingReqInfo != undefined)
							{
								Ext.get('txtKdMainAssetRequestTracking').dom.value = rowSelectedTrackingReqInfo.ASSET_MAINT_ID;
								Ext.get('txtNamaMainAssetRequestTracking').dom.value = rowSelectedTrackingReqInfo.ASSET_MAINT_NAME;
								Ext.get('txtLocationAssetReqTracking').dom.value= rowSelectedTrackingReqInfo.LOCATION;
								Ext.get('txtProblemRequestTracking').dom.value = rowSelectedTrackingReqInfo.PROBLEM;
								Ext.get('txtDeptAssetRequestTracking').dom.value = rowSelectedTrackingReqInfo.DEPT_NAME_ASSET;
								Ext.get('txtTglSelesaiRequestTracking').dom.value = ShowDate(rowSelectedTrackingReqInfo.REQ_FINISHDATE);
							}
							else
							{
								Ext.get('txtKdMainAssetRequestTracking').dom.value = '';
								Ext.get('txtNamaMainAssetRequestTracking').dom.value = '';
								Ext.get('txtLocationAssetReqTracking').dom.value= '';
								Ext.get('txtProblemRequestTracking').dom.value = '';
								Ext.get('txtDeptAssetRequestTracking').dom.value = '';
								Ext.get('txtTglSelesaiRequestTracking').dom.value = '';
							};
						}
					}
				}
			]
		}
	);

    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
		border:false,
		height:754,
	    items:
		[
			
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:90,
			    layout: 'form',
			    border: false,
			    items:
				[
					vTabPanelReqInfoTracking,
					{
						xtype:'fieldset',
						style:{'margin-top':'3px'},
						title: nmTitleTabAppInfoTracking,	
						anchor:  '99.99%',
						height:145,
						items :
						[
							getItemPanelApproverAppTracking(lebar),
							{
								xtype: 'textfield',
								fieldLabel: nmStatusDescAppTracking + ' ',
								name: 'txtKetStatusApproveAppTracking',
								readOnly:true,
								id: 'txtKetStatusApproveAppTracking',
								anchor: '100%'
							},getItemPanelDateApproveTracking(lebar),
							{
								xtype: 'textarea',
								fieldLabel: nmDescAppTracking + ' ',
								name: 'txtDeskripsiAppTracking',
								id: 'txtDeskripsiAppTracking',
								scroll:true,
								readOnly:true,
								anchor: '100% 27%'
							}
						]
					},
					{
						xtype:'fieldset',
						title: nmTitleTabWOInfoTracking,
						anchor:  '99.99%',
						height:90,
						items :
						[
							getItemPanelDateWOInfoTracking(lebar),
							getItemPanelDescWOInfoTracking() 
						]
					},
					{
						xtype:'fieldset',
						title: nmTitleTabCloseInfoTracking,
						anchor:  '99.99%',
						height:93,
						items :
						[
							{
								xtype: 'textfield',
								fieldLabel: nmFinishDateCloseTracking + ' ',
								name: 'txtFinishDateCloseTracking',
								readOnly:true,
								id: 'txtFinishDateCloseTracking',
								anchor: '40%'
							},
							{
								xtype: 'textarea',
								fieldLabel: nmNoteCloseTracking + ' ',
								name: 'txtNoteCloseTracking',
								id: 'txtNoteCloseTracking',
								scroll:true,
								readOnly:true,
								anchor: '100% 50%'
							}
						]
					}  
				]
			}
		]
	};
    return items;
};

function getItemPanelDescWOInfoTracking() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    labelWidth:90,
				height:30,
			    items:
				[
				    {
				        xtype: 'textarea',
				        fieldLabel: nmNotesWOTracking + ' ',
				        name: 'txtDeskripsiWOTracking',
				        id: 'txtDeskripsiWOTracking',
				        scroll: true,
						readOnly:true,
				        anchor: '100% 90%'
				    }
				]
			}
		]

    }
	return items ;
};

function getItemPanelDateWOInfoTracking(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 57,
	    items:
		[
			{
			    columnWidth: .25,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmDateWOTracking + ' ',
						id: 'txtDateWOTracking',
						name: 'txtDateWOTracking',
						readOnly:true,
						anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .27,
			    layout: 'form',
			    border: false,
				labelWidth:100,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmFinishDateWOTracking + ' ',
						id: 'txtFinishDateWOTracking',
						name: 'txtFinishDateWOTracking',
						readOnly:true,
						anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .2,
			    layout: 'form',
			    border: false,
				labelWidth:40,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmPICWOTracking + ' ',
						id: 'txtKdSupervisorWOTracking',
						name: 'txtKdSupervisorWOTracking',
						readOnly:true,
						anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .28,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: '',
						hideLabel:true,
						id: 'txtNamaSupervisorWOTracking',
						name: 'txtNamaSupervisorWOTracking',
						readOnly:true,
						anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};



function getItemPanelApproverAppTracking(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelApproverAppTracking2(lebar) 
				]
			},
			{
			    columnWidth: .4,
			    layout: 'form',
			    border: false,
				labelWidth:70,
			    items:
				[ 
					{
						xtype: 'textfield',
						fieldLabel: nmStatusAppTracking + ' ',
						readOnly:true,
						name: 'txtStatusApproveAppTracking',
						id: 'txtStatusApproveAppTracking',
						anchor: '99.5%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelDateApproveTracking(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmStartDateAppTracking + ' ',
						name: 'txtStartDateAppTracking',
						readOnly:true,
						id: 'txtStartDateAppTracking',
						anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmFinishDateAppTracking + ' ',
						name: 'txtFinishDateAppTracking',
						readOnly:true,
						id: 'txtFinishDateAppTracking',
						anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .4,
			    layout: 'form',
			    border: false,
				labelWidth:70,
			    items:
				[ 
					{
						xtype: 'textfield',
						fieldLabel: nmRepairAppTracking + ' ',
						name: 'txtRepairAppTracking',
						readOnly:true,
						id: 'txtRepairAppTracking',
						anchor: '99.55%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelApproverAppTracking2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmApproverTracking + ' ',
					    name: 'txtKdApproverAppTracking',
					    id: 'txtKdApproverAppTracking',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
					    name: 'txtNamaApproverAppTracking',
					    id: 'txtNamaApproverAppTracking',
					    anchor: '50%',
						readOnly:true
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelNoRequestTracking(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .4,
			    layout: 'form',
				labelWidth:90,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmReqIDTracking + ' ',
					    name: 'txtNoRequestTracking',
					    id: 'txtNoRequestTracking',
						readOnly:true,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmReqDateTracking + ' ',
					    id: 'txtRequestDateTracking',
					    name: 'txtRequestDateTracking',
						readOnly:true,
					    anchor: '60%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoRequestTracking2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoRequestTracking3(lebar) 
				]
			}
		]
	}
    return items;
};

function getItemPanelDepartTrackingRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmDeptTracking + ' ',
						name: 'txtDeptRequesTracking',
						id: 'txtDeptRequesTracking',
						readOnly:true,
						anchor: '99.60%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoAssetTrackingRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoAssetTrackingRequest2(lebar) 
				]
			},
			{
			    columnWidth: .4,
			    layout: 'form',
			    border: false,
				labelWidth:70,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmLocationTracking + ' ',
					    name: 'txtLocationAssetReqTracking',
					    id: 'txtLocationAssetReqTracking',
						readOnly:true,
					    anchor: '99.99%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelProblemTrackingRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .48,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textarea',
						fieldLabel: nmProblemTracking + ' ',
						name: 'txtProblemRequestTracking',
						id: 'txtProblemRequestTracking',
						scroll:true,
						readOnly:true,
						anchor: '99.99%',
						height:50
				    }
				]
			},
			{
			    columnWidth: .52,
			    layout: 'form',
				labelWidth:80,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmDeptTracking + ' ',
					    id: 'txtDeptAssetRequestTracking',
					    name: 'txtDeptAssetRequestTracking',
					    readOnly: true,
					    anchor: '99.99%'
					},getItemPanelDateTargetApproveRequest(lebar) 
				]
			}
		]
	}
    return items;
};

function getItemPanelDateTargetApproveRequest(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 400,
	    items:
		[
			{
			    columnWidth: .7,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmTargetDateTracking + ' ',
					    id: 'txtTglSelesaiRequestTracking',
					    name: 'txtTglSelesaiRequestTracking',
					    readOnly: true,
					    anchor: '98%'
					}
				]
			}//,
			// {
			    // columnWidth: .3,
			    // layout: 'form',
			    // border: false,
			    // items:
				// [
					// {
					    // xtype: 'button',
					    // text: nmBtnHistory,
					    // width: 80,
					    // hideLabel: true,
					    // id: 'btnOkHistoryApproveRequest',
					    // handler: function() 
					    // {
							// var criteria;
							// criteria='WHERE ASSET_MAINT_ID=~' + Ext.get('txtKdMainAssetApproveRequest').getValue() + '~'
							// FormLookupHistoryAsset(criteria,Ext.get('txtNamaMainAsetApproveRequest').dom.value);
					    // }
					// }
				// ]
			// }
		]
	}
    return items;
};


function getItemPanelNoRequestTracking3(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmRequesterTracking + ' ',
					    name: 'txtKdRequesterTracking',
					    id: 'txtKdRequesterTracking',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .7,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
						readOnly:true,
					    name: 'txtNamaRequesterTracking',
					    id: 'txtNamaRequesterTracking',
					    anchor: '99.70%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoAssetTrackingRequest2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmAssetTracking + ' ',
					    name: 'txtKdMainAssetRequestTracking',
					    id: 'txtKdMainAssetRequestTracking',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
						readOnly:true,
					    name: 'txtNamaMainAssetRequestTracking',
					    id: 'txtNamaMainAssetRequestTracking',
					    anchor: '50%'
					}
				]
			}
		]
	}
    return items;
};

function RefreshDataTracking() 
{
    dsTrackingList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountTracking, 
					Sort: 'req_date', 
					Sortdir: 'ASC', 
					target:'ViewTracking',
					param : ''
				}			
			}
		);       
	
	rowSelectedTracking = undefined;
	return dsTrackingList;
};

function RefreshDataTrackingReqInfo(str) 
{
	if (str === undefined)
	{
		str='';
	};
	
    dsTrackingListReqInfo.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: 1000, 
					Sort: 'req_date', 
					Sortdir: 'ASC', 
					target:'ViewTrackingReqInfo',
					param : str
				}			
			}
		);       
	
	return dsTrackingListReqInfo;
};

function RefreshDataTrackingAppInfo(str) 
{
	if (str === undefined)
	{
		str='';
	};
	
    dsTrackingListAppInfo.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: 1000, 
					Sort: 'app_id', 
					Sortdir: 'ASC', 
					target:'ViewTrackingAppInfo',
					param : str
				}			
			}
		);       
	
	return dsTrackingListAppInfo;
};

function RefreshDataTrackingWOInfo(str) 
{
	if (str === undefined)
	{
		str='';
	};
	
    dsTrackingListWOInfo.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: 1000, 
					Sort: 'wo_cm_date', 
					Sortdir: 'ASC', 
					target:'ViewTrackingWOInfo',
					param : str
				}			
			}
		);       
	
	return dsTrackingListWOInfo;
};

function RefreshDataTrackingCloseInfo(str) 
{
	if (str === undefined)
	{
		str='';
	};
	
    dsTrackingListCloseInfo.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: 1000, 
					Sort: 'finish_date', 
					Sortdir: 'ASC', 
					target:'ViewTrackingCloseInfo',
					param : str
				}			
			}
		);       
	
	return dsTrackingListCloseInfo;
};

function mComboMaksDataTracking() 
{
    var cboMaksDataTracking = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataTracking',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmMaksData + ' ',
		    width: 50,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountTracking,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountTracking = b.data.displayText;
			        RefreshDataTrackingFilter();
			    }
			}
		}
	);
    return cboMaksDataTracking;
};

function RefreshDataTrackingFilter() 
{

	var KataKunci='';
    if (Ext.get('txtReqIdViewTrackingFilter').getValue() != '')
    { 
		//KataKunci = ' where req_id like ~%' + Ext.get('txtReqIdViewTrackingFilter').getValue() + '%~';
                KataKunci = ' req_id like ~%' + Ext.get('txtReqIdViewTrackingFilter').getValue() + '%~';
	};
	
	if (Ext.get('txtLocViewTrackingFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			//KataKunci = ' where location like ~%' + Ext.get('txtLocViewTrackingFilter').getValue() + '%~';
                        KataKunci = ' location like ~%' + Ext.get('txtLocViewTrackingFilter').getValue() + '%~';
		}
		else
		{
                        KataKunci += ' and location like ~%' + Ext.get('txtLocViewTrackingFilter').getValue() + '%~';
		};
	};
	
	if (Ext.get('txtDeptViewTrackingFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			//KataKunci = ' where dept_name like ~%' + Ext.get('txtDeptViewTrackingFilter').getValue() + '%~';
                        KataKunci = ' dept_name like ~%' + Ext.get('txtDeptViewTrackingFilter').getValue() + '%~';
		}
		else
		{
			KataKunci += ' and dept_name like ~%' + Ext.get('txtDeptViewTrackingFilter').getValue() + '%~'; 
		};
	};
	
   if (Ext.get('cboCategoryTrackingView').getValue() != '' && selectCategoryTrackingView != undefined)
    { 
		if (selectCategoryTrackingView.id != undefined)
		{
			if(selectCategoryTrackingView.id != ' 9999')
			{
				if (selectCategoryTrackingView.leaf === false)
				{
					if (KataKunci === '')
					{
						//KataKunci = ' where left(category_id,' + selectCategoryTrackingView.id.length + ')=' + selectCategoryTrackingView.id
                                                KataKunci = ' substr(category_id, 1, ' + selectCategoryTrackingView.id.length + ') = ~' + selectCategoryTrackingView.id + '~'
					}
					else
					{
						//KataKunci +=' and left(category_id,' + selectCategoryTrackingView.id.length + ')=' + selectCategoryTrackingView.id
                                                KataKunci +=' and substr(category_id, 1, ' + selectCategoryTrackingView.id.length + ') = ~' + selectCategoryTrackingView.id + '~'
					};
				}
				else
				{
					if (KataKunci === '')
					{
						//KataKunci =' WHERE Category_id=~'+ selectCategoryTrackingView.id + '~'
                                                KataKunci =' cCategory_id = ~'+ selectCategoryTrackingView.id + '~'
					}
					else
					{
						//KataKunci +=' and Category_id=~'+ selectCategoryTrackingView.id + '~'
                                                KataKunci +=' and category_id = ~'+ selectCategoryTrackingView.id + '~'
					};
				};
			};
		};
	};
	
	if( Ext.get('chkWithTglTracking').dom.checked === true )
	{
			KataKunci += '@^@' + Ext.get('dtpTglAwalFilterTracking').getValue() + '&&'+ Ext.get('dtpTglAkhirFilterTracking').getValue();
	};
        
    if (KataKunci != undefined) 
    {  
		dsTrackingList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountTracking, 
					Sort: 'req_id', 
					Sortdir: 'ASC', 
					target:'ViewTracking',
					param : KataKunci
				}			
			}
		);   
    }
	else
	{
		RefreshDataTracking();
	};
    
	return dsTrackingList;
};


function ShowPesanWarningTracking(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorTracking(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoTracking(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function mComboCategoryTrackingView() 
{
	var Field = ['CATEGORY_ID','CATEGORY_NAME'];
	var dsCategoryTrackingView = new WebApp.DataStore({ fields: Field });
	
	Ext.TreeComboTrackingView = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treeTrackingView = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreeTrackingView  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						id:IdCategoryTrackingView,
						children: StrTreeComboTrackingView,
						autoScroll: true
					}
				) 
				treeTrackingView.setRootNode(rootTreeTrackingView); 
				
				this.list = treeTrackingView
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboTrackingView.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treeTrackingView
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				if (node.attributes.id === IdCategoryTrackingView)
				{
					this.setRawValue(valueCategoryTrackingView);
				}
				else
				{
					this.setRawValue(node.attributes.text);
				};
				
				selectCategoryTrackingView = node.attributes;
				
				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategoryTrackingView
		}
	);
	
	
	
	var cboCategoryTrackingView = new Ext.TreeComboTrackingView
	(
		{
			id:'cboCategoryTrackingView',
			fieldLabel: nmCategoryTracking + ' ',
			width:120,
			value:valueCategoryTrackingView
		}
	);
	
	
	return cboCategoryTrackingView;
 
};


function GetStrTreeComboTrackingView()
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetDataTreeComboSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeComboTrackingView= cst.arr;
			},
			failure:function()
			{
			    
			}
		}
	);
};







