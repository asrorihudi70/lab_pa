var dsSetWarningLogMetMonList;
var SetWarningLogMetMonLookUps;
var cellSelectMeteringSetWarningLogMetMon;

CurrentPage.page = SetWarningLogMetMonLookUp(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function SetWarningLogMetMonLookUp(mod_id) 
{

	var lebar=600;
    SetWarningLogMetMonLookUps = new Ext.Window   	
    (
		{
		    id: mod_id,
		    title: nmTitleFormWarningLogMet,
		    closable: true,
		    closeAction: 'destroy',
		    width: lebar,
		    height: 300,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupWarningLogMetMon',
		    modal: true,
		    items: 
			[	
				getFormEntrySetWarningLogMetMon(lebar)
			],
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					RefreshDataSetWarningLogMetMon();
				}
            }
		}
	);

    SetWarningLogMetMonLookUps.show();
	RefreshDataSetWarningLogMetMon();
	RefreshDataSetWarningLogMetMon();
};


function getFormEntrySetWarningLogMetMon(lebar) 
{
    var pnlSetWarningLogMetMon = new Ext.FormPanel
    (
		{
		    id: 'PanelSetWarningLogMetMon',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 235,//365,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 10px 10px 10px',
		    iconCls: 'SetupWarningLogMetMon',
		    border: false,
		    items:
			[GetDTLMeteringSetMateringAsset()]
		}
	); 
	
	var FormTRSetWarningLogMetMon = new Ext.Panel
	(
		{
		    id: 'FormTRSetWarningLogMetMon',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[
				pnlSetWarningLogMetMon,
				{
					layout: 'hBox',
					width:470,
					border: false,
					bodyStyle: 'padding:0px 0px 0px 0px',
					defaults: { margins: '3 3 3 3' },
					anchor: '98.9%',
					layoutConfig: 
					{
						align: 'middle',
						pack:'end'
					},
					items:
					[
						{
							xtype:'button',
							text:nmBtnOK,
							width:70,
							style:{'margin-left':'0px','margin-top':'0px'},
							hideLabel:true,
							id: 'btnOkLookupWarningLogMetMon',
							handler:function()
							{
								SetWarningLogMetMonLookUps.close();
							}
						}
					]
				}
			]
		}
	);

    return FormTRSetWarningLogMetMon;
};

function GetDTLMeteringSetMateringAsset() 
{
    var fldDetail = ['CURRENT_METER','ASSET_MAINT_ID','ASSET_MAINT_NAME','EMP_ID','EMP_NAME','TGL_TERAKHIR'];
	
    dsSetWarningLogMetMonList = new WebApp.DataStore({ fields: fldDetail })

    var gridMeteringSetMateringAsset = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsSetWarningLogMetMonList,
		    border: true,
		    columnLines: true,
		    frame: false,
			width:566,
			height:220,
			autoScroll:true,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
					        cellSelectMeteringSetWarningLogMetMon = dsSetWarningLogMetMonList.getAt(row);
					    }
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					cellSelectMeteringSetWarningLogMetMon = dsSetWarningLogMetMonList.getAt(ridx);
					InputLogMeteringLookUp(cellSelectMeteringSetWarningLogMetMon.data.ASSET_MAINT_ID,cellSelectMeteringSetWarningLogMetMon.data.ASSET_MAINT_NAME); 
				}
			},
		    cm: MeteringSetWarningLogMetMonColumModel(),
			 viewConfig: { forceFit: true }
		}
	);

    return gridMeteringSetMateringAsset;
};

function MeteringSetWarningLogMetMonColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colAsetIDSetWarningLogMetMon',
				header: nmColKdAsetWarningLogMet,
				dataIndex: 'ASSET_MAINT_ID',
				width:150
			},
			{
				id: 'colAsetNameSetWarningLogMetMon',
				header: nmColNamaAsetWarningLogMet,
				dataIndex: 'ASSET_MAINT_NAME',
				width:200
			},
			{
				id: 'colCurrentSetWarningLogMetMon',
				header: nmColCurrentWarningLogMet,
				dataIndex: 'CURRENT_METER',
				width:100
			},
			{
			   id:'colLastWarningLogMetMonering',
			   header: nmColLastDateWarningLogMet,
			   dataIndex: 'TGL_TERAKHIR',
			   width: 120,
			   renderer: function(v, params, record) 
				{
					return ShowDate(record.data.TGL_TERAKHIR);
				}
			},
			{
				id: 'colEmpWarningLogMetMonering',
				header: nmColEmpWarningLogMet,
				dataIndex: 'EMP_NAME',
				width:120
			}
		]
	)
};


function RefreshDataSetWarningLogMetMon()
{	
	dsSetWarningLogMetMonList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'TGL_TERAKHIR',
                                Sort: 'tgl_terakhir',
				Sortdir: 'DESC', 
				target:'ViewWarningLogMetering',
				param: ''
			} 
		}
	);

	return dsSetWarningLogMetMonList;
};


 


