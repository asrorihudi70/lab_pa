var CurrentTRPlan =
{
    data: Object,
    details: Array,
    row: 0
};

var mRecordPlan = Ext.data.Record.create
(
        [
           {name: 'KD_ASSET_MAIN', mapping:'KD_ASSET_MAIN'},
           {name: 'ASSET_MAIN_NAME', mapping:'ASSET_MAIN_NAME'},
           {name: 'KD_LOCATION', mapping:'KD_LOCATION'},
           {name: 'LOCATION', mapping:'LOCATION'},
           {name: 'PROBLEM', mapping:'PROBLEM'},
           {name: 'FINISH_DATE', mapping:'FINISH_DATE'},
           {name: 'STATUS_REQ_PM', mapping:'STATUS_REQ_PM'},
           {name: 'DESCRIPTION_REQ', mapping:'DESCRIPTION_REQ'},
           {name: 'LINE', mapping:'LINE'}
        ]
);

var selectDepartPMPlanEntry;
var dsDepartPMPlanEntry;
var dsTRPlanList;
var dsDTLTRPlanList;
var AddNewPlan = true;
var selectCountPlan = 50;
var now = new Date();
var selectPMPlan;
var rowSelectedPlan;
var cellSelectedPlanDet;
var TRPlanLookUps;



CurrentPage.page = getPanelPlan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelPlan(mod_id) 
{
    var Field = ['KD_Plan_PM','KD_EMPLOYEE','KD_DEPARTMENT','Plan_DATE','PROBLEM','FINISH_DATE','DEPARTMENT','EMPLOYEE_NAME','IMPACT','KD_ASSET_MAIN','ASSET_MAIN_NAME','KD_LOCATION','LOCATION'];
	
    dsTRPlanList = new WebApp.DataStore({ fields: Field });

    var grListTRPlan = new Ext.grid.EditorGridPanel
	(
		{
		    stripeRows: true,
		    store: dsTRPlanList,
		    anchor: '100% 100%',
		    columnLines: true,
		    border: false,
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
					        rowSelectedPlan = dsTRPlanList.getAt(row);
					    }
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'colTglPlanViewPlan',
					    header: 'Plan Date',
					    dataIndex: 'Plan_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.Plan_DATE);
					    }
					},
					{
					    header: 'Planer',
					    width: 100,
					    sortable: true,
					    dataIndex: 'EMPLOYEE_NAME',
					    id: 'colPlanerViewPlan'
					},
					{
					    id: 'colDeptViewPlan',
					    header: 'Department',
					    dataIndex: 'DEPARTMENT',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'colKdAssetMaintViewPlan',
					    header: 'Asset Maint. Code',
					    dataIndex: 'KD_ASSET_MAIN',
					    sortable: true,
					    width: 130
					},
					{
					    id: 'colAssetMaintViewPlan',
					    header: 'Asset Maintenance',
					    dataIndex: 'ASSET_MAIN_NAME',
					    sortable: true,
					    width: 130
					},
					{
					    id: 'colLocationViewPlan',
					    header: 'Location',
					    dataIndex: 'LOCATION',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'colProblemViewPlan',
					    header: "Problem ",
					    dataIndex: 'PROBLEM',
					    width: 300
					},
					{
					    id: 'colImpactViewPlan',
					    header: "Impact",
					    dataIndex: 'IMPACT',
					    width: 100
					},
					{
					    id: 'colTglSelesaiViewPlan',
					    header: "Target Date",
					    dataIndex: 'FINISH_DATE',
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.FINISH_DATE);
					    }
					}
				]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsTRPlanList,
            pageSize: selectCountPlan,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditPlan',
				    text: 'Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedPlan != undefined) 
						{
							PlanLookUp(rowSelectedPlan.data);
						}
						else 
						{
							PlanLookUp();
						}
				    }
				}, ' ', '-',
				{
					xtype: 'checkbox',
					id: 'chkWithTglPlan',
					hideLabel:true,
					checked: false,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilterPlan').dom.disabled=false;
							Ext.get('dtpTglAkhirFilterPlan').dom.disabled=false;
							
						}
						else
						{
							Ext.get('dtpTglAwalFilterPlan').dom.disabled=true;
							Ext.get('dtpTglAkhirFilterPlan').dom.disabled=true;
							
							
						};
					}
				}
				, ' ', '-', 'Plan Date : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: 'Date From : ',
				    id: 'dtpTglAwalFilterPlan',
				    format: 'd/M/Y',
				    value: now,
				    width: 100,
				    onInit: function() { }
				}, ' ', ' to ', ' ', {
				    xtype: 'datefield',
				    fieldLabel: 's/d',
				    id: 'dtpTglAkhirFilterPlan',
				    format: 'd/M/Y',
				    value: now,
				    width: 100
				}
			]
		}
	);

    var FormTRPlan = new Ext.Panel
	(
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Manage Complain',
		    border: false,
		    shadhow: true,
		    iconCls: 'Plan',
		    margins: '0 5 5 0',
		    items: [grListTRPlan],
		    tbar:
			[
				'Asset Maintenance : ', ' ',
				{
				    xtype: 'textfield',
				    fieldLabel: 'Asset Maintenance : ',
				    id: 'txtStatusPlanFilter',
				    width: 200,
				    onInit: function() { }
				},' ', 
				{
				    text: 'Refresh',
				    tooltip: 'Choose Transaction',
				    iconCls: 'refresh',
				    anchor: '25%',
				    handler: function(sm, row, rec) 
					{
				        RefreshDataPlanFilter(false);
				    }
				}
			],
		    listeners:
			{
			    'afterrender': function() 
				{
			    }
			}
		}
	);
    RefreshDataPlanFilter(true);
    RefreshDataPlanFilter(true);
	
    return FormTRPlan

};


function PlanLookUp(rowdata) 
{
    var lebar = 735;
    TRPlanLookUps = new Ext.Window
	(
		{
		    id: 'gridPlan',
		    title: 'Manage Complain',
		    closeAction: 'destroy',
		    width: lebar,
		    height: 500,
		    border: false,
			resizable:false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'Plan',
		    modal: true,
		    items: getFormEntryTRPlan(lebar),
		    listeners:
			{
			    activate: function() 
				{
			    },
			    afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedPlan=undefined;
					RefreshDataPlanFilter(true);
				}
			}
		}
	);

    TRPlanLookUps.show();
    if (rowdata == undefined) 
	{
        PlanAddNew();
    }
    else 
	{
		CekSPTB(rowdata.No_Kas_Keluar);
        TRPlanInit(rowdata)
    }

};

function getFormEntryTRPlan(lebar) 
{
    var pnlTRPlan = new Ext.FormPanel
	(
		{
		    id: 'PanelTRPlan',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    bodyStyle: 'padding:10px 10px 10px 10px',
			height:146,
		    anchor: '100%',
		    width: lebar,
		    border: false,
		    items: [getItemPanelInputPlan(lebar)],
		    tbar:
			[
				{
				    text: 'New',
				    id: 'btnTambahPlan',
				    tooltip: 'New Record',
				    iconCls: 'add',
				    handler: function() 
					{
				        PlanAddNew();
				    }
				}, '-',
				{
				    text: 'Save',
				    id: 'btnSimpanPlan',
				    tooltip: 'Save Data ',
				    iconCls: 'save',
				    handler: function() 
					{
				        PlanSave(false);
				        RefreshDataPlanFilter(false);
				    }
				}, '-',
				{
				    text: 'Save & Close',
				    id: 'btnSimpanKeluarPlan',
				    tooltip: 'Save Data & Close ',
				    iconCls: 'saveexit',
				    handler: function() 
					{
						var x =  PlanSave(true);
				         RefreshDataPlanFilter(false);
						if (x===undefined)
						{
							TRPlanLookUps.close();
						};    
				    }
				}, '-',
				{
				    text: 'Remove',
				    id: 'btnHapusPlan',
				    tooltip: 'Remove the selected item',
				    iconCls: 'remove',
				    handler: function() 
					{
				        PlanDelete();
				        RefreshDataPlanFilter(false);
				    }
				}, '-',
				{
				    text: 'Lookup',
				    id: 'btnLookupPlan',
				    tooltip: 'Lookup Account',
				    iconCls: 'find',
				    handler: function() 
					{
						// var criteria;
				        // criteria = ' WHERE Kd_Unit_Kerja =~' + selectCMPlan + '~';
				        // FormLookupCashIn(criteria);
				    }
				}, 
				'-', '->', '-',
				{
				    text: 'Print',
					id:'btnCetakPlan',
				    tooltip: 'Print',
				    iconCls: 'print',
				    handler: function()
					{ CetakPlan() }
				}
			]
		}
	); 

 var GDtabDetailPlan = new Ext.TabPanel   
	(
		{
			id:'GDtabDetailPlan',
	        region: 'center',
	        activeTab: 0,
	        anchor: '100% 69%',
			border:false,
	        plain: true,
	        defaults: 
			{
		        autoScroll: true
	        },
			items: GetDTLTRPlanGrid(),
	        tbar: 
			[
				{
					id:'btnTambahBrsPlan',
					text: 'Add Row',
					tooltip: 'Tambah Baris Baru ',
					iconCls: 'AddRow',
					handler: function() 
					{ 
						TambahBarisPlan();
					}
				},
				'-',
				{
					id:'btnHpsBrsPlan',
					text: 'Remove Row',
					tooltip: 'Hapus Baris',
					iconCls: 'RemoveRow',
					handler: function()
					{
						if (dsDTLTRPlanList.getCount() > 0 )
						{
							if (cellSelectedPlanDet != undefined)
							{
								if(CurrentTRPlan != undefined)
								{
									HapusBarisPlan();
								}
							}
							else
							{
								ShowPesanWarningPlan('Silahkan pilih dahulu baris yang akan dihapus','Hapus Baris');
							}
						}
					}
				},' ','-'
			] 
		}
	);
	
   
    var FormTRPlan = new Ext.Panel
	(
		{
		    id: 'FormTRPlan',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRPlan,GDtabDetailPlan]

		}
	);

    return FormTRPlan
};
///---------------------------------------------------------------------------------------///

function CetakPlan() 
{
  
};


function TambahBarisPlan()
{
	var x=true;
	
		if (x === true)
		{
			var p = RecordBaruPlan();
			dsDTLTRPlanList.insert(dsDTLTRPlanList.getCount(), p);
		};
};

function HapusBarisPlan()
{
	if (cellSelectedPlanDet.data.MAK != '')
	{
		Ext.Msg.show
		(
			{
			   title:'Hapus Baris',
			   msg: 'Apakah baris ini akan dihapus ?' + ' ' + 'Baris :'+ ' ' + (CurrentTRPlan.row + 1) + ' dengan MAK : '+ ' ' + cellSelectedPlanDet.data.MAK + ' dan Account : ' + cellSelectedPlanDet.data.Account ,
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
						if(dsDTLTRPlanList.data.items[CurrentTRPlan.row].data.Urut_Kas_Keluar === '')
						{
							
							dsDTLTRPlanList.removeAt(CurrentTRPlan.row);
						}
						else
						{
							Ext.Msg.show
							(
								{
								   title:'Hapus Baris',
								   msg: 'Apakah anda yakin baris ini akan dihapus ?, Data ini telah tersimpan ke database' ,
								   buttons: Ext.MessageBox.YESNO,
								   fn: function (btn) 
								   {			
										if (btn =='yes') 
										{
											PlanDeleteDetail();
										};
									}
								}
							)
						};	
					}; 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
	else
	{
		dsDTLTRPlanList.removeAt(CurrentTRPlan.row);
	};
};

function PlanDeleteDetail()
{
	
};

function getParamPlanDeleteDetail()
{
	 var params =
	{
	
	};
	
    return params
};

function GetDTLTRPlanGrid() 
{
    var fldDetail = ['KD_Plan_CM','LINE','KD_ASSET_MAIN','PROBLEM','FINISH_DATE','DESCRIPTION_REQ','IMPACT','STATUS_REQ_CM','LOCATION','KD_LOCATION','ASSET_MAIN_NAME'];
	
    dsDTLTRPlanList = new WebApp.DataStore({ fields: fldDetail })

    var gridDTLTRPlan = new Ext.grid.EditorGridPanel
	(
		{
		    title: 'Detail Manage Complain',
		    stripeRows: true,
		    store: dsDTLTRPlanList,
		    border: false,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			autoScroll:true,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
					        cellSelectedPlanDet = dsDTLTRPlanList.getAt(row);
					        CurrentTRPlan.row = row;
					        CurrentTRPlan.data = cellSelectedPlanDet;
					    }
					}
				}
			),
		    cm: TRPlanDetailColumModel()
			//, viewConfig:{forceFit: true}
		}
	);

    return gridDTLTRPlan;
};

function TRPlanDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colKdAssetPlan',
				header: "Asset Maint. Code",
				dataIndex: 'KD_ASSET_MAIN',
				width:100,
				editor: new Ext.form.TextField
				(
					{
						id:'fieldKdAssetPlan',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var str='';
									if(Ext.get('fieldKdAssetPlan').dom.value != '')
									{
										//str=' and KD_ASSET_MAIN like ~%' + Ext.get('fieldKdAssetPlan').dom.value + '%~';
                                                                                str=' and kd_asset_main like ~%' + Ext.get('fieldKdAssetPlan').dom.value + '%~';
									};
									GetLookupAssetPlan(str)
								};
							}
						}
					}
				)
			},
			{
				id: 'colAsetNamePlan',
				header: "Asset Maintenance",
				dataIndex: 'ASSET_MAIN_NAME',
				sortable: false,
				editor: new Ext.form.TextField
				(
					{
						id:'fieldAsetNamePlan',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var str='';
									if(Ext.get('fieldAsetNamePlan').dom.value != '')
									{
										//str=' and ASSET_MAIN_NAME like ~%' + Ext.get('fieldAsetNamePlan').dom.value + '%~';
                                                                                str=' and asset_main_name like ~%' + Ext.get('fieldAsetNamePlan').dom.value + '%~';
									};
									
								};
							}
						}
					}
				),
				width: 200,
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					return str;
                }
			}, 
			{
				id: 'colLocationPlan',
				header: "Location",
				dataIndex: 'LOCATION',
				width:100
			}, 
			{
				id: 'colProblemPlan',
				header: "Problem",
				width:200,
				dataIndex: 'PROBLEM', 
				editor: new Ext.form.TextField
				(
					{
						id:'fieldcolProblemPlan',
						allowBlank: true,
						enableKeyEvents : true,
						width:30
					} 
				),
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					return str;
                }
			}, 
			{
				id: 'colDeskripsiPlan',
				header: "Description",
				width:200,
				dataIndex: 'DESCRIPTION', 
				editor: new Ext.form.TextField
				(
					{
						id:'fieldcolDeskripsiPlan',
						allowBlank: true,
						enableKeyEvents : true,
						width:90
					}
				),
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					return str;
                }
			},
			{
				id: 'colTglSelesaiPlan',
				header: "Target Date",
				width:150,
				dataIndex: 'FINISH_DATE', 
				editor: new Ext.form.DateField
				(
					{
						id:'fieldcolTglSelesaiPlan',
						allowBlank: true,
						enableKeyEvents : true,
						width:90
					}
				)
			},	
			{
				id: 'colStatusPlan',
				header: "Status ",
				dataIndex: 'STATUS_REQ_PM',
				width:70
			}
		]
	)
};


function RecordBaruPlan()
{
	var p = new mRecordPlan
	(
		{
			'MAK':'',
			'KD_ASSET_MAIN':'',
		   'ASSET_MAIN_NAME':'', 
		   'KD_LOCATION':'', 
		   'LOCATION':'',
		   'PROBLEM':'',
		   'FINISH_DATE':'',
		   'STATUS_REQ_PM':'', 
		   'DESCRIPTION_REQ':'',
		   'LINE':''
		}
	);
	
	return p;
};

function TRPlanInit(rowdata)
{
    AddNewPlan = false;
   
	
};


function RefreshDataPlanDetail(no_kas_keluar) 
{
    var strKriteriaPlan='';
    //strKriteriaPlan = 'where no_kas_keluar = ~' + no_kas_keluar + '~'
    strKriteriaPlan = 'no_kas_keluar = ~' + no_kas_keluar + '~'
   
    dsDTLTRPlanList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'urut_kas_keluar',
			    Sortdir: 'ASC',
			    target: 'PlanDetail',
			    param: strKriteriaPlan
			}
		}
	);
    return dsDTLTRPlanList;
};





///---------------------------------------------------------------------------------------///
function PlanAddNew() 
{
    AddNewPlan = true;
	rowSelectedPlan=undefined;
	dsDTLTRPlanList.removeAll();

};
///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamPlan() 
{
    var params =
	{
		Table:'CashOut'
	};
	
    return params
};

function GetListCountPlanDetail()
{
	var x=0;
	for(var i = 0 ; i < dsDTLTRPlanList.getCount();i++)
	{
		
	}
	return x;
};

function getArrDetailPlan()
{
	var x='';
	for(var i = 0 ; i < dsDTLTRPlanList.getCount();i++)
	{
		
	}	
	
	return x;
};


function getItemPanelInputPlan(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:98,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoPlan(lebar),getItemPanelNoPlan2(lebar),mComboDepartPMPlanEntry()  				
				]
			}
		]
	};
    return items;
};



function getItemPanelNoPlan(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .45,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'No. Plan ',
					    name: 'txtNoPlan',
					    id: 'txtNoPlan',
						emptyText:'Automatically from the system ...',
						readOnly:true,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .55,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Plan Date ',
					    id: 'dtpTanggalPlan',
					    name: 'dtpTanggalPlan',
					    format: 'd/M/Y',
					    value: now,
					    anchor: '70%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoPlan2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 45,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoPlan3() 
				]
			}
		]
	}
    return items;
};

function getItemPanelNoPlan3() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:100,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Planer ',
					    name: 'txtKdPlanerPlan',
					    id: 'txtKdPlanerPlan',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .7,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
					    name: 'txtPlanerPlan',
					    id: 'txtPlanerPlan',
					    anchor: '99.99%',
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var criteria='';
									FormLookupEmployee(criteria);
								};
							}
						}
					}
				]
			}
		]
	}
    return items;
};

function RefreshDataPlan() {
    dsTRPlanList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: selectCountPlan,
			    //Sort: 'No_Kas_Keluar',
                            Sort: 'no_kas_keluar',
			    Sortdir: 'ASC',
			    target: 'ViewPlan',
			    param: ''
			}
		}
	);
    return dsTRPlanList;
};

function mComboMaksDataPlan() 
{
    var cboMaksDataPlan = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataPlan',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: 'Max.Data ',
		    width: 50,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountPlan,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountPlan = b.data.displayText;
			        RefreshDataPlanFilter(false);
			    }
			}
		}
	);
    return cboMaksDataPlan;
};

function RefreshDataPlanFilter(mBol) 
{
    var strPlan='';
    
};



function PlanSave(IsExit) 
{	
	
};


function ShowPesanWarningPlan(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorPlan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoPlan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO
		}
	);
};


function PlanDelete() 
{
   
};


function mComboDepartPMPlanEntry() 
{
    var Field = ['KD_DEPARTMENT','DEPARTMENT'];
	
    dsDepartPMPlanEntry = new WebApp.DataStore({ fields: Field });
    dsDepartPMPlanEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'KD_DEPARTMENT',
                            Sort: 'kd_department',
			    Sortdir: 'ASC',
			    target: 'cboDepartPlanEntry',
			    param: ''
			}
		}
	);

    var cboDepartPMPlanEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDepartPMPlanEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Choose Department...',
		    fieldLabel: 'Department ',
		    align: 'Right',
		    anchor:'60%',
		    store: dsDepartPMPlanEntry,
		    valueField: 'KD_DEPARTMENT',
		    displayField: 'DEPARTMENT',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectDepartPMPlanEntry = b.data.KD_DEPARTMENT;
			    }
			}
		}
	);

    return cboDepartPMPlanEntry;
};







