var CurrentServicePMResult=
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPersonIntPMResult=
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPersonExtPMResult=
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPartPMResult=
{
    data: Object,
    details: Array,
    row: 0
};



// var select & value combo ==============================
var selectStatusPMResultEntry;
var selectCategoryPMResultView=' 9999';
var valueCategoryPMResultView=' All';
var IdCategoryPMResultView=' 9999';
var selectStatusPMResultView=' 9999';
var valueStatusPMResultView=' All';
var valueStatusPMResultView2='(\' 9999\',\'5\',\'3\')';
var DefaultStatusWOPMResultView='5';
var DefaultStatusResultPMResultView='3';
//========================================================

// var select grid ==============================
var rowSelectedPMResult;
var rowSelectedPMResultTemp;
var selectCountPMResult = 50;
var cellSelectServicePMResult;
var cellSelectPersonIntPMResult;
var cellSelectPersonExtPMResult;
var cellSelectPartPMResult;
//================================================


// var data store ===========================
var dsTRPMResultList;
var dsDtlCraftPersonIntPMResult;
var dsDtlCraftPersonIntPMResultTemp;
var dsDtlCraftPersonExtPMResult;
var dsDtlCraftPersonExtPMResultTemp;
var dsDtlPartPMResult;
var dsDtlPartPMResultTemp;
var dsDtlServicePMResult;
var dsStatusPMResultView;
var dsLookVendorListPMResult;

var dsDtlCraftPersonIntPMResultTempView;
var dsDtlCraftPersonExtPMResultTempView;

//===========================================



//var component===========================
var TRPMResultLookUps;
var vTabPanelRepairPMResult ;
var vTabPanelRepairExternalPMResult ;
//=========================================



var NowPMResult = new Date();
var AddNewPMResult = true;
var IdWOPMResult;
var IdPMResult;
var IsExtRepairPMResult;
var FocusCtrlPMResult;
var varTabRepairPMResult=1;
var varCatIdPMResult;
var SchIdPMResult;
var StrTreeComboPMResultView;
var rootTreePMResultView;
var treePMResultView;


var mRecordServicePMResult = Ext.data.Record.create
	(
		[
		   {name: 'RESULT_PM_ID', mapping:'RESULT_PM_ID'},	
		   {name: 'SCH_PM_ID', mapping:'SCH_PM_ID'},
		   {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
		   {name: 'SERVICE_NAME', mapping:'SERVICE_NAME'},
		   {name: 'CATEGORY_ID', mapping:'CATEGORY_ID'},
		   {name: 'TAG', mapping:'TAG'}
		]
	)
	
	var mRecordPartPMResult = Ext.data.Record.create
	(
		[
		   {name: 'RESULT_PM_ID', mapping:'RESULT_PM_ID'},		
		   {name: 'SCH_PM_ID', mapping:'SCH_PM_ID'},
		   {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
		   {name: 'PART_ID', mapping:'PART_ID'},
		   {name: 'PART_NAME', mapping:'PART_NAME'},
		   {name: 'QTY', mapping:'QTY'},
		   {name: 'UNIT_COST', mapping:'UNIT_COST'},
		   {name: 'TOT_COST', mapping:'TOT_COST'},
		   {name: 'DESC_SCH_PART', mapping:'DESC_SCH_PART'},
		   {name: 'TAG', mapping:'TAG'},
		   {name: 'ROW', mapping:'ROW'}
		]
	);
	
var mRecordPersonPMResult = Ext.data.Record.create
	(
		[
		   {name: 'RESULT_PM_ID', mapping:'RESULT_PM_ID'},	
		   {name: 'SCH_PM_ID', mapping:'SCH_PM_ID'},
		   {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
		   {name: 'ROW_SCH', mapping:'ROW_SCH'},
		   {name: 'EMP_ID', mapping:'EMP_ID'},
		   {name: 'EMP_NAME', mapping:'EMP_NAME'},
		   {name: 'VENDOR_ID', mapping:'VENDOR_ID'},
		   {name: 'VENDOR', mapping:'VENDOR'},
		   {name: 'PERSON_NAME', mapping:'PERSON_NAME'},
		   {name: 'COST', mapping:'COST'},
		   {name: 'REAL_COST', mapping:'REAL_COST'},
		   {name: 'DESC_SCH_PERSON', mapping:'DESC_SCH_PERSON'},
		   {name: 'ROW', mapping:'ROW'},
		   {name: 'ROW_RSLT', mapping:'ROW_RSLT'}
		]
	);
	
	var mRecVendorPMResult = Ext.data.Record.create
	(
		[
		   {name: 'KD', mapping:'KD'},
		   {name: 'NAME', mapping:'NAME'}
		]
	);


CurrentPage.page = getPanelPMResult(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelPMResult(mod_id) 
{
    var Field =['RESULT_PM_ID','WO_PM_ID_RESULT','FINISH_DATE','DIFF_FINISH_DATE','LAST_COST',
'DIFF_COST','DESC_RESULT_PM','WO_PM_ID','EMP_ID','WO_PM_DATE','WO_PM_FINISH_DATE',
'DESC_WO_PM','STATUS_ID','DUE_DATE','METER','DESC_SCH_PM','ASSET_MAINT_ID','YEARS',
'SERVICE_NAME','ASSET_MAINT_NAME','ASSET_MAINT','LOCATION_ID','DEPT_ID_ASET','EMP_NAME',
'SERVICE_ID','CATEGORY_ID','SCH_PM_ID','ROW_SCH','LOCATION','REFERENCE'];
    dsTRPMResultList = new WebApp.DataStore({ fields: Field });
	
	 var chkInternalPMResult = new Ext.grid.CheckColumn
	(
		{
			id: 'chkInternalPMResult',
			header: nmRepairExtResultPM,
			align: 'center',
			disabled:true,
			dataIndex: 'IS_EXT_REPAIR',
			width: 70
		}
	);

    var grListTRPMResult = new Ext.grid.EditorGridPanel
	(
		{
		    stripeRows: true,
		    store: dsTRPMResultList,
		    anchor: '100% 91.9999%',
		    columnLines: true,
			autoScroll:true,
		    border: false,
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
					        rowSelectedPMResult = dsTRPMResultList.getAt(row);
							var x=rowSelectedPMResult.data.WO_PM_ID;
							if ( x != null && x !='' && x != undefined)
							{
								var y=rowSelectedPMResult.data;
								var strEmp='';
								var strVend='';
//								var str = 'where category_id =~' + y.CATEGORY_ID + '~ and sch_pm_id=~'  + y.SCH_PM_ID + '~'
//			str += ' and service_id =~' + y.SERVICE_ID + '~ and row_sch =' + y.ROW_SCH
//			str += ' and wo_pm_id =~' + x + '~'
								var str = 'category_id = ~' + y.CATEGORY_ID + '~ and sch_pm_id= ~'  + y.SCH_PM_ID + '~'
			str += ' and service_id = ~' + y.SERVICE_ID + '~ and row_sch = ' + y.ROW_SCH
			str += ' and wo_pm_id = ~' + x + '~'
			
								strEmp = str + ' and emp_id is not null and emp_id != ~ ~'
								strVend = str + ' and vendor_id is not null and vendor_id != ~ ~'
								LoadDataEmployeePMResultTempView(strEmp);
								LoadDataVendorPMResultTempView(strVend);
							};
					    }
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedPMResult = dsTRPMResultList.getAt(ridx);
					var x=rowSelectedPMResult.data.WO_PM_ID;
					if ( x != null && x !='' && x != undefined)
					{
						var y=rowSelectedPMResult.data;
						var strEmp='';
						var strVend='';
//						var str = 'where category_id =~' + y.CATEGORY_ID + '~ and sch_pm_id=~'  + y.SCH_PM_ID + '~'
//	str += ' and service_id =~' + y.SERVICE_ID + '~ and row_sch =' + y.ROW_SCH
//	str += ' and wo_pm_id =~' + x + '~'
						var str = 'category_id = ~' + y.CATEGORY_ID + '~ and sch_pm_id = ~'  + y.SCH_PM_ID + '~'
	str += ' and service_id = ~' + y.SERVICE_ID + '~ and row_sch = ' + y.ROW_SCH
	str += ' and wo_pm_id = ~' + x + '~'
						strEmp = str + ' and emp_id is not null and emp_id != ~ ~'
						strVend = str + ' and vendor_id is not null and vendor_id != ~ ~'
						LoadDataEmployeePMResultTempView(strEmp);
						LoadDataVendorPMResultTempView(strVend);
					};
							
					if (rowSelectedPMResult != undefined) 
					{
						PMResultLookUp(rowSelectedPMResult.data);
					}
					else 
					{
						ShowPesanWarningPMResult(nmWarningSelectEditData,nmEditData)
					};
				}
			},
		    cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'colWOImagePMResultView',
					    header: nmColResultPM,
					    dataIndex: 'RESULT_PM_ID',
					    sortable: true,
					    width: 50,
						align:'center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store) 
						{
							if ( value === null )
							{
								metaData.css = 'BeforeResult';
							}
							else
							{
								metaData.css = 'Result';
							};
								return '';     
						}
					},
					{
					    id: 'colResultIdViewPMResult', 
					    header: nmColResultIDPM,
					    dataIndex: 'RESULT_PM_ID',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'colWOIdViewPMResult', 
					    header: nmColWOIDResultPM,
					    dataIndex: 'WO_PM_ID',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'colWODateViewPMResult',
					    header: nmColWODateResultPM,
					    dataIndex: 'WO_PM_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.WO_PM_DATE);
					    }
					},
					{
					    id: 'colWODateViewPMResult', //+ Code
					    header: nmColAssetIdNameResultPM,
					    dataIndex: 'ASSET_MAINT',
					    sortable: true,
					    width: 250
					},
					{
					    id: 'colStartDateViewPMResult',
					    header: nmColSchStartDateResultPM,
					    dataIndex: 'SCH_DUE_DATE',
					    sortable: true,
					    width: 140,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.SCH_DUE_DATE);
					    }
					},
					{
					    id: 'colFinishResultDateViewPMResult',
					    header: nmColRsltFinishDateResultPM,
					    dataIndex: 'FINISH_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.FINISH_DATE);
					    }
					},
					{
					    id: 'colDescViewPMResult',
					    header: nmColRsltDescResultPM,
					    dataIndex: 'DESC_RESULT_PM',
					    width: 300
					}//,chkInternalPMResult
				]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsTRPMResultList,
            pageSize: selectCountPMResult,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditPMResult',
				    text: nmEditData,
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedPMResult != undefined) 
						{
							PMResultLookUp(rowSelectedPMResult.data);
						}
						else 
						{
							ShowPesanWarningPMResult(nmWarningSelectEditData,nmEditData)
						}
				    }
				}, ' ', '-',
				{
					xtype: 'checkbox',
					id: 'chkWithTglPMResult',
					hideLabel:true,
					checked: false,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilterPMResult').dom.disabled=false;
							Ext.get('dtpTglAkhirFilterPMResult').dom.disabled=false;
							
						}
						else
						{
							Ext.get('dtpTglAwalFilterPMResult').dom.disabled=true;
							Ext.get('dtpTglAkhirFilterPMResult').dom.disabled=true;
							
							
						};
					}
				}
				, ' ', '-', nmWODateResultPM + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmWODateResultPM + ' ',
				    id: 'dtpTglAwalFilterPMResult',
				    format: 'd/M/Y',
				    value: NowPMResult,
				    width: 100,
				    onInit: function() { }
				}, ' ', nmSd + ' ', ' ', {
				    xtype: 'datefield',
				    fieldLabel: nmSd + ' ',
				    id: 'dtpTglAkhirFilterPMResult',
				    format: 'd/M/Y',
				    value: NowPMResult,
				    width: 100
				}
			]
		}
	);
	
	var LegendViewPMResult = new Ext.Panel
	(
		{
		    id: 'LegendViewPMResult',
		    region: 'center',
			border:false,
			bodyStyle: 'padding:0px 7px 0px 7px',
		    layout: 'column',
			frame:true,
			//height:30,
			anchor: '100% 8.0001%',
			autoScroll:false,
			items:
			[	
				{
					columnWidth: .15,
					layout: 'form',
					//height:30,
					anchor: '100% 8.0001%',
					border: false,
					html: '<img src="' + WebAppUrl.UrlImg + '/images/icons/16x16/Before.png" class="text-desc-legend"/>' + " " + " Work Order" 
				},
				{
					columnWidth: .2,
					layout: 'form',
					border: false,
					//height:30,
					anchor: '100% 8.0001%',
					html: '<img src="' + WebAppUrl.UrlImg + '/images/icons/16x16/After.png" class="text-desc-legend"/>' + " " + " Job Close"
				}
			]
		
		}
	)

    var FormTRPMResult = new Ext.Panel
	(
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormResultPM,
		    border: false,
		    shadhow: true,
			autoScroll:false,
		    iconCls: 'PMResult',
		    margins: '0 5 5 0',
		    items: [grListTRPMResult,LegendViewPMResult],
		    tbar:
			[
				nmCatResultPM + ' : ', ' ',mComboCategoryPMResultView(), 
				' ', '-',nmStatusResultPM + ' : ', ' ',
				mComboStatusPMResultView(),
				' ', 
				{
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    anchor: '25%',
				    handler: function(sm, row, rec) 
					{
				        RefreshDataPMResultFilter();
				    }
				}
			],
		    listeners:
			{
			    'afterrender': function() 
				{
			    }
			}
		}
	);
    RefreshDataPMResult();
    RefreshDataPMResult();
	GetStrTreeComboPMResultView();
	
    return FormTRPMResult

};

function LoadDataEmployeePMResultTempView(str) 
{
	 var fldDetail = ['SCH_PM_ID','SERVICE_ID','ROW_SCH','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_WO_PM_PERSON','VENDOR','EMP_NAME','ROW','ROW_PERSON'];

    dsDtlCraftPersonIntPMResultTempView = new WebApp.DataStore({ fields: fldDetail })
    dsDtlCraftPersonIntPMResultTempView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'EMP_ID',
                            Sort: 'emp_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPersonPM',
			    param: str
			}
		}
	);
    return dsDtlCraftPersonIntPMResultTempView;
};

function LoadDataVendorPMResultTempView(str) 
{
	 var fldDetail = ['SCH_PM_ID','SERVICE_ID','ROW_SCH','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_WO_PM_PERSON','VENDOR','EMP_NAME','ROW','ROW_PERSON'];

    dsDtlCraftPersonExtPMResultTempView = new WebApp.DataStore({ fields: fldDetail })
	
    dsDtlCraftPersonExtPMResultTempView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'VENDOR_ID',
                            Sort: 'vendor_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPersonPM',
			     param: str
			}
		}
	);
    return dsDtlCraftPersonExtPMResultTempView;
};

function RefreshDataPMResult() 
{
    dsTRPMResultList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: selectCountPMResult,
			    //Sort: 'RESULT_PM_ID',
                            Sort: 'result_pm_id',
			    Sortdir: 'ASC',
			    target: 'ViewResultPM',
			    param: ''
			}
		}
	);
    return dsTRPMResultList;
};

function RefreshDataPMResultFilter() 
{
    var KataKunci='';
	
	if (Ext.get('cboCategoryPMResultView').getValue() != '' && selectCategoryPMResultView != undefined)
    { 
		if(selectCategoryPMResultView.id != undefined)
		{
			if(selectCategoryPMResultView.id != ' 9999')
			{
				if (selectCategoryPMResultView.leaf === false)
				{
					//KataKunci =' where substr(category_id,1,' + selectCategoryPMResultView.id.length + ')=~' +  selectCategoryPMResultView.id +'~'
                                        KataKunci =' substr(category_id,1,' + selectCategoryPMResultView.id.length + ') = ~' +  selectCategoryPMResultView.id +'~'
				}
				else
				{
					//KataKunci =' WHERE category_id=~'+ selectCategoryPMResultView.id + '~'
                                        KataKunci =' category_id = ~'+ selectCategoryPMResultView.id + '~'
				};
			};
		};
	};
	
	if (Ext.get('cboStatusPMResultView').getValue() != '' && selectStatusPMResultView != undefined)
    { 
		if(selectStatusPMResultView != ' 9999')
		{
			if (selectStatusPMResultView === DefaultStatusWOPMResultView)
			{
				if (KataKunci === '' )
				{
					//KataKunci = ' where result_pm_id is null';
                                        KataKunci = ' result_pm_id is null';
				}
				else
				{
                                        KataKunci += ' and result_pm_id is null';
				};
			}
			else
			{
				if (KataKunci === '' )
				{
					//KataKunci = ' where result_pm_id is not null';
                                        KataKunci = ' result_pm_id is not null';
				}
				else
				{
					KataKunci += ' and result_pm_id is not null';
				};
			};
		};
	};
	
	if( Ext.get('chkWithTglPMResult').dom.checked === true )
	{
			KataKunci += '@^@' + Ext.get('dtpTglAwalFilterPMResult').getValue() + '&&'+ Ext.get('dtpTglAkhirFilterPMResult').getValue();
	};
        
    if (KataKunci != undefined) 
    {  
		dsTRPMResultList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountPMResult,
					//Sort: 'RESULT_PM_ID',
                                        Sort: 'result_pm_id',
					Sortdir: 'ASC',
					target: 'ViewResultPM',
					param: KataKunci
				}
			}
		);
    }
	else
	{
		RefreshDataPMResult();
	};
    
	return dsTRPMResultList;
    
};



function PMResultLookUp(rowdata) 
{
    var lebar = 800; //735;
	
	IdWOPMResult='';
	IdPMResult='';
	IsExtRepairPMResult=false;
    SchIdPMResult='';
    varTabRepairPMResult=1;
	varCatIdPMResult='';
	TRPMResultLookUps='';
	cellSelectServicePMResult='';
	cellSelectPersonIntPMResult='';
	cellSelectPersonExtPMResult='';
	cellSelectPartPMResult='';
	
	
    TRPMResultLookUps = new Ext.Window
	(
		{
		    id: 'gridPMResult',
		    title: nmTitleFormResultPM,
		    closeAction: 'destroy',
		    width: lebar,
		    height: 560,//540,
		    border: false,
			resizable:false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'PMResult',
		    modal: true,
		    items: getFormEntryTRPMResult(lebar),
		    listeners:
			{
			    activate: function() 
				{
					if (varBtnOkLookupEmp === true)
					{
						var j=dsDtlCraftPersonIntPMResult.getCount()-1;
						 dsDtlCraftPersonIntPMResultTemp.insert(dsDtlCraftPersonIntPMResultTemp.getCount(),dsDtlCraftPersonIntPMResult.data.items[j]);
						 GetPersonIntServicePMResult(cellSelectServicePMResult.data.SERVICE_ID);
						 varBtnOkLookupEmp=false;
					};
					
					 // if (varBtnOkLookupVendor === true)
					// {
						// var j=dsDtlCraftPersonExtPMResult.getCount()-1;
						 // dsDtlCraftPersonExtPMResultTemp.insert(dsDtlCraftPersonExtPMResultTemp.getCount(), dsDtlCraftPersonExtPMResult.data.items[j]);
						  // GetPersonExtServicePMResult(cellSelectServicePMResult.data.SERVICE_ID);
						 // varBtnOkLookupVendor=false;
					// };
					
					 if (varBtnOkLookupPart === true)
					{
						var j=dsDtlPartPMResult.getCount()-1;
						 dsDtlPartPMResultTemp.insert(dsDtlPartPMResultTemp.getCount(), dsDtlPartPMResult.data.items[j]);
						  GetPartPMResult(cellSelectServicePMResult.data.SERVICE_ID);
						 CalcTotalPartPMResult('','',false)
						 varBtnOkLookupPart=false;
					};
			    },
			    afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedPMResult=undefined;
					RefreshDataPMResultFilter();
				}
			}
		}
	);

    TRPMResultLookUps.show();
    if (rowdata == undefined) 
	{
        //PMResultAddNew();
    }
    else 
	{
        TRPMResultInit(rowdata)
    }
};

function getFormEntryTRPMResult(lebar) 
{
    var pnlTRPMResult = new Ext.FormPanel
	(
		{
		    id: 'PanelTRPMResult',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    bodyStyle: 'padding:10px 10px 0px 10px',
			height:538,//518,
		    anchor: '100%',
		    width: lebar,
		    border: false,
			//autoScroll:true,
		    items: [getItemPanelInputPMResult(lebar)],
		    tbar:
			[
				{
				    text: nmTambah,
				    id: 'btnTambahPMResult',
				    tooltip: nmTambah,
				    iconCls: 'add',
					disabled:true,
				    handler: function() 
					{
				        //PMResultAddNew();
				    }
				}, '-',
				{
				    text: nmSimpan,
				    id: 'btnSimpanPMResult',
				    tooltip: nmSimpan,
				    iconCls: 'save',
				    handler: function() 
					{
				        PMResultSave(false);
				        RefreshDataPMResultFilter();
				    }
				}, '-',
				{
				    text: nmSimpanKeluar,
				    id: 'btnSimpanKeluarPMResult',
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{
						var x =  PMResultSave(true);
				         RefreshDataPMResultFilter();
						if (x===undefined)
						{
							TRPMResultLookUps.close();
						};    
				    }
				}, '-',
				{
				    text: nmHapus,
				    id: 'btnHapusPMResult',
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
				        PMResultDelete();
				        RefreshDataPMResultFilter();
				    }
				}, '-',
				{
				    text: nmLookup,
				    id: 'btnLookupPMResult',
				    tooltip: nmLookup,
				    iconCls: 'find',
				    handler: function() 
					{
						if (FocusCtrlPMResult === 'colVendorRdo' && varTabRepairPMResult === 2 && cellSelectServicePMResult != '' &&  cellSelectServicePMResult != undefined)
						{	
							//var p = GetRecordBaruPersonPMResult(dsDtlCraftPersonExtPMResult);
							//FormLookupVendor(p,'','',true,dsDtlCraftPersonExtPMResult,true);
							
							var criteria='';
							if (Ext.get('txtNamaVendorPMResult').dom.value != '')
							{
								//criteria = ' where VENDOR like ~%' + Ext.get('txtNamaVendorPMResult').dom.value + '%~';
                                                                criteria = ' vendor like ~%' + Ext.get('txtNamaVendorPMResult').dom.value + '%~';
							};
							FormLookupVendor(GetItemLookupVendorPMResult(),criteria);
							
						}
						else if (FocusCtrlPMResult === 'colEmpRdo' && varTabRepairPMResult === 2 && cellSelectServicePMResult != '' &&  cellSelectServicePMResult != undefined)
						{	
							// var p = GetRecordBaruPersonPMResult(dsDtlCraftPersonExtPMResult);
							// FormLookupVendor(p,'','',true,dsDtlCraftPersonExtPMResult,true);
							
							var criteria='';
							if (Ext.get('txtNamaVendorPMResult').dom.value != '')
							{
								//criteria = ' where VENDOR like ~%' + Ext.get('txtNamaVendorPMResult').dom.value + '%~';
                                                                criteria = ' vendor like ~%' + Ext.get('txtNamaVendorPMResult').dom.value + '%~';
							};
							FormLookupVendor(GetItemLookupVendorPMResult(),criteria);
							
						}
						else if (FocusCtrlPMResult === 'colEmp')
						{
							var p = GetRecordBaruPersonPMResult(dsDtlCraftPersonIntPMResult);
							FormLookupEmployee('','','',true,p,dsDtlCraftPersonIntPMResult,true);
							FocusCtrlPMResult='';
						}
						else if ((FocusCtrlPMResult === 'colPart' || varTabRepairPMResult === 3 ) && FocusCtrlPMResult != 'txtPIC')
						{
							var p = GetRecordBaruPartPMResult();
							//var	str = ' where category_id = ~' + varCatIdPMResult + '~'
                                                        var	str = ' category_id = ~' + varCatIdPMResult + '~'
							var strList=GetStrListPartPMResult(true);
							if (strList !='')
							{
								//str += ' and PART_ID not in ( ' + strList + ') '
                                                                str += ' and part_id not in ( ' + strList + ') '
							};
							FormLookupPart(str,dsDtlPartPMResult,p,true,'',true);
							FocusCtrlPMResult='';
						}
						else if ((FocusCtrlPMResult === 'colService' || varTabRepairPMResult === 1 ) && FocusCtrlPMResult != 'txtPIC')
						{
							var p = GetRecordBaruServicePMResult();
							var str = '';
							var strList = GetStrListServicePMResult();
							
							if (strList !='')
							{
								//strList = ' and SERVICE_ID not in ( ' + strList + ') '
                                                                strList = ' and service_id not in ( ' + strList + ') '
							};
							//str = ' where category_id = ~' + varCatIdPMResult + '~';
                                                        str = ' category_id = ~' + varCatIdPMResult + '~';
							str += strList;
							
							FormLookupServiceCategory('','',str,true,p,dsDtlServicePMResult,true);
							FocusCtrlPMResult='';
						};
				    }
				}, 
				'-', '->', '-',
				{
				    text: nmCetak,
					id:'btnCetakPMResult',
				    tooltip: nmCetak,
				    iconCls: 'print',
				    handler: function()
					{ 
						var cKriteria;
						cKriteria = Ext.get('txtKdMainAssetPMResult').dom.value + ' - ' + Ext.get('txtNamaMainAsetPMResult').dom.value + '###1###';
						cKriteria += Ext.get('txtNamaSupervisorPMResult').dom.value + '###2###';
						cKriteria += Ext.get('dtpFinishPMResult').dom.value + '###3###';
						cKriteria += Ext.get('dtpFinishPMResult').dom.value + '###4###';
						cKriteria += IdPMResult + '###5###';
						cKriteria += Ext.get('dtpPMResult').dom.value + '###6###';
						cKriteria += Ext.get('dtpWOFinishPMResult').dom.value + '###7###';
						cKriteria += Ext.get('txtResultDescPMResult').dom.value + '###8###';
						cKriteria += Ext.get('dtpResultPMResult').dom.value + '###9###';
						cKriteria += GetNilaiCurrencyPMResult(Ext.get('txtLastCostPMResult').dom.value) + '###10###';
						ShowReport('', '920011', cKriteria);
					}
				}
			]
		}
	); 
	
    var FormTRPMResult = new Ext.Panel
	(
		{
		    id: 'FormTRPMResult',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRPMResult]
		}
	);

    return FormTRPMResult
};
///---------------------------------------------------------------------------------------///

function GetItemLookupVendorPMResult()
{
	var p = new mRecVendorPMResult
	(
		{
			KD:'txtKdVendorPMResult',
			NAME: 'txtNamaVendorPMResult'
		}
	);
	return p;
};

function GetStrListPartPMResult(mBolLookup)
{
	var str='';
	
	for (var i = 0; i < dsDtlPartPMResult.getCount() ; i++) 
	{
		//if (dsDtlPartPMResult.data.items[i].data.PART_ID != '')
		//{
			// if (mBolLookup === true)
			// {
				if(dsDtlPartPMResult.getCount() === 1)
				{
					str +=  '\'' + dsDtlPartPMResult.data.items[i].data.PART_ID + '\'';
				}
				else
				{
					if (i === dsDtlPartPMResult.getCount()-1 )
					{
						str +=  '\'' + dsDtlPartPMResult.data.items[i].data.PART_ID + '\'';
					}
					else
					{
						str +=  '\'' + dsDtlPartPMResult.data.items[i].data.PART_ID + '\'' + ',' ;
					};
				};
			// }
			// else
			// {
				// // if (i === dsDtlPartPMResult.getCount()-2 || dsDtlPartPMResult.getCount() === 1)
				// // {
					// // str +=  '\'' + dsDtlPartPMResult.data.items[i].data.PART_ID + '\'';
				// // }
				// // else
				// // {
					// // str +=  '\'' + dsDtlPartPMResult.data.items[i].data.PART_ID + '\'' + ',' ;
				// // };
			// };
		//};
	}
	
	return str;
};

function TRPMResultInit(rowdata)
{
    AddNewPMResult = false;
	rowSelectedPMResultTemp=rowSelectedPMResult;
    Ext.get('txtKdMainAssetPMResult').dom.value=rowdata.ASSET_MAINT_ID;
	Ext.get('txtNamaMainAsetPMResult').dom.value=rowdata.ASSET_MAINT_NAME;
	Ext.get('txtLocationPMResult').dom.value=rowdata.LOCATION;
	Ext.get('dtpFinishPMResult').dom.value=ShowDate(rowdata.DUE_DATE);
	Ext.get('dtpPMResult').dom.value=ShowDate(rowdata.WO_PM_DATE);
	Ext.get('dtpWOFinishPMResult').dom.value=ShowDate(rowdata.WO_PM_FINISH_DATE);
	Ext.get('txtKdSupervisorPMResult').dom.value=rowdata.EMP_ID;
	Ext.get('txtNamaSupervisorPMResult').dom.value=rowdata.EMP_NAME;
	
	IdWOPMResult=rowdata.WO_PM_ID;
	IdPMResult=rowdata.RESULT_PM_ID;
	SchIdPMResult=rowdata.SCH_PM_ID;
	varCatIdPMResult=rowdata.CATEGORY_ID;
	
	
	
	if (rowdata.DESC_WO_PM != null)
	{
		Ext.get('txtDeskripsiPMResult').dom.value=rowdata.DESC_WO_PM;
	}
	else
	{
		Ext.get('txtDeskripsiPMResult').dom.value='';
	};
	
	
	if (rowdata.RESULT_PM_ID != null)
	{
		IdPMResult=rowdata.RESULT_PM_ID;
		Ext.get('dtpResultPMResult').dom.value=ShowDate(rowdata.FINISH_DATE);
		Ext.get('txtLastCostPMResult').dom.value=formatCurrency(rowdata.LAST_COST);
		
		if (rowdata.DESC_RESULT_PM != null)
		{
			Ext.get('txtResultDescPMResult').dom.value=rowdata.DESC_RESULT_PM;
		}
		else
		{
			Ext.get('txtResultDescPMResult').dom.value='';
		};
		
		if (rowdata.REFERENCE != null)
		{
			Ext.get('txtReffPMResult').dom.value=rowdata.REFERENCE;
		}
		else
		{
			Ext.get('txtReffPMResult').dom.value='';
		};
		
		LoadDataServiceResultPMResult(IdPMResult,rowdata.CATEGORY_ID);
		LoadDataPartResultPMResult(IdPMResult,varCatIdPMResult);
		GetJumlahTotalResultPersonPartPMResult();
		
		 if (dsDtlCraftPersonIntPMResultTempView.getCount() > 0 )
		{
			LoadDataEmployeeResultPMResult(IdPMResult,varCatIdPMResult)
			IsExtRepairPMResult=false;
			
		}
		else if( dsDtlCraftPersonExtPMResultTempView.getCount() > 0 )
		{
			LoadDataVendorResultPMResult(IdPMResult,varCatIdPMResult)
			IsExtRepairPMResult=true;
			GetVendorPMResult(false);
		}
		else
		{
			IsExtRepairPMResult=false;
		};
		
		
		GetJumlahTotalPersonPartPMResult(false);
	}
	else
	{
		IdPMResult='';
		GetJumlahTotalPersonPartPMResult(true);
		Ext.get('dtpResultPMResult').dom.value=ShowDate(rowdata.WO_PM_FINISH_DATE);
		LoadDataServicePMResult(IdWOPMResult,SchIdPMResult,varCatIdPMResult);
		LoadDataPartPMResult(varCatIdPMResult);
		
		 if (dsDtlCraftPersonIntPMResultTempView.getCount() > 0 )
		{
			LoadDataEmployeePMResult(IdWOPMResult,SchIdPMResult,varCatIdPMResult)
			IsExtRepairPMResult=false;
		}
		else if( dsDtlCraftPersonExtPMResultTempView.getCount() > 0 )
		{
			LoadDataVendorPMResult(IdWOPMResult,SchIdPMResult,varCatIdPMResult)
			IsExtRepairPMResult=true;
			GetVendorPMResult(true);
		}
		else
		{
			IsExtRepairPMResult=false;
		};
		
		// if (IsExtRepairPMResult === false)
		// {
			// LoadDataEmployeePMResult(IdWOPMResult,SchIdPMResult,varCatIdPMResult)
		// }
		// else
		// {
			// LoadDataVendorPMResult(SchIdPMResult,varCatIdPMResult)
		// };
	};
	
};

function LoadDataServicePMResult(WOId,SchId,CatId) 
{
    dsDtlServicePMResult.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_ID',
                            Sort: 'service_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOServicePM',
			    //param: 'where wo_pm_id =~' + WOId + '~ and SCH_PM_ID =~' + SchId + '~ and category_id=~' + CatId + '~'
                            param: 'wo_pm_id = ~' + WOId + '~ and sch_pm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlServicePMResult;
};

function LoadDataServiceResultPMResult(ResultId,CatId) 
{
    dsDtlServicePMResult.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_ID',
                            Sort: 'service_id',
			    Sortdir: 'ASC',
			    target: 'ViewResultServicePM',
			    //param: 'where RESULT_PM_ID =~' + ResultId + '~ and category_id= ~' + CatId + '~'
                            param: 'result_pm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlServicePMResult;
};



function GetNilaiCurrencyPMResult(dblNilai)
{
	for (var i = 0; i < dblNilai.length; i++) 
	{
		var y = dblNilai.substr(i, 1)
		if (y === '.') 
		{
			dblNilai = dblNilai.replace('.', '');
		}
	};
	
	return dblNilai;
};

function LoadDataEmployeePMResult(WOId,SchId,CatId) 
{
    dsDtlCraftPersonIntPMResultTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'EMP_ID',
                            Sort: 'emp_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPersonPM',
			    //param: 'where wo_pm_id =~' + WOId + '~ and SCH_PM_ID =~' + SchId + '~ and category_id=~' + CatId + '~'
                            param: 'wo_pm_id = ~' + WOId + '~ and sch_pm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlCraftPersonIntPMResultTemp;
};

function LoadDataEmployeeResultPMResult(ResultId,CatId) 
{
    dsDtlCraftPersonIntPMResultTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'EMP_ID',
                            Sort: 'emp_id',
			    Sortdir: 'ASC',
			    target: 'ViewResultPersonPM',
			   //param: 'where RESULT_PM_ID =~' + ResultId + '~ and category_id= ~' + CatId + '~'
                           param: 'result_pm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlCraftPersonIntPMResultTemp;
};

function ReLoadDataEmployeeResultPMResult(ResultId,CatId,ServId) 
{
    dsDtlCraftPersonIntPMResult.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'EMP_ID',
                            Sort: 'emp_id',
			    Sortdir: 'ASC',
			    target: 'ViewResultPersonPM',
			   //param: 'where RESULT_PM_ID =~' + ResultId + '~ and category_id= ~' + CatId + '~ and service_id = ~' + ServId + '~'
                           param: 'result_pm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~ and service_id = ~' + ServId + '~'
			}
		}
	);
    return dsDtlCraftPersonIntPMResult;
};

function LoadDataVendorPMResult(WOId,SchId,CatId) 
{
    dsDtlCraftPersonExtPMResultTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'VENDOR_ID',
                            Sort: 'vendor_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPersonPM',
			    //param: 'where wo_pm_id =~' + WOId + '~ and SCH_PM_ID =~' + SchId + '~ and category_id=~' + CatId + '~'
                            param: 'wo_pm_id = ~' + WOId + '~ and sch_pm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlCraftPersonExtPMResultTemp;
};

function LoadDataVendorResultPMResult(ResultId,CatId) 
{
    dsDtlCraftPersonExtPMResultTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'VENDOR_ID',
                            Sort: 'vendor_id',
			    Sortdir: 'ASC',
			    target: 'ViewResultPersonPM',
			   //param: 'where RESULT_PM_ID =~' + ResultId + '~ and category_id= ~' + CatId + '~'
                           param: 'result_pm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlCraftPersonExtPMResultTemp;
};

function ReLoadDataVendorResultPMResult(ResultId,CatId,ServId) 
{
    dsDtlCraftPersonExtPMResult.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'VENDOR_ID',
                            Sort: 'vendor_id',
			    Sortdir: 'ASC',
			    target: 'ViewResultPersonPM',
			   //param: 'where RESULT_PM_ID =~' + ResultId + '~ and category_id= ~' + CatId + '~ and service_id = ~' + ServId + '~'
                           param: 'result_pm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~ and service_id = ~' + ServId + '~'
			}
		}
	);
    return dsDtlCraftPersonExtPMResult;
};


function LoadDataPartPMResult(CatId) 
{
    dsDtlPartPMResultTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'PART_ID',
                            Sort: 'part_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPartPM',
			    //param: 'where category_id=~' + CatId + '~'
                            param: 'category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlPartPMResultTemp;
};

function LoadDataPartResultPMResult(ResultId,CatId) 
{
    dsDtlPartPMResultTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'PART_ID',
                            Sort: 'part_id',
			    Sortdir: 'ASC',
			    target: 'ViewResultPartPM',
			   //param: 'where RESULT_PM_ID =~' + ResultId + '~ and category_id= ~' + CatId + '~'
                           param: 'result_pm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlPartPMResultTemp;
};

function ReLoadDataPartResultPMResult(ResultId,CatId,ServId) 
{
    dsDtlPartPMResult.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'PART_ID',
                            Sort: 'part_id',
			    Sortdir: 'ASC',
			    target: 'ViewResultPartPM',
			   //param: 'where RESULT_PM_ID =~' + ResultId + '~ and category_id= ~' + CatId + '~ and service_id = ~' + ServId + '~'
                           param: 'result_pm_id = ~' + ResultId + '~ and category_id = ~' + CatId + '~ and service_id = ~' + ServId + '~'
			}
		}
	);
    return dsDtlPartPMResult;
};

///---------------------------------------------------------------------------------------///
function PMResultAddNew() 
{
    AddNewPMResult = true; 
	
	IdPMResult='';
	Ext.get('dtpResultPMResult').dom.value=ShowDate(rowSelectedPMResultTemp.data.WO_PM_FINISH_DATE);
	GetJumlahTotalPersonPartPMResult(true);
	Ext.get('txtResultDescPMResult').dom.value='';
	Ext.get('txtReffPMResult').dom.value='';
	dsDtlServicePMResult.removeAll();
	LoadDataServicePMResult(SchIdPMResult);
	
	if (IsExtRepairPMResult === false)
	{
		dsDtlCraftPersonIntPMResult.removeAll();
		dsDtlCraftPersonIntPMResultTemp.removeAll();
		LoadDataEmployeePMResult(SchIdPMResult,varCatIdPMResult)
	}
	else
	{
		dsDtlCraftPersonExtPMResult.removeAll();
		dsDtlCraftPersonExtPMResultTemp.removeAll();
		LoadDataVendorPMResult(SchIdPMResult,varCatIdPMResult)
		Ext.get('txtKdVendorPMResult').dom.value='';
		Ext.get('txtNamaVendorPMResult').dom.value='';
		GetVendorPMResult(true);
	};
	
	dsDtlPartPMResult.removeAll();
	dsDtlPartPMResultTemp.removeAll();
	LoadDataPartPMResult(SchIdPMResult,varCatIdPMResult);
};
///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamPMResult() 
{
    var params =
	{
		Table:'ViewResultPM',
		ResultId: IdPMResult,
		WOId: IdWOPMResult,
		CatId:varCatIdPMResult,
		FinishDate: Ext.get('dtpResultPMResult').dom.value,
		Cost: GetNilaiCurrencyPMResult(Ext.get('txtLastCostPMResult').dom.value),
		Desc:Ext.get('txtResultDescPMResult').dom.value,
		Ref:Ext.get('txtReffPMResult').dom.value,
		ListService:getArrDetailServicePMResult(),
		ListPerson:getArrDetailPersonPMResult(),
		ListPart:getArrDetailPartPMResult(),
		JmlFieldService: mRecordServicePMResult.prototype.fields.length-3,
		JmlFieldPerson: mRecordPersonPMResult.prototype.fields.length-3,
		JmlFieldPart: mRecordPartPMResult.prototype.fields.length-4,
		JmlListService:GetListCountDetailServicePMResult(),
		JmlListPerson:GetListCountDetailPersonPMResult(),
		JmlListPart:GetListCountDetailPartPMResult(),
		Hapus:1,
		Service:0
	};
    return params;
};

function getArrDetailServicePMResult()
{
	var x='';
	for(var i = 0 ; i < dsDtlServicePMResult.getCount();i++)
	{
		if (dsDtlServicePMResult.data.items[i].data.SERVICE_ID != '' && dsDtlServicePMResult.data.items[i].data.SERVICE_NAME != '' && dsDtlServicePMResult.data.items[i].data.CATEGORY_ID != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'RESULT_PM_ID=' + dsDtlServicePMResult.data.items[i].data.RESULT_PM_ID
			y += z + 'SERVICE_ID=' + dsDtlServicePMResult.data.items[i].data.SERVICE_ID
			y += z + 'CATEGORY_ID='+ dsDtlServicePMResult.data.items[i].data.CATEGORY_ID
			
			if (i === (dsDtlServicePMResult.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
		dsDtlServicePMResult.data.items[i].data.TAG='';
	}	
	return x;
};

function GetListCountDetailServicePMResult()
{
	var x=0;
	for(var i = 0 ; i < dsDtlServicePMResult.getCount();i++)
	{
		if (dsDtlServicePMResult.data.items[i].data.SERVICE_ID != '' && dsDtlServicePMResult.data.items[i].data.SERVICE_NAME  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getArrDetailPersonPMResult()
{
	var x='';
	var ds='';
	if (IsExtRepairPMResult === false)
	{
		ds=dsDtlCraftPersonIntPMResultTemp;
	}
	else
	{
		ds=dsDtlCraftPersonExtPMResultTemp;
		for(var i = 0 ; i < ds.getCount();i++)
		{
			if(ds.data.items[i].data.PERSON_NAME === '')
			{
				ds.removeAt(i);
			};
		}
	};
	
	for(var i = 0 ; i < ds.getCount();i++)
	{
		var  mDataKd='';
		var  mDataName='';
		var  mFlag='';
		
		if (IsExtRepairPMResult === false)  // internal
		{
			mDataKd = ds.data.items[i].data.EMP_ID;
			mDataName = ds.data.items[i].data.EMP_NAME;
			mFlag='xxx';
		}
		else
		{   
			mDataKd = ds.data.items[i].data.VENDOR_ID;  // external
			mDataName = ds.data.items[i].data.VENDOR;
			mFlag=ds.data.items[i].data.PERSON_NAME;
		};
		
		if (mDataKd != '' && mDataName != '' && mFlag != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'RESULT_PM_ID=' + ds.data.items[i].data.RESULT_PM_ID
			y += z +'SERVICE_ID=' + ds.data.items[i].data.SERVICE_ID
			y += z + 'ROW_SCH='+ds.data.items[i].data.ROW_SCH
			y += z + 'EMP_ID='+ds.data.items[i].data.EMP_ID
			y += z + 'EMP_NAME='+ds.data.items[i].data.EMP_NAME
			y += z + 'VENDOR_ID='+ds.data.items[i].data.VENDOR_ID
			y += z + 'VENDOR='+ds.data.items[i].data.VENDOR
			
			if (IsExtRepairPMResult === false)  // internal
			{
				y += z + 'PERSON_NAME='+ ds.data.items[i].data.EMP_NAME
			}
			else
			{
				y += z + 'PERSON_NAME='+ ds.data.items[i].data.PERSON_NAME
			};
			
			y += z + 'COST='+ ds.data.items[i].data.COST
			y += z + 'DESC_SCH_PERSON='+ds.data.items[i].data.DESC_SCH_PERSON
			y += z + 'ROW_RSLT='+ds.data.items[i].data.ROW_RSLT
			
			if (i === (ds.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};

function GetListCountDetailPersonPMResult()
{
	var x=0;
	
	if (IsExtRepairPMResult === false)
	{
		ds=dsDtlCraftPersonIntPMResultTemp;
	}
	else
	{
		ds=dsDtlCraftPersonExtPMResultTemp; 
	};
	
	for(var i = 0 ; i < ds.getCount();i++)
	{
	
		var  mDataKd='';
		var  mDataName='';
		var  mFlag='';
		
		if (IsExtRepairPMResult === false)  // internal
		{
			mDataKd = ds.data.items[i].data.EMP_ID;
			mDataName = ds.data.items[i].data.EMP_NAME;
			mFlag='xxx';
		}
		else
		{  
			mDataKd = ds.data.items[i].data.VENDOR_ID; // external
			mDataName = ds.data.items[i].data.VENDOR;
			mFlag = ds.data.items[i].data.PERSON_NAME;
		};
	
		if (mDataKd != '' && mDataName  != '' && mFlag !='')
		{
			x += 1;
		};
	}
	return x;
	
};

function getArrDetailPartPMResult()
{
	var x='';
	for(var i = 0 ; i < dsDtlPartPMResultTemp.getCount();i++)
	{
		if (dsDtlPartPMResultTemp.data.items[i].data.PART_ID != '' && dsDtlPartPMResultTemp.data.items[i].data.PART_NAME != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'RESULT_PM_ID=' +  dsDtlPartPMResultTemp.data.items[i].data.RESULT_PM_ID
			y += z + 'SERVICE_ID=' + dsDtlPartPMResultTemp.data.items[i].data.SERVICE_ID
			y += z + 'PART_ID=' + dsDtlPartPMResultTemp.data.items[i].data.PART_ID
			y += z + 'QTY='+ dsDtlPartPMResultTemp.data.items[i].data.QTY
			y += z + 'UNIT_COST='+ dsDtlPartPMResultTemp.data.items[i].data.UNIT_COST
			y += z + 'TOT_COST='+ (dsDtlPartPMResultTemp.data.items[i].data.UNIT_COST * dsDtlPartPMResultTemp.data.items[i].data.QTY)
			y += z + 'DESC_SCH_PART='+dsDtlPartPMResultTemp.data.items[i].data.DESC_SCH_PART
			
			if (i === (dsDtlPartPMResultTemp.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
		dsDtlPartPMResultTemp.data.items[i].data.TAG='';
	}	
	return x;
};

function GetListCountDetailPartPMResult()
{
	var x=0;
	for(var i = 0 ; i < dsDtlPartPMResultTemp.getCount();i++)
	{
		if (dsDtlPartPMResultTemp.data.items[i].data.PART_ID != '' && dsDtlPartPMResultTemp.data.items[i].data.PART_NAME  != '')
		{
			x += 1;
		};
	}
	return x;
	
};


function getItemPanelInputPMResult(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
		border:false,
		height:754,//734,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar-35,
				labelWidth:75,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype:'fieldset',
						title: nmTitleTabInfoSchResultPM,	
						anchor:  '99.99%',
						height:'70px',
						items :
						[
							getItemPanelNoAssetPMResult(lebar)
						]
					},
					{
						xtype:'fieldset',
						title: nmTitleTabInfoWOResultPM,
						anchor:  '99.99%',
						height:'280px',//260,
						items :
						[
							getItemPanelTanggalPMResult(lebar),
							getItemPanelVendorAppPMResult(lebar), 
							getItemPanelCardLayoutPMResult(),
							getItemPanelDescPMResult()
						]
					},
					{
						xtype:'fieldset',
						title: nmTitleTabInfoRsltResultPM,	
						anchor:  '99.99%',
						labelWidth:140,
						height:'83px',
						items :
						[
							getItemPanelResultPMResult2(lebar),
							{
								xtype: 'textarea',
								fieldLabel: nmNoteResultPM + ' ',
								name: 'txtResultDescPMResult',
								id: 'txtResultDescPMResult',
								scroll:true,
								anchor: '100% 46%'//'100% 60%'
				            }
						]
					}
				]
			}
		]
	};
    return items;
};

function getItemPanelTanggalPMResult(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 70,
	    items:
		[
			{
			    columnWidth: .23,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmWODateResultPM + ' ',
						id: 'dtpPMResult',
						name: 'dtpPMResult',
						readOnly:true,
						anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .27,
			    layout: 'form',
			    border: false,
				labelWidth:100,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmWOFinishResultPM + ' ',
						id: 'dtpWOFinishPMResult',
						name: 'dtpWOFinishPMResult',
						readOnly:true,
						anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .2,
			    layout: 'form',
			    border: false,
				labelWidth:40,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: nmPICResultPM + ' ',
						id: 'txtKdSupervisorPMResult',
						name: 'txtKdSupervisorPMResult',
						readOnly:true,
						anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: '',
						hideLabel:true,
						id: 'txtNamaSupervisorPMResult',
						name: 'txtNamaSupervisorPMResult',
						readOnly:true,
						anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelResultPMResult2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 57,
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:140,
			    items:
				[
					{
					    xtype: 'datefield',
						fieldLabel: nmFinishDateResultPM + ' ',
						id: 'dtpResultPMResult',
						name: 'dtpResultPMResult',
						format: 'd/M/Y',
						value: NowPMResult,
						anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:80,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmFinalCostResultPM + ' ',
					    name: 'txtLastCostPMResult',
					    id: 'txtLastCostPMResult',
					    anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:50,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmRefResultPM + ' ',
					    name: 'txtReffPMResult',
					    id: 'txtReffPMResult',
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelCardLayoutPMResult()
{
	 var FormPanelCardLayoutPMResult = new Ext.Panel
	(
		{
		    id: 'FormPanelCardLayoutPMResult',
		    trackResetOnLoad: true,
		    width: 742,
			height: 195,//175
			layout: 'card',
			border:false,
			activeItem: 0,
			items: 
			[
				{
					xtype: 'panel',
					layout: 'form',
					border:false,
					id: 'CardPMResult1',
					items:
					[	 
						getItemTabPanelRepairInternalPMResult()
					]
				}
			]
		}
	);
	
	return FormPanelCardLayoutPMResult;
};

function getItemTabPanelRepairInternalPMResult()
{

	var TotalServicePMResult = new Ext.Panel
	(
		{
			frame: false,
			layout: 'column',
			width: 700,
			border:false,
			labelAlign:'right',
			labelWidth:210,
			style: 
			{
				'margin-top': '3px'
			},
			items: 
			[
				{
					columnWidth: .5,
					layout: 'form',
					border: false,
					labelWidth:210,
					items:
					{
						xtype: 'textfield',
						fieldLabel: nmTotalPersonPartEstResultPM + ' ',
						style: 
						{
							'font-weight': 'bold',
							'text-align':'right'
						},
						id:'txtTotalServicePMResult',
						name:'txtTotalServicePMResult',
						readOnly:true,
						width: 110
					}
				},
				{
					columnWidth: .5,
					layout: 'form',
					border: false,
					labelWidth:210,
					items:
					{
						xtype: 'textfield',
						fieldLabel: nmTotalPersonPartRealResultPM + ' ',
						style: 
						{
							'font-weight': 'bold',
							'text-align':'right'
						},
						id:'txtTotalServiceRealPMResult',
						name:'txtTotalServiceRealPMResult',
						readOnly:true,
						width: 110
					}
				}
			]
		}
	);
	
	var TotalPersonPMResult = new Ext.Panel
	(
		{
			frame: false,
			layout: 'column',
			width: 725,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '3px',
				'margin-left': '410px'//'318px'
			},
			items: 
			[
				{
					columnWidth: .42,//.37,
					layout: 'form',
					border: false,
					labelWidth:140,
					items:
					{
						xtype: 'textfield',
						fieldLabel: nmTotalResultPM + ' ',
						style: 
						{
							'font-weight': 'bold',
							'text-align':'right'
						},
						id:'txtTotalPersonPMResult',
						name:'txtTotalPersonPMResult',
						readOnly:true,
						width: 150//120
					}
				}//,
				// {
					// columnWidth: .3,
					// layout: 'form',
					// border: false,
					// visible:false,
					// items:
					// {
						// xtype: 'textfield',
						// fieldLabel: '',
						// hideLabel:true,
						// style: 
						// {
							// 'font-weight': 'bold',
							// 'text-align':'right'
						// },
						// id:'txtTotalRealCostPersonPMResult',
						// name:'txtTotalRealCostPersonPMResult',
						// visible:false,
						// readOnly:true,
						// width: 120
					// }
				// }
			]
		}
	);

	var TotalPartPMResult = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 725,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '3px',
				'margin-left': '500px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: nmTotalResultPM + ' ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalPartPMResult',
					name:'txtTotalPartPMResult',
					readOnly:true,
					width: 150
				}
			]
		}
	);
	
    vTabPanelRepairPMResult = new Ext.TabPanel
	(
		{
		    id: 'vTabPanelRepairPMResult',
		    region: 'center',
		    margins: '7 7 7 7',
			style:
			{
			   'margin-top': '5px'
			},
		    bodyStyle: 'padding:7px 7px 7px 7px',
		    activeTab: 0,
		    plain: true,
		    defferedRender: false,
		    frame: false,
		    border: true,
		    height: 180,//160,
			width:742,
		    anchor: '100%',
		    items:
			[
				{
					 title: nmTitleTabServiceResultPM,
					 id: 'tabServicePMResult',
					 items:
					 [
						GetDTLServicePMResultGrid(),TotalServicePMResult
					 ],
					 listeners:
					{
						activate: function() 
						{
							FocusCtrlPMResult === 'colService'
							varTabRepairPMResult=1;
						}
					}
				},
			    {
					 title: nmTitleTabPersonResultPM,
					 id: 'tabPersonPMResult',
					 items:
					 [
						getItemPanelCardLayoutGridServicePMResult(),TotalPersonPMResult
					 ],
			        listeners:
					{
						activate: function() 
						{
							if (cellSelectServicePMResult != undefined)
							{
								if (cellSelectServicePMResult != '' && cellSelectServicePMResult.data != undefined)
								{
									if (cellSelectServicePMResult.data.SERVICE_ID === undefined || cellSelectServicePMResult.data.SERVICE_ID === '')
									{
										vTabPanelRepairPMResult.setActiveTab('tabServicePMResult');
										varTabRepairPMResult=1;
										alert(nmAlertTabService);
									}
									else
									{
										if (IsExtRepairPMResult === true)
										{
											Ext.getCmp('FormPanelCardLayoutGridPersonPMResult').getLayout().setActiveItem(1);	
											FocusCtrlPMResult = 'colVendor'
											GetPersonExtServicePMResult(cellSelectServicePMResult.data.SERVICE_ID);
										}
										else
										{
											Ext.getCmp('FormPanelCardLayoutGridPersonPMResult').getLayout().setActiveItem(0);	
											FocusCtrlPMResult = 'colEmp'
											GetPersonIntServicePMResult(cellSelectServicePMResult.data.SERVICE_ID);
										};
										CalcTotalPersonPMResult('',false);
										//CalcTotalRealCostPersonPMResult('');
										varTabRepairPMResult=2;
									};
								}
								else
								{
									vTabPanelRepairPMResult.setActiveTab('tabServicePMResult');
									varTabRepairPMResult=1;
									alert(nmAlertTabService);
								};
							}
							else
							{
								vTabPanelRepairPMResult.setActiveTab('tabServicePMResult');
								varTabRepairPMResult=1;
								alert(nmAlertTabService);
							};
						}
					}
				},
				{
					 title: nmTitleTabPartResultPM,
					 id: 'tabWOPartInternalPMResult',
					 items:
					 [
						GetDTLPartInternalPMResultGrid(),TotalPartPMResult
					 ],
					 listeners:
					{
						activate: function() 
						{
							if (cellSelectServicePMResult != undefined)
							{
								if (cellSelectServicePMResult != '' && cellSelectServicePMResult.data != undefined)
								{
									if (cellSelectServicePMResult.data.SERVICE_ID === undefined || cellSelectServicePMResult.data.SERVICE_ID === '')
									{
										vTabPanelRepairPMResult.setActiveTab('tabServicePMResult');
										varTabRepairPMResult=1;
										alert(nmAlertTabService);
									}
									else
									{
										FocusCtrlPMResult === 'colPart'
										varTabRepairPMResult=3;
										GetPartPMResult(cellSelectServicePMResult.data.SERVICE_ID);
										CalcTotalPartPMResult('','',false)
									};	
								}
								else
								{
									vTabPanelRepairPMResult.setActiveTab('tabServicePMResult');
									varTabRepairPMResult=1;
									alert(nmAlertTabService);
								};
							}
							else
							{
								vTabPanelRepairPMResult.setActiveTab('tabServicePMResult');
								varTabRepairPMResult=1;
								alert(nmAlertTabService);
							};
						}
					}
				}
			]
		}
	)

		return vTabPanelRepairPMResult;
};

function GetDTLServicePMResultGrid() 
{
    var fldDetail = ['RESULT_PM_ID','SCH_PM_ID','SERVICE_ID','SERVICE_NAME'];

    dsDtlServicePMResult = new WebApp.DataStore({ fields: fldDetail })

    var gridServicePMResult  = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlServicePMResult,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:726,
		    height:115,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectServicePMResult = dsDtlServicePMResult.getAt(row);
					        CurrentServicePMResult.row = row;
					        CurrentServicePMResult.data = cellSelectServicePMResult;
					    }
					}
				}
			),
				cm: ServicePMResultDetailColumModel(),
				 tbar: 
			         [
				        {
					        id:'btnTambahBrsServicePMResult',
					        text: nmTambahBaris,
					        tooltip: nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						TambahBarisServicePMResult(dsDtlServicePMResult);
					        }
				        },'-',
				        {
					        id:'btnHpsBrsServicePMResult',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlServicePMResult.getCount() > 0 )
								{
									if (cellSelectServicePMResult != undefined)
									{
										if(CurrentServicePMResult != undefined)
										{
											HapusBarisServicePMResult();
										};
									}
									else
									{
										ShowPesanWarningPMResult(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			            ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridServicePMResult;
};

function HapusBarisServicePMResult()
{
	if (cellSelectServicePMResult.data.SERVICE_ID != '' && cellSelectServicePMResult.data.SERVICE_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg:nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + ' ' + (CurrentServicePMResult.row + 1) + ' ' +nmOperatorDengan + ' ' + nmServIDResultPM + ' : ' + cellSelectServicePMResult.data.SERVICE_ID + ' ' + nmOperatorAnd + ' ' + nmServNameResultPM + ' : ' + cellSelectServicePMResult.data.SERVICE_NAME ,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlServicePMResult.data.items[CurrentServicePMResult.row].data.TAG === 1 || dsDtlServicePMResult.data.items[CurrentServicePMResult.row].data.RESULT_PM_ID === '')
							{
								deletePersonPartServiceRowDeletePMResult(cellSelectServicePMResult.data.SERVICE_ID);
								dsDtlServicePMResult.removeAt(CurrentServicePMResult.row);
								cellSelectServicePMResult=undefined;
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailServicePMResult();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			dsDtlServicePMResult.removeAt(CurrentServicePMResult.row);
		};
};

function deletePersonPartServiceRowDeletePMResult(service_id)
{
	var x=0;
	var y=0;
	
	if (service_id != '' && service_id != undefined)
	{
		if( IsExtRepairPMResult === true)
		{
			for (var i = 0; i < dsDtlCraftPersonExtPMResultTemp.getCount() ; i++) 
			{
				if(dsDtlCraftPersonExtPMResultTemp.data.items[i].data.SERVICE_ID === service_id)
				{
					x += dsDtlCraftPersonExtPMResultTemp.data.items[i].data.COST ;
					dsDtlCraftPersonExtPMResultTemp.removeAt(i);
				};
			};
		}
		else
		{
			for (var i = 0; i < dsDtlCraftPersonIntPMResultTemp.getCount() ; i++) 
			{
				if(dsDtlCraftPersonIntPMResultTemp.data.items[i].data.SERVICE_ID === service_id)
				{
					x += dsDtlCraftPersonIntPMResultTemp.data.items[i].data.COST ;
					dsDtlCraftPersonIntPMResultTemp.removeAt(i);
				};
			};
		};
		
		for (var i = 0; i < dsDtlPartPMResultTemp.getCount() ; i++) 
		{
			if(dsDtlPartPMResultTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				y += (dsDtlPartPMResultTemp.data.items[i].data.UNIT_COST * dsDtlPartPMResultTemp.data.items[i].data.QTY);
				dsDtlPartPMResultTemp.removeAt(i);
			};
		};
		
		var a=GetNilaiCurrencyPMResult(Ext.get('txtTotalServiceRealPMResult').dom.value);
		var b=GetNilaiCurrencyPMResult(Ext.get('txtLastCostPMResult').dom.value);
		
		if (a > 0)
		{
			Ext.get('txtTotalServiceRealPMResult').dom.value=formatCurrency(a-(x+y));
		}
		else
		{
			Ext.get('txtTotalServiceRealPMResult').dom.value=0
		};
		
		if (b > 0)
		{
			Ext.get('txtLastCostPMResult').dom.value=formatCurrency(b-(x+y));
		}
		else
		{
			Ext.get('txtLastCostPMResult').dom.value=0
		};
	};
};

function GetLookupServicePMResult(str,idx,ds)
{
	if (IdPMResult === '' || IdPMResult === undefined)
	{
		var p = new mRecordServicePMResult
		(
			{
				'RESULT_PM_ID':'',
				'SCH_PM_ID':'',
				'SERVICE_ID':'',
				'SERVICE_NAME':'',
				'CATEGORY_ID':varCatIdPMResult,
				'TAG':1
			}
		);
		FormLookupServiceCategory('','',str,true,p,ds,false);
	}
	else
	{	
		var p = new mRecordServicePMResult
		(
			{
				'RESULT_PM_ID':IdPMResult,
				'SCH_PM_ID':'',
				'SERVICE_ID':'',
				'SERVICE_NAME':'',
				'CATEGORY_ID':varCatIdPMResult,
				'TAG':1
			}
		);
		FormLookupServiceCategory('','',str,true,p,ds,false);
	};
};

function TambahBarisServicePMResult(ds)
{
	var p = GetRecordBaruServicePMResult();
	ds.insert(ds.getCount(), p);
};

function DeleteDetailServicePMResult()
{
	Ext.Ajax.request
	(
		{
			url: WebAppUrl.UrlDeleteData, //"./Datapool.mvc/DeleteDataObj",
			params:  getParamDeleteDetailServicePMResult(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoPMResult(nmPesanHapusSukses,nmHeaderHapusData);
					deletePersonPartServiceRowDeletePMResult(cellSelectServicePMResult.data.SERVICE_ID);
					GetJumlahTotalResultPersonPartPMResult();
					dsDtlServicePMResult.removeAt(CurrentServicePMResult.row);
					cellSelectServicePMResult=undefined;
					LoadDataServiceResultPMResult(IdPMResult,varCatIdPMResult);
						//GetJumlahPartPMResult();
					
					AddNewPMResult = false;
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningPMResult(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorPMResult(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailServicePMResult()
{
	var params =
	{	
		Table: 'ViewResultPM',   
	    ResultId: CurrentServicePMResult.data.data.RESULT_PM_ID,
		CatId: varCatIdPMResult,
		ServiceId: CurrentServicePMResult.data.data.SERVICE_ID,
		Hapus:1,
		Service:1
		
	};
	
    return params;
};


function ServicePMResultDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdServicePMResult',
			    header: nmServIDResultPM,
			    dataIndex: 'SERVICE_ID',
			    width: 150,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdServicePMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListServicePMResult();
									if (strList !='')
									{
									    //strList = ' and SERVICE_ID not in ( ' + strList + ') '
                                                                            strList = ' and service_id not in ( ' + strList + ') '
									};
									
						            if (Ext.get('fieldcolKdServicePMResult').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdPMResult + '~'
                                                                                str = ' category_id = ~' + varCatIdPMResult + '~'
										str += strList;
										//str += ' and SERVICE_ID like ~%' + Ext.get('fieldcolKdServicePMResult').dom.value + '%~';
                                                                                str += ' and service_id like ~%' + Ext.get('fieldcolKdServicePMResult').dom.value + '%~';
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdPMResult + '~';
                                                                                str = ' category_id = ~' + varCatIdPMResult + '~';
										str += strList;
									};
									GetLookupServicePMResult(str,CurrentServicePMResult.row,dsDtlServicePMResult);  
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMResult='colService';
							}
						}
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						str = "<div style='color:blue;'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colServiceNamePMResult',
			    header: nmServNameResultPM,
			    dataIndex: 'SERVICE_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolServiceNamePMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListServicePMResult();
									if (strList !='')
									{
									    //strList = ' and SERVICE_ID not in ( ' + strList + ') '
                                                                            strList = ' and service_id not in ( ' + strList + ') '
									};
									
						            if (Ext.get('fieldcolServiceNamePMResult').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdPMResult + '~'
                                                                                str = ' category_id = ~' + varCatIdPMResult + '~'
										str += strList;
										//str += ' and  SERVICE_NAME like ~%' + Ext.get('fieldcolServiceNamePMResult').dom.value + '%~';
                                                                                str += ' and  service_name like ~%' + Ext.get('fieldcolServiceNamePMResult').dom.value + '%~';
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdPMResult + '~'
                                                                                str = ' category_id = ~' + varCatIdPMResult + '~'
										str += strList;
									};
									GetLookupServicePMResult(str,CurrentServicePMResult.row,dsDtlServicePMResult);  
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMResult='colService';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			}
		]
	)
};



function getItemPanelCardLayoutGridServicePMResult()
{

	 var FormPanelCardLayoutGridPersonPMResult = new Ext.Panel
	(
		{
		    id: 'FormPanelCardLayoutGridPersonPMResult',
		    trackResetOnLoad: true,
		    width: 725,
			height: 115,//150,
			layout: 'card',
			border:false,
			activeItem: 0,
			items: 
			[
				{
					xtype: 'panel',
					layout: 'form',
					width: 725,
					height: 150,
					border:false,
					id: 'CardGridServicePMResult1',
					items:
					[	 
						GetDTLCraftPersonPMResultGrid()
					]
				},
				{
					xtype: 'panel',
					layout: 'form',
					width: 725,
					height: 150,
					border:false,
					id: 'CardGridServicePMResult2',
					items:
					[
						GetDTLCraftPersonExtPMResultGrid()
					]
				}
			]
		}
	);
	
	return FormPanelCardLayoutGridPersonPMResult;
};



function GetDTLCraftPersonPMResultGrid() 
{
   var fldDetail = ['RESULT_PM_ID','SCH_PM_ID','SERVICE_ID','ROW_SCH','ROW_RSLT','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_SCH_PERSON','VENDOR','EMP_NAME','CATEGORY_ID','RESULT_PM_ID','ROW','REAL_COST'];

    dsDtlCraftPersonIntPMResult = new WebApp.DataStore({ fields: fldDetail })
	dsDtlCraftPersonIntPMResultTemp = new WebApp.DataStore({ fields: fldDetail })
	
    var gridCraftPersonPMResult = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlCraftPersonIntPMResult,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:725,
		    height:115,//120,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectPersonIntPMResult = dsDtlCraftPersonIntPMResult.getAt(row);
					        CurrentPersonIntPMResult.row = row;
					        CurrentPersonIntPMResult.data = cellSelectPersonIntPMResult;
					    }
					}
				}
			),
				cm: CraftPersonPMResultDetailColumModel(),
				tbar: 
			         [
				        {
					        id:'btnTambahBrsCraftPersonIntPMResult',
					        text: nmTambahBaris,
					        tooltip: nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						TambahBarisPersonIntPMResult();
					        }
				        },'-',
				        {
					        id:'btnHpsBrsCraftPersonIntPMResult',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlCraftPersonIntPMResult.getCount() > 0 )
								{
									if (cellSelectPersonIntPMResult != undefined)
									{
										if(CurrentPersonIntPMResult != undefined)
										{
											HapusBarisPersonIntPMResult();
										};
									}
									else
									{
										ShowPesanWarningPMResult(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			            ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridCraftPersonPMResult;
};

function TambahBarisPersonIntPMResult()
{
	var p = GetRecordBaruPersonPMResult(dsDtlCraftPersonIntPMResult);
	dsDtlCraftPersonIntPMResult.insert(dsDtlCraftPersonIntPMResult.getCount(), p);
};

function HapusBarisPersonIntPMResult()
{
	if (cellSelectPersonIntPMResult.data.EMP_ID != '' && cellSelectPersonIntPMResult.data.EMP_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentPersonIntPMResult.row + 1) + ' ' + nmOperatorDengan + ' ' + nmEmpIDResultPM + ' : ' + cellSelectPersonIntPMResult.data.EMP_ID + ' ' + nmOperatorAnd + nmEmpNameResultPM + ' : ' + cellSelectPersonIntPMResult.data.EMP_NAME,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlCraftPersonIntPMResult.data.items[CurrentPersonIntPMResult.row].data.ROW_RSLT=== '' )
							{
								GetTotalRemovePMResult(CurrentPersonIntPMResult,true);
								HapusRowTempPMResult(dsDtlCraftPersonIntPMResultTemp,CurrentPersonIntPMResult.data.data.ROW);
								dsDtlCraftPersonIntPMResult.removeAt(CurrentPersonIntPMResult.row);
								
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPersonPMResult();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			dsDtlCraftPersonIntPMResult.removeAt(CurrentPersonIntPMResult.row);
		};
};

function GetTotalRemovePMResult(current,mBolEmp)
{
	var a;
	var b=0;
	
	if(mBolEmp === true )
	{
		a='txtTotalPersonPMResult';
	}
	else
	{
		a='txtTotalPartPMResult';
	};
	
	var x=GetNilaiCurrencyPMResult(Ext.get(a).dom.value);
	var y=GetNilaiCurrencyPMResult(Ext.get('txtTotalServiceRealPMResult').dom.value);
	var z=GetNilaiCurrencyPMResult(Ext.get('txtLastCostPMResult').dom.value);
	
	if(mBolEmp === true)
	{
		b=current.data.data.COST;
	}
	else
	{
		b=current.data.data.UNIT_COST * current.data.data.QTY;
	};
	
	if (x > 0)
	{
		Ext.get(a).dom.value=formatCurrency(x-b);
	}
	else
	{
		Ext.get(a).dom.value=0;
	};
	
	if (y > 0)
	{
		Ext.get('txtTotalServiceRealPMResult').dom.value=formatCurrency(y-b);
	}
	else
	{
		Ext.get('txtTotalServiceRealPMResult').dom.value=0
	};
	
	if (z > 0)
	{
		Ext.get('txtLastCostPMResult').dom.value=formatCurrency(z-b);
	}
	else
	{
		Ext.get('txtLastCostPMResult').dom.value=0
	};
};

function DeleteDetailPersonPMResult()
{
	Ext.Ajax.request
	(
		{
			url: WebAppUrl.UrlDeleteData, //"./Datapool.mvc/DeleteDataObj",
			params:  getParamDeleteDetailPersonPMResult(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoPMResult(nmPesanHapusSukses,nmHeaderHapusData);
					if (IsExtRepairPMResult === false)
					{
						//GetJumlahTotalResultPersonPMResult('where RESULT_PM_ID =~' + IdPMResult + '~ and category_id = ~' + varCatIdPMResult + '~' + ' and service_id= ~ ' + cellSelectServicePMResult.data.SERVICE_ID + '~' + ' and EMP_ID <> ~~' + 'and EMP_ID is not null');
                                                GetJumlahTotalResultPersonPMResult('result_pm_id = ~' + IdPMResult + '~ and category_id = ~' + varCatIdPMResult + '~' + ' and service_id = ~ ' + cellSelectServicePMResult.data.SERVICE_ID + '~' + ' and emp_id <> ~~' + ' and emp_id is not null');
						HapusRowTempPMResult(dsDtlCraftPersonIntPMResultTemp,CurrentPersonIntPMResult.data.data.ROW);
						dsDtlCraftPersonIntPMResult.removeAt(CurrentPersonIntPMResult.row);
						cellSelectPersonIntPMResult=undefined;
						LoadDataEmployeeResultPMResult(IdPMResult,varCatIdPMResult);
						ReLoadDataEmployeeResultPMResult(IdPMResult,varCatIdPMResult,cellSelectServicePMResult.data.SERVICE_ID);
					}
					else
					{
						//GetJumlahTotalResultPersonPMResult('where RESULT_PM_ID =~' + IdPMResult + '~ and category_id = ~' + varCatIdPMResult + '~' + ' and service_id= ~ ' + cellSelectServicePMResult.data.SERVICE_ID + '~' + ' and VENDOR_ID <> ~~' + 'and VENDOR_ID is not null');
                                                GetJumlahTotalResultPersonPMResult('result_pm_id = ~' + IdPMResult + '~ and category_id = ~' + varCatIdPMResult + '~' + ' and service_id = ~ ' + cellSelectServicePMResult.data.SERVICE_ID + '~' + ' and vendor_id <> ~~' + ' and vendor_id is not null');
						HapusRowTempPMResult(dsDtlCraftPersonExtPMResultTemp,CurrentPersonExtPMResult.data.data.ROW);
						dsDtlCraftPersonExtPMResult.removeAt(CurrentPersonExtPMResult.row);
						cellSelectPersonExtPMResult=undefined;
						LoadDataVendorResultPMResult(IdPMResult,varCatIdPMResult);
						ReLoadDataVendorResultPMResult(IdPMResult,varCatIdPMResult,cellSelectServicePMResult.data.SERVICE_ID);
					};
					
					AddNewPMResult = false;
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningPMResult(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorPMResult(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailPersonPMResult()
{
	var x,y,z;
	
	if (IsExtRepairPMResult === false)
	{
		x=CurrentPersonIntPMResult.data.data.RESULT_PM_ID;
		y=CurrentPersonIntPMResult.data.data.SERVICE_ID;
		z=CurrentPersonIntPMResult.data.data.ROW_RSLT;
	}
	else
	{
		x=CurrentPersonExtPMResult.data.data.RESULT_PM_ID;
		y=CurrentPersonExtPMResult.data.data.SERVICE_ID;
		z=CurrentPersonExtPMResult.data.data.ROW_RSLT;
	};
	
	var params =
	{	
		Table: 'ViewResultPM',   
	    ResultId: x,
		ServiceId:y,
		RowResult:z,
		CatId: varCatIdPMResult,
		Hapus:3
	};
	
    return params;
};

function GetDTLCraftPersonExtPMResultGrid() 
{
    var fldDetail = ['RESULT_PM_ID','SCH_PM_ID','SERVICE_ID','ROW_SCH','ROW_RSLT','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_SCH_PERSON','VENDOR','EMP_NAME','CATEGORY_ID','RESULT_PM_ID','ROW'];

    dsDtlCraftPersonExtPMResult = new WebApp.DataStore({ fields: fldDetail });
	dsDtlCraftPersonExtPMResultTemp = new WebApp.DataStore({ fields: fldDetail })

    var gridCraftPersonExtPMResult = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlCraftPersonExtPMResult,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:725,
		    height:85,//120
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectPersonExtPMResult = dsDtlCraftPersonExtPMResult.getAt(row);
					        CurrentPersonExtPMResult.row = row;
					        CurrentPersonExtPMResult.data = cellSelectPersonExtPMResult;
					    }
					}
				}
			),
				cm: CraftPersonExtPMResultDetailColumModel(),
				 tbar: 
			         [
				        {
					        id:'btnTambahBrsCraftPersonExtPMResult',
					        text: nmTambahBaris,
					        tooltip: nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						TambahBarisPersonExtPMResult();
					        }
				        },'-',
				        {
					        id:'btnHpsBrsCraftPersonExtPMResult',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlCraftPersonExtPMResult.getCount() > 0 )
								{
									if (cellSelectPersonExtPMResult != undefined)
									{
										if(CurrentPersonExtPMResult != undefined)
										{
											HapusBarisPersonExtPMResult();
										};
									}
									else
									{
										ShowPesanWarningPMResult(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			            ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridCraftPersonExtPMResult;
};

function TambahBarisPersonExtPMResult()
{
	if (Ext.get('txtKdVendorPMResult').dom.value === '')
	{
		ShowPesanWarningPMResult(nmGetValidasiKosong(nmVendIDNameResultPM), nmTambahBaris);
	}
	else
	{
		var p = GetRecordBaruPersonPMResult(dsDtlCraftPersonExtPMResult);
		dsDtlCraftPersonExtPMResult.insert(dsDtlCraftPersonExtPMResult.getCount(), p);
		
		var j=dsDtlCraftPersonExtPMResult.getCount()-1;
		dsDtlCraftPersonExtPMResultTemp.insert(dsDtlCraftPersonExtPMResultTemp.getCount(), dsDtlCraftPersonExtPMResult.data.items[j]);
		GetPersonExtServicePMResult(cellSelectServicePMResult.data.SERVICE_ID);
	};
	// var p = GetRecordBaruPersonPMResult(dsDtlCraftPersonExtPMResult);
	// dsDtlCraftPersonExtPMResult.insert(dsDtlCraftPersonExtPMResult.getCount(), p);
};

function HapusBarisPersonExtPMResult()
{
	if (cellSelectPersonExtPMResult.data.VENDOR_ID != '' && cellSelectPersonExtPMResult.data.VENDOR != '' && cellSelectPersonExtPMResult.data.PERSON_NAME !='')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentPersonExtPMResult.row + 1) + ' ' + nmOperatorDengan + ' '+ nmPersonNameResultPM + ' : ' + cellSelectPersonExtPMResult.data.PERSON_NAME,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlCraftPersonExtPMResult.data.items[CurrentPersonExtPMResult.row].data.ROW_RSLT === '' )
							{
								GetTotalRemovePMResult(CurrentPersonExtPMResult,true);
								HapusRowTempPMResult(dsDtlCraftPersonExtPMResultTemp,CurrentPersonExtPMResult.data.data.ROW);
								dsDtlCraftPersonExtPMResult.removeAt(CurrentPersonExtPMResult.row);
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPersonPMResult();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			if(CurrentPersonExtPMResult.data.data.COST != '' && CurrentPersonExtPMResult.data.data.COST != undefined)
			{
				GetTotalRemovePMResult(CurrentPersonExtPMResult,true)
			};
			HapusRowTempPMResult(dsDtlCraftPersonExtPMResultTemp,CurrentPersonExtPMResult.data.data.ROW);
			dsDtlCraftPersonExtPMResult.removeAt(CurrentPersonExtPMResult.row);
		};
};

function GetDTLPartInternalPMResultGrid() {
     var fldDetail = ['RESULT_PM_ID','SCH_PM_ID','SERVICE_ID','PART_ID','QTY','UNIT_COST','TOT_COST','PART_NAME','PRICE','DESC_SCH_PART','TAG','ROW'];

    dsDtlPartPMResult = new WebApp.DataStore({ fields: fldDetail });
	  dsDtlPartPMResultTemp = new WebApp.DataStore({ fields: fldDetail })

    var gridPartInternalPMResult = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlPartPMResult,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:725,
		    height: 115,//120,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
							cellSelectPartPMResult = dsDtlPartPMResult.getAt(row);
					        CurrentPartPMResult.row = row;
					        CurrentPartPMResult.data = cellSelectPartPMResult;
					    }
					}
				}
			),
			cm: PartInternalPMResultDetailColumModel(),
			tbar:
			         [
				        {
				            id: 'btnTambahBrsPartPMResult',
				            text: nmTambahBaris,
				            tooltip: nmTambahBaris,
				            iconCls: 'AddRow',
				            handler: function() 
							{
								TambahBarisPartPMResult();
				            }
				        }, '-',
				        {
				            id: 'btnHpsBrsPartPMResult',
				            text: nmHapusBaris,
				            tooltip: nmHapusBaris,
				            iconCls: 'RemoveRow',
				            handler: function() 
							{
								if (dsDtlPartPMResult.getCount() > 0 )
								{
									if (cellSelectPartPMResult != undefined)
									{
										if(CurrentPartPMResult != undefined)
										{
											HapusBarisPartPMResult();
										};
									}
									else
									{
										ShowPesanWarningPMResult(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
				            }
				        }, ' ', '-'
			            ]
		    , viewConfig: { forceFit: true }
		}
	);

		return gridPartInternalPMResult;
};

function TambahBarisPartPMResult()
{
	var p = GetRecordBaruPartPMResult();
	dsDtlPartPMResult.insert(dsDtlPartPMResult.getCount(), p);
};

function HapusBarisPartPMResult()
{
	if (cellSelectPartPMResult.data.PART_ID != '' && cellSelectPartPMResult.data.PART_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				  msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentPartPMResult.row + 1) + ' ' + nmOperatorDengan + ' ' + nmPartNumResultPM + ' : ' + cellSelectPartPMResult.data.PART_ID + ' ' + nmOperatorAnd + ' ' + nmPartNameResultPM + ' : ' + cellSelectPartPMResult.data.PART_NAME ,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlPartPMResult.data.items[CurrentPartPMResult.row].data.TAG === 1 || dsDtlPartPMResult.data.items[CurrentPartPMResult.row].data.RESULT_PM_ID === '')
							{
								GetTotalRemovePMResult(CurrentPartPMResult,false);
								HapusRowTempPMResult(dsDtlPartPMResultTemp,CurrentPartPMResult.data.data.ROW);
								dsDtlPartPMResult.removeAt(CurrentPartPMResult.row);
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPartPMResult();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			if(CurrentPartPMResult.data.data.UNIT_COST != '' && CurrentPartPMResult.data.data.UNIT_COST != undefined)
			{
				GetTotalRemovePMResult(CurrentPartPMResult,false);
			};
			dsDtlPartPMResult.removeAt(CurrentPartPMResult.row);
		};
};

function DeleteDetailPartPMResult()
{
	Ext.Ajax.request
	(
		{
			url: WebAppUrl.UrlDeleteData, //"./Datapool.mvc/DeleteDataObj",
			params:  getParamDeleteDetailPartPMResult(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					GetJumlahTotalResultPartPMResult();
					HapusRowTempPMResult(dsDtlPartPMResultTemp,CurrentPartPMResult.data.data.ROW);
					dsDtlPartPMResult.removeAt(CurrentPartPMResult.row);
					cellSelectPartPMResult=undefined;
					LoadDataPartResultPMResult(IdPMResult,varCatIdPMResult);
					ReLoadDataPartResultPMResult(IdPMResult,varCatIdPMResult,cellSelectServicePMResult.data.SERVICE_ID);
					AddNewPMResult = false;
					ShowPesanInfoPMResult(nmPesanHapusSukses,nmHeaderHapusData);
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningPMResult(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorPMResult(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailPartPMResult()
{
	
	var params =
	{	
		Table: 'ViewResultPM',   
	    ResultId: CurrentPartPMResult.data.data.RESULT_PM_ID,
		ServiceId: CurrentPartPMResult.data.data.SERVICE_ID,
		PartId: CurrentPartPMResult.data.data.PART_ID,
		CatId: varCatIdPMResult,
		Hapus:4
	};
	
    return params;
};

function CraftPersonPMResultDetailColumModel() 
{
   return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdEmployeePMResult',
			    header: nmEmpIDResultPM,
			    dataIndex: 'EMP_ID',
			    width: 150,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdEmployeePMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
						            if (Ext.get('fieldcolKdEmployeePMResult').dom.value != '')
									{
										//str = ' where EMP_ID like ~%' + Ext.get('fieldcolKdEmployeePMResult').dom.value + '%~';
                                                                                str = ' emp_id like ~%' + Ext.get('fieldcolKdEmployeePMResult').dom.value + '%~';
									};
									GetLookupPersonIntPMResult(str)    
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMResult='colEmp';
							}
						}
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						str = "<div style='color:blue;'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colEmployeeNamePMResult',
			    header: nmEmpNameResultPM,
			    dataIndex: 'EMP_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolEmployeeNamePMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
						            if (Ext.get('fieldcolEmployeeNamePMResult').dom.value != '')
									{
										//str = ' where EMP_NAME like ~%' + Ext.get('fieldcolEmployeeNamePMResult').dom.value + '%~';
                                                                                str = ' emp_name like ~%' + Ext.get('fieldcolEmployeeNamePMResult').dom.value + '%~';
									};
									GetLookupPersonIntPMResult(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMResult='colEmp';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell,record) 
				{
					var str='';
					if (value != undefined)
					{
						if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};
					};
			        return str;
			    }
			},
			{
			    id: 'colDescCraftPersonPMResult',
			    header: nmDescResultPM,
			    width: 200,
			    dataIndex: 'DESC_SCH_PERSON',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescCraftPersonPMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(value, cell,record) 
				{
			       var str='';
					if (value != undefined)
					{
						if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};
					};
			        return str;
			    }
			},
			{
			    id: 'colCostCraftPersonPMResult',
			    header: nmCostResultPM,
			    width: 150,
				align:'right',
			    dataIndex: 'COST',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostCraftPersonPMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPersonPMResult(CurrentPersonIntPMResult.row,true);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.COST) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.COST);
					};
				}
			},
			{
			    id: 'colRealCostCraftPersonPMResult',
			    header: nmRealCostResultPM,
			    width: 150,
				align:'right',
				hidden:true,
			    dataIndex: 'REAL_COST',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolRealCostCraftPersonPMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalRealCostPersonPMResult(CurrentPersonIntPMResult.row,true);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.REAL_COST) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.REAL_COST);
					};
				}
			}
		]
	)

};

function GetLookupPersonIntPMResult(str)
{
	if (cellSelectPersonIntPMResult.data.ROW_SCH === '' || cellSelectPersonIntPMResult.data.ROW_SCH === undefined)
	{
		var p = new mRecordPersonPMResult
		(
			{
				'RESULT_PM_ID':IdPMResult,
				'SCH_PM_ID':'',
				'SERVICE_ID':cellSelectServicePMResult.data.SERVICE_ID,
				'ROW_SCH':'',
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonIntPMResult.data.items[CurrentPersonIntPMResult.row].data.COST,
				'REAL_COST':dsDtlCraftPersonIntPMResult.data.items[CurrentPersonIntPMResult.row].data.REAL_COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonIntPMResult.data.items[CurrentPersonIntPMResult.row].data.DESC_SCH_PERSON,
				'ROW': dsDtlCraftPersonIntPMResult.data.items[CurrentPersonIntPMResult.row].data.ROW,
				'ROW_RSLT':''
			}
		);
		FormLookupEmployee('','',str,true,p,dsDtlCraftPersonIntPMResult,false);
	}
	else
	{	
		var p = new mRecordPersonPMResult
		(
			{
				'RESULT_PM_ID':IdPMResult,
				'SCH_PM_ID':'',
				'SERVICE_ID':cellSelectServicePMResult.data.SERVICE_ID,
				'ROW_SCH':dsDtlCraftPersonIntPMResult.data.items[CurrentPersonIntPMResult.row].data.ROW_SCH,
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonIntPMResult.data.items[CurrentPersonIntPMResult.row].data.COST,
				'REAL_COST':dsDtlCraftPersonIntPMResult.data.items[CurrentPersonIntPMResult.row].data.REAL_COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonIntPMResult.data.items[CurrentPersonIntPMResult.row].data.DESC_SCH_PERSON,
				'ROW': dsDtlCraftPersonIntPMResult.data.items[CurrentPersonIntPMResult.row].data.ROW,
				'ROW_RSLT':dsDtlCraftPersonIntPMResult.data.items[CurrentPersonIntPMResult.row].data.ROW_RSLT
			}
		);
		FormLookupEmployee('','',str,true,p,dsDtlCraftPersonIntPMResult,false);
	};
};

function CalcTotalPersonPMResult(idx,mBolGrid)
{
	var ds;
	var col;
	if(IsExtRepairPMResult === false)
	{
		ds=dsDtlCraftPersonIntPMResult;
		col=Ext.get('fieldcolCostCraftPersonPMResult')
	}
	else
	{
		ds=dsDtlCraftPersonExtPMResult;
		col=Ext.get('fieldcolCostCraftPersonExtPMResult')
	};
	
    var total=Ext.num(0);
	for(var i=0;i < ds.getCount();i++)
	{
		if (i === idx)
		{
			if (col != null) 
			{
				if (Ext.num(col.dom.value) != null) 
				{
					total += (Ext.num(col.dom.value));
				};
			};
		}
		else
		{
			total += Ext.num(ds.data.items[i].data.COST);
		};
	}
	
	Ext.get('txtTotalPersonPMResult').dom.value = formatCurrency(total);	
	if (mBolGrid === true)
	{
		Ext.get('txtTotalServiceRealPMResult').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalPMResultAllService(true,false)));
		Ext.get('txtLastCostPMResult').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalPMResultAllService(true,false)));
		
	};
};

function CalcTotalPMResultAllService(mBol,mBolPart)
{
	var ds;
	
	if (mBol != true)
	{
		if(IsExtRepairPMResult === false)
		{
			ds=dsDtlCraftPersonIntPMResultTemp;
		}
		else
		{
			ds=dsDtlCraftPersonExtPMResultTemp;
		};
	}
	else
	{
		ds=dsDtlPartPMResultTemp;
	};
	
    var total=Ext.num(0);
	for(var i=0;i < ds.getCount();i++)
	{
		if (mBol != true)
		{
			total += Ext.num(ds.data.items[i].data.COST);
		}
		else
		{	
			//total += Ext.num(ds.data.items[i].data.TOT_COST);
			total += Ext.num(ds.data.items[i].data.UNIT_COST * ds.data.items[i].data.QTY);
		};
	}
	
	if (mBolPart === true)
	{
		ds=dsDtlPartPMResultTemp;
	}
	else
	{
		if(IsExtRepairPMResult === false)
		{
			ds=dsDtlCraftPersonIntPMResultTemp;
		}
		else
		{
			ds=dsDtlCraftPersonExtPMResultTemp;
		};
	};
	
	for(var i=0;i < ds.getCount();i++)
	{
		if (mBolPart === true)
		{
			if ( ds.data.items[i].data.SERVICE_ID != cellSelectServicePMResult.data.SERVICE_ID)
			{
				//total += Ext.num(ds.data.items[i].data.TOT_COST);
				total += Ext.num(ds.data.items[i].data.UNIT_COST * ds.data.items[i].data.QTY);
			};
		}
		else
		{	
			if ( ds.data.items[i].data.SERVICE_ID != cellSelectServicePMResult.data.SERVICE_ID)
			{
				total += Ext.num(ds.data.items[i].data.COST);
			};
		};
	};
	
	return total;
};

function CalcTotalRealCostPersonPMResult(idx)
{
	var ds;
	var col;
	if(IsExtRepairPMResult === false)
	{
		ds=dsDtlCraftPersonIntPMResult;
		col=Ext.get('fieldcolRealCostCraftPersonPMResult')
	}
	else
	{
		ds=dsDtlCraftPersonExtPMResult;
		col=Ext.get('fieldcolRealCostCraftPersonExtPMResult')
	};
	
    var total=Ext.num(0);
	for(var i=0;i < ds.getCount();i++)
	{
		if (i === idx)
		{
			if (col != null) 
			{
				if (Ext.num(col.dom.value) != null) 
				{
					total += (Ext.num(col.dom.value));
				};
			};
		}
		else
		{
			total += Ext.num(ds.data.items[i].data.REAL_COST);
		}
	}

	//Ext.get('txtTotalRealCostPersonPMResult').dom.value = formatCurrency(total);
	
};

function CraftPersonExtPMResultDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdVendorPMResult',
			    header: nmVendIDResultPM,
			    dataIndex: 'VENDOR_ID',
			    width: 80,
				hidden:true,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdVendorPMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									if (Ext.get('fieldcolKdVendorPMResult').dom.value != '')
									{
										//str = ' where VENDOR_ID like ~%' + Ext.get('fieldcolKdVendorPMResult').dom.value + '%~';
                                                                                str = ' vendor_id like ~%' + Ext.get('fieldcolKdVendorPMResult').dom.value + '%~';
									};
						            GetLookupPersonExtPMResult(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMResult='colVendor';
							}
						}
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						str = "<div style='color:blue;'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colVendorNamePMResult',
			    header: nmVendNameResultPM,
			    dataIndex: 'VENDOR',
			    sortable: false,
				hidden:true,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolVendorNamePMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						           var str = '';
									if (Ext.get('fieldcolVendorNamePMResult').dom.value != '')
									{
										//str = ' where VENDOR like ~%' + Ext.get('fieldcolVendorNamePMResult').dom.value + '%~';
                                                                                str = ' vendor like ~%' + Ext.get('fieldcolVendorNamePMResult').dom.value + '%~';
									};
						            GetLookupPersonExtPMResult(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMResult='colVendor';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell, record) 
				{
			        var str='';
					if (value != undefined)
					{
						if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};

					};
			        return str;
			    }
			},
			{
			    id: 'colContactPersonCraftPersonPMResult',
			    header: nmPersonNameResultPM,
			    width: 250,
			    dataIndex: 'PERSON_NAME',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolContactPersonCraftPersonPMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colDescCraftPersonExtPMResult',
			    header: nmDescResultPM,
			    width: 250,
			    dataIndex: 'DESC_SCH_PERSON',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescCraftPersonExtPMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(value, cell, record) 
				{
			       var str='';
					if (value != undefined)
					{
						if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};

					};
			        return str;
			    }
			},
			{
			    id: 'colCostCraftPersonExtPMResult',
			    header: nmCostResultPM,
			    width: 90,
			    dataIndex: 'COST',
				align:'right',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostCraftPersonExtPMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPersonPMResult(CurrentPersonExtPMResult.row,true);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.COST) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.COST);
					};
				}
			},
			{
			    id: 'colRealCostCraftPersonExtPMResult',
			    header: nmRealCostResultPM,
			    width: 150,
				align:'right',
			    dataIndex: 'REAL_COST',
				hidden:true,
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolRealCostCraftPersonExtPMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalRealCostPersonPMResult(CurrentPersonIntPMResult.row);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.REAL_COST) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.REAL_COST);
					};
				}
			}
		]
	)

};

function GetLookupPersonExtPMResult(str)
{
	if (cellSelectPersonExtPMResult.data.ROW_SCH === '' || cellSelectPersonExtPMResult.data.ROW_SCH === undefined)
	{
		var p = new mRecordPersonPMResult
		(
			{
				'RESULT_PM_ID':IdPMResult,
				'SCH_PM_ID':'',
				'SERVICE_ID':cellSelectServicePMResult.data.SERVICE_ID,
				'ROW_SCH':'',
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonExtPMResult.data.items[CurrentPersonExtPMResult.row].data.COST,
				'REAL_COST':dsDtlCraftPersonExtPMResult.data.items[CurrentPersonIntPMResult.row].data.REAL_COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonExtPMResult.data.items[CurrentPersonExtPMResult.row].data.DESC_SCH_PERSON,
				'ROW':dsDtlCraftPersonExtPMResult.data.items[CurrentPersonExtPMResult.row].data.ROW,
				'ROW_RSLT':''
			}
		);
		FormLookupVendor(p,str,'',true,dsDtlCraftPersonExtPMResult,false);
	}
	else
	{	
		var p = new mRecordPersonPMResult
		(
			{
				'RESULT_PM_ID':IdPMResult,
				'SCH_PM_ID':'',
				'SERVICE_ID':cellSelectServicePMResult.data.SERVICE_ID,
				'ROW_SCH':dsDtlCraftPersonExtPMResult.data.items[CurrentPersonExtPMResult.row].data.ROW_SCH,
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonExtPMResult.data.items[CurrentPersonExtPMResult.row].data.COST,
				'REAL_COST':dsDtlCraftPersonExtPMResult.data.items[CurrentPersonIntPMResult.row].data.REAL_COST,
				'DESC_SCH_PERSON':dsDtlCraftPersonExtPMResult.data.items[CurrentPersonExtPMResult.row].data.DESC_SCH_PERSON,
				'ROW':dsDtlCraftPersonExtPMResult.data.items[CurrentPersonExtPMResult.row].data.ROW,
				'ROW_RSLT':dsDtlCraftPersonExtPMResult.data.items[CurrentPersonExtPMResult.row].data.ROW_RSLT
			}
		);
		FormLookupVendor(p,str,'',true,dsDtlCraftPersonExtPMResult,false);
	};
};

function PartInternalPMResultDetailColumModel() 
{
     return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdPartPMResult',
			    header: nmPartNumResultPM,
			    dataIndex: 'PART_ID',
			    width: 140,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdPartPMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListPartPMResult(false);
									
									if(Ext.get('fieldcolKdPartPMResult').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdPMResult + '~'
                                                                                str = ' category_id = ~' + varCatIdPMResult + '~'
										//str +=' and PART_ID like ~%' + Ext.get('fieldcolKdPartPMResult').dom.value + '%~';
                                                                                str +=' and part_id like ~%' + Ext.get('fieldcolKdPartPMResult').dom.value + '%~';
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdPMResult + '~'
                                                                                str = ' category_id = ~' + varCatIdPMResult + '~'
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									};
									GetLookupPartPMResult(str,CurrentPartPMResult.row,dsDtlPartPMResult);
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMResult='colPart';
							}
						}
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						str = "<div style='color:blue;'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colPartPMResult',
			    header: nmPartNameResultPM,
			    dataIndex: 'PART_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolPartPMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListPartPMResult(false);
									if(Ext.get('fieldcolPartPMResult').dom.value != '')
									{
										//str = ' where category_id = ~' + varCatIdPMResult + '~'
                                                                                str = ' category_id = ~' + varCatIdPMResult + '~'
										//str += ' and PART_NAME like ~%' + Ext.get('fieldcolPartPMResult').dom.value + '%~';
                                                                                str += ' and part_name like ~%' + Ext.get('fieldcolPartPMResult').dom.value + '%~';
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdPMResult + '~'
                                                                                str = ' category_id = ~' + varCatIdPMResult + '~'
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									};
									GetLookupPartPMResult(str,CurrentPartPMResult.row,dsDtlPartPMResult);
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMResult='colPart';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell,record) 
				{
					var str='';
					if( value != undefined)
					{
						if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};
					};
			        return str;
			    }
			},
			{
			    id: 'colQtyPartPMResult',
			    header: nmQtyResultPM,
				align:'center',
			    width: 50,
			    dataIndex: 'QTY',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolQtyPartPMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPartPMResult(CurrentPartPMResult.row,false,true);
								}; 
							}
						}
					}
				),
				renderer: function(value, cell,record) 
				{
					var str='';
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						str = "<div style='color:blue;'>" + value + "</div>";
					}
					else
					{
						str=value;
					};
			        return str;
			    }
			},
			{
			    id: 'colCostPartPMResult',
			    header:nmUnitCostResultPM,
			    width: 150,
				align:'right',
			    dataIndex: 'UNIT_COST',
				editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostPartPMResult',
					    allowBlank: false,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPartPMResult(CurrentPartPMResult.row,true,true);
								}; 
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.UNIT_COST) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.UNIT_COST);
					};
				}	
			},
			{
			    id: 'colTotCostPartInternalPMResult',
			    header: nmTotCostResultPM,
			    width: 150,
				align:'right',
			    dataIndex: 'TOT_COST',
				renderer: function(v, params, record) 
				{
					if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
					{
						return "<div style='color:blue;'>" + formatCurrency(record.data.UNIT_COST * record.data.QTY) + "</div>";
					}
					else
					{
						return formatCurrency(record.data.UNIT_COST * record.data.QTY);
					};
				}	
			},
			{
			    id: 'colDescPartPMResult',
			    header: nmDescResultPM,
			    width: 200,
			    dataIndex: 'DESC_SCH_PART',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescPartPMResult',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(value, params, record) 
				{
					var str='';
					if (value != undefined)
					{
						if (record.data.SCH_PM_ID != undefined && record.data.SCH_PM_ID !='')
						{
							str = "<div style='white-space:normal;color:blue;padding:2px 2px 2px 2px'>" + value + "</div>";
						}
						else
						{
							str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
						};
					};
					return str ;
			    }
			}
		]
	)
};

function GetLookupPartPMResult(str,idx,ds)
{
	if (IdPMResult === '' || IdPMResult === undefined)
	{
		var p = new mRecordPartPMResult
		(
			{
				'RESULT_PM_ID':IdPMResult,
				'SCH_PM_ID':'',
				'SERVICE_ID':cellSelectServicePMResult.data.SERVICE_ID,
				'PART_ID':'',
				'PART_NAME':'', 
				'QTY':ds.data.items[idx].data.QTY,
				'UNIT_COST':ds.data.items[idx].data.UNIT_COST,
				'TOT_COST':ds.data.items[idx].data.UNIT_COST * ds.data.items[idx].data.QTY,
				'DESC_SCH_PART':ds.data.items[idx].data.DESC_SCH_PART,
				'TAG':1,
				'ROW': ds.data.items[idx].data.ROW
			}
		);
		FormLookupPart(str,ds,p,true,'',false);
	}
	else
	{	
		var p = new mRecordPartPMResult
		(
			{
				'RESULT_PM_ID':IdPMResult,
				'SCH_PM_ID':'',
				'PART_ID':'',
				'SERVICE_ID':cellSelectServicePMResult.data.SERVICE_ID,
				'PART_NAME':'', 
				'QTY':ds.data.items[idx].data.QTY,
				'UNIT_COST':ds.data.items[idx].data.UNIT_COST,
				'TOT_COST': ds.data.items[idx].data.UNIT_COST * ds.data.items[idx].data.QTY,//ds.data.items[idx].data.TOT_COST,
				'DESC_SCH_PART':ds.data.items[idx].data.DESC_SCH_PART,
				'TAG':1,
				'ROW': ds.data.items[idx].data.ROW
			}
		);
		FormLookupPart(str,ds,p,false,idx,false);
	};
};


function CalcTotalPartPMResult(idx,mCost,mBolGrid)
{
	var total=Ext.num(0);
	for(var i=0;i < dsDtlPartPMResult.getCount();i++)
	{
		if (i === idx)
		{
			if (mCost===true)
			{
				if (Ext.get('fieldcolCostPartPMResult') != null) 
				{
					if (Ext.num(Ext.get('fieldcolCostPartPMResult').dom.value) != null) 
					{
						total += (Ext.num(Ext.get('fieldcolCostPartPMResult').dom.value) * Ext.num(dsDtlPartPMResult.data.items[i].data.QTY));
						
					};
			    };
			}
		    else
			{
			    if (Ext.get('fieldcolQtyPartPMResult') != null)
				{
			        if (Ext.num(Ext.get('fieldcolQtyPartPMResult').dom.value) != null) 
					{
			            total += (Ext.num(Ext.get('fieldcolQtyPartPMResult').dom.value) * Ext.num(dsDtlPartPMResult.data.items[i].data.UNIT_COST));
			        };
			    };
			}
		}
		else
		{
			total += Ext.num(dsDtlPartPMResult.data.items[i].data.UNIT_COST)*Ext.num(dsDtlPartPMResult.data.items[i].data.QTY);
		}
	}

	Ext.get('txtTotalPartPMResult').dom.value = formatCurrency(total);	
	if (mBolGrid === true)
	{
		Ext.get('txtTotalServiceRealPMResult').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalPMResultAllService(false,true)));
		Ext.get('txtLastCostPMResult').dom.value=formatCurrency(Ext.num(total) + Ext.num(CalcTotalPMResultAllService(false,true)));
		
	};
	
};

function getItemPanelDescPMResult() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    labelWidth: 75,
				height:30,
			    items:
				[
				    {
				        xtype: 'textarea',
				        fieldLabel: nmWONotesResultPM + ' ',
				        name: 'txtDeskripsiPMResult',
				        id: 'txtDeskripsiPMResult',
				        scroll: true,
						readOnly:true,
				        anchor: '100% 90%'//'100% 87%'
				    }
				]
			}
		]

    }
	return items ;
};


function getItemPanelNoAssetPMResult(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 57,
	    items:
		[
			{
			    columnWidth: 1,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoAssetPMResult2(lebar) 
				]
			}
		]
	}
    return items;
};


function getItemPanelNoAssetPMResult2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 57,
	    items:
		[
			{
			    columnWidth: .22,
			    layout: 'form',
			    border: false,
				labelWidth:75,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmAssetIdNameEntryResultPM + ' ',
					    name: 'txtKdMainAssetPMResult',
					    id: 'txtKdMainAssetPMResult',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
						readOnly:true,
					    name: 'txtNamaMainAsetPMResult',
					    id: 'txtNamaMainAsetPMResult',
					    anchor: '99.99%'
					}
				]
			},
			{
			    columnWidth: .25,
			    layout: 'form',
				labelWidth:60,
			    border: false,
			    items:
				[
					{
						 xtype: 'textfield',
					    fieldLabel: nmLocResultPM + ' ',
					    id: 'txtLocationPMResult',
					    name: 'txtLocationPMResult',
					    readOnly:true,
					    anchor: '100%'
					}
				]
			},
			{
			    columnWidth: .22,
			    layout: 'form',
			    border: false,
				labelWidth:63,
			    items:
				[
					{
						 xtype: 'textfield',
					    fieldLabel: nmDueDateResultPM + ' ',
					    id: 'dtpFinishPMResult',
					    name: 'dtpFinishPMResult',
					    readOnly: true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelVendorAppPMResult(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 57,
	    items:
		[
			{
			    columnWidth: .25,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmVendIDNameResultPM + ' ',
					    name: 'txtKdVendorPMResult',
					    id: 'txtKdVendorPMResult',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
						readOnly: true,
					    name: 'txtNamaVendorPMResult',
					    id: 'txtNamaVendorPMResult',
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype:'button',
						text:nmBtnVendorInfo,
						width:90,
						hideLabel:true,
						id: 'btnLookupVendorPMResult',
						handler:function() 
						{
							if(IsExtRepairPMResult === true)
							{
								if(dsLookVendorListPMResult != undefined)
								{
									if(dsLookVendorListPMResult.getCount() > 0 )
									{
										InfoVendorLookUp(dsLookVendorListPMResult.data.items[0])
									};
								};
							};
						}
					}
				]
			}
		]
	}
    return items;
};



function mComboMaksDataPMResult() 
{
    var cboMaksDataPMResult = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataPMResult',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: 'Maks.Data ',
		    width: 50,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountPMResult,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountPMResult = b.data.displayText;
			        RefreshDataPMResultFilter();
			    }
			}
		}
	);
    return cboMaksDataPMResult;
};


function PMResultSave(mBol) 
{	
	if (IdPMResult == '' || IdPMResult === undefined) 
	{
		Ext.Ajax.request
		(
			{
				url: WebAppUrl.UrlSaveData, //"./Datapool.mvc/CreateDataObj",
				params: getParamPMResult(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoPMResult(nmPesanSimpanSukses,nmHeaderSimpanData);
						RefreshDataPMResult();
						if(mBol === false)
						{
							IdPMResult=cst.ResultId;
							Ext.get('txtReffPMResult').dom.value=cst.Ref;
							LoadDataServiceResultPMResult(IdPMResult,varCatIdPMResult);
							LoadDataPartResultPMResult(IdPMResult,varCatIdPMResult);
							if (cellSelectServicePMResult != '' && cellSelectServicePMResult != undefined)
							{
								ReLoadDataPartResultPMResult(IdPMResult,varCatIdPMResult,cellSelectServicePMResult.data.SERVICE_ID);
							};
							GetJumlahTotalResultPersonPartPMResult();
							
							if (IsExtRepairPMResult === false)
							{
								LoadDataEmployeeResultPMResult(IdPMResult,varCatIdPMResult);
								if (cellSelectServicePMResult != '' && cellSelectServicePMResult != undefined)
								{
									ReLoadDataEmployeeResultPMResult(IdPMResult,varCatIdPMResult,cellSelectServicePMResult.data.SERVICE_ID);
								};
							}
							else
							{
								LoadDataVendorResultPMResult(IdPMResult,varCatIdPMResult);
								if (cellSelectServicePMResult != '' && cellSelectServicePMResult != undefined)
								{
									ReLoadDataVendorResultPMResult(IdPMResult,varCatIdPMResult,cellSelectServicePMResult.data.SERVICE_ID);
								};
							};
						};
						AddNewPMResult = false;
					}
					else if  (cst.success === false && cst.pesan===0)
					{
						ShowPesanWarningPMResult(nmPesanSimpanGagal,nmHeaderSimpanData);
					}
					else 
					{
						ShowPesanErrorPMResult(nmPesanSimpanError,nmHeaderSimpanData);
					};
				}
			}
		)
	}
	else 
	{
		Ext.Ajax.request
		 (
			{
				url: WebAppUrl.UrlSaveData, //"./Datapool.mvc/CreateDataObj",
				params: getParamPMResult(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoPMResult(nmPesanSimpanSukses,nmHeaderSimpanData);
						RefreshDataPMResult();
						if(mBol === false)
						{
							Ext.get('txtReffPMResult').dom.value=cst.Ref;
							LoadDataServiceResultPMResult(IdPMResult,varCatIdPMResult);
							LoadDataPartResultPMResult(IdPMResult,varCatIdPMResult);
							if (cellSelectServicePMResult != '' && cellSelectServicePMResult != undefined)
							{
								ReLoadDataPartResultPMResult(IdPMResult,varCatIdPMResult,cellSelectServicePMResult.data.SERVICE_ID);
							};
							GetJumlahTotalResultPersonPartPMResult();
							
							if (IsExtRepairPMResult === false)
							{
								LoadDataEmployeeResultPMResult(IdPMResult,varCatIdPMResult);
								if (cellSelectServicePMResult != '' && cellSelectServicePMResult != undefined)
								{
									ReLoadDataEmployeeResultPMResult(IdPMResult,varCatIdPMResult,cellSelectServicePMResult.data.SERVICE_ID);
								};
							}
							else
							{
								LoadDataVendorResultPMResult(IdPMResult,varCatIdPMResult);
								if (cellSelectServicePMResult != '' && cellSelectServicePMResult != undefined)
								{
									ReLoadDataVendorResultPMResult(IdPMResult,varCatIdPMResult,cellSelectServicePMResult.data.SERVICE_ID);
								};
							};
						};
					}
					else if  (cst.success === false && cst.pesan===0)
					{
						ShowPesanWarningPMResult(nmPesanSimpanGagal,nmHeaderSimpanData);
					}
					else 
					{
						ShowPesanErrorPMResult(nmPesanSimpanError,nmHeaderSimpanData);
					};
				}
			}
		)
	};
};


function ShowPesanWarningPMResult(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPMResult(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPMResult(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function PMResultDelete() 
{
    if ((IdPMResult != null) && (IdPMResult != '') && (IdPMResult != undefined) )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmTitleFormResultPM) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData, //"./Datapool.mvc/DeleteDataObj",
								params: getParamPMResult(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoPMResult(nmPesanHapusSukses,nmHeaderHapusData);
										PMResultAddNew();
										RefreshDataPMResult();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningPMResult(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorPMResult(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
};

function mComboCategoryPMResultView() 
{
	var Field = ['CATEGORY_ID','CATEGORY_NAME'];
	
    var dsCategoryPMResultView = new WebApp.DataStore({ fields: Field });
	
	Ext.TreeComboPMResultView = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treePMResultView = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreePMResultView  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						id:IdCategoryPMResultView,
						children: StrTreeComboPMResultView,
						autoScroll: true
					}
				) 
				treePMResultView.setRootNode(rootTreePMResultView); 
				
				this.list = treePMResultView
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboPMResultView.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treePMResultView
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				if (node.attributes.id === IdCategoryPMResultView)
				{
					this.setRawValue(valueCategoryPMResultView);
				}
				else
				{
					this.setRawValue(node.attributes.text);
				};
				
				selectCategoryPMResultView = node.attributes;
			
				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategoryPMResultView
		}
	);
	
	
	var cboCategoryPMResultView = new Ext.TreeComboPMResultView
	(
		{
			id:'cboCategoryPMResultView',
			fieldLabel: nmCatResultPM + ' ',
			width:120,
			value:valueCategoryPMResultView
		}
	);
    
    return cboCategoryPMResultView;
};

function GetRecordBaruPartPMResult()
{
	var p = new mRecordPartPMResult
	(
		{
			RESULT_PM_ID:'',
			SCH_PM_ID:'',
			SERVICE_ID: cellSelectServicePMResult.data.SERVICE_ID,
			PART_ID :'',
		    PART_NAME :'',
		    QTY :1,
		    UNIT_COST :0,
			TOT_COST:0,
			DESC_SCH_PART:'',
			TAG:1,
			ROW:GetUrutRowPMResult(dsDtlPartPMResult)
		}
	);
	return p;
};

function GetRecordBaruPersonPMResult(ds)
{
	var p = new mRecordPersonPMResult
	(
		{
			RESULT_PM_ID:'',
			SCH_PM_ID:'',
			SERVICE_ID:cellSelectServicePMResult.data.SERVICE_ID,
			ROW_SCH :'',
		    EMP_ID :'',
			EMP_NAME:'',
			VENDOR_ID :GetVendorTambahBarisPMResult(1),
			VENDOR:GetVendorTambahBarisPMResult(2),
		    PERSON_NAME :'',
			COST:'',
			REAL_COST:'',
			DESC_SCH_PERSON:'',
			ROW:GetUrutRowPMResult(ds),
			ROW_RSLT :''
		}
	);
	return p;
};

function GetVendorTambahBarisPMResult(x)
{
	var str='';
	if(IsExtRepairPMResult === true)
	{
		if(x=== 1)
		{
			str=Ext.get('txtKdVendorPMResult').dom.value;
		}
		else
		{
			str=Ext.get('txtNamaVendorPMResult').dom.value;
		};
	};
	return str;
};

function GetRecordBaruServicePMResult()
{
	var p = new mRecordServicePMResult
	(
		{
			RESULT_PM_ID:'',
			SCH_PM_ID:'',
			SERVICE_ID:'',
			SERVICE_NAME :'',
			CATEGORY_ID:varCatIdPMResult,
			TAG:1
		}
	);
	return p;
};

function GetNilaiCurrencyPMResult(dblNilai)
{
	for (var i = 0; i < dblNilai.length; i++) 
	{
		var y = dblNilai.substr(i, 1)
		if (y === '.') 
		{
			dblNilai = dblNilai.replace('.', '');
		}
	};
	
	return dblNilai;
};

function GetPersonIntServicePMResult(service_id)
{
	if (service_id != '' && service_id != undefined)
	{
		dsDtlCraftPersonIntPMResult.removeAll();
		for (var i = 0; i < dsDtlCraftPersonIntPMResultTemp.getCount() ; i++) 
		{
			if(dsDtlCraftPersonIntPMResultTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlCraftPersonIntPMResult.insert(dsDtlCraftPersonIntPMResult.getCount(), dsDtlCraftPersonIntPMResultTemp.data.items[i]);
			};
		};
	};
};

function GetPersonExtServicePMResult(service_id)
{
	if (service_id != '' && service_id != undefined)
	{
		dsDtlCraftPersonExtPMResult.removeAll();
		for (var i = 0; i < dsDtlCraftPersonExtPMResultTemp.getCount() ; i++) 
		{
			if(dsDtlCraftPersonExtPMResultTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlCraftPersonExtPMResult.insert(dsDtlCraftPersonExtPMResult.getCount(), dsDtlCraftPersonExtPMResultTemp.data.items[i]);
			};
		};
	};
};

function GetPartPMResult(service_id)
{
	if (service_id != '' && service_id != undefined)
	{
		dsDtlPartPMResult.removeAll();
		for (var i = 0; i < dsDtlPartPMResultTemp.getCount() ; i++) 
		{
			if(dsDtlPartPMResultTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlPartPMResult.insert(dsDtlPartPMResult.getCount(), dsDtlPartPMResultTemp.data.items[i]);
			};
		};
	};	
};


function HapusRowTempPMResult(ds,idx)
{
	if (cellSelectServicePMResult != '' && cellSelectServicePMResult.data != undefined)
	{
		if (cellSelectServicePMResult.data.SERVICE_ID != undefined || cellSelectServicePMResult.data.SERVICE_ID != '')
		{
			for (var i = 0; i < ds.getCount() ; i++) 
			{
				if(ds.data.items[i].data.SERVICE_ID === cellSelectServicePMResult.data.SERVICE_ID && ds.data.items[i].data.ROW === idx)
				{
					ds.removeAt(i);
				};
			}
		};
	};
};

function GetStrListServicePMResult()
{
	var str='';
	
	for (var i = 0; i < dsDtlServicePMResult.getCount() ; i++) 
	{
		if(dsDtlServicePMResult.getCount() === 1)
		{
			str +=  '\'' + dsDtlServicePMResult.data.items[i].data.SERVICE_ID + '\'';
		}
		else
		{
			if (i === dsDtlServicePMResult.getCount()-1 )
			{
				str +=  '\'' + dsDtlServicePMResult.data.items[i].data.SERVICE_ID + '\'';
			}
			else
			{
				str +=  '\'' + dsDtlServicePMResult.data.items[i].data.SERVICE_ID + '\'' + ',' ;
			};
		};
	}
	
	return str;
};


function GetUrutRowPMResult(ds)
{
	var x=1;
	if (ds != undefined && ds.data != undefined )
	{
		if ( ds.data.length > 0 )
		{
			x = ds.data.items[ds.getCount()-1].data.ROW + 1;
		};
	};
	
	return x;
};

function GetJumlahTotalPersonPartPMResult(mBolAwal)
{
//	var strPart = ' where category_id =~' + varCatIdPMResult + '~'
//		strPart += ' and sch_pm_id =~' + SchIdPMResult + '~'
//		strPart += ' and wo_pm_id =~' + IdWOPMResult + '~'

	var strPart = 'category_id = ~' + varCatIdPMResult + '~'
		strPart += ' and sch_pm_id = ~' + SchIdPMResult + '~'
		strPart += ' and wo_pm_id = ~' + IdWOPMResult + '~'

	var strPerson = strPart;
									
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess, //"./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahTotalCostPM',
				Params:	strPerson + '@^@' + strPart 
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.get('txtTotalServicePMResult').dom.value=formatCurrency(cst.TotalCost);
					if (mBolAwal=== true)
					{
						Ext.get('txtTotalServiceRealPMResult').dom.value=formatCurrency(cst.TotalCost);
						Ext.get('txtLastCostPMResult').dom.value=formatCurrency(cst.TotalCost);
					};
				}
				else
				{
					Ext.get('txtTotalServicePMResult').dom.value=formatCurrency(0);
					if (mBolAwal=== true)
					{
						Ext.get('txtTotalServiceRealPMResult').dom.value=formatCurrency(0);
						Ext.get('txtLastCostPMResult').dom.value=formatCurrency(0);
					};
				};
			}

		}
	);
};

function GetJumlahTotalResultPersonPartPMResult()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess, //"./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'GetProsesResultPartPersonPM',
				//Params:	'where RESULT_PM_ID =~' + IdPMResult + '~ and category_id =~' + varCatIdPMResult + '~'
                                Params:	'result_pm_id = ~' + IdPMResult + '~ and category_id = ~' + varCatIdPMResult + '~'
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.get('txtTotalServiceRealPMResult').dom.value=formatCurrency(cst.TotalCost);
				}
				else
				{
					Ext.get('txtTotalServiceRealPMResult').dom.value=formatCurrency(0);
				};
			}

		}
	);
};

function GetJumlahTotalResultPersonPMResult(str)
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess, //"./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahResultPersonPM',
				Params:	str
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.get('txtTotalPersonPMResult').dom.value=formatCurrency(cst.Jumlah);
				}
				else
				{
					Ext.get('txtTotalPersonPMResult').dom.value=formatCurrency(0);
				};
			}

		}
	);
};

function GetJumlahTotalResultPartPMResult(str)
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess, //"./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahResultPartPM',
				Params:	str
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.get('txtTotalPartPMResult').dom.value=formatCurrency(cst.Jumlah);
				}
				else
				{
					Ext.get('txtTotalPartPMResult').dom.value=formatCurrency(0);
				};
			}

		}
	);
};

function mComboStatusPMResultView() 
{
	var Field = ['STATUS_ID','STATUS_NAME'];
	
    dsStatusPMResultView = new WebApp.DataStore({ fields: Field });
    dsStatusPMResultView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ID',
                            Sort: 'status_id',
			    Sortdir: 'ASC',
			    target: 'ViewComboStatusVw',
			    //param: 'where status_id in ' + valueStatusPMResultView2
                            param: 'status_id in ' + valueStatusPMResultView2
			}
		}
	);
	
    var cboStatusPMResultView = new Ext.form.ComboBox
	(
		{
		    id: 'cboStatusPMResultView',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmStatusResultPM + ' ',
		    align: 'Right',
		    width:120,
		    store: dsStatusPMResultView,
		    valueField: 'STATUS_ID',
		    displayField: 'STATUS_NAME',
			value:valueStatusPMResultView,
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectStatusPMResultView = b.data.STATUS_ID;
			    }
			}
		}
	);
	
    return cboStatusPMResultView;
};

function GetStrTreeComboPMResultView()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess, //"./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetDataTreeComboSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeComboPMResultView= cst.arr;
			},
			failure:function()
			{
			    
			}
		}
	);
};

function GetVendorPMResult(mBolAwal)
{
	var strModul;
	var strParam;
	
	if(mBolAwal === true)
	{
		strModul='ProsesGetVendorPMWorkOrder';
		//strParam = 'where wo_pm_id =~' + IdWOPMResult + '~ and sch_pm_id=~' + SchIdPMResult + '~ and category_id=~' + varCatIdPMResult + '~';
                strParam = 'wo_pm_id = ~' + IdWOPMResult + '~ and sch_pm_id = ~' + SchIdPMResult + '~ and category_id = ~' + varCatIdPMResult + '~';
	}
	else
	{
		strModul='ProsesGetVendorPMResult';
		//strParam='where result_pm_id=~' + IdPMResult + '~ and category_id=~' + varCatIdPMResult + '~';
                strParam='result_pm_id = ~' + IdPMResult + '~ and category_id = ~' + varCatIdPMResult + '~';
	};
	
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess, //"./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: strModul,
				Params:	strParam
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.get('txtKdVendorPMResult').dom.value=cst.Id;
					Ext.get('txtNamaVendorPMResult').dom.value=cst.Nama;
					LoadVendorInfoDetailPMResult(cst.Id);
				}
				else
				{
					Ext.get('txtKdVendorPMResult').dom.value='';
					Ext.get('txtNamaVendorPMResult').dom.value='';
				};
			}
		}
	);
};

function LoadVendorInfoDetailPMResult(id)
{
	var fldDetail = ['VENDOR_ID','VENDOR','CONTACT1','CONTACT2','VEND_ADDRESS','VEND_CITY','VEND_PHONE1','VEND_PHONE2','VEND_POS_CODE','COUNTRY'];
	
	dsLookVendorListPMResult = new WebApp.DataStore({ fields: fldDetail });
	
	dsLookVendorListPMResult.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'VENDOR_ID',
                                Sort: 'vendor_id',
				Sortdir: 'ASC',
				target: 'LookupVendor',
				//param: 'where vendor_id=~' + id + '~'
                                param: 'vendor_id = ~' + id + '~'
			}
		}
	);
	return dsLookVendorListPMResult;
};










