
var CurrentServicePMWorkOrder =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPersonIntPMWorkOrder =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPersonExtPMWorkOrder =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentPartPMWorkOrder =
{
    data: Object,
    details: Array,
    row: 0
};


// var select & value combo ==============================
var selectCountPMWorkOrder = 50;
var selectStatusPMWorkOrderView=' 9999';
var selectCategoryPMWorkOrderView=' 9999';
var IdCategoryPMWorkOrderView=' 9999';
var valueStatusPMWorkOrderView=' All';
var valueCategoryPMWorkOrderView=' All';
var valueStatusPMWorkOrderView2='(\' 9999\',\'4\',\'5\')';
var DefaultStatusSchPMWorkOrderView='4';
var DefaultStatusWOPMWorkOrderView='5';
//=================================================

// var select grid ==============================
var rowSelectedPMWorkOrder;
var rowSelectedPMWorkOrderTemp;
var cellSelectServicePMWorkOrder;
var cellSelectPersonIntPMWorkOrder;
var cellSelectPersonExtPMWorkOrder;
var cellSelectPartPMWorkOrder;
//================================================


// var data store ===========================
var dsTRPMWorkOrderList;
var dsStatusPMWorkOrderView;
var dsDtlCraftPersonIntPMWorkOrder;
var dsDtlCraftPersonIntPMWorkOrderTemp;
var dsDtlCraftPersonIntPMWorkOrderTempView;
var dsDtlCraftPersonExtPMWorkOrder;
var dsDtlCraftPersonExtPMWorkOrderTemp;
var dsDtlCraftPersonExtPMWorkOrderTempView;
var dsDtlPartPMWorkOrder;
var dsDtlPartPMWorkOrderTemp;
var dsDtlServicePMWorkOrder;
var dsLookVendorListPMWorkOrder;
//==========================================



//var component===========================
var TRPMWorkOrderLookUps;
var vTabPanelRepairPMWorkOrder;
//=========================================


var AddNewPMWorkOrder = true;
var AppIdPMWorkOrder;
var SchIdPMWorkOrder;
var IsExtRepairPMWorkOrder=false;
var NowPMWorkOrder = new Date();
var FocusCtrlPMWorkOrder;
var varTabRepairPMWorkOrder=1;
var varCatIdPMWorkOrder;
var StrTreeComboPMWorkOrderView;
var rootTreePMWorkOrderView;
var treePMWorkOrderView;

	
	var mRecordServicePMWorkOrder = Ext.data.Record.create
	(
		[
		   {name: 'WO_PM_ID', mapping:'WO_PM_ID'},
		   {name: 'SCH_PM_ID', mapping:'SCH_PM_ID'},
		   {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
		   {name: 'SERVICE_NAME', mapping:'SERVICE_NAME'},
		   {name: 'CATEGORY_ID', mapping:'CATEGORY_ID'},
		   {name: 'ROW_SCH', mapping:'ROW_SCH'},
		   {name: 'DUE_DATE', mapping:'DUE_DATE'},
		   {name: 'TAG', mapping:'TAG'}
		]
	)
	
	var mRecordPartPMWorkOrder = Ext.data.Record.create
	(
		[
		   {name: 'SCH_PM_ID', mapping:'SCH_PM_ID'},
		   {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
		   {name: 'ROW_SCH', mapping:'ROW_SCH'},
		   {name: 'PART_ID', mapping:'PART_ID'},
		   {name: 'PART_NAME', mapping:'PART_NAME'},
		   {name: 'QTY', mapping:'QTY'},
		   {name: 'UNIT_COST', mapping:'UNIT_COST'},
		   {name: 'TOT_COST', mapping:'TOT_COST'},
		   {name: 'DESC_SCH_PART', mapping:'DESC_SCH_PART'},
		   {name: 'TAG', mapping:'TAG'},
		   {name: 'ROW', mapping:'ROW'}
		]
	);
	
var mRecordPersonPMWorkOrder = Ext.data.Record.create
	(
		[
		   {name: 'SCH_PM_ID', mapping:'SCH_PM_ID'},
		   {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
		   {name: 'ROW_SCH', mapping:'ROW_SCH'},
		   {name: 'EMP_ID', mapping:'EMP_ID'},
		   {name: 'EMP_NAME', mapping:'EMP_NAME'},
		   {name: 'VENDOR_ID', mapping:'VENDOR_ID'},
		   {name: 'VENDOR', mapping:'VENDOR'},
		   {name: 'PERSON_NAME', mapping:'PERSON_NAME'},
		   {name: 'COST', mapping:'COST'},
		   {name: 'DESC_WO_PM_PERSON', mapping:'DESC_WO_PM_PERSON'},
		   {name: 'ROW_PERSON', mapping:'ROW_PERSON'},
		   {name: 'ROW', mapping:'ROW'}
		]
	);
	
	var mRecVendorPMWorkOrder = Ext.data.Record.create
	(
		[
		   {name: 'KD', mapping:'KD'},
		   {name: 'NAME', mapping:'NAME'}
		]
	);

CurrentPage.page = getPanelPMWorkOrder(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelPMWorkOrder(mod_id) 
{
    var Field =['WO_PM_ID','EMP_ID','WO_PM_DATE','WO_PM_FINISH_DATE','DESC_WO_PM','ASSET_MAINT_ID','YEARS','SCH_PM_ID','ASSET_MAINT_NAME','ASSET_MAINT','EMP_NAME','DEPT_ID','DEPT_NAME','STATUS_ID','CATEGORY_ID','LOCATION_ID','LOCATION','DEPT_ID_ASSET','DEPT_NAME_ASSET','SERVICE_ID','ROW_SCH','DUE_DATE','METER','DESC_SCH_PM','STATUS_ID_SCH','SERVICE_NAME'];

	 var chkServicePMWorkOrder = new Ext.grid.CheckColumn
	(
		{
			id: 'chkServicePMWorkOrder',
			header: "Select",
			align: 'center',
			disabled:true,
			dataIndex: 'SELECT',
			width: 50
		}
	);
	
    dsTRPMWorkOrderList = new WebApp.DataStore({ fields: Field });

    var grListTRPMWorkOrder = new Ext.grid.EditorGridPanel
	(
		{
		    stripeRows: true,
		    store: dsTRPMWorkOrderList,
		    anchor: '100% 92%',
		    columnLines: true,
			autoScroll:true,
		    border: false,
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
					        rowSelectedPMWorkOrder = dsTRPMWorkOrderList.getAt(row);
							var x=rowSelectedPMWorkOrder.data.WO_PM_ID;
							if ( x != null && x !='' && x != undefined)
							{
								var y=rowSelectedPMWorkOrder.data;
								var strEmp='';
								var strVend='';
//								var str = 'where category_id =~' + y.CATEGORY_ID + '~ and sch_pm_id=~'  + y.SCH_PM_ID + '~'
//			str += ' and service_id =~' + y.SERVICE_ID + '~ and row_sch =' + y.ROW_SCH
//			str += ' and wo_pm_id =~' + x + '~'
								var str = 'category_id = ~' + y.CATEGORY_ID + '~ and sch_pm_id = ~'  + y.SCH_PM_ID + '~'
			str += ' and service_id = ~' + y.SERVICE_ID + '~ and row_sch = ' + y.ROW_SCH
			str += ' and wo_pm_id = ~' + x + '~'

                                                                strEmp = str + ' and emp_id is not null and emp_id != ~ ~'
								strVend = str + ' and vendor_id is not null and vendor_id != ~ ~'
								LoadDataEmployeePMWorkOrderTempView(strEmp);
								LoadDataVendorPMWorkOrderTempView(strVend);
							};
					    }
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					if (rowSelectedPMWorkOrder != undefined) 
					{
						PMWorkOrderLookUp(rowSelectedPMWorkOrder.data);
					}
					else 
					{
						ShowPesanWarningPMWorkOrder(nmWarningSelectEditData,nmEditData)
					};
				},
				rowclick: function (sm, ridx, cidx)
				{
					rowSelectedPMWorkOrder = dsTRPMWorkOrderList.getAt(ridx);
					var x=rowSelectedPMWorkOrder.data.WO_PM_ID;
					if ( x != null && x !='' && x != undefined)
					{
						var y=rowSelectedPMWorkOrder.data;
						var strEmp='';
						var strVend='';
//						var str = 'where category_id =~' + y.CATEGORY_ID + '~ and sch_pm_id=~'  + y.SCH_PM_ID + '~'
//	str += ' and service_id =~' + y.SERVICE_ID + '~ and row_sch =' + y.ROW_SCH
//	str += ' and wo_pm_id =~' + x + '~'
						var str = 'category_id = ~' + y.CATEGORY_ID + '~ and sch_pm_id = ~'  + y.SCH_PM_ID + '~'
	str += ' and service_id = ~' + y.SERVICE_ID + '~ and row_sch = ' + y.ROW_SCH
	str += ' and wo_pm_id = ~' + x + '~'
						strEmp = str + ' and emp_id is not null and emp_id != ~ ~'
						strVend = str + ' and vendor_id is not null and vendor_id != ~ ~'
						LoadDataEmployeePMWorkOrderTempView(strEmp);
						LoadDataVendorPMWorkOrderTempView(strVend);
					};
				}
			},
		     cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),//chkServicePMWorkOrder,
					{
					    id: 'colWOImagePMWorkOrderView',
					    header: nmColWOPM,
					    dataIndex: 'WO_PM_ID',
					    sortable: true,
					    width: 30,
						align:'center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store) 
						{
							if ( value === null )
							{
								metaData.css = 'BeforeWO';
							}
							else
							{
								metaData.css = 'WO';
							};
								return '';     
						}
					},
					{
					    id: 'colWOIdViewPMWorkOrder', //+ Code
					    header: nmColIDWOPM,
					    dataIndex: 'WO_PM_ID',
					    sortable: true,
					    width: 100
					},
					{
					    id: 'colWODateViewPMWorkOrder',
					    header: nmColDateWOPM,
					    dataIndex: 'WO_PM_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
							if (record.data.WO_PM_DATE != null)
							{
								return ShowDate(record.data.WO_PM_DATE);
							}
							else
							{
								return '';
							}
					    }
					},
					{
					    id: 'colStartDateViewPMWorkOrder',
					    header: nmStartDateWOPM,
					    dataIndex: 'DUE_DATE',
					    sortable: true,
					    width: 100,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.DUE_DATE);
					    }
					},
					{
					    id: 'colWODateViewPMWorkOrder', //+ Code
					    header: nmColAssetIdNameWOPM,
					    dataIndex: 'ASSET_MAINT',
					    sortable: true,
					    width: 250
					},
					{
					    id: 'colLocAsetViewPMWorkOrder',
					    header: nmColLocWOPM,
					    dataIndex: 'LOCATION',
					    width: 100
					},
					{
					    id: 'colServiceViewPMWorkOrder', 
					    header: nmServWOPM,
					    dataIndex: 'SERVICE_NAME',
					    sortable: true,
					    width: 150
					},
					{
					    id: 'colDescViewPMWorkOrder',
					    header: nmDescWOPM,
					    dataIndex: 'DESC_WO_PM',
					    width: 300
					}
				]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsTRPMWorkOrderList,
            pageSize: selectCountPMWorkOrder,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditPMWorkOrder',
				    text: nmEditData,
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedPMWorkOrder != undefined) 
						{
							PMWorkOrderLookUp(rowSelectedPMWorkOrder.data);
						}
						else 
						{
							ShowPesanWarningPMWorkOrder(nmWarningSelectEditData,nmEditData)
						};
				    }
				}, ' ', '-',
				{
					xtype: 'checkbox',
					id: 'chkWithTglPMWorkOrder',
					hideLabel:true,
					checked: false,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilterPMWorkOrder').dom.disabled=false;
							Ext.get('dtpTglAkhirFilterPMWorkOrder').dom.disabled=false;
							
						}
						else
						{
							Ext.get('dtpTglAwalFilterPMWorkOrder').dom.disabled=true;
							Ext.get('dtpTglAkhirFilterPMWorkOrder').dom.disabled=true;
							
							
						};
					}
				}
				, ' ', '-', nmWODateWOPM + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmWODateWOPM + ' ',
				    id: 'dtpTglAwalFilterPMWorkOrder',
				    format: 'd/M/Y',
				    value: NowPMWorkOrder,
				    width: 100,
				    onInit: function() { }
				}, ' ', ' ' + nmSd + ' ', ' ', {
				    xtype: 'datefield',
				    fieldLabel: nmSd + ' ',
				    id: 'dtpTglAkhirFilterPMWorkOrder',
				    format: 'd/M/Y',
				    value: NowPMWorkOrder,
				    width: 100
				}
			]
		}
	);
	
	var LegendViewPMWorkOrder = new Ext.Panel
	(
		{
		    id: 'LegendViewPMWorkOrder',
		    region: 'center',
			border:false,
			bodyStyle: 'padding:0px 7px 0px 7px',
		    layout: 'column',
			frame:true,
			//height:30,
			anchor: '100% 8%',
			autoScroll:false,
			items:
			[	
				{
					columnWidth: .15,
					layout: 'form',
					//height:30,
					anchor: '100% 8%',
					border: false,
					html: '<img src="' + WebAppUrl.UrlImg + '/images/icons/16x16/Before.png" class="text-desc-legend"/>' + " " + " On Schedule" 
				},
				{
					columnWidth: .2,
					layout: 'form',
					border: false,
					//height:30,
					anchor: '100% 8%',
					html: '<img src="' + WebAppUrl.UrlImg + '/images/icons/16x16/After.png" class="text-desc-legend"/>' + " " + " Work Order"
				}
			]
		
		}
	)


    var FormTRPMWorkOrder = new Ext.Panel
	(
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormWOPM,
		    border: false,
		    shadhow: true,
			autoScroll:false,
		    iconCls: 'PMWorkOrder',
		    margins: '0 5 5 0',
		    items: [grListTRPMWorkOrder,LegendViewPMWorkOrder],
		    tbar:
			[
				nmCatWOPM + ' : ', ' ', mComboCategoryPMWorkOrderView(), 
				//,' ', '-','Repair : ', ' ',mComboRepairPMWorkOrderView(),
				' ', '-',nmStatusWOPM + ' : ', ' ',
				mComboStatusPMWorkOrderView(),
				' ', 
				{
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    anchor: '25%',
				    handler: function(sm, row, rec) 
					{
				        RefreshDataPMWorkOrderFilter();
				    }
				}
			],
		    listeners:
			{
			    'afterrender': function() 
				{
			    }
			}
		}
	);
    RefreshDataPMWorkOrder();
    RefreshDataPMWorkOrder();
	GetStrTreeComboPMWorkOrderView();
    return FormTRPMWorkOrder

};

function RefreshDataPMWorkOrder() 
{
    dsTRPMWorkOrderList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountPMWorkOrder,
					//Sort: 'DUE_DATE',
                                        Sort: 'due_date',
					Sortdir: 'ASC',
					target: 'ViewWorkOrderPM',
					param: ''
				}
			}
		);       
	
	rowSelectedPMWorkOrder = undefined;
	return dsTRPMWorkOrderList;
};

function RefreshDataPMWorkOrderFilter() 
{

	var KataKunci='';
	
	if (Ext.get('cboCategoryPMWorkOrderView').getValue() != '' && selectCategoryPMWorkOrderView != undefined)
    { 
		if(selectCategoryPMWorkOrderView.id != undefined)
		{
			if(selectCategoryPMWorkOrderView.id != ' 9999')
			{
				if (selectCategoryPMWorkOrderView.leaf === false)
				{
					//KataKunci =' where subst(category_id,1,' + selectCategoryPMWorkOrderView.id.length + ')=~' +  selectCategoryPMWorkOrderView.id + '~'
                                        KataKunci ='subst(category_id,1,' + selectCategoryPMWorkOrderView.id.length + ') = ~' +  selectCategoryPMWorkOrderView.id + '~'
				}
				else
				{
					//KataKunci =' WHERE Category_id=~'+ selectCategoryPMWorkOrderView.id + '~'
                                        KataKunci =' category_id = ~'+ selectCategoryPMWorkOrderView.id + '~'
				};
			};
		};
	};
		
    // if (Ext.get('cboRepairPMWorkOrderView').getValue() != '' && selectRepairPMWorkOrderView != undefined)
    // { 
		// if(selectRepairPMWorkOrderView != 99)
		// {
			// if (KataKunci === '')
			// {
				// KataKunci = ' where IS_EXT_REPAIR =  ' + selectRepairPMWorkOrderView ;
			// }
			// else
			// {
				// KataKunci += ' and  IS_EXT_REPAIR =  ' + selectRepairPMWorkOrderView ;
			// }; 
		// };
	// };
	
	if (Ext.get('cboStatusPMWorkOrderView').getValue() != '' && selectStatusPMWorkOrderView != undefined)
    { 
		if(selectStatusPMWorkOrderView != ' 9999')
		{
			if (selectStatusPMWorkOrderView === DefaultStatusSchPMWorkOrderView)
			{
				if (KataKunci === '' )
				{
					//KataKunci = ' where wo_pm_id is null';
                                        KataKunci = ' wo_pm_id is null';
				}
				else
				{
					KataKunci += ' and wo_pm_id is null';
				};
			}
			else
			{
				if (KataKunci === '' )
				{
					//KataKunci = ' where wo_pm_id is not null';
                                        KataKunci = ' wo_pm_id is not null';
				}
				else
				{
					KataKunci += ' and wo_pm_id is not null';
				};
			};
		};
	};
	
	if( Ext.get('chkWithTglPMWorkOrder').dom.checked === true )
	{
			KataKunci += '@^@' + Ext.get('dtpTglAwalFilterPMWorkOrder').getValue() + '&&'+ Ext.get('dtpTglAkhirFilterPMWorkOrder').getValue();
	};
        
    if (KataKunci != undefined) 
    {  
		 dsTRPMWorkOrderList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountPMWorkOrder,
					//Sort: 'DUE_DATE',
                                        Sort: 'due_date',
					Sortdir: 'ASC',
					target: 'ViewWorkOrderPM',
					param: KataKunci
				}
			}
		);
    }
	else
	{
		RefreshDataPMWorkOrder();
	};
    
	return dsTRPMWorkOrderList;
};

function PMWorkOrderLookUp(rowdata) 
{
	AppIdPMWorkOrder='';
    SchIdPMWorkOrder='';
    IsExtRepairPMWorkOrder=false;
    varTabRepairPMWorkOrder=1;
	varCatIdPMWorkOrder='';
	TRPMWorkOrderLookUps='';
	cellSelectServicePMWorkOrder='';
	cellSelectPersonIntPMWorkOrder='';
	cellSelectPersonExtPMWorkOrder='';
	cellSelectPartPMWorkOrder='';
	rowSelectedPMWorkOrderTemp='';

    var lebar = 750;
    TRPMWorkOrderLookUps = new Ext.Window
	(
		{
		    id: 'gridPMWorkOrder',
		    title: nmTitleFormWOPM,
		    closeAction: 'destroy',
		    width: lebar,
		    height: 549,//559,
		    border: false,
			resizable:false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'PMWorkOrder',
		    modal: true,
		    items: getFormEntryTRPMWorkOrder(lebar),
		    listeners:
			{
			    activate: function() 
				{
				    if (varBtnOkLookupEmp === true)
					{
						var j=dsDtlCraftPersonIntPMWorkOrder.getCount()-1;
						 dsDtlCraftPersonIntPMWorkOrderTemp.insert(dsDtlCraftPersonIntPMWorkOrderTemp.getCount(),dsDtlCraftPersonIntPMWorkOrder.data.items[j]);
						 GetPersonIntServicePMWorkOrder(cellSelectServicePMWorkOrder);
						 varBtnOkLookupEmp=false;
					};
					
					 // if (varBtnOkLookupVendor === true)
					// {
						// var j=dsDtlCraftPersonExtPMWorkOrder.getCount()-1;
						 // dsDtlCraftPersonExtPMWorkOrderTemp.insert(dsDtlCraftPersonExtPMWorkOrderTemp.getCount(), 	dsDtlCraftPersonExtPMWorkOrder.data.items[j]);
						  // GetPersonExtServicePMWorkOrder(cellSelectServicePMWorkOrder);
						 // varBtnOkLookupVendor=false;
					// };
					
					 // if (varBtnOkLookupPart === true)
					// {
						// var j=dsDtlPartPMWorkOrder.getCount()-1;
						 // dsDtlPartPMWorkOrderTemp.insert(dsDtlPartPMWorkOrderTemp.getCount(), dsDtlPartPMWorkOrder.data.items[j]);
						 // GetPartPMWorkOrder(cellSelectServicePMWorkOrder.data.SERVICE_ID);
						 // CalcTotalPartPMWorkOrder('','',false)
						 // varBtnOkLookupPart=false;
					// };
			    },
			    afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedPMWorkOrder=undefined;
					RefreshDataPMWorkOrderFilter();
				}
			}
		}
	);

    TRPMWorkOrderLookUps.show();
    if (rowdata == undefined) 
	{
        PMWorkOrderAddNew();
    }
    else 
	{
        TRPMWorkOrderInit(rowdata)
    }

};

function getFormEntryTRPMWorkOrder(lebar) 
{
    var pnlTRPMWorkOrder = new Ext.FormPanel
	(
		{
		    id: 'PanelTRPMWorkOrder',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    bodyStyle: 'padding:10px 10px 0px 10px',
			height:526,
		    anchor: '100%',
		    width: lebar,
		    border: false,
			//autoScroll:true,
		    items: [getItemPanelInputPMWorkOrder(lebar)],
		    tbar:
			[
				{
				    text: nmTambah,
				    id: 'btnTambahPMWorkOrder',
				    tooltip: nmTambah,
				    iconCls: 'add',
				    handler: function() 
					{
				        PMWorkOrderAddNew();
				    }
				}, '-',
				{
				    text: nmSimpan,
				    id: 'btnSimpanPMWorkOrder',
				    tooltip: nmSimpan,
				    iconCls: 'save',
				    handler: function() 
					{
				        PMWorkOrderSave(false);
				        RefreshDataPMWorkOrderFilter();
				    }
				}, '-',
				{
				    text: nmSimpanKeluar,
				    id: 'btnSimpanKeluarPMWorkOrder',
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{
						var x =  PMWorkOrderSave(true);
				         RefreshDataPMWorkOrderFilter();
						if (x===undefined)
						{
							TRPMWorkOrderLookUps.close();
						};    
				    }
				}, '-',
				{
				    text: nmHapus,
				    id: 'btnHapusPMWorkOrder',
				    tooltip: nmHapus,
				    iconCls: 'remove',
					disabled:false,
				    handler: function() 
					{
				        PMWorkOrderDelete();
				        RefreshDataPMWorkOrderFilter();
				    }
				}, '-',
				{
				    text: nmLookup,
				    id: 'btnLookupPMWorkOrder',
				    tooltip: nmLookup,
				    iconCls: 'find',
				    handler: function() 
					{
						if (FocusCtrlPMWorkOrder === 'colVendorRdo' && varTabRepairPMWorkOrder === 2 && cellSelectServicePMWorkOrder != '' &&  cellSelectServicePMWorkOrder != undefined)
						{	
							//var p = GetRecordBaruPersonPMWorkOrder(dsDtlCraftPersonExtPMWorkOrder);
							//FormLookupVendor(p,'','',true,dsDtlCraftPersonExtPMWorkOrder,true);
							
							 var criteria='';
							if (Ext.get('txtNamaVendorPMWorkOrder').dom.value != '')
							{
								//criteria = ' where VENDOR like ~%' + Ext.get('txtNamaVendorPMWorkOrder').dom.value + '%~';
                                                                criteria = ' vendor like ~%' + Ext.get('txtNamaVendorPMWorkOrder').dom.value + '%~';
							};
							FormLookupVendor(GetItemLookupVendorPMWorkOrder(),criteria);
						}
						else if (FocusCtrlPMWorkOrder === 'colEmpRdo' && varTabRepairPMWorkOrder === 2 && cellSelectServicePMWorkOrder != '' &&  cellSelectServicePMWorkOrder != undefined)
						{	
							//var p = GetRecordBaruPersonPMWorkOrder(dsDtlCraftPersonExtPMWorkOrder);
							//FormLookupVendor(p,'','',true,dsDtlCraftPersonExtPMWorkOrder,true);
							 var criteria='';
							if (Ext.get('txtNamaVendorPMWorkOrder').dom.value != '')
							{
								//criteria = ' where VENDOR like ~%' + Ext.get('txtNamaVendorPMWorkOrder').dom.value + '%~';
                                                                criteria = ' vendor like ~%' + Ext.get('txtNamaVendorPMWorkOrder').dom.value + '%~';
							};
							FormLookupVendor(GetItemLookupVendorPMWorkOrder(),criteria);
						}
						else if (FocusCtrlPMWorkOrder === 'colEmp'&& cellSelectServicePMWorkOrder != '' &&  cellSelectServicePMWorkOrder != undefined)
						{
							var p = GetRecordBaruPersonPMWorkOrder(dsDtlCraftPersonIntPMWorkOrder);
							FormLookupEmployee('','','',true,p,dsDtlCraftPersonIntPMWorkOrder,true);
							FocusCtrlPMWorkOrder='';
						}
						else if ((FocusCtrlPMWorkOrder === 'colPart' || varTabRepairPMWorkOrder === 3 ) && FocusCtrlPMWorkOrder != 'txtPIC')
						{
							var p = GetRecordBaruPartPMWorkOrder();
							//var	str = ' where category_id = ~' + varCatIdPMWorkOrder + '~'
                                                        var	str = ' category_id = ~' + varCatIdPMWorkOrder + '~'
							var strList=GetStrListPartPMWorkOrder(true);
							if (strList !='')
							{
								//str += ' and PART_ID not in ( ' + strList + ') '
                                                                str += ' and part_id not in ( ' + strList + ') '
							};
							FormLookupPart(str,dsDtlPartPMWorkOrder,p,true,'',true);
							FocusCtrlPMWorkOrder='';
						}
						else if ((FocusCtrlPMWorkOrder === 'colService' || varTabRepairPMWorkOrder === 1 ) && FocusCtrlPMWorkOrder != 'txtPIC')
						{
							var strList = GetStrListServicePMWorkOrder(false);
								var strList2 = GetStrListServicePMWorkOrder(true);
								
								if (strList !='')
								{
									//strList = ' and SERVICE_ID not in ( ' + strList + ') '
                                                                        strList = ' and service_id not in ( ' + strList + ') '
								};
								
								// if (strList2 !='')
								// {
									// strList2 = ' and ROW_SCH not in ( ' + strList2 + ') '
								// };
								
								strList2 += strList;
							var p = GetRecordBaruServicePMWorkOrder()
								if(rowSelectedPMWorkOrderTemp != '' && rowSelectedPMWorkOrderTemp != undefined)
								{
									//str = ' where sch_pm_id=~'+rowSelectedPMWorkOrderTemp.SCH_PM_ID + '~ and ' ;
                                                                        str = ' sch_pm_id = ~'+rowSelectedPMWorkOrderTemp.SCH_PM_ID + '~ and ' ;
									str += 'category_id = ~'+rowSelectedPMWorkOrderTemp.CATEGORY_ID + '~ and ' ;
									str += 'due_date = ~'+ShowDate(rowSelectedPMWorkOrderTemp.DUE_DATE) + '~  ' ;
									str += strList
								};
								
								FormLookupServiceWOPM(str,p,dsDtlServicePMWorkOrder)
				
							}
						else if (FocusCtrlPMWorkOrder === 'txtPIC')
						{
							FormLookupEmployee('txtKdApproverPMWorkOrder','txtNamaApproverPMWorkOrder','');
							FocusCtrlPMWorkOrder='';
						};
				    }
				}, 
				'-', '->', '-',
				{
				    text: nmCetak,
					id:'btnCetakPMWorkOrder',
				    tooltip: nmCetak,
				    iconCls: 'print',
				    handler: function() {
				    cKriteria = Ext.get('txtKdMainAssetPMWorkOrder').dom.value + ' - ' + Ext.get('txtNamaMainAsetPMWorkOrder').dom.value + '###1###';
					cKriteria += 'Preventive Maintenance' + '###2###';
					cKriteria += Ext.get('dtpPengerjaanPMWorkOrder').dom.value + '###3###';						
					cKriteria += AppIdPMWorkOrder + '###4###';
					ShowReport('', '910005', cKriteria);
					}
				}
			]
		}
	); 
	
	


    var FormTRPMWorkOrder = new Ext.Panel
	(
		{
		    id: 'FormTRPMWorkOrder',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRPMWorkOrder]
		}
	);

    return FormTRPMWorkOrder
};
///---------------------------------------------------------------------------------------///



function TRPMWorkOrderInit(rowdata)
{
	rowSelectedPMWorkOrderTemp=rowdata;
	varCatIdPMWorkOrder=rowdata.CATEGORY_ID
	SchIdPMWorkOrder=rowdata.SCH_PM_ID;
	Ext.get('txtKdMainAssetPMWorkOrder').dom.value=rowdata.ASSET_MAINT_ID;
	Ext.get('txtNamaMainAsetPMWorkOrder').dom.value=rowdata.ASSET_MAINT_NAME;
	Ext.get('txtLocationPMWorkOrder').dom.value=rowdata.LOCATION;
	
	LoadDataPartPMWorkOrderTemp(varCatIdPMWorkOrder);
	
	
	if (rowdata.WO_PM_ID === null || rowdata.WO_PM_ID === undefined || rowdata.WO_PM_ID === '')
	{
		AddNewPMWorkOrder = true;
		Ext.get('rdoInternalApproveReq').dom.checked=true;
		IsExtRepairPMWorkOrder=false;
		AppIdPMWorkOrder='';
		DisabledVendorPMWorkOrder(true);
		//var str = 'where category_id =~' + varCatIdPMWorkOrder + '~ and sch_pm_id = ~' + SchIdPMWorkOrder + '~ and service_id = ~' + rowdata.SERVICE_ID + '~ and row_sch = ' + rowdata.ROW_SCH
                var str = 'category_id = ~' + varCatIdPMWorkOrder + '~ and sch_pm_id = ~' + SchIdPMWorkOrder + '~ and service_id = ~' + rowdata.SERVICE_ID + '~ and row_sch = ' + rowdata.ROW_SCH
		LoadDataServicePMWorkOrder(str);
		GetJumlahTotalPersonPartPMWorkOrder('');
	}
	else
	{
		 AddNewPMWorkOrder=false;
		 AppIdPMWorkOrder=rowdata.WO_PM_ID;
		 Ext.get('txtKdApproverPMWorkOrder').dom.value=rowdata.EMP_ID;
		 Ext.get('txtNamaApproverPMWorkOrder').dom.value=rowdata.EMP_NAME;
		 Ext.get('dtpPengerjaanPMWorkOrder').dom.value=ShowDate(rowdata.WO_PM_DATE);
		 Ext.get('dtpSelesaiPMWorkOrder').dom.value=ShowDate(rowdata.WO_PM_FINISH_DATE);
			
		//var str = 'where category_id =~' + varCatIdPMWorkOrder + '~ and sch_pm_id=~'  + SchIdPMWorkOrder + '~'
                var str = 'category_id = ~' + varCatIdPMWorkOrder + '~ and sch_pm_id = ~'  + SchIdPMWorkOrder + '~'
		    str += ' and wo_pm_id = ~' + rowdata.WO_PM_ID + '~'
		
		var str2 = ' and service_id = ~' + rowdata.SERVICE_ID + '~ and row_sch = ' + rowdata.ROW_SCH
		
			
		var strListServ = GetStrListServicePMWorkOrder(false);
		var strListRowSch = GetStrListServicePMWorkOrder(true);
		var strPerson ='';
			strPerson += str
			
			if (strListServ != '')
			{
				strPerson +=' and service_id in (' + strListServ + ')';
			};
			
			if (strListRowSch != '')
			{
				strPerson +=' and row_sch in (' + strListRowSch + ')';
			};	
		
		GetJumlahTotalPersonPartPMWorkOrder(strPerson);
		LoadDataServiceWOPMWorkOrder(str);
		 
		 if (dsDtlCraftPersonIntPMWorkOrderTempView.getCount() > 0 )
		{
			IsExtRepairPMWorkOrder=false;
			DisabledVendorPMWorkOrder(true);
			LoadDataEmployeePMWorkOrderTemp(str);
			Ext.get('rdoInternalApproveReq').dom.checked=true;
		}
		else if( dsDtlCraftPersonExtPMWorkOrderTempView.getCount() > 0 )
		{
			IsExtRepairPMWorkOrder=true;
			DisabledVendorPMWorkOrder(false);
			LoadDataVendorPMWorkOrderTemp(str);
			GetVendorPMWorkOrder();
			Ext.get('rdoExternalApproveReq').dom.checked=true;
		}
		else
		{
			IsExtRepairPMWorkOrder=false;
		};
		 
		 
		 if (rowdata.DESC_WO_PM != null)
		 {
			Ext.get('txtDeskripsiPMWorkOrder').dom.value=rowdata.DESC_WO_PM;
		 }
		 else
		 {
			Ext.get('txtDeskripsiPMWorkOrder').dom.value='';
		 };
	};
	
};

function mEnabledPMWorkOrder(mBol)
{
	 Ext.get('btnLookupPMWorkOrder').dom.disabled=mBol;
	 //document.getElementById('btnTambahBrsServicePMWorkOrder').disabled=mBol;
	 Ext.get('btnTambahBrsServicePMWorkOrder').dom.disabled=mBol;
	 Ext.get('btnHpsBrsServicePMWorkOrder').dom.disabled=mBol;
	 Ext.get('rdoInternalApproveReq').dom.disabled=mBol;
	 Ext.get('rdoExternalApproveReq').dom.disabled=mBol;
	 Ext.get('dtpPengerjaanPMWorkOrder').dom.disabled=mBol;
	 Ext.get('dtpSelesaiPMWorkOrder').dom.disabled=mBol;
	 Ext.get('txtDeskripsiPMWorkOrder').dom.disabled=mBol;
	 
	 if (varTabRepairPMWorkOrder === 2)
	 {
		 Ext.get('btnTambahBrsCraftPersonIntPMWorkOrder').dom.disabled=mBol;
		 Ext.get('btnHpsBrsCraftPersonIntPMWorkOrder').dom.disabled=mBol;
		 Ext.get('btnTambahBrsCraftPersonExtPMWorkOrder').dom.disabled=mBol;
		 Ext.get('btnHpsBrsCraftPersonExtPMWorkOrder').dom.disabled=mBol;
	 }
	 else if (varTabRepairPMWorkOrder === 3)
	 {
		 Ext.get('btnTambahBrsPartPMWorkOrder').dom.disabled=mBol;
		 Ext.get('btnHpsBrsPartPMWorkOrder').dom.disabled=mBol;
	 };
};


function LoadDataServicePMWorkOrder(str) 
{
    dsDtlServicePMWorkOrder.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_ID',
                            Sort: 'service_id',
			    Sortdir: 'ASC',
			    target: 'ViewSchServicePM',
			    param: str
			}
		}
	);
    return dsDtlServicePMWorkOrder;
};

function LoadDataServiceWOPMWorkOrder(str) 
{
    dsDtlServicePMWorkOrder.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_ID',
                            Sort: 'service_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOServicePM',
			    param: str
			}
		}
	);
    return dsDtlServicePMWorkOrder;
};

function LoadDataEmployeePMWorkOrderTemp(str) 
{
    dsDtlCraftPersonIntPMWorkOrderTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'EMP_ID',
                            Sort: 'emp_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPersonPM',
			    param: str
			}
		}
	);
    return dsDtlCraftPersonIntPMWorkOrderTemp;
};

function LoadDataEmployeePMWorkOrderTempView(str) 
{
	 var fldDetail = ['SCH_PM_ID','SERVICE_ID','ROW_SCH','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_WO_PM_PERSON','VENDOR','EMP_NAME','ROW','ROW_PERSON'];

    dsDtlCraftPersonIntPMWorkOrderTempView = new WebApp.DataStore({ fields: fldDetail })
    dsDtlCraftPersonIntPMWorkOrderTempView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'EMP_ID',
                            Sort: 'emp_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPersonPM',
			    param: str
			}
		}
	);
    return dsDtlCraftPersonIntPMWorkOrderTempView;
};

function LoadDataEmployeePMWorkOrder(str) 
{
	
    dsDtlCraftPersonIntPMWorkOrder.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'EMP_ID',
                            Sort: 'emp_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPersonPM',
			    param: str
			}
		}
	);
    return dsDtlCraftPersonIntPMWorkOrder;
};

function LoadDataVendorPMWorkOrderTemp(str) 
{
    dsDtlCraftPersonExtPMWorkOrderTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'VENDOR_ID',
                            Sort: 'vendor_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPersonPM',
			     param: str
			}
		}
	);
    return dsDtlCraftPersonExtPMWorkOrderTemp;
};

function LoadDataVendorPMWorkOrderTempView(str) 
{
	 var fldDetail = ['SCH_PM_ID','SERVICE_ID','ROW_SCH','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_WO_PM_PERSON','VENDOR','EMP_NAME','ROW','ROW_PERSON'];

    dsDtlCraftPersonExtPMWorkOrderTempView = new WebApp.DataStore({ fields: fldDetail })
	
    dsDtlCraftPersonExtPMWorkOrderTempView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'VENDOR_ID',
                            Sort: 'vendor_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPersonPM',
			     param: str
			}
		}
	);
    return dsDtlCraftPersonExtPMWorkOrderTempView;
};

function LoadDataVendorPMWorkOrder(str) 
{	
    dsDtlCraftPersonExtPMWorkOrder.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'VENDOR_ID',
                            Sort: 'vendor_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPersonPM',
			     param: str
			}
		}
	);
    return dsDtlCraftPersonExtPMWorkOrder;
};

function LoadDataPartPMWorkOrderTemp(CatId) 
{
    dsDtlPartPMWorkOrderTemp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'PART_ID',
                            Sort: 'part_id',
			    Sortdir: 'ASC',
			    target: 'ViewWOPartPM',
			    //param: 'where category_id=~' + CatId + '~'
                            param: 'category_id = ~' + CatId + '~'
			}
		}
	);
    return dsDtlPartPMWorkOrderTemp;
};

function LoadDataPartPMWorkOrder(SchId,CatId,ServId) 
{	
    dsDtlPartPMWorkOrder.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'PART_ID',
                            Sort: 'part_id',
			    Sortdir: 'ASC',
			    target: 'ViewSchPartCM',
			    //param: 'where SCH_PM_ID =~' + SchId + '~ and category_id=~' + CatId + '~ and service_id = ~' + ServId + '~'
                            param: 'sch_pm_id = ~' + SchId + '~ and category_id = ~' + CatId + '~ and service_id = ~' + ServId + '~'
			}
		}
	);
    return dsDtlPartPMWorkOrder;
};

function DisabledVendorPMWorkOrder(mBol)
{
	//Ext.get('rdoInternalApproveReq').dom.checked=mBol;
	
	Ext.get('txtNamaVendorPMWorkOrder').dom.disabled=mBol;
	
	if (mBol === true)
	{
		Ext.get('txtNamaVendorPMWorkOrder').dom.value='';
		Ext.get('txtKdVendorPMWorkOrder').dom.value='';
	};
	
};

///---------------------------------------------------------------------------------------///
function PMWorkOrderAddNew() 
{
    AddNewPMWorkOrder = true;
	AppIdPMWorkOrder='';
	//SchIdPMWorkOrder='';
	//dsDtlServicePMWorkOrder.removeAll();
	dsDtlPartPMWorkOrder.removeAll();
	dsDtlPartPMWorkOrderTemp.removeAll();
	dsDtlCraftPersonIntPMWorkOrder.removeAll();
	dsDtlCraftPersonIntPMWorkOrderTemp.removeAll();
	dsDtlCraftPersonExtPMWorkOrder.removeAll();
	dsDtlCraftPersonExtPMWorkOrderTemp.removeAll();
	Ext.get('txtTotalServicePMWorkOrder').dom.value=0;
	if (varTabRepairPMWorkOrder === 2)
	{
		Ext.get('txtTotalPersonCostPMWorkOrder').dom.value=0;
		
	};
	Ext.get('rdoInternalApproveReq').dom.checked = true
	IsExtRepairPMWorkOrder=false;
	varTabRepairPMWorkOrder=1;
	Ext.get('txtKdApproverPMWorkOrder').dom.value='';
	Ext.get('txtNamaApproverPMWorkOrder').dom.value='';
	Ext.get('txtDeskripsiPMWorkOrder').dom.value='';
	Ext.get('txtKdVendorPMWorkOrder').dom.value='';
	Ext.get('txtNamaVendorPMWorkOrder').dom.value='';

};
///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamPMWorkOrder() 
{
    var params =
	{
		Table:'ViewWorkOrderPM',
		WOId:AppIdPMWorkOrder,
		SchId:SchIdPMWorkOrder,
		CatId:varCatIdPMWorkOrder,
		PIC:Ext.get('txtKdApproverPMWorkOrder').dom.value,
		WODate:Ext.get('dtpPengerjaanPMWorkOrder').dom.value,
		FinishDate:Ext.get('dtpSelesaiPMWorkOrder').dom.value,
		DescApp:Ext.get('txtDeskripsiPMWorkOrder').dom.value,
		IsExt:GetIsExtPMWorkOrder(),
		ListService:getArrDetailServicePMWorkOrder(),
		ListPerson:getArrDetailPersonPMWorkOrder(),
		ListPart:getArrDetailPartPMWorkOrder(),
		JmlFieldService: mRecordServicePMWorkOrder.prototype.fields.length-5,
		JmlFieldPerson: mRecordPersonPMWorkOrder.prototype.fields.length-2,
		JmlFieldPart: mRecordPartPMWorkOrder.prototype.fields.length-4,
		JmlListService:GetListCountDetailServicePMWorkOrder(),
		JmlListPerson:GetListCountDetailPersonPMWorkOrder(),
		JmlListPart:GetListCountDetailPartPMWorkOrder(),
		Hapus:1,
		Service:0
	};
	
    return params
};

function getArrDetailServicePMWorkOrder()
{
	var x='';
	for(var i = 0 ; i < dsDtlServicePMWorkOrder.getCount();i++)
	{
		if (dsDtlServicePMWorkOrder.data.items[i].data.SERVICE_ID != '' && dsDtlServicePMWorkOrder.data.items[i].data.SERVICE_NAME != '' && dsDtlServicePMWorkOrder.data.items[i].data.CATEGORY_ID != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'SERVICE_ID=' + dsDtlServicePMWorkOrder.data.items[i].data.SERVICE_ID
			y += z + 'CATEGORY_ID='+ dsDtlServicePMWorkOrder.data.items[i].data.CATEGORY_ID
			y += z + 'ROW_SCH='+ dsDtlServicePMWorkOrder.data.items[i].data.ROW_SCH
			
			if (i === (dsDtlServicePMWorkOrder.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
		dsDtlServicePMWorkOrder.data.items[i].data.TAG='';
	}	
	return x;
};

function GetListCountDetailServicePMWorkOrder()
{
	var x=0;
	for(var i = 0 ; i < dsDtlServicePMWorkOrder.getCount();i++)
	{
		if (dsDtlServicePMWorkOrder.data.items[i].data.SERVICE_ID != '' && dsDtlServicePMWorkOrder.data.items[i].data.SERVICE_NAME  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getArrDetailPersonPMWorkOrder()
{
	var x='';
	var ds='';
	if (IsExtRepairPMWorkOrder === false)
	{
		ds=dsDtlCraftPersonIntPMWorkOrderTemp;
	}
	else
	{
		ds=dsDtlCraftPersonExtPMWorkOrderTemp;
		for(var i = 0 ; i < ds.getCount();i++)
		{
			if(ds.data.items[i].data.PERSON_NAME === '')
			{
				ds.removeAt(i);
			};
		}
	};
	
	if(ds.getCount() > 0) 
	{
		for(var i = 0 ; i < ds.getCount();i++)
		{
			var  mDataKd='';
			var  mDataName='';
			var  mFlag='';
			
			if (IsExtRepairPMWorkOrder === false)  // internal
			{
				mDataKd = ds.data.items[i].data.EMP_ID;
				mDataName = ds.data.items[i].data.EMP_NAME;
				mFlag='xxx';
			}
			else
			{   
				mDataKd = ds.data.items[i].data.VENDOR_ID;  // external
				mDataName = ds.data.items[i].data.VENDOR;
				mFlag=ds.data.items[i].data.PERSON_NAME;
			};
			
			if (mDataKd != '' && mDataName != '' && mFlag != '')
			{
				var y='';
				var z='@@##$$@@';
				
				y = 'SERVICE_ID=' + ds.data.items[i].data.SERVICE_ID
				y += z + 'ROW_SCH='+ds.data.items[i].data.ROW_SCH
				y += z + 'EMP_ID='+ds.data.items[i].data.EMP_ID
				y += z + 'EMP_NAME='+ds.data.items[i].data.EMP_NAME
				y += z + 'VENDOR_ID='+ds.data.items[i].data.VENDOR_ID
				y += z + 'VENDOR='+ds.data.items[i].data.VENDOR
				
				if (IsExtRepairPMWorkOrder === false)  // internal
				{
					y += z + 'PERSON_NAME='+ ds.data.items[i].data.EMP_NAME
				}
				else
				{
					y += z + 'PERSON_NAME='+ ds.data.items[i].data.PERSON_NAME
				};
				
				
				y += z + 'COST='+ ds.data.items[i].data.COST
				y += z + 'DESC_WO_PM_PERSON='+ ds.data.items[i].data.DESC_WO_PM_PERSON
				y += z + 'ROW_PERSON='+ ds.data.items[i].data.ROW_PERSON
				
				if (i === (ds.getCount()-1))
				{
					x += y 
				}
				else
				{
					x += y + '##[[]]##'
				};
			};
		}	
	}
	else
	{
		if (IsExtRepairPMWorkOrder === true)  // external
		{
			if(Ext.get('txtKdVendorPMWorkOrder').dom.value != '' && dsDtlServicePMWorkOrder.getCount() > 0)
			{
				var y='';
				var z='@@##$$@@';
				
				
				y = 'SERVICE_ID=' + dsDtlServicePMWorkOrder.data.items[0].data.SERVICE_ID
				y += z + 'ROW_SCH=1'
				y += z + 'EMP_ID='+ ''
				y += z + 'EMP_NAME='+ ''
				y += z + 'VENDOR_ID='+ Ext.get('txtKdVendorPMWorkOrder').dom.value
				y += z + 'VENDOR='+ Ext.get('txtNamaVendorPMWorkOrder').dom.value
				if (rowSelectedLookVendor != undefined)
				{
					y += z + 'PERSON_NAME='+ rowSelectedLookVendor.data.CONTACT1
				}
				else
				{
					y += z + 'PERSON_NAME='+ ''
				};
				y += z + 'COST=0'
				y += z + 'DESC_WO_PM_PERSON='+ ''
				y += z + 'ROW_PERSON='+ ''
				
				
				x += y 
				
			};
		};
	};
	

	return x;
};

function GetListCountDetailPersonPMWorkOrder()
{
	var x=0;
	
	if (IsExtRepairPMWorkOrder === false)
	{
		ds=dsDtlCraftPersonIntPMWorkOrderTemp;
	}
	else
	{
		ds=dsDtlCraftPersonExtPMWorkOrderTemp; 
	};
	
	
	if(ds.getCount() > 0)
	{
		for(var i = 0 ; i < ds.getCount();i++)
		{
		
			var  mDataKd='';
			var  mDataName='';
			var  mFlag='';
			
			if (IsExtRepairPMWorkOrder === false)  // internal
			{
				mDataKd = ds.data.items[i].data.EMP_ID;
				mDataName = ds.data.items[i].data.EMP_NAME;
				mFlag='xxx';
			}
			else
			{  
				mDataKd = ds.data.items[i].data.VENDOR_ID; // external
				mDataName = ds.data.items[i].data.VENDOR;
				mFlag = ds.data.items[i].data.PERSON_NAME;
			};
		
			if (mDataKd != '' && mDataName  != '' && mFlag !='')
			{
				x += 1;
			};
		}
	}
	else
	{
		if (IsExtRepairPMWorkOrder === true)  // external
		{
			if(Ext.get('txtKdVendorPMWorkOrder').dom.value != '' && dsDtlServicePMWorkOrder.getCount() > 0)
			{
				x=1;
			};
		};
	};
	
	
	return x;
	
};

function getArrDetailPartPMWorkOrder()
{
	var x='';
	for(var i = 0 ; i < dsDtlPartPMWorkOrderTemp.getCount();i++)
	{
		if (dsDtlPartPMWorkOrderTemp.data.items[i].data.PART_ID != '' && dsDtlPartPMWorkOrderTemp.data.items[i].data.PART_NAME != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'SERVICE_ID=' + dsDtlPartPMWorkOrderTemp.data.items[i].data.SERVICE_ID
			y += z + 'PART_ID=' + dsDtlPartPMWorkOrderTemp.data.items[i].data.PART_ID
			y += z + 'QTY='+ dsDtlPartPMWorkOrderTemp.data.items[i].data.QTY
			y += z + 'UNIT_COST='+ dsDtlPartPMWorkOrderTemp.data.items[i].data.UNIT_COST
			y += z + 'TOT_COST='+ (dsDtlPartPMWorkOrderTemp.data.items[i].data.UNIT_COST * dsDtlPartPMWorkOrderTemp.data.items[i].data.QTY)
			y += z + 'DESC_SCH_PART='+dsDtlPartPMWorkOrderTemp.data.items[i].data.DESC_SCH_PART
			
			if (i === (dsDtlPartPMWorkOrderTemp.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
		dsDtlPartPMWorkOrderTemp.data.items[i].data.TAG='';
	}	
	return x;
};

function GetListCountDetailPartPMWorkOrder()
{
	var x=0;
	for(var i = 0 ; i < dsDtlPartPMWorkOrderTemp.getCount();i++)
	{
		if (dsDtlPartPMWorkOrderTemp.data.items[i].data.PART_ID != '' && dsDtlPartPMWorkOrderTemp.data.items[i].data.PART_NAME  != '')
		{
			x += 1;
		};
	}
	return x;
	
};


function GetIsExtPMWorkOrder()
{
	var x;
	if (Ext.get('rdoInternalApproveReq').dom.checked === true) 
	{
		x=0;
	}
	else
	{
		x=1;
	};
	
	return x;
};

function getItemPanelInputPMWorkOrder(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,//49,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
		border:false,
		height:734,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,//49,
				labelWidth:90,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype:'fieldset',
						title: nmTitleTabInfoAsetWOPM,	
						anchor:  '99.99%',
						height:'40px',
						 items:
						 [
							getItemPanelNoAssetPMWorkOrder(lebar)
						 ]
					},
					{
						xtype:'fieldset',
						title: nmTitleTabInfoWOPM,	
						anchor:  '99.99%',
						height:'348px',
						items :
						[
							getItemPanelTanggalAppPMWorkOrder(lebar),
							getItemPanelApproverPMWorkOrder(lebar),
							getItemPanelRadioVendorPMWorkOrder(lebar), 
							getItemTabPanelRepairPMWorkOrder(),
							{
								xtype: 'textarea',
								fieldLabel: nmNotesWOPM + ' ',
								name: 'txtDeskripsiPMWorkOrder',
								style:{'margin-top':'5px'},
								id: 'txtDeskripsiPMWorkOrder',
								scroll:true,
								anchor: '100% 10%'
							}
						]
					}
				]
			}
		]
	};
    return items;
};


function getItemTabPanelRepairPMWorkOrder()
{
	var TotalServicePMWorkOrder = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 666,
			border:false,
			labelAlign:'right',
			labelWidth:175,
			style: 
			{
				'margin-top': '3px'//,
				//'margin-left': '310px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: nmTotalPersonPartWOPM + ' ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalServicePMWorkOrder',
					name:'txtTotalServicePMWorkOrder',
					readOnly:true,
					width: 150
				}
			]
		}
	);
	
	var TotalPersonCostPMWorkOrder = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 666,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '3px',
				'margin-left': '461px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: nmTotalWOPM + ' ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalPersonCostPMWorkOrder',
					name:'txtTotalPersonCostPMWorkOrder',
					readOnly:true,
					width: 150
				}
			]
		}
	);

	var TotalPartPMWorkOrder = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 666,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '3px',
				'margin-left': '461px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: nmTotalWOPM + ' ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalPartPMWorkOrder',
					name:'txtTotalPartPMWorkOrder',
					readOnly:true,
					width: 150
				}
			]
		}
	);

    vTabPanelRepairPMWorkOrder = new Ext.TabPanel
	(
		{
		    id: 'vTabPanelRepairPMWorkOrder',
		    region: 'center',
		    margins: '5 5 5 5',
			style:
			{
			   'margin-top': '-3px'
			},
		    bodyStyle: 'padding:5px 5px 5px 5px',
		    activeTab: 0,
		    plain: true,
		    defferedRender: false,
		    frame: false,
		    border: true,
		    height: 240,//182,//195,//215,
			width:742,
		    anchor: '100%',
		    items:
			[
				{
					 title:nmTitleTabServiceWOPM,
					 id: 'tabServicePMWorkOrder',
					 items:
					 [
						GetDTLServicePMWorkOrderGrid(),TotalServicePMWorkOrder
					 ],
					 listeners:
					{
						activate: function() 
						{
							FocusCtrlPMWorkOrder === 'colService'
							varTabRepairPMWorkOrder=1;
						}
					}
				},
			    {
					 title: nmTitleTabPersonWOPM,
					 id: 'tabPersonPMWorkOrder',
					 items:
					 [
						getItemPanelCardLayoutGridPersonPMWorkOrder(),TotalPersonCostPMWorkOrder
					 ],
			        listeners:
					{
						  activate: function() 
						  {
							if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder != undefined)
							{
								if (cellSelectServicePMWorkOrder.data.SERVICE_ID === undefined || cellSelectServicePMWorkOrder.data.SERVICE_ID === '')
								{
									vTabPanelRepairPMWorkOrder.setActiveTab('tabServicePMWorkOrder');
									varTabRepairPMWorkOrder=1;
									alert(nmAlertTabService);
								}
								else
								{
									if (IsExtRepairPMWorkOrder === true)
									{
										Ext.getCmp('FormPanelCardLayoutGridPersonPMWorkOrder').getLayout().setActiveItem(1);	
										FocusCtrlPMWorkOrder = 'colVendor'
										GetPersonExtServicePMWorkOrder(cellSelectServicePMWorkOrder);
									}
									else
									{
										Ext.getCmp('FormPanelCardLayoutGridPersonPMWorkOrder').getLayout().setActiveItem(0);	
										FocusCtrlPMWorkOrder = 'colEmp'
										GetPersonIntServicePMWorkOrder(cellSelectServicePMWorkOrder);
									};
									CalcTotalPersonPMWorkOrder('',false);
									varTabRepairPMWorkOrder=2;
									
								};
							}
							else
							{
								vTabPanelRepairPMWorkOrder.setActiveTab('tabServicePMWorkOrder');
								varTabRepairPMWorkOrder=1;
								alert(nmAlertTabService);
							};
						  }
					}
				},
				{
					 title: nmTitleTabPartWOPM,
					 id: 'tabPartPMWorkOrder',
					 items:
					 [
						GetDTLPartPMWorkOrderGrid(),TotalPartPMWorkOrder
					 ],
					 listeners:
					{
						activate: function() 
						{
							if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder != undefined)
							{
								if (cellSelectServicePMWorkOrder.data.SERVICE_ID === undefined || cellSelectServicePMWorkOrder.data.SERVICE_ID === '')
								{
									vTabPanelRepairPMWorkOrder.setActiveTab('tabServicePMWorkOrder');
									varTabRepairPMWorkOrder=1;
									alert(nmAlertTabService);
								}
								else
								{
									FocusCtrlPMWorkOrder === 'colPart'
									varTabRepairPMWorkOrder=3;
									GetPartPMWorkOrder(cellSelectServicePMWorkOrder);
									CalcTotalPartPMWorkOrder('','',false);
									
								};	
							}
							else
							{
								vTabPanelRepairPMWorkOrder.setActiveTab('tabServicePMWorkOrder');
								varTabRepairPMWorkOrder=1;
								alert(nmAlertTabService);
							};
						}
					}
				}
			]
		}
	)
		return vTabPanelRepairPMWorkOrder;
};

function getItemPanelCardLayoutGridPersonPMWorkOrder()
{
	 var FormPanelCardLayoutGridPersonPMWorkOrder = new Ext.Panel
	(
		{
		    id: 'FormPanelCardLayoutGridPersonPMWorkOrder',
		    trackResetOnLoad: true,
		    width: 680,//666,
			height: 178,
			layout: 'card',
			border:false,
			activeItem: 0,
			items: 
			[
				{
					xtype: 'panel',
					layout: 'form',
					width: 680,//666,
					height: 178,
					border:false,
					id: 'CardGridPersonPMWorkOrder1',
					items:
					[	 
						GetDTLCraftPersonIntPMWorkOrderGrid()
					]
				},
				{
					xtype: 'panel',
					layout: 'form',
					width: 680,
					height: 178,
					border:false,
					id: 'CardGridPersonPMWorkOrder2',
					items:
					[
						GetDTLCraftPersonExtPMWorkOrderGrid()
					]
				}
			]
		}
	);
	
	return FormPanelCardLayoutGridPersonPMWorkOrder;
};

function GetDTLCraftPersonIntPMWorkOrderGrid() 
{
    var fldDetail = ['SCH_PM_ID','SERVICE_ID','ROW_SCH','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_WO_PM_PERSON','VENDOR','EMP_NAME','ROW','ROW_PERSON'];

    dsDtlCraftPersonIntPMWorkOrder = new WebApp.DataStore({ fields: fldDetail })
	dsDtlCraftPersonIntPMWorkOrderTemp = new WebApp.DataStore({ fields: fldDetail })

    var gridCraftPersonIntPMWorkOrder  = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlCraftPersonIntPMWorkOrder,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:679,
		    height:178,//120,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectPersonIntPMWorkOrder = dsDtlCraftPersonIntPMWorkOrder.getAt(row);
					        CurrentPersonIntPMWorkOrder.row = row;
					        CurrentPersonIntPMWorkOrder.data = cellSelectPersonIntPMWorkOrder;
					    }
					}
				}
			),
				cm: CraftPersonIntPMWorkOrderDetailColumModel(),
				 tbar: 
			         [
				        {
					        id:'btnTambahBrsCraftPersonIntPMWorkOrder',
					        text: nmTambahBaris,
					        tooltip: nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						TambahBarisPersonPMWorkOrder(dsDtlCraftPersonIntPMWorkOrder,dsDtlCraftPersonIntPMWorkOrderTemp);
					        }
				        },'-',
				        {
					        id:'btnHpsBrsCraftPersonIntPMWorkOrder',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlCraftPersonIntPMWorkOrder.getCount() > 0 )
								{
									if (cellSelectPersonIntPMWorkOrder != undefined)
									{
										if(CurrentPersonIntPMWorkOrder != undefined)
										{
											HapusBarisPersonIntPMWorkOrder();
										};
									}
									else
									{
										ShowPesanWarningPMWorkOrder(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			            ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridCraftPersonIntPMWorkOrder;
};

function TambahBarisPersonPMWorkOrder(ds)
{
	if(IsExtRepairPMWorkOrder === true)
	{
		if (Ext.get('txtKdVendorPMWorkOrder').dom.value === '')
		{
			ShowPesanWarningPMWorkOrder(nmGetValidasiKosong(nmVendIDNameWOPM), nmTambahBaris);
		}
		else
		{
			var p = GetRecordBaruPersonPMWorkOrder(ds);
			ds.insert(ds.getCount(), p);
			
			var j=dsDtlCraftPersonExtPMWorkOrder.getCount()-1;
			dsDtlCraftPersonExtPMWorkOrderTemp.insert(dsDtlCraftPersonExtPMWorkOrderTemp.getCount(),dsDtlCraftPersonExtPMWorkOrder.data.items[j]);
			GetPersonExtServicePMWorkOrder(cellSelectServicePMWorkOrder);
		};
	}
	else
	{
		var p1 = GetRecordBaruPersonPMWorkOrder(ds);
		ds.insert(ds.getCount(), p1);
	};
};

function HapusBarisPersonIntPMWorkOrder()
{
	if (cellSelectPersonIntPMWorkOrder.data.EMP_ID != '' && cellSelectPersonIntPMWorkOrder.data.EMP_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentPersonIntPMWorkOrder.row + 1) + ' ' + nmOperatorDengan  + ' ' + nmEmpIDWOPM + ' : ' + cellSelectPersonIntPMWorkOrder.data.EMP_ID + ' ' + nmOperatorAnd + ' ' + nmEmpNameWOPM + ' : ' + cellSelectPersonIntPMWorkOrder.data.EMP_NAME,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlCraftPersonIntPMWorkOrder.data.items[CurrentPersonIntPMWorkOrder.row].data.ROW_PERSON=== '' )
							{
								GetTotalRemovePMWorkOrder(CurrentPersonIntPMWorkOrder,true);
								HapusRowTempPMWorkOrder(dsDtlCraftPersonIntPMWorkOrderTemp,CurrentPersonIntPMWorkOrder.data.data.ROW);
								dsDtlCraftPersonIntPMWorkOrder.removeAt(CurrentPersonIntPMWorkOrder.row);
								
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase(),
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPersonPMWorkOrder();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			if(CurrentPersonIntPMWorkOrder.data.data.COST != '' && CurrentPersonIntPMWorkOrder.data.data.COST != undefined)
			{
				GetTotalRemovePMWorkOrder(CurrentPersonIntPMWorkOrder,true)
			};
			dsDtlCraftPersonIntPMWorkOrder.removeAt(CurrentPersonIntPMWorkOrder.row);
		};
};

function HapusBarisPersonExtPMWorkOrder()
{
	if (cellSelectPersonExtPMWorkOrder.data.VENDOR_ID != '' && cellSelectPersonExtPMWorkOrder.data.VENDOR != '' && cellSelectPersonExtPMWorkOrder.data.PERSON_NAME !='')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentPersonExtPMWorkOrder.row + 1) + ' ' + nmOperatorDengan + ' ' + nmPersonNameWOPM + ' : ' + cellSelectPersonExtPMWorkOrder.data.PERSON_NAME ,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlCraftPersonExtPMWorkOrder.data.items[CurrentPersonExtPMWorkOrder.row].data.ROW_PERSON === '' )
							{
								GetTotalRemovePMWorkOrder(CurrentPersonExtPMWorkOrder,true)
								HapusRowTempPMWorkOrder(dsDtlCraftPersonExtPMWorkOrderTemp,CurrentPersonExtPMWorkOrder.data.data.ROW);
								dsDtlCraftPersonExtPMWorkOrder.removeAt(CurrentPersonExtPMWorkOrder.row);
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPersonPMWorkOrder();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			if(CurrentPersonExtPMWorkOrder.data.data.COST != '' && CurrentPersonExtPMWorkOrder.data.data.COST != undefined)
			{
				GetTotalRemovePMWorkOrder(CurrentPersonExtPMWorkOrder,true)
			};
			HapusRowTempPMWorkOrder(dsDtlCraftPersonExtPMWorkOrderTemp,CurrentPersonExtPMWorkOrder.data.data.ROW);
			dsDtlCraftPersonExtPMWorkOrder.removeAt(CurrentPersonExtPMWorkOrder.row);
		};
};

function DeleteDetailPersonPMWorkOrder()
{
	Ext.Ajax.request
	(
		{
			url: WebAppUrl.UrlDeleteData, //"./Datapool.mvc/DeleteDataObj",
			params:  getParamDeleteDetailPersonPMWorkOrder(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoPMWorkOrder(nmPesanHapusSukses,nmHeaderHapusData);
//					var str = ' where category_id =~' + varCatIdPMWorkOrder + '~'
//						str += ' and sch_pm_id =~' + SchIdPMWorkOrder + '~'
//						str += ' and wo_pm_id =~' + AppIdPMWorkOrder + '~'

					var str = ' category_id = ~' + varCatIdPMWorkOrder + '~'
						str += ' and sch_pm_id = ~' + SchIdPMWorkOrder + '~'
						str += ' and wo_pm_id = ~' + AppIdPMWorkOrder + '~'

					var strListServ = GetStrListServicePMWorkOrder(false);
					var strListRowSch = GetStrListServicePMWorkOrder(true);
					var strPerson ='';
						strPerson += str
						
						if (strListServ != '')
						{
							strPerson +=' and service_id in (' + strListServ + ')';
						};
						
						if (strListRowSch != '')
						{
							strPerson +=' and row_sch in (' + strListRowSch + ')';
						};				
						
					var str2 ='';
						str2 += str + ' and service_id = ~' + cellSelectServicePMWorkOrder.data.SERVICE_ID + '~'
						str2 += ' and row_sch = ' + cellSelectServicePMWorkOrder.data.ROW_SCH
						
					if (IsExtRepairPMWorkOrder === false)
					{
						GetTotalRemovePMWorkOrder(CurrentPersonIntPMWorkOrder,true);
						//GetJumlahTotalPersonPMWorkOrder('where SCH_PM_ID  =~' + SchIdPMWorkOrder  + '~ and category_id = ~' + varCatIdPMWorkOrder  + '~' + ' and service_id= ~ ' + cellSelectServicePMWorkOrder.data.SERVICE_ID + '~' + ' and EMP_ID <> ~~' + 'and EMP_ID is not null');
                                                GetJumlahTotalPersonPMWorkOrder('sch_pm_id  = ~' + SchIdPMWorkOrder  + '~ and category_id = ~' + varCatIdPMWorkOrder  + '~' + ' and service_id = ~' + cellSelectServicePMWorkOrder.data.SERVICE_ID + '~' + ' and emp_id <> ~~' + ' and emp_id is not null');
						HapusRowTempPMWorkOrder(dsDtlCraftPersonIntPMWorkOrderTemp,CurrentPersonIntPMWorkOrder.data.data.ROW);
						dsDtlCraftPersonIntPMWorkOrder.removeAt(CurrentPersonIntPMWorkOrder.row);
						cellSelectPersonIntPMWorkOrder=undefined;
						LoadDataEmployeePMWorkOrderTemp(strPerson);
						LoadDataEmployeePMWorkOrder(str2);
					}
					else
					{
						GetTotalRemovePMWorkOrder(CurrentPersonExtPMWorkOrder,true);
						//GetJumlahTotalPersonPMWorkOrder('where SCH_PM_ID  =~' + SchIdPMWorkOrder  + '~ and category_id = ~' + varCatIdPMWorkOrder  + '~' + ' and service_id= ~ ' + cellSelectServicePMWorkOrder.data.SERVICE_ID + '~' + ' and VENDOR_ID <> ~~' + 'and VENDOR_ID is not null');
                                                GetJumlahTotalPersonPMWorkOrder('sch_pm_id  = ~' + SchIdPMWorkOrder  + '~ and category_id = ~' + varCatIdPMWorkOrder  + '~' + ' and service_id = ~' + cellSelectServicePMWorkOrder.data.SERVICE_ID + '~' + ' and vendor_id <> ~~' + ' and vendor_id is not null');
						HapusRowTempPMWorkOrder(dsDtlCraftPersonExtPMWorkOrderTemp,CurrentPersonExtPMWorkOrder.data.data.ROW);
						dsDtlCraftPersonExtPMWorkOrder.removeAt(CurrentPersonExtPMWorkOrder.row);
						cellSelectPersonExtPMWorkOrder=undefined;
						LoadDataVendorPMWorkOrderTemp(strPerson);
						LoadDataVendorPMWorkOrder(str2);
					};
					
					AddNewPMWorkOrder = false;
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningPMWorkOrder(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else if (cst.success === false && cst.pesan===1)
				{
					ShowPesanWarningPMWorkOrder(nmPesanHapusGagal + ' , ' + nmKonfirmasiResult,nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorPMWorkOrder(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailPersonPMWorkOrder()
{
	var x,y;
	
	if (IsExtRepairPMWorkOrder === false)
	{
		x=CurrentPersonIntPMWorkOrder.data.data.SCH_PM_ID;
		y=CurrentPersonIntPMWorkOrder.data.data.ROW_PERSON;
	}
	else
	{
		x=CurrentPersonExtPMWorkOrder.data.data.SCH_PM_ID;
		y=CurrentPersonExtPMWorkOrder.data.data.ROW_PERSON;
	};
	
	var params =
	{	
		Table: 'ViewWorkOrderPM',   
		WOId: AppIdPMWorkOrder,
		SchId:SchIdPMWorkOrder,
		CatId:varCatIdPMWorkOrder,
		ServiceId:cellSelectServicePMWorkOrder.data.SERVICE_ID,
		RowSch: cellSelectServicePMWorkOrder.data.ROW_SCH,
		RowPerson:y,
		Hapus:3
	};
	
    return params;
};

function CraftPersonIntPMWorkOrderDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdEmployeePMWorkOrder',
			    header: nmEmpIDWOPM,
			    dataIndex: 'EMP_ID',
			    width: 150,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdEmployeePMWorkOrder',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
						            if (Ext.get('fieldcolKdEmployeePMWorkOrder').dom.value != '')
									{
										//str = ' where EMP_ID like ~%' + Ext.get('fieldcolKdEmployeePMWorkOrder').dom.value + '%~';
                                                                                str = ' emp_id like ~%' + Ext.get('fieldcolKdEmployeePMWorkOrder').dom.value + '%~';
									};
									GetLookupPersonIntPMWorkOrder(str)    
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMWorkOrder='colEmp';
							}
						}
					}
				)
			},
			{
			    id: 'colEmployeeNamePMWorkOrder',
			    header: nmEmpNameWOPM,
			    dataIndex: 'EMP_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolEmployeeNamePMWorkOrder',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
						            if (Ext.get('fieldcolEmployeeNamePMWorkOrder').dom.value != '')
									{
										//str = ' where EMP_NAME like ~%' + Ext.get('fieldcolEmployeeNamePMWorkOrder').dom.value + '%~';
                                                                                str = ' emp_name like ~%' + Ext.get('fieldcolEmployeeNamePMWorkOrder').dom.value + '%~';
									};
									GetLookupPersonIntPMWorkOrder(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMWorkOrder='colEmp';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colDescCraftPersonPMWorkOrder',
			    header: nmDescWOPM,
			    width: 200,
			    dataIndex: 'DESC_WO_PM_PERSON',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescCraftPersonPMWorkOrder',
					    allowBlank: true,
					    width: 30
					}
				),
			    renderer: function(value, cell) 
				{
			       var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colCostCraftPersonPMWorkOrder',
			    header: nmCostWOPM,
			    width: 150,
				align:'right',
			    dataIndex: 'COST',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostCraftPersonPMWorkOrder',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPersonPMWorkOrder(CurrentPersonIntPMWorkOrder.row,true);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.COST);
				}
			}
		]
	)

};

function GetLookupPersonIntPMWorkOrder(str)
{
	if (cellSelectPersonIntPMWorkOrder.data.ROW_PERSON === '' || cellSelectPersonIntPMWorkOrder.data.ROW_PERSON === undefined)
	{
		var p = new mRecordPersonPMWorkOrder
		(
			{
				'SCH_PM_ID':SchIdPMWorkOrder,
				'SERVICE_ID':cellSelectServicePMWorkOrder.data.SERVICE_ID,
				'ROW_SCH':cellSelectServicePMWorkOrder.data.ROW_SCH,
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonIntPMWorkOrder.data.items[CurrentPersonIntPMWorkOrder.row].data.COST,
				'DESC_WO_PM_PERSON':dsDtlCraftPersonIntPMWorkOrder.data.items[CurrentPersonIntPMWorkOrder.row].data.DESC_WO_PM_PERSON,
				'ROW_PERSON':'',
				'ROW': dsDtlCraftPersonIntPMWorkOrder.data.items[CurrentPersonIntPMWorkOrder.row].data.ROW
			}
		);
		FormLookupEmployee('','',str,true,p,dsDtlCraftPersonIntPMWorkOrder,false);
	}
	else
	{	
		var p = new mRecordPersonPMWorkOrder
		(
			{
				'SCH_PM_ID':SchIdPMWorkOrder,
				'SERVICE_ID':cellSelectServicePMWorkOrder.data.SERVICE_ID,
				'ROW_SCH':cellSelectServicePMWorkOrder.data.ROW_SCH,
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonIntPMWorkOrder.data.items[CurrentPersonIntPMWorkOrder.row].data.COST,
				'DESC_WO_PM_PERSON':dsDtlCraftPersonIntPMWorkOrder.data.items[CurrentPersonIntPMWorkOrder.row].data.DESC_WO_PM_PERSON,
				'ROW_PERSON': dsDtlCraftPersonIntPMWorkOrder.data.items[CurrentPersonIntPMWorkOrder.row].data.ROW_PERSON,
				'ROW': dsDtlCraftPersonIntPMWorkOrder.data.items[CurrentPersonIntPMWorkOrder.row].data.ROW
			}
		);
		FormLookupEmployee('','',str,true,p,dsDtlCraftPersonIntPMWorkOrder,false);
	};
};

function GetDTLCraftPersonExtPMWorkOrderGrid() 
{
    var fldDetail = ['SCH_PM_ID','SERVICE_ID','ROW_SCH','EMP_ID','VENDOR_ID','PERSON_NAME','COST','DESC_WO_PM_PERSON','VENDOR','EMP_NAME','ROW','ROW_PERSON'];

    dsDtlCraftPersonExtPMWorkOrder = new WebApp.DataStore({ fields: fldDetail })
	dsDtlCraftPersonExtPMWorkOrderTemp = new WebApp.DataStore({ fields: fldDetail })
	
    var gridCraftPersonExtPMWorkOrder  = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlCraftPersonExtPMWorkOrder,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:679,
		    height:150,//92,//106,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectPersonExtPMWorkOrder = dsDtlCraftPersonExtPMWorkOrder.getAt(row);
					        CurrentPersonExtPMWorkOrder.row = row;
					        CurrentPersonExtPMWorkOrder.data = cellSelectPersonExtPMWorkOrder;
					    }
					}
				}
			),
				cm: CraftPersonExtPMWorkOrderDetailColumModel(),
				 tbar: 
			         [
				        {
					        id:'btnTambahBrsCraftPersonExtPMWorkOrder',
					        text: nmTambahBaris,
					        tooltip: nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						TambahBarisPersonPMWorkOrder(dsDtlCraftPersonExtPMWorkOrder);
					        }
				        },'-',
				        {
					        id:'btnHpsBrsCraftPersonExtPMWorkOrder',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlCraftPersonExtPMWorkOrder.getCount() > 0 )
								{
									if (cellSelectPersonExtPMWorkOrder != undefined)
									{
										if(CurrentPersonExtPMWorkOrder != undefined)
										{
											HapusBarisPersonExtPMWorkOrder();
										};
									}
									else
									{
										ShowPesanWarningPMWorkOrder(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			            ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridCraftPersonExtPMWorkOrder;
};

function CraftPersonExtPMWorkOrderDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdVendorPMWorkOrder',
			    header: nmVendIDResultWOPM,
			    dataIndex: 'VENDOR_ID',
			    width: 80,
				hidden:true,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdVendorPMWorkOrder',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									if (Ext.get('fieldcolKdVendorPMWorkOrder').dom.value != '')
									{
										//str = ' where VENDOR_ID like ~%' + Ext.get('fieldcolKdVendorPMWorkOrder').dom.value + '%~';
                                                                                str = ' vendor_id like ~%' + Ext.get('fieldcolKdVendorPMWorkOrder').dom.value + '%~';
									};
						            GetLookupPersonExtPMWorkOrder(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMWorkOrder='colVendor';
							}
						}
					}
				)
			},
			{
			    id: 'colVendorNamePMWorkOrder',
			    header: nmVendNameWOPM,
			    dataIndex: 'VENDOR',
			    sortable: false,
				hidden:true,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolVendorNamePMWorkOrder',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						           var str = '';
									if (Ext.get('fieldcolVendorNamePMWorkOrder').dom.value != '')
									{
										//str = ' where VENDOR like ~%' + Ext.get('fieldcolVendorNamePMWorkOrder').dom.value + '%~';
                                                                                str = ' vendor like ~%' + Ext.get('fieldcolVendorNamePMWorkOrder').dom.value + '%~';
									};
						            GetLookupPersonExtPMWorkOrder(str)
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMWorkOrder='colVendor';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell) 
				{
			        var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colContactPersonCraftPersonPMWorkOrder',
			    header:nmPersonNameWOPM,
			    width: 250,
			    dataIndex: 'PERSON_NAME',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolContactPersonCraftPersonPMWorkOrder',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90
					}
				)
			},
			{
			    id: 'colDescCraftPersonExtPMWorkOrder',
			    header: nmDescWOPM,
			    width: 250,
			    dataIndex: 'DESC_WO_PM_PERSON',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescCraftPersonExtPMWorkOrder',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(value, cell) 
				{
			       var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{
			    id: 'colCostCraftPersonExtPMWorkOrder',
			    header: nmCostWOPM,
			    width: 90,
			    dataIndex: 'COST',
				align:'right',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostCraftPersonExtPMWorkOrder',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPersonPMWorkOrder(CurrentPersonExtPMWorkOrder.row,true);
								};
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.COST);
				}
			}
		]
	)

};

function GetLookupPersonExtPMWorkOrder(str)
{
	if (cellSelectPersonExtPMWorkOrder.data.ROW_SCH === '' || cellSelectPersonExtPMWorkOrder.data.ROW_SCH === undefined)
	{
		var p = new mRecordPersonPMWorkOrder
		(
			{
				'SCH_PM_ID':SchIdPMWorkOrder,
				'SERVICE_ID':cellSelectServicePMWorkOrder.data.SERVICE_ID,
				'ROW_SCH':cellSelectServicePMWorkOrder.data.ROW_SCH,
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonExtPMWorkOrder.data.items[CurrentPersonExtPMWorkOrder.row].data.COST,
				'DESC_WO_PM_PERSON':dsDtlCraftPersonExtPMWorkOrder.data.items[CurrentPersonExtPMWorkOrder.row].data.DESC_WO_PM_PERSON,
				'ROW_PERSON':'',
				'ROW':dsDtlCraftPersonExtPMWorkOrder.data.items[CurrentPersonExtPMWorkOrder.row].data.ROW
			}
		);
		FormLookupVendor(p,str,'',true,dsDtlCraftPersonExtPMWorkOrder,false);
	}
	else
	{	
		var p = new mRecordPersonPMWorkOrder
		(
			{
				'SCH_PM_ID':SchIdPMWorkOrder,
				'SERVICE_ID':cellSelectServicePMWorkOrder.data.SERVICE_ID,
				'ROW_SCH':cellSelectServicePMWorkOrder.data.ROW_SCH,
				'EMP_ID':'',
				'EMP_NAME':'',
				'VENDOR_ID':'',
				'VENDOR':'',
				'PERSON_NAME':'',
				'COST':dsDtlCraftPersonExtPMWorkOrder.data.items[CurrentPersonExtPMWorkOrder.row].data.COST,
				'DESC_WO_PM_PERSON':dsDtlCraftPersonExtPMWorkOrder.data.items[CurrentPersonExtPMWorkOrder.row].data.DESC_WO_PM_PERSON,
				'ROW_PERSON':dsDtlCraftPersonExtPMWorkOrder.data.items[CurrentPersonExtPMWorkOrder.row].data.ROW_PERSON,
				'ROW':dsDtlCraftPersonExtPMWorkOrder.data.items[CurrentPersonExtPMWorkOrder.row].data.ROW
			}
		);
		FormLookupVendor(p,str,'',true,dsDtlCraftPersonExtPMWorkOrder,false);
	};
};

function CalcTotalPersonPMWorkOrder(idx,mBolGrid)
{
	var ds;
	var col;
	if(IsExtRepairPMWorkOrder === false)
	{
		ds=dsDtlCraftPersonIntPMWorkOrder;
		col=Ext.get('fieldcolCostCraftPersonPMWorkOrder')
	}
	else
	{
		ds=dsDtlCraftPersonExtPMWorkOrder;
		col=Ext.get('fieldcolCostCraftPersonExtPMWorkOrder')
	};
	
    var total=Ext.num(0);
	for(var i=0;i < ds.getCount();i++)
	{
		if (i === idx)
		{
			if (col != null) 
			{
				if (Ext.num(col.dom.value) != null) 
				{
					total += (Ext.num(col.dom.value));
				};
			};
		}
		else
		{
			total += Ext.num(ds.data.items[i].data.COST);
		}
	}
	
	Ext.get('txtTotalPersonCostPMWorkOrder').dom.value = formatCurrency(total);	
	if (mBolGrid === true) {
	    try {
	        Ext.get('txtTotalServicePMWorkOrder').dom.value = formatCurrency(Ext.num(total) + Ext.num(CalcTotalPMResultAllService(true, false)));
	    }
	    catch (e)
		{ return};
		
	};

	// var x=0;
	// var xTemp=0;
	// x=Ext.num(GetNilaiCurrencyPMWorkOrder(Ext.get('txtTotalPersonCostPMWorkOrder').dom.value ));
	// Ext.get('txtTotalPersonCostPMWorkOrder').dom.value = formatCurrency(total);
	// if (mBolGrid === true)
	// {
		// xTemp=Ext.num(GetNilaiCurrencyPMWorkOrder(Ext.get('txtTotalServicePMWorkOrder').dom.value ))-x;
		// Ext.get('txtTotalServicePMWorkOrder').dom.value=formatCurrency(Ext.num(total) + Ext.num(xTemp));
	// };
};

function CalcTotalPMWorkOrderAllService(mBol,mBolPart)
{
	var ds;
	
	if (mBol != true)
	{
		if(IsExtRepairPMWorkOrder === false)
		{
			ds=dsDtlCraftPersonIntPMWorkOrderTemp;
		}
		else
		{
			ds=dsDtlCraftPersonExtPMWorkOrderTemp;
		};
	}
	else
	{
		ds=dsDtlPartPMWorkOrderTemp;
	};
	
    var total=Ext.num(0);
	for(var i=0;i < ds.getCount();i++)
	{
		if (mBol != true)
		{
			total += Ext.num(ds.data.items[i].data.COST);
		}
		else
		{	
			//total += Ext.num(ds.data.items[i].data.TOT_COST);
			total += Ext.num(ds.data.items[i].data.UNIT_COST * ds.data.items[i].data.QTY);
		};
	}
	
	if (mBolPart === true)
	{
		ds=dsDtlPartPMWorkOrderTemp;
	}
	else
	{
		if(IsExtRepairPMResult === false)
		{
			ds=dsDtlCraftPersonIntPMWorkOrderTemp;
		}
		else
		{
			ds=dsDtlCraftPersonExtPMWorkOrderTemp;
		};
	};
	
	for(var i=0;i < ds.getCount();i++)
	{
		if (mBolPart === true)
		{
			if ( ds.data.items[i].data.SERVICE_ID != cellSelectServicePMResult.data.SERVICE_ID)
			{
				//total += Ext.num(ds.data.items[i].data.TOT_COST);
				total += Ext.num(ds.data.items[i].data.UNIT_COST * ds.data.items[i].data.QTY);
			};
		}
		else
		{	
			if ( ds.data.items[i].data.SERVICE_ID != cellSelectServicePMResult.data.SERVICE_ID)
			{
				total += Ext.num(ds.data.items[i].data.COST);
			};
		};
	};
	
	return total;
};

function GetDTLServicePMWorkOrderGrid() 
{
    var fldDetail = ['WO_PM_ID','SCH_PM_ID','SERVICE_ID','SERVICE_NAME','TAG','CATEGORY_ID','DUE_DATE','ROW_SCH','CATEGORY_ID'];

    dsDtlServicePMWorkOrder = new WebApp.DataStore({ fields: fldDetail })

    var gridServicePMWorkOrder  = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlServicePMWorkOrder,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:679,//666,
		    height:178,//134,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
					    {
					        cellSelectServicePMWorkOrder = dsDtlServicePMWorkOrder.getAt(row);
					        CurrentServicePMWorkOrder.row = row;
					        CurrentServicePMWorkOrder.data = cellSelectServicePMWorkOrder;
							GetPartPMWorkOrderSelectService(cellSelectServicePMWorkOrder.data.SERVICE_ID,cellSelectServicePMWorkOrder.data.ROW_SCH);
					    }
					}
				}
			),
				cm: ServicePMWorkOrderDetailColumModel(),
				 tbar: 
			         [
				        {
					        id:'btnTambahBrsServicePMWorkOrder',
							name:'btnTambahBrsServicePMWorkOrder',
					        text: nmTambahBaris,
					        tooltip: nmTambahBaris,
					        iconCls: 'AddRow',
					        handler: function() 
					        { 
        						//TambahBarisServicePMWorkOrder(dsDtlServicePMWorkOrder);
								var str='';
								var strList = GetStrListServicePMWorkOrder(false);
								var strList2 = GetStrListServicePMWorkOrder(true);
								
								if (strList !='')
								{
									//strList = ' and SERVICE_ID not in ( ' + strList + ') '
                                                                        strList = ' and service_id not in ( ' + strList + ') '
								};
								
								// if (strList2 !='')
								// {
									// strList2 = ' and ROW_SCH not in ( ' + strList2 + ') '
								// };
								
								strList2 += strList;
								
								var p = GetRecordBaruServicePMWorkOrder()
								if(rowSelectedPMWorkOrderTemp != '' && rowSelectedPMWorkOrderTemp != undefined)
								{
									// str = rowSelectedPMWorkOrderTemp.SCH_PM_ID + '@#@' ;
									// str += rowSelectedPMWorkOrderTemp.CATEGORY_ID + '@#@' ;
									// str += ShowDate(rowSelectedPMWorkOrderTemp.DUE_DATE) + '@#@' ;
									// str += strList
									//str = ' where sch_pm_id=~'+rowSelectedPMWorkOrderTemp.SCH_PM_ID + '~ and ' ;
                                                                        str = ' sch_pm_id = ~'+rowSelectedPMWorkOrderTemp.SCH_PM_ID + '~ and ' ;
									str += 'category_id = ~'+rowSelectedPMWorkOrderTemp.CATEGORY_ID + '~ and ' ;
									str += 'due_date = ~'+ShowDate(rowSelectedPMWorkOrderTemp.DUE_DATE) + '~  ' ;
									str += strList
								};
								
								FormLookupServiceWOPM(str,p,dsDtlServicePMWorkOrder)
					        }
				        },'-',
				        {
					        id:'btnHpsBrsServicePMWorkOrder',
					        text: nmHapusBaris,
					        tooltip: nmHapusBaris,
					        iconCls: 'RemoveRow',
					        handler: function()
					        {
								if (dsDtlServicePMWorkOrder.getCount() > 0 )
								{
									if (cellSelectServicePMWorkOrder != undefined)
									{
										if(CurrentServicePMWorkOrder != undefined)
										{
											HapusBarisServicePMWorkOrder();
										};
									}
									else
									{
										ShowPesanWarningPMWorkOrder(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
					        }
				        },' ','-'
			        ]
		    , viewConfig:{forceFit: true}
		}
	);

		return gridServicePMWorkOrder;
};

function HapusBarisServicePMWorkOrder()
{
	if (cellSelectServicePMWorkOrder.data.SERVICE_ID != '' && cellSelectServicePMWorkOrder.data.SERVICE_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentServicePMWorkOrder.row + 1) + ' ' + nmOperatorDengan + ' ' + nmServIDWOPM + ' : ' + cellSelectServicePMWorkOrder.data.SERVICE_ID + ' ' + nmOperatorAnd + ' ' + nmServNameWOPM + ' : ' + cellSelectServicePMWorkOrder.data.SERVICE_NAME ,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlServicePMWorkOrder.data.items[CurrentServicePMWorkOrder.row].data.TAG === 1 || dsDtlServicePMWorkOrder.data.items[CurrentServicePMWorkOrder.row].data.WO_PM_ID === '')
							{
								deletePersonPartServiceRowDeletePMWorkOrder(cellSelectServicePMWorkOrder.data.SERVICE_ID);
								dsDtlServicePMWorkOrder.removeAt(CurrentServicePMWorkOrder.row);
								cellSelectServicePMWorkOrder=undefined;
								
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg:nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailServicePMWorkOrder();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			dsDtlServicePMWorkOrder.removeAt(CurrentServicePMWorkOrder.row);
		};
};

function deletePersonPartServiceRowDeletePMWorkOrder(service_id)
{
	var y=0;
	if (service_id != '' && service_id != undefined)
	{
		if(IsExtRepairPMWorkOrder === true)
		{
			for (var i = 0; i < dsDtlCraftPersonExtPMWorkOrderTemp.getCount() ; i++) 
			{
				if(dsDtlCraftPersonExtPMWorkOrderTemp.data.items[i].data.SERVICE_ID === service_id)
				{
					dsDtlCraftPersonExtPMWorkOrderTemp.removeAt(i);
				};
			};
		}
		else
		{
			for (var i = 0; i < dsDtlCraftPersonIntPMWorkOrderTemp.getCount() ; i++) 
			{
				if(dsDtlCraftPersonIntPMWorkOrderTemp.data.items[i].data.SERVICE_ID === service_id)
				{
					dsDtlCraftPersonIntPMWorkOrderTemp.removeAt(i);
				};
			};
		};
		
		for (var i = 0; i < dsDtlPartPMWorkOrderTemp.getCount() ; i++) 
		{
			if(dsDtlPartPMWorkOrderTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlPartPMWorkOrderTemp.removeAt(i);
			};
		};
		
		for (var i = 0; i < dsDtlPartPMWorkOrderTemp.getCount() ; i++) 
		{
			if(dsDtlPartPMWorkOrderTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				y += (dsDtlPartPMWorkOrderTemp.data.items[i].data.UNIT_COST * dsDtlPartPMWorkOrderTemp.data.items[i].data.QTY);
				dsDtlPartPMWorkOrderTemp.removeAt(i);
			};
		};
		
		var a=GetNilaiCurrencyPMWorkOrder(Ext.get('txtTotalServicePMWorkOrder').dom.value);
		
		if (a > 0)
		{
			Ext.get('txtTotalServicePMWorkOrder').dom.value=formatCurrency(a-y);
		}
		else
		{
			Ext.get('txtTotalServicePMWorkOrder').dom.value=0
		};
		
	};
};


function GetLookupServicePMWorkOrder(str,idx,ds)
{
	if (AppIdPMWorkOrder === '' || AppIdPMWorkOrder === undefined)
	{
		var p = new mRecordServicePMWorkOrder
		(
			{
				'WO_PM_ID':'',
				'SCH_PM_ID':SchIdPMWorkOrder,
				'SERVICE_ID':'',
				'SERVICE_NAME':'',
				'CATEGORY_ID':varCatIdPMWorkOrder,
				'ROW_SCH':'',
				'DUE_DATE':'',
				'TAG':1
			}
		);
		FormLookupServiceCategory('','',str,true,p,ds,false);
	}
	else
	{	
		var p = new mRecordServicePMWorkOrder
		(
			{
				'WO_PM_ID':AppIdPMWorkOrder,
				'SCH_PM_ID':SchIdPMWorkOrder,
				'SERVICE_ID':'',
				'SERVICE_NAME':'',
				'CATEGORY_ID':varCatIdPMWorkOrder,
				'ROW_SCH':'',
				'DUE_DATE':'',
				'TAG':1
			}
		);
		FormLookupServiceCategory('','',str,true,p,ds,false);
	};
};

function TambahBarisServicePMWorkOrder(ds)
{
	var p = GetRecordBaruServicePMWorkOrder();
	ds.insert(ds.getCount(), p);
};

function DeleteDetailServicePMWorkOrder()
{
	Ext.Ajax.request
	(
		{
			url:  WebAppUrl.UrlDeleteData, //"./Datapool.mvc/DeleteDataObj",
			params:  getParamDeleteDetailServicePMWorkOrder(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoPMWorkOrder(nmPesanHapusSukses,nmHeaderHapusData);
					deletePersonPartServiceRowDeletePMWorkOrder(cellSelectServicePMWorkOrder.data.SERVICE_ID);
					GetJumlahTotalPersonPartPMWorkOrder(true);
					dsDtlServicePMWorkOrder.removeAt(CurrentServicePMWorkOrder.row);
					cellSelectServicePMWorkOrder=undefined;
					LoadDataServicePMWorkOrder(SchIdPMWorkOrder);
					
					AddNewPMWorkOrder = false;
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningPMWorkOrder(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else if (cst.success === false && cst.pesan===1)
				{
					ShowPesanWarningPMWorkOrder(nmPesanHapusGagal + ' , ' + nmKonfirmasiResult,nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorPMWorkOrder(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailServicePMWorkOrder()
{
	var params =
	{	
		Table: 'ViewWorkOrderPM',   
	    WOId: AppIdPMWorkOrder,
		SchId:SchIdPMWorkOrder,
		CatId:varCatIdPMWorkOrder,
		ServiceId: cellSelectServicePMWorkOrder.data.SERVICE_ID,
		RowSch:cellSelectServicePMWorkOrder.data.ROW_SCH,
		Hapus:1,
		Service:1
	};
	
    return params;
};


function ServicePMWorkOrderDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdServicePMWorkOrder',
			    header: nmServIDWOPM,
			    dataIndex: 'SERVICE_ID',
			    width: 150//,
			    // editor: new Ext.form.TextField
				// (
					// {
					    // id: 'fieldcolKdServicePMWorkOrder',
					    // allowBlank: false,
					    // enableKeyEvents: true,
					    // listeners:
						// {
						    // 'specialkey': function() 
							// {
						        // if (Ext.EventObject.getKey() === 13) 
								// {
						            // var str = '';
									// var strList = GetStrListServicePMWorkOrder();
									// if (strList !='')
									// {
									    // strList = ' and SERVICE_ID not in ( ' + strList + ') '
									// };
									
						            // if (Ext.get('fieldcolKdServicePMWorkOrder').dom.value != '')
									// {
										// str = ' where category_id = ~' + varCatIdPMWorkOrder + '~'
										// str += strList;
										// str += ' and SERVICE_ID like ~%' + Ext.get('fieldcolKdServicePMWorkOrder').dom.value + '%~';
									// }
									// else
									// {
										// str = ' where category_id = ~' + varCatIdPMWorkOrder + '~';
										// str += strList;
									// };
									// GetLookupServicePMWorkOrder(str,CurrentServicePMWorkOrder.row,dsDtlServicePMWorkOrder);  
						        // };
						    // },
							// 'focus' : function()
							// {
								// FocusCtrlPMWorkOrder='colService';
							// }
						// }
					// }
				// )
			},
			{
			    id: 'colServiceNamePMWorkOrder',
			    header: nmServNameWOPM,
			    dataIndex: 'SERVICE_NAME',
			    sortable: false,
			    // editor: new Ext.form.TextField
				// (
					// {
					    // id: 'fieldcolServiceNamePMWorkOrder',
					    // allowBlank: false,
					    // enableKeyEvents: true,
					    // listeners:
						// {
						    // 'specialkey': function() 
							// {
						        // if (Ext.EventObject.getKey() === 13) 
								// {
						            // var str = '';
									// var strList = GetStrListServicePMWorkOrder();
									// if (strList !='')
									// {
									    // strList = ' and SERVICE_ID not in ( ' + strList + ') '
									// };
									
						            // if (Ext.get('fieldcolServiceNamePMWorkOrder').dom.value != '')
									// {
										// str = ' where category_id = ~' + varCatIdPMWorkOrder + '~'
										// str += strList;
										// str += ' and  SERVICE_NAME like ~%' + Ext.get('fieldcolServiceNamePMWorkOrder').dom.value + '%~';
									// }
									// else
									// {
										// str = ' where category_id = ~' + varCatIdPMWorkOrder + '~'
										// str += strList;
									// };
									// GetLookupServicePMWorkOrder(str,CurrentServicePMWorkOrder.row,dsDtlServicePMWorkOrder);  
						        // };
						    // },
							// 'focus' : function()
							// {
								// FocusCtrlPMWorkOrder='colService';
							// }
						// }
					// }
				// ),
			    width: 200,
			    renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
			    }
			},
			{ 
				id: 'colDueDateWOPMWorkOrder',
				header: nmDueDateWOPM,
				dataIndex: 'DUE_DATE',
				width: 100,
				renderer: function(v, params, record) 
				{
					return ShowDate(record.data.DUE_DATE);
				}
            }
		]
	)
};

function GetDTLPartPMWorkOrderGrid() 
{
    var fldDetail = ['SCH_PM_ID','SERVICE_ID','PART_ID','QTY','UNIT_COST','TOT_COST','PART_NAME','PRICE','DESC_SCH_PART','TAG','ROW','INSTRUCTION'];

    dsDtlPartPMWorkOrder = new WebApp.DataStore({ fields: fldDetail })
	dsDtlPartPMWorkOrderTemp = new WebApp.DataStore({ fields: fldDetail })

    var gridPartPMWorkOrder = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDtlPartPMWorkOrder,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			width:679,
		    height: 178,//120,//150,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
							cellSelectPartPMWorkOrder = dsDtlPartPMWorkOrder.getAt(row);
					        CurrentPartPMWorkOrder.row = row;
					        CurrentPartPMWorkOrder.data = cellSelectPartPMWorkOrder;
					    }
					}
				}
			),
			cm: PartPMWorkOrderDetailColumModel(),
		    // tbar:
			         // [
				        // {
				            // id: 'btnTambahBrsPartPMWorkOrder',
				            // text: 'Add Row',
				            // tooltip: 'Add Row',
				            // iconCls: 'AddRow',
				            // handler: function() 
							// {
								// TambahBarisPartPMWorkOrder(dsDtlPartPMWorkOrder);
				            // }
				        // }, '-',
				        // {
				            // id: 'btnHpsBrsPartPMWorkOrder',
				            // text: 'Remove Row',
				            // tooltip: 'Remove Row',
				            // iconCls: 'RemoveRow',
				            // handler: function() 
							// {
								// if (dsDtlPartPMWorkOrder.getCount() > 0 )
								// {
									// if (cellSelectPartPMWorkOrder != undefined)
									// {
										// if(CurrentPartPMWorkOrder != undefined)
										// {
											// HapusBarisPartPMWorkOrder();
										// };
									// }
									// else
									// {
										// ShowPesanWarningPMWorkOrder('Please select row to edit','Remove Row');
									// };
								// };
				            // }
				        // }, ' ', '-'
			            // ]
		    viewConfig: { forceFit: true }
		}
	);

		return gridPartPMWorkOrder;
};

function HapusBarisPartPMWorkOrder()
{
	if (cellSelectPartPMWorkOrder.data.PART_ID != '' && cellSelectPartPMWorkOrder.data.PART_NAME != '')
		{
			Ext.Msg.show
			(
				{
				   title:'Remove Row',
				   msg: 'Are you sure delete this row ?' + ' ' + 'Baris :'+ ' ' + (CurrentPartPMWorkOrder.row + 1) + ' dengan Part  Number : '+ ' ' + cellSelectPartPMWorkOrder.data.PART_ID + ' dan Part Name : ' + cellSelectPartPMWorkOrder.data.PART_NAME ,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDtlPartPMWorkOrder.data.items[CurrentPartPMWorkOrder.row].data.TAG === 1 )
							{
								var x=GetNilaiCurrencyPMWorkOrder(Ext.get('txtTotalPartPMWorkOrder').dom.value);
								if (x > 0)
								{
									Ext.get('txtTotalPartPMWorkOrder').dom.value=formatCurrency(x-(CurrentPartPMWorkOrder.data.data.UNIT_COST * CurrentPartPMWorkOrder.data.data.QTY));
								}
								else
								{
									Ext.get('txtTotalPartPMWorkOrder').dom.value=0;
								};
								HapusRowTempPMWorkOrder(dsDtlPartPMWorkOrderTemp,CurrentPartPMWorkOrder.data.data.ROW);
								dsDtlPartPMWorkOrder.removeAt(CurrentPartPMWorkOrder.row);
								
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:'Remove Row',
									   msg: 'Apakah anda yakin baris ini akan dihapus ?, Data ini telah tersimpan ke database' ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												DeleteDetailPartPMWorkOrder();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			dsDtlPartPMWorkOrder.removeAt(CurrentPartPMWorkOrder.row);
		};
};

function DeleteDetailPartPMWorkOrder()
{
	Ext.Ajax.request
	(
		{
			url:  WebAppUrl.UrlDeleteData, // "./Datapool.mvc/DeleteDataObj",
			params:  getParamDeleteDetailPartPMWorkOrder(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					GetJumlahTotalPartPMWorkOrder();
					HapusRowTempPMWorkOrder(dsDtlPartPMWorkOrderTemp,CurrentPartPMWorkOrder.data.data.ROW);
					dsDtlPartPMWorkOrder.removeAt(CurrentPartPMWorkOrder.row);
					cellSelectPartPMWorkOrder=undefined;
					LoadDataPartPMWorkOrderTemp(SchIdPMWorkOrder,varCatIdPMWorkOrder);
					LoadDataPartPMWorkOrder(SchIdPMWorkOrder,varCatIdPMWorkOrder,cellSelectServicePMWorkOrder.data.SERVICE_ID);
					AddNewPMWorkOrder = false;
					ShowPesanInfoPMWorkOrder(nmPesanHapusSukses,nmHeaderHapusData);
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningPMWorkOrder(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else if (cst.success === false && cst.pesan===1)
				{
					ShowPesanWarningPMWorkOrder(nmPesanHapusGagal + ' , ' + nmKonfirmasiResult,nmHeaderHapusData);
				}
				else 
				{
					ShowPesanErrorPMWorkOrder(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamDeleteDetailPartPMWorkOrder()
{
	
	var params =
	{	
		Table: 'ViewWorkOrderPM',
		WOId: AppIdPMWorkOrder,		
	    SchId:SchIdPMWorkOrder,
		CatId:varCatIdPMWorkOrder,
		ServiceId:cellSelectServicePMWorkOrder.data.SERVICE_ID,
		PartId: CurrentPartPMWorkOrder.data.data.PART_ID,
		Hapus:4
	};
	
    return params;
};

function TambahBarisPartPMWorkOrder(ds)
{
	var p = GetRecordBaruPartPMWorkOrder();
	ds.insert(ds.getCount(), p);
};

function PartPMWorkOrderDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdPartPMWorkOrder',
			    header: nmPartNumWOPM,
			    dataIndex: 'PART_ID',
			    width: 140,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdPartPMWorkOrder',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListPartPMWorkOrder(false);
									
									if(Ext.get('fieldcolKdPartPMWorkOrder').dom.value != '')
									{
										
										//str +=' where PART_ID like ~%' + Ext.get('fieldcolKdPartPMWorkOrder').dom.value + '%~';
                                                                                str +=' part_id like ~%' + Ext.get('fieldcolKdPartPMWorkOrder').dom.value + '%~';
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									}
									else
									{
										if (strList !='')
										{
											//strList = ' where PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									};
									GetLookupPartPMWorkOrder(str,CurrentPartPMWorkOrder.row,dsDtlPartPMWorkOrder);
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMWorkOrder='colPart';
							}
						}
					}
				)
			},
			{
			    id: 'colPartPMWorkOrder',
			    header: nmPartNameWOPM,
			    dataIndex: 'PART_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolPartPMWorkOrder',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var str = '';
									var strList = GetStrListPartPMWorkOrder(false);
									
									if(Ext.get('fieldcolPartPMWorkOrder').dom.value != '')
									{
										
										//str += ' where PART_NAME like ~%' + Ext.get('fieldcolPartPMWorkOrder').dom.value + '%~';
                                                                                str += ' part_name like ~%' + Ext.get('fieldcolPartPMWorkOrder').dom.value + '%~';
										if (strList !='')
										{
											//strList = ' and PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' and part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									}
									else
									{
										//str = ' where category_id = ~' + varCatIdPMWorkOrder + '~'
										if (strList !='')
										{
											//strList = ' where PART_ID not in ( ' + strList + ') ';
                                                                                        strList = ' part_id not in ( ' + strList + ') ';
											str += strList ;
										};
									};
									GetLookupPartPMWorkOrder(str,CurrentPartPMWorkOrder.row,dsDtlPartPMWorkOrder);
						        };
						    },
							'focus' : function()
							{
								FocusCtrlPMWorkOrder='colPart';
							}
						}
					}
				),
			    width: 200,
			    renderer: function(value, cell) 
				{
			        var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
			        return str;
			    }
			},
			{
			    id: 'colQtyPartPMWorkOrder',
			    header: nmQtyWOPM,
				align:'center',
			    width: 50,
			    dataIndex: 'QTY',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolQtyPartPMWorkOrder',
					    allowBlank: false,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPartPMWorkOrder(CurrentPartPMWorkOrder.row,false,true);
								}; 
							}
						}
					}
				)
			},
			{
			    id: 'colCostPartPMWorkOrder',
			    header: nmUnitCostWOPM,
			    width: 150,
				align:'right',
			    dataIndex: 'UNIT_COST',
				editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostPartPMWorkOrder',
					    allowBlank: false,
					    enableKeyEvents: true,
					    width: 90,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									CalcTotalPartPMWorkOrder(CurrentPartPMWorkOrder.row,true,true);
								}; 
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.UNIT_COST);
				}	
			},
			{
			    id: 'colTotCostPartInternalPMWorkOrder',
			    header: nmTotCostWOPM,
			    width: 150,
				align:'right',
			    dataIndex: 'TOT_COST',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.UNIT_COST * record.data.QTY);
				}	
			},
			{
			    id: 'colDescPartPMWorkOrder',
			    header: nmDescWOPM,
			    width: 200,
			    dataIndex: 'INSTRUCTION',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolDescPartPMWorkOrder',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(v, params, record) //(value, cell) 
				{
			        // var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
			        // return str;
					return record.data.INSTRUCTION;
			    }
			}
		]
	)
};

function GetLookupPartPMWorkOrder(str,idx,ds)
{
	if (SchIdPMWorkOrder === '' || SchIdPMWorkOrder === undefined)
	{
		var p = new mRecordPartPMWorkOrder
		(
			{
				'SCH_PM_ID':SchIdPMWorkOrder,
				'SERVICE_ID':cellSelectServicePMWorkOrder.data.SERVICE_ID,
				'ROW_SCH':cellSelectServicePMWorkOrder.data.ROW_SCH,
				'PART_ID':'',
				'PART_NAME':'', 
				'QTY':ds.data.items[idx].data.QTY,
				'UNIT_COST':ds.data.items[idx].data.UNIT_COST,
				'TOT_COST':ds.data.items[idx].data.UNIT_COST * ds.data.items[idx].data.QTY,
				'DESC_SCH_PART':ds.data.items[idx].data.DESC_SCH_PART,
				'TAG':1,
				'ROW': ds.data.items[idx].data.ROW
			}
		);
		FormLookupPart(str,ds,p,true,'',false);
	}
	else
	{	
		var p = new mRecordPartPMWorkOrder
		(
			{
				'SCH_PM_ID':SchIdPMWorkOrder,
				'PART_ID':'',
				'SERVICE_ID':cellSelectServicePMWorkOrder.data.SERVICE_ID,
				'ROW_SCH':cellSelectServicePMWorkOrder.data.ROW_SCH,
				'PART_NAME':'', 
				'QTY':ds.data.items[idx].data.QTY,
				'UNIT_COST':ds.data.items[idx].data.UNIT_COST,
				'TOT_COST':ds.data.items[idx].data.UNIT_COST * ds.data.items[idx].data.QTY,
				'DESC_SCH_PART':ds.data.items[idx].data.DESC_SCH_PART,
				'TAG':1,
				'ROW': ds.data.items[idx].data.ROW
			}
		);
		FormLookupPart(str,ds,p,false,idx,false);
	};
};


function CalcTotalPartPMWorkOrder(idx,mCost,mBolGrid)
{
	var total=Ext.num(0);
	for(var i=0;i < dsDtlPartPMWorkOrder.getCount();i++)
	{
		if (i === idx)
		{
			if (mCost===true)
			{
				if (Ext.get('fieldcolCostPartPMWorkOrder') != null) 
				{
					if (Ext.num(Ext.get('fieldcolCostPartPMWorkOrder').dom.value) != null) 
					{
						total += (Ext.num(Ext.get('fieldcolCostPartPMWorkOrder').dom.value) * Ext.num(dsDtlPartPMWorkOrder.data.items[i].data.QTY));
					};
			    };
			}
		    else
			{
			    if (Ext.get('fieldcolQtyPartPMWorkOrder') != null)
				{
			        if (Ext.num(Ext.get('fieldcolQtyPartPMWorkOrder').dom.value) != null) 
					{
			            total += (Ext.num(Ext.get('fieldcolQtyPartPMWorkOrder').dom.value) * Ext.num(dsDtlPartPMWorkOrder.data.items[i].data.UNIT_COST));
			        };
			    };
			}
		}
		else
		{
			total += Ext.num(dsDtlPartPMWorkOrder.data.items[i].data.UNIT_COST)*Ext.num(dsDtlPartPMWorkOrder.data.items[i].data.QTY);
		}
	}

	Ext.get('txtTotalPartPMWorkOrder').dom.value = formatCurrency(total);
	if (mBolGrid === true)
	{
		Ext.get('txtTotalServicePMWorkOrder').dom.value=formatCurrency(Ext.num(total) + Ext.num(GetNilaiCurrencyPMWorkOrder(Ext.get('txtTotalPersonCostPMWorkOrder').dom.value)));
	};
};


function getItemPanelRadioVendorPMWorkOrder(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 56,
		height:30,
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					getRadioVendorPMWorkOrder()
				]
			},
			{
			    columnWidth: .2,
			    layout: 'form',
			    border: false,
				labelWidth:58,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmVendIDNameWOPM + ' ',
					    name: 'txtKdVendorPMWorkOrder',
					    id: 'txtKdVendorPMWorkOrder',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
					    name: 'txtNamaVendorPMWorkOrder',
					    id: 'txtNamaVendorPMWorkOrder',
					    anchor: '99%',
					    listeners:
						{
						    'specialkey': function() {
						        if (Ext.EventObject.getKey() === 13) 
								{
						            var criteria='';
									if (Ext.get('txtNamaVendorPMWorkOrder').dom.value != '')
									{
										//criteria = ' where VENDOR like ~%' + Ext.get('txtNamaVendorPMWorkOrder').dom.value + '%~';
                                                                                criteria = ' vendor like ~%' + Ext.get('txtNamaVendorPMWorkOrder').dom.value + '%~';
									};
						            FormLookupVendor(GetItemLookupVendorPMWorkOrder(),criteria);
						        };
						    }
						},
						'focus' : function()
						{
							FocusCtrlPMWorkOrder='colVendor';
						}
					}
				]
			},
			{
			    columnWidth: .15,
			    layout: 'form',
				id:'layoutBtnVendorPMWorkOrder',
				name:'layoutBtnVendorPMWorkOrder',
			    border: false,
			    items:
				[
					{
						xtype:'button',
						text:nmBtnVendorInfo,
						width:90,
						hideLabel:true,
						id: 'btnLookupVendorPMWorkOrder',
						handler:function() 
						{
							if(IsExtRepairPMWorkOrder === true)
							{
								if (rowSelectedLookVendor != undefined)
								{
									InfoVendorLookUp(rowSelectedLookVendor);
								}
								else
								{
									if(dsLookVendorListPMWorkOrder != undefined)
									{
										if(dsLookVendorListPMWorkOrder.getCount() > 0 )
										{
											InfoVendorLookUp(dsLookVendorListPMWorkOrder.data.items[0])
										};
									};
								};
							};
						}
					}
				]
			}
		]
	}
    return items;
};

function LoadVendorInfoDetailPMWorkOrder(id)
{
	var fldDetail = ['VENDOR_ID','VENDOR','CONTACT1','CONTACT2','VEND_ADDRESS','VEND_CITY','VEND_PHONE1','VEND_PHONE2','VEND_POS_CODE','COUNTRY'];
	
	dsLookVendorListPMWorkOrder = new WebApp.DataStore({ fields: fldDetail });
	
	dsLookVendorListPMWorkOrder.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'VENDOR_ID',
                                Sort: 'vendor_id',
				Sortdir: 'ASC',
				target: 'LookupVendor',
				//param: 'where vendor_id=~' + id + '~'
                                param: 'vendor_id = ~' + id + '~'
			}
		}
	);
	return dsLookVendorListPMWorkOrder;
};

function GetItemLookupVendorPMWorkOrder()
{
	var p = new mRecVendorPMWorkOrder
	(
		{
			KD:'txtKdVendorPMWorkOrder',
			NAME: 'txtNamaVendorPMWorkOrder'
		}
	);
	return p;
};


function getRadioVendorPMWorkOrder()
{
	var items=
	{
		xtype: 'radiogroup',
		fieldLabel: nmRepairWOPM  + ' ',
		itemCls: 'x-check-group-alt',
		anchor:'99%',
		columns: 2,
		items: 
		[        
			{
				boxLabel: nmRdoIntWOPM, 
				name: 'rdoVendorApproveReq', 
				inputValue: '0', 
				id:'rdoInternalApproveReq',
				style: 
				{
					'margin-top': '3.5px'
				},
				handler: function() 
				{
					if(Ext.get('rdoInternalApproveReq').dom.checked===true)
					{
						DisabledVendorPMWorkOrder(true);
						IsExtRepairPMWorkOrder=false;
						if (varTabRepairPMWorkOrder=== 2)
						{
							Ext.getCmp('FormPanelCardLayoutGridPersonPMWorkOrder').getLayout().setActiveItem(0);
							if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder.data != undefined)
							{
								if (cellSelectServicePMWorkOrder.data.SERVICE_ID != undefined && cellSelectServicePMWorkOrder.data.SERVICE_ID !='')
								{
									GetPersonIntServicePMWorkOrder(cellSelectServicePMWorkOrder);
									CalcTotalPersonPMWorkOrder('',false);
								};
							};
							FocusCtrlPMWorkOrder = 'colEmpRdo';
						};
					}
					else
					{
						DisabledVendorPMWorkOrder(false);
						IsExtRepairPMWorkOrder=true;
						if (varTabRepairPMWorkOrder=== 2)
						{
							Ext.getCmp('FormPanelCardLayoutGridPersonPMWorkOrder').getLayout().setActiveItem(1);
							if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder.data != undefined)
							{
								if (cellSelectServicePMWorkOrder.data.SERVICE_ID != undefined && cellSelectServicePMWorkOrder.data.SERVICE_ID !='')
								{
									GetPersonExtServicePMWorkOrder(cellSelectServicePMWorkOrder);
									CalcTotalPersonPMWorkOrder('',false);
								};
							};
							FocusCtrlPMWorkOrder = 'colVendorRdo';
						};
						
						if (AppIdPMWorkOrder != '' && SchIdPMWorkOrder != '' && varCatIdPMWorkOrder != '')
						{
							GetVendorPMWorkOrder()
						};
						
					};
				}
           },
			 {
				boxLabel: nmRdoExtWOPM, 
				name: 'rdoVendorApproveReq', 
				inputValue: '1', 
				id:'rdoExternalApproveReq',
				style: 
				{
				    'margin-top': '3.5px'
				},
				handler: function() 
				{
					if(Ext.get('rdoExternalApproveReq').dom.checked===true)
					{
						DisabledVendorPMWorkOrder(false);
						IsExtRepairPMWorkOrder=true;
						if (varTabRepairPMWorkOrder=== 2)
						{
							Ext.getCmp('FormPanelCardLayoutGridPersonPMWorkOrder').getLayout().setActiveItem(1);
							if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder.data != undefined)
							{
								if (cellSelectServicePMWorkOrder.data.SERVICE_ID != undefined && cellSelectServicePMWorkOrder.data.SERVICE_ID !='')
								{
									GetPersonExtServicePMWorkOrder(cellSelectServicePMWorkOrder);
									CalcTotalPersonPMWorkOrder('',false);
								};
							};
							FocusCtrlPMWorkOrder = 'colVendorRdo';
						};
						
						if (AppIdPMWorkOrder != '' && SchIdPMWorkOrder != '' && varCatIdPMWorkOrder != '')
						{
							GetVendorPMWorkOrder()
						};
					}
					else
					{
						DisabledVendorPMWorkOrder(true);
						IsExtRepairPMWorkOrder=false;
						if (varTabRepairPMWorkOrder=== 2)
						{
							Ext.getCmp('FormPanelCardLayoutGridPersonPMWorkOrder').getLayout().setActiveItem(0);
							if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder.data != undefined)
							{
								if (cellSelectServicePMWorkOrder.data.SERVICE_ID != undefined && cellSelectServicePMWorkOrder.data.SERVICE_ID !='')
								{
									GetPersonIntServicePMWorkOrder(cellSelectServicePMWorkOrder);
									CalcTotalPersonPMWorkOrder('',false);
								};
							};
							FocusCtrlPMWorkOrder = 'colEmpRdo';
						};
					};
				}
			}
		]
	};
	
	return items;
};


function GetVendorPMWorkOrder()
{
	 Ext.Ajax.request
	 (
		{
			url:  WebAppUrl.UrlExecProcess, //"./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetVendorPMWorkOrder',
				//Params:	'where wo_pm_id =~' + AppIdPMWorkOrder + '~ and sch_pm_id=~' + SchIdPMWorkOrder + '~ and category_id=~' + varCatIdPMWorkOrder + '~'
                                Params:	'wo_pm_id = ~' + AppIdPMWorkOrder + '~ and sch_pm_id = ~' + SchIdPMWorkOrder + '~ and category_id = ~' + varCatIdPMWorkOrder + '~'
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.get('txtKdVendorPMWorkOrder').dom.value=cst.Id;
					Ext.get('txtNamaVendorPMWorkOrder').dom.value=cst.Nama;
					LoadVendorInfoDetailPMWorkOrder(cst.Id);
				}
				else
				{
					Ext.get('txtKdVendorPMWorkOrder').dom.value='';
					Ext.get('txtNamaVendorPMWorkOrder').dom.value='';
				};
			}

		}
	);
};




function getItemPanelNoAssetPMWorkOrder(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoAssetPMWorkOrder2(lebar) 
				]
			},
			{
			    columnWidth: .4,
			    layout: 'form',
			    border: false,
				labelWidth:70,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmLocWOPM +' ',
					    name: 'txtLocationPMWorkOrder',
					    id: 'txtLocationPMWorkOrder',
						readOnly:true,
					    anchor: '99.99%'
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelApproverPMWorkOrder(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmPICWOPM + ' ',
					    name: 'txtKdApproverPMWorkOrder',
					    id: 'txtKdApproverPMWorkOrder',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .7,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
					    name: 'txtNamaApproverPMWorkOrder',
					    id: 'txtNamaApproverPMWorkOrder',
					    anchor: '99.99%',
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var criteria='';
									if (Ext.get('txtNamaApproverPMWorkOrder').dom.value != '')
									{
										//criteria = ' where EMP_NAME like ~%' + Ext.get('txtNamaApproverPMWorkOrder').dom.value + '%~';
                                                                                criteria = 'emp_name like ~%' + Ext.get('txtNamaApproverPMWorkOrder').dom.value + '%~';
									};
									FormLookupEmployee('txtKdApproverPMWorkOrder','txtNamaApproverPMWorkOrder',criteria);
								};
							},
							'focus' : function()
							{
								FocusCtrlPMWorkOrder='txtPIC';
							}
						}
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelTanggalAppPMWorkOrder(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    width: lebar - 56,
		style:{'margin-top': '3px'},
	    items:
		[
			{
			    columnWidth: .32,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						 xtype: 'datefield',
					    fieldLabel: nmWODateWOPM + ' ',
					    id: 'dtpPengerjaanPMWorkOrder',
					    name: 'dtpPengerjaanPMWorkOrder',
					    format: 'd/M/Y',
					    value: NowPMWorkOrder,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:70,
			    items:
				[
					{
						 xtype: 'datefield',
					    fieldLabel: nmFinishDateWOPM + ' ',
					    id: 'dtpSelesaiPMWorkOrder',
					    name: 'dtpSelesaiPMWorkOrder',
					    format: 'd/M/Y',
					    value: NowPMWorkOrder,
					    anchor: '98%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoAssetPMWorkOrder2(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		width: lebar - 56,
	    items:
		[
			{
			    columnWidth: .3,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmAssetIdNameEntryWOPM + ' ',
					    name: 'txtKdMainAssetPMWorkOrder',
					    id: 'txtKdMainAssetPMWorkOrder',
						readOnly:true,
					    anchor: '98%'
					}
				]
			},
			{
			    columnWidth: .6,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
						readOnly:true,
					    name: 'txtNamaMainAsetPMWorkOrder',
					    id: 'txtNamaMainAsetPMWorkOrder',
					    anchor: '50%'
					}
				]
			}
		]
	}
    return items;
};


function mComboMaksDataPMWorkOrder() 
{
    var cboMaksDataPMWorkOrder = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataPMWorkOrder',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmMaksData + ' ',
		    width: 50,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountPMWorkOrder,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountPMWorkOrder = b.data.displayText;
			        RefreshDataPMWorkOrderFilter();
			    }
			}
		}
	);
    return cboMaksDataPMWorkOrder;
};


function PMWorkOrderSave(mBol) 
{	
	if (ValidasiEntryPMWorkOrder(nmHeaderSimpanData,false) == 1 )
	{
		if (AppIdPMWorkOrder == '' || AppIdPMWorkOrder === undefined) 
		{
			Ext.Ajax.request
			(
				{
					url:  WebAppUrl.UrlSaveData, //"./Datapool.mvc/CreateDataObj",
					params: getParamPMWorkOrder(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true && cst.App === true) 
						{
							ShowPesanInfoPMWorkOrder(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataPMWorkOrder();
							if(mBol === false)
							{
								AppIdPMWorkOrder=cst.WOId;
								
//								var str = ' where category_id =~' + varCatIdPMWorkOrder + '~'
//									str += ' and sch_pm_id =~' + SchIdPMWorkOrder + '~'
//									str += ' and wo_pm_id =~' + AppIdPMWorkOrder + '~'
								var str = 'category_id = ~' + varCatIdPMWorkOrder + '~'
									str += ' and sch_pm_id = ~' + SchIdPMWorkOrder + '~'
									str += ' and wo_pm_id = ~' + AppIdPMWorkOrder + '~'
								LoadDataServiceWOPMWorkOrder(str);
								
								var strListServ = GetStrListServicePMWorkOrder(false);
								var strListRowSch = GetStrListServicePMWorkOrder(true);
								var strPerson ='';
								    strPerson += str
									
									if (strListServ != '')
									{
										strPerson +=' and service_id in (' + strListServ + ')';
									};
									
									if (strListRowSch != '')
									{
										strPerson +=' and row_sch in (' + strListRowSch + ')';
									};
									
									GetJumlahTotalPersonPartPMWorkOrder(strPerson);
								
								if (IsExtRepairPMWorkOrder === false)
								{
									LoadDataEmployeePMWorkOrderTemp(strPerson);
									if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder != undefined)
									{
										var str2='';
											str2 += str + ' and service_id = ~' + cellSelectServicePMWorkOrder.data.SERVICE_ID + '~'
											str2 += ' and row_sch = ' + cellSelectServicePMWorkOrder.data.ROW_SCH
											
										LoadDataEmployeePMWorkOrder(str2);
									};
								}
								else
								{
									LoadDataVendorPMWorkOrderTemp(strPerson);
									if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder != undefined)
									{
										var str2 ='';
										    str2 += str + ' and service_id = ~' + cellSelectServicePMWorkOrder.data.SERVICE_ID + '~'
											str2 += ' and row_sch = ' + cellSelectServicePMWorkOrder.data.ROW_SCH
											
										LoadDataVendorPMWorkOrder(str2);
									};
								};
							};
							
							AddNewPMWorkOrder = false;
						}
						// else if (cst.success === true && cst.App === false) 
						// {
							// ShowPesanInfoPMWorkOrder('This request succeesed rejected',nmHeaderSimpanData);
						// }
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningPMWorkOrder(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorPMWorkOrder(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlSaveData, //"./Datapool.mvc/CreateDataObj",
					params: getParamPMWorkOrder(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoPMWorkOrder(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataPMWorkOrder();
							if(mBol === false)
							{
//								 var str = ' where category_id =~' + varCatIdPMWorkOrder + '~'
//									str += ' and sch_pm_id =~' + SchIdPMWorkOrder + '~'
//									str += ' and wo_pm_id =~' + AppIdPMWorkOrder + '~'

								 var str = ' category_id = ~' + varCatIdPMWorkOrder + '~'
									str += ' and sch_pm_id = ~' + SchIdPMWorkOrder + '~'
									str += ' and wo_pm_id = ~' + AppIdPMWorkOrder + '~'
		
								LoadDataServiceWOPMWorkOrder(str);
								
								
								var strListServ = GetStrListServicePMWorkOrder(false);
								var strListRowSch = GetStrListServicePMWorkOrder(true);
								var strPerson ='';
								    strPerson += str
									
									if (strListServ != '')
									{
										strPerson +=' and service_id in (' + strListServ + ')';
									};
									
									if (strListRowSch != '')
									{
										strPerson +=' and row_sch in (' + strListRowSch + ')';
									};
									
									GetJumlahTotalPersonPartPMWorkOrder(strPerson);
								
								if (IsExtRepairPMWorkOrder === false)
								{
									LoadDataEmployeePMWorkOrderTemp(strPerson);
									if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder != undefined)
									{
										var str2 = str + ' and service_id = ~' + cellSelectServicePMWorkOrder.data.SERVICE_ID + '~'
											str2 += ' and row_sch = ' + cellSelectServicePMWorkOrder.data.ROW_SCH
											
										LoadDataEmployeePMWorkOrder(str2);
									};
								}
								else
								{
									LoadDataVendorPMWorkOrderTemp(str);
									if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder != undefined)
									{
										var str2 = str + ' and service_id = ~' + cellSelectServicePMWorkOrder.data.SERVICE_ID + '~'
											str2 += ' and row_sch = ' + cellSelectServicePMWorkOrder.data.ROW_SCH
											
										LoadDataVendorPMWorkOrder(str2);
									};
								};
							};
						}
						// else if (cst.success === true && cst.App === false) 
						// {
							// ShowPesanInfoPMWorkOrder('This request succeesed rejected',nmHeaderSimpanData);
						// }
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningPMWorkOrder(nmPesanEditGagal,nmHeaderEditData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningPMWorkOrder(nmPesanEditGagal + ' , ' + nmKonfirmasiResult,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorPMWorkOrder(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};


function PMWorkOrderDelete()
{
    if ((AppIdPMWorkOrder != null) && (AppIdPMWorkOrder != '') && (AppIdPMWorkOrder != undefined) )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus('') ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData, //"./Datapool.mvc/DeleteDataObj",
								params: getParamPMWorkOrder(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoPMWorkOrder(nmPesanHapusSukses,nmHeaderHapusData);
										PMWorkOrderAddNew();
										RefreshDataPMWorkOrder();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningPMWorkOrder(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else if (cst.success === false && cst.pesan===1)
									{
										ShowPesanWarningPMWorkOrder(nmPesanHapusGagal + ' , ' + nmKonfirmasiResult,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorPMWorkOrder(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
};

function ValidasiEntryPMWorkOrder(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtKdApproverPMWorkOrder').getValue() == '') || (Ext.get('txtNamaApproverPMWorkOrder').getValue() == ''))
	{
		if (Ext.get('txtKdApproverPMWorkOrder').getValue() == '') 
		{
			x = 0;
			if (mBolHapus != true)
			{
				ShowPesanWarningPMWorkOrder(nmGetValidasiKosong(nmApproverIDWOPM), modul);
			};
		}
		else if (Ext.get('txtNamaApproverPMWorkOrder').getValue() == '') 
		{
			x = 0;
			if (mBolHapus != true)
			{
				ShowPesanWarningPMWorkOrder(nmGetValidasiKosong(nmApproverNameWOPM), modul);
			};
		}
	};
	return x;
};

function ShowPesanWarningPMWorkOrder(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPMWorkOrder(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPMWorkOrder(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};



function mComboCategoryPMWorkOrderView() 
{
	var Field = ['CATEGORY_ID','CATEGORY_NAME'];
    var dsCategoryPMWorkOrderView = new WebApp.DataStore({ fields: Field });
	
	Ext.TreeComboPMWorkOrderView = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treePMWorkOrderView = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreePMWorkOrderView  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						id:IdCategoryPMWorkOrderView,
						children: StrTreeComboPMWorkOrderView,
						autoScroll: true
					}
				) 
				treePMWorkOrderView.setRootNode(rootTreePMWorkOrderView); 
				
				this.list = treePMWorkOrderView
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboPMWorkOrderView.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treePMWorkOrderView
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				if (node.attributes.id === IdCategoryPMWorkOrderView)
				{
					this.setRawValue(valueCategoryPMWorkOrderView);
				}
				else
				{
					this.setRawValue(node.attributes.text);
				};
				
				selectCategoryPMWorkOrderView = node.attributes;
			
				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategoryPMWorkOrderView
		}
	);
	
	
	var cboCategoryPMWorkOrderView = new Ext.TreeComboPMWorkOrderView
	(
		{
			id:'cboCategoryPMWorkOrderView',
			fieldLabel: nmCatWOPM + ' ',
			width:150,
			value:valueCategoryPMWorkOrderView
		}
	);
	 
    return cboCategoryPMWorkOrderView;
};

function GetRecordBaruPartPMWorkOrder()
{
	var p = new mRecordPartPMWorkOrder
	(
		{
			SCH_PM_ID:SchIdPMWorkOrder,
			SERVICE_ID: cellSelectServicePMWorkOrder.data.SERVICE_ID,
			ROW_SCH:cellSelectServicePMWorkOrder.data.ROW_SCH,
			PART_ID :'',
		    PART_NAME :'',
		    QTY :1,
		    UNIT_COST :0,
			TOT_COST:0,
			DESC_SCH_PART:'',
			TAG:1,
			ROW:GetUrutRowPMWorkOrder(dsDtlPartPMWorkOrder)
		}
	);
	return p;
};

function GetRecordBaruPersonPMWorkOrder(ds)
{
	var p = new mRecordPersonPMWorkOrder
	(
		{
			SCH_PM_ID:'',
			SERVICE_ID:cellSelectServicePMWorkOrder.data.SERVICE_ID,
			ROW_SCH :cellSelectServicePMWorkOrder.data.ROW_SCH,
		    EMP_ID :'',
			EMP_NAME:'',
		    VENDOR_ID :GetVendorTambahBarisPMWorkOrder(1),
			VENDOR:GetVendorTambahBarisPMWorkOrder(2),
		    PERSON_NAME :'',
			COST:'',
			DESC_WO_PM_PERSON:'',
			ROW_PERSON:'',
			ROW:GetUrutRowPMWorkOrder(ds)
		}
	);
	return p;
};

function GetVendorTambahBarisPMWorkOrder(x)
{
	var str='';
	if(IsExtRepairPMWorkOrder === true)
	{
		if(x=== 1)
		{
			str=Ext.get('txtKdVendorPMWorkOrder').dom.value;
		}
		else
		{
			str=Ext.get('txtNamaVendorPMWorkOrder').dom.value;
		};
	};
	return str;
};

function GetRecordBaruServicePMWorkOrder()
{
	var p = new mRecordServicePMWorkOrder
	(
		{
			WO_PM_ID:'',
			SCH_PM_ID:SchIdPMWorkOrder,
			SERVICE_ID:'',
			SERVICE_NAME :'',
			CATEGORY_ID:varCatIdPMWorkOrder,
			ROW_SCH:'',
			DUE_DATE:'',
			TAG:1
		}
	);
	return p;
};

function GetNilaiCurrencyPMWorkOrder(dblNilai)
{
	for (var i = 0; i < dblNilai.length; i++) 
	{
		var y = dblNilai.substr(i, 1)
		if (y === '.') 
		{
			dblNilai = dblNilai.replace('.', '');
		}
	};
	
	return dblNilai;
};

function GetPersonIntServicePMWorkOrder(row)
{
	if (row != '' && row != undefined)
	{
		if(row.data != '' && row.data != undefined)
		{
			if ( (row.data.SERVICE_ID !='' && row.data.SERVICE_ID != undefined) && (row.data.ROW_SCH !='' && row.data.ROW_SCH != undefined))
			{
				dsDtlCraftPersonIntPMWorkOrder.removeAll();
				for (var i = 0; i < dsDtlCraftPersonIntPMWorkOrderTemp.getCount() ; i++) 
				{
					if((dsDtlCraftPersonIntPMWorkOrderTemp.data.items[i].data.SERVICE_ID === row.data.SERVICE_ID) && (dsDtlCraftPersonIntPMWorkOrderTemp.data.items[i].data.ROW_SCH === row.data.ROW_SCH))
					{
						dsDtlCraftPersonIntPMWorkOrder.insert(dsDtlCraftPersonIntPMWorkOrder.getCount(), dsDtlCraftPersonIntPMWorkOrderTemp.data.items[i]);
					};
				};
			};
		};
	};
};

function GetPersonExtServicePMWorkOrder(row)
{
	if (row != '' && row != undefined)
	{
		if(row.data != '' && row.data != undefined)
		{
			if ( (row.data.SERVICE_ID !='' && row.data.SERVICE_ID != undefined) && (row.data.ROW_SCH !='' && row.data.ROW_SCH != undefined))
			{
				dsDtlCraftPersonExtPMWorkOrder.removeAll();
				for (var i = 0; i < dsDtlCraftPersonExtPMWorkOrderTemp.getCount() ; i++) 
				{
					if((dsDtlCraftPersonExtPMWorkOrderTemp.data.items[i].data.SERVICE_ID === row.data.SERVICE_ID) && (dsDtlCraftPersonExtPMWorkOrderTemp.data.items[i].data.ROW_SCH === row.data.ROW_SCH))
					{
						dsDtlCraftPersonExtPMWorkOrder.insert(dsDtlCraftPersonExtPMWorkOrder.getCount(), dsDtlCraftPersonExtPMWorkOrderTemp.data.items[i]);
					};
				};
			};
		};
	};
};

function GetPartPMWorkOrder(row)
{
	if (row != '' && row != undefined)
	{
		if(row.data != '' && row.data != undefined)
		{
			if ( (row.data.SERVICE_ID !='' && row.data.SERVICE_ID != undefined) && (row.data.ROW_SCH !='' && row.data.ROW_SCH != undefined))
			{
				dsDtlPartPMWorkOrder.removeAll();
				for (var i = 0; i < dsDtlPartPMWorkOrderTemp.getCount() ; i++) 
				{
					if((dsDtlPartPMWorkOrderTemp.data.items[i].data.SERVICE_ID === row.data.SERVICE_ID) && (dsDtlPartPMWorkOrderTemp.data.items[i].data.ROW_SCH === row.data.ROW_SCH))
					{
						dsDtlPartPMWorkOrder.insert(dsDtlPartPMWorkOrder.getCount(), dsDtlPartPMWorkOrderTemp.data.items[i]);
					};
				};
			};
		};
	};
};

function GetPartPMWorkOrderSelectService(service_id,row)
{
	if(service_id != '' && service_id != undefined)
	{
		dsDtlPartPMWorkOrder.removeAll();
		for (var i = 0; i < dsDtlPartPMWorkOrderTemp.getCount() ; i++) 
		{
			if(dsDtlPartPMWorkOrderTemp.data.items[i].data.SERVICE_ID === service_id)
			{
				dsDtlPartPMWorkOrderTemp.data.items[i].data.ROW_SCH=row
			};
		};
	};
};

function HapusRowTempPMWorkOrder(ds,idx)
{
	if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder.data != undefined)
	{
		if (cellSelectServicePMWorkOrder.data.SERVICE_ID != undefined || cellSelectServicePMWorkOrder.data.SERVICE_ID != '')
		{
			for (var i = 0; i < ds.getCount() ; i++) 
			{
				if(ds.data.items[i].data.SERVICE_ID === cellSelectServicePMWorkOrder.data.SERVICE_ID && ds.data.items[i].data.ROW === idx)
				{
					ds.removeAt(i);
				};
			}
		};
	};
};

function HapusServiceRowTempPMWorkOrder(service_id)
{
	if (cellSelectServicePMWorkOrder != '' && cellSelectServicePMWorkOrder.data != undefined)
	{
		if (cellSelectServicePMWorkOrder.data.SERVICE_ID != undefined || cellSelectServicePMWorkOrder.data.SERVICE_ID != '')
		{
			for (var i = 0; i < ds.getCount() ; i++) 
			{
				if(ds.data.items[i].data.SERVICE_ID === cellSelectServicePMWorkOrder.data.SERVICE_ID && ds.data.items[i].data.ROW === idx)
				{
					ds.removeAt(i);
				};
			}
		};
	};
};



function GetStrListServicePMWorkOrder(mBolRow)
{
	var str='';
	
	for (var i = 0; i < dsDtlServicePMWorkOrder.getCount() ; i++) 
	{
		if (dsDtlServicePMWorkOrder.data.items[i].data.SERVICE_ID != '')
		{
			if(dsDtlServicePMWorkOrder.getCount() === 1)
			{
				if (mBolRow === true)
				{
					str +=  dsDtlServicePMWorkOrder.data.items[i].data.ROW_SCH  
				}
				else
				{
					str +=  '\'' + dsDtlServicePMWorkOrder.data.items[i].data.SERVICE_ID + '\'' 
				};
			}
			else
			{
				if (i === dsDtlServicePMWorkOrder.getCount()-1 )
				{
					if (mBolRow === true)
					{
						str +=  dsDtlServicePMWorkOrder.data.items[i].data.ROW_SCH  
					}
					else
					{
						str +=  '\'' + dsDtlServicePMWorkOrder.data.items[i].data.SERVICE_ID + '\'' 
					};
				}
				else
				{
					if (mBolRow === true)
					{
						str +=  dsDtlServicePMWorkOrder.data.items[i].data.ROW_SCH  + ',' ;
					}
					else
					{
						str +=  '\'' + dsDtlServicePMWorkOrder.data.items[i].data.SERVICE_ID + '\'' + ',' ;
					};
				};
			};
		};
	}

	return str;
};

function GetStrListPartPMWorkOrder(mBolLookup)
{
	var str='';
	
	for (var i = 0; i < dsDtlPartPMWorkOrder.getCount() ; i++) 
	{
		if(dsDtlPartPMWorkOrder.getCount() === 1)
		{
			str +=  '\'' + dsDtlPartPMWorkOrder.data.items[i].data.PART_ID + '\'';
		}
		else
		{
			if (i === dsDtlPartPMWorkOrder.getCount()-1 )
			{
				str +=  '\'' + dsDtlPartPMWorkOrder.data.items[i].data.PART_ID + '\'';
			}
			else
			{
				str +=  '\'' + dsDtlPartPMWorkOrder.data.items[i].data.PART_ID + '\'' + ',' ;
			};
		};	
	}
	
	return str;
};


function GetUrutRowPMWorkOrder(ds)
{
	var x=1;
	if (ds != undefined && ds.data != undefined )
	{
		if ( ds.data.length > 0 )
		{
			x = ds.data.items[ds.getCount()-1].data.ROW + 1;
		};
	};
	
	return x;
};

function GetJumlahTotalPersonPartPMWorkOrder(strPerson)
{
//	var strPart = ' where category_id =~' + varCatIdPMWorkOrder + '~'
//		strPart += ' and sch_pm_id =~' + SchIdPMWorkOrder + '~'
//		strPart += ' and wo_pm_id =~' + AppIdPMWorkOrder + '~'

	var strPart = 'category_id = ~' + varCatIdPMWorkOrder + '~' 
		strPart += ' and sch_pm_id = ~' + SchIdPMWorkOrder + '~'
		strPart += ' and wo_pm_id = ~' + AppIdPMWorkOrder + '~'

	str = strPerson + '@^@' + strPart ;

	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess, //  "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahTotalCostPM',
				Params:	str
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.get('txtTotalServicePMWorkOrder').dom.value=formatCurrency(cst.TotalCost);
				}
				else
				{
					Ext.get('txtTotalServicePMWorkOrder').dom.value=formatCurrency(0);
				};
			}

		}
	)
};

function GetJumlahTotalPartPMWorkOrder()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess , //"./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahApprovePartCM',
				//Params:	'where SCH_PM_ID =~' + SchIdPMWorkOrder + '~ and category_id =~' + varCatIdPMWorkOrder + '~ and service_id =~' + cellSelectServicePMWorkOrder.data.SERVICE_ID + '~'
                                Params:	'sch_pm_id = ~' + SchIdPMWorkOrder + '~ and category_id = ~' + varCatIdPMWorkOrder + '~ and service_id = ~' + cellSelectServicePMWorkOrder.data.SERVICE_ID + '~'
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.get('txtTotalPartPMWorkOrder').dom.value=formatCurrency(cst.Jumlah);
				}
				else
				{
					Ext.get('txtTotalPartPMWorkOrder').dom.value=formatCurrency(0);
				};
			}

		}
	);
};

function GetJumlahTotalPersonPMWorkOrder(str)
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess, //"./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetJumlahApprovePersonCM',
				Params:	str
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.get('txtTotalPersonCostPMWorkOrder').dom.value=formatCurrency(cst.Jumlah);
				}
				else
				{
					Ext.get('txtTotalPersonCostPMWorkOrder').dom.value=formatCurrency(0);
				};
			}

		}
	);
};

function mComboStatusPMWorkOrderView() 
{
	var Field = ['STATUS_ID','STATUS_NAME'];
	
    dsStatusPMWorkOrderView = new WebApp.DataStore({ fields: Field });
    dsStatusPMWorkOrderView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ID',
                            Sort: 'status_id',
			    Sortdir: 'ASC',
			    target: 'ViewComboStatusVw',
			    //param: 'where status_id in ' + valueStatusPMWorkOrderView2
                            param: 'status_id in ' + valueStatusPMWorkOrderView2
			}
		}
	);
	
    var cboStatusPMWorkOrderView = new Ext.form.ComboBox
	(
		{
		    id: 'cboStatusPMWorkOrderView',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmStatusWOPM + ' ',
		    align: 'Right',
		    width:120,
		    store: dsStatusPMWorkOrderView,
		    valueField: 'STATUS_ID',
		    displayField: 'STATUS_NAME',
			value:valueStatusPMWorkOrderView,
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectStatusPMWorkOrderView = b.data.STATUS_ID;
			    }
			}
		}
	);
	
    return cboStatusPMWorkOrderView;
};

function GetTotalRemovePMWorkOrder(current,mBolEmp)
{
	var a;
	var b=0;
	
	if(mBolEmp === true )
	{
		a='txtTotalPersonCostPMWorkOrder';
	}
	else
	{
		a='txtTotalPartPMWorkOrder';
	};
	
	var x=GetNilaiCurrencyPMWorkOrder(Ext.get(a).dom.value);
	var y=GetNilaiCurrencyPMWorkOrder(Ext.get('txtTotalServicePMWorkOrder').dom.value);
	
	if(mBolEmp === true)
	{
		b=current.data.data.COST;
	}
	else
	{
		b=current.data.data.UNIT_COST * current.data.data.QTY;
	};
	
	if (x > 0)
	{
		Ext.get(a).dom.value=formatCurrency(x-b);
	}
	else
	{
		Ext.get(a).dom.value=0;
	};
	
	if (y > 0)
	{
		Ext.get('txtTotalServicePMWorkOrder').dom.value=formatCurrency(y-b);
	}
	else
	{
		Ext.get('txtTotalServicePMWorkOrder').dom.value=0
	};
};

function GetStrTreeComboPMWorkOrderView()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess, //"./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetDataTreeComboSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeComboPMWorkOrderView= cst.arr;
			},
			failure:function()
			{
			    
			}
		}
	);
};





	













