var dsCategorySchedulePM;
var selectCategorySchedulePM;

var selectCategorySchedulePMView;
var dsCategorySchedulePMView;

var dsSetSchedulePMList;
var AddNewSetSchedulePM;
var selectCountSetSchedulePM=50;
var rowSelectedSetSchedulePM;
var SetSchedulePMLookUps;
var NowPMSchedule = new Date();
var StrTreeComboSchedulePMView;
var rootTreeSchedulePMView;
var treeSchedulePMView;
var IdCategorySchedulePMView = ' 9999'
var valueCategorySchedulePMView=' All';
var rootTreeServSchedulePMEntry;
var treeSetServSchedulePMEntry;

CurrentPage.page = getPanelSetSchedulePM(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetSchedulePM(mod_id) 
{
	
    var Field =['Status','SCH_PM_ID','ASSET_MAINT_ID','YEARS','SERVICE_ID','ROW_SCH','STATUS_ID','DUE_DATE','METER','DESC_SCH_PM','ASSET_MAINT_NAME','SERVICE_NAME','STATUS_NAME','DEPT_NAME','LOCATION','CATEGORY_ID','CATEGORY_NAME','PATH'];
    dsSetSchedulePMList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetSchedulePM();
	GetStrTreeComboSchedulePMView();
    var grListSetSchedulePM = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetSchedulePM',
		    stripeRows: true,
		    store: dsSetSchedulePMList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 91.9999%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetSchedulePM=undefined;
							rowSelectedSetSchedulePM = dsSetSchedulePMList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetSchedulePM = dsSetSchedulePMList.getAt(ridx);
					if (rowSelectedSetSchedulePM != undefined)
					{
						SetSchedulePMLookUp(rowSelectedSetSchedulePM.data);
					}
					else
					{
						SetSchedulePMLookUp();
					};
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'colStatusImageSchPMView',
					    header: nmStatusSchPM,
					    //dataIndex: 'Status',
						dataIndex: 'PATH',
					    sortable: true,
					    width: 50,
						align:'center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store) 
						{						   
							return '<img src="' + value + '" class="text-desc-legend"/>'	

							// switch (value) 
							 // {
								 // case 1:  
									 // metaData.css = 'StatusHijau';
									 // break;
								 // case 2:   
									 // metaData.css = 'StatusKuning';
									 // break;
								 // case 3:  
									// metaData.css = 'StatusMerah';
									
									 // break;
							 // }
								// return '';     							
						}
					},
                    {
                        id: 'colCategorySchPM',
                        header: nmCatSchPM,                      
                        dataIndex: 'CATEGORY_NAME',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'colIdAssetSchPM',
					    header: nmAsetIDSchPM,					   
					    dataIndex: 'ASSET_MAINT_ID',
					    width: 80,
					    sortable: true
					},
					{
					    id: 'colAssetNameSchPM',
					    header: nmAsetNameSchPM,					   
					    dataIndex: 'ASSET_MAINT_NAME',
					    width: 120,
					    sortable: true
					}
					,
					{
					    id: 'colServiceSchPM',
					    header: nmServiceSchPM,					   
					    dataIndex: 'SERVICE_NAME',
					    width: 120,
					    sortable: true
					},
					{
					    id: 'colDueDateSchPM',
					    header: nmDueDateSchPM,					   
					    dataIndex: 'DUE_DATE',
					    width: 120,
						renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.DUE_DATE);
					    },
					    sortable: true
					}
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetSchedulePMList,
            pageSize: selectCountSetSchedulePM,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetSchedulePM',
				    text: nmEditData,
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetSchedulePM != undefined)
						{
						    SetSchedulePMLookUp(rowSelectedSetSchedulePM.data);
						}
						else
						{
						    SetSchedulePMLookUp();
						}
				    }
				}, ' ', '-',
				{
					xtype: 'checkbox',
					id: 'chkWithTglPMSchedule',
					hideLabel:true,
					checked: false,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilterPMSchedule').dom.disabled=false;
							Ext.get('dtpTglAkhirFilterPMSchedule').dom.disabled=false;
							
						}
						else
						{
							Ext.get('dtpTglAwalFilterPMSchedule').dom.disabled=true;
							Ext.get('dtpTglAkhirFilterPMSchedule').dom.disabled=true;
							
							
						};
					}
				}
				, ' ', '-', nmStartDateScheduleCM + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmStartDateScheduleCM + ' ',
				    id: 'dtpTglAwalFilterPMSchedule',
				    format: 'd/M/Y',
				    value: NowPMSchedule,
				    width: 100,
				    onInit: function() { }
				}, ' ', nmSd + ' ', ' ', {
				    xtype: 'datefield',
				    fieldLabel: nmSd + ' ',
				    id: 'dtpTglAkhirFilterPMSchedule',
				    format: 'd/M/Y',
				    value: NowPMSchedule,
				    width: 100
				}
			]
		    ,viewConfig: { forceFit: true }
		}
	);
	
	var LegendViewPMSchedule = new Ext.Panel
	(
		{
		    id: 'LegendViewPMSchedule',
		    region: 'center',
			border:false,
			bodyStyle: 'padding:0px 7px 0px 7px',
		    layout: 'column',
			frame:true,
			//height:32,
			anchor: '100% 8.0001%',
			autoScroll:false,
			items:
			[	
				{
					columnWidth: .033,
					layout: 'form',
					style:{'margin-top':'-1px'},
					//height:32,
					anchor: '100% 8.0001%',
					border: false,
					html: '<img src="' + WebAppUrl.UrlImg + '/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
				},
				{
					columnWidth: .13,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style:{'margin-top':'1px'},
					border: false,
					html: " " + nmLegFutureSchPM
				},
				{
					columnWidth: .033,
					layout: 'form',
					style:{'margin-top':'-1px'},
					border: false,
					//height:32,
					anchor: '100% 8.0001%',
					html: '<img src="' + WebAppUrl.UrlImg + '/images/icons/16x16/kuning.png" class="text-desc-legend"/>' 
				},
				{
					columnWidth: .13,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style:{'margin-top':'1px'},
					border: false,
					html: " " + nmLegOnSchSchPM
				},
				{
					columnWidth: .033,
					layout: 'form',
					style:{'margin-top':'-1px'},
					border: false,
					//height:35,
					anchor: '100% 8.0001%',
					html: '<img src="' + WebAppUrl.UrlImg + '/images/icons/16x16/merah.png" class="text-desc-legend"/>'  
				},
				{
					columnWidth: .13,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style:{'margin-top':'1px'},
					border: false,
					html: " " + nmLegLostSchPM
				},
				{
					columnWidth: .033,
					layout: 'form',
					style:{'margin-top':'-1px'},
					border: false,
					//height:35,
					anchor: '100% 8.0001%',
					html: '<img src="' + WebAppUrl.UrlImg + '/images/icons/16x16/biru.png" class="text-desc-legend"/>'  
				},
				{
					columnWidth: .13,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style:{'margin-top':'1px'},
					border: false,
					html: " " + nmLegCloseSchPM
				}
			]
		
		}
	)


    var FormSetSchedulePM = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    //title: nmFormSchedulePM,
		    title: nmTitleFormSchPM,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupSchedulePM',
		    items: [grListSetSchedulePM,LegendViewPMSchedule],
		    tbar:
			[
				nmCatSchPM + ' : ', ' ',
				mComboCategorySchedulePMView()
				, ' ','-',
				nmAsetNameFilterSchPM + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmAsetNameFilterSchPM + ' ',
					id: 'txtAsetSchedulePMFilter',                   
					anchor: '65%',
					onInit: function() { }
				}, ' ',
				{
				    id: 'btnRefreshSetSchedulePM',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetSchedulePMFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetSchedulePM();
				}
			}
		}
	);
    //END var FormSetSchedulePM--------------------------------------------------
	
	RefreshDataSetSchedulePM();
	RefreshDataSetSchedulePM();
    return FormSetSchedulePM ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetSchedulePMLookUp(rowdata) 
{
	var lebar=600;
    SetSchedulePMLookUps = new Ext.Window   	
    (
		{
		    id: 'SetSchedulePMLookUps',
		    title: nmTitleFormSchPM,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 210,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupSchedulePM',
		    modal: true,
		    items: getFormEntrySetSchedulePM(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetSchedulePM=undefined;
					RefreshDataSetSchedulePM();
				}
            }
		}
	);


    SetSchedulePMLookUps.show();
	if (rowdata == undefined)
	{
		SetSchedulePMAddNew();
	}
	else
	{
		SetSchedulePMInit(rowdata)
	}	
};


function getFormEntrySetSchedulePM(lebar) 
{
    var pnlSetSchedulePM = new Ext.FormPanel
    (
		{
		    id: 'PanelSetSchedulePM',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 180,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupSchedulePM',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 130,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetSchedulePM',
							labelWidth:65,
						    border: false,
						    items:
							[
								mComboCategorySchedulePM(),
								{
								    xtype: 'textfield',
								    fieldLabel: nmYearsSchPM + ' ',
								    name: 'txtYearsSchedulePM',
								    id: 'txtYearsSchedulePM',
								    anchor: '40%'
									//readOnly:true
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetSchedulePM',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() 
					{ 
						SetSchedulePMAddNew() 
					}
				}, '-',
				{
				    id: 'btnSimpanSetSchedulePM',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetSchedulePMSave(false);
						RefreshDataSetSchedulePM();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetSchedulePM',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetSchedulePMSave(true);
						RefreshDataSetSchedulePM();
						if (x===undefined)
						{
							SetSchedulePMLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetSchedulePM',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetSchedulePMDelete() ;
							RefreshDataSetSchedulePM();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetSchedulePM',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',
					disabled:true,
					handler: function() 
					{
						//LoadReport(950002);
					}
				}
			]
		}
	); 

    return pnlSetSchedulePM
};


function SetSchedulePMSave(mBol) 
{
	if (ValidasiEntrySetSchedulePM(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetSchedulePM == true) 
		{
			Ext.Ajax.request
			(
				{
					url:  WebAppUrl.UrlSaveData, //"./Datapool.mvc/CreateDataObj",
					params: getParamSetSchedulePM(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetSchedulePM(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetSchedulePM();
							
							AddNewSetSchedulePM = false;

						}
						else if  (cst.success === false && cst.Counter===0)
						{
							//ShowPesanWarningSetSchedulePM(nmPesanSimpanGagal,nmHeaderSimpanData);
							
						}
						else 
						{
							//ShowPesanErrorSetSchedulePM(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url:  WebAppUrl.UrlSaveData, //"./Datapool.mvc/CreateDataObj",
					params: getParamSetSchedulePM(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetSchedulePM(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetSchedulePM();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetSchedulePM(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetSchedulePM(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};//END FUNCTION SetSchedulePMSave
///---------------------------------------------------------------------------------------///

function SetSchedulePMDelete() 
{
	if (ValidasiEntrySetSchedulePM(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmSchedulePM) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url:  WebAppUrl.UrlDeleteData,  //'"./Datapool.mvc/DeleteDataObj",
								params: getParamSetSchedulePM(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetSchedulePM(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetSchedulePM();
										SetSchedulePMAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetSchedulePM(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetSchedulePM(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetSchedulePM(modul,mBolHapus)
{
	var x = 1;
	if (selectCategorySchedulePM == '' || selectCategorySchedulePM===undefined || (Ext.get('txtYearsSchedulePM').getValue() == ''))
	{
		if (selectCategorySchedulePM == '' && mBolHapus === true)
		{
			ShowPesanWarningSetSchedulePM(nmGetValidasiKosong(nmCatSchPM),modul);
			x=0;
		}
		else if (Ext.get('txtYearsSchedulePM').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetSchedulePM(nmGetValidasiKosong(nmYearsSchPM),modul);
			};
			
		}else if (selectCategorySchedulePM===undefined)
		{
			x=0
		}
		;
	};	
	
	return x;
};

function ShowPesanWarningSetSchedulePM(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetSchedulePM(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetSchedulePM(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetSchedulePMInit(rowdata) 
{
    AddNewSetSchedulePM = false;
    Ext.get('txtYearsSchedulePM').dom.value = rowdata.YEARS;
    Ext.get('cboCategorySchedulePM').dom.value = rowdata.CATEGORY_NAME;
	selectCategorySchedulePMView= rowdata.CATEGORY_ID;
};
///---------------------------------------------------------------------------------------///



function SetSchedulePMAddNew() 
{
    AddNewSetSchedulePM = true;   
	Ext.get('cboCategorySchedulePM').dom.value = '';
    Ext.get('txtYearsSchedulePM').dom.value = '';
	rowSelectedSetSchedulePM   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetSchedulePM() 
{
    var params =
	{	
		Table: 'SchedulePM',   
	    Years: Ext.get('txtYearsSchedulePM').getValue(),
	    Category: selectCategorySchedulePM
	};
    return params
};


function RefreshDataSetSchedulePM()
{	
	dsSetSchedulePMList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetSchedulePM, 
					//Sort: 'SCH_PM_ID',
                                        Sort: 'sch_pm_id',
					Sortdir: 'ASC', 
					target:'viViewSchedulePM',
					param : ''
				}			
			}
		);       
	
	rowSelectedSetSchedulePM = undefined;
	return dsSetSchedulePMList;
};

function RefreshDataSetSchedulePMFilter() 
{   
	var strKriteria = '';
	
	if (Ext.get('cboCategorySchedulePMView').getValue() != '' && selectCategorySchedulePMView != undefined)
    { 
		if (selectCategorySchedulePMView.id != undefined)
		{
			if(selectCategorySchedulePMView.id != ' 9999')
			{
				if (selectCategorySchedulePMView.leaf === false)
				{
					//strKriteria =' where substr(category_id,1,' + selectCategorySchedulePMView.id.length + ')=~' +  selectCategorySchedulePMView.id + '~'
                                        strKriteria =' substr(category_id,1,' + selectCategorySchedulePMView.id.length + ') = ~' +  selectCategorySchedulePMView.id + '~'
				}
				else
				{
					//strKriteria =' where Category_id=~'+ selectCategorySchedulePMView.id + '~'
                                        strKriteria =' category_id = ~'+ selectCategorySchedulePMView.id + '~'
				};
			};
		};
	};
	
	if(Ext.get('txtAsetSchedulePMFilter').dom.value != '')
	{
		if (strKriteria === '')
		{
			//strKriteria =' where asset_maint_name like ~%'+ Ext.get('txtAsetSchedulePMFilter').dom.value + '%~'
                        strKriteria =' asset_maint_name like ~%'+ Ext.get('txtAsetSchedulePMFilter').dom.value + '%~'
		}
		else
		{
			strKriteria =' and asset_maint_name like ~%' + Ext.get('txtAsetSchedulePMFilter').dom.value + '%~'
		};
	};
	
	// if (selectCategorySchedulePMView != ' 9999' && selectCategorySchedulePMView != undefined)
	// {
		// strKriteria=' WHERE Category_ID=~'+ selectCategorySchedulePMView + '~'
	// };
	
	if( Ext.get('chkWithTglPMSchedule').dom.checked === true )
	{
			strKriteria += '@^@' + Ext.get('dtpTglAwalFilterPMSchedule').getValue() + '&&'+ Ext.get('dtpTglAkhirFilterPMSchedule').getValue();
	};
	
	
	dsSetSchedulePMList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetSchedulePM, 
					//Sort: 'SCH_PM_ID',
                                        Sort: 'sch_pm_id',
					Sortdir: 'ASC', 
					target:'viViewSchedulePM',
					param : strKriteria
				}			
			}
		);       
    return dsSetSchedulePMList;
};



function mComboMaksDataSetSchedulePM()
{
  var cboMaksDataSetSchedulePM = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetSchedulePM',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:40,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetSchedulePM,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetSchedulePM=b.data.displayText ;
					RefreshDataSetSchedulePM();					
				} 
			}
		}
	);
	return cboMaksDataSetSchedulePM;
};
 
 
 


function mComboCategorySchedulePM()
{
	var Field = ['CATEGORY_ID', 'CATEGORY_NAME'];
	dsCategorySchedulePM = new WebApp.DataStore({ fields: Field });

	
	Ext.TreeComboSchedulePMEntry = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treeSetServSchedulePMEntry = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreeServSchedulePMEntry  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						children: StrTreeComboSchedulePMView,
						autoScroll: true
					}
				) 
				treeSetServSchedulePMEntry.setRootNode(rootTreeServSchedulePMEntry); 
				
				this.list = treeSetServSchedulePMEntry
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboSchedulePMEntry.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treeSetServSchedulePMEntry
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				this.setRawValue(node.attributes.text);
				selectCategorySchedulePM = node.attributes.id;

				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategorySchedulePM
		}
	);
	
	
	var cboCategorySchedulePM = new Ext.TreeComboSchedulePMEntry
	(
		{
			id:'cboCategorySchedulePM',
			fieldLabel: nmCatSchPM + ' ',
			emptyText: nmPilihCategory,
			anchor:'60%'
		}
	);
	
		return cboCategorySchedulePM;

};


function mComboCategorySchedulePMView()
{
	var Field = ['CATEGORY_ID', 'CATEGORY_NAME'];
	dsCategorySchedulePMView = new WebApp.DataStore({ fields: Field });

	Ext.TreeComboSchedulePMView = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treeSchedulePMView = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreeSchedulePMView  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						id:IdCategorySchedulePMView,
						children: StrTreeComboSchedulePMView,
						autoScroll: true
					}
				) 
				treeSchedulePMView.setRootNode(rootTreeSchedulePMView); 
				
				this.list = treeSchedulePMView
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboSchedulePMView.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treeSchedulePMView
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				if (node.attributes.id === IdCategorySchedulePMView)
				{
					this.setRawValue(valueCategorySchedulePMView);
				}
				else
				{
					this.setRawValue(node.attributes.text);
				};
				
				selectCategorySchedulePMView = node.attributes;
				
				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategorySchedulePMView
		}
	);
	
	
	
	var cboCategorySchedulePMView = new Ext.TreeComboSchedulePMView
	(
		{
			id:'cboCategorySchedulePMView',
			fieldLabel: nmCatSchPM + ' ',
			anchor:'40%',
			width:100,
			value:valueCategorySchedulePMView
		}
	);
	
	return cboCategorySchedulePMView;
};

function GetStrTreeComboSchedulePMView()
{
     Ext.Ajax.request
     (
            {
                    url:  WebAppUrl.UrlExecProcess, //"./Module.mvc/ExecProc",
                    params:
                    {
                            UserID: 'Admin',
                            ModuleID: 'ProsesGetDataTreeComboSetCat',
                            Params:	''
                    },
                    success : function(resp)
                    {
                            var cst = Ext.decode(resp.responseText);
                            StrTreeComboSchedulePMView= cst.arr;
                    },
                    failure:function()
                    {

                    }
            }
    );
};

