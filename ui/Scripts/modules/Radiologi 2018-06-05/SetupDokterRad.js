var dataSource_viRadSetupDokter;
var selectCount_viRadSetupDokter=50;
var NamaForm_viRadSetupDokter="SetupDokter";
var mod_name_viRadSetupDokter="SetupDokter";
var now_viRadSetupDokter= new Date();
var rowSelected_viRadSetupDokter;
var setLookUps_viRadSetupDokter;
var tanggal = now_viRadSetupDokter.format("d/M/Y");
var jam = now_viRadSetupDokter.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viRadSetupDokter;
var kodesetupDokter='';

var CurrentData_viRadSetupDokter =
{
	data: Object,
	details: Array,
	row: 0
};
var tmpkd_unit;
var default_Kd_Unit;
var dsunitRad_viSetupDokterRad;
var RadSetupDokter={};
RadSetupDokter.form={};
RadSetupDokter.func={};
RadSetupDokter.vars={};
RadSetupDokter.func.parent=RadSetupDokter;
RadSetupDokter.form.ArrayStore={};
RadSetupDokter.form.ComboBox={};
RadSetupDokter.form.DataStore={};
RadSetupDokter.form.Record={};
RadSetupDokter.form.Form={};
RadSetupDokter.form.Grid={};
RadSetupDokter.form.Panel={};
RadSetupDokter.form.TextField={};
RadSetupDokter.form.Button={};

RadSetupDokter.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viRadSetupDokter(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viRadSetupDokter(mod_id_viRadSetupDokter){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viRadSetupDokter = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viRadSetupDokter = new WebApp.DataStore
	({
        fields: FieldMaster_viRadSetupDokter
    });
    dataGriRadSetupDokter();
    // Grid gizi Perencanaan # --------------
	GridDataView_viRadSetupDokter = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viRadSetupDokter,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							/*kodesetupDokter=rowSelected_viRadSetupDokter.data.kd_Dokter
							alert(kodesetupDokter);*/
							rowSelected_viRadSetupDokter = undefined;
							rowSelected_viRadSetupDokter = dataSource_viRadSetupDokter.getAt(row);
							CurrentData_viRadSetupDokter
							CurrentData_viRadSetupDokter.row = row;
							CurrentData_viRadSetupDokter.data = rowSelected_viRadSetupDokter.data;
							kodesetupDokter=rowSelected_viRadSetupDokter.data.kd_dokter;
							//DataInitRadSetupDokter(rowSelected_viRadSetupDokter.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					kodesetupDokter=rowSelected_viRadSetupDokter.data.kd_Dokter;
					rowSelected_viRadSetupDokter = dataSource_viRadSetupDokter.getAt(ridx);
					if (rowSelected_viRadSetupDokter != undefined)
					{
						DataInitRadSetupDokter(rowSelected_viRadSetupDokter.data);
						//setLookUp_viRadSetupDokter(rowSelected_viRadSetupDokter.data);
					}
					else
					{
						//setLookUp_viRadSetupDokter();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Dokter',
						dataIndex: 'kd_dokter',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama Dokter',
						dataIndex: 'nama',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Jenis Dokter',
						dataIndex: 'jenis_dokter',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'spesialisasi',
						dataIndex: 'spesialisasi',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
				]
			),
			/* tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viRadSetupDokter',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Dokter Rad',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Dokter Rad',
						id: 'btnEdit_viRadSetupDokter',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viRadSetupDokter != undefined)
							{
								DataInitRadSetupDokter(rowSelected_viRadSetupDokter.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			}, */
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viRadSetupDokter, selectCount_viRadSetupDokter, dataSource_viRadSetupDokter),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabRadSetupDokter = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viRadSetupDokter = new Ext.Panel
    (
		{
			title: NamaForm_viRadSetupDokter,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viRadSetupDokter,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabRadSetupDokter,
					GridDataView_viRadSetupDokter],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddSetupDokter_viRadSetupDokter',
						handler: function(){
							AddNewRadSetupDokter();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viRadSetupDokter',
						handler: function()
						{
							loadMask.show();
							dataSave_viRadSetupDokter();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viRadSetupDokter',
						handler: function()
						{
							if (kodesetupDokter==='')
							{
								ShowPesanErrorRadSetupDokter('Tidak ada data yang dipilih', 'Error');
							}
							else
							{
								Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
									if (button == 'yes'){
										loadMask.show();
										dataDelete_viRadSetupDokter();
									}
								});
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viRadSetupDokter',
						handler: function()
						{
							dataSource_viRadSetupDokter.removeAll();
							dataGriRadSetupDokter();
							kodesetupDokter='';
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viRadSetupDokter;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 120,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Dokter'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdSetupDokter_RadSetupDokter',
								name: 'txtKdSetupDokter_RadSetupDokter',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningRadSetupDokter('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama Dokter'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							RadSetupDokter.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: RadSetupDokter.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdSetupDokter_RadSetupDokter').setValue(b.data.kd_dokter);
									
									GridDataView_viRadSetupDokter.getView().refresh();
									
								},
								/* onShowList:function(a){
									dataSource_viRadSetupDokter.removeAll();
									
									var recs=[],
									recType=dataSource_viRadSetupDokter.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viRadSetupDokter.add(recs);
									
								}, */
								insert	: function(o){
									return {
										kd_dokter   : o.kd_dokter,
										nama 		: o.nama,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_dokter+'</td><td width="200">'+o.nama+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/main/functionLAB/getDokter",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Spesialisasi'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 60,
								xtype: 'textfield',
								id: 'txtSpesialisasi_RadSetupDokter',
								name: 'txtSpesialisasi_RadSetupDokter',
								width: 150,
								tabIndex:3
							},{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Unit Rad'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},mComboUnitRad()
							
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}

function mComboUnitRad(){ 
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitlab_viSetupDokterRad= new WebApp.DataStore({ fields: Field });
    dsunitlab_viSetupDokterRad.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama_unit',
			Sortdir: 'ASC',
			target: 'ViewSetupUnit',
			param: "parent = '5'"
		}
	});
    var cboUnitRad_viSetupDokterRad = new Ext.form.ComboBox({
		id: 'cboUnitRad_viSetupDokterRad',
		x: 130,
		y: 90,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		readOnly: true,
		mode: 'local',
		emptyText: '',
		fieldLabel:  ' ',
		align: 'Right',
		width: 230,
		emptyText:'Pilih Unit',
		store: dsunitlab_viSetupDokterRad,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		//value:'All',
		editable: false,
			listeners:
			{
				'select': function(a,b,c)
				{
					tmpkd_unit = b.data.KD_UNIT;
				}
			}
	});
    return cboUnitRad_viSetupDokterRad;
}
//------------------end---------------------------------------------------------

function dataGriRadSetupDokter(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionRAD/getDokterPenunjang",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorRadSetupDokter('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					tmpkd_unit=cst.kd_unit;
					Ext.getCmp('cboUnitRad_viSetupDokterRad').setValue(cst.nama_unit);
					var recs=[],
						recType=dataSource_viRadSetupDokter.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viRadSetupDokter.add(recs);
					
					
					
					GridDataView_viRadSetupDokter.getView().refresh();
				}
				else 
				{
					ShowPesanErrorRadSetupDokter('Gagal membaca data Dokter', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viRadSetupDokter(){
	if (ValidasiSaveRadSetupDokter(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/main/functionLAB/simpanSetupDokter",
				params: getParamSaveRadSetupDokter(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorRadSetupDokter('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoRadSetupDokter('Berhasil menyimpan data ini','Information');
						dataSource_viRadSetupDokter.removeAll();
						dataGriRadSetupDokter();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorRadSetupDokter('Gagal menyimpan data ini', 'Error');
						dataSource_viRadSetupDokter.removeAll();
						dataGriRadSetupDokter();
					};
				}
			}
			
		)
	}
}

function dataDelete_viRadSetupDokter(){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/main/functionLAB/hapusSetupDokter",
				params: getParamDeleteRadSetupDokter(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorRadSetupDokter('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoRadSetupDokter('Berhasil menghapus data ini','Information');
						AddNewRadSetupDokter()
						dataSource_viRadSetupDokter.removeAll();
						dataGriRadSetupDokter();
						kodesetupDokter='';
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorRadSetupDokter('Gagal menghapus data ini', 'Error');
						dataSource_viRadSetupDokter.removeAll();
						dataGriRadSetupDokter();
					};
				}
			}
			
		)
	
}

function AddNewRadSetupDokter(){
	Ext.getCmp('txtKdSetupDokter_RadSetupDokter').setValue('');
	RadSetupDokter.vars.nama.setValue('');
	RadSetupDokter.vars.nama.focus();
	Ext.getCmp('txtSpesialisasi_RadSetupDokter').setValue('');
};

function DataInitRadSetupDokter(rowdata){
	Ext.getCmp('txtKdSetupDokter_RadSetupDokter').setValue(rowdata.kd_Dokter);
	RadSetupDokter.vars.nama.setValue(rowdata.Dokter);
	Ext.getCmp('txtSpesialisasi_RadSetupDokter').setValue(rowdata.alamat);
};

function getParamSaveRadSetupDokter(){
	var	params =
	{
		KdSetupDokter:Ext.getCmp('txtKdSetupDokter_RadSetupDokter').getValue(),
		NamaSetupDokter:RadSetupDokter.vars.nama.getValue(),
		Spesialisasi:Ext.getCmp('txtSpesialisasi_RadSetupDokter').getValue(),
		kd_unit:'5'
	}
   
    return params
};

function getParamDeleteRadSetupDokter(){
	var	params =
	{
		KdSetupDokter:kodesetupDokter,
		kd_unit:'5'
	}
   
    return params
};

function ValidasiSaveRadSetupDokter(modul,mBolHapus){
	var x = 1;
	if(RadSetupDokter.vars.nama.getValue() === '' /*|| Ext.getCmp('txtKdSetupDokter_RadSetupDokter').getValue() ===''*/){
		if(RadSetupDokter.vars.nama.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningRadSetupDokter('Nama unit masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningRadSetupDokter('Kode unit masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtKdSetupDokter_RadSetupDokter').getValue().length > 3){
		ShowPesanWarningRadSetupDokter('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningRadSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorRadSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoRadSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};