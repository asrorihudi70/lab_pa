var CurrentHasilRad = {
    data: Object,
    details: Array,
    row: 0
};

var tanggaltransaksitampung;
var rowSelectedDetHasilRad;
var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView = 'All';
var nowTglTransaksi = new Date();
var currentKdUnitRadiologi;
var tmpkdunitpopupentry;

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwj = 'Belum Posting';
var dsTRHasilRadList;
var dsTRDetailHasilRadList;
var dsTRDetailDiagnosaList;
var AddNewHasilRad = true;
var selectCountHasilRad = 10;

var rowSelectedHasilRad;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRrwj;
var valueStatusCMHasilRadView = 'All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew = true;
var tigaharilalu = new Date().add(Date.DAY, -3);//setting tgl 3 hari sebelum hari ini (-3)
var tampil;
var firstGrid;
var tmpsimpan;

//var FocusCtrlCMHasilRad;
var vkode_customer;
CurrentPage.page = getPanelHasilRad(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelHasilRad(mod_id) {

    var Field = ['KD_PASIEN', 'NAMA', 'ALAMAT', 'TGL_MASUK', 'KD_DOKTER', 'NAMA_UNIT', 'KD_KELAS',
        'URUT_MASUK', 'NAMA_DOKTER', 'TGL_LAHIR', 'JENIS_KELAMIN', 'GOL_DARAH', 'NAMA_UNIT_ASAL','KD_UNIT','NO_TRANSAKSI','KD_KASIR','PATIENT_ID',
		'STUDY_UID', 'KD_DOKTER_HASIL', 'NAMA_DOKTER_HASIL', 'TGL_BACA','FLAG_BACA'];
    dsTRHasilRadList = new WebApp.DataStore({
        fields: Field
    });

    refeshHasilRad("transaksi.ispay = '1' #awal");
	getCurrentKdUnitRad();
    var grListTRHasilRad = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dsTRHasilRadList,
        anchor: '100% 70%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelectedHasilRad = dsTRHasilRadList.getAt(row);
                    // console.log(rowSelectedHasilRad);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelectedHasilRad = dsTRHasilRadList.getAt(ridx);
                tmpsimpan = rowSelectedHasilRad;
                if (rowSelectedHasilRad !== undefined) {
                    HasilRadLookUp(rowSelectedHasilRad.data);
                } else {
                    HasilRadLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
					{
                        id: 'colMedrecViewHasilRad',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colMedrecViewHasilRad',
                        header: 'No. Medrec',
                        dataIndex: 'KD_PASIEN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }, {
                        id: 'colNamaViewHasilRad',
                        header: 'Nama Pasien',
                        dataIndex: 'NAMA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    }, {
                        id: 'colAlamatViewHasilRad',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        width: 220,
                        sortable: false,
                        hideable: false,
                        menuDisabled: true

                    }, {
                        id: 'colTglmasukViewHasilRad',
                        header: 'Tgl. Masuk',
                        dataIndex: 'TGL_MASUK',
                        width: 60,
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        renderer: function (v, params, record) {
                            return ShowDate(record.data.TGL_MASUK);
                        }

                    }, {
                        id: 'colKddokterViewHasilRad',
                        header: 'Kode Dokter',
                        dataIndex: 'KD_DOKTER',
                        sortable: false,
                        hidden: true,
                        menuDisabled: true,
                        width: 170
                    },
                    {
                        id: 'colNamaunitViewHasilRad',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 60
                    }, {
                        id: 'colKdkelasViewHasilRad',
                        header: 'Kode Kelas',
                        dataIndex: 'KD_KELAS',
                        sortable: false,
                        hidden: true,
                        menuDisabled: true,
                        width: 90
                    }, {
                        id: 'colKddokterViewHasilRad',
                        dataIndex: 'KD_DOKTER',
                        header: 'kode Dokter',
                        width: 250,
                        sortable: false,
                        hidden: true,
                        menuDisabled: true
                    },
                    {
                        id: 'colUrutmasukViewHasilRad',
                        header: 'Urut Masuk',
                        dataIndex: 'URUT_MASUK',
                        sortable: false,
                        hidden: true,
                        menuDisabled: true,
                        width: 90
                    },
                    {
                        id: 'colNamadokterViewHasilRad',
                        header: 'Dokter',
                        dataIndex: 'NAMA_DOKTER',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 90
                    },
                    {
                        id: 'colTglLahirViewHasilRad',
                        header: 'Tgl. Lahir',
                        dataIndex: 'TGL_LAHIR',
                        sortable: false,
                        hidden: true,
                        menuDisabled: true,
                        width: 90
                    },
                    {
                        id: 'colJKViewHasilRad',
                        header: 'Jenis Kelamin',
                        dataIndex: 'JENIS_KELAMIN',
                        sortable: false,
                        hidden: true,
                        menuDisabled: true,
                        width: 90
                    },
                    {
                        id: 'colGolDarViewHasilRad',
                        header: 'Golongan Darah',
                        dataIndex: 'GOL_DARAH',
                        sortable: false,
                        hidden: true,
                        menuDisabled: true,
                        width: 90
                    },
                    {
                        id: 'colasalViewHasilRad',
                        header: 'Unit Asal',
                        dataIndex: 'NAMA_UNIT_ASAL',
                        sortable: false,
                        hidden: true,
                        menuDisabled: true,
                        width: 90
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [{
                id: 'btnEditHasilRad',
                text: nmEditData,
                tooltip: nmEditData,
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    if (rowSelectedHasilRad !== undefined) {
                        HasilRadLookUp(rowSelectedHasilRad.data);
                    } else {
                        ShowPesanWarningHasilRad('Pilih data data tabel  ', 'Edit data');
                    }
                }
            }]
    });



    var FormDepanHasilRad = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Hasil Radiologi',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getItemPanelHasilRad(),
            grListTRHasilRad
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });

    return FormDepanHasilRad;

};

function HasilRadLookUp(rowdata) {
    var lebar = 900;
    FormLookUpsdetailTRrwj = new Ext.Window({
        id: 'gridHasilRad',
        title: 'Hasil Radiologi',
        closeAction: 'destroy',
        width: lebar,
        height: 550,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: dataGrid_viInformasiUnitdokter(rowdata),
        listeners: {
        }
    });

    FormLookUpsdetailTRrwj.show();
    if (rowdata === undefined) {
        HasilRadAddNew();
    } else {
        TRHasilRadInit(rowdata);
    }

}
;



var tmpdatahasilsebelumsaving;
var tmpKdProduk  = {};
tmpKdProduk.data = {};
tmpKdProduk.data.URUT = ''; 
function dataGrid_viInformasiUnitdokter(mod_id, rowdata)
{
    var Field_Hasil_viDaftar = ['KLASIFIKASI', 'DESKRIPSI', 'KD_TEST', 'NORMAL', 'SATUAN', 'HASIL', 'URUT','CATATAN_DOKTER'];

    dsTRDetailHasilRadList = new WebApp.DataStore({fields: Field_Hasil_viDaftar});

    firstGrid = new Ext.grid.GridPanel({
        x: 10,
        y: 10,
        ddGroup: 'secondGridDDGroup',
        store: dsTRDetailHasilRadList,
        autoScroll: true,
        columnLines: true,
        border: true,
        enableDragDrop: true,
        height: 200,
        stripeRows: true,
        trackMouseOver: true,
        title: 'Hasil Pemeriksaan',
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        colModel: new Ext.grid.ColumnModel
                (
                        [
                            new Ext.grid.RowNumberer(),
                            {
                                id: 'colNRM_viDaftar',
                                header: 'Kd. Test',
                                dataIndex: 'KD_TEST',
                                sortable: true,
                                width: 20
                            },
                            {
                                id: 'colNMPASIEN_viDaftar',
                                header: 'Pemeriksaan',
                                dataIndex: 'DESKRIPSI',
                                sortable: true,
                                width: 100
                            }
                        ]
                        ),
        selModel: new Ext.grid.RowSelectionModel
                (
                        {
                            singleSelect: true,
                            listeners:
                                    {
                                        rowselect: function (sm, row, rec)
                                        {
                                            rowSelectedDetHasilRad = dsTRDetailHasilRadList.getAt(row);
                                            console.log(rowSelectedDetHasilRad)
                                            tmpKdProduk = rowSelectedDetHasilRad;
                                            if (rowSelectedDetHasilRad !== undefined)
                                            {
                                                tampil = 0;
                                                Ext.get('textareapopuphasil').dom.value = rowSelectedDetHasilRad.data.HASIL;
                                                Ext.get('TxtCatatanDokter').dom.value = rowSelectedDetHasilRad.data.CATATAN_DOKTER;
                                                tmpdatahasilsebelumsaving = rowSelectedDetHasilRad.data;

                                            }
                                        }
                                    }
                        }
                ),
        listeners:
                {
                    rowdblclick: function (sm, ridx, cidx)
                    {
                        rowSelectedDetHasilRad = dsTRDetailHasilRadList.getAt(ridx);
                        if (rowSelectedDetHasilRad !== undefined)
                        {
                            tampil = 1;
                            InputLookupHasil(rowSelectedDetHasilRad.data);
                        }
                    },
                    'afterrender': function () {
                        tampil = 0;
                    }
                },
        tbar: [
            {
                xtype: 'label',
                text: '*) Double klik untuk input Hasil Rad',
            }
        ],
        viewConfig:
                {
                    forceFit: true
                }
    });

    var FrmTabs_popupdatahasilrad = new Ext.Panel
            (
                    {
                        id: mod_id,
                        closable: true,
                        region: 'center',
                        layout: 'column',
                        height: 400,
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: false,
                        shadhow: true,
                        margins: '5 5 5 5',
                        anchor: '99%',
                        iconCls: '',
                        items:
                                [
                                    {
                                        columnWidth: .99,
                                        layout: 'absolute',
                                        bodyStyle: 'padding: 10px 10px 10px 10px',
                                        border: true,
                                        width: 100,
                                        height: 200,
                                        anchor: '100% 100%',
                                        items:
                                                [
                                                    {
                                                        x: 10,
                                                        y: 10,
                                                        xtype: 'label',
                                                        text: 'No. Medrec '
                                                    },
                                                    {
                                                        x: 110,
                                                        y: 10,
                                                        xtype: 'label',
                                                        text: ' : '
                                                    },
                                                    {
                                                        x: 120,
                                                        y: 10,
                                                        xtype: 'textfield',
                                                        name: 'TxtPopupMedrec',
                                                        id: 'TxtPopupMedrec',
                                                        width: 80
                                                    },
                                                    {
                                                        x: 210,
                                                        y: 10,
                                                        xtype: 'label',
                                                        text: 'Tanggal '
                                                    },
                                                    {
                                                        x: 250,
                                                        y: 10,
                                                        xtype: 'label',
                                                        text: ' : '
                                                    },
                                                    {
                                                        x: 260,
                                                        y: 10,
                                                        xtype: 'textfield',
                                                        name: 'TxtPopupTglCekPasien',
                                                        id: 'TxtPopupTglCekPasien',
                                                        width: 110
                                                    },
                                                    {
                                                        x: 10,
                                                        y: 40,
                                                        xtype: 'label',
                                                        text: 'Nama Pasien '
                                                    },
                                                    {
                                                        x: 110,
                                                        y: 40,
                                                        xtype: 'label',
                                                        text: ' : '
                                                    },
                                                    {
                                                        x: 120,
                                                        y: 40,
                                                        xtype: 'textfield',
                                                        name: 'TxtPopupNamaPasien',
                                                        id: 'TxtPopupNamaPasien',
                                                        width: 250,
                                                        enableKeyEvents: true,
                                                        listeners:
                                                                {
                                                                    'specialkey': function ()
                                                                    {
                                                                        if (Ext.EventObject.getKey() === 13)
                                                                        {
                                                                            getkriteriacari();
                                                                        }
                                                                    }
                                                                }
                                                    },
                                                    {
                                                        x: 380,
                                                        y: 20,
                                                        xtype: 'label',
                                                        text: 'Tgl Lahir '
                                                    },
                                                    {
                                                        x: 380,
                                                        y: 40,
                                                        xtype: 'textfield',
                                                        name: 'TxtPopupTglLahirPasien',
                                                        id: 'TxtPopupTglLahirPasien',
                                                        width: 100
                                                    },
                                                    {
                                                        x: 490,
                                                        y: 20,
                                                        xtype: 'label',
                                                        text: 'Thn '
                                                    },
                                                    {
                                                        x: 490,
                                                        y: 40,
                                                        xtype: 'textfield',
                                                        name: 'TxtPopupThnLahirPasien',
                                                        id: 'TxtPopupThnLahirPasien',
                                                        width: 30
                                                    },
                                                    {
                                                        x: 530,
                                                        y: 20,
                                                        xtype: 'label',
                                                        text: 'Bln '
                                                    },
                                                    {
                                                        x: 530,
                                                        y: 40,
                                                        xtype: 'textfield',
                                                        name: 'TxtPopupBlnLahirPasien',
                                                        id: 'TxtPopupBlnLahirPasien',
                                                        width: 30
                                                    },
                                                    {
                                                        x: 570,
                                                        y: 20,
                                                        xtype: 'label',
                                                        text: 'Hari '
                                                    },
                                                    {
                                                        x: 570,
                                                        y: 40,
                                                        xtype: 'textfield',
                                                        name: 'TxtPopupHariLahirPasien',
                                                        id: 'TxtPopupHariLahirPasien',
                                                        width: 30
                                                    },
                                                    {
                                                        x: 10,
                                                        y: 70,
                                                        xtype: 'label',
                                                        text: 'Alamat '
                                                    },
                                                    {
                                                        x: 110,
                                                        y: 70,
                                                        xtype: 'label',
                                                        text: ' : '
                                                    },
                                                    {
                                                        x: 120,
                                                        y: 70,
                                                        xtype: 'textfield',
                                                        name: 'TxtPopupAlamat',
                                                        id: 'TxtPopupAlamat',
                                                        width: 480
                                                    },
                                                    {
                                                        x: 10,
                                                        y: 100,
                                                        xtype: 'label',
                                                        text: 'Dokter '
                                                    },
                                                    {
                                                        x: 110,
                                                        y: 100,
                                                        xtype: 'label',
                                                        text: ' : '
                                                    },
                                                    // {
                                                        // x: 120,
                                                        // y: 100,
                                                        // xtype: 'textfield',
                                                        // name: 'TxtPopupNamaDokter',
                                                        // id: 'TxtPopupNamaDokter',
                                                        // width: 240
                                                    // },
                                                    mComboDOKTER_HasilRad(),
                                                    {
                                                        x       : 330,
                                                        y       : 105,
                                                        xtype   : 'label',
                                                        text    : 'Tgl Baca : ',
                                                    },{
                                                        x       : 390,
                                                        y       : 100,
                                                        id      : 'ID_date_baca',
                                                        xtype   : 'datefield',
                                                        format  : 'd/M/Y',
                                                    },
													{
                                                        x: 510,
                                                        y: 105,
                                                        xtype: 'label',
                                                        text: 'Status Baca : '
                                                    },
													{
														x: 585,
                                                        y: 105,
														xtype: 'checkbox',
														boxLabel: '',
														name: 'idStatusBaca',
														id: 'idStatusBaca',
														listeners: {
															render: function(c){
																c.getEl().on({
																	click: function() {
																		SaveStatusBaca(Ext.getCmp('idStatusBaca').getValue());
																	}
																});
															}
														}
													},
													{
                                                        x: 600,
                                                        y: 105,
                                                        xtype: 'label',
                                                        text: '*) Centang untuk merubah status baca hasil.'
                                                    },
                                                     {
                                                        x: 10,
                                                        y: 130,
                                                        xtype: 'label',
                                                        text: 'Catatan Dokter '
                                                    },
                                                    {
                                                        x: 110,
                                                        y: 130,
                                                        xtype: 'label',
                                                        text: ' : '
                                                    },
                                                    {
                                                        x: 120,
                                                        y: 130,
                                                        xtype: 'textarea',
                                                        name: 'TxtCatatanDokter',
                                                        id: 'TxtCatatanDokter',
                                                        width: 480
                                                    },
                                                ]
                                    },
                                    {
                                        columnWidth: .50,
                                        layout: 'absolute',
                                        bodyStyle: 'padding: 10px 10px 10px 10px',
                                        border: false,
                                        width: 100,
                                        height: 290,
                                        anchor: '100% 100%',
                                        items:
                                                [
                                                    firstGrid
                                                ]
                                    },
                                    {
                                        columnWidth: .50,
                                        layout: 'absolute',
                                        bodyStyle: 'padding: 10px 10px 10px 10px',
                                        border: false,
                                        width: 100,
                                        height: 290,
                                        anchor: '100% 100%',
                                        items:
                                                [
                                                    {
                                                        x: 10,
                                                        y: 10,
                                                        xtype: 'textarea',
                                                        name: 'textarea',
                                                        id: 'textareapopuphasil',
                                                        anchor: '100%',
                                                        height: 270,
                                                        listeners:
                                                                {
                                                                    /* blur: function() {
                                                                     tampil=0;
                                                                     Ext.Ajax.request ({
                                                                     url: baseURL + "index.php/main/CreateDataObj",
                                                                     params: getParamDetailHasilRad(tmpdatahasilsebelumsaving),
                                                                     success: function(o)
                                                                     {
                                                                     var cst = Ext.decode(o.responseText);
                                                                     if (cst.success === true)
                                                                     {
                                                                     RefreshDataHasilRadDetail(cst.KD_PASIEN,cst.TGL_MASUK,cst.URUT_MASUK);
                                                                     }
                                                                     else if  (cst.success === false && cst.pesan===0)
                                                                     {
                                                                     ShowPesanWarningHasilRad('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
                                                                     }
                                                                     else
                                                                     {
                                                                     ShowPesanErrorHasilRad('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
                                                                     }
                                                                     }
                                                                     });
                                                                     } */
                                                                }
                                                    }
                                                ]
                                    }
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_viDaftar',
                                                    handler: function ()
                                                    {

                                                        Ext.Ajax.request({
                                                            url: baseURL + "index.php/rad/Radiologi/simpan_hasil",
                                                            params: {
                                                                hasil       : Ext.getCmp('textareapopuphasil').getValue(),
                                                                kd_pasien   : tmpsimpan.data.KD_PASIEN,
                                                                kd_unit     : tmpkdunitpopupentry,
                                                                tgl_masuk   : tmptglMasukPopupentry,
                                                                urut_masuk  : tmpsimpan.data.URUT_MASUK,
                                                                urut        : tmpKdProduk.data.URUT,
                                                                kd_test     : tmpKdProduk.data.KD_TEST,
                                                                dokter      : Ext.getCmp('cboDOKTER_viHasilRad').getValue(),
                                                                tgl_baca    : Ext.getCmp('ID_date_baca').getValue(),
                                                                catatan_dokter    : Ext.getCmp('TxtCatatanDokter').getValue()
                                                            },
                                                            success: function (o){
                                                                var cst = Ext.decode(o.responseText);
                                                                if (cst.status === true) {
                                                                    RefreshDataHasilRadDetail(tmpsimpan.data.KD_PASIEN, tmptglMasukPopupentry, tmpsimpan.data.URUT_MASUK);
                                                                    ShowPesanInfoHasilRad('Data berhasil di simpan ','Simpan Data');
																	getkriteriacari();
                                                                }else{
                                                                    ShowPesanWarningHasilRad('Data tidak berhasil di simpan ','Simpan Data');
                                                                }
                                                            }
                                                        });
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Cetak',
                                                    iconCls: 'print',
                                                    id: 'btnPrint_viDaftar',
                                                    handler: function ()
                                                    {
                                                        // DISINI
                                                        var url = baseURL + "index.php/laporan/lap_hasil/cetak/lap_hasil_rad";
                                                        new Ext.Window({
                                                            title: 'Preview',
                                                            width: 1000,
                                                            height: 600,
                                                            constrain: true,
                                                            modal: true,
                                                            html: "<iframe  style ='width: 100%; height: 100%;' src='" + url+"/"+rowSelectedHasilRad.data.NO_TRANSAKSI+"/"+rowSelectedHasilRad.data.KD_KASIR+"/true'></iframe>",
                                                            tbar : [
                                                                {
                                                                    xtype   : 'button',
                                                                    text    : 'Cetak Direct',
                                                                    iconCls : 'print',
                                                                    handler : function(){
                                                                        window.open(url+"/"+rowSelectedHasilRad.data.NO_TRANSAKSI+"/"+rowSelectedHasilRad.data.KD_KASIR+"/false", '_blank');
                                                                    }
                                                                }
                                                            ]
                                                        }).show();
                                                        // DataPrint_HasilRad();
                                                    }
                                                },{
                                                    xtype   : 'button',
                                                    text    : 'Document',
                                                    // iconCls : 'Edit_Tr',
                                                    iconCls : 'Edit_Tr',
                                                    id      : 'btnDocument_Pasien',
                                                    handler : function ()
                                                    {
                                                        lookUpFileManagement_Hasil();
                                                    }
                                                },
												
												// 0-01-05-91
												/*{
                                                    xtype: 'textfield',
                                                    fieldLabel: "&nbsp;&nbsp;&nbsp;Image1",
                                                    id: 'image1',
                                                    name: 'image1',
                                                    inputType:'file',
                                                }*/
                                            ]
                                }
                    }
            );

    return FrmTabs_popupdatahasilrad;
}
//
function SaveStatusBaca(status){

	Ext.Ajax.request({
		url: baseURL + "index.php/rad/Radiologi/setStatusBaca",
		params: {
			status 		: status,
			kd_pasien	: rowSelectedHasilRad.data.KD_PASIEN,
			kd_unit		: rowSelectedHasilRad.data.KD_UNIT,
			tgl_masuk	: rowSelectedHasilRad.data.TGL_MASUK,
			urut_masuk	: rowSelectedHasilRad.data.URUT_MASUK,
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			if (cst.status === true) 
			{
				getkriteriacari();
				ShowPesanInfoHasilRad('Berhasil update status baca Hasil Radiologi!', 'Hapus Data');
			} 
			else 
			{
				getkriteriacari();
				ShowPesanWarningHasilRad('Gagal update status baca Hasil Radiologi!', 'WARNING');
			};
		}
	});
}
//
function mComboDOKTER_HasilRad()
{ 
    var Field = ['KD_DOKTER','NAMA'];
    var dsdokter_viHasilRad = new WebApp.DataStore({ fields: Field });
    dsdokter_viHasilRad.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang ',
            param: "kd_unit = '51' and d.jenis_dokter=1 "
        }
    });
    
    // console.log(rowSelectedPenJasRad);
    var form = new Ext.form.ComboBox({
        id              : 'cboDOKTER_viHasilRad',
        x               : 120,
        y               : 100,
        typeAhead       : true,
        triggerAction   : 'all',
        lazyRender      : true,
        mode            : 'local',
        emptyText       : '',
        align           : 'Right',
        width           : 200,
        emptyText       :'Pilih Dokter',
        store           : dsdokter_viHasilRad,
        valueField      : 'KD_DOKTER',
        displayField    : 'NAMA',
        enableKeyEvents : true,
    });
    
    return form;
}
function DataPrint_HasilRad()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/cetaklaporanRadLab/Lap_Rad",
                        params: getdatahasilpasien(),
                        failure: function (o)
                        {

                        },
                        success: function (o)
                        {

                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                window.open(cst.url, 'RAD', "height=,width=");
                            } else
                            {

                            }
                            ;
                        }
                    }
            );

}
;

function getdatahasilpasien()
{
    var tmpumur;
    if (Ext.get('TxtPopupThnLahirPasien').getValue() !== '0')
    {
        tmpumur = Ext.get('TxtPopupThnLahirPasien').getValue() + " Thn";
    } else if (Ext.getCmp('TxtPopupBlnLahirPasien').getValue() !== '0')
    {
        tmpumur = Ext.get('TxtPopupThnLahirPasien').getValue() + " Bln";
    } else
    {
        tmpumur = Ext.getCmp('TxtPopupHariLahirPasien').getValue() + " Hari";
    }
    console.log(rowSelectedHasilRad);
    var params = {
        Tgl: Ext.get('TxtPopupTglCekPasien').getValue(),
        KdPasien: Ext.get('TxtPopupMedrec').getValue(),
        Nama: Ext.get('TxtPopupNamaPasien').getValue(),
        JenisKelamin: tmpjkpopupentry,
        Alamat: Ext.get('TxtPopupAlamat').getValue(),
        Poli: tmppolipopupentry,
        urutmasuk: tmpurutmasukpopupentry,
        Dokter: Ext.get('TxtPopupNamaDokter').getValue(),
        Umur: tmpumur,
        kd_unit : rowSelectedHasilRad.data.KD_UNIT,
    };
    return params;
}

function ShowPesanWarningDiagnosa(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.WARNING,
        width: 250
    });
}
;

function ShowPesanErrorDiagnosa(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.ERROR,
        width: 250
    });
}
;

function ShowPesanInfoDiagnosa(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.INFO,
        width: 250
    });
}
;
var tmptglMasukPopupentry;
var tmpurutmasukpopupentry;
var tmpjkpopupentry;
var tmppolipopupentry;

function TRHasilRadInit(rowdata) {
    AddNewHasilRad = false;

    Ext.get('TxtPopupMedrec').dom.value = rowdata.KD_PASIEN;
    Ext.get('TxtPopupTglCekPasien').dom.value = ShowDate(rowdata.TGL_MASUK);
    Ext.get('TxtPopupNamaPasien').dom.value = rowdata.NAMA;
    Ext.get('TxtPopupTglLahirPasien').dom.value = ShowDate(rowdata.TGL_LAHIR);
    Ext.get('TxtPopupAlamat').dom.value = rowdata.ALAMAT;
    // Ext.get('TxtPopupNamaDokter').dom.value = rowdata.NAMA_DOKTER;
    tmpjkpopupentry = rowdata.JENIS_KELAMIN;
    tmpurutmasukpopupentry = rowdata.URUT_MASUK;
    tmptglMasukPopupentry = rowdata.TGL_MASUK;
    tmppolipopupentry = rowdata.NAMA_UNIT;
	tmpkdunitpopupentry = rowdata.KD_UNIT;
    setUsia(ShowDate(rowdata.TGL_LAHIR));
    Ext.getCmp('cboDOKTER_viHasilRad').setValue(rowdata.NAMA_DOKTER_HASIL);
    Ext.get('ID_date_baca').dom.value = ShowDate(rowdata.TGL_BACA);

    Ext.getCmp('TxtPopupMedrec').disable();
    Ext.getCmp('TxtPopupTglCekPasien').disable();
    Ext.getCmp('TxtPopupNamaPasien').disable();
    Ext.getCmp('TxtPopupTglLahirPasien').disable();
    Ext.getCmp('TxtPopupAlamat').disable();
    // Ext.getCmp('TxtPopupNamaDokter').disable();
    Ext.getCmp('TxtPopupThnLahirPasien').disable();
    Ext.getCmp('TxtPopupBlnLahirPasien').disable();
    Ext.getCmp('TxtPopupHariLahirPasien').disable();
    RefreshDataHasilRadDetail(rowdata.KD_PASIEN, rowdata.TGL_MASUK, rowdata.URUT_MASUK, currentKdUnitRadiologi);
    Ext.Ajax.request({
        url: baseURL + "index.php/main/getcurrentshift",
        params:
                {
                    command: '0'
                },
        failure: function (o) {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            tampungshiftsekarang = o.responseText;
        }

    });
	
	Ext.Ajax.request({
        url: baseURL + "index.php/rad/Radiologi/getStatusBaca",
        params:
		{
			kd_pasien	: rowdata.KD_PASIEN,
			kd_unit		: rowdata.KD_UNIT,
			tgl_masuk	: rowdata.TGL_MASUK,
			urut_masuk	: rowdata.URUT_MASUK,
		},
        failure: function (o) {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            console.log( o.responseText);
			if(o.responseText == 't'){
				Ext.getCmp('idStatusBaca').setValue(true);
			}else{
				Ext.getCmp('idStatusBaca').setValue(false);
			}
        }

    });
}
;

function RefreshDataHasilRadDetail(kd_pasien, tgl_masuk, urut) {
    var strKriteriaHasilRad = '';

    strKriteriaHasilRad = "Rad_hasil.Kd_Pasien = '" + kd_pasien + "' And Rad_hasil.Tgl_Masuk = ('" + tgl_masuk + "')  and Rad_hasil.Urut_Masuk =" + urut + "  and Rad_hasil.kd_unit= '"+ tmpkdunitpopupentry +"'";

    dsTRDetailHasilRadList.load({
        params: {
            Skip: 0,
            Take: 1000,
            Sort: 'tgl_transaksi',
            Sortdir: 'ASC',
            target: 'ViewDetHasilRad',
            param: strKriteriaHasilRad
        }
    });
    return dsTRDetailHasilRadList;
};

function getParamDetailHasilRad() {
    var hasil;
    if (tampil == 1) {
        hasil = Ext.getCmp('textareahasil').getValue();
    } else {
        hasil = Ext.getCmp('textareapopuphasil').getValue();
    }

    
    var params = {
        Table: 'ViewHasilRad',
        KdPasien:tmpsimpan.data.KD_PASIEN,
        KdUnit: tmpkdunitpopupentry,
        Tgl:tmptglMasukPopupentry,
        UrutMasuk:tmpsimpan.data.URUT_MASUK,
        urut:tmpKdProduk.data.URUT,
        KdTest:tmpKdProduk.data.KD_TEST,
        Hasil:hasil,
        id_template:rowSelectedTemplate.data.KODE,
    };
    return params;
}
;

function getParamTemplateHasilRad() {
    var hasil;
    var tmpidTemplate;
    var tmpisiTemplate;
    var tmpkd_dokter;
    var tmpkd_produk;
    if (tmptemplate === undefined)
    {
        tmpidTemplate = '';
        tmpisiTemplate = Ext.getCmp('cboTemplateRequestEntry').getValue();
        tmpkd_dokter= tmpsimpan.data.KD_DOKTER;
        tmpkd_produk= tmpKdProduk.data.KD_TEST;
        hasil = Ext.getCmp('textareahasil').getValue();
    }else
    {
        tmpidTemplate = tmptemplate.data.KODE;
        tmpisiTemplate = tmptemplate.data.KET;
        tmpkd_dokter= tmptemplate.data.KD_DOKTER;
        tmpkd_produk= tmptemplate.data.KD_PRODUK;
        hasil= tmptemplate.data.NAMA;
    }
    

    
    var params = {
        Table: 'ViewHasilRad',
        id_template:tmpidTemplate,
        template: hasil,
        kd_dokter:tmpkd_dokter,
        kd_produk:tmpkd_produk,
        keterangan:tmpisiTemplate
    };
  // keterangan text,
    return params;
}
;

function getItemPanelmedrec(lebar) {
    var items = {
        layout: 'column',
        border: false,
        items: [{
                columnWidth: .40,
                layout: 'form',
                labelWidth: 100,
                border: false,
                items:
                        [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No. Medrec',
                                name: 'txtNoMedrecDetransaksi',
                                id: 'txtNoMedrecDetransaksi',
                                readOnly: true,
                                anchor: '99%',
                                listeners: {
                                }
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'Nama Pasien ',
                                readOnly: true,
                                name: 'txtNamaPasienDetransaksi',
                                id: 'txtNamaPasienDetransaksi',
                                anchor: '100%',
                                listeners: {
                                }
                            }
                        ]
            },
            {
                columnWidth: .60,
                layout: 'form',
                border: false,
                labelWidth: 2,
                items: [
                ]
            }]
    };
    return items;
}
;

function refeshHasilRad(param) {
	
    dsTRHasilRadList.load({
		params:{
			Skip: 0,
			Take: '10000',
			Sort: '',
			Sortdir: 'ASC',
			target: 'ViewHasilRad',
			param: param
		}
	});
    return dsTRHasilRadList;
}

function Datasave_HasilRad() {
    Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: getParamDetailHasilRad(),
		success: function (o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				Ext.Ajax.request({
					url: baseURL + "index.php/rad/viewhasilrad/saveTemplate",
					params: getParamTemplateHasilRad(),
					success: function (o){
					}
				});
				ShowPesanInfoHasilRad('Data berhasil di simpan', 'Simpan Data');
				FormLookInputLookupHasil.close();
				RefreshDataHasilRadDetail(cst.KD_PASIEN, cst.TGL_MASUK, cst.URUT_MASUK);
				Ext.getCmp('textareapopuphasil').setValue('');
				Ext.getCmp('textareahasil').setValue('');
				var hasil;
				if (tampil == 1) {
					hasil = Ext.getCmp('textareahasil').getValue();
				} else {
					hasil = Ext.getCmp('textareapopuphasil').getValue();
				}
				// Ext.getCmp('textareapopuphasil').setValue(cst.HASIL);
				Ext.getCmp('textareapopuphasil').setValue(hasil);
				Ext.getCmp('textareahasil').setValue(hasil);
				// Ext.getCmp('textareahasil').setValue(cst.HASIL);
			} else if (cst.success === false && cst.pesan === 0){
				ShowPesanWarningHasilRad('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
			} else {
				ShowPesanErrorHasilRad('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
			}
		}
	});
}
function ShowPesanWarningHasilRad(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.WARNING,
        width: 250
    });
}
;

function ShowPesanErrorHasilRad(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.ERROR,
        width: 250
    });
}
;

function ShowPesanInfoHasilRad(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.INFO,
        width: 250
    });
}
;

function getItemPanelHasilRad() {
    var items = {
        layout: 'column',
        border: false,
        items: [{
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: false,
                width: 1100,
                height: 5,
                anchor: '100% 100%',
                items: []
            }, {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 130,
                anchor: '100% 100%',
                items: [{
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec '
                    }, {
                        x: 130,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    }, {
                        x: 140,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtFilterGridDataView_MEDREC_viKasirRwj',
                        id: 'TxtFilterGridDataView_MEDREC_viKasirRwj',
                        width: 110,
                        enableKeyEvents: true,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        var tmpNoMedrec = Ext.get('TxtFilterGridDataView_MEDREC_viKasirRwj').getValue();
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                        {
                                            if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                            {
                                                var tmpgetNoMedrec = formatnomedrec(Ext.get('TxtFilterGridDataView_MEDREC_viKasirRwj').getValue())
                                                Ext.getCmp('TxtFilterGridDataView_MEDREC_viKasirRwj').setValue(tmpgetNoMedrec);
                                                getkriteriacari();
                                            } else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                {
                                                    getkriteriacari();
                                                } else
                                                    Ext.getCmp('TxtFilterGridDataView_MEDREC_viKasirRwj').setValue('');
                                            }
                                        }
                                    }

                                }
                    }, {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    }, {
                        x: 130,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    }, {
                        x: 140,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtFilterGridDataView_NAMA_viKasirRwj',
                        id: 'TxtFilterGridDataView_NAMA_viKasirRwj',
                        width: 240,
                        enableKeyEvents: true,
                        listeners: {
                            'specialkey': function () {
                                if (Ext.EventObject.getKey() === 13) {
                                    getkriteriacari()
                                }
                            }
                        }
                    }, {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Tgl Kunjung '
                    }, {
                        x: 130,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    }, {
                        x: 140,
                        y: 70,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterHasilRad',
                        format: 'd/M/Y',
                        value: tigaharilalu
                    }, {
                        x: 250,
                        y: 70,
                        xtype: 'label',
                        text: ' s/d '
                    }, {
                        x: 280,
                        y: 70,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterHasilRad',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },{
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Status Baca Hasil '
                    }, {
                        x: 130,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    }, 
					{
						x: 140,
						y: 100,
						xtype: 'checkbox',
						boxLabel: '',
						name: 'idStatusBacaCari',
						id: 'idStatusBacaCari',
						listeners: {
							render: function(c){
								c.getEl().on({
									click: function() {
										getkriteriacari();
									}
								});
							}
						}
					},
					{
                        x: 310,
                        y: 100,
                        xtype: 'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function (sm, row, rec) {
                            getkriteriacari()
                        }
                    }]
            }, {
                //                columnWidth:.99,
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: false,
                width: 1100,
                height: 5,
                anchor: '100% 100%',
                items: []
            }]
    };
    return items;
}
;

function setUsia(Tanggal)
{
	console.log(Tanggal);
    Ext.Ajax.request
            ({
                url: baseURL + "index.php/main/GetUmur",
                params: {
                    TanggalLahir: ShowDateReal2(Tanggal)
                },
                success: function (o)
                {
                    var tmphasil = o.responseText;
                    var tmp = tmphasil.split(' ');

                    if (tmp.length === 6)
                    {
                        Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
                        Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                        Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[4]);
                    } else if (tmp.length === 4) {
                        if (tmp[1] === 'years') {
                            if (tmp[3] === 'day') {
                                Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
                                Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                                Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                            } else {
                                Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
                                Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                                Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                            }
                        } else {
                            Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
                            Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                        }
                    } else if (tmp.length === 2) {
                        if (tmp[1] === 'year') {
                            Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'years') {
                            Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mon') {
                            Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
                            Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mons') {
                            Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
                            Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else {
                            Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
                            Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[0]);
                        }
                    } else if (tmp.length === 1) {
                        Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
                        Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                        Ext.getCmp('TxtPopupHariLahirPasien').setValue('1');
                    } else {
                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                    }
                }
            });
}

function getkriteriacari()
{
    var tmpkriteria = " transaksi.ispay = '1' AND left(unit.kd_unit,1) = '"+ currentKdUnitRadiologi[0] +"' and kunjungan.tgl_masuk >= '" + Ext.get('dtpTglAwalFilterHasilRad').getValue() + "' and kunjungan.tgl_masuk <= '" + Ext.get('dtpTglAkhirFilterHasilRad').getValue() + "'";

    if (Ext.get('TxtFilterGridDataView_MEDREC_viKasirRwj').getValue() !== "")
    {
        tmpkriteria += " AND Pasien.kd_pasien like '" + Ext.get('TxtFilterGridDataView_MEDREC_viKasirRwj').getValue() + "%'";
    } else if (Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() !== "")
    {
        tmpkriteria += " AND pasien.Nama like '" + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + "%'";
    }
	
	if(Ext.getCmp('idStatusBacaCari').getValue() == true){
		tmpkriteria +=  "   AND rad_hasil_flag.flag_baca = '1' ";
	}else{
		tmpkriteria +=  "  AND (rad_hasil_flag.flag_baca = '0' or rad_hasil_flag.flag_baca is null) ";
	} 
    refeshHasilRad(tmpkriteria);
}

function formLookUpAddTemplate(id, template){
     var simple = new Ext.FormPanel({
        labelWidth: 75, // label settings here cascade unless overridden
        frame:false,
        anchor : '100%',
        width : '100%',
        bodyStyle : 'padding : 5px 5px 5px 5px',
        defaults: {width: 230},
        defaultType: 'textfield',
        items: [
            {
                fieldLabel  : 'Template',
                name        : 'txt_template',
                id          : 'txt_template',
                value       : template,
                allowBlank:false
            },
        ],

        buttons: [{
            text: 'Simpan',
            handler : function(){
                Ext.Ajax.request({
                    url: baseURL + "index.php/rad/Radiologi/simpan_template_hasil",
                    params: {
                        template        : Ext.getCmp('txt_template').getValue(),
                        id_template     : id,
                        keterangan      : true,
                    },
                    success: function (o){
                        var cst = Ext.decode(o.responseText);
                        if (cst.status === true) {
                            RefreshDataTemplateHasilRad();
                            FormLookInputLookupHasil.close();
                        }else{
                            ShowPesanWarningHasilRad('Data tidak berhasil di simpan ','Simpan Data');
                        }
                    }
                });
            },
        },{
            text: 'Keluar',
            handler : function(){
                FormLookInputLookupHasil.close();
            }
        }]
    });

    var FormLookInputLookupHasil = new Ext.Window({
        id: 'win_FormLookInputLookupHasil',
        title: 'Template',
        closeAction: 'destroy',
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        width   : 350,
        height   : 120,
        iconCls: 'Request',
        modal: true,
        defaults: {width: 230},
        defaultType: 'textfield',
        items: [
            simple,
        ],
    });
    return FormLookInputLookupHasil;
}

var tmptemplate;
function InputLookupHasil(rowdata) {
    var Field_Hasil_Rad = ['KODE', 'NAMA', 'KD_DOKTER', 'KD_PRODUK', 'KET'];

    DsTemplateRad = new WebApp.DataStore({fields: Field_Hasil_Rad});

    GridTemplateHasilRad = new Ext.grid.GridPanel({
        x: 5,
        y: 5,
        ddGroup: 'secondGridDDGroup',
        store: DsTemplateRad,
        autoScroll: true,
        columnLines: true,
        border: true,
        enableDragDrop: true,
        height: 420,
        width:300,
        stripeRows: true,
        trackMouseOver: true,
        title: 'Template Hasil Pemeriksaan',
        plugins: [new Ext.ux.grid.FilterRow()],
        colModel: new Ext.grid.ColumnModel
                (
                        [
                            new Ext.grid.RowNumberer(),
                            {
                                id: 'colNMPASIEN_viDaftar',
                                header: 'Template Hasil',
                                dataIndex: 'KET',
                                sortable: true,
                                width: 100,
                                filter : {},
                            }
                        ]
                        ),
        selModel: new Ext.grid.RowSelectionModel
                (
                        {
                            singleSelect: true,
                            listeners:
                                    {
                                        rowselect: function (sm, row, rec)
                                        {
                                            rowSelectedTemplate = DsTemplateRad.getAt(row);
                                            console.log(rowSelectedTemplate);
                                            tmptemplate = rowSelectedTemplate;
                                            if (rowSelectedTemplate !== undefined)
                                            {
                                                tampil = 0;
                                                Ext.get('textareahasil').dom.value = rowSelectedTemplate.data.NAMA;
                                                tmpdatahasilsebelumsaving = rowSelectedTemplate.data;

                                            }
                                        }
                                    }
                        }
                ),
        listeners:
                {
                    rowdblclick: function (sm, ridx, cidx)
                    {
                        rowSelectedDetHasilRad = dsTRDetailHasilRadList.getAt(ridx);
                        rowSelectedTemplate = DsTemplateRad.getAt(ridx);
                        console.log(rowSelectedTemplate);
                        formLookUpAddTemplate(rowSelectedTemplate.data.KODE, rowSelectedTemplate.data.KET).show();
                        if (rowSelectedDetHasilRad !== undefined)
                        {
                            tampil = 1;
                            // InputLookupHasil(rowSelectedDetHasilRad.data);
                        }
                    },
                    'afterrender': function () {
                        tampil = 0;
                    }
                },
        tbar: [
            {
                xtype   : 'button',
                text    : 'Tambah',
                handler : function(){
                    formLookUpAddTemplate().show();
                }
            },
            {
                xtype   : 'button',
                text    : 'Hapus',
                handler : function(){
                    Ext.Ajax.request({
                        url: baseURL + "index.php/rad/Radiologi/delete_template_hasil",
                        params: {
                            id_template     : rowSelectedTemplate.data.KODE,
                        },
                        success: function (o){
                            var cst = Ext.decode(o.responseText);
                            if (cst.status === true) {
                                RefreshDataTemplateHasilRad();
                            }else{
                                ShowPesanWarningHasilRad('Data tidak berhasil di simpan ','Simpan Data');
                            }
                        }
                    });
                }
            },
           /* {
                xtype:'label',
                text:'Nama : '
            },
            {xtype: 'tbspacer', width: 10},
            mComboPencarianTemplate(),*/
            // {
            //     xtype:'textfield',
            //     id:'txtcaritemplatehasil',
            //     width : 245
            // },
            {xtype: 'tbspacer', width: 10},
            
        ],
        viewConfig:
                {
                    forceFit: true
                }
    });
    FormLookInputLookupHasil = new Ext.Window({
        id: 'gridKonsultasi',
        title: rowdata.DESKRIPSI,
        closeAction: 'destroy',
        width: 800,
        height: 500,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        iconCls: 'Request',
        modal: true,
        items: {
            layout: 'hBox',
            border: false,
            bodyStyle: 'padding:0px 0px 0px 0px',
            defaults: {
                margins: '3 3 3 3'
            },
            // anchor: '90%',
            items:
                    [
                        {
                            columnWidth: .50,
                            layout: 'absolute',
                            bodyStyle: 'padding: 0px 0px 0px 0px',
                            border: false,
                            width: 800,
                            height: 500,
                            // anchor: '100% 100%',
                            items:
                                    [
                                        GridTemplateHasilRad,
                                        {
                                            x: 310,
                                            y: 5,
                                            xtype: 'label',
											text:'Input hasil / pilih hasil berdasarkan template :'
                                        },
                                        {
                                            x: 310,
                                            y: 25,
                                            xtype: 'textarea',
                                            name: 'textarea',
                                            id: 'textareahasil',
                                            height: 419,
                                            width: 470
                                        }
                                    ]
                        }
                    ],
            fbar: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hideLabel: false,
                    handler: function (sm, row, rec)
                    {
                        tampil = 1;
                        Ext.Ajax.request({
                            url: baseURL + "index.php/rad/Radiologi/simpan_template_hasil",
                            params: {
                                template    : Ext.getCmp('textareahasil').getValue(),
                                id_template : rowSelectedTemplate.data.KODE,
                                keterangan  : false,
                            },
                            success: function (o){
                                var cst = Ext.decode(o.responseText);
                                if (cst.status === true) {
                                    RefreshDataTemplateHasilRad();
                                    ShowPesanInfoHasilRad('Data berhasil di simpan ','Simpan Data');
                                }else{
                                    ShowPesanWarningHasilRad('Data tidak berhasil di simpan ','Simpan Data');
                                }
                            }
                        });
                    }
                },{
                    xtype: 'button',
                    text: 'Pilih Template',
                    hideLabel: false,
                    handler: function (sm, row, rec)
                    {
                        Ext.getCmp('textareapopuphasil').setValue(Ext.getCmp('textareahasil').getValue());
                        FormLookInputLookupHasil.close();
                    }
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    hideLabel: false,
                    handler: function (sm, row, rec)
                    {
                        FormLookInputLookupHasil.close();
                    }
                }
            ]
        },
        listeners: {
            'afterrender': function () {
                // Ext.getCmp('textareahasil').setValue(rowdata.HASIL);
                // console.log(rowdata);
                RefreshDataTemplateHasilRad(rowdata);
                tampil = 1;

            }
        }
    });

    FormLookInputLookupHasil.show();
}
;

var selectTemplate;
function RefreshDataTemplateHasilRad(rowdata) {
    var strKriteriaTemplateHasilRad = '';

    strKriteriaTemplateHasilRad = "";

    DsTemplateRad.load({
        params: {
            Skip: 0,
            Take: 1000,
            Sort: 'id_template',
            Sortdir: 'ASC',
            target: 'ViewTemplateHasilRad',
            param: strKriteriaTemplateHasilRad
        }
    });
    return dsTRDetailHasilRadList;
}
;

function PencarianDataTemplateHasilRad(Nama) {
    var strKriteriaTemplateHasilRad = '';

    strKriteriaTemplateHasilRad = "keterangan like '%"+ Nama +"%'";

    DsTemplateRad.load({
        params: {
            Skip: 0,
            Take: 1000,
            Sort: 'id_template',
            Sortdir: 'ASC',
            target: 'ViewTemplateHasilRad',
            param: strKriteriaTemplateHasilRad
        }
    });
    return dsTRDetailHasilRadList;
}
;

function mComboPencarianTemplate()
{
    var Field = ['KODE', 'KET'];

    dsPencarianTemplateRequestEntry = new WebApp.DataStore({fields: Field});
    dsPencarianTemplateRequestEntry.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    //Sort: 'DEPT_ID',
                                    Sort: 'id_template',
                                    Sortdir: 'ASC',
                                    target: 'ViewTemplateHasilRad',
                                    param: ''
                                }
                    }
            )

    var cboTemplateRequestEntry = new Ext.form.ComboBox
            (
                    {
                        id: 'cboTemplateRequestEntry',
                        typeAhead: false,
                        hideTrigger: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: false,
                        emptyText: '',
                        selectOnFocus: true,
                        fieldLabel: 'Nama',
                        align: 'Right',
                        store: dsPencarianTemplateRequestEntry,
                        valueField: 'KODE',
                        displayField: 'KET',
                        width:245,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectTemplate = b.data.KET;
                                         // alert(selectTemplate);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                            {
                                                selectTemplate = Ext.get('cboTemplateRequestEntry').getValue()
                                                PencarianDataTemplateHasilRad(selectTemplate);
                                                // alert(selectTemplate);
                                            }
                                        }, c);
                                    }

                                }
                    }
            );
    return cboTemplateRequestEntry;


};

function getCurrentKdUnitRad(){
	Ext.Ajax.request
	({
		url:  baseURL + "index.php/rad/viewhasilrad/getKdUnitCurrentRad",
		params: {text:''},
		failure: function(o)
		{
			ShowPesanErrorHasilRad("Error get Kode unit current. Hubungi Admin!","Error");
		},	    
		success: function(o)
		{
			var cst = Ext.decode(o.responseText);
			if(cst.success == true){
				currentKdUnitRadiologi=cst.kd_unit;
			} else{
				ShowPesanErrorHasilRad("Gagal get Kode unit current!","Error");
			}
			
		}
	});
}


function FormLookupUploadfile_HasilRad(Link, Direktori = null, File_name = null) 
{  
    console.log("cekk");
    v_Link  = "";
    v_Direktori     = "";
    v_File_name     = "";
    vWinFormEntryUploadfile = new Ext.Window
    (           
        {
            id: 'formLookupUploadfile_HasilRad',
            title: 'Upload File',
            closeAction: 'destroy',
            width: 450,
            height: 120,
            border: false,          
            frame: true,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'find',            
            modal: true,                                   
            items: 
            [
                GetpanelUploadFile_HasilRad(this.Direktori, this.File_name)
            ],      
            listeners:
            { 
                activate: function()
                { 
                    nLink        = Link;
                    strDirektori = Direktori;
                    strFileName  = File_name;
                } 
            }
        }
    );  
    vWinFormEntryUploadfile.show();
};


function GetpanelUploadFile_HasilRad(Direktori, File_name)
{
    var vWinFormUploadfile_HasilRad = new Ext.FormPanel
    ({
        //id: 'FormUpldSeminar',
        layout: 'form',
        width: 400,
        height: 100,
        frame: true,
        border: false,
        fileUpload: true,       
        anchor: '100% 100%',
        items:
        [
            {
                xtype: 'fileuploadfield',
                emptyText: 'Path file upload',
                fieldLabel: 'Path file',
                frame: false,
                border: false,
                name: 'file',
                id: 'file',
                anchor: '100%',
                buttonText: 'browse'
            },
            {
                xtype: 'hidden',
                name: 'direktori',
                id: 'direktori',
            },
            {
                xtype: 'hidden',
                name: 'file_name',
                id: 'file_name',
            }
        ],
        buttons:
        [
            {
                text: 'Upload',
                handler: function() 
                {
                    // var strUrl="./"+nLink;
                    var strUrl= nLink;
                    Ext.getCmp('direktori').setValue(strDirektori);
                    Ext.getCmp('file_name').setValue(strFileName);
                    /*if (nLink == uFormFoto || nLink == uFormkehadiran || nLink == uFormTunjPerPeriode) 
                    {
                        Ext.get('file').dom.value = strFileName;
                        strUrl="./"+nLink
                    }else
                    {
                        // Ext.get('direktori').dom.value = strPathUploadAll + strDirektori;
                        Ext.get('file').dom.value = strFileName;
                        strUrl="./"+nLink
                    } */
                    
                    if (vWinFormUploadfile_HasilRad.getForm().isValid()) 
                    {
                        vWinFormUploadfile_HasilRad.getForm().submit({

                            url: strUrl,//"./home.mvc/Upload",
                            // waitMsg: 'Uploading your photo...',
                            // waitMsg: 'Uploading your File...',
                            success: function(form, o) //(3)
                            {   
                            },                              
                            failure: function(form, o) //(4)
                            {
                            }
                        });
                        Ext.Msg.alert('Success', 'Data berhasil di upload');
                    }

                }
            },
            {
                text: 'Cancel',
                handler: function() 
                {
                    vWinFormEntryUploadfile.close();
                }
            }
        ]
    });     
    return vWinFormUploadfile_HasilRad;
};
function lookUpFileManagement_Hasil(){
    // var url_file = baseURL+"index.php/rad/file_management/gallery/"+Ext.getCmp('TxtPopupMedrec').getValue()+"_"+rowSelectedHasilRad.data.TGL_MASUK+"_"+rowSelectedHasilRad.data.KD_UNIT;
    // var url_file = "http://10.11.0.31:8080/oviyam2/viewer.html?patientID=01763461&studyUID=1.2.826.0.1.3680043.6.15913.20900.20130415103520.168.6&serverName=PACS";
    // var url_file = "http://10.11.0.35:8080/wado?requestType=WADO&studyUID=1.2.826.0.1.3680043.6.15913.20900.20130415103520.168.6&seriesUID=1.2.826.0.1.3680043.2.1545.1.2.1.7.20181115.170217.756.16&objectUID=1.2.826.0.1.3680043.6.30943.7375.20130415103520.168.5&columns=128";
    // var url_file = "http://10.11.0.35:8080/oviyam/";
    var url_file = "http://10.11.0.35:8080/oviyam/viewer.html?patientID="+rowSelectedHasilRad.data.PATIENT_ID+"&studyUID="+rowSelectedHasilRad.data.STUDY_UID+"&serverName=PACS";
    // var url_file = baseURL+"index.php/main/functionRAD/get_oviyam";
    /*jQuery.support.cors = true;
    $.ajax({url: url_file, success: function(result){
        // $("#_fileManagement_HasilRAD").html(result);
        alert(result);
    }});*/
    // var url = 'http://api.alice.com/cors';
    // 
    if (rowSelectedHasilRad.data.PATIENT_ID == null || rowSelectedHasilRad.data.PATIENT_ID == '' || rowSelectedHasilRad.data.STUDY_UID == null || rowSelectedHasilRad.data.STUDY_UID == '') {
        Ext.Msg.alert('Information', 'Tidak ada dokument pemeriksaan');
    }else{
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("target", "_blank");
        form.setAttribute("action", url_file);
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "data");
        hiddenField.setAttribute("value", '');
        form.appendChild(hiddenField);
        document.body.appendChild(form);
        form.submit();
    }
    /* FormFileManagement = new Ext.Window({
        id: '_fileManagement_HasilRAD',
        title: 'File Management',
        closeAction: 'destroy',
        width: '100%',
        height: '100%',
        maximized : true,
        constrain: true,
        modal: true,
        // html: "<div style='text-align:center;'><img  style='height: 100%;'src='" + url_file + "'></img></div>",
        html: "<iframe  style='height: 100%;width: 100%;'src='" + url_file + "'></iframe>",
        // html: "<frame style='height: 100%;width: 100%;'src='" + url_file + "'></frame>",
        // html: "<object style='height: 100%;width: 100%;' width='100%' height='100%' data='" + url_file + "'></object>",
        // html: "<embed style='height: 100%;width: 100%;' width='100%' height='100%' type='video/webm' src='" + url_file + "'></object>",
        tbar    : [
            // {
            //         xtype       : 'textfield',
            //         fieldLabel  : "&nbsp;&nbsp;&nbsp;Image1",
            //         id          : 'file_',
            //         name        : 'file_',
            //         inputType   : 'file',
            // },
            {
                    xtype       : 'button',
                    id          : 'btnUpload_file',
                    text        : 'Upload Document',
                    handler     : function(){
                        var name_file = Ext.getCmp('TxtPopupMedrec').getValue()+"_"+rowSelectedHasilRad.data.TGL_MASUK+"_"+rowSelectedHasilRad.data.KD_UNIT;
                        FormFileManagement.close();
                        FormLookupUploadfile_HasilRad(baseURL+'index.php/rad/file_management/upload_hasil','./Img Asset/RAD', name_file);
                    }
                },{
                    xtype       : 'button',
                    id          : 'btnReload_Upload',
                    text        : 'Reload',
                    handler     : function(){
                        
                    }
                },
        ],
        listeners: {
        }
    });

    FormFileManagement.show();*/
};