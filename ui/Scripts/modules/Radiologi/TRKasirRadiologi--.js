// Data Source ExtJS # --------------

/**
*	Nama File 		: TRKasirRadiologi.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Kasir Radiologi adalah proses untuk pembayaran pasien pada radiologi
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Kasir Radiologi # --------------

var dataSource_viKasirRadiologi;
var selectCount_viKasirRadiologi=50;
var NamaForm_viKasirRadiologi="Kasir Radiologi ";
var mod_name_viKasirRadiologi="viKasirRadiologi";
var now_viKasirRadiologi= new Date();
var addNew_viKasirRadiologi;
var rowSelected_viKasirRadiologi;
var setLookUps_viKasirRadiologi;
var mNoKunjungan_viKasirRadiologi='';

var CurrentData_viKasirRadiologi =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viKasirRadiologi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Radiologi # --------------

// Start Project Kasir Radiologi # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viKasirRadiologi
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viKasirRadiologi(mod_id_viKasirRadiologi)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viKasirRadiologi = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viKasirRadiologi = new WebApp.DataStore
	({
        fields: FieldMaster_viKasirRadiologi
    });
    
    // Grid Kasir Radiologi # --------------
	var GridDataView_viKasirRadiologi = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viKasirRadiologi,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viKasirRadiologi = undefined;
							rowSelected_viKasirRadiologi = dataSource_viKasirRadiologi.getAt(row);
							CurrentData_viKasirRadiologi
							CurrentData_viKasirRadiologi.row = row;
							CurrentData_viKasirRadiologi.data = rowSelected_viKasirRadiologi.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viKasirRadiologi = dataSource_viKasirRadiologi.getAt(ridx);
					if (rowSelected_viKasirRadiologi != undefined)
					{
						setLookUp_viKasirRadiologi(rowSelected_viKasirRadiologi.data);
					}
					else
					{
						setLookUp_viKasirRadiologi();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir Radiologi
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_viKasirRadiologi',
						header: 'No. Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMPASIEN_viKasirRadiologi',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 35,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colALAMAT_viKasirRadiologi',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					//-------------- ## --------------
					{
						id: 'colTglKunj_viKasirRadiologi',
						header:'Tgl Kunjungan',
						dataIndex: 'TGL_KUNJUNGAN',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_KUNJUNGAN);
						}
					},
					//-------------- ## --------------
					{
						id: 'colPoliTujuan_viKasirRadiologi',
						header:'Poli Tujuan',
						dataIndex: 'NAMA_UNIT',
						width: 50,
						sortable: true,
						filter: {}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viKasirRadiologi',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viKasirRadiologi',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viKasirRadiologi != undefined)
							{
								setLookUp_viKasirRadiologi(rowSelected_viKasirRadiologi.data)
							}
							else
							{								
								setLookUp_viKasirRadiologi();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viKasirRadiologi, selectCount_viKasirRadiologi, dataSource_viKasirRadiologi),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viKasirRadiologi = new Ext.Panel
    (
		{
			title: NamaForm_viKasirRadiologi,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viKasirRadiologi,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viKasirRadiologi],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viKasirRadiologi,
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'Jenis Transaksi : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_MediSmart(125, "ComboJenisTransaksi_viKasirRadiologi"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'No. : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMedrec_viKasirRadiologi',
							emptyText: 'No. Medrec',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viKasirRadiologi = 1;
										DataRefresh_viKasirRadiologi(getCriteriaFilterGridDataView_viKasirRadiologi());
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Kunjungan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_viKasirRadiologi',
							value: now_viKasirRadiologi,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viKasirRadiologi();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Kelompok : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_MediSmart(125, "ComboKelompok_viKasirRadiologi"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viKasirRadiologi',
							emptyText: 'Nama',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viKasirRadiologi = 1;
										DataRefresh_viKasirRadiologi(getCriteriaFilterGridDataView_viKasirRadiologi());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viKasirRadiologi',
							value: now_viKasirRadiologi,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viKasirRadiologi();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Poli Tujuan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_MediSmart(125, "ComboPoli_viKasirRadiologi"),
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Alamat : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_ALAMAT_viKasirRadiologi',
							emptyText: 'Alamat',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viKasirRadiologi = 1;
										DataRefresh_viKasirRadiologi(getCriteriaFilterGridDataView_viKasirRadiologi());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viKasirRadiologi',
							handler: function() 
							{					
								DfltFilterBtn_viKasirRadiologi = 1;
								DataRefresh_viKasirRadiologi(getCriteriaFilterGridDataView_viKasirRadiologi());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viKasirRadiologi;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viKasirRadiologi # --------------

/**
*	Function : setLookUp_viKasirRadiologi
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viKasirRadiologi(rowdata)
{
    var lebar = 985;
    setLookUps_viKasirRadiologi = new Ext.Window
    (
    {
        id: 'SetLookUps_viKasirRadiologi',
		name: 'SetLookUps_viKasirRadiologi',
        title: NamaForm_viKasirRadiologi, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viKasirRadiologi(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viKasirRadiologi=undefined;
                //datarefresh_viKasirRadiologi();
				mNoKunjungan_viKasirRadiologi = '';
            }
        }
    }
    );

    setLookUps_viKasirRadiologi.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viKasirRadiologi();
		// Ext.getCmp('btnDelete_viKasirRadiologi').disable();	
    }
    else
    {
        // datainit_viKasirRadiologi(rowdata);
    }
}
// End Function setLookUpGridDataView_viKasirRadiologi # --------------

/**
*	Function : getFormItemEntry_viKasirRadiologi
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viKasirRadiologi(lebar,rowdata)
{
    var pnlFormDataBasic_viKasirRadiologi = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodata_viKasirRadiologi(lebar),
				//-------------- ## -------------- 
				getItemDataKunjungan_viKasirRadiologi(lebar), 
				//-------------- ## --------------
				getItemGridTransaksi_viKasirRadiologi(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compNoreg_viKasirRadiologi',
					id: 'compNoreg_viKasirRadiologi',
					items: 
					[
						{ 
							xtype: 'tbspacer',
							height: 30,
							width: 1,
						},
						//-------------- ## --------------
						{
							xtype: 'displayfield',				
							width: 70,								
							value: 'Jumlah:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-top':'3px','margin-left':'540px'},
						},
						//-------------- ## --------------
						{
		                    xtype: 'textfield',
		                    id: 'txtDRGridTransaksiEditData_viKasirRadiologi',
		                    name: 'txtDRGridTransaksiEditData_viKasirRadiologi',
							style:{'text-align':'right','margin-top':'3px','margin-left':'555px'},
		                    width: 100,
		                    value: 0,
		                    readOnly: true,
		                },
		                //-------------- ## --------------
		                {
		                    xtype: 'textfield',
		                    id: 'txtCRGridTransaksiEditData_viKasirRadiologi',
		                    name: 'txtCRGridTransaksiEditData_viKasirRadiologi',
							style:{'text-align':'right','margin-top':'3px','margin-left':'551px'},
		                    width: 100,
		                    value: 0,
		                    readOnly: true,
		                },
		                //-------------- ## --------------
		            ],
		        },
                //-------------- ## --------------
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viKasirRadiologi',
						handler: function(){
							dataaddnew_viKasirRadiologi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viKasirRadiologi',
						handler: function()
						{
							datasave_viKasirRadiologi(addNew_viKasirRadiologi);
							datarefresh_viKasirRadiologi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viKasirRadiologi',
						handler: function()
						{
							var x = datasave_viKasirRadiologi(addNew_viKasirRadiologi);
							datarefresh_viKasirRadiologi();
							if (x===undefined)
							{
								setLookUps_viKasirRadiologi.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viKasirRadiologi',
						handler: function()
						{
							datadelete_viKasirRadiologi();
							datarefresh_viKasirRadiologi();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Lookup ',
		                iconCls:'find',
		                menu: 
		                [
		                	{
						        text: 'Lookup Hasil Radiologi',
						        id:'btnLookUpHasilRadiologi_viKasirRadiologi',
						        iconCls: 'find',
						        handler: function(){
						            FormSetHasilRadiologi_viKasirRadiologi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Lookup Ganti Dokter',
						        id:'btnLookUpGantiDokter_viKasirRadiologi',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupGantiDokter_viKasirRadiologi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Lookup Transaksi Lama',
						        id:'btnLookUpTransaksiLama_viKasirRadiologi',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupTransaksiLama_viKasirRadiologi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
		                ],
		            },
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Pembayaran ',
		                iconCls:'print',
		                menu: 
		                [
		                	{
						        text: 'Pembayaran',
						        id:'btnPembayaran_viKasirRadiologi',
						        iconCls: 'print',
						        handler: function(){
						            FormSetPembayaran_viKasirRadiologi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
        					{
						        text: 'Transfer ke Rawat Inap',
						        id:'btnTransferPembayaran_viKasirRadiologi',
						        iconCls: 'print',
						        handler: function(){
						            FormSetTransferPembayaran_viKasirRadiologi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Edit Tarif',
						        id:'btnEditTarif_viKasirRadiologi',
						        iconCls: 'print',
						        handler: function(){
						            FormSetEditTarif_viKasirRadiologi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
		                ],
		            },
					//-------------- ## --------------
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viKasirRadiologi;
}
// End Function getFormItemEntry_viKasirRadiologi # --------------

/**
*	Function : getItemPanelInputBiodata_viKasirRadiologi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viKasirRadiologi(lebar) 
{
    var rdAsalPasien_viKasirRadiologi = new Ext.form.RadioGroup
	({  
        fieldLabel: '',  
        columns: 3, 
        width: 450,
		border:false,
        items: 
		[  
			{
				boxLabel: '1. R. Jalan/UGD', 
				width:70,
				id: 'rdSatuAsalPasien_viKasirRadiologi', 
				name: 'rdSatuAsalPasien_viKasirRadiologi', 
				inputValue: '1'
			},  
			//-------------- ## --------------
			{
				boxLabel: '2. Rawat Inap', 
				width:70,
				id: 'rdDuaAsalPasien_viKasirRadiologi', 
				name: 'rdDuaAsalPasien_viKasirRadiologi', 
				inputValue: '2'
			},
			//-------------- ## --------------
			{
				boxLabel: '3. Kunjungan Langsung', 
				width:70,
				id: 'rdTigaAsalPasien_viKasirRadiologi', 
				name: 'rdTigaAsalPasien_viKasirRadiologi', 
				inputValue: '3'
			},
			//-------------- ## --------------
        ]  
    });
	//-------------- ## --------------

    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'Asal Pasien',
                anchor: '100%',
                width: 250,
                items: 
                [
                    rdAsalPasien_viKasirRadiologi,
                    { 
						xtype: 'tbspacer',
						width: 15,
					},
					//-------------- ## --------------
                    {
						xtype: 'displayfield',
						flex: 1,
						width: 50,
						name: '',
						value: 'No. Reg:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtNoReg_viKasirRadiologi',
						id: 'txtNoReg_viKasirRadiologi',
						emptyText: 'No. Reg',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
                    //-------------- ## --------------  
                    {
						xtype: 'displayfield',
						flex: 1,
						width: 55,
						name: '',
						value: 'No. Foto:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtNoFoto_viKasirRadiologi',
						id: 'txtNoFoto_viKasirRadiologi',
						emptyText: 'No. Foto',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------  
			{
				xtype: 'compositefield',
				fieldLabel: 'NoMedRec',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',
						width: 100,
						name: 'txtNoMedrec_viKasirRadiologi',
						id: 'txtNoMedrec_viKasirRadiologi',
						style:{'text-align':'right'},
						emptyText: 'No. Medrec',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					{ 
						xtype: 'tbspacer',
						width: 140,
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'No. Trans:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',														
						width : 325,	
						name: 'txtNoTrans_viKasirRadiologi',
						id: 'txtNoTrans_viKasirRadiologi',
						emptyText: 'No. Trans',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',
						flex: 1,
						width: 55,
						name: '',
						value: 'Tanggal:',
						fieldLabel: 'Label'
					},
					{
		                xtype: 'compositefield',
		                fieldLabel: 'Tanggal',
		                anchor: '100%',
		                width: 250,
		                items: 
		                [
		                    {
		                        xtype: 'textfield',
		                        id: 'TxtTglEditData_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 30,
		                    },
		                    //-------------- ## --------------  
		                    {
		                        xtype: 'textfield',
		                        id: 'TxtBlnEditData_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 30,
		                    },
		                    //-------------- ## --------------  
		                    {
		                        xtype: 'textfield',
		                        id: 'TxtThnEditData_viKasirRadiologi',
		                        emptyText: '2014',
		                        width: 50,
		                    },
		                    //-------------- ## --------------  
		                ]
		            },
				]
			},	
			//-------------- ## --------------				
			{
				xtype: 'compositefield',
				fieldLabel: 'Nama Pasien',
				name: 'compNoreg_viKasirRadiologi',
				id: 'compNoreg_viKasirRadiologi',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						anchor: '100%',
						width: 245,
						name: 'txtNamaPasien_viKasirRadiologi',
						id: 'txtNamaPasien_viKasirRadiologi',
						emptyText: 'Nama Pasien',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 70,								
						value: 'Dokter:',
						fieldLabel: 'Label',
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 50,
						name: 'txtDokter_viKasirRadiologi',
						id: 'txtDokter_viKasirRadiologi',
						emptyText: 'Dokter',
					},
					Vicbo_Dokter(455, "ComboNamaDokter_viKasirRadiologi"),
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------	
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
								xtype: 'displayfield',				
								width: 70,								
								value: 'Tgl. Lahir',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 50,								
								value: 'Thn',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 30,								
								value: 'Bln',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 30,								
								value: 'Hari',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 80,								
								value: 'Jenis Kelamin',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 65,								
								value: 'G. Darah',
							},
				            //-------------- ## --------------
							{
								xtype: 'displayfield',				
								width: 70,								
								value: 'Kelas:',
							},
				            //-------------- ## --------------
				            {
								xtype: 'textfield',
								flex: 1,
								width : 510,	
								name: 'txtKelas_viKasirRadiologi',
								id: 'txtKelas_viKasirRadiologi',
								emptyText: 'Kelas',
								listeners:
								{
									'specialkey': function() 
									{
										if (Ext.EventObject.getKey() === 13) 
										{
										};
									}
								}
							},
							//-------------- ## --------------
							{ 
								xtype: 'tbspacer',
								height: 25,
							},
							//-------------- ## --------------
				        ]
				    },
		        ]
		    },
			//-------------- ## --------------
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhirEditData_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 30,
		                    },
		                    //-------------- ## -------------- 
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir2EditData_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtThnLhirEditData_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 50,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir3EditData_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir4EditData_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            viComboJenisTransaksi_MediSmart(80,'CmboJenisKelaminEditData_viKasirRadiologi'),
				            //-------------- ## --------------
				            viComboJenisTransaksi_MediSmart(60,'CmboGlgnDrhEditData_viKasirRadiologi'),
				            //-------------- ## --------------
				            { 
								xtype: 'tbspacer',
								width: 5,
							},
							//-------------- ## --------------
							{
								xtype: 'displayfield',				
								width: 70,								
								value: 'Kelmpk P.:',
							},
				            //-------------- ## --------------
				            viComboJenisTransaksi_MediSmart(510,'CmboKelompokEditData_viKasirRadiologi'),
							//-------------- ## --------------
							{ 
								xtype: 'tbspacer',
								height: 25,
							},
							//-------------- ## --------------
				        ]
				    },
		        ]
		    },
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Alamat',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						anchor: '100%',
						width: 245,
						name: 'txtAlamat_viKasirRadiologi',
						id: 'txtAlamat_viKasirRadiologi',
						emptyText: 'Alamat',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 70,								
						value: '',
						fieldLabel: 'Label',
					},
					Vicbo_Dokter(510, "CmboKelompok2EditData_viKasirRadiologi"),
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------	
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viKasirRadiologi # --------------

/**
*	Function : getItemDataKunjungan_viKasirRadiologi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataKunjungan_viKasirRadiologi(lebar)
{
    var ItemDataKunjungan_viKasirRadiologi = 
	{
        xtype: 'panel',
        title: '',
		name: 'DetailItemDataKunjungan_viKasirRadiologi',
		id: 'DetailItemDataKunjungan_viKasirRadiologi',		
        height: 100,
		border:false,
        items: 
		[			
			{
				xtype: 'compositefield',
				fieldLabel: '',
				anchor: '100%',
				width: 1200,
				items: 
				[
					RujukanDetailEditData_viKasirRadiologi(),
					//-------------- ## --------------		
					KeluhanPasienDetailEditData_viKasirRadiologi(),
					//-------------- ## --------------	
				]
			},
        ]
    }
    return ItemDataKunjungan_viKasirRadiologi;
};

// End Function getItemDataKunjungan_viKasirRadiologi # --------------

/**
*	Function : RujukanDetailEditData_viKasirRadiologi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function RujukanDetailEditData_viKasirRadiologi()
{
	var rdJenisKunjungan_viKasirRadiologi = new Ext.form.RadioGroup
	({  
        fieldLabel: 'Cara Penerimaan',  
        columns: 2, 
        width: 250,
		border:false,
        items: 
		[  
			{
				boxLabel: 'Datang Senidri', 
				width:70,
				id: 'rdDatangSendiri_viKasirRadiologi', 
				name: 'rdDatangSendiri_viKasirRadiologi', 
				inputValue: '1'
			},  
			//-------------- ## --------------
			{
				boxLabel: 'Rujukan', 
				width:70,
				id: 'rdDatangRujukan_viKasirRadiologi', 
				name: 'rdDatangRujukan_viKasirRadiologi', 
				inputValue: '2'
			} 
			//-------------- ## --------------
        ]  
    });
	//-------------- ## --------------

	var itemsRujukanDetailEditData_viKasirRadiologi = 
	{
        xtype: 'panel',
        title: '',
        bodyStyle: 'padding: 5px 5px 5px 5px ;',
        width: 500,
        height: 100,
		border:true,
        items: 
		[			
			rdJenisKunjungan_viKasirRadiologi,
			{
				xtype: 'compositefield',
				fieldLabel: ' ',	
				labelAlign: 'Left',
				//labelSeparator: '',
				labelWidth:70,
				defaults: 
				{
					flex: 1
				},
				items: 
				[
					{
					   xtype: 'displayfield',
					   value: 'Dari:',
					   width:40
					},
					Vicbo_Dokter(90, "ComboDari_viKasirRadiologi"),
					//-------------- ## --------------
					{
					   xtype: 'displayfield',
					   value: 'Nama:',
					   width:40
					},
					Vicbo_Dokter(290, "ComboDariNama_viKasirRadiologi"),
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------
			{ 
				xtype: 'tbspacer',
				height: 5,
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Alamat',		
				labelAlign: 'Left',
				labelWidth:105,
				defaults: 
				{
					flex: 1
				},
				items: 
				[
					{
					   xtype: 'displayfield',
					   value: 'Alamat:',
					   width:40
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						anchor: '100%',
						width: 290,
						name: 'txtAlamatEditData_viKasirRadiologi',
						id: 'txtAlamatEditData_viKasirRadiologi',
						emptyText: 'Alamat',
					},
					//-------------- ## --------------
					{
					   xtype: 'displayfield',
					   value: 'Kota:',
					   width:40
					},
					Vicbo_Dokter(90, "ComboKotaEditData_viKasirRadiologi"),
					//-------------- ## --------------
				]
			}
			//-------------- ## --------------
        ]
    }
    return itemsRujukanDetailEditData_viKasirRadiologi;
};

// End Function RujukanDetailEditData_viKasirRadiologi # --------------

/**
*	Function : KeluhanPasienDetailEditData_viKasirRadiologi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function KeluhanPasienDetailEditData_viKasirRadiologi()
{
	var itemsKeluhanPasienDetailEditData_viKasirRadiologi = 
	{
        xtype: 'panel',
        title: '',
        //bodyStyle: 'padding: 5px;',
        width: 460,
        height: 100,
		border:false,
        items: 
		[			
			{ 
				xtype: 'tbspacer',
				height: 15,
			},
			//-------------- ## --------------
			{
				xtype: 'displayfield',				
				width: 200,								
				value: 'Keluhan Pasien:',
				fieldLabel: 'Label',
			},
			{
	            xtype: 'textarea',
	            fieldLabel: 'Keluhan Pasien',
	            id: 'txtKeluhanPasienEditData_viKasirRadiologi',
				name: 'txtKeluhanPasienEditData_viKasirRadiologi',
				width: 450,
				height: 50,
			},
			//-------------- ## --------------
        ]
    }
    return itemsKeluhanPasienDetailEditData_viKasirRadiologi;
};

// End Function KeluhanPasienDetailEditData_viKasirRadiologi # --------------

/**
*	Function : getItemGridTransaksi_viKasirRadiologi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viKasirRadiologi(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height:225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viKasirRadiologi()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viKasirRadiologi # --------------

/**
*	Function : gridDataViewEdit_viKasirRadiologi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viKasirRadiologi()
{
    
    chkSelected_viKasirRadiologi = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viKasirRadiologi',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viKasirRadiologi = 
	[
	];
	
    dsDataGrdJab_viKasirRadiologi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viKasirRadiologi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viKasirRadiologi,
        height: 220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viKasirRadiologi,
			{
				dataIndex: '',
				header: 'Code',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{			
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 250
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dok',
				sortable: true,
				width: 85,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Cd. Trf',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Harga/Unit',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'DR',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'CR',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
        ],
        plugins:chkSelected_viKasirRadiologi,
    }    
    return items;
}
// End Function gridDataViewEdit_viKasirRadiologi # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP HASIL RADIOLOGI ## ------------------------------------------------------------------

/**
*   Function : FormSetHasilRadiologi_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan windows popup Hasil Radiologi
*/

function FormSetHasilRadiologi_viKasirRadiologi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryHasilRadiologi_viKasirRadiologi = new Ext.Window
    (
        {
            id: 'FormGrdHasilRadiologi_viKasirRadiologi',
            title: 'Hasil Radiologi',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 365,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryHasilRadiologiGridDataView_viKasirRadiologi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryHasilRadiologi_viKasirRadiologi.show();
    mWindowGridLookupHasilRadiologi  = vWinFormEntryHasilRadiologi_viKasirRadiologi; 
};

// End Function FormSetHasilRadiologi_viKasirRadiologi # --------------

/**
*   Function : getFormItemEntryHasilRadiologiGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form hasil radiologi
*/
function getFormItemEntryHasilRadiologiGridDataView_viKasirRadiologi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataHasilRadiologiWindowPopup_viKasirRadiologi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataHasilRadiologiGridDataView_viKasirRadiologi(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiHasilRadiologiGridDataView_viKasirRadiologi(lebar),
				//-------------- ## --------------
				/*{ 
					xtype: 'tbspacer',
					height: 10,
				},
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Transaksi',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnTransaksiGridTransaksiHasilRadiologiGridDataView_viKasirRadiologi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Hasil Radiologi',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnHasilRadiologiGridTransaksiHasilRadiologiGridDataView_viKasirRadiologi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
					]
				},*/	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataHasilRadiologiWindowPopup_viKasirRadiologi;
}
// End Function getFormItemEntryHasilRadiologiGridDataView_viKasirRadiologi # --------------

/**
*   Function : getItemPanelInputBiodataHasilRadiologiGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form hasil radiologi
*/

function getItemPanelInputBiodataHasilRadiologiGridDataView_viKasirRadiologi(lebar) 
{
    var items =
    {
        //title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
								xtype: 'displayfield',				
								width: 72,								
								value: 'No. MedRec:',
							},
				            //-------------- ## --------------
				            {
								xtype: 'textfield',
								flex: 1,
								width : 80,	
								name: 'txtNoMedRecHasilRadiologi_viKasirRadiologi',
								id: 'txtNoMedRecHasilRadiologi_viKasirRadiologi',
								emptyText: 'No. MedRec',
								listeners:
								{
									'specialkey': function() 
									{
										if (Ext.EventObject.getKey() === 13) 
										{
										};
									}
								}
							},
							//-------------- ## --------------
							{
								xtype: 'displayfield',				
								width: 50,								
								value: 'Tanggal:',
							},
				            //-------------- ## --------------
				            {
								xtype: 'textfield',
								flex: 1,
								width : 30,	
								name: 'txtTglHasilRadiologi_viKasirRadiologi',
								id: 'txtTglHasilRadiologi_viKasirRadiologi',
								emptyText: '01',
							},
							{
								xtype: 'textfield',
								flex: 1,
								width : 30,	
								name: 'txtBlnHasilRadiologi_viKasirRadiologi',
								id: 'txtBlnHasilRadiologi_viKasirRadiologi',
								emptyText: '01',
							},
							{
								xtype: 'textfield',
								flex: 1,
								width : 50,	
								name: 'txtThnHasilRadiologi_viKasirRadiologi',
								id: 'txtThnHasilRadiologi_viKasirRadiologi',
								emptyText: '2014',
							},
							//-------------- ## --------------
							{ 
								xtype: 'tbspacer',
								width: 23,
								height: 25,
							},
							//-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 70,								
								value: 'Tgl. Lahir',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 50,								
								value: 'Thn',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 30,								
								value: 'Bln',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 30,								
								value: 'Hari',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 80,								
								value: 'Jenis Kelamin',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 65,								
								value: 'G. Darah',
							},
				            //-------------- ## --------------
				        ]
				    },
		        ]
		    },
			//-------------- ## --------------
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
								xtype: 'displayfield',				
								width: 72,								
								value: 'Nama Pasien:',
							},
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtPasienHasilRadiologi_viKasirRadiologi',
		                        emptyText: 'Nama Pasien',
		                        width: 285,
		                    },
		                    //-------------- ## -------------- 
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhirHasilRadiologi_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 30,
		                    },
		                    //-------------- ## -------------- 
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir2HasilRadiologi_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtThnLhirHasilRadiologi_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 50,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir3HasilRadiologi_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir4HasilRadiologi_viKasirRadiologi',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            viComboJenisTransaksi_MediSmart(80,'CmboJenisKelaminHasilRadiologi_viKasirRadiologi'),
				            //-------------- ## --------------
				            viComboJenisTransaksi_MediSmart(60,'CmboGlgnDrhHasilRadiologi_viKasirRadiologi'),
				            //-------------- ## --------------
				            { 
								xtype: 'tbspacer',
								height: 25,
							},
							//-------------- ## --------------
				        ]
				    },
		        ]
		    },
			//-------------- ## --------------
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [
					{
						xtype: 'compositefield',
						fieldLabel: '',
						items: 
						[
							{
								xtype: 'displayfield',				
								width: 72,								
								value: 'Dokter:',
							},
							{
								xtype: 'textfield',					
								fieldLabel: '',								
								anchor: '100%',
								width: 260,
								name: 'txtDokterHasilRadiologi_viKasirRadiologi',
								id: 'txtDokterHasilRadiologi_viKasirRadiologi',
								emptyText: 'Dokter',
							},
							//-------------- ## --------------
							{
								xtype: 'displayfield',				
								width: 52,								
								value: 'Alamat:',
							},
							{
								xtype: 'textfield',					
								fieldLabel: '',								
								anchor: '100%',
								width: 308,
								name: 'txtAlamatHasilRadiologi_viKasirRadiologi',
								id: 'txtAlamatHasilRadiologi_viKasirRadiologi',
								emptyText: 'Alamat',
							},
							//-------------- ## --------------
						]
					},
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataHasilRadiologiGridDataView_viKasirRadiologi # --------------

/**
*   Function : getItemGridTransaksiHasilRadiologiGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form hasil radiologi
*/

function getItemGridTransaksiHasilRadiologiGridDataView_viKasirRadiologi(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:true,
        width: lebar-80,
        height:225, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditHasilRadiologiGridDataView_viKasirRadiologi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiHasilRadiologiGridDataView_viKasirRadiologi # --------------

/**
*   Function : gridDataViewEditHasilRadiologiGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form hasil radiologi
*/

function gridDataViewEditHasilRadiologiGridDataView_viKasirRadiologi()
{
    
    var FieldGrdKasirHasilRadiologiGridDataView_viKasirRadiologi = 
    [
    ];
    
    dsDataGrdJabHasilRadiologiGridDataView_viKasirRadiologi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirHasilRadiologiGridDataView_viKasirRadiologi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabHasilRadiologiGridDataView_viKasirRadiologi,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kd. Test',
                sortable: true,
                width: 90
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Pemeriksaan',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {
                dataIndex: '',
                header: 'Hasil Pemeriksaan',
                sortable: true,
                width: 400,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditHasilRadiologiGridDataView_viKasirRadiologi # --------------

// ## END MENU LOOKUP - LOOKUP HASIL RADIOLOGI ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupGantiDokter_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupGantiDokter_viKasirRadiologi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_viKasirRadiologi = new Ext.Window
    (
        {
            id: 'FormGrdLookupGantiDokter_viKasirRadiologi',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupGantiDokter_viKasirRadiologi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_viKasirRadiologi.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_viKasirRadiologi; 
};

// End Function FormSetLookupGantiDokter_viKasirRadiologi # --------------

/**
*   Function : getFormItemEntryLookupGantiDokter_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupGantiDokter_viKasirRadiologi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataGantiDokterWindowPopup_viKasirRadiologi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputGantiDokterDataView_viKasirRadiologi(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataGantiDokterWindowPopup_viKasirRadiologi;
}
// End Function getFormItemEntryLookupGantiDokter_viKasirRadiologi # --------------

/**
*   Function : getItemPanelInputGantiDokterDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputGantiDokterDataView_viKasirRadiologi(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Tanggal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtTglGantiDokter_viKasirRadiologi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtBlnGantiDokter_viKasirRadiologi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtThnGantiDokter_viKasirRadiologi',
                        emptyText: '2014',
                        width: 50,
                    },
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Asal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboDokterAsal_viKasirRadiologi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Ganti',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboDokterGanti_viKasirRadiologi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputGantiDokterDataView_viKasirRadiologi # --------------

// ## END MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP TRANSAKSI LAMA ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupTransaksiLama_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan windows popup transaksi lama
*/

function FormSetLookupTransaksiLama_viKasirRadiologi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryLookupTransaksiLama_viKasirRadiologi = new Ext.Window
    (
        {
            id: 'FormGrdLookupTransaksiLama_viKasirRadiologi',
            title: 'Buka Transaksi Lama',
            closeAction: 'destroy',
            closable:true,
            width: 500,
            height: 175,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryTransaksiLama_viKasirRadiologi(subtotal,NO_KUNJUNGAN),
                //-------------- ## --------------
            ],
        }
    );
    vWinFormEntryLookupTransaksiLama_viKasirRadiologi.show();
    mWindowGridLookupLookupTransaksiLama  = vWinFormEntryLookupTransaksiLama_viKasirRadiologi; 
}
// End Function FormSetLookupTransaksiLama_viKasirRadiologi # --------------

/**
*   Function : getFormItemEntryTransaksiLama_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form produk
*/
function getFormItemEntryTransaksiLama_viKasirRadiologi(subtotal,NO_KUNJUNGAN)
{
	var lebar = 515;
	var itemsEntryTransaksiLama_viKasirRadiologi = 
    {
        xtype: 'panel',
        title: '',
        name: 'PanelListProdukGridDataView_viKasirRadiologi',
        id: 'PanelListProdukGridDataView_viKasirRadiologi',        
        bodyStyle: 'padding: 15px;',
        //height: 1170,
        border:false,
        items: 
        [           
            {
	            xtype: 'radiogroup',
	            //fieldLabel: 'Single Column',
	            itemCls: 'x-check-group-alt',
	            columns: 1,
	            items: 
	            [
	                {
	                	boxLabel: 'Transaksi Berjalan', 
	                	name: 'ckboxTreeTransaksiBerjalan_viKasirRadiologi',
	                	id: 'ckboxTreeTransaksiBerjalan_viKasirRadiologi',  
	                	inputValue: 1,
	                },
	                //-------------- ## -------------- 
	                {
	                	boxLabel: 'Transaksi Lama', 
	                	name: 'ckboxTreeTransaksiLama_viKasirRadiologi',
	                	id: 'ckboxTreeTransaksiLama_viKasirRadiologi',  
	                	inputValue: 2, 
	                	checked: true,
	                },
	                //-------------- ## -------------- 
	            ]
	        },
			//-------------- ## --------------
			{
                xtype: 'fieldset',
                title: '',
                frame: false,
                items: 
                [
	                {
				        xtype: 'panel',
				        title: '',
				        border:false,
				        items: 
				        [
			                {
								xtype: 'compositefield',
								fieldLabel: ' ',
								labelSeparator: '',
								anchor: '100%',
								width: 500,
								items: 
								[
				                    {
										xtype: 'displayfield',
										flex: 1,
										width: 80,
										name: '',
										value: 'Dari Tanggal:',
									},
				                    {
										xtype: 'datefield',
										fieldLabel: 'Tanggal',
										id: 'DateDariTransaksiLama_viKasirRadiologi',
										name: 'DateDariTransaksiLama_viKasirRadiologi',
										width: 130,
										format: 'd/M/Y',
									},
				                    //-------------- ## --------------
				                    {
										xtype: 'displayfield',
										flex: 1,
										width: 80,
										name: '',
										value: 's/d Tanggal:',
									},
				                    {
										xtype: 'datefield',
										fieldLabel: 'Tanggal',
										id: 'DateSmpDgnTransaksiLama_viKasirRadiologi',
										name: 'DateSmpDgnTransaksiLama_viKasirRadiologi',
										width: 130,
										format: 'd/M/Y',
									},
				                    //-------------- ## --------------
				                ],
				            },
				            //-------------- ## --------------
				        ],
				    },
				    //-------------- ## --------------
                ],
            },
            //-------------- ## --------------
            {
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [
		            {
						xtype: 'compositefield',
						fieldLabel: ' ',
						labelSeparator: '',
						anchor: '100%',
						width: 500,
						items: 
						[
				            {
				                xtype:'button',
				                text:'Ok',
				                width:70,
				                style:{'margin-left':'300px'},
				                hideLabel:true,
				                id: 'btnOkTransaksiLama_viKasirRadiologi',
				                handler:function()
				                {
				                }   
				            },
				            //-------------- ## --------------
				            {
				                xtype:'button',
				                text:'Cancel',
				                width:70,
				                style:{'margin-left':'304px'},
				                hideLabel:true,
				                id: 'btnCancelTransaksiLama_viKasirRadiologi',
				                handler:function()
				                {
				                }   
				            },
				            //-------------- ## --------------
				        ],
				    },
				    //-------------- ## --------------
				]
			},
			//-------------- ## --------------
        ]
    }
    return itemsEntryTransaksiLama_viKasirRadiologi;
}
// End Function getFormItemEntryTransaksiLama_viKasirRadiologi # --------------

// ## END MENU LOOKUP - LOOKUP TRANSAKSI LAMA ## ------------------------------------------------------------------

// ## END MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

/**
*   Function : FormSetPembayaran_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan windows popup Pembayaran
*/

function FormSetPembayaran_viKasirRadiologi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPembayaran_viKasirRadiologi = new Ext.Window
    (
        {
            id: 'FormGrdPembayaran_viKasirRadiologi',
            title: 'Pembayaran',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryGridDataView_viKasirRadiologi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPembayaran_viKasirRadiologi.show();
    mWindowGridLookupPembayaran  = vWinFormEntryPembayaran_viKasirRadiologi; 
};

// End Function FormSetPembayaran_viKasirRadiologi # --------------

/**
*   Function : getFormItemEntryGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function getFormItemEntryGridDataView_viKasirRadiologi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataWindowPopup_viKasirRadiologi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataGridDataView_viKasirRadiologi(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiGridDataView_viKasirRadiologi(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnOkGridTransaksiPembayaranGridDataView_viKasirRadiologi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnCancelGridTransaksiPembayaranGridDataView_viKasirRadiologi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtBiayaGridTransaksiPembayaranGridDataView_viKasirRadiologi',
                            name: 'txtBiayaGridTransaksiPembayaranGridDataView_viKasirRadiologi',
							style:{'text-align':'right','margin-left':'150px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtPiutangGridTransaksiPembayaranGridDataView_viKasirRadiologi',
                            name: 'txtPiutangGridTransaksiPembayaranGridDataView_viKasirRadiologi',
							style:{'text-align':'right','margin-left':'146px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtTunaiGridTransaksiPembayaranGridDataView_viKasirRadiologi',
                            name: 'txtTunaiGridTransaksiPembayaranGridDataView_viKasirRadiologi',
							style:{'text-align':'right','margin-left':'142px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtDiscGridTransaksiPembayaranGridDataView_viKasirRadiologi',
                            name: 'txtDiscGridTransaksiPembayaranGridDataView_viKasirRadiologi',
							style:{'text-align':'right','margin-left':'138px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataWindowPopup_viKasirRadiologi;
}
// End Function getFormItemEntryGridDataView_viKasirRadiologi # --------------

/**
*   Function : getItemPanelInputBiodataGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemPanelInputBiodataGridDataView_viKasirRadiologi(lebar) 
{
    var items =
    {
        title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
				xtype: 'textfield',
				fieldLabel: 'No. Transaksi',
				id: 'txtNoTransaksiPembayaranGridDataView_viKasirRadiologi',
				name: 'txtNoTransaksiPembayaranGridDataView_viKasirRadiologi',
				emptyText: 'No. Transaksi',
				readOnly: true,
				flex: 1,
				width: 195,				
			},
			//-------------- ## --------------
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				id: 'txtNamaPasienPembayaranGridDataView_viKasirRadiologi',
				name: 'txtNamaPasienPembayaranGridDataView_viKasirRadiologi',
				emptyText: 'Nama Pasien',
				flex: 1,
				anchor: '100%',					
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran',
				anchor: '100%',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_MediSmart(195,'CmboPembayaranGridDataView_viKasirRadiologi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				anchor: '100%',
				labelSeparator: '',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_MediSmart(195,'CmboPembayaranDuaGridDataView_viKasirRadiologi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataGridDataView_viKasirRadiologi # --------------

/**
*   Function : getItemGridTransaksiGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemGridTransaksiGridDataView_viKasirRadiologi(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar-80,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridDataView_viKasirRadiologi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiGridDataView_viKasirRadiologi # --------------

/**
*   Function : gridDataViewEditGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridDataViewEditGridDataView_viKasirRadiologi()
{
    
    var FieldGrdKasirGridDataView_viKasirRadiologi = 
    [
    ];
    
    dsDataGrdJabGridDataView_viKasirRadiologi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viKasirRadiologi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viKasirRadiologi,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kode',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Qty',
                sortable: true,
                width: 40
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Biaya',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Piutang',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tunai',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Disc',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridDataView_viKasirRadiologi # --------------

// ## END MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

/**
*   Function : FormSetTransferPembayaran_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan windows popup Transfer ke Rawat Inap
*/

function FormSetTransferPembayaran_viKasirRadiologi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryTransferPembayaran_viKasirRadiologi = new Ext.Window
    (
        {
            id: 'FormGrdTrnsferPembayaran_viKasirRadiologi',
            title: 'Pemindahan Tagihan Ke Unit Rawat Inap',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 450,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormTransferPembayaranItemEntryGridDataView_viKasirRadiologi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryTransferPembayaran_viKasirRadiologi.show();
    mWindowGridLookupTransferPembayaran  = vWinFormEntryTransferPembayaran_viKasirRadiologi; 
};

// End Function FormSetTransferPembayaran_viKasirRadiologi # --------------

/**
*   Function : getFormTransferPembayaranItemEntryGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form transfer ke rawat inap
*/
function getFormTransferPembayaranItemEntryGridDataView_viKasirRadiologi(subtotal,NO_KUNJUNGAN)
{
    var pnlFormTransferPembayaranDataWindowPopup_viKasirRadiologi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            items:
            //-------------- #items# --------------
            [
                {
                    xtype: 'fieldset',
                    title: 'Tagihan dipindahkan ke',
                    frame: false,
                    width: 350,
                    height: 165,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Transaksi',
                            id: 'txtNoTransaksiTransaksiTransferPembayaran_viKasirRadiologi',
                            name: 'txtNoTransaksiTransaksiTransferPembayaran_viKasirRadiologi',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
							xtype: 'compositefield',
							fieldLabel: 'Tgl. Lahir',
							anchor: '100%',
							width: 130,
							items: 
							[
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'DateTransaksiTransferPembayaran_viKasirRadiologi',
									name: 'DateTransaksiTransferPembayaran_viKasirRadiologi',
									width: 130,
									format: 'd/M/Y',
								}
								//-------------- ## --------------				
							]
						},
						//-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Rekam Medis',
                            id: 'txtNoRekamMedisTransaksiTransferPembayaran_viKasirRadiologi',
                            name: 'txtNoRekamMedisTransaksiTransferPembayaran_viKasirRadiologi',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nama Pasien',
                            id: 'txtNamaPasienTransaksiTransferPembayaran_viKasirRadiologi',
                            name: 'txtNamaPasienTransaksiTransferPembayaran_viKasirRadiologi',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Unit Perawatan',
                            id: 'txtUnitPerawatanTransaksiTransferPembayaran_viKasirRadiologi',
                            name: 'txtUnitPerawatanTransaksiTransferPembayaran_viKasirRadiologi',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: '',
                    frame: false,
                    width: 350,
                    height: 125,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jumlah biaya',
                            id: 'txtJumlhBiayaTransaksiTransferPembayaran_viKasirRadiologi',
                            name: 'txtJumlhBiayaTransaksiTransferPembayaran_viKasirRadiologi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'PAID',
                            id: 'txtPAIDTransaksiTransferPembayaran_viKasirRadiologi',
                            name: 'txtPAIDTransaksiTransferPembayaran_viKasirRadiologi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jmlh dipindahkan',
                            id: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viKasirRadiologi',
                            name: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viKasirRadiologi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Saldo Tagihan',
                            id: 'txtSaldoTagihanTransaksiTransferPembayaran_viKasirRadiologi',
                            name: 'txtSaldoTagihanTransaksiTransferPembayaran_viKasirRadiologi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: 'Alasan Transfer',
                    frame: false,
                    width: 350,
                    height: 62,
                    items: 
                    [
						{
					        xtype: 'panel',
					        title: '',
					        border:false,
					        items: 
					        [           
					            viComboJenisTransaksi_MediSmart(320,'CmboAlasanTransfer_viKasirRadiologi'),
					            //-------------- ## -------------- 
					        ]
					    }
                    ]
                },
                //-------------- ## --------------
                {
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					width: 160,
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnOkTransferPembayaran_viKasirRadiologi',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
						{
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnCancelTransferPembayaran_viKasirRadiologi',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
					]
				},
                //-------------- ## --------------
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormTransferPembayaranDataWindowPopup_viKasirRadiologi;
}
// End Function getFormTransferPembayaranItemEntryGridDataView_viKasirRadiologi # --------------

// ## END MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

/**
*   Function : FormSetEditTarif_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan windows popup Edit Tarif
*/

function FormSetEditTarif_viKasirRadiologi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryEditTarif_viKasirRadiologi = new Ext.Window
    (
        {
            id: 'FormGrdEditTarif_viKasirRadiologi',
            title: 'Edit Tarif',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryEditTarifGridDataView_viKasirRadiologi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryEditTarif_viKasirRadiologi.show();
    mWindowGridLookupEditTarif  = vWinFormEntryEditTarif_viKasirRadiologi; 
};

// End Function FormSetEditTarif_viKasirRadiologi # --------------

/**
*   Function : getFormItemEntryEditTarifGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/
function getFormItemEntryEditTarifGridDataView_viKasirRadiologi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataEditTarifWindowPopup_viKasirRadiologi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukEditTarifGridDataView_viKasirRadiologi(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiEditTarifGridDataView_viKasirRadiologi(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiEditTarifGridDataView_viKasirRadiologi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiEditTarifGridDataView_viKasirRadiologi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viKasirRadiologi',
                            name: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viKasirRadiologi',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viKasirRadiologi',
                            name: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viKasirRadiologi',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataEditTarifWindowPopup_viKasirRadiologi;
}
// End Function getFormItemEntryEditTarifGridDataView_viKasirRadiologi # --------------

/**
*   Function : getItemPanelInputProdukEditTarifGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemPanelInputProdukEditTarifGridDataView_viKasirRadiologi(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukEditTarifGridDataView_viKasirRadiologi',
                name: 'txtProdukEditTarifGridDataView_viKasirRadiologi',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukEditTarifGridDataView_viKasirRadiologi # --------------

/**
*   Function : getItemGridTransaksiEditTarifGridDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemGridTransaksiEditTarifGridDataView_viKasirRadiologi(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridEditTarifDataView_viKasirRadiologi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiEditTarifGridDataView_viKasirRadiologi # --------------

/**
*   Function : gridDataViewEditGridEditTarifDataView_viKasirRadiologi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function gridDataViewEditGridEditTarifDataView_viKasirRadiologi()
{
    
    var FieldGrdKasirGridDataView_viKasirRadiologi = 
    [
    ];
    
    dsDataGrdJabGridDataView_viKasirRadiologi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viKasirRadiologi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viKasirRadiologi,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridEditTarifDataView_viKasirRadiologi # --------------

// ## END MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

// ## END MENU PEMBAYARAN ## ------------------------------------------------------------------

// --------------------------------------- # End Form # ---------------------------------------

// End Project Kasir Radiologi # --------------