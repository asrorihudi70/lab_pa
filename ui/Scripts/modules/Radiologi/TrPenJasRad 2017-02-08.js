var CurrentPenJasRad ={
    data: Object,
    details: Array,
    row: 0
};

var mRecordRwj = Ext.data.Record.create([
   {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
   {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
   {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
   {name: 'KD_TARIF', mapping:'KD_TARIF'},
   {name: 'HARGA', mapping:'HARGA'},
   {name: 'QTY', mapping:'QTY'},
   {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
   {name: 'DESC_REQ', mapping:'DESC_REQ'},
   {name: 'URUT', mapping:'URUT'}
]);
var Trkdunit2;
var CurrentHistory ={
	data: Object,
	details: Array,
	row: 0
};
var tmpkd_unit = '';
var kodeunit;
var tmp_kodeunitkamar_RAD;
var	tmp_kdspesial_RAD;
var tmp_nokamar_RAD;
var tmplunas;
var kodeunittransfer;
var dsTRDetailHistoryList_rad;
var kodepasien;
var namapasien;
var namaunit;
var kodepay ;
var kdkasir;
var uraianpay;
var mRecordKasirRAD = Ext.data.Record.create([
	{name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
	{name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
	{name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
	{name: 'KD_TARIF', mapping: 'KD_TARIF'},
	{name: 'HARGA', mapping: 'HARGA'},
	{name: 'QTY', mapping: 'QTY'},
	{name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
	{name: 'DESC_REQ', mapping: 'DESC_REQ'},
	{name: 'URUT', mapping: 'URUT'}
]);

var AddNewKasirRADKasir = true;
var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var kdpaytransfer = 'T1';
var kdkasir;
var tanggaltransaksitampung;
var kdkasirasal_rad;
var notransaksiasal_rad;
var kdcustomeraa;
var vflag;
var tmp_NoTransaksi;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var tampungtypedata;
var jenispay;
var tapungkd_pay;
var tranfertujuan = 'Rawat Inap';
var cellSelecteddeskripsi;
var vkd_unit;
var notransaksi;
var noTransaksiPilihan;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();
var tigaharilalu = new Date().add(Date.DAY, -3);
var nowTglTransaksiGrid = new Date();
var tglGridBawah = nowTglTransaksiGrid.format("d/M/Y");

var tigaharilaluNew = tigaharilalu.format("d/M/Y");

var gridDTItemTest;

var selectSetDr;
var selectSetGDarah;
var selectSetJK;
var selectSetKelPasien;
var selectSetPerseorangan;
var selectSetPerusahaan;
var selectSetAsuransi;

var radelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwj='Belum Posting';
var selectCountStatusLunasByr_viKasirRwj='Belum Lunas';
var selectCountJenTr_viPenJasRad='Transaksi Baru';
var dsTRPenJasRadList;
var dsTRDetailPenJasRadList;
var AddNewPenJasRad = true;
var selectCountPenJasRad = 50;
var TmpNotransaksi='';
var KdKasirAsal='';
var TglTransaksi='';
var databaru = 0;
var No_Kamar='';
var Kd_Spesial=0;
var kodeUnitRad;
var dsPeroranganRadRequestEntry;

var rowSelectedPenJasRad;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRrwj;
var valueStatusCMRWJView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
var tmpparams = '';
var grListPenJasRad;

//VALIDASI JENIS TRANSAKSI
var combovalues = 'Transaksi Baru';
var radiovalues = '1';
var combovaluesunit = "";
var combovaluesunittujuan = "";
var ComboValuesKamar = "";

var vkode_customer;
CurrentPage.page = getPanelPenJasRad(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
var tmpurut;

//membuat form

function getPanelPenJasRad(mod_id) 
{
    var Field = ['KD_PASIEN','NO_TRANSAKSI','NAMA','ALAMAT','SPESIALISASI','KELAS','NAMA_KAMAR','KAMAR','MASUK','NO_TRANSAKSI_ASAL','KD_KASIR_ASAL',
				 'DOKTER','KD_DOKTER','KD_UNIT_KAMAR','KD_CUSTOMER','CUSTOMER','TGL_MASUK','URUT_MASUK','TGL_INAP','KD_SPESIAL','KD_KASIR','TGL_TRANSAKSI','KD_UNIT',
				 'CO_STATUS','KD_USER','TGL_LAHIR','JENIS_KELAMIN','GOL_DARAH','POSTING_TRANSAKSI', 
				 'NAMA_UNIT','POLIKLINIK','NAMA_UNIT_ASLI','KELPASIEN','LUNAS','DOKTER_ASAL','KD_DOKTER_ASAL','URUT','NAMA_UNIT_ASAL','KD_UNIT_ASAL','HP','SJP','NO_FOTO'];
    dsTRPenJasRadList = new WebApp.DataStore({ fields: Field });
	
     var k="tr.tgl_transaksi >='"+tigaharilaluNew+"' and tr.tgl_transaksi <='"+tglGridBawah+"'    and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc";
	//---------------------EDIT DEFAULT UNIT RAD 31 01 2017
	
    refeshpenjasrad(k);

    grListPenJasRad = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
			id:'PenjasRad',
            store: dsTRPenJasRadList,
            anchor: '100% 50%',
            columnLines: false,
            autoScroll:true,
            border: false,
			sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedPenJasRad = dsTRPenJasRadList.getAt(row);
                            
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedPenJasRad = dsTRPenJasRadList.getAt(ridx);
					noTransaksiPilihan = rowSelectedPenJasRad.data.NO_TRANSAKSI;
                    if (rowSelectedPenJasRad !== undefined)
                    {
                        // console.log(rowSelectedPenJasRad.data);
                        PenJasRadLookUp(rowSelectedPenJasRad.data);
                    }
                    else
                    {
                        PenJasRadLookUp();
						Ext.getCmp('cboDOKTER_viPenJasRad').disable();
						Ext.getCmp('cboGDR').disable();
						Ext.getCmp('cboJK').disable();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   
                    {
                        id: 'colReqIdViewRWJ',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80
                    },
                    {
                        id: 'colTglRWJViewRWJ',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TGL_TRANSAKSI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TGL_TRANSAKSI);

							}
                    },
					{
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colRWJerViewRWJ'
                    },
					{
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colRWJerViewRWJ'
                    },
                    {
                        id: 'colLocationViewRWJ',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewRWJ',
                        header: 'Dokter',
                        dataIndex: 'DOKTER',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colImpactViewRWJ',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT_ASLI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 90
                    },
					{
                        header: 'Status Transaksi',
                        width: 100,
                        sortable: false,
                        hideable:true,
                        hidden:true,
                        menuDisabled:true,
                        dataIndex: 'POSTING_TRANSAKSI',
                        id: 'txtposting',
						renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
						
                    },
					{
                        id: 'colCoSTatusViewRWJ',
                        header: 'Status Lunas',
                        dataIndex: 'LUNAS',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80,
						renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
								case '':
                                         metaData.css = 'StatusMerah'; // 
                                         break;
                                 case undefined:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
                    {
                        id: 'colnofotViewrad',
                        header: 'No. Foto',
                        dataIndex: 'NO_FOTO',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 100
                    }
                   
                ]
            ),
            viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditRWJ',
                        text: 'Lookup',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedPenJasRad != undefined)
                            {
								// console.log(rowSelectedPenJasRad.data);
								
                                    PenJasRadLookUp(rowSelectedPenJasRad.data);
                            }
                            else
                            {
							ShowPesanWarningPenJasRad('Pilih data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },
                    {
                        id: 'btnUpdateFotoRad',
                        text: 'Update Foto',
                        tooltip: nmEditData,
                        // disable: true,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            // PenJasRadUpdateFotoLookUp();
                            if (Ext.get('cboJenisTr_viPenJasRad').getValue() === 'Transaksi Lama' && rowSelectedPenJasRad.data.LUNAS === 't') {
                                PenJasRadUpdateFotoLookUp(rowSelectedPenJasRad.data);
                            }else if(Ext.get('cboJenisTr_viPenJasRad').getValue() === 'Transaksi Lama' && rowSelectedPenJasRad.data.LUNAS === 'f'){
                                ShowPesanWarningPenJasRad('Transaksi Belum Selesai');
                            }else if (Ext.get('cboJenisTr_viPenJasRad').getValue() === 'Transaksi Baru' && rowSelectedPenJasRad.data.LUNAS === 'f'){
                                ShowPesanWarningPenJasRad('Blm Ada Transaksi Atas Pasien Tersebut');
                            }else{
                                ShowPesanWarningPenJasRad('Silahkan Cek Data Kembali');
                            }
                        }
                    },
                    {
                        id: 'btnBatalTransaksiRad',
                        text: 'Batal Transaksi',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            // console.log(rowSelectedPenJasRad.data);
                            var tmpdatatransaksi = rowSelectedPenJasRad.data;
                            if (Ext.get('cboJenisTr_viPenJasRad').getValue() === 'Transaksi Lama') {
                                if (rowSelectedPenJasRad === undefined) {
                                    ShowPesanWarningPenJasRad('Silahkan Pilih Data Terlebih Dahulu');
                                }else{
                                    Ext.Msg.confirm('Warning', 'Apakah Transaksi ini akan dihapus?', function(button){
                                        if (button == 'yes'){
                                            if(rowSelectedPenJasRad != undefined) {
                                                var params = {
                                                    kd_unit: tmpdatatransaksi.KD_UNIT,
                                                    Tglkunjungan: tmpdatatransaksi.TGL_TRANSAKSI,
                                                    Kodepasein: tmpdatatransaksi.KD_PASIEN,
                                                    urut: tmpdatatransaksi.URUT
                                                };
                                                Ext.Ajax.request
                                                        ({
                                                            url: baseURL + "index.php/main/functionRWJ/deletekunjungan",
                                                            params: params,
                                                            failure: function (o)
                                                            {
                                                                loadMask.hide();
                                                                // console.log(o);
                                                                ShowPesanWarningPenJasRad('Error proses database', 'Hapus Data');
                                                            },
                                                            success: function (o)
                                                            {
                                                                var cst = Ext.decode(o.responseText);
                                                                if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === false)
                                                                {
                                                                    ShowPesanWarningPenJasRad('Data transaksi tidak berhasil ditemukan', 'Hapus Data Kunjungan');
                                                                } else if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === true)
                                                                {
                                                                    ShowPesanWarningPenJasRad('Anda telah melakukan pembayaran', 'Hapus Data Kunjungan');
                                                                } else if (cst.success === true)
                                                                {
                                                                    // RefreshDatahistoribayar(datapasienvariable.kd_pasien);
                                                                    ShowPesanInfoPenJasRad('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
                                                                    validasiJenisTr();
                                                                } else if (cst.success === false && cst.pesan === 0)
                                                                {
                                                                    ShowPesanWarningPenJasRad('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                                } else
                                                                {
                                                                    if (cst.kd_kasir) {
                                                                        ShowPesanWarningPenJasRad('Anda tidak diberikan hak akses untuk menghapus data kunjungan '+cst.kd_kasir, 'Hapus Data Kunjungan');
                                                                    }else{
                                                                        ShowPesanErrorPenJasRad('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                                    }
                                                                };
                                                            }})
                                            }
                                        }
                                    })
                                }
                            }else {

                            }
                        }
                    }
                ]
            }
	);
	
	
	//form depan awal dan judul tab
    var FormDepanPenJasRad = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Penata Jasa Radiologi',
            border: false,
            shadhow: true,
            autoScroll:false,
			width: '100%',
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
                        getItemPanelPenJasRadAwal(),
                        grListPenJasRad
                   ],
            listeners:
            {
                'afterrender': function()
                {
					Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
					Ext.getCmp('cboStatusLunas_viPenJasRad').disable();
                    Ext.getCmp('btnBatalTransaksiRad').disable();
				}
            }
        }
    );
	
   return FormDepanPenJasRad;

};


function mComboStatusBayar_viPenJasRad()
{
  var cboStatus_viPenJasRad = new Ext.form.ComboBox
	(
		{
                    id:'cboStatus_viPenJasRad',
                    x: 155,
                    y: 70,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusByr_viKasirRwj,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusByr_viKasirRwj=b.data.displayText ;
                            }
                    }
		}
	);
	return cboStatus_viPenJasRad;
};
function mComboStatusLunas_viPenJasRad()
{
  var cboStatusLunas_viPenJasRad = new Ext.form.ComboBox
	(
		{
                    id:'cboStatusLunas_viPenJasRad',
                    x: 155,
                    y: 70,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Lunas'], [3, 'Belum Lunas']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusLunasByr_viKasirRwj,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusLunasByr_viKasirRwj=b.data.displayText ;
                            }
                    }
		}
	);
	return cboStatusLunas_viPenJasRad;
};

//COMBO JENIS TRANSAKSI
function mComboJenisTrans_viPenJasRad()
{
  var cboJenisTr_viPenJasRad = new Ext.form.ComboBox
	(
		{
                    id:'cboJenisTr_viPenJasRad',
                    x: 155,
                    y: 10,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS TRANSAKSI',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Transaksi Baru'],[2, 'Transaksi Lama']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountJenTr_viPenJasRad,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
								combovalues=b.data.displayText;
								if(b.data.Id==1){
									Ext.getCmp('cbounitrads_viPenJasRad').setValue(null);
									Ext.getCmp('cbounitrads_viPenJasRad').disable();
                                    Ext.getCmp('btnBatalTransaksiRad').disable();
                                    tmppasienbarulama = 'Baru';
								}else{
									Ext.getCmp('cbounitrads_viPenJasRad').enable();
									Ext.getCmp('cbounitrads_viPenJasRad').setValue(combovaluesunittujuan);
                                    tmppasienbarulama = 'Lama';
                                    Ext.getCmp('btnBatalTransaksiRad').enable();
								}
								
                                validasiJenisTr();
								
                            }
                    }
		}
	);
	return cboJenisTr_viPenJasRad;
};

var tmppasienbarulama = 'Baru';
function mComboUnitTujuans_viPenJasRad(){
	var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitrads_viPenJasRad = new WebApp.DataStore({ fields: Field });
    dsunitrads_viPenJasRad.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama_unit',
			Sortdir: 'ASC',
			target: 'ViewSetupUnit',
			param: "parent = '5'"
		}
	});
    var cbounitrads_viPenJasRad = new Ext.form.ComboBox({
		id: 'cbounitrads_viPenJasRad',
		x: 455,
		y: 10,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		disabled:true,
		fieldLabel:  ' ',
		align: 'Right',
		width: 230,
		emptyText:'Pilih Unit',
		store: dsunitrads_viPenJasRad,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		//value:'All',
		editable: false,
		listeners:{
			'select': function(a,b,c){
				combovaluesunittujuan=b.data.KD_UNIT;
				validasiJenisTr();
			}
		}
	});
    return cbounitrads_viPenJasRad;
	
};


function validasiJenisTr(){
    var kriteria = "";
	var strkriteria=getCriteriaFilter_viDaftar();
    if (combovalues === 'Transaksi Lama')
    {
        Ext.getCmp('cboStatusLunas_viPenJasRad').enable();
    
        if(radiovalues === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarwjigd = "";
                }else
                {
					tmpkreteriarwjigd = "and kd_unit_asal = '" + Ext.getCmp('cboUNIT_viKasirRwj').getValue() + "'";
                }
            }else {
                tmpkreteriarwjigd = "";
            }
			
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianamarwi = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianamarwi = "";
            }
			
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrecrwi = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrecrwi = "";
            }
			if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
				tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
			}else{
				tmpkriteriaunittujuancrwi="";
			}
			/* if(selectCountStatusByr_viKasirRwj !== "")
            {
                if(selectCountStatusByr_viKasirRwj === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirRwj === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
			if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
			
            kriteria = " kd_bagian = 5 and left(kd_unit_asal, 1) in('3') and left(kd_pasien, 2) not in ('RD') "+tmpkriteriaunittujuancrwi+" "+ tmpkreteriarwjigd +" "+ tmpkriterianamarwi +" "+ tmpkriteriamedrecrwi +" "+tmplunas+" "+strkriteria+"   ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit);
            
        }else if (radiovalues === '2'){   
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and kd_unit_asal = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else
            {
                tmpkriteriakamar = "";
            }
			
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
			
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
			if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
				tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
			}else{
				tmpkriteriaunittujuancrwi="";
			}
			/* if(selectCountStatusByr_viKasirRwj !== "")
            {
                if(selectCountStatusByr_viKasirRwj === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirRwj === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
			if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            kriteria = " kd_bagian = 5 "+tmpkriteriaunittujuancrwi+" "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriamedrec +"  "+tmplunas+" and left(kd_pasien, 2) not in ('RD')   and left(kd_unit_asal, 1) = '1'  "+strkriteria+" ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit);
        }else if (radiovalues === '3')
        {
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
			
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
			if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
				tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
			}else{
				tmpkriteriaunittujuancrwi="";
			}
			/* if(selectCountStatusByr_viKasirRwj !== "")
            {
                if(selectCountStatusByr_viKasirRwj === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirRwj === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
			if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = " and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = " and lunas='f' ";
                }else
                {
                    tmplunas = "";
                }
            }
            kriteria = " kd_bagian = 5 "+tmpkriteriaunittujuancrwi+" "+ tmpkriterianama +""+ tmpkriteriamedrec +"  and left(kd_pasien, 2) = 'RD' and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+tmplunas+"   ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasRadKunjunganLangsung';
            loadpenjasrad(kriteria, tmpunit);
        
		}else if (radiovalues === '4')
        {
			if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
			
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
			if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
				tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
			}else{
				tmpkriteriaunittujuancrwi="";
			}
			/* if(selectCountStatusByr_viKasirRwj !== "")
            {
                if(selectCountStatusByr_viKasirRwj === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirRwj === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
			if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                   tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }//"+tmplunas+"
			kriteria = " kd_bagian = 5 and left(kd_unit_asal, 1) in('2') "+tmpkriteriaunittujuancrwi+" "+tmplunas+"  and left(kd_pasien, 2) not in ('RD')  and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "'  ORDER BY tgl_transaksi desc, no_transaksi";
			tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit);
		}else
        {
            kriteria = "posting_transaksi = 'f'  and kd_bagian = 5 and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit);
        }
		
    }else if (combovalues === 'Transaksi Baru')
    {
        Ext.getCmp('cboStatusLunas_viPenJasRad').disable();
        if(radiovalues === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarwjigd = "";
                }else
                {
                tmpkreteriarwjigd = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRwj').getValue() + "'";
                }
            }else {
                tmpkreteriarwjigd = "";
            }
            
			if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianamarwi = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }
            
			if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrecrwi = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else{
                tmpkriteriamedrecrwi = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkreteriarwjigd +" "+ tmpkriterianamarwi +" "+ tmpkriteriamedrecrwi +" and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc limit 10";
            tmpunit = 'ViewRwjPenjasRad';
            //loadpenjasrad(tmpparams, tmpunit);
			refeshpenjasrad(tmpparams);
        }
        else if (radiovalues === '2')
        {
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and u.kd_unit = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else {
                tmpkriteriakamar = "";
            }
			
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else {
                tmpkriterianama = "";
            }
			
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else {
                tmpkriteriamedrec = "";
            }
             tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" and left(u.kd_unit,1) IN ('1') ORDER BY tr.no_transaksi desc limit 10";
             tmpunit = 'ViewRwiPenjasRad';
             //loadpenjasrad(tmpparams, tmpunit);
			 refeshpenjasrad(tmpparams);
			 
        }else if (radiovalues === '3')
        {
            PenJasRadLookUp();
        }else if (radiovalues === '4')
        {
			 if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarwjigd = "";
                }else
                {
                tmpkreteriarwjigd = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRwj').getValue() + "'";
                }
            }else {
                tmpkreteriarwjigd = "";
            }
            
			if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianamarwi = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }
            
			if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrecrwi = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else{
                tmpkriteriamedrecrwi = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkreteriarwjigd +" "+ tmpkriterianamarwi +" "+ tmpkriteriamedrecrwi +" and left(u.kd_unit,1) IN ('2') ORDER BY tr.no_transaksi desc limit 10";
            tmpunit = 'ViewRwjPenjasRad';
            //loadpenjasrad(tmpparams, tmpunit);
			refeshpenjasrad(tmpparams);
		}
        
    }
}

//VALIDASI COMBO UNIT/POLI
function getDataCariUnitPenjasRad(kriteria)
{
	if (kriteria===undefined)
	{
		kriteria="kd_bagian=3 and parent<>'0'";
	}
	dsunit_viKasirRwj.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'kd_unit',
                        Sortdir: 'ASC',
                        target: 'ViewSetupUnit',
                        param: kriteria
                    }
            }
	);
	return dsunit_viKasirRwj;
}
function mComboUnit_viKasirRwj() 
{
	
    var Field = ['KD_UNIT','NAMA_UNIT'];
	
    dsunit_viKasirRwj = new WebApp.DataStore({ fields: Field });
	getDataCariUnitPenjasRad();
    var cboUNIT_viKasirRwj = new Ext.form.ComboBox
	(
            {
                id: 'cboUNIT_viKasirRwj',
                x: 155,
                y: 70,
                typeAhead: true,
                triggerAction: 'all',
				emptyText:'Poli',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 100,
                store: dsunit_viKasirRwj,
                valueField: 'KD_UNIT',
                displayField: 'NAMA_UNIT',
                value:'All',
				listeners:
                    {

                        'select': function(a, b, c) 
                            {					       
                                //RefreshDataFilterPenJasRad();
								combovaluesunit=b.data.valueField;
								validasiJenisTr();
							}

                    }
            }
	);
	
    return cboUNIT_viKasirRwj;
};

function mcomboKamarSpesial()
{
    var Field = ['no_kamar','kamar'];
    ds_KamarSpesial_viJasRad = new WebApp.DataStore({fields: Field});

	ds_KamarSpesial_viJasRad.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewSetupKelasSpesial',
                param: ""
            }
        }
    )

    var cboRujukanKamarSpesialJasRadRequestEntry = new Ext.form.ComboBox
    (
        {
            x: 155,
            y: 70,
            id: 'cboRujukanKamarSpesialJasRadRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Kamar...',
            fieldLabel: 'Kamar ',
            align: 'Right',
            store: ds_KamarSpesial_viJasRad,
            valueField: 'no_kamar',
            displayField: 'kamar',
            Width:'150',
            listeners:
                {
                    'select': function(a, b, c)
					{
						ComboValuesKamar=b.data.valueField;
						validasiJenisTr();    
					},
                    'render': function(c)
					{
						
					}


		}
        }
    )

    return cboRujukanKamarSpesialJasRadRequestEntry;
}

//LOOKUP DETAIL TRANSAKSI RADORATORIUM
function PenJasRadLookUp(rowdata) 
{
    var lebar = 900;
    FormLookUpsdetailTRrwj = new Ext.Window
    (
        {
            id: 'gridPenJasRad',
            title: 'Penata Jasa Radiologi',
            closeAction: 'destroy',
            width: lebar,
            height: 550,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryPenJasRad(lebar,rowdata),
            listeners:
            {
				 activate: function()
				{
					if (rowdata === undefined){
						Ext.getCmp('txtNoMedrecL').setReadOnly(true);
						Ext.getCmp('txtNamaUnit').setReadOnly(true);
						Ext.getCmp('txtNamaPasienL').setReadOnly(false);
						Ext.getCmp('txtAlamatL').setReadOnly(false);
						Ext.getCmp('dtpTtlL').setReadOnly(false);
						Ext.getCmp('txtCustomerLamaHide').hide();
					}else{
						
					}
					
					
				}
            }
        }
    );

    FormLookUpsdetailTRrwj.show();
    if (rowdata === undefined) 
	{
        RWJAddNew();
	}
	else 
	{
		TRRWJInit(rowdata);
	}

};
function PenjasLookUpRAD(rowdata)
{
    var lebar = 580;
    var FormLookUpsdetailTRPenjasRAD = new Ext.Window
            (
                    {
                        id: 'gridPenjasRAD',
                        title: 'Penata Jasa Radiologi',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 500,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryPenjasBayarRAD(lebar),
                        listeners:
                                {
                                }
                    }
            );

    FormLookUpsdetailTRPenjasRAD.show();
    /* if (rowdata == undefined)
    {
        PenjasBayarRADAddNew();
    } else
    { */
        TRPenjasBayarRADInit(rowdata)
    //}

}
;
function mEnabledKasirRADCM(mBol)
{
   /*  Ext.get('btnLookupKasirRAD').dom.disabled = mBol;
    Ext.get('btnHpsBrsKasirRAD').dom.disabled = mBol; */
}
;
function PenjasBayarRADAddNew()
{
    AddNewKasirRADKasir = true;
    Ext.get('txtNoTransaksiKasirRADKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTgltransaksi.format('d/M/Y');
    Ext.get('txtNoMedrecDetransaksi').dom.value = '';
    Ext.get('txtNamaPasienDetransaksi').dom.value = '';

    //Ext.get('txtKdUrutMasuk').dom.value = '';
    Ext.get('cboStatus_viKasirRADKasir').dom.value = ''
    rowSelectedPenJasRad = undefined;
    dsTRDetailPenJasRadList.removeAll();
    mEnabledKasirRADCM(false);


}
;
function loaddatastorePembayaran(jenis_pay)
{
	// console.log(jenis_pay);
    dsComboBayar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'nama',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboBayar',
                                    param: 'jenis_pay=~' + jenis_pay + '~'
                                }
                    }
            )
}
function TRPenjasBayarRADInit(rowdata)
{
    AddNewKasirRADKasir = false;
    
	// console.log(rowdata);
	if (rowdata===undefined)
	{
		Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganL').getValue().format('d/M/Y');
		Ext.get('txtNoMedrecDetransaksi').dom.value = Ext.getCmp('txtNoMedrecL').getValue();
		Ext.get('txtNamaPasienDetransaksi').dom.value = Ext.getCmp('txtNamaPasienL').getValue();
		tanggaltransaksitampung = Ext.getCmp('dtpKunjunganL').getValue();
	}
	else
	{
		tanggaltransaksitampung = rowdata.TGL_TRANSAKSI;
		Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganL').getValue().format('d/M/Y'); //ShowDate(rowdata.TGL_TRANSAKSI);
		Ext.get('txtNoMedrecDetransaksi').dom.value = rowdata.KD_PASIEN;
		Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	}
	
	var notransnya;
	if (Ext.getCmp('txtNoTransaksiPenJasRad').getValue()==='' || Ext.getCmp('txtNoTransaksiPenJasRad').getValue()===undefined){
		notransnya=rowdata.NO_TRANSAKSI;
	}
	else
	{
		notransnya=Ext.getCmp('txtNoTransaksiPenJasRad').getValue();
	}
	var modul='';
	if(radiovalues == 1){
		modul='igd';
	} else if(radiovalues == 2){
		modul='rwi';
	} else if(radiovalues == 4){
		modul='rwj';
	} else{
		modul='langsung';
	}
	Ext.get('txtNoTransaksiKasirRADKasir').dom.value = notransnya;
	RefreshDataKasirRADKasirDetail(notransnya,kd_kasir_rad);
   
    // take the displayField value 
	Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/cekPembayaran",
        params: {
            notrans: notransnya,
			Modul:modul
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
			var cst = Ext.decode(o.responseText);
			// console.log(cst.ListDataObj);
			loaddatastorePembayaran(cst.ListDataObj[0].jenis_pay);
			notransaksi=cst.ListDataObj[0].no_transaksi;
			vkd_unit = cst.ListDataObj[0].kd_unit;
			kodeunit=cst.ListDataObj[0].kd_unit;
			kdkasir = cst.ListDataObj[0].kd_kasir;
			notransaksiasal_rad = cst.ListDataObj[0].no_transaksi_asal;
			kdkasirasal_rad = cst.ListDataObj[0].kd_kasir_asal;
			Ext.get('cboPembayaran').dom.value =cst.ListDataObj[0].ket_payment;
			Ext.get('cboJenisByr').dom.value =cst.ListDataObj[0].cara_bayar; 
			vflag = cst.ListDataObj[0].flag;
			tapungkd_pay = cst.ListDataObj[0].kd_pay;
			vkode_customer =  cst.ListDataObj[0].kd_customer;
        }

    });
    tampungtypedata = 0;
    
    jenispay = 1;
    showCols(Ext.getCmp('gridDTItemTest'));
    hideCols(Ext.getCmp('gridDTItemTest'));
   
    //(rowdata.NO_TRANSAKSI;


    Ext.Ajax.request({
        url: baseURL + "index.php/main/getcurrentshift",
        params: {
            command: '0',
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {

            tampungshiftsekarang = o.responseText
        }

    });



}
;
function getItemPanelNoTransksiRADKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Transaksi ',
                                                name: 'txtNoTransaksiKasirRADKasir',
                                                id: 'txtNoTransaksiKasirRADKasir',
                                                emptyText: nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%'
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 55,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTanggalDetransaksi',
                                                name: 'dtpTanggalDetransaksi',
                                                format: 'd/M/Y',
                                                readOnly: true,
                                                value: now,
                                                anchor: '100%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function mComboJenisByrView()
{
    var Field = ['JENIS_PAY', 'DESKRIPSI', 'TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({fields: Field});
    dsJenisbyrView.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'jenis_pay',
                                    Sortdir: 'ASC',
                                    target: 'ComboJenis',
                                }
                    }
            );

    var cboJenisByr = new Ext.form.ComboBox
            (
                    {
                        id: 'cboJenisByr',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Pembayaran      ',
                        align: 'Right',
                        anchor: '100%',
                        store: dsJenisbyrView,
                        valueField: 'JENIS_PAY',
                        displayField: 'DESKRIPSI',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        loaddatastorePembayaran(b.data.JENIS_PAY);
                                        tampungtypedata = b.data.TYPE_DATA;
                                        jenispay = b.data.JENIS_PAY;
                                        showCols(Ext.getCmp('gridDTItemTest'));
                                        hideCols(Ext.getCmp('gridDTItemTest'));
                                        getTotalDetailProduk();
                                        Ext.get('cboPembayaran').dom.value = 'Pilih Pembayaran...';
                                    },
                                }
                    }
            );

    return cboJenisByr;
}
;
function getTotalDetailProduk()
{
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < dsTRDetailKasirRADKasirList.getCount(); i++)
    {

        var recordterakhir;

        //alert(TotalProduk);
        if (tampungtypedata == 0)
        {
            tampunggrid = parseInt(dsTRDetailKasirRADKasirList.data.items[i].data.BAYARTR);

            //recordterakhir= tampunggrid
            //TotalProduk.toString().replace(/./gi, "");
            TotalProduk += tampunggrid

            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirRAD').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 3)
        {
            tampunggrid = parseInt(dsTRDetailKasirRADKasirList.data.items[i].data.PIUTANG);
            //TotalProduk.toString().replace(/./gi, "");
            //recordterakhir=tampunggrid
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirRAD').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 1)
        {
            tampunggrid = parseInt(dsTRDetailKasirRADKasirList.data.items[i].data.DISCOUNT);

            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirRAD').dom.value = formatCurrency(recordterakhir);
        }
    }
	
	if (Ext.getCmp('txtJumlah2EditData_viKasirRAD').getValue()==='0' || Ext.getCmp('txtJumlah2EditData_viKasirRAD').getValue()===0 )
	{
		Ext.getCmp('btnSimpanKasirRAD').disable();
		Ext.getCmp('btnTransferKasirRAD').disable();
	}
	else
	{
		Ext.getCmp('btnSimpanKasirRAD').enable();
		Ext.getCmp('btnTransferKasirRAD').enable();
	}
    bayar = Ext.get('txtJumlah2EditData_viKasirRAD').getValue();
    return bayar;
}
;


    
function hideCols(grid)
{
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);

    }
}
;
function showCols(grid) {
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);

    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
    }

}
;
function mComboPembayaran()
{
    var Field = ['KD_PAY', 'JENIS_PAY', 'PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});

    var cboPembayaran = new Ext.form.ComboBox
            (
                    {
                        id: 'cboPembayaran',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Pembayaran...',
                        labelWidth: 80,
                        align: 'Right',
                        store: dsComboBayar,
                        valueField: 'KD_PAY',
                        displayField: 'PAYMENT',
                        anchor: '100%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tapungkd_pay = b.data.KD_PAY;
                                    },
                                }
                    }
            );

    return cboPembayaran;
}
;
function getItemPanelmedreckasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Medrec ',
                                                name: 'txtNoMedrecDetransaksi',
                                                id: 'txtNoMedrecDetransaksi',
                                                readOnly: true,
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: '',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtNamaPasienDetransaksi',
                                                id: 'txtNamaPasienDetransaksi',
                                                anchor: '100%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getItemPanelInputKasir(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: true,
                height: 110,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getItemPanelNoTransksiRADKasir(lebar),
                                            getItemPanelmedreckasir(lebar), getItemPanelUnitKasir(lebar)
                                        ]
                            }
                        ]
            };
    return items;
}
;



function getItemPanelUnitKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboJenisByrView()
                                        ]
                            }, {
                                columnWidth: .60,
                                layout: 'form',
                                labelWidth: 0.9,
                                border: false,
                                items:
                                        [
                                            mComboPembayaran()
                                        ]
                            }
                        ]
            }
    return items;
}
;

function RefreshDataKasirRADKasirDetail(no_transaksi,kd_kasir)
{
    var strKriteriaKasirRAD = '';

    if (kd_kasir !== undefined) {
        strKriteriaKasirRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
    }else{
        strKriteriaKasirRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
    }

    dsTRDetailKasirRADKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailbayar',
                                    param: strKriteriaKasirRAD
                                }
                    }
            );
    return dsTRDetailKasirRADKasirList;
}
;
function GetDTLTRKasirRADGrid()
{
    var fldDetail = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'BAYARTR', 'DISCOUNT', 'PIUTANG'];

    dsTRDetailKasirRADKasirList = new WebApp.DataStore({fields: fldDetail})

    gridDTLTRKasirRAD = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Detail Bayar',
                        stripeRows: true,
                        id: 'gridDTLTRKasirRAD',
                        store: dsTRDetailKasirRADKasirList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        height: 230,
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        cm: TRKasirRawatJalanColumModel()
                    }
            );

    return gridDTLTRKasirRAD;
}
;

function TRKasirRawatJalanColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiKasirRAD',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 250,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiKasirRAD',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            id: 'colURUTKasirRAD',
                            header: 'Urut',
                            dataIndex: 'URUT',
                            sortable: false,
                            hidden: true,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            hidden: true,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colQtyKasirRAD',
                            header: 'Qty',
                            width: 91,
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                        },
                        {
                            id: 'colHARGAKasirRAD',
                            header: 'Harga',
                            align: 'right',
                            hidden: false,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colPiutangKasirRAD',
                            header: 'Puitang',
                            width: 80,
                            dataIndex: 'PIUTANG',
                            align: 'right',
                            //hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolPuitangRAD',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                //getTotalDetailProduk();
                                return formatCurrency(record.data.PIUTANG);
                            }
                        },
                        {
                            id: 'colTunaiKasirRAD',
                            header: 'Tunai',
                            width: 80,
                            dataIndex: 'BAYARTR',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolTunaiRAD',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProduk();

                                return formatCurrency(record.data.BAYARTR);
                            }

                        },
                        {
                            id: 'colDiscountKasirRAD',
                            header: 'Discount',
                            width: 80,
                            dataIndex: 'DISCOUNT',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolDiscountRAD',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.DISCOUNT);
                            }
                        }

                    ]
                    )
}
;

function getParamDetailTransaksiKasirRAD()
{
    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    }
    ;

    var params =
            {
                kdUnit: vkd_unit,
                TrKodeTranskasi: Ext.get('txtNoTransaksiKasirRADKasir').getValue(),
                Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
                Shift: tampungshiftsekarang,
                kdKasir: kdkasir,
                bayar: tapungkd_pay,
                Flag: vflag,
                Typedata: tampungtypedata,
                Totalbayar: getTotalDetailProduk(),
                List: getArrDetailTrKasirRAD(),
                JmlField: mRecordKasirRAD.prototype.fields.length - 4,
                JmlList: GetListCountDetailTransaksi(),
                Hapus: 1,
                Ubah: 0
            };
    return params
}
;

function getArrDetailTrKasirRAD()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirRADKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirRADKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirRADKasirList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirRADKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.QTY
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.BAYARTR
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.PIUTANG
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.DISCOUNT



            if (i === (dsTRDetailKasirRADKasirList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }

    return x;
}
;

function ValidasiEntryCMKasirRAD(modul, mBolHapus)
{
    var x = 1;

    if ((Ext.get('txtNoTransaksiKasirRADKasir').getValue() == '')

            || (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
            || (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
            || (Ext.get('dtpTanggalDetransaksi').getValue() == '')
            || dsTRDetailKasirRADKasirList.getCount() === 0
            || (Ext.get('cboPembayaran').getValue() == '')
            || (Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...'))
    {
        if (Ext.get('txtNoTransaksiKasirRADKasir').getValue() == '' && mBolHapus === true)
        {
            x = 0;
        } else if (Ext.get('cboPembayaran').getValue() == '' || Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...')
        {
            ShowPesanWarningPenJasRad(('Data pembayaran tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasRad(('Data no. medrec tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasRad('Nama pasien belum terisi', modul);
            x = 0;
        } else if (Ext.get('dtpTanggalDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasRad(('Data tanggal kunjungan tidak boleh kosong'), modul);
            x = 0;
        }
        //cboPembayaran

        else if (dsTRDetailKasirRADKasirList.getCount() === 0)
        {
            ShowPesanWarningPenJasRad('Data dalam tabel kosong', modul);
            x = 0;
        }
        ;
    }
    ;
    return x;
}
;
function Datasave_KasirRADKasir(mBol)
{
    if (ValidasiEntryCMKasirRAD(nmHeaderSimpanData, false) == 1)
    {
        Ext.Ajax.request
                (
                        {
                            //url: "./Datapool.mvc/CreateDataObj",
                            url: baseURL + "index.php/main/functionKasirPenunjang/savepembayaran",
                            params: getParamDetailTransaksiKasirRAD(),
                            failure: function (o)
                            {
                                ShowPesanWarningPenJasRad('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                            },
                            success: function (o)
                            {
								 RefreshDatahistoribayar('0');
                                RefreshDataPenJasRadDetail('0');
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfoPenJasRad(nmPesanSimpanSukses, nmHeaderSimpanData);
									RefreshDatahistoribayar(Ext.get('txtNoTransaksiKasirRADKasir').dom.value);
									RefreshDataKasirRADKasirDetail(Ext.get('txtNoTransaksiKasirRADKasir').dom.value,kd_kasir_rad);
                                    ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiKasirRADKasir').dom.value,kd_kasir_rad);
                                    //RefreshDataKasirRADKasir();
                                    if (mBol === false)
                                    {
                                        ViewGridBawahLookupPenjasRad();
                                    }
                                    ;
                                } else
                                {
                                    ShowPesanWarningPenJasRad('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                }
                                ;
                            }
                        }
                )

    } else
    {
        if (mBol === true)
        {
            return false;
        }
        ;
    }
    ;

}

function TransferLookUp_rad(rowdata)
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_Rad = new Ext.Window
            (
                    {
                        id: 'gridTransfer_Rad',
                        title: 'Transfer',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 410,
                        border: false,
                        resizable: false,
                        plain: false,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryTRTransfer_rad(lebar),
                        listeners:
                                {
                                     activate: function()
                                    {
                                        Ext.getCmp('cboalasan_transfer').setValue('Pembayaran Disatukan');                                        
                                    }
                                }
                    }
            );

    FormLookUpsdetailTRTransfer_Rad.show();
    //  Transferbaru();

};
function getParamTransferRwi_dari_rad()
{

/*
       KDkasirIGD:'01',
		TrKodeTranskasi: notransaksi,
		KdUnit: kodeunit,
		Kdpay: kdpaytransfer,
		Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
		Tglasal:  ShowDate(tgltrans),
		Shift: tampungshiftsekarang,
		TRKdTransTujuan:Ext.get(txtTranfernoTransaksiRWJ).dom.value,
		KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRWJ).dom.value,
		TglTranasksitujuan : Ext.get(dtpTanggaltransaksiRujuanRWJ).dom.value,
		KDunittujuan : Trkdunit2,
		KDalasan :Ext.get(cboalasan_transfer).dom.value,
		KasirRWI:'05',
		Kdcustomer:kdcustomeraa,
		kodeunitkamar:tmp_kodeunitkamar,
		kdspesial:tmp_kdspesial,
		nokamar:tmp_nokamar,


*/
    var params =
            {
                KDkasirIGD: kdkasir,
                Kdcustomer: vkode_customer,
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                Kdpay: kdpaytransfer,
                Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
                Tglasal: ShowDate(TglTransaksi),
                Shift: tampungshiftsekarang,
                TRKdTransTujuan: Ext.get(txtTranfernoTransaksiRAD).dom.value,
                KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRAD).dom.value,
                TglTranasksitujuan: Ext.get(dtpTanggaltransaksiRujuanRAD).dom.value,
                KDunittujuan: Trkdunit2,
                KDalasan: Ext.get(cboalasan_transfer).dom.value,
                KasirRWI: kdkasirasal_rad,
				kodeunitkamar:tmp_kodeunitkamar_RAD,
				kdspesial:tmp_kdspesial_RAD,
				nokamar:tmp_nokamar_RAD,


            };
    return params
}
;
function TransferData_rad(mBol)
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/saveTransfer",
                        params: getParamTransferRwi_dari_rad(),
                        failure: function (o)
                        {

                            ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            // ViewGridBawahLookupPenjasRad();
                        },
                        success: function (o)
                        {	
						
                            
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {	
                                //TransferData_rad_SQL();
                                Ext.getCmp('btnSimpanKasirRAD').disable();
                                Ext.getCmp('btnTransferKasirRAD').disable();
                                ShowPesanInfoPenJasRad('Transfer Berhasil', 'transfer ');
								RefreshDatahistoribayar(Ext.getCmp('txtNoTransaksiPenJasRad').getValue());
                                FormLookUpsdetailTRTransfer_Rad.close();
								
                            } else
                            {
                                ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            }
                            ;
                        }
                    }
            )

}
;

function getItemPanelButtonTransfer_rad(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                height: 30,
                anchor: '100%',
                style: {'margin-top': '-1px'},
                items:
                        [
                            {
                                layout: 'hBox',
                                width: 400,
                                border: false,
                                bodyStyle: 'padding:5px 0px 5px 5px',
                                defaults: {margins: '3 3 3 3'},
                                anchor: '90%',
                                layoutConfig:
                                        {
                                            align: 'middle',
                                            pack: 'end'
                                        },
                                items:
                                        [
                                            {
                                                xtype: 'button',
                                                text: 'Simpan',
                                                width: 70,
                                                style: {'margin-left': '0px', 'margin-top': '0px'},
                                                hideLabel: true,
                                                id: 'btnOkTransfer_rad',
                                                handler: function ()
                                                {
												
                                                    TransferData_rad(false);
													 Ext.getCmp('btnSimpanKasirRAD').disable();
													 Ext.getCmp('btnTransferKasirRAD').disable();
													
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                text: 'Tutup',
                                                width: 70,
                                                hideLabel: true,
                                                id: 'btnCancelTransfer_rad',
                                                handler: function ()
                                                {
                                                    FormLookUpsdetailTRTransfer_Rad.close();
                                                }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getFormEntryTRTransfer_rad(lebar)
{
    var pnlTRTransfer = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRTransfer',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 425,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputTransfer_RAD(lebar), getItemPanelButtonTransfer_rad(lebar)],
                        tbar:
                                [
                                ]
                    }
            );

    var FormDepanTransfer = new Ext.Panel
            (
                    {
                        id: 'FormDepanTransfer',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        items:
                                [
                                    pnlTRTransfer

                                ]

                    }
            );

    return FormDepanTransfer
}
;

function mComboTransferTujuan()
{
    var cboTransferTujuan = new Ext.form.ComboBox
            (
                    {
                        id: 'cboTransferTujuan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        hidden: true,
                        fieldLabel: 'Transfer',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Gawat Darurat'], [2, 'Rawat Inap'], [3, 'Rawat Jalan']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: tranfertujuan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tranfertujuan = b.data.displayText;
                                        //RefreshDataSetDokter();
                                        ViewGridBawahLookupPenjasRad();

                                    }
                                }
                    }
            );
    return cboTransferTujuan;
}
;

function getTransfertujuan_rad(lebar)
{
    var items =
            {
                Width: lebar - 2,
                height: 170,
                layout: 'form',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                labelWidth: 130,
                items:
                        [
                            {
                                xtype: 'tbspacer',
                                height: 2
                            },
                            mComboTransferTujuan(),
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Transaksi',
                                //maxLength: 200,
                                name: 'txtTranfernoTransaksiRAD',
                                id: 'txtTranfernoTransaksiRAD',
                                labelWidth: 130,
                                readonly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                id: 'dtpTanggaltransaksiRujuanRAD',
                                name: 'dtpTanggaltransaksiRujuanRAD',
                                format: 'd/M/Y',
                                readOnly: true,
                                //  value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Medrec',
                                //maxLength: 200,
                                name: 'txtTranfernomedrecRAD',
                                id: 'txtTranfernomedrecRAD',
                                labelWidth: 130,
                                readOnly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama Pasien',
                                //maxLength: 200,
                                name: 'txtTranfernamapasienRAD',
                                id: 'txtTranfernamapasienRAD',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Unit Perawatan',
                                //maxLength: 200,
                                name: 'txtTranferunitRAD',
                                id: 'txtTranferunitRAD',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
							{
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        name: 'txtTranferkelaskamar_RAD',
                                        id: 'txtTranferkelaskamar_RAD',
										readOnly : true,
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                            }
                        ]
            }
    return items;
}
;

function getItemPanelNoTransksiTransfer(lebar)
{
    var items =
            {
                Width: lebar,
                height: 110,
                layout: 'column',
                border: true,
                items:
                        [
                            {
                                columnWidth: .990,
                                layout: 'form',
                                Width: lebar - 10,
                                labelWidth: 130,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah Biaya',
                                                maxLength: 200,
                                                name: 'txtjumlahbiayasal',
                                                id: 'txtjumlahbiayasal',
                                                width: 100,
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Paid',
                                                maxLength: 200,
                                                name: 'txtpaid',
                                                id: 'txtpaid',
                                                readOnly: true,
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                value: 0,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah dipindahkan',
                                                maxLength: 200,
                                                name: 'txtjumlahtranfer',
                                                id: 'txtjumlahtranfer',
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Saldo tagihan',
                                                maxLength: 200,
                                                name: 'txtsaldotagihan',
                                                id: 'txtsaldotagihan',
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                value: 0,
                                                width: 100,
                                                anchor: '95%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function mComboalasan_transfer()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_transfer = new WebApp.DataStore({fields: Field});
    dsalasan_transfer.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_alasan',
                                    Sortdir: 'ASC',
                                    target: 'ComboAlasanTransfer',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboalasan_transfer = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_transfer',
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Alasan Transfer ',
                        align: 'Right',
                        width: 100,
                        editable: false,
                        anchor: '100%',
                        store: dsalasan_transfer,
                        valueField: 'ALASAN',
                        displayField: 'ALASAN',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                    }

                                }
                    }
            );

    return cboalasan_transfer;
};

function getItemPanelInputTransfer_RAD(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: false,
                height: 330,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                height: 330,
                                border: false,
                                items:
                                        [
                                            getTransfertujuan_rad(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 5
                                            },
                                            getItemPanelNoTransksiTransfer(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            mComboalasan_transfer()

                                        ]
                            },
                        ]
            };
    return items;
}
;
function showhide_unit(kode)
{
	if(kode==='05')
	{
	Ext.getCmp('txtTranferunitRAD').hide();
	Ext.getCmp('txtTranferkelaskamar_RAD').show();

	}else{
	Ext.getCmp('txtTranferunitRAD').show();
	Ext.getCmp('txtTranferkelaskamar_RAD').hide();
	}
}
function getFormEntryPenjasBayarRAD(lebar)
{
    var pnlTRKasirRAD = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirRAD',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 130,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputKasir(lebar)],
                        tbar:
                                [
                                ]
                    }
            );
    var x;
    var paneltotal = new Ext.Panel
            (
                    {
                        id: 'paneltotal',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        height: 67,
                        anchor: '100% 8.0000%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        html: " Total :"
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirRAD',
                                                        name: 'txtJumlah2EditData_viKasirRAD',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        hidden: true,
                                                        html: " Bayar :"
                                                    },
                                                    {
                                                        xtype: 'numberfield',
                                                        id: 'txtJumlahEditData_viKasirRAD',
                                                        name: 'txtJumlahEditData_viKasirRAD',
                                                        hidden: true,
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        //readOnly: true,
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                ]
                    }
            )
    var GDtabDetailKasirRAD = new Ext.Panel
            (
                    {
                        id: 'GDtabDetailKasirRAD',
                        region: 'center',
                        activeTab: 0,
                        height: 350,
                        anchor: '100% 100%',
                        border: false,
                        plain: true,
                        defaults: {autoScroll: true},
                        items: [GetDTLTRKasirRADGrid(), paneltotal],
                        tbar:
                                [
                                    {
                                        text: 'Bayar',
                                        id: 'btnSimpanKasirRAD',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        handler: function ()
                                        {
                                            Datasave_KasirRADKasir(false);
                                           // FormDepan2KasirRAD.close();
                                            Ext.getCmp('btnSimpanKasirRAD').disable();
                                            Ext.getCmp('btnTransferKasirRAD').disable();
                                            //Ext.getCmp('btnHpsBrsKasirRAD').disable();
                                        }
                                    },
									
                                    {
                                        id: 'btnTransferKasirRAD',
                                        text: 'Transfer',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                        /* disabled: true, */
                                        handler: function (sm, row, rec)
                                        {
											//Disini
                                            TransferLookUp_rad();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/GetPasienTranferLab_rad",
                                                params: {
                                                    notr: notransaksiasal_rad,
                                                    notransaksi: Ext.getCmp('txtNoTransaksiKasirRADKasir').getValue(),
                                                    kdkasir: kdkasirasal_rad
                                                },
                                                success: function (o)
                                                {	
												showhide_unit(kdkasirasal_rad);
                                                    if (o.responseText == "") {
                                                        ShowPesanWarningPenJasRad('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                    } else {
                                                        var tmphasil = o.responseText;
                                                        var tmp = tmphasil.split("<>");
                                                        if (tmp[5] == undefined && tmp[3] == undefined) {
                                                            ShowPesanWarningPenJasRad('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                            FormLookUpsdetailTRTransfer_Rad.close();
                                                        } else {
															console.log(tmp[12]);
                                                            Ext.get(txtTranfernoTransaksiRAD).dom.value = tmp[3];
                                                            Ext.get(dtpTanggaltransaksiRujuanRAD).dom.value = ShowDate(tmp[4]);
                                                            Ext.get(txtTranfernomedrecRAD).dom.value = tmp[5];
                                                            Ext.get(txtTranfernamapasienRAD).dom.value = tmp[2];
                                                            Ext.get(txtTranferunitRAD).dom.value = tmp[1];
                                                            Trkdunit2 = tmp[6];
															Ext.get(txtTranferkelaskamar_RAD).dom.value=tmp[9];
															Ext.get(txtjumlahbiayasal).dom.value=tmp[12];
															Ext.get(txtjumlahtranfer).dom.value=tmp[12];
															
															tmp_kodeunitkamar_RAD=tmp[8];
															tmp_kdspesial_RAD=tmp[10];
															tmp_nokamar_RAD=tmp[11];
                                                            var kasir = tmp[7];
                                                            Ext.Ajax.request({
                                                                url: baseURL + "index.php/main/GettotalTranfer",
                                                                params: {
                                                                    notr: notransaksi,
																	kd_kasir:kdkasir
                                                                },
                                                                success: function (o)
                                                                {
																	console.log(o);
                                                                    Ext.get(txtjumlahbiayasal).dom.value = formatCurrency(o.responseText);
                                                                    Ext.get(txtjumlahtranfer).dom.value = formatCurrency(o.responseText);
																	/* Ext.get(txtjumlahbiayasal).dom.value=tmp[12];
																	Ext.get(txtjumlahtranfer).dom.value=tmp[12]; */
                                                                }
                                                            });
                                                        }

                                                    }
                                                }
                                            });

                                        }, //disabled :true
                                    },
                                    {
                                        xtype: 'splitbutton',
                                        text: 'Cetak',
                                        iconCls: 'print',
                                        id: 'btnPrint_viDaftar',
                                        handler: function ()
                                        {
                                        },
                                        menu: new Ext.menu.Menu({
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
                                                            id: 'btnPrintBillRadRad',
                                                            handler: function ()
                                                            {
                                                                printbillRadRad();
                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Kwitansi',
                                                            id: 'btnPrintKwitansiRadRad',
                                                            handler: function ()
                                                            {
                                                                printkwitansiRadRad();
                                                            }
                                                        }
                                                    ]
                                        })
                                    }
                                ]
                    }
            );



    var pnlTRKasirRAD2 = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirRAD2',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 380,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [GDtabDetailKasirRAD,
                        ]
                    }
            );




    FormDepan2KasirRAD = new Ext.Panel
            (
                    {
                        id: 'FormDepan2KasirRAD',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                height: 380,
                        shadhow: true,
                        items: [pnlTRKasirRAD, pnlTRKasirRAD2,
                        ]
                    }
            );

    return FormDepan2KasirRAD
}
function paramUpdateTransaksi()
{
    var params =
            {
                TrKodeTranskasi: Ext.get('txtNoTransaksiPenJasRad').dom.value,
                KDkasir: kdkasir
            };
    return params
}
;
function UpdateTutuptransaksi(mBol)
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/ubah_co_status_transksi",
                        params: paramUpdateTransaksi(),
                        failure: function (o)
                        {
                            ShowPesanInfoPenJasRad('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                            validasiJenisTr();
                        },
                        success: function (o)
                        {
                            //RefreshDatahistoribayar(Ext.get('cellSelectedtutup);

                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoPenJasRad(nmPesanSimpanSukses, nmHeaderSimpanData);

                                //RefreshDataKasirRADKasir();
                                if (mBol === false)
                                {
                                    validasiJenisTr();
                                }
                                ;
                                cellSelecteddeskripsi = '';
                            } else
                            {
                                ShowPesanInfoPenJasRad('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                                cellSelecteddeskripsi = '';
                            }
                            ;
                        }
                    }
            )
}
;
function getFormEntryPenJasRad(lebar,data) 
{
    var pnlTRRWJ = new Ext.FormPanel
    (
        {
            id: 'PanelTRPenJasRad',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding: 5px 5px 5px 5px',
		    height:255,
            width: lebar,
            border: false,
            items: [getItemPanelPenJasRad(lebar)],
            tbar:
            [
				'-',
				{
                        text: 'Simpan',
                        id: 'btnSimpanRWJ',
                        tooltip: nmSimpan,
                        iconCls: 'save',
                        handler: function()
						{
							Datasave_PenJasRad(false);  
                            // console.log(Ext.getCmp('txtKdUnitRad').getValue());                                  

						}
				},  
				
				'-',
				{
					id: 'btnPembayaranPenjasRAD',
					text: 'Pembayaran',
					tooltip: nmEditData,
					iconCls: 'Edit_Tr',
					handler: function (sm, row, rec)
					{
						if (rowSelectedPenJasRad != undefined) {
							PenjasLookUpRAD(rowSelectedPenJasRad.data);
						} else {
							PenjasLookUpRAD();
							//ShowPesanWarningPenJasRad('Pilih data tabel  ', 'Pembayaran');
						}
					}//, disabled: true
				}, ' ', ' ', '' + ' ', '' + ' ', '' + ' ', '' + ' ', ' ', '' + '-',
				{
					text: 'Tutup Transaksi',
					id: 'btnTutupTransaksiPenjasRAD',
					tooltip: nmHapus,
					iconCls: 'remove',
					handler: function ()
					{
						if (noTransaksiPilihan == '' || noTransaksiPilihan == 'undefined') {
							ShowPesanWarningPenJasRad('Pilih data tabel  ', 'Pembayaran');
						} else {
							UpdateTutuptransaksi(false);
							Ext.getCmp('btnPembayaranPenjasRAD').disable();
							Ext.getCmp('btnTutupTransaksiPenjasRAD').disable();
							Ext.getCmp('btnHpsBrsItemRad').disable();
						}
					}//, disabled: true
				},
					
            ]
        }
    );
 var x;
 var GDtabDetailRad = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailRad',
        region: 'center',
        activeTab: 0,
		height:250,
		width:855,
		//anchor: '100%',
        border:true,
        plain: true,
        defaults:
		{
			autoScroll: true
		},
		items: [GridDetailItemPemeriksaan(),GetDTLTRHistoryGrid()],

			listeners:
			{   
				'tabchange' : function (panel, tab) {
					if (x ==1)
					{
						Ext.getCmp('btnLookupPenJasRad').hide()
						//Ext.getCmp('btnSimpanRWJ').hide()
						Ext.getCmp('btnHpsBrsItemRad').hide()
						x=2;
						return x;
					}else 
					{ 	
						Ext.getCmp('btnLookupPenJasRad').show()
						Ext.getCmp('btnSimpanRWJ').show()
						Ext.getCmp('btnHpsBrsItemRad').show()
						x=1;	
						return x;
					}

				}
			}
        }
		
    );
	
   
   
   var pnlTRRWJ2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRPenJasRad2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:460,
            //anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailRad
			
			]
        }
    );

	
   
   
    var FormDepanPenJasRad = new Ext.Panel
	(
		{
		    id: 'FormDepanPenJasRad',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[
				pnlTRRWJ,pnlTRRWJ2	
			]

		}
	);
    return FormDepanPenJasRad
};

function DataDeletePenJasRadDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeletePenJasRadDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoPenJasRad(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailPenJasRadList.removeAt(CurrentPenJasRad.row);
                    cellSelecteddeskripsi=undefined;
                    ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,kd_kasir_rad);
                    AddNewPenJasRad = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningPenJasRad(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningPenJasRad(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeletePenJasRadDetail()
{
    var params =
    {//^^^
		Table: 'ViewDetailTransaksiPenJasRad',
        TrKodeTranskasi: CurrentPenJasRad.data.data.NO_TRANSAKSI,
		TrTglTransaksi:  CurrentPenJasRad.data.data.TGL_TRANSAKSI,
		TrKdPasien :	 CurrentPenJasRad.data.data.KD_PASIEN,
		TrKdNamaPasien : Ext.get('txtNamaPasienL').getValue(),
		//TrAlamatPasien : Ext.get('txtAlamatL').getValue(),	
		TrKdUnit :		 Ext.get('txtKdUnitRad').getValue(),
		TrNamaUnit :	 Ext.get('txtNamaUnit').getValue(),
		Uraian :		 CurrentPenJasRad.data.data.DESKRIPSI2,
		AlasanHapus : 	 variablehistori,
		TrHarga :		 CurrentPenJasRad.data.data.HARGA,
		
		TrKdProduk :	 CurrentPenJasRad.data.data.KD_PRODUK,
        RowReq: CurrentPenJasRad.data.data.URUT,
        Hapus:2
    };
	
    return params
};

function getParamDataupdatePenJasRadDetail()
{
    var params =
    {
        Table: 'ViewDetailTransaksiPenJasRad',
        TrKodeTranskasi: CurrentPenJasRad.data.data.NO_TRANSAKSI,
        RowReq: CurrentPenJasRad.data.data.URUT,

        Qty: CurrentPenJasRad.data.data.QTY,
        Ubah:1
    };
	
    return params
};

function GridDetailItemPemeriksaan() 
{
    var fldDetail = ['kd_produk','deskripsi','deskripsi2','kd_tarif','harga','qty','desc_req','tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi'];
	
    dsTRDetailPenJasRadList = new WebApp.DataStore({ fields: fldDetail })
    gridDTItemTest = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Item Test',
			id: 'gridDTItemTest',
            stripeRows: true,
            store: dsTRDetailPenJasRadList,
            border: false,
            columnLines: true,
            frame: false,
			width:50,
			height:100,
            //anchor: '100%',
            autoScroll:true,
			viewConfig: {forceFit: true},
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailPenJasRadList.getAt(row);
                            CurrentPenJasRad.row = row;
                            CurrentPenJasRad.data = cellSelecteddeskripsi;
                        }
                    }
                }
            ),
			
            cm: TRPenJasRadColumModel(),
            tbar:
		[
			{
				text: 'Tambah Item Pemeriksaan',
				id: 'btnLookupPenJasRad',
				tooltip: nmLookup,
				iconCls: 'find',
				handler: function()
				{
					getTindakanRad();
					setLookUp_getTindakanPenjasRad();
					
				}
			},
			{
					id:'btnHpsBrsItemRad',
					text: 'Hapus Baris',
					tooltip: 'Hapus Baris',
					disabled:false,
					iconCls: 'RemoveRow',
					handler: function()
					{
						if (dsTRDetailPenJasRadList.getCount() > 0 ) {
							if (cellSelecteddeskripsi != undefined)
							{
                                // console.log(cellSelecteddeskripsi.data);
								Ext.Msg.confirm('Warning', 'Apakah item pemeriksaan ini akan dihapus?', function(button){
									if (button == 'yes'){
										if(CurrentPenJasRad != undefined) {
											var line = gridDTItemTest.getSelectionModel().selection.cell[0];
											
                                            Ext.Ajax.request
                                             (
                                                {
                                                    url: baseURL + "index.php/main/functionRAD/deletedetailrad",
                                                    params: getParamHapusDetailTransaksiRAD(),
                                                    failure: function(o)
                                                    {
                                                        ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                                                    },  
                                                    success: function(o) 
                                                    {
                                                        var cst = Ext.decode(o.responseText);
                                                        if (cst.success === true && cst.tmp === 'ada') 
                                                        {
                                                            ShowPesanInfoPenJasRad('Data Berhasil Di hapus', 'Sukses');
                                                            dsTRDetailPenJasRadList.removeAt(line);
                                                            gridDTItemTest.getView().refresh();
                                                            Delete_Detail_Rad_SQL();
                                                        }else if (cst.success === true && cst.tmp === 'kosong') {
                                                            dsTRDetailPenJasRadList.removeAt(line);
                                                        }
                                                        else 
                                                        {
                                                                ShowPesanWarningPenJasRad('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
                                                        };
                                                    }
                                                }
                                            )
										}
									}
								})
							} else {
								ShowPesanWarningPenJasRad('Pilih record ','Hapus data');
							}
						}
					}
			},
			{
				text: 'Pemakaian Film',
				id: 'btnpemakaianfilmobatPenjasRAD',
				tooltip: nmHapus,
				iconCls: 'Edit_Tr',
				handler: function ()
				{
					var tmpdata = cellSelecteddeskripsi;
					if (tmpdata === undefined) {
						ShowPesanWarningPenJasRad('Pilih record ','Update Film');
					}else{
						PenJasRadPemakianFilmLookUp(tmpdata.data);
					}

					// if (noTransaksiPilihan == '' || noTransaksiPilihan == 'undefined') {
					// 	ShowPesanWarningPenJasRad('Pilih data tabel  ', 'Pembayaran');
					// } else {
					// 	UpdateTutuptransaksi(false);
					// 	Ext.getCmp('btnPembayaranPenjasRAD').disable();
					// 	Ext.getCmp('btnTutupTransaksiPenjasRAD').disable();
					// 	Ext.getCmp('btnHpsBrsItemRad').disable();
					// }
				}//, disabled: true
			}
					
		],
			
        }
		
		
    );
	
	

    return gridDTItemTest;
};

function TRPenJasRadColumModel()
{

	checkColumn_penata__rad = new Ext.grid.CheckColumn({
	   header: 'Cito',
	   dataIndex: 'cito',
	   id: 'checkid',
	   width: 55,
	   renderer: function(value)
	   {
        console.log("Grid");
		},
	});
    return new Ext.grid.ColumnModel
    (
        [ 
            new Ext.grid.RowNumberer(),
			{
				header: 'Cito',
                dataIndex: 'cito',
                width:65,
				menuDisabled:true,
				listeners:{
					itemclick: function(dv, record, items, index, e)
					{
									Ext.getCmp('btnHpsBrsItemRadL').enable();
									console.log("test");
					},
				},
				renderer:function (v, metaData, record)
				{
					if ( record.data.cito=='0')
					{
					record.data.cito='Tidak'
					}else if (record.data.cito=='1')
					{
					metaData.style  ='background:#FF0000;  "font-weight":"bold";';
					record.data.cito='Ya'
					}else if (record.data.cito=='Ya')
					{
					metaData.style  ='background:#FF0000;  "font-weight":"bold";';
					}
					
					return record.data.cito; 
				},
				editor:new Ext.form.ComboBox
				({
					id: 'cboKasus',
					typeAhead: true,
					triggerAction: 'all',
					lazyRender: true,
					mode: 'local',
					selectOnFocus: true,
					forceSelection: true,
					emptyText: 'Silahkan Pilih...',
					width: 50,
					anchor: '95%',
					value: 1,
					store: new Ext.data.ArrayStore({
						id: 0,
						fields: ['Id', 'displayText'],
						data: [[1, 'Ya'], [2, 'Tidak']]
					}),
					valueField: 'displayText',
					displayField: 'displayText',
					value		: '',
					   
				})
			},
			{
                header: 'kd_tarif',
                dataIndex: 'kd_tarif',
                width:250,
				menuDisabled:true,
				hidden :true

            },
            {
                header: 'Kode Produk',
                dataIndex: 'kd_produk',
                width:100,
                menuDisabled:true,
                hidden:true
            },
            /* {
                header:'Nama Produk',
                dataIndex: 'deskripsi',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320

            }, */
			{	id:'nama_pemereksaaan_rad',
				dataIndex: 'deskripsi',
				header: 'Nama Pemeriksaan',
				sortable: true,
				menuDisabled:true,
				width: 320,
				/* editor:new Nci.form.Combobox.autoComplete({
					store	: TrPenJasRad.form.ArrayStore.produk,
					select	: function(a,b,c){
					//	console.log(b);
					//Disini
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/main/functionRAD/cekProduk",
								params:{kd_rad:b.data.kd_produk} ,
								failure: function(o)
								{
									ShowPesanErrorPenJasRad('Hubungi Admin', 'Error');
								},
								success: function(o)
								{
									Ext.getCmp('btnHpsBrsItemRadL').enable();
									console.log("test");
									var cst = Ext.decode(o.responseText);
									if (cst.success === true)
									{
										var line = gridDTItemTest.getSelectionModel().selection.cell[0];
										dsTRDetailPenJasRadList.data.items[line].data.deskripsi=b.data.deskripsi;
										dsTRDetailPenJasRadList.data.items[line].data.uraian=b.data.uraian;
										dsTRDetailPenJasRadList.data.items[line].data.kd_tarif=b.data.kd_tarif;
										dsTRDetailPenJasRadList.data.items[line].data.kd_produk=b.data.kd_produk;
										dsTRDetailPenJasRadList.data.items[line].data.tgl_transaksi=b.data.tgl_transaksi;
										dsTRDetailPenJasRadList.data.items[line].data.tgl_berlaku=b.data.tgl_berlaku;
										dsTRDetailPenJasRadList.data.items[line].data.harga=b.data.harga;
										dsTRDetailPenJasRadList.data.items[line].data.qty=b.data.qty;
										dsTRDetailPenJasRadList.data.items[line].data.jumlah=b.data.jumlah;

										gridDTItemTest.getView().refresh();
									}
									else
									{
										ShowPesanInfoPenJasRad('Nilai normal item '+b.data.deskripsi+' belum tersedia', 'Information');
									};
								}
							}

						)
						
					},
					insert	: function(o){
						return {
							uraian        	: o.uraian,
							kd_tarif 		: o.kd_tarif,
							kd_produk		: o.kd_produk,
							tgl_transaksi	: o.tgl_transaksi,
							tgl_berlaku		: o.tgl_berlaku,
							harga			: o.harga,
							qty				: o.qty,
							deskripsi		: o.deskripsi,
							jumlah			: o.jumlah,
							text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_produk+'</td><td width="150">'+o.deskripsi+'</td></tr></table>'
						}
					},
					param	: function(){
					var kd_cus_gettarif;
					if(Ext.get('cboKelPasienRad').getValue()=='Perseorangan'){
						kd_cus_gettarif=Ext.getCmp('cboPerseoranganRad').getValue();
					}else if(Ext.get('cboKelPasienRad').getValue()=='Perusahaan'){
						kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryRad').getValue();
					}else {
						kd_cus_gettarif=Ext.getCmp('cboAsuransiRad').getValue();
						  }
					var params={};
					params['kd_unit']=kodeUnitRad;
					params['kd_customer']=kd_cus_gettarif;
					return params;
					},
					url		: baseURL + "index.php/main/functionRAD/getProduk",
					valueField: 'deskripsi',
					displayField: 'text',
					listWidth: 210
				}) */
			},
            {
				header: 'Tanggal Transaksi',
				dataIndex: 'tgl_transaksi',
				width: 130,
				menuDisabled:true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_transaksi == undefined){
						record.data.tgl_transaksi=tglGridBawah;
						return record.data.tgl_transaksi;
					} else{
						if(record.data.tgl_transaksi.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_transaksi);
						} else{
							var tgl=record.data.tgl_transaksi.split("/");

							if(tgl[2].length == 4 && isNaN(tgl[1])){
								return record.data.tgl_transaksi;
							} else{
								return ShowDate(record.data.tgl_transaksi);
							}
						}

					}
				}
            },
			{
				header: 'Tanggal Berlaku',
				dataIndex: 'tgl_berlaku',
				width: 130,
				menuDisabled:true,
				hidden: true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_berlaku == undefined || record.data.tgl_berlaku == null){
						record.data.tgl_berlaku=tglGridBawah;
						return record.data.tgl_berlaku;
					} else{
						if(record.data.tgl_berlaku.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_berlaku);
						} else{
							var tglb=record.data.tgl_berlaku.split("-");

							if(tglb[2].length == 4 && isNaN(tglb[1])){
								return record.data.tgl_berlaku;
							} else{
								return ShowDate(record.data.tgl_berlaku);
							}
						}

					}
				}
            },
            {
                header: 'Harga',
                align: 'right',
                hidden: false,
                menuDisabled:true,
                dataIndex: 'harga',
                width:100,
				renderer: function(v, params, record)
				{
					return formatCurrency(record.data.harga);
				}
            },
            {	id:'qty_rad',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'qty',
                editor: new Ext.form.NumberField (
					{
					
					}
                   /*  {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
						{
							'specialkey' : function()
							{
								Dataupdate_PenJasRad(false);
							}
						}
                    } */
                ),



            },
			 {
                header: 'Dokter',
                width:91,
				menuDisabled:true,
                dataIndex: 'jumlah',
				hidden: true,

            },

        ]
    )
};

/* function TRPenJasRadColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsirwj',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
                menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiRWJ',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320
                
            }
			,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
               menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colHARGARWj',
                header: 'Harga',
                align: 'right',
                hidden: true,
                menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },
            {
                id: 'colProblemRWJ',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
							{ 
								'specialkey' : function()
								{
									
											Dataupdate_PenJasRad(false);

								}
							}
                    }
                ),
				
				
              
            },

            {
                id: 'colImpactRWJ',
                header: 'CR',
                width:80,
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactRWJ',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
				
            }

        ]
    )
}; */


function RecordBaruRWJ()
{
	var tgltranstoday = Ext.getCmp('dtpKunjunganL').getValue();
	var p = new mRecordRwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tgltranstoday.format('Y/m/d'), 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
var kd_kasir_rad;
//----------MEMASUKAN DATA YG DIPILIH DARI DATA GRID KEDALAM LOOKUP EDIT DATA PASIEN
function TRRWJInit(rowdata)
{
	txtUmurRad
    tmpurut = rowdata.URUT;
    AddNewPenJasRad = false;
	
	if(Ext.get('cboJenisTr_viPenJasRad').getValue()=='Transaksi Lama'){
		Ext.get('txtNoTransaksiPenJasRad').dom.value = rowdata.NO_TRANSAKSI;
	}
	// console.log(rowdata);
	TmpNotransaksi=rowdata.NO_TRANSAKSI;
	KdKasirAsal=rowdata.KD_KASIR;

	TglTransaksi=rowdata.TGL_TRANSAKSI;
	Kd_Spesial=rowdata.KD_SPESIAL;
	No_Kamar=rowdata.KAMAR;
	kodeunit=rowdata.KD_UNIT;
	kodeunittransfer=rowdata.KD_UNIT;
	if (combovalues==='Transaksi Lama' )
	{
		ViewGridBawahLookupPenjasRad(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
		Ext.getCmp('btnPembayaranPenjasRAD').enable();
		Ext.getCmp('btnTutupTransaksiPenjasRAD').enable();
		Ext.getCmp('btnHpsBrsKasirRAD').enable();
		RefreshDatahistoribayar(rowdata.NO_TRANSAKSI);
		kodeUnitRad=rowdata.KD_UNIT_ASAL;
		if (rowdata.LUNAS==='t')
		{
			Ext.getCmp('btnPembayaranPenjasRAD').enable();
			//Ext.getCmp('btnHpsBrsKasirRAD').disable();
			Ext.getCmp('btnTutupTransaksiPenjasRAD').disable();
		}
	}
	else
	{
		Ext.getCmp('btnPembayaranPenjasRAD').enable();
		Ext.getCmp('btnHpsBrsKasirRAD').disable();
		Ext.getCmp('btnTutupTransaksiPenjasRAD').disable();
		kodeUnitRad=rowdata.KD_UNIT;
	}
	var modul='';
	if(radiovalues == 1){
		modul='igd';
	} else if(radiovalues == 2){
		modul='rwi';
	} else if(radiovalues == 4){
		modul='rwj';
	} else{
		modul='langsung';
	}
	Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/cekPembayaran",
        params: {
            notrans: rowdata.NO_TRANSAKSI,
			Modul:modul
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst.ListDataObj);
			kodepasien = rowdata.KD_PASIEN;
			namapasien = rowdata.NAMA;
			console.log(rowdata.KD_UNIT);
			//kodeUnitRad = rowdata.KD_UNIT;
			namaunit = rowdata.NAMA_UNIT;
			kodepay = cst.ListDataObj[0].kd_pay;
			uraianpay = cst.ListDataObj[0].cara_bayar;
			kdcustomeraa = rowdata.KD_CUSTOMER;
			/* loaddatastorePembayaran(cst.ListDataObj[0].jenis_pay);
			Ext.get('cboPembayaran').dom.value =cst.ListDataObj[0].ket_payment;
			Ext.get('cboJenisByr').dom.value =cst.ListDataObj[0].cara_bayar; 
			vflag = cst.ListDataObj[0].flag;
			tapungkd_pay = cst.ListDataObj[0].kd_pay; */
			
        }

    });
	
	Ext.get('dtpTtlL').dom.value=ShowDate(rowdata.TGL_LAHIR);
	Ext.get('txtNoMedrecL').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienL').dom.value = rowdata.NAMA;
	Ext.get('txtAlamatL').dom.value = rowdata.ALAMAT;
	Ext.get('txtKdDokter').dom.value   = rowdata.KD_DOKTER;
	
	// if (rowdata.NAMA_UNIT_ASLI==='')
	// {
	// 	Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT;
	// }
	// else
	// {
	// 	Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT_ASLI;
	// }
	
	Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
	// console.log(rowdata);
	console.log(rowdata.KD_UNIT);
	//kodeUnitRad=rowdata.KD_UNIT;
			
	Ext.get('cboJK').dom.value = rowdata.JENIS_KELAMIN;
	if (rowdata.JENIS_KELAMIN === 't')
	{
		Ext.get('cboJK').dom.value= 'Laki-laki';
	}else
	{
		Ext.get('cboJK').dom.value= 'Perempuan'
	}
	Ext.getCmp('cboJK').disable();
	
	Ext.get('cboGDR').dom.value = rowdata.GOL_DARAH;
	if (rowdata.GOL_DARAH === '0')
	{
		Ext.get('cboGDR').dom.value= '-';
	}else if (rowdata.GOL_DARAH === '1')
	{
		Ext.get('cboGDR').dom.value= 'A+'
	}else if (rowdata.GOL_DARAH === '2')
	{
		Ext.get('cboGDR').dom.value= 'B+'
	}else if (rowdata.GOL_DARAH === '3')
	{
		Ext.get('cboGDR').dom.value= 'AB+'
	}else if (rowdata.GOL_DARAH === '4')
	{
		Ext.get('cboGDR').dom.value= 'O+'
	}else if (rowdata.GOL_DARAH === '5')
	{
		Ext.get('cboGDR').dom.value= 'A-'
	}else if (rowdata.GOL_DARAH === '6')
	{
		Ext.get('cboGDR').dom.value= 'B-'
	}
	Ext.getCmp('cboGDR').disable();
	
	//alert(rowdata.KD_CUSTOMER);
	Ext.get('txtKdCustomerLamaHide').dom.value= rowdata.KD_CUSTOMER;//tampung kd customer untuk transaksi selain kunjungan langsung
	vkode_customer = rowdata.KD_CUSTOMER;
	Ext.getCmp('cboKelPasienRad').disable();
	
	Ext.get('cboKelPasienRad').dom.value= rowdata.KELPASIEN;
	Ext.getCmp('cboKelPasienRad').enable();
	Ext.get('txtCustomerLamaHide').hide();
	if(rowdata.KELPASIEN == 'Perseorangan'){
		Ext.getCmp('cboPerseoranganRad').setValue(rowdata.CUSTOMER);
		Ext.getCmp('cboPerseoranganRad').show();
	} else if(rowdata.KELPASIEN == 'Perusahaan'){
		Ext.getCmp('cboPerusahaanRequestEntryRad').setValue(rowdata.CUSTOMER);
		Ext.getCmp('cboPerusahaanRequestEntryRad').show();
	} else{
		Ext.getCmp('cboAsuransiRad').setValue(rowdata.CUSTOMER);
		Ext.getCmp('cboAsuransiRad').show();
	} 
	
    if(tmppasienbarulama == 'Baru'){
        Ext.getCmp('txtDokterPengirimL').setValue(rowdata.DOKTER);
        Ext.getCmp('cboDOKTER_viPenJasRad').setValue('');
        Ext.getCmp('cboUnitRad_viPenJasRad').setValue('');
        Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT_ASLI;
        tmpkd_unit = '';
        tmpkddoktertujuan = '';
        Ext.get('txtKdUnitRad').dom.value   = rowdata.KD_UNIT;
        kd_kasir_rad = '';
    }else{
        Ext.getCmp('txtDokterPengirimL').setValue(rowdata.DOKTER_ASAL);
        Ext.getCmp('cboDOKTER_viPenJasRad').setValue(rowdata.DOKTER);
        Ext.getCmp('cboUnitRad_viPenJasRad').setValue(rowdata.NAMA_UNIT_ASLI);
        Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT_ASAL;
        tmpkd_unit = rowdata.KD_UNIT;
        tmpkddoktertujuan = rowdata.KD_DOKTER;
        Ext.get('txtKdUnitRad').dom.value   = rowdata.KD_UNIT_ASAL;
        kd_kasir_rad = rowdata.KD_KASIR;
    }
	// getDokterPengirim(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
	Ext.getCmp('txtnotlprad').setValue(rowdata.HP);
    // Ext.getCmp('txtnoseprad').setValue(rowdata.SJP);
	
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText
			
	    }
		
	
	});
	
	//---------------------EDIT DEFAULT UNIT RAD 31 01 2017
	Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/getDefaultUnit",
        params: {
            notrans: rowdata.NO_TRANSAKSI,
			Modul:modul
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
			var cst = Ext.decode(o.responseText);
			Ext.getCmp('cboUnitRad_viPenJasRad').setValue(cst.kd_unit);
			//Ext.getCmp('cbounitrads_viPenJasRad').setValue(cst.kd_unit);
			//combovaluesunittujuan=cst.kd_unit;
			tmpkd_unit = cst.kd_unit;
        }

    });
	//,,
	//alert(tampungshiftsekarang);
	setUsia(ShowDate(rowdata.TGL_LAHIR));
};


function RWJAddNew() 
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			tampungshiftsekarang=o.responseText;
		}
	});
    AddNewPenJasRad = true;
	Ext.get('txtNoTransaksiPenJasRad').dom.value = '';
	Ext.get('txtNoMedrecL').dom.value='';
	Ext.get('txtNamaPasienL').dom.value = '';
	Ext.get('txtKdDokter').dom.value   = undefined;
	Ext.get('txtNamaUnit').dom.value   = 'Radiologi';
	Ext.getCmp('txtDokterPengirimL').setValue('DOKTER LUAR');
	Ext.get('txtKdUrutMasuk').dom.value = '';
	rowSelectedPenJasRad=undefined;
	dsTRDetailPenJasRadList.removeAll();
	
	databaru = 1;
	Ext.get('txtNamaUnit').readOnly;

};

function RefreshDataPenJasRadDetail(no_transaksi) 
{
    var strKriteriaRWJ='';
    //strKriteriaRWJ = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaRWJ = "\"no_transaksi\" = ~" + no_transaksi + "~" + " And kd_unit ='4'";
    //strKriteriaRWJ = 'no_transaksi = ~0000004~';
   
    dsTRDetailPenJasRadList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailRWJGridBawah',
			    param: strKriteriaRWJ
			}
		}
	);
    return dsTRDetailPenJasRadList;
};

//tampil grid bawah penjas rad
function ViewGridBawahLookupPenjasRad(no_transaksi,kd_kasir) 
{
	
    var strKriteriaRWJ='';
    if (kd_kasir !== undefined) {
        strKriteriaRWJ = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
    }else{
        strKriteriaRWJ = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
    }
    
    // strKriteriaRWJ = "\"no_transaksi\" = ~" + no_transaksi + "~" ;//+ " And kd_kasir ='03'";
 
   
    dsTRDetailPenJasRadList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailGridBawahPenJasLab',
			    param: strKriteriaRWJ
			}
		}
	);
    return dsTRDetailPenJasRadList;
};

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiRAD() 
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/main/functionRAD/getCurrentShiftRad",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			tampungshiftsekarang=o.responseText;
		}
	});
	var KdCust='';
	var TmpCustoLama='';
	var pasienBaru;
	var modul='';
	if(radiovalues == 1){
		modul='igd';
	} else if(radiovalues == 2){
		modul='rwi';
	} else if(radiovalues == 4){
		modul='rwj';
	} else{
		modul='langsung';
	}
	/* if(Ext.get('cboKelPasienRad').getValue()=='Perseorangan'){
		KdCust=Ext.getCmp('cboPerseoranganRad').getValue();
	}else if(Ext.get('cboKelPasienRad').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('cboPerusahaanRequestEntryRad').getValue();
	}else {
		KdCust=Ext.getCmp('cboAsuransiRad').getValue();
	} */
	
	if(Ext.getCmp('txtNoMedrecL').getValue() == ''){
		pasienBaru=1;
	} else{
		pasienBaru=0;
	}
	
    var params =
	{
		//Table:'ViewDetailTransaksiPenJasRad',
		
		KdTransaksi: Ext.get('txtNoTransaksiPenJasRad').getValue(),
		KdPasien:Ext.getCmp('txtNoMedrecL').getValue(),
		NmPasien:Ext.getCmp('txtNamaPasienL').getValue(),
		Ttl:Ext.getCmp('dtpTtlL').getValue(),
		Alamat:Ext.getCmp('txtAlamatL').getValue(),
		JK:Ext.getCmp('cboJK').getValue(),
		GolDarah:Ext.getCmp('cboGDR').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitRad').getValue(),
		KdDokter:tmpkddoktertujuan,
		KdUnitTujuan:tmpkd_unit,
		Tgl: Ext.getCmp('dtpKunjunganL').getValue(),
		TglTransaksiAsal:TglTransaksi,
		urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
		KdCusto:vkode_customer,
		TmpCustoLama:vkode_customer,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
		NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiRad').getValue(),
		NoAskes:Ext.getCmp('txtNoAskesRad').getValue(),
		NoSJP:Ext.getCmp('txtNoSJPRad').getValue(),
		Shift:tampungshiftsekarang,
		List:getArrPenJasRad(),
		JmlField: mRecordRwj.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		dtBaru:databaru,
		Hapus:1,
		Ubah:0,
		unit:52,
		TmpNotransaksi:TmpNotransaksi,
		KdKasirAsal:KdKasirAsal,
		KdSpesial:Kd_Spesial,
		Kamar:No_Kamar,
		pasienBaru:pasienBaru,
		listTrDokter	: '',
		Modul:modul,
		KdProduk:'',//sTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.KD_PRODUK
        URUT :tmpurut,
        no_reg:Ext.getCmp('txtnoregrad').getValue()
		
	};
    return params
};

function getParamKonsultasi() 
{

    var params =
	{
		
		Table:'ViewDetailTransaksiPenJasRad', //data access listnya belum dibuat
		
		//TrKodeTranskasi: Ext.get('txtNoTransaksiPenJasRad').getValue(),
		KdUnitAsal : Ext.get('txtKdUnitRad').getValue(),
		KdDokterAsal : Ext.get('txtKdDokter').getValue(),
		KdUnit: selectKlinikPoli,
		KdDokter:selectDokter,
		KdPasien:Ext.get('txtNoMedrecL').getValue(),
		TglTransaksi : Ext.get('dtpKunjunganL').dom.value,
		KDCustomer :vkode_customer,
	};
    return params
};

function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailPenJasRadList.getCount();i++)
	{
		if (dsTRDetailPenJasRadList.data.items[i].data.KD_PRODUK != '' || dsTRDetailPenJasRadList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};


function getArrPenJasRad()
{
	var x='';
	var arr=[];
	for(var i = 0 ; i < dsTRDetailPenJasRadList.getCount();i++)
	{
		if (dsTRDetailPenJasRadList.data.items[i].data.kd_produk != '' && dsTRDetailPenJasRadList.data.items[i].data.deskripsi != '')
		{
			var o={};
			var y='';
			var z='@@##$$@@';
			o['URUT']= dsTRDetailPenJasRadList.data.items[i].data.urut;
			o['KD_PRODUK']= dsTRDetailPenJasRadList.data.items[i].data.kd_produk;
			o['QTY']= dsTRDetailPenJasRadList.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= dsTRDetailPenJasRadList.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= dsTRDetailPenJasRadList.data.items[i].data.tgl_berlaku;
			o['HARGA']= dsTRDetailPenJasRadList.data.items[i].data.harga;
			o['KD_TARIF']= dsTRDetailPenJasRadList.data.items[i].data.kd_tarif;
			o['cito']= dsTRDetailPenJasRadList.data.items[i].data.cito;
            o['kd_unit']= dsTRDetailPenJasRadList.data.items[i].data.kd_unit;
			arr.push(o);
		};
	}	
	
	return Ext.encode(arr);
};

//panel dalam lookup detail pasien
function getItemPanelPenJasRad(lebar) 
{
	//pengaturan panel data pasien
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
		bodyStyle: 'padding: 5px 5px 5px 5px',
	    border:false,
	    height:225,
	    items:
		[
					{ //awal panel biodata pasien
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 5px 5px 5px 5px',
						border: true,
						width: 100,
						height: 120,
						anchor: '100% 100%',
						items:
						[
                            //bagian pinggir kiri                            
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Transaksi  '
							},
							{
								x: 100,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x : 110,
								y : 10,
								xtype: 'textfield',
								name: 'txtNoTransaksiPenJasRad',
								id: 'txtNoTransaksiPenJasRad',
								width: 100,
								emptyText:nmNomorOtomatis,
								readOnly:true

							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'No. Medrec  '
							},
							{
								x: 100,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 110,
								y: 40,
								xtype: 'textfield',
								fieldLabel: 'No. Medrec',
								name: 'txtNoMedrecL',
								id: 'txtNoMedrecL',
								readOnly:true,
								width: 120,
								emptyText:'Otomatis',
								listeners: 
								{ 
									
								}
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Unit  '
							},
							{
								x: 100,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 110,
								y: 70,
								xtype: 'textfield',
								name: 'txtNamaUnit',
								id: 'txtNamaUnit',
								readOnly:true,
								width: 120
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Dokter Pengirim'
							},
							{
								x: 100,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 110,
								y: 100,
								xtype: 'textfield',
								fieldLabel: '',
								readOnly:true,
								name: 'txtDokterPengirimL',
								id: 'txtDokterPengirimL',
								width: 200
							},
							{
								x: 10,
								y: 190,
								xtype: 'label',
								text: 'Unit Rad'
							},
							{
								x: 100,
								y: 190,
								xtype: 'label',
								text: ':'
							},
							mComboUnitRad(),
                            // {
                            //     x: 360,
                            //     y: 190,
                            //     xtype: 'label',
                            //     text: 'No. SEP'
                            // },
                            // {
                            //     x: 400,
                            //     y: 190,
                            //     xtype: 'label',
                            //     text: ':'
                            // },
                            // {
                            //     x: 410,
                            //     y: 185,
                            //     xtype: 'textfield',
                            //     fieldLabel: '',
                            //     readOnly:true,
                            //     name: 'txtnoseprad',
                            //     id: 'txtnoseprad',
                            //     width: 200
                            // },
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Dokter Rad'
							},
							{
								x: 100,
								y: 130,
								xtype: 'label',
								text: ':'
							},
							mComboDOKTER(),
                            {
                                x: 360,
                                y: 130,
                                xtype: 'label',
                                text: 'No. Tlp'
                            },
                            {
                                x: 400,
                                y: 130,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 410,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtnotlprad',
                                id: 'txtnotlprad',
                                width: 150
                            },
							//bagian tengah
							{
								x: 260,
								y: 10,
								xtype: 'label',
								text: 'Tanggal Kunjung '
							},
							{
								x: 350,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 360,
								y: 10,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'dtpKunjunganL',
								name: 'dtpKunjunganL',
								format: 'd/M/Y',
								readOnly : true,
								value: now,
								width: 110
							},
                            {
                                x: 480,
                                y: 10,
                                xtype: 'label',
                                text: 'No. Reg : '
                            },
                            {
                                x: 530,
                                y: 10,
                                xtype: 'textfield',
                                id: 'txtnoregrad',
                                name: 'txtnoregrad',
                                readOnly : true,
                                width: 110
                            },
                            {
                                x: 650,
                                y: 10,
                                xtype: 'label',
                                text: 'No. Foto : '
                            },
                            {
                                x: 705,
                                y: 10,
                                xtype: 'textfield',
                                id: 'txtnofotorad',
                                name: 'txtnofotorad',
                                readOnly : true,
                                width: 110
                            },
							{
								x: 260,
								y: 40,
								xtype: 'label',
								text: 'Nama Pasien '
							},
							{
								x: 350,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 360,
								y: 40,
								xtype: 'textfield',
								fieldLabel: '',
								readOnly:true,
								name: 'txtNamaPasienL',
								id: 'txtNamaPasienL',
								width: 200
							},
							{
								x: 260,
								y: 70,
								xtype: 'label',
								text: 'Alamat '
							},
							{
								x: 350,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 360,
								y: 70,
								xtype: 'textfield',
								fieldLabel: '',
								readOnly:true,
								name: 'txtAlamatL',
								id: 'txtAlamatL',
								width: 395
							},
							{
								x: 360,
								y: 100,
								xtype: 'label',
								text: 'Jenis Kelamin '
							},
							{
								x: 445,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							mComboJK(),
							{
								x: 570,
								y: 100,
								xtype: 'label',
								text: 'Gol. Darah '
							},
							{
								x: 640,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							mComboGDarah(),
							{
								x: 570,
								y: 40,
								xtype: 'label',
								text: 'Tanggal lahir '
							},
							{
								x: 640,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 645,
								y: 40,
								xtype: 'datefield',
								fieldLabel: 'Tanggal ',
								id: 'dtpTtlL',
								name: 'dtpTtlL',
								format: 'd/M/Y',
								readOnly : true,
								value: now,
								width: 110
							},
							{
								x: 765,
								y: 40,
								xtype: 'label',
								text: 'Umur '
							},
							{
								x: 795,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 805,
								y: 40,
								xtype: 'textfield',
								fieldLabel: '',
								name: 'txtUmurRad',
								id: 'txtUmurRad',
								width: 30,
								//anchor: '95%',
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
												if (Ext.EventObject.getKey() === 13)
                                                {
													Ext.Ajax.request
													(
															{
																url: baseURL + "index.php/main/functionLAB/cekUsia",
																params: {umur: Ext.get('txtUmurRad').getValue()},
																failure: function (o)
																{
																	ShowPesanErrorPenJasRad('Proses Error ! ', 'Radiologi');
																},
																success: function (o)
																{
																	var cst = Ext.decode(o.responseText);
																	if (cst.success === true)
																	{
																		Ext.getCmp('dtpTtlL').setValue(cst.tahunumur);
																	}
																	else
																	{
																		ShowPesanErrorPenJasRad('Proses Error ! ', 'Radiologi');
																	}
																}
															}
													)
												}
											}
										}
							},
							{
								x: 10,
								y: 160,
								xtype: 'label',
								text: 'Kelompok Pasien '
							},
							{
								x: 100,
								y: 160,
								xtype: 'label',
								text: ':'
							},
							mCboKelompokpasien(),
							mComboPerseorangan(),
							mComboPerusahaan(),
							mComboAsuransi(),
							{
								x: 410,
								y: 160,
								xtype: 'textfield',
								fieldLabel: 'Nama Peserta',
								maxLength: 200,
								name: 'txtNamaPesertaAsuransiRad',
								id: 'txtNamaPesertaAsuransiRad',
								emptyText:'Nama Peserta Asuransi',
								width: 150
							},
							{
								x: 250,
								y: 190,
								xtype: 'textfield',
								fieldLabel: 'No. Askes',
								maxLength: 200,
								name: 'txtNoAskesRad',
								id: 'txtNoAskesRad',
								emptyText:'No Askes',
								width: 150
							},
							{
								x: 410,
								y: 190,
								xtype: 'textfield',
								fieldLabel: 'No. SJP',
								maxLength: 200,
								name: 'txtNoSJPRad',
								id: 'txtNoSJPRad',
								emptyText:'No SJP',
								width: 150
							},
							{
								x: 110,
								y: 130,
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtCustomerLamaHide',
								id: 'txtCustomerLamaHide',
								readOnly:true,
								width: 280
							},
							{
								x: 110,
								y: 130,
								xtype: 'textfield',
								fieldLabel:'Kode customer  ',
								name: 'txtKdCustomerLamaHide',
								id: 'txtKdCustomerLamaHide',
								readOnly:true,
								hidden:true,
								width: 280
							},
							
							
							
							//---------------------hidden----------------------------------
							
							{
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtKdDokter',
								id: 'txtKdDokter',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							
							{
								xtype: 'textfield',
								fieldLabel:'Unit  ',
								name: 'txtKdUnitRad',
								id: 'txtKdUnitRad',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								name: 'txtKdUrutMasuk',
								id: 'txtKdUrutMasuk',
								readOnly:true,
								hidden:true,
								anchor: '100%'
							}
								
							//-------------------------------------------------------------
							
						]
					},	//akhir panel biodata pasien
			
		]
	};
    return items;
};



function getItemPanelUnit(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Unit  ',
					    name: 'txtKdUnitRad',
					    id: 'txtKdUnitRad',
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'textfield',
					    name: 'txtNamaUnit',
					    id: 'txtNamaUnit',
						readOnly:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function refeshpenjasrad(kriteria)
{
    /* dsTRPenJasRadList.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPenJasRad',
                    param : kriteria
                }			
            }
        );   
    return dsTRPenJasRadList; */
	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/rad/viewpenjasrad/getPasien",
			params: getParamRefreshRad(kriteria),
			failure: function(o)
			{
				ShowPesanWarningPenJasRad('Hubungi Admin', 'Error');
			},
			success: function(o)
			{   
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					dsTRPenJasRadList.removeAll();
					var recs=[],
					recType=dsTRPenJasRadList.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsTRPenJasRadList.add(recs);
					grListPenJasRad.getView().refresh();

				}
				else
				{
					ShowPesanWarningPenJasRad('Gagal membaca Data ini', 'Error');
				};
			}
		}

	);
	Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/getDefaultUnit",
        params: {
            notrans: 'no',
			Modul:'no'
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
			var cst = Ext.decode(o.responseText);
			
			//Ext.getCmp('cbounitrads_viPenJasRad').setValue(cst.kd_unit);
			combovaluesunittujuan=cst.kd_unit;
			
        }

    });
}


function getParamRefreshRad(krite){
	var unit;
	if(radiovalues == 1){
		unit = 'IGD';
	} else if(radiovalues == 2){
		unit = 'RWI';
	} else if(radiovalues == 3){
		unit = 'Langsung';
	} else{
		unit = 'RWJ';
	}
	var params =
	{
		unit:unit,
		kriteria:krite
		/* tglAwal:
		tglAkhir:dtpTglAwalFilterPenJasRad */
		
	}
	return params;
}

function loadpenjasrad(tmpparams, tmpunit)
{
    dsTRPenJasRadList.load
		(
                    { 
                        params:  
                        {   
                            Skip: 0, 
                            Take: '',
                            Sort: '',
                            Sortdir: 'ASC', 
                            target: tmpunit,
                            param : tmpparams
                        }			
                    }
		);   
    return dsTRPenJasRadList;
}

function Datasave_PenJasRad(mBol) 
{	
	 if (ValidasiEntryPenJasRad(nmHeaderSimpanData,false) == 1 )
	 {
			Ext.Ajax.request
			 (
				{
					url: baseURL + "index.php/main/functionRAD/savedetailpenyakit",
					params: getParamDetailTransaksiRAD(),
					failure: function(o)
					{
						ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
						ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,kd_kasir_rad);
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
                            tmpkd_pasien_sql = cst.kdPasien;
                            tmpno_trans_sql = cst.notrans;
                            tmpkd_kasir_sql = cst.kdkasir;
                            tmp_tgl_sql = cst.tgl;
                            tmp_urut_sql = cst.urut;
                            tmpurut = cst.urut;
                            kd_kasir_rad = cst.kdkasir;
							loadMask.hide();
							ShowPesanInfoPenJasRad("Berhasil menyimpan data ini","Information");
							Ext.get('txtNoTransaksiPenJasRad').dom.value=cst.notrans;
							Ext.get('txtNoMedrecL').dom.value=cst.kdPasien;
							ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,tmpkd_kasir_sql);
							Ext.getCmp('btnPembayaranPenjasRAD').enable();
							Ext.getCmp('btnTutupTransaksiPenjasRAD').enable();
                            Ext.getCmp('txtnoregrad').setValue(cst.noreg);
							if(mBol === false)
							{
								ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,tmpkd_kasir_sql);
							};

                            
                            Datasave_PenJasRad_SQL();
						}
						else 
						{
								ShowPesanWarningPenJasRad('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	 }
	 else
	{
		if(mBol === true)
		{
			return false;
		};
	}; 
	
};

var tmpkd_pasien_sql = '';
var tmpno_trans_sql = '';
var tmpkd_kasir_sql = '';
var tmp_tgl_sql = '';
var tmp_urut_sql = '';


function Dataupdate_PenJasRad(mBol) 
{	
	if (ValidasiEntryPenJasRad(nmHeaderSimpanData,false) == 1 )
	{
		
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",
					params: getParamDataupdatePenJasRadDetail(),
					success: function(o) 
					{
						ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,kd_kasir_rad);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoPenJasRad(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataPenJasRad();
							if(mBol === false)
							{
								ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,kd_kasir_rad);
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningPenJasRad(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningPenJasRad(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorPenJasRad(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};
function ValidasiEntryPenJasRad(modul,mBolHapus)
{
	var x = 1;
	//cboUnitRad_viPenJasRad
	if((Ext.get('cboUnitRad_viPenJasRad').getValue() == '') || (Ext.get('txtNamaPasienL').getValue() == '') || (Ext.get('cboDOKTER_viPenJasRad').getValue() == 'Pilih Dokter') || (Ext.get('dtpKunjunganL').getValue() == '') || (dsTRDetailPenJasRadList.getCount() === 0 ))
	{
		if (Ext.get('txtNoTransaksiPenJasRad').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('txtNoMedrecL').getValue() == '') 
		{
			ShowPesanWarningPenJasRad('No. Medrec tidak boleh kosong!','Warning');
			x = 0;
		}
		else if (Ext.get('cboUnitRad_viPenJasRad').getValue() == '') 
		{
			ShowPesanWarningPenJasRad('No. Unit Radiologi Wajib diisi!','Warning');
			x = 0;
		}
		else if (Ext.get('txtNamaPasienL').getValue() == '') 
		{
			ShowPesanWarningPenJasRad('Nama tidak boleh kosong!','Warning');
			x = 0;
		}
		else if (Ext.get('dtpKunjunganL').getValue() == '') 
		{
			ShowPesanWarningPenJasRad('Tanggal kunjungan tidak boleh kosong!','Warning');
			x = 0;
		}
		else if (Ext.get('cboDOKTER_viPenJasRad').getValue() == 'Pilih Dokter' || Ext.get('cboDOKTER_viPenJasRad').dom.value  === undefined) 
		{
			ShowPesanWarningPenJasRad('Dokter Radiologi tidak boleh kosong!','Warning');
			x = 0;
		}
		else if (dsTRDetailPenJasRadList.getCount() === 0) 
		{
			ShowPesanWarningPenJasRad('Daftar item test tidak boleh kosong!','Warning');
			x = 0;
		};
	};
	return x;
};

function ShowPesanWarningPenJasRad(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPenJasRad(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPenJasRad(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function DataDeletePenJasRad() 
{
   if (ValidasiEntryPenJasRad(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                //url: "./Datapool.mvc/DeleteDataObj",
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiRAD(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoPenJasRad(nmPesanHapusSukses,nmHeaderHapusData);
                                        //RefreshDataPenJasRad();
                                        RWJAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningPenJasRad(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningPenJasRad(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorPenJasRad(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};

function RefreshDatacombo(jeniscus) 
{

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customer+'~)'
            }
        }
    )
	
    // rowSelectedPenJasRad = undefined;
    return ds_customer_viDaftar;
};
function mComboKelompokpasien()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    )
    var cboKelompokpasien = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: radelisi,
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien;
};

function getKelompokpasienlama(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
	//	title: 'Kelompok Pasien Lama',
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
				//title: 'Kelompok Pasien Lama',
			    items:
				[
					{	 
						xtype: 'tbspacer',
						height: 2
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Kelompok Pasien Asal',
						//maxLength: 200,
						name: 'txtCustomerLama',
						id: 'txtCustomerLama',
						labelWidth:130,
						width: 100,
						anchor: '95%'
					},
					
					
				]
			}
			
		]
	}
    return items;
};


function getItemPanelNoTransksiKelompokPasien(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[
					
					
					{	 
						xtype: 'tbspacer',
						
						height:3
					},
						{  

						xtype: 'combo',
						fieldLabel: 'Kelompok Pasien Baru',
						id: 'kelPasien',
						 editable: false,
						//value: 'Perseorangan',
						store: new Ext.data.ArrayStore
							(
								{
								id: 0,
								fields:
								[
								'Id',
								'displayText'
								],
								   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
								}
							),
							  displayField: 'displayText',
							  mode: 'local',
							  width: 100,
							  forceSelection: true,
							  triggerAction: 'all',
							  emptyText: 'Pilih Salah Satu...',
							  selectOnFocus: true,
							  anchor: '95%',
							  listeners:
								{
										'select': function(a, b, c)
									{
										if(b.data.displayText =='Perseorangan')
										{
											jeniscus='0'
											Ext.getCmp('txtNoSJP').disable();
											Ext.getCmp('txtNoAskes').disable();}
										else if(b.data.displayText =='Perusahaan')
										{
											jeniscus='1';
											Ext.getCmp('txtNoSJP').disable();
											Ext.getCmp('txtNoAskes').disable();}
										else if(b.data.displayText =='Asuransi')
										{
											jeniscus='2';
											Ext.getCmp('txtNoSJP').enable();
											Ext.getCmp('txtNoAskes').enable();
										}
									
										RefreshDatacombo(jeniscus);
									}

								}
					    }, 
					    {
							columnWidth: .990,
							layout: 'form',
							border: false,
							labelWidth:130,
							items:
							[
								mComboKelompokpasien()
							]
						},
						{
							xtype: 'textfield',
							fieldLabel: 'No. SJP',
							maxLength: 200,
							name: 'txtNoSJP',
							id: 'txtNoSJP',
							width: 100,
							anchor: '95%'
						},
						{
							xtype: 'textfield',
							fieldLabel: 'No. Askes',
							maxLength: 200,
							name: 'txtNoAskes',
							id: 'txtNoAskes',
							width: 100,
							anchor: '95%'
						 }
						
						//mComboKelompokpasien
		
		
				]
			}
			
		]
	}
    return items;
};

//combo jenis kelamin
function mComboJK()
{
    var cboJK = new Ext.form.ComboBox
	(
		{
			id:'cboJK',
			x: 455,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Jenis Kelamin',
			fieldLabel: 'Jenis Kelamin ',
			editable: false,
			width:95,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[true, 'Laki - Laki'], [false, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetJK,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJK=b.data.valueField ;
				},
/* 				'render': function(c) {
									c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('txtTempatLahir').focus();
									}, c);
								}
 */			}
		}
	);
	return cboJK;
};

//combo golongan darah
function mComboGDarah()
{
    var cboGDR = new Ext.form.ComboBox
	(
		{
			id:'cboGDR',
			x: 645,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Gol. Darah',
			fieldLabel: 'Gol. Darah ',
			width:50,
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetGDarah,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGDarah=b.data.displayText ;
				},
				/*'render': function(c) {
									c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('txtTempatLahir').focus();
									}, c);
								}*/
			}
		}
	);
	return cboGDR;
};

//Combo Dokter Lookup
function mComboDOKTER()
{ 
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viPenJasRad = new WebApp.DataStore({ fields: Field });
    dsdokter_viPenJasRad.load
	({
		params:
		{
			Skip: 0,
			Take: 1000,
			Sort: 'kd_dokter',
			Sortdir: 'ASC',
			target: 'ViewDokterPenunjang',
			param: "kd_unit = '5'"
		}
	});
	
    var cboDOKTER_viPenJasRad = new Ext.form.ComboBox
	(
            {
                id: 'cboDOKTER_viPenJasRad',
                x: 110,
				y: 130,
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 200,
				emptyText:'Pilih Dokter',
                store: dsdokter_viPenJasRad,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                //value:'All',
				editable: false,
                listeners:
                {
                    'select': function(a,b,c)
                    {
                        tmpkddoktertujuan=b.data.KD_DOKTER ;
                    },
                }
            }
	);
	
    return cboDOKTER_viPenJasRad;
};
var tmpkddoktertujuan;

function mComboUnitRad(){ 
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitrad_viPenJasRad = new WebApp.DataStore({ fields: Field });
    dsunitrad_viPenJasRad.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama_unit',
			Sortdir: 'ASC',
			target: 'ViewSetupUnit',
			param: "parent = '5'"
		}
	});
    var cbounitrad_viPenJasRad = new Ext.form.ComboBox({
		id: 'cboUnitRad_viPenJasRad',
		x: 110,
		y: 190,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel:  ' ',
		align: 'Right',
		width: 130,
		emptyText:'Pilih Unit',
		store: dsunitrad_viPenJasRad,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		//value:'All',
		editable: false,
        listeners:
            {
                'select': function(a,b,c)
                {
                    tmpkd_unit = b.data.KD_UNIT;
                }
            }
	});
    return cbounitrad_viPenJasRad;
}


function mCboKelompokpasien(){  
 var cboKelPasienRad = new Ext.form.ComboBox
	(
		{
			
			id:'cboKelPasienRad',
			fieldLabel: 'Kelompok Pasien',
			x: 110,
			y: 160,
			mode: 'local',
			width: 130,
			forceSelection: true,
			lazyRender:true,
			triggerAction: 'all',
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
				id: 0,
				fields:
				[
					'Id',
					'displayText'
				],
				   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
				}
			),			
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetKelPasien,
			selectOnFocus: true,
			tabIndex:22,
			listeners:
			{
				'select': function(a, b, c)
					{
					   Combo_Select(b.data.displayText);
					   if(b.data.displayText == 'Perseorangan')
					   {
						  // cboPerseorangan
							//Ext.getCmp('cboRujukanDariRequestEntry').hide();
							//Ext.getCmp('cboRujukanRequestEntry').hide();
					   }
					   else if(b.data.displayText == 'Perusahaan')
					   {
							//Ext.getCmp('cboRujukanDariRequestEntryRad').show();
							//Ext.getCmp('cboRujukanRequestEntry').show(); 
					   }
					   else if(b.data.displayText == 'Asuransi')
					   {
							//Ext.getCmp('cboRujukanDariRequestEntryRad').show();
							//Ext.getCmp('cboRujukanRequestEntry').show();  
					   }
					},
				'render': function(a,b,c)
				{
					Ext.getCmp('txtNamaPesertaAsuransiRad').hide();
					Ext.getCmp('txtNoAskesRad').hide();
					Ext.getCmp('txtNoSJPRad').hide();
					Ext.getCmp('cboPerseoranganRad').hide();
					Ext.getCmp('cboAsuransiRad').hide();
					Ext.getCmp('cboPerusahaanRequestEntryRad').hide();
					
				}
			}

		}
	);
	return cboKelPasienRad;
};

function mComboPerseorangan()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganRadRequestEntry = new WebApp.DataStore({fields: Field});
    dsPeroranganRadRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: 'customer',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'jenis_cust=0'
			}
		}
	)
    var cboPerseoranganRad = new Ext.form.ComboBox
	(
		{
			id:'cboPerseoranganRad',
			x: 250,
			y: 160,
			editable: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis',
			tabIndex:23,
			width:150,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			store:dsPeroranganRadRequestEntry,
			value:selectSetPerseorangan,
			listeners:
			{
				'select': function(a,b,c)
				{
					vkode_customer = b.data.KD_CUSTOMER;
				    selectSetPerseorangan=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseoranganRad;
};

function mComboPerusahaan()
{
    var Field = ['KD_PERUSAHAAN','PERUSAHAAN'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboPerusahaan',
			    param: ''
			}
		}
	)
    var cboPerusahaanRequestEntryRad = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntryRad',
			x: 250,
			y: 160,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
			width:150,
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_PERUSAHAAN',
		    displayField: 'PERUSAHAAN',
			//anchor: '95%',
		    listeners:
			{
			    'select': function(a,b,c)
				{
					vkode_customer = b.data.KD_PERUSAHAAN;
					alert(vkode_customer)
					selectSetPerusahaan=b.data.valueField ;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRad;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomer',
                param: "status=true"
            }
        }
    )
    var cboAsuransiRad = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransiRad',
			x: 250,
			y: 160,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: 'Asuransi',
			align: 'Right',
			width:150,
			//anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					vkode_customer = b.data.KD_CUSTOMER;
					alert(vkode_customer)
					selectSetAsuransi=b.data.valueField ;
				}
			}
		}
	);
	return cboAsuransiRad;
};


function Combo_Select(combo)
{
   var value = combo
   //var txtgetnamaPeserta = Ext.getCmp('txtNamaPesertaAsuransiRad') ;

   if(value == "Perseorangan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiRad').hide()
        Ext.getCmp('txtNoAskesRad').hide()
        Ext.getCmp('txtNoSJPRad').hide()
        Ext.getCmp('cboPerseoranganRad').show()
        Ext.getCmp('cboAsuransiRad').hide()
        Ext.getCmp('cboPerusahaanRequestEntryRad').hide()
        

   }
   else if(value == "Perusahaan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiRad').hide()
        Ext.getCmp('txtNoAskesRad').hide()
        Ext.getCmp('txtNoSJPRad').hide()
        Ext.getCmp('cboPerseoranganRad').hide()
        Ext.getCmp('cboAsuransiRad').hide()
        Ext.getCmp('cboPerusahaanRequestEntryRad').show()
        
   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiRad').show()
         Ext.getCmp('txtNoAskesRad').show()
         Ext.getCmp('txtNoSJPRad').show()
         Ext.getCmp('cboPerseoranganRad').hide()
         Ext.getCmp('cboAsuransiRad').show()
         Ext.getCmp('cboPerusahaanRequestEntryRad').hide()
         
       }
}


function setdisablebutton()
{
Ext.getCmp('btnLookUpKonsultasi_viKasirIgd').disable();
Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').disable();
Ext.getCmp('btngantipasien').disable();
Ext.getCmp('btnposting').disable();	

Ext.getCmp('btnLookupPenJasRad').disable()
Ext.getCmp('btnSimpanRWJ').disable()
Ext.getCmp('btnHpsBrsRWJ').disable()
Ext.getCmp('btnHpsBrsDiagnosa').disable()
Ext.getCmp('btnSimpanDiagnosa').disable()
Ext.getCmp('btnLookupDiagnosa').disable()
}

function setenablebutton()
{
//Ext.getCmp('btnLookUpKonsultasi_viKasirIgd').enable();
Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').enable();
Ext.getCmp('btngantipasien').enable();
//Ext.getCmp('btnposting').enable();	

Ext.getCmp('btnLookupPenJasRad').enable()
Ext.getCmp('btnSimpanRWJ').enable()
//Ext.getCmp('btnHpsBrsRWJ').enable()
//Ext.getCmp('btnHpsBrsDiagnosa').enable()
//Ext.getCmp('btnSimpanDiagnosa').enable()
//Ext.getCmp('btnLookupDiagnosa').enable()	
}

//Criteria untuk pencarian berdasarkan no medrec, nama pasien dll
function getCriteriaFilter_viDaftar()//^^^
{
      	 var strKriteria = "";

          /*  if (Ext.get('txtNoMedrecPenJasRad').getValue() != "")
            {
                strKriteria = "and pasien.kd_pasien = " + "'" + Ext.get('txtNoMedrecPenJasRad').getValue() +"'";
            }
            
            if (Ext.get('txtNamaPasienPenJasRad').getValue() != "")//^^^
            {
                if (strKriteria == "")
                    {
                        strKriteria = "and lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasienPenJasRad').getValue() +"%')" ;
                    }
                    else
                        {
                            strKriteria += "and lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasienPenJasRad').getValue() +"%')";
                        }
                
            }
			if (Ext.get('cboStatus_viPenJasRad').getValue() != "")
            {
				if (Ext.get('cboStatus_viPenJasRad').getValue()==='Belum Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = "and tr.posting_transaksi " + "= 'false'" ;
                    }
                    else
                       {
                            strKriteria += " and tr.posting_transaksi " + "= 'false'";
                       }
				}
				if (Ext.get('cboStatus_viPenJasRad').getValue()==='Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = "and tr.posting_transaksi " + "='true'" ;
                    }
                    else
                       {
                            strKriteria += " and tr.posting_transaksi " + "='true'";
                       }
				}
				if (Ext.get('cboStatus_viPenJasRad').getValue()==='Semua')
				{
					
				}
                
                
            }
			if (Ext.get('cboStatusLunas_viPenJasRad').getValue() != "")
            {
				if (Ext.get('cboStatusLunas_viPenJasRad').getValue()==='Belum Lunas')
				{
					if (strKriteria == "")
                    {
                        strKriteria = "and tr.co_status " + "= 'f'" ;
                    }
                    else
                       {
                            strKriteria += " and tr.co_status " + "= 'f'";
                       }
				}
				if (Ext.get('cboStatusLunas_viPenJasRad').getValue()==='Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = "and tr.co_status " + "='t'" ;
                    }
                    else
                       {
                            strKriteria += " and tr.co_status " + "='t'";
                       }
				}
				if (Ext.get('cboStatusLunas_viPenJasRad').getValue()==='Semua')
				{
					
				}
                
                
            } */
			if (Ext.get('dtpTglAwalFilterPenJasRad').getValue() != "")
            {
                 if (strKriteria == "")
                    {
                        strKriteria = "and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasRad').getValue()+"'";
                        }
                
            }
			if (Ext.get('dtpTglAkhirFilterPenJasRad').getValue() != "")
            {
                if (strKriteria == "")
                    {
                        strKriteria = "and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue()+"'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue()+"'";
                        }
                
            }
			/* if (Ext.get('cboUNIT_viKasirRwj').getValue() != "")
            {
					if (strKriteria == "")
                    {
                        strKriteria = " unit.kd_unit ='" + Ext.getCmp('cboUNIT_viKasirRwj').getValue() + "'"  ;
                    }
                    else
                       {
                            strKriteria += " and unit.kd_unit ='" + Ext.getCmp('cboUNIT_viKasirRwj').getValue() + "'";
                       }
                
            } */
	
	 return strKriteria;
}

function getItemPanelPenJasRadAwal()
{
	//var tmptruefalse = true;
    var items =
    {
        layout:'column',
        border:true,
        items:
        [
            {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: '100%',
                height: 100,
                anchor: '100% 100%',
                items:
                [
					//-----------COMBO JENIS TRANSAKSI-----------------------------
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Jenis Transaksi '
					},
					{
						x: 145,
						y: 10,
						xtype: 'label',
						text: ':'
					},
					mComboJenisTrans_viPenJasRad(),
					{
						x: 310,
						y: 10,
						xtype: 'label',
						text: 'Unit Rad '
					},
					{
						x: 445,
						y: 10,
						xtype: 'label',
						text: ':'
					},
					mComboUnitTujuans_viPenJasRad(),
					
					//--------RADIO BUTTON PILIHAN ASAL PASIEN---------------------
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Asal Pasien '
					},
					{
						x: 145,
						y: 40,
						xtype: 'label',
						text: ':'
					},
					{
						x: 155,
						y: 40,
						xtype: 'radiogroup',
						id: 'rbrujukan',
						fieldLabel: 'Asal Pasien ',
						items: [
									{
										boxLabel: 'IGD',
										name: 'rb_auto',
										id: 'rb_pilihan1',
										checked: true,
										inputValue: '1',
										handler: function (field, value) {
										if (value === true)
										{
												getDataCariUnitPenjasRad("kd_bagian=3 and parent<>'0'");
												Ext.getCmp('cboUNIT_viKasirRwj').show();
												Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
												radiovalues='1';
												validasiJenisTr();
												/* tmpparams = "'" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "'";//tanya
												tmpunit = 'ViewRwjPenjasRad';
												loadpenjasrad(tmpparams, tmpunit); */
										}
										}
									},
									{
										boxLabel: 'RWJ',
										name: 'rb_auto',
										id: 'rb_pilihan4',
										inputValue: '1',
										handler: function (field, value) {
										if (value === true)
										{
												getDataCariUnitPenjasRad("kd_bagian=2 and type_unit=false");
												Ext.getCmp('cboUNIT_viKasirRwj').show();
												Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
												radiovalues='4';
												validasiJenisTr();
												/* tmpparams = "'" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "'";//tanya
												tmpunit = 'ViewRwjPenjasRad';
												loadpenjasrad(tmpparams, tmpunit); */
										}
										}
									},
									{
										boxLabel: 'RWI',
										name: 'rb_auto',
										id: 'rb_pilihan2',
										inputValue: '2',
										handler: function (field, value) {
										if (value === true)
										{
											Ext.getCmp('cboUNIT_viKasirRwj').hide();
											Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').show();
											radiovalues='2';
											validasiJenisTr();
											/* tmpparams = " tr.co_Status='0' ORDER BY tr.no_transaksi desc limit 50";
											tmpunit = 'ViewRwiPenjasRad';
											loadpenjasrad(tmpparams, tmpunit); */
										}
									 }
									},
									{
										boxLabel: 'Kunj. Langsung',
										name: 'rb_auto',
										id: 'rb_pilihan3',
										inputValue: 'K.Kd_Pasien',
										handler: function (field, value) {
										if (value === true)
										{
											Ext.getCmp('cboUNIT_viKasirRwj').hide();
											Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').show();
											radiovalues='3';
											validasiJenisTr();
											//PenJasRadLookUp();
											//belum
											Ext.getCmp('txtNamaUnit').setValue="Radiologi";
										}
									}	
									},
								]
					},
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Detail Asal Pasien '
					},
					{
						x: 145,
						y: 70,
						xtype: 'label',
						text: ':'
					},
					//COMBO POLI dan KAMAR
					mComboUnit_viKasirRwj(),
					mcomboKamarSpesial(),
					
                ]
            },
			//---------------JARAK 1----------------------------
			 {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: false,
                width: 100,
                height: 5,
                anchor: '50% 50%',
                items:
                [
						
				]
			 },
			//--------------------------------------------------
            {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: '100%',
                height: 135,
                anchor: '100% 100%',
                items:
                [
					{
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec  '
                    },
					{
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
					{
						x : 155,
                        y : 10,
                        xtype: 'textfield',
                        fieldLabel: 'No. Medrec (Enter Untuk Mencari)',
                        name: 'txtNoMedrecPenJasRad',
                        id: 'txtNoMedrecPenJasRad',
                        enableKeyEvents: true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrecPenJasRad').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrecPenJasRad').getValue())
                                             Ext.getCmp('txtNoMedrecPenJasRad').setValue(tmpgetNoMedrec);
											 validasiJenisTr();
                                             /* var tmpkriteria = getCriteriaFilter_viDaftar();
                                             refeshpenjasrad(tmpkriteria); */
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
														validasiJenisTr();
                                                        /* tmpkriteria = getCriteriaFilter_viDaftar();
                                                        refeshpenjasrad(tmpkriteria); */
                                                    }
                                                    else
                                                    Ext.getCmp('txtNoMedrecPenJasRad').setValue('')
                                            }
                                }
                            }

                        }

                    },	
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
					{
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 40,
                        xtype: 'textfield',
                        name: 'txtNamaPasienPenJasRad',
                        id: 'txtNamaPasienPenJasRad',
                        width: 200,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) 
                                        {
											//getCriteriaFilter_viDaftar();
											validasiJenisTr();
											/* tmpkriteria = getCriteriaFilter_viDaftar();
                                            refeshpenjasrad(tmpkriteria); */

                                            //RefreshDataFilterPenJasRad();

                                        } 						
                                }
                        }
                    },
					/* {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Posting '
                    },
					{
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },					
                    mComboStatusBayar_viPenJasRad(), */
					{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Lunas '
                    },
					{
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },					
                    mComboStatusLunas_viPenJasRad(),
					{
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Tanggal Kunjungan '
                    },
					{
                        x: 145,
                        y: 100,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 100,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterPenJasRad',
                        format: 'd/M/Y',
                        value: tigaharilalu
                    },
					{
                        x: 270,
                        y: 100,
                        xtype: 'label',
                        text: 's/d '
                    },
					{
                        x: 305,
                        y: 100,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterPenJasRad',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
					{
                        x: 415,
                        y: 100,
                        xtype:'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        anchor: '25%',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function(sm, row, rec)
                        {
                            validasiJenisTr();
							/* tmpparams = getCriteriaFilter_viDaftar();
							refeshpenjasrad(tmpparams); */
                        }
                    }
                    
                ]
            },
			
		//----------------------------------------------------------------	
			
			{
                columnWidth:.10,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: '100%',
                height: 135,
                anchor: '100% 100%',
                items:
				[
					
				]
			},
        //----------------------------------------------------------------    
        ]
    }
    return items;
};


function getTindakanRad(){
	var kd_cus_gettarif=vkode_customer;
	/* if(Ext.get('cboKelPasienRad').getValue()=='Perseorangan'){
		kd_cus_gettarif=Ext.getCmp('cboPerseoranganRad').getValue();
	}else if(Ext.get('cboKelPasienRad').getValue()=='Perusahaan'){
		kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryRad').getValue();
	}else {
		kd_cus_gettarif=Ext.getCmp('cboAsuransiRad').getValue();
	  } */
	var modul='';
	if(radiovalues == 1){
		modul='igd';
	} else if(radiovalues == 2){
		modul='rwi';
	} else if(radiovalues == 4){
		modul='rwj';
	} else{
		modul='langsung';
	}
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionRAD/getGridProduk",
			params: {kd_unit:tmpkd_unit,
					 kd_customer:kd_cus_gettarif,
					 penjas:modul,
                     kdunittujuan:tmpkd_unit
					},
			failure: function(o)
			{
				ShowPesanErrorPenJasRad('Error, pasien! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrd_getTindakanPenjasRad.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrd_getTindakanPenjasRad.add(recs);
					GridGetTindakanPenjasRad.getView().refresh();
					
					
					for(var i = 0 ; i < dsDataGrd_getTindakanPenjasRad.getCount();i++)
					{
						var o= dsDataGrd_getTindakanPenjasRad.getRange()[i].data;
						for(var je = 0 ; je < dsTRDetailPenJasRadList.getCount();je++)
						{
							var p= dsTRDetailPenJasRadList.getRange()[je].data;
							if (o.kd_produk === p.kd_produk)
							{
								o.pilihchkprodukrad = true;
								
								
							}
						}
					}
					GridGetTindakanPenjasRad.getView().refresh();
				}
				else 
				{
					ShowPesanErrorPenJasRad('Gagal membaca data pasien ini', 'Error');
				};
			}
		}
		
	)
	
}

function setLookUp_getTindakanPenjasRad(rowdata){
    var lebar = 985;
    setLookUps_getTindakanPenjasRad = new Ext.Window({
        id: 'FormLookUpGetTindakan',
        title: 'Daftar Pemeriksaan', 
        closeAction: 'destroy',        
        width: 600,
        height: 330,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_getTindakanPenjasRad(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
               // rowSelected_getTindakanPenjasRad=undefined;
            }
        }
    });

    setLookUps_getTindakanPenjasRad.show();
   
}


function getFormItemEntry_getTindakanPenjasRad(lebar,rowdata){
    var pnlFormDataBasic_getTindakanPenjasRad = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				gridDataViewEdit_getTindakanPenjasRad()
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambahkan',
						id:'btnTambahKanPermintaan_getTindakanPenjasRad',
						iconCls: 'add',
						//disabled:true,
						handler:function()
						{
							/* var jumlah=0;
							var items=[];
							for(var i = 0 ; i < dsDataGrd_getTindakanPenjasRad.getCount();i++){
								var o=dsDataGrd_getTindakanPenjasRad.getRange()[i].data;
									if(o.pilihchkprodukrad == true){
										jumlah += 1;
										
										// for(var x = 0; x < 100; x++){
											// arr[x] = [];    
											// for(var y = 0; y < 100; y++){ 
												// arr[x][y] = x*y;    
											// }    
										// }
										var recs=[],
											recType=dsTRDetailPenJasRadList.recordType;

										for(var i=0; i<dsDataGrd_getTindakanPenjasRad.getCount(); i++){

											recs.push(new recType(dsDataGrd_getTindakanPenjasRad.data.items[i].data));

										}
										dsTRDetailPenJasRadList.add(recs);

										gridDTItemTest.getView().refresh();
									}
							}
							console.log(items[0][0]) */
							
							
							
							
							var params={};
							params['jumlah']=dsDataGrd_getTindakanPenjasRad.getCount();
							console.log(dsDataGrd_getTindakanPenjasRad.getCount());
							dsTRDetailPenJasRadList.removeAll();
							var recs=[],
							recType=dsTRDetailPenJasRadList.recordType;
							var adadata=false;
							for(var i = 0 ; i < dsDataGrd_getTindakanPenjasRad.getCount();i++)
							{
								console.log(dsDataGrd_getTindakanPenjasRad.data.items[i].data.pilihchkprodukrad);
								if (dsDataGrd_getTindakanPenjasRad.data.items[i].data.pilihchkprodukrad === true)
								{
									console.log(dsDataGrd_getTindakanPenjasRad.data.items[i].data);
									recs.push(new recType(dsDataGrd_getTindakanPenjasRad.data.items[i].data));
									adadata=true;
								}
							}
							dsTRDetailPenJasRadList.add(recs);
							gridDTItemTest.getView().refresh(); 
							console.log(recs);
							if (adadata===true)
							{
								setLookUps_getTindakanPenjasRad.close(); 
							}
							else
							{
								ShowPesanWarningPenJasRad('Ceklis data item pemeriksaan  ','Radiologi');
							}
							
						}
						  
					},
					
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_getTindakanPenjasRad;
}

function RefreshDatahistoribayar(no_transaksi)
{
    var strKriteriaKasirRAD = '';

    strKriteriaKasirRAD = 'no_transaksi= ~' + no_transaksi + '~';

    dsTRDetailHistoryList_rad.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewHistoryBayar',
                                    param: strKriteriaKasirRAD
                                }
                    }
            );
    return dsTRDetailHistoryList_rad;
}
;

function GetDTLTRHistoryGrid()
{

    var fldDetail = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'SHIFT', 'KD_USER'];

    dsTRDetailHistoryList_rad = new WebApp.DataStore({fields: fldDetail})

    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'History Bayar',
                        stripeRows: true,
                        store: dsTRDetailHistoryList_rad,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100% 25%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
															console.log(row);
                                                            cellSelecteddeskripsi = dsTRDetailHistoryList_rad.getAt(row);
                                                            CurrentHistory.row = row;
                                                            CurrentHistory.data = cellSelecteddeskripsi;
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRHistoryColumModel(),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnHpsBrsKasirRAD',
                                        text: 'Hapus Pembayaran',
                                        tooltip: 'Hapus Baris',
                                        iconCls: 'RemoveRow',
                                        //hidden :true,
                                        handler: function ()
                                        {
                                            if (dsTRDetailHistoryList_rad.getCount() > 0)
                                            {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                    if (CurrentHistory != undefined)
                                                    {
                                                        HapusBarisDetailbayar();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningPenJasRad('Pilih record ', 'Hapus data');
                                                }
                                            }
                                            /* Ext.getCmp('btnEditKasirRAD').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirRAD').disable();
                                            Ext.getCmp('btnHpsBrsKasirRAD').disable(); */
                                        },
                                        disabled: true
                                    }
                                ]
                    }

            );

    return gridDTLTRHistory;
}
;
function HapusBarisDetailbayar()
{
    if (cellSelecteddeskripsi != undefined)
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
            //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
            /*var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
             if (btn == 'ok')
             {
             variablehistori=combo;
             if (variablehistori!='')
             {
             DataDeleteKasirRADKasirDetail();
             }
             else
             {
             ShowPesanWarningKasirRAD('Silahkan isi alasan terlebih dahaulu','Keterangan');
             }
             }	
             });  */
            msg_box_alasanhapus_RAD();
        } else
        {
            dsTRDetailHistoryList.removeAt(CurrentHistory.row);
        }
        ;
    }
}
;
function msg_box_alasanhapus_RAD()
{
    var lebar = 250;
    form_msg_box_alasanhapus_RAD = new Ext.Window
            (
                    {
                        id: 'alasan_hapusRad',
                        title: 'Alasan Hapus Pembayaran',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                mComboalasan_hapusRad(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'hapus',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_hapusRad',
                                                                    handler: function ()
                                                                    {
                                                                        DataDeleteKasirRADKasirDetail();
                                                                        form_msg_box_alasanhapus_RAD.close();
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_hapusRad',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanhapus_RAD.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanhapus_RAD.show();
}
;

function mComboalasan_hapusRad()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_hapusRad = new WebApp.DataStore({fields: Field});

    dsalasan_hapusRad.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'alasanhapus',
                                    param: ''
                                }
                    }
            );
    var cboalasan_hapusRad = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_hapusRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Alasan...',
                        labelWidth: 1,
                        store: dsalasan_hapusRad,
                        valueField: 'KD_ALASAN',
                        displayField: 'ALASAN',
                        anchor: '95%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
//                                  var selectalasan_hapusRad = b.data.KD_PROPINSI;
                                        //alert("is");
                                        selectDokter = b.data.KD_DOKTER;
                                    },
                                    'render': function (c)
                                    {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('kelPasien').focus();
                                        }, c);
                                    }
                                }
                    }
            );

    return cboalasan_hapusRad;
}
;
function DataDeleteKasirRADKasirDetail()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/deletedetail_bayar",
                        params: getParamDataDeleteKasirRADKasirDetail(),
                        success: function (o)
                        {
                            //	RefreshDatahistoribayar(Kdtransaksi);
                           // RefreshDataFilterKasirRADKasir();
                            RefreshDatahistoribayar('0');
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoPenJasRad("Proses Tutup Transaksi Berhasil", nmHeaderHapusData);
								Ext.getCmp('btnPembayaranPenjasRAD').enable();
								Ext.getCmp('btnTutupTransaksiPenjasRAD').enable();
                                //alert(Kdtransaksi);					 

                                //refeshKasirRADKasir();
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningPenJasRad(nmPesanHapusGagal, nmHeaderHapusData);
                            } else
                            {
                                ShowPesanWarningPenJasRad(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirRADKasirDetail()
{
    var params =
            {
                TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
                TrTglbayar: CurrentHistory.data.data.TGL_BAYAR,
                Urut: CurrentHistory.data.data.URUT,
                Jumlah: CurrentHistory.data.data.BAYAR,
                Username: CurrentHistory.data.data.USERNAME,
                Shift: tampungshiftsekarang,
                Shift_hapus: CurrentHistory.data.data.SHIFT,
                Kd_user: CurrentHistory.data.data.KD_USER,
                Tgltransaksi: TglTransaksi,
                Kodepasein: kodepasien,
                NamaPasien: namapasien,
                KodeUnit: kodeunit,
                Namaunit: namaunit,
                Kodepay: kodepay,
                Uraian: CurrentHistory.data.data.DESKRIPSI,
                KDkasir: kd_kasir_rad,
                KeTterangan: Ext.get('cboalasan_hapusRad').dom.value

            };
    Kdtransaksi = CurrentHistory.data.data.NO_TRANSAKSI;
    return params
}
;


function TRHistoryColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Bayar',
                            dataIndex: 'URUT',
                            //hidden:true

                        },
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        },
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }
                        },
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colStatHistory',
                            header: 'History',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colPetugasHistory',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME',
                            //hidden:true
                        }
                    ]
                    )
}
;
//var a={};
function gridDataViewEdit_getTindakanPenjasRad(){
    var FieldGrdKasir_getTindakanPenjasRad = [];
    dsDataGrd_getTindakanPenjasRad= new WebApp.DataStore({
        fields: FieldGrdKasir_getTindakanPenjasRad
    });
    chkgetTindakanPenjasRad = new Ext.grid.CheckColumn
		(
			{
				
				id: 'chkgetTindakanPenjasRad',
				header: 'Pilih',
				align: 'center',
				//disabled:false,
				sortable: true,
				dataIndex: 'pilihchkprodukrad',
				anchor: '10% 100%',
				width: 30,
				listeners: 
				{
					checkchange: function()
					{
						alert('hai');
					}
				}
				/* renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    switch (value) {
                        case 't':
							alert('hai');
                            return true;
							
                            break;
                        case 'f':
							alert('fei');
                            return false;
                            break;
                    }
                    return false;
                } */  
		
			}
		); 
    GridGetTindakanPenjasRad =new Ext.grid.EditorGridPanel({
		xtype: 'editorgrid',
		//title: 'Dafrar Pemeriksaan',
		store: dsDataGrd_getTindakanPenjasRad,
		autoScroll: true,
		columnLines: true,
		border: false,
		height: 250,
		anchor: '100% 100%',
		plugins: [new Ext.ux.grid.FilterRow()],
		selModel: new Ext.grid.RowSelectionModel
				(
						{
							singleSelect: true,
							listeners:
									{
									}
						}
				),
		listeners:
				{
					// Function saat ada event double klik maka akan muncul form view
					rowclick: function (sm, ridx, cidx)
					{
						
					},
					rowdblclick: function (sm, ridx, cidx)
					{
						
					}

				},
		/* xtype: 'editorgrid',
        store: dsDataGrd_getTindakanPenjasRad,
        height: 250,
        autoScroll: true,
		columnLines: true,
		border: false,
		plugins: [ new Ext.ux.grid.FilterRow(),chkgetTindakanPenjasRad ],
		selModel: new Ext.grid.RowSelectionModel ({
            singleSelect: true,
            listeners:{
                rowclick: function(sm, row, rec){
					/* rowSelectedGridPasien_viGzPermintaanDiet = undefined;
					rowSelectedGridPasien_viGzPermintaanDiet = dsDataGrdPasien_viGzPermintaanDiet.getAt(row);
					CurrentDataPasien_viGzPermintaanDiet
					CurrentDataPasien_viGzPermintaanDiet.row = row;
					CurrentDataPasien_viGzPermintaanDiet.data = rowSelectedGridPasien_viGzPermintaanDiet.data;
					
					dsTRDetailDietGzPermintaanDiet.removeAll();
					getGridWaktu(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
					
					if(GzPermintaanDiet.vars.status_order == 'false'){
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
					} else{
						Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
					}
					Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue(CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
					 
                },
            }
        }), */
        //stripeRows: true,
		
        colModel:new Ext.grid.ColumnModel([
        	//new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kp_produk',
				header: 'Kode Produk',
				sortable: true,
				width: 70,
				 filter: {}
			},
			{
				dataIndex: 'deskripsi',
				header: 'Nama Pemeriksaan',
				width: 70,
				align:'left',
				 filter: {}
			},{
				dataIndex: 'uraian',
				header: 'Uraian',
				hidden : true,
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'kd_tarif',
				header: 'Kd Tarif',
				width: 70,
				hidden : true,
				align:'center'
			},{
				dataIndex: 'tgl_transaksi',
				header: 'Tgl Transaksi',
				hidden : true,
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'tgl_berlaku',
				header: 'Tgl Berlaku',
				width: 70,
				hidden : true,
				align:'center'
			},{
				dataIndex: 'harga',
				header: 'harga',
				hidden : true,
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'qty',
				header: 'qty',
				width: 70,
				hidden : true,
				align:'center'
			},{
				dataIndex: 'jumlah',
				header: 'jumlah',
				width: 70,
				hidden : true,
				align:'center'
			},
			chkgetTindakanPenjasRad
        ]),
		viewConfig:{
			forceFit: true
		} 
    });
    return GridGetTindakanPenjasRad;
}

function getDokterPengirim(no_transaksi,kd_kasir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionRAD/getDokterPengirim",
			params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
			failure: function(o)
			{
				ShowPesanErrorPenJasRad('Hubungi Admin', 'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					Ext.getCmp('txtDokterPengirimL').setValue(cst.nama);
				}
				else
				{
					ShowPesanErrorPenJasRad('Gagal membaca dokter pengirim', 'Error');
				};
			}
		}

	)
}

function printbillRadRad()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakbill_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarning_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            } else
                            {
                                ShowPesanError_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            }
                        }
                    }
            );
}
;

var tmpkdkasirRadLab = '1';
function dataparamcetakbill_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectPrintingRadLab',
                No_TRans: Ext.get('txtNoTransaksiKasirRADKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'rad',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirRAD').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirRAD').getValue(),
                TlpPasien: Ext.get('txtnotlprad').getValue(),
                
            };
    return paramscetakbill_vikasirDaftarRadLab;
}
;

function printkwitansiRadRad()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakkwitansi_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan !== '')
                            {
                                ShowPesanWarningKasirrwj('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanWarningKasirrwj('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            );
}
;

function dataparamcetakkwitansi_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectKwitansiRadLab',
                No_TRans: Ext.get('txtNoTransaksiKasirRADKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'rad',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirRAD').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirRAD').getValue()
            };
    return paramscetakbill_vikasirDaftarRadLab;
}
;

function getParamHapusDetailTransaksiRAD() 
{   
    var tmpparam = cellSelecteddeskripsi.data;
    var params =
    {       
        no_tr: tmpparam.no_transaksi,
        urut:tmpparam.urut,
        tgl_transaksi:tmpparam.tgl_transaksi
    };
    return params
};

function Datasave_PenJasRad_SQL(mBol) 
{   
     if (ValidasiEntryPenJasRad(nmHeaderSimpanData,false) == 1 )
     {
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/CreateDataObj",
                    params: getParamDetailTransaksiRAD_SQL(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasLab').dom.value,kd_kasir_rad);
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                        }
                        else 
                        {
                                ShowPesanWarningPenJasRad('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

function getParamDetailTransaksiRAD_SQL() 
{
    
    var KdCust='';
    var TmpCustoLama='';
    var pasienBaru;
    var modul='';
    if(radiovalues == 1){
        modul='igd';
    } else if(radiovalues == 2){
        modul='rwi';
    } else if(radiovalues == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    
    if(Ext.getCmp('txtNoMedrecL').getValue() == ''){
        pasienBaru=1;
    } else{
        pasienBaru=0;
    }
    
    var params =
    {
        Table: 'SQL_RAD',
        KdTransaksi: Ext.get('txtNoTransaksiPenJasRad').getValue(),
        KdPasien:Ext.getCmp('txtNoMedrecL').getValue(),
        NmPasien:Ext.getCmp('txtNamaPasienL').getValue(),
        Ttl:Ext.getCmp('dtpTtlL').getValue(),
        Alamat:Ext.getCmp('txtAlamatL').getValue(),
        JK:Ext.getCmp('cboJK').getValue(),
        GolDarah:Ext.getCmp('cboGDR').getValue(),
        KdUnit: Ext.getCmp('txtKdUnitRad').getValue(),
        KdDokter:tmpkddoktertujuan,
        KdUnitTujuan:tmpkd_unit,
        Tgl: Ext.getCmp('dtpKunjunganL').getValue(),
        TglTransaksiAsal:TglTransaksi,
        urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
        KdCusto:vkode_customer,
        TmpCustoLama:vkode_customer,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
        NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiRad').getValue(),
        NoAskes:Ext.getCmp('txtNoAskesRad').getValue(),
        NoSJP:Ext.getCmp('txtNoSJPRad').getValue(),
        Shift:tampungshiftsekarang,
        List:getArrPenJasRad(),
        JmlField: mRecordRwj.prototype.fields.length-4,
        JmlList:GetListCountDetailTransaksi(),
        dtBaru:databaru,
        Hapus:1,
        Ubah:0,
        unit:41,
        TmpNotransaksi:TmpNotransaksi,
        KdKasirAsal:KdKasirAsal,
        KdSpesial:Kd_Spesial,
        Kamar:No_Kamar,
        pasienBaru:pasienBaru,
        listTrDokter : '',
        Modul:modul,
        KdProduk:'',
        URUT :tmp_urut_sql,
        kunjungan_rad:tmppasienbarulama
        
    };
    return params
};

function Delete_Detail_Rad_SQL(){
    Ext.Ajax.request
     (
        {
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteRadDetail(),
            failure: function(o)
            {
                ShowPesanWarningPenJasRad('Error Hapus. Hubungi Admin!', 'Gagal');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    // ShowPesanInfoPenJasRad('Data Berhasil Di hapus', 'Sukses');
                    // dsTRDetailPenJasRadList.removeAt(line);
                    // gridDTItemTest.getView().refresh();
                    // Delete_Detail_Rad_SQL();
                }
                else 
                {
                        ShowPesanWarningPenJasRad('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
                };
            }
        }
    )
}

function getParamDataDeleteRadDetail(){
    var tmpparam = cellSelecteddeskripsi.data;
    var params =
    {       
        Table: 'SQL_RAD',
        no_tr: tmpparam.no_transaksi,
        urut:tmpparam.urut,
        tgl_transaksi:tmpparam.tgl_transaksi     
    };
    return params

}
var tmpunitfotorad;
var tmpurutfotorad;
var tmptglfotorad;
function PenJasRadUpdateFotoLookUp(rowdata) 
{
    var lebar = 275;
    FormPenJasRadUpdateFotoLookUp = new Ext.Window
    (
        {
            id: 'windowPenJasRadUpdateFotoLookUp',
            title: 'Update Foto Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 190,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: [formpopupUpdateFotoRad()],
            listeners:
            {
                 activate: function()
                {
                    if (rowdata === undefined){

                    }else{
                        // console.log(rowdata);
                        tmpunitfotorad = rowdata.KD_UNIT;
                        tmptglfotorad = rowdata.TGL_TRANSAKSI;
                        tmpurutfotorad = rowdata.URUT;
                        var tmpdate = new Date(rowdata.TGL_TRANSAKSI);
                        var realdate = tmpdate.format("d/M/Y");
                        Ext.getCmp('TxtPopupMedrecRad').setValue(rowdata.KD_PASIEN);
                        Ext.getCmp('TxtPopupNamaPasien').setValue(rowdata.NAMA);
                        Ext.getCmp('TxtPopupTglKunjungPasien').setValue(realdate);
                        Ext.getCmp('TxtPopupNoFotoPasien').setValue(rowdata.NO_FOTO)
                    }
                    
                    
                }
            }
        }
    );

    FormPenJasRadUpdateFotoLookUp.show();
    // if (rowdata === undefined) 
    // {
    //     // RWJAddNew();
    // }
    // else 
    // {
    //     // TRRWJInit(rowdata);
    // }

};

function formpopupUpdateFotoRad() {
    var FrmTabs_popupdatahasilrad = new Ext.Panel
            (
                    {
                        id: 'formpopupUpdateFotoRad',
                        closable: true,
                        region: 'center',
                        layout: 'column',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: false,
                        shadhow: true,
                        margins: '5 5 5 5',
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelUpdateFotoRad()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_RAD',
                                                    // disabled: true,
                                                    handler: function ()
                                                    {
                                                        if (Ext.getCmp('TxtPopupNoFotoPasien').getValue() !== '') {
                                                            UpdateFotoRad();
                                                        }else{
                                                            ShowPesanWarningPenJasRad('No. Foto Tidak Boleh Kosong', 'Warning');
                                                        }
                                                        
                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupdatahasilrad;
}
;

function PanelUpdateFotoRad() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: false,
                width: 450,
                height: 150,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec'
                    },
                    {
                        x: 90,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtPopupMedrecRad',
                        id: 'TxtPopupMedrecRad',
                        disable:true,
                        width: 80
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
                    {
                        x: 90,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtPopupNamaPasien',
                        id: 'TxtPopupNamaPasien',
                        disable:true,
                        width: 150
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Tgl. Kunjungan '
                    },
                    {
                        x: 90,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtPopupTglKunjungPasien',
                        id: 'TxtPopupTglKunjungPasien',
                        disable:true,
                        width: 80
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'No. Foto '
                    },
                    {
                        x: 90,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 100,
                        xtype: 'textfield',
                        name: 'TxtPopupNoFotoPasien',
                        id: 'TxtPopupNoFotoPasien',
                        width: 80
                    },
                ]
            }
        ]
    };
    return items;
}
;

function UpdateFotoRad(){
    Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/functionRAD/updatefotorad",
                    params: getParamUpdateFotoRAD(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            ShowPesanInfoPenJasRad("Berhasil menyimpan data ini","Information");
                        }
                        else 
                        {
                                ShowPesanWarningPenJasRad('Data Gagal Di Simpan', 'Gagal');
                        };
                    }
                }
            )
};

function getParamUpdateFotoRAD(){
    var params =
            {
                kdpasien: Ext.getCmp('TxtPopupMedrecRad').getValue(),
                kdunit: tmpunitfotorad,
                tgl: tmptglfotorad,
                urut: tmpurutfotorad,
                nofoto: Ext.getCmp('TxtPopupNoFotoPasien').getValue()
            };
    return params
}

function TransferData_rad_SQL(){
    Ext.Ajax.request
        (
            {
                url: baseURL + "index.php/rad_sql/function_rad_sql/saveTransfer",
                params: getParamTransferRwi_dari_rad(),
                failure: function (o)
                {

                    ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                    // ViewGridBawahLookupPenjasRad();
                },
                success: function (o)
                {   
                
                    
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {   
                        // TransferData_rad_SQL();
                        // Ext.getCmp('btnSimpanKasirRAD').disable();
                        // Ext.getCmp('btnTransferKasirRAD').disable();
                        // ShowPesanInfoPenJasRad('Transfer Berhasil', 'transfer ');
                        // RefreshDatahistoribayar(Ext.getCmp('txtNoTransaksiPenJasRad').getValue());
                        // FormLookUpsdetailTRTransfer_Rad.close();
                        
                    } else
                    {
                        ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                    }
                    ;
                }
            }
        )
}
var tmpnotransPF;
var tmptgltransPF;
var tmpuruttransPF;
function PenJasRadPemakianFilmLookUp(rowdata) 
{
    var lebar = 500;
    FormPenJasRadPemakianFilmLookUp = new Ext.Window
    (
        {
            id: 'windowFormPenJasRadPemakianFilmLookUp',
            title: 'Pemakaian Film',
            closeAction: 'destroy',
            width: lebar,
            height: 400,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: [formpopupPemakaianFilmRad()],
            listeners:
            {
                 activate: function()
                {
                    if (rowdata === undefined){

                    }else{
                        loadFilmpenjasrad(rowdata);
                        tmpnotransPF = rowdata.no_transaksi;
                        tmptgltransPF = rowdata.tgl_transaksi;
                        tmpuruttransPF = rowdata.urut;
                        var tmpkode_produkPF = rowdata.kd_produk + '-' + rowdata.deskripsi;
                        Ext.getCmp('TxtPopupTindakanPFRad').setValue(tmpkode_produkPF);
                        Ext.getCmp('TxtPopupTindakanPFRad').disable();
                    }
                    
                    
                }
            }
        }
    );

    FormPenJasRadPemakianFilmLookUp.show();
};

function formpopupPemakaianFilmRad() {
	var Field = ['NAMA_OBAT','KD_PRODUK','HARGA','QTY','QTY_RSK','QTY_STD','MAX_QTY','JNS_BHN'];
	    dtsgridpemakaianfilm = new WebApp.DataStore({ fields: Field });
		
	    //  var k="tr.tgl_transaksi >='"+tigaharilaluNew+"' and tr.tgl_transaksi <='"+tglGridBawah+"'    and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc";
		
	    // refeshpenjasrad(k);

	    gridpemakaianfilm = new Ext.grid.EditorGridPanel
	    (
	        {
	            stripeRows: true,
				id:'PenjasRad',
	            store: dtsgridpemakaianfilm,
	            anchor: '100% 100%',
                height:'300',
	            columnLines: false,
	            autoScroll:true,
	            border: false,
                editable:true,
				sort :false,
	            sm: new Ext.grid.RowSelectionModel
	            (
	                {
	                    singleSelect: true,
	                    listeners:
	                    {
	                        rowselect: function(sm, row, rec)
	                        {
	                            
	                            
	                        }
	                    }
	                }
	            ),
	        cm: new Ext.grid.ColumnModel
	            (
	                [
	                   
	                    {
	                        id: 'colkdfilmpemakaianfilm',
	                        header: 'Kd. Film',
	                        dataIndex: 'KD_PRODUK',
	                        sortable: false,
	                        hideable:false,
	                        menuDisabled:true,
	                        width: 50
	                    },
	                    {
	                        id: 'coluraianpemakaianfilm',
	                        header: 'Uraian',
	                        dataIndex: 'NAMA_OBAT',
	                        sortable: false,
	                        hideable:false,
	                        menuDisabled:true,
	                        width: 100,
	                    },
						{
							id: 'coljmlpemakaianfilm',
	                        header: 'Jml',
	                        dataIndex: 'QTY',
	                        width: 50,
	                        sortable: false,
	                        hideable:false,
	                        menuDisabled:true,
                            editor:{
                                xtype:'textfield'
                            }                        
	                    },
						{
							id: 'coljmlrskpemakaianfilm',
	                        header: 'jml. Rsk',
	                        dataIndex: 'QTY_RSK',
	                        width: 50,
	                        sortable: false,
	                        hideable:false,
	                        menuDisabled:true,
                            editor:{
                                xtype:'textfield'
                            }                          
	                    },
	                   
	                ]
	            ),
	            viewConfig: {forceFit: true},
	            tbar:
	                [
	                    {
	                        id: 'btnEditRWJ',
	                        text: 'Simpan',
	                        tooltip: nmEditData,
	                        iconCls: 'Save',
	                        handler: function(sm, row, rec)
	                        {
								loadMask.show();
                                DataUpdate_PemakaianFotoPenJasRad();
	                        }
	                    }
	                ]
	            }
		);
    var FrmTabs_formpopupPemakianFilmRad = new Ext.Panel
            (
                    {
                        id: 'FrmTabs_formpopupPemakianFilmRad',
                        closable: true,
                        region: 'center',
                        layout: 'column',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        // margins: '5 5 5 5',
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelPemakaianFilmRad(),gridpemakaianfilm
                                ]
                    }
            );
    return FrmTabs_formpopupPemakianFilmRad;
}
;
var gridpemakaianfilm;
var dtsgridpemakaianfilm;
function PanelPemakaianFilmRad() {
	    
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: true,
                width: 500,
                height: 30,
                anchor: '100% 100%',
                items: [
                	{
                        x: 10,
                        y: 7,
                        xtype: 'label',
                        text: 'Tindakan '
                    },
                    {
                        x: 90,
                        y: 7,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 5,
                        xtype: 'textfield',
                        name: 'TxtPopupTindakanPFRad',
                        id: 'TxtPopupTindakanPFRad',
                        editable:false,
                        width: 300
                    },
                ]
            }
        ]
    };
    return items;
}
;

function loadFilmpenjasrad(rowdata)
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionRAD/getdatafoto",
			params: getParamRefreshlistfotoRad(rowdata),
			failure: function(o)
			{
				ShowPesanWarningPenJasRad('Hubungi Admin', 'Error');
			},
			success: function(o)
			{   
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					dtsgridpemakaianfilm.removeAll();
					var recs=[],
					recType=dsTRPenJasRadList.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dtsgridpemakaianfilm.add(recs);
					gridpemakaianfilm.getView().refresh();

				}
				else
				{
					ShowPesanWarningPenJasRad('Gagal membaca Data ini', 'Error');
				};
			}
		}

	);
}

function getParamRefreshlistfotoRad(rowdata){

	// var kriteria = "WHERE dfo.kd_kasir = '"+ rowdata. +"' AND dfo.no_transaksi = '4603972' AND dfo.tgl_transaksi = '2017-01-18' AND dfo.urut = 1";
	var params =
	{
		kd_kasir:'',
		notrans:rowdata.no_transaksi,
		tgltransaksi:rowdata.tgl_transaksi,
		urut:rowdata.urut		
	}
	return params;
}

function getArrfotoPenJasRad()
{
    var x='';
    var arr=[];
    for(var i = 0 ; i < dtsgridpemakaianfilm.getCount();i++)
    {
        var o={};
        var y='';
        var z='@@##$$@@';
        o['KD_PRD_RAD_FO']= dtsgridpemakaianfilm.data.items[i].data.KD_PRODUK;
        o['QTY_RAD_FO']= dtsgridpemakaianfilm.data.items[i].data.QTY;
        o['QTY_RSK_RAD_FO']= dtsgridpemakaianfilm.data.items[i].data.QTY_RSK;
        arr.push(o);
        
    }   
    
    return Ext.encode(arr);
};

function DataUpdate_PemakaianFotoPenJasRad() 
{   
    Ext.Ajax.request
     (
        {
            url: baseURL + "index.php/main/functionRAD/UpdatePemakaianFotoRad",
            params: getParamDetailPemakaianFotoRAD(),
            failure: function(o)
            {
				loadMask.hide();
                ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    loadMask.hide();
                    ShowPesanInfoPenJasRad("Berhasil menyimpan data ini","Information");
                    // Ext.get('txtNoTransaksiPenJasRad').dom.value=cst.notrans;
                    // Ext.get('txtNoMedrecL').dom.value=cst.kdPasien;
                    // ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value);
                    // Ext.getCmp('btnPembayaranPenjasRAD').enable();
                    // Ext.getCmp('btnTutupTransaksiPenjasRAD').enable();
                    // Ext.getCmp('txtnoregrad').setValue(cst.noreg);
                    // if(mBol === false)
                    // {
                    //     ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value);
                    // };

                    // tmpkd_pasien_sql = cst.kdPasien;
                    // tmpno_trans_sql = cst.notrans;
                    // tmpkd_kasir_sql = cst.kdkasir;
                    // tmp_tgl_sql = cst.tgl;
                    // tmp_urut_sql = cst.urut;
                    // tmpurut = cst.urut;
                    DataUpdate_PemakaianFotoPenJasRad_SQL();
                }
                else 
                {
					loadMask.hide();
					ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                };
            }
        }
    )
};

function getParamDetailPemakaianFotoRAD() 
{
    
    var params =
    {        
        notrans: tmpnotransPF,
        tgltrans:tmptgltransPF,
        uruttrans:tmpuruttransPF,
        List:getArrfotoPenJasRad(),
        
    };
    return params
};

function DataUpdate_PemakaianFotoPenJasRad_SQL(){
    Ext.Ajax.request
        (
            {
                url: baseURL + "index.php/rad_sql/function_rad_sql/UpdatePemakaianFotoRad",
                params: getParamDetailPemakaianFotoRAD(),
                failure: function (o)
                {

                    ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                },
                success: function (o)
                {   
                
                    
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {   
                        
                    } else
                    {
                        ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                    }
                    ;
                }
            }
        )
}

function setUsia(Tanggal)
{
    Ext.Ajax.request
            ({
                url: baseURL + "index.php/main/GetUmur",
                params: {
                    TanggalLahir: Tanggal
                },
                success: function (o)
                {
                    var tmphasil = o.responseText;
                    var tmp = tmphasil.split(' ');

                    if (tmp.length === 6)
                    {
                        Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[4]);
                    } else if (tmp.length === 4) {
                        if (tmp[1] === 'years') {
                            if (tmp[3] === 'day') {
                                Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                            } else {
                                Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                            }
                        } else {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                        }
                    } else if (tmp.length === 2) {
                        if (tmp[1] === 'year') {
                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'years') {
                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mon') {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mons') {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[0]);
                        }
                    } else if (tmp.length === 1) {
                        Ext.getCmp('txtUmurRad').setValue('0');
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue('1');
                    } else {
                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                    }
                }
            });
}

