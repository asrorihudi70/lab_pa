var TrPenJasRad={};
TrPenJasRad.setting={};
TrPenJasRad.mod_id='060102';
getSetting(TrPenJasRad.mod_id,['KD_UNIT_RAD','KD_KASIR_RAD_IGD','KD_KASIR_RAD_RWJ','KD_KASIR_RAD_RWI'],function(data){
	TrPenJasRad.setting=data;
});

var CurrentPenJasRad ={
    data: Object,
    details: Array,
    row: 0
};
var total_bayar;
var data_transfer;
var data_unit_asal;
var tmp_group_dokter        = 1;
var DataStoreSecondGridStore_PJRAD = new Ext.data.JsonStore();
var DataStorefirstGridStore_PJRAD  = new Ext.data.JsonStore();
var fieldsDokterPenindak = [
    {name: 'KD_DOKTER', mapping : 'KD_DOKTER'},
    {name: 'NAMA', mapping : 'NAMA'}
];
dsDataDokterPenindak_PJ_RAD = new WebApp.DataStore({ fields: fieldsDokterPenindak });
var tmpKdJob                = 1;

var currentJasaDokterKdTarif_RAD;
var currentJasaDokterKdProduk_RAD;
var currentJasaDokterUrutDetailTransaksi_RAD;
var currentJasaDokterHargaJP_RAD;
var GridDokterTr_RAD;
var dsGridJasaDokterPenindak_RAD;
var trcellCurrentTindakan_RAD;
var currentKdKasirRAD;
var mRecordRad = Ext.data.Record.create([
   {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
   {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
   {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
   {name: 'KD_TARIF', mapping:'KD_TARIF'},
   {name: 'HARGA', mapping:'HARGA'},
   {name: 'QTY', mapping:'QTY'},
   {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
   {name: 'DESC_REQ', mapping:'DESC_REQ'},
   {name: 'URUT', mapping:'URUT'}
]);
var Trkdunit2;
var CurrentHistory ={
    data: Object,
    details: Array,
    row: 0
};
//---------TAMBAH BARU 27-SEPTEMBER-2017
var selectSetGolDarah;
var selectSetJK;
var ID_JENIS_KELAMIN;

var tmpkd_unit = '';
var tmpkd_unit_asal = '';
var tmpkd_unit_tarif_mir = '';
var kodeunit;
var tmp_kodeunitkamar_RAD;
var tmp_kdspesial_RAD;
var tmp_nokamar_RAD;
var tmplunas;
var kodeunittransfer;
var tglTransaksiAwalPembandingTransferRAD;
var dsTRDetailHistoryList_rad;
var kodepasien;
var namapasien;
var namaunit;
var kodepay ;
var uraianpay;
var vTanggal;
var vKdUnit;
var vUrutMasuk;
var mRecordKasirRAD = Ext.data.Record.create([
    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
    {name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
    {name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
    {name: 'HARGA', mapping: 'HARGA'},
    {name: 'QTY', mapping: 'QTY'},
    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
    {name: 'URUT', mapping: 'URUT'}
]);

var AddNewKasirRADKasir = true;
var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var kdpaytransfer = 'T1';
var kdkasir;
var kdkasir_tmp;
var tanggaltransaksitampung;
var kdkasirasal_rad;
var notransaksiasal_rad;
var kdcustomeraa;
var vflag;
var tmp_NoTransaksi;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var tampungtypedata;
var jenispay;
var tapungkd_pay;
var tranfertujuan = 'Rawat Inap';
var cellSelecteddeskripsi;
var vkd_unit;
var notransaksi;
var noTransaksiPilihan;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();
var tigaharilalu = new Date().add(Date.DAY, -0);
var nowTglTransaksiGrid = new Date();
var tglGridBawah = nowTglTransaksiGrid.format("d/M/Y");

var tigaharilaluNew = tigaharilalu.format("d/M/Y");

var gridDTItemTest;

var selectSetDr;
var selectSetGDarah;
var selectSetJK;
var selectSetKelPasien;
var selectSetPerseorangan;
var selectSetPerusahaan;
var selectSetAsuransi;

var radelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwj      = 'Belum Posting';
var selectCountStatusLunasByr_viKasirRwj = 'Belum Lunas';
var selectCountJenTr_viPenJasRad         = 'Transaksi Baru';
var dsTRPenJasRadList;
var dsTRDetailPenJasRadList;
var AddNewPenJasRad = true;
var selectCountPenJasRad = 50;
var TmpNotransaksi='';
var KdKasirAsal='';
var TglTransaksi='';
var databaru = 0;
var No_Kamar='';
var Kd_Spesial=0;
var kodeUnitRad;
var dsPeroranganRadRequestEntry;
var dsdokter_viPenJasRad;

var rowSelectedPenJasRad;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRrwj;
var valueStatusCMRWJView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
var tmpparams = '';
var grListPenJasRad;
// -------------Edit Tarif Harga---------------
var CurrentNoTransaksi_Rad;
var CurrentKdKasir_Rad;
var CurrentRow;
var dsDataStoreGridTarifKomponent = new Ext.data.JsonStore();
var DataStoreGridKomponent;
// --------------------------------------------

//VALIDASI JENIS TRANSAKSI
var combovalues = 'Transaksi Baru';
var radiovalues = '1';
var combovaluesunit = "";
var combovaluesunittujuan = "";
var ComboValuesKamar = "";

var vkode_customerRAD;
CurrentPage.page = getPanelPenJasRad(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
var tmpurut;
var citopersentasi = 0;
var kd_unit_konfig = "";
TrPenJasRadproduk=new Ext.data.ArrayStore({id: 0,fields:['kd_produk','deskripsi','harga','tglberlaku'],data: []});

var order_rad = true

var KasirRADDataStoreProduk=new Ext.data.ArrayStore({id: 0,fields:['kd_produk','deskripsi','harga','tglberlaku'],data: []});

//membuat form

     Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/get_zusers",
        params: {
            command: '0',
        },
        failure: function (o)
        {
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            kd_unit_konfig = cst.data_user[0].kd_unit;
            // console.log(cst.data_user[0].kd_unit);
        }
    });
function msg_box_alasanbukatrans_RAD(data)
{
    var lebar = 250;
    form_msg_box_alasanbukatrans_RAD = new Ext.Window
            (
                    {
                        id: 'alasan_bukatransRAD',
                        title: 'Alasan Buka Transaksi',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    //fieldLabel: 'No. Transaksi ',
                                                    name: 'txtAlasanbukatransRAD',
                                                    id: 'txtAlasanbukatransRAD',
                                                    emptyText: 'Alasan Buka Transaksi',
                                                    anchor: '99%',
                                                },
                                               //mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Buka',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_bukatransLAb',
                                                                    handler: function ()
                                                                    {
                                                                        if (Ext.getCmp('txtAlasanbukatransRAD').getValue().trim()==='' || Ext.getCmp('txtAlasanbukatransRAD').getValue().trim==='Alasan Buka Transaksi')
                                                                        {
                                                                            ShowPesanWarningPenJasRad("Isi alasan buka transaksi !", "Perhatian");
                                                                        }else{
                                                                            Ext.Ajax.request
                                                                            (
                                                                                    {
                                                                                        url: baseURL + "index.php/main/functionKasirPenunjang/bukatransaksi",
                                                                                        params: {
                                                                                            no_transaksi : data.notrans_bukatrans,
                                                                                            kd_kasir     : data.kd_kasir_bukatrans,
                                                                                            tgl_transaksi: data.tgl_transaksi_bukatrans,
                                                                                            kd_pasien    : data.kd_pasien_bukatrans,
                                                                                            urut_masuk   : data.urut_masuk_bukatrans,
                                                                                            kd_unit      : data.kd_unit_bukatrans,
                                                                                            keterangan   : Ext.getCmp('txtAlasanbukatransRAD').getValue(),
                                                                                            kd_bagian_shift : 5
                                                                                        },
                                                                                        success: function (o)
                                                                                        {
                                                                                            //  RefreshDatahistoribayar_LAB(Kdtransaksi);
                                                                                           // RefreshDataFilterKasirLABKasir();
                                                                                            //RefreshDatahistoribayar_LAB('0');
                                                                                            var cst = Ext.decode(o.responseText);
                                                                                            if (cst.success === true)
                                                                                            {
                                                                                                ShowPesanInfoPenJasRad("Proses Buka Transaksi Berhasil", nmHeaderHapusData);
                                                                                                Ext.getCmp('btnBukaTransaksiRAD').disable();
                                                                                                validasiJenisTr();
                                                                                                
                                                                                            } else {
                                                                                                ShowPesanWarningPenJasRad(nmPesanHapusError, nmHeaderHapusData);
                                                                                            }
                                                                                            ;
                                                                                        }
                                                                                    }
                                                                            )
                                                                            form_msg_box_alasanbukatrans_RAD.close();
                                                                        }
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_bukatransRAD',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanbukatrans_RAD.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanbukatrans_RAD.show();
}
function getPanelPenJasRad(mod_id) 
{
    var Field = ['KD_PASIEN','NO_TRANSAKSI','NAMA','ALAMAT','SPESIALISASI','KELAS','NAMA_KAMAR','KAMAR','MASUK','NO_TRANSAKSI_ASAL','KD_KASIR_ASAL',
                 'DOKTER','KD_DOKTER','KD_UNIT_KAMAR','KD_CUSTOMER','CUSTOMER','TGL_MASUK','URUT_MASUK','TGL_INAP','KD_SPESIAL','KD_KASIR','TGL_TRANSAKSI','KD_UNIT',
                 'CO_STATUS','KD_USER','TGL_LAHIR','JENIS_KELAMIN','GOL_DARAH','POSTING_TRANSAKSI', 
                 'NAMA_UNIT','POLIKLINIK','NAMA_UNIT_ASLI','KELPASIEN','LUNAS','DOKTER_ASAL','KD_DOKTER_ASAL','URUT','NAMA_UNIT_ASAL','KD_UNIT_ASAL','HP','SJP','NO_FOTO','DOKTER_RAD','PATIENT_ID','STUDY_UID','ORDER_MNG'];
    dsTRPenJasRadList = new WebApp.DataStore({ fields: Field });
    
     var k="tr.tgl_transaksi >='"+tigaharilaluNew+"' and tr.tgl_transaksi <='"+tglGridBawah+"'    and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc";
    //---------------------EDIT DEFAULT UNIT RAD 31 01 2017
    
    refeshpenjasrad(k);
    getcitorad();

    grListPenJasRad = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            id:'PenjasRad',
            store: dsTRPenJasRadList,
            flex:1,
            style:'padding: 4px;',
            columnLines: false,
            autoScroll:true,
            sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedPenJasRad = dsTRPenJasRadList.getAt(row);
                            kdkasir_tmp          = rowSelectedPenJasRad.data.KD_KASIR;
                            vKdUnit              = rowSelectedPenJasRad.data.KD_UNIT;
                            vUrutMasuk           = rowSelectedPenJasRad.data.URUT_MASUK;
                            vTanggal             = rowSelectedPenJasRad.data.MASUK;
                            if(Ext.get('cboJenisTr_viPenJasRad').getValue()=='Transaksi Baru'){
                                Ext.getCmp('btnBatalTransaksiRad').disable();
                                Ext.getCmp('btnGantiKelompokRad').disable();
                                Ext.getCmp('btnGantiDokterRad').disable();
                                Ext.getCmp('btnBukaTransaksiRAD').disable();
                                Ext.getCmp('btnCetakLabelRAD').disable();
                            }
                            else
                            {
                                
                                //alert(vKdUnit);
                                if (rowSelectedPenJasRad.data.LUNAS==='1' || rowSelectedPenJasRad.data.LUNAS==='1' || rowSelectedPenJasRad.data.LUNAS===1){
                                    Ext.getCmp('btnGantiKelompokRad').disable();
                                    Ext.getCmp('btnGantiDokterRad').disable();
                                    if (rowSelectedPenJasRad.data.CO_STATUS==='1' || rowSelectedPenJasRad.data.CO_STATUS==='1' || rowSelectedPenJasRad.data.CO_STATUS===1){
                                        Ext.getCmp('btnBukaTransaksiRAD').enable();
                                    }else{
                                        Ext.getCmp('btnBukaTransaksiRAD').disable();
                                    }
                                    
                                    Ext.getCmp('btnBatalTransaksiRad').disable();
                                }else{
                                    Ext.getCmp('btnGantiKelompokRad').enable();
                                    Ext.getCmp('btnGantiDokterRad').enable();
                                    Ext.getCmp('btnBukaTransaksiRAD').disable();
                                    Ext.getCmp('btnBatalTransaksiRad').enable();
                                }
                                
                            }
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedPenJasRad = dsTRPenJasRadList.getAt(ridx);
                    // console.log(rowSelectedPenJasRad)
					vKdUnit= rowSelectedPenJasRad.data.KD_UNIT;
					vUrutMasuk= rowSelectedPenJasRad.data.URUT_MASUK;
					vTanggal= rowSelectedPenJasRad.data.MASUK;
                    noTransaksiPilihan = rowSelectedPenJasRad.data.NO_TRANSAKSI;
                    if (rowSelectedPenJasRad !== undefined)
                    {
                        // console.log(rowSelectedPenJasRad.data);
                        PenJasRadLookUp(rowSelectedPenJasRad.data);
                    }
                    else
                    {
                        PenJasRadLookUp();
                        Ext.getCmp('cboDOKTER_viPenJasRad').disable();
                        Ext.getCmp('cboGDR').disable();
                        Ext.getCmp('cboJK').disable();
                    }

                    get_total_bayar(rowSelectedPenJasRad.data.NO_TRANSAKSI, rowSelectedPenJasRad.data.KD_KASIR);
                    get_unit_asal(rowSelectedPenJasRad.data.NO_TRANSAKSI, rowSelectedPenJasRad.data.KD_KASIR);
                }
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   
                    {
                        id: 'colReqIdViewRWJ',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80
                    },
                    {
                        id: 'colTglRWJViewRWJ',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TGL_TRANSAKSI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 75,
                        renderer: function(v, params, record)
                        {
                            return ShowDate(record.data.TGL_TRANSAKSI);

                        }
                    },
                    {
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colRWJerViewRWJ'
                    },
                    {
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colRWJerViewRWJ'
                    },
                    {
                        id: 'colLocationViewRWJ',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewRWJ',
                        header: 'Dokter',
                        dataIndex: 'DOKTER',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colImpactViewRWJ',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT_ASLI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 90
                    },
                    {
                        header: 'Status Transaksi',
                        width: 100,
                        sortable: false,
                        hideable:true,
                        hidden:true,
                        menuDisabled:true,
                        dataIndex: 'POSTING_TRANSAKSI',
                        id: 'txtposting',
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                        
                    },
                    {
                        id: 'colCoSTatusViewRWJ',
                        header: 'Status Lunas',
                        dataIndex: 'LUNAS',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80,
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 1:
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 0:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                                case '':
                                         metaData.css = 'StatusMerah'; // 
                                         break;
                                 case undefined:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
                    {
                        id: 'colBukaTransaksiViewRAD',
                        header: 'Status Tutup Transaksi',
                        dataIndex: 'CO_STATUS',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80,
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 1:
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 0:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                                case '':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                                case undefined:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
                    {
                        id: 'colnofotViewrad',
                        header: 'No. Foto',
                        dataIndex: 'NO_FOTO',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 100
                    }
                   
                ]
            ),
            viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditRWJ',
                        text: 'Lookup',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedPenJasRad != undefined)
                            {
                                // console.log(rowSelectedPenJasRad.data);
                                
                                    PenJasRadLookUp(rowSelectedPenJasRad.data);
                            }
                            else
                            {
                            ShowPesanWarningPenJasRad('Pilih data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },
                    {
                        id: 'btnUpdateFotoRad',
                        text: 'Update Foto',
                        tooltip: nmEditData,
                        // disable: true,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            // PenJasRadUpdateFotoLookUp();
                            if (Ext.get('cboJenisTr_viPenJasRad').getValue() === 'Transaksi Lama' && rowSelectedPenJasRad.data.LUNAS === '1') {
                                PenJasRadUpdateFotoLookUp(rowSelectedPenJasRad.data);
                            }else if(Ext.get('cboJenisTr_viPenJasRad').getValue() === 'Transaksi Lama' && rowSelectedPenJasRad.data.LUNAS === '0'){
                                ShowPesanWarningPenJasRad('Transaksi Belum Selesai');
                            }else if (Ext.get('cboJenisTr_viPenJasRad').getValue() === 'Transaksi Baru' && rowSelectedPenJasRad.data.LUNAS === '0'){
                                ShowPesanWarningPenJasRad('Blm Ada Transaksi Atas Pasien Tersebut');
                            }else{
                                ShowPesanWarningPenJasRad('Silahkan Cek Data Kembali');
                            }
                        }
                    },
                    {
                        id: 'btnBatalTransaksiRad',
                        text: 'Batal Transaksi',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            // console.log(rowSelectedPenJasRad.data);
                            var tmpdatatransaksi = rowSelectedPenJasRad.data;
                            if (Ext.get('cboJenisTr_viPenJasRad').getValue() === 'Transaksi Lama') {
                                if (rowSelectedPenJasRad === undefined) {
                                    ShowPesanWarningPenJasRad('Silahkan Pilih Data Terlebih Dahulu');
                                }else{
                                    Ext.Msg.confirm('Warning', 'Apakah Transaksi ini akan dihapus?', function(button){
                                        if (button == 'yes'){
                                            // console.log(tmpdatatransaksi);
                                            var urutmasuktransaksi;
                                            if (tmpdatatransaksi.URUT != '') {
                                                urutmasuktransaksi = tmpdatatransaksi.URUT;
                                            }else{
                                                urutmasuktransaksi = tmpdatatransaksi.URUT_MASUK;
                                            }
                                            if(rowSelectedPenJasRad != undefined) {
                                              
                                                var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan pembatalan:', function(btn, combo){
                                                 if (btn == 'ok')
                                                 {
                                                 //variablehistori=combo;
                                                 if (combo!='')
                                                 {
                                                     var params = {
                                                        kd_unit: tmpdatatransaksi.KD_UNIT,
                                                        Tglkunjungan: tmpdatatransaksi.TGL_TRANSAKSI,
                                                        Kodepasein: tmpdatatransaksi.KD_PASIEN,
                                                        urut: urutmasuktransaksi,
                                                        alasanbatal: combo
                                                    };
                                                    Ext.Ajax.request
                                                            ({
                                                                url: baseURL + "index.php/main/functionRAD/deletekunjungan",
                                                                params: params,
                                                                failure: function (o)
                                                                {
                                                                    loadMask.hide();
                                                                    ShowPesanWarningPenJasRad('Error proses database', 'Hapus Data');
                                                                },
                                                                success: function (o)
                                                                {
                                                                    var cst = Ext.decode(o.responseText);
                                                                    if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === false)
                                                                    {
                                                                        ShowPesanWarningPenJasRad('Data transaksi tidak berhasil ditemukan', 'Hapus Data Kunjungan');
                                                                    } else if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === true)
                                                                    {
                                                                        ShowPesanWarningPenJasRad('Anda telah melakukan pembayaran', 'Hapus Data Kunjungan');
                                                                    } else if (cst.success === true)
                                                                    {
                                                                        // RefreshDatahistoribayar(datapasienvariable.kd_pasien);
                                                                        ShowPesanInfoPenJasRad('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
                                                                        validasiJenisTr();
                                                                    } else if (cst.success === false && cst.pesan === 0)
                                                                    {
                                                                        ShowPesanWarningPenJasRad('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                                    } else
                                                                    {
                                                                        if (cst.kd_kasir) {
                                                                            ShowPesanWarningPenJasRad('Anda tidak diberikan hak akses untuk menghapus data kunjungan '+cst.kd_kasir, 'Hapus Data Kunjungan');
                                                                        }else{
                                                                            ShowPesanErrorPenJasRad('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                                        }
                                                                    };
                                                                }})
                                                 }
                                                 else
                                                 {
                                                 ShowPesanWarningPenJasRad('Silahkan isi alasan terlebih dahaulu','Keterangan');
                                                 }
                                                 }  
                                                 }); 
                                            }
                                        }
                                    })
                                }
                            }else {

                            }
                        }
                    },
                    {
                        id: 'btnGantiKelompokRad',
                        text: 'Ganti Kelompok',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            var tmpdatatransaksi = rowSelectedPenJasRad.data;
                            panelActiveDataPasien = 0;
                            KelompokPasienLookUp_rad(tmpdatatransaksi);
                        }
                    },
                    {
                        id: 'btnGantiDokterRad',
                        text: 'Ganti Dokter',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            var tmpdatatransaksi = rowSelectedPenJasRad.data;
                            panelActiveDataPasien = 1;
                            GantiDokterPasienLookUp_rad(tmpdatatransaksi);

                        }
                    },{                             
                        id: 'btnBukaTransaksiRAD', text: 'Buka Transaksi', tooltip: 'Buka Transaksi', iconCls: 'gantipasien', disabled:true,
                        handler: function () {
                            
                            var parameter_bukatrans = {
                                notrans_bukatrans       : rowSelectedPenJasRad.data.NO_TRANSAKSI,
                                kd_kasir_bukatrans      : rowSelectedPenJasRad.data.KD_KASIR,
                                kd_pasien_bukatrans     : rowSelectedPenJasRad.data.KD_PASIEN,
                                kd_unit_bukatrans       : rowSelectedPenJasRad.data.KD_UNIT,
                                tgl_transaksi_bukatrans : rowSelectedPenJasRad.data.TGL_TRANSAKSI,
                                urut_masuk_bukatrans    : rowSelectedPenJasRad.data.URUT_MASUK
                            }
                            msg_box_alasanbukatrans_RAD(parameter_bukatrans);
                        }
                    },
                    //------------TAMBAH BARU 27-September-2017
                    {                               
                        id: 'btnEditDataPasienRAD', text: 'Edit data Pasien', tooltip: 'Edit data Pasien', iconCls: 'gantipasien', disabled:true,
                        handler: function () {
                            console.log(rowSelectedPenJasRad.data);
                            var parameter_editpasien = {
                                medrec                      : rowSelectedPenJasRad.data.KD_PASIEN,
                                nama                        : rowSelectedPenJasRad.data.NAMA,
                                urut_masuk                  : rowSelectedPenJasRad.data.URUT_MASUK,
                                jk                          : rowSelectedPenJasRad.data.JENIS_KELAMIN,
                                tgl_lahir                   : rowSelectedPenJasRad.data.TGL_LAHIR,
                                alamat                      : rowSelectedPenJasRad.data.ALAMAT,
                                hp                          : rowSelectedPenJasRad.data.HP,
                                goldarah                    : rowSelectedPenJasRad.data.GOL_DARAH
                            }
                            setLookUpGridDataView_viRAD(parameter_editpasien);
                            //msg_box_alasanbukatrans_RAD(parameter_bukatrans);
                        }
                    },
                    {                               
                        id: 'btnCetakLabelRAD', text: 'Cetak Label', tooltip: 'Cetak Label', iconCls: 'print', disabled:false,
                        handler: function () {
                            // console.log(rowSelectedPenJasRad.data);
                            // var parameter_editpasien = {
                            //     medrec                      : rowSelectedPenJasRad.data.KD_PASIEN,
                            //     nama                        : rowSelectedPenJasRad.data.NAMA,
                            //     urut_masuk                  : rowSelectedPenJasRad.data.URUT_MASUK,
                            //     jk                          : rowSelectedPenJasRad.data.JENIS_KELAMIN,
                            //     tgl_lahir                   : rowSelectedPenJasRad.data.TGL_LAHIR,
                            //     alamat                      : rowSelectedPenJasRad.data.ALAMAT,
                            //     hp                          : rowSelectedPenJasRad.data.HP,
                            //     goldarah                    : rowSelectedPenJasRad.data.GOL_DARAH
                            // }
                            // setLookUpGridDataView_viRAD(parameter_editpasien);
                            panelPrintLabelDosisRadiologi_resepRWJ();
                            //msg_box_alasanbukatrans_RAD(parameter_bukatrans);
                        }
                    },
                ]
            }
    );
    
    //hani 20-07-2023 //add order management
    
	//  timeInterval = setInterval(function() {
    //         if (Ext.getCmp('chkorder_rab').getValue() == true){
	// 			var k_ordermanagement = "kd_bagian = 5 and kd_unit not in ('44','45') and lunas='0'  and tgl_transaksi >= '"+now.format('Y-m-d')+"' and tgl_transaksi <= '"+now.format('Y-m-d')+"' and left(kd_pasien, 2) not in ('LB')  and tgl_transaksi >='"+now.format('Y-m-d')+"' and tgl_transaksi <='"+now.format('Y-m-d')+"' ORDER BY tgl_transaksi desc, no_transaksi";
	// 			refeshpenjasrad_ordermanagement(k_ordermanagement);
	// 		}else{
	// 			// var k_ordermanagement = "kd_bagian = 5 and kd_unit not in ('44','45') and lunas='0'  and tgl_transaksi >= '"+now.format('Y-m-d')+"' and tgl_transaksi <= '"+now.format('Y-m-d')+"' and left(kd_pasien, 2) not in ('LB')  and tgl_transaksi >='"+now.format('Y-m-d')+"' and tgl_transaksi <='"+now.format('Y-m-d')+"' ORDER BY tgl_transaksi desc, no_transaksi";
	// 			var kd_unit_asal_order
	// 			console.log(radiovalues)
	// 			if(radiovalues === '1') {	
	// 				kd_unit_asal_order = '3'
	// 			}else if (radiovalues === '4') {
	// 				kd_unit_asal_order = '2'
	// 			}else if (radiovalues === '2'){
	// 				kd_unit_asal_order = '1'
	// 			}

	// 			var k_ordermanagement = "kd_bagian = 5 and kd_unit not in ('44','45') and lunas='0' and left(kd_unit_asal, 1) in('"+ kd_unit_asal_order +"')     and lunas='0'  and tgl_transaksi >= '"+now.format('Y-m-d')+"' and tgl_transaksi <= '"+now.format('Y-m-d')+"' and left(kd_pasien, 2) not in ('LB')  and tgl_transaksi >='"+now.format('Y-m-d')+"' and tgl_transaksi <='"+now.format('Y-m-d')+"' ORDER BY tgl_transaksi desc, no_transaksi";
	// 			refeshpenjasrad_ordermanagement(k_ordermanagement);
	// 		}
    //     }, 10000);
	var FieldOrderManajemen = [
            'KD_PASIEN','NO_TRANSAKSI','NAMA','ALAMAT','SPESIALISASI','KELAS','NAMA_KAMAR','KAMAR','MASUK','NO_TRANSAKSI_ASAL','KD_KASIR_ASAL',
				 'DOKTER','KD_DOKTER','KD_UNIT_KAMAR','KD_CUSTOMER','CUSTOMER','TGL_MASUK','URUT_MASUK','TGL_INAP','KD_SPESIAL','KD_KASIR','TGL_TRANSAKSI','KD_UNIT_ASAL','KD_UNIT',
				 'CO_STATUS','KD_USER','TGL_LAHIR','JENIS_KELAMIN','GOL_DARAH','POSTING_TRANSAKSI', 'TELEPON',
				 'NAMA_UNIT','POLIKLINIK','NAMA_UNIT_ASLI','KELPASIEN','LUNAS','DOKTER_ASAL','KD_DOKTER_ASAL','URUT','NAMA_UNIT_ASAL','HP','SJP','ORDER_MNG','CITO'
        ];
        // Deklarasi penamaan yang akan digunakan pada Grid # --------------      
        dataSourceGridOrder = new WebApp.DataStore({ fields: FieldOrderManajemen });
		var k_ordermanagement = "kd_bagian = 5 and left(kd_unit_asal, 1) in('1','2','3')     and lunas='0'  and tgl_transaksi >= '"+now.format('Y-m-d')+"' and tgl_transaksi <= '"+now.format('Y-m-d')+"' and left(kd_pasien, 2) not in ('RD')  and order_mng = 1 AND (dilayani = '0' or dilayani is null) ORDER BY cito desc, tgl_transaksi desc, no_transaksi";
		refeshpenjasrad_ordermanagement(k_ordermanagement);

        GridDataViewOrderManagement = new Ext.grid.EditorGridPanel({
			id:'PenjasRadiologi_Order',
			title: 'Order Management',
            store: dataSourceGridOrder,
            autoScroll: true,
            flex: 1,
            columnLines: true,
            border: true,
            style: 'padding: 4px;',
			hidden: false,
            plugins: [new Ext.ux.grid.FilterRow()],
            selModel: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                            // rowSelectedPenJasRad = dataSourceGridOrder.getAt(row);
                             rowSelectedPenJasRad = dataSourceGridOrder.getAt(row);
                            kdkasir_tmp          = rowSelectedPenJasRad.data.KD_KASIR;
                            vKdUnit              = rowSelectedPenJasRad.data.KD_UNIT;
                            vUrutMasuk           = rowSelectedPenJasRad.data.URUT_MASUK;
                            vTanggal             = rowSelectedPenJasRad.data.MASUK;
                            if(Ext.get('cboJenisTr_viPenJasRad').getValue()=='Transaksi Baru'){
                                Ext.getCmp('btnBatalTransaksiRad').disable();
                                Ext.getCmp('btnGantiKelompokRad').disable();
                                Ext.getCmp('btnGantiDokterRad').disable();
                                Ext.getCmp('btnBukaTransaksiRAD').disable();
                                Ext.getCmp('btnCetakLabelRAD').disable();
                            }
                            else
                            {
                                
                                //alert(vKdUnit);
                                if (rowSelectedPenJasRad.data.LUNAS==='1' || rowSelectedPenJasRad.data.LUNAS==='1' || rowSelectedPenJasRad.data.LUNAS===1){
                                    Ext.getCmp('btnGantiKelompokRad').disable();
                                    Ext.getCmp('btnGantiDokterRad').disable();
                                    if (rowSelectedPenJasRad.data.CO_STATUS==='1' || rowSelectedPenJasRad.data.CO_STATUS==='1' || rowSelectedPenJasRad.data.CO_STATUS===1){
                                        Ext.getCmp('btnBukaTransaksiRAD').enable();
                                    }else{
                                        Ext.getCmp('btnBukaTransaksiRAD').disable();
                                    }
                                    
                                    Ext.getCmp('btnBatalTransaksiRad').disable();
                                }else{
                                    Ext.getCmp('btnGantiKelompokRad').enable();
                                    Ext.getCmp('btnGantiDokterRad').enable();
                                    Ext.getCmp('btnBukaTransaksiRAD').disable();
                                    Ext.getCmp('btnBatalTransaksiRad').enable();
                                }
                                
                            }
                    }
                }
            }),
            listeners: {
                rowdblclick: function(sm, ridx, cidx) {
                    // rowSelectedPenJasRad = dataSourceGridOrder.getAt(ridx);
					rowSelectedPenJasRad = dataSourceGridOrder.getAt(ridx);
                    // console.log(rowSelectedPenJasRad)
					vKdUnit= rowSelectedPenJasRad.data.KD_UNIT;
					vUrutMasuk= rowSelectedPenJasRad.data.URUT_MASUK;
					vTanggal= rowSelectedPenJasRad.data.MASUK;
                    noTransaksiPilihan = rowSelectedPenJasRad.data.NO_TRANSAKSI;
                    if (rowSelectedPenJasRad !== undefined)
                    {
                        // console.log(rowSelectedPenJasRad.data);
                        PenJasRadLookUp(rowSelectedPenJasRad.data);
                    }
                    else
                    {
                        PenJasRadLookUp();
                        Ext.getCmp('cboDOKTER_viPenJasRad').disable();
                        Ext.getCmp('cboGDR').disable();
                        Ext.getCmp('cboJK').disable();
                    }

                    get_total_bayar(rowSelectedPenJasRad.data.NO_TRANSAKSI, rowSelectedPenJasRad.data.KD_KASIR);
                    get_unit_asal(rowSelectedPenJasRad.data.NO_TRANSAKSI, rowSelectedPenJasRad.data.KD_KASIR);
                }
            },
            colModel: new Ext.grid.ColumnModel([
                new Ext.grid.RowNumberer(),
                {
                    header: 'Cito',
                    dataIndex: 'CITO',
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    width: 20,
                    renderer: function(v, params, record){
                        if(record.data.CITO == 1){
                            params.style  ='background:#FF0000; width: 82px; font-weight: bolder;';
                            return 'Ya';
                        }else{
                            return 'Tidak';
                        }
				}
                },
                {
                    header: 'No Transaksi',
                    dataIndex: 'NO_TRANSAKSI',
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    width: 30
                },{
                    header: 'Tgl. Transaksi',
                    dataIndex: 'TGL_TRANSAKSI',
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    width: 30,
					renderer: function(v, params, record){
					return ShowDate(record.data.TGL_TRANSAKSI);
				}
                },{
                    header: 'No Medrec',
                    dataIndex: 'KD_PASIEN',
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    width: 30
                },{
                    header: 'Pasien',
                    dataIndex: 'NAMA',
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    width: 30
                }, {
                    header: 'Alamat',
                    dataIndex: 'ALAMAT',
                    sortable: false,
                    width: 50
                }, {
                    header: 'Pelaksana',
                    dataIndex: 'DOKTER',
                    sortable: false,
                    width: 40
                }, {
                    header: 'Unit',
                    dataIndex: 'NAMA_UNIT_ASAL',
                    width: 30,
                    sortable: false,
                    hideable: false,
                    menuDisabled: true
                }, {
                    header: 'Status Lunas',
                    dataIndex: 'LUNAS',
                    sortable: false,
                    width: 40,
					renderer: function(value, metaData, record, rowIndex, colIndex, store){
					switch (value){
						case '1':
							metaData.css = 'StatusHijau'; // 
							break;
						case '0':
							metaData.css = 'StatusMerah'; // rejected
							break;
						case '':
							metaData.css = 'StatusMerah'; // rejected
							break;
						case undefined:
							metaData.css = 'StatusMerah'; // rejected
							break;
					}
					return '';
				}
                },{
                    header: 'Status Transaksi',
                    dataIndex: 'CO_STATUS',
                    sortable: false,
                    width: 40,
					renderer: function(value, metaData, record, rowIndex, colIndex, store){
					switch (value){
						case 1:
							metaData.css = 'StatusHijau'; // 
							break;
						case 0:
							metaData.css = 'StatusMerah'; // rejected
							break;
						case '':
							metaData.css = 'StatusMerah'; // rejected
							break;
						case undefined:
							metaData.css = 'StatusMerah'; // rejected
							break;
					}
					return '';
				}
                },
            ]),
            tbar: {
                xtype: 'toolbar',
                id: 'toolbarOrderManajemen',
                items: [
                    { xtype: 'tbspacer', height: 3, width: 5 },
                    {
                        x: 40,
                        y: 40,
                        xtype: 'checkbox',
                        name: 'chkorder',
                        id: 'chkorder_rab',
                        boxLabel: 'Semua Order Mangement',
                        width: 50,
                        // disabled: true,
                        checked: true,
                        listeners: {},
						handler: function (a, b) {
								// console.log(a)
							if (a.checked == true){
								var k_ordermanagement = "kd_bagian = 5 and lunas='0'  and tgl_transaksi >= '"+now.format('Y-m-d')+"' and tgl_transaksi <= '"+now.format('Y-m-d')+"' and left(kd_pasien, 2) not in ('RD')   and order_mng = 1 AND (dilayani = '0' or dilayani is null) ORDER BY cito desc, tgl_transaksi desc, no_transaksi";
								refeshpenjasrad_ordermanagement(k_ordermanagement);
							}else{
								// var k_ordermanagement = "kd_bagian = 5 and kd_unit not in ('44','45') and lunas='0'  and tgl_transaksi >= '"+now.format('Y-m-d')+"' and tgl_transaksi <= '"+now.format('Y-m-d')+"' and left(kd_pasien, 2) not in ('LB')  and tgl_transaksi >='"+now.format('Y-m-d')+"' and tgl_transaksi <='"+now.format('Y-m-d')+"' ORDER BY tgl_transaksi desc, no_transaksi";
								var kd_unit_asal_order
								console.log(radiovalues)
								if(radiovalues === '1') {	
									kd_unit_asal_order = '3'
								}else if (radiovalues === '4') {
									kd_unit_asal_order = '2'
								}else if (radiovalues === '2'){
									kd_unit_asal_order = '1'
								}

								var k_ordermanagement = "kd_bagian = 5  and left(kd_unit_asal, 1) in('"+ kd_unit_asal_order +"')     and lunas='0'  and tgl_transaksi >= '"+now.format('Y-m-d')+"' and tgl_transaksi <= '"+now.format('Y-m-d')+"' and left(kd_pasien, 2) not in ('LB')  and tgl_transaksi >='"+now.format('Y-m-d')+"' and order_mng = 1 AND (dilayani = '0' or dilayani is null) and tgl_transaksi <='"+now.format('Y-m-d')+"' ORDER BY cito desc, tgl_transaksi desc, no_transaksi";
								refeshpenjasrad_ordermanagement(k_ordermanagement);
							}
						},
                    },
                    { xtype: 'tbspacer', height: 3, width: 180 },
                    {
                        xtype: 'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        disabled: false,
						hidden:true,
                        id: 'btnRefreshOrder',
                        handler: function() {
                            viewGridOrderAll_RASEPRWJ();
                            total_pasien_order_mng_obtrwj();
                            total_pasien_dilayani_order_mng_obtrwj();
                            ordermanajemen = false;
                        }
                    },
                    {
                        xtype: 'label',
						hidden:true,
                        text: '*) Enter untuk mencari'
                    },
                    { xtype: 'tbspacer', height: 3, width: 580 },
                ]
            },
            viewConfig: {
                forceFit: true
            }
        });
	
//======================================================================================
   
    
    //form depan awal dan judul tab
    var FormDepanPenJasRad = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            // layout: 'form',
            layout:{
                type:'vbox',
                align:'stretch'
            },
            title: 'Pelayanan Radiologi',
            border: false,
            // shadhow: true,
            autoScroll:false,
            // width: '100%',
            // iconCls: 'Request',
            // margins: '0 5 5 0',
            items: [
                        getItemPanelPenJasRadAwal(),
                        grListPenJasRad,
                        GridDataViewOrderManagement
                   ],
            listeners:
            {
                'afterrender': function()
                {
                    Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
                    Ext.getCmp('cboStatusLunas_viPenJasRad').disable();
                    Ext.getCmp('btnBatalTransaksiRad').disable();
                    Ext.getCmp('btnGantiKelompokRad').disable();
                    Ext.getCmp('btnGantiDokterRad').disable();
                    Ext.getCmp('btnBukaTransaksiRAD').disable();    
                    Ext.getCmp('btnCetakLabelRAD').disable();
                }
            }
        }
    );
    
   return FormDepanPenJasRad;

};
//hani 20-07-2023 //order management
var refeshpenjasrad_ordermanagement;
function refeshpenjasrad_ordermanagement(kriteria)
{
	dataSourceGridOrder.load
		(
                    { 
                        params:  
                        {   
                            Skip: 0, 
                            Take: '',
                            Sort: '',
                            Sortdir: 'ASC', 
                            target: 'ViewPenJasRad',
                            param : kriteria
                        }			
                    }
		);   
    return dataSourceGridOrder;
}

//============================================================

//=================CETAK LABEL================================
function panelPrintLabelDosisRadiologi_resepRWJ() {

        panelWindowPrintLabelDosisRadiologi_ResepRWJ = new Ext.Window({
            id: 'pnlWindowPrintLabelDosisRadiologi_ResepRWJ',
            title: 'List Label Radiologi',
            width: 810,
            height: 300,
            border: false,
            resizable: false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
                gridListRadiologiResepRWJ(),
            ],
            fbar: [{
                    xtype: 'button',
                    text: 'OK',
                    width: 70,
                    hideLabel: true,
                    id: 'btnOkPrintLabelDosisRadiologi_ResepRWJ',
                    handler: function() {

                        cetak_label_Radiologi();
                        panelWindowPrintLabelDosisRadiologi_ResepRWJ.close();
                        // setLookUps_viApotekResepRWJ.close();
                        // Ext.getCmp('txtKdNamaPasien').focus(true);

                    }
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    width: 70,
                    hideLabel: true,
                    id: 'btnCancelPrintLabelDosisRadiologi_ResepRWJ',
                    handler: function() {
                        panelWindowPrintLabelDosisRadiologi_ResepRWJ.close();
                    }
                }
            ],
            listeners: {
                activate: function() {


                },
                afterShow: function() {
                    this.activate();

                },
                deactivate: function() {

                },
                close: function() {
                    // Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').focus(false, 10);
                }
            }
        });

        panelWindowPrintLabelDosisRadiologi_ResepRWJ.show();
        getListRadiologiDosisRadiologi_ResepRWJ(rowSelectedPenJasRad.data.NO_TRANSAKSI,rowSelectedPenJasRad.data.KD_KASIR);
        // ViewGridBawahLookupPenjasRad(rowSelectedPenJasRad.data.NO_TRANSAKSI,rowSelectedPenJasRad.data.KD_KASIR);
    };

    function getListRadiologiDosisRadiologi_ResepRWJ(no_transaksi, kd_kasir) {
        var strKriteriaRAD='';
        if (kd_kasir !== undefined) {
            strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
        }else{
            strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
        }
        // strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" ;//+ " And kd_kasir ='03'";
        dsGridListDosisRadiologi_ResepRWJ.load({
            params:{
                Skip: 0,
                Take: 1000,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target: 'ViewDetailGridBawahPenJasRad',
                param: strKriteriaRAD
            }
        });
        return dsGridListDosisRadiologi_ResepRWJ;

        // Ext.Ajax.request({
        //     url: baseURL + "index.php/rad/functionAPOTEK/getListDosisRadiologi",
        //     params: { query: "o.no_out = " + Ext.getCmp('txtTmpNoout').getValue() + "" + " And o.tgl_out ='" + Ext.getCmp('txtTmpTglout').getValue() + "'  ", resep: 'Y' },
        //     /*params: {
        //     	no_out:Ext.getCmp('txtTmpNoout').getValue(),
        //     	tgl_out:Ext.getCmp('txtTmpTglout').getValue(),
        //     },*/
        //     failure: function(o) {
        //         ShowPesanErrorResepRWJ('Error list dosis Radiologi. Hubungi Admin!', 'Error');
        //     },
        //     success: function(o) {
        //         dsGridListDosisRadiologi_ResepRWJ.removeAll();
        //         var cst = Ext.decode(o.responseText);

        //         if (cst.success === true) {
        //             var recs = [],
        //                 recType = dsGridListDosisRadiologi_ResepRWJ.recordType;
        //             for (var i = 0; i < cst.ListDataObj.length; i++) {
        //                 recs.push(new recType(cst.ListDataObj[i]));
        //             }
        //             dsGridListDosisRadiologi_ResepRWJ.add(recs);
        //             GridListDosisRadiologi_ResepRWJ.getView().refresh();
        //             console.log(dsGridListDosisRadiologi_ResepRWJ);
        //             /*GridListDosisRadiologi_ResepRWJ.getSelectionModel().selectRow(0);
        //             GridListDosisRadiologi_ResepRWJ.getView().focusRow(0);*/
        //             //	GridListDosisRadiologi_ResepRWJ.startEditing(0, 1);
        //         } else {
        //             ShowPesanErrorResepRWJ('Gagal membaca data list dosis Radiologi', 'Error');
        //         };
        //     }
        // });
    }

    function gridListRadiologiResepRWJ() {
        // var fldDetail = ['pilih', 'kd_prd', 'nama_Radiologi', 'jml_out', 'dosis', 'satuan', 'cara_pakai', 'takaran'];
         var fldDetail = ['pilih','cito','kd_produk','deskripsi','deskripsi2','kd_unit','kd_tarif','harga','qty','desc_req','tgl_berlaku',
    'no_transaksi','urut','desc_status','tgl_transaksi','kp_produk','dokter','group','jumlah_dokter','kd_kasir','catatan'];
        dsGridListDosisRadiologi_ResepRWJ = new WebApp.DataStore({ fields: fldDetail });
        //ISAL//
        GridListDosisRadiologi_ResepRWJ = new Ext.grid.EditorGridPanel({
            id: 'GridListDosisRadiologi_ResepRWJ',
            store: dsGridListDosisRadiologi_ResepRWJ,
            autoScroll: true,
            columnLines: true,
            border: true,
            width: 800,
            height: 230,
            stripeRows: true,
            title: '',
            anchor: '100% 100%',
            plugins: [new Ext.ux.grid.FilterRow()],
            selModel: new Ext.grid.CellSelectionModel
                // Tanda aktif saat salah satu baris dipilih # --------------
                ({
                    singleSelect: true,
                    listeners: {
                        cellselect: function(sm, row, rec) {
                            cell_select_index_label = rec;
                            console.log(cell_select_index_label);
                            var line = GridListDosisRadiologi_ResepRWJ.getSelectionModel().selection.cell[0];
                            if (cell_select_index_label == 1) {
                                var o = dsGridListDosisRadiologi_ResepRWJ.getRange()[line].data;
                                console.log(o.pilih);
                                if (o.pilih == undefined || o.pilih == '') {
                                    o.pilih = 'V';
                                } else {
                                    o.pilih = '';
                                }
                                GridListDosisRadiologi_ResepRWJ.getView().refresh();
                            }


                        }
                    }
                }),
            colModel: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    // sm, 
                    { //1
                        dataIndex: 'pilih',
                        header: 'Pilih',
                        width: 20,
                        menuDisabled: false,
                        align: 'center',
                        editor: new Ext.form.TextField({
                            allowBlank: false,
                            enableKeyEvents: true,
                            maxLength: 1,
                            readOnly: true,
                            listeners: {
                                keyDown: function(a, b, c) {
                                    if (b.getKey() == 32) {
                                        var line = GridListDosisRadiologi_ResepRWJ.getSelectionModel().selection.cell[0];
                                        var o = dsGridListDosisRadiologi_ResepRWJ.getRange()[line].data;
                                        console.log(o.pilih);
                                        if (o.pilih == undefined || o.pilih == '') {
                                            o.pilih = 'V';
                                        } else {
                                            o.pilih = '';
                                        }
                                        GridListDosisRadiologi_ResepRWJ.getView().refresh();
                                        if (line + 1 == dsGridListDosisRadiologi_ResepRWJ.getCount()) {

                                            GridListDosisRadiologi_ResepRWJ.startEditing(0, 1);
                                        } else {
                                            GridListDosisRadiologi_ResepRWJ.startEditing(line + 1, 1);
                                        }
                                    }

                                    if (b.getKey() == 13) {
                                        Ext.getCmp('cboJenisEtiket').focus(true, 100);
                                    }
                                }
                            }
                        })
                    },
                    { //2
                        dataIndex: 'kd_produk',
                        header: 'Kode',
                        width: 70,
                        menuDisabled: true,
                    },
                    { //3
                        dataIndex: 'deskripsi',
                        header: 'Nama Pemeriksaan',
                        width: 130,
                        menuDisabled: true,
                    },
                    { //5
                        dataIndex: 'qty',
                        header: 'Qty',
                        align: 'center',
                        width: 30,
                        menuDisabled: true,
                    },

                ]),
            // sm: sm,	
            listeners: {
                'cellclick': function(grd, rowIndex, colIndex, e) {
           
                }

            },
            viewConfig: {
                forceFit: true
            }
        });
        return GridListDosisRadiologi_ResepRWJ;
    }

    function cetak_label_Radiologi() {
        var params = {
            kd_pasien: rowSelectedPenJasRad.data.KD_PASIEN,
            dokter: rowSelectedPenJasRad.data.DOKTER,
            dokter_asal: rowSelectedPenJasRad.data.DOKTER_ASAL,
            nama: rowSelectedPenJasRad.data.NAMA,
            nama_unit_asal: rowSelectedPenJasRad.data.NAMA_UNIT_ASAL,
            kd_unit_asal: rowSelectedPenJasRad.data.KD_UNIT_ASAL,
            no_transaksi: rowSelectedPenJasRad.data.NO_TRANSAKSI,
            tgl_transaksi: rowSelectedPenJasRad.data.TGL_TRANSAKSI,
            tgl_lahir: rowSelectedPenJasRad.data.TGL_LAHIR,
            list_Radiologi: getParamCetakLabelRadiologi_Resep()
        };
        console.log(params);
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("target", "_blank");
        form.setAttribute("action", baseURL + "index.php/rad/cetak_label_radiologi/print_label_Radiologi_pdf");
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "data");
        hiddenField.setAttribute("value", Ext.encode(params));
        form.appendChild(hiddenField);
        document.body.appendChild(form);
        form.submit();
    }

    function getParamCetakLabelRadiologi_Resep() {
        
            var params = {

            }
            //==================== PARAM TAMPUNG LIST Radiologi ETIKET JIKA MENGGUNAKAN EVENT KEYBOARD =====================

        var data_item = [];
        var items = dsGridListDosisRadiologi_ResepRWJ.data.items;
        console.log(items);
        for (var i = 0, iLen = items.length; i < iLen; i++) {
            if (items[i].data.pilih == 'V') {
                var data = {};
                data.kd_produk = items[i].data.kd_produk;
                data.deskripsi = items[i].data.deskripsi;
                data_item.push(data);
            }
        }

        params['list_Radiologi'] = data_item;


        return params;
    }
//============================================================
function mComboStatusBayar_viPenJasRad()
{
  var cboStatus_viPenJasRad = new Ext.form.ComboBox
    (
        {
                    id:'cboStatus_viPenJasRad',
                    x: 155,
                    y: 70,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusByr_viKasirRwj,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusByr_viKasirRwj=b.data.displayText ;
                            }
                    }
        }
    );
    return cboStatus_viPenJasRad;
};
function mComboStatusLunas_viPenJasRad()
{
  var cboStatusLunas_viPenJasRad = new Ext.form.ComboBox
    (
        {
                    id:'cboStatusLunas_viPenJasRad',
                    x: 155,
                    y: 100,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Lunas'], [3, 'Belum Lunas']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusLunasByr_viKasirRwj,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusLunasByr_viKasirRwj=b.data.displayText ;
                            }
                    }
        }
    );
    return cboStatusLunas_viPenJasRad;
};

//COMBO JENIS TRANSAKSI
function mComboJenisTrans_viPenJasRad()
{
  var cboJenisTr_viPenJasRad = new Ext.form.ComboBox
    (
        {
                    id:'cboJenisTr_viPenJasRad',
                    x: 155,
                    y: 10,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS TRANSAKSI',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Transaksi Baru'],[2, 'Transaksi Lama']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountJenTr_viPenJasRad,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                combovalues=b.data.displayText;
                                if(b.data.Id==1){
                                    Ext.getCmp('cbounitrads_viPenJasRad').setValue(null);
                                    Ext.getCmp('cbounitrads_viPenJasRad').disable();
                                    Ext.getCmp('btnBatalTransaksiRad').disable();
                                    Ext.getCmp('btnGantiKelompokRad').disable();
                                    Ext.getCmp('btnGantiDokterRad').disable();
                                    Ext.getCmp('btnBukaTransaksiRAD').disable();
                                    Ext.getCmp('btnCetakLabelRAD').disable();
                                    tmppasienbarulama = 'Baru';
					                GridDataViewOrderManagement.show();
                                    // -------uncheck Semua Order Manaement---------
                                    order_rad = true;
                                    Ext.getCmp('chkorder_rab').setValue(false);
                                    // ---------------------------------------------
                                }else{
                                    Ext.getCmp('cbounitrads_viPenJasRad').enable();
                                    Ext.getCmp('cbounitrads_viPenJasRad').setValue(combovaluesunittujuan);
                                    tmppasienbarulama = 'Lama';
                                    Ext.getCmp('btnBatalTransaksiRad').enable();
                                    Ext.getCmp('btnGantiKelompokRad').enable();
                                    Ext.getCmp('btnGantiDokterRad').enable();
                                    Ext.getCmp('btnBukaTransaksiRAD').enable();
                                    Ext.getCmp('btnCetakLabelRAD').enable();
                                    order_rad = false;
					                GridDataViewOrderManagement.hide();
                                    /* Ext.getCmp('cbounitrads_viPenJasRad').enable();
                                    Ext.getCmp('cbounitrads_viPenJasRad').setValue(combovaluesunittujuan);
                                    tmppasienbarulama = 'Lama';
                                    if (rowSelectedPenJasRad.data.LUNAS==='t' || rowSelectedPenJasRad.data.LUNAS==='true' || rowSelectedPenJasRad.data.LUNAS===true){
                                        Ext.getCmp('btnGantiKelompokRad').disable();
                                        Ext.getCmp('btnGantiDokterRad').disable();
                                        if (rowSelectedPenJasRad.data.CO_STATUS==='t' || rowSelectedPenJasRad.data.CO_STATUS==='true' || rowSelectedPenJasRad.data.CO_STATUS===true){
                                            Ext.getCmp('btnBukaTransaksiRAD').enable();
                                        }else{
                                            Ext.getCmp('btnBukaTransaksiRAD').disable();
                                        }
                                        Ext.getCmp('btnBatalTransaksiRad').disable();
                                        //Ext.getCmp('btnHapusKunjunganLAB').disable();
                                    }else{
                                        Ext.getCmp('btnGantiKelompokRad').enable();
                                        Ext.getCmp('btnGantiDokterRad').enable();
                                        Ext.getCmp('btnBukaTransaksiRAD').disable();
                                        Ext.getCmp('btnBatalTransaksiRad').enable();
                                        //Ext.getCmp('btnHapusKunjunganLAB').enable();
                                    } */
                                }
                                
                                validasiJenisTr();
                                
                            }
                    }
        }
    );
    return cboJenisTr_viPenJasRad;
};

var tmppasienbarulama = 'Baru';
function mComboUnitTujuans_viPenJasRad(){
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitrads_viPenJasRad = new WebApp.DataStore({ fields: Field });
    dsunitrads_viPenJasRad.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'nama_unit',
            Sortdir: 'ASC',
            target: 'ViewSetupUnit',
            param: "kd_bagian = '5'"
        }
    });
    var cbounitrads_viPenJasRad = new Ext.form.ComboBox({
        id: 'cbounitrads_viPenJasRad',
        x: 455,
        y: 10,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        emptyText: '',
        disabled:true,
        fieldLabel:  ' ',
        align: 'Right',
        width: 230,
        emptyText:'Pilih Unit',
        store: dsunitrads_viPenJasRad,
        valueField: 'KD_UNIT',
        displayField: 'NAMA_UNIT',
        //value:'All',
        editable: false,
        listeners:{
            'select': function(a,b,c){
                combovaluesunittujuan=b.data.KD_UNIT;
                validasiJenisTr();
            }
        }
    });
    return cbounitrads_viPenJasRad;
    
};

/* di command karena load di ganti lagi ke postgre karena ini sudah di custom untuk load ke sql
function validasiJenisTr(){
    var kriteria = "";
    var strkriteria=getCriteriaFilter_viDaftar();
    console.log(radiovalues);
    if (combovalues === 'Transaksi Lama')
    {
        Ext.getCmp('cboStatusLunas_viPenJasRad').enable();
    
        if(radiovalues === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarad = "";
                }else
                {
                    tmpkreteriarad = "and kd_unit_asal = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianamarwi = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianamarwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrecrwi = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrecrwi = "";
            }
            if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kunjungan.kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }
            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = "|| WHERE lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "|| WHERE lunas='f'";
                }else
                {
                    tmplunas = "|| ";
                }
            }
            
            kriteria = " U.KD_BAGIAN = 5 and LEFT(uAsal.KD_UNIT,1) in('3') and LEFT(kunjungan.KD_PASIEN,2) not in ('RD') "+tmpkriteriaunittujuancrwi+" "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriamedrecrwi +" "+strkriteria+"   ";
            tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit, tmplunas);
            
        }else if (radiovalues === '2'){
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and kd_unit_asal = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else
            {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kunjungan.kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }
            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = "|| WHERE lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "|| WHERE lunas='f'";
                }else
                {
                    tmplunas = "|| ";
                }
            }
            kriteria = " U.KD_BAGIAN = 5 "+tmpkriteriaunittujuancrwi+" "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriamedrec +"   and LEFT(kunjungan.KD_PASIEN,2) not in ('RD')   and LEFT(uAsal.KD_UNIT,1) = '1'  "+strkriteria+" ";
            tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit, tmplunas);
        }else if (radiovalues === '3')
        {
            var strKriteriatgl = "";
            var tmptanggalawal = Ext.getCmp('dtpTglAwalFilterPenJasRad').getValue();
            var tmptanggalakhir = Ext.getCmp('dtpTglAkhirFilterPenJasRad').getValue();
            var tglawal = tmptanggalawal.getFullYear() + '/' + (tmptanggalawal.getMonth() + 1) + '/' + tmptanggalawal.getDate();
            var tglakhir =  tmptanggalakhir.getFullYear() + '/' + (tmptanggalakhir.getMonth() + 1) + '/' + tmptanggalakhir.getDate();
           
            strKriteriatgl = "and tgl_transaksi >= '" + tglawal + "' and tgl_transaksi <= '" + tglakhir+"'" ;

            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }
            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "|| ";
                }
            }
            kriteria = " KD_BAGIAN = 5 "+tmpkriteriaunittujuancrwi+" "+ tmpkriterianama +""+ tmpkriteriamedrec +"  and LEFT(KD_PASIEN,2) = 'RD' "+strKriteriatgl+" "+tmplunas+" ";
            tmpunit = 'ViewPenJasRadKunjunganLangsung';
            // console.log(strKriteriatgl);
            loadpenjasrad(kriteria, tmpunit, tmplunas);
        
        }else if (radiovalues === '4')
        {
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kunjungan.kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }
            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                   tmplunas = "|| WHERE lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "|| WHERE lunas='f'";
                }else
                {
                    tmplunas = "|| ";
                }
            }//
            kriteria = " U.KD_BAGIAN = 5 and LEFT(uAsal.KD_UNIT,1) in('2') "+tmpkriteriaunittujuancrwi+"   and LEFT(kunjungan.KD_PASIEN,2) not in ('RD')  "+strkriteria+"  ";
            tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit, tmplunas);
        }else
        {
            kriteria = "posting_transaksi = 'f'  and U.KD_BAGIAN = 5 and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' ";
            tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit);
        }
        
    }else if (combovalues === 'Transaksi Baru')
    {
        Ext.getCmp('cboStatusLunas_viPenJasRad').disable();
        if(radiovalues === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarad = "";
                }else
                {
                tmpkreteriarad = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianamarwi = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrecrwi = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else{
                tmpkriteriamedrecrwi = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriamedrecrwi +" and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc ";
            tmpunit = 'ViewRwjPenjasRad';
            //loadpenjasrad(tmpparams, tmpunit);
            refeshpenjasrad(tmpparams);
        }
        else if (radiovalues === '2')
        {
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and u.kd_unit = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else {
                tmpkriteriamedrec = "";
            }
             tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" and left(u.kd_unit,1) IN ('1') ORDER BY tr.no_transaksi desc";
             tmpunit = 'ViewRwiPenjasRad';
             //loadpenjasrad(tmpparams, tmpunit);
             refeshpenjasrad(tmpparams);
             
        }else if (radiovalues === '3')
        {
            PenJasRadLookUp();
        }else if (radiovalues === '4')
        {
             if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarad = "";
                }else
                {
                tmpkreteriarad = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianamarwi = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrecrwi = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else{
                tmpkriteriamedrecrwi = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriamedrecrwi +" and left(u.kd_unit,1) IN ('2') ORDER BY tr.no_transaksi desc ";
            tmpunit = 'ViewRwjPenjasRad';
            //loadpenjasrad(tmpparams, tmpunit);
            refeshpenjasrad(tmpparams);
        }
        
    }
}
*/

function validasiJenisTr(){

    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/getDefaultUnit",
        params: {
            notrans: 0,
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            // console.log(cst.kd_unit);
            kd_unit_konfig = "'"+cst.kd_unit+"'";
        }
    });

    var kriteria = "";
    var strkriteria = getCriteriaFilter_viDaftar();
    if (combovalues === 'Transaksi Lama')
    {
        Ext.getCmp('cboStatusLunas_viPenJasRad').enable();
    
        if(radiovalues === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarad = "";
                }else
                {
                    tmpkreteriarad = "and kd_unit_asal = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianamarwi = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianamarwi = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamatrwi = " AND lower(ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamatrwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrecrwi = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrecrwi = "";
            }

            if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }

            // tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";
            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = "and lunas='1'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "and lunas='0'";
                }else
                {
                    tmplunas = "";
                }
            }
            // IGD
            console.log("A");
            kriteria = " kd_bagian = 5 and left(kd_unit_asal, 1) in('3') and left(kd_pasien, 2) not in ('RD') "+tmpkriteriaunittujuancrwi+" "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" "+tmplunas+" "+strkriteria+"   ORDER BY tgl_transaksi desc, no_transaksi ";
            tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit);
            
        }else if (radiovalues === '2'){   
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and kd_unit_asal = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else
            {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamat = " AND lower(ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamat = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            
            if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }

            // tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";

            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = "and lunas='1'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "and lunas='0'";
                }else
                {
                    tmplunas = "";
                }
            }
            console.log("B");
            kriteria = " kd_bagian = 5 "+tmpkriteriaunittujuancrwi+" "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +"  "+tmplunas+" and left(kd_pasien, 2) not in ('RD')   and left(kd_unit_asal, 1) = '1'  "+strkriteria+" ORDER BY tgl_transaksi desc, no_transaksi ";
            tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit);
        }else if (radiovalues === '3')
        {
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamat = " AND lower(ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamat = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }

            if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }
            // tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";
            
            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = " and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = " and lunas='f' ";
                }else
                {
                    tmplunas = "";
                }
            }
            console.log("C");
            kriteria = " kd_bagian = 5 "+tmpkriteriaunittujuancrwi+" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +"   and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+tmplunas+"   ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasRadKunjunganLangsung';
            loadpenjasrad(kriteria, tmpunit);
            
        }else if (radiovalues === '4')
        {
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }

             if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamat = " AND lower(ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamat = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }
            // tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";

            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                   tmplunas = "and lunas='1'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "and lunas='0'";
                }else
                {
                    tmplunas = "";
                }
            }
            console.log("D");
            //"+tmplunas+"
            // kriteria = " kd_bagian = 5 and left(kd_unit_asal, 1) in('2') "+tmpkriteriaunittujuancrwi+" "+tmplunas+"  and left(kd_pasien, 2) not in ('RD')  and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "'  ORDER BY tgl_transaksi desc, no_transaksi";
            kriteria = " kd_bagian = 5 "+tmpkriteriaunittujuancrwi+" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +"  "+tmplunas+" and left(kd_pasien, 2) not in ('RD')   and left(kd_unit_asal, 1) in('2')  "+strkriteria+" ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit);
        }else
        {
            kriteria = "posting_transaksi = '0'  and kd_bagian = 5 and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasRad';
            loadpenjasrad(kriteria, tmpunit);
        }
        
    }else if (combovalues === 'Transaksi Baru')
    {
        Ext.getCmp('cboStatusLunas_viPenJasRad').disable();
        if(radiovalues === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarad = "";
                }else
                {
                tmpkreteriarad = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianamarwi = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamatrwi = " AND lower(pasien.ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamatrwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrecrwi = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else{
                tmpkriteriamedrecrwi = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc";
            tmpunit = 'ViewRwjPenjasRad';
            //loadpenjasrad(tmpparams, tmpunit);
            refeshpenjasrad(tmpparams);
            // var k_ordermanagement  = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" and left(uasal.kd_unit,1) IN ('3')  and tr.order_mng = 1 AND ua.dilayani = '0' ORDER BY tr.no_transaksi desc";
            var k_ordermanagement  = " kd_bagian = 5 and left(kd_unit_asal, 1) in('3') and left(kd_pasien, 2) not in ('RD') and lunas='0' and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "'  and order_mng = 1 AND (dilayani = '0' or dilayani is null) ORDER BY cito desc, tgl_transaksi desc, no_transaksi ";
		    refeshpenjasrad_ordermanagement(k_ordermanagement);
        }
        else if (radiovalues === '2')
        {
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and u.kd_unit = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianama = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else {
                tmpkriterianama = "";
            }

             if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamat = " AND lower(pasien.ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamat = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrec = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else {
                tmpkriteriamedrec = "";
            }
             tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +" and left(u.kd_unit,1) IN ('1') ORDER BY tr.no_transaksi desc";
             tmpunit = 'ViewRwiPenjasRad';
             //loadpenjasrad(tmpparams, tmpunit);
             refeshpenjasrad(tmpparams);
            //  var k_ordermanagement = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +" and left(uasal.kd_unit,1) IN ('1') and tr.order_mng = 1 AND ua.dilayani = '0'  ORDER BY tr.no_transaksi desc";
             var k_ordermanagement = " kd_bagian = 5      and lunas='0' and left(kd_pasien, 2) not in ('RD')   and left(kd_unit_asal, 1) in('1')  and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' and order_mng = 1 AND (dilayani = '0' or dilayani is null) ORDER BY cito desc, tgl_transaksi desc, no_transaksi";
		    refeshpenjasrad_ordermanagement(k_ordermanagement);
             
        }else if (radiovalues === '3')
        {
            PenJasRadLookUp();
            Ext.getCmp('txtnotlprad').setReadOnly(false);
             var k_ordermanagement = " kd_bagian = 5       and lunas='0' and left(kd_pasien, 2) not in ('RD')   and left(kd_unit_asal, 1) = '2'  and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "'  and order_mng = 1 AND (dilayani = '0' or dilayani is null) ORDER BY cito desc, tgl_transaksi desc, no_transaksi ";
		    refeshpenjasrad_ordermanagement(k_ordermanagement);

        }else if (radiovalues === '4')
        {
             if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarad = "";
                }else
                {
                tmpkreteriarad = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienPenJasRad').getValue())
            {
               tmpkriterianamarwi = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasRad').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamatrwi = " AND lower(pasien.ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamatrwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecPenJasRad').getValue())
            {
               tmpkriteriamedrecrwi = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasRad').dom.value + "'";
            }else{
                tmpkriteriamedrecrwi = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" and left(u.kd_unit,1) IN ('2') ORDER BY tr.no_transaksi desc";
            tmpunit = 'ViewRwjPenjasRad';
            //loadpenjasrad(tmpparams, tmpunit);
            refeshpenjasrad(tmpparams);
            //  var k_ordermanagement = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "' "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" and left(uasal.kd_unit,1) IN ('2') and tr.order_mng = 1 AND ua.dilayani = '0'  ORDER BY tr.no_transaksi desc";
             var k_ordermanagement = " kd_bagian = 5       and lunas='0' and left(kd_pasien, 2) = 'RD'  and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "' and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue() + "'  and order_mng = 1 AND (dilayani = '0' or dilayani is null) ORDER BY cito desc, tgl_transaksi desc, no_transaksi ";
		    refeshpenjasrad_ordermanagement(k_ordermanagement);

        }
        
    }
}


//VALIDASI COMBO UNIT/POLI
function getDataCariUnitPenjasRad(kriteria)
{
    if (kriteria===undefined)
    {
        kriteria="kd_bagian=5 and parent<>'0'";
    }
    dsunit_viKasirRwj.load
    (
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'kd_unit',
                        Sortdir: 'ASC',
                        target: 'ViewSetupUnit',
                        param: kriteria
                    }
            }
    );
    return dsunit_viKasirRwj;
}
function mComboUnit_viKasirRad() 
{
    
    var Field = ['KD_UNIT','NAMA_UNIT'];
    
    dsunit_viKasirRwj = new WebApp.DataStore({ fields: Field });
    getDataCariUnitPenjasRad();
    var cboUNIT_viKasirRad = new Ext.form.ComboBox
    (
            {
                id: 'cboUNIT_viKasirRad',
                x: 155,
                y: 70,
                typeAhead: true,
                triggerAction: 'all',
                emptyText:'Poli',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 100,
                store: dsunit_viKasirRwj,
                valueField: 'KD_UNIT',
                displayField: 'NAMA_UNIT',
                value:'All',
                listeners:
                    {

                        'select': function(a, b, c) 
                            {                          
                                //RefreshDataFilterPenJasRad();
                                combovaluesunit=b.data.valueField;
                                validasiJenisTr();
                            }

                    }
            }
    );
    
    return cboUNIT_viKasirRad;
};

function mcomboKamarSpesial()
{
    var Field = ['no_kamar','kamar'];
    ds_KamarSpesial_viJasRad = new WebApp.DataStore({fields: Field});

    ds_KamarSpesial_viJasRad.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewSetupKelasSpesial',
                param: ""
            }
        }
    )

    var cboRujukanKamarSpesialJasRadRequestEntry = new Ext.form.ComboBox
    (
        {
            x: 155,
            y: 70,
            id: 'cboRujukanKamarSpesialJasRadRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Kamar...',
            fieldLabel: 'Kamar ',
            align: 'Right',
            store: ds_KamarSpesial_viJasRad,
            valueField: 'no_kamar',
            displayField: 'kamar',
            Width:'150',
            listeners:
                {
                    'select': function(a, b, c)
                    {
                        ComboValuesKamar=b.data.valueField;
                        validasiJenisTr();    
                    },
                    'render': function(c)
                    {
                        
                    }


        }
        }
    )

    return cboRujukanKamarSpesialJasRadRequestEntry;
}

//LOOKUP DETAIL TRANSAKSI RADORATORIUM
function PenJasRadLookUp(rowdata) {
    var lebar = 900;
    FormLookUpsdetailTRrwj = new Ext.Window({
		id: 'gridPenJasRad',
		title: 'Pelayanan Radiologi',
		closeAction: 'destroy',
		width: lebar,
		height: 600,
		border: false,
		resizable: false,
		plain: true,
		layout: 'fit',
		constrain: true,
		modal: true,
		items: getFormEntryPenJasRad(lebar,rowdata),
		listeners:{
			activate: function(){
				if (rowdata === undefined){
					Ext.getCmp('txtNamaPasienL').focus(false,100);
					// Ext.getCmp('txtNoMedrecL').setReadOnly(true);
					Ext.getCmp('txtNamaUnit').setReadOnly(true);
					Ext.getCmp('txtNamaPasienL').setReadOnly(false);
					Ext.getCmp('txtAlamatL').setReadOnly(false);
					Ext.getCmp('dtpTtlL').setReadOnly(false);
					Ext.getCmp('txtCustomerLamaHide').hide();
				}else{
					Ext.getCmp('cboDOKTER_viPenJasRad').focus(false,1000);
				}
			}
		}
	});
    FormLookUpsdetailTRrwj.show();
    if (rowdata === undefined){
        PenjasRadAddNew();
    }else{
        TRPenjasRadInit(rowdata);
    }
}
function PenjasLookUpRAD(rowdata)
{
    //console.log("ttttt");

    var lebar = 580;
    var FormLookUpsdetailTRPenjasRAD = new Ext.Window
            (
                    {
                        id: 'gridPenjasRAD',
                        title: 'Pelayanan Radiologi',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 500,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryPenjasBayarRAD(lebar),
                        listeners:
                                {
                                }
                    }
            );

    FormLookUpsdetailTRPenjasRAD.show();
    /* if (rowdata == undefined)
    {
        PenjasBayarRADAddNew();
    } else
    { */
        TRPenjasBayarRADInit(rowdata)
    //}

}
;
function mEnabledKasirRADCM(mBol)
{
   /*  Ext.get('btnLookupKasirRAD').dom.disabled = mBol;
    Ext.get('btnHpsBrsKasirRAD').dom.disabled = mBol; */
}
;
function PenjasBayarRADAddNew()
{
    AddNewKasirRADKasir = true;
    Ext.get('txtNoTransaksiKasirRADKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTgltransaksi.format('d/M/Y');
    Ext.get('txtNoMedrecDetransaksi').dom.value = '';
    Ext.get('txtNamaPasienDetransaksi').dom.value = '';

    //Ext.get('txtKdUrutMasuk').dom.value = '';
    Ext.get('cboStatus_viKasirRADKasir').dom.value = ''
    rowSelectedPenJasRad = undefined;
    dsTRDetailPenJasRadList.removeAll();
    mEnabledKasirRADCM(false);


}
;
function loaddatastorePembayaran(jenis_pay)
{
    // console.log(jenis_pay);
    dsComboBayar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'nama',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboBayar',
                                    param: 'jenis_pay=~' + jenis_pay + '~'
                                }
                    }
            )
}
function TRPenjasBayarRADInit(rowdata)
{
    AddNewKasirRADKasir = false;
    
    // console.log(rowdata);
    if (rowdata===undefined)
    {
        Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganL').getValue().format('d/M/Y');
        Ext.get('txtNoMedrecDetransaksi').dom.value = Ext.getCmp('txtNoMedrecL').getValue();
        Ext.get('txtNamaPasienDetransaksi').dom.value = Ext.getCmp('txtNamaPasienL').getValue();
        tanggaltransaksitampung = Ext.getCmp('dtpKunjunganL').getValue();
    }
    else
    {
        tanggaltransaksitampung = rowdata.TGL_TRANSAKSI;
        Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganL').getValue().format('d/M/Y'); //ShowDate(rowdata.TGL_TRANSAKSI);
        Ext.get('txtNoMedrecDetransaksi').dom.value = rowdata.KD_PASIEN;
        Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
    }
    
    var notransnya;
    if (Ext.getCmp('txtNoTransaksiPenJasRad').getValue()==='' || Ext.getCmp('txtNoTransaksiPenJasRad').getValue()===undefined){
        notransnya=rowdata.NO_TRANSAKSI;
    }
    else
    {
        notransnya=Ext.getCmp('txtNoTransaksiPenJasRad').getValue();
    }
    var modul='';
    if(radiovalues == 1){
        modul='igd';
    } else if(radiovalues == 2){
        modul='rwi';
    } else if(radiovalues == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    Ext.get('txtNoTransaksiKasirRADKasir').dom.value = notransnya;
    RefreshDataKasirRADKasirDetail(notransnya,kd_kasir_rad);
    notransaksiasal_rad='';
    // take the displayField value 
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/cekPembayaran",
        params: {
            notrans: notransnya,
            Modul:modul,
            KdKasir:kd_kasir_rad,
            KdPasien:Ext.getCmp('txtNoMedrecL').getValue()
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            // console.log(cst.ListDataObj);
            loaddatastorePembayaran(cst.ListDataObj[0].jenis_pay);
            notransaksi=cst.ListDataObj[0].no_transaksi;
            vkd_unit = cst.ListDataObj[0].kd_unit;
            kodeunit=cst.ListDataObj[0].kd_unit;
            kdkasir = cst.ListDataObj[0].kd_kasir;
            notransaksiasal_rad = cst.ListDataObj[0].no_transaksi_asal;
            kdkasirasal_rad = cst.ListDataObj[0].kd_kasir_asal;
            Ext.get('cboPembayaran').dom.value =cst.ListDataObj[0].ket_payment;
            Ext.get('cboJenisByr').dom.value =cst.ListDataObj[0].cara_bayar; 
            vflag = cst.ListDataObj[0].flag;
            tapungkd_pay = cst.ListDataObj[0].kd_pay;
            vkode_customerRAD =  cst.ListDataObj[0].kd_customer;
        }

    });
    tampungtypedata = 0;
    
    jenispay = 1;
    showCols(Ext.getCmp('gridDTItemTest'));
    hideCols(Ext.getCmp('gridDTItemTest'));
   
    //(rowdata.NO_TRANSAKSI;


    Ext.Ajax.request({
        url: baseURL + "index.php/main/getcurrentshift",
        params: {
            command: '0',
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {

            tampungshiftsekarang = o.responseText
        }

    });



}
;
function getItemPanelNoTransksiRADKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Transaksi ',
                                                name: 'txtNoTransaksiKasirRADKasir',
                                                id: 'txtNoTransaksiKasirRADKasir',
                                                emptyText: nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%'
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 55,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTanggalDetransaksi',
                                                name: 'dtpTanggalDetransaksi',
                                                format: 'd/M/Y',
                                                readOnly: true,
                                                value: now,
                                                anchor: '100%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function mComboJenisByrView()
{
    var Field = ['JENIS_PAY', 'DESKRIPSI', 'TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({fields: Field});
    dsJenisbyrView.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'jenis_pay',
                                    Sortdir: 'ASC',
                                    target: 'ComboJenis',
                                }
                    }
            );

    var cboJenisByr = new Ext.form.ComboBox
            (
                    {
                        id: 'cboJenisByr',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Pembayaran      ',
                        align: 'Right',
                        anchor: '100%',
                        store: dsJenisbyrView,
                        valueField: 'JENIS_PAY',
                        displayField: 'DESKRIPSI',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        loaddatastorePembayaran(b.data.JENIS_PAY);
                                        tampungtypedata = b.data.TYPE_DATA;
                                        jenispay = b.data.JENIS_PAY;
                                        showCols(Ext.getCmp('gridDTItemTest'));
                                        hideCols(Ext.getCmp('gridDTItemTest'));
                                        getTotalDetailProdukRAD();
                                        Ext.get('cboPembayaran').dom.value = 'Pilih Pembayaran...';
                                    },
                                }
                    }
            );

    return cboJenisByr;
}
;
function getTotalDetailProdukRAD()
{
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < dsTRDetailKasirRADKasirList.getCount(); i++)
    {

        var recordterakhir;

        //alert(TotalProduk);
        if (tampungtypedata == 0)
        {
            tampunggrid = parseInt(dsTRDetailKasirRADKasirList.data.items[i].data.BAYARTR);

            //recordterakhir= tampunggrid
            //TotalProduk.toString().replace(/./gi, "");
            TotalProduk += tampunggrid

            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirRAD').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 3)
        {
            tampunggrid = parseInt(dsTRDetailKasirRADKasirList.data.items[i].data.PIUTANG);
            //TotalProduk.toString().replace(/./gi, "");
            //recordterakhir=tampunggrid
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirRAD').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 1)
        {
            tampunggrid = parseInt(dsTRDetailKasirRADKasirList.data.items[i].data.DISCOUNT);

            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirRAD').dom.value = formatCurrency(recordterakhir);
        }
    }
    
    if (Ext.getCmp('txtJumlah2EditData_viKasirRAD').getValue()==='0' || Ext.getCmp('txtJumlah2EditData_viKasirRAD').getValue()===0 )
    {
        Ext.getCmp('btnSimpanKasirRAD').disable();
        Ext.getCmp('btnTransferKasirRAD').disable();
    }
    else
    {
        /* Ext.getCmp('btnSimpanKasirRAD').enable();
        Ext.getCmp('btnTransferKasirRAD').enable(); */
    }
    bayar = Ext.get('txtJumlah2EditData_viKasirRAD').getValue();
    return bayar;
}
;


    
function hideCols(grid)
{
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);

    }
}
;
function showCols(grid) {
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);

    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
    }

}
;
function mComboPembayaran()
{
    var Field = ['KD_PAY', 'JENIS_PAY', 'PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});

    var cboPembayaran = new Ext.form.ComboBox
            (
                    {
                        id: 'cboPembayaran',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Pembayaran...',
                        labelWidth: 80,
                        align: 'Right',
                        store: dsComboBayar,
                        valueField: 'KD_PAY',
                        displayField: 'PAYMENT',
                        anchor: '100%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tapungkd_pay = b.data.KD_PAY;
                                    },
                                }
                    }
            );

    return cboPembayaran;
}
;
function getItemPanelmedreckasirRAD(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Medrec ',
                                                name: 'txtNoMedrecDetransaksi',
                                                id: 'txtNoMedrecDetransaksi',
                                                readOnly: true,
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: '',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtNamaPasienDetransaksi',
                                                id: 'txtNamaPasienDetransaksi',
                                                anchor: '100%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getItemPanelInputKasirRAD(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: true,
                height: 120,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getItemPanelNoTransksiRADKasir(lebar),
                                            getItemPanelmedreckasirRAD(lebar), getItemPanelUnitKasirRAD(lebar),
                                            getItemPanelTanggalBayarKasirRAD(lebar)
                                        ]
                            }
                        ]
            };
    return items;
}
;

function getItemPanelTanggalBayarKasirRAD(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                        {
                            columnWidth: .40,
                            layout: 'form',
                            border: false,
                            labelWidth: 100,
                            items:
                            [
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: 'Tanggal Bayar',
                                            id: 'dtpTanggalBayarDetransaksiRAD',
                                            name: 'dtpTanggalBayarDetransaksiRAD',
                                            format: 'd/M/Y',
                                            //readOnly: true,
                                            value: now,
                                            anchor: '100%',
                                            enableKeyEvents:true,
                                            listeners:
                                                    {
                                                        
                                                    },
                                        }
                            ]
                        }
                            
                        ]
            }
    return items;
}

function getItemPanelUnitKasirRAD(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboJenisByrView()
                                        ]
                            }, {
                                columnWidth: .60,
                                layout: 'form',
                                labelWidth: 0.9,
                                border: false,
                                items:
                                        [
                                            mComboPembayaran()
                                        ]
                            }
                        ]
            }
    return items;
}
;

function RefreshDataKasirRADKasirDetail(no_transaksi,kd_kasir)
{
    var strKriteriaKasirRAD = '';

    if (kd_kasir !== undefined) {
        strKriteriaKasirRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
    }else{
        strKriteriaKasirRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
    }

    dsTRDetailKasirRADKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailbayar',
                                    param: strKriteriaKasirRAD
                                }
                    }
            );
    return dsTRDetailKasirRADKasirList;
}
;
function GetDTLTRKasirRADGrid()
{
    var fldDetailRAD = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'BAYARTR', 'DISCOUNT', 'PIUTANG'];

    dsTRDetailKasirRADKasirList = new WebApp.DataStore({fields: fldDetailRAD})

    gridDTLTRKasirRAD = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Detail Bayar',
                        stripeRows: true,
                        id: 'gridDTLTRKasirRAD',
                        store: dsTRDetailKasirRADKasirList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        height: 230,
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        cm: TRKasirRawatJalanColumModelRAD()
                    }
            );

    return gridDTLTRKasirRAD;
}
;

function TRKasirRawatJalanColumModelRAD()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiKasirRAD',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 250,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiKasirRAD',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            id: 'colURUTKasirRAD',
                            header: 'Urut',
                            dataIndex: 'URUT',
                            sortable: false,
                            hidden: true,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            hidden: true,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colQtyKasirRAD',
                            header: 'Qty',
                            width: 91,
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                        },
                        {
                            id: 'colHARGAKasirRAD',
                            header: 'Harga',
                            align: 'right',
                            hidden: false,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colPiutangKasirRAD',
                            header: 'Puitang',
                            width: 80,
                            dataIndex: 'PIUTANG',
                            align: 'right',
                            //hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolPuitangRAD',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                //getTotalDetailProdukRAD();
                                return formatCurrency(record.data.PIUTANG);
                            }
                        },
                        {
                            id: 'colTunaiKasirRAD',
                            header: 'Tunai',
                            width: 80,
                            dataIndex: 'BAYARTR',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolTunaiRAD',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProdukRAD();

                                return formatCurrency(record.data.BAYARTR);
                            }

                        },
                        {
                            id: 'colDiscountKasirRAD',
                            header: 'Discount',
                            width: 80,
                            dataIndex: 'DISCOUNT',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolDiscountRAD',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.DISCOUNT);
                            }
                        }

                    ]
                    )
}
;

function getParamDetailTransaksiKasirRAD()
{
    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    }
    ;

    var params =
            {
                kdUnit: vkd_unit,
                TrKodeTranskasi: Ext.get('txtNoTransaksiKasirRADKasir').getValue(),
                Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
                Shift: tampungshiftsekarang,
                kdKasir: kdkasir,
                bayar: tapungkd_pay,
                Flag: vflag,
                Typedata: tampungtypedata,
                Totalbayar: getTotalDetailProdukRAD(),
                List: getArrDetailTrKasirRAD(),
                JmlField: mRecordKasirRAD.prototype.fields.length - 4,
                JmlList: GetListCountDetailTransaksiRAD(),
                Hapus: 1,
                Ubah: 0,
                TglBayar : Ext.get('dtpTanggalBayarDetransaksiRAD').dom.value,
            };
    return params
}
;

function getArrDetailTrKasirRAD()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirRADKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirRADKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirRADKasirList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirRADKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.QTY
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.BAYARTR
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.PIUTANG
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.DISCOUNT



            if (i === (dsTRDetailKasirRADKasirList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }

    return x;
}
;

function ValidasiEntryCMKasirRAD(modul, mBolHapus)
{
    var x = 1;

    if ((Ext.get('txtNoTransaksiKasirRADKasir').getValue() == '')

            || (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
            || (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
            || (Ext.get('dtpTanggalDetransaksi').getValue() == '')
            || dsTRDetailKasirRADKasirList.getCount() === 0
            || (Ext.get('cboPembayaran').getValue() == '')
            || (Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...'))
    {
        if (Ext.get('txtNoTransaksiKasirRADKasir').getValue() == '' && mBolHapus === true)
        {
            x = 0;
        } else if (Ext.get('cboPembayaran').getValue() == '' || Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...')
        {
            ShowPesanWarningPenJasRad(('Data pembayaran tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasRad(('Data no. medrec tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasRad('Nama pasien belum terisi', modul);
            x = 0;
        } else if (Ext.get('dtpTanggalDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasRad(('Data tanggal kunjungan tidak boleh kosong'), modul);
            x = 0;
        }
        //cboPembayaran

        else if (dsTRDetailKasirRADKasirList.getCount() === 0)
        {
            ShowPesanWarningPenJasRad('Data dalam tabel kosong', modul);
            x = 0;
        }
        ;
    }
    ;
    return x;
}
;
function Datasave_KasirRADKasir(mBol)
{
    if (ValidasiEntryCMKasirRAD(nmHeaderSimpanData, false) == 1)
    {
        console.log(tglTransaksiAwalPembandingTransferRAD);
        var tgl = Date.parse(tglTransaksiAwalPembandingTransferRAD);
        var tgl_bayar = Date.parse(Ext.getCmp('dtpTanggalBayarDetransaksiRAD').getValue().format('Y-m-d')+" 00:00:00");
        if (tgl > tgl_bayar ){
            ShowPesanWarningPenJasRad('Tanggal bayar Tidak boleh kurang dari tanggal transaksi pasien', 'Gagal');
            Ext.getCmp('btnSimpanKasirRAD').enable();
            Ext.getCmp('btnTransferKasirRAD').enable();
        }else{
        Ext.Ajax.request
                (
                        {
                            //url: "./Datapool.mvc/CreateDataObj",
                            url: baseURL + "index.php/main/functionRAD/savepembayaran",
                            params: getParamDetailTransaksiKasirRAD(),
                            failure: function (o)
                            {
                                ShowPesanWarningPenJasRad('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                            },
                            success: function (o)
                            {
                                 RefreshDatahistoribayar('0');
                                RefreshDataPenJasRadDetail('0');
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfoPenJasRad(nmPesanSimpanSukses, nmHeaderSimpanData);
                                    /* Ext.getCmp('btnSimpanKasirRAD').disable();
                                    Ext.getCmp('btnTransferKasirRAD').disable(); */
                                    Ext.getCmp('btnPrint_viDaftar').enable();
                                    RefreshDatahistoribayar(Ext.get('txtNoTransaksiKasirRADKasir').dom.value);
                                    RefreshDataKasirRADKasirDetail(Ext.get('txtNoTransaksiKasirRADKasir').dom.value,kd_kasir_rad);
                                    
                                    ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiKasirRADKasir').dom.value,kd_kasir_rad);
                                    //RefreshDataKasirRADKasir();
                                   /*  if (mBol === false)
                                    {
                                        ViewGridBawahLookupPenjasRad();
                                    }
                                    ; */
                                } else
                                {
                                    ShowPesanWarningPenJasRad('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                }
                                ;
                            }
                        }
                )
        }

    } else
    {
        if (mBol === true)
        {
            return false;
        }
        ;
    }
    ;

}

function TransferLookUp_rad(rowdata)
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_Rad = new Ext.Window
            (
                    {
                        id: 'gridTransfer_Rad',
                        title: 'Transfer',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 410,
                        border: false,
                        resizable: false,
                        plain: false,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryTRTransfer_rad(lebar),
                        listeners:
                                {
                                     activate: function()
                                    {
                                        Ext.getCmp('cboalasan_transfer').setValue('Pembayaran Disatukan');                                        
                                    }
                                }
                    }
            );

    FormLookUpsdetailTRTransfer_Rad.show();
    //  Transferbaru();

};
function getParamTransferRwi_dari_rad()
{

/*
       KDkasirIGD:'01',
        TrKodeTranskasi: notransaksi,
        KdUnit: kodeunit,
        Kdpay: kdpaytransfer,
        Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
        Tglasal:  ShowDate(tgltrans),
        Shift: tampungshiftsekarang,
        TRKdTransTujuan:Ext.get(txtTranfernoTransaksiRWJ).dom.value,
        KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRWJ).dom.value,
        TglTranasksitujuan : Ext.get(dtpTanggaltransaksiRujuanRWJ).dom.value,
        KDunittujuan : Trkdunit2,
        KDalasan :Ext.get(cboalasan_transfer).dom.value,
        KasirRWI:'05',
        Kdcustomer:kdcustomeraa,
        kodeunitkamar:tmp_kodeunitkamar,
        kdspesial:tmp_kdspesial,
        nokamar:tmp_nokamar,


*/
    var params =
            {
                KDkasirIGD: kdkasir,
                Kdcustomer: vkode_customerRAD,
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                Kdpay: kdpaytransfer,
                Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
                Tglasal: ShowDate(TglTransaksi),
                Shift: tampungshiftsekarang,
                TRKdTransTujuan: Ext.get(txtTranfernoTransaksiRAD).dom.value,
                KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRAD).dom.value,
                TglTranasksitujuan: Ext.get(dtpTanggaltransaksiRujuanRAD).dom.value,
                KDunittujuan: Trkdunit2,
                KDalasan: Ext.get(cboalasan_transfer).dom.value,
                KasirRWI: kdkasirasal_rad,
                kodeunitkamar:tmp_kodeunitkamar_RAD,
                kdspesial:tmp_kdspesial_RAD,
                nokamar:tmp_nokamar_RAD,
                TglTransfer:Ext.get(dtpTanggaltransferRAD).dom.value


            };
    return params
}
;

function TransferData_rad(mBol){
    var tmp_data_transfer = data_transfer.split("<>");
    var tf_modul          = tmp_data_transfer[1]; 
    var tf_nama           = tmp_data_transfer[2]; 
    var tf_no_transaksi   = tmp_data_transfer[3]; 
    var tf_tgl_transaksi  = tmp_data_transfer[4]; 
    var tf_medrec         = tmp_data_transfer[5]; 
    var tf_unit_asal      = tmp_data_transfer[6]; 
    var tmp_nokamar_RAD      = tmp_data_transfer[11]; 
    var tmp_kdspesial_RAD      = tmp_data_transfer[10]; 
    var tmp_kodeunitkamar_RAD      = tmp_data_transfer[8]; 
    // if (tglTransaksiAwalPembandingTransferRAD > Ext.getCmp('dtpTanggaltransferRAD').getValue().format('Y-m-d')+" 00:00:00"){
    //     ShowPesanWarningPenJasRad('Tanggal transfer Tidak boleh kurang dari tanggal transaksi pasien', 'Gagal');
    // }else{
    loadMask.show();
    var UrlTransfer="";
    /* if (radiovalues === 2 || radiovalues ==='2'){
        UrlTransfer="index.php/main/functionRAD/saveTransferRWI";
    }else{ */
        // UrlTransfer="index.php/main/functionRAD/saveTransfer";
        UrlTransfer="index.php/rad/Radiologi/save_transfer";
    //}
    var parameter = {
        KDkasirIGD: kdkasir,
        Kdcustomer: rowSelectedPenJasRad.data.KD_CUSTOMER,
        TrKodeTranskasi: Ext.getCmp('txtNoTransaksiPenJasRad').getValue(),
        KdUnit: kodeunit,
        Kdpay: kdpaytransfer,
        Jumlahtotal: total_bayar,
        Tglasal: ShowDate(TglTransaksi),
        Shift: tampungshiftsekarang,
        TRKdTransTujuan: tf_no_transaksi,
        KdpasienIGDtujuan: tf_medrec,
        TglTranasksitujuan: ShowDate(tf_tgl_transaksi),
        KDunittujuan: tf_unit_asal,
        KDalasan: 'Pembayaran Di satukan',
        KasirRWI: data_unit_asal.KD_KASIR_ASAL,
        kodeunitkamar:tmp_kodeunitkamar_RAD,
        kdspesial:tmp_kdspesial_RAD,
        nokamar:tmp_nokamar_RAD,
        TglTransfer:now.format('Y-m-d'),
        GD_TglMasuk     : vTanggal,
        GD_UrutMasuk    : vUrutMasuk,
        GD_KdUnit       : vKdUnit,
        GD_KdDokter     : Ext.getCmp('cboDOKTER_viPenJasRad').getValue(),
        kd_dokter_penunjang     : tmpkddoktertujuan,
        no_photo_rad    : Ext.getCmp('txtnofotorad').getValue(),
    };
    
                            /*Ext.Ajax.request({
                                url: baseURL + "index.php/rad/Radiologi/save_transfer",
                                params: parameter,
                                failure: function (o){
                                    ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                                },
                                success: function (o){  
                                    loadMask.hide();
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true){   
                                        kdkasir_tmp = kdkasir;
                                        rowSelectedPenJasRad.data.NO_TRANSAKSI  = cst.no_transaksi;
                                        rowSelectedPenJasRad.data.KD_KASIR      = cst.kd_kasir;
                                        Ext.getCmp('btnSimpanPenjasRAD').disable();
                                        ShowPesanInfoPenJasRad('Transfer Berhasil', 'transfer ');
                                        Ext.getCmp('btnTambahBarisPenJasRad').disable();
                                        Ext.getCmp('btnLookupPenJasRad').disable();
                                        Ext.getCmp('btnHpsBrsItemRad').disable();
                                        Ext.getCmp('btnpemakaianfilmRadiologiPenjasRAD').disable();
                                        Ext.getCmp('btnDocumentPenjasRAD').disable();
                                        Ext.getCmp('btnLookUpEditDokterPenindak_rad').disable();
                                        RefreshDatahistoribayar(Ext.getCmp('txtNoTransaksiPenJasRad').getValue());
                                        validasiJenisTr();
                                    } else{
                                        ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                                    };
                                }
                            });*/
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + UrlTransfer,
                        // params: getParamTransferRwi_dari_rad(),
                        params: parameter,
                        failure: function (o)
                        {

                            ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            // ViewGridBawahLookupPenjasRad();
                        },
                        success: function (o)
                        {   
                        
                            loadMask.hide();
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {   
                                //TransferData_rad_SQL();
                                Ext.getCmp('btnSimpanKasirRAD').disable();
                                Ext.getCmp('btnTransferKasirRAD').disable();
                                ShowPesanInfoPenJasRad('Transfer Berhasil', 'transfer ');
                                Ext.getCmp('btnPrint_viDaftar').enable();
                                RefreshDatahistoribayar(Ext.getCmp('txtNoTransaksiPenJasRad').getValue());
                                FormLookUpsdetailTRTransfer_Rad.close();
                                Ext.getCmp('btnTambahBarisPenJasRad').disable();
								Ext.getCmp('btnLookupPenJasRad').disable();
								Ext.getCmp('btnHpsBrsItemRad').disable();
								Ext.getCmp('btnpemakaianfilmRadiologiPenjasRAD').disable();
								Ext.getCmp('btnDocumentPenjasRAD').disable();
								Ext.getCmp('btnLookUpEditDokterPenindak_rad').disable();
                            } else
                            {
                                ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            }
                            ;
                        }
                    }
            )
    // }

};

function getItemPanelButtonTransfer_rad(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                height: 30,
                anchor: '100%',
                style: {'margin-top': '-1px'},
                items:
                        [
                            {
                                layout: 'hBox',
                                width: 400,
                                border: false,
                                bodyStyle: 'padding:5px 0px 5px 5px',
                                defaults: {margins: '3 3 3 3'},
                                anchor: '90%',
                                layoutConfig:
                                        {
                                            align: 'middle',
                                            pack: 'end'
                                        },
                                items:
                                        [
                                            {
                                                xtype: 'button',
                                                text: 'Simpan',
                                                width: 70,
                                                style: {'margin-left': '0px', 'margin-top': '0px'},
                                                hideLabel: true,
                                                id: 'btnOkTransfer_rad',
                                                handler: function ()
                                                {
                                                
                                                    TransferData_rad(false);
                                                     Ext.getCmp('btnSimpanKasirRAD').disable();
                                                     Ext.getCmp('btnTransferKasirRAD').disable();
                                                    
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                text: 'Tutup',
                                                width: 70,
                                                hideLabel: true,
                                                id: 'btnCancelTransfer_rad',
                                                handler: function ()
                                                {
                                                    FormLookUpsdetailTRTransfer_Rad.close();
                                                }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getFormEntryTRTransfer_rad(lebar)
{
    var pnlTRTransfer = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRTransfer',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 425,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputTransfer_RAD(lebar), getItemPanelButtonTransfer_rad(lebar)],
                        tbar:
                                [
                                ]
                    }
            );

    var FormDepanTransfer = new Ext.Panel
            (
                    {
                        id: 'FormDepanTransfer',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        items:
                                [
                                    pnlTRTransfer

                                ]

                    }
            );

    return FormDepanTransfer
}
;

function mComboTransferTujuan()
{
    var cboTransferTujuan = new Ext.form.ComboBox
            (
                    {
                        id: 'cboTransferTujuan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        hidden: true,
                        fieldLabel: 'Transfer',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Gawat Darurat'], [2, 'Rawat Inap'], [3, 'Rawat Jalan']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: tranfertujuan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tranfertujuan = b.data.displayText;
                                        //RefreshDataSetDokter();
                                        ViewGridBawahLookupPenjasRad();

                                    }
                                }
                    }
            );
    return cboTransferTujuan;
}
;

function getTransfertujuan_rad(lebar)
{
    var items =
            {
                Width: lebar - 2,
                height: 170,
                layout: 'form',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                labelWidth: 130,
                items:
                        [
                            {
                                xtype: 'tbspacer',
                                height: 2
                            },
                            mComboTransferTujuan(),
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Transaksi',
                                //maxLength: 200,
                                name: 'txtTranfernoTransaksiRAD',
                                id: 'txtTranfernoTransaksiRAD',
                                labelWidth: 130,
                                readonly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                id: 'dtpTanggaltransaksiRujuanRAD',
                                name: 'dtpTanggaltransaksiRujuanRAD',
                                format: 'd/M/Y',
                                readOnly: true,
                                //  value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Medrec',
                                //maxLength: 200,
                                name: 'txtTranfernomedrecRAD',
                                id: 'txtTranfernomedrecRAD',
                                labelWidth: 130,
                                readOnly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama Pasien',
                                //maxLength: 200,
                                name: 'txtTranfernamapasienRAD',
                                id: 'txtTranfernamapasienRAD',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Unit Perawatan',
                                //maxLength: 200,
                                name: 'txtTranferunitRAD',
                                id: 'txtTranferunitRAD',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        name: 'txtTranferkelaskamar_RAD',
                                        id: 'txtTranferkelaskamar_RAD',
                                        readOnly : true,
                                        labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal transfer ',
                                id: 'dtpTanggaltransferRAD',
                                name: 'dtpTanggaltransferRAD',
                                format: 'd/M/Y',
                                //readOnly: true,
                                value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                        ]
            }
    return items;
}
;

function getItemPanelNoTransksiTransferRAD(lebar)
{
    var items =
            {
                Width: lebar,
                height: 110,
                layout: 'column',
                border: true,
                items:
                        [
                            {
                                columnWidth: .990,
                                layout: 'form',
                                Width: lebar - 10,
                                labelWidth: 130,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah Biaya',
                                                maxLength: 200,
                                                name: 'txtjumlahbiayasal',
                                                id: 'txtjumlahbiayasal',
                                                width: 100,
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Paid',
                                                maxLength: 200,
                                                name: 'txtpaid',
                                                id: 'txtpaid',
                                                readOnly: true,
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                value: 0,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah dipindahkan',
                                                maxLength: 200,
                                                name: 'txtjumlahtranfer',
                                                id: 'txtjumlahtranfer',
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Saldo tagihan',
                                                maxLength: 200,
                                                name: 'txtsaldotagihan',
                                                id: 'txtsaldotagihan',
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                value: 0,
                                                width: 100,
                                                anchor: '95%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function mComboalasan_transfer()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_transfer = new WebApp.DataStore({fields: Field});
    dsalasan_transfer.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_alasan',
                                    Sortdir: 'ASC',
                                    target: 'ComboAlasanTransfer',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboalasan_transfer = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_transfer',
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Alasan Transfer ',
                        align: 'Right',
                        width: 100,
                        editable: false,
                        anchor: '100%',
                        store: dsalasan_transfer,
                        valueField: 'ALASAN',
                        displayField: 'ALASAN',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                    }

                                }
                    }
            );

    return cboalasan_transfer;
};

function getItemPanelInputTransfer_RAD(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: false,
                height: 330,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                height: 330,
                                border: false,
                                items:
                                        [
                                            getTransfertujuan_rad(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 5
                                            },
                                            getItemPanelNoTransksiTransferRAD(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            mComboalasan_transfer()

                                        ]
                            },
                        ]
            };
    return items;
}
;
function showhide_unit(kode)
{
    if(kode==='05')
    {
    Ext.getCmp('txtTranferunitRAD').hide();
    Ext.getCmp('txtTranferkelaskamar_RAD').show();

    }else{
    Ext.getCmp('txtTranferunitRAD').show();
    Ext.getCmp('txtTranferkelaskamar_RAD').hide();
    }
}
function getFormEntryPenjasBayarRAD(lebar)
{
    var pnlTRKasirRAD = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirRAD',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 130,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputKasirRAD(lebar)],
                        tbar:
                                [
                                ]
                    }
            );
    var x;
    var paneltotal = new Ext.Panel
            (
                    {
                        id: 'paneltotal',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        height: 67,
                        anchor: '100% 8.0000%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        html: " Total :"
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirRAD',
                                                        name: 'txtJumlah2EditData_viKasirRAD',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        hidden: true,
                                                        html: " Bayar :"
                                                    },
                                                    {
                                                        xtype: 'numberfield',
                                                        id: 'txtJumlahEditData_viKasirRAD',
                                                        name: 'txtJumlahEditData_viKasirRAD',
                                                        hidden: true,
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        //readOnly: true,
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                ]
                    }
            )
    var GDtabDetailKasirRAD = new Ext.Panel
            (
                    {
                        id: 'GDtabDetailKasirRAD',
                        region: 'center',
                        activeTab: 0,
                        height: 350,
                        anchor: '100% 100%',
                        border: false,
                        plain: true,
                        defaults: {autoScroll: true},
                        items: [GetDTLTRKasirRADGrid(), paneltotal],
                        tbar:
                                [
                                    {
                                        text: 'Bayar',
                                        id: 'btnSimpanKasirRAD',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        handler: function ()
                                        {
                                            Datasave_KasirRADKasir(false);
                                           // FormDepan2KasirRAD.close();
                                            Ext.getCmp('btnSimpanKasirRAD').disable();
                                            Ext.getCmp('btnTransferKasirRAD').disable();
                                            //Ext.getCmp('btnHpsBrsKasirRAD').disable();
                                        }
                                    },
                                    
                                    {
                                        id: 'btnTransferKasirRAD',
                                        text: 'Transfer',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                        /* disabled: true, */
                                        handler: function (sm, row, rec)
                                        {
                                            //Disini
                                            TransferLookUp_rad();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/GetPasienTranferLab_rad",
                                                params: {
                                                    notr: notransaksiasal_rad,
                                                    notransaksi: Ext.getCmp('txtNoTransaksiKasirRADKasir').getValue(),
                                                    kdkasir: kdkasirasal_rad
                                                },
                                                success: function (o)
                                                {   
                                                showhide_unit(kdkasirasal_rad);
                                                    if (o.responseText == "") {
                                                        ShowPesanWarningPenJasRad('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                    } else {
                                                        var tmphasil = o.responseText;
                                                        var tmp = tmphasil.split("<>");
                                                        if (tmp[5] == undefined && tmp[3] == undefined) {
                                                            ShowPesanWarningPenJasRad('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                            FormLookUpsdetailTRTransfer_Rad.close();
                                                        } else {
                                                            console.log(tmp[12]);
                                                            Ext.get(txtTranfernoTransaksiRAD).dom.value = tmp[3];
                                                            Ext.get(dtpTanggaltransaksiRujuanRAD).dom.value = ShowDate(tmp[4]);
                                                            Ext.get(txtTranfernomedrecRAD).dom.value = tmp[5];
                                                            Ext.get(txtTranfernamapasienRAD).dom.value = tmp[2];
                                                            Ext.get(txtTranferunitRAD).dom.value = tmp[9];
                                                            Trkdunit2 = tmp[6];
                                                            Ext.get(txtTranferkelaskamar_RAD).dom.value=tmp[9];
                                                            Ext.get(txtjumlahbiayasal).dom.value=tmp[12];
                                                            Ext.get(txtjumlahtranfer).dom.value=tmp[12];
                                                            
                                                            tmp_kodeunitkamar_RAD=tmp[8];
                                                            tmp_kdspesial_RAD=tmp[10];
                                                            tmp_nokamar_RAD=tmp[11];
                                                            var kasir = tmp[7];
                                                            Ext.Ajax.request({
                                                                url: baseURL + "index.php/main/GettotalTranfer",
                                                                params: {
                                                                    notr: notransaksi,
                                                                    kd_kasir:kdkasir
                                                                },
                                                                success: function (o)
                                                                {
                                                                    console.log(o);
                                                                    Ext.get(txtjumlahbiayasal).dom.value = formatCurrency(o.responseText);
                                                                    Ext.get(txtjumlahtranfer).dom.value = formatCurrency(o.responseText);
                                                                    /* Ext.get(txtjumlahbiayasal).dom.value=tmp[12];
                                                                    Ext.get(txtjumlahtranfer).dom.value=tmp[12]; */
                                                                }
                                                            });
                                                        }

                                                    }
                                                }
                                            });

                                        }, //disabled :true
                                    },
                                    {
                                        xtype: 'splitbutton',
                                        text: 'Cetak',
                                        iconCls: 'print',
                                        id: 'btnPrint_viDaftar',
                                        //disabled: true,
                                        handler: function ()
                                        {
                                        },
                                        menu: new Ext.menu.Menu({
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
                                                            id: 'btnPrintBillRadRad',
                                                            handler: function ()
                                                            {
                                                                Ext.Msg.confirm('Warning', 'Apakah anda ingin mencetak billing ?', function(button){
                                                                if (button == 'yes'){
                                                                    //3 12 W 1 N
                                                                    //25 November 2020
                                                                    //===================================
                                                                    
                                                                    // printbillRadRad();
                                                                    var url_laporan = baseURL + "index.php/laporan/lap_billing_penunjang/";
                                                                    GetDTLPreviewBilling(url_laporan+"/preview_pdf", Ext.getCmp('txtNoTransaksiKasirRADKasir').getValue(), kdkasir);
                                                                    //GetDTLPreviewBilling(url_laporan+"/preview_pdf", rowSelectedPenJasRad.data.NO_TRANSAKSI, rowSelectedPenJasRad.data.KD_KASIR);
                                                                    
                                                                    //===================================                                                                    

                                                                }
                                                                })
                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Kwitansi',
                                                            id: 'btnPrintKwitansiRadRad',
                                                            handler: function ()
                                                            {
                                                                Ext.Msg.confirm('Warning', 'Apakah anda ingin mencetak kwitansi ?', function(button){
                                                                if (button == 'yes'){
                                                                    printkwitansiRadRad();
                                                                }
                                                                })
                                                            }
                                                        }
                                                    ]
                                        })
                                    }
                                ]
                    }
            );



    var pnlTRKasirRAD2 = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirRAD2',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 380,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [GDtabDetailKasirRAD,
                        ]
                    }
            );




    FormDepan2KasirRAD = new Ext.Panel
            (
                    {
                        id: 'FormDepan2KasirRAD',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                height: 380,
                        shadhow: true,
                        items: [pnlTRKasirRAD, pnlTRKasirRAD2,
                        ]
                    }
            );

    return FormDepan2KasirRAD
}
function paramUpdateTransaksi()
{
    var params =
            {
                TrKodeTranskasi: Ext.get('txtNoTransaksiPenJasRad').dom.value,
                KDkasir: kdkasir
            };
    return params
}
;
function UpdateTutuptransaksi(mBol)
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/ubah_co_status_transksi",
                        params: paramUpdateTransaksi(),
                        failure: function (o)
                        {
                            ShowPesanInfoPenJasRad('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                            validasiJenisTr();
                        },
                        success: function (o)
                        {
                            //RefreshDatahistoribayar(Ext.get('cellSelectedtutup);

                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoPenJasRad(nmPesanSimpanSukses, nmHeaderSimpanData);

                                //RefreshDataKasirRADKasir();
                                if (mBol === false)
                                {
                                    validasiJenisTr();
                                }
                                ;
                                cellSelecteddeskripsi = '';
                            } else
                            {
                                ShowPesanInfoPenJasRad('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                                cellSelecteddeskripsi = '';
                            }
                            ;
                        }
                    }
            )
}
;
function getFormEntryPenJasRad(lebar,data) {
    var pnlTRPenjasRAD = new Ext.FormPanel({
		id: 'PanelTRPenJasRad',
		layout: 'fit',
		height:255,
		border: false,
		items: [getItemPanelPenJasRad(lebar)],
		tbar:[
			{
				text: 'Simpan',
				id: 'btnSimpanRAD',
				tooltip: nmSimpan,
				iconCls: 'save',
				handler: function(){
					loadMask.show();
					Datasave_PenJasRad(false);
					loadMask.hide();
				}
			},'-',{
                hidden : true,
                id: 'btnSimpanPenjasRAD',
                text: 'Simpan',
                tooltip: nmEditData,
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec){
					// Datasave_PenJasRad(false); 
						console.log(data_transfer);
						var tmp_data_transfer = data_transfer.split("<>");
						var tf_modul          = tmp_data_transfer[1]; 
						var tf_nama           = tmp_data_transfer[2]; 
						var tf_no_transaksi   = tmp_data_transfer[3]; 
						var tf_tgl_transaksi  = tmp_data_transfer[4]; 
						var tf_medrec         = tmp_data_transfer[5]; 
						var tf_unit_asal      = tmp_data_transfer[6]; 
						var tmp_nokamar_RAD      = tmp_data_transfer[11]; 
						var tmp_kdspesial_RAD      = tmp_data_transfer[10]; 
						var tmp_kodeunitkamar_RAD      = tmp_data_transfer[8]; 
						kodeunit = TrPenJasRad.setting.KD_UNIT_RAD;
						if (Ext.getCmp('rb_pilihan1').getValue() == true) {
							// IGD
							kdkasir = TrPenJasRad.setting.KD_KASIR_RAD_IGD;
						}else if (Ext.getCmp('rb_pilihan4').getValue() == true) {
							// RWJ
							kdkasir = TrPenJasRad.setting.KD_KASIR_RAD_RWJ;
						}else if (Ext.getCmp('rb_pilihan2').getValue() == true) {
							// RWI
							kdkasir = TrPenJasRad.setting.KD_KASIR_RAD_RWI;
						}
						// console.log(data_transfer[5]);
						if (tf_medrec == undefined && tf_no_transaksi == undefined) {
							ShowPesanWarningPenJasLab('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
						} else {
							var parameter = {
									KDkasirIGD: kdkasir,
									Kdcustomer: rowSelectedPenJasRad.data.KD_CUSTOMER,
									TrKodeTranskasi: Ext.getCmp('txtNoTransaksiPenJasRad').getValue(),
									KdUnit: kodeunit,
									Kdpay: kdpaytransfer,
									Jumlahtotal: total_bayar,
									Tglasal: ShowDate(TglTransaksi),
									Shift: tampungshiftsekarang,
									TRKdTransTujuan: tf_no_transaksi,
									KdpasienIGDtujuan: tf_medrec,
									TglTranasksitujuan: ShowDate(tf_tgl_transaksi),
									KDunittujuan: tf_unit_asal,
									KDalasan: 'Pembayaran Di satukan',
									KasirRWI: data_unit_asal.KD_KASIR_ASAL,
									kodeunitkamar:tmp_kodeunitkamar_RAD,
									kdspesial:tmp_kdspesial_RAD,
									nokamar:tmp_nokamar_RAD,
									TglTransfer:now.format('Y-m-d'),
									GD_TglMasuk 	: vTanggal,
									GD_UrutMasuk 	: vUrutMasuk,
									GD_KdUnit 		: vKdUnit,
                                    GD_KdDokter     : Ext.getCmp('cboDOKTER_viPenJasRad').getValue(),
									kd_dokter_penunjang 	: tmpkddoktertujuan,
									no_photo_rad 	: Ext.getCmp('txtnofotorad').getValue(),
								
								};
							Ext.Ajax.request({
								url: baseURL + "index.php/rad/Radiologi/save_transfer",
								params: parameter,
								failure: function (o){
									ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
								},
								success: function (o){  
									loadMask.hide();
									var cst = Ext.decode(o.responseText);
									if (cst.success === true){   
										kdkasir_tmp = kdkasir;
                                        rowSelectedPenJasRad.data.NO_TRANSAKSI  = cst.no_transaksi;
                                        rowSelectedPenJasRad.data.KD_KASIR      = cst.kd_kasir;
										Ext.getCmp('btnSimpanPenjasRAD').disable();
										ShowPesanInfoPenJasRad('Transfer Berhasil', 'transfer ');
										Ext.getCmp('btnTambahBarisPenJasRad').disable();
										Ext.getCmp('btnLookupPenJasRad').disable();
										Ext.getCmp('btnHpsBrsItemRad').disable();
										Ext.getCmp('btnpemakaianfilmRadiologiPenjasRAD').disable();
										Ext.getCmp('btnDocumentPenjasRAD').disable();
										Ext.getCmp('btnLookUpEditDokterPenindak_rad').disable();
										RefreshDatahistoribayar(Ext.getCmp('txtNoTransaksiPenJasRad').getValue());
										validasiJenisTr();
									} else{
										ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
									};
								}
							});
						}
					
                    
                }//, disabled: true
            },{
				id: 'btnPembayaranPenjasRAD',
				text: 'Pembayaran',
				tooltip: nmEditData,
                hidden : false,
				iconCls: 'Edit_Tr',
				handler: function (sm, row, rec){
					if (rowSelectedPenJasRad != undefined) {
						PenjasLookUpRAD(rowSelectedPenJasRad.data);
					} else {
						PenjasLookUpRAD();
						//ShowPesanWarningPenJasRad('Pilih data tabel  ', 'Pembayaran');
					}
				}//, disabled: true
			},'-',
			{
				text: 'Tutup Transaksi',
				id: 'btnTutupTransaksiPenjasRAD',
				tooltip: nmHapus,
				iconCls: 'remove',
				handler: function (){
					if (noTransaksiPilihan == '' || noTransaksiPilihan == 'undefined') {
						ShowPesanWarningPenJasRad('Pilih data tabel  ', 'Pembayaran');
					} else {
						UpdateTutuptransaksi(false);
						Ext.getCmp('btnPembayaranPenjasRAD').disable();
						Ext.getCmp('btnTutupTransaksiPenjasRAD').disable();
						Ext.getCmp('btnHpsBrsItemRad').disable();
					}
				}//, disabled: true
			},'-',{
                    text    : 'Cetak',
                    id      : 'btnCetak_Billing',
                    iconCls : 'print',
                    handler : function(){
                        // var url_laporan = baseURL + "index.php/fisiotherapy/lap_billing/";
                        var url_laporan = baseURL + "index.php/laporan/lap_billing_penunjang/";
                        /*var params={
                            no_transaksi    : rowSelectedPenJasRad.data.NO_TRANSAKSI, 
                            kd_kasir        : rowSelectedPenJasRad.data.KD_KASIR,
                        } ;
                        console.log(params);
                        var form = document.createElement("form");
                        form.setAttribute("method", "post");
                        form.setAttribute("target", "_blank");
                        form.setAttribute("action", url_laporan+"/preview_pdf");
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", "data");
                        hiddenField.setAttribute("value", Ext.encode(params));
                        form.appendChild(hiddenField);
                        document.body.appendChild(form);
                        form.submit();    */
                        GetDTLPreviewBilling(url_laporan+"/preview_pdf", rowSelectedPenJasRad.data.NO_TRANSAKSI, rowSelectedPenJasRad.data.KD_KASIR);
                    }
                },'-',{
                    id: 'btnSendToPACSPenjasRAD',
                    text: 'Kirim ke PACS',
                    tooltip: nmEditData,
                    iconCls: 'Edit_Tr',
                    handler: function (sm, row, rec)
                    {
                        Ext.Ajax.request
                        (
                            {
                                url: baseURL + "index.php/main/functionRAD/send_to_pacs",
                                params: {
                                        no_transaksi: Ext.get('txtNoTransaksiPenJasRad').getValue(),
                                        kd_pasien:Ext.getCmp('txtNoMedrecL').getValue(),
                                        kd_kasir: kd_kasir_rad
                                },
                                failure: function(o)
                                {
                                    ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                                },  
                                success: function(o) 
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true) 
                                    {
                                        rowSelectedPenJasRad.data.PATIENT_ID    = cst.patient_id;
                                        rowSelectedPenJasRad.data.STUDY_UID     = cst.study_uid;
                                        ShowPesanInfoPenJasRad('Data Berhasil di kirim ke PACS', 'Sukses');
                                        /* dsTRDetailPenJasRadList.removeAt(line);
                                        gridDTItemTest.getView().refresh(); */
                                        //Delete_Detail_Rad_SQL();
                                    }
                                    else 
                                    {
                                        //ShowPesanWarningPenJasRad('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
                                    };
                                }
                            }
                        )
                    }//, disabled: true
                },'-',{
					id: 'btnPreviewDokument_pacsPenataJasaRad',
					text: 'Document',
					tooltip: nmEditData,
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec){
                        var url_file = "http://10.11.0.35:8080/oviyam/viewer.html?patientID="+rowSelectedPenJasRad.data.PATIENT_ID+"&studyUID="+rowSelectedPenJasRad.data.STUDY_UID+"&serverName=PACS";
                        if (rowSelectedPenJasRad.data.PATIENT_ID == null || rowSelectedPenJasRad.data.PATIENT_ID == '' || rowSelectedPenJasRad.data.STUDY_UID == null || rowSelectedPenJasRad.data.STUDY_UID == '') {
                            Ext.Msg.alert('Information', 'Tidak ada dokument pemeriksaan');
                        }else{
                            var form = document.createElement("form");
                            form.setAttribute("method", "post");
                            form.setAttribute("target", "_blank");
                            form.setAttribute("action", url_file);
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", "data");
                            hiddenField.setAttribute("value", '');
                            form.appendChild(hiddenField);
                            document.body.appendChild(form);
                            form.submit();
                        }
					},
                },
		]
	});
	var x;
	var GDtabDetailRad = new Ext.TabPanel({
        id:'GDtabDetailRad',
		style:'padding:4px;',
        activeTab: 0,
		flex:1,
        border:true,
        defaults:{
            autoScroll: true
        },
        items: [GridDetailItemPemeriksaan(),GetDTLTRHistoryGrid()],
		listeners:{   
			'tabchange' : function (panel, tab) {
				if (x ==1){
					Ext.getCmp('btnLookupPenJasRad').hide()
					Ext.getCmp('btnSimpanRAD').show()
					Ext.getCmp('btnHpsBrsItemRad').hide()
					x=2;
					return x;
				}else{   
					Ext.getCmp('btnLookupPenJasRad').show()
					Ext.getCmp('btnSimpanRAD').show()
					Ext.getCmp('btnHpsBrsItemRad').show()
					x=1;    
					return x;
				}

			}
		}
	});
	// var pnlTRPenjasRAD2 = new Ext.FormPanel({
		// id: 'PanelTRPenJasRad2',
		// fileUpload: true,
		// region: 'north',
		// layout: 'fit',
		// flex:1,
		// // bodyStyle: 'padding:10px 10px 10px 10px',
		// height:460,
		// //anchor: '100%',
		// width: lebar,
		// border: false,
		// items: [    
			// GDtabDetailRad
		// ]
	// });
    var FormDepanPenJasRad = new Ext.Panel({
		id: 'FormDepanPenJasRad',
		layout:{
			type:'vbox',
			align:'stretch'
		},
		border: true,
		items:[
			pnlTRPenjasRAD,GDtabDetailRad  
		]
	});
    return FormDepanPenJasRad
}

function DataDeletePenJasRadDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeletePenJasRadDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoPenJasRad(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailPenJasRadList.removeAt(CurrentPenJasRad.row);
                    cellSelecteddeskripsi=undefined;
                    ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,kd_kasir_rad);
                    AddNewPenJasRad = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningPenJasRad(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningPenJasRad(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeletePenJasRadDetail()
{
    var params =
    {//^^^
        Table: 'ViewDetailTransaksiPenJasRad',
        TrKodeTranskasi: CurrentPenJasRad.data.data.NO_TRANSAKSI,
        TrTglTransaksi:  CurrentPenJasRad.data.data.TGL_TRANSAKSI,
        TrKdPasien :     CurrentPenJasRad.data.data.KD_PASIEN,
        TrKdNamaPasien : Ext.get('txtNamaPasienL').getValue(),
        //TrAlamatPasien : Ext.get('txtAlamatL').getValue(),    
        TrKdUnit :       Ext.get('txtKdUnitRad').getValue(),
        TrNamaUnit :     Ext.get('txtNamaUnit').getValue(),
        Uraian :         CurrentPenJasRad.data.data.DESKRIPSI2,
        AlasanHapus :    variablehistori,
        TrHarga :        CurrentPenJasRad.data.data.HARGA,
        
        TrKdProduk :     CurrentPenJasRad.data.data.KD_PRODUK,
        RowReq: CurrentPenJasRad.data.data.URUT,
        Hapus:2
    };
    
    return params
};

function getParamDataupdatePenJasRadDetail()
{
    var params =
    {
        Table: 'ViewDetailTransaksiPenJasRad',
        TrKodeTranskasi: CurrentPenJasRad.data.data.NO_TRANSAKSI,
        RowReq: CurrentPenJasRad.data.data.URUT,

        Qty: CurrentPenJasRad.data.data.QTY,
        Ubah:1
    };
    
    return params
};

function GridDetailItemPemeriksaan() {
    var fldDetailRAD = ['cito','kd_produk','deskripsi','deskripsi2','kd_unit','kd_tarif','harga','qty','desc_req','tgl_berlaku',
    'no_transaksi','urut','desc_status','tgl_transaksi','kp_produk','dokter','group','jumlah_dokter','kd_kasir','catatan'];
    dsTRDetailPenJasRadList = new WebApp.DataStore({ fields: fldDetailRAD })
    gridDTItemTest = new Ext.grid.EditorGridPanel({
		title: 'Item Test',
		id: 'gridDTItemTest',
		stripeRows: true,
		store: dsTRDetailPenJasRadList,
		border: false,
		columnLines: true,
		autoScroll:true,
		viewConfig: {forceFit: true},
		sm: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners:{
				cellselect: function(sm, row, rec){
					cellSelecteddeskripsi = dsTRDetailPenJasRadList.getAt(row);
					CurrentPenJasRad.row = row;
					CurrentPenJasRad.data = cellSelecteddeskripsi;
					currentJasaDokterKdTarif_RAD=cellSelecteddeskripsi.data.kd_tarif;
					currentJasaDokterKdProduk_RAD=cellSelecteddeskripsi.data.kd_produk;
					currentJasaDokterUrutDetailTransaksi_RAD=cellSelecteddeskripsi.data.urut;
					currentJasaDokterHargaJP_RAD=cellSelecteddeskripsi.data.harga;
					// tmp_group_dokter=cellSelecteddeskripsi.data.group
				}
			}
		}),
		cm: TRPenJasRadColumModel(),
		tbar:[
            {
                text: 'Tambah Baris',
                id: 'btnTambahBarisPenJasRad',
                tooltip: nmLookup,
                iconCls: 'Edit_Tr',
                handler: function(){
					if (Ext.get('cboDOKTER_viPenJasRad').getValue() == 'Pilih Dokter' || Ext.get('cboDOKTER_viPenJasRad').dom.value  === undefined){
						ShowPesanWarningPenJasRad('Dokter Radiologi tidak boleh kosong!','Warning');
                    }else{
						TambahBarisRAD();
					}
                }
            },{
                text: 'Tambah Item Pemeriksaan',
                id: 'btnLookupPenJasRad',
                tooltip: nmLookup,
                iconCls: 'find',
                handler: function(){
					if (Ext.get('cboDOKTER_viPenJasRad').getValue() == 'Pilih Dokter' || Ext.get('cboDOKTER_viPenJasRad').dom.value  === undefined){
						ShowPesanWarningPenJasRad('Dokter Radiologi tidak boleh kosong!','Warning');
                    }else{
						getTindakanRad();
						setLookUp_getTindakanPenjasRad();
					}
                }
            },{
				id:'btnHpsBrsItemRad',
				text: 'Hapus Baris',
				tooltip: 'Hapus Baris',
				disabled:false,
				iconCls: 'RemoveRow',
				handler: function(){
					if (dsTRDetailPenJasRadList.getCount() > 0 ) {
						if (cellSelecteddeskripsi != undefined) {
							Ext.Msg.confirm('Warning', 'Apakah item pemeriksaan ini akan dihapus?', function(button){
								if (button == 'yes'){
									if(CurrentPenJasRad != undefined) {
										var line = gridDTItemTest.getSelectionModel().selection.cell[0];
										Ext.Ajax.request({
											url: baseURL + "index.php/main/functionRAD/deletedetailrad",
											params: getParamHapusDetailTransaksiRAD(),
											failure: function(o){
												ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
											},  
											success: function(o){
												var cst = Ext.decode(o.responseText);
												if (cst.success === true && cst.tmp === 'ada'){
													ShowPesanInfoPenJasRad('Data Berhasil Di hapus', 'Sukses');
													dsTRDetailPenJasRadList.removeAt(line);
													gridDTItemTest.getView().refresh();
													//Delete_Detail_Rad_SQL();
												}else if (cst.success === true && cst.tmp === 'kosong') {
													dsTRDetailPenJasRadList.removeAt(line);
												}else{
														ShowPesanWarningPenJasRad('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
												}
											}
										});
									}
								}
							})
						} else {
							ShowPesanWarningPenJasRad('Pilih record ','Hapus data');
						}
					}
				}
            },{
                text: 'Pemakaian Film',
                id: 'btnpemakaianfilmRadiologiPenjasRAD',
                tooltip: nmHapus,
                iconCls: 'Edit_Tr',
                handler: function (){
                    var tmpdata = cellSelecteddeskripsi;
                    if (tmpdata === undefined) {
                        ShowPesanWarningPenJasRad('Pilih record ','Update Film');
                    }else{
                        PenJasRadPemakianFilmLookUp(tmpdata.data);
                    }

                    // if (noTransaksiPilihan == '' || noTransaksiPilihan == 'undefined') {
                    //  ShowPesanWarningPenJasRad('Pilih data tabel  ', 'Pembayaran');
                    // } else {
                    //  UpdateTutuptransaksi(false);
                    //  Ext.getCmp('btnPembayaranPenjasRAD').disable();
                    //  Ext.getCmp('btnTutupTransaksiPenjasRAD').disable();
                    //  Ext.getCmp('btnHpsBrsItemRad').disable();
                    // }
                }//, disabled: true
            },{
                text: 'Document',
                id: 'btnDocumentPenjasRAD',
                tooltip: 'Document',
                iconCls: 'Edit_Tr',
                handler: function (){
                    lookUpFileManagement();
                }
            },{
				id		:'btnLookUpEditDokterPenindak_rad',
				text	: 'Edit Pelaksana',
				iconCls	: 'Edit_Tr',
				handler	: function(){
					if(currentJasaDokterKdProduk_RAD == '' || currentJasaDokterKdProduk_RAD == undefined){
						ShowPesanWarningPenJasRad('Pilih item dokter penindak yang akan diedit!','Error');
					} else{
						if(currentJasaDokterUrutDetailTransaksi_RAD == 0 || currentJasaDokterUrutDetailTransaksi_RAD == undefined){
							ShowPesanWarningPenJasRad('Item ini belum ada dokter penindaknya! atau data harap simpan terlebih dahulu.','Error');
						} else{
							loaddatastoredokterVisite_REVISI();
							PilihDokterLookUpPJ_RAD_REVISI();
						}
					}
				}
			}
        ]
	});
    return gridDTItemTest;
}
function loaddatastoredokterVisite_REVISI(){
	Ext.Ajax.request({
		url: baseURL +  "index.php/general/control_cmb_dokter/getCmbDokterVisite",
		params: {
			kd_kasir        : cellSelecteddeskripsi.data.kd_kasir,
			no_transaksi    : cellSelecteddeskripsi.data.no_transaksi,
			tgl_transaksi   : cellSelecteddeskripsi.data.tgl_transaksi,
			kd_produk       : cellSelecteddeskripsi.data.kd_produk,
			urut            : cellSelecteddeskripsi.data.urut,
		},
		success: function(response) {
			var cst  = Ext.decode(response.responseText);
			for(var i=0,iLen=cst['data'].length; i<iLen; i++){
				var recs    = [],recType = dsDataDokterPenindak_PJ_RAD.recordType;
				var o       = cst['data'][i];
				recs.push(new recType(o));
				DataStoreSecondGridStore_PJRAD.add(recs);
			}
		},
	});
}
function loaddatastorePenangan(){
	Ext.Ajax.request({
		url: baseURL +  "index.php/general/control_cmb_dokter/getCmbDokterInapInt",
		params: {
			groups : tmp_group_dokter,
		},
		success: function(response) {
			var cst  = Ext.decode(response.responseText);
			for(var i=0,iLen=cst['data'].length; i<iLen; i++){
				var recs    = [],recType = dsDataDaftarPenangan_Penjas_RAD.recordType;
				var o       = cst['data'][i];
				recs.push(new recType(o));
				dsDataDaftarPenangan_Penjas_RAD.add(recs);
			}
		},
		callback : function(){
			Ext.getCmp('txtSearchDokter_PENJASRAD').focus();
		}
	});
}
function cboPenangan_PenjasRAD(){
	var tmpValue;
	/*if (tmpKdJob == 3) {
		tmpValue = "Perawat";
	}else{
		tmpValue = "Dokter";
	}*/
	var Field = ['KD_JOB','KD_COMPONENT','LABEL'];
	dsDataDaftarPenangan_Penjas_RAD = new WebApp.DataStore({ fields: Field });
	loaddatastorePenangan();
	var cboPenangan_PENJASRAD       = new Ext.form.ComboBox({
		id: 'cboPenangan_PENJASRAD',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih penangan...',
		fieldLabel: 'Penangan ',
		align: 'Right',
		store: dsDataDaftarPenangan_Penjas_RAD,
		valueField: 'KD_JOB',
		displayField: 'LABEL',
		//anchor           : '100%',
		value           : tmpValue,
		listeners:{
			select : function(a, b, c){
				DataStorefirstGridStore_PJRAD.removeAll();
				tmpKdJob =  b.data.KD_JOB;
				loaddatastoredokter_REVISI(cellSelecteddeskripsi,null);
			}
		}
	});
	return cboPenangan_PENJASRAD;
}
function firstGridPenerimaan(){
	DataStorefirstGridStore_PJRAD.removeAll();
	var cols = [
		{ id : 'KD_DOKTER', header: "Kode Pelaksana", width: .25, sortable: true, dataIndex: 'KD_DOKTER',hidden : false},
		{ id : 'NAMA', header: "Nama", width: .75, sortable: true, dataIndex: 'NAMA'}
	];
	var firstGrid;
		firstGrid = new Ext.grid.GridPanel({
		ddGroup          : 'secondGridDDGroup',
		store            : DataStorefirstGridStore_PJRAD,
		columns          : cols,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		enableDragDrop   : true,
		stripeRows       : true,
		flex:1,
		trackMouseOver   : true,
		title            : 'Dokter/ Perawat penangan',
		plugins          : [new Ext.ux.grid.FilterRow()], 
		colModel         : new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
					id: 'colNMKd_Dokter',
					header: 'KD Dokter',
					dataIndex: 'KD_DOKTER',
					sortable: true,
					hidden : false,
					width: .25
			},
			{
					id: 'colNMDokter',
					header: 'Nama',
					dataIndex: 'NAMA',
					sortable: true,
					width: .75
			}
		]),
		listeners : {
			afterrender : function(comp) {
				var firstGridDropTargetEl =  firstGrid.getView().scroller.dom;
				var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
					ddGroup    : 'firstGridDDGroup',
					notifyDrop : function(ddSource, e, data){
						var records   =  ddSource.dragData.selections;
                        var kd_dokter = "";
						var line      = "";
						for(var i=0,iLen=records.length; i<iLen; i++){
							var o       = records[i];
                            kd_dokter   = o.data.KD_DOKTER;
							line        = o.data.LINE;
						}
						Ext.Ajax.request({
							url: baseURL +  "index.php/rawat_inap/control_visite_dokter/deleteDokter",
							params: {
								kd_kasir        : cellSelecteddeskripsi.data.kd_kasir,
								no_transaksi    : cellSelecteddeskripsi.data.no_transaksi,
								urut            : cellSelecteddeskripsi.data.urut,
								tgl_transaksi   : cellSelecteddeskripsi.data.tgl_transaksi,
								kd_dokter       : kd_dokter,
								kd_produk       : cellSelecteddeskripsi.data.kd_produk,
                                kd_unit         : cellSelecteddeskripsi.data.kd_unit,
								line            : line,
							},
							success: function(response) {
								var cst  = Ext.decode(response.responseText);
								var dataRowIndexDetail = gridDTItemTest.getSelectionModel().selection.cell[0];
								// PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
								dsTRDetailPenJasRadList.getRange()[dataRowIndexDetail].set('jumlah_dokter', cst.JUMLAH_DOKTER);
								Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
								firstGrid.store.add(records);
								firstGrid.getView().refresh();
								return true
							},
						});
					}
				});
			},
			rowdblclick: function(dataview, index, item, e) {
				var group_dokter = 0;
				console.log(tmp_group_dokter);
				if (tmp_group_dokter!= 0 || tmp_group_dokter != '' || tmp_group_dokter != 'undefined' || tmp_group_dokter != null) {
					// group_dokter = cellSelecteddeskripsi_panatajasaigd.data.GROUP;
					group_dokter =tmp_group_dokter;
				}
				Ext.Ajax.request({
					url: baseURL +  "index.php/rad/control_visite_dokter/insertDokter",
					params: {
						label           : Ext.getCmp("cboPenangan_PENJASRAD").getValue(),
						kd_job          : tmpKdJob,
						no_transaksi    : cellSelecteddeskripsi.data.no_transaksi,
						tgl_transaksi   : cellSelecteddeskripsi.data.tgl_transaksi,
						kd_produk       : cellSelecteddeskripsi.data.kd_produk,
						urut            : cellSelecteddeskripsi.data.urut,
						kd_kasir        : cellSelecteddeskripsi.data.kd_kasir,
						kd_unit         : cellSelecteddeskripsi.data.kd_unit,
						kd_dokter       : DataStorefirstGridStore_PJRAD.data.items[index].data.KD_DOKTER,
						kd_tarif        : cellSelecteddeskripsi.data.kd_tarif,
						tgl_berlaku     : cellSelecteddeskripsi.data.tgl_berlaku,
						group           : group_dokter,
					},
					success: function(response) {
						DataStoreSecondGridStore_PJRAD.removeAll();
						loaddatastoredokterVisite_REVISI();
						var cst  = Ext.decode(response.responseText);
						var dataRowIndexDetail = gridDTItemTest.getSelectionModel().selection.cell[0];
                        dsTRDetailPenJasRadList.getRange()[dataRowIndexDetail].set('deskripsi', cst.data.DESKRIPSI+" ("+cst.data.DAFTAR_DOKTER+")");
						dsTRDetailPenJasRadList.getRange()[dataRowIndexDetail].set('jumlah_dokter', cst.data.JUMLAH_DOKTER);
						var selected = firstGrid.getSelectionModel().getSelections();
						if(selected.length>0) {
							for(var i    =0;i<selected.length;i++) {
								DataStorefirstGridStore_PJRAD.remove(selected[i]);
							}
						}
					}
				});
			}
		},
		viewConfig:{
			forceFit: true,
		}
	});
	return firstGrid;
}
function seconGridPenerimaan(){
	DataStoreSecondGridStore_PJRAD.removeAll();
	var secondGrid;
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];
	secondGridStoreLapPenerimaan = new Ext.data.JsonStore({
		fields : fieldsDokterPenindak,
		root   : 'data'
	});
	var cols = [
		{ id : 'KD_DOKTER', header: "Kode Pelaksana", width: .25, sortable: true, dataIndex: 'KD_DOKTER',hidden : false},
		{ header: "Nama", width: .75, sortable: true, dataIndex: 'NAMA'},
		{ header: "JP", width: .25, dataIndex: 'JP'},
		{ header: "PRC", width: .25, dataIndex: 'PRC'},
	];
	secondGrid = new Ext.grid.GridPanel({
		ddGroup          : 'firstGridDDGroup',
		store            : DataStoreSecondGridStore_PJRAD,
		columns          : cols,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		enableDragDrop   : true,
		flex:1,
		enableDragDrop   : true,
		stripeRows       : true,
		autoExpandColumn : 'KD_DOKTER',
		title            : 'Dokter/ Perawat yang menangani',
		plugins          : [new Ext.ux.grid.FilterRow()],
		listeners : {
			afterrender : function(comp) {
				var group_dokter = 0;
				if (tmp_group_dokter!= 0 || tmp_group_dokter != '' || tmp_group_dokter != 'undefined' || tmp_group_dokter != null) {
					group_dokter = tmp_group_dokter;
				}
				var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
				var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'secondGridDDGroup',
					notifyDrop : function(ddSource, e, data){
						var records   =  ddSource.dragData.selections;
						var kd_dokter = "";
						for(var i=0,iLen=records.length; i<iLen; i++){
							var o       = records[i];
							kd_dokter = o.data.KD_DOKTER;
						}
						Ext.Ajax.request({
							url: baseURL +  "index.php/rad/control_visite_dokter/insertDokter",
							params: {
								label           : Ext.getCmp("cboPenangan_PENJASRAD").getValue(),
								kd_job          : tmpKdJob,
								no_transaksi    : cellSelecteddeskripsi.data.no_transaksi,
								tgl_transaksi   : cellSelecteddeskripsi.data.tgl_transaksi,
								kd_produk       : cellSelecteddeskripsi.data.kd_produk,
								urut            : cellSelecteddeskripsi.data.urut,
								kd_kasir        : cellSelecteddeskripsi.data.kd_kasir,
								kd_unit         : cellSelecteddeskripsi.data.kd_unit,
								kd_dokter       : kd_dokter,
								kd_tarif        : cellSelecteddeskripsi.data.kd_tarif,
								tgl_berlaku     : cellSelecteddeskripsi.data.tgl_berlaku,
								group           : group_dokter,
							},
							success: function(response) {
								DataStoreSecondGridStore_PJRAD.removeAll();
								loaddatastoredokterVisite_REVISI();
								// RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
								var cst  = Ext.decode(response.responseText);
								var dataRowIndexDetail = gridDTItemTest.getSelectionModel().selection.cell[0];
								//console.log(cst);
                                dsTRDetailPenJasRadList.getRange()[dataRowIndexDetail].set('deskripsi', cst.data.DESKRIPSI+" ("+cst.data.DAFTAR_DOKTER+")");
								dsTRDetailPenJasRadList.getRange()[dataRowIndexDetail].set('jumlah_dokter', cst.data.JUMLAH_DOKTER);
								Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
								//secondGrid.store.add(records);
								secondGrid.getView().refresh();
								return true
							},
						});
					}
				});
			},
			rowdblclick: function(dataview, index, item, e) {
				//DataStorefirstGridStore.data.items[index].data.KD_DOKTER
				Ext.Ajax.request({
					url: baseURL +  "index.php/rawat_inap/control_visite_dokter/deleteDokter",
					params: {
						kd_kasir        : cellSelecteddeskripsi.data.kd_kasir,
						no_transaksi    : cellSelecteddeskripsi.data.no_transaksi,
						urut            : cellSelecteddeskripsi.data.urut,
						tgl_transaksi   : cellSelecteddeskripsi.data.tgl_transaksi,
						kd_dokter       : DataStoreSecondGridStore_PJRAD.data.items[index].data.KD_DOKTER,
						kd_produk       : cellSelecteddeskripsi.data.kd_produk,
						kd_unit         : cellSelecteddeskripsi.data.kd_unit,
                        line            : DataStoreSecondGridStore_PJRAD.data.items[index].data.LINE,
					},
					success: function(response) {
						var cst  = Ext.decode(response.responseText);
						// PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
						var dataRowIndexDetail = gridDTItemTest.getSelectionModel().selection.cell[0];
						dsTRDetailPenJasRadList.getRange()[dataRowIndexDetail].set('jumlah_dokter', cst.JUMLAH_DOKTER);
						DataStoreSecondGridStore_PJRAD.removeAll();
						loaddatastoredokterVisite_REVISI();

					},
				});
			},
			callback : function(){
				Ext.getCmp('txtSearchDokter_PENJASRAD').focus();
			}
		},
		viewConfig:{
			forceFit: true,
		}
	});
	return secondGrid;
}
function loaddatastoredokter_REVISI(params, dokter = null,mod){
	DataStorefirstGridStore_PJRAD.removeAll();
	var tmp_urut;
	if (typeof params.data.urut == 'undefined') {
		tmp_urut = params.data.urut_masuk;
	}else{
		tmp_urut = params.data.urut;
	}
	Ext.Ajax.request({
		url: baseURL +  "index.php/general/control_cmb_dokter/getCmbPelaksana_penunjang",
		params: {
			kd_unit          : cellSelecteddeskripsi.data.kd_unit,
			mod_id           : TrPenJasRad.mod_id,
			jenis_dokter     : tmpKdJob,
			kd_kasir         : cellSelecteddeskripsi.data.kd_kasir,
			no_transaksi     : cellSelecteddeskripsi.data.no_transaksi,
			urut             : tmp_urut,
			tgl_transaksi    : cellSelecteddeskripsi.data.tgl_transaksi,
			kd_job           : tmpKdJob,
			txtDokter        : dokter,
            kd_unit_         : TrPenJasRad.setting.KD_UNIT_RAD[0],
		},
		success: function(response) {
			var cst  = Ext.decode(response.responseText);
			console.log(cst);
			for(var i=0,iLen=cst['data'].length; i<iLen; i++){
				var recs    = [],recType = dsDataDokterPenindak_PJ_RAD.recordType;
				var o       = cst['data'][i];
				recs.push(new recType(o));	
				DataStorefirstGridStore_PJRAD.add(recs);
			}
		}
	});
}
function PilihDokterLookUpPJ_RAD_REVISI() {    
    var FormLookUDokter_RAD = new Ext.Window({
		xtype : 'fieldset',
		layout:'fit',
		id: 'winTRDokterPenindak_RAD_REVISI',
		title: 'Pilih Pelaksana',
		closeAction: 'destroy',
		width: 600,
		height: 400,
		border: true,
		resizable: false,
		constrain : true,    
		modal: true,
		items: [ 
			{
				layout:{
					type:'hbox',
					align:'stretch'
				},
				bodyStyle:'padding: 4px;',
				border:false,
				items:[
					{
						border:false,
						flex:1,
						layout:{
							type:'vbox',
							align:'stretch'
						},
						items:[
							cboPenangan_PenjasRAD(),
							seconGridPenerimaan(),
						]
					},{
						border:false,
						flex:1,
						layout:{
							type:'vbox',
							align:'stretch'
						},
						style:'padding-left: 4px;',
						items:[
							{
								xtype   : 'textfield',
								anchor  : '100%',
								id      : 'txtSearchDokter_PENJASRAD',
								name    : 'txtSearchDokter_PENJASRAD',
								listeners : {
									specialkey: function (){
										if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
											DataStorefirstGridStore_PJRAD.removeAll();
											tmpSearchDokter = Ext.getCmp('txtSearchDokter_PENJASRAD').getValue();
											loaddatastoredokter_REVISI(cellSelecteddeskripsi, Ext.getCmp('txtSearchDokter_PENJASRAD').getValue());
										}
									},
								}
							},
							firstGridPenerimaan(),
						]
					}
				]
			}
		],
		tbar :[
			{
				xtype:'button',
				text:'Simpan',
				iconCls : 'save',
				hideLabel:true,
				id: 'BtnOktrDokter',
				handler:function(){
					//RefreshDataDetail_kasirrwi(tmpno_transaksi);
					
					//PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('DESKRIPSI')+" ("+tmpNamaDokter+")");
					FormLookUDokter_RAD.close();
				}
			},
			'-',
		],
		listeners:{
			activate: function(){
			},
			afterShow: function(){
				this.activate();
			},
			deactivate: function(){
				batal_isi_tindakan='y';
			}
		}
	});
    FormLookUDokter_RAD.show();
    FormLookUDokter_RAD.center();
}

var dsgridpilihdokterpenindak_RAD;
dsgridpilihdokterpenindak_RAD	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_dokter','nama'],
	data: []
});
function PilihDokterLookUp_rad(edit) 
{
    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			header			: 'kd_component',
			dataIndex		: 'kd_component',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },
		{
			header			: 'tgl_berlaku',
			dataIndex		: 'tgl_berlaku',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },
		{
			header			: 'Komponent',
			dataIndex		: 'component',
			width			: 200,
			menuDisabled	: true,
			hidden 		: false
        },
        {
			header			: 'kd_dokter',
			dataIndex		: 'kd_dokter',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },{
            header			:'Nama Dokter',
            dataIndex		: 'nama',
            sortable		: false,
            hidden			: false,
			menuDisabled	: true,
			width			: 250,
            editor			: new Nci.form.Combobox.autoComplete({
				store	: dsgridpilihdokterpenindak_RAD,
				select	: function(a,b,c){
					var line	= GridDokterTr_RAD.getSelectionModel().selection.cell[0];
					dsGridJasaDokterPenindak_RAD.getRange()[line].data.kd_dokter=b.data.kd_dokter;
					dsGridJasaDokterPenindak_RAD.getRange()[line].data.nama=b.data.nama;
					GridDokterTr_RAD.getView().refresh();
				},
				insert	: function(o){
					return {
						kd_dokter       : o.kd_dokter,
						nama       		: o.nama,
						text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_dokter+'</td><td width="200">'+o.nama+'</td></tr></table>'
					}
				},
				url		: baseURL + "index.php/main/functionIGD/getdokterpenindak",
				valueField: 'nama_Radiologi',
				displayField: 'text',
				listWidth: 380
			})
			//getTrDokter(dsTrDokter)
	    },
        ]
    );
	
	var fldDetail = [];
    dsGridJasaDokterPenindak_RAD = new WebApp.DataStore({ fields: fldDetail });
	GridDokterTr_RAD= new Ext.grid.EditorGridPanel({
		id			: 'GridDokterTr_RAD',
		stripeRows	: true,
		width		: 487,
		height		: 160,
        store		: dsGridJasaDokterPenindak_RAD,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridTrDokterColumnModel,
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				trcellCurrentTindakan_RAD = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				
			}
		},
		viewConfig	: {forceFit: true},
    });
 
	
    var lebar = 500;
    var FormLookUDokter_RAD = new Ext.Window
    (
        {
            id: 'winTRDokterPenindak_RAD',
            title: 'Pilih Dokter Penindak',
            closeAction: 'destroy',
            width: 500,
            height: 220,
            border: false,
            resizable: false,
            iconCls: 'Request',
			constrain : true,    
			modal: true,
           	items: [ 
				GridDokterTr_RAD
			],
			tbar :
			[
				/* {
					xtype	: 'button',
					id		: 'btnaddtrdokter',
					iconCls	: 'add',
					text	: 'Tambah Data Dokter',
					handler	:  function()
					{
						dataTrDokter = [];		
						TambahBaristrDokter(dsTrDokter);	
					}
				},
				'-', */
				{
					xtype:'button',
					text:'Simpan',
					iconCls	: 'save',
					hideLabel:true,
					id: 'BtnOktrDokter',
					handler:function()
					{
						Datasave_PenJasRad(false,true);
						
						/* if(dataTrDokter.length == '' || dataTrDokter.length== 'undefined')
						{
							var jumlah =0;
						}
						else
						{
							var jumlah = dataTrDokter.length;
						}
						for(var i = 0 ;i< dsTrDokter.getCount()  ;i++)
						{	
							dataTrDokter.push({
								index		: i,
								no_tran 	: NoTrans,
								urut		: Urt,
								tgl_trans 	: TglTrans,
								kd_produk 	: kdPrdk,
								kd_tarif	: kdTrf,
								tgl_berlaku	: tglBerlaku,
								tarif		: trf,
								kd_dokter 	: dsTrDokter.data.items[i].data.KD_DOKTER ,
								kd_job 		: dsTrDokter.data.items[i].data.KD_JOB ,
							});
						}
						FormLookUDokter_IGD.close(); */	
					}
				},
				'-',
				/* {
					xtype	: 'button',
					id		: 'btndeltrdokter',
					iconCls	: 'remove',
					text	: 'Delete',
					handler	: function()  
					{
						if (dsTrDokter.getCount() > 0 )
						{
							if(trcellCurrentTindakan_IGD != undefined)
							{
								HapusDataTrDokter(dsTrDokter);
							}
						}else{
							ShowPesanWarningDiagnosa_igd('Pilih record ','Hapus data');
						}	
					}
				},
				'-', */
			],
            listeners:
            { 
            }
        }
    );
	FormLookUDokter_RAD.show();
	if(edit == true){
		GetgridEditDokterPenindakJasa_RAD()
	} else{
		GetgridPilihDokterPenindakJasa_RAD(currentJasaDokterKdProduk_RAD,currentJasaDokterKdTarif_RAD);
	}
	
};
function GetgridEditDokterPenindakJasa_RAD(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/viewgrideditjasadokterpenindak",
		params: {
			kd_unit:Ext.getCmp('txtKdUnitRad').getValue(),
			urut:currentJasaDokterUrutDetailTransaksi_RAD,
			kd_kasir:currentKdKasirRAD,
			no_transaksi:TmpNotransaksi,
			tgl_transaksi:TglTransaksi,
			
		},
		failure: function(o)
		{
			ShowPesanErrorPenJasRad('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_RAD.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				if(cst.totalrecords > 0){
					var recs=[],
						recType=dsGridJasaDokterPenindak_RAD.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsGridJasaDokterPenindak_RAD.add(recs);
					GridDokterTr_RAD.getView().refresh();
				} else{
					GetgridPilihDokterPenindakJasa_RAD(currentJasaDokterKdProduk_RAD,currentJasaDokterKdTarif_RAD);
				}
			} 
			else 
			{
				ShowPesanErrorPenJasRad('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}
function GetgridPilihDokterPenindakJasa_RAD(kd_produk,kd_tarif){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/viewgridjasadokterpenindak",
		params: {
			kd_produk:kd_produk,
			kd_tarif:kd_tarif,
			kd_unit:Ext.getCmp('txtKdUnitRad').getValue()
		},
		failure: function(o)
		{
			ShowPesanErrorPenJasRad('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_RAD.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsGridJasaDokterPenindak_RAD.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsGridJasaDokterPenindak_RAD.add(recs);
				GridDokterTr_RAD.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorPenJasRad('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}
function TRPenJasRadColumModel(){
    var kd_cus_gettarif=vkode_customerRAD;
    var modul='';
    if(radiovalues == 1){
        modul='igd';
    } else if(radiovalues == 2){
        modul='rwi';
    } else if(radiovalues == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    // checkColumn_penata__rad = new Ext.grid.CheckColumn({
    //    header: 'Cito',
    //    dataIndex: 'cito',
    //    id: 'checkid',
    //    width: 55,
    //    renderer: function(value)
    //    {
 //        console.log("Grid");
    //  },
    // });
    return new Ext.grid.ColumnModel([ 
            new Ext.grid.RowNumberer(),
            {
                header: 'Cito',
                dataIndex: 'cito',
                width:65,
                menuDisabled:true,
                listeners:{
                    itemclick: function(dv, record, items, index, e){
                        Ext.getCmp('btnHpsBrsItemRadL').enable();
                        console.log("test");
                    },
                },
                renderer:function (v, metaData, record){
                    if (record.data.cito=='0'){
                        record.data.cito='Tidak';
                    }else if (record.data.cito=='1' || record.data.cito=='Ya'){
                        metaData.style  ='background:#FF0000; width: 38px; font-weight: bolder;';
                        record.data.cito='Ya';
                    }
                    return record.data.cito; 
                },
                editor:new Ext.form.ComboBox({
                    id: 'cboKasus',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    selectOnFocus: true,
                    forceSelection: true,
                    emptyText: 'Silahkan Pilih...',
                    width: 50,
                    anchor: '95%',
                    value: 1,
                    store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: ['Id', 'displayText'],
                        data: [[1, 'Ya'], [2, 'Tidak']]
                    }),
                    valueField: 'displayText',
                    displayField: 'displayText',
                    value       : '',
                    listeners:{
                        'select': function(a,b,c){
                            var harga = 0;
                            var hargaasli = 0;
                            var tmpdata = CurrentPenJasRad.data;
                            var currentdata = tmpdata.deskripsi;
                            // console.log(tmpdata.data.kp_produk);
                            if (tmpdata.data.kp_produk === '' || tmpdata.data.kp_produk === undefined) {
								ShowPesanWarningPenJasRad('Produk Belum Di isi');
								gridDTItemTest.startEditing(CurrentPenJasRad.row, 5);
								dsTRDetailPenJasRadList.getRange()[CurrentPenJasRad.row].set('cito', 'Tidak');
							}else{
								if (b.data.displayText === 'Ya') {
									Ext.Ajax.request({
										store   : KasirRADDataStoreProduk,
										url     : baseURL + "index.php/main/functionRAD/getProdukList",
										params  :  {
											//kd_unit:tmpkd_unit,
											kd_unit:tmpkd_unit_asal,
											kd_customer:vkode_customerRAD,
											text:tmpdata.data.kp_produk,
											penjas:modul,
											kdunittujuan:tmpkd_unit
										},
										failure : function(o){
											ShowPesanErrorPenJasRad("Data produk tidak ada!",'WARNING');
										},
										success : function(o){
											var cst = Ext.decode(o.responseText);
											if (cst.processResult == 'SUCCESS'){
												console.log(cst.listData);
												hargaasli = cst.listData.harga;
												var hargapersen = ((parseInt(hargaasli)*parseInt(citopersentasi))/100);
												harga = parseInt(hargaasli) + parseInt(hargapersen);
												dsTRDetailPenJasRadList.getRange()[CurrentPenJasRad.row].set('harga', harga);
											}else{
												ShowPesanErrorPenJasRad('Data produk tidak ada!','WARNING');
											};
										}
									});                                            
								}else{
									if (hargaasli == 0) {
										Ext.Ajax.request({
											store   : KasirRADDataStoreProduk,
											url     : baseURL + "index.php/main/functionRAD/getProdukList",
											params  :  {
												//kd_unit:tmpkd_unit,
												kd_unit:tmpkd_unit_asal,
												kd_customer:vkode_customerRAD,
												text:tmpdata.data.kp_produk,
												penjas:modul,
												kdunittujuan:tmpkd_unit
											},
											failure : function(o){
												ShowPesanErrorPenJasRad("Data produk tidak ada!",'WARNING');
											},
											success : function(o){
												var cst = Ext.decode(o.responseText);
												if (cst.processResult == 'SUCCESS'){
													hargaasli = cst.listData.harga;
													dsTRDetailPenJasRadList.getRange()[CurrentPenJasRad.row].set('harga', hargaasli);
												}else{
													ShowPesanErrorPenJasRad('Data produk tidak ada!','WARNING');
												};
											}
										});
									}else{
										dsTRDetailPenJasRadList.getRange()[CurrentPenJasRad.row].set('harga', hargaasli);
									}                                            
								}
							}
                            
                        },
                    }
                       
                })
            },{
                header: 'kd_tarif',
                dataIndex: 'kd_tarif',
                width:250,
                menuDisabled:true,
                hidden :true

            },{
                header: 'Kode Produk',
                dataIndex: 'kd_produk',
                width:100,
                menuDisabled:true,
                hidden:true
            },{
                header:'Unit ',
                dataIndex: 'kd_unit',
                sortable: false,
                hidden:true,
                menuDisabled:true,
                width:320

            },{
                header: 'Kd Produk',
                dataIndex: 'kp_produk',
                width:100,
                menuDisabled:true,
                hidden:false,
                editor      : new Ext.form.TextField({
                    id                  : 'fieldcolKdProduk',
                    allowBlank          : true,
                    enableKeyEvents     : true,
                    width               : 30,
                    listeners           :{ 
                        'specialkey' : function(a, b){
                            if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
                                Ext.Ajax.request({
                                    store   : KasirRADDataStoreProduk,
                                    url     : baseURL + "index.php/main/functionRAD/getProdukList",
                                    params  :  {
                                            //kd_unit:tmpkd_unit,
											kd_unit:tmpkd_unit_asal,
                                            kd_customer:vkode_customerRAD,
                                            text:Ext.getCmp('fieldcolKdProduk').getValue(),
                                            penjas:modul,
                                            kdunittujuan:tmpkd_unit
                                    },
                                    failure : function(o){
                                        ShowPesanErrorPenJasRad("Data produk tidak ada!",'WARNING');
                                    },
                                    success : function(o){
                                        var cst = Ext.decode(o.responseText);
                                        if (cst.processResult == 'SUCCESS'){
                                            console.log(cst.listData);
                                            kd_produk           = cst.listData.kd_produk;
                                            kp_produk           = cst.listData.kp_produk;
                                            kd_tarif            = cst.listData.kd_tarif;
                                            deskripsi           = cst.listData.deskripsi;
                                            harga               = cst.listData.tarifx;
                                            tgl_berlaku         = cst.listData.tgl_berlaku;
                                            // jumlah              = cst.listData.jumlah;
                                            // status_konsultasi   = cst.listData.status_konsultasi;
                                            // var hargadb = toInteger(cst.listData.harga);
                                            // var tmpharga = formatCurrency(hargadb);
                                            dsTRDetailPenJasRadList.getRange()[CurrentPenJasRad.row].set('deskripsi', cst.listData.deskripsi);
                                            dsTRDetailPenJasRadList.getRange()[CurrentPenJasRad.row].set('qty', 1);
                                            dsTRDetailPenJasRadList.getRange()[CurrentPenJasRad.row].set('harga', cst.listData.harga);
                                            dsTRDetailPenJasRadList.getRange()[CurrentPenJasRad.row].set('kd_produk', kd_produk);

                                            dsTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.kd_produk         = cst.listData.kd_produk;
                                            dsTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.kp_produk         = cst.listData.kp_produk;
                                            dsTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.kd_unit           = cst.listData.kd_unit;
                                            dsTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.harga             = toInteger(cst.listData.harga);
                                            dsTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.deskripsi         = cst.listData.deskripsi;
                                            currentJasaDokterHargaJP_KasirRAD                                               = toInteger(cst.listData.harga);
                                            dsTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.tgl_berlaku       = cst.listData.tgl_berlaku;
                                            dsTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.kd_tarif          = cst.listData.kd_tarif;
                                            dsTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.tgl_transaksi     = tglGridBawah;
                                            dsTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.qty               = 1;
                                            gridDTItemTest.startEditing(CurrentPenJasRad.row, 10);
                                        }else{
                                            ShowPesanErrorPenJasRad('Data produk tidak ada!','WARNING');
                                        };
                                    }
                                })
                            }
                        }
                    }
                })
            },{   
				id:'nama_pemereksaaan_rad',
                dataIndex: 'deskripsi',
                header: 'Nama Pemeriksaan',
                sortable: true,
                menuDisabled:true,
                width: 320,
                editor:new Nci.form.Combobox.autoComplete({
                    store   : TrPenJasRadproduk,
                    select  : function(a,b,c){
                    //  console.log(b);
                    //Disini
                        // Ext.Ajax.request
                        // (
                            // {
                                // url: baseURL + "index.php/main/functionRAD/cekProduk",
                                // params:{kd_rad:b.data.kd_produk} ,
                                // failure: function(o)
                                // {
                                    // ShowPesanErrorPenJasRad('Hubungi Admin', 'Error');
                                // },
                                // success: function(o)
                                // {
                                    // Ext.getCmp('btnHpsBrsItemRadL').enable();
                                    // console.log("test");
                                    // var cst = Ext.decode(o.responseText);
                                    // if (cst.success === true)
                                    // {
                                        var line = gridDTItemTest.getSelectionModel().selection.cell[0];
                                        dsTRDetailPenJasRadList.data.items[line].data.deskripsi=b.data.deskripsi;
                                        dsTRDetailPenJasRadList.data.items[line].data.uraian=b.data.uraian;
                                        dsTRDetailPenJasRadList.data.items[line].data.kd_tarif=b.data.kd_tarif;
                                        dsTRDetailPenJasRadList.data.items[line].data.kd_produk=b.data.kd_produk;
                                        dsTRDetailPenJasRadList.data.items[line].data.kd_unit=b.data.kd_unit;
                                        dsTRDetailPenJasRadList.data.items[line].data.tgl_transaksi=b.data.tgl_transaksi;
                                        dsTRDetailPenJasRadList.data.items[line].data.tgl_berlaku=b.data.tgl_berlaku;
                                        dsTRDetailPenJasRadList.data.items[line].data.harga=b.data.harga;
                                        dsTRDetailPenJasRadList.data.items[line].data.qty=b.data.qty;
                                        dsTRDetailPenJasRadList.data.items[line].data.jumlah=b.data.jumlah;

                                        gridDTItemTest.getView().refresh();
										Datasave_PenJasRad(false); 
                                    // }
                                    // else
                                    // {
                                        // ShowPesanInfoPenJasRad('Nilai normal item '+b.data.deskripsi+' belum tersedia', 'Information');
                                    // };
                                // }
                            // }

                        // )
                        
                    },
                    insert  : function(o){
                        return {
                            uraian          : o.uraian,
                            kd_tarif        : o.kd_tarif,
                            kd_produk       : o.kd_produk,
                            tgl_transaksi   : o.tgl_transaksi,
                            tgl_berlaku     : o.tgl_berlaku,
                            harga           : o.harga,
                            qty             : o.qty,
                            kd_unit             : o.kd_unit,
                            deskripsi       : o.deskripsi,
                            jumlah          : o.jumlah,
                            text            :  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_produk+'</td><td width="150">'+o.deskripsi+'</td></tr></table>'
                        }
                    },
                    param   : function(){
                    // var kd_cus_gettarif;
                    // if(Ext.get('cboKelPasienRad').getValue()=='Perseorangan'){
                        // kd_cus_gettarif=Ext.getCmp('cboPerseoranganRad').getValue();
                    // }else if(Ext.get('cboKelPasienRad').getValue()=='Perusahaan'){
                        // kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryRad').getValue();
                    // }else {
                        // kd_cus_gettarif=Ext.getCmp('cboAsuransiRad').getValue();
                          // }
                    var params={};
                    params['kd_unit']=kodeunit;
                    params['kd_customer']=vkode_customerRAD;
                    return params;
                    },
                    url     : baseURL + "index.php/main/functionRAD/getProduk",
                    valueField: 'deskripsi',
                    displayField: 'text',
                    listWidth: 210
                })
            },{
                header: 'Tanggal Transaksi',
                dataIndex: 'tgl_transaksi',
                width: 130,
                menuDisabled:true,
                renderer: function(v, params, record) {
                    if(record.data.tgl_transaksi == undefined){
                        record.data.tgl_transaksi=tglGridBawah;
                        return record.data.tgl_transaksi;
                    } else{
                        if(record.data.tgl_transaksi.substring(5, 4) == '-'){
                            return ShowDate(record.data.tgl_transaksi);
                        } else{
                            var tgl=record.data.tgl_transaksi.split("/");
                            if(tgl[2].length == 4 && isNaN(tgl[1])){
                                return record.data.tgl_transaksi;
                            } else{
                                return ShowDate(record.data.tgl_transaksi);
                            }
                        }

                    }
                }
            },{
                header: 'Tanggal Berlaku',
                dataIndex: 'tgl_berlaku',
                width: 130,
                menuDisabled:true,
                hidden: true,
                renderer: function(v, params, record){
                    // if(record.data.tgl_berlaku == undefined || record.data.tgl_berlaku == null){
                    //  record.data.tgl_berlaku=tglGridBawah;
                    //  return record.data.tgl_berlaku;
                    // } else{
                    //  if(record.data.tgl_berlaku.substring(5, 4) == '-'){
                    //      return ShowDate(record.data.tgl_berlaku);
                    //  } else{
                    //      var tglb=record.data.tgl_berlaku.split("-");

                    //      if(tglb[2].length == 4 && isNaN(tglb[1])){
                    //          return record.data.tgl_berlaku;
                    //      } else{
                    //          return ShowDate(record.data.tgl_berlaku);
                    //      }
                    //  }

                    // }
                }
            },{
                header: 'Harga',
                align: 'right',
                hidden: false,
                menuDisabled:true,
                dataIndex: 'harga',
                width:100,
                listeners:{
                    dblclick: function(dataview, index, item, e) {
                        console.log(dsTRDetailPenJasRadList.data.items[item].data);
                        CurrentNoTransaksi_Rad = dsTRDetailPenJasRadList.data.items[item].data.no_transaksi;
                        CurrentKdKasir_Rad = dsTRDetailPenJasRadList.data.items[item].data.kd_kasir;
                        CurrentRow = item;
                        msg_box_edittarif_RAD(dsTRDetailPenJasRadList.data.items[item].data)
                        // PilihEditTarifLookUp_Rad(dsTRDetailPenJasRadList.data.items[item].data);
                    },
                },
                renderer: function(v, params, record){
                    // var hargadb = toInteger(record.data.harga);
                    // return formatCurrency(toInteger(record.data.harga));
                    // console.log(record.data.harga);
     //                console.log(record.data.HARGA);
                    return formatCurrency(record.data.harga);
                }
            },{   
				id:'qty_rad',
                header: 'Qty',
                width:91,
                align: 'right',
                menuDisabled:true,
                dataIndex: 'qty',
                editor         : new Ext.form.TextField({
                    id:'fieldcolProblemRAD',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
                        listeners:{
                            'specialkey' : function(a, b){
                                if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
                                    var lineRAD = gridDTItemTest.getSelectionModel().selection.cell[0];
                                    // var igdUrut=toInteger(dsTRDetailPenJasRadList.data.items[lineRAD].data.URUT)-1;
                                    // senderProdukIGD(
                                    //     notransaksi, 
                                    //     dsTRDetailPenJasRadList.data.items[lineRAD].data.KD_PRODUK, 
                                    //     dsTRDetailPenJasRadList.data.items[lineRAD].data.KD_TARIF, 
                                    //     CurrentKdKasir_KasirIGD, 
                                    //     igdUrut, 
                                    //     dsTRDetailPenJasRadList.data.items[lineRAD].data.KD_UNIT, 
                                    //     tglGridBawah,
                                    //     dsTRDetailPenJasRadList.data.items[lineRAD].data.TGL_BERLAKU,
                                    //     Ext.get('fieldcolProblemRAD').dom.value,
                                    //     dsTRDetailKasirIGDList.data.items[lineRAD].data.HARGA,
                                    //     false
                                    // );
                                    TambahBarisRAD();
                                    
                                    gridDTItemTest.startEditing(lineRAD+1, 5);
                                    loadMask.show();
                                    Datasave_PenJasRad_LangsungTambahBaris(false); 
                                    loadMask.hide();
                                }
                            },
                        }
                    }
                   /*  {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                Dataupdate_PenJasRad(false);
                            }
                        }
                    } */
                )
            },
			// {
                // header: 'Dokter',
                // width:91,
                // menuDisabled:true,
                // dataIndex: 'jumlah',
                // hidden: true,

            // }, 
			{
                header: 'Pelaksana',
                width:150,
                menuDisabled:true,
                dataIndex: 'jumlah_dokter',
                hidden: false,

            },
            {
                header: 'Catatan',
                width:150,
                menuDisabled:true,
                dataIndex: 'catatan',
                hidden: false,

            }
        ]
    )
}
/* function TRPenJasRadColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsirwj',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
                menuDisabled:true,
                hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
                menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiRWJ',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320
                
            }
            ,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
               menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colHARGARWj',
                header: 'Harga',
                align: 'right',
                hidden: true,
                menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
                renderer: function(v, params, record) 
                            {
                            return formatCurrency(record.data.HARGA);
                            
                            }   
            },
            {
                id: 'colProblemRWJ',
                header: 'Qty',
                width:91,
                align: 'right',
                menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
                        listeners:
                            { 
                                'specialkey' : function()
                                {
                                    
                                            Dataupdate_PenJasRad(false);

                                }
                            }
                    }
                ),
                
                
              
            },

            {
                id: 'colImpactRWJ',
                header: 'CR',
                width:80,
                dataIndex: 'IMPACT',
                hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactRWJ',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
                
            }

        ]
    )
}; */

// ------------------------Update Tarif Harga Produk-----------------------------
    function msg_box_edittarif_RAD(data)
    {
        var lebar = 250;
        form_msg_box_edittarif_RAD = new Ext.Window
            ({
                id: 'edittarifRAD',
                title: 'Edit Tarif',
                closeAction: 'destroy',
                width: lebar,
                height: 100,
                border: false,
                resizable: false,
                plain: true,
                layout: 'fit',
                iconCls: 'Request',
                modal: true,
                items:
                    {
                        columnWidth: 250,
                        layout: 'form',
                        labelWidth: 1,
                        border: false,
                        items:
                        [
                            {
                                xtype: 'tbspacer',
                                height: 4
                            },
                            {
                                xtype: 'textfield',
                                name: 'txtPasswordRAD',
                                id: 'txtPasswordRAD',
                                emptyText: 'Masukan Password',
                                anchor: '99%',
                            },
                            {
                                layout: 'hBox',
                                border: false,
                                bodyStyle: 'padding:5px 0px 20px 20px',
                                defaults: {margins: '3 3 1 1'},
                                anchor: '95%',
                                layoutConfig:
                                {
                                    align: 'middle',
                                    pack: 'end'
                                },
                                items:
                                [
                                    {
                                        xtype: 'button',
                                        text: 'Buka',
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnOkedittarifLAb',
                                        handler: function ()
                                        {
                                            if (Ext.getCmp('txtPasswordRAD').getValue().trim()==='' || Ext.getCmp('txtPasswordRAD').getValue().trim==='Masukan Password')
                                            {
                                                ShowPesanWarningPenJasRad("Password Salah!", "Warning");
                                            }else{
                                                Ext.Ajax.request({
                                                    url: baseURL + "index.php/rad/control_data_tarif_component/bukaedittarif",
                                                    params: {
                                                        password   : Ext.getCmp('txtPasswordRAD').getValue(),
                                                    },
                                                    success: function (o)
                                                    {
                                                        var cst = Ext.decode(o.responseText);
                                                        if (cst.success === true)
                                                        {
                                                            PilihEditTarifLookUp_Rad(data);
                                                        } else {
                                                            ShowPesanWarningPenJasRad("Password Salah!", "Warning");
                                                        };
                                                    }
                                                })
                                                form_msg_box_edittarif_RAD.close();
                                            }
                                        }
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Batal',
                                        width: 70,
                                        hideLabel: true,
                                        id: 'btnCanceledittarifRAD',
                                        handler: function ()
                                        {
                                            form_msg_box_edittarif_RAD.close();
                                        }
                                    }
                                ]
                            }

                        ]
                    }
                });
        form_msg_box_edittarif_RAD.show();
    }

function PilihEditTarifLookUp_Rad(data) 
{
	loaddatastoreTarifComponentRad(data);
        var formLookupEditTarif_Rad = new Ext.Window
		(
			{
				id: 'formEditTarif_Rad',
				title: 'Edit data tarif',
				closeAction: 'destroy',
				width: 600,
				height: 400,
				border: false,
				resizable: false,
				plain: true,
				layout: 'fit',
				iconCls: 'Request',
				modal: true,
				bodyStyle:'padding: 3px;',
				items: [
                {
					columnWidth: .40,
					layout: 'form',
					labelWidth: 100,
					border: false,
					bodyStyle:'padding: 10px;',
					items:[
						{
							xtype 		: 'textfield',
							id 			: 'TxtNamaProduk',
							fieldLabel 	: 'Produk',
							anchor		: '100%',
							readOnly    : true,
                            value		: data.deskripsi,
                            bodyStyle	: 'margin:10px;',
						},
						GridEditTarif_Rad(),
                        // paneltotal
					]
				},
				],
				bbar:[  
					{
						text: 'Cito',
						id: 'btnHitungCito_Rad',
						tooltip: 'Hitung Cito',
						hidden:true,
						//iconCls: 'save',
						handler: function ()
						{
							PilihEditCitoLookUp_Rad(data);
						},
						callback : function(){
							Ext.getCmp('TxtPercentaseTarif').focus();
						}
					},'-',
					{
						text: 'Discount',
						id: 'btnDiscount_Rad',
						tooltip: 'Hitung Discount',
						hidden:true,
						//iconCls: 'save',
						handler: function ()
						{
							//savePostingManualKasirRWI();
						}
					},'-',
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalTarif',
						width       : 100,
						readOnly    : true,
					},
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalMarkup',
						hidden:true,
						width       : 100,
						readOnly    : true,
					},
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalDisk',
						hidden:true,
						width       : 100,
						readOnly    : true,
					},
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalTarif_Baru',
						width       : 100,
						readOnly    : true,
					},
				],
                tbar:[
                    {
                        text: 'Simpan',
                        id: 'btnSimpanEditTarif_Rad',
                        tooltip: 'Edit Tarif',
                        iconCls: 'save',
                        handler: function ()
                        { 
							Ext.Ajax.request({
								url 	: baseURL + "index.php/rad/control_data_tarif_component/update_tarif",
								params 	: listDataParams_EditTarif_Rad(0, false, false),
								success : function (o)
								{
									var cst = Ext.decode(o.responseText);
									if (cst.status === true) {
										ShowPesanInfoPenJasRad('Tarif berhasil diperbarui', 'Informasi');
										dsTRDetailPenJasRadList.getRange()[CurrentRow].set('harga', cst.total_harga);
										var kuantiti=0;
										kuantiti=dsTRDetailPenJasRadList.getRange()[CurrentRow].get('qty');
										// dsTRDetailPenJasRadList.getRange()[CurrentRow].set('total', cst.total_harga * kuantiti);
									}else{
										ShowPesanWarningPenJasRad('Tarif tidak berhasil diperbarui', 'Peringatan');
									}
								}
							});
                        }
					},{
						text: 'Reset',
						id: 'btnEditTarif_Rad',
						iconCls: 'cancel',
						handler: function ()
						{
							dsDataStoreGridTarifKomponent.removeAll();
							loaddatastoreTarifComponentRad(data);
						}
					},        
                ],
				listeners:
				{
				
				}
            }
		);
        formLookupEditTarif_Rad.show();
};

function listDataParams_EditTarif_Rad(persentase, mark, disc){
		//console.log(persentase+" "+mark+" "+disc);
		var data_grid = gridEditTarifRad.getSelectionModel().grid.store.data;
		var list      = "";
		// console.log(data_grid);
		// console.log(Ext.getCmp('gridEditTarifKasirrwi').getCount());
		for(var x=0; x<data_grid.length; x++){
			var result = data_grid.items[x].data;
			var y      = '';
			var z      = '@@##$$@@';

			var tmpMarkup 	= result.MARKUP;
			var hasilMarkup = result.TARIF;
			var tmpDisc  	= result.DISC;
			var hasilDisc  	= result.TARIF;
			var Totaltarif  = result.TARIF;
			var hasilTarifLama  = result.TARIF;
			var hasilTarifBaru  = result.TARIF_BARU;

			if (persentase > 0 && mark === true) {
				hasilMarkup = (tmpMarkup / 100) * persentase;
				if (result.MARKUP > 0) {
					hasilMarkup = hasilMarkup + tmpMarkup;
				}
			}else{
				hasilMarkup	= result.MARKUP;
			}

			if (persentase > 0 && disc === true) {
				hasilDisc 	= tmpDisc / 100 * persentase;
				if (result.MARKUP > 0) {
					hasilDisc 	= hasilDisc + tmpDisc;
				}
			}else{
				hasilDisc 	= result.DISC;
			}

			y   = result.KD_COMPONENT
			y   += z + hasilMarkup
			y   += z + hasilDisc
			y   += z + hasilTarifLama
			y   += z + hasilTarifBaru


			if (x === (data_grid.length - 1))
			{
				list += y
			} else
			{
				list += y + '##[[]]##'
			};
		}
		//return list;
		
		var params =
		{
			no_transaksi: CurrentNoTransaksi_Rad,
			list 		: list,
			urut 		: result.URUT,
			kd_kasir	: CurrentKdKasir_Rad,
			total_tarif	: Ext.getCmp('txtFieldTotalTarif_Baru').getValue(),
			tarif_sebelumnya: Ext.getCmp('txtFieldTotalTarif').getValue(),
		};
		return params
	}

function GridEditTarif_Rad(){
    	var rowLine = 0;
    	var rowData = "";
		dsDataStoreGridTarifKomponent.removeAll();        
	    var Field = ['TARIF','DISC','MARKUP'];
	    DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
    	//loaddatastoreProdukComponent();

		var cm = new Ext.grid.ColumnModel({
			// specify any defaults for each column
			defaults: {
				sortable: false // columns are not sortable by default           
			},
			columns: [
			{
				header 		: 'Kd Kasir',
				dataIndex 	: 'KD_KASIR',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'No Transaksi',
				dataIndex 	: 'NO_TRANSAKSI',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Urut',
				dataIndex 	: 'URUT',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Tgl Transaksi',
				dataIndex 	: 'TGL_TRANSAKSI',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Kd Component',
				dataIndex 	: 'KD_COMPONENT',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header: 'Component',
				dataIndex: 'COMPONENT',
				width: 200
			}, 
			{
				header: 'Tarif Lama',
				dataIndex: 'TARIF',
                align: 'right',
				width: 75,
                renderer: function (v, params, record)
				{
					return formatCurrency(record.data.TARIF);
				},
			}, 
			{
				header: 'Markup',
				dataIndex: 'MARKUP',
				hidden:true,
				width: 75,
				editor: new Ext.form.NumberField
				(
					{
						id: 'fieldcolMarkRad',
						allowBlank: true,
						enableKeyEvents: true,
						width: 30,
						enableKeyEvents : true,
						listeners : {
							specialkey : function(a, b, c){
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
									var tmpTotalPerhitungan = gridEditTarifRad.getStore().getRange()[rowLine].get('TARIF');
									tmpTotalPerhitungan = (tmpTotalPerhitungan+Ext.getCmp('fieldcolMarkRad').getValue())- gridEditTarifRad.getStore().getRange()[rowLine].get('DISC');
									gridEditTarifRad.getStore().getRange()[rowLine].set('TARIF_BARU', tmpTotalPerhitungan);
									gridEditTarifRad.getStore().getRange()[rowLine].set('MARKUP', Ext.getCmp('fieldcolMarkRad').getValue());

									var totalMarkup = 0;
									var totalDisc 	= 0;
									var totalTarif_Baru= 0;
					                for(var i = 0 ; i < dsDataStoreGridTarifKomponent.getCount();i++)
									{
										totalMarkup 	+= gridEditTarifRad.getStore().getRange()[i].get('MARKUP');
										totalDisc 		+= gridEditTarifRad.getStore().getRange()[i].get('DISC');
										totalTarif_Baru += gridEditTarifRad.getStore().getRange()[i].get('TARIF_BARU');
									}
									Ext.getCmp('txtFieldTotalMarkup').setValue(totalMarkup);
									Ext.getCmp('txtFieldTotalDisk').setValue(totalDisc);
									Ext.getCmp('txtFieldTotalTarif_Baru').setValue(totalTarif_Baru);
								}
							}
						},
					}
				),
			}, 
			{
				header: 'Discount',
				dataIndex: 'DISC',
				hidden:true,
				width: 75,
				editor: new Ext.form.NumberField
				(
					{
						id: 'fieldcolDiscRad',
						allowBlank: true,
						enableKeyEvents: true,
						width: 30,
						enableKeyEvents : true,
						listeners : {
							specialkey : function(a, b, c){
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
									var tmpTotalPerhitungan = gridEditTarifRad.getStore().getRange()[rowLine].get('TARIF');
									tmpTotalPerhitungan = (tmpTotalPerhitungan+gridEditTarifRad.getStore().getRange()[rowLine].get('MARKUP'))- Ext.getCmp('fieldcolDiscRad').getValue();
									gridEditTarifRad.getStore().getRange()[rowLine].set('TARIF_BARU', tmpTotalPerhitungan);
									gridEditTarifRad.getStore().getRange()[rowLine].set('DISC', Ext.getCmp('fieldcolDiscRad').getValue());

									var totalMarkup = 0;
									var totalDisc 	= 0;
									var totalTarif_Baru= 0;
					                for(var i = 0 ; i < dsDataStoreGridTarifKomponent.getCount();i++)
									{
										totalMarkup 	+= gridEditTarifRad.getStore().getRange()[i].get('MARKUP');
										totalDisc 		+= gridEditTarifRad.getStore().getRange()[i].get('DISC');
										totalTarif_Baru += gridEditTarifRad.getStore().getRange()[i].get('TARIF_BARU');
									}
									Ext.getCmp('txtFieldTotalMarkup').setValue(totalMarkup);
									Ext.getCmp('txtFieldTotalDisk').setValue(totalDisc);
									Ext.getCmp('txtFieldTotalTarif_Baru').setValue(totalTarif_Baru);
								}
							}
						},
					}
				),
			}, 
            {
				id: 'fieldcolTarifBaruKasirRWJ',
	            header: 'Tarif Baru',
	            dataIndex: 'TARIF_BARU',
                align: 'right',
	            width: 75,
				editor: new Ext.form.NumberField
				(
					{
						id: 'fieldcolTarifBaruKasirRWJ2',
						allowBlank: true,
						enableKeyEvents: true,
						width: 30,
						enableKeyEvents : true,
						listeners : {
							specialkey : function(a, b, c){
								console.log(a);
								console.log(b);
								console.log(c);
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
									var tmpTotalPerhitungan = Ext.getCmp('fieldcolTarifBaruKasirRWJ2').getValue();
									gridEditTarifRad.getStore().getRange()[rowLine].set('TARIF_BARU', tmpTotalPerhitungan);
									// gridEditTarifRad.getStore().getRange()[rowLine].set('DISC', Ext.getCmp('fieldcolDiscRad').getValue());

									// var totalMarkup = 0;
									// var totalDisc 	= 0;
									var totalTarif_Baru= 0;
					                for(var i = 0 ; i < dsDataStoreGridTarifKomponent.getCount();i++)
									{
										// totalMarkup 	+= gridEditTarifRad.getStore().getRange()[i].get('MARKUP');
										// totalDisc 		+= gridEditTarifRad.getStore().getRange()[i].get('DISC');
										totalTarif_Baru += gridEditTarifRad.getStore().getRange()[i].get('TARIF_BARU');
									}
									console.log(totalTarif_Baru);
									// Ext.getCmp('txtFieldTotalMarkup').setValue(totalMarkup);
									// Ext.getCmp('txtFieldTotalDisk').setValue(totalDisc);
									Ext.getCmp('txtFieldTotalTarif_Baru').setValue(totalTarif_Baru);
								}
							}
						},
					}
				),
	        }]
	    });

        gridEditTarifRad = new Ext.grid.EditorGridPanel
        (
            {
				title: '',
				id: 'gridEditTarifRad',
				store: dsDataStoreGridTarifKomponent,
				clicksToEdit: 1, 
				editable: true,
				border: true,
				columnLines: true,
				frame: false,
				stripeRows       : true,
				trackMouseOver   : true,
				height: 250,
				width: 560,
				autoScroll: true,
				sm: new Ext.grid.CellSelectionModel
				(
					{
						singleSelect: true,
						listeners:
						{
						}
					}
				),
				sm: new Ext.grid.RowSelectionModel
				(
					{
						singleSelect: true,
						listeners:
						{
							rowselect: function (sm, row, rec)
							{
								rowLine = row;
							}
						}
					}
				),
                cm: cm,
                viewConfig: {
                    forceFit: true
                },  
            }
        );
        return gridEditTarifRad;
    };

function loaddatastoreTarifComponentRad(params){
    	// var rowpasien=rowSelectedKasirrwjKasir.data;
        Ext.Ajax.request({
            url: baseURL +  "index.php/rad/control_data_tarif_component/get_data_component",
            params: {
                // no_transaksi 	: rowpasien.NO_TRANSAKSI,
                no_transaksi 	: params.no_transaksi,
                kd_produk 		: params.kd_produk,
                kd_unit 		: params.kd_unit,
                kd_tarif 		: params.kd_tarif,
                tgl_berlaku 	: params.tgl_berlaku,
                tgl_transaksi 	: params.tgl_transaksi,
                urut 			: params.urut,
                kd_kasir 		: params.kd_kasir,
            },
            success: function(response) {
                //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
                var cst  = Ext.decode(response.responseText);
				var totalTarif 	= 0;
				var totalMarkup = 0;
				var totalDisc 	= 0;
				var totalTarif_Baru= 0;
                //dsDataPerawatPenindak_KASIR_RWI.load(myData);
                for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                    var recs    = [],recType = DataStoreGridKomponent.recordType;
                    var o       = cst['data'][i];
                    recs.push(new recType(o));
                    dsDataStoreGridTarifKomponent.add(recs);
                }
                for(var i = 0 ; i < dsDataStoreGridTarifKomponent.getCount();i++)
				{
					totalTarif 	+= dsDataStoreGridTarifKomponent.data.items[i].data.TARIF;
					totalMarkup += dsDataStoreGridTarifKomponent.data.items[i].data.MARKUP;
					totalDisc 	+= dsDataStoreGridTarifKomponent.data.items[i].data.DISC;
					totalTarif_Baru 	+= dsDataStoreGridTarifKomponent.data.items[i].data.TARIF_BARU;
				}
				Ext.getCmp('txtFieldTotalTarif').setValue(totalTarif);
				Ext.getCmp('txtFieldTotalMarkup').setValue(totalMarkup);
				Ext.getCmp('txtFieldTotalDisk').setValue(totalDisc);
				Ext.getCmp('txtFieldTotalTarif_Baru').setValue(totalTarif_Baru);
            },
        });
    }
// ------------------------------------------------------------------------------


function RecordBaruRAD()
{
    var tgltranstoday = Ext.getCmp('dtpKunjunganL').getValue();
    var p = new mRecordRad
    (
        {
            'cito':'Tidak',
            'DESKRIPSI2':'',
            'KD_PRODUK':'',
            'DESKRIPSI':'', 
            'KD_TARIF':'', 
            'HARGA':'',
            'QTY':'',
            'TGL_TRANSAKSI':tgltranstoday.format('Y/m/d'), 
            'DESC_REQ':'',
            'KD_TARIF':'',
            'URUT':''
        }
    );
    
    return p;
};
var kd_kasir_rad;
//----------MEMASUKAN DATA YG DIPILIH DARI DATA GRID KEDALAM LOOKUP EDIT DATA PASIEN
function TRPenjasRadInit(rowdata)
{
    console.log('cek');
    console.log(rowdata);
    var urutmasuk;
    // console.log(rowdata);
    if (rowdata.URUT != '') {
        urutmasuk = rowdata.URUT;
    }else{
        urutmasuk = rowdata.URUT_MASUK
    }
    tmpurut = urutmasuk;
    AddNewPenJasRad = false;
    
    if(Ext.get('cboJenisTr_viPenJasRad').getValue()=='Transaksi Lama'){
        Ext.get('txtNoTransaksiPenJasRad').dom.value = rowdata.NO_TRANSAKSI;
    }else if(Ext.get('cboJenisTr_viPenJasRad').getValue()=='Transaksi Baru' && rowdata.ORDER_MNG == '1'){
        Ext.get('txtNoTransaksiPenJasRad').dom.value = rowdata.NO_TRANSAKSI;
    }
    // console.log(rowdata);
    TmpNotransaksi=rowdata.NO_TRANSAKSI;
    KdKasirAsal=rowdata.KD_KASIR;
	currentKdKasirRAD=rowdata.KD_KASIR;
    TglTransaksi=rowdata.TGL_TRANSAKSI;
    tglTransaksiAwalPembandingTransferRAD=rowdata.TGL_TRANSAKSI;
    Kd_Spesial=rowdata.KD_SPESIAL;
    No_Kamar=rowdata.KAMAR;
    kodeunit=rowdata.KD_UNIT;
    kodeunittransfer=rowdata.KD_UNIT;
    if (combovalues==='Transaksi Lama' )
    {
        ViewGridBawahLookupPenjasRad(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
        Ext.getCmp('btnPembayaranPenjasRAD').enable();
        Ext.getCmp('btnTutupTransaksiPenjasRAD').enable();
        Ext.getCmp('btnHpsBrsKasirRAD').enable();
        RefreshDatahistoribayar(rowdata.NO_TRANSAKSI);
        kodeUnitRad=rowdata.KD_UNIT_ASAL;
        console.log(rowdata.LUNAS);
        if (rowdata.LUNAS==='1'){
            Ext.getCmp('btnPembayaranPenjasRAD').enable();
            Ext.getCmp('btnSimpanPenjasRAD').disable();
            Ext.getCmp('btnTutupTransaksiPenjasRAD').disable();
        }
        if (rowdata.CO_STATUS==='1'){
               
            Ext.getCmp('btnTambahBarisPenJasRad').disable();
            Ext.getCmp('btnLookupPenJasRad').disable();
            Ext.getCmp('btnHpsBrsItemRad').disable();
            Ext.getCmp('btnpemakaianfilmRadiologiPenjasRAD').disable();
            Ext.getCmp('btnDocumentPenjasRAD').disable();
            Ext.getCmp('btnLookUpEditDokterPenindak_rad').enable();
        }else{
            Ext.getCmp('btnTambahBarisPenJasRad').enable();
            Ext.getCmp('btnLookupPenJasRad').enable();
            Ext.getCmp('btnHpsBrsItemRad').enable();
            Ext.getCmp('btnpemakaianfilmRadiologiPenjasRAD').enable();
			Ext.getCmp('btnDocumentPenjasRAD').enable();
            Ext.getCmp('btnLookUpEditDokterPenindak_rad').enable();
        }
    }
    else
    {
        Ext.getCmp('btnPembayaranPenjasRAD').disable();
        Ext.getCmp('btnHpsBrsKasirRAD').disable();
        Ext.getCmp('btnTutupTransaksiPenjasRAD').disable();
        kodeUnitRad=rowdata.KD_UNIT;

        if(order_rad == true && rowdata.ORDER_MNG == '1'){
            ViewGridBawahLookupPenjasRad(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
            Ext.getCmp('btnPembayaranPenjasRAD').enable();
            Ext.getCmp('btnTutupTransaksiPenjasRAD').enable();
            Ext.getCmp('btnHpsBrsKasirRAD').enable();
            RefreshDatahistoribayar(rowdata.NO_TRANSAKSI);
            kodeUnitRad=rowdata.KD_UNIT_ASAL;
        }
    }
    var modul='';
    if(radiovalues == 1){
        modul='igd';
    } else if(radiovalues == 2){
        modul='rwi';
    } else if(radiovalues == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/cekPembayaran",
        params: {
            notrans: rowdata.NO_TRANSAKSI,
            Modul:modul,
            KdKasir: rowdata.KD_KASIR
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            console.log(cst.ListDataObj);
            kodepasien = rowdata.KD_PASIEN;
            namapasien = rowdata.NAMA;
            // console.log(rowdata.KD_UNIT);
            //kodeUnitRad = rowdata.KD_UNIT;
            namaunit = rowdata.NAMA_UNIT;
            kodepay = cst.ListDataObj[0].kd_pay;
            kdkasir= cst.ListDataObj[0].kd_kasir;
            uraianpay = cst.ListDataObj[0].cara_bayar;
            kdcustomeraa = rowdata.KD_CUSTOMER;
            //loaddatastorePembayaran(cst.ListDataObj[0].jenis_pay);
            /* notransaksiasal_rad = cst.ListDataObj[0].no_transaksi_asal;
            kdkasirasal_rad = cst.ListDataObj[0].kd_kasir_asal;
            Ext.get('cboPembayaran').dom.value =cst.ListDataObj[0].ket_payment;
            Ext.get('cboJenisByr').dom.value =cst.ListDataObj[0].cara_bayar; 
            vflag = cst.ListDataObj[0].flag;
            tapungkd_pay = cst.ListDataObj[0].kd_pay;
            vkode_customerRAD =  cst.ListDataObj[0].kd_customer; */
            
        }

    });
    
    Ext.get('dtpTtlL').dom.value=ShowDate(rowdata.TGL_LAHIR);
    Ext.get('txtNoMedrecL').dom.value= rowdata.KD_PASIEN;
    Ext.get('txtNamaPasienL').dom.value = rowdata.NAMA;
    Ext.get('txtAlamatL').dom.value = rowdata.ALAMAT;
    Ext.get('txtKdDokter').dom.value   = rowdata.KD_DOKTER;
    
    // if (rowdata.NAMA_UNIT_ASLI==='')
    // {
    //  Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT;
    // }
    // else
    // {
    //  Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT_ASLI;
    // }
    
    Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
    // console.log(rowdata);
    // console.log(rowdata.KD_UNIT);
    //kodeUnitRad=rowdata.KD_UNIT;
            
    Ext.get('cboJK').dom.value = rowdata.JENIS_KELAMIN;
    if (rowdata.JENIS_KELAMIN == '1' || rowdata.JENIS_KELAMIN == 1)
    {
        Ext.get('cboJK').dom.value= 'Laki-laki';
    }else
    {
        Ext.get('cboJK').dom.value= 'Perempuan'
    }
    Ext.getCmp('cboJK').disable();
    
    Ext.get('cboGDR').dom.value = rowdata.GOL_DARAH;
    if (rowdata.GOL_DARAH === '0')
    {
        Ext.get('cboGDR').dom.value= '-';
    }else if (rowdata.GOL_DARAH === '1')
    {
        Ext.get('cboGDR').dom.value= 'A+'
    }else if (rowdata.GOL_DARAH === '2')
    {
        Ext.get('cboGDR').dom.value= 'B+'
    }else if (rowdata.GOL_DARAH === '3')
    {
        Ext.get('cboGDR').dom.value= 'AB+'
    }else if (rowdata.GOL_DARAH === '4')
    {
        Ext.get('cboGDR').dom.value= 'O+'
    }else if (rowdata.GOL_DARAH === '5')
    {
        Ext.get('cboGDR').dom.value= 'A-'
    }else if (rowdata.GOL_DARAH === '6')
    {
        Ext.get('cboGDR').dom.value= 'B-'
    }
    Ext.getCmp('cboGDR').disable();
    
    //alert(rowdata.KD_CUSTOMER);
    Ext.get('txtKdCustomerLamaHide').dom.value= rowdata.KD_CUSTOMER;//tampung kd customer untuk transaksi selain kunjungan langsung
    vkode_customerRAD = rowdata.KD_CUSTOMER;
    //Ext.getCmp('cboKelPasienRad').disable();
    
    Ext.get('cboKelPasienRad').dom.value= rowdata.KELPASIEN;
    
    
    if(tmppasienbarulama == 'Baru'){
        console.log(rowdata);
        Ext.getCmp('txtDokterPengirimL').setValue(rowdata.DOKTER);
        Ext.getCmp('cboDOKTER_viPenJasRad').setValue('');
        tmpkddoktertujuan = rowdata.KD_DOKTER_RAD;
        // Ext.getCmp('cboDOKTER_viPenJasRad').setValue(rowdata.DOKTER_RAD);
        Ext.getCmp('cboUnitRad_viPenJasRad').setValue('');
        Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT_ASLI;
        tmpkd_unit_asal = rowdata.KD_UNIT;
        Ext.get('txtKdUnitRad').dom.value   = rowdata.KD_UNIT;
        kd_kasir_rad = '';
        Ext.getCmp('cboUnitRad_viPenJasRad').enable();
        Ext.getCmp('cboDOKTER_viPenJasRad').enable();

        Ext.getCmp('cboKelPasienRad').enable();
        Ext.get('txtCustomerLamaHide').hide();
         if(rowdata.KELPASIEN == 'Perseorangan'){
            Ext.getCmp('cboPerseoranganRad').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboPerseoranganRad').show();
            Ext.getCmp('cboPerseoranganRad').enable();
        } else if(rowdata.KELPASIEN == 'Perusahaan'){
            Ext.getCmp('cboPerusahaanRequestEntryRad').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboPerusahaanRequestEntryRad').show();
            Ext.getCmp('cboPerusahaanRequestEntryRad').enable();
        } else{
            Ext.getCmp('cboAsuransiRad').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboAsuransiRad').show();
            Ext.getCmp('cboAsuransiRad').enable();
        } 

        if(order_rad == true && rowdata.ORDER_MNG == '1'){
            Ext.getCmp('txtDokterPengirimL').setValue(rowdata.DOKTER_ASAL);
            Ext.getCmp('cboDOKTER_viPenJasRad').setValue('');
            Ext.getCmp('cboUnitRad_viPenJasRad').setValue(rowdata.NAMA_UNIT_ASLI);
            Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT_ASAL;
            tmpkd_unit_asal = rowdata.KD_UNIT;
            tmpkddoktertujuan = rowdata.KD_DOKTER;
            Ext.get('txtKdUnitRad').dom.value   = rowdata.KD_UNIT_ASAL;
            kd_kasir_rad = rowdata.KD_KASIR;
            Ext.getCmp('cboUnitRad_viPenJasRad').disable();
            // Ext.getCmp('cboDOKTER_viPenJasRad').disable();

            Ext.getCmp('cboKelPasienRad').disable();
            Ext.get('txtCustomerLamaHide').hide();
            if(rowdata.KELPASIEN == 'Perseorangan'){
                Ext.getCmp('cboPerseoranganRad').setValue(rowdata.CUSTOMER);
                Ext.getCmp('cboPerseoranganRad').show();
                Ext.getCmp('cboPerseoranganRad').disable();
            } else if(rowdata.KELPASIEN == 'Perusahaan'){
                Ext.getCmp('cboPerusahaanRequestEntryRad').setValue(rowdata.CUSTOMER);
                Ext.getCmp('cboPerusahaanRequestEntryRad').show();
                Ext.getCmp('cboPerusahaanRequestEntryRad').disable();
            } else{
                Ext.getCmp('cboAsuransiRad').setValue(rowdata.CUSTOMER);
                Ext.getCmp('cboAsuransiRad').show();
                Ext.getCmp('cboAsuransiRad').disable();
            }
        }
    }else{
        Ext.getCmp('txtDokterPengirimL').setValue(rowdata.DOKTER_ASAL);
        // Ext.getCmp('cboDOKTER_viPenJasRad').setValue('');
        Ext.getCmp('cboDOKTER_viPenJasRad').setValue(rowdata.DOKTER);
        Ext.getCmp('cboUnitRad_viPenJasRad').setValue(rowdata.NAMA_UNIT_ASLI);
        Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT_ASAL;
        tmpkd_unit_asal = rowdata.KD_UNIT;
        tmpkddoktertujuan = rowdata.KD_DOKTER;
        Ext.get('txtKdUnitRad').dom.value   = rowdata.KD_UNIT_ASAL;
        kd_kasir_rad = rowdata.KD_KASIR;
        Ext.getCmp('cboUnitRad_viPenJasRad').disable();
        // Ext.getCmp('cboDOKTER_viPenJasRad').disable();

        Ext.getCmp('cboKelPasienRad').disable();
        Ext.get('txtCustomerLamaHide').hide();
        if(rowdata.KELPASIEN == 'Perseorangan'){
            Ext.getCmp('cboPerseoranganRad').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboPerseoranganRad').show();
            Ext.getCmp('cboPerseoranganRad').disable();
        } else if(rowdata.KELPASIEN == 'Perusahaan'){
            Ext.getCmp('cboPerusahaanRequestEntryRad').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboPerusahaanRequestEntryRad').show();
            Ext.getCmp('cboPerusahaanRequestEntryRad').disable();
        } else{
            Ext.getCmp('cboAsuransiRad').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboAsuransiRad').show();
            Ext.getCmp('cboAsuransiRad').disable();
        }
    }
    // getDokterPengirim(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
    Ext.getCmp('txtnotlprad').setValue(rowdata.HP);
    // Ext.getCmp('txtnoseprad').setValue(rowdata.SJP);
    
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/getcurrentshift",
         params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
             var cst = Ext.decode(o.responseText);
            
        },      
        success: function(o) {
            tampungshiftsekarang=o.responseText
            
        }
        
    
    });
    
    //---------------------EDIT DEFAULT UNIT RAD 31 01 2017
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/getUnitDefault",
        params: {
            notrans: rowdata.NO_TRANSAKSI,
            Modul:modul
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            Ext.getCmp('cboUnitRad_viPenJasRad').setValue(cst.kd_unit);
            getComboDokterRad();
            //Ext.getCmp('cbounitrads_viPenJasRad').setValue(cst.kd_unit);
            //combovaluesunittujuan=cst.kd_unit;
            tmpkd_unit = cst.kd_unit;
            // console.log(rowSelectedPenJasRad)
            var TrPenJasRad_kd_unit = tmpkd_unit_asal;
            if (rowSelectedPenJasRad.data.KD_UNIT_ASAL != undefined) {
                TrPenJasRad_kd_unit = rowSelectedPenJasRad.data.KD_UNIT_ASAL;
            }


            Ext.Ajax.request({
                url: baseURL + "index.php/main/functionRAD/getTarifMir",
                params: {
                    kd_unit:TrPenJasRad_kd_unit,
                    //kd_unit:tmpkd_unit_asal,
                    //kd_unit_asal:tmpkd_unit_asal,
                    kd_customer:vkode_customerRAD,
                    penjas:modul,
                    kdunittujuan:cst.kd_unit
                },
                failure: function (o)
                {
                    var cst = Ext.decode(o.responseText);
                },
                success: function (o) {
                    var cst = Ext.decode(o.responseText);
                    // console.log(cst.ListDataObj);
                    tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                }

            });
        }

    });
    //,,
    //alert(tampungshiftsekarang);
    setUsia(ShowDate(rowdata.TGL_LAHIR));
    
};


function PenjasRadAddNew() 
{
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
        params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function(o) {
            tampungshiftsekarang=o.responseText;
        }
    });
    //alert('hai');
    AddNewPenJasRad = true;
    Ext.get('txtNoTransaksiPenJasRad').dom.value = '';
    Ext.get('txtNoMedrecL').dom.value='';
    Ext.get('txtNamaPasienL').dom.value = '';
    Ext.get('txtKdDokter').dom.value   = undefined;
    Ext.get('txtNamaUnit').dom.value   = 'Radiologi';
    Ext.getCmp('txtDokterPengirimL').setValue('DOKTER LUAR');
    Ext.get('txtKdUrutMasuk').dom.value = '';
    tglTransaksiAwalPembandingTransferRAD=nowTglTransaksiGrid.format('Y-m-d')+" 00:00:00";
    Ext.getCmp('btnPembayaranPenjasRAD').disable();
    //Ext.getCmp('btnHpsBrsKasirRAD').disable();
    Ext.getCmp('btnTutupTransaksiPenjasRAD').disable();
    Ext.get('cboKelPasienRad').dom.value= 'Perseorangan';
    Ext.getCmp('cboPerseoranganRad').setValue('UMUM');
    Ext.getCmp('cboPerseoranganRad').show();
    rowSelectedPenJasRad=undefined;
    dsTRDetailPenJasRadList.removeAll();
    vkode_customerRAD='0000000001';
    databaru = 1;
    Ext.get('txtNamaUnit').readOnly;
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/getDefaultUnit",
        params: {
           text:''
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            Ext.getCmp('cboUnitRad_viPenJasRad').setValue(cst.kd_unit);
            getComboDokterRad();
            //Ext.getCmp('cbounitrads_viPenJasRad').setValue(cst.kd_unit);
            //combovaluesunittujuan=cst.kd_unit;
            tmpkd_unit = cst.kd_unit;
            
            Ext.Ajax.request({
                url: baseURL + "index.php/main/functionRAD/getTarifMir",
                params: {
                    kd_unit:cst.kd_unit,
                     //kd_unit_asal:tmpkd_unit_asal,
                     kd_customer:vkode_customerRAD,
                     penjas:'langsung',
                     kdunittujuan:cst.kd_unit
                },
                failure: function (o)
                {
                    var cst = Ext.decode(o.responseText);
                },
                success: function (o) {
                    var cst = Ext.decode(o.responseText);
                    // console.log(cst.ListDataObj);
                    tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                }

            });
        }

    });
    
    
    

};

function RefreshDataPenJasRadDetail(no_transaksi) 
{
    var strKriteriaRAD='';
    //strKriteriaRAD = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" + " And kd_unit ='4'";
    //strKriteriaRAD = 'no_transaksi = ~0000004~';
   
    dsTRDetailPenJasRadList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target: 'ViewDetailRWJGridBawah',
                param: strKriteriaRAD
            }
        }
    );
    return dsTRDetailPenJasRadList;
};

//tampil grid bawah penjas rad
function ViewGridBawahLookupPenjasRad(no_transaksi,kd_kasir) {
    var strKriteriaRAD='';
    if (kd_kasir !== undefined) {
        strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
    }else{
        strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
    }
    // strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" ;//+ " And kd_kasir ='03'";
    dsTRDetailPenJasRadList.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'tgl_transaksi',
			//Sort: 'tgl_transaksi',
			Sortdir: 'ASC',
			target: 'ViewDetailGridBawahPenJasRad',
			param: strKriteriaRAD
		}
	});
    return dsTRDetailPenJasRadList;
};

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiRAD() 
{
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/functionRAD/getCurrentShiftRad",
        params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function(o) {
            tampungshiftsekarang=o.responseText;
        }
    });
    var KdCust='';
    var TmpCustoLama='';
    var pasienBaru;
    var modul='';
    if(radiovalues == 1){
        modul='igd';
    } else if(radiovalues == 2){
        modul='rwi';
    } else if(radiovalues == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    /* if(Ext.get('cboKelPasienRad').getValue()=='Perseorangan'){
        KdCust=Ext.getCmp('cboPerseoranganRad').getValue();
    }else if(Ext.get('cboKelPasienRad').getValue()=='Perusahaan'){
        KdCust=Ext.getCmp('cboPerusahaanRequestEntryRad').getValue();
    }else {
        KdCust=Ext.getCmp('cboAsuransiRad').getValue();
    } */
    
    if(Ext.getCmp('txtNoMedrecL').getValue() == ''){
        pasienBaru=1;
    } else{
        pasienBaru=0;
    }

    console.log(tmpkd_unit);
    
    var params =
    {
        //Table:'ViewDetailTransaksiPenJasRad',
        
        KdTransaksi: Ext.get('txtNoTransaksiPenJasRad').getValue(),
        KdPasien:Ext.getCmp('txtNoMedrecL').getValue(),
        NmPasien:Ext.getCmp('txtNamaPasienL').getValue(),
        Ttl:Ext.getCmp('dtpTtlL').getValue(),
        Alamat:Ext.getCmp('txtAlamatL').getValue(),
        JK:Ext.getCmp('cboJK').getValue(),
        GolDarah:Ext.getCmp('cboGDR').getValue(),
        KdUnit: Ext.getCmp('txtKdUnitRad').getValue(),
        KdDokter:tmpkddoktertujuan,
        KdUnitTujuan:tmpkd_unit,
        Tgl: Ext.getCmp('dtpKunjunganL').getValue(),
        TglTransaksiAsal:TglTransaksi,
        urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
        KdCusto:vkode_customerRAD,
        TmpCustoLama:vkode_customerRAD,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
        NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiRad').getValue(),
        NoAskes:Ext.getCmp('txtNoAskesRad').getValue(),
        NoSJP:Ext.getCmp('txtNoSJPRad').getValue(),
        Shift:tampungshiftsekarang,
        List:getArrPenJasRad(),
        JmlField: mRecordRad.prototype.fields.length-4,
        JmlList:GetListCountDetailTransaksiRAD(),
        dtBaru:databaru,
        Hapus:1,
        Ubah:0,
        unit:52,
        TmpNotransaksi:TmpNotransaksi,
        KdKasirAsal:KdKasirAsal,
        KdSpesial:Kd_Spesial,
        Kamar:No_Kamar,
        pasienBaru:pasienBaru,
        listTrDokter    : '',
        Modul:modul,
        KdProduk:'',//sTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.KD_PRODUK
        URUT :tmpurut,
        no_reg:Ext.getCmp('txtnoregrad').getValue()
        
    };
    return params
};

function getParamKonsultasi() 
{

    var params =
    {
        
        Table:'ViewDetailTransaksiPenJasRad', //data access listnya belum dibuat
        
        //TrKodeTranskasi: Ext.get('txtNoTransaksiPenJasRad').getValue(),
        KdUnitAsal : Ext.get('txtKdUnitRad').getValue(),
        KdDokterAsal : Ext.get('txtKdDokter').getValue(),
        KdUnit: selectKlinikPoli,
        KdDokter:selectDokter,
        KdPasien:Ext.get('txtNoMedrecL').getValue(),
        TglTransaksi : Ext.get('dtpKunjunganL').dom.value,
        KDCustomer :vkode_customerRAD,
    };
    return params
};

function GetListCountDetailTransaksiRAD(){
    var x=0;
    for(var i = 0 ; i < dsTRDetailPenJasRadList.getCount();i++){
        console.log(dsTRDetailPenJasRadList);
        if (dsTRDetailPenJasRadList.data.items[i].data.KD_PRODUK != '' || dsTRDetailPenJasRadList.data.items[i].data.DESKRIPSI  != ''){
            x += 1;
        };
    }
    return x;
}


function getArrPenJasRad()
{
    var x='';
    var arr=[];
    for(var i = 0 ; i < dsTRDetailPenJasRadList.getCount();i++)
    {
        // console.log(dsTRDetailPenJasRadList.data.items[i].data.kd_produk);
        if (dsTRDetailPenJasRadList.data.items[i].data.kd_produk != undefined && dsTRDetailPenJasRadList.data.items[i].data.deskripsi != undefined && dsTRDetailPenJasRadList.data.items[i].data.kd_produk != '' && dsTRDetailPenJasRadList.data.items[i].data.deskripsi != '')
        {
            var o={};
            var y='';
            var z='@@##$$@@';
            o['URUT']= dsTRDetailPenJasRadList.data.items[i].data.urut;
            o['KD_PRODUK']= dsTRDetailPenJasRadList.data.items[i].data.kd_produk;
            o['QTY']= dsTRDetailPenJasRadList.data.items[i].data.qty;
            o['TGL_TRANSAKSI']= dsTRDetailPenJasRadList.data.items[i].data.tgl_transaksi;
            o['TGL_BERLAKU']= dsTRDetailPenJasRadList.data.items[i].data.tgl_berlaku;
            o['HARGA']= dsTRDetailPenJasRadList.data.items[i].data.harga;
            o['KD_TARIF']= dsTRDetailPenJasRadList.data.items[i].data.kd_tarif;
            o['cito']= dsTRDetailPenJasRadList.data.items[i].data.cito;
            o['kd_unit']= dsTRDetailPenJasRadList.data.items[i].data.kd_unit;
            arr.push(o);
        };
    }   
    
    return Ext.encode(arr);
}
function getItemPanelPenJasRad(lebar) {
    var items ={
        layout: 'fit',
        labelAlign: 'right',
        bodyStyle: 'padding: 4px;',
        border:false,
        items:[
			{
				columnWidth:.99,
				layout: 'absolute',
				border: true,
				width: 100,
				height: 120,
				anchor: '100% 100%',
				items:[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'No. Transaksi  '
					},{
						x: 100,
						y: 10,
						xtype: 'label',
						text: ':'
					},{
						x : 110,
						y : 10,
						xtype: 'textfield',
						name: 'txtNoTransaksiPenJasRad',
						id: 'txtNoTransaksiPenJasRad',
						width: 100,
						emptyText:nmNomorOtomatis,
						readOnly:true
					},{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'No. Medrec  '
					},{
						x: 100,
						y: 40,
						xtype: 'label',
						text: ':'
					},{
						x: 110,
						y: 40,
						xtype: 'textfield',
						fieldLabel: 'No. Medrec',
						name: 'txtNoMedrecL',
						id: 'txtNoMedrecL',
						// readOnly:true,
						width: 120,
						emptyText:'Otomatis',
                        listeners:{
                            'specialkey' : function(){
                                var tmpNoMedrec = Ext.get('txtNoMedrecL').getValue()
								var tmpgetNoMedrec
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 ){
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 ){
										  tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrecL').getValue())
										 Ext.getCmp('txtNoMedrecL').setValue(tmpgetNoMedrec);
										 console.log(tmpgetNoMedrec)
									}else{
										if (tmpNoMedrec.length === 10){
											tmpgetNoMedrec = tmpNoMedrec
											Ext.getCmp('txtNoMedrecL').setValue(tmpgetNoMedrec);
											
										}else{
											Ext.getCmp('txtNoMedrecL').setValue('');
										}
									}

									Ext.Ajax.request({
										url: baseURL + "index.php/main/functionRAD/getDataPasien",
										params: {kd_pasien :tmpgetNoMedrec },
										success: function (o)
										{
											var cst = Ext.decode(o.responseText);
											if (cst.success === true)
											{
												// ShowPesanInfoPenJasLab("Proses Hapus Pembayaran Berhasil", nmHeaderHapusData);
												Ext.getCmp('txtNoMedrecL').setValue(cst.data.KD_PASIEN);
												Ext.getCmp('txtNamaPasienL').setValue(cst.data.NAMA);
												Ext.getCmp('txtAlamatL').setValue(cst.data.ALAMAT);
												Ext.getCmp('dtpTtlL').setValue(ShowDate(cst.data.TGL_LAHIR));
												var tahunLahir= ShowDate(cst.data.TGL_LAHIR);
												var tahunLahirSplit= tahunLahir.split('/');
												var tahunSekarang=nowTglTransaksiGrid.format("Y");
												var selisihTahunLahir=tahunSekarang-tahunLahirSplit[2];
												Ext.get('txtUmurRad').dom.value=selisihTahunLahir;

												jenisKelaminPasien=cst.data.JENIS_KELAMIN;		
												Ext.get('cboJK').dom.value = cst.data.JENIS_KELAMIN;
												if (cst.data.JENIS_KELAMIN === '1')
												{
													Ext.get('cboJK').dom.value= 'Laki-laki';
													
												}else{
													Ext.get('cboJK').dom.value= 'Perempuan'
													
												}

												Ext.getCmp('txtnotlprad').setValue(cst.data.TELEPON);

												Ext.get('cboGDR').dom.value = cst.data.GOL_DARAH;
												if (cst.data.GOL_DARAH === '0')
												{
													Ext.get('cboGDR').dom.value= '-';
												}else if (cst.data.GOL_DARAH === '1')
												{
													Ext.get('cboGDR').dom.value= 'A+'
												}else if (cst.data.GOL_DARAH === '2')
												{
													Ext.get('cboGDR').dom.value= 'B+'
												}else if (cst.data.GOL_DARAH === '3')
												{
													Ext.get('cboGDR').dom.value= 'AB+'
												}else if (cst.data.GOL_DARAH === '4')
												{
													Ext.get('cboGDR').dom.value= 'O+'
												}else if (cst.data.GOL_DARAH === '5')
												{
													Ext.get('cboGDR').dom.value= 'A-'
												}else if (cst.data.GOL_DARAH === '6')
												{
													Ext.get('cboGDR').dom.value= 'B-'
												}
												
											} else if (cst.success === false)
											{
												ShowPesanWarningPenJasLab("Pasien tidak ditemukan !");
											}
											;
										}
									});
                                }
                            }
                        }
					},{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Unit  '
					},{
						x: 100,
						y: 70,
						xtype: 'label',
						text: ':'
					},{
						x: 110,
						y: 70,
						xtype: 'textfield',
						name: 'txtNamaUnit',
						id: 'txtNamaUnit',
						readOnly:true,
						width: 120
					},{
						x: 10,
						y: 100,
						xtype: 'label',
						text: 'Dokter Pengirim'
					},{
						x: 100,
						y: 100,
						xtype: 'label',
						text: ':'
					},{
						x: 110,
						y: 100,
						xtype: 'textfield',
						fieldLabel: '',
						readOnly:true,
						name: 'txtDokterPengirimL',
						id: 'txtDokterPengirimL',
						width: 200
					},{
						x: 10,
						y: 190,
						xtype: 'label',
						text: 'Unit Rad'
					},{
						x: 100,
						y: 190,
						xtype: 'label',
						text: ':'
					},
					mComboUnitRad(),
					// {
					//     x: 360,
					//     y: 190,
					//     xtype: 'label',
					//     text: 'No. SEP'
					// },
					// {
					//     x: 400,
					//     y: 190,
					//     xtype: 'label',
					//     text: ':'
					// },
					// {
					//     x: 410,
					//     y: 185,
					//     xtype: 'textfield',
					//     fieldLabel: '',
					//     readOnly:true,
					//     name: 'txtnoseprad',
					//     id: 'txtnoseprad',
					//     width: 200
					// },
					{
						x: 10,
						y: 130,
						xtype: 'label',
						text: 'Dokter Rad'
					},{
						x: 100,
						y: 130,
						xtype: 'label',
						text: ':'
					},
					mComboDOKTER(),
					{
						x: 360,
						y: 130,
						xtype: 'label',
						text: 'No. Tlp'
					},{
						x: 400,
						y: 130,
						xtype: 'label',
						text: ':'
					},{
						x: 410,
						y: 130,
						xtype: 'textfield',
						fieldLabel: '',
						readOnly:true,
						name: 'txtnotlprad',
						id: 'txtnotlprad',
						width: 150,
						enableKeyEvents: true,
						listeners:{
							'specialkey': function (){
								if (Ext.EventObject.getKey() === 13){
									Ext.getCmp('cboDOKTER_viPenJasRad').focus();
								}
							}
						}
					}, {
						x: 260,
						y: 10,
						xtype: 'label',
						text: 'Tanggal Kunjung '
					},{
						x: 350,
						y: 10,
						xtype: 'label',
						text: ':'
					},{
						x: 360,
						y: 10,
						xtype: 'datefield',
						fieldLabel: 'Tanggal hari ini ',
						id: 'dtpKunjunganL',
						name: 'dtpKunjunganL',
						format: 'd/M/Y',
						//readOnly : true,
						value: now,
						width: 110
					},{
						x: 480,
						y: 10,
						xtype: 'label',
						text: 'No. Reg : '
					},{
						x: 530,
						y: 10,
						xtype: 'textfield',
						id: 'txtnoregrad',
						name: 'txtnoregrad',
						readOnly : true,
						width: 110
					},{
						x: 650,
						y: 10,
						xtype: 'label',
						text: 'No. Foto : '
					},{
						x: 705,
						y: 10,
						xtype: 'textfield',
						id: 'txtnofotorad',
						name: 'txtnofotorad',
						width: 110
					},{
						x: 260,
						y: 40,
						xtype: 'label',
						text: 'Nama Pasien '
					},{
						x: 350,
						y: 40,
						xtype: 'label',
						text: ':'
					},{
						x: 360,
						y: 40,
						xtype: 'textfield',
						fieldLabel: '',
						readOnly:true,
						name: 'txtNamaPasienL',
						id: 'txtNamaPasienL',
						width: 200,
						enableKeyEvents: true,
						listeners:{
							'specialkey': function (){
								if (Ext.EventObject.getKey() === 13){
									Ext.getCmp('dtpTtlL').focus();
								}
							}
						}
					},{
						x: 260,
						y: 70,
						xtype: 'label',
						text: 'Alamat '
					},{
						x: 350,
						y: 70,
						xtype: 'label',
						text: ':'
					},{
						x: 360,
						y: 70,
						xtype: 'textfield',
						fieldLabel: '',
						readOnly:true,
						name: 'txtAlamatL',
						id: 'txtAlamatL',
						width: 395,
						enableKeyEvents: true,
						listeners:{
							'specialkey': function (){
								if (Ext.EventObject.getKey() === 13){
									if (combovalues === 'Transaksi Lama'){
										Ext.getCmp('txtnotlprad').focus();
									}else{
										Ext.getCmp('cboJK').focus();
									}
								}
							}
						}
					},{
						x: 360,
						y: 100,
						xtype: 'label',
						text: 'Jenis Kelamin '
					},{
						x: 445,
						y: 100,
						xtype: 'label',
						text: ':'
					},
					mComboJK(),
					{
						x: 570,
						y: 100,
						xtype: 'label',
						text: 'Gol. Darah '
					},{
						x: 640,
						y: 100,
						xtype: 'label',
						text: ':'
					},
					mComboGDarah(),
					{
						x: 570,
						y: 40,
						xtype: 'label',
						text: 'Tanggal lahir '
					},{
						x: 640,
						y: 40,
						xtype: 'label',
						text: ':'
					},{
						x: 645,
						y: 40,
						xtype: 'datefield',
						fieldLabel: 'Tanggal ',
						id: 'dtpTtlL',
						selectOnFocus: true,
						forceSelection: true,
						name: 'dtpTtlL',
						format: 'd/M/Y',
						readOnly : true,
						value: now,
						width: 110,
						enableKeyEvents: true,
						listeners:{
							'specialkey': function (){
								if (Ext.EventObject.getKey() === 13){
									var tmptanggal = Ext.get('dtpTtlL').getValue();
									Ext.Ajax.request({
										url: baseURL + "index.php/main/GetUmur",
										params: {
											TanggalLahir: tmptanggal
										},
										success: function (o){
											var tmphasil = o.responseText;
											var tmp = tmphasil.split(' ');
											if (tmp.length == 6){
												Ext.getCmp('txtUmurRad').setValue(tmp[0]);
												getParamBalikanHitungUmur(tmptanggal);
											}else if(tmp.length == 4){
												if(tmp[1]== 'years' && tmp[3] == 'day'){
													Ext.getCmp('txtUmurRad').setValue(tmp[0]);
												}else if(tmp[1]== 'years' && tmp[3] == 'days'){
													Ext.getCmp('txtUmurRad').setValue(tmp[0]);
												}else if(tmp[1]== 'year' && tmp[3] == 'days'){
													Ext.getCmp('txtUmurRad').setValue(tmp[0]);
												}else{
													Ext.getCmp('txtUmurRad').setValue(tmp[0]);
												}
												getParamBalikanHitungUmur(tmptanggal);
											}else if(tmp.length == 2 ){
												if (tmp[1] == 'year' ){
													Ext.getCmp('txtUmurRad').setValue(tmp[0]);
												}else if (tmp[1] == 'years' ){
													Ext.getCmp('txtUmurRad').setValue(tmp[0]);
												}else if (tmp[1] == 'mon'  ){
													Ext.getCmp('txtUmurRad').setValue('0');
												}else if (tmp[1] == 'mons'  ){
													Ext.getCmp('txtUmurRad').setValue('0');
												}else{
													Ext.getCmp('txtUmurRad').setValue('0');
												}
												getParamBalikanHitungUmur(tmptanggal);
											}else if(tmp.length == 1){
												Ext.getCmp('txtUmurRad').setValue('0');
												getParamBalikanHitungUmur(tmptanggal);
											}else{
												alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
											}   
										}
									});
									Ext.getCmp('txtAlamatL').focus();
								}
							}
						}
					},{
						x: 765,
						y: 40,
						xtype: 'label',
						text: 'Umur '
					},{
						x: 795,
						y: 40,
						xtype: 'label',
						text: ':'
					},{
						x: 805,
						y: 40,
						xtype: 'textfield',
						fieldLabel: '',
						name: 'txtUmurRad',
						id: 'txtUmurRad',
						width: 30,
						listeners:{
							'specialkey': function () {
								if (Ext.EventObject.getKey() === 13){
									Ext.Ajax.request({
										url: baseURL + "index.php/main/functionLAB/cekUsia",
										params: {umur: Ext.get('txtUmurRad').getValue()},
										failure: function (o){
											ShowPesanErrorPenJasRad('Proses Error ! ', 'Radiologi');
										},
										success: function (o){
											var cst = Ext.decode(o.responseText);
											if (cst.success === true){
												Ext.getCmp('dtpTtlL').setValue(cst.tahunumur);
											}else{
												ShowPesanErrorPenJasRad('Proses Error ! ', 'Radiologi');
											}
										}
									});
								}
							}
						}
					},{
						x: 10,
						y: 160,
						xtype: 'label',
						text: 'Kelompok Pasien '
					},{
						x: 100,
						y: 160,
						xtype: 'label',
						text: ':'
					},
					mCboKelompokpasien(),
					mComboPerseorangan(),
					mComboPerusahaan(),
					mComboAsuransi(),
					{
						x: 410,
						y: 160,
						xtype: 'textfield',
						fieldLabel: 'Nama Peserta',
						maxLength: 200,
						name: 'txtNamaPesertaAsuransiRad',
						id: 'txtNamaPesertaAsuransiRad',
						emptyText:'Nama Peserta Asuransi',
						width: 150
					},{
						x: 250,
						y: 190,
						xtype: 'textfield',
						fieldLabel: 'No. Askes',
						maxLength: 200,
						name: 'txtNoAskesRad',
						id: 'txtNoAskesRad',
						emptyText:'No Askes',
						width: 150
					},{
						x: 410,
						y: 190,
						xtype: 'textfield',
						fieldLabel: 'No. SJP',
						maxLength: 200,
						name: 'txtNoSJPRad',
						id: 'txtNoSJPRad',
						emptyText:'No SJP',
						width: 150
					},{
						x: 110,
						y: 130,
						xtype: 'textfield',
						fieldLabel:'Dokter  ',
						name: 'txtCustomerLamaHide',
						id: 'txtCustomerLamaHide',
						readOnly:true,
						width: 280
					},{
						x: 110,
						y: 130,
						xtype: 'textfield',
						fieldLabel:'Kode customer  ',
						name: 'txtKdCustomerLamaHide',
						id: 'txtKdCustomerLamaHide',
						readOnly:true,
						hidden:true,
						width: 280
					},{
						xtype: 'textfield',
						fieldLabel:'Dokter  ',
						name: 'txtKdDokter',
						id: 'txtKdDokter',
						readOnly:true,
						hidden:true,
						anchor: '99%'
					},{
						xtype: 'textfield',
						fieldLabel:'Unit  ',
						name: 'txtKdUnitRad',
						id: 'txtKdUnitRad',
						readOnly:true,
						hidden:true,
						anchor: '99%'
					},{
						xtype: 'textfield',
						name: 'txtKdUrutMasuk',
						id: 'txtKdUrutMasuk',
						readOnly:true,
						hidden:true,
						anchor: '100%'
					}
				]
			}
        ]
    };
    return items;
}
function getParamBalikanHitungUmur(tgl){
    Ext.getCmp('dtpTtlL').setValue(tgl);
}

function getItemPanelUnit(lebar) 
{
    var items =
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                columnWidth: .40,
                layout: 'form',
                labelWidth:100,
                border: false,
                items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel:'Unit  ',
                        name: 'txtKdUnitRad',
                        id: 'txtKdUnitRad',
                        readOnly:true,
                        anchor: '99%'
                    }
                ]
            },
            {
                columnWidth: .60,
                layout: 'form',
                border: false,
                labelWidth:1,
                items:
                [
                    {
                        xtype: 'textfield',
                        name: 'txtNamaUnit',
                        id: 'txtNamaUnit',
                        readOnly:true,
                        anchor: '100%'
                    }
                ]
            }
        ]
    }
    return items;
};

function refeshpenjasrad(kriteria)
{
    /* dsTRPenJasRadList.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPenJasRad',
                    param : kriteria
                }           
            }
        );   
    return dsTRPenJasRadList; */
    
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rad/viewpenjasrad/getPasien",
            params: getParamRefreshRad(kriteria),
            failure: function(o)
            {
                ShowPesanWarningPenJasRad('Hubungi Admin', 'Error');
            },
            success: function(o)
            {   
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    dsTRPenJasRadList.removeAll();
                    var recs=[],
                    recType=dsTRPenJasRadList.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    dsTRPenJasRadList.add(recs);
                    grListPenJasRad.getView().refresh();

                }
                else
                {
                    ShowPesanWarningPenJasRad('Gagal membaca Data ini', 'Error');
                };
            }
        }

    );
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/getDefaultUnit",
        params: {
            notrans: 'no',
            Modul:'no'
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            getComboDokterRad();
            //Ext.getCmp('cbounitrads_viPenJasRad').setValue(cst.kd_unit);
            combovaluesunittujuan=cst.kd_unit;
            
        }

    });
}


function getParamRefreshRad(krite){
    var unit;
    if(radiovalues == 1){
        unit = 'IGD';
    } else if(radiovalues == 2){
        unit = 'RWI';
    } else if(radiovalues == 3){
        unit = 'Langsung';
    } else{
        unit = 'RWJ';
    }
    var params =
    {
        unit:unit,
        kriteria:krite
        /* tglAwal:
        tglAkhir:dtpTglAwalFilterPenJasRad */
        
    }
    return params;
}

function loadpenjasrad(tmpparams, tmpunit)
{
    dsTRPenJasRadList.load
        (
                    { 
                        params:  
                        {   
                            Skip: 0, 
                            Take: '',
                            Sort: '',
                            Sortdir: 'ASC', 
                            target: tmpunit,
                            param : tmpparams
                        }           
                    }
        );   
    return dsTRPenJasRadList;
}

function Datasave_PenJasRad(mBol,jasa,callback) 
{   
if(jasa==undefined){
		jasa=false;
	}
     if (ValidasiEntryPenJasRad(nmHeaderSimpanData,false) == 1 )
     {
        // console.log(tglTransaksiAwalPembandingTransferRAD);
        // console.log(Ext.getCmp('dtpKunjunganL').getValue().format('Y-m-d')+" 00:00:00");
        //   if (tglTransaksiAwalPembandingTransferRAD > Ext.getCmp('dtpKunjunganL').getValue().format('Y-m-d')+" 00:00:00"){
        //      ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,kd_kasir_rad);
        //     ShowPesanWarningPenJasRad('Tanggal transaksi pasien Tidak boleh kurang dari tanggal kunjungan', 'Gagal');
            
        // }else{
         loadMask.show();
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/functionRAD/savedetailpenyakit",
                    params: getParamDetailTransaksiRAD(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,kd_kasir_rad);
                    },  
                    success: function(o) 
                    {
                        loadMask.hide();
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            // console.log(cst);
                            tmpkd_pasien_sql = cst.kdPasien;
                            tmpno_trans_sql = cst.notrans;
                            tmpkd_kasir_sql = cst.kdkasir;
                            tmp_tgl_sql = cst.tgl;
                            tmp_urut_sql = cst.urut;
                            tmpurut = cst.urut;
                            kd_kasir_rad = cst.kdkasir;
                            loadMask.hide();
                            ShowPesanInfoPenJasRad("Berhasil menyimpan data ini","Information");
                            Ext.get('txtNoTransaksiPenJasRad').dom.value=cst.notrans;
                            Ext.get('txtNoMedrecL').dom.value=cst.kdPasien;
                            ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,tmpkd_kasir_sql);
                            Ext.getCmp('btnPembayaranPenjasRAD').enable();
                            Ext.getCmp('btnTutupTransaksiPenjasRAD').enable();
                            Ext.getCmp('txtnoregrad').setValue(cst.noreg);
                            if(mBol === false)
                            {
                                ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,tmpkd_kasir_sql);
                            };

                            if (Ext.get('cboJenisTr_viPenJasRad').getValue()!='Transaksi Baru' && radiovalues != '3')
                            {
                                validasiJenisTr();
                            }
                           // Datasave_PenJasRad_SQL();
						   if(jasa == true){
								SimpanJasaDokterPenindak(currentJasaDokterKdProduk_RAD,currentJasaDokterKdTarif_RAD,currentJasaDokterUrutDetailTransaksi_RAD,currentJasaDokterHargaJP_RAD);
							}

                            get_total_bayar(cst.notrans, cst.kdkasir);
                            get_unit_asal(cst.notrans, cst.kdkasir);
							if(callback != undefined){
								callback();
							}
                        }
                        else 
                        {
                                ShowPesanWarningPenJasRad('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        // }
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};
function SimpanJasaDokterPenindak(kd_produk,kd_tarif,urut,harga){
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionIGD/savejasadokterpenindak",
		params: getParamsJasaDokterPenindak(urut,harga),
		failure: function(o)
		{
			ShowPesanWarningPenJasRad('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			//dsGridJasaDokterPenindak_IGD.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				ShowPesanInfoPenJasRad('Dokter penindak berhasil disimpan','Information');
				GetgridEditDokterPenindakJasa_RAD();
			} 
			else 
			{
				ShowPesanWarningPenJasRad('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}
function getParamsJasaDokterPenindak(urut,harga){
	var params = {
		kd_kasir:currentKdKasirRAD,
		no_transaksi:TmpNotransaksi,
		tgl_transaksi:TglTransaksi,
		urut:urut,
		harga:harga
	};
	params['jumlah'] = dsGridJasaDokterPenindak_RAD.getCount();
	for(var i = 0 ; i < dsGridJasaDokterPenindak_RAD.getCount();i++)
	{
		params['kd_component-'+i]=dsGridJasaDokterPenindak_RAD.data.items[i].data.kd_component;
		params['kd_dokter-'+i]=dsGridJasaDokterPenindak_RAD.data.items[i].data.kd_dokter;
	}
	return params;
}
function Datasave_PenJasRad_LangsungTambahBaris(mBol) 
{   
     if (ValidasiEntryPenJasRad(nmHeaderSimpanData,false) == 1 )
     {
         loadMask.show();
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/functionRAD/savedetailpenyakit",
                    params: getParamDetailTransaksiRAD(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,kd_kasir_rad);
                    },  
                    success: function(o) 
                    {
                        loadMask.hide();
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            // console.log(cst);
                            tmpkd_pasien_sql = cst.kdPasien;
                            tmpno_trans_sql = cst.notrans;
                            tmpkd_kasir_sql = cst.kdkasir;
                            tmp_tgl_sql = cst.tgl;
                            tmp_urut_sql = cst.urut;
                            tmpurut = cst.urut;
                            kd_kasir_rad = cst.kdkasir;
                            loadMask.hide();
                            //ShowPesanInfoPenJasRad("Berhasil menyimpan data ini","Information");
                            Ext.get('txtNoTransaksiPenJasRad').dom.value=cst.notrans;
                            Ext.get('txtNoMedrecL').dom.value=cst.kdPasien;
                            //ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,tmpkd_kasir_sql);
                            Ext.getCmp('btnPembayaranPenjasRAD').enable();
                            Ext.getCmp('btnTutupTransaksiPenjasRAD').enable();
                            Ext.getCmp('txtnoregrad').setValue(cst.noreg);
                            /* if(mBol === false)
                            {
                                ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,tmpkd_kasir_sql);
                            }; */
                            if (Ext.get('cboJenisTr_viPenJasRad').getValue()!='Transaksi Baru' && radiovalues != '3')
                            {
                                validasiJenisTr();
                            }
                            get_total_bayar(cst.notrans,cst.kdkasir);
                            //Datasave_PenJasRad_SQL();
                        }
                        else 
                        {
                                ShowPesanWarningPenJasRad('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

var tmpkd_pasien_sql = '';
var tmpno_trans_sql = '';
var tmpkd_kasir_sql = '';
var tmp_tgl_sql = '';
var tmp_urut_sql = '';


function Dataupdate_PenJasRad(mBol) 
{   
    if (ValidasiEntryPenJasRad(nmHeaderSimpanData,false) == 1 )
    {
        
            Ext.Ajax.request
             (
                {
                    //url: "./Datapool.mvc/CreateDataObj",
                    url: baseURL + "index.php/main/CreateDataObj",
                    params: getParamDataupdatePenJasRadDetail(),
                    success: function(o) 
                    {
                        ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,kd_kasir_rad);
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            ShowPesanInfoPenJasRad(nmPesanSimpanSukses,nmHeaderSimpanData);
                            //RefreshDataPenJasRad();
                            if(mBol === false)
                            {
                                ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value,kd_kasir_rad);
                            };
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarningPenJasRad(nmPesanSimpanGagal,nmHeaderSimpanData);
                        }
                        else if (cst.success === false && cst.pesan===1)
                        {
                            ShowPesanWarningPenJasRad(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
                        }
                        else 
                        {
                            ShowPesanErrorPenJasRad(nmPesanSimpanError,nmHeaderSimpanData);
                        };
                    }
                }
            )
        
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
};
function ValidasiEntryPenJasRad(modul,mBolHapus)
{
    var x = 1;
    //cboUnitRad_viPenJasRad
    if((Ext.get('cboUnitRad_viPenJasRad').getValue() == '') || (Ext.get('txtNamaPasienL').getValue() == '') || (Ext.get('cboDOKTER_viPenJasRad').getValue() == 'Pilih Dokter') || (Ext.get('dtpKunjunganL').getValue() == '') || (dsTRDetailPenJasRadList.getCount() === 0 ))
    {
        if (Ext.get('txtNoTransaksiPenJasRad').getValue() == '' && mBolHapus === true) 
        {
            x = 0;
        }
        else if (Ext.get('txtNoMedrecL').getValue() == '') 
        {
            ShowPesanWarningPenJasRad('No. Medrec tidak boleh kosong!','Warning');
            x = 0;
        }
        else if (Ext.get('cboUnitRad_viPenJasRad').getValue() == '') 
        {
            ShowPesanWarningPenJasRad('No. Unit Radiologi Wajib diisi!','Warning');
            x = 0;
        }
        else if (Ext.get('txtNamaPasienL').getValue() == '') 
        {
            ShowPesanWarningPenJasRad('Nama tidak boleh kosong!','Warning');
            x = 0;
        }
        else if (Ext.get('dtpKunjunganL').getValue() == '') 
        {
            ShowPesanWarningPenJasRad('Tanggal kunjungan tidak boleh kosong!','Warning');
            x = 0;
        }
        else if (Ext.get('cboDOKTER_viPenJasRad').getValue() == 'Pilih Dokter' || Ext.get('cboDOKTER_viPenJasRad').dom.value  === undefined) 
        {
            ShowPesanWarningPenJasRad('Dokter Radiologi tidak boleh kosong!','Warning');
            x = 0;
        }
        else if (dsTRDetailPenJasRadList.getCount() === 0) 
        {
            ShowPesanWarningPenJasRad('Daftar item test tidak boleh kosong!','Warning');
            x = 0;
        };
    };
    return x;
};

function ShowPesanWarningPenJasRad(str, modul) 
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width:250
        }
    );
};

function ShowPesanErrorPenJasRad(str, modul) {
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width:250
        }
    );
};

function ShowPesanInfoPenJasRad(str, modul) {
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width:250
        }
    );
};


function DataDeletePenJasRad() 
{
   if (ValidasiEntryPenJasRad(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                //url: "./Datapool.mvc/DeleteDataObj",
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiRAD(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoPenJasRad(nmPesanHapusSukses,nmHeaderHapusData);
                                        //RefreshDataPenJasRad();
                                        PenjasRadAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningPenJasRad(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningPenJasRad(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorPenJasRad(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};

function RefreshDatacombo(jeniscus) 
{

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customerRAD+'~)'
            }
        }
    )
    
    // rowSelectedPenJasRad = undefined;
    return ds_customer_viDaftar;
};
function mComboKelompokpasien()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    )
    var cboKelompokpasien = new Ext.form.ComboBox
    (
        {
            id:'cboKelompokpasien',
            typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: radelisi,
                        align: 'Right',
                        anchor: '95%',
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetKelompokpasien=b.data.displayText ;
                    selectKdCustomer=b.data.KD_CUSTOMER;
                    selectNamaCustomer=b.data.CUSTOMER;
                
                }
            }
        }
    );
    return cboKelompokpasien;
};

function getKelompokpasienlama(lebar) 
{
    var items =
    {
        Width:lebar,
        height:40,
        layout: 'column',
        border: false,
    //  title: 'Kelompok Pasien Lama',
        
        items:
        [
            {
                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                //title: 'Kelompok Pasien Lama',
                items:
                [
                    {    
                        xtype: 'tbspacer',
                        height: 2
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kelompok Pasien Asal',
                        //maxLength: 200,
                        name: 'txtCustomerLama',
                        id: 'txtCustomerLama',
                        labelWidth:130,
                        width: 100,
                        anchor: '95%'
                    },
                    
                    
                ]
            }
            
        ]
    }
    return items;
};


function getItemPanelNoTransksiKelompokPasien(lebar) 
{
    var items =
    {
        Width:lebar,
        height:120,
        layout: 'column',
        border: false,
        
        
        items:
        [
            {

                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                items:
                [
                    
                    
                    {    
                        xtype: 'tbspacer',
                        
                        height:3
                    },
                        {  

                        xtype: 'combo',
                        fieldLabel: 'Kelompok Pasien Baru',
                        id: 'kelPasien',
                         editable: false,
                        //value: 'Perseorangan',
                        store: new Ext.data.ArrayStore
                            (
                                {
                                id: 0,
                                fields:
                                [
                                'Id',
                                'displayText'
                                ],
                                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                }
                            ),
                              displayField: 'displayText',
                              mode: 'local',
                              width: 100,
                              forceSelection: true,
                              triggerAction: 'all',
                              emptyText: 'Pilih Salah Satu...',
                              selectOnFocus: true,
                              anchor: '95%',
                              listeners:
                                {
                                        'select': function(a, b, c)
                                    {
                                        if(b.data.displayText =='Perseorangan')
                                        {
                                            jeniscus='0'
                                            Ext.getCmp('txtNoSJP').disable();
                                            Ext.getCmp('txtNoAskes').disable();}
                                        else if(b.data.displayText =='Perusahaan')
                                        {
                                            jeniscus='1';
                                            Ext.getCmp('txtNoSJP').disable();
                                            Ext.getCmp('txtNoAskes').disable();}
                                        else if(b.data.displayText =='Asuransi')
                                        {
                                            jeniscus='2';
                                            Ext.getCmp('txtNoSJP').enable();
                                            Ext.getCmp('txtNoAskes').enable();
                                        }
                                    
                                        RefreshDatacombo(jeniscus);
                                    }

                                }
                        }, 
                        {
                            columnWidth: .990,
                            layout: 'form',
                            border: false,
                            labelWidth:130,
                            items:
                            [
                                mComboKelompokpasien()
                            ]
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. SJP',
                            maxLength: 200,
                            name: 'txtNoSJP',
                            id: 'txtNoSJP',
                            width: 100,
                            anchor: '95%'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Askes',
                            maxLength: 200,
                            name: 'txtNoAskes',
                            id: 'txtNoAskes',
                            width: 100,
                            anchor: '95%'
                         }
                        
                        //mComboKelompokpasien
        
        
                ]
            }
            
        ]
    }
    return items;
};

//combo jenis kelamin
function mComboJK()
{
    var cboJK = new Ext.form.ComboBox
    (
        {
            id:'cboJK',
            x: 455,
            y: 100,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            tabIndex:3,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Jenis Kelamin',
            fieldLabel: 'Jenis Kelamin ',
            editable: false,
            width:95,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[true, 'Laki - Laki'], [false, 'Perempuan']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetJK,
            enableKeyEvents: true,
            listeners:
            {
                'specialkey': function (){
                    if (Ext.EventObject.getKey() === 13){
                        Ext.getCmp('cboGDR').focus();
                    }
                },
                'select': function(a,b,c)
                {
                    selectSetJK=b.data.valueField ;
                },
/*              'render': function(c) {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('txtTempatLahir').focus();
                                    }, c);
                                }
 */         }
        }
    );
    return cboJK;
};

//combo golongan darah
function mComboGDarah()
{
    var cboGDR = new Ext.form.ComboBox
    (
        {
            id:'cboGDR',
            x: 645,
            y: 100,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            tabIndex:3,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Gol. Darah',
            fieldLabel: 'Gol. Darah ',
            width:50,
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-'], [7, 'AB-'], [8, 'O-']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetGDarah,
            enableKeyEvents: true,
            listeners:
            {
                'specialkey': function (){
                    if (Ext.EventObject.getKey() === 13){
                        Ext.getCmp('txtnotlprad').focus();
                    }
                },
                'select': function(a,b,c)
                {
                    selectSetGDarah=b.data.displayText ;
                },
                /*'render': function(c) {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('txtTempatLahir').focus();
                                    }, c);
                                }*/
            }
        }
    );
    return cboGDR;
};

//Combo Dokter Lookup
function getComboDokterRad(kdUnit)
{
    dsdokter_viPenJasRad.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang',
            param: "kd_unit = '5' and d.jenis_dokter=1 "
        }
    });
    /* dsdokter_viPenJasRad.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang',
            param: "kd_unit = '"+kdUnit+"'"
        }
    }); */
    return dsdokter_viPenJasRad;
}
function mComboDOKTER()
{ 
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viPenJasRad = new WebApp.DataStore({ fields: Field });
    dsdokter_viPenJasRad.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang ',
            param: "kd_unit = '5' and d.jenis_dokter=1 "
        }
    });
    
    // console.log(rowSelectedPenJasRad);
    var cboDOKTER_viPenJasRad = new Ext.form.ComboBox
    (
            {
                id: 'cboDOKTER_viPenJasRad',
                x: 110,
                y: 130,
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 200,
                emptyText:'Pilih Dokter',
                store: dsdokter_viPenJasRad,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                // value: rowSelectedPenJasRad.data.DOKTER_RAD,
                // editable: false,
                enableKeyEvents: true,
                
                listeners:
                {
                    'specialkey': function (){
                            if (Ext.EventObject.getKey() === 13){
                                Ext.getCmp('cboKelPasienRad').focus();
                            }
                        },
                    'select': function(a,b,c)
                    {
                        tmpkddoktertujuan=b.data.KD_DOKTER ;
                    },
                }
            }
    );
    
    return cboDOKTER_viPenJasRad;
};
var tmpkddoktertujuan;

function mComboUnitRad(){ 
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitrad_viPenJasRad = new WebApp.DataStore({ fields: Field });
    dsunitrad_viPenJasRad.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'nama_unit',
            Sortdir: 'ASC',
            target: 'ViewSetupUnit',
            param: "kd_bagian = '5'"
        }
    });
    var cbounitrad_viPenJasRad = new Ext.form.ComboBox({
        id: 'cboUnitRad_viPenJasRad',
        x: 110,
        y: 190,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        emptyText: '',
        fieldLabel:  ' ',
        align: 'Right',
        width: 130,
        emptyText:'Pilih Unit',
        store: dsunitrad_viPenJasRad,
        valueField: 'KD_UNIT',
        displayField: 'NAMA_UNIT',
        //value:'All',
        editable: false,
        listeners:
            {
                'select': function(a,b,c)
                {
                    console.log(b.data);
                    tmpkd_unit = b.data.KD_UNIT;
                    if (Ext.getCmp('txtDokterPengirimL').getValue()=='DOKTER LUAR')
                    {
                        Ext.Ajax.request({
                            url: baseURL + "index.php/main/functionRAD/getTarifMir",
                            params: {
                                kd_unit:b.data.KD_UNIT,
                                 //kd_unit_asal:tmpkd_unit_asal,
                                 kd_customer:vkode_customerRAD,
                                 penjas:'langsung',
                                 kdunittujuan:b.data.KD_UNIT
                            },
                            failure: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                            },
                            success: function (o) {
                                var cst = Ext.decode(o.responseText);
                                // console.log(cst.ListDataObj);
                                tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                            }

                        });
                    }
                    
                    //getComboDokterRad(b.data.KD_UNIT);
                }
            }
    });
    return cbounitrad_viPenJasRad;
}


function mCboKelompokpasien(){  
 var cboKelPasienRad = new Ext.form.ComboBox
    (
        {
            
            id:'cboKelPasienRad',
            fieldLabel: 'Kelompok Pasien',
            x: 110,
            y: 160,
            mode: 'local',
            width: 130,
            forceSelection: true,
            lazyRender:true,
            triggerAction: 'all',
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                id: 0,
                fields:
                [
                    'Id',
                    'displayText'
                ],
                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                }
            ),          
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetKelPasien,
            selectOnFocus: true,
            tabIndex:22,
            listeners:
            {
                'select': function(a, b, c)
                    {
                       Combo_Select(b.data.displayText);
                       if(b.data.displayText == 'Perseorangan')
                       {
                          // cboPerseorangan
                            //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                            //Ext.getCmp('cboRujukanRequestEntry').hide();
                            Ext.getCmp('cboPerseoranganRad').focus(false,100);
                       }
                       else if(b.data.displayText == 'Perusahaan')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryRad').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show(); 
                            Ext.getCmp('cboPerusahaanRequestEntryRad').focus(false,100);
                       }
                       else if(b.data.displayText == 'Asuransi')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryRad').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show(); 
                            Ext.getCmp('cboAsuransiRad').focus(false,100);                          
                       }
                    },
                'select': function(a, b, c)
                    {
                       Combo_Select(b.data.displayText);
                       if(b.data.displayText == 'Perseorangan')
                       {
                          // cboPerseorangan
                            //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                            //Ext.getCmp('cboRujukanRequestEntry').hide();
                            Ext.getCmp('cboPerseoranganRad').focus(false,100);
                       }
                       else if(b.data.displayText == 'Perusahaan')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryLab').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show(); 
                            Ext.getCmp('cboPerusahaanRequestEntryRad').focus(false,100);
                       }
                       else if(b.data.displayText == 'Asuransi')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryLab').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show();  
                             Ext.getCmp('cboAsuransiRad').focus(false,100);
                       }
                      
                    },
                'render': function(a,b,c)
                {
                    Ext.getCmp('txtNamaPesertaAsuransiRad').hide();
                    Ext.getCmp('txtNoAskesRad').hide();
                    Ext.getCmp('txtNoSJPRad').hide();
                    Ext.getCmp('cboPerseoranganRad').hide();
                    Ext.getCmp('cboAsuransiRad').hide();
                    Ext.getCmp('cboPerusahaanRequestEntryRad').hide();
                    
                }
            }

        }
    );
    return cboKelPasienRad;
};

function mComboPerseorangan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganRadRequestEntry = new WebApp.DataStore({fields: Field});
    dsPeroranganRadRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=0'
            }
        }
    )
    var cboPerseoranganRad = new Ext.form.ComboBox
    (
        {
            id:'cboPerseoranganRad',
            x: 250,
            y: 160,
            editable: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            fieldLabel: 'Jenis',
            tabIndex:23,
            width:150,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            store:dsPeroranganRadRequestEntry,
            value:selectSetPerseorangan,
            listeners:
            {
                'select': function(a,b,c)
                {
                    vkode_customerRAD = b.data.KD_CUSTOMER;
                    selectSetPerseorangan=b.data.displayText ;
                }
            }
        }
    );
    return cboPerseoranganRad;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=1'
            }
        }
    )
    var cboPerusahaanRequestEntryRad = new Ext.form.ComboBox
    (
        {
            id: 'cboPerusahaanRequestEntryRad',
            x: 250,
            y: 160,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: 'Perusahaan',
            align: 'Right',
            width:150,
            store: dsPerusahaanRequestEntry,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            //anchor: '95%',
            listeners:
            {
                'select': function(a,b,c)
                {
                    vkode_customerRAD = b.data.KD_CUSTOMER;
                    alert(vkode_customerRAD)
                    selectSetPerusahaan=b.data.valueField ;
                }
            }
        }
    );

    return cboPerusahaanRequestEntryRad;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=2'
            }
        }
    )
    var cboAsuransiRad = new Ext.form.ComboBox
    (
        {
            id:'cboAsuransiRad',
            x: 250,
            y: 160,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: 'Asuransi',
            align: 'Right',
            width:150,
            //anchor: '95%',
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            listeners:
            {
                'select': function(a,b,c)
                {
                    vkode_customerRAD = b.data.KD_CUSTOMER;
                    //alert(vkode_customerRAD)
                    selectSetAsuransi=b.data.valueField ;
                }
            }
        }
    );
    return cboAsuransiRad;
};


function Combo_Select(combo)
{
   var value = combo
   //var txtgetnamaPeserta = Ext.getCmp('txtNamaPesertaAsuransiRad') ;

   if(value == "Perseorangan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiRad').hide()
        Ext.getCmp('txtNoAskesRad').hide()
        Ext.getCmp('txtNoSJPRad').hide()
        Ext.getCmp('cboPerseoranganRad').show()
        Ext.getCmp('cboAsuransiRad').hide()
        Ext.getCmp('cboPerusahaanRequestEntryRad').hide()
        

   }
   else if(value == "Perusahaan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiRad').hide()
        Ext.getCmp('txtNoAskesRad').hide()
        Ext.getCmp('txtNoSJPRad').hide()
        Ext.getCmp('cboPerseoranganRad').hide()
        Ext.getCmp('cboAsuransiRad').hide()
        Ext.getCmp('cboPerusahaanRequestEntryRad').show()
        
   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiRad').show()
         Ext.getCmp('txtNoAskesRad').show()
         Ext.getCmp('txtNoSJPRad').show()
         Ext.getCmp('cboPerseoranganRad').hide()
         Ext.getCmp('cboAsuransiRad').show()
         Ext.getCmp('cboPerusahaanRequestEntryRad').hide()
         
       }
}


function setdisablebutton()
{
Ext.getCmp('btnLookUpKonsultasi_viKasirRad').disable();
Ext.getCmp('btnLookUpGantiDokter_viKasirRad').disable();
Ext.getCmp('btngantipasien').disable();
Ext.getCmp('btnposting').disable(); 

Ext.getCmp('btnLookupPenJasRad').disable()
Ext.getCmp('btnSimpanRAD').disable()
Ext.getCmp('btnHpsBrsRAD').disable()
Ext.getCmp('btnHpsBrsDiagnosa').disable()
Ext.getCmp('btnSimpanDiagnosa').disable()
Ext.getCmp('btnLookupDiagnosa').disable()
}

function setenablebutton()
{
//Ext.getCmp('btnLookUpKonsultasi_viKasirRad').enable();
Ext.getCmp('btnLookUpGantiDokter_viKasirRad').enable();
Ext.getCmp('btngantipasien').enable();
//Ext.getCmp('btnposting').enable();    

Ext.getCmp('btnLookupPenJasRad').enable()
Ext.getCmp('btnSimpanRAD').enable()
//Ext.getCmp('btnHpsBrsRAD').enable()
//Ext.getCmp('btnHpsBrsDiagnosa').enable()
//Ext.getCmp('btnSimpanDiagnosa').enable()
//Ext.getCmp('btnLookupDiagnosa').enable()  
}

//Criteria untuk pencarian berdasarkan no medrec, nama pasien dll
function getCriteriaFilter_viDaftar()//^^^
{   
    /*
    var strKriteria = "";
    var tmptanggalawal = Ext.getCmp('dtpTglAwalFilterPenJasRad').getValue();
    var tmptanggalakhir = Ext.getCmp('dtpTglAkhirFilterPenJasRad').getValue();
    var tglawal = tmptanggalawal.getFullYear() + '/' + (tmptanggalawal.getMonth() + 1) + '/' + tmptanggalawal.getDate();
    var tglakhir =  tmptanggalakhir.getFullYear() + '/' + (tmptanggalakhir.getMonth() + 1) + '/' + tmptanggalakhir.getDate();
    if (Ext.get('dtpTglAwalFilterPenJasRad').getValue() != "")
    {
     if (strKriteria == "")
        {
            strKriteria = "and tr.tgl_transaksi >= '" + tglawal + "'" ;
        }
        else
            {
                strKriteria += " and tr.tgl_transaksi >= '" + tglawal+"'";
            }

    }
    if (Ext.get('dtpTglAkhirFilterPenJasRad').getValue() != "")
    {
    if (strKriteria == "")
        {
            strKriteria = "and tr.tgl_transaksi <= '" + tglakhir+"'" ;
        }
        else
            {
                strKriteria += " and tr.tgl_transaksi <= '" + tglakhir+"'";
            }

    }
    */
        var strKriteria = "";
        if (Ext.get('dtpTglAwalFilterPenJasRad').getValue() != "")
            {
                 if (strKriteria == "")
                    {
                        strKriteria = "and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasRad').getValue()+"'";
                        }
                
            }
            if (Ext.get('dtpTglAkhirFilterPenJasRad').getValue() != "")
            {
                if (strKriteria == "")
                    {
                        strKriteria = "and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue()+"'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasRad').getValue()+"'";
                        }
                
            }   
     return strKriteria;
}

function getItemPanelPenJasRadAwal()
{
    //var tmptruefalse = true;
    var items =
    {
        layout:'column',
        border:true,
        items:
        [
            {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: '100%',
                height: 100,
                anchor: '100% 100%',
                items:
                [
                    //-----------COMBO JENIS TRANSAKSI-----------------------------
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Jenis Transaksi '
                    },
                    {
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    mComboJenisTrans_viPenJasRad(),
                    {
                        x: 310,
                        y: 10,
                        xtype: 'label',
                        text: 'Unit Rad '
                    },
                    {
                        x: 445,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    mComboUnitTujuans_viPenJasRad(),
                    
                    //--------RADIO BUTTON PILIHAN ASAL PASIEN---------------------
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Asal Pasien '
                    },
                    {
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 40,
                        xtype: 'radiogroup',
                        id: 'rbrujukan',
                        fieldLabel: 'Asal Pasien ',
                        items: [
                                    {
                                        boxLabel: 'IGD',
                                        name: 'rb_auto',
                                        id: 'rb_pilihan1',
                                        checked: true,
                                        inputValue: '1',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
                                                Ext.getCmp('dtpTglAwalFilterPenJasRad').setValue(now);
                                                Ext.getCmp('dtpTglAkhirFilterPenJasRad').setValue(now);
                                                getDataCariUnitPenjasRad("kd_bagian=3 and parent<>'0'");
                                                Ext.getCmp('cboUNIT_viKasirRad').show();
                                                Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
                                                //--------------TAMBAH BARU 27-SEPTEMBER-2017
                                                Ext.getCmp('btnEditDataPasienRAD').disable();
                                                radiovalues='1';
                                                validasiJenisTr();
                                                /* tmpparams = "'" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "'";//tanya
                                                tmpunit = 'ViewRwjPenjasRad';
                                                loadpenjasrad(tmpparams, tmpunit); */
                                        }
                                        }
                                    },
                                    {
                                        boxLabel: 'RWJ',
                                        name: 'rb_auto',
                                        id: 'rb_pilihan4',
                                        inputValue: '1',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
                                                Ext.getCmp('dtpTglAwalFilterPenJasRad').setValue(now);
                                                Ext.getCmp('dtpTglAkhirFilterPenJasRad').setValue(now);
                                                getDataCariUnitPenjasRad("kd_bagian=2 and type=0");
                                                Ext.getCmp('cboUNIT_viKasirRad').show();
                                                Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
                                                //--------------TAMBAH BARU 27-SEPTEMBER-2017
                                                Ext.getCmp('btnEditDataPasienRAD').disable();
                                                radiovalues='4';
                                                validasiJenisTr();
                                                /* tmpparams = "'" + Ext.get('dtpTglAwalFilterPenJasRad').getValue() + "'";//tanya
                                                tmpunit = 'ViewRwjPenjasRad';
                                                loadpenjasrad(tmpparams, tmpunit); */
                                        }
                                        }
                                    },
                                    {
                                        boxLabel: 'RWI',
                                        name: 'rb_auto',
                                        id: 'rb_pilihan2',
                                        inputValue: '2',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
                                            Ext.getCmp('dtpTglAwalFilterPenJasRad').setValue(new Date().add(Date.DAY, -30));
                                            Ext.getCmp('dtpTglAkhirFilterPenJasRad').setValue(now);
                                            Ext.getCmp('cboUNIT_viKasirRad').hide();
                                            Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').show();
                                            //--------------TAMBAH BARU 27-SEPTEMBER-2017
                                            Ext.getCmp('btnEditDataPasienRAD').disable();
                                            radiovalues='2';
                                            validasiJenisTr();
                                            /* tmpparams = " tr.co_Status='0' ORDER BY tr.no_transaksi desc limit 50";
                                            tmpunit = 'ViewRwiPenjasRad';
                                            loadpenjasrad(tmpparams, tmpunit); */
                                        }
                                     }
                                    },
                                    {
                                        boxLabel: 'Kunj. Langsung',
                                        name: 'rb_auto',
                                        id: 'rb_pilihan3',
                                        hidden  : false,
                                        inputValue: 'K.Kd_Pasien',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
                                            Ext.getCmp('dtpTglAwalFilterPenJasRad').setValue(now);
                                            Ext.getCmp('dtpTglAkhirFilterPenJasRad').setValue(now);
                                            Ext.getCmp('cboUNIT_viKasirRad').hide();
                                            Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').show();
                                            radiovalues='3';
                                            //--------------TAMBAH BARU 27-SEPTEMBER-2017
                                            if (combovalues==='Transaksi Lama'){
                                                Ext.getCmp('btnEditDataPasienRAD').enable();
                                            }else{
                                                Ext.getCmp('btnEditDataPasienRAD').disable();
                                            }
                                            validasiJenisTr();
                                            //PenJasRadLookUp();
                                            //belum
                                            Ext.get('txtNamaUnit').dom.value   = 'Radiologi';
                                        }
                                    }   
                                    },
                                ]
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Detail Asal Pasien '
                    },
                    {
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },
                    //COMBO POLI dan KAMAR
                    mComboUnit_viKasirRad(),
                    mcomboKamarSpesial(),
                    
                ]
            },
            //---------------JARAK 1----------------------------
             {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: false,
                width: 100,
                height: 5,
                anchor: '50% 50%',
                items:
                [
                        
                ]
             },
            //--------------------------------------------------
            {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: '100%',
                height: 165,
                anchor: '100% 100%',
                items:
                [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec  '
                    },
                    {
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x : 155,
                        y : 10,
                        xtype: 'textfield',
                        fieldLabel: 'No. Medrec (Enter Untuk Mencari)',
                        name: 'txtNoMedrecPenJasRad',
                        id: 'txtNoMedrecPenJasRad',
                        enableKeyEvents: true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrecPenJasRad').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrecPenJasRad').getValue())
                                             Ext.getCmp('txtNoMedrecPenJasRad').setValue(tmpgetNoMedrec);
                                             validasiJenisTr();
                                             /* var tmpkriteria = getCriteriaFilter_viDaftar();
                                             refeshpenjasrad(tmpkriteria); */
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                        validasiJenisTr();
                                                        /* tmpkriteria = getCriteriaFilter_viDaftar();
                                                        refeshpenjasrad(tmpkriteria); */
                                                    }
                                                    else
                                                    Ext.getCmp('txtNoMedrecPenJasRad').setValue('')
                                            }
                                }
                            }

                        }

                    },  
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
                    {
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 40,
                        xtype: 'textfield',
                        name: 'txtNamaPasienPenJasRad',
                        id: 'txtNamaPasienPenJasRad',
                        width: 200,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) 
                                        {
                                            //getCriteriaFilter_viDaftar();
                                            validasiJenisTr();
                                            /* tmpkriteria = getCriteriaFilter_viDaftar();
                                            refeshpenjasrad(tmpkriteria); */

                                            //RefreshDataFilterPenJasRad();

                                        }                       
                                }
                        }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Alamat '
                    },
                    {
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },                  
                    {   
                        x : 155,
                        y : 70,
                        xtype: 'textfield',
                        name: 'txtalamatpasienrad',
                        id: 'txtalamatpasienrad',
                        width: 400,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) 
                                        {
                                            validasiJenisTr();
                                        }                       
                                }
                        }
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Status Lunas '
                    },
                    {
                        x: 145,
                        y: 100,
                        xtype: 'label',
                        text: ':'
                    },                  
                    mComboStatusLunas_viPenJasRad(),
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Tanggal Kunjungan '
                    },
                    {
                        x: 145,
                        y: 130,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 130,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterPenJasRad',
                        format: 'd/M/Y',
                        value: tigaharilalu
                    },
                    {
                        x: 270,
                        y: 130,
                        xtype: 'label',
                        text: 's/d '
                    },
                    {
                        x: 305,
                        y: 130,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterPenJasRad',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 415,
                        y: 130,
                        xtype:'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        anchor: '25%',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function(sm, row, rec)
                        {
                            // kd_unit_konfig = '';
                            validasiJenisTr();
                        }
                    }
                    
                ]
            },
            
        //----------------------------------------------------------------  
            
            {
                columnWidth:.10,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: '100%',
                height: 135,
                anchor: '100% 100%',
                items:
                [
                    
                ]
            },
        //----------------------------------------------------------------    
        ]
    }
    return items;
};


function getTindakanRad(){
    var kd_cus_gettarif=vkode_customerRAD;
    /* if(Ext.get('cboKelPasienRad').getValue()=='Perseorangan'){
        kd_cus_gettarif=Ext.getCmp('cboPerseoranganRad').getValue();
    }else if(Ext.get('cboKelPasienRad').getValue()=='Perusahaan'){
        kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryRad').getValue();
    }else {
        kd_cus_gettarif=Ext.getCmp('cboAsuransiRad').getValue();
      } */
    var modul='';
    if(radiovalues == 1){
        modul='igd';
    } else if(radiovalues == 2){
        modul='rwi';
    } else if(radiovalues == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionRAD/getGridProduk",
            params: {
                     kd_unit:rowSelectedPenJasRad.data.KD_UNIT_ASAL,
                     //kd_unit_asal:tmpkd_unit_asal,
                     kd_customer:kd_cus_gettarif,
                     penjas:modul,
                     kdunittujuan:tmpkd_unit
                    },
            failure: function(o)
            {
                ShowPesanErrorPenJasRad('Error, pasien! Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                
                if (cst.success === true) 
                {
                    //tmpkd_unit_tarif_mir='';
                    var recs=[],
                        recType=dsDataGrd_getTindakanPenjasRad.recordType;
                        
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        
                        recs.push(new recType(cst.ListDataObj[i]));
                        
                    }
                    dsDataGrd_getTindakanPenjasRad.add(recs);
                    GridGetTindakanPenjasRad.getView().refresh();
                    
                    
                    for(var i = 0 ; i < dsDataGrd_getTindakanPenjasRad.getCount();i++)
                    {
                        var o= dsDataGrd_getTindakanPenjasRad.getRange()[i].data;
                        for(var je = 0 ; je < dsTRDetailPenJasRadList.getCount();je++)
                        {
                            var p= dsTRDetailPenJasRadList.getRange()[je].data;
                            if (o.kd_produk === p.kd_produk)
                            {
                                o.pilihchkprodukrad = true;
                                
                                
                            }
                        }
                    }
                    GridGetTindakanPenjasRad.getView().refresh();
                }
                else 
                {
                    ShowPesanErrorPenJasRad('Gagal membaca data pasien ini', 'Error');
                };
            }
        }
        
    )
    
}

function setLookUp_getTindakanPenjasRad(rowdata){
    var lebar = 985;
    setLookUps_getTindakanPenjasRad = new Ext.Window({
        id: 'FormLookUpGetTindakan',
        title: 'Daftar Pemeriksaan', 
        closeAction: 'destroy',        
        width: 600,
        height: 330,
        resizable:false,
        autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,        
        items: getFormItemEntry_getTindakanPenjasRad(lebar,rowdata),
        listeners:{
            activate: function(){
                
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
               // rowSelected_getTindakanPenjasRad=undefined;
            }
        }
    });

    setLookUps_getTindakanPenjasRad.show();
   
}


function getFormItemEntry_getTindakanPenjasRad(lebar,rowdata){
    var pnlFormDataBasic_getTindakanPenjasRad = new Ext.FormPanel({
        title: '',
        region: 'north',
        layout: 'form',
        bodyStyle: 'padding:10px 10px 10px 10px',
        anchor: '100%',
        labelWidth: 1,
        autoWidth: true,
        width: lebar,
        border: false,
        items:[
                gridDataViewEdit_getTindakanPenjasRad()
            ],
            fileUpload: true,
            tbar: {
                xtype: 'toolbar',
                items: 
                [
                    {
                        xtype: 'button',
                        text: 'Tambahkan',
                        id:'btnTambahKanPermintaan_getTindakanPenjasRad',
                        iconCls: 'add',
                        //disabled:true,
                        handler:function()
                        {
                            /* var jumlah=0;
                            var items=[];
                            for(var i = 0 ; i < dsDataGrd_getTindakanPenjasRad.getCount();i++){
                                var o=dsDataGrd_getTindakanPenjasRad.getRange()[i].data;
                                    if(o.pilihchkprodukrad == true){
                                        jumlah += 1;
                                        
                                        // for(var x = 0; x < 100; x++){
                                            // arr[x] = [];    
                                            // for(var y = 0; y < 100; y++){ 
                                                // arr[x][y] = x*y;    
                                            // }    
                                        // }
                                        var recs=[],
                                            recType=dsTRDetailPenJasRadList.recordType;

                                        for(var i=0; i<dsDataGrd_getTindakanPenjasRad.getCount(); i++){

                                            recs.push(new recType(dsDataGrd_getTindakanPenjasRad.data.items[i].data));

                                        }
                                        dsTRDetailPenJasRadList.add(recs);

                                        gridDTItemTest.getView().refresh();
                                    }
                            }
                            console.log(items[0][0]) */
                            
                            
                            
                            
                            var params={};
                            params['jumlah']=dsDataGrd_getTindakanPenjasRad.getCount();
                            console.log(dsDataGrd_getTindakanPenjasRad.getCount());
                            dsTRDetailPenJasRadList.removeAll();
                            var recs=[],
                            recType=dsTRDetailPenJasRadList.recordType;
                            var adadata=false;
                            for(var i = 0 ; i < dsDataGrd_getTindakanPenjasRad.getCount();i++)
                            {
                                console.log(dsDataGrd_getTindakanPenjasRad.data.items[i].data.pilihchkprodukrad);
                                if (dsDataGrd_getTindakanPenjasRad.data.items[i].data.pilihchkprodukrad === true)
                                {
                                    console.log(dsDataGrd_getTindakanPenjasRad.data.items[i].data);
                                    recs.push(new recType(dsDataGrd_getTindakanPenjasRad.data.items[i].data));
                                    adadata=true;
                                }
                            }
                            dsTRDetailPenJasRadList.add(recs);
                            gridDTItemTest.getView().refresh(); 
                            console.log(recs);
                            if (adadata===true)
                            {
                                setLookUps_getTindakanPenjasRad.close(); 
                                loadMask.show();
                                Datasave_PenJasRad(false); 
                                loadMask.hide();
                            }
                            else
                            {
                                ShowPesanWarningPenJasRad('Ceklis data item pemeriksaan  ','Radiologi');
                            }
                            
                        }
                          
                    },
                    
                    
                ]
            }//,items:
        }
    )

    return pnlFormDataBasic_getTindakanPenjasRad;
}

function RefreshDatahistoribayar(no_transaksi)
{
    var strKriteriaKasirRAD = '';

    strKriteriaKasirRAD = 'no_transaksi= ~' + no_transaksi + '~ and kd_kasir= ~'+kdkasir_tmp+'~';

    dsTRDetailHistoryList_rad.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewHistoryBayar',
                                    param: strKriteriaKasirRAD
                                }
                    }
            );
    return dsTRDetailHistoryList_rad;
}
;

function GetDTLTRHistoryGrid()
{

    var fldDetailRAD = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'SHIFT', 'KD_USER'];

    dsTRDetailHistoryList_rad = new WebApp.DataStore({fields: fldDetailRAD})

    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'History Bayar',
                        stripeRows: true,
                        store: dsTRDetailHistoryList_rad,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100% 25%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
                                                            Ext.getCmp('btnHpsBrsKasirRAD').enable();
                                                            console.log(row);
                                                            cellSelecteddeskripsi = dsTRDetailHistoryList_rad.getAt(row);
                                                            CurrentHistory.row = row;
                                                            CurrentHistory.data = cellSelecteddeskripsi;
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRHistoryColumModel(),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnHpsBrsKasirRAD',
                                        text: 'Hapus Pembayaran',
                                        tooltip: 'Hapus Baris',
                                        iconCls: 'RemoveRow',
                                        //hidden :true,
                                        handler: function ()
                                        {
                                            if (dsTRDetailHistoryList_rad.getCount() > 0)
                                            {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                    if (CurrentHistory != undefined)
                                                    {
                                                        HapusBarisDetailbayar();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningPenJasRad('Pilih record ', 'Hapus data');
                                                }
                                            }
                                            /* Ext.getCmp('btnEditKasirRAD').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirRAD').disable();
                                            Ext.getCmp('btnHpsBrsKasirRAD').disable(); */
                                        },
                                        disabled: true
                                    }
                                ]
                    }

            );

    return gridDTLTRHistory;
}
;
function HapusBarisDetailbayar()
{
    if (cellSelecteddeskripsi != undefined)
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
            //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
            /*var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
             if (btn == 'ok')
             {
             variablehistori=combo;
             if (variablehistori!='')
             {
             DataDeleteKasirRADKasirDetail();
             }
             else
             {
             ShowPesanWarningKasirRAD('Silahkan isi alasan terlebih dahaulu','Keterangan');
             }
             }  
             });  */
            msg_box_alasanhapus_RAD();
        } else
        {
            dsTRDetailHistoryList.removeAt(CurrentHistory.row);
        }
        ;
    }
}
;
function msg_box_alasanhapus_RAD()
{
    var lebar = 250;
    form_msg_box_alasanhapus_RAD = new Ext.Window
            (
                    {
                        id: 'alasan_hapusRad',
                        title: 'Alasan Hapus Pembayaran',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    //fieldLabel: 'No. Transaksi ',
                                                    name: 'txtAlasanHapusRAD',
                                                    id: 'txtAlasanHapusRAD',
                                                    emptyText: 'Alasan Hapus',
                                                    anchor: '99%',
                                                },
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'hapus',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_hapusRad',
                                                                    handler: function ()
                                                                    {
                                                                        //alert(kdkasir);
                                                                        DataDeleteKasirRADKasirDetail();
                                                                        form_msg_box_alasanhapus_RAD.close();
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_hapusRad',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanhapus_RAD.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanhapus_RAD.show();
}
;

function mComboalasan_hapusRad()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_hapusRad = new WebApp.DataStore({fields: Field});

    dsalasan_hapusRad.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'alasanhapus',
                                    param: ''
                                }
                    }
            );
    var cboalasan_hapusRad = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_hapusRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Alasan...',
                        labelWidth: 1,
                        store: dsalasan_hapusRad,
                        valueField: 'KD_ALASAN',
                        displayField: 'ALASAN',
                        anchor: '95%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
//                                  var selectalasan_hapusRad = b.data.KD_PROPINSI;
                                        //alert("is");
                                        selectDokter = b.data.KD_DOKTER;
                                    },
                                    'render': function (c)
                                    {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('kelPasien').focus();
                                        }, c);
                                    }
                                }
                    }
            );

    return cboalasan_hapusRad;
}
;
function DataDeleteKasirRADKasirDetail()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/deletedetail_bayar",
                        params: getParamDataDeleteKasirRADKasirDetail(),
                        success: function (o)
                        {
                            //  RefreshDatahistoribayar(Kdtransaksi);
                           // RefreshDataFilterKasirRADKasir();
                            //RefreshDatahistoribayar('0');
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoPenJasRad("Proses Hapus Pembayaran Berhasil", nmHeaderHapusData);
                                Ext.getCmp('btnPembayaranPenjasRAD').enable();
                                RefreshDatahistoribayar(Ext.getCmp('txtNoTransaksiPenJasRad').getValue());
                                Ext.getCmp('btnTutupTransaksiPenjasRAD').enable();
                                validasiJenisTr();
                                //alert(Kdtransaksi);                    

                                //refeshKasirRADKasir();
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningPenJasRad(nmPesanHapusGagal, nmHeaderHapusData);
                            }else  if (cst.success === false && cst.type === 6)
                            {
                                ShowPesanWarningPenJasRad("Transaksi sudah ditutup.", nmHeaderHapusData);
                            } else
                            {
                                ShowPesanWarningPenJasRad(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirRADKasirDetail()
{
    var params =
            {
                TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
                TrTglbayar: CurrentHistory.data.data.TGL_BAYAR,
                Urut: CurrentHistory.data.data.URUT,
                Jumlah: CurrentHistory.data.data.BAYAR,
                Username: CurrentHistory.data.data.USERNAME,
                Shift: tampungshiftsekarang,
                Shift_hapus: CurrentHistory.data.data.SHIFT,
                Kd_user: CurrentHistory.data.data.KD_USER,
                Tgltransaksi: TglTransaksi,
                Kodepasein: kodepasien,
                NamaPasien: namapasien,
                KodeUnit: kodeunit,
                Namaunit: namaunit,
                Kodepay: kodepay,
                Uraian: CurrentHistory.data.data.DESKRIPSI,
                KDkasir: kdkasir,
                KeTterangan: Ext.get('txtAlasanHapusRAD').dom.value

            };
    Kdtransaksi = CurrentHistory.data.data.NO_TRANSAKSI;
    return params
}
;


function TRHistoryColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Bayar',
                            dataIndex: 'URUT',
                            //hidden:true

                        },
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        },
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }
                        },
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colStatHistory',
                            header: 'History',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colPetugasHistory',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME',
                            //hidden:true
                        }
                    ]
                    )
}
;
//var a={};
function gridDataViewEdit_getTindakanPenjasRad(){
    var FieldGrdKasir_getTindakanPenjasRad = [];
    dsDataGrd_getTindakanPenjasRad= new WebApp.DataStore({
        fields: FieldGrdKasir_getTindakanPenjasRad
    });
    chkgetTindakanPenjasRad = new Ext.grid.CheckColumn
        (
            {
                
                id: 'chkgetTindakanPenjasRad',
                header: 'Pilih',
                align: 'center',
                //disabled:false,
                sortable: true,
                dataIndex: 'pilihchkprodukrad',
                anchor: '10% 100%',
                width: 30,
                listeners: 
                {
                    checkchange: function()
                    {
                        alert('hai');
                    },
                }
                /* renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    switch (value) {
                        case 't':
                            alert('hai');
                            return true;
                            
                            break;
                        case 'f':
                            alert('fei');
                            return false;
                            break;
                    }
                    return false;
                } */  
        
            }
        ); 
    GridGetTindakanPenjasRad =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
        //title: 'Dafrar Pemeriksaan',
        store: dsDataGrd_getTindakanPenjasRad,
        autoScroll: true,
        columnLines: true,
        border: false,
        height: 250,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
                (
                        {
                            singleSelect: true,
                            listeners:
                                    {
                                    }
                        }
                ),
        listeners:
                {
                    // Function saat ada event double klik maka akan muncul form view
                    rowclick: function (sm, ridx, cidx)
                    {
                        
                    },
                    rowdblclick: function (sm, ridx, cidx)
                    {
                        
                    }

                },
        /* xtype: 'editorgrid',
        store: dsDataGrd_getTindakanPenjasRad,
        height: 250,
        autoScroll: true,
        columnLines: true,
        border: false,
        plugins: [ new Ext.ux.grid.FilterRow(),chkgetTindakanPenjasRad ],
        selModel: new Ext.grid.RowSelectionModel ({
            singleSelect: true,
            listeners:{
                rowclick: function(sm, row, rec){
                    /* rowSelectedGridPasien_viGzPermintaanDiet = undefined;
                    rowSelectedGridPasien_viGzPermintaanDiet = dsDataGrdPasien_viGzPermintaanDiet.getAt(row);
                    CurrentDataPasien_viGzPermintaanDiet
                    CurrentDataPasien_viGzPermintaanDiet.row = row;
                    CurrentDataPasien_viGzPermintaanDiet.data = rowSelectedGridPasien_viGzPermintaanDiet.data;
                    
                    dsTRDetailDietGzPermintaanDiet.removeAll();
                    getGridWaktu(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
                    
                    if(GzPermintaanDiet.vars.status_order == 'false'){
                        Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
                        Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
                    } else{
                        Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
                        Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
                    }
                    Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue(CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
                     
                },
            }
        }), */
        //stripeRows: true,
        
        colModel:new Ext.grid.ColumnModel([
            //new Ext.grid.RowNumberer(),   
            {
                dataIndex: 'kp_produk',
                header: 'Kode Produk',
                sortable: true,
                width: 70,
                 filter: {}
            },
            {
                dataIndex: 'deskripsi',
                header: 'Nama Pemeriksaan',
                width: 70,
                align:'left',
                 filter: {}
            },{
                dataIndex: 'uraian',
                header: 'Uraian',
                hidden : true,
                sortable: true,
                width: 70
            },
            {
                dataIndex: 'kd_tarif',
                header: 'Kd Tarif',
                width: 70,
                hidden : true,
                align:'center'
            },{
                dataIndex: 'tgl_transaksi',
                header: 'Tgl Transaksi',
                hidden : true,
                sortable: true,
                width: 70
            },
            {
                dataIndex: 'tgl_berlaku',
                header: 'Tgl Berlaku',
                width: 70,
                hidden : true,
                align:'center'
            },{
                dataIndex: 'harga',
                header: 'Harga',
                hidden : false,
                sortable: true,
                width: 70
            },
            {
                dataIndex: 'qty',
                header: 'qty',
                width: 70,
                hidden : true,
                align:'center'
            },{
                dataIndex: 'jumlah',
                header: 'jumlah',
                width: 70,
                hidden : true,
                align:'center'
            },
            chkgetTindakanPenjasRad
        ]),
        viewConfig:{
            forceFit: true
        } 
    });
    return GridGetTindakanPenjasRad;
}

function getDokterPengirim(no_transaksi,kd_kasir){
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionRAD/getDokterPengirim",
            params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
            failure: function(o)
            {
                ShowPesanErrorPenJasRad('Hubungi Admin', 'Error');
            },
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    Ext.getCmp('txtDokterPengirimL').setValue(cst.nama);
                }
                else
                {
                    ShowPesanErrorPenJasRad('Gagal membaca dokter pengirim', 'Error');
                };
            }
        }

    )
}

function printbillRadRad()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakbill_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarning_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            } else
                            {
                                ShowPesanError_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            }
                        }
                    }
            );
}
;

var tmpkdkasirRadLab = '1';
function dataparamcetakbill_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectPrintingRadAJA',
                notrans_asal:notransaksiasal_rad,
                No_TRans: Ext.get('txtNoTransaksiKasirRADKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'rad',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirRAD').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirRAD').getValue(),
                TlpPasien: Ext.get('txtnotlprad').getValue(),
                tgl_bayar: Ext.get('dtpTanggalBayarDetransaksiRAD').getValue(),
                
            };
    var urut=[];
    var kd_produk=[];
    var items=Ext.getCmp('gridDTLTRKasirRAD').getStore().data.items;
    for(var i=0,iLen=items.length; i<iLen; i++){
        if(items[i].data.TAG==true){
            urut.push(items[i].data.URUT);
            kd_produk.push(items[i].data.KD_PRODUK);
        }
    }
    paramscetakbill_vikasirDaftarRadLab['no_urut[]']=urut;
    paramscetakbill_vikasirDaftarRadLab['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarRadLab;
}
;

function printkwitansiRadRad()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakkwitansi_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan !== '')
                            {
                                ShowPesanWarningPenJasRad('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanWarningPenJasRad('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            );
}
;

function dataparamcetakkwitansi_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectKwitansiRadAJA',
                No_TRans: Ext.get('txtNoTransaksiKasirRADKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'rad',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirRAD').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirRAD').getValue()
            };
    return paramscetakbill_vikasirDaftarRadLab;
}
;

function getParamHapusDetailTransaksiRAD() 
{   
    var tmpparam = cellSelecteddeskripsi.data;
    var params =
    {       
        no_tr: tmpparam.no_transaksi,
        urut:tmpparam.urut,
        tgl_transaksi:tmpparam.tgl_transaksi
    };
    return params
};

function Datasave_PenJasRad_SQL(mBol) 
{   
     if (ValidasiEntryPenJasRad(nmHeaderSimpanData,false) == 1 )
     {
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/CreateDataObj",
                    params: getParamDetailTransaksiRAD_SQL(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasLab').dom.value,kd_kasir_rad);
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                        }
                        else 
                        {
                                ShowPesanWarningPenJasRad('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

function getParamDetailTransaksiRAD_SQL() 
{
    
    var KdCust='';
    var TmpCustoLama='';
    var pasienBaru;
    var modul='';
    if(radiovalues == 1){
        modul='igd';
    } else if(radiovalues == 2){
        modul='rwi';
    } else if(radiovalues == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    
    if(Ext.getCmp('txtNoMedrecL').getValue() == ''){
        pasienBaru=1;
    } else{
        pasienBaru=0;
    }
    
    var params =
    {
        Table: 'SQL_RAD',
        KdTransaksi: Ext.get('txtNoTransaksiPenJasRad').getValue(),
        KdPasien:Ext.getCmp('txtNoMedrecL').getValue(),
        NmPasien:Ext.getCmp('txtNamaPasienL').getValue(),
        Ttl:Ext.getCmp('dtpTtlL').getValue(),
        Alamat:Ext.getCmp('txtAlamatL').getValue(),
        JK:Ext.getCmp('cboJK').getValue(),
        GolDarah:Ext.getCmp('cboGDR').getValue(),
        KdUnit: Ext.getCmp('txtKdUnitRad').getValue(),
        KdDokter:tmpkddoktertujuan,
        KdUnitTujuan:tmpkd_unit,
        Tgl: Ext.getCmp('dtpKunjunganL').getValue(),
        TglTransaksiAsal:TglTransaksi,
        urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
        KdCusto:vkode_customerRAD,
        TmpCustoLama:vkode_customerRAD,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
        NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiRad').getValue(),
        NoAskes:Ext.getCmp('txtNoAskesRad').getValue(),
        NoSJP:Ext.getCmp('txtNoSJPRad').getValue(),
        Shift:tampungshiftsekarang,
        List:getArrPenJasRad(),
        JmlField: mRecordRad.prototype.fields.length-4,
        JmlList:GetListCountDetailTransaksiRAD(),
        dtBaru:databaru,
        Hapus:1,
        Ubah:0,
        unit:41,
        TmpNotransaksi:TmpNotransaksi,
        KdKasirAsal:KdKasirAsal,
        KdSpesial:Kd_Spesial,
        Kamar:No_Kamar,
        pasienBaru:pasienBaru,
        listTrDokter : '',
        Modul:modul,
        KdProduk:'',
        URUT :tmp_urut_sql,
        kunjungan_rad:tmppasienbarulama
        
    };
    return params
};

function Delete_Detail_Rad_SQL(){
    Ext.Ajax.request
     (
        {
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteRadDetail(),
            failure: function(o)
            {
                ShowPesanWarningPenJasRad('Error Hapus. Hubungi Admin!', 'Gagal');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    // ShowPesanInfoPenJasRad('Data Berhasil Di hapus', 'Sukses');
                    // dsTRDetailPenJasRadList.removeAt(line);
                    // gridDTItemTest.getView().refresh();
                    // Delete_Detail_Rad_SQL();
                }
                else 
                {
                        ShowPesanWarningPenJasRad('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
                };
            }
        }
    )
}

function getParamDataDeleteRadDetail(){
    var tmpparam = cellSelecteddeskripsi.data;
    var params =
    {       
        Table: 'SQL_RAD',
        no_tr: tmpparam.no_transaksi,
        urut:tmpparam.urut,
        tgl_transaksi:tmpparam.tgl_transaksi     
    };
    return params

}
var tmpunitfotorad;
var tmpurutfotorad;
var tmptglfotorad;
function PenJasRadUpdateFotoLookUp(rowdata) 
{
    var lebar = 275;
    FormPenJasRadUpdateFotoLookUp = new Ext.Window
    (
        {
            id: 'windowPenJasRadUpdateFotoLookUp',
            title: 'Update Foto Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 190,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: [formpopupUpdateFotoRad()],
            listeners:
            {
                 activate: function()
                {
                    if (rowdata === undefined){

                    }else{
                        // console.log(rowdata);
                        tmpunitfotorad = rowdata.KD_UNIT;
                        tmptglfotorad = rowdata.TGL_TRANSAKSI;
                        tmpurutfotorad = rowdata.URUT;
                        var tmpdate = new Date(rowdata.TGL_TRANSAKSI);
                        var realdate = tmpdate.format("d/M/Y");
                        Ext.getCmp('TxtPopupMedrecRad').setValue(rowdata.KD_PASIEN);
                        Ext.getCmp('TxtPopupNamaPasien').setValue(rowdata.NAMA);
                        Ext.getCmp('TxtPopupTglKunjungPasien').setValue(realdate);
                        Ext.getCmp('TxtPopupNoFotoPasien').setValue(rowdata.NO_FOTO)
                    }
                    
                    
                }
            }
        }
    );

    FormPenJasRadUpdateFotoLookUp.show();
    // if (rowdata === undefined) 
    // {
    //     // PenjasRadAddNew();
    // }
    // else 
    // {
    //     // TRPenjasRadInit(rowdata);
    // }

};

function formpopupUpdateFotoRad() {
    var FrmTabs_popupdatahasilrad = new Ext.Panel
            (
                    {
                        id: 'formpopupUpdateFotoRad',
                        closable: true,
                        region: 'center',
                        layout: 'column',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: false,
                        shadhow: true,
                        margins: '5 5 5 5',
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelUpdateFotoRad()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_RAD',
                                                    // disabled: true,
                                                    handler: function ()
                                                    {
                                                        if (Ext.getCmp('TxtPopupNoFotoPasien').getValue() !== '') {
                                                            UpdateFotoRad();
                                                        }else{
                                                            ShowPesanWarningPenJasRad('No. Foto Tidak Boleh Kosong', 'Warning');
                                                        }
                                                        
                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupdatahasilrad;
}
;

function PanelUpdateFotoRad() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: false,
                width: 450,
                height: 150,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec'
                    },
                    {
                        x: 90,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtPopupMedrecRad',
                        id: 'TxtPopupMedrecRad',
                        disable:true,
                        width: 80
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
                    {
                        x: 90,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtPopupNamaPasien',
                        id: 'TxtPopupNamaPasien',
                        disable:true,
                        width: 150
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Tgl. Kunjungan '
                    },
                    {
                        x: 90,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtPopupTglKunjungPasien',
                        id: 'TxtPopupTglKunjungPasien',
                        disable:true,
                        width: 80
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'No. Foto '
                    },
                    {
                        x: 90,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 100,
                        xtype: 'textfield',
                        name: 'TxtPopupNoFotoPasien',
                        id: 'TxtPopupNoFotoPasien',
                        width: 80
                    },
                ]
            }
        ]
    };
    return items;
}
;

function UpdateFotoRad(){
    Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/functionRAD/updatefotorad",
                    params: getParamUpdateFotoRAD(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            ShowPesanInfoPenJasRad("Berhasil menyimpan data ini","Information");
                        }
                        else 
                        {
                                ShowPesanWarningPenJasRad('Data Gagal Di Simpan', 'Gagal');
                        };
                    }
                }
            )
};

function getParamUpdateFotoRAD(){
    var params =
            {
                kdpasien: Ext.getCmp('TxtPopupMedrecRad').getValue(),
                kdunit: tmpunitfotorad,
                tgl: tmptglfotorad,
                urut: tmpurutfotorad,
                nofoto: Ext.getCmp('TxtPopupNoFotoPasien').getValue()
            };
    return params
}

function TransferData_rad_SQL(){
    Ext.Ajax.request
        (
            {
                url: baseURL + "index.php/rad_sql/function_rad_sql/saveTransfer",
                params: getParamTransferRwi_dari_rad(),
                failure: function (o)
                {

                    ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                    // ViewGridBawahLookupPenjasRad();
                },
                success: function (o)
                {   
                
                    
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {   
                        // TransferData_rad_SQL();
                        // Ext.getCmp('btnSimpanKasirRAD').disable();
                        // Ext.getCmp('btnTransferKasirRAD').disable();
                        // ShowPesanInfoPenJasRad('Transfer Berhasil', 'transfer ');
                        // RefreshDatahistoribayar(Ext.getCmp('txtNoTransaksiPenJasRad').getValue());
                        // FormLookUpsdetailTRTransfer_Rad.close();
                        
                    } else
                    {
                        ShowPesanWarningPenJasRad('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                    }
                    ;
                }
            }
        )
}
var tmpnotransPF;
var tmptgltransPF;
var tmpuruttransPF;
function PenJasRadPemakianFilmLookUp(rowdata) 
{
    var lebar = 500;
    FormPenJasRadPemakianFilmLookUp = new Ext.Window
    (
        {
            id: 'windowFormPenJasRadPemakianFilmLookUp',
            title: 'Pemakaian Film',
            closeAction: 'destroy',
            width: lebar,
            height: 400,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: [formpopupPemakaianFilmRad()],
            listeners:
            {
                 activate: function()
                {
                    if (rowdata === undefined){

                    }else{
                        loadFilmpenjasrad(rowdata);
                        tmpnotransPF = rowdata.no_transaksi;
                        tmptgltransPF = rowdata.tgl_transaksi;
                        tmpuruttransPF = rowdata.urut;
                        var tmpkode_produkPF = rowdata.kd_produk + '-' + rowdata.deskripsi;
                        Ext.getCmp('TxtPopupTindakanPFRad').setValue(tmpkode_produkPF);
                        Ext.getCmp('TxtPopupTindakanPFRad').disable();
                    }
                    
                    
                }
            }
        }
    );

    FormPenJasRadPemakianFilmLookUp.show();
};

function formpopupPemakaianFilmRad() {
    var Field = ['NAMA_Radiologi','KD_PRODUK','HARGA','QTY','QTY_RSK','QTY_STD','MAX_QTY','JNS_BHN'];
        dtsgridpemakaianfilm = new WebApp.DataStore({ fields: Field });
        
        //  var k="tr.tgl_transaksi >='"+tigaharilaluNew+"' and tr.tgl_transaksi <='"+tglGridBawah+"'    and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc";
        
        // refeshpenjasrad(k);

        gridpemakaianfilm = new Ext.grid.EditorGridPanel
        (
            {
                stripeRows: true,
                id:'PenjasRad',
                store: dtsgridpemakaianfilm,
                anchor: '100% 100%',
                height:'300',
                columnLines: false,
                autoScroll:true,
                border: false,
                editable:true,
                sort :false,
                sm: new Ext.grid.RowSelectionModel
                (
                    {
                        singleSelect: true,
                        listeners:
                        {
                            rowselect: function(sm, row, rec)
                            {
                                
                                
                            }
                        }
                    }
                ),
            cm: new Ext.grid.ColumnModel
                (
                    [
                       
                        {
                            id: 'colkdfilmpemakaianfilm',
                            header: 'Kd. Film',
                            dataIndex: 'KD_PRODUK',
                            sortable: false,
                            hideable:false,
                            menuDisabled:true,
                            width: 50
                        },
                        {
                            id: 'coluraianpemakaianfilm',
                            header: 'Uraian',
                            dataIndex: 'NAMA_Radiologi',
                            sortable: false,
                            hideable:false,
                            menuDisabled:true,
                            width: 100,
                        },
                        {
                            id: 'coljmlpemakaianfilm',
                            header: 'Jml',
                            dataIndex: 'QTY',
                            width: 50,
                            sortable: false,
                            hideable:false,
                            menuDisabled:true,
                            editor:{
                                xtype:'textfield'
                            }                        
                        },
                        {
                            id: 'coljmlrskpemakaianfilm',
                            header: 'jml. Rsk',
                            dataIndex: 'QTY_RSK',
                            width: 50,
                            sortable: false,
                            hideable:false,
                            menuDisabled:true,
                            editor:{
                                xtype:'textfield'
                            }                          
                        },
                       
                    ]
                ),
                viewConfig: {forceFit: true},
                tbar:
                    [
                        {
                            id: 'btnEditRWJ',
                            text: 'Save & Exit',
                            tooltip: nmEditData,
                            iconCls: 'Save',
                            handler: function(sm, row, rec)
                            {
                                loadMask.show();
                                DataUpdate_PemakaianFotoPenJasRad();
                            }
                        }
                    ]
                }
        );
    var FrmTabs_formpopupPemakianFilmRad = new Ext.Panel
            (
                    {
                        id: 'FrmTabs_formpopupPemakianFilmRad',
                        closable: true,
                        region: 'center',
                        layout: 'column',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        // margins: '5 5 5 5',
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelPemakaianFilmRad(),gridpemakaianfilm
                                ]
                    }
            );
    return FrmTabs_formpopupPemakianFilmRad;
}
;
var gridpemakaianfilm;
var dtsgridpemakaianfilm;
function PanelPemakaianFilmRad() {
        
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: true,
                width: 500,
                height: 30,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 7,
                        xtype: 'label',
                        text: 'Tindakan '
                    },
                    {
                        x: 90,
                        y: 7,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 5,
                        xtype: 'textfield',
                        name: 'TxtPopupTindakanPFRad',
                        id: 'TxtPopupTindakanPFRad',
                        editable:false,
                        width: 300
                    },
                ]
            }
        ]
    };
    return items;
}
;

function loadFilmpenjasrad(rowdata)
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionRAD/getdatafoto",
            params: getParamRefreshlistfotoRad(rowdata),
            failure: function(o)
            {
                ShowPesanWarningPenJasRad('Hubungi Admin', 'Error');
            },
            success: function(o)
            {   
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    dtsgridpemakaianfilm.removeAll();
                    var recs=[],
                    recType=dsTRPenJasRadList.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    dtsgridpemakaianfilm.add(recs);
                    gridpemakaianfilm.getView().refresh();

                }
                else
                {
                    ShowPesanWarningPenJasRad('Gagal membaca Data ini', 'Error');
                };
            }
        }

    );
}

function getParamRefreshlistfotoRad(rowdata){

    // var kriteria = "WHERE dfo.kd_kasir = '"+ rowdata. +"' AND dfo.no_transaksi = '4603972' AND dfo.tgl_transaksi = '2017-01-18' AND dfo.urut = 1";
    var params =
    {
        kd_kasir:'',
        notrans:rowdata.no_transaksi,
        tgltransaksi:rowdata.tgl_transaksi,
        urut:rowdata.urut       
    }
    return params;
}

function getArrfotoPenJasRad()
{
    var x='';
    var arr=[];
    for(var i = 0 ; i < dtsgridpemakaianfilm.getCount();i++)
    {
        var o={};
        var y='';
        var z='@@##$$@@';
        o['KD_PRD_RAD_FO']= dtsgridpemakaianfilm.data.items[i].data.KD_PRODUK;
        o['QTY_RAD_FO']= dtsgridpemakaianfilm.data.items[i].data.QTY;
        o['QTY_RSK_RAD_FO']= dtsgridpemakaianfilm.data.items[i].data.QTY_RSK;
        arr.push(o);
        
    }   
    
    return Ext.encode(arr);
};

function DataUpdate_PemakaianFotoPenJasRad() 
{   
    Ext.Ajax.request
     (
        {
            url: baseURL + "index.php/main/functionRAD/UpdatePemakaianFotoRad",
            params: getParamDetailPemakaianFotoRAD(),
            failure: function(o)
            {
                loadMask.hide();
                ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    loadMask.hide();
                    //ShowPesanInfoPenJasRad("Berhasil menyimpan data ini","Information");
                    FormPenJasRadPemakianFilmLookUp.close();
                    // Ext.get('txtNoTransaksiPenJasRad').dom.value=cst.notrans;
                    // Ext.get('txtNoMedrecL').dom.value=cst.kdPasien;
                    // ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value);
                    // Ext.getCmp('btnPembayaranPenjasRAD').enable();
                    // Ext.getCmp('btnTutupTransaksiPenjasRAD').enable();
                    // Ext.getCmp('txtnoregrad').setValue(cst.noreg);
                    // if(mBol === false)
                    // {
                    //     ViewGridBawahLookupPenjasRad(Ext.get('txtNoTransaksiPenJasRad').dom.value);
                    // };

                    // tmpkd_pasien_sql = cst.kdPasien;
                    // tmpno_trans_sql = cst.notrans;
                    // tmpkd_kasir_sql = cst.kdkasir;
                    // tmp_tgl_sql = cst.tgl;
                    // tmp_urut_sql = cst.urut;
                    // tmpurut = cst.urut;
                   // DataUpdate_PemakaianFotoPenJasRad_SQL();
                }
                else 
                {
                    loadMask.hide();
                    ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                };
            }
        }
    )
};

function getParamDetailPemakaianFotoRAD() 
{
    
    var params =
    {        
        notrans: tmpnotransPF,
        tgltrans:tmptgltransPF,
        uruttrans:tmpuruttransPF,
        List:getArrfotoPenJasRad(),
        
    };
    return params
};

function DataUpdate_PemakaianFotoPenJasRad_SQL(){
    Ext.Ajax.request
        (
            {
                url: baseURL + "index.php/rad_sql/function_rad_sql/UpdatePemakaianFotoRad",
                params: getParamDetailPemakaianFotoRAD(),
                failure: function (o)
                {

                    ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                },
                success: function (o)
                {   
                
                    
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {   
                        
                    } else
                    {
                        ShowPesanWarningPenJasRad('Error simpan. Hubungi Admin!', 'Gagal');
                    }
                    ;
                }
            }
        )
}

function setUsia(Tanggal)
{
    Ext.Ajax.request
            ({
                url: baseURL + "index.php/main/GetUmur",
                params: {
                    TanggalLahir: Tanggal
                },
                success: function (o)
                {
                    var tmphasil = o.responseText;
                    var tmp = tmphasil.split(' ');

                    if (tmp.length === 6)
                    {
                        Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[4]);
                    } else if (tmp.length === 4) {
                        if (tmp[1] === 'years') {
                            if (tmp[3] === 'day') {
                                Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                            } else {
                                Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                            }
                        } else {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                        }
                    } else if (tmp.length === 2) {
                        if (tmp[1] === 'year') {
                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'years') {
                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mon') {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mons') {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[0]);
                        }
                    } else if (tmp.length === 1) {
                        Ext.getCmp('txtUmurRad').setValue('0');
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue('1');
                    } else {
                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                    }
                }
            });
}

/*----------------------------------------------------Tambah Baris Item Test---------------------------------------------------------
 Oleh     : HDHT 
 Tanggal  : 01-februari-2017
 BANDUNG 
 */

function TambahBarisRAD()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRAD();
        dsTRDetailPenJasRadList.insert(dsTRDetailPenJasRadList.getCount(), p);
    };
};
/*----------------------------------------------------Ganti Kelompok Pasien---------------------------------------------------------
 Oleh     : HDHT 
 Tanggal  : 21-februari-2017
 MADIUN 
 */

var jeniscus_rad;
var labelisi_rad;
var panelActiveDataPasien;
function KelompokPasienLookUp_rad(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien_rad = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien_rad(lebar,rowdata),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien_rad.show();
    KelompokPasienbaru_rad(rowdata);

};

function KelompokPasienbaru_rad(rowdata) 
{
    jeniscus_rad=0;
    KelompokPasienAddNew_rad = true;
    Ext.getCmp('cboKelompokpasien_rad').show()
    Ext.getCmp('txtCustomer_radLama').disable();
    Ext.get('txtCustomer_radLama').dom.value=rowdata.CUSTOMER;
    Ext.get('txtRWJNoSEP').dom.value = rowdata.SJP;
    console.log(rowdata);
    // Ext.getCmp('txtRWJNoSEP').disable();
    // Ext.getCmp('txtRWJNoAskes').disable();
    
    RefreshDatacombo_rad(jeniscus_rad,rowdata.KELPASIEN);
};

function getFormEntryTRKelompokPasien_rad(lebar,rowdata) 
{
    var pnlTRKelompokPasien_rad = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien_rad',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
                    getItemPanelInputKelompokPasien_rad(lebar),
                    getItemPanelButtonKelompokPasien_rad(lebar,rowdata)
            ],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasien = new Ext.Panel
    (
        {
            id: 'FormDepanKelompokPasien',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: true,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: [pnlTRKelompokPasien_rad 
                
            ]

        }
    );

    return FormDepanKelompokPasien
};

function getItemPanelInputKelompokPasien_rad(lebar) 
{
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding:10px 10px 10px 0px',
        border:false,
        height:170,
        items:
        [
            {
                columnWidth: .9,
                width: lebar -35,
                labelWidth:100,
                layout: 'form',
                border: false,
                items:
                [
                    getKelompokpasienlama_rad(lebar),   
                    getItemPanelNoTransksiKelompokPasien_rad(lebar) ,
                    
                ]
            }
        ]
    };
    return items;
};

function getKelompokpasienlama_rad(lebar) 
{
    var items =
    {
        Width:lebar,
        height:40,
        layout: 'column',
        border: false,
        
        items:
        [
            {
                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                items:
                [{   
                        xtype: 'tbspacer',
                        height: 2
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kelompok Pasien Asal',
                            name: 'txtCustomer_radLama',
                            id: 'txtCustomer_radLama',
                            labelWidth:130,
                            editable: false,
                            width: 100,
                            anchor: '95%'
                         }
                    ]
            }
            
        ]
    }
    return items;
};

function getItemPanelNoTransksiKelompokPasien_rad(lebar) 
{
    var items =
    {
        Width:lebar,
        height:120,
        layout: 'column',
        border: false,
        
        
        items:
        [
            {

                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                items:
                [{   
                        xtype: 'tbspacer',
                        height:3
                    },{ 
                        xtype: 'combo',
                        fieldLabel: 'Kelompok Pasien Baru',
                        id: 'kelPasien_rad',
                        editable: false,
                        store: new Ext.data.ArrayStore
                            (
                                {
                                id: 0,
                                fields:
                                [
                                'Id',
                                'displayText'
                                ],
                                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                }
                            ),
                              displayField: 'displayText',
                              mode: 'local',
                              width: 100,
                              forceSelection: true,
                              triggerAction: 'all',
                              emptyText: 'Pilih Salah Satu...',
                              selectOnFocus: true,
                              anchor: '95%',
                              listeners:
                                 {
                                        'select': function(a, b, c)
                                    {
                                        if(b.data.displayText =='Perseorangan')
                                        {jeniscus_rad='0'
                                            Ext.getCmp('txtRWJNoSEP').disable();
                                            Ext.getCmp('txtRWJNoAskes').disable();
                                            }
                                        else if(b.data.displayText =='Perusahaan')
                                        {jeniscus_rad='1';
                                            Ext.getCmp('txtRWJNoSEP').disable();
                                            Ext.getCmp('txtRWJNoAskes').disable();
                                            }
                                        else if(b.data.displayText =='Asuransi')
                                        {jeniscus_rad='2';
                                            Ext.getCmp('txtRWJNoSEP').enable();
                                            Ext.getCmp('txtRWJNoAskes').enable();
                                        }
                                        
                                        RefreshDatacombo_rad(jeniscus_rad);
                                    }

                                }
                        },{
                            columnWidth: .990,
                            layout: 'form',
                            border: false,
                            labelWidth:130,
                            items:
                            [
                                                mComboKelompokpasien_rad()
                            ]
                        },{
                            xtype: 'textfield',
                            fieldLabel:'No SEP  ',
                            name: 'txtRWJNoSEP',
                            id: 'txtRWJNoSEP',
                            width: 100,
                            anchor: '99%'
                         }, {
                             xtype: 'textfield',
                            fieldLabel:'No Asuransi  ',
                            name: 'txtRWJNoAskes',
                            id: 'txtRWJNoAskes',
                            width: 100,
                            anchor: '99%'
                         }
                                    
                ]
            }
            
        ]
    }
    return items;
};

function mComboKelompokpasien_rad()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viPJ_RAD = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    if (jeniscus_rad===undefined || jeniscus_rad==='')
    {
        jeniscus_rad=0;
    }
    ds_customer_viPJ_RAD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_rad +'~'
            }
        }
    )
    var cboKelompokpasien_rad = new Ext.form.ComboBox
    (
        {
            id:'cboKelompokpasien_rad',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih...',
            fieldLabel: labelisi_rad,
            align: 'Right',
            anchor: '95%',
            store: ds_customer_viPJ_RAD,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetKelompokpasien=b.data.displayText ;
                    selectKdCustomer=b.data.KD_CUSTOMER;
                    selectNamaCustomer=b.data.CUSTOMER;
                
                }
            }
        }
    );
    return cboKelompokpasien_rad;
};


function RefreshDatacombo_rad(jeniscus_rad,vkode_customerRAD_rad) 
{

    ds_customer_viPJ_RAD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_rad +'~ and kontraktor.kd_customer not in(~'+ vkode_customerRAD_rad+'~)'
            }
        }
    )
    
    return ds_customer_viPJ_RAD;
};

function getItemPanelButtonKelompokPasien_rad(lebar,rowdata) 
{
    var items =
    {
        layout: 'column',
        border: false,
        height:30,
        anchor:'100%',
        style:{'margin-top':'-1px'},
        items:
        [
            {
                layout: 'hBox',
                width:400,
                border: false,
                bodyStyle: 'padding:5px 0px 5px 5px',
                defaults: { margins: '3 3 3 3' },
                anchor: '90%',
                layoutConfig: 
                {
                    align: 'middle',
                    pack:'end'
                },
                items:
                [
                    {
                        xtype:'button',
                        text:'Simpan',
                        width:70,
                        style:{'margin-left':'0px','margin-top':'0px'},
                        hideLabel:true,
                        id:Nci.getId(),
                        handler:function()
                        {
                            if(panelActiveDataPasien == 0){
                                Datasave_Kelompokpasien_rad(rowdata);
                            }
                            if(panelActiveDataPasien == 1){
                                Datasave_GantiDokter_rad(rowdata);
                            }
                            
                        }
                    },
                    {
                        xtype:'button',
                        text:'Tutup',
                        width:70,
                        hideLabel:true,
                        id:Nci.getId(),
                        handler:function() 
                        {
                            if(panelActiveDataPasien == 0){
                                FormLookUpsdetailTRKelompokPasien_rad.close();
                            }
                            if(panelActiveDataPasien == 1){
                                FormLookUpsdetailTRGantiDokter_rad.close();
                            }
                            
                        }
                    }
                ]
            }
        ]
    }
    return items;
};

function Datasave_Kelompokpasien_rad(rowdata) 
{   
    if (ValidasiEntryUpdateKelompokPasien_rad(nmHeaderSimpanData,false) == 1 )
    {           
            Ext.Ajax.request
            (
                {
                    url: baseURL +  "index.php/main/functionRAD/UpdateGantiKelompok",   
                    params: getParamKelompokpasien_rad(rowdata),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasRad('Simpan kelompok pasien gagal', 'Gagal');
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            validasiJenisTr();
                            ShowPesanInfoPenJasRad("Mengganti kelompok pasien berhasil", "Success");
                            FormLookUpsdetailTRKelompokPasien_rad.close();
                        }else 
                        {
                            ShowPesanWarningPenJasRad('Simpan kelompok pasien gagal', 'Gagal');
                        };
                    }
                }
             )
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
    
};

function ValidasiEntryUpdateKelompokPasien_rad(modul,mBolHapus)
{
    var x = 1;
    
    if((Ext.get('kelPasien_rad').getValue() == '') || (Ext.get('kelPasien_rad').dom.value  === undefined ))
    {
        if (Ext.get('kelPasien_rad').getValue() == '' && mBolHapus === true) 
        {
            ShowPesanWarningPenJasRad(nmGetValidasiKosong('Kelompok Pasien'), modul);
            x = 0;
        }
    };
    return x;
};

function getParamKelompokpasien_rad(rowdata) 
{   var params;
    var urut;
    if (rowdata.URUT != '') {
        urut = rowdata.URUT;
    }else{
        urut = rowdata.URUT_MASUK;
    }
    if(panelActiveDataPasien == 0){
        params = {
        KDCustomer  : selectKdCustomer,
        KDNoSJP     : Ext.get('txtRWJNoSEP').getValue(),
        KDNoAskes   : Ext.get('txtRWJNoAskes').getValue(),
        KdPasien    : rowdata.KD_PASIEN,
        TglMasuk    : rowdata.MASUK,
        KdUnit      : rowdata.KD_UNIT,
        UrutMasuk   : urut,
        KdDokter    : rowdata.KD_DOKTER,
        }
    }else if(panelActiveDataPasien == 1 ){
        params = {
            KdPasien    : rowdata.KD_PASIEN,
            TglMasuk    : rowdata.MASUK,
            KdUnit      : rowdata.KD_UNIT,
            UrutMasuk   : urut,
            KdDokter    : vKdDokter,
            NoTransaksi : rowdata.NO_TRANSAKSI,
            KdKasir     : rowdata.KD_KASIR,
            KdUnitDulu  : rowdata.KD_UNIT_ASAL,
        }
    }
    return params
};

/*----------------------------------------------------Ganti Dokter Radiologi---------------------------------------------------------
 Oleh     : HDHT 
 Tanggal  : 22-februari-2017
 MADIUN 
 */

function GantiDokterPasienLookUp_rad(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRGantiDokter_rad = new Ext.Window
    (
        {
            id: 'idGantiDokter',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiDokter_rad(lebar,rowdata),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRGantiDokter_rad.show();
    // GantiPasien_rad(rowdata);

};

function getFormEntryTRGantiDokter_rad(lebar,rowdata) 
{
    var pnlTRKelompokPasien_rad = new Ext.FormPanel
    (
        {
            id: 'PanelTRGanti',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
                    getItemPanelInputGantiDokter_rad(lebar,rowdata),
                    getItemPanelButtonKelompokPasien_rad(lebar,rowdata)
            ],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiDokter = new Ext.Panel
    (
        {
            id: 'FormDepanGantiDokter',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: true,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: [pnlTRKelompokPasien_rad 
                
            ]

        }
    );

    return FormDepanGantiDokter
};

function getItemPanelInputGantiDokter_rad(lebar,rowdata) 
{
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding:10px 10px 10px 0px',
        border:false,
        height:170,
        items:
        [
            {
                columnWidth: .9,
                width: lebar -35,
                labelWidth:100,
                layout: 'form',
                border: false,
                items:
                [   
                    {
                        xtype: 'textfield',
                        fieldLabel:  'Unit Asal ',
                        name: 'txtUnitAsal_DataPasien',
                        id: 'txtUnitAsal_DataPasien',
                        value:rowdata.NAMA_UNIT_ASLI,
                        readOnly:true,
                        width: 100,
                        anchor: '99%'
                    },{
                        xtype: 'textfield',
                        fieldLabel: 'Dokter Asal ',
                        name: 'txtDokterAsal_DataPasien',
                        id: 'txtDokterAsal_DataPasien',
                        value:rowdata.DOKTER,
                        readOnly:true,
                        width: 100,
                        anchor: '99%'
                    },
                    mComboDokterGantiEntry()
                ]
            }
        ]
    };
    return items;
};
var dsDokterRequestEntry;
var vKdDokter;
function mComboDokterGantiEntry(){ 
    var Field = ['KD_DOKTER','NAMA'];

    dsDokterRequestEntry = new WebApp.DataStore({ fields: Field });
    dsDokterRequestEntry.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang',
            param: "kd_unit = '5' and d.jenis_dokter=1  "
        }
    });
    var cboDokterGantiEntry = new Ext.form.ComboBox({
        id: 'cboDokterRequestEntry',
        typeAhead: true,
        triggerAction: 'all',
        name:'txtdokter',
        lazyRender: true,
        mode: 'local',
        selectOnFocus:true,
        forceSelection: true,
        emptyText:'Pilih Dokter...',
        fieldLabel: 'Dokter Baru',
        align: 'Right',
        store: dsDokterRequestEntry,
        valueField: 'KD_DOKTER',
        displayField: 'NAMA',
        anchor:'100%',
        listeners:{
            'select': function(a,b,c){
                vKdDokter = b.data.KD_DOKTER;
            },
        }
    });
    return cboDokterGantiEntry;
};

function Datasave_GantiDokter_rad(rowdata) 
{   
    //console.log(Ext.get('cboDokterRequestEntry').getValue());
    if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntry').dom.value  === 'Pilih Dokter...'))
    {
        ShowPesanWarningPenJasRad('Dokter baru harap diisi', "Informasi");
    }else{
        Ext.Ajax.request
        (
            {
                url: baseURL +  "index.php/main/functionRAD/UpdateGantiDokter", 
                params: getParamKelompokpasien_rad(rowdata),
                failure: function(o)
                {
                    ShowPesanWarningPenJasRad('Simpan dokter pasien gagal', 'Gagal');
                },  
                success: function(o) 
                {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true) 
                    {
                        panelActiveDataPasien = 1;
                        validasiJenisTr();
                        FormLookUpsdetailTRGantiDokter_rad.close();
                        ShowPesanInfoPenJasRad("Mengganti dokter pasien berhasil", "Success");

                    }else 
                    {
                        ShowPesanWarningPenJasRad('Simpan dokter pasien gagal', 'Gagal');
                    };
                }
            }
        ) 
    }
    
};

function getcitorad(){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/cekCitoRadiologi",
        params: {Modul:''},
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            // console.log(cst.ListDataObj);
            // kodepasien = rowdata.KD_PASIEN;
            // namapasien = rowdata.NAMA;
            // console.log(rowdata.KD_UNIT);
            // namaunit = rowdata.NAMA_UNIT;
            // kodepay = cst.ListDataObj[0].kd_pay;
            // uraianpay = cst.ListDataObj[0].cara_bayar;
            // kdcustomeraa = rowdata.KD_CUSTOMER;
            // cst.ListDataObj
            citopersentasi = cst.cito;
            
        }

    });
}

//------------------TAMBAH BARU 27-September-2017
function setLookUpGridDataView_viRAD(rowdata){
    var lebar = 819; // Lebar Form saat PopUp
    setLookUpsGridDataView_viRAD = new Ext.Window({
        id: 'setLookUpsGridDataView_viRAD',
        title: 'Edit Data Pasien',
        closeAction: 'destroy',
        autoScroll: true,
        width: 640,
        height: 225,
        resizable: false,
        border: false,
        plain: true,
        layout: 'fit',
        modal: true,
        items: getFormItemEntryGridDataView_viRAD(lebar, rowdata),
        listeners:{
            activate: function (){
            },
            afterShow: function (){
                this.activate();
            },
            deactivate: function (){
                
            }
        }
    });
    setLookUpsGridDataView_viRAD.show();
    datainit_viRAD(rowdata)
    
}
function getFormItemEntryGridDataView_viRAD(lebar, rowdata)
{
    var pnlFormDataWindowPopup_viRAD = new Ext.FormPanel
            (
                    {
                        title: '',
                        // region: 'center',
                        fileUpload: true,
                        margin:true,
                        // layout: 'anchor',
                        // padding: '8px',
                        bodyStyle: 'padding-top:5px;',
                        // Tombol pada tollbar Edit Data Pasien
                        tbar: {
                            xtype: 'toolbar',
                            items:
                                    [
                                        {
                                            xtype: 'button',
                                            text: 'Save',
                                            id: 'btnSimpanWindowPopup_viRAD',
                                            iconCls: 'save',
                                            handler: function (){
                                                datasave_EditDataPasienRAD(false); //1.1
                                                
                                            }
                                        },
                                        //-------------- ## --------------
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        //-------------- ## --------------
                                        {
                                            xtype: 'button',
                                            text: 'Save & Close',
                                            id: 'btnSimpanExitWindowPopup_viRAD',
                                            iconCls: 'saveexit',
                                            handler: function ()
                                            {
                                                var x = datasave_EditDataPasienRAD(false); //1.2
                                                
                                                if (x === undefined)
                                                {
                                                    setLookUpsGridDataView_viRAD.close();
                                                }
                                            }
                                        },
                                        //-------------- ## --------------
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        //-------------- ## --------------
                                    ]
                        },
                        //-------------- #items# --------------
                        items:
                                [
                                    // Isian Pada Edit Data Pasien
                                    {
                                        xtype: 'panel',
                                        title: '',
                                        layout:'form',
                                        margin:true,
                                        border:false,
                                        labelAlign: 'right',
                                        width: 610,
                                        items:
                                                [//---------------# Penampung data untuk fungsi update # ---------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        id: 'TxtTmpAgama_viRAD',
                                                        name: 'TxtTmpAgama_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        id: 'TxtTmpPekerjaan_viRAD',
                                                        name: 'TxtTmpPekerjaan_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        id: 'TxtTmpPendidikan_viRAD',
                                                        name: 'TxtTmpPendidikan_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    //------------------------ end ---------------------------------------------                                
                                                    //-------------- # Pelengkap untuk kriteria ke Net. # --------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'KD_CUSTOMER',
                                                        id: 'TxtWindowPopup_KD_CUSTOMER_viRAD',
                                                        name: 'TxtWindowPopup_KD_CUSTOMER_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'URUT_MASUK',
                                                        id: 'TxtWindowPopup_Urut_Masuk_viRAD',
                                                        name: 'TxtWindowPopup_Urut_Masuk_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'KD_PASIEN',
                                                        id: 'TxtWindowPopup_KD_PASIEN_viRAD',
                                                        name: 'TxtWindowPopup_KD_PASIEN_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    //-------------- # End Pelengkap untuk kriteria ke Net. # --------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'No. MedRec',
                                                        id: 'TxtWindowPopup_NO_RM_viRAD',
                                                        name: 'TxtWindowPopup_NO_RM_viRAD',
                                                        emptyText: 'No. MedRec',
                                                        labelSeparator: '',
                                                        readOnly: true,
                                                        flex: 1,
                                                        width: 195
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nama',
                                                        id: 'TxtWindowPopup_NAMA_viRAD',
                                                        name: 'TxtWindowPopup_NAMA_viRAD',
                                                        emptyText: 'Nama',
                                                        labelSeparator: '',
                                                        flex: 1,
                                                        anchor: '100%'
                                                    },{
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Alamat',
                                                        id: 'TxtWindowPopup_ALAMAT_viRAD',
                                                        name: 'TxtWindowPopup_ALAMAT_viRAD',
                                                        emptyText: 'Alamat',
                                                        labelSeparator: '',
                                                        flex: 1,
                                                        anchor: '100%'
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'compositefield',
                                                        fieldLabel: 'No Tlp',
                                                        labelSeparator: '',
                                                        anchor: '100%',
                                                        width: 250,
                                                        items:
                                                                [
                                                                    {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtWindowPopup_No_Tlp_viRAD',
                                                                        name: 'TxtWindowPopup_No_Tlp_viRAD',
                                                                        emptyText: 'No Tlp',
                                                                        flex: 1,
                                                                        width: 195
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'tbspacer',
                                                                        width: 17,
                                                                        height: 23
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'displayfield',
                                                                        fieldLabel: 'Label',
                                                                        value: 'Tgl. Lahir',
                                                                        style: {'text-align': 'right'},
                                                                        id: '',
                                                                        name: '',
                                                                        flex: 1,
                                                                        width: 58
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'datefield',
                                                                        fieldLabel: 'Tanggal',
                                                                        id: 'DateWindowPopup_TGL_LAHIR_viRAD',
                                                                        name: 'DateWindowPopup_TGL_LAHIR_viRAD',
                                                                        width: 198,
                                                                        format: 'd/m/Y',
                                                                        listeners:{
                                                                            'specialkey' : function(){
                                                                                if (Ext.EventObject.getKey() === 13){
                                                                                    var tmptanggal = Ext.get('DateWindowPopup_TGL_LAHIR_viRAD').getValue();
                                                                                    Ext.Ajax.request({
                                                                                        url: baseURL + "index.php/main/GetUmur",
                                                                                        params: {
                                                                                            TanggalLahir: tmptanggal
                                                                                        },
                                                                                        success: function (o){
                                                                                            var tmphasil = o.responseText;
                                                                                            var tmp = tmphasil.split(' ');
                                                                                            if (tmp.length == 6){
                                                                                                Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                getParamBalikanHitungUmurPopUp(tmptanggal);
                                                                                            }else if(tmp.length == 4){
                                                                                                if(tmp[1]== 'years' && tmp[3] == 'day'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }else if(tmp[1]== 'years' && tmp[3] == 'days'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }else if(tmp[1]== 'year' && tmp[3] == 'days'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }else{
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }
                                                                                                getParamBalikanHitungUmurPopUp(tmptanggal);
                                                                                            }else if(tmp.length == 2 ){
                                                                                                if (tmp[1] == 'year' ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }else if (tmp[1] == 'years' ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }else if (tmp[1] == 'mon'  ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                                                                                                }else if (tmp[1] == 'mons'  ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                                                                                                }else{
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                                                                                                }
                                                                                                getParamBalikanHitungUmurPopUp(tmptanggal);
                                                                                            }else if(tmp.length == 1){
                                                                                                Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                                                                                                getParamBalikanHitungUmurPopUp(tmptanggal);
                                                                                            }else{
                                                                                                alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                                                                                            }   
                                                                                        }
                                                                                    });
                                                                                }
                                                                            },
                                                                        }
                                                                    }
                                                                    //-------------- ## --------------              
                                                                ]
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'compositefield',
                                                        fieldLabel: 'Umur',
                                                        labelSeparator: '',
                                                        anchor: '100%',
                                                        width: 50,
                                                        items:
                                                                [
                                                                    {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtWindowPopup_TAHUN_viRAD',
                                                                        name: 'TxtWindowPopup_TAHUN_viRAD',
                                                                        emptyText: 'Thn',
                                                                        style: {'text-align': 'right'},
                                                                        readOnly: true,
                                                                        flex: 1,
                                                                        width: 35
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'displayfield',
                                                                        fieldLabel: 'Label',
                                                                        value: 'Thn',
                                                                        id: '',
                                                                        name: '',
                                                                        flex: 1,
                                                                        width: 20
                                                                    },
                                                                    //-------------- ## --------------
                                                                    /* {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtWindowPopup_BULAN_viRAD',
                                                                        name: 'TxtWindowPopup_BULAN_viRAD',
                                                                        emptyText: 'Bln',
                                                                        style: {'text-align': 'right'},
                                                                        readOnly: true,
                                                                        flex: 1,
                                                                        width: 35
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'displayfield',
                                                                        fieldLabel: 'Label',
                                                                        value: 'Bln',
                                                                        id: '',
                                                                        name: '',
                                                                        flex: 1,
                                                                        width: 20
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtWindowPopup_HARI_viRAD',
                                                                        name: 'TxtWindowPopup_HARI_viRAD',
                                                                        emptyText: 'Hari',
                                                                        style: {'text-align': 'right'},
                                                                        readOnly: true,
                                                                        flex: 1,
                                                                        width: 35
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'displayfield',
                                                                        fieldLabel: 'Label',
                                                                        value: 'Hari',
                                                                        id: '',
                                                                        name: '',
                                                                        flex: 1,
                                                                        width: 20
                                                                    } */
                                                                    //-------------- ## --------------          
                                                                ]
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'compositefield',
                                                        fieldLabel: 'Jenis Kelamin',
                                                        labelSeparator: '',
                                                        anchor: '100%',
                                                        width: 250,
                                                        items:
                                                                [
                                                                    mComboJKRAD(),
                                                                    {
                                                                        xtype: 'displayfield',
                                                                        fieldLabel: 'Label',
                                                                        value: 'Golongan Darah',
                                                                        id: '',
                                                                        name: '',
                                                                        flex: 1,
                                                                        width: 90
                                                                    },
                                                                    mComboGolDarahRAD()
                                                                            //-------------- ## --------------              
                                                                ]
                                                    },
                                                    //-------------- ## --------------                      

                                                    //-------------- ## --------------
                                                ]
                                    },
                                    //-------------- ## --------------
                                    /*{
                                     xtype: 'spacer',
                                     width: 10,
                                     height: 1
                                     },*/
                                    //-------------- ## --------------
                                    
                                            //-------------- ## --------------
                                ]
                                //-------------- #items# --------------
                    }
            )
    return pnlFormDataWindowPopup_viRAD;
}

function datainit_viRAD(rowdata){
    var tmpjk;
    var tmp_data;
    addNew_viDataPasien = false;
    if (rowdata.jk === "t" || rowdata.jk === 1){
        tmpjk = "1";
        tmp_data = "Laki- laki";
        Ext.get('cboJKRAD').dom.value= 'Laki-laki';
    }else{
        tmpjk = "2";
        tmp_data = "Perempuan";
        Ext.get('cboJKRAD').dom.value= 'Perempuan'
    }
    ID_JENIS_KELAMIN = rowdata.jk;
    Ext.getCmp('cboGolDarahRAD').setValue(rowdata.goldarah);
    Ext.getCmp('TxtWindowPopup_ALAMAT_viRAD').setValue(rowdata.alamat);
    Ext.getCmp('TxtWindowPopup_NAMA_viRAD').setValue(rowdata.nama);
    Ext.getCmp('TxtWindowPopup_NO_RM_viRAD').setValue(rowdata.medrec);
    Ext.getCmp('TxtWindowPopup_No_Tlp_viRAD').setValue(rowdata.hp);
    Ext.get('DateWindowPopup_TGL_LAHIR_viRAD').dom.value=ShowDate(rowdata.tgl_lahir);
    setUsiaPopUp(ShowDate(rowdata.tgl_lahir));
}
function mComboGolDarahRAD()
        {
            var cboGolDarah = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboGolDarahRAD',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                //allowBlank: false,
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Gol. Darah...',
                                fieldLabel: 'Gol. Darah ',
                                width: 50,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[0, '-'], [1, 'A+'], [2, 'B+'], [3, 'AB+'], [4, 'O+'], [5, 'A-'], [6, 'B-'], [7, 'AB-'], [8, 'O-']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetGolDarah,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetGolDarah = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                /* c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboWarga').focus();
                                                }, c); */
                                            }
                                        }
                            }
                    );
            return cboGolDarah;
        }
        ;

        function mComboJKRAD()
        {
            
            var cboJK = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboJKRAD',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Jenis Kelamin...',
                                fieldLabel: 'Jenis Kelamin ',
                                width: 100,
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[true, 'Laki - Laki'], [false, 'Perempuan']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetJK,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetJK = b.data.displayText;
//                                        getdatajeniskelamin(b.data.displayText)
                                                //alert(jenis_kelamin)
                                            },
                                            'render': function (c) {
                                                /* c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtTempatLahir').focus();
                                                }, c); */
                                            }
                                        }
                            }
                    );
            return cboJK;
        }
        ;
function setUsiaPopUp(Tanggal)
{
    Ext.Ajax.request
            ({
                url: baseURL + "index.php/main/GetUmur",
                params: {
                    TanggalLahir: Tanggal
                },
                success: function (o)
                {
                    var tmphasil = o.responseText;
                    var tmp = tmphasil.split(' ');

                    if (tmp.length === 6)
                    {
                        Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[4]);
                    } else if (tmp.length === 4) {
                        if (tmp[1] === 'years') {
                            if (tmp[3] === 'day') {
                                Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                            } else {
                                Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                            }
                        } else {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                        }
                    } else if (tmp.length === 2) {
                        if (tmp[1] === 'year') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'years') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mon') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mons') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[0]);
                        }
                    } else if (tmp.length === 1) {
                        Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue('1');
                    } else {
                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                    }
                }
            });
}
function datasave_EditDataPasienRAD(mBol){  
    
    if (ValidasiEntry_viDataPasienRAD('Simpan Data', true) == 1){
        Ext.Ajax.request({
            url: baseURL + "index.php/main/functionKasirPenunjang/simpanEditDataPasien",
            params: dataparam_datapasviDataPasienRAD(),
            failure: function (o){
                ShowPesanErrorPenJasRad('Data tidak berhasil diupdate. Hubungi admin! ', 'Edit Data');
            },
            success: function (o){
                var cst = Ext.decode(o.responseText);
                if (cst.success === true){
                    ShowPesanInfoPenJasRad('Data berhasil disimpan', 'Edit Data');
                    validasiJenisTr();
                    //DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
                } else if (cst.success === false && cst.pesan === 0){
                    ShowPesanErrorPenJasRad('Data tidak berhasil diupdate ', 'Edit Data');
                } else{
                    ShowPesanErrorPenJasRad('Data tidak berhasil diupdate. ', 'Edit Data');
                }
            }
        });
    } else{
        if (mBol === true){
            return false;
        }
    }
    
}
function ValidasiEntry_viDataPasienRAD(modul, mBolHapus)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viRAD').getValue() === 'Nama' ||
            (Ext.get('TxtWindowPopup_ALAMAT_viRAD').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_NAMA_viRAD').getValue() === '' ||
                     (Ext.get('TxtWindowPopup_ALAMAT_viRAD').getValue() === ''))))
    {
        if (Ext.get('TxtWindowPopup_NAMA_viRAD').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viRAD').getValue() === ''))
        {
            ShowPesanWarningPenJasRad('Nama belum terisi', modul);
            x = 0;
        }  
    }

    return x;
}

function dataparam_datapasviDataPasienRAD(){
    var params_viDataPasienRAD ={
                //-------------- # modelist Net. # --------------
        //-------------- # textfield # --------------
        NO_MEDREC: Ext.getCmp('TxtWindowPopup_NO_RM_viRAD').getValue(),
        NAMA: Ext.getCmp('TxtWindowPopup_NAMA_viRAD').getValue(),
        ALAMAT: Ext.getCmp('TxtWindowPopup_ALAMAT_viRAD').getValue(),
        NO_TELP: Ext.getCmp('TxtWindowPopup_No_Tlp_viRAD').getValue(),
        //NO_HP: Ext.get('TxtWindowPopup_NO_HP_viDataPasien').getValue(),

        //-------------- # datefield # --------------
        TGL_LAHIR: Ext.get('DateWindowPopup_TGL_LAHIR_viRAD').getValue(),
        //-------------- # combobox # --------------
        ID_JENIS_KELAMIN: Ext.getCmp('cboJKRAD').getValue(),
        ID_GOL_DARAH: Ext.getCmp('cboGolDarahRAD').getValue(),
        TRUESQL: false
    }
    return params_viDataPasienRAD
}
function getParamBalikanHitungUmurPopUp(tgl){
    Ext.getCmp('DateWindowPopup_TGL_LAHIR_viRAD').setValue(tgl);
}


function lookUpFileManagement(){
    var tmp_notransaksi = Ext.getCmp('txtNoTransaksiPenJasRad').getValue();
    var url_file = baseURL+"index.php/rad/file_management/gallery/"+Ext.getCmp('txtNoMedrecL').getValue()+"_"+tmp_notransaksi+"_"+rowSelectedPenJasRad.data.KD_UNIT;
    FormFileManagement = new Ext.Window({
        id: '_fileManagement_PJ_RAD',
        title: 'File Management',
        closeAction: 'destroy',
        width: 900,
        height: 600,
        constrain: true,
        modal: true,
        html: "<iframe  style='width: 100%; height: 100%;' src='" + url_file + "'></iframe>",
        tbar    : [
            {
                xtype       : 'button',
                id          : 'btnUpload_file',
                text        : 'Upload Document',
                handler     : function(){
                    // var name_file = Ext.getCmp('txtNoMedrecL').getValue()+"_"+rowSelectedPenJasRad.data.TGL_TRANSAKSI+"_"+rowSelectedPenJasRad.data.KD_UNIT;
                    // FormFileManagement.close();
                    // FormLookupUploadfile_PJ_RAD(baseURL+'index.php/rad/file_management/upload_hasil','./Img Asset/RAD', name_file);
                    
                    if (tmp_notransaksi.length != 0) {
                        var name_file = Ext.getCmp('txtNoMedrecL').getValue()+"_"+tmp_notransaksi+"_"+rowSelectedPenJasRad.data.KD_UNIT;
                        FormFileManagement.close();
                        FormLookupUploadfile_PJ_RAD(baseURL+'index.php/rad/file_management/upload_hasil','./Img Asset/RAD', name_file);
                    }else{
                        ShowPesanWarningPenJasRad("No Transaksi Radiologi tidak terbaca", "Peringatan");
                    }
                }
            },{
                xtype       : 'button',
                id          : 'btnReload_Upload',
                text        : 'Reload',
                handler     : function(){
                    
                }
            },
        ],
        listeners: {
        }
    });

    FormFileManagement.show();
};


function FormLookupUploadfile_PJ_RAD(Link, Direktori = null, File_name = null) 
{  
    console.log("cekk");
    v_Link  = "";
    v_Direktori     = "";
    v_File_name     = "";
    vWinFormEntryUploadfile = new Ext.Window
    (           
        {
            id: 'formLookupUploadfile_PJ_RAD',
            title: 'Upload File',
            closeAction: 'destroy',
            width: 450,
            height: 140,
            border: false,          
            frame: true,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'find',            
            modal: true,                                   
            items: 
            [
                GetpanelUploadFile_PJ_RAD(this.Direktori, this.File_name)
            ],      
            listeners:
            { 
                activate: function()
                { 
                    nLink        = Link;
                    strDirektori = Direktori;
                    strFileName  = File_name;
                } 
            }
        }
    );  
    vWinFormEntryUploadfile.show();
};


function GetpanelUploadFile_PJ_RAD(Direktori, File_name)
{
    var vWinFormUploadfile_PJ_RAD = new Ext.FormPanel
    ({
        //id: 'FormUpldSeminar',
        layout: 'form',
        width: 400,
        height: 130,
        frame: true,
        border: false,
        fileUpload: true,       
        anchor: '100% 100%',
        items:
        [
            {
                xtype: 'fileuploadfield',
                emptyText: 'Path file upload',
                fieldLabel: 'Path file',
                frame: false,
                border: false,
                name: 'file',
                id: 'file',
                anchor: '100%',
                buttonText: 'browse'
            },
            {
                xtype: 'hidden',
                name: 'direktori',
                id: 'direktori',
            },
            {
                xtype: 'hidden',
                name: 'file_name',
                id: 'file_name',
            },{
                xtype: 'textfield',
                fieldLabel: 'Catatan',
                frame: false,
                border: false,
                name: 'catatan',
                id: 'catatan',
                anchor: '100%',
            },
        ],
        buttons:
        [
            {
                text: 'Upload',
                handler: function() 
                {
                    // var strUrl="./"+nLink;
                    var strUrl= nLink;
                    Ext.getCmp('direktori').setValue(strDirektori);
                    Ext.getCmp('file_name').setValue(strFileName);
                    /*if (nLink == uFormFoto || nLink == uFormkehadiran || nLink == uFormTunjPerPeriode) 
                    {
                        Ext.get('file').dom.value = strFileName;
                        strUrl="./"+nLink
                    }else
                    {
                        // Ext.get('direktori').dom.value = strPathUploadAll + strDirektori;
                        Ext.get('file').dom.value = strFileName;
                        strUrl="./"+nLink
                    } */
                    
                    var files = Ext.getCmp('file').getValue();
                    if(files.length > 0){
                        if (vWinFormUploadfile_PJ_RAD.getForm().isValid()) 
                        {
                            vWinFormUploadfile_PJ_RAD.getForm().submit({

                                url: strUrl,//"./home.mvc/Upload",
                                // waitMsg: 'Uploading your photo...',
                                // waitMsg: 'Uploading your File...',
                                success: function(form, o) //(3)
                                {   
                                },                              
                                failure: function(form, o) //(4)
                                {
                                }
                            });
                            Ext.Msg.alert('Success', 'Data berhasil di upload');
                        }
                    }else{
                        Ext.Msg.alert('Informasi', 'Harap pilih file untuk di upload');
                    }
                    Ext.getCmp('file').setValue();
                    // Ext.getCmp('catatan').setValue();
                }
            },{
                text: 'Preview',
                handler: function() 
                {
                    vWinFormEntryUploadfile.close();
                    lookUpFileManagement();
                }
            },{
                text: 'Close',
                handler: function() 
                {
                    vWinFormEntryUploadfile.close();
                }
            }
        ]
    });     
    return vWinFormUploadfile_PJ_RAD;
};


function get_total_bayar(no_transaksi, kd_kasir){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/GettotalTranfer",
        params: {
            notr: no_transaksi,
            kd_kasir:kd_kasir
        },
        success: function (o)
        {
            var cst     = Ext.decode(o.responseText);
            total_bayar = cst;
        }
    });
}

function get_data_transfer(no_transaksi, no_transaksi_tujuan, kd_kasir_tujuan){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/GetPasienTranferLab_rad",
        params: {
            notr: no_transaksi_tujuan,
            notransaksi: no_transaksi,//tmp_NoTransaksi,
            kdkasir: kd_kasir_tujuan
        },
        success: function (o)
        {   
                // showhide_unit(kdkasirasal_lab);
                if (o.responseText == "") {
                    ShowPesanWarningPenJasRad('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                } else {
                    data_transfer = o.responseText;
                }
        }
    });
}

function get_unit_asal(no_transaksi, kd_kasir){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/get_unit_asal",
        params: {
            no_transaksi: no_transaksi,
            kd_kasir: kd_kasir
        },
        success: function (o)
        {   
            var cst         = Ext.decode(o.responseText);
            // console.log(cst[0]);
            if (cst.status === true) {
                data_unit_asal  = cst.data[0];
                get_data_transfer(no_transaksi, data_unit_asal.NO_TRANSAKSI_ASAL, data_unit_asal.KD_KASIR_ASAL);
            }
        }
    });
}


function GetDTLPreviewBilling(url, no_transaksi, kd_kasir) {
	window.open(baseURL + "index.php/laporan/lap_billing_penunjang/print_pdf_lab/"+no_transaksi+"/"+kd_kasir, '_blank');

    // new Ext.Window({
    //     title: 'Preview Billing',
    //     width: 1000,
    //     height: 600,
    //     constrain: true,
    //     modal: true,
    //     html: "<iframe  style ='width: 100%; height: 100%;' src='" + url+"/"+no_transaksi+"/"+kd_kasir+"'></iframe>",
    //     tbar : [
    //         {
    //             xtype   : 'button',
    //             text    : 'Cetak Direct',
    //             iconCls : 'print',
    //             handler : function(){
    //                 window.open(baseURL + "index.php/laporan/lap_billing_penunjang/print_pdf/"+no_transaksi+"/"+kd_kasir, '_blank');
    //             }
    //         }
    //     ]
    // }).show();
};
