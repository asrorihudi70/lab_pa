var TrPenJasRad={};
TrPenJasRad.form={};
TrPenJasRad.func={};
TrPenJasRad.vars={};
TrPenJasRad.func.parent=TrPenJasRad;
TrPenJasRad.form.ArrayStore={};
TrPenJasRad.form.ComboBox={};
TrPenJasRad.form.DataStore={};
TrPenJasRad.form.Record={};
TrPenJasRad.form.Form={};
TrPenJasRad.form.Grid={};
TrPenJasRad.form.Panel={};
TrPenJasRad.form.TextField={};
TrPenJasRad.form.Button={};
var dspasienorder_mng_rad;
var autocom_rad;
var cellCurrentLab='';
var cbopasienorder_mng_rad;
var dsTrDokter	= new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA','KD_JOB','KD_PRODUK'] });
var dataTrDokter = [];
var Penata_dsLookProdukList_rad;


TrPenJasRad.form.ArrayStore.kodepasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'no transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});

TrPenJasRad.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'no transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});
	
TrPenJasRad.form.ArrayStore.notransaksi=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'no transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});


var CurrentPenJasRad =
{
    data: Object,
    details: Array,
    row: 0
};

var RAd_tanggaltransaksitampung;
var rAd_mRecord = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var RAD_CurrentDiagnosa =
{
    data: Object,
    details: Array,
    row: 0
};


var gridDTLTRRad;
var now = new Date();



var tigaharilalu = new Date().add(Date.DAY, -3);
var nowTglTransaksi_RADGridRad = new Date();
var tglGridBawahRad = nowTglTransaksi_RADGridRad.format("d/M/Y");


var dsTRDetailPenJasRadList;
var AddNewPenJasRad = true;
var selectCountPenJasRad = 50;
var tmpkasir = '5';

var rowSelectedPenJasRad;
var cellSelecteddeskripsi_rad;
var nowTglTransaksi_RAD = new Date();


var vkode_customer_RAD;
CurrentPage.page = getPanelPenJasRad(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
var TglTransaksiAsal_RAD='';
var TmpNotransaksiRad;
var KdKasirAsalRad;
var Kd_SpesialRad;
var No_KamarRad;

var selectSetGDarahRad;
var selectSetJKRad;
var selectSetKelPasienRad;
var selectSetPerseoranganRad;
var selectSetPerusahaanRad;
var selectSetAsuransiRad;
var tmpunit;
function getPanelPenJasRad(mod_id) 
{
var i = setInterval(function(){
total_pasien_order_mng_rad();
load_data_pasienorder_rad();
}, 200000);
    var FormDepanPenJasRad = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Penata Jasa Radiologi',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
                        getItemPanelInputRad(),
                        getFormEntryPenJasRad()
                   ],
           tbar:
            [
				'-',
				{
                        text: 'Add new',
                        id: 'btnAddPenJasRad',
                        iconCls: 'add',
                        handler: function()
						{
							addNewData_RAD();

						}
				},  
				'-',
				{
                        text: 'Save',
                        id: 'btnSimpanPenJasRad',
                        tooltip: nmSimpan,
                        iconCls: 'save',
						disabled:true,
                        handler: function()
						{
							loadMask.show();
							Datasave_PenJasRad(false);                                    

						}
				},  
				{xtype: 'tbspacer',height: 3, width:370},
				{
				xtype: 'label',
				text: 'Order Unit lain : ' 
				},{xtype: 'tbspacer',height: 3, width:5},
				 {
					x: 40,
					y: 40,
					xtype: 'textfield',
					fieldLabel: 'No. Medrec',
					name: 'txtcounttr_rad',
					id: 'txtcounttr_rad',
					width: 50,
					disabled:true,
					listeners: 
					{ 
						
					}
				},mComboorder_rad()
				
			],
            listeners:
            {
                'afterrender': function()
                {
					Ext.Ajax.request(
					{
						url: baseURL + "index.php/main/functionRAD/getCurrentShiftRad",
						params: {
							command: '0',
							// parameter untuk url yang dituju (fungsi didalam controller)
						},
						failure: function(o)
						{
							var cst = Ext.decode(o.responseText);
						},	    
						success: function(o) {
							tampungshiftsekarang=o.responseText;
						}
					});
				}, 'beforeclose': function(){clearInterval(i);}
            }
        }
    );
	
   return FormDepanPenJasRad;

};
function load_data_pasienorder_rad(param)
{
	console.log(param);
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRADPoliklinik/getPasien",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
	
		},	    
		success: function(o) {
			cbopasienorder_mng_rad.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dspasienorder_mng_rad.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dspasienorder_mng_rad.add(recs);
				console.log(o);
			}
		}
	});

}



function total_pasien_order_mng_rad()
{
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRADPoliklinik/gettotalpasienorder_mng",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
		
			Ext.getCmp('txtcounttr_rad').setValue(cst.jumlahpasien);
			console.log(cst);
		}
	});
};



function mComboorder_rad()
{ 
    var Field = ['no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien','display'];
		total_pasien_order_mng_rad();
		load_data_pasienorder_rad();
    dspasienorder_mng_rad = new WebApp.DataStore({ fields: Field });
	cbopasienorder_mng_rad= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_mng_rad',
				typeAhead		: true,
			    triggerAction	: 'all',
			    lazyRender		: true,
			    mode			: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 230,
			store: dspasienorder_mng_rad,
			valueField: 'kd_pasien',
			displayField: 'display',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
									ViewGridBawahPenjasRad(b.data.no_transaksi,b.data);
									TrPenJasLab.form.ComboBox.kdpasien.disable();
									TrPenJasLab.form.ComboBox.kdpasien.setValue(b.data.kd_pasien);
									TrPenJasLab.form.ComboBox.pasien.disable();
									TrPenJasLab.form.ComboBox.pasien.setValue(b.data.nama);
									
									Ext.getCmp('btnLookupItemPemeriksaan').disable();
									Ext.getCmp('btnHpsBrsItemLabL').disable();
									Ext.getCmp('btnSimpanPenJasLab').disable();
									
									TrPenJasLab.vars.kd_tarif=b.data.kd_tarif;
									
									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/functionRAD/getCurrentShiftRad",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											tampungshiftsekarang=o.responseText;
										}
									});
									
								},
				   keyUp: function(a,b,c){
				    	$this1=this;
				    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
				    		clearTimeout(this.time);
				    	
				    		this.time=setTimeout(function(){
			    				if($this1.lastQuery != '' ){
				    				var value=$this1.lastQuery;
				    				var param={};
		        		    		param['text']=$this1.lastQuery;
		        		    		load_data_pasienorder_rad(param);

		        		    		
			    				}
		    		    	},1000);
				    	}
				    }
			}
		}
	);return cbopasienorder_mng_rad;
};


function getFormEntryPenJasRad(lebar,data) 
{
 var GDtabDetailRad = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailRad',
        region: 'center',
        activeTab: 0,
		height:240,
        //anchor: '100% 100%',
        border:false,
        plain: true,
        defaults:  {autoScroll: true},
		items: [GetDTLTRRADGrid()],
        tbar:
			[
				{
					text: 'Tambah Item Pemeriksaan',
					id: 'btnLookupItemPemeriksaanRad',
					tooltip: nmLookup,
					iconCls: 'Edit_Tr',
					disabled:true,
					handler: function()
					{
					
					var records = new Array();
					records.push(new dsTRDetailPenJasRadList.recordType());
					dsTRDetailPenJasRadList.add(records);

					/*	var p = RecordBaru_RAD();
						dsTRDetailPenJasRadList.insert(dsTRDetailPenJasRadList.getCount(), p);
					*/
					}
				}, 
				{
					id:'btnHpsBrsItemRad',
					text: 'Hapus Baris',
					tooltip: 'Hapus Baris',
					disabled:false,
					iconCls: 'RemoveRow',
					handler: function()
					{
						if (dsTRDetailPenJasRadList.getCount() > 0 ) {
							if (cellSelecteddeskripsi_rad != undefined)
							{
								Ext.Msg.confirm('Warning', 'Apakah item pemeriksaan ini akan dihapus?', function(button){
									if (button == 'yes'){
										if(CurrentPenJasRad != undefined) {
											var line = gridDTLTRRad.getSelectionModel().selection.cell[0];
											dsTRDetailPenJasRadList.removeAt(line);
											gridDTLTRRad.getView().refresh();
										}
									}
								})
							} else {
								ShowPesanWarningPenJasRad('Pilih record ','Hapus data');
							}
						}
					}
				}
			]
        }
		
    );
	
    return GDtabDetailRad;
};


function GetDTLTRRADGrid() 
{
    var fldDetail = ['kd_produk','deskripsi','deskripsi2','kd_tarif','harga','qty','desc_req','tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi','jumlah'];
	
    dsTRDetailPenJasRadList = new WebApp.DataStore({ fields: fldDetail })
    RefreshDataPenJasRadDetail() ;
    gridDTLTRRad = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Item Test',
            stripeRows: true,
            store: dsTRDetailPenJasRadList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi_rad = dsTRDetailPenJasRadList.getAt(row);
                            CurrentPenJasRad.row = row;
                            CurrentPenJasRad.data = cellSelecteddeskripsi_rad;
                        }
                    }
                }
            ),
            cm: TRPenJasRadColumModel(),
			listeners: {
			rowclick: function( $this, rowIndex, e )
			{
        	cellCurrentLab = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
			var NoTrans =  Ext.getCmp('nci-gen1').getValue();
			var TglTrans =  Ext.get('dtpKunjunganRad').dom.value;
			var kdPrdk = dsTRDetailPenJasRadList.data.items[cellCurrentLab].data.kd_produk;
			var kdTrf = dsTRDetailPenJasRadList.data.items[cellCurrentLab].data.kd_tarif;
			var tglBerlaku = dsTRDetailPenJasRadList.data.items[cellCurrentLab].data.tgl_berlaku;
			var trf = dsTRDetailPenJasRadList.data.items[cellCurrentLab].data.harga;
			if(columnIndex == 9 && dsTRDetailPenJasRadList.data.items[cellCurrentLab].data.jumlah == 'Ada'){
				PilihDokterLookUp(NoTrans,TglTrans,kdPrdk,kdTrf,tglBerlaku,trf);
        	}
			else if(columnIndex == 9 && dsTRDetailPenJasRadList.data.items[cellCurrentLab].data.JUMLAH == null)
			{
				ShowPesanInfoPenJasRad('Maaf Produk ini tidak mempunyai jasa Dokter', 'Informasi');
			}	
        	}
			}
			
        }
		
		
    );
	
	

    return gridDTLTRRad;
};
function getproduk_PJRWJ(kd_cus)
{

var str='LOWER(tarif.kd_tarif)=LOWER(~'+kd_cus+'~) and tarif.kd_unit= ~5~ ' 

Penata_dsLookProdukList_rad.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'tgl_transaksi',
                        Sortdir: 'ASC',
                        target: 'LookupProduk',
                        param: str
                    }
            }
	);
 return Penata_dsLookProdukList_rad;
}
function TRPenJasRadColumModel() 
{
 var fldDetail = ['TARIF','KLASIFIKASI','PERENT','TGL_BERAKHIR','KD_KAT','KD_TARIF','KD_KLAS','DESKRIPSI','YEARS','NAMA_UNIT','KD_PRODUK','TGL_BERLAKU','CHEK','JUMLAH'];
//var str='LOWER(tarif.kd_tarif)=LOWER(~TU~) and tarif.kd_unit= ~5~ ' 
 Penata_dsLookProdukList_rad = new WebApp.DataStore({ fields: fldDetail })
/* Penata_dsLookProdukList_rad.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'tgl_transaksi',
                        Sortdir: 'ASC',
                        target: 'LookupProduk',
                        param: str
                    }
            }
	); */
autocom_rad= new Ext.form.ComboBox({
        		id				: Nci.getId(),
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: Penata_dsLookProdukList_rad,
        		valueField		: 'DESKRIPSI',
        		hideTrigger		: true,
        		displayField	: 'DESKRIPSI',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){
					
        				var line= gridDTLTRRad.getSelectionModel().selection.cell[0];
						var p = RecordBaru_RAD();
						
						dsTRDetailPenJasRadList.data.items[line].data.deskripsi2=b.data.DESKRIPSI;
        				dsTRDetailPenJasRadList.data.items[line].data.kd_tarif=b.data.KD_TARIF;
        				dsTRDetailPenJasRadList.data.items[line].data.deskripsi=b.data.DESKRIPSI;
        				dsTRDetailPenJasRadList.data.items[line].data.tgl_berlaku=b.data.TGL_BERLAKU;
						dsTRDetailPenJasRadList.data.items[line].data.qty=1;
						dsTRDetailPenJasRadList.data.items[line].data.kd_produk=b.data.KD_PRODUK;
						dsTRDetailPenJasRadList.data.items[line].data.harga=b.data.TARIF;
						dsTRDetailPenJasRadList.data.items[line].data.jumlah=b.data.JUMLAH;
						gridDTLTRRad.getView().refresh();
						Ext.getCmp('btnSimpanPenJasRad').enable();
        				
        		    }
        		}
        	});
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                header: 'Uraian',
                dataIndex: 'deskripsi2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
			{
                header: 'kd_tarif',
                dataIndex: 'kd_tarif',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
            {
                header: 'Kode Produk',
                dataIndex: 'kd_produk',
                width:100,
                menuDisabled:true,
                hidden:true
            },
            {
                header:'Nama Produk',
                dataIndex: 'deskripsi',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320,
				editor :  autocom_rad
                
            }
			,
            {
				header: 'Tanggal Transaksi',
				dataIndex: 'tgl_transaksi',
				width: 130,
				menuDisabled:true,
				renderer: function(v, params, record){
				if(record.data.tgl_transaksi == undefined){
					record.data.tgl_transaksi=tglGridBawahRad;
					return record.data.tgl_transaksi;
					} 
					else{
					if(record.data.tgl_transaksi.substring(5, 4) == '-'){
						return ShowDate(record.data.tgl_transaksi);
					} else{
						var tgl=record.data.tgl_transaksi.split("/");

						if(tgl[2].length == 4 && isNaN(tgl[1])){
							return record.data.tgl_transaksi;
						} else{
							return ShowDate(record.data.tgl_transaksi);
						}
					}
				   }
				}
            },
			{
				header: 'Tanggal Berlaku',
				dataIndex: 'tgl_berlaku',
				width: 130,
				menuDisabled:true,
				hidden: true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_berlaku == undefined){
						record.data.tgl_berlaku=tglGridBawahRad;
						return record.data.tgl_berlaku;
					} else{
								if(record.data.tgl_berlaku.substring(5, 4) == '-'){
						return ShowDate(record.data.tgl_berlaku);
						} else{
							var tgl=record.data.tgl_berlaku.split("/");

							if(tgl[2].length == 4 && isNaN(tgl[1])){
								return record.data.tgl_berlaku;
							} else{
								return ShowDate(record.data.tgl_berlaku);
							}
					  }
				    }
				}
            },
            {
                header: 'Harga',
                align: 'right',
                hidden: true,
                menuDisabled:true,
                dataIndex: 'harga',
                width:100,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.harga);
							
							}	
            },
            {
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'qty',
                editor: new Ext.form.TextField
                (
					{allowBlank: false}
                ),
				
				
              
            },
			
			 {
                header: 'Dokter',
                width:91,
				menuDisabled:true,
                dataIndex: 'jumlah',
 
            },

          

        ]
    )
};


function RefreshDataPenJasRadDetail(no_transaksi,kasir) 
{
    var strKriteria_RAD='';
    strKriteria_RAD = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = '"+ kasir +"'";
       
    dsTRDetailPenJasRadList.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'tgl_transaksi',
                        Sortdir: 'ASC',
                        target: 'ViewDetailRWJGridBawah',
                        param: strKriteria_RAD
                    }
            }
	);
    return dsTRDetailPenJasRadList;
};

function RecordBaru_RAD()
{
/*
'kd_produk','','','kd_tarif','harga','qty','desc_req','tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi'*/
	var tmptgl = Ext.getCmp('dtpKunjunganRad').getValue();
	var p = new rAd_mRecord
	(
		{
		    'deskripsi2':'',
			'kd_produk':'',
		    'deskripsi':'', 
		    'kd_tarif':'', 
		    'harga':'',
		    'qty':'',
		    'tgl_transaksi': tmptgl.format('Y/m/d'), //ShowDate(), //RAd_tanggaltransaksitampung, ....
		    'desc_req':'',
		    'urut':'',
			'jumlah':'',
		}
	);
	
	return p;
};

//tampil grid bawah penjas lab
function ViewGridBawahLookupPenjasRad(no_transaksi) 
{
     var strKriteria_RAD='';
    strKriteria_RAD = "\"no_transaksi\" = ~" + no_transaksi + "~" + " And kd_kasir ='03'";
   
    dsTRDetailPenJasRadList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailGridBawahPenJasLab',
			    param: strKriteria_RAD
			}
		}
	);
    return dsTRDetailPenJasRadList; 
}

function ViewGridBawahPenjasRad(no_transaksi,data) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionRAD/getItemPemeriksaan",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				ShowPesanErrorPenJasRad('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsTRDetailPenJasRadList.removeAll();
					var recs=[],
						recType=dsTRDetailPenJasRadList.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsTRDetailPenJasRadList.add(recs);
					
					gridDTLTRRad.getView().refresh();
					if(dsTRDetailPenJasRadList.getCount() > 0 ){
						Ext.getCmp('cboDOKTER_viPenJasRad').setReadOnly(true);
						TmpNotransaksiRad=data.no_transaksi;
						KdKasirAsalRad=data.kd_kasir;
						TglTransaksiAsal_RAD=data.tgl_transaksi;
						Kd_SpesialRad=data.kd_spesial;
						No_KamarRad=data.no_kamar;
						
						getDokterPengirim_rad(data.no_transaksi,data.kd_kasir);
						//TrPenJasRad.form.ComboBox.notransaksi.setValue(data.no_transaksi);
						Ext.getCmp('txtNamaUnitRad').setValue(data.nama_unit);
						Ext.getCmp('txtKdUnitRad').setValue(data.kd_unit);
						Ext.getCmp('txtKdDokter_RAD').setValue(data.kd_dokter);
						Ext.getCmp('cboDOKTER_viPenJasRad').setValue(data.kd_dokter);
						Ext.getCmp('txtAlamatRad').setValue(data.alamat);
						Ext.getCmp('dtpTanggalLahirL').setValue(ShowDate(data.tgl_lahir));
						Ext.getCmp('txtKdUrutMasukRad').setValue(data.urut_masuk);
						
						Ext.getCmp('cboJKPenJasRad').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKPenJasRad').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKPenJasRad').setValue('Perempuan');
						}
						Ext.getCmp('cboJKPenJasRad').disable();
						
						Ext.getCmp('cboGDRPenJasRad').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('B-');
						}
						Ext.getCmp('cboGDRPenJasRad').disable();
						
						Ext.getCmp('txtKdCustomerLamaHideR').setValue(data.kd_customer);
						//tampung kd customer untuk transaksi selain kunjungan langsung
						vkode_customer_RAD = data.kd_customer;
						Ext.getCmp('cboKelPasienRad').disable();
						
						
							if(data.kelpasien == 'Perseorangan'){
							Ext.getCmp('cboKelPasienRad').setValue(data.kelpasien);
							Ext.getCmp('cboPerseoranganRad').setValue(data.kd_customer);
							Ext.getCmp('cboPerseoranganRad').show();
						} else if(data.kelpasien == 'Perusahaan'){
							Ext.getCmp('cboKelPasienRad').setValue(data.kelpasien);
							Ext.getCmp('cboPerusahaanRequestEntryRad').setValue(data.kd_customer);
							Ext.getCmp('cboPerusahaanRequestEntryRad').show();
						} else{
							Ext.getCmp('cboKelPasienRad').setValue(data.kelpasien);
							Ext.getCmp('cboAsuransiRad').setValue(data.kd_customer);
							Ext.getCmp('cboAsuransiRad').show();
						}
				/* 		if(vkode_customer_RAD=='0000000001'){
							Ext.getCmp('cboKelPasienRad').setValue('Perseorangan');
							Ext.getCmp('txtCustomerLamaHideR').show();
						}else{
							Ext.getCmp('txtCustomerLamaHideR').show();
							Ext.getCmp('cboKelPasienRad').hide();
							Ext.getCmp('txtCustomerLamaHideR').setValue(data.customer);
						}
						 */
						//Ext.getCmp('txtNoTransaksiPenJasLab').disable();
						Ext.getCmp('txtDokterPengirimRad').disable();
						Ext.getCmp('txtNamaUnitRad').disable();
						Ext.getCmp('cboDOKTER_viPenJasRad').disable();
						Ext.getCmp('txtAlamatRad').disable();
						Ext.getCmp('cboJKPenJasRad').disable();
						Ext.getCmp('cboGDRPenJasRad').disable();
						Ext.getCmp('cboKelPasienRad').disable();
						Ext.getCmp('dtpTanggalLahirL').disable();
						Ext.getCmp('dtpKunjunganRad').disable();
					//	Ext.getCmp('txtCustomerLamaHide').disable();
						
						Ext.getCmp('btnLookupItemPemeriksaanRad').disable();
						Ext.getCmp('btnHpsBrsItemRad').disable();
						Ext.getCmp('btnSimpanPenJasRad').disable();
						
					} else {
						TmpNotransaksiRad=data.no_transaksi;
						KdKasirAsalRad=data.kd_kasir;
						TglTransaksiAsal_RAD=data.tgl_transaksi;
						Kd_SpesialRad=data.kd_spesial;
						No_KamarRad=data.no_kamar
						
						Ext.getCmp('txtNamaUnitRad').setValue(data.nama_unit_asli);
						Ext.getCmp('txtKdUnitRad').setValue(data.kd_unit);
						Ext.getCmp('txtDokterPengirimRad').setValue(data.dokter);
						Ext.getCmp('txtAlamatRad').setValue(data.alamat);
						Ext.getCmp('dtpTanggalLahirL').setValue(ShowDate(data.tgl_lahir));
						
						Ext.getCmp('cboJKPenJasRad').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKPenJasRad').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKPenJasRad').setValue('Perempuan');
						}
						Ext.getCmp('cboJKPenJasRad').disable();
						
						Ext.getCmp('cboGDRPenJasRad').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRPenJasRad').setValue('B-');
						}
						Ext.getCmp('cboGDRPenJasRad').disable();
						
						Ext.getCmp('txtKdCustomerLamaHideR').setValue(data.kd_customer);
						//tampung kd customer untuk transaksi selain kunjungan langsung
						vkode_customer_RAD = data.kd_customer;
						Ext.getCmp('cboKelPasienRad').enable();
					/* 	if(vkode_customer_RAD=='0000000001'){
							Ext.getCmp('cboKelPasienRad').setValue('Perseorangan');
							Ext.getCmp('txtCustomerLamaHideR').hide();
						}else{
							Ext.getCmp('txtCustomerLamaHideR').show();
							Ext.getCmp('cboKelPasienRad').hide();
							Ext.getCmp('txtCustomerLamaHideR').setValue(data.customer);
						}
						 */
						// Ext.getCmp('cboKelPasienLab').enable();
						 if(data.kelpasien == 'Perseorangan'){
							Ext.getCmp('cboKelPasienRad').setValue(data.kelpasien);
							Ext.getCmp('cboPerseoranganRad').setValue(data.kd_customer);
							Ext.getCmp('cboPerseoranganRad').show();
						} else if(data.kelpasien == 'Perusahaan'){
							Ext.getCmp('cboKelPasienRad').setValue(data.kelpasien);
							Ext.getCmp('cboPerusahaanRequestEntryRad').setValue(data.kd_customer);
							Ext.getCmp('cboPerusahaanRequestEntryRad').show();
						} else{
							Ext.getCmp('cboKelPasienRad').setValue(data.kelpasien);
							Ext.getCmp('cboAsuransiRad').setValue(data.kd_customer);
							Ext.getCmp('cboAsuransiRad').show();
						}
						Ext.getCmp('txtDokterPengirimRad').disable();
						Ext.getCmp('txtNamaUnitRad').disable();
						Ext.getCmp('cboDOKTER_viPenJasRad').enable();
						Ext.getCmp('txtAlamatRad').disable();
						Ext.getCmp('cboJKPenJasRad').disable();
						Ext.getCmp('cboGDRPenJasRad').disable();
						Ext.getCmp('cboKelPasienRad').enable();
						Ext.getCmp('dtpTanggalLahirL').disable();
						Ext.getCmp('dtpKunjunganRad').disable();
						Ext.getCmp('txtCustomerLamaHideR').disable();
						
						Ext.getCmp('btnLookupItemPemeriksaanRad').enable();
						Ext.getCmp('btnHpsBrsItemRad').disable();
						Ext.getCmp('btnSimpanPenJasRad').enable();
					}
				}
			}
		}
		
	)
};

function getParamDetailTransaksiRad() 
{
    var KdCust='';
	var TmpCustoLama='';
	var pasienBaruR;
	
	
	if(Ext.get('cboKelPasienRad').getValue()==='Perseorangan'){
		KdCust=Ext.getCmp('cboPerseoranganRad').getValue();
	}else if(Ext.get('cboKelPasienRad').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('cboPerusahaanRequestEntryRad').getValue();
	}else {
		KdCust=Ext.getCmp('cboAsuransiRad').getValue();
	}
	
    Ext.Ajax.request(
	{
		url: baseURL + "index.php/main/functionRAD/getCurrentShiftRad",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			tampungshiftsekarang=o.responseText;
		}
	});
	
	if(TrPenJasRad.form.ComboBox.kdpasien.getValue() == ''){
		pasienBaruR=1;
	} else{
		pasienBaruR=0;
	}
	
    var params =
	{ 
		KdTransaksi: TrPenJasRad.form.ComboBox.notransaksi.getValue(),
		KdPasien:TrPenJasRad.form.ComboBox.kdpasien.getValue(),
		NmPasien:TrPenJasRad.form.ComboBox.pasien.getValue(),
		Ttl:Ext.getCmp('dtpTanggalLahirL').getValue(),
		Alamat:Ext.getCmp('txtAlamatRad').getValue(),
		JK:Ext.getCmp('cboJKPenJasRad').getValue(),
		GolDarah:Ext.getCmp('cboGDRPenJasRad').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitRad').getValue(),
		KdDokter:Ext.getCmp('cboDOKTER_viPenJasRad').getValue(),
		Tgl: Ext.getCmp('dtpKunjunganRad').getValue(),

		TglTransaksiAsal:TglTransaksiAsal_RAD,

		KdCusto:KdCust,
		TmpCustoLama:Ext.getCmp('txtKdCustomerLamaHideR').getValue(),//jika customer dengan transaksi lama
		NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiRad').getValue(),
		NoAskes:Ext.getCmp('txtNoAskesRad').getValue(),
		NoSJP:Ext.getCmp('txtNoSJPRad').getValue(),

		Shift: tampungshiftsekarang,
		List:getArrDetailPenJasRad(),
		JmlField: rAd_mRecord.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		unit:5,
		TmpNotransaksi:TmpNotransaksiRad,
		KdKasirAsal:KdKasirAsalRad,
		KdSpesial:Kd_SpesialRad,
		Kamar:No_KamarRad,
		pasienBaru:pasienBaruR,
		
		listTrDokter	: Ext.encode(dataTrDokter),
		
		KdProduk:dsTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.KD_PRODUK
		
	};
    return params;
};


function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailPenJasRadList.getCount();i++)
	{
		if (dsTRDetailPenJasRadList.data.items[i].data.KD_PRODUK != '' || dsTRDetailPenJasRadList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getArrDetailPenJasRad()
{
console.log(dsTRDetailPenJasRadList);
	var x='';
	var arr=[];
	for(var i = 0 ; i < dsTRDetailPenJasRadList.getCount();i++)
	{
		if (dsTRDetailPenJasRadList.data.items[i].data.KD_PRODUK !== '' && dsTRDetailPenJasRadList.data.items[i].data.DESKRIPSI !== '')
		{
		/*
		sTRDetailPenJasRadList.data.items[line].data.deskripsi2=b.data.DESKRIPSI;
        				dsTRDetailPenJasRadList.data.items[line].data.kd_tarif=b.data.KD_TARIF;
        				dsTRDetailPenJasRadList.data.items[line].data.deskripsi=b.data.DESKRIPSI;
        				dsTRDetailPenJasRadList.data.items[line].data.tgl_berlaku=b.data.TGL_BERLAKU;
						dsTRDetailPenJasRadList.data.items[line].data.qty=1;
						dsTRDetailPenJasRadList.data.items[line].data.kd_produk=b.data.KD_PRODUK;
						dsTRDetailPenJasRadList.data.items[line].data.harga
						
						
						
						*/
			var x={};
			var y='';
			x['URUT']= dsTRDetailPenJasRadList.data.items[i].data.urut;
			x['KD_PRODUK']= dsTRDetailPenJasRadList.data.items[i].data.kd_produk;
			x['QTY']= dsTRDetailPenJasRadList.data.items[i].data.qty;
			x['TGL_TRANSAKSI']= dsTRDetailPenJasRadList.data.items[i].data.tgl_transaksi;
			x['TGL_BERLAKU']= dsTRDetailPenJasRadList.data.items[i].data.tgl_berlaku;
			x['HARGA']= dsTRDetailPenJasRadList.data.items[i].data.harga;
			x['KD_TARIF']= dsTRDetailPenJasRadList.data.items[i].data.kd_tarif;
			arr.push(x);
			
	
		};
	}	
	
	return Ext.encode(arr);
};

//panel dalam lookup detail pasien
function getItemPanelInputRad(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 10px',
	    border:false,
	    height:240,
	    items:
		[   
					{
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: true,
						width: 100,
						height: 120,
						anchor: '100% 100%',
						items:
						[                          
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Transaksi  '
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							/* {
								x : 110,
								y : 10,
								xtype: 'textfield',
								name: 'txtNoTransaksiPenJasLab',
								id: 'txtNoTransaksiPenJasLab',
								width: 100,
								emptyText:nmNomorOtomatis,
								readOnly:false

							}, */
							TrPenJasRad.form.ComboBox.notransaksi= new Nci.form.Combobox.autoComplete({
								store	: TrPenJasRad.form.ArrayStore.notransaksi,
								select	: function(a,b,c){
									ViewGridBawahPenjasRad(b.data.no_transaksi,b.data);
									TrPenJasRad.form.ComboBox.kdpasien.disable();
									TrPenJasRad.form.ComboBox.kdpasien.setValue(b.data.kd_pasien);
									TrPenJasRad.form.ComboBox.pasien.disable();
									Ext.Ajax.request
										({
											url:  baseURL + "index.php/main/functionLABPoliklinik/gettarif",
											 params: {kd_customer: b.data.kd_customer},
											failure: function(o)
											{},	    
											success: function(o)
											{
											 var cst = Ext.decode(o.responseText);
											
											 getproduk_PJRWJ(cst.kd_tarif);
											}
										});
									TrPenJasRad.form.ComboBox.pasien.setValue(b.data.nama);
									Ext.getCmp('btnLookupItemPemeriksaanRad').disable();
									Ext.getCmp('btnHpsBrsItemRad').disable();
									Ext.getCmp('btnSimpanPenJasRad').disable();
									TrPenJasRad.vars.kd_tarif=b.data.kd_tarif;
									
									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/functionRAD/getCurrentShiftRad",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											tampungshiftsekarang=o.responseText;
										}
									});
									
								},
								width	: 100,
								insert	: function(o){
									return {
										nama        		:o.nama,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										no_transaksi		:o.no_transaksi,
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjunganRad').getValue(),
										a:0
									}
								},
								url		: baseURL + "index.php/main/functionRAD/getPasien",
								valueField: 'no_transaksi',
								displayField: 'text',
								listWidth: 480,
								x : 130,
								y : 10,
								emptyText: 'No Transaksi'
							}),
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'No. Medrec  '
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							/* {
								x: 110,
								y: 40,
								xtype: 'textfield',
								fieldLabel: 'No. Medrec',
								name: 'txtNoMedrecL',
								id: 'txtNoMedrecL',
								readOnly:true,
								width: 120,
								listeners: 
								{ 
									
								}
							}, */
							TrPenJasRad.form.ComboBox.kdpasien= new Nci.form.Combobox.autoComplete({
								store	: TrPenJasRad.form.ArrayStore.kodepasien,
								select	: function(a,b,c){
									TrPenJasRad.form.ComboBox.notransaksi.setValue('');
									ViewGridBawahPenjasRad(b.data.no_transaksi,b.data);
									TrPenJasRad.form.ComboBox.pasien.disable();
									TrPenJasRad.form.ComboBox.pasien.setValue(b.data.nama);
									TrPenJasRad.form.ComboBox.notransaksi.disable();
										Ext.Ajax.request
										({
											url:  baseURL + "index.php/main/functionLABPoliklinik/gettarif",
											 params: {kd_customer: b.data.kd_customer},
											failure: function(o)
											{},	    
											success: function(o)
											{
											 var cst = Ext.decode(o.responseText);
											
											 getproduk_PJRWJ(cst.kd_tarif);
											}
										});
									TrPenJasRad.form.ComboBox.notransaksi.setValue(b.data.no_transaksi);
									TrPenJasRad.vars.kd_tarif=b.data.kd_tarif;
								
									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/functionRAD/getCurrentShiftRad",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											tampungshiftsekarang=o.responseText;
										}
									});
									
								},
								width	: 120,
								insert	: function(o){
									return {
										no_transaksi		:o.no_transaksi,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										nama        		:o.nama,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										kd_pasien	 		:o.kd_pasien
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjunganRad').getValue(),
										a:1
									}
								},
								url		: baseURL + "index.php/main/functionRAD/getPasien",
								valueField: 'kd_pasien',
								displayField: 'text',
								listWidth: 480,
								x: 130,
								y: 40,
								emptyText: 'Kode Pasien'
							}),
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Unit  '
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 70,
								xtype: 'textfield',
								name: 'txtNamaUnitRad',
								id: 'txtNamaUnitRad',
								readOnly:true,
								width: 120
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Dokter Pengirim'
							},
							{
								x: 120,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 100,
								xtype: 'textfield',
								fieldLabel: '',
								readOnly:true,
								name: 'txtDokterPengirimRad',
								id: 'txtDokterPengirimRad',
								width: 230
							},
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Dokter Rad'
							},
							{
								x: 120,
								y: 130,
								xtype: 'label',
								text: ':'
							},
							mComboDOKTER_RAD(),
							{
								x: 10,
								y: 160,
								xtype: 'label',
								text: 'Kelompok Pasien '
							},
							{
								x: 120,
								y: 160,
								xtype: 'label',
								text: ':'
							},
							mCboKelompokpasien_RAD(),
							mComboPerseorangan_RAD(),
							mComboPerusahaan_RAD(),
							mComboAsuransi_RAD(),
							//bagian tengah
							{
								x: 280,
								y: 10,
								xtype: 'label',
								text: 'Tanggal Kunjung '
							},
							{
								x: 390,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 400,
								y: 10,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'dtpKunjunganRad',
								name: 'dtpKunjunganRad',
								format: 'd/M/Y',
								//readOnly : true,
								value: now,
								width: 110
							},
							{
								x: 280,
								y: 40,
								xtype: 'label',
								text: 'Nama Pasien '
							},
							{
								x: 390,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							/* {
								x: 360,
								y: 40,
								xtype: 'textfield',
								fieldLabel: '',
								readOnly:true,
								name: 'txtNamaPasienL',
								id: 'txtNamaPasienL',
								width: 200,
								listeners: 
								{ 
									
								}
							}, */
							TrPenJasRad.form.ComboBox.pasien= new Nci.form.Combobox.autoComplete({
								store	: TrPenJasRad.form.ArrayStore.pasien,
								select	: function(a,b,c){
									TrPenJasRad.form.ComboBox.notransaksi.setValue('');
									ViewGridBawahPenjasRad(b.data.no_transaksi,b.data);
									TrPenJasRad.form.ComboBox.kdpasien.disable();
										Ext.Ajax.request
										({
											url:  baseURL + "index.php/main/functionLABPoliklinik/gettarif",
											 params: {kd_customer: b.data.kd_customer},
											failure: function(o)
											{},	    
											success: function(o)
											{
											 var cst = Ext.decode(o.responseText);
											
											 getproduk_PJRWJ(cst.kd_tarif);
											}
										});
									TrPenJasRad.form.ComboBox.kdpasien.setValue(b.data.kd_pasien);
									TrPenJasRad.form.ComboBox.notransaksi.disable();
									TrPenJasRad.form.ComboBox.notransaksi.setValue(b.data.no_transaksi);
									TrPenJasRad.vars.kd_tarif=b.data.kd_tarif;
									
									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/functionRAD/getCurrentShiftRad",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											tampungshiftsekarang=o.responseText;
										}
									});
									
								},
								width	: 200,
								insert	: function(o){
									return {
										no_transaksi		:o.no_transaksi,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										nama        		:o.nama,
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjunganRad').getValue(),
										a:2
									}
								},
								url		: baseURL + "index.php/main/functionRAD/getPasien",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 480,
								x: 400,
								y: 40,
								emptyText: 'Nama'
							}),
							{
								x: 280,
								y: 70,
								xtype: 'label',
								text: 'Alamat '
							},
							{
								x: 390,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 400,
								y: 70,
								xtype: 'textfield',
								fieldLabel: '',
								name: 'txtAlamatRad',
								id: 'txtAlamatRad',
								width: 420,
								listeners: 
								{ 
									
								}
							},
							{
								x: 400,
								y: 100,
								xtype: 'label',
								text: 'Jenis Kelamin '
							},
							{
								x: 485,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							mComboJK_RAD(),
							{
								x: 610,
								y: 100,
								xtype: 'label',
								text: 'Gol. Darah '
							},
							{
								x: 680,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							mComboGDarah_RAD(),
							{
								x: 610,
								y: 40,
								xtype: 'label',
								text: 'Tanggal lahir '
							},
							{
								x: 700,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 710,
								y: 40,
								xtype: 'datefield',
								fieldLabel: 'Tanggal ',
								id: 'dtpTanggalLahirL',
								name: 'dtpTanggalLahirL',
								format: 'd/M/Y',
								value: now,
								width: 110
							},
							{
								x: 475,
								y: 160,
								xtype: 'textfield',
								fieldLabel: 'Nama Peserta',
								maxLength: 200,
								name: 'txtNamaPesertaAsuransiRad',
								id: 'txtNamaPesertaAsuransiRad',
								emptyText:'Nama Peserta Asuransi',
								width: 150
							},
							{
								x: 280,
								y: 190,
								xtype: 'textfield',
								fieldLabel: 'No. Askes',
								maxLength: 200,
								name: 'txtNoAskesRad',
								id: 'txtNoAskesRad',
								emptyText:'No Askes',
								width: 150
							},
							{
								x: 475,
								y: 190,
								xtype: 'textfield',
								fieldLabel: 'No. SJP',
								maxLength: 200,
								name: 'txtNoSJPRad',
								id: 'txtNoSJPRad',
								emptyText:'No SJP',
								width: 150
							},
							
							//---------------------hidden----------------------------------
							{
								x: 130,
								y: 160,
								xtype: 'textfield',
								fieldLabel:'Nama customer lama',
								name: 'txtCustomerLamaHideR',
								id: 'txtCustomerLamaHideR',
								readOnly:true,
								hidden:true,
								width: 280
							},
							{
								x: 130,
								y: 160,
								xtype: 'textfield',
								fieldLabel:'Kode customer  ',
								name: 'txtKdCustomerLamaHideR',
								id: 'txtKdCustomerLamaHideR',
								readOnly:true,
								hidden:true,
								width: 280
							},
							
							{
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtKdDokter_RAD',
								id: 'txtKdDokter_RAD',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtKdDokter_RADPengirimR',
								id: 'txtKdDokter_RADPengirimR',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								fieldLabel:'Unit  ',
								name: 'txtKdUnitRad',
								id: 'txtKdUnitRad',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								name: 'txtKdUrutMasukRad',
								id: 'txtKdUrutMasukRad',
								readOnly:true,
								hidden:true,
								anchor: '100%'
							}
								
							//-------------------------------------------------------------
							
						]
					},	//akhir panel biodata pasien
			
		]
	};
    return items;
};

function Datasave_PenJasRad(mBol) 
{	
			Ext.Ajax.request
			 (
				{
					url: baseURL + "index.php/main/functionRAD/savedetailpenyakit",
					params: getParamDetailTransaksiRad(),
					failure: function(o)
					{
						loadMask.hide();
						ShowPesanErrorPenJasRad('Error! Gagal menyimpan data ini. Hubungi Admin', 'Error');
						ViewGridBawahPenjasRad(TrPenJasRad.form.ComboBox.notransaksi.getValue());
					},	
					success: function(o) 
					{
						
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
                            loadMask.hide();
							ShowPesanInfoPenJasRad("Berhasil menyimpan data ini","Information");
							TrPenJasRad.form.ComboBox.notransaksi.setValue(cst.NO_TRANS);
							TrPenJasRad.form.ComboBox.kdpasien.setValue(cst.KD_PASIEN);
							if(cst.KD_PASIEN.substring(0, 1) ==='L'){
								Ext.getCmp('txtNamaUnitRad').setValue('Radiologi');
								Ext.getCmp('txtDokterPengirimRad').setValue('-');
							}
							Ext.getCmp('cboDOKTER_viPenJasRad').disable();
							Ext.getCmp('btnLookupItemPemeriksaanRad').disable();
							Ext.getCmp('btnHpsBrsItemRad').disable();
							Ext.getCmp('btnSimpanPenJasRad').disable();
							ViewGridBawahPenjasRad(TrPenJasRad.form.ComboBox.notransaksi.getValue());
							//RefreshDataFilterPenJasRad();
						}
						else 
						{
							loadMask.hide();
							ShowPesanErrorPenJasRad('Gagal menyimpan data ini. Hubungi Admin', 'Error');
						};
					}
				}
			)
	
};

function getDokterPengirim_rad(no_transaksi,kd_kasir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionRAD/getDokterPengirim",
			params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
			failure: function(o)
			{
				ShowPesanErrorPenJasRad('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtDokterPengirimRad').setValue(cst.nama);
				}
				else 
				{
					ShowPesanErrorPenJasRad('Gagal membaca dokter pengirim', 'Error');
				};
			}
		}
		
	)
}

//combo jenis kelamin
function mComboJK_RAD()
{
    var cboJKPenJasRad = new Ext.form.ComboBox
	(
		{
			id:'cboJKPenJasRad',
			x: 495,
            y: 100,
			typeAhead: false,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis Kelamin ',
			width:105,
			editable:false,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetJKRad,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJKRad=b.data.valueField;
				},
                                'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('txtTempatLahir').focus();
                                                    }, c);
                                                }
			}
		}
	);
	return cboJKPenJasRad;
};

//combo golongan darah
function mComboGDarah_RAD()
{
    var cboGDRPenJasRad = new Ext.form.ComboBox
	(
		{
			id:'cboGDRPenJasRad',
			x: 705,
            y: 100,
			typeAhead: false,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Gol. Darah ',
			width:80,
			editable:false,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetGDarahRad,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGDarahRad=b.data.displayText ;
				},
				/*'render': function(c) {
									c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('txtTempatLahir').focus();
									}, c);
								}*/
			}
		}
	);
	return cboGDRPenJasRad;
};

//Combo Dokter Lookup
function mComboDOKTER_RAD()
{ 
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viPenJasRad = new WebApp.DataStore({ fields: Field });
    dsdokter_viPenJasRad.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,

                        Sort: 'kd_dokter',
                        Sortdir: 'ASC',
                        target: 'ViewDokterPenunjang',
                        param: "kd_unit = '5'" //+"~ )"
                    }
            }
	);
	
    var cboDOKTER_viPenJasRad = new Ext.form.ComboBox
	(
            {
                id: 'cboDOKTER_viPenJasRad',
                x: 130,
				y: 130,
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 230,
                store: dsdokter_viPenJasRad,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
				editable:true,
                listeners:
                    {

                        'select': function(a, b, c) 
						{	
							Ext.getCmp('btnLookupItemPemeriksaanRad').enable();
                        }

                    }
            }
	);
	
    return cboDOKTER_viPenJasRad;
};

function mCboKelompokpasien_RAD(){  
 var cboKelPasienRad = new Ext.form.ComboBox
	(
		{
			
			id:'cboKelPasienRad',
			fieldLabel: 'Kelompok Pasien',
			x: 130,
			y: 160,
			mode: 'local',
			width: 130,
			//anchor: '95%',
			forceSelection: true,
			lazyRender:true,
			triggerAction: 'all',
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
				id: 0,
				fields:
				[
					'Id',
					'displayText'
				],
				   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
				}
			),			
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetKelPasienRad,
			selectOnFocus: true,
			tabIndex:22,
			listeners:
			{
				'select': function(a, b, c)
					{
					   Combo_Select_RAD(b.data.displayText);
					   if(b.data.displayText == 'Perseorangan')
					   {
						  
					   }
					   else if(b.data.displayText == 'Perusahaan')
					   {
						   
					   }
					   else if(b.data.displayText == 'Asuransi')
					   {
						   
					   }
					},
				'render': function(a,b,c)
				{
					Ext.getCmp('txtNamaPesertaAsuransiRad').hide();
					Ext.getCmp('txtNoAskesRad').hide();
					Ext.getCmp('txtNoSJPRad').hide();
					Ext.getCmp('cboPerseoranganRad').hide();
					Ext.getCmp('cboAsuransiRad').hide();
					Ext.getCmp('cboPerusahaanRequestEntryRad').hide();
					
				}
			}

		}
	);
	return cboKelPasienRad;
};

function mComboPerseorangan_RAD(){
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganRadRequest_RAD = new WebApp.DataStore({fields: Field});
    dsPeroranganRadRequest_RAD.load
	(
		{
		params:{
			Skip: 0,
			Take: 1000,
			Sort: '',
			Sortdir: 'ASC',
			target: 'ViewComboKontrakCustomer',
			param: 'customer.status=true and kontraktor.jenis_cust=0 order by customer.customer'
		}}
	)
    var cboPerseoranganRad = new Ext.form.ComboBox
	(
		{
		id:'cboPerseoranganRad',
		x: 280,
		y: 160,
		editable: false,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		selectOnFocus:true,
		forceSelection: true,
		fieldLabel: 'Jenis',
		tabIndex:23,
		width:150,
		valueField: 'KD_CUSTOMER',
		displayField: 'CUSTOMER',
		store: dsPeroranganRadRequest_RAD,
		value:selectSetPerseoranganRad,
		listeners:
		{
			'select': function(a,b,c)
			{
alert(b.data.KD_CUSTOMER);

			Ext.Ajax.request
				({
					url:  baseURL + "index.php/main/functionLABPoliklinik/gettarif",
					 params: {kd_customer: b.data.KD_CUSTOMER},
					failure: function(o)
					{},	    
					success: function(o)
					{
					 var cst = Ext.decode(o.responseText);
					
					 getproduk_PJRWJ(cst.kd_tarif);
					}
				});
			}
		}}
	);
	return cboPerseoranganRad;
};

function mComboPerusahaan_RAD()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaan_RAD = new WebApp.DataStore({fields: Field});
    dsPerusahaan_RAD.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'customer.status=true and kontraktor.jenis_cust=1 order by customer.customer'
			}
		}
	)
    var cboPerusahaanRequestEntryRad = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntryRad',
			x: 280,
			y: 160,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
			width:150,
		    store: dsPerusahaan_RAD,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
		    listeners:
			{
			    'select': function(a,b,c)
				{
					alert(b.data.KD_CUSTOMER);

						Ext.Ajax.request
							({
								url:  baseURL + "index.php/main/functionLABPoliklinik/gettarif",
								 params: {kd_customer: b.data.KD_CUSTOMER},
								failure: function(o)
								{},	    
								success: function(o)
								{
								 var cst = Ext.decode(o.responseText);
								
								 getproduk_PJRWJ(cst.kd_tarif);
								}
							});
				}
			}
		}
	);

    return cboPerusahaanRequestEntryRad;
};

function mComboAsuransi_RAD()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar_RAD = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar_RAD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'customer.status=true and kontraktor.jenis_cust=2 order by customer.customer'
            }
        }
    )
    var cboAsuransiRad = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransiRad',
			x: 280,
			y: 160,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Asuransi',
			align: 'Right',
			width:150,
			//anchor: '95%',
			store: ds_customer_viDaftar_RAD,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
				alert(b.data.KD_CUSTOMER);

			Ext.Ajax.request
				({
					url:  baseURL + "index.php/main/functionLABPoliklinik/gettarif",
					 params: {kd_customer: b.data.KD_CUSTOMER},
					failure: function(o)
					{},	    
					success: function(o)
					{
					 var cst = Ext.decode(o.responseText);
					
					 getproduk_PJRWJ(cst.kd_tarif);
					}
				});
				}
			}
		}
	);
	return cboAsuransiRad;
};


function Combo_Select_RAD(combo)
{
   var value = combo

   if(value == "Perseorangan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiRad').hide()
        Ext.getCmp('txtNoAskesRad').hide()
        Ext.getCmp('txtNoSJPRad').hide()
        Ext.getCmp('cboPerseoranganRad').show()
        Ext.getCmp('cboAsuransiRad').hide()
        Ext.getCmp('cboPerusahaanRequestEntryRad').hide()
        

   }
   else if(value == "Perusahaan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiRad').hide()
        Ext.getCmp('txtNoAskesRad').hide()
        Ext.getCmp('txtNoSJPRad').hide()
        Ext.getCmp('cboPerseoranganRad').hide()
        Ext.getCmp('cboAsuransiRad').hide()
        Ext.getCmp('cboPerusahaanRequestEntryRad').show()
        
   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiRad').show()
         Ext.getCmp('txtNoAskesRad').show()
         Ext.getCmp('txtNoSJPRad').show()
         Ext.getCmp('cboPerseoranganRad').hide()
         Ext.getCmp('cboAsuransiRad').show()
         Ext.getCmp('cboPerusahaanRequestEntryRad').hide()
         
       }
}


function addNewData_RAD(){
	TmpNotransaksiRad='';
	KdKasirAsalRad='';
	TglTransaksiAsal_RAD='';
	Kd_SpesialRad='';
	vkode_customer_RAD = '';
	No_KamarRad='';
	TrPenJasRad.vars.kd_tarif='';
	
	TrPenJasRad.form.ComboBox.kdpasien.setValue('');
	Ext.getCmp('txtDokterPengirimRad').setValue('');
	TrPenJasRad.form.ComboBox.notransaksi.setValue('');
	Ext.getCmp('txtNamaUnitRad').setValue('');
	Ext.getCmp('txtKdUnitRad').setValue('');
	Ext.getCmp('txtKdDokter_RAD').setValue('');
	Ext.getCmp('cboDOKTER_viPenJasRad').setValue('');
	Ext.getCmp('txtAlamatRad').setValue('');
	TrPenJasRad.form.ComboBox.pasien.setValue('');
	Ext.getCmp('dtpTanggalLahirL').setValue(nowTglTransaksi_RAD);
	Ext.getCmp('txtKdUrutMasukRad').setValue('');
	Ext.getCmp('cboJKPenJasRad').setValue('');
	Ext.getCmp('cboGDRPenJasRad').setValue('')
	Ext.getCmp('txtKdCustomerLamaHideR').setValue('');//tampung kd customer untuk transaksi selain kunjungan langsung
	Ext.getCmp('cboKelPasienRad').setValue('');
	Ext.getCmp('txtCustomerLamaHideR').setValue('');
	Ext.getCmp('cboAsuransiRad').setValue('');
	Ext.getCmp('txtNamaPesertaAsuransiRad').setValue('');
	Ext.getCmp('txtNoSJPRad').setValue('');
	Ext.getCmp('txtNoAskesRad').setValue('');
	Ext.getCmp('cboPerusahaanRequestEntryRad').setValue('');
	Ext.getCmp('txtKdDokter_RADPengirimR').setValue('');
	
	Ext.getCmp('txtCustomerLamaHideR').hide();
	Ext.getCmp('cboAsuransiRad').hide();
	Ext.getCmp('txtNamaPesertaAsuransiRad').hide();
	Ext.getCmp('txtNoSJPRad').hide();
	Ext.getCmp('txtNoAskesRad').hide();
	Ext.getCmp('cboPerusahaanRequestEntryRad').hide();
	Ext.getCmp('cboKelPasienRad').show();
	
	Ext.getCmp('btnLookupItemPemeriksaanRad').disable();
	Ext.getCmp('btnHpsBrsItemRad').disable();
	
	TrPenJasRad.form.ComboBox.notransaksi.enable();
	Ext.getCmp('txtDokterPengirimRad').enable();
	Ext.getCmp('txtNamaUnitRad').enable();
	Ext.getCmp('cboDOKTER_viPenJasRad').enable();
	TrPenJasRad.form.ComboBox.pasien.enable();
	TrPenJasRad.form.ComboBox.kdpasien.enable();
	Ext.getCmp('txtAlamatRad').enable();
	Ext.getCmp('cboJKPenJasRad').enable();
	Ext.getCmp('cboGDRPenJasRad').enable();
	Ext.getCmp('cboKelPasienRad').enable();
	Ext.getCmp('dtpTanggalLahirL').enable();
	Ext.getCmp('dtpKunjunganRad').enable();
	
	TrPenJasRad.form.ComboBox.notransaksi.setReadOnly(false);
	Ext.getCmp('txtNamaUnitRad').setReadOnly(true);
	Ext.getCmp('cboDOKTER_viPenJasRad').setReadOnly(false);
	TrPenJasRad.form.ComboBox.pasien.setReadOnly(false);
	Ext.getCmp('txtAlamatRad').setReadOnly(false);
	Ext.getCmp('cboJKPenJasRad').setReadOnly(false);
	Ext.getCmp('cboGDRPenJasRad').setReadOnly(false);
	Ext.getCmp('cboKelPasienRad').setReadOnly(false);
	Ext.getCmp('dtpTanggalLahirL').setReadOnly(false);
	//Ext.getCmp('dtpKunjunganRad').setReadOnly(true);
	
	ViewGridBawahLookupPenjasRad('');
	//ViewGridBawahPenjasRad('');
}

function ShowPesanWarningPenJasRad(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPenJasRad(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPenJasRad(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function PilihDokterLookUp(NoTrans,TglTrans,kdPrdk,kdTrf,tglBerlaku,trf) 
{
	
	
	RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk);
	
	 var FormPilihDokter = 
	  {
			layout: 'column',
						border: false,
						anchor: '90%',
						items:
						[
						
							{
								columnWidth: 0.18,
								layout: 'form',
								labelWidth:70,
								border: false,
								items:
								[
								{
								xtype:'button',
								text:'Simpan',
							 	width:70,
								hideLabel:true,
								id: 'BtnOktrDokter',
								handler:function()
								{
								if(dataTrDokter.length == '' || dataTrDokter.length== 'undefined')
								{
									var jumlah =0;
								}
								else
								{
									var jumlah = dataTrDokter.length;
								}
								for(var i = 0 ;i< dsTrDokter.getCount()  ;i++)
								{	
								dataTrDokter.push({
								index		: i,
								no_tran 	: NoTrans,
								//urut		: Urt,
								tgl_trans 	: TglTrans,
								kd_produk 	: kdPrdk,
								kd_tarif	: kdTrf,
								tgl_berlaku	: tglBerlaku,
								tarif		: trf,
								kd_dokter 	: dsTrDokter.data.items[i].data.KD_DOKTER ,
								kd_job 		: dsTrDokter.data.items[i].data.KD_JOB ,
								});
								}
								FormLookUDokter.close();	
								}
								},
								],
								},			
								{
								columnWidth: 0.2,
								layout: 'form',
								labelWidth:70,
								border: false,
								items:
								[	
								{
								xtype:'button',
								text:'Batal' ,
								width:70,
								hideLabel:true,
								id: 'BtnCancelGantiDokter',
								handler:function() 
									{
								dataTrDokter = [];	
								FormLookUDokter.close()
									}
								}
								]
								}
								
									
								]
  }

    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
         new Ext.grid.RowNumberer(),
        {
        	 header			: 'kd_dokter',
        	 dataIndex		: 'KD_DOKTER',
			 width			: 80,
        	 menuDisabled	: true,
        	 hidden 		: false
        },{
            header			:'Nama Dokter',
            dataIndex		: 'NAMA',
            sortable		: false,
            hidden			: false,
			menuDisabled	: true,
			width			: 200,
            editor			: getTrDokter(dsTrDokter)
	    },{
            header			: 'Job',
            dataIndex		: 'KD_JOB',
            width			:150,
		   	menuDisabled	:true,
			editor			: JobDokter(),

        },
        ]
    );
		
  var GridPilihDokter= new Ext.grid.EditorGridPanel({
        title		: '',
		id			: 'GridDokterTr',
		stripeRows	: true,
		width		: 490,
		height		: 245,
        store		: RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk),
        border		: false,
        frame		: false,
        anchor		: '100% 60%',
        autoScroll	: true,
        cm			: GridTrDokterColumnModel,
		
        //viewConfig	: {forceFit: true},
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
        	trcellCurrentTindakan = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
   		  }
		}
    });

 
	
	var PanelTrDokter = new Ext.Panel
	({
            id: 'PanelPilitTrDokter',
            region: 'center',
			width: 490,
			height: 260,
            border:false,
            layout: 'form',
            frame:true,
            anchor: '100% 8.0001%',
            autoScroll:false,
			
            items:
            [
			GridPilihDokter,
			{xtype: 'tbspacer',height: 3, width:100},	
			FormPilihDokter
			]
	})
 
	
    var lebar = 500;
    var FormLookUDokter = new Ext.Window
    (
        {
            id: 'gridTrDokter',
            title: 'Pilih Dokter Tindakan',
            closeAction: 'destroy',
            width: lebar,
            height: 270,
            border: false,
            resizable: false,
            plain: true,
            layout: 'column',
            iconCls: 'Request',
            modal: true,
			tbar		:[
						{
						xtype	: 'button',
						id		: 'btnaddtrdokter',
						iconCls	: 'add',
						text	: 'Tambah Data Dokter',
						handler	:  function()
						{
						dataTrDokter = [];		
						TambahBaristrDokter(dsTrDokter);	
						}
						},
						'-',
						{
						xtype	: 'button',
						id		: 'btndeltrdokter',
						iconCls	: 'remove',
						text	: 'Delete Data Dokter',
						handler	: function()  
						{
						    if (dsTrDokter.getCount() > 0 )
							{
                            if(trcellCurrentTindakan != undefined)
							{
                             HapusDataTrDokter(dsTrDokter);
                            }
                        	}else{
                            ShowPesanWarningDiagnosa('Pilih record ','Hapus data');
                    		}	
							}
						},
						'-',
			],
           	items: [ 
			//GridPilihDokter,
			
			PanelTrDokter
			],
            listeners:
            { 
            }
        }
    );
 
 
 FormLookUDokter.show();


	

};

var mtrDataDokter = Ext.data.Record.create([
	   {name: 'KD_DOKTER', mapping:'KD_DOKTER'},
	   {name: 'NAMA', mapping:'NAMA'},
	   {name: 'KD_JOB', mapping:'KD_JOB'},
	]);

function TambahBaristrDokter(store){
    var x=true;
    if (x === true) {
        var p = BaristrDokter();
        store.insert(store.getCount(), p);
    }
}

function BaristrDokter(){
	var p = new mtrDataDokter({
		'KD_DOKTER':'',
		'NAMA':'',
	    'KD_JOB':'',
	});
	return p;
};

function getTrDokter(store){
	var trDokterData = new Ext.form.ComboBox({
	   typeAhead: true,
	   triggerAction: 'all',
	   lazyRender: true,
	   mode: 'local',
	   hideTrigger:true,
	   forceSelection: true,
	   selectOnFocus:true,
	   store: GetDokter(),
	   valueField: 'NAMA',
	   displayField: 'NAMA',
	   anchor: '95%',
	   listeners:{
		   select: function(a, b, c){
			  store.data.items[trcellCurrentTindakan].data.KD_DOKTER = b.data.KD_DOKTER;
		   }		
	   }
	});
	return trDokterData;
}

function GetDokter(){
	var dataDokter  = new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA'] });
	dataDokter.load({ 
		params: { 
			Skip: 0, 
			Take: 50, 
			target:'ViewComboDokter',
			param: ''
		} 
	});
	return dataDokter;
}

function JobDokter(){
	var combojobdokter = new Ext.form.ComboBox({
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '96.8%',
		emptyText		: '',
		store			: new Ext.data.ArrayStore({
			fields	: ['Id','displayText'],
			data	: [[1, 'Dokter'],[2, 'Dokter Anastesi']]
		}),
		valueField		: 'displayText',
		displayField	: 'displayText'
	});
	return combojobdokter;
};


function RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk)
{
	dsTrDokter.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewTrDokter',
			param	:"b.kd_kasir='06' and b.no_transaksi=~"+NoTrans+"~ and b.tgl_transaksi=~"+TglTrans+"~ and a.kd_produk = ~"+kdPrdk+"~"
		} 
	});
	
	return dsTrDokter;
}

function HapusDataTrDokter(store)
{

    if ( trcellCurrentTindakan != undefined ){
        if (store.data.items[trcellCurrentTindakan].data.KD_DOKTER != '' && store.data.items[trcellCurrentTindakan].data.KD_PRODUK != ''){
					DataDeleteTrDokter(store);
					dsTrDokter.removeAt(trcellCurrentTindakan);
				}
               
        }/*else{
            dsTrDokter.removeAt(trcellCurrentTindakan);
        }*/
   

}

function DataDeleteTrDokter(store){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/DeleteDataObj",
        params:  
		{
			Table			:'ViewTrDokter',
			no_transaksi	:Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),
			kd_unit			:Ext.getCmp('txtKdUnitIGD').getValue(),
			kd_produk		:store.data.items[trcellCurrentTindakan].data.KD_PRODUK,
			kd_kasir		:'06',
			kd_dokter		:store.data.items[trcellCurrentTindakan].data.KD_DOKTER,
			urut			:PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,
			tgl_transaksi	:PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,
		},
		//getParamDataDeleteKasirIGDDetail(),
        success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                store.removeAt(CurrentKasirIGD.row);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
                RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.KD_PRODUK);
				trcellCurrentTindakan=undefined;
            }else{
                ShowPesanInfoDiagnosa(nmPesanHapusError,nmHeaderHapusData);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
				RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.KD_PRODUK);
            }
        }
    });
}

