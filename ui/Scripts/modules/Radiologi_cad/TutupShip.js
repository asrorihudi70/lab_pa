var CurrentKasirShift =
{
    data: Object,
    details: Array,
    row: 0
};

var tampungshiftsekarang_TutupShiftRad;
var tampungshiftnanti_TutupShiftRad;
var AddNewKasirShift = true;
var now = new Date();
var FormLookUp_TutupShiftRad;
var nowTglTransaksi = new Date();
//var FocusCtrlCMShift;

CurrentPage.page = getPanelShift_TutupShiftRad(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelShift_TutupShiftRad(mod_id) 
{
   
    var FormDepanShift_TutupShiftRad = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Shift Radiologi',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [ShiftLookUp_TutupShiftRad()],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	

	
   return FormDepanShift_TutupShiftRad

};


function ShiftLookUp_TutupShiftRad(rowdata) 
{
    var lebar = 350;
    FormLookUp_TutupShiftRad = new Ext.Window
    (
        {
            id: 'gridShift',
            title: 'Tutup Shift',
            closeAction: 'destroy',
            width: lebar,
            height: 240,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
			bodyStyle: 'padding:5px 5px 5px 5px',
            modal: true,
            items: getFormEntry_TutupShiftRad(lebar),
            listeners:
            {
                activate: function()
                {
                    getShiftRad();
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
					
                }
            },
			fbar:[
				{
					xtype:'button',
					text:nmBtnOK,
					width:70,
					style:{'margin-left':'0px','margin-top':'0px'},
					hideLabel:true,
					id: 'btnOkShift_TutupShiftRad',
					handler:function()
					{
						TutupShiftSave_TutupShiftRad(false);
						
					}
				},
				{
						xtype:'button',
						text:nmBtnCancel ,
						width:70,
						hideLabel:true,
						id: 'btnCancelShift_TutupShiftRad',
						handler:function() 
						{
							FormLookUp_TutupShiftRad.close();
						}
				}
			]
        }
    );

    FormLookUp_TutupShiftRad.show();
    if (rowdata == undefined) 
	{
        getShiftRad();
    }
    else 
	{
        
    }

};

function getFormEntry_TutupShiftRad(lebar) 
{
    var pnlTutupShift_TutupShiftRad = new Ext.FormPanel
    (
        {
            id: 'PanelTRShift',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:300,
            anchor: '100%',
            width: lebar,
            border: false,
            items: 
			[
				getItemPanelInputShift_TutupShiftRad(lebar)
			],
			tbar:
            [
               
               
            ]
        }
    );
    var FormPanelDepanShift_TutupShiftRad = new Ext.Panel
	(
		{
		    id: 'FormPanelShiftDepan_TutupShiftRad',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTutupShift_TutupShiftRad	
				
			]

		}
	);

    return FormPanelDepanShift_TutupShiftRad
};
//---------------------------------------------------------------------------------------///

function getItemPanelInputShift_TutupShiftRad(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-45,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:120,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelDetailShift_TutupShiftRad(lebar)		
				]
			}
		]
	};
    return items;
};


function getItemPanelDetailShift_TutupShiftRad(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .70,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'numberfield',
					    fieldLabel:  'Shift Ke',
					    name: 'txtNilaiShift_TutupShiftRad',
					    id: 'txtNilaiShift_TutupShiftRad',
						readOnly:true,
					    anchor: '100%'
					},
					
					{
					    xtype: 'textfield',
					    fieldLabel: 'Shift Selanjutnya',
					    name: 'txtNilaiShiftSelanjutnya_TutupShiftRad',
					    id: 'txtNilaiShiftSelanjutnya_TutupShiftRad',
						readOnly:true,
					    anchor: '100%'
						
					},
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tgl Shift ',
					    id: 'dtpTanggalShift_TutupShiftRad',
					    name: 'dtpTanggalShift_TutupShiftRad',
					    format: 'd/M/Y',
						readOnly:true,
					    value: now,
					    anchor: '100%'
					
				
					}
					
				]
			},
			
		]
	}
    return items;
};


//---------------------------------------------------------------------------------------///
function getShiftRad() 
{
    AddNewKasirShift = true;
   // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request
   // ajax request untuk mengambil data current shift	
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/rad/functionTutupShift/getCurrentShiftRad",
		params: {
	        //UserID: 'Admin',
	        command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			tampungshiftsekarang_TutupShiftRad=o.responseText
			Ext.get('txtNilaiShift_TutupShiftRad').dom.value =tampungshiftsekarang_TutupShiftRad ;
	    }
	
	});
	
	
	 // ajax request untuk mengambil data max shift	
	Ext.Ajax.request(
	{ 
	    url: baseURL + "index.php/rad/functionTutupShift/getMaxkdbagian",
		params: {
	        command: '0',
		
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			tampungshiftnanti_TutupShiftRad= o.responseText;
			if (tampungshiftsekarang_TutupShiftRad <tampungshiftnanti_TutupShiftRad) {
				Ext.get('txtNilaiShiftSelanjutnya_TutupShiftRad').dom.value =parseFloat(tampungshiftsekarang_TutupShiftRad)+1;
			} else{
				Ext.get('txtNilaiShiftSelanjutnya_TutupShiftRad').dom.value =1;
			}

	    }
	});

};



//---------------------------------------------------------------------------------------///


function TutupShiftSave_TutupShiftRad(mBol)
{
    if (ValidasiEntryTutupShift(nmHeaderSimpanData,false) == 1 )
    {
            
		Ext.Ajax.request({
			url: baseURL + "index.php/rad/functionTutupShift/tutupShift",
			params: getParamTutupShift(),
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfoShift('Simpan Shift Berhasil','Simpan Shift');
					FormLookUp_TutupShiftRad.close();
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanErrorShift('Simpan Shift Gagal','Simpan Shift');
				}
				else
				{
					ShowPesanErrorShift('Simpan Shift Gagal','Simpan Shift');
				};
			}
		})
            
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };

};//END FUNCTION TutupShiftSave_TutupShiftRad
//---------------------------------------------------------------------------------------///
function getParamTutupShift()
{
    var params =
	{
	    shiftKe: Ext.getCmp('txtNilaiShift_TutupShiftRad').getValue(),
		shiftSelanjutnya :Ext.getCmp('txtNilaiShiftSelanjutnya_TutupShiftRad').getValue(),
		tanggal :Ext.getCmp('dtpTanggalShift_TutupShiftRad').getValue(),
		
	};
    return params
};


function ValidasiEntryTutupShift(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtNilaiShift_TutupShiftRad').getValue() == '' || (Ext.get('txtNilaiShiftSelanjutnya_TutupShiftRad').getValue() == ''))
	{
		if (Ext.get('txtNilaiShift_TutupShiftRad').getValue() == '' && mBolHapus === true)
		{
			x=0;
		}
		else if (Ext.get('txtNilaiShiftSelanjutnya_TutupShiftRad').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningTutupShift(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};

function ShowPesanWarningShift(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorShift(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoShift(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};










