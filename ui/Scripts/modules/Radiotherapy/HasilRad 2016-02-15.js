var CurrentHasilRad = {
    data: Object,
    details: Array,
    row: 0
};

var tanggaltransaksitampung;

var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView = 'All';
var nowTglTransaksi = new Date();

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwj = 'Belum Posting';
var dsTRHasilRadList;
var dsTRDetailHasilRadList;
var dsTRDetailDiagnosaList;
var AddNewHasilRad = true;
var selectCountHasilRad = 10;

var rowSelectedHasilRad;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRrwj;
var valueStatusCMHasilRadView = 'All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew = true;
var tigaharilalu = new Date().add(Date.DAY, -3);//setting tgl 3 hari sebelum hari ini (-3)
var tampil;
var firstGrid;

//var FocusCtrlCMHasilRad;
var vkode_customer;
CurrentPage.page = getPanelHasilRad(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelHasilRad(mod_id) {
    
    var Field = ['KD_PASIEN', 'NAMA', 'ALAMAT', 'TGL_MASUK',' KD_DOKTER ','NAMA_UNIT' ,'KD_KELAS',
                 'URUT_MASUK', 'NAMA_DOKTER','TGL_LAHIR','JENIS_KELAMIN', 'GOL_DARAH','NAMA_UNIT_ASAL'];
    dsTRHasilRadList = new WebApp.DataStore({
        fields: Field
    });
    
    refeshHasilRad();
    var grListTRHasilRad = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dsTRHasilRadList,
        anchor: '100% 70%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout    : 'fit',
        region    : 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function(sm, row, rec) {
                    rowSelectedHasilRad = dsTRHasilRadList.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function(sm, ridx, cidx) {
                rowSelectedHasilRad = dsTRHasilRadList.getAt(ridx);
                if (rowSelectedHasilRad !== undefined) {
                    HasilRadLookUp(rowSelectedHasilRad.data);
                } else {
                    HasilRadLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
            [

                {
                    id: 'colMedrecViewHasilRad',
                    header: 'No. Medrec',
                    dataIndex: 'KD_PASIEN',
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    width: 50
                }, {
                    id: 'colNamaViewHasilRad',
                    header: 'Nama Pasien',
                    dataIndex: 'NAMA',
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    width: 150
                }, {
                    id: 'colAlamatViewHasilRad',
                    header: 'Alamat',
                    dataIndex: 'ALAMAT',
                    width: 220,
                    sortable: false,
                    hideable: false,
                    menuDisabled: true
                    
                }, {
                    id: 'colTglmasukViewHasilRad',
                    header: 'Tgl. Masuk',
                    dataIndex: 'TGL_MASUK',
                    width: 60,
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    renderer: function(v, params, record) {
                        return ShowDate(record.data.TGL_MASUK);
                    }
                    
                }, {
                    id: 'colKddokterViewHasilRad',
                    header: 'Kode Dokter',
                    dataIndex: 'KD_DOKTER',
                    sortable: false,
                    hidden: true,
                    menuDisabled: true,
                    width: 170
                },
                {
                    id: 'colNamaunitViewHasilRad',
                    header: 'Unit',
                    dataIndex: 'NAMA_UNIT',
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    width: 60
                }, {
                    id: 'colKdkelasViewHasilRad',
                    header: 'Kode Kelas',
                    dataIndex: 'KD_KELAS',
                    sortable: false,
                    hidden: true,
                    menuDisabled: true,
                    width: 90
                }, {
                    id: 'colKddokterViewHasilRad',
                    dataIndex: 'KD_DOKTER',
                    header: 'kode Dokter',
                    width: 250,
                    sortable: false,
                    hidden: true,
                    menuDisabled: true                    
                },
                {
                    id: 'colUrutmasukViewHasilRad',
                    header: 'Urut Masuk',
                    dataIndex: 'URUT_MASUK',
                    sortable: false,
                    hidden: true,
                    menuDisabled: true,
                    width: 90
                },
                {
                    id: 'colNamadokterViewHasilRad',
                    header: 'Dokter',
                    dataIndex: 'NAMA_DOKTER',
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    width: 90
                },
                {
                    id: 'colTglLahirViewHasilRad',
                    header: 'Tgl. Lahir',
                    dataIndex: 'TGL_LAHIR',
                    sortable: false,
                    hidden: true,
                    menuDisabled: true,
                    width: 90
                },
                {
                    id: 'colJKViewHasilRad',
                    header: 'Jenis Kelamin',
                    dataIndex: 'JENIS_KELAMIN',
                    sortable: false,
                    hidden: true,
                    menuDisabled: true,
                    width: 90
                },
                {
                    id: 'colGolDarViewHasilRad',
                    header: 'Golongan Darah',
                    dataIndex: 'GOL_DARAH',
                    sortable: false,
                    hidden: true,
                    menuDisabled: true,
                    width: 90
                },
                {
                    id: 'colasalViewHasilRad',
                    header: 'Unit Asal',
                    dataIndex: 'NAMA_UNIT_ASAL',
                    sortable: false,
                    hidden: true,
                    menuDisabled: true,
                    width: 90
                }
            ]
        ),
        viewConfig: {
            forceFit: true
        },
        tbar: [{
            id: 'btnEditHasilRad',
            text: nmEditData,
            tooltip: nmEditData,
            iconCls: 'Edit_Tr',
            handler: function(sm, row, rec) {
                HasilRadLookUp();
            }
        }]
    });



    var FormDepanHasilRad = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Hasil Radiologi',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getItemPanelHasilRad(),
            grListTRHasilRad
        ],
        listeners: {
            'afterrender': function() {
                
            }
        }
    });

    return FormDepanHasilRad;

};

function HasilRadLookUp(rowdata) {
    var lebar = 900;
    FormLookUpsdetailTRrwj = new Ext.Window({
        id: 'gridHasilRad',
        title: 'Hasil Radiologi',
        closeAction: 'destroy',
        width: lebar,
        height: 500,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: dataGrid_viInformasiUnitdokter(rowdata),
        listeners: {

        }
    });

    FormLookUpsdetailTRrwj.show();
    if (rowdata === undefined) {
        HasilRadAddNew();
    } else {
        TRHasilRadInit(rowdata);
    }

};

function InputLookupHasil(rowdata) {
    
    FormLookInputLookupHasil = new Ext.Window({
        id: 'gridKonsultasi',
        title: rowdata.DESKRIPSI,
        closeAction: 'destroy',
        width: 500,
        height: 400,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        iconCls: 'Request',
        modal: true,
        items: {
            layout: 'hBox',
            border: false,
            bodyStyle: 'padding:5px 0px 5px 5px',
            defaults: {
            margins: '3 3 3 3'
            },
            anchor: '90%',
            items: 
            [
                {
                    columnWidth: .50,
                    layout: 'absolute',
                    bodyStyle: 'padding: 10px 10px 10px 10px',
                    border: false,
                    width: 475,
                    height: 400,
                    anchor: '100% 100%',
                    items:
                    [
                        {  
                            x : 10,
                            y : 10, 
                            xtype: 'textarea',
                            name: 'textarea',
                            id: 'textareahasil',
                            anchor: '100%',
                            height: 280
                        },
                        {
                            // x: 310,
                            // y: 300,
							x: 390,
                            y: 300,
                            xtype:'button',
                            text: 'Simpan',
                            iconCls: 'save',
                            width: 70,
                            height: 35,
                            hideLabel: false,
                            handler: function(sm, row, rec)
                            {
								tampil=1;
                                Datasave_HasilRad(rowdata);
                            }
                        },
                        /* {
                            x: 390,
                            y: 300,
                            xtype:'button',
                            text: 'Batal',
                            iconCls: 'cancel',
                            width: 70,
                            height: 35,
                            hideLabel: false,
                            handler: function(sm, row, rec)
                            {
//                                RefreshDataFilterPenJasRad();
                            }
                        } */
                    ]
                }
            ]
        },
        listeners: {
			'afterrender': function() {
				Ext.getCmp('textareahasil').setValue(rowdata.HASIL);
				tampil=1;

			}
        }
    });

    FormLookInputLookupHasil.show();


};

var tmpdatahasilsebelumsaving;
function dataGrid_viInformasiUnitdokter(mod_id,rowdata)
{
    var Field_Hasil_viDaftar = ['KLASIFIKASI','DESKRIPSI','KD_TEST','NORMAL','SATUAN','HASIL','URUT'];
    
    dsTRDetailHasilRadList = new WebApp.DataStore({fields: Field_Hasil_viDaftar});

        firstGrid = new Ext.grid.GridPanel({
            x                : 10,
            y                : 10, 
            ddGroup          : 'secondGridDDGroup',
            store            : dsTRDetailHasilRadList,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 200,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Hasil Pemeriksaan',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNRM_viDaftar',
						header: 'Kd. Test',
						dataIndex: 'KD_TEST',
						sortable: true,
						width: 20
					},
					{
						id: 'colNMPASIEN_viDaftar',
						header: 'Pemeriksaan',
						dataIndex: 'DESKRIPSI',
						sortable: true,
						width: 100
					}
				]
			),
			selModel: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelectedDetHasilRad = dsTRDetailHasilRadList.getAt(row);
							if (rowSelectedDetHasilRad !== undefined)
							{
								tampil=0;
								Ext.get('textareapopuphasil').dom.value = rowSelectedDetHasilRad.data.HASIL;
								tmpdatahasilsebelumsaving = rowSelectedDetHasilRad.data;
								
							}
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedDetHasilRad = dsTRDetailHasilRadList.getAt(ridx);
					if (rowSelectedDetHasilRad !== undefined)
					{
						tampil=1;
						InputLookupHasil(rowSelectedDetHasilRad.data);
					}
				},
				'afterrender': function(){
					tampil=0;
				}
			},
			viewConfig: 
				{
						forceFit: true
				}
        });
        
        var FrmTabs_popupdatahasilrad = new Ext.Panel
        (
		{
		    id: mod_id,
		    closable  : true,
		    region    : 'center',
		    layout    : 'column',
			height    : 400,
		    itemCls   : 'blacklabel',
		    bodyStyle : 'padding: 5px 5px 5px 5px',
		    border    : false,
		    shadhow   : true,
		    margins   : '5 5 5 5',
		    anchor    : '99%',
		    iconCls   : '',
		    items     : 
			[
				{
					columnWidth: .99,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 100,
					height: 150,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'No. Medrec '
						},
						{
							x: 110,
							y: 10,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 10,
							xtype: 'textfield',
							name: 'TxtPopupMedrec',
							id: 'TxtPopupMedrec',
							width: 80
						},
						{
							x: 210,
							y: 10,
							xtype: 'label',
							text: 'Tanggal '
						},
						{
							x: 250,
							y: 10,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 260,
							y : 10,
							xtype: 'textfield',
							name: 'TxtPopupTglCekPasien',
							id: 'TxtPopupTglCekPasien',
							width: 110
						},
						{
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Nama Pasien '
						},
						{
							x: 110,
							y: 40,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupNamaPasien',
							id: 'TxtPopupNamaPasien',
							width: 250,
							enableKeyEvents: true,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										getkriteriacari();
									} 						
								}
							}
						},
						{
							x: 380,
							y: 20,
							xtype: 'label',
							text: 'Tgl Lahir '
						},
						{   
							x : 380,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupTglLahirPasien',
							id: 'TxtPopupTglLahirPasien',
							width: 100
						},
						{
							x: 490,
							y: 20,
							xtype: 'label',
							text: 'Thn '
						},
						{   
							x : 490,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupThnLahirPasien',
							id: 'TxtPopupThnLahirPasien',
							width: 30
						},
						{
							x: 530,
							y: 20,
							xtype: 'label',
							text: 'Bln '
						},
						{   
							x : 530,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupBlnLahirPasien',
							id: 'TxtPopupBlnLahirPasien',
							width: 30
						},
						{
							x: 570,
							y: 20,
							xtype: 'label',
							text: 'Hari '
						},
						{   
							x : 570,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupHariLahirPasien',
							id: 'TxtPopupHariLahirPasien',
							width: 30
						},
						{
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Alamat '
						},
						{
							x: 110,
							y: 70,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 70,
							xtype: 'textfield',
							name: 'TxtPopupAlamat',
							id: 'TxtPopupAlamat',
							width: 480
						},
						{
							x: 10,
							y: 100,
							xtype: 'label',
							text: 'Dokter '
						},
						{
							x: 110,
							y: 100,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtPopupNamaDokter',
							id: 'TxtPopupNamaDokter',
							width: 240
						}
					]
				},
				{
					columnWidth: .50,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 100,
					height: 290,
					anchor: '100% 100%',
					items:
					[
						firstGrid                                            
					]
				},
				{
					columnWidth: .50,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 100,
					height: 290,
					anchor: '100% 100%',
					items:
					[
						{  
							x : 10,
							y : 10, 
							xtype: 'textarea',
							name: 'textarea',
							id: 'textareapopuphasil',
							anchor: '100%',
							height: 270,
							listeners:
							{
								blur: function() {
									tampil=0;
									Ext.Ajax.request ({
										url: baseURL + "index.php/main/CreateDataObj",
										params: getParamDetailHasilRad(tmpdatahasilsebelumsaving),
										success: function(o)
										{
											var cst = Ext.decode(o.responseText);
											if (cst.success === true)
											{
												RefreshDataHasilRadDetail(cst.KD_PASIEN,cst.TGL_MASUK,cst.URUT_MASUK);
											}
											else if  (cst.success === false && cst.pesan===0)
											{
												ShowPesanWarningHasilRad('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
											}
											else
											{
												ShowPesanErrorHasilRad('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
											}
										}
									});
								}
							}
						}
					]
				}
			],
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viDaftar',
						handler: function()
						{
                            
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype:'button',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viDaftar',
						handler: function()
						{
						   DataPrint_HasilRad();
						}
					}
				]
			}
    }
    );
    
    return FrmTabs_popupdatahasilrad;
}

function DataPrint_HasilRad() 
{
	Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/main/cetaklaporanRadLab/LapRad",
			params: getdatahasilpasien(),
			failure: function(o)
			{

			},	
			success: function(o) 
			{
				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					window.open(cst.url, 'RAD', "height=,width=");                                                       
				}
				else 
				{

				};
			}
		}
	);
	
};

function getdatahasilpasien()
{
    var tmpumur;
    if (Ext.get('TxtPopupThnLahirPasien').getValue() !== '0')
    {
        tmpumur = Ext.get('TxtPopupThnLahirPasien').getValue() + " Thn";
    }else if (Ext.getCmp('TxtPopupBlnLahirPasien').getValue() !== '0')
    {
        tmpumur = Ext.get('TxtPopupThnLahirPasien').getValue() + " Bln";
    }else
    {
		tmpumur = Ext.getCmp('TxtPopupHariLahirPasien').getValue() + " Hari";
    }
    
	var params = {
        Tgl: Ext.get('TxtPopupTglCekPasien').getValue(),
        KdPasien: Ext.get('TxtPopupMedrec').getValue(),
        Nama: Ext.get('TxtPopupNamaPasien').getValue(),        
        JenisKelamin: tmpjkpopupentry,
        Alamat: Ext.get('TxtPopupAlamat').getValue(),
        Poli: tmppolipopupentry,
        urutmasuk: tmpurutmasukpopupentry,
        Dokter:Ext.get('TxtPopupNamaDokter').getValue(),
        Umur: tmpumur
    };
    return params;
}

function ShowPesanWarningDiagnosa(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.WARNING,
        width: 250
    });
};

function ShowPesanErrorDiagnosa(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.ERROR,
        width: 250
    });
};

function ShowPesanInfoDiagnosa(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.INFO,
        width: 250
    });
};
var tmptglMasukPopupentry;
var tmpurutmasukpopupentry;
var tmpjkpopupentry;
var tmppolipopupentry;

function TRHasilRadInit(rowdata) {
    AddNewHasilRad = false;

    Ext.get('TxtPopupMedrec').dom.value = rowdata.KD_PASIEN;
    Ext.get('TxtPopupTglCekPasien').dom.value = ShowDate(rowdata.TGL_MASUK);
    Ext.get('TxtPopupNamaPasien').dom.value = rowdata.NAMA;
    Ext.get('TxtPopupTglLahirPasien').dom.value = ShowDate(rowdata.TGL_LAHIR);
    Ext.get('TxtPopupAlamat').dom.value = rowdata.ALAMAT;
    Ext.get('TxtPopupNamaDokter').dom.value = rowdata.NAMA_DOKTER;
    tmpjkpopupentry = rowdata.JENIS_KELAMIN;
    tmpurutmasukpopupentry = rowdata.URUT_MASUK;
    tmptglMasukPopupentry = rowdata.TGL_MASUK;
    tmppolipopupentry = rowdata.NAMA_UNIT;
    setUsia(ShowDate(rowdata.TGL_LAHIR));
    
    Ext.getCmp('TxtPopupMedrec').disable();
    Ext.getCmp('TxtPopupTglCekPasien').disable();
    Ext.getCmp('TxtPopupNamaPasien').disable();
    Ext.getCmp('TxtPopupTglLahirPasien').disable();
    Ext.getCmp('TxtPopupAlamat').disable();
    Ext.getCmp('TxtPopupNamaDokter').disable();
    Ext.getCmp('TxtPopupThnLahirPasien').disable();
    Ext.getCmp('TxtPopupBlnLahirPasien').disable();
    Ext.getCmp('TxtPopupHariLahirPasien').disable();
    RefreshDataHasilRadDetail(rowdata.KD_PASIEN,rowdata.TGL_MASUK,rowdata.URUT_MASUK,'5');
    Ext.Ajax.request({
        url: baseURL + "index.php/main/getcurrentshift",
        params: 
            {
                command: '0'
            },
            failure: function(o) {
            var cst = Ext.decode(o.responseText);
        },
        success: function(o) {
            tampungshiftsekarang = o.responseText;
        }

    });
};

function RefreshDataHasilRadDetail(kd_pasien,tgl_masuk,urut) {
    var strKriteriaHasilRad = '';
    
    strKriteriaHasilRad = "Rad_hasil.Kd_Pasien = '"+kd_pasien+"' And Rad_hasil.Tgl_Masuk = ('"+tgl_masuk+"')  and Rad_hasil.Urut_Masuk ="+urut+"  and Rad_hasil.kd_unit= '5'";

    dsTRDetailHasilRadList.load({
        params: {
            Skip: 0,
            Take: 1000,
            Sort: 'tgl_transaksi',
            Sortdir: 'ASC',
            target: 'ViewDetHasilRad',
            param: strKriteriaHasilRad
        }
    });
    return dsTRDetailHasilRadList;
};

function getParamDetailHasilRad(rowdata) {
	var hasil;
	if(tampil==1){
		hasil=Ext.getCmp('textareahasil').getValue();
	} else{
		hasil=Ext.getCmp('textareapopuphasil').getValue();
	}
    var params = {
        Table: 'ViewHasilRad',

        KdPasien: Ext.get('TxtPopupMedrec').getValue(),
        KdUnit: '5',
        Tgl: tmptglMasukPopupentry,
        UrutMasuk : tmpurutmasukpopupentry,
        urut: rowdata.URUT,
        KdTest:rowdata.KD_TEST,
        Hasil: hasil
    };
    return params;
};

function getItemPanelmedrec(lebar) {
    var items = {
        layout: 'column',
        border: false,
        items: [{
            columnWidth: .40,
            layout: 'form',
            labelWidth: 100,
            border: false,
            items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: 'No. Medrec',
					name: 'txtNoMedrecDetransaksi',
					id: 'txtNoMedrecDetransaksi',
					readOnly: true,
					anchor: '99%',
					listeners: {

					}
				}, {
					xtype: 'textfield',
					fieldLabel: 'Nama Pasien ',
					readOnly: true,
					name: 'txtNamaPasienDetransaksi',
					id: 'txtNamaPasienDetransaksi',
					anchor: '100%',
					listeners: {

					}
				}
			]
        }, 
		{
            columnWidth: .60,
            layout: 'form',
            border: false,
            labelWidth: 2,
            items: [

            ]
        }]
    };
    return items;
};

function refeshHasilRad(param) {
    dsTRHasilRadList.load
    (
        { 
            params:  
            {    
                Skip: 0, 
                Take: '10000',
                Sort: '',
                Sortdir: 'ASC', 
                target: 'ViewHasilRad',
                param : param
            }		
        }
    );
    return dsTRHasilRadList;
}

function Datasave_HasilRad(rowdata) {
    
    Ext.Ajax.request
	(
		{
		url: baseURL + "index.php/main/CreateDataObj",
		params: getParamDetailHasilRad(rowdata),
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfoHasilRad('Data berhasil di simpan','Simpan Data');
					RefreshDataHasilRadDetail(cst.KD_PASIEN,cst.TGL_MASUK,cst.URUT_MASUK);
					Ext.getCmp('textareapopuphasil').setValue('');
					Ext.getCmp('textareahasil').setValue('');
					Ext.getCmp('textareapopuphasil').setValue(cst.HASIL);
					Ext.getCmp('textareahasil').setValue(cst.HASIL);
				
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningHasilRad('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorHasilRad('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
			}
		}
	);
};

function ShowPesanWarningHasilRad(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.WARNING,
        width: 250
    });
};

function ShowPesanErrorHasilRad(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.ERROR,
        width: 250
    });
};

function ShowPesanInfoHasilRad(str, modul) {
    Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.INFO,
        width: 250
    });
};

function getItemPanelHasilRad() {
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width: 1100,
            height: 5,
            anchor: '100% 100%',
            items: []
        }, {

            layout: 'absolute',
            bodyStyle: 'padding: 10px 10px 10px 10px',
            border: true,
            width: 1190,
            height: 130,
            anchor: '100% 100%',
            items: [{
                x: 10,
                y: 10,
                xtype: 'label',
                text: 'No. Medrec '
            }, {
                x: 110,
                y: 10,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 10,
                xtype: 'textfield',
                name: 'TxtFilterGridDataView_MEDREC_viKasirRwj',
                id: 'TxtFilterGridDataView_MEDREC_viKasirRwj',
                width: 110,
                enableKeyEvents: true,
                listeners:
                {
                    'specialkey' : function()
                    {
                        var tmpNoMedrec = Ext.get('TxtFilterGridDataView_MEDREC_viKasirRwj').getValue();
                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                        {
                            if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
							{
								 var tmpgetNoMedrec = formatnomedrec(Ext.get('TxtFilterGridDataView_MEDREC_viKasirRwj').getValue())
								 Ext.getCmp('TxtFilterGridDataView_MEDREC_viKasirRwj').setValue(tmpgetNoMedrec);
								 getkriteriacari();
							}
							else
							{
								if (tmpNoMedrec.length === 10)
								{
									getkriteriacari();
								}
								else
								Ext.getCmp('TxtFilterGridDataView_MEDREC_viKasirRwj').setValue('');
							}
                        }
                    }

                }
            }, {
                x: 10,
                y: 40,
                xtype: 'label',
                text: 'Nama Pasien '
            }, {
                x: 110,
                y: 40,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 40,
                xtype: 'textfield',
                name: 'TxtFilterGridDataView_NAMA_viKasirRwj',
                id: 'TxtFilterGridDataView_NAMA_viKasirRwj',
                width: 240,
                enableKeyEvents: true,
                listeners: {
                    'specialkey': function() {
                        if (Ext.EventObject.getKey() === 13) {
                           getkriteriacari()
                        }
                    }
                }
            }, {
                x: 10,
                y: 70,
                xtype: 'label',
                text: 'Tgl Kunjung '
            }, {
                x: 110,
                y: 70,
                xtype: 'label',
                text: ' : '
            }, {
                x: 120,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAwalFilterHasilRad',
                format: 'd/M/Y',
                value: tigaharilalu
            }, {
                x: 230,
                y: 70,
                xtype: 'label',
                text: ' s/d '
            }, {
                x: 260,
                y: 70,
                xtype: 'datefield',
                id: 'dtpTglAkhirFilterHasilRad',
                format: 'd/M/Y',
                value: now,
                width: 100
            }, {
                x: 290,
                y: 100,
                xtype: 'button',
                text: 'Refresh',
                iconCls: 'refresh',
                width: 70,
                height: 20,
                hideLabel: false,
                handler: function(sm, row, rec) {
                    getkriteriacari()
                }
            }]
        }, {
            //                columnWidth:.99,
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width: 1100,
            height: 5,
            anchor: '100% 100%',
            items: []
        } ]
    };
    return items;
};

function setUsia(Tanggal)
{
    Ext.Ajax.request
	( {
		url: baseURL + "index.php/main/GetUmur",
		params: {
			TanggalLahir: ShowDateReal(Tanggal)
		},
		success: function(o)
		{
			var tmphasil = o.responseText;
			var tmp = tmphasil.split(' ');
			
			if (tmp.length === 6)
			{
				Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
				Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
				Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[4]);
			} else if(tmp.length === 4) {
				if(tmp[1]=== 'years') {
					if (tmp[3] === 'day') {
						Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
					}else {
						Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
						Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
					}
				}else{
					Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
					Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
				}
			} else if(tmp.length === 2 ) {
				if (tmp[1] === 'year' ) {
					Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
				} else if (tmp[1] === 'years' ) {
					Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
				} else if (tmp[1] === 'mon'  ) {
					Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
					Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
				} else if (tmp[1] === 'mons'  ) {
					Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
					Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
				} else {
					Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[0]);
				}
			}  else if(tmp.length === 1) {
				Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
				Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
				Ext.getCmp('TxtPopupHariLahirPasien').setValue('1');
			}else  {
				alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
			}                                              
		}
    });	
}

function getkriteriacari()
{
    var tmpkriteria = "unit.kd_unit = '5' and kunjungan.tgl_masuk >= '"+ Ext.get('dtpTglAwalFilterHasilRad').getValue() +"' and kunjungan.tgl_masuk <= '"+ Ext.get('dtpTglAkhirFilterHasilRad').getValue() +"'";
    
    if (Ext.get('TxtFilterGridDataView_MEDREC_viKasirRwj').getValue() !== "")
    {
        tmpkriteria += " AND Pasien.kd_pasien like '"+ Ext.get('TxtFilterGridDataView_MEDREC_viKasirRwj').getValue() +"%'";
    }else if(Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() !== "")
    {
        tmpkriteria += " AND pasien.Nama like '"+ Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() +"%'";
    }
    
    refeshHasilRad(tmpkriteria);
}