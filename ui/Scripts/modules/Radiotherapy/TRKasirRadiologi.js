var CurrentHistory =
        {
            data: Object,
            details: Array,
            row: 0
        };
var tampungmedrec;
var FormDepan2KasirRAD;
var CurrentPenataJasaKasirRAD =
        {
            data: Object,
            details: Array,
            row: 0
        };
var tmp_kodeunitkamar_RAD;
var tmp_kdspesial_RAD;
var tmp_nokamar_RAD;
var tampungshiftsekarang, form_msg_box_alasanhapus;
var tampungtypedata;
var tanggaltransaksitampung;
var kdpaytransfer = 'T1';
var mRecordKasirRAD = Ext.data.Record.create
        (
                [
                    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
                    {name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
                    {name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
                    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
                    {name: 'HARGA', mapping: 'HARGA'},
                    {name: 'QTY', mapping: 'QTY'},
                    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
                    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
                    {name: 'URUT', mapping: 'URUT'}
                ]
                );
var selectindukunitRADKasir = 'Radiologi';
var cellSelecteddeskripsi;
var cellSelected2deskripsi;
var Kdtransaksi;
var tapungkd_pay;
var dsTRDetailHistoryList;
var AddNewHistory = true;
var selectCountHistory = 50;
var now = new Date();
var rowSelectedHistory;
var FormLookUpsdetailTRHistory;
var now = new Date();
var cellSelectedtutup;
var vkd_unit;
var kdcustomeraa;
var nowTglTransaksi = new Date();
var FormLookUpsdetailTRTransfer_radiologi;
var labelisi;
var jenispay;
var variablehistori;
var selectCountStatusByr_viKasirRADKasir = 'Belum Lunas';
var tranfertujuan = 'Rawat Inap';
var dsTRKasirRADKasirList;
var dsTRDetailKasirRADKasirList;
var AddNewKasirRADKasir = true;
var selectCountKasirRADKasir = 50;
var now = new Date();
var rowSelectedKasirRADKasir;

var FormLookUpsdetailTRKasirRAD;
var valueStatusCMKasirRADView = 'All';
var nowTglTransaksi = new Date();
var dsComboBayar;
var vkode_customer;
var vflag;
var gridDTLTRKasirRAD;
var notransaksi;
var tgltrans;
var kodepasien;
var namapasien;
var kodeunit;
var namaunit;
var kodepay;
var kdkasir;
var uraianpay;
CurrentPage.page = getPanelKasirRAD(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var tmp_NoTransaksi = 0;
function getPanelKasirRAD(mod_id)
{

    var Field = ['KD_DOKTER', 'NO_TRANSAKSI', 'KD_UNIT', 'KD_PASIEN', 'NAMA', 'NAMA_UNIT', 'ALAMAT',
        'TANGGAL_TRANSAKSI', 'NAMA_DOKTER', 'KD_CUSTOMER', 'CUSTOMER', 'URUT_MASUK', 'LUNAS',
        'KET_PAYMENT', 'CARA_BAYAR', 'JENIS_PAY', 'KD_PAY', 'POSTING', 'TYPE_DATA', 'CO_STATUS', 'KD_KASIR',
        'NAMAUNITASAL', 'NO_TRANSAKSI_ASAL', 'KD_KASIR_ASAL', 'FLAG'];
    dsTRKasirRADKasirList = new WebApp.DataStore({fields: Field});
    refeshKasirRADKasir();
    var grListTRKasirRAD = new Ext.grid.EditorGridPanel
            (
                    {
                        stripeRows: true,
                        store: dsTRKasirRADKasirList,
                        columnLines: false,
                        autoScroll: true,
                        anchor: '100% 45%',
                        //height:325,
                        border: false,
                        sort: false,
                        sm: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        rowselect: function (sm, row, rec)
                                                        {
                                                            rowSelectedKasirRADKasir = dsTRKasirRADKasirList.getAt(row);
                                                        }
                                                    }
                                        }
                                ),
                        listeners:
                                {
                                    rowdblclick: function (sm, ridx, cidx)
                                    {
                                        rowSelectedKasirRADKasir = dsTRKasirRADKasirList.getAt(ridx);

                                        if (rowSelectedKasirRADKasir != undefined)
                                        {
                                            if (rowSelectedKasirRADKasir.data.LUNAS == 'f')
                                            {
                                                KasirLookUpRAD(rowSelectedKasirRADKasir.data);
                                            } else
                                            {
                                                ShowPesanWarningKasirRAD('Pembayaran telah balance', 'Pembayaran');
                                            }
                                        } else {
                                            ShowPesanWarningKasirRAD('Silahkan Pilih data   ', 'Pembayaran');
                                        }
                                    },
                                    rowclick: function (sm, ridx, cidx)
                                    {
                                        cellSelectedtutup = rowSelectedKasirRADKasir.data.NO_TRANSAKSI;

                                        if (rowSelectedKasirRADKasir.data.LUNAS == 't' && rowSelectedKasirRADKasir.data.CO_STATUS == 't')
                                        {
                                            Ext.getCmp('btnEditKasirRAD').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirRAD').disable();
                                            Ext.getCmp('btnHpsBrsKasirRAD').disable();
                                            Ext.getCmp('btnLookupKasirRAD').disable();
                                            Ext.getCmp('btnSimpanRAD').disable();
                                            Ext.getCmp('btnHpsBrsRAD').disable();
                                            //Ext.getCmp('btnTransferKasirRAD').disable();
                                        } else if (rowSelectedKasirRADKasir.data.LUNAS == 'f' && rowSelectedKasirRADKasir.data.CO_STATUS == 'f')
                                        {
                                            Ext.getCmp('btnTutupTransaksiKasirRAD').disable();
                                            Ext.getCmp('btnHpsBrsKasirRAD').enable();
                                            Ext.getCmp('btnEditKasirRAD').enable();
                                            Ext.getCmp('btnLookupKasirRAD').enable();
                                            Ext.getCmp('btnSimpanRAD').enable();
                                            Ext.getCmp('btnHpsBrsRAD').enable();
                                           // Ext.getCmp('btnTransferKasirRAD').enable();

                                        } else if (rowSelectedKasirRADKasir.data.LUNAS == 't' && rowSelectedKasirRADKasir.data.CO_STATUS == 'f')
                                        {
                                            Ext.getCmp('btnEditKasirRAD').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirRAD').enable();
                                            Ext.getCmp('btnHpsBrsKasirRAD').enable();
                                            Ext.getCmp('btnLookupKasirRAD').disable();
                                            Ext.getCmp('btnSimpanRAD').disable();
                                            Ext.getCmp('btnHpsBrsRAD').disable();
                                           // Ext.getCmp('btnTransferKasirRAD').disable();

                                        }

                                        kdkasirasal = rowSelectedKasirRADKasir.data.KD_KASIR_ASAL;
                                        notransaksiasal = rowSelectedKasirRADKasir.data.NO_TRANSAKSI_ASAL;
                                        notransaksi = rowSelectedKasirRADKasir.data.NO_TRANSAKSI;
                                        tgltrans = rowSelectedKasirRADKasir.data.TANGGAL_TRANSAKSI;
                                        kodepasien = rowSelectedKasirRADKasir.data.KD_PASIEN;
                                        namapasien = rowSelectedKasirRADKasir.data.NAMA;
                                        kodeunit = rowSelectedKasirRADKasir.data.KD_UNIT;
                                        namaunit = rowSelectedKasirRADKasir.data.NAMA_UNIT;
                                        kodepay = rowSelectedKasirRADKasir.data.KD_PAY;
                                        kdkasir = rowSelectedKasirRADKasir.data.KD_KASIR;
                                        uraianpay = rowSelectedKasirRADKasir.data.CARA_BAYAR;
                                        kdcustomeraa = rowSelectedKasirRADKasir.data.KD_CUSTOMER;
                                        Ext.Ajax.request(
                                                {
                                                    //url: "./home.mvc/getModule",
                                                    //url: baseURL + "index.php/main/getTrustee",
                                                    url: baseURL + "index.php/main/getcurrentshift",
                                                    params: {
                                                        //UserID: 'Admin',
                                                        command: '0',
                                                        // parameter untuk url yang dituju (fungsi didalam controller)
                                                    },
                                                    failure: function (o)
                                                    {
                                                        var cst = Ext.decode(o.responseText);
                                                    },
                                                    success: function (o) {
                                                        tampungshiftsekarang = o.responseText
                                                    }

                                                });
                                        RefreshDatahistoribayar(rowSelectedKasirRADKasir.data.NO_TRANSAKSI);
                                        RefreshDataKasirRADDetail(rowSelectedKasirRADKasir.data.NO_TRANSAKSI);
                                    }
                                },
                        cm: new Ext.grid.ColumnModel
                                (
                                        [
                                            //CUSTOMER
                                            {
                                                id: 'colLUNAScoba',
                                                header: 'Status Lunas',
                                                dataIndex: 'LUNAS',
                                                sortable: true,
                                                width: 90,
                                                align: 'center',
                                                renderer: function (value, metaData, record, rowIndex, colIndex, store)
                                                {
                                                    switch (value)
                                                    {
                                                        case 't':
                                                            metaData.css = 'StatusHijau'; // 
                                                            break;
                                                        case 'f':
                                                            metaData.css = 'StatusMerah'; // rejected
                                                            break;
                                                    }
                                                    return '';
                                                }
                                            },
                                            {
                                                id: 'coltutuptr',
                                                header: 'Tutup transaksi',
                                                dataIndex: 'CO_STATUS',
                                                sortable: true,
                                                width: 90,
                                                align: 'center',
                                                renderer: function (value, metaData, record, rowIndex, colIndex, store)
                                                {
                                                    switch (value)
                                                    {
                                                        case 't':
                                                            metaData.css = 'StatusHijau'; // 
                                                            break;
                                                        case 'f':
                                                            metaData.css = 'StatusMerah'; // rejected
                                                            break;
                                                    }
                                                    return '';
                                                }
                                            },
                                            {
                                                id: 'colReqIdViewKasirRAD',
                                                header: 'No. Transaksi',
                                                dataIndex: 'NO_TRANSAKSI',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 80
                                            },
                                            {
                                                id: 'colTglKasirRADViewKasirRAD',
                                                header: 'Tgl Transaksi',
                                                dataIndex: 'TANGGAL_TRANSAKSI',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 75,
                                                renderer: function (v, params, record)
                                                {
                                                    return ShowDate(record.data.TANGGAL_TRANSAKSI);
                                                }
                                            },
                                            {
                                                header: 'No. Medrec',
                                                width: 65,
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                dataIndex: 'KD_PASIEN',
                                                id: 'colKasirRADerViewKasirRAD'
                                            },
                                            {
                                                header: 'Pasien',
                                                width: 190,
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                dataIndex: 'NAMA',
                                                id: 'colKasirRADerViewKasirRAD'
                                            },
                                            {
                                                id: 'colLocationViewKasirRAD',
                                                header: 'Alamat',
                                                dataIndex: 'ALAMAT',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 170
                                            },
                                            {
                                                id: 'colDeptViewKasirRAD',
                                                header: 'Dokter',
                                                dataIndex: 'NAMA_DOKTER',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 150
                                            },
                                            {
                                                id: 'colunitViewKasirRAD',
                                                header: 'Unit',
                                                dataIndex: 'NAMA_UNIT',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                hidden: true,
                                                width: 90
                                            },
                                            {
                                                id: 'colasaluViewKasirRAD',
                                                header: 'Unit asal',
                                                dataIndex: 'NAMAUNITASAL',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 90
                                            },
                                            {
                                                id: 'colcustomerViewKasirRAD',
                                                header: 'Kel. Pasien',
                                                dataIndex: 'CUSTOMER',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 90
                                            }
                                        ]
                                        ),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnEditKasirRAD',
                                        text: 'Pembayaran',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                        handler: function (sm, row, rec)
                                        {
                                            if (rowSelectedKasirRADKasir != undefined)
                                            {
                                                KasirLookUpRAD(rowSelectedKasirRADKasir.data);
                                            } else
                                            {
                                                ShowPesanWarningKasirRAD('Pilih data tabel  ', 'Pembayaran');
                                                //alert('');
                                            }
                                        }, disabled: true
                                    }, ' ', ' ', '' + ' ', '' + ' ', '' + ' ', '' + ' ', ' ', '' + '-',
									{
                                        text: 'Tutup Transaksi',
                                        id: 'btnTutupTransaksiKasirRAD',
                                        tooltip: nmHapus,
                                        iconCls: 'remove',
                                        handler: function ()
                                        {
                                            if (cellSelectedtutup == '' || cellSelectedtutup == 'undefined')
                                            {
                                                ShowPesanWarningKasirRAD('Pilih data tabel  ', 'Pembayaran');
                                            } else
                                            {
                                                UpdateTutuptransaksi(false);
                                                Ext.getCmp('btnEditKasirRAD').disable();
                                                Ext.getCmp('btnTutupTransaksiKasirRAD').disable();
                                                Ext.getCmp('btnHpsBrsKasirRAD').disable();
                                            }
                                        },
                                        disabled: true
                                    }
                                ]
                    }
            );

    var LegendViewCMRequest = new Ext.Panel
            (
                    {
                        id: 'LegendViewCMRequest',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        layout: 'column',
                        frame: true,
                        //height:32,
                        anchor: '100% 8.0001%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        columnWidth: .033,
                                        layout: 'form',
                                        style: {'margin-top': '-1px'},
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        border: false,
                                        html: '<img src="' + baseURL + 'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                                    },
                                    {
                                        columnWidth: .08,
                                        layout: 'form',
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        style: {'margin-top': '1px'},
                                        border: false,
                                        html: " Lunas"
                                    },
                                    {
                                        columnWidth: .033,
                                        layout: 'form',
                                        style: {'margin-top': '-1px'},
                                        border: false,
                                        //height:35,
                                        anchor: '100% 8.0001%',
                                        //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                                        html: '<img src="' + baseURL + 'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                                    },
                                    {
                                        columnWidth: .1,
                                        layout: 'form',
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        style: {'margin-top': '1px'},
                                        border: false,
                                        html: " Belum Lunas"
                                    }
                                ]

                    }
            )
    var GDtabDetailRAD = new Ext.TabPanel
            (
                    {
                        id: 'GDtabDetailRAD',
                        region: 'center',
                        activeTab: 0,
                        anchor: '100% 40%',
                        border: false,
                        plain: true,
                        defaults:
                                {
                                    autoScroll: true
                                },
                        items: [
                            GetDTLTRHistoryGrid(), GetDTLTRRADGrid()
                                    //-------------- ## --------------
                        ],
                        listeners:
                                {
                                }
                    }

            );

    var FormDepanKasirRAD = new Ext.Panel
            (
                    {
                        id: mod_id,
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        title: 'Radiologi',
                        border: false,
                        //shadhow: true,
                        iconCls: 'Request',
                        margins: '0 5 5 0',
                        items: [
                            {
                                xtype: 'panel',
                                layout: 'column',
                                plain: true,
                                activeTab: 0,
                                height: 160,
                                defaults:
                                        {
                                            bodyStyle: 'padding:10px',
                                            autoScroll: true
                                        },
                                items:
                                        [
                                            {
                                                columnWidth: .99,
                                                height: 150,
                                                layout: 'form',
                                                bodyStyle: 'padding:6px 6px 3px 3px',
                                                border: false,
                                                anchor: '100% 100%',
                                                items:
                                                        [
                                                            mComboUnitInduk(),
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: ' No. Medrec' + ' ',
                                                                id: 'txtFilterKasirNomedrecRad',
                                                                anchor: '35%',
                                                                onInit: function () { },
                                                                listeners:
                                                                        {
                                                                            'specialkey': function ()
                                                                            {
                                                                                var tmpNoMedrec = Ext.get('txtFilterKasirNomedrecRad').getValue()
                                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                                {
                                                                                    if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                                                                    {
                                                                                        var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterKasirNomedrecRad').getValue())
                                                                                        Ext.getCmp('txtFilterKasirNomedrecRad').setValue(tmpgetNoMedrec);
                                                                                        var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                        RefreshDataFilterKasirRADKasir();
                                                                                    } else
                                                                                    {
                                                                                        if (tmpNoMedrec.length === 10)
                                                                                        {
                                                                                            RefreshDataFilterKasirRADKasir();
                                                                                        } else
                                                                                            Ext.getCmp('txtFilterKasirNomedrecRad').setValue('')
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                xtype: 'tbspacer',
                                                                height: 3
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: ' Pasien' + ' ',
                                                                id: 'TxtFilterGridDataView_NAMA_viKasirRADKasir',
                                                                anchor: '50%',
                                                                enableKeyEvents: true,
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {

                                                                                RefreshDataFilterKasirRADKasir();

                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                xtype: 'tbspacer',
                                                                height: 3
                                                            },
                                                            getItemPanelcombofilter(),
                                                            getItemPaneltgl_filter()
                                                        ]
                                            },
                                        ]}
                            ,
                            grListTRKasirRAD, GDtabDetailRAD, LegendViewCMRequest],
                        //GetDTLTRHistoryGrid()
                        tbar:
                                [
                                ],
                        listeners:
                                {
                                    'afterrender': function ()
                                    {
                                        loaddatastoreunitRadRAD();
                                    }
                                }
                    }
            );

    return FormDepanKasirRAD;

}
;

function showhide_unit(kode)
{
	if(kode==='05')
	{
	Ext.getCmp('txtTranferunitRAD').hide();
	Ext.getCmp('txtTranferkelaskamar_RAD').show();

	}else{
	Ext.getCmp('txtTranferunitRAD').show();
	Ext.getCmp('txtTranferkelaskamar_RAD').hide();
	}
}
function dataparam_viradRAD()
{
    var paramsload_viradRAD =
            {
                Table: 'ViewComboUnitRadLab'
            }
    return paramsload_viradRAD;
}

var dsunitinduk_viKasirRADKasir;

function loaddatastoreunitRadRAD()
{
    dsunitinduk_viKasirRADKasir.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_unit',
                                    Sortdir: 'ASC',
                                    target: 'ComboUnitRadLab',
                                    param: ""
                                }
                    }
            );
}

function TransferLookUp_Radiogi(rowdata)
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_radiologi = new Ext.Window
            (
                    {
                        id: 'gridTransfer_rad',
                        title: 'Transfer',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 410,
                        border: false,
                        resizable: false,
                        plain: false,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryTRTransfer_radiologi(lebar),
                        listeners:
                                {
                                }
                    }
            );

    FormLookUpsdetailTRTransfer_radiologi.show();
    //  Transferbaru();

}
;

function getFormEntryTRTransfer_radiologi(lebar)
{
    var pnlTRTransfer = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRTransfer',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 425,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputTransfer_radiologi(lebar), getItemPanelButtonTransfer_radiologi(lebar)],
                        tbar:
                                [
                                ]
                    }
            );

    var FormDepanTransfer = new Ext.Panel
            (
                    {
                        id: 'FormDepanTransfer',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        items:
                                [
                                    pnlTRTransfer

                                ]

                    }
            );

    return FormDepanTransfer
}
;

function getItemPanelInputTransfer_radiologi(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: false,
                height: 330,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                height: 330,
                                border: false,
                                items:
                                        [
                                            getTransfertujuan_rad(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 5
                                            },
                                            getItemPanelNoTransksiTransfer(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            mComboalasan_transfer()
                                        ]
                            },
                        ]
            };
    return items;
}
;

function mComboalasan_transfer()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_transfer = new WebApp.DataStore({fields: Field});
    dsalasan_transfer.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_alasan',
                                    Sortdir: 'ASC',
                                    target: 'ComboAlasanTransfer',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboalasan_transfer = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_transfer',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Alasan Transfer ',
                        align: 'Right',
                        width: 100,
                        anchor: '100%',
                        store: dsalasan_transfer,
                        valueField: 'ALASAN',
                        displayField: 'ALASAN',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                    }

                                }
                    }
            );

    return cboalasan_transfer;
}
;

function getItemPanelNoTransksiTransfer(lebar)
{
    var items =
            {
                Width: lebar,
                height: 110,
                layout: 'column',
                border: true,
                items:
                        [
                            {
                                columnWidth: .990,
                                layout: 'form',
                                Width: lebar - 10,
                                labelWidth: 130,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah Biaya',
                                                maxLength: 200,
                                                name: 'txtjumlahbiayasal',
                                                id: 'txtjumlahbiayasal',
                                                width: 100,
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Paid',
                                                maxLength: 200,
                                                name: 'txtpaid',
                                                id: 'txtpaid',
                                                readOnly: true,
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                value: 0,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah dipindahkan',
                                                maxLength: 200,
                                                name: 'txtjumlahtranfer',
                                                id: 'txtjumlahtranfer',
                                                align: 'right',
                                                readOnly: false,
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Saldo tagihan',
                                                maxLength: 200,
                                                name: 'txtsaldotagihan',
                                                id: 'txtsaldotagihan',
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                value: 0,
                                                width: 100,
                                                anchor: '95%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getTransfertujuan_rad(lebar)
{
    var items =
            {
                Width: lebar - 2,
                height: 170,
                layout: 'form',
                border: true,
                bodyStyle: 'padding:20px 10px 10px 10px',
                labelWidth: 130,
                items:
                        [
                            {
                                xtype: 'tbspacer',
                                height: 2
                            },
                            //mComboTransferTujuan(),

                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Transaksi',
                                //maxLength: 200,
                                name: 'txtTranfernoTransaksiRAD',
                                id: 'txtTranfernoTransaksiRAD',
                                labelWidth: 130,
                                readonly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                id: 'dtpTanggaltransaksiRujuanRAD',
                                name: 'dtpTanggaltransaksiRujuanRAD',
                                format: 'd/M/Y',
                                readOnly: true,
                                //  value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Medrec',
                                //maxLength: 200,
                                name: 'txtTranfernomedrecRAD',
                                id: 'txtTranfernomedrecRAD',
                                labelWidth: 130,
                                readOnly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama Pasien',
                                //maxLength: 200,
                                name: 'txtTranfernamapasienRAD',
                                id: 'txtTranfernamapasienRAD',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'Unit Perawatan',
                                //maxLength: 200,
                                name: 'txtTranferunitRAD',
                                id: 'txtTranferunitRAD',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },{
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        name: 'txtTranferkelaskamar_RAD',
                                        id: 'txtTranferkelaskamar_RAD',
										readOnly : true,
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                            }

                        ]
            }
    return items;
}
;
function getItemPanelButtonTransfer_radiologi(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                height: 30,
                anchor: '100%',
                style: {'margin-top': '-1px'},
                items:
                        [
                            {
                                layout: 'hBox',
                                width: 400,
                                border: false,
                                bodyStyle: 'padding:5px 0px 5px 5px',
                                defaults: {margins: '3 3 3 3'},
                                anchor: '90%',
                                layoutConfig:
                                        {
                                            align: 'middle',
                                            pack: 'end'
                                        },
                                items:
                                        [
                                            {
                                                xtype: 'button',
                                                text: 'Simpan',
                                                width: 70,
                                                style: {'margin-left': '0px', 'margin-top': '0px'},
                                                hideLabel: true,
                                                id: 'btnOkTransfer_radio',
                                                handler: function ()
                                                {
                                                    TransferData_radiologi(false);
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                text: 'Tutup',
                                                width: 70,
                                                hideLabel: true,
                                                id: 'btnCancelTransfer_radio',
                                                handler: function ()
                                                {
                                                    FormLookUpsdetailTRTransfer_radiologi.close();
                                                }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getParamTransferRwi_dari_rad()
{
    var params =
            {
                KDkasirIGD: kdkasir,
                Kdcustomer: kdcustomeraa,
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                Kdpay: kdpaytransfer,
                Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
                Tglasal: ShowDate(tgltrans),
                Shift: tampungshiftsekarang,
                TRKdTransTujuan: Ext.get(txtTranfernoTransaksiRAD).dom.value,
                KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRAD).dom.value,
                TglTranasksitujuan: Ext.get(dtpTanggaltransaksiRujuanRAD).dom.value,
                KDunittujuan: Trkdunit2,
                KDalasan: Ext.get(cboalasan_transfer).dom.value,
                KasirRWI: kdkasirasal,
				kodeunitkamar:tmp_kodeunitkamar_RAD,
				kdspesial:tmp_kdspesial_RAD,
				nokamar:tmp_nokamar_RAD,


            };
    return params
}
;
function TransferData_radiologi(mBol)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/saveTransfer",
                        params: getParamTransferRwi_dari_rad(),
                        failure: function (o)
                        {
                            ShowPesanWarningKasirRAD('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            RefreshDataFilterKasirRADKasir();
                        },
                        success: function (o)
                        {
                            RefreshDataFilterKasirRADKasir();
                            FormLookUpsdetailTRTransfer_radiologi.close();
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {	RefreshDataKasirRADDetail(notransaksi);
								Ext.getCmp('btnTransferKasirRAD').disable();
								Ext.getCmp('btnSimpanRAD').disable();
								ShowPesanInfoKasirRAD('Transfer Berhasil', 'transfer ');
                                FormLookUpsdetailTRTransfer_radiologi.close();


                            } else
                            {
                                ShowPesanWarningKasirRAD('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            }
                            ;
                        }
                    }
            )
}
;


function RefreshDataKasirRADDetail(no_transaksi)
{
	tmp_NoTransaksi = no_transaksi;
    var strKriteriaRAD = '';
    strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~";

    dsTRDetailKasirRADList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailRWJGridBawah',
                                    param: strKriteriaRAD
                                }
                    }
            );
    return dsTRDetailKasirRADList;
}
;


function GetDTLTRRADGrid()
{
    var fldDetail = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI'];

    dsTRDetailKasirRADList = new WebApp.DataStore({fields: fldDetail})
    RefreshDataKasirRADDetail();
    var gridDTLTRRAD = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Item Pemeriksaan',
                        stripeRows: true,
                        store: dsTRDetailKasirRADList,
                        border: true,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
                                                            cellSelected2deskripsi = dsTRDetailKasirRADList.getAt(row);
                                                            CurrentPenataJasaKasirRAD.row = row;
                                                            CurrentPenataJasaKasirRAD.data = cellSelected2deskripsi;
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRRawatJalanColumModel(),
                        tbar:
                                [
                                    {
                                        text: 'Tambah Produk',
                                        id: 'btnLookupKasirRAD',
                                        tooltip: nmLookup,
                                        iconCls: 'find',
                                        disabled: true,
                                        hidden: true,
                                        handler: function ()
                                        {
                                            var p = RecordBaruRAD();
                                            var str = '';
                                            str = kodeunit;
                                            FormLookupKasirRAD(str, dsTRDetailKasirRADList, p, true, '', true);
                                        }
                                    },
                                    {
                                        text: 'Simpan',
                                        id: 'btnSimpanRAD',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        disabled: true,
                                        hidden: true,
                                        handler: function ()
                                        {
                                            Datasave_KasirRAD(false);
                                            Ext.getCmp('btnLookupKasirRAD').disable();
                                            Ext.getCmp('btnSimpanRAD').disable();
											Ext.getCmp('btnTransferKasirRAD').disable();
									        Ext.getCmp('btnHpsBrsRAD').disable();

                                        }
                                    },
                                    {
                                        id: 'btnHpsBrsRAD',
                                        text: 'Hapus Baris',
                                        tooltip: 'Hapus Baris',
                                        disabled: true,
                                        iconCls: 'RemoveRow',
                                        hidden: true,
                                        handler: function ()
                                        {
                                            if (dsTRDetailKasirRADList.getCount() > 0)
                                            {
                                                if (cellSelected2deskripsi != undefined)
                                                {
                                                    if (CurrentPenataJasaKasirRAD != undefined)
                                                    {
                                                        HapusBarisRAD();
                                                        Ext.getCmp('btnLookupKasirRAD').disable();
                                                        Ext.getCmp('btnSimpanRAD').disable();
                                                        Ext.getCmp('btnHpsBrsRAD').disable();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningRAD('Pilih record ', 'Hapus data');
                                                }
                                            }
                                        }
                                    }
                                ],
                        viewConfig: {forceFit: true}
                    }
            );

    return gridDTLTRRAD;
}
;

function HapusBarisRAD()
{
    if (cellSelected2deskripsi != undefined)
    {
        if (cellSelected2deskripsi.data.DESKRIPSI2 != '' && cellSelected2deskripsi.data.KD_PRODUK != '')
        {
            var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function (btn, combo) {
                if (btn == 'ok')
                {
                    variablehistori = combo;
                    DataDeleteKasirRADDetail();
                    dsTRDetailKasirRADList.removeAt(CurrentPenataJasaKasirRAD.row);
                }
            });
        } else
        {
            dsTRDetailKasirRADList.removeAt(CurrentPenataJasaKasirRAD.row);
        }
        ;
    }
}
;

function DataDeleteKasirRADDetail()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: getParamDataDeleteKasirRADDetail(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirRAD(nmPesanHapusSukses, nmHeaderHapusData);
                                dsTRDetailKasirRADList.removeAt(CurrentPenataJasaKasirRAD.row);
                                cellSelecteddeskripsi = undefined;
                                RefreshDataKasirRADDetail(notransaksi);
                                AddNewKasirRAD = false;
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningKasirRAD(nmPesanHapusGagal, nmHeaderHapusData);
                            } else
                            {
                                ShowPesanWarningKasirRAD(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirRADDetail()
{

    var params =
            {
                Table: 'ViewTrKasirRwj',
                TrKodeTranskasi: CurrentPenataJasaKasirRAD.data.data.NO_TRANSAKSI,
                TrTglTransaksi: CurrentPenataJasaKasirRAD.data.data.TGL_TRANSAKSI,
                TrKdPasien: CurrentPenataJasaKasirRAD.data.data.KD_PASIEN,
                TrKdNamaPasien: namapasien,
                TrKdUnit: kodeunit,
                TrNamaUnit: namaunit,
                Uraian: CurrentPenataJasaKasirRAD.data.data.DESKRIPSI2,
                AlasanHapus: variablehistori,
                TrHarga: CurrentPenataJasaKasirRAD.data.data.HARGA,
                TrKdProduk: CurrentPenataJasaKasirRAD.data.data.KD_PRODUK,
                RowReq: CurrentPenataJasaKasirRAD.data.data.URUT,
                Hapus: 2
            };

    return params
}
;


function Datasave_KasirRAD(mBol)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/savedetailpenyakit",
                        params: getParamDetailTransaksiRAD(),
                        failure: function (o)
                        {
                            ShowPesanWarningRAD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                            RefreshDataKasirRADDetail(notransaksi);
                        },
                        success: function (o)
                        {
                            RefreshDataKasirRADDetail(notransaksi);
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirRAD(nmPesanSimpanSukses, nmHeaderSimpanData);
                                RefreshDataFilterKasirRAD();
                                if (mBol === false)
                                {
                                    RefreshDataKasirRADDetail(notransaksi);
                                }
                                ;
                            } else
                            {
                                ShowPesanWarningKasirRAD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDetailTransaksiRAD()
{
    var params =
            {
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                Tgl: tgltrans,
                Shift: tampungshiftsekarang,
                List: getArrDetailTrRAD(),
                JmlField: mRecordRAD.prototype.fields.length - 4,
                JmlList: GetListCountPenataDetailTransaksi(),
                Hapus: 1,
                Ubah: 0
            };
    return params
}
;

function getArrDetailTrRAD()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirRADList.getCount(); i++)
    {
        if (dsTRDetailKasirRADList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirRADList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirRADList.data.items[i].data.URUT
            y += z + dsTRDetailKasirRADList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirRADList.data.items[i].data.QTY
            y += z + ShowDate(dsTRDetailKasirRADList.data.items[i].data.TGL_BERLAKU)
            y += z + dsTRDetailKasirRADList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirRADList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirRADList.data.items[i].data.URUT


            if (i === (dsTRDetailKasirRADList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }
    return x;
}
;


function RecordBaruRAD()
{
    var p = new mRecordRAD
            (
                    {
                        'DESKRIPSI2': '',
                        'KD_PRODUK': '',
                        'DESKRIPSI': '',
                        'KD_TARIF': '',
                        'HARGA': '',
                        'QTY': '',
                        'TGL_TRANSAKSI': tanggaltransaksitampung,
                        'DESC_REQ': '',
                        'KD_TARIF':'',
                                'URUT': ''
                    }
            );
    return p;
}
;
var mRecordRAD = Ext.data.Record.create
        (
                [
                    {name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
                    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
                    {name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
                    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
                    {name: 'HARGA', mapping: 'HARGA'},
                    {name: 'QTY', mapping: 'QTY'},
                    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
                    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
                    // {name: 'KD_TARIF', mapping:'KD_TARIF'},
                    {name: 'URUT', mapping: 'URUT'}
                ]
                );


function TRRawatJalanColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiRAD',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            // width:.30,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiRAD',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colHARGARAD',
                            header: 'Harga',
                            align: 'right',
                            hidden: true,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            //  width:.10,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colProblemRAD',
                            header: 'Qty',
                            width: '100%',
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolProblemRAD',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 100,
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {

                                                                Dataupdate_KasirRAD(false);
                                                                //RefreshDataFilterKasirRAD();
                                                                //RefreshDataFilterKasirRAD();

                                                            }
                                                        }
                                            }
                                    ),
                        },
                        {
                            id: 'colImpactRAD',
                            header: 'CR',
                            // width:80,
                            dataIndex: 'IMPACT',
                            hidden: true,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolImpactRAD',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30
                                            }
                                    )

                        }

                    ]
                    )
}
;


function getItemPaneltgl_filter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTglAwalFilterKasirRAD',
                                                name: 'dtpTglAwalFilterKasirRAD',
                                                format: 'd/M/Y',
                                                //readOnly : true,
                                                value: now,
                                                anchor: '96.7%',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                var tmpNoMedrec = Ext.get('txtFilterKasirNomedrecRad').getValue()
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterKasirRADKasir();
                                                                }
                                                            }
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 's/d ',
                                                id: 'dtpTglAkhirFilterKasirRAD',
                                                name: 'dtpTglAkhirFilterKasirRAD',
                                                format: 'd/M/Y',
                                                //readOnly : true,
                                                value: now,
                                                anchor: '100%',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                var tmpNoMedrec = Ext.get('txtFilterKasirNomedrecRad').getValue()
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterKasirRADKasir();
                                                                }
                                                            }
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getItemPanelcombofilter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboStatusBayar_viKasirRADKasir()
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                        [
                                            mComboUnit_viKasirRADKasir()
                                        ]
                            }
                        ]
            }
    return items;
}
;


function mComboStatusBayar_viKasirRADKasir()
{
    var cboStatus_viKasirRADKasir = new Ext.form.ComboBox
            (
                    {
                        id: 'cboStatus_viKasirRADKasir',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        fieldLabel: 'Status Lunas',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua'], [2, 'Lunas'], [3, 'Belum Lunas']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectCountStatusByr_viKasirRADKasir,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectCountStatusByr_viKasirRADKasir = b.data.displayText;
                                        RefreshDataFilterKasirRADKasir();
                                    }
                                }
                    }
            );
    return cboStatus_viKasirRADKasir;
}
;

function mComboTransferTujuan()
{
    var cboTransferTujuan = new Ext.form.ComboBox
            (
                    {
                        id: 'cboTransferTujuan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        fieldLabel: 'Transfer',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Gawat Darurat'], [2, 'Rawat Inap'], [3, 'Rawat Jalan']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: tranfertujuan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tranfertujuan = b.data.displayText;
                                        RefreshDataFilterKasirRADKasir();
                                    }
                                }
                    }
            );
    return cboTransferTujuan;
}
;

function mComboUnitInduk()
{
    var Field = ['kd_bagian', 'nama_unit'];

    dsunitinduk_viKasirRADKasir = new WebApp.DataStore({fields: Field});

    var cboUnitInduk = new Ext.form.ComboBox
            (
                    {
                        id: 'cboUnitInduk',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '22,7%',
                        emptyText: '',
                        fieldLabel: 'Pilih Poli',
                        store: dsunitinduk_viKasirRADKasir,
                        valueField: 'kd_bagian',
                        displayField: 'nama_unit',
                        hidden: true,
                        value: selectindukunitRADKasir,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectindukunitRADKasir = b.data.displayText;
                                        RefreshDataFilterKasirRADKasir();
                                    }
                                }
                    }
            );
    return cboUnitInduk;
}
;

function mComboUnit_viKasirRADKasir()
{
    var Field = ['KD_UNIT', 'NAMA_UNIT'];

    dsunit_viKasirRADKasir = new WebApp.DataStore({fields: Field});
    dsunit_viKasirRADKasir.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_unit',
                                    Sortdir: 'ASC',
                                    target: 'ComboUnit',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboUNIT_viKasirRADKasir = new Ext.form.ComboBox
            (
                    {
                        id: 'cboUNIT_viKasirRADKasir',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Poli ',
                        align: 'Right',
                        //width: 100,
                        anchor: '100%',
                        store: dsunit_viKasirRADKasir,
                        valueField: 'KD_UNIT',
                        displayField: 'NAMA_UNIT',
                        value: 'All',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        RefreshDataFilterKasirRADKasir();
                                    }
                                }
                    }
            );

    return cboUNIT_viKasirRADKasir;
}
;



function KasirLookUpRAD(rowdata)
{
    var lebar = 580;
    FormLookUpsdetailTRKasirRAD = new Ext.Window
            (
                    {
                        id: 'gridKasirRAD',
                        title: 'Kasir Radiologi',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 500,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryKasirRAD(lebar),
                        listeners:
                                {
                                }
                    }
            );

    FormLookUpsdetailTRKasirRAD.show();
    if (rowdata == undefined)
    {
        KasirRADAddNew();
    } else
    {
        TRKasirRADInit(rowdata)
    }

}
;



function getFormEntryKasirRAD(lebar)
{
    var pnlTRKasirRAD = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirRAD',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 130,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputKasir(lebar)],
                        tbar:
                                [
                                ]
                    }
            );
    var x;
    var paneltotal = new Ext.Panel
            (
                    {
                        id: 'paneltotal',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        height: 67,
                        anchor: '100% 8.0000%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        html: " Total :"
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirRAD',
                                                        name: 'txtJumlah2EditData_viKasirRAD',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        hidden: true,
                                                        html: " Bayar :"
                                                    },
                                                    {
                                                        xtype: 'numberfield',
                                                        hidden: true,
                                                        id: 'txtJumlahEditData_viKasirRAD',
                                                        name: 'txtJumlahEditData_viKasirRAD',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        //readOnly: true,
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                ]

                    }
            )
    var GDtabDetailKasirRAD = new Ext.Panel
            (
                    {
                        id: 'GDtabDetailKasirRAD',
                        region: 'center',
                        activeTab: 0,
                        height: 350,
                        anchor: '100% 100%',
                        border: false,
                        plain: true,
                        defaults: {autoScroll: true},
                        items: [GetDTLTRKasirRADGrid(), paneltotal],
                        tbar:
                                [
                                    {
                                        text: 'Bayar',
                                        id: 'btnSimpanKasirRAD',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        handler: function ()
                                        {
                                            Datasave_KasirRADKasir(false);
                                            //FormDepan2KasirRAD.close()
                                        }
                                    },
                                    {
                                        id: 'btnTransferKasirRAD',
                                        text: 'Transfer',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                       // disabled: true,
                                        handler: function (sm, row, rec)
                                        {

                                            Ext.Ajax.request
                                                    ({
                                                        url: baseURL + "index.php/main/GetPasienTranferLab_rad",
                                                        params: {
                                                            notr: notransaksiasal,
															notransaksi: tmp_NoTransaksi,
                                                            kdkasir: kdkasirasal
                                                        },
                                                        success: function (o)
                                                        {
                                                            TransferLookUp_Radiogi();
                                                            if (o.responseText == "") {
                                                                ShowPesanWarningKasirRAD('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                                FormLookUpsdetailTRTransfer_radiologi.close();
                                                            } else {
                                                                var tmphasil = o.responseText;
                                                                var tmp = tmphasil.split("<>");

                                                                if (tmp[5] == undefined && tmp[3] == undefined) {
                                                                    ShowPesanWarningKasirRAD('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                                    FormLookUpsdetailTRTransfer_radiologi.close();
                                                                } else {
																	showhide_unit(kdkasirasal);
                                                                    Ext.get(txtTranfernoTransaksiRAD).dom.value = tmp[3];
                                                                    Ext.get(dtpTanggaltransaksiRujuanRAD).dom.value = ShowDate(tmp[4]);
                                                                    Ext.get(txtTranfernomedrecRAD).dom.value = tmp[5];
                                                                    tampungmedrec = tmp[5];
                                                                    Ext.get(txtTranfernamapasienRAD).dom.value = tmp[2];
                                                                    Ext.get(txtTranferunitRAD).dom.value = tmp[1];
                                                                    Trkdunit2 = tmp[6];
                                                                    var kasir = tmp[7];
																	Ext.get(txtTranferkelaskamar_RAD).dom.value=tmp[9];
																	tmp_kodeunitkamar_RAD=tmp[8];
																	tmp_kdspesial_RAD=tmp[10];
																	Ext.get(txtjumlahbiayasal).dom.value=tmp[12];
																	console.log("test");
																	console.log(tmp);
																	tmp_nokamar_RAD=tmp[11];
                                                                    Ext.Ajax.request
                                                                            ({
                                                                                url: baseURL + "index.php/main/GettotalTranfer",
                                                                                params: {
                                                                                    notr: notransaksi,
																					kd_kasir:kdkasir
                                                                                },
                                                                                success: function (o)
                                                                                {
                                                                                    Ext.get(txtjumlahbiayasal).dom.value = formatCurrency(o.responseText);
                                                                                    Ext.get(txtjumlahtranfer).dom.value = formatCurrency(o.responseText);
																					Ext.get(txtjumlahbiayasal).dom.value=tmp[12];
																					Ext.get(txtjumlahtranfer).dom.value=tmp[12];
                                                                                    var datax = tampungmedrec.substring(0, 2);
                                                                                    if (datax === "RD" || tampungmedrec === undefined)
                                                                                    {
                                                                                        FormLookUpsdetailTRTransfer_radiologi.close();
                                                                                        ShowPesanWarningKasirRAD('Data Tidak dapat di transfer, hubungi Admin', 'Gagal');

                                                                                    }
                                                                                }, failure: function (o)
                                                                                {
                                                                                    var datax = tampungmedrec.substring(0, 2);
                                                                                    if (datax === "RD" || tampungmedrec === undefined)
                                                                                    {
                                                                                        FormLookUpsdetailTRTransfer_radiologi.close();
                                                                                        ShowPesanWarningKasirRAD('Data Tidak dapat di transfer,hubungi Admin', 'Gagal');
                                                                                    }
                                                                                }
                                                                            });

                                                                }

                                                            }
                                                        }

                                                    });


                                        }, //disabled :true
                                    },
                                    {
                                        xtype: 'splitbutton',
                                        text: 'Cetak',
                                        iconCls: 'print',
                                        id: 'btnPrint_viDaftar',
                                        handler: function ()
                                        {
                                        },
                                        menu: new Ext.menu.Menu({
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
                                                            id: 'btnPrintBillRadRAD',
                                                            handler: function ()
                                                            {
                                                                printbillRadLab();
                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Kwitansi',
                                                            id: 'btnPrintKwitansiRadRAD',
                                                            handler: function ()
                                                            {
                                                                printkwitansiRadLab();
                                                            }
                                                        }
                                                    ]
                                        })
                                    }
                                ]
                    }
            );



    var pnlTRKasirRAD2 = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirRAD2',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 380,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [GDtabDetailKasirRAD,
                        ]
                    }
            );




    FormDepan2KasirRAD = new Ext.Panel
            (
                    {
                        id: 'FormDepan2KasirRAD',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                height: 380,
                        shadhow: true,
                        items: [pnlTRKasirRAD, pnlTRKasirRAD2,
                        ]
                    }
            );

    return FormDepan2KasirRAD
}
;

function TambahBarisKasirRAD()
{
    var x = true;

    if (x === true)
    {
        var p = RecordBaruKasirRAD();
        dsTRDetailKasirRADKasirList.insert(dsTRDetailKasirRADKasirList.getCount(), p);
    }
    ;
}
;

function GetDTLTRHistoryGrid()
{

    var fldDetail = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'SHIFT', 'KD_USER'];

    dsTRDetailHistoryList = new WebApp.DataStore({fields: fldDetail})

    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'History Bayar',
                        stripeRows: true,
                        store: dsTRDetailHistoryList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100% 25%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
                                                            cellSelecteddeskripsi = dsTRDetailHistoryList.getAt(row);
                                                            CurrentHistory.row = row;
                                                            CurrentHistory.data = cellSelecteddeskripsi;
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRHistoryColumModel(),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnHpsBrsKasirRAD',
                                        text: 'Hapus Pembayaran',
                                        tooltip: 'Hapus Baris',
                                        iconCls: 'RemoveRow',
                                        //hidden :true,
                                        handler: function ()
                                        {
                                            if (dsTRDetailHistoryList.getCount() > 0)
                                            {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                    if (CurrentHistory != undefined)
                                                    {
                                                        HapusBarisDetailbayar();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningKasirRAD('Pilih record ', 'Hapus data');
                                                }
                                            }
                                            Ext.getCmp('btnEditKasirRAD').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirRAD').disable();
                                            Ext.getCmp('btnHpsBrsKasirRAD').disable();
                                        },
                                        disabled: true
                                    }
                                ]
                    }
            );

    return gridDTLTRHistory;
}
;

function TRHistoryColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Bayar',
                            dataIndex: 'URUT',
                            //hidden:true

                        },
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        },
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }
                        },
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colStatHistory',
                            header: 'History',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colPetugasHistory',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME',
                            //hidden:true
                        }

                    ]
                    )
}
;

function msg_box_alasanhapus()
{
    var lebar = 250;
    form_msg_box_alasanhapus = new Ext.Window
            (
                    {
                        id: 'alasan_hapus',
                        title: 'Alasan Hapus Pembayaran',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                mComboalasan_hapus(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'hapus',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_hapus',
                                                                    handler: function ()
                                                                    {
                                                                        DataDeleteKasirRADKasirDetail();
                                                                        form_msg_box_alasanhapus.close();
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_hapus',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanhapus.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanhapus.show();
}
;

function mComboalasan_hapus()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_hapus = new WebApp.DataStore({fields: Field});

    dsalasan_hapus.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'alasanhapus',
                                    param: ''
                                }
                    }
            );
    var cboalasan_hapus = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_hapus',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Alasan...',
                        labelWidth: 1,
                        store: dsalasan_hapus,
                        valueField: 'KD_ALASAN',
                        displayField: 'ALASAN',
                        anchor: '95%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
//                                  var selectalasan_hapus = b.data.KD_PROPINSI;
                                        //alert("is");
                                        selectDokter = b.data.KD_DOKTER;
                                    },
                                    'render': function (c)
                                    {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('kelPasien').focus();
                                        }, c);
                                    }
                                }
                    }
            );

    return cboalasan_hapus;
}
;
function HapusBarisDetailbayar()
{
    if (cellSelecteddeskripsi != undefined)
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
            //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
            /*	var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
             if (btn == 'ok')
             {
             variablehistori=combo;
             if (variablehistori!='')
             {
             DataDeleteKasirRADKasirDetail();
             }
             else
             {
             ShowPesanWarningKasirRAD('Silahkan isi alasan terlebih dahaulu','Keterangan');
             
             }
             }
             });*/
            msg_box_alasanhapus();
        } else
        {
            dsTRDetailHistoryList.removeAt(CurrentHistory.row);
        }
        ;
    }
}
;

function DataDeleteKasirRADKasirDetail()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/deletedetail_bayar",
                        params: getParamDataDeleteKasirRADKasirDetail(),
                        success: function (o)
                        {
                            //	RefreshDatahistoribayar(Kdtransaksi);
                            RefreshDataFilterKasirRADKasir();
                            RefreshDatahistoribayar('0');
                            RefreshDataKasirRADDetail('0');
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirRAD(nmPesanHapusSukses, nmHeaderHapusData);
                                //alert(Kdtransaksi);					 

                                //refeshKasirRADKasir();
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningKasirRAD(nmPesanHapusGagal, nmHeaderHapusData);
                            } else
                            {
                                ShowPesanWarningKasirRAD(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirRADKasirDetail()
{
    var params =
            {
                //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
                //Table: 'ViewKasirRAD' '','',// 
                TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
                TrTglbayar: CurrentHistory.data.data.TGL_BAYAR,
                Urut: CurrentHistory.data.data.URUT,
                Jumlah: CurrentHistory.data.data.BAYAR,
                Username: CurrentHistory.data.data.USERNAME,
                Shift: tampungshiftsekarang,
                Shift_hapus: CurrentHistory.data.data.SHIFT,
                Kd_user: CurrentHistory.data.data.KD_USER,
                Tgltransaksi: tgltrans,
                Kodepasein: kodepasien,
                NamaPasien: namapasien,
                KodeUnit: kodeunit,
                Namaunit: namaunit,
                Kodepay: kodepay,
                Uraian: CurrentHistory.data.data.DESKRIPSI,
                KDkasir: kdkasir,
                KeTterangan: Ext.get('cboalasan_hapus').dom.value

            };
    Kdtransaksi = CurrentHistory.data.data.NO_TRANSAKSI;
    return params
}
;



function GetDTLTRKasirRADGrid()
{
    var fldDetail = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'BAYARTR', 'DISCOUNT', 'PIUTANG'];

    dsTRDetailKasirRADKasirList = new WebApp.DataStore({fields: fldDetail})
    RefreshDataKasirRADKasirDetail();
    gridDTLTRKasirRAD = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Detail Bayar',
                        stripeRows: true,
                        id: 'gridDTLTRKasirRAD',
                        store: dsTRDetailKasirRADKasirList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        height: 230,
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        cm: TRKasirRawatJalanColumModel()
                    }


            );



    return gridDTLTRKasirRAD;
}
;

function TRKasirRawatJalanColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiKasirRAD',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 250,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiKasirRAD',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            id: 'colURUTKasirRAD',
                            header: 'Urut',
                            dataIndex: 'URUT',
                            sortable: false,
                            hidden: true,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            hidden: true,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colQtyKasirRAD',
                            header: 'Qty',
                            width: 91,
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                        },
                        {
                            id: 'colHARGAKasirRAD',
                            header: 'Harga',
                            align: 'right',
                            hidden: false,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colPiutangKasirRAD',
                            header: 'Puitang',
                            width: 80,
                            dataIndex: 'PIUTANG',
                            align: 'right',
                            //hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolPuitangRAD',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.PIUTANG);
                            }
                        },
                        {
                            id: 'colTunaiKasirRAD',
                            header: 'Tunai',
                            width: 80,
                            dataIndex: 'BAYARTR',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolTunaiRAD',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProduk();

                                return formatCurrency(record.data.BAYARTR);
                            }

                        },
                        {
                            id: 'colDiscountKasirRAD',
                            header: 'Discount',
                            width: 80,
                            dataIndex: 'DISCOUNT',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolDiscountRAD',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.DISCOUNT);
                            }
                        }

                    ]
                    )
}
;




function RecordBaruKasirRAD()
{

    var p = new mRecordKasirRAD
            (
                    {
                        'DESKRIPSI2': '',
                        'KD_PRODUK': '',
                        'DESKRIPSI': '',
                        'KD_TARIF': '',
                        'HARGA': '',
                        'QTY': '',
                        'TGL_TRANSAKSI': tanggaltransaksitampung,
                        'DESC_REQ': '',
                        'KD_TARIF':'',
                                'URUT': ''
                    }
            );

    return p;
}
;
function TRKasirRADInit(rowdata)
{
    AddNewKasirRADKasir = false;
	kdkasirasal = rowdata.KD_KASIR_ASAL;
    notransaksiasal = rowdata.NO_TRANSAKSI_ASAL;
    vkd_unit = rowdata.KD_UNIT;
    RefreshDataKasirRADKasirDetail(rowdata.NO_TRANSAKSI);
    Ext.get('txtNoTransaksiKasirRADKasir').dom.value = rowdata.NO_TRANSAKSI;
    tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
    Ext.get('txtNoMedrecDetransaksi').dom.value = rowdata.KD_PASIEN;
    Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
    Ext.get('cboPembayaran').dom.value = rowdata.KET_PAYMENT;
    Ext.get('cboJenisByr').dom.value = rowdata.CARA_BAYAR; // take the displayField value 
    loaddatastorePembayaran(rowdata.JENIS_PAY);
    vkode_customer = rowdata.KD_CUSTOMER;
    tampungtypedata = 0;
    tapungkd_pay =  rowdata.KD_PAY;
    jenispay = 1;
    var vkode_customer = rowdata.LUNAS


    showCols(Ext.getCmp('gridDTLTRKasirRAD'));
    hideCols(Ext.getCmp('gridDTLTRKasirRAD'));
    vflag = rowdata.FLAG;
    //(rowdata.NO_TRANSAKSI;


    Ext.Ajax.request(
            {
                url: baseURL + "index.php/main/getcurrentshift",
                params: {
                    command: '0',
                },
                failure: function (o)
                {
                    var cst = Ext.decode(o.responseText);

                },
                success: function (o) {

                    tampungshiftsekarang = o.responseText
                }

            });



}
;

function mEnabledKasirRADCM(mBol)
{
    Ext.get('btnLookupKasirRAD').dom.disabled = mBol;
    Ext.get('btnHpsBrsKasirRAD').dom.disabled = mBol;
}
;


//---------------------------------------------------------------------------------------///
function KasirRADAddNew()
{
    AddNewKasirRADKasir = true;
    Ext.get('txtNoTransaksiKasirRADKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
    Ext.get('txtNoMedrecDetransaksi').dom.value = '';
    Ext.get('txtNamaPasienDetransaksi').dom.value = '';

    //Ext.get('txtKdUrutMasuk').dom.value = '';
    Ext.get('cboStatus_viKasirRADKasir').dom.value = ''
    rowSelectedKasirRADKasir = undefined;
    dsTRDetailKasirRADKasirList.removeAll();
    mEnabledKasirRADCM(false);


}
;

function RefreshDataKasirRADKasirDetail(no_transaksi)
{
    var strKriteriaKasirRAD = '';

    strKriteriaKasirRAD = 'no_transaksi = ~' + no_transaksi + '~';

    dsTRDetailKasirRADKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailbayar',
                                    param: strKriteriaKasirRAD
                                }
                    }
            );
    return dsTRDetailKasirRADKasirList;
}
;

function RefreshDatahistoribayar(no_transaksi)
{
    var strKriteriaKasirRAD = '';

    strKriteriaKasirRAD = 'no_transaksi= ~' + no_transaksi + '~';

    dsTRDetailHistoryList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewHistoryBayar',
                                    param: strKriteriaKasirRAD
                                }
                    }
            );
    return dsTRDetailHistoryList;
}
;


//---------------------------------------------------------------------------------------///

function GetListCountPenataDetailTransaksi()
{

    var x = 0;
    for (var i = 0; i < dsTRDetailKasirRADList.getCount(); i++)
    {
        if (dsTRDetailKasirRADList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirRADList.data.items[i].data.DESKRIPSI != '')
        {
            x += 1;
        }
        ;
    }
    return x;

}
;

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiKasirRAD()
{
    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    }
    ;

    var params =
            {
                kdUnit: vkd_unit,
                TrKodeTranskasi: Ext.get('txtNoTransaksiKasirRADKasir').getValue(),
                Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
                Shift: tampungshiftsekarang,
                kdKasir: kdkasir,
                bayar: tapungkd_pay,
                Flag: vflag,
                Typedata: tampungtypedata,
                Totalbayar: getTotalDetailProduk(),
                List: getArrDetailTrKasirRAD(),
                JmlField: mRecordKasirRAD.prototype.fields.length - 4,
                JmlList: GetListCountDetailTransaksi(),
                Hapus: 1,
                Ubah: 0
            };
    return params
}
;

function paramUpdateTransaksi()
{
    var params =
            {
                TrKodeTranskasi: cellSelectedtutup,
                KDkasir: kdkasir
            };
    return params
}
;

function GetListCountDetailTransaksi()
{

    var x = 0;
    for (var i = 0; i < dsTRDetailKasirRADKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirRADKasirList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirRADKasirList.data.items[i].data.DESKRIPSI != '')
        {
            x += 1;
        }
        ;
    }
    return x;

}
;

function getTotalDetailProduk()
{
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < dsTRDetailKasirRADKasirList.getCount(); i++)
    {

        var recordterakhir;

        //alert(TotalProduk);
        if (tampungtypedata == 0)
        {
            tampunggrid = parseInt(dsTRDetailKasirRADKasirList.data.items[i].data.BAYARTR);

            TotalProduk += tampunggrid

            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirRAD').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 3)
        {
            tampunggrid = parseInt(dsTRDetailKasirRADKasirList.data.items[i].data.PIUTANG);

            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirRAD').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 1)
        {
            tampunggrid = parseInt(dsTRDetailKasirRADKasirList.data.items[i].data.DISCOUNT);

            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirRAD').dom.value = formatCurrency(recordterakhir);
        }

    }
    bayar = Ext.get('txtJumlah2EditData_viKasirRAD').getValue();
    return bayar;
}
;







function getArrDetailTrKasirRAD()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirRADKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirRADKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirRADKasirList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirRADKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.QTY
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.BAYARTR
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.PIUTANG
            y += z + dsTRDetailKasirRADKasirList.data.items[i].data.DISCOUNT



            if (i === (dsTRDetailKasirRADKasirList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }

    return x;
}
;


function getItemPanelInputKasir(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: true,
                height: 110,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getItemPanelNoTransksiRADKasir(lebar), getItemPanelmedreckasir(lebar), getItemPanelUnitKasir(lebar)
                                        ]
                            }
                        ]
            };
    return items;
}
;



function getItemPanelUnitKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboJenisByrView()

                                        ]
                            }, {
                                columnWidth: .60,
                                layout: 'form',
                                labelWidth: 0.9,
                                border: false,
                                items:
                                        [mComboPembayaran()

                                        ]
                            }
                        ]
            }
    return items;
}
;



function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'nama',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboBayar',
                                    param: 'jenis_pay=~' + jenis_pay + '~'
                                }
                    }
            )
}

function mComboPembayaran()
{
    var Field = ['KD_PAY', 'JENIS_PAY', 'PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});


    var cboPembayaran = new Ext.form.ComboBox
            (
                    {
                        id: 'cboPembayaran',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Pembayaran...',
                        labelWidth: 80,
                        align: 'Right',
                        store: dsComboBayar,
                        valueField: 'KD_PAY',
                        displayField: 'PAYMENT',
                        anchor: '100%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        tapungkd_pay = b.data.KD_PAY;
                                        //getTotalDetailProduk();


                                    },
                                }
                    }
            );

    return cboPembayaran;
}
;


function mComboJenisByrView()
{
    var Field = ['JENIS_PAY', 'DESKRIPSI', 'TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({fields: Field});
    dsJenisbyrView.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'jenis_pay',
                                    Sortdir: 'ASC',
                                    target: 'ComboJenis',
                                }
                    }
            );

    var cboJenisByr = new Ext.form.ComboBox
            (
                    {
                        id: 'cboJenisByr',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Pembayaran      ',
                        align: 'Right',
                        anchor: '100%',
                        store: dsJenisbyrView,
                        valueField: 'JENIS_PAY',
                        displayField: 'DESKRIPSI',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        loaddatastorePembayaran(b.data.JENIS_PAY);
                                        tampungtypedata = b.data.TYPE_DATA;
                                        jenispay = b.data.JENIS_PAY;
                                        showCols(Ext.getCmp('gridDTLTRKasirRAD'));
                                        hideCols(Ext.getCmp('gridDTLTRKasirRAD'));
                                        getTotalDetailProduk();
                                        Ext.get('cboPembayaran').dom.value = 'Pilih Pembayaran...';
                                    },
                                }
                    }
            );

    return cboJenisByr;
}
;
function hideCols(grid)
{
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);

    }
}
;
function showCols(grid) {
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);

    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
    }

}
;



function getItemPanelDokter(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Dokter  ',
                                                name: 'txtKdDokter',
                                                id: 'txtKdDokter',
                                                //emptyText:nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%'
                                            }, {
                                                xtype: 'textfield',
                                                fieldLabel: 'Kelompok Pasien',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtCustomer',
                                                id: 'txtCustomer',
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .600,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                // fieldLabel:'Unit : ',
                                                name: 'txtNamaDokter',
                                                id: 'txtNamaDokter',
                                                //emptyText:nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                // fieldLabel:'Unit : ',
                                                name: 'txtKdUrutMasuk',
                                                id: 'txtKdUrutMasuk',
                                                //emptyText:nmNomorOtomatis,
                                                readOnly: true,
                                                hidden: true,
                                                anchor: '100%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getItemPanelNoTransksiRADKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Transaksi ',
                                                name: 'txtNoTransaksiKasirRADKasir',
                                                id: 'txtNoTransaksiKasirRADKasir',
                                                emptyText: nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%'
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 55,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTanggalDetransaksi',
                                                name: 'dtpTanggalDetransaksi',
                                                format: 'd/M/Y',
                                                readOnly: true,
                                                value: now,
                                                anchor: '100%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function getItemPanelmedreckasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Medrec ',
                                                name: 'txtNoMedrecDetransaksi',
                                                id: 'txtNoMedrecDetransaksi',
                                                readOnly: true,
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: '',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtNamaPasienDetransaksi',
                                                id: 'txtNamaPasienDetransaksi',
                                                anchor: '100%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function RefreshDataKasirRADKasir()
{
    dsTRKasirRADKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountKasirRADKasir,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewKasirRAD',
                                    param: ''
                                }
                    }
            );

    rowSelectedKasirRADKasir = undefined;
    return dsTRKasirRADKasirList;
}
;

function refeshKasirRADKasir()
{
    dsTRKasirRADKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountKasirRADKasir,
                                    //Sort: 'no_transaksi',
                                    Sort: '',
                                    //Sort: 'no_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewKasirRAD',
                                    param: ''
                                }
                    }
            );
    return dsTRKasirRADKasirList;
}

function RefreshDataFilterKasirRADKasir()
{

    var KataKunci = '';

    if (Ext.getCmp('txtFilterKasirNomedrecRad').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = '   LOWER(kode_pasien) like  LOWER( ~' + Ext.getCmp('txtFilterKasirNomedrecRad').getValue() + '%~)';

        } else
        {

            KataKunci += ' and  LOWER(kode_pasien) like  LOWER( ~' + Ext.getCmp('txtFilterKasirNomedrecRad').getValue() + '%~)';
        }
        ;

    }
    ;
    if (KataKunci == '')
    {
        //alert ('');
        KataKunci = '   kd_bagian = 5';
    } else
    {

        KataKunci += ' and  kd_bagian = 5';
    }
    ;

    if (Ext.getCmp('TxtFilterGridDataView_NAMA_viKasirRADKasir').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = '  LOWER(nama) like  LOWER( ~' + Ext.getCmp('TxtFilterGridDataView_NAMA_viKasirRADKasir').getValue() + '%~)';

        } else
        {

            KataKunci += ' and  LOWER(nama) like  LOWER( ~' + Ext.getCmp('TxtFilterGridDataView_NAMA_viKasirRADKasir').getValue() + '%~)';
        }
        ;

    }
    ;


    if (Ext.getCmp('cboUNIT_viKasirRADKasir').getValue() != '' && Ext.getCmp('cboUNIT_viKasirRADKasir').getValue() != 'All' && Ext.getCmp('cboUNIT_viKasirRADKasir').getValue() != 'Semua')
    {
        if (KataKunci == '')
        {

            KataKunci = '  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirRADKasir').getValue() + '%~)';
        } else
        {

            KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirRADKasir').getValue() + '%~)';
        }
        ;
    }
    ;


    if (Ext.get('cboStatus_viKasirRADKasir').getValue() == 'Lunas' || Ext.getCmp('cboStatus_viKasirRADKasir').getValue() == 2)
    {
        if (KataKunci == '')
        {

            KataKunci = '   lunas = ~true~';
        } else
        {

            KataKunci += ' and lunas =  ~true~';
        }
        ;

    }
    ;


    if (Ext.get('cboStatus_viKasirRADKasir').getValue() == 'Semua' || Ext.getCmp('cboStatus_viKasirRADKasir').getValue() == 1)
    {
    }
    ;
    if (Ext.get('cboStatus_viKasirRADKasir').getValue() == 'Belum Lunas' || Ext.getCmp('cboStatus_viKasirRADKasir').getValue() == 3)
    {
        if (KataKunci == '')
        {
            KataKunci = '   lunas = ~false~';
        } else
        {
            KataKunci += ' and lunas =  ~false~';
        }
        ;


    }
    ;
    if (Ext.get('dtpTglAwalFilterKasirRAD').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = " (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirRAD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirRAD').getValue() + "~)";
        } else
        {

            KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirRAD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirRAD').getValue() + "~)";
        }
        ;

    }
    ;



    if (KataKunci != undefined || KataKunci != '')
    {
        dsTRKasirRADKasirList.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCountKasirRADKasir,
                                        //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                                        //Sort: 'no_transaksi',
                                        Sortdir: 'ASC',
                                        target: 'ViewKasirRAD',
                                        param: KataKunci
                                    }
                        }
                );
    } else
    {

        dsTRKasirRADKasirList.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCountKasirRADKasir,
                                        //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                                        //Sort: 'no_transaksi',
                                        Sortdir: 'ASC',
                                        target: 'ViewKasirRAD',
                                        param: ''
                                    }
                        }
                );
    }
    ;

    return dsTRKasirRADKasirList;
}
;



function Datasave_KasirRADKasir(mBol)
{
    if (ValidasiEntryCMKasirRAD(nmHeaderSimpanData, false) == 1)
    {
        Ext.Ajax.request
                (
                        {
                            //url: "./Datapool.mvc/CreateDataObj",
                            url: baseURL + "index.php/main/functionKasirPenunjang/savepembayaran",
                            params: getParamDetailTransaksiKasirRAD(),
                            failure: function (o)
                            {
                                ShowPesanWarningKasirRAD('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                RefreshDataFilterKasirRADKasir();
                            },
                            success: function (o)
                            {
                                RefreshDatahistoribayar('0');
                                RefreshDataKasirRADDetail('0');
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
								
                                {
									Ext.getCmp('btnTransferKasirRAD').disable();
									Ext.getCmp('btnSimpanKasirRAD').disable();
                                    ShowPesanInfoKasirRAD(nmPesanSimpanSukses, nmHeaderSimpanData);
                                    RefreshDataKasirRADKasirDetail(Ext.get('txtNoTransaksiKasirRADKasir').dom.value);
                                    //RefreshDataKasirRADKasir();
                                    if (mBol === false)
                                    {
                                        RefreshDataFilterKasirRADKasir();
                                    }
                                    ;
                                } else
                                {
                                    ShowPesanWarningKasirRAD('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                }
                                ;
                            }
                        }
                )
    } else
    {
        if (mBol === true)
        {
            return false;
        }
        ;
    }
    ;

}
;
function UpdateTutuptransaksi(mBol)
{

    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/ubah_co_status_transksi",
                        params: paramUpdateTransaksi(),
                        failure: function (o)
                        {
                            ShowPesanWarningKasirRAD('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                            RefreshDataFilterKasirRADKasir();
                        },
                        success: function (o)
                        {
                            //RefreshDatahistoribayar(Ext.get('cellSelectedtutup);

                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirRAD(nmPesanSimpanSukses, nmHeaderSimpanData);

                                //RefreshDataKasirRADKasir();
                                if (mBol === false)
                                {
                                    RefreshDataFilterKasirRADKasir();
                                }
                                ;
                                cellSelectedtutup = '';
                            } else
                            {
                                ShowPesanWarningKasirRAD('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                                cellSelectedtutup = '';
                            }
                            ;
                        }
                    }
            )



}
;

function ValidasiEntryCMKasirRAD(modul, mBolHapus)
{
    var x = 1;

    if ((Ext.get('txtNoTransaksiKasirRADKasir').getValue() == '')

            || (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
            || (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
            || (Ext.get('dtpTanggalDetransaksi').getValue() == '')
            || dsTRDetailKasirRADKasirList.getCount() === 0
            || (Ext.get('cboPembayaran').getValue() == '')
            || (Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...'))
    {
        if (Ext.get('txtNoTransaksiKasirRADKasir').getValue() == '' && mBolHapus === true)
        {
            x = 0;
        } else if (Ext.get('cboPembayaran').getValue() == '' || Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...')
        {
            ShowPesanWarningKasirRAD(('Data pembayaran tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirRAD(('Data no. medrec tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirRAD('Nama pasien belum terisi', modul);
            x = 0;
        } else if (Ext.get('dtpTanggalDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirRAD(('Data tanggal kunjungan tidak boleh kosong'), modul);
            x = 0;
        }
        //cboPembayaran

        else if (dsTRDetailKasirRADKasirList.getCount() === 0)
        {
            ShowPesanWarningKasirRAD('Data dalam tabel kosong', modul);
            x = 0;
        }
        ;
    }
    ;
    return x;
}
;




function ShowPesanWarningKasirRAD(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanErrorKasirRAD(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoKasirRAD(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function printbillRadLab()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakbill_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningKasirRAD('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            } else
                            {
                                ShowPesanErrorKasirRAD('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            }
                        }
                    }
            );
}
;

var tmpkdkasirRadLab = '1';
function dataparamcetakbill_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectPrintingRadLab',
                No_TRans: Ext.get('txtNoTransaksiKasirRADKasir').getValue(),
                KdKasir: tmpkdkasirRadLab,
                modul: 'rad'
            };
    return paramscetakbill_vikasirDaftarRadLab;
}
;

function printkwitansiRadLab()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakkwitansi_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan !== '')
                            {
                                ShowPesanWarningKasirRAD('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanErrorKasirRAD('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            );
}
;

function dataparamcetakkwitansi_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectKwitansiRadLab',
                No_TRans: Ext.get('txtNoTransaksiKasirRADKasir').getValue(),
            };
    return paramscetakbill_vikasirDaftarRadLab;
}
;

