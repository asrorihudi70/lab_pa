var GridProtokol={};
GridProtokol.data_store_hasil_lab;
GridProtokol.grid_store_hasil_lab;
GridProtokol.get_grid;
GridProtokol.column;

GridProtokol.column=function(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
        {
			header: '',
			dataIndex: 'item_test',
			width:400,
			menuDisabled:true,
			hidden:true
		},
		{
			header: 'Jumlah',
			dataIndex: 'jml',
			width:150,
			menuDisabled:true,
			
		},
		{
			header:'Tanggal Rencana Tindakan',
			dataIndex: 'tgl_rencana_tindakan',
			sortable: false,
			hidden:false,
			menuDisabled:true,
			width:200
			
		},
		
		
    ]);
}

GridProtokol.get_grid=function(){
	var fldDetail = ['item_test', 'jumlah', 'tgl_rencana_tindakan'];
    GridProtokol.data_store_hasil_lab = new WebApp.DataStore({ fields: fldDetail });
	GridProtokol.data_store_hasil_lab.removeAll();
    GridProtokol.grid_store_hasil_lab = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'grid_store_resume_protokol',
        store: GridProtokol.data_store_hasil_lab,
        border: false,
        columnLines: true,
        autoScroll:true,
        height 		: 500,
        anchor 		: '100% 100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: GridProtokol.column(),
		viewConfig:{forceFit: true}
    });
    return GridProtokol.grid_store_hasil_lab;
}