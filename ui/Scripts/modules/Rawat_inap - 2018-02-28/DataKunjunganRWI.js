// Data Source ExtJS # --------------

/**
 *	Nama File 		: TRDataKunjunganRWI.js
 *	Menu 			: Pendaftaran
 *	Model id 		: 030101
 *	Keterangan 		: Data Pasien adalah proses untuk melihat list/ daftar pasien, selain itu data pasien ini digunakan untuk___
 *					  ___melengkapi atau memperbaiki biodata dari pasien
 *	Di buat tanggal : 15 April 2014
 *	Oleh 			: SDY_RI
 *	Edit                    : HDHT
 */

// Deklarasi Variabel pada Data Pasien # --------------
var CurrentHistoryDataKunjunganRWI =
        {
            data: Object,
            details: Array,
            row: 0
        };
var DataKunjunganRWIvariable = {};
DataKunjunganRWIvariable.kd_pasien;
DataKunjunganRWIvariable.urut;
DataKunjunganRWIvariable.tglkunjungan;
DataKunjunganRWIvariable.kd_unit;
var mod_name_viDataKunjunganRWI = "viDataKunjunganRWI";
var NamaForm_viDataKunjunganRWI = "Data Kunjungan Rawat Inap";
var selectSetJK;
var selectSetGolDarah;
var selectSetSatusMarital;
var icons_viDataKunjunganRWI = "Data_pegawai";
var DfltFilterBtn_viDataKunjunganRWI = 0;
var slctCount_viDataKunjunganRWI = 10;
var now_viDataKunjunganRWI = new Date();
var rowSelectedGridDataView_viDataKunjunganRWI;
var setLookUpsGridDataView_viDataKunjunganRWI;
var addNew_viDataKunjunganRWI;
var tmphasil;
var tmpjk;
/* 
	PERBARUAN VARIALBLE GANTI KELOMPOK PASIEN
	LEH 	: HADAD
	TANGGAL : 2016 - 12 -31
 */
var cellSelecteddeskripsi_DataKunjunganRWI;
var jeniscus_RWJ;
var labelisi_RWJ;
var FormLookUpsdetailTRKelompokPasien_rwj;
var vkode_customer_RWJ;
var KelompokPasienAddNew_RWJ=true;
var vCustomer;
var vCo_status;
var vNoSEP;
var vKd_Pasien;
var vTanggal;
var vKdUnit;
var vKdUnitDulu = "";
var vKdDokter;
var vNoAsuransi;
var vNamaUnit;
var vNamaDokter;
var vKdKasir;
var vUrutMasuk;
var vNoTransaksi;
var panelActiveDataKunjunganRWI;
var statusCekDetail;
var dsDokterRequestEntry;
/* ------------------------------- END --------------------------- */
var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});
CurrentPage.page = dataGrid_viDataKunjunganRWI(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


function loaddatastoredokter(){
	dsDokterRequestEntry.load({
         params	:{
            Skip	: 0,
		    Take	: 1000,
            Sort	: 'nama',
		    Sortdir	: 'ASC',
		    target	: 'ViewComboDokter',
		    param	: 'where dk.kd_unit=~'+ vKdUnit+ '~'
		}
    });
}

// Start Project Data Pasien # --------------

// --------------------------------------- # Start Function # ---------------------------------------

/**
 *	Function : ShowPesanWarning_viDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
 */

function ShowPesanWarning_viDataKunjunganRWI(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            )
}
// End Function ShowPesanWarning_viDataKunjunganRWI # --------------

/**
 *	Function : ShowPesanError_viDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
 */

function ShowPesanError_viDataKunjunganRWI(str, modul){
    Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.ERROR,
		width: 250
	});
}
// End Function ShowPesanError_viDataKunjunganRWI # --------------

/**
 *	Function : ShowPesanInfo_viDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat data berhasil
 */

function ShowPesanInfo_viDataKunjunganRWI(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            )
}
// End Function ShowPesanInfo_viDataKunjunganRWI # --------------

/**
 *	Function : getCriteriaFilterGridDataView_viDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk memfilter data yang diambil dari Net.
 */
function getCriteriaFilterGridDataView_viDataKunjunganRWI(){
    var strKriteriaGridDataView_viDataKunjunganRWIGridDataView_viDataKunjunganRWI = "";

    //strKriteriaGridDataView_viDataKunjunganRWI = " WHERE PS.KD_CUSTOMER = '" + DfltFilter_KD_CUSTOMER_viDataKunjunganRWI + "' ";

    if (DfltFilterBtn_viDataKunjunganRWI == 1)
    {
        if (Ext.getCmp('TxtFilterGridDataView_NO_RM_viDataKunjunganRWI').getValue() != "")
        {
            strKriteriaGridDataView_viDataKunjunganRWI += " AND PS.KD_PASIEN LIKE '" + DfltFilter_KD_CUSTOMER_viDataKunjunganRWI + ".%" + Ext.getCmp('TxtFilterGridDataView_NO_RM_viDataKunjunganRWI').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NAMA_viDataKunjunganRWI').getValue() != "")
        {
            strKriteriaGridDataView_viDataKunjunganRWI += " AND PS.NAMA LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NAMA_viDataKunjunganRWI').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NO_TELP_viDataKunjunganRWI').getValue() != "")
        {
            strKriteriaGridDataView_viDataKunjunganRWI += " AND PS.NO_TELP LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NO_TELP_viDataKunjunganRWI').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NO_HP_viDataKunjunganRWI').getValue() != "")
        {
            strKriteriaGridDataView_viDataKunjunganRWI += " AND PS.NO_HP LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NO_HP_viDataKunjunganRWI').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_ALAMAT_viDataKunjunganRWI').getValue() != "")
        {
            strKriteriaGridDataView_viDataKunjunganRWI += " AND PS.ALAMAT LIKE '%" + Ext.getCmp('TxtFilterGridDataView_ALAMAT_viDataKunjunganRWI').getValue() + "%' ";
        }
    }

    //strKriteriaGridDataView_viDataKunjunganRWI += " ORDER BY PS.KD_PASIEN ASC ";

    // DfltFilterBtn_viDataKunjunganRWI = 0; // Tidak diaktifkan agar saat tutup windows popup tetap di filter
    //return strKriteriaGridDataView_viDataKunjunganRWI;
}
// End Function getCriteriaFilterGridDataView_viDataKunjunganRWI # --------------

/**
 *	Function : ValidasiEntry_viDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk mengecek isian
 */
function ValidasiEntry_viDataKunjunganRWI(modul, mBolHapus)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viDataKunjunganRWI').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataKunjunganRWI').getValue() === 'Tempat Lahir' ||
            (Ext.get('TxtWindowPopup_ALAMAT_viDataKunjunganRWI').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_NAMA_viDataKunjunganRWI').getValue() === '' ||
                    (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataKunjunganRWI').getValue() === '' || (Ext.get('TxtWindowPopup_ALAMAT_viDataKunjunganRWI').getValue() === ''))))))
    {
        if (Ext.get('TxtWindowPopup_NAMA_viDataKunjunganRWI').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viDataKunjunganRWI').getValue() === ''))
        {
            ShowPesanWarning_viDataKunjunganRWI('Nama belum terisi', modul);
            x = 0;
        } else if (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataKunjunganRWI').getValue() === 'Tempat Lahir' || (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataKunjunganRWI').getValue() === ''))
        {
            ShowPesanWarning_viDaftar("Tempat Lahir belum terisi", modul);
            x = 0;
        } else if (Ext.get('TxtWindowPopup_ALAMAT_viDataKunjunganRWI').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_ALAMAT_viDataKunjunganRWI').getValue() === ''))
        {
            ShowPesanWarning_viDaftar("Alamat belum terisi", modul);
            x = 0;
        }
    }

    return x;
}


function ValidasiEntryHistori_viDataKunjunganRWI(modul)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viDataKunjunganRWI').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viDataKunjunganRWI').getValue() === ''))
    {
        ShowPesanWarning_viDataKunjunganRWI('Nama belum terisi', modul);
        x = 0;
    }

    return x;
}
// End Function ValidasiEntry_viDataKunjunganRWI # --------------

/**
 *	Function : DataRefresh_viDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk mengambil data dari Net.
 *	Digunakan pada View Grid Pertama, Filter Grid 
 */

function DataRefresh_viDataKunjunganRWI(criteria){
    /* dataSourceGrid_viDataKunjunganRWI.load({
		params:{
			Skip: 0,
			Take: slctCount_viDataKunjunganRWI,
			Sort: 'kunjungan.tgl_masuk',
			Sortdir: 'ASC',
			target: 'Vi_ViewDataPasien',
			param: criteria

		}
	});
    return dataSourceGrid_viDataKunjunganRWI; */
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/rawat_inap/viewdatakunjunganpasienrwi/getPasien",
			params: {
				query:criteria
			},
			failure: function(o)
			{
				ShowPesanError_viDataKunjunganRWI('Hubungi Admin! Error get data pasien.', 'Error');
			},	
			success: function(o) 
			{
				dataSourceGrid_viDataKunjunganRWI.removeAll();
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dataSourceGrid_viDataKunjunganRWI.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dataSourceGrid_viDataKunjunganRWI.add(recs);
				}
			}
		}
		
	)
}
// End Function DataRefresh_viDataKunjunganRWI # --------------

/**
 *	Function : datainit_viDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
 */

function datainit_viDataKunjunganRWI(rowdata){
	var tmp_data;
    addNew_viDataKunjunganRWI = false;
    //-------------- # textfield # --------------
    Ext.get('TxtWindowPopup_KD_CUSTOMER_viDataKunjunganRWI').dom.value = rowdata.TGL_MASUK;
    Ext.get('TxtWindowPopup_KD_PASIEN_viDataKunjunganRWI').dom.value = rowdata.KD_UNIT;
    Ext.get('TxtWindowPopup_NO_RM_viDataKunjunganRWI').dom.value = rowdata.KD_PASIEN;
    Ext.get('TxtWindowPopup_Urut_Masuk_viDataKunjunganRWI').dom.value = rowdata.URUT_MASUK;
    Ext.get('TxtWindowPopup_NAMA_viDataKunjunganRWI').dom.value = rowdata.NAMA;
    Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataKunjunganRWI').dom.value = rowdata.TEMPAT_LAHIR;
    Ext.get('TxtWindowPopup_TAHUN_viDataKunjunganRWI').dom.value = rowdata.TAHUN;
    Ext.get('TxtWindowPopup_BULAN_viDataKunjunganRWI').dom.value = rowdata.BULAN;
    Ext.get('TxtWindowPopup_HARI_viDataKunjunganRWI').dom.value = rowdata.HARI;
    Ext.get('TxtWindowPopup_ALAMAT_viDataKunjunganRWI').dom.value = rowdata.ALAMAT;
    Ext.get('TxtTmpAgama_viDataKunjunganRWI').dom.value = rowdata.KD_AGAMA;
    Ext.get('TxtTmpPekerjaan_viDataKunjunganRWI').dom.value = rowdata.KD_PEKERJAAN;
    Ext.get('TxtTmpPendidikan_viDataKunjunganRWI').dom.value = rowdata.KD_PENDIDIKAN;
    //Ext.get('TxtWindowPopup_NO_HP_viDataKunjunganRWI').dom.value=TmpNoHP;
    Ext.get('TxtWindowPopup_TAHUN_viDataKunjunganRWI').dom.value = '';
    Ext.get('TxtWindowPopup_BULAN_viDataKunjunganRWI').dom.value = '';
    Ext.get('TxtWindowPopup_HARI_viDataKunjunganRWI').dom.value = '';
    setUsia(rowdata.TGL_LAHIR_PASIEN);
    //-------------- # datefield # --------------
    Ext.getCmp('DateWindowPopup_TGL_LAHIR_viDataKunjunganRWI').setValue(rowdata.TGL_LAHIR_PASIEN);

    //-------------- # combobox # --------------
    if (rowdata.JENIS_KELAMIN === "t" || rowdata.JENIS_KELAMIN === 1){
        tmpjk = "1";
        tmp_data = "Laki- laki";
    }else{
        tmpjk = "2";
        tmp_data = "Perempuan";
    }
    ID_JENIS_KELAMIN = rowdata.JENIS_KELAMIN;
    Ext.getCmp('cboJK').setValue(tmpjk);
    Ext.getCmp('cboGolDarah').setValue(rowdata.GOL_DARAH);
    Ext.getCmp('cboStatusMarital').setValue(rowdata.STATUS_MARITA);
    Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowdata.PEKERJAAN);
    Ext.getCmp('cboAgamaRequestEntry').setValue(rowdata.AGAMA);
    Ext.getCmp('cboPendidikanRequestEntry').setValue(rowdata.PENDIDIKAN);
    Ext.getCmp('TxtWindowPopup_NO_TELP_viDataKunjunganRWI').setValue(rowdata.TELEPON);
    Ext.getCmp('cbokelurahan_editDataKunjunganRWI').setValue(rowdata.KELURAHAN);
    Ext.getCmp('cboKecamatan_editDataKunjunganRWI').setValue(rowdata.KECAMATAN);
    Ext.getCmp('cboKabupaten_editDataKunjunganRWI').setValue(rowdata.KABUPATEN);
    Ext.getCmp('cboPropinsi_EditDataKunjunganRWI').setValue(rowdata.PROPINSI);
    loaddatastorekelurahan(rowdata.KD_KECAMATAN);
    loaddatastorekecamatan(rowdata.KD_KABUPATEN);
    loaddatastorekabupaten(rowdata.KD_PROPINSI);
    selectPropinsiRequestEntry = rowdata.KD_PROPINSI;
    selectKabupatenRequestEntry = rowdata.KD_KABUPATEN;
    selectKecamatanpasien = rowdata.KD_KECAMATAN;
    kelurahanpasien = rowdata.KD_KELURAHAN;
    Ext.getCmp('TxtWindowPopup_Email_viDataKunjunganRWI').setValue(rowdata.EMAIL);
    Ext.getCmp('TxtWindowPopup_HP_viDataKunjunganRWI').setValue(rowdata.HP);
    Ext.getCmp('TxtWindowPopup_Nama_ayah_viDataKunjunganRWI').setValue(rowdata.AYAH);
    Ext.getCmp('TxtWindowPopup_NamaIbu_viDataKunjunganRWI').setValue(rowdata.IBU);
    Ext.getCmp('TxtWindowPopup_Nama_keluarga_viDataKunjunganRWI').setValue(rowdata.NAMA_KELUARGA);
}
// End Function datainit_viDataKunjunganRWI # --------------

/**
 *	Function : dataparam_datapasviDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk mengirim balik isian ke Net.
 *	Digunakan saat save, edit, delete
 */

function setUsia(Tanggal){
    Ext.Ajax.request({
		url: baseURL + "index.php/main/GetUmur",
		params: {
			TanggalLahir: Tanggal
		},
		success: function (o){
			var tmphasil = o.responseText;
			var tmp = tmphasil.split(' ');
			if (tmp.length == 6){
				Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
				Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue(tmp[2]);
				Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue(tmp[4]);
			} else if (tmp.length == 4)
			{
				if (tmp[1] === 'years' && tmp[3] === 'day')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue(tmp[2]);
				} else if (tmp[1] === 'year' && tmp[3] === 'days')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue(tmp[2]);
				} else
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue(tmp[2]);
				}
			} else if (tmp.length == 2)
			{

				if (tmp[1] == 'year')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue('0');
				} else if (tmp[1] == 'years')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue('0');
				} else if (tmp[1] == 'mon')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue('0');
				} else if (tmp[1] == 'mons')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue('0');
				} else {
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue(tmp[0]);
				}
			} else if (tmp.length == 1)
			{
				Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue('0');
				Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
				Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue('1');
			} else
			{
				alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
			}
		}


            });



}
/*
    PERBARUAN UPDATE DATA PASIEN
    OLEH    : HADAD AL GOJALI 
    TANGGAL : 2016 - 01 - 30
    ALASAN  : DATA TIDAK MENGUPDATE KE SQL SERVER 
 */

function dataparam_datapasviDataKunjunganRWI(){
    var params_viDataKunjunganRWI ={
                //-------------- # modelist Net. # --------------
		Table: 'Vi_ViewDataKunjunganRWI',
		//-------------- # textfield # --------------
		NO_MEDREC: Ext.getCmp('TxtWindowPopup_NO_RM_viDataKunjunganRWI').getValue(),
		NAMA: Ext.getCmp('TxtWindowPopup_NAMA_viDataKunjunganRWI').getValue(),
		TEMPAT_LAHIR: Ext.getCmp('TxtWindowPopup_TEMPAT_LAHIR_viDataKunjunganRWI').getValue(),
		ALAMAT: Ext.getCmp('TxtWindowPopup_ALAMAT_viDataKunjunganRWI').getValue(),
		NO_TELP: Ext.getCmp('TxtWindowPopup_NO_TELP_viDataKunjunganRWI').getValue(),
		//NO_HP: Ext.get('TxtWindowPopup_NO_HP_viDataKunjunganRWI').getValue(),

		//-------------- # datefield # --------------
		TGL_LAHIR: Ext.get('DateWindowPopup_TGL_LAHIR_viDataKunjunganRWI').getValue(),
		//-------------- # combobox # --------------
		ID_JENIS_KELAMIN: Ext.getCmp('cboJK').getValue(),
		ID_GOL_DARAH: Ext.getCmp('cboGolDarah').getValue(),
		KD_STS_MARITAL: Ext.getCmp('cboStatusMarital').getValue(),
		NAMA_PEKERJAAN: Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
		NAMA_AGAMA: Ext.getCmp('cboAgamaRequestEntry').getValue(),
		NAMA_PENDIDIKAN: Ext.getCmp('cboPendidikanRequestEntry').getValue(),
		//KD_PEKERJAAN:Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
		//KD_AGAMA:Ext.getCmp('cboAgamaRequestEntry').getValue(),

		KD_AGAMA: Ext.getCmp('TxtTmpAgama_viDataKunjunganRWI').getValue(),
		KD_PEKERJAAN: Ext.getCmp('TxtTmpPekerjaan_viDataKunjunganRWI').getValue(),
		KD_PENDIDIKAN: Ext.getCmp('TxtTmpPendidikan_viDataKunjunganRWI').getValue(),
        TMPPARAM: '0',
		KD_KELURAHAN: kelurahanpasien,
		AYAHPASIEN: Ext.getCmp('TxtWindowPopup_Nama_ayah_viDataKunjunganRWI').getValue(),
		IBUPASIEN: Ext.getCmp('TxtWindowPopup_NamaIbu_viDataKunjunganRWI').getValue(),
		NAMA_KELUARGA: Ext.getCmp('TxtWindowPopup_Nama_keluarga_viDataKunjunganRWI').getValue(),
		Emailpasien: Ext.getCmp('TxtWindowPopup_Email_viDataKunjunganRWI').getValue(),
		HPpasien: Ext.getCmp('TxtWindowPopup_HP_viDataKunjunganRWI').getValue()
	}
    return params_viDataKunjunganRWI
}
/**
 *	Function : datasave_dataPasiDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk menyimpan dan mengedit data entry 
 */

function getParamRequest()
{
    var params =
            {
                Table: 'Vi_ViewDataPasien',
                NOMEDREC: Ext.get('TxtWindowPopup_NO_RM_viDataKunjunganRWI').getValue(),
                TGLMASUK: Ext.get('TxtWindowPopup_KD_CUSTOMER_viDataKunjunganRWI').getValue(),
                URUTMASUK: Ext.get('TxtWindowPopup_Urut_Masuk_viDataKunjunganRWI').getValue(),
                KDUNIT: Ext.get('TxtWindowPopup_KD_PASIEN_viDataKunjunganRWI').getValue(),
                KET: Ext.get('TxtHistoriDeleteDataKunjunganRWI').getValue(),
                TMPPARAM: '1'
            };
    return params
}
;

function datasavehistori_viDataKunjunganRWI()
{
    if (ValidasiEntryHistori_viDataKunjunganRWI('Simpan Data', true) === 1)
    {
        Ext.Ajax.request
                (
                        {
                            url: baseURL + "index.php/main/CreateDataObj",
                            params: getParamRequest(),
                            success: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfo_viDataKunjunganRWI('Data transaksi pasien berhasil di hapus', 'Edit Data');
                                    DataRefresh_viDataKunjunganRWI(getCriteriaFilterGridDataView_viDataKunjunganRWI());
                                    //tmphasil = true;
                                } else
                                {
                                    ShowPesanError_viDataKunjunganRWI('Data transaksi pasien tidak berhasil di hapus' + cst.pesan, 'Edit Data');
                                    //tmphasil = false;
                                }
                            }
                        }
                )
    } else
    {
        if (mBol === true)
        {
            return false;
        }
    }
}

function datasave_dataPasiDataKunjunganRWI(mBol){
    var cbo_kabupaten = Ext.getCmp('cboKabupaten_editDataKunjunganRWI').getValue();
    var cbo_kecamatan = Ext.getCmp('cboKecamatan_editDataKunjunganRWI').getValue();    
    var cbo_kelurahan = Ext.getCmp('cbokelurahan_editDataKunjunganRWI').getValue();    
    if (cbo_kabupaten.length == 0 || cbo_kecamatan.length == 0 || cbo_kelurahan.length == 0) {
        ShowPesanWarning_viDataKunjunganRWI('Cek kembali alamat lengkap pasien', 'Peringatan');
    }else{
        if (ValidasiEntry_viDataKunjunganRWI('Simpan Data', true) == 1){
            Ext.Ajax.request({
    			url: baseURL + "index.php/main/CreateDataObj",
    			params: dataparam_datapasviDataKunjunganRWI(),
    			failure: function (o){
    				ShowPesanError_viDataKunjunganRWI('Data tidak berhasil diupdate. Hubungi admin! ', 'Edit Data');
    			},
    			success: function (o){
    				var cst = Ext.decode(o.responseText);
    				if (cst.success === true){
    					ShowPesanInfo_viDataKunjunganRWI('Data berhasil disimpan', 'Edit Data');
    					DataRefresh_viDataKunjunganRWI(getCriteriaFilterGridDataView_viDataKunjunganRWI());
    				} else if (cst.success === false && cst.pesan === 0){
    					ShowPesanError_viDataKunjunganRWI('Data tidak berhasil diupdate ', 'Edit Data');
    				} else{
    					ShowPesanError_viDataKunjunganRWI('Data tidak berhasil diupdate. ', 'Edit Data');
    				}
    			}
    		});
        } else{
            if (mBol === true){
                return false;
            }
        }
    }
}
// End Function datasave_dataPasiDataKunjunganRWI # --------------

/**
 *	Function : datadelete_viDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk menghapus data entry 
 */

function datadelete_viDataKunjunganRWI()
{
    //}
}
// End Function datadelete_viDataKunjunganRWI # --------------

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
 *	Function : dataGrid_viDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
 */

function dataGrid_viDataKunjunganRWI(mod_id_viDataKunjunganRWI)
{/*
 
 
 Public $;
 public $;
 public $;
 public $; */

    var FieldGrid_viDataKunjunganRWI =
            [
                'KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'TGL_LAHIR_PASIEN', 'AGAMA',
                'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN', 'PENDIDIKAN', 'PEKERJAAN',
                'KD_UNIT', 'TGL_MASUK', 'URUT_MASUK', 'KD_KABUPATEN', 'KABUPATEN', 'KECAMATAN', 'KD_KECAMATAN', 'PROPINSI',
                'KD_PROPINSI', 'KELURAHAN', 'EMAIL', 'HP', 'AYAH', 'IBU',
                'KD_AGAMA', 'KD_PEKERJAAN', 'KD_PENDIDIKAN', 'TELEPON', 'NAMA_UNIT', 'NO_ASURANSI'
            ];
    // Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSourceGrid_viDataKunjunganRWI = new WebApp.DataStore({
        fields: FieldGrid_viDataKunjunganRWI

    });



    // Pemangilan Function untuk memangil data yang akan ditampilkan # --------------

    // Grid Data Pasien # --------------
    var GridDataView_viDataKunjunganRWI = new Ext.grid.EditorGridPanel({
        xtype       : 'editorgrid',
        store       : dataSourceGrid_viDataKunjunganRWI,
        title       : '',
        // anchor      : '100% 40%',
        columnLines : true,
        border      : false,
		flex:1,
        plugins     : [new Ext.ux.grid.FilterRow()],
        selModel    : new Ext.grid.RowSelectionModel
                        // Tanda aktif saat salah satu baris dipilih # --------------
                        ({
                            singleSelect: true,
                            listeners:
                                {
                                    rowselect: function (sm, row, rec)
                                    {
                                        //rowSelectedGridDataView_viDataKunjunganRWI = undefined;
    									// Ext.getCmp('btnGantiKekompokPasien').disable();
    									// Ext.getCmp('btnGantiDokter').disable();
                                        // Ext.getCmp('btnUnitPasien').disable();
                                        Ext.getCmp('btnHpsDataKunjunganRWI').disable();
    									Ext.getCmp('btnGantiTanggalMasuk').disable();
                                        rowSelectedGridDataView_viDataKunjunganRWI = dataSourceGrid_viDataKunjunganRWI.getAt(row);
                                    }
                                }
                        }),
                        // Proses eksekusi baris yang dipilih # --------------
                        listeners:
                        {
                            // Function saat ada event double klik maka akan muncul form view # --------------
                            rowdblclick: function (sm, ridx, cidx)
                            {
                                rowSelectedGridDataView_viDataKunjunganRWI = dataSourceGrid_viDataKunjunganRWI.getAt(ridx);
                                if (rowSelectedGridDataView_viDataKunjunganRWI != undefined)
                                {
                                    setLookUpGridDataView_viDataKunjunganRWI(rowSelectedGridDataView_viDataKunjunganRWI.data);
                                }else{
                                    ShowPesanWarning_viDataKunjunganRWI('Silahkan pilih Data Pasien ', 'Edit Data');
                                }
                            },
                            rowclick: function (sm, ridx, cidx)
                            {
                                rowSelectedGridDataView_viDataKunjunganRWI.data.KD_PASIEN;
                                vNoAsuransi = rowSelectedGridDataView_viDataKunjunganRWI.data.NO_ASURANSI;
                                RefreshDatahistoriKunjunganPasienRWI(rowSelectedGridDataView_viDataKunjunganRWI.data.KD_PASIEN);
								
                                //rowSelectedGridDataView_viDataKunjunganRWI.data.KD_PASIEN;

                            }
                        },
                        /**
                         *	Mengatur tampilan pada Grid Data Pasien
                         *	Terdiri dari : Judul, Isi dan Event
                         *	Isi pada Grid di dapat dari pemangilan dari Net.
                         *	dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSourceGrid_viDataKunjunganRWI
                         *	didapat dari Function DataRefresh_viDataKunjunganRWI()
                         */
                        colModel: new Ext.grid.ColumnModel
                                        (
                                                [
                                                    new Ext.grid.RowNumberer(),
                                                    {
                                                        id: 'colNRM_viDaftar',
                                                        header: 'No.Medrec',
                                                        dataIndex: 'KD_PASIEN',
                                                        sortable: true,
                                                        width: 20
//						filter:
//						{
//							type: 'int'
//						}
                                                    },
                                                    {
                                                        id: 'colNMPASIEN_viDaftar',
                                                        header: 'Nama',
                                                        dataIndex: 'NAMA',
                                                        sortable: true,
                                                        width: 40
//						filter:
//						{}
                                                    },
                                                    {
                                                        id: 'colALAMAT_viDaftar',
                                                        header: 'Alamat',
                                                        dataIndex: 'ALAMAT',
                                                        width: 60,
                                                        sortable: true
//						filter: {}
                                                    },
                                                    {
                                                        id: 'colTglKunj_viDaftar',
                                                        header: 'Tgl Lahir Pasien',
                                                        dataIndex: 'TGL_LAHIR_PASIEN',
                                                        width: 20,
                                                        sortable: true,
                                                        //format: 'd/M/Y',
//						filter: {},
														/* 
                                                        renderer: function (v, params, record)
                                                        {
                                                            return ShowDate(record.data.TGL_LAHIR_PASIEN);
                                                        }  */
                                                    }

                                                    //-------------- ## --------------
                                                ]
                                                ),
                                // Tolbar ke Dua # --------------
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            id: 'ToolbarGridDataView_viDataKunjunganRWI',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Edit Data',
                                                            iconCls: 'Edit_Tr',
                                                            tooltip: 'Edit Data',
                                                            width: 100,
                                                            id: 'BtnEditGridDataView_viDataKunjunganRWI',
                                                            handler: function (sm, row, rec)
                                                            {
                                                                if (rowSelectedGridDataView_viDataKunjunganRWI != undefined)
                                                                {
                                                                    setLookUpGridDataView_viDataKunjunganRWI(rowSelectedGridDataView_viDataKunjunganRWI.data);
                                                                } else
                                                                {
                                                                    ShowPesanWarning_viDataKunjunganRWI('Silahkan pilih Data Pasien ', 'Edit Data');
                                                                }
                                                            }
                                                        }
                                                    ]
                                        },
                                bbar: bbar_paging(mod_name_viDataKunjunganRWI, slctCount_viDataKunjunganRWI, dataSourceGrid_viDataKunjunganRWI),
                                // End Button Bar Pagging # --------------
                                viewConfig:
                                {
                                    forceFit: true
                                }
    })
                    DataRefresh_viDataKunjunganRWI();

                    var top = new Ext.FormPanel(
                            {
                                labelAlign: 'top',
                                frame: true,
                                title: '',
                                bodyStyle: 'padding:5px 5px 0',
                                //width: 600,
                                items: [{
                                        layout: 'column',
                                        items:
                                                [
                                                    {
                                                        columnWidth: .3,
                                                        layout: 'form',
                                                        items:
                                                                [
                                                                    {
                                                                        columnWidth: .5,
                                                                        layout: 'form',
                                                                        items: [
                                                                            {
                                                                                xtype: 'textfield',
                                                                                fieldLabel: 'Alamat Pasien ',
                                                                                name: 'txtAlamatPasien_DataKunjunganRWI',
                                                                                enableKeyEvents: true,
                                                                                id: 'txtAlamatPasien_DataKunjunganRWI',
                                                                                anchor: '95%',
                                                                                listeners:
                                                                                        {
                                                                                            'keyup': function ()
                                                                                            {
                                                                                                var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                                DataRefresh_viDataKunjunganRWI(tmpkriteria);
																								
                                                                                            }
                                                                                        }
                                                                            }
                                                                        ]
                                                                    }
                                                                ]
                                                    }
                                                ]
                                    }]
                            });

                    // Kriteria filter pada Grid # --------------
                    var FrmFilterGridDataView_viDataKunjunganRWI = new Ext.Panel
                            (
                                    {
                                        title: NamaForm_viDataKunjunganRWI,
                                        iconCls: icons_viDataKunjunganRWI,
                                        id: mod_id_viDataKunjunganRWI,
                                        // region: 'center',
                                        layout: {
											type:'vbox',
											align:'stretch'
										},
                                        closable: true,
										// flex:1,
                                        border: false,
                                        // margins: '0 5 5 0',
                                        items: [
                                            {
                                                layout: 'form',
												// title:'Pencarian',
                                                bodyStyle: 'padding-top:2px',
                                                border: false,
												labelAlign:'right',
                                                items:
                                                        [
                                                            // {
                                                                // xtype: 'tbspacer',
                                                                // height: 3
                                                            // },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'No. Medrec / NIK',
																// labelWidth:200,
                                                                id: 'txtNoMedrec_DataKunjunganRWI',
                                                                anchor: '40%',
                                                                onInit: function () { },
                                                                listeners:
                                                                        {
                                                                            'specialkey': function ()
                                                                            {
                                                                                var tmpNoMedrec = Ext.get('txtNoMedrec_DataKunjunganRWI').getValue()
                                                                                // if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                                                {
                                                                                    if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                                                                    {

                                                                                        var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrec_DataKunjunganRWI').getValue())
                                                                                        Ext.getCmp('txtNoMedrec_DataKunjunganRWI').setValue(tmpgetNoMedrec);
                                                                                        var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                        DataRefresh_viDataKunjunganRWI(tmpkriteria);
																						RefreshDatahistoriKunjunganPasienRWI(tmpgetNoMedrec);


                                                                                    } else
                                                                                    {
                                                                                        if (tmpNoMedrec.length === 10)
                                                                                        {
                                                                                            tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                            DataRefresh_viDataKunjunganRWI(tmpkriteria);
																							RefreshDatahistoriKunjunganPasienRWI(Ext.get('txtNoMedrec_DataKunjunganRWI').getValue());
																							Ext.Ajax.request
																							(
																									{
																										url: baseURL + "index.php/rawat_jalan/viewkunjungan/ceknikpasien2",
																										params: {part_number_nik: Ext.get('txtNoMedrec_DataKunjunganRWI').getValue()},
																										failure: function (o)
																										{
																											ShowPesanError_viDataKunjunganRWI('Pencarian error ! ', 'Data Pasien');
																										},
																										success: function (o)
																										{
																											var cst = Ext.decode(o.responseText);
																											if (cst.success === true)
																											{
																												Ext.getCmp('txtNoMedrec_DataKunjunganRWI').setValue(cst.nomedrec);
																											}
																											else
																											{
																												//Ext.getCmp('txtNoMedrec_DataKunjunganRWI').setValue('');
																											}
																										}
																									}
																							) 
                                                                                        } else
                                                                                        {
                                                                                            Ext.getCmp('txtNoMedrec_DataKunjunganRWI').setValue('')
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                        }
                                                            },
                                                            // {
                                                                // xtype: 'tbspacer',
                                                                // height: 3
                                                            // },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Pasien',
                                                                name: 'txtNamaPasien_DataKunjunganRWI',
                                                                id: 'txtNamaPasien_DataKunjunganRWI',
                                                                enableKeyEvents: true,
                                                                anchor: '40%',
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                if (Ext.EventObject.getKey() === 13)
                                                                                {
                                                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    DataRefresh_viDataKunjunganRWI(tmpkriteria);
																					console.log(dataSourceGrid_viDataKunjunganRWI.getCount());
																					var x=dataSourceGrid_viDataKunjunganRWI.getCount();
																					if (x===1)
																					{
																						RefreshDatahistoriKunjunganPasienRWI(Ext.get('txtNoMedrec_DataKunjunganRWI').getValue());
																					}
																					else
																					{
																						RefreshDatahistoriKunjunganPasienRWI();
																					}
                                                                                }
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Alamat',
                                                                name: 'txtAlamatPasien_DataKunjunganRWI',
                                                                id: 'txtAlamatPasien_DataKunjunganRWI',
                                                                enableKeyEvents: true,
                                                                anchor: '60%',
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                if (Ext.EventObject.getKey() === 13)
                                                                                {
                                                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    DataRefresh_viDataKunjunganRWI(tmpkriteria);
                                                                                    // console.log(dataSourceGrid_viDataKunjunganRWI.getCount());
                                                                                    var x=dataSourceGrid_viDataKunjunganRWI.getCount();
                                                                                    if (x===1)
                                                                                    {
                                                                                        RefreshDatahistoriKunjunganPasienRWI(Ext.get('txtNoMedrec_DataKunjunganRWI').getValue());
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        RefreshDatahistoriKunjunganPasienRWI();
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Keluarga',
                                                                name: 'txtKeluargaPasien_DataKunjunganRWI',
                                                                id: 'txtKeluargaPasien_DataKunjunganRWI',
                                                                enableKeyEvents: true,
                                                                anchor: '60%',
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                if (Ext.EventObject.getKey() === 13)
                                                                                {
                                                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    DataRefresh_viDataKunjunganRWI(tmpkriteria);
                                                                                    // console.log(dataSourceGrid_viDataKunjunganRWI.getCount());
                                                                                    var x=dataSourceGrid_viDataKunjunganRWI.getCount();
                                                                                    if (x===1)
                                                                                    {
                                                                                        RefreshDatahistoriKunjunganPasienRWI(Ext.get('txtNoMedrec_DataKunjunganRWI').getValue());
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        RefreshDatahistoriKunjunganPasienRWI();
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                            },
                                                            // {
                                                                // xtype: 'tbspacer',
                                                                // height: 3
                                                            // },
                                                            //getItemPanelcombofilter(),
                                                            //	getItemPaneltgl_filter()
                                                        ]},
                                            GridDataView_viDataKunjunganRWI,
                                            // {
                                                // xtype: 'tbspacer',
                                                // height: 7
                                            // }, 
											gridKunjunganpasien_editdata()]

                                    }
                            )
                    return FrmFilterGridDataView_viDataKunjunganRWI;

                    //-------------- # End form pencarian # --------------
                }
// End Function dataGrid_viDataKunjunganRWI # --------------

        function gridKunjunganpasien_editdata()
        {
            var fldDetail = ['KD_PASIEN', 'TGL_MASUK', 'NAMA_UNIT', 'URUT', 'DOKTER', 'KD_UNIT', 'JAM', 'TGL_KELUAR', 'CUST', 'NO_TRANSAKSI', 'CO_STATUS', 'NO_SEP', 'KD_DOKTER', 'KD_KASIR', 'KD_CUSTOMER'];
            dskunjunganpasienrwi = new WebApp.DataStore({fields: fldDetail})
            var gridDataKunjunganRWIrwj = new Ext.grid.EditorGridPanel({
                title: 'Kunjungan',
                stripeRows: true,
				height:200,
                store: dskunjunganpasienrwi,
                border: false,
                autoScroll: true,
                columnLines: true,
                frame: false,
                // anchor: '100% 35%',
                sm: new Ext.grid.RowSelectionModel
                ({
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function (sm, row, rec) {
                            cellSelecteddeskripsi_DataKunjunganRWI = dskunjunganpasienrwi.getAt(row);
							
							vCo_status = cellSelecteddeskripsi_DataKunjunganRWI.data.CO_STATUS;

							/* 
								PERBARUAN VARIABLE YANG DIKIRIM UNTUK GANTI KELOMPOK PASIEN
								OLEH 	: HADAD
								TANGGAL : 2016 - 12 - 31
							*/

							vNamaUnit 	= cellSelecteddeskripsi_DataKunjunganRWI.data.NAMA_UNIT;
							vNamaDokter = cellSelecteddeskripsi_DataKunjunganRWI.data.DOKTER;
							vKdDokter 	= cellSelecteddeskripsi_DataKunjunganRWI.data.DOKTER;
							selectKdCustomer = cellSelecteddeskripsi_DataKunjunganRWI.data.KD_CUSTOMER; 
							vKdKasir 	= cellSelecteddeskripsi_DataKunjunganRWI.data.KD_KASIR;
							vNoTransaksi= cellSelecteddeskripsi_DataKunjunganRWI.data.NO_TRANSAKSI;
							vCustomer 	= cellSelecteddeskripsi_DataKunjunganRWI.data.CUST;
							vNoSEP 		= cellSelecteddeskripsi_DataKunjunganRWI.data.NO_SEP;
							vKd_Pasien	= cellSelecteddeskripsi_DataKunjunganRWI.data.KD_PASIEN;
							vTanggal	= cellSelecteddeskripsi_DataKunjunganRWI.data.TGL_MASUK;
							vKdUnit		= cellSelecteddeskripsi_DataKunjunganRWI.data.KD_UNIT;
							vUrutMasuk	= cellSelecteddeskripsi_DataKunjunganRWI.data.URUT;
                            console.log(vKdUnit.substr(0,1));
                            if (vKdUnit.substr(0,1) == 1 || vKdUnit.substr(0,1) == '1') {
                                if(vCo_status == "Belum" || vCo_status === false ){
                                    // Ext.getCmp('btnGantiKekompokPasien').enable();
                                    // Ext.getCmp('btnGantiDokter').enable();
                                    // Ext.getCmp('btnUnitPasien').enable();
                                    Ext.getCmp('btnHpsDataKunjunganRWI').enable();
                                    Ext.getCmp('btnGantiTanggalMasuk').enable();
                                }else{
                                    // Ext.getCmp('btnGantiKekompokPasien').disable();                                                      
                                    // Ext.getCmp('btnUnitPasien').disable();
                                    // Ext.getCmp('btnGantiDokter').disable();                                                     
                                    Ext.getCmp('btnHpsDataKunjunganRWI').disable();                                                     
                                    Ext.getCmp('btnGantiTanggalMasuk').disable();                                                     
                                }
                            }else{                                            
                                Ext.getCmp('btnHpsDataKunjunganRWI').disable();    
                                Ext.getCmp('btnGantiTanggalMasuk').disable();    
                            }
							//loaddatastoredokter();
							cekDetailTransaksi();
							/* ------------------------------------------ END --------------------- */
                            CurrentHistoryDataKunjunganRWI.row = row;
                            CurrentHistoryDataKunjunganRWI.data = cellSelecteddeskripsi_DataKunjunganRWI;
                            DataKunjunganRWIvariable.kd_pasien = CurrentHistoryDataKunjunganRWI.data.data.KD_PASIEN;
                            DataKunjunganRWIvariable.urut = CurrentHistoryDataKunjunganRWI.data.data.URUT;
                            DataKunjunganRWIvariable.tglkunjungan = CurrentHistoryDataKunjunganRWI.data.data.TGL_MASUK;
                            DataKunjunganRWIvariable.kd_unit = CurrentHistoryDataKunjunganRWI.data.data.KD_UNIT;
							
                        }
                    }
				}),
				tbar: [
					{
						id: 'btnHpsDataKunjunganRWI', 
						text: 'Hapus Kunjungan', 
						tooltip: 'Hapus Kunjungan', 
						iconCls: 'RemoveRow',
						disabled:true,
						handler: function () {
							var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan kunjungan:', function(btn, combo){
								if (btn == 'ok')
								{
									if (combo!='') {
										var params = {
											kd_unit: DataKunjunganRWIvariable.kd_unit,
											Tglkunjungan: DataKunjunganRWIvariable.tglkunjungan,
											Kodepasein: DataKunjunganRWIvariable.kd_pasien,
											urut: DataKunjunganRWIvariable.urut,
											alasan:combo
										};
										loadMask.show();
										Ext.Ajax.request
										({
											url: baseURL + "index.php/rawat_inap/viewdatakunjunganpasienrwi/deletekunjungan_Revisi",
											params: params,
											failure: function (o)
											{
												loadMask.hide();
												ShowPesanError_viDataKunjunganRWI('Error proses database', 'Hapus Data');
											},
											success: function (o)
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true)
												{
													RefreshDatahistoriKunjunganPasienRWI(DataKunjunganRWIvariable.kd_pasien);
													ShowPesanInfo_viDataKunjunganRWI('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
												} else
												{
													if (cst.pesan == '') {
														ShowPesanError_viDataKunjunganRWI('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
													}else{
														ShowPesanWarning_viDataKunjunganRWI(cst.pesan, 'Hapus Data Kunjungan');
													}
												};
											}
										});
										loadMask.hide();
									} else {
										ShowPesanWarning_viDataKunjunganRWI('Silahkan isi alasan terlebih dahulu!','Keterangan');

									}
								}

							});
						}
					}, {								
						id            : 'btnGantiTanggalMasuk', 
                        text        : 'Ganti Tanggal Masuk', 
                        tooltip     : 'Ganti Tanggal Masuk', 
                        iconCls     : 'gantipasien', 
                        disabled    :true,
						handler: function () {
                            setLookUpGantiTanggal_viDataKunjunganRWI();
						}
					},/*{								
						id: 'btnGantiDokter', text: 'Ganti Dokter', tooltip: 'Ganti dokter pasien', iconCls: 'gantipasien', disabled:true,
						handler: function () {
							//Button Ganti Dokter;
							panelActiveDataKunjunganRWI = 1;
							loaddatastoredokter();
							GantiDokterPasienLookUp_rwj();
						}
					},{								
						id: 'btnUnitPasien', text: 'Ganti Unit', tooltip: 'Ganti unit pasien', iconCls: 'gantipasien', disabled:true,
						handler: function () {
							//Button Ganti Unit;
							panelActiveDataKunjunganRWI = 2;
							//loaddatastoredokter();
							//console.log(statusCekDetail);
							if(statusCekDetail == false){
								KonsultasiDataKunjunganRWILookUp_rwj();
							}else{
								//console.log("kd_kasir = "+vKdKasir+" - no_transaksi = "+vNoTransaksi);
								ShowPesanInfo_viDataKunjunganRWI("Tidak dapat ganti unit karena sudah ada produk atau tindakan", "Failed");
							}
						}
					} */
				],
				cm: kunjunganpasiencolummodel(),
				viewConfig: {forceFit: true}
			});
            return gridDataKunjunganRWIrwj;
        };

        function kunjunganpasiencolummodel()
        {
            return new Ext.grid.ColumnModel(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKdTransaksi', 
						header: 'No. Transaksi', 
						dataIndex: 'NO_TRANSAKSI', 
						width: 100, 
						menuDisabled: true, 
						hidden: false
					},
					{
						id: 'colenamunit', 
						header: 'Unit', 
						dataIndex: 'NAMA_UNIT', 
						width: 80, 
						hidden: false
					},
					{
						id: 'coldok', 
						header: 'Dokter', 
						dataIndex: 'DOKTER', 
						width: 150, 
						menuDisabled: true, 
						hidden: false
					},
					{
						id: 'colkelpas', 
						header: 'Kelompok Pasien', 
						dataIndex: 'CUST', 
						width: 150, 
						menuDisabled: true, 
						hidden: false
					},
					{
						id: 'colTGlMasuk', 
						header: 'Tgl. Masuk', 
						dataIndex: 'TGL_MASUK', 
						menuDisabled: true, 
						width: 100
					},
					{
						id: 'coletglkeluar',
						header: 'Tgl. Keluar',
						menuDisabled: true,
						dataIndex: 'TGL_KELUAR'

					},
					{
						id: 'colStatHistory',
						header: 'Jam Masuk',
						menuDisabled: true,
						dataIndex: 'JAM',
					},
					{
						id: 'colStatHistory',
						header: 'NO SEP',
						menuDisabled: true,
						dataIndex: 'NO_SEP',
					},
					{
						id: 'coleurutmasuk',
						header: 'urut Masuk',
						menuDisabled: true,
						dataIndex: 'URUT',
						hidden:true

					},
					{
						id: 'colStatusTransaksi',
						header: 'Status Transaksi',
						menuDisabled: true,
						dataIndex: 'CO_STATUS',
						//hidden:true
					}
				]
			)
        };
		
function RefreshDatahistoriKunjunganPasienRWI(kd_pasien) {
	 var strKriteriaKasirrwj = '';
	strKriteriaKasirrwj = ' k.kd_pasien= ~' + kd_pasien + '~';
	dskunjunganpasienrwi.load
	({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'tgl_transaksi',
			Sortdir: 'ASC',
			target: 'ViewkunjunganPasien',
			param: strKriteriaKasirrwj
		}
	});
	return dskunjunganpasienrwi; 
	/*Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/rawat_inap/viewdatakunjunganpasienrwi/readkunjunganpasienrwi",
			params: {
				kd_pasien:kd_pasien
			},
			failure: function(o)
			{
				// ShowPesanError_viDataKunjunganRWI('Hubungi Admin! Error get data history pasien rwi.', 'Error');
			},	
			success: function(o) 
			{
				dskunjunganpasienrwi.removeAll();
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dskunjunganpasienrwi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dskunjunganpasienrwi.add(recs);
				}
			}
		}
		
	)*/
};
        /**
         *	Function : setLookUpGridDataView_viDataKunjunganRWI
         *	
         *	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
         */
function setLookUpGridDataView_viDataKunjunganRWI(rowdata){
    var lebar = 819; // Lebar Form saat PopUp
    setLookUpsGridDataView_viDataKunjunganRWI = new Ext.Window({
        id: 'setLookUpsGridDataView_viDataKunjunganRWI',
        title: NamaForm_viDataKunjunganRWI,
        closeAction: 'destroy',
        autoScroll: true,
        width: 640,
        height: 425,
        resizable: false,
        border: false,
        plain: true,
        layout: 'fit',
        iconCls: icons_viDataKunjunganRWI,
        modal: true,
        items: getFormItemEntryGridDataView_viDataKunjunganRWI(lebar, rowdata),
        listeners:{
            activate: function (){
            },
            afterShow: function (){
                this.activate();
            },
            deactivate: function (){
                rowSelectedGridDataView_viDataKunjunganRWI = undefined;
                DataRefresh_viDataKunjunganRWI(getCriteriaFilterGridDataView_viDataKunjunganRWI());
            }
        }
    });
    setLookUpsGridDataView_viDataKunjunganRWI.show();
    if (rowdata == undefined){
        dataaddnew_viDataKunjunganRWI();
    } else{
        datainit_viDataKunjunganRWI(rowdata)
    }
}

function setLookUpGantiTanggal_viDataKunjunganRWI(){
    console.log(cellSelecteddeskripsi_DataKunjunganRWI);
	var lebar = 400; // Lebar Form saat PopUp
	setLookUpsGantiTanggal_viDataKunjunganRWI = new Ext.Window({
		id: 'setLookUpGantiTanggal_viDataKunjunganRWI',
		title: "Ganti Tanggal Masuk",
		closeAction: 'destroy',
		autoScroll: true,
		width: lebar,
		height: 240,
		resizable: false,
		border: false,
		plain: true,
		layout: 'fit',
		iconCls: icons_viDataKunjunganRWI,
		modal: true,
		items: [
            {
                xtype: 'panel',
                layout:'form',
                margin:true,
                border:false,
                bodyStyle : 'padding:10px;',
                labelAlign: 'right',
                width     : '100%',
                anchor : '100%',
                items:[
                    {
                        xtype     : 'textfield',
                        name      : 'txt_GantiTanggal',
                        id        : 'txt_GantiTanggal',
                        width     : '100%',
                        disable   : true,
                        readOnly  : true,
                        value     : cellSelecteddeskripsi_DataKunjunganRWI.data.TGL_MASUK,
                        fieldLabel: 'Tgl Masuk Lama',
                    },{
                        xtype     : 'textfield',
                        name      : 'txt_GantiJam',
                        id        : 'txt_GantiJam',
                        width     : '100%',
                        disable   : true,
                        readOnly  : true,
                        value     : cellSelecteddeskripsi_DataKunjunganRWI.data.JAM,
                        fieldLabel: 'Jam Masuk Lama',
                    },{
                        xtype     : 'datefield',
                        name      : 'txt_GantiTanggal_Baru',
                        id        : 'txt_GantiTanggal_Baru',
                        width     : 200,
                        fieldLabel: 'Tgl Masuk Baru',
                    },{
                        xtype     : 'textfield',
                        name      : 'txt_GantiJam_Baru',
                        id        : 'txt_GantiJam_Baru',
                        width     : '100%',
                        value     : cellSelecteddeskripsi_DataKunjunganRWI.data.JAM,
                        fieldLabel: 'Jam Masuk Baru',
                    },{
                        xtype     : 'datefield',
                        name      : 'txt_GantiTanggal_Baru_verifikasi',
                        id        : 'txt_GantiTanggal_Baru_verifikasi',
                        width     : 200,
                        hidden    : true,
                        fieldLabel: 'Tgl Masuk Baru Verifikasi',
                    },
                ]
            }
        ],
        bbar: [
        {
            id: 'btnSimpangantiTanggal_DaftarPasienRWI', 
            text: 'Simpan', 
            tooltip: 'Simpan', 
            iconCls: 'save',
            align   : 'right',
            handler : function(){
                var tmp_tgl_masuk                 = Ext.getCmp('txt_GantiTanggal').getValue();
                var tmp_jam_masuk                 = Ext.getCmp('txt_GantiJam').getValue();
                var tmp_tgl_masuk_baru            = Ext.getCmp('txt_GantiTanggal_Baru').getValue();
                var tmp_jam_masuk_baru            = Ext.getCmp('txt_GantiJam_Baru').getValue();
                // var tmp_tgl_masuk_baru_verifikasi = Ext.getCmp('txt_GantiTanggal_Baru_verifikasi').getValue();
                if (tmp_tgl_masuk_baru.length == 0) {
                    ShowPesanWarning_viDataKunjunganRWI("Tanggal baru harap diisi !", "Informasi");
                /*}else if (tmp_tgl_masuk_baru_verifikasi.length == 0) {
                    ShowPesanWarning_viDataKunjunganRWI("Tanggal baru verifikasi harap diisi !", "Informasi");
                }else if(tmp_tgl_masuk_baru_verifikasi.format('Y-m-d') != tmp_tgl_masuk_baru.format('Y-m-d')){
                    console.log(tmp_tgl_masuk_baru_verifikasi.format('Y-m-d'));
                    console.log(tmp_tgl_masuk_baru.format('Y-m-d'));
                    ShowPesanWarning_viDataKunjunganRWI("Tanggal baru tidak cocok !", "Informasi");*/
                }else{
                    Ext.Ajax.request({
                        url: baseURL +  "index.php/rawat_inap/data_kunjungan/update_tanggal_masuk",   
                        params: {
                            kd_pasien       : cellSelecteddeskripsi_DataKunjunganRWI.data.KD_PASIEN,
                            kd_unit         : cellSelecteddeskripsi_DataKunjunganRWI.data.KD_UNIT,
                            tgl_masuk       : tmp_tgl_masuk_baru.format('Y-m-d'),
                            tgl_masuk_lama  : tmp_tgl_masuk,
                            jam_masuk       : tmp_jam_masuk_baru,
                            jam_masuk_lama  : tmp_jam_masuk,
                        },
                        failure: function(o)
                        {
                            ShowPesanWarning_viDataKunjunganRWI('Simpan kelompok pasien gagal', 'Gagal');
                        },  
                        success: function(o){
                            var cst = Ext.decode(o.responseText);
                            if (cst.status === true){
                                Ext.MessageBox.show({
                                    title: "Informasi",
                                    msg: "Berhasil disimpan",
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.INFO,
                                    width: 250
                                });
                                RefreshDatahistoriKunjunganPasienRWI(cellSelecteddeskripsi_DataKunjunganRWI.data.KD_PASIEN);
                            }else{
                                ShowPesanWarning_viDataKunjunganRWI('Gagal simpan', 'Gagal');
                            }
                        }
                    });
                }
            }
        }]
	});
	setLookUpsGantiTanggal_viDataKunjunganRWI.show();
}
// End Function setLookUpGridDataView_viDataKunjunganRWI # --------------

/**
 *	Function : getFormItemEntryGridDataView_viDataKunjunganRWI
 *	
 *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
 *	Di pangil dari Function setLookUpGridDataView_viDataKunjunganRWI
 */
function getFormItemEntryGridDataView_viDataKunjunganRWI(lebar, rowdata)
{
	var pnlFormDataWindowPopup_viDataKunjunganRWI = new Ext.FormPanel
			(
					{
						title: '',
						// region: 'center',
						fileUpload: true,
						margin:true,
						// layout: 'anchor',
						// padding: '8px',
						bodyStyle: 'padding-top:5px;',
						// Tombol pada tollbar Edit Data Pasien
						tbar: {
							xtype: 'toolbar',
							items:
									[
										{
											xtype: 'button',
											text: 'Save',
											id: 'btnSimpanWindowPopup_viDataKunjunganRWI',
											iconCls: 'save',
											handler: function (){
												datasave_dataPasiDataKunjunganRWI(false); //1.1
												DataRefresh_viDataKunjunganRWI(getCriteriaFilterGridDataView_viDataKunjunganRWI());
											}
										},
										//-------------- ## --------------
										{
											xtype: 'tbseparator'
										},
										//-------------- ## --------------
										{
											xtype: 'button',
											text: 'Save & Close',
											id: 'btnSimpanExitWindowPopup_viDataKunjunganRWI',
											iconCls: 'saveexit',
											handler: function ()
											{
												var x = datasave_dataPasiDataKunjunganRWI(false); //1.2
												DataRefresh_viDataKunjunganRWI(getCriteriaFilterGridDataView_viDataKunjunganRWI());
												if (x === undefined)
												{
													setLookUpsGridDataView_viDataKunjunganRWI.close();
												}
											}
										},
										//-------------- ## --------------
										{
											xtype: 'tbseparator'
										},
										//-------------- ## --------------
									]
						},
						//-------------- #items# --------------
						items:
								[
									// Isian Pada Edit Data Pasien
									{
										xtype: 'panel',
										title: '',
										layout:'form',
										margin:true,
										border:false,
										labelAlign: 'right',
										width: 610,
										items:
												[//---------------# Penampung data untuk fungsi update # ---------------
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpAgama_viDataKunjunganRWI',
														name: 'TxtTmpAgama_viDataKunjunganRWI',
														readOnly: true,
														hidden: true
													},
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpPekerjaan_viDataKunjunganRWI',
														name: 'TxtTmpPekerjaan_viDataKunjunganRWI',
														readOnly: true,
														hidden: true
													},
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpPendidikan_viDataKunjunganRWI',
														name: 'TxtTmpPendidikan_viDataKunjunganRWI',
														readOnly: true,
														hidden: true
													},
													//------------------------ end ---------------------------------------------								
													//-------------- # Pelengkap untuk kriteria ke Net. # --------------
													{
														xtype: 'textfield',
														fieldLabel: 'KD_CUSTOMER',
														id: 'TxtWindowPopup_KD_CUSTOMER_viDataKunjunganRWI',
														name: 'TxtWindowPopup_KD_CUSTOMER_viDataKunjunganRWI',
														readOnly: true,
														hidden: true
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'URUT_MASUK',
														id: 'TxtWindowPopup_Urut_Masuk_viDataKunjunganRWI',
														name: 'TxtWindowPopup_Urut_Masuk_viDataKunjunganRWI',
														readOnly: true,
														hidden: true
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'KD_PASIEN',
														id: 'TxtWindowPopup_KD_PASIEN_viDataKunjunganRWI',
														name: 'TxtWindowPopup_KD_PASIEN_viDataKunjunganRWI',
														readOnly: true,
														hidden: true
													},
													//-------------- # End Pelengkap untuk kriteria ke Net. # --------------
													{
														xtype: 'textfield',
														fieldLabel: 'No. MedRec',
														id: 'TxtWindowPopup_NO_RM_viDataKunjunganRWI',
														name: 'TxtWindowPopup_NO_RM_viDataKunjunganRWI',
														emptyText: 'No. MedRec',
														labelSeparator: '',
														readOnly: true,
														flex: 1,
														width: 195
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'Nama',
														id: 'TxtWindowPopup_NAMA_viDataKunjunganRWI',
														name: 'TxtWindowPopup_NAMA_viDataKunjunganRWI',
														emptyText: 'Nama',
														labelSeparator: '',
														flex: 1,
														anchor: '100%'
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Tempat Lahir',
														labelSeparator: '',
														anchor: '100%',
														width: 250,
														items:
																[
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_TEMPAT_LAHIR_viDataKunjunganRWI',
																		name: 'TxtWindowPopup_TEMPAT_LAHIR_viDataKunjunganRWI',
																		emptyText: 'Tempat Lahir',
																		flex: 1,
																		width: 195
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'tbspacer',
																		width: 17,
																		height: 23
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Tgl. Lahir',
																		style: {'text-align': 'right'},
																		id: '',
																		name: '',
																		flex: 1,
																		width: 58
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'datefield',
																		fieldLabel: 'Tanggal',
																		id: 'DateWindowPopup_TGL_LAHIR_viDataKunjunganRWI',
																		name: 'DateWindowPopup_TGL_LAHIR_viDataKunjunganRWI',
																		width: 198,
																		format: 'd/m/Y',
                                                                        listeners:{
                                                                            'specialkey' : function(){                      
                                                                                if (Ext.EventObject.getKey() == 9 ||  Ext.EventObject.getKey() == 13){                 
                                                                                    var tmptanggal = Ext.get('DateWindowPopup_TGL_LAHIR_viDataKunjunganRWI').getValue();
                                                                                    Ext.Ajax.request({
                                                                                        url: baseURL + "index.php/main/GetUmur",
                                                                                        params: {
                                                                                            TanggalLahir: Ext.getCmp('DateWindowPopup_TGL_LAHIR_viDataKunjunganRWI').getValue()
                                                                                        },
                                                                                        success: function(o){
                                                                                            var tmphasil = o.responseText;
                                                                                            var tmp = tmphasil.split(' ');
                                                                                            if (tmp.length == 6){
                                                                                                Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
                                                                                                Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue(tmp[2]);
                                                                                                Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue(tmp[4]);
                                                                                                getParamBalikanHitungUmurDataKunjunganRWI(tmptanggal);
                                                                                            }else if(tmp.length == 4){
                                                                                                if(tmp[1]== 'years' && tmp[3] == 'day'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue(tmp[2]);  
                                                                                                }else if(tmp[1]== 'years' && tmp[3] == 'days'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue(tmp[2]);  
                                                                                                }else if(tmp[1]== 'year' && tmp[3] == 'days'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue(tmp[2]);  
                                                                                                }else{
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue(tmp[2]);
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue('0');
                                                                                                }
                                                                                                getParamBalikanHitungUmurDataKunjunganRWI(tmptanggal);
                                                                                            }else if(tmp.length == 2 ){
                                                                                                if (tmp[1] == 'year' ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue('0');
                                                                                                }else if (tmp[1] == 'years' ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue('0');
                                                                                                }else if (tmp[1] == 'mon'  ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue('0');
                                                                                                }else if (tmp[1] == 'mons'  ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue('0');
                                                                                                }else{
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue(tmp[0]);
                                                                                                }
                                                                                                getParamBalikanHitungUmurDataKunjunganRWI(tmptanggal);
                                                                                            }else if(tmp.length == 1){
                                                                                                Ext.getCmp('TxtWindowPopup_TAHUN_viDataKunjunganRWI').setValue('0');
                                                                                                Ext.getCmp('TxtWindowPopup_BULAN_viDataKunjunganRWI').setValue('0');
                                                                                                Ext.getCmp('TxtWindowPopup_HARI_viDataKunjunganRWI').setValue('1');
                                                                                                getParamBalikanHitungUmurDataKunjunganRWI(tmptanggal);
                                                                                            }else{
                                                                                                alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                                                                                            }                                                               
                                                                                        }
                                                                                    });
                                                                                }
                                                                            },
                                                                        }
																	}
																	//-------------- ## --------------				
																]
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Umur',
														labelSeparator: '',
														anchor: '100%',
														width: 50,
														items:
																[
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_TAHUN_viDataKunjunganRWI',
																		name: 'TxtWindowPopup_TAHUN_viDataKunjunganRWI',
																		emptyText: 'Thn',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Thn',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_BULAN_viDataKunjunganRWI',
																		name: 'TxtWindowPopup_BULAN_viDataKunjunganRWI',
																		emptyText: 'Bln',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Bln',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_HARI_viDataKunjunganRWI',
																		name: 'TxtWindowPopup_HARI_viDataKunjunganRWI',
																		emptyText: 'Hari',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Hari',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	}
																	//-------------- ## --------------			
																]
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Jenis Kelamin',
														labelSeparator: '',
														anchor: '100%',
														width: 250,
														items:
																[
																	mComboJK(),
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Golongan Darah',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 90
																	},
																	mComboGolDarah()
																			//-------------- ## --------------				
																]
													},
													//-------------- ## --------------						

													//-------------- ## --------------
												]
									},
									//-------------- ## --------------
									/*{
									 xtype: 'spacer',
									 width: 10,
									 height: 1
									 },*/
									//-------------- ## --------------
									tabsWindowPopupDataLainnya_viDataKunjunganRWI(rowdata)
											//-------------- ## --------------
								]
								//-------------- #items# --------------
					}
			)
	return pnlFormDataWindowPopup_viDataKunjunganRWI;
}
// End Function getFormItemEntryGridDataView_viDataKunjunganRWI # --------------

        /**
         *	Function : tabsWindowPopupDataLainnya_viDataKunjunganRWI
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit pada panel
         *	Di pangil dari Function getFormItemEntryGridDataView_viDataKunjunganRWI
         */
        function tabsWindowPopupDataLainnya_viDataKunjunganRWI(rowdata)
        {
            var TabsDataLainnya_viDataKunjunganRWI = new Ext.TabPanel
                    (
                            {
                                id: 'GDtabDetailIGD',
                                //region: 'center',
                                activeTab: 2,
                                // anchor: '100% 400%',
                                border: false,
                                // plain: true,
                                defaults:
                                        {
                                            autoScroll: true
                                        },
                                items: [
                                    tabsDetailWindowPopupDataLainnyaAktif_viDataKunjunganRWI(rowdata),
                                    tabsDetailWindowPopupkontak_viDataKunjunganRWI(rowdata),
                                    tabsDetailWindowPopupalamatpasien_viDataKunjunganRWI(rowdata),
                                    tabsDetailWindowPopupkeluarga_viDataKunjunganRWI(rowdata)


                                ]
                            }
                    );

            return TabsDataLainnya_viDataKunjunganRWI;
        }
// End Function tabsWindowPopupDataLainnya_viDataKunjunganRWI # --------------

        /**
         *	Function : tabsDetailWindowPopupDataLainnyaAktif_viDataKunjunganRWI
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit pada panel
         *	Di pangil dari Function tabsWindowPopupDataLainnya_viDataKunjunganRWI
         */
        function tabsDetailWindowPopupDataLainnyaAktif_viDataKunjunganRWI(rowdata)
        {
            var itemstabsDetailWindowPopupDataLainnyaAktif_viDataKunjunganRWI =
                    {
                        xtype: 'panel',
                        title: 'Data Pasien',
                        id: 'tabsDetailWindowPopupDataLainnyaAktifItem_viDataKunjunganRWI',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 110,
                        border: false,
                        items:
                                [
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboAgamaRequestEntry(),
                                                    mComboSatusMarital()
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboPekerjaanRequestEntry(),
                                                    mComboPendidikanRequestEntry()
                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupDataLainnyaAktif_viDataKunjunganRWI;
        }
// End Function tabsDetailWindowPopupDataLainnyaAktif_viDataKunjunganRWI # --------------



        function tabsDetailWindowPopupalamatpasien_viDataKunjunganRWI(rowdata)
        {
            var itemstabsDetailWindowPopupalamatpasien_viDataKunjunganRWI =
                    {
                        xtype: 'panel',
                        title: 'Tempat Tinggal',
                        id: 'tabsDetailWindowPopupAlamatpasienItem_viDataKunjunganRWI',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 150,
                        border: false,
                        items:
                                [
                                    //-------------- ## --------------
                                    {
                                        xtype: 'textarea',
                                        fieldLabel: 'Alamat',
                                        id: 'TxtWindowPopup_ALAMAT_viDataKunjunganRWI',
                                        name: 'TxtWindowPopup_ALAMAT_viDataKunjunganRWI',
                                        emptyText: 'Alamat',
                                        labelSeparator: '',
                                        anchor: '100%',
                                        flex: 1
                                    },
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboPropinsi_EditDataKunjunganRWI(),
                                                    mComboKabupaten_DataKunjunganRWI(),
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboKecamatan_editDataKunjunganRWI(),
                                                    mCombokelurahan_editDataKunjunganRWI()
                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupalamatpasien_viDataKunjunganRWI;
        }


        function loaddatastorekelurahan(kd_kecamatan)
        {
            dsKelurahan.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            Sort: 'kelurahan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKelurahan',
                                            param: 'kd_kecamatan=' + kd_kecamatan
                                        }
                            }
                    )
        }


        function mCombokelurahan_editDataKunjunganRWI()
        {
            var Field = ['KD_KELURAHAN', 'KD_KECAMATAN', 'KELURAHAN'];
            dsKelurahan = new WebApp.DataStore({fields: Field});

            var cbokelurahan_editDataKunjunganRWI = new Ext.form.ComboBox
                    (
                            {
                                id: 'cbokelurahan_editDataKunjunganRWI',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Kelurahan...',
                                fieldLabel: 'Kelurahan',
                                align: 'Right',
                                tabIndex: 21,
//		    anchor:'60%',
                                store: dsKelurahan,
                                valueField: 'KD_KELURAHAN',
                                displayField: 'KELURAHAN',
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                kelurahanpasien = b.data.KD_KELURAHAN;
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    {
                                                        Ext.getCmp('txtpos').focus();
                                                    }
                                                    //atau Ext.EventObject.ENTER
                                                    else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {

                                                        kelurahanpasien = c.value;
                                                    }
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cbokelurahan_editDataKunjunganRWI;
        }
        ;


function mComboPropinsi_EditDataKunjunganRWI(){
	var Field = ['KD_PROPINSI', 'PROPINSI'];
	dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
	dsPropinsiRequestEntry.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'propinsi',
			Sortdir: 'ASC',
			target: 'ViewComboPropinsi',
			param: ''
		}
	});
	var cboPropinsi_EditDataKunjunganRWI = new Ext.form.ComboBox({
		id: 'cboPropinsi_EditDataKunjunganRWI',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		forceSelection: true,
		emptyText: 'Pilih a Propinsi...',
		selectOnFocus: true,
		fieldLabel: 'Propinsi',
		align: 'Right',
		store: dsPropinsiRequestEntry,
		valueField: 'KD_PROPINSI',
		displayField: 'PROPINSI',
		anchor: '99%',
		tabIndex: 18,
		listeners:{
			'select': function (a, b, c){
                Ext.getCmp('cboKabupaten_editDataKunjunganRWI').setValue('');
                Ext.getCmp('cboKecamatan_editDataKunjunganRWI').setValue('');
                Ext.getCmp('cbokelurahan_editDataKunjunganRWI').setValue('');
				selectPropinsiRequestEntry = b.data.KD_PROPINSI;
				loaddatastorekabupaten(b.data.KD_PROPINSI);
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) //atau Ext.EventObject.ENTER
					{
						Ext.getCmp('cboKabupaten_editDataKunjunganRWI').focus();
						selectPropinsiRequestEntry = c.value;
						loaddatastorekabupaten(selectPropinsiRequestEntry);
					} else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
					{
						selectPropinsiRequestEntry = c.value;
						loaddatastorekabupaten(selectPropinsiRequestEntry);
					}
				}, c);
			}
		}
	});
	return cboPropinsi_EditDataKunjunganRWI;
}
        function mComboKabupaten_DataKunjunganRWI()
        {
            var Field = ['KD_KABUPATEN', 'KD_PROPINSI', 'KABUPATEN'];
            dsKabupatenRequestEntry = new WebApp.DataStore({fields: Field});

            var cboKabupaten_editDataKunjunganRWI = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboKabupaten_editDataKunjunganRWI',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Kabupaten...',
                                fieldLabel: 'Kab/Kod',
                                align: 'Right',
                                store: dsKabupatenRequestEntry,
                                valueField: 'KD_KABUPATEN',
                                displayField: 'KABUPATEN',
                                tabIndex: 19,
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                Ext.getCmp('cboKecamatan_editDataKunjunganRWI').setValue('');
                                                Ext.getCmp('cbokelurahan_editDataKunjunganRWI').setValue('');
                                                selectKabupatenRequestEntry = b.data.KD_KABUPATEN;
                                                loaddatastorekecamatan(b.data.KD_KABUPATEN);
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    {
                                                        Ext.getCmp('cboKecamatan_editDataKunjunganRWI').focus();
                                                        selectKabupatenRequestEntry = c.value;
                                                        loaddatastorekecamatan(selectKabupatenRequestEntry);
                                                    } else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {
                                                        selectKabupatenRequestEntry = c.value;
                                                        loaddatastorekecamatan(selectKabupatenRequestEntry);
                                                    }
                                                }, c);
                                            }

                                        }
                            }
                    );
            return cboKabupaten_editDataKunjunganRWI;
        }
        ;


        function loaddatastorekabupaten(kd_propinsi)
        {
            dsKabupatenRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'kabupaten',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKabupaten',
                                            param: 'kd_propinsi=' + kd_propinsi
                                        }
                            }
                    )
        }

        function loaddatastorekecamatan(kd_kabupaten)
        {
            dsKecamatanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            Sort: 'kecamatan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKecamatan',
                                            param: 'kd_kabupaten=' + kd_kabupaten
                                        }
                            }
                    )
        }


        function mComboKecamatan_editDataKunjunganRWI()
        {
            var Field = ['KD_KECAMATAN', 'KD_KABUPATEN', 'KECAMATAN'];
            dsKecamatanRequestEntry = new WebApp.DataStore({fields: Field});

            var cboKecamatan_editDataKunjunganRWI = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboKecamatan_editDataKunjunganRWI',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'pilih Kecamatan...',
                                fieldLabel: 'Kecamatan',
                                align: 'Right',
                                tabIndex: 20,
                                store: dsKecamatanRequestEntry,
                                valueField: 'KD_KECAMATAN',
                                displayField: 'KECAMATAN',
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                Ext.getCmp('cbokelurahan_editDataKunjunganRWI').setValue();
                                                selectKecamatanpasien = b.data.KD_KECAMATAN;
                                                loaddatastorekelurahan(b.data.KD_KECAMATAN)
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13)
                                                    {
                                                        Ext.getCmp('cbokelurahan_editDataKunjunganRWI').focus();
                                                        selectKecamatanpasien = c.value;
                                                    }																//atau Ext.EventObject.ENTER
                                                    else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {
                                                        loaddatastorekelurahan(c.value);
                                                        selectKecamatanpasien = c.value;
                                                    }
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboKecamatan_editDataKunjunganRWI;
        }
        ;


        function tabsDetailWindowPopupkontak_viDataKunjunganRWI(rowdata)
        {
            var itemstabsDetailWindowPopupkontak_viDataKunjunganRWI =
                    {
                        xtype: 'panel',
                        title: 'Kontak',
                        id: 'tabsDetailWindowPopupDatakontakItem_viDataKunjunganRWI',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 90,
                        border: false,
                        items:
                                [
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: ' No. Tlpn' + ' ',
                                                        id: 'TxtWindowPopup_NO_TELP_viDataKunjunganRWI',
                                                        name: 'TxtWindowPopup_NO_TELP_viDataKunjunganRWI',
                                                        //emptyText: '',
                                                        anchor: '95%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: ' Email' + ' ',
                                                        id: 'TxtWindowPopup_Email_viDataKunjunganRWI',
                                                        name: 'TxtWindowPopup_Email_viDataKunjunganRWI',
                                                        anchor: '95%'
                                                    }
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'No. Hp',
                                                        id: 'TxtWindowPopup_HP_viDataKunjunganRWI',
                                                        name: 'TxtWindowPopup_HP_viDataKunjunganRWI',
                                                        anchor: '95%'
                                                    }


                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupkontak_viDataKunjunganRWI;
        }




        function tabsDetailWindowPopupkeluarga_viDataKunjunganRWI(rowdata)
        {
            var itemstabsDetailWindowPopupkeluarga_viDataKunjunganRWI =
                    {
                        xtype: 'panel',
                        title: 'Keluarga',
                        id: 'tabsDetailWindowPopupDataKeluargaItem_viDataKunjunganRWI',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 90,
                        border: false,
                        items:
                                [
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nama Ayah' + ' ',
                                                        id: 'TxtWindowPopup_Nama_ayah_viDataKunjunganRWI',
                                                        name: 'TxtWindowPopup_Nama_ayah_viDataKunjunganRWI',
                                                        //emptyText: '',
                                                        anchor: '95%'
                                                    }
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nama Ibu',
                                                        id: 'TxtWindowPopup_NamaIbu_viDataKunjunganRWI',
                                                        name: 'TxtWindowPopup_NamaIbu_viDataKunjunganRWI',
                                                        anchor: '95%'
                                                    }


                                                ]
                                            }]
                                    },{
										xtype: 'textfield',
										fieldLabel: 'Nama Keluarga',
										id: 'TxtWindowPopup_Nama_keluarga_viDataKunjunganRWI'
									}
                                ]
                    }
            return itemstabsDetailWindowPopupkeluarga_viDataKunjunganRWI;
        }


        function getCriteriaFilter_viDaftar()
        {
            var strKriteria = "";

            if (Ext.get('txtNoMedrec_DataKunjunganRWI').getValue() != "")
            {
                strKriteria = " pasien.kd_pasien = " + "'" + Ext.get('txtNoMedrec_DataKunjunganRWI').getValue() + "'"+ " or pasien.part_number_nik= "+ "'" + Ext.get('txtNoMedrec_DataKunjunganRWI').dom.value + "'" ;
				
            }

            if (Ext.get('txtNamaPasien_DataKunjunganRWI').getValue() != "")
            {
                if (strKriteria == "")
                {
                    strKriteria = " lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasien_DataKunjunganRWI').getValue() + "%')";
                } else
                {
                    strKriteria += " and lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasien_DataKunjunganRWI').getValue() + "%')";
                }

            }
            
            if (Ext.get('txtAlamatPasien_DataKunjunganRWI').getValue() != "")
            {
                if (strKriteria == "")
                {
                    strKriteria = " lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamatPasien_DataKunjunganRWI').getValue() + "%')";
                } else
                {
                    strKriteria += " and lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamatPasien_DataKunjunganRWI').getValue() + "%')";
                }

            }

            if (Ext.get('txtKeluargaPasien_DataKunjunganRWI').getValue() != "")
            {
                if (strKriteria == "")
                {
                    strKriteria = " lower(pasien.nama_keluarga) " + "LIKE lower('" + Ext.get('txtKeluargaPasien_DataKunjunganRWI').getValue() + "%')";
                } else
                {
                    strKriteria += " and lower(pasien.nama_keluarga) " + "LIKE lower('" + Ext.get('txtKeluargaPasien_DataKunjunganRWI').getValue() + "%')";
                }

            }
            return strKriteria;
        }

// --------------------------------------- # End Form # ---------------------------------------


// End Project Data Pasien # --------------

        function mComboPekerjaanRequestEntry()
        {
            var Field = ['KD_PEKERJAAN', 'PEKERJAAN'];

            dsPekerjaanRequestEntry = new WebApp.DataStore({fields: Field});
            dsPekerjaanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'pekerjaan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboPekerjaan',
                                            param: ''
                                        }
                            }
                    )

            var cboPekerjaanRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboPekerjaanRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Pekerjaan...',
                                fieldLabel: 'Pekerjaan',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsPekerjaanRequestEntry,
                                valueField: 'KD_PEKERJAAN',
                                displayField: 'PEKERJAAN',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectPekerjaanRequestEntry = b.data.KD_PROPINSI;
                                                Ext.getCmp('cboPekerjaanRequestEntry').setValue(b.data.KD_PEKERJAAN);
                                                Ext.getCmp('TxtTmpPekerjaan_viDataKunjunganRWI').setValue(b.data.KD_PEKERJAAN);
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboSukuRequestEntry').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboPekerjaanRequestEntry;
        }
        ;

        function mComboPendidikanRequestEntry()
        {
            var Field = ['KD_PENDIDIKAN', 'PENDIDIKAN'];

            dsPendidikanRequestEntry = new WebApp.DataStore({fields: Field});
            dsPendidikanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'pendidikan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboPendidikan',
                                            param: ''
                                        }
                            }
                    )

            var cboPendidikanRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboPendidikanRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Pendidikan...',
                                fieldLabel: 'Pendidikan',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsPendidikanRequestEntry,
                                valueField: 'KD_PENDIDIKAN',
                                displayField: 'PENDIDIKAN',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectPendidikanRequestEntry = b.data.KD_PENDIDIKAN;
                                                Ext.getCmp('cboPendidikanRequestEntry').setValue(b.data.KD_PENDIDIKAN);
                                                Ext.getCmp('TxtTmpPendidikan_viDataKunjunganRWI').setValue(b.data.KD_PENDIDIKAN);
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboPekerjaanRequestEntry').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboPendidikanRequestEntry;
        }
        ;

        function mComboAgamaRequestEntry()
        {
            var Field = ['KD_AGAMA', 'AGAMA'];

            dsAgamaRequestEntry = new WebApp.DataStore({fields: Field});
            dsAgamaRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'agama',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboAgama',
                                            param: ''
                                        }
                            }
                    )

            var cboAgamaRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboAgamaRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Agama...',
                                fieldLabel: 'Agama',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsAgamaRequestEntry,
                                valueField: 'KD_AGAMA',
                                displayField: 'AGAMA',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectAgamaRequestEntry = b.data.KD_AGAMA;
                                                Ext.getCmp('cboAgamaRequestEntry').setValue(b.data.KD_AGAMA);
                                                Ext.getCmp('TxtTmpAgama_viDataKunjunganRWI').setValue(b.data.KD_AGAMA);

                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboGolDarah').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboAgamaRequestEntry;
        }
        ;

        function mComboSatusMarital()
        {
            var cboStatusMarital = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboStatusMarital',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Status...',
                                fieldLabel: 'Status Marital ',
                                width: 110,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[0, 'Blm Kawin'], [1, 'Kawin'], [2, 'Janda'], [3, 'Duda']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetSatusMarital,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetSatusMarital = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtAlamat').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboStatusMarital;
        }
        ;

        function mComboGolDarah()
        {
            var cboGolDarah = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboGolDarah',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                //allowBlank: false,
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Gol. Darah...',
                                fieldLabel: 'Gol. Darah ',
                                width: 50,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[0, '-'], [1, 'A+'], [2, 'B+'], [3, 'AB+'], [4, 'O+'], [5, 'A-'], [6, 'B-'], [7, 'AB-'], [8, 'O-']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetGolDarah,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetGolDarah = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboWarga').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboGolDarah;
        }
        ;

        function mComboJK()
        {
			
            var cboJK = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboJK',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Jenis Kelamin...',
                                fieldLabel: 'Jenis Kelamin ',
                                width: 100,
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetJK,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetJK = b.data.displayText;
//                                        getdatajeniskelamin(b.data.displayText)
                                                //alert(jenis_kelamin)
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtTempatLahir').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboJK;
        }
        ;


        function form_histori() {
            var form = new Ext.form.FormPanel({
                baseCls: 'x-plain',
                labelWidth: 55,
                url: 'save-form.php',
                defaultType: 'textfield',
                items:
                        [
                            {
                                x: 0,
                                y: 60,
                                xtype: 'textarea',
                                id: 'TxtHistoriDeleteDataKunjunganRWI',
                                hideLabel: true,
                                name: 'msg',
                                anchor: '100% 100%'  // anchor width by percentage and height by raw adjustment
                            }

                        ]
            });

            var window = new Ext.Window({
                title: 'Pesan',
                width: 400,
                height: 300,
                minWidth: 300,
                minHeight: 200,
                layout: 'fit',
                plain: true,
                bodyStyle: 'padding:5px;',
                buttonAlign: 'center',
                items: form,
                buttons: [{
                        xtype: 'button',
                        text: 'Save',
                        //height: 20,
                        id: 'btnSimpanHistori_viDataKunjunganRWI',
                        iconCls: 'save',
                        handler: function ()
                        {
                            datasavehistori_viDataKunjunganRWI(false); //1.1
                            window.close();
                            setLookUpsGridDataView_viDataKunjunganRWI.close();
                            //DataRefresh_viDataKunjunganRWI(getCriteriaFilterGridDataView_viDataKunjunganRWI());
                        }
                    }]
            });

            window.show();
        }
        ;

        function item()
        {
            var pnlFormDataHistori_viDataKunjunganRWI = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'center',
                                fileUpload: true,
                                layout: 'anchor',
                                padding: '8px',
                                bodyStyle: 'padding:10px 0px 10px 10px;',
                                items:
                                        [
                                        ]
                            }
                    );
            return pnlFormDataHistori_viDataKunjunganRWI;
        }
		
/* 
	PERBARUAN GANTI KELOMPOK PASIEN 
	OLEH 	: HADAD
	TANGGAL : 2016 - 12 - 30
*/
/* =============================================================================================================================================== */
function KelompokPasienLookUp_rwj(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien_rwj = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien_rwj(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien_rwj.show();
    KelompokPasienbaru_rwj();

};

function KelompokPasienbaru_rwj() 
{
	jeniscus_RWJ=0;
    KelompokPasienAddNew_RWJ = true;
    Ext.getCmp('cboKelompokpasien_RWJ').show()
	Ext.getCmp('txtCustomer_rwjLama').disable();
	Ext.get('txtCustomer_rwjLama').dom.value=vCustomer;
	Ext.get('txtRWJNoSEP').dom.value = vNoSEP;
	
	RefreshDatacombo_rwj(jeniscus_RWJ);
};

function getFormEntryTRKelompokPasien_rwj(lebar) 
{
    var pnlTRKelompokPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien_igd',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
					getItemPanelInputKelompokPasien_rwj(lebar),
					getItemPanelButtonKelompokPasien_rwj(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasien = new Ext.Panel
	(
		{
		    id: 'FormDepanKelompokPasien',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien_igd	
				
			]

		}
	);

    return FormDepanKelompokPasien
};

function getItemPanelInputKelompokPasien_rwj(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama_rwj(lebar),	
					getItemPanelNoTransksiKelompokPasien_rwj(lebar)	,
					
				]
			}
		]
	};
    return items;
};


/* =============================================================================================================================================== */
/* 
	PERBARUAN GANTI DOKTER  
	OLEH 	: HADAD
	TANGGAL : 2017 - 01 - 05
*/
/* =============================================================================================================================================== */
function GantiDokterPasienLookUp_rwj(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRGantiDokter_rwj = new Ext.Window
    (
        {
            id: 'idGantiDokter',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiDokter_rwj(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRGantiDokter_rwj.show();
    GantiPasien_rwj();

};

function GantiPasien_rwj() 
{
	//RefreshDatacombo_rwj(jeniscus_RWJ);
};

function getFormEntryTRGantiDokter_rwj(lebar) 
{
    var pnlTRKelompokPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRGanti',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
					getItemPanelInputGantiDokter_rwj(lebar),
					getItemPanelButtonKelompokPasien_rwj(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiDokter = new Ext.Panel
	(
		{
		    id: 'FormDepanGantiDokter',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien_igd	
				
			]

		}
	);

    return FormDepanGantiDokter
};

function getItemPanelInputGantiDokter_rwj(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[	
					{
						xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'txtUnitAsal_DataKunjunganRWI',
					    id: 'txtUnitAsal_DataKunjunganRWI',
						value:vNamaUnit,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'txtDokterAsal_DataKunjunganRWI',
					    id: 'txtDokterAsal_DataKunjunganRWI',
						value:vNamaDokter,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},
					mComboDokterGantiEntry()
				]
			}
		]
	};
    return items;
};


/* =============================================================================================================================================== */
/* 
	PERBARUAN GANTI UNIT
	HADAD
	2017 - 01 -05
 */
 
/* =============================================================================================================================================== */
function KonsultasiDataKunjunganRWILookUp_rwj(lebar){
	
    var lebar = 440;
    FormLookUpsdetailTRKonsultasi_DataKunjunganRWI = new Ext.Window
    (
        {
            id: 'idKonsultasiDataKunjunganRWI',
            title: 'Ganti Unit',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiUnit_rwj(lebar),
            listeners:
            {
                
            }
        }
    );
    FormLookUpsdetailTRKonsultasi_DataKunjunganRWI.show();
    //KonsultasiAddNew();
};



function getFormEntryTRGantiUnit_rwj(lebar) 
{
    var pnlTRUnitPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRGantiUnit',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar-35,
            border: false,
            items: [
					getItemPanelInputGantiUnit_rwj(lebar),
					getItemPanelButtonKelompokPasien_rwj(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiUnit = new Ext.Panel
	(
		{
		    id: 'FormDepanGantiUnit',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRUnitPasien_igd]

		}
	);

    return FormDepanGantiUnit
};


function getItemPanelInputGantiUnit_rwj(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[	
					{
						xtype: 'textfield',
					    fieldLabel:  'Poli Asal ',
					    name: 'txtUnitAsal_DataKunjunganRWI',
					    id: 'txtUnitAsal_DataKunjunganRWI',
						value:vNamaUnit,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},
					mComboPoliklinik(),
					mComboDokterGantiEntry()
				]
			}
		]
	};
    return items;
};
/* =============================================================================================================================================== */

function getItemPanelButtonKelompokPasien_rwj(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id:Nci.getId(),
						handler:function()
						{
							if(panelActiveDataKunjunganRWI == 0){
								Datasave_Kelompokpasien_rwj();
							}
							if(panelActiveDataKunjunganRWI == 1){
								Datasave_GantiDokter_rwj();
							}
							if(panelActiveDataKunjunganRWI == 2){
								Datasave_GantiUnit_rwj();
							}
						}
					},
					{
						xtype:'button',
						text:'Tutup',
						width:70,
						hideLabel:true,
						id:Nci.getId(),
						handler:function() 
						{
							if(panelActiveDataKunjunganRWI == 0){
								FormLookUpsdetailTRKelompokPasien_rwj.close();
							}
							
							if(panelActiveDataKunjunganRWI == 1){
								FormLookUpsdetailTRGantiDokter_rwj.close();
							}
							
							if(panelActiveDataKunjunganRWI == 2){
								FormLookUpsdetailTRKonsultasi_DataKunjunganRWI.close();
							}
						}
					}
				]
			}
		]
	}
    return items;
};

function getKelompokpasienlama_rwj(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height: 2
						},
						{
							xtype: 'textfield',
							fieldLabel: 'Kelompok Pasien Asal',
							name: 'txtCustomer_rwjLama',
							id: 'txtCustomer_rwjLama',
							labelWidth:130,
							editable: false,
							width: 100,
							anchor: '95%'
						 }
					]
			}
			
		]
	}
    return items;
};
function getItemPanelNoTransksiKelompokPasien_rwj(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height:3
					},{ 
					    xtype: 'combo',
						fieldLabel: 'Kelompok Pasien Baru',
						id: 'kelPasien_RWJ',
						editable: false,
						store: new Ext.data.ArrayStore
							(
								{
								id: 0,
								fields:
								[
								'Id',
								'displayText'
								],
								   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
								}
							),
							  displayField: 'displayText',
							  mode: 'local',
							  width: 100,
							  forceSelection: true,
							  triggerAction: 'all',
							  emptyText: 'Pilih Salah Satu...',
							  selectOnFocus: true,
							  anchor: '95%',
							  listeners:
								 {
										'select': function(a, b, c)
									{
										if(b.data.displayText =='Perseorangan')
										{jeniscus_RWJ='0'
											//Ext.getCmp('txtRWJNoSEP').disable();
											//Ext.getCmp('txtRWJNoAskes').disable();
											}
										else if(b.data.displayText =='Perusahaan')
										{jeniscus_RWJ='1';
											//Ext.getCmp('txtRWJNoSEP').disable();
											//Ext.getCmp('txtRWJNoAskes').disable();
											}
										else if(b.data.displayText =='Asuransi')
										{jeniscus_RWJ='2';
											//Ext.getCmp('txtRWJNoSEP').enable();
											//Ext.getCmp('txtRWJNoAskes').enable();
										}
										
										RefreshDatacombo_rwj(jeniscus_RWJ);
									}

								}
						},{
							columnWidth: .990,
							layout: 'form',
							border: false,
							labelWidth:130,
							items:
							[
												mComboKelompokpasien_rwj()
							]
						},{
							xtype: 'textfield',
							fieldLabel:'No SEP  ',
							name: 'txtRWJNoSEP',
							id: 'txtRWJNoSEP',
							width: 100,
							anchor: '99%'
						 }, {
                            xtype: 'textfield',
                            fieldLabel:'No Asuransi  ',
                            name: 'txtRWJNoAskes',
                            id: 'txtRWJNoAskes',
                            width: 100,
                            anchor: '99%',
                            value : vNoAsuransi,
						 }
									
				]
			}
			
		]
	}
    return items;
};

function mComboKelompokpasien_rwj()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viPJ_IGD = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	if (jeniscus_RWJ===undefined || jeniscus_RWJ==='')
	{
		jeniscus_RWJ=0;
	}
	ds_customer_viPJ_IGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_RWJ +'~'
            }
        }
    )
    var cboKelompokpasien_RWJ = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien_RWJ',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: labelisi_RWJ,
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viPJ_IGD,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien_RWJ;
};


function RefreshDatacombo_rwj(jeniscus_RWJ) 
{

    ds_customer_viPJ_IGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_RWJ +'~ and kontraktor.kd_customer not in(~'+ vkode_customer_RWJ+'~)'
            }
        }
    )
	
    return ds_customer_viPJ_IGD;
};

/* 
	============================= AJAX 
 */
function Datasave_Kelompokpasien_rwj(mBol) 
{	
    if (ValidasiEntryUpdateKelompokPasien_RWJ(nmHeaderSimpanData,false) == 1 )
    {           

        var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan ganti kelompok:', function(btn, combo){
            if (btn == 'ok')
            {
                if (combo!='') {
                    Ext.Ajax.request
                    (
                        {
                            url: baseURL +  "index.php/main/functionRWJ/UpdateGantiKelompok",   
                            params: {
                                        KDCustomer  : selectKdCustomer,
                                        KDNoSJP     : Ext.get('txtRWJNoSEP').getValue(),
                                        KDNoAskes   : Ext.get('txtRWJNoAskes').getValue(),
                                        KdPasien    : vKd_Pasien,
                                        TglMasuk    : vTanggal,
                                        KdUnit      : vKdUnit,
                                        UrutMasuk   : vUrutMasuk,
                                        KdDokter    : vKdDokter,
                                        alasan      : combo,
                            },
                            failure: function(o)
                            {
                                ShowPesanWarning_viDataKunjunganRWI('Simpan kelompok pasien gagal', 'Gagal');
                            },  
                            success: function(o) 
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true) 
                                {
                                    panelActiveDataKunjunganRWI = 1;
                                    FormLookUpsdetailTRKelompokPasien_rwj.close();
                                    RefreshDatahistoriKunjunganPasienRWI(vKd_Pasien);
                                    // Ext.getCmp('btnGantiKekompokPasien').disable(); 
                                    // Ext.getCmp('btnGantiDokter').disable(); 
                                    // Ext.getCmp('btnUnitPasien').disable();  
                                    Ext.getCmp('btnHpsDataKunjunganRWI').disable();   
                                    Ext.getCmp('btnGantiTanggalMasuk').disable();   
                                    ShowPesanInfo_viDataKunjunganRWI("Mengganti kelompok pasien berhasil", "Success");

                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                    DataRefresh_viDataKunjunganRWI(tmpkriteria);
                                }else 
                                {
                                    panelActiveDataKunjunganRWI = 1;
                                    ShowPesanWarning_viDataKunjunganRWI('Simpan kelompok pasien gagal', 'Gagal');
                                };
                            }
                        }
                    );
                }else{
                    ShowPesanWarning_viDataKunjunganRWI('Harap memasukkan alasan perpindahan', 'Peringatan');
                }
            }
        });
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
	
};

function Datasave_GantiUnit_rwj(mBol) 
{	
	if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntry').dom.value  === 'Pilih Dokter...'))
	{
		ShowPesanWarning_viDataKunjunganRWI('Dokter baru harap diisi', "Informasi");
	}else{	
			Ext.Ajax.request
			(
				{
					url: baseURL +  "index.php/main/functionRWJ/UpdateGantiUnit",	
					params: getParamKelompokpasien_RWJ(),
					failure: function(o)
					{
						ShowPesanWarning_viDataKunjunganRWI('Simpan unit pasien gagal', 'Gagal');
						loadMask.hide();
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.totalrecords === 1) 
						{
                            panelActiveDataKunjunganRWI = 1;
							vKdUnitDulu = "";
							FormLookUpsdetailTRKonsultasi_DataKunjunganRWI.close();
							RefreshDatahistoriKunjunganPasienRWI(vKd_Pasien);
							// Ext.getCmp('btnGantiKekompokPasien').disable();	
							// Ext.getCmp('btnGantiDokter').disable();	
                            // Ext.getCmp('btnUnitPasien').disable();  
                            Ext.getCmp('btnHpsDataKunjunganRWI').disable(); 
							Ext.getCmp('btnGantiTanggalMasuk').disable();	
							ShowPesanInfo_viDataKunjunganRWI("Mengganti unit pasien berhasil", "Success");
						}else 
						{
							ShowPesanWarning_viDataKunjunganRWI('Simpan   pasien gagal', 'Gagal');
						};
					}
				}
			 )
	}
	
};

function Datasave_GantiDokter_rwj(mBol) 
{	
	//console.log(Ext.get('cboDokterRequestEntry').getValue());
	if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntry').dom.value  === 'Pilih Dokter...'))
	{
		ShowPesanWarning_viDataKunjunganRWI('Dokter baru harap diisi', "Informasi");
	}else{
		Ext.Ajax.request
		(
			{
				url: baseURL +  "index.php/main/functionRWJ/UpdateGantiDokter",	
				params: getParamKelompokpasien_RWJ(),
				failure: function(o)
				{
					ShowPesanWarning_viDataKunjunganRWI('Simpan dokter pasien gagal', 'Gagal');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
                        panelActiveDataKunjunganRWI = 1;
						FormLookUpsdetailTRGantiDokter_rwj.close();
						RefreshDatahistoriKunjunganPasienRWI(vKd_Pasien);
						// Ext.getCmp('btnGantiKekompokPasien').disable();	
						// Ext.getCmp('btnGantiDokter').disable();	
                        // Ext.getCmp('btnUnitPasien').disable();  
                        Ext.getCmp('btnHpsDataKunjunganRWI').disable(); 
						Ext.getCmp('btnGantiTanggalMasuk').disable();	
						ShowPesanInfo_viDataKunjunganRWI("Mengganti dokter pasien berhasil", "Success");
					}else 
					{
						ShowPesanWarning_viDataKunjunganRWI('Simpan dokter pasien gagal', 'Gagal');
					};
				}
			}
		) 
	}
	
};

function cekDetailTransaksi() 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL +  "index.php/main/functionRWJ/cekDetailTransaksi",	
			params: getParamKelompokpasien_RWJ(),
			failure: function(o)
			{
				ShowPesanWarning_viDataKunjunganRWI('Data gagal di periksa', 'Gagal');
				loadMask.hide();
			},	
			success: function(o) 
			{
				//console.log(o.responseText);
				if(o.responseText > 0){
					statusCekDetail = true;
				}else{
					statusCekDetail = false;
				}
			}
		}
	)
};

function ValidasiEntryUpdateKelompokPasien_RWJ(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('kelPasien_RWJ').getValue() == '') || (Ext.get('kelPasien_RWJ').dom.value  === undefined ))
	{
		if (Ext.get('kelPasien_RWJ').getValue() == '' && mBolHapus === true) 
		{
			ShowPesanWarning_viDataKunjunganRWI(nmGetValidasiKosong('Kelompok Pasien'), modul);
			x = 0;
		}
	};
	return x;
};

function ValidasiEntryUpdateGantiDokter_RWJ(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ))
	{
		ShowPesanWarning_viDataKunjunganRWI(nmGetValidasiKosong('Dokter baru harap diisi'), modul);
		x = 0;
	};
	return x;
};

function getParamKelompokpasien_RWJ() 
{
	var params;
	if(panelActiveDataKunjunganRWI == 0){
		params = {
			KDCustomer 	: selectKdCustomer,
			KDNoSJP  	: Ext.get('txtRWJNoSEP').getValue(),
			KDNoAskes  	: Ext.get('txtRWJNoAskes').getValue(),
			KdPasien  	: vKd_Pasien,
			TglMasuk  	: vTanggal,
			KdUnit  	: vKdUnit,
			UrutMasuk  	: vUrutMasuk,
			KdDokter  	: vKdDokter,
		}
	}else if(panelActiveDataKunjunganRWI == 1 || panelActiveDataKunjunganRWI == 2){
		params = {
			KdPasien  	: vKd_Pasien,
			TglMasuk  	: vTanggal,
			KdUnit  	: vKdUnit,
			UrutMasuk  	: vUrutMasuk,
			KdDokter  	: vKdDokter,
			NoTransaksi	: vNoTransaksi,
			KdKasir  	: vKdKasir,
			KdUnitDulu 	: vKdUnitDulu,
		}
	}else if(panelActiveDataKunjunganRWI == 'undefined'){
		params = { data : "null", }
	}else{
		params = { data : "null", }
	}
    return params
};

function mComboDokterGantiEntry(){ 
	/* var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry = new WebApp.DataStore({fields: Field}); */
    var cboDokterGantiEntry = new Ext.form.ComboBox({
	    id: 'cboDokterRequestEntry',
	    typeAhead: true,
	    triggerAction: 'all',
		name:'txtdokter',
	    lazyRender: true,
	    mode: 'local',
	    selectOnFocus:true,
        forceSelection: true,
	    emptyText:'Pilih Dokter...',
	    fieldLabel: 'Dokter Baru',
	    align: 'Right',
	    store: dsDokterRequestEntry,
	    valueField: 'KD_DOKTER',
	    displayField: 'NAMA',
		anchor:'100%',
	    listeners:{
		    'select': function(a,b,c){
				vKdDokter = b.data.KD_DOKTER;
            },
		}
    });
    return cboDokterGantiEntry;
};

function mComboPoliklinik(){
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
	ds_Poli_viDaftar.load({
        params	:{
            Skip	: 0,
            Take	: 1000,
            Sort	: 'NAMA_UNIT',
            Sortdir	: 'ASC',
            target	: 'ViewSetupUnitB',
            param	: " parent<>'0'"
        }
    });
    var cboPoliklinikRequestEntry = new Ext.form.ComboBox({
        id				: 'cboPoliklinikRequestEntry',
        typeAhead		: true,
        triggerAction	: 'all',
		width			: 170,
        lazyRender		: true,
        mode			: 'local',
        selectOnFocus	: true,
        forceSelection	: true,
        emptyText		: 'Pilih Poliklinik...',
        fieldLabel		: 'Poliklinik ',
        align			: 'Right',
        store			: ds_Poli_viDaftar,
        valueField		: 'KD_UNIT',
        displayField	: 'NAMA_UNIT',
        anchor			: '100%',
        listeners:{
            select: function(a, b, c){
				vKdUnitDulu 	= vKdUnit;
				vKdUnit  		= b.data.KD_UNIT;
				loaddatastoredokter();
            }
		}
    });
    return cboPoliklinikRequestEntry;
};

/*
    
    PERBARUAN FORMAT TANGGAL LAHIR 
    OLEH    : HADAD AL GOJALI 
    TANGGAL : 2017 - 01 - 21 

 */
function getParamBalikanHitungUmurDataKunjunganRWI(tgl){
    var param;
    var tglPisah=tgl.split("/");
    var tglAsli;
    if (tglPisah[1]==="01"){
        tglAsli=tglPisah[0]+"/"+"Jan"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="02"){
        tglAsli=tglPisah[0]+"/"+"Feb"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="03"){
        tglAsli=tglPisah[0]+"/"+"Mar"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="04"){
        tglAsli=tglPisah[0]+"/"+"Apr"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="05"){
        tglAsli=tglPisah[0]+"/"+"May"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="06"){
        tglAsli=tglPisah[0]+"/"+"Jun"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="07"){
        tglAsli=tglPisah[0]+"/"+"Jul"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="08"){
        tglAsli=tglPisah[0]+"/"+"Aug"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="09"){
        tglAsli=tglPisah[0]+"/"+"Sep"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="10"){
        tglAsli=tglPisah[0]+"/"+"Oct"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="11"){
        tglAsli=tglPisah[0]+"/"+"Nov"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="12"){
        tglAsli=tglPisah[0]+"/"+"Dec"+"/"+tglPisah[2];
    }else{
        tglAsli=tgl;
    }
    Ext.getCmp('DateWindowPopup_TGL_LAHIR_viDataKunjunganRWI').setValue(tgl);
}