var now = new Date();
var DefJamPerpindahan    = now.format('H:i:s');
var selectCountStatusByr_viPelunasanPasienRWIPelunasan;
var selectCountPelunasanPasienRWIPelunasan = '50';
var rowSelectedPelunasanPasienRWIPelunasan;
var sebulanyanglalu = new Date().add(Date.DAY, -30);
var dsRekapitulasiTindakanPelunasan_RWI;

var PenataJasaPelunasanPasienRWI = {};
var Current_PelunasanPasienRWI = {
    data: Object,
    details: Array,
    row: 0
};
var griddetailtrdokter_PelunasanPasienRWI;
var varkd_tarif_rwi;
var indexRow;
var tampungtypedata;
var dsTRDetailPelunasanrwiPelunasanList;
PenataJasaPelunasanPasienRWI.form = {};
PenataJasaPelunasanPasienRWI.form.Class = {};
PenataJasaPelunasanPasienRWI.form.DataStore = {};
PenataJasaPelunasanPasienRWI.dsGridTindakan;
PenataJasaPelunasanPasienRWI.form.ComboBox = {};
PenataJasaPelunasanPasienRWI.form.Grid = {};
PenataJasaPelunasanPasienRWI.func = {};
PenataJasaPelunasanPasienRWI.form.DataStore.produk = new Ext.data.ArrayStore({id: 0, fields: ['kd_produk', 'deskripsi', 'harga', 'tglberlaku','"kd_tarif'], data: []});
PenataJasaPelunasanPasienRWI.form.DataStore.trdokter = new Ext.data.ArrayStore({id: 0, fields: ['kd_dokter', 'nama'], data: []});
PenataJasaPelunasanPasienRWI.form.DataStore.dokter_inap_int = new Ext.data.ArrayStore({id: 0, fields: ['kd_job', 'label'], data: []});
var TmpUrl      = "";
var asli_sisa_tagihan      = 0;

CurrentPage.page = getPanelPelunasanPasienRWI(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var fldDetail = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'BAYARTR', 'DISCOUNT', 'PIUTANG', 'POTONG'];
dsTRDetailPelunasanrwiPelunasanList = new WebApp.DataStore({fields: fldDetail})
function getPanelPelunasanPasienRWI(mod_id)
{
    var Field = ['KD_PASIEN','NAMA','TGL_MASUK','TGL_PULANG','KD_KASIR','NO_TRANSAKSI','DOKTER','LUNAS','KD_UNIT','UNIT','CO_STATUS', 'KD_UNIT_KAMAR', 'VERIFIED'];
    dsTRPelunasanPasienRWIPelunasanList = new WebApp.DataStore({fields: Field});
    RefreshDataPelunasanPasienRWIPelunasan();

    grListTRPelunasanPasienRWI = new Ext.grid.EditorGridPanel
            (
                    {
                        stripeRows: true,
                        store: dsTRPelunasanPasienRWIPelunasanList,
                        columnLines: false,
                        autoScroll: true,
                        anchor: '100% 25%',
                        height: 200,
                        border: false,
                        sort: false,
                        sm: new Ext.grid.RowSelectionModel
                        (
                            {
                                singleSelect: true,
                                listeners:
                                        {
                                            rowselect: function (sm, row, rec)
                                            {
                                                rowSelectedPelunasanPasienRWIPelunasan = dsTRPelunasanPasienRWIPelunasanList.getAt(row);
                                                cek_verified();
                                            }
                                        }
                            }
                        ),
                        listeners:
                                {
                                    rowdblclick: function (sm, ridx, cidx)
                                    {

                                        rowSelectedPelunasanPasienRWIPelunasan = dsTRPelunasanPasienRWIPelunasanList.getAt(ridx);

                                        if (rowSelectedPelunasanPasienRWIPelunasan != undefined)
                                        {

                                        }

                                    },
                                    rowclick: function (sm, ridx, cidx)
                                    {
                                        var kodepasien      = rowSelectedPelunasanPasienRWIPelunasan.data.KD_PASIEN; 
                                        var kodeunit        = rowSelectedPelunasanPasienRWIPelunasan.data.KD_UNIT; 
                                        var tmpTanggalMasuk = rowSelectedPelunasanPasienRWIPelunasan.data.TGL_MASUK; 
                                        var notransaksi     = rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI; 
                                        TmpUrl              = baseURL+"index.php/rawat_inap/lap_bill_kwitansi_lunas/preview_billing/"+kodepasien+"/"+kodeunit+"/"+tmpTanggalMasuk+"/02/"+"/"+notransaksi+"/true/true";                           // 
                                        Ext.Ajax.request
                                        ({
                                            url: baseURL + "index.php/rawat_inap/pelunasan/cek_data",
                                            params: {
                                                no_transaksi:rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI,
                                                kd_kasir    :rowSelectedPelunasanPasienRWIPelunasan.data.KD_KASIR,
                                            },
                                            failure: function (o){
                                            },
                                            success: function (o){
                                                RefreshDatahistoribayar(rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI);
                                            }
                                        })
                                        RefreshDatahistoribayar(rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI);
                                        RefreshDataDetail_PelunasanPasienRWI(rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI);
                                        RefreshRekapitulasiTindakanPelunasan_RWI(rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI, '02');
                                    }


                                },
                        cm: new Ext.grid.ColumnModel
                                (
                                        [
                                            {
                                                id: 'colVERIFIEDcoba',
                                                header: 'Verified',
                                                dataIndex: 'VERIFIED',
                                                sortable: true,
                                                width: 25,
                                                align: 'center',
                                                renderer: function (value, metaData, record, rowIndex, colIndex, store)
                                                {
                                                    console.log(value);
                                                    switch (value)
                                                    {
                                                        case '1':
                                                            metaData.css = 'StatusHijau'; // 
                                                            break;
                                                        case '0':
                                                            metaData.css = 'StatusMerah'; // rejected

                                                            break;
                                                    }
                                                    return '';
                                                }
                                            },
                                            {
                                                id: 'coltutuptr',
                                                header: 'Tutup transaksi',
                                                dataIndex: 'CO_STATUS',
                                                sortable: true,
                                                hidden: true,
                                                width: 25,
                                                align: 'center',
                                                renderer: function (value, metaData, record, rowIndex, colIndex, store)
                                                {
                                                    switch (value)
                                                    {
                                                        case 't':
                                                            metaData.css = 'StatusHijau'; // 
                                                            break;
                                                        case 'f':
                                                            metaData.css = 'StatusMerah'; // rejected

                                                            break;
                                                    }
                                                    return '';
                                                }
                                            },
                                            {
                                                id: 'colReqIdViewPelunasanPasienRWI',
                                                header: 'No. Transaksi',
                                                dataIndex: 'NO_TRANSAKSI',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 40
                                            },
                                            {
                                                id: 'colTglPelunasanPasienRWIViewPelunasanPasienRWI',
                                                header: 'Tgl Masuk',
                                                dataIndex: 'TGL_MASUK',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 40,
                                                renderer: function (v, params, record)
                                                {
                                                    return ShowDate(record.data.TGL_MASUK);

                                                }
                                            },
                                            {
                                                id: 'colTglPelunasanPasienRWITglPulang',
                                                header: 'Tgl Pulang',
                                                dataIndex: 'TGL_PULANG',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 40,
                                                renderer: function (v, params, record)
                                                {
                                                    return ShowDate(record.data.TGL_PULANG);

                                                }
                                            },
                                            {
                                                header: 'No. Medrec',
                                                width: 40,
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                dataIndex: 'KD_PASIEN',
                                                id: 'colPelunasanPasienRWIerViewPelunasanPasienRWI'
                                            },
                                            {
                                                header: 'Pasien',
                                                width: 190,
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                dataIndex: 'NAMA',
                                                id: 'colPelunasanPasienRWIerViewPelunasanPasienRWI'
                                            },
                                            {
                                                id: 'colDeptViewPelunasanPasienRWI',
                                                header: 'Dokter',
                                                dataIndex: 'DOKTER',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 75
                                            },
                                            {
                                                id: 'colunitViewPelunasanPasienRWI',
                                                header: 'Unit',
                                                dataIndex: 'UNIT',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 100
                                            }

                                        ]
                                        ), 
                        viewConfig: {forceFit: true},
                        tbar:[
                                {
                                    id      : 'btnPelunasanrwi',
                                    text    : 'Pembayaran',
                                    xtype   : 'button',
                                    tooltip : nmEditData,
                                    iconCls : 'Edit_Tr',
                                    handler : function (sm, row, rec)
                                    {
                                        RefreshDataKasirrwiKasirDetail(rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI);
                                        PelunasanLookUprwi();
                                        // getDataTransaksi();
                                        getDataDetail_bayaran();
                                        // Ext.getCmp('cboJenisByr').setValue('TUNAI');
                                        console.log(dsTRDetailPelunasanrwiPelunasanList);
                                        /*if (rowSelectedPelunasanrwiPelunasan != undefined)
                                        {
                                            PelunasanLookUprwi(rowSelectedPelunasanrwiPelunasan.data);
                                        } else
                                        {
                                            ShowPesanWarningPelunasanrwi('Pilih data tabel  ', 'Pembayaran');
                                            //alert('');
                                        }*/
                                    }, disabled: false
                                },
                                {
                                    xtype: 'button',
                                    text: 'Cetak',
                                    iconCls: 'print',
                                    hidden : true,
                                    id: 'btnPrint_viPelunasanPasien',
                                    handler: function ()
                                    {
                                        fnDlgRWICetakBilling(rowSelectedPelunasanPasienRWIPelunasan.data);
                                    },
                                    /*
                                    {
                                        id      :'btnLookUpPreviewBilling',
                                        text    : 'Preview Billing',
                                        iconCls : 'Edit_Tr',
                                        handler : function(){
                                          GetDTLTRGridPreviewBilling();
                                        }
                                    },*/
                                },
                                {
                                    xtype:'splitbutton',
                                    text: 'Cetak',
                                    iconCls: 'print',
                                    arrowAlign:'right',
                                    menu: [
                                    {
                                        text: 'Billing',
                                        id: 'btnPrint_viPelunasanPasienBilling',
                                        iconCls: 'print',
                                        handler: function () {
                                            //GantiDokterLookUp_penatajasarwi_penatajasarwi();
                                            // GantiDokterPasienLookUp_rwi_REVISI();
                                            fnDlgRWICetakBilling(rowSelectedPelunasanPasienRWIPelunasan.data);
                                        }
                                    },{
                                        text: 'Kwitansi',
                                        id: 'btnPrint_viPelunasanPasienKwitansi',
                                        iconCls: 'print',
                                        handler: function () {
                                            fnDlgRWICetakKwitansi();
                                        }
                                    },
                                    ]
                                },
                                {
                                    xtype: 'button',
                                    text: 'Preview Billing',
                                    iconCls: 'Edit_Tr',
                                    id: 'btnLookUpPreviewBilling',
                                    handler: function ()
                                    {
                                        GetDTLTRGridPreviewBilling();
                                    },
                                },'-',{
                                    xtype   : 'button',
                                    text    : 'Verified',
                                    id      : 'btnVerifiedPelunasan',
                                    handler: function ()
                                    {
                                        update_verified(true);
                                    },
                                },{
                                    xtype   : 'button',
                                    text    : 'Un Verified',
                                    id      : 'btnUnVerifiedPelunasan',
                                    handler: function ()
                                    {
                                        update_verified(false);
                                    },
                                },
                        ]
                    }
            );
    var LegendViewCMRequest = new Ext.Panel
            (
                    {
                        id: 'LegendViewCMRequest',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        //height:32,
                        anchor: '100% 5%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        columnWidth: .033,
                                        layout: 'form',
                                        style: {'margin-top': '-1px'},
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        border: false,
                                        html: '<img src="' + baseURL + 'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                                    },
                                    {
                                        columnWidth: .08,
                                        layout: 'form',
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        style: {'margin-top': '1px'},
                                        border: false,
                                        html: " Verified"
                                    },
                                    {
                                        columnWidth: .033,
                                        layout: 'form',
                                        style: {'margin-top': '-1px'},
                                        border: false,
                                        //height:35,
                                        anchor: '100% 8.0001%',
                                        //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                                        html: '<img src="' + baseURL + 'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                                    },
                                    {
                                        columnWidth: .1,
                                        layout: 'form',
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        style: {'margin-top': '1px'},
                                        border: false,
                                        html: " Belum Verified"
                                    }
                                ]

                    }
            )
    var GDtabDetailrwi = new Ext.TabPanel
            (
                    {
                        id: 'GDtabDetailrwi',
                        region: 'center',
                        activeTab: 0,
                        anchor: '100% 53%',
                        border: false,
                        plain: true,
                        defaults:
                                {
                                    autoScroll: true
                                },
                        items: [
                            GetDTLTRGrid_panatajasarwiSecond(),
                            GetDTLTRPelunasanGrid(), 
                            GetDTLTRGridRekapitulasiTindakanPelunasan_RWI(),
                                    //-------------- ## --------------
                        ],
                        listeners:
                                {
                                }
                    }

            );
    var FormDepanPelunasanPasienRWI = new Ext.Panel
            (
                    {
                        id: mod_id,
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        title: 'Pelunasan Pasien',
                        border: false,
                        shadhow: true,
                        iconCls: 'Request',
                        margins: '0 5 5 0',
                        items: [
                                {
                                    xtype: 'panel',
                                    plain: true,
                                    activeTab: 0,
                                    height: 150,
                                    defaults:
                                            {
                                                bodyStyle: 'padding:10px',
                                                autoScroll: true
                                            },
                                    items: [
                                        {
                                            layout: 'form',
                                            margins: '0 5 5 0',
                                            border: true,
                                            items:
                                                    [
                                                        {
                                                            xtype: 'textfield',
                                                            fieldLabel: ' No. Medrec' + ' ',
                                                            id: 'txtFilterNomedrec_LunasPasienrwi',
                                                            anchor: '70%',
                                                            onInit: function () { },
                                                            listeners:
                                                                    {
                                                                        'specialkey': function ()
                                                                        {
                                                                            var tmpNoMedrec = Ext.get('txtFilterNomedrec_LunasPasienrwi').getValue()
                                                                            if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 )
                                                                            {
                                                                                if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                                                                {

                                                                                    var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec_LunasPasienrwi').getValue())
                                                                                    Ext.getCmp('txtFilterNomedrec_LunasPasienrwi').setValue(tmpgetNoMedrec);
                                                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    RefreshDataFilterPelunasanPasienRWIPelunasan();

                                                                                } else
                                                                                {
                                                                                    if (tmpNoMedrec.length === 10)
                                                                                    {
                                                                                        RefreshDataFilterPelunasanPasienRWIPelunasan();
                                                                                    } else
                                                                                        Ext.getCmp('txtFilterNomedrec_LunasPasienrwi').setValue('')
                                                                                }
                                                                            }
                                                                            cek_verified();
                                                                        }

                                                                    }
                                                        },
                                                        {
                                                            xtype: 'tbspacer',
                                                            height: 3
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            fieldLabel: ' Pasien' + ' ',
                                                            id: 'TxtFilterGridDataView_NAMA_viPelunasanPasienRWIPelunasan',
                                                            anchor: '70%',
                                                            enableKeyEvents: true,
                                                            listeners:
                                                                    {
                                                                        'specialkey': function ()
                                                                        {
                                                                            if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)// || tmpNoMedrec.length === 10)
                                                                            {
                                                                            RefreshDataFilterPelunasanPasienRWIPelunasan();
                                                                            }
                                                                        },
                                                                    }
                                                        },
                                                        {
                                                            xtype: 'tbspacer',
                                                            height: 3
                                                        },
                                                        getItemPanelcombofilter(),
                                                        getItemPaneltglmasukfilter(),
                                                        getItemPaneltglkeluarfilter(),
                                                        
                                                    ]}
                                    ]},
                                grListTRPelunasanPasienRWI, 
                                GDtabDetailrwi, 
                                LegendViewCMRequest
                            ],
                    }
            );
    return FormDepanPelunasanPasienRWI;
};

function GetDTLTRPelunasanGrid()
{
    var fldDetail = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'KD_UNIT'];
    dsTRDetailPelunasanList = new WebApp.DataStore({fields: fldDetail})
    var gridDTLTRPelunasan = new Ext.grid.EditorGridPanel
        (
            {
                title: 'Pelunasan Bayar',
                stripeRows: true,
                store: dsTRDetailPelunasanList,
                border: false,
                columnLines: true,
                frame: false,
                height: 400,
                anchor: '100%',
                autoScroll: true,
                sm: new Ext.grid.CellSelectionModel
                        (
                            {
                                singleSelect: true,
                                listeners:
                                        {
                                            cellselect: function (sm, row, rec)
                                            {
                                                // DISINI
                                                /*console.log(sm);
                                                console.log(row);*/
                                                indexRow = row;
                                                // console.log(dsTRDetailPelunasanList.data.items[row]);
                                            }
                                        }
                            }
                        ),
                cm: TRPelunasanColumModel(),
                tbar:[
                    {
                        text: 'Hapus',
                        id: 'btnHapusPelunasanrwi',
                        iconCls: 'hapus',
                        disabled: false,
                        handler: function ()
                        {
                            //if (statusHitung == false) {
                                // hitungtotaltagihanPelunasanrwi();
                            //}
                            //
                            var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function (btn, combo) {
                                if (btn == 'ok')
                                {
                                    variablehistori = combo;
                                    if (variablehistori != '')
                                    {
                                        delete_pembayaran(variablehistori);
                                    } else
                                    {
                                        ShowPesanWarningPelunasanrwi('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

                                    }
                                }

                            });
                            // Ext.getCmp('btnEditPelunasanrwi').disable();
                        }
                    },
                ],
                viewConfig: {forceFit: true},
            }
        );
    return gridDTLTRPelunasan;
};

function TRPelunasanColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'colKdUnitbayar_Infopasien',
                            header: 'KD Unit',
                            dataIndex: 'KD_UNIT',
                            //hidden: true,
                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Bayar',
                            dataIndex: 'URUT',

                        }

                        ,
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        }
                        ,
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }

                        }
                        ,
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true

                        }
                        ,
                        {
                            id: 'colStatPelunasan',
                            header: 'Pelunasan',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true




                        },
                        {
                            id: 'colPetugasPelunasan',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME'
                        }
                    ]
                    )
};

/* 
    CREATE  : HADAD AL GOJALI
    TGL     : 05-05-2017
    KET     : REKAPITULASI TINDAKAN RWI

 */
 function GetDTLTRGridRekapitulasiTindakanPelunasan_RWI() {

    var fldDetail = ['kd_produk', 'kd_unit', 'deskripsi', 'qty', 'dokter', 'tgl_tindakan', 'qty', 'desc_req', 'tgl_berlaku', 
            'no_transaksi', 'urut', 'desc_status', 'tgl_transaksi', 'kd_tarif', 'harga', 'jumlah_dokter', 'tarif', 'no_faktur', 'folio'];
    dsRekapitulasiTindakanPelunasan_RWI = new WebApp.DataStore({fields: fldDetail});
    
    var GridRekapitulasiTindakanPelunasan_RWI = new Ext.grid.EditorGridPanel({
        title: 'Rekapitulasi Tindakan Yang Diberikan',
        id: 'GridRekapTindakanPelunasan_RWI',
        stripeRows: true,
        height: 130,
        store: dsRekapitulasiTindakanPelunasan_RWI,
        border: false,
        frame: false,
        columnLines: true,
        anchor: '100% 100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
                   
                }
            }
        }),
        cm: TRRekapitulasiTindakanPelunasan_RWIColumnModel(),
        viewConfig: {forceFit: true},
    });
    return GridRekapitulasiTindakanPelunasan_RWI;
};

function TRRekapitulasiTindakanPelunasan_RWIColumnModel() {
    return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            id: 'colKdUnitRekapitulasiPelunasan_RWI',
            header: 'Unit',
            dataIndex: 'kd_unit',
            width: 75,
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colReffRekapitulasiPelunasan_RWI',
            header: 'Reff',
            dataIndex: 'no_faktur',
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colKdProdukRekapitulasiPelunasan_RWI',
            header: 'Kode Produk',
            dataIndex: 'kd_produk',
            width: 100,
            menuDisabled: true
        },
        {
            id: 'colDeskripsiRekapitulasiPelunasan_RWI',
            header: 'Nama Produk',
            dataIndex: 'deskripsi',
            sortable: false,
            hidden: false,
            menuDisabled: true,
            width: 250
        },{
            id: 'colHARGARekapitulasiPelunasan_RWI',
            header: 'Jumlah',
            align: 'right',
            hidden: false,
            menuDisabled: true,
            dataIndex: 'harga',
            renderer: function (v, params, record)
            {
                return formatCurrency(record.data.harga);

            }
        },
        {
            id: 'colQTYRekapitulasiPelunasan_RWI',
            header: 'Qty',
            width: 50,
            align: 'left',
            menuDisabled: true,
            dataIndex: 'qty'
        },

    ]);
};


function RefreshRekapitulasiTindakanPelunasan_RWI(no_transaksi,kd_Pelunasan){
    Ext.Ajax.request
    ({
        url: baseURL + "index.php/main/functionRWI/getRekapitulasiTindakan",
        params: {
            no_transaksi:no_transaksi,
            kd_Pelunasan:kd_Pelunasan,
            co_status:true,
        },
        failure: function (o)
        {
            ShowPesanErrorPelunasanrwi('Error membaca rekapitulasi tindakan. Hubungi Admin!', 'WARNING');
        },
        success: function (o)
        {
            var cst = Ext.decode(o.responseText);
            if (cst.success === true)
            {
                dsRekapitulasiTindakanPelunasan_RWI.removeAll();
                var recs=[],
                    recType=dsRekapitulasiTindakanPelunasan_RWI.recordType;
                    
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                dsRekapitulasiTindakanPelunasan_RWI.add(recs);
                //GridRekapitulasiTindakanPelunasanRWI.getView().refresh();
            } else{
                ShowPesanWarningPelunasanrwi('Gagal membaca rekapitulasi tindakan!', 'WARNING');
            }
        }
    })
}

function GetDTLTRGrid_panatajasarwiSecond() {

    var fldDetail = ['KD_PRODUK','KP_PRODUK', 'KD_UNIT', 'DESKRIPSI', 'QTY', 'DOKTER', 'TGL_TINDAKAN', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 
            'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'KD_TARIF', 'HARGA', 'JUMLAH_DOKTER', 'TARIF', 'NO_FAKTUR', 'FOLIO'];
    dsTRDetailPelunasanPasienRWIList_panatajasarwi = new WebApp.DataStore({fields: fldDetail});
    PenataJasaPelunasanPasienRWI.form.Grid.produk = new Ext.grid.EditorGridPanel({
        title: 'Tindakan Yang Diberikan',
        id: 'PjTransGrid2',
        stripeRows: true,
        height: 130,
        //plugins: [editor],
        store: dsTRDetailPelunasanPasienRWIList_panatajasarwi,
        border: false,
        frame: false,
        columnLines: true,
        anchor: '100% 100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true
        }),
        cm: TRRawatInapColumModel2_InfoPasien(),
        viewConfig: {forceFit: true},
    });
    return PenataJasaPelunasanPasienRWI.form.Grid.produk;
};

function TRRawatInapColumModel2_InfoPasien() {
    return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            header      : 'Tanggal Transaksi',
            dataIndex   : 'TGL_TRANSAKSI',
            width       : 130,
            menuDisabled: true
        },
        {
            id: 'colReffRWI',
            header: 'Reff',
            dataIndex: 'NO_FAKTUR',
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colKdUnitRWI',
            header: 'Unit',
            dataIndex: 'KD_UNIT',
            width: 75,
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colKdProduk_panatajasarwi2',
            header: 'Kode Produk',
            dataIndex: 'KP_PRODUK',
            width: 100,
            menuDisabled: true
        },
        {
            id: 'colDeskripsiRWI2',
            header: 'Nama Produk',
            dataIndex: 'DESKRIPSI',
            sortable: false,
            hidden: false,
            menuDisabled: true,
            width: 250
        },{
            id: 'colHARGA_penatajasa2',
            header: 'Harga',
            align: 'right',
            hidden: true,
            menuDisabled: true,
            dataIndex: 'HARGA',
            renderer: function (v, params, record)
            {
                return formatCurrency(record.data.HARGA);

            }
        },
        {
            id: 'coljmlhdokRWI2',
            header: 'Dok',
            width: 50,
            dataIndex: 'JUMLAH_DOKTER',
            hidden: false
        },
        {
            id: 'colQTY_penatajasa2',
            header: 'Qty',
            width: 50,
            align: 'right',
            menuDisabled: true,
            dataIndex: 'QTY'
        },
        {
            id: 'colTarifRWI',
            header: 'Tarif',
            dataIndex: 'TARIF',
            align: 'right',
            hidden: false,
            renderer: function (v, params, record)
            {
                return formatCurrency(record.data.TARIF);

            }
        },
        {
            id: 'colFolioRWI',
            header: 'Folio',
            dataIndex: 'FOLIO',
            hidden: false,
        },
        {
            id: 'xbu',
            header: 'xbu',
            align: 'right',
            hidden: true,
            menuDisabled: true,
            renderer: function(v, params, record)
                {
                    var a='';
                    //console.log(record.data.NO_TRANSAKSI);
                    if (record.data.NO_TRANSAKSI !== '')
                    {
                        //getVisiteDokterDiawal(record.data.NO_TRANSAKSI, record.data.TGL_TRANSAKSI, record.data.KD_PRODUK,record.data.URUT);
                    }
                }

        }, 

    ]
            );
}
;

function getItemPanelcombofilter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            ComboKelasUnit_PelunasanPasienRWI()
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                       [
                                            ComboKamar_PelunasanPasienRWI(),
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getItemPaneltglmasukfilter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal Masuk',
                                                id: 'dtpTglAwalFilterPelunasanPasienRWI',
                                                name: 'dtpTglAwalFilterPelunasanPasienRWI',
                                                format: 'd/M/Y',
                                                value: sebulanyanglalu,
                                                width: '100px',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterPelunasanPasienRWIPelunasan();
                                                                }
                                                            }
                                                        }
                                            },
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                       [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 's/d',
                                                id: 'dtpTglAkhirFilterPelunasanPasienRWI',
                                                name: 'dtpTglAkhirFilterPelunasanPasienRWI',
                                                format: 'd/M/Y',
                                                value: now,
                                                width: '100px',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterPelunasanPasienRWIPelunasan();
                                                                }
                                                            }
                                                        }
                                            },
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getItemPaneltglkeluarfilter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal Keluar ',
                                                id: 'dtpTglkeluarAwalFilterPelunasanPasienRWI',
                                                name: 'dtpTglkeluarAwalFilterPelunasanPasienRWI',
                                                format: 'd/M/Y',
                                                value: sebulanyanglalu,
                                                width: '100px',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterPelunasanPasienRWIPelunasan();
                                                                }
                                                            }
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                       [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 's/d',
                                                id: 'dtpTglkeluarAkhirFilterPelunasanPasienRWI',
                                                name: 'dtpTglkeluarAkhirFilterPelunasanPasienRWI',
                                                format: 'd/M/Y',
                                                value: now,
                                                width: '100px',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterPelunasanPasienRWIPelunasan();
                                                                }
                                                            }
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
};

var CurrentNoKamar_PelunasanPasienRWI;
function ComboKamar_PelunasanPasienRWI(){
    dsKamar_PelunasanPasienRWI = new WebApp.DataStore({fields: ['no_kamar','nama_kamar']})
    loadKamar_PelunasanPasienRWI();
    cboKamar_PelunasanPasienRWI = new Ext.form.ComboBox
    (
        {
            id: 'cboKamar_PelunasanPasienRWI',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            fieldLabel: 'Kamar ',
            store: dsKamar_PelunasanPasienRWI,
            valueField: 'no_kamar',
            displayField: 'nama_kamar',
            width:150,
            listeners:
            {
                'select': function(a, b, c)
                {
                    CurrentNoKamar_PelunasanPasienRWI=b.data.no_kamar;
                    RefreshDataFilterPelunasanPasienRWIPelunasan()
                }
            }
        }
    )
    return cboKamar_PelunasanPasienRWI;
} 
var CurrentKdUnit_PelunasanPasienRWI;
function ComboKelasUnit_PelunasanPasienRWI(){
    dsKelasUnit_PelunasanPasienRWI = new WebApp.DataStore({fields: ['kd_unit','nama_unit']})
    loadKelasUnit_PelunasanPasienRWI();
    cboKelasUnit_PelunasanPasienRWI = new Ext.form.ComboBox
    (
        {
            id: 'cboKelasUnit_PelunasanPasienRWI',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            fieldLabel: 'Kelas ',
            store: dsKelasUnit_PelunasanPasienRWI,
            valueField: 'kd_unit',
            displayField: 'nama_unit',
            width:150,
            listeners:
            {
                'select': function(a, b, c)
                {
                    Ext.getCmp('cboKamar_PelunasanPasienRWI').setValue('');
                    loadKamar_PelunasanPasienRWI(b.data.kd_unit);
                    CurrentKdUnit_PelunasanPasienRWI=b.data.kd_unit;
                    RefreshDataFilterPelunasanPasienRWIPelunasan()
                }
            }
        }
    )
    return cboKelasUnit_PelunasanPasienRWI;
} 

function loadKelasUnit_PelunasanPasienRWI(param){
    if (param==='' || param===undefined) {
        param={
            text: '0',
        };
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/rawat_inap/viewgridinfopasienrwi/getunitkelas",
        params: {
            text:param,
        },
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            dsKelasUnit_PelunasanPasienRWI.removeAll();
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = dsKelasUnit_PelunasanPasienRWI.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                dsKelasUnit_PelunasanPasienRWI.add(recs);
                //console.log(o);
            }
        }
    });
};

function loadKamar_PelunasanPasienRWI(param){
    if (param==='' || param===undefined) {
        param={
            text: '0',
        };
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/rawat_inap/viewgridinfopasienrwi/getkamar",
        params: {
            text:param,
        },
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            dsKamar_PelunasanPasienRWI.removeAll();
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = dsKamar_PelunasanPasienRWI.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                dsKamar_PelunasanPasienRWI.add(recs);
                console.log(o);
            }
        }
    });
};


function RefreshDataPelunasanPasienRWIPelunasan()
{
    var KataKunci = " t.CO_Status = 'T' AND t.Kd_Unit IN (SELECT Kd_Unit FROM Unit WHERE Parent in ('1001','1002','1003','1'))";
    //KataKunci += "AND K.TGL_MASUK >= '"+sebulanyanglalu.format("d/M/Y")+"' and K.TGL_MASUK <= '"+now.format("d/M/Y")+"' AND t.Tgl_CO >= '"+sebulanyanglalu.format("d/M/Y")+"' and t.Tgl_CO  <= '"+now.format("d/M/Y")+"' and ng.akhir = 't' limit 50";
    KataKunci += "AND  K.TGL_MASUK <= '"+now.format("d/M/Y")+"' AND t.Tgl_CO >= '"+sebulanyanglalu.format("d/M/Y")+"' and t.Tgl_CO  <= '"+now.format("d/M/Y")+"' and ng.akhir = 't' limit 50";
    dsTRPelunasanPasienRWIPelunasanList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountPelunasanPasienRWIPelunasan,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'DESC',
                                    target: 'ViewPelunasanPasienRWI',
                                    param: KataKunci
                                }
                    }
            );

    return dsTRPelunasanPasienRWIPelunasanList;
}
;

function loadDataComboUserLapPerincianPasienRWIPelunasan(param){
    if (param==='' || param===undefined) {
        param='';
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRWI/kelaskamar",
        params: {kode:param},
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            cbokelaskamarrwi.store.removeAll();
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = dsget_data_kamarrwi.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                dsget_data_kamarrwi.add(recs);
                //console.log(o);
            }
        }
    });
}

function RefreshDatahistoribayar(no_transaksi)
{
    var strKriteriaPelunasanPasienRWI = '';

    strKriteriaPelunasanPasienRWI = 'no_transaksi= ~' + no_transaksi + '~ ';

    dsTRDetailPelunasanList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewPelunasanBayarRWI',
                                    param: strKriteriaPelunasanPasienRWI
                                }
                    }
            );
    return dsTRDetailPelunasanList;
}
;

function RefreshDataDetail_PelunasanPasienRWI(no_transaksi)
{
    var strKriteriaRWI = '';
    strKriteriaRWI = "\"no_transaksi\" = ~" + no_transaksi + "~";

    dsTRDetailPelunasanPasienRWIList_panatajasarwi.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailRWIGridBawah_REVISI',
                                    param: strKriteriaRWI
                                }
                    }
            );
    return dsTRDetailPelunasanPasienRWIList_panatajasarwi;
}
;

function RefreshDataFilterPelunasanPasienRWIPelunasan()
{

    var KataKunci = "t.CO_Status = 'T' ";
    if (Ext.get('txtFilterNomedrec_LunasPasienrwi').getValue() != '')
    {
         KataKunci += " and  t.kd_pasien =  '" + Ext.get('txtFilterNomedrec_LunasPasienrwi').getValue() + "'";

    };

    if (Ext.get('TxtFilterGridDataView_NAMA_viPelunasanPasienRWIPelunasan').getValue() != '')
    {
        
            KataKunci += ' and  LOWER(p.nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viPelunasanPasienRWIPelunasan').getValue() + '%~)';
    };

    KataKunci += "AND t.Kd_Unit IN (SELECT Kd_Unit FROM Unit WHERE Parent in ('1001','1002','1003')) ";
    //KataKunci += "AND K.TGL_MASUK >= '"+Ext.get('dtpTglAwalFilterPelunasanPasienRWI').getValue()+"' and K.TGL_MASUK <= '"+Ext.get('dtpTglAkhirFilterPelunasanPasienRWI').getValue()+"' ";
    KataKunci += "AND K.TGL_MASUK >= '"+Ext.get('dtpTglAwalFilterPelunasanPasienRWI').getValue()+"' and K.TGL_MASUK <= '"+Ext.get('dtpTglAkhirFilterPelunasanPasienRWI').getValue()+"' ";
    KataKunci += "AND t.Tgl_CO >= '"+Ext.get('dtpTglkeluarAwalFilterPelunasanPasienRWI').getValue()+"' and t.Tgl_CO  <= '"+Ext.get('dtpTglkeluarAkhirFilterPelunasanPasienRWI').getValue()+"' and ng.akhir = 't' order by k.TGL_MASUK desc";
    loadMask.show();


   if (Ext.get('cboKelasUnit_PelunasanPasienRWI').getValue() != ''){
       
            KataKunci += ' and t.Kd_Unit =  ~'+ CurrentKdUnit_PelunasanPasienRWI + '~';
    };

    if (Ext.get('cboKamar_PelunasanPasienRWI').getValue() != ''){
       
            KataKunci += ' and q.no_kamar =  ~'+ CurrentNoKamar_PelunasanPasienRWI + '~';
    };

    if (KataKunci != undefined || KataKunci != '')
    {
        dsTRPelunasanPasienRWIPelunasanList.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCountPelunasanPasienRWIPelunasan,
                                        Sort: 'tgl_transaksi',
                                        Sortdir: 'DESC',
                                        target: 'ViewPelunasanPasienRWI',
                                        param: KataKunci
                                    },
                            callback: function(){
                                loadMask.hide();
                            }
                        }
                );
    } else
    {
        dsTRPelunasanPasienRWIPelunasanList.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCountPelunasanPasienRWIPelunasan,
                                        Sort: 'tgl_transaksi',
                                        Sortdir: 'DESC',
                                        target: 'ViewPelunasanPasienRWI',
                                        param: KataKunci
                                    },
                            callback: function(){
                                loadMask.hide();
                            }
                        }
                );
    }
    ;

    return dsTRPelunasanPasienRWIPelunasanList;
};

/*-------------------------------Penambahan Cetak Billing-------------------------------------------
Oleh    : HDHT
Tanggal : 16 Maret 2017
Tempat  : Bandung
---------------------------------------------------------------------------------------------------*/

function fnDlgRWICetakBilling(data)
{
    winRWICetakBilling = new Ext.Window
    (
        {
            id: 'winRWICetakBilling',
            title: 'Cetak Billing',
            closeAction: 'destroy',
            width:314,
            height: 300,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'cost',
            modal: true,
            items: [ItemDlgRWICetakBilling()],
            listeners:{
                 'show':function(win){
                        Ext.getCmp('dtpTanggalawalTransaksiCetakBillRWI').hide();
                        Ext.getCmp('dtpTanggalakhirTransaksiCetakBillRWI').hide();
                        Ext.getCmp('lbltanggalawal1').hide();
                        Ext.getCmp('lbltanggalawal2').hide();
                        Ext.getCmp('lbltanggalawal3').hide();
                        Ext.getCmp('cboJenisPrintPelunasanPasienRWI').setValue('Billing');
                        // console.log(data);
                        Ext.getCmp('TxtCariNamaPembayaran').setValue(data.NAMA);
                        Ext.getCmp('cboSpesialisasicetakbillPelunasanPasienRWI').setValue('Semua Spesialisasi');
                        tmpcetakkwitansi = 'true';
                  }

            },
            fbar:[
                {
                    id: 'btnOkLookupCetakBillingPelunasanPasienRWI',
                    text: 'OK',
                    handler: function (sm, row, rec)
                    {      
                        // if (Ext.getCmp('cboJenisPrintPelunasanPasienRWI').getValue() === 'Billing') {
                            var Spesialisasi = "";
                            var nama_Spesialisasi = "";
                                if (Ext.getCmp('cboSpesialisasicetakbillPelunasanPasienRWI').getValue() === 'Semua Spesialisasi')
                                {
                                    Spesialisasi = 'NULL';
                                    nama_Spesialisasi = 'Semua Spesialisasi';
                                }else{
                                    Spesialisasi = kdspesialisasi;
                                    nama_Spesialisasi = namaSpesialisasi;
                                }
                                var params={
                                            Spesialisasi    : Spesialisasi, //split[2]
                                            dtlspesialisasi : nama_Spesialisasi,
                                            Tglpulang       : Ext.getCmp('dtpTanggalPulang').getValue(), //split[0]
                                            jenisprint      : Ext.getCmp('cboJenisPrintPelunasanPasienRWI').getValue(), //split[1]
                                            folio           : tmpcgfolio,//split[5]
                                            notrans         : data.NO_TRANSAKSI, //split[3]
                                            KdKasir         : data.KD_KASIR, //split[3]
                                            KdPelunasan     : data.KD_Pelunasan,
                                            nama            : data.NAMA,
                                            kdpasien        : data.KD_PASIEN,
                                            reff            : Ext.getCmp('TxtCariReferensi').getValue(), //split[6]
                                            TglMasuk        : data.TGL_MASUK,
                                            kdunit          : data.KD_UNIT,
                                            kdunit_kamar    : data.KD_UNIT_KAMAR,
                                            costat          : 'true',
                                            cetakan         : Ext.getCmp('cboJenisPrintPelunasanPasienRWI').getValue(),
                                            cetakkwitansi   : Ext.getCmp('cbCetakWitansi').getValue(),
                                            detailData      : Ext.getCmp('cbDetailData').getValue(),
                                };
                                Ext.Ajax.request({
                                //url: baseURL + "index.php/rawat_inap/lap_bill_Pelunasan_rwi/Cetak",
                                url: baseURL + "index.php/rawat_inap/lap_bill_kwitansi_lunas/Cetak",
                                params: params,
                                success: function(o){
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true){
                                        ShowPesanInfoPelunasanPasienRWI('Sedang di Print.','Information');
                                    }else if  (cst.success === false && cst.pesan===0){
                                        ShowPesanWarningPelunasanPasienRWI('Data tidak berhasil di Print '  + cst.pesan,'Print Data');
                                    }else{
                                        ShowPesanErrorPelunasanPasienRWI('Data tidak berhasil di Print '  + cst.pesan,'Print Data');
                                    }
                                }
                            });
                            // var form = document.createElement("form");
                            // form.setAttribute("method", "post");
                            // form.setAttribute("target", "_blank");
                            // form.setAttribute("action", baseURL + "index.php/rawat_inap/lap_bill_Pelunasan_rwi/cetakkwitansi");
                            // var hiddenField = document.createElement("input");
                            // hiddenField.setAttribute("type", "hidden");
                            // hiddenField.setAttribute("name", "data");
                            // hiddenField.setAttribute("value", Ext.encode(params));
                            // form.appendChild(hiddenField);
                            // document.body.appendChild(form);
                            // form.submit();  
                            // frmDlgRWJPerLaporandetail.close();
                        // }else{
                        //     printbillRwi(data);
                        // }               
                        
                    }
                },
                {
                    id: 'btnCancelLookupCetakBillingPelunasanPasienRWI',
                    text: 'Batal',
                    handler: function (sm, row, rec)
                    {
                        winRWICetakBilling.close();
                    }
                }
            
            ]

        }
    );
    winRWICetakBilling.show();
    
    // selectDataCetakBillingPelunasanPasienRWI(data);
};

function fnDlgRWICetakKwitansi(data)
{
    winRWICetakKwitansi = new Ext.Window
    (
        {
            id: 'winRWICetakKwitansi',
            title: 'Cetak Kwitansi',
            closeAction: 'destroy',
            width:314,
            height: 300,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'cost',
            modal: true,
            items: [GetDTLTRRPelunasanGrid()],
            listeners:{
            },
            fbar:[
                {
                    id: 'btnOkLookupCetakKwitansiPelunasanPasienRWI',
                    text: 'OK',
                    handler: function (sm, row, rec)
                    { 
                        var gridKwitansi    = Ext.getCmp("idPelunasanBayarKwitansi").store.data;
                        var tmp_urut        = "";
                        var index_urut      = 0;
                        // console.log(gridKwitansi);
                        for (var i = 0; i < gridKwitansi.length; i++) {
                            if (gridKwitansi.items[i].data.TAG == true) {
                                tmp_urut += "'"+gridKwitansi.items[i].data.URUT+"',";
                                index_urut++;
                            }
                        }

                        if (index_urut > 1) {
                            ShowPesanWarningPelunasanrwi("Cetak kwitansi hanya untuk 1 kali pembayaran", "Peringatan");
                        }else{
                            Ext.Ajax.request
                            (
                                    {
                                        url: baseURL + "index.php/rawat_inap/lap_bill_kwitansi_lunas/cetakkwitansi_lunas",
                                        params: {
                                            no_transaksi      : rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI,
                                            kd_unit           : rowSelectedPelunasanPasienRWIPelunasan.data.KD_UNIT,
                                            urut              : tmp_urut,
                                            kd_kasir          : '02',
                                        },
                                        success: function (o)
                                        {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {

                                            }else{
                                                // ShowPesanErrorPelunasanPasienRWI( cst.pesan, 'Cetak Kwitansi');
                                            }
                                        }
                                    }
                            );
                        }
                    }
                },
                {
                    id: 'btnCancelLookupCetakKwitansiPelunasanPasienRWI',
                    text: 'Batal',
                    handler: function (sm, row, rec)
                    {
                        winRWICetakKwitansi.close();
                    }
                }
            
            ]

        }
    );
    winRWICetakKwitansi.show();
};

var tmpcgfolio = '';
var tmpcetakan = 'Billing';
var tmpcetakkwitansi = 'true';
function ItemDlgRWICetakBilling(){
 var items = new Ext.Panel({
        bodyStyle:'padding: 0px 0px 0px 0px',
        layout:'form',
        border:false,
        autoScroll: true,
        items:[
            {
                xtype : 'fieldset',
                title : '',
                layout: 'absolute',
                width : 300,
                height : 225,
                border: true,
                items:[
                    {
                        x: 0,
                        y: 0,
                        xtype: 'label',
                        text: 'Jenis '
                    }, {
                        x: 90,
                        y: 0,
                        xtype: 'label',
                        text: ' : '
                    },
                   mComboJenisPrintPelunasanPasienRWI(),
                    {
                        x: 0,
                        y: 30,
                        xtype: 'label',
                        text: 'Tanggal Pulang '
                    }, {
                        x: 90,
                        y: 30,
                        xtype: 'label',
                        text: ' : '
                    },
                   {
                        x: 100,
                        y: 30,
                        xtype: 'datefield',
                        name: 'dtpTanggalPulang',
                        id: 'dtpTanggalPulang', 
                        format: 'd/M/Y',
                        value: now,
                        width: 122
                    },
                    {
                        x: 0,
                        y: 60,
                        xtype: 'label',
                        text: 'Pembayaran'
                    }, 
                    {
                        x: 90,
                        y: 60,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 60,
                        xtype: 'textfield',
                        name: 'TxtCariNamaPembayaran',
                        id: 'TxtCariNamaPembayaran',
                        width: 170,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                            // load_pengkajian(tmpkriteriaPengkajian);
                                        }
                                    }
                                }
                    },
                    {
                        x: 0,
                        y: 90,
                        xtype: 'label',
                        text: 'Referensi'
                    }, 
                    {
                        x: 90,
                        y: 90,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 90,
                        xtype: 'textfield',
                        name: 'TxtCariReferensi',
                        id: 'TxtCariReferensi',
                        width: 170,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                            // load_pengkajian(tmpkriteriaPengkajian);
                                        }
                                    }
                                }
                    },
                    {
                        x: 0,
                        y: 120,
                        xtype: 'label',
                        text: 'Cetak Kwitansi',
                        id:'lblchc1'
                    },
                    {
                        x: 90,
                        y: 120,
                        xtype: 'label',
                        text: ' : ',
                        id:'lblchc2'
                    },
                    {
                        x: 100,
                        y: 120,
                        xtype: 'checkboxgroup',
                        boxMaxWidth: 20,
                        id:'checkboxkwitansi',
                        itemCls: 'x-check-group-alt',
                        columns: 1,
                        items: [
                            {boxLabel: '', name: 'cbCetakWitansi', id: 'cbCetakWitansi', checked: false, disabled:true}
                        ]
                    },
                    {
                        x: 0,
                        y: 150,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 90,
                        y: 150,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboRWISpesialisasicetakbillPelunasanPasienRWI(),
                    {
                        x: 0,
                        y: 180,
                        xtype: 'label',
                        text: 'Tanggal Transaksi ',
                        id:'lbltanggalawal1'
                    },
                    {
                        x: 90,
                        y: 180,
                        xtype: 'label',
                        text: ' : ',
                        id:'lbltanggalawal2'
                    },
                    {
                        x: 100,
                        y: 180,
                        xtype: 'datefield',
                        name: 'dtpTanggalawalTransaksiCetakBillRWI',
                        id: 'dtpTanggalawalTransaksiCetakBillRWI', 
                        
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 210,
                        y: 185,
                        xtype: 'label',
                        text: 'S/D',
                        id:'lbltanggalawal3'

                    },
                    {
                        x: 240,
                        y: 180,
                        xtype: 'datefield',
                        name: 'dtpTanggalakhirTransaksiCetakBillRWI',
                        id: 'dtpTanggalakhirTransaksiCetakBillRWI', 
                        
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 0,
                        y: 180,
                        xtype: 'label',
                        text: 'Detail data',
                        id:'checkDetail'
                    },
                    {
                        x: 90,
                        y: 180,
                        xtype: 'label',
                        text: ' : ',
                        id:'checkDetailLabel'
                    },
                    {
                        x: 100,
                        y: 180,
                        xtype: 'checkboxgroup',
                        boxMaxWidth: 20,
                        id:'checkboxDetailData',
                        itemCls: 'x-check-group-alt',
                        columns: 1,
                        items: [
                            {boxLabel: '', name: 'cbDetailData', id: 'cbDetailData', checked: false}
                        ],
                    },
                ]
            }
        ]
    })
    return items;
}

var selectSetJenisPrinter;
function mComboJenisPrintPelunasanPasienRWI()
{
    var cboJenisPrintPelunasanPasienRWI = new Ext.form.ComboBox
        (
            {
                x: 100,
                y: 0,
                id: 'cboJenisPrintPelunasanPasienRWI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                selectOnFocus: true,
                forceSelection: true,
                emptyText: '',
                width: 170,
                store: new Ext.data.ArrayStore
                        (
                                {
                                    id: 0,
                                    fields:
                                            [
                                                'Id',
                                                'displayText'
                                            ],
                                    // data: [[0, 'Billing'], [1, 'Kwitansi']]
                                    data: [[0, 'Billing']]
                                }
                        ),
                valueField: 'Id',
                displayField: 'displayText',
                value: selectSetJenisPrinter,
                listeners:
                        {
                            'select': function (a, b, c)
                            {
                                if(b.data.displayText === 'Kwitansi')
                                {
                                    tmpcetakan = b.data.displayText;
                                    Ext.getCmp('dtpTanggalawalTransaksiCetakBillRWI').hide();
                                    Ext.getCmp('dtpTanggalakhirTransaksiCetakBillRWI').hide();
                                    Ext.getCmp('lbltanggalawal1').hide();
                                    Ext.getCmp('lbltanggalawal2').hide();
                                    Ext.getCmp('lbltanggalawal3').hide();
                                    Ext.getCmp('lblchc1').hide();
                                    Ext.getCmp('lblchc2').hide();
                                    Ext.getCmp('checkboxkwitansi').hide();
                                    tmpcetakkwitansi = 'false';

                                }else{
                                    Ext.getCmp('dtpTanggalawalTransaksiCetakBillRWI').hide();
                                    Ext.getCmp('dtpTanggalakhirTransaksiCetakBillRWI').hide();
                                    Ext.getCmp('lbltanggalawal1').hide();
                                    Ext.getCmp('lbltanggalawal2').hide();
                                    Ext.getCmp('lbltanggalawal3').hide();
                                    Ext.getCmp('lblchc1').show();
                                    Ext.getCmp('lblchc2').show();
                                    Ext.getCmp('checkboxkwitansi').show();
                                }
                            }
                        }
            }
        );
    return cboJenisPrintPelunasanPasienRWI;
}
;

var ds_SpesialisasicetakbillPelunasanPasienRWI;
var kdspesialisasi;
var namaSpesialisasi;
function mComboRWISpesialisasicetakbillPelunasanPasienRWI()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];

    ds_SpesialisasicetakbillPelunasanPasienRWI = new WebApp.DataStore({fields: Field});
    ds_SpesialisasicetakbillPelunasanPasienRWI.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_spesial',
            Sortdir: 'ASC',
            target: 'ViewSetupSpesial',
            param: 'kd_spesial <> 0'
        }
    })

    var cboSpesialisasicetakbillPelunasanPasienRWI = new Ext.form.ComboBox
    ({
        x:100,
        y:150,
        id: 'cboSpesialisasicetakbillPelunasanPasienRWI',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        selectOnFocus: true,
        forceSelection: true,
        emptyText: 'Pilih Spesialisasi...',
        fieldLabel: 'Spesialisasi ',
        align: 'Right',
        store: ds_SpesialisasicetakbillPelunasanPasienRWI,
        valueField: 'KD_SPESIAL',
        displayField: 'SPESIALISASI',
        width : 170,
        listeners:
        {
            'select': function (a, b, c)
            {
                kdspesialisasi = b.data.KD_SPESIAL;
                namaSpesialisasi = b.data.SPESIALISASI;
            },
            'render': function (c)
            {
                // c.getEl().on('keypress', function (e) {
                //     if (e.getKey() == 13 || e.getKey() == 9) //atau Ext.EventObject.ENTER
                //         Ext.getCmp('cboKelas_panatajasarwi').focus();
                // }, c);
            }
        }
    })

    return cboSpesialisasicetakbillPelunasanPasienRWI;
}
;

function printbillRwi(data)
{
    Ext.Ajax.request
    (
            {
                url: baseURL + "index.php/main/CreateDataObj",
                params: dataparamcetakbill_viPelunasanDaftarRWI(data),
                success: function (o)
                {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {

                    } else if (cst.success === false && cst.pesan === 0)
                    {
                        ShowPesanWarningPelunasanPasienRWI(cst.pesan, 'Cetak Kwitansi');
                    } else
                    {
                        ShowPesanErrorPelunasanPasienRWI( cst.pesan, 'Cetak Kwitansi');
                    }
                }
            }
    );
};

function dataparamcetakbill_viPelunasanDaftarRWI(data)
{
    var paramscetakbill_viPelunasanDaftarRWI =
            {
                Table: 'DirectKwitansirwi',
                No_TRans         : data.NO_TRANSAKSI, //split[3]
                KdPelunasan         : data.KD_Pelunasan,
            };
    return paramscetakbill_viPelunasanDaftarRWI;
}
;


function ShowPesanWarningPelunasanPasienRWI(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
};

function ShowPesanErrorPelunasanPasienRWI(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 250
                    }
            );
};

function ShowPesanInfoPelunasanPasienRWI(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
};

/*
    =============================================================================================
                                        PREVIEW BILLING
    =============================================================================================
*/

    function GetDTLTRGridPreviewBilling() {
        var url = TmpUrl;
        new Ext.Window({
            title: 'Preview Billing',
            width: 1000,
            height: 600,
            constrain: true,
            modal: true,
            html: "<iframe  style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
        }).show();
    };

function PelunasanLookUprwi(rowdata)
{
    var lebar = 580;
    FormLookUpsdetailTRPelunasanrwi = new Ext.Window
    (
        {
            id: 'gridPelunasanrwi',
            title: 'Pelunasan Rawat Inap',
            closeAction: 'destroy',
            width: lebar,
            height: 480,
            border: false,
            resizable: false,
            plain: true,
            // layout: 'form',
            iconCls: 'Edit_Tr',
            modal: true,
            items: getFormEntryPelunasanrwi(lebar),
            listeners:
            {
            }
        }
    );

    FormLookUpsdetailTRPelunasanrwi.show();
    Ext.get('cboJenisByr').dom.value = "TUNAI"; // take the displayField value 
    Ext.get('cboPembayaranPelunasanRWI').dom.value = "TUNAI"; // take the displayField value 
    tapungkd_pay = "TU";
    /*if (rowdata == undefined)
    {
        PelunasanrwiAddNew();
        
    } else
    {
        TRPelunasanRwiInit(rowdata);
        // console.log(rowdata.LUNAS);
        if(rowdata.LUNAS == 'f'){
            getTotalBayarSummaryPelunasanRWI();
        } 
    }*/

};


function getFormEntryPelunasanrwi(lebar)
{
    var pnlTRPelunasanrwi = new Ext.FormPanel
    (
        {
            id: 'PanelTRPelunasanrwi',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height: 130,
            width: 570,
            border: false,
            items: [getItemPanelInputPelunasan(lebar)],
            tbar:
            [
            ]
        }
    );
    var x;
    var pnlTRPelunasanrwi2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRPelunasanrwi2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height: 380,
            width: 570,
            // anchor: '100%',
            border: false,
            items: [getTabPanelPembayaranSummaryDetailPelunasanRWI()]
        }
    );
    var FormDepanPelunasanrwi = new Ext.Panel
    (
        {
            id: 'FormDepanPelunasanrwi',
            region: 'center',
            // width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: true,
            bodyStyle: 'background:#FFFFFF;',
            height: 450,
            shadhow: true,
            items: [pnlTRPelunasanrwi, pnlTRPelunasanrwi2]
        }
    );
    return FormDepanPelunasanrwi
};


function getItemPanelInputPelunasan(lebar)
{
    var items =
            {
                width: 545,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: true,
                height: 110,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getItemPanelNoTransksirwiPelunasan(lebar), getItemPanelmedrecPelunasan(lebar), getItemPanelUnitPelunasan(lebar)
                                        ]
                            }
                        ]
            };
    return items;
};


function getItemPanelNoTransksirwiPelunasan(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Transaksi ',
                                                name: 'txtNoTransaksiPelunasanrwiLunas',
                                                id: 'txtNoTransaksiPelunasanrwiLunas',
                                                readOnly: true,
                                                value   : rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI,
                                                anchor: '99%',
                                            }
                                        ]
                            },
                            {
                                columnWidth: .58,
                                layout: 'form',
                                border: false,
                                labelWidth: 55,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTanggalDetransaksi',
                                                name: 'dtpTanggalDetransaksi',
                                                format: 'd/M/Y',
                                                readOnly: true,
                                                value: now,
                                                anchor: '100%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function getItemPanelmedrecPelunasan(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Medrec ',
                                                name: 'txtNoMedrecDetransaksi',
                                                id: 'txtNoMedrecDetransaksi',
                                                readOnly: true,
                                                value   : rowSelectedPelunasanPasienRWIPelunasan.data.KD_PASIEN,
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .58,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: '',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtNamaPasienDetransaksi',
                                                id: 'txtNamaPasienDetransaksi',
                                                value   : rowSelectedPelunasanPasienRWIPelunasan.data.NAMA,
                                                anchor: '100%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function getItemPanelUnitPelunasan(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboJenisByrView()

                                        ]
                            }, {
                                columnWidth: .58,
                                layout: 'form',
                                labelWidth: 0.9,
                                border: false,
                                items:
                                        [mComboPembayaran()

                                        ]
                            }
                        ]
            }
    return items;
}
;


function mComboJenisByrView()
{
    var Field = ['JENIS_PAY', 'DESKRIPSI', 'TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({fields: Field});
    dsJenisbyrView.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'jenis_pay',
                                    Sortdir: 'ASC',
                                    target: 'ComboJenis',
                                }
                    }
            );

    var cboJenisByr = new Ext.form.ComboBox
            (
                    {
                        id: 'cboJenisByr',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Pembayaran      ',
                        align: 'Right',
                        anchor: '100%',
                        store: dsJenisbyrView,
                        valueField: 'JENIS_PAY',
                        displayField: 'DESKRIPSI',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        // console.log(b);
                                        loaddatastorePembayaran(b.data.JENIS_PAY);
                                        tampungtypedata = b.data.TYPE_DATA;
                                        // jenispay = b.data.JENIS_PAY;
                                        // showCols(Ext.getCmp('gridDTLTRPelunasanrwinap'));
                                        // hideCols(Ext.getCmp('gridDTLTRPelunasanrwinap'));
                                        // getTotalDetailProdukRWI();
                                        Ext.get('cboPembayaranPelunasanRWI').dom.value = 'Pilih Pembayaran...';



                                    },
                                }
                    }
            );

    return cboJenisByr;
};

function getTotalDetailProdukRWI()
{
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < dsTRDetailPelunasanrwiPelunasanList.getCount(); i++)
    {


        //alert(TotalProduk);
        if (tampungtypedata == 0)
        {
            tampunggrid = parseInt(dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.BAYARTR);

            //recordterakhir= tampunggrid
            //TotalProduk.toString().replace(/./gi, "");
            TotalProduk += tampunggrid

            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viPelunasanrwi').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 3)
        {
            tampunggrid = parseInt(dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.PIUTANG);
            //TotalProduk.toString().replace(/./gi, "");
            //recordterakhir=tampunggrid
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viPelunasanrwi').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 1)
        {
            tampunggrid = parseInt(dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.DISCOUNT);
            //  TotalProduk.toString().replace(/./gi, "");
            //recordterakhir=dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.DISCOUNT
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viPelunasanrwi').dom.value = formatCurrency(recordterakhir);
        }



    }
    bayar = Ext.get('txtJumlah2EditData_viPelunasanrwi').getValue();
    return bayar;
}
;

function mComboPembayaran()
{
    var Field = ['KD_PAY', 'JENIS_PAY', 'PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});


    var cboPembayaranPelunasanRWI = new Ext.form.ComboBox
            (
                    {
                        id: 'cboPembayaranPelunasanRWI',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        labelWidth: 80,
                        align: 'Right',
                        store: dsComboBayar,
                        valueField: 'KD_PAY',
                        displayField: 'PAYMENT',
                        anchor: '100%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        tapungkd_pay = b.data.KD_PAY;
                                        //getTotalDetailProdukRWI();


                                    },
                                }
                    }
            );

    return cboPembayaranPelunasanRWI;
}
;

function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'nama',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboBayar',
                                    param: 'jenis_pay=~' + jenis_pay + '~'
                                }
                    }
            )
}

function getTabPanelPembayaranSummaryDetailPelunasanRWI(){
    var items ={
        xtype:'tabpanel',
        activeTab: 1,
        plain: true,
        defferedRender: true,
        height:300,
        width:547,
        defaults:{
            bodyStyle:'padding:0px',
            autoScroll: true
        },
        items:[
            DataPanelPembayaranSummryPelunasanRWI(),
            DataPanelPembayaranDetailPelunasanRWI()
        ],
        tbar:[
            {
                text: 'Simpan',
                id: 'btnSimpanPelunasanrwi',
                tooltip: nmSimpan,
                iconCls: 'save',
                handler: function ()
                {
                    //if (statusHitung == false) {
                        // hitungtotaltagihanPelunasanrwi();
                    //}
                    save_pembayaran();
                    // Ext.getCmp('btnEditPelunasanrwi').disable();


                }
            },
        
        ]
    };
    return items;
}


function DataPanelPembayaranSummryPelunasanRWI(){
    
    var FormPanelPembayaranSummryPelunasanRWI = new Ext.Panel
    (
        {
            id: 'FormPanelPembayaranSummryPelunasanRWI',
            // region: 'center',
            // width: '100%',
            width: 545,
            layout: 'form',
            title: 'Summary Bayar',
            border: true,
            shadhow: true,
            items: 
            [ 
                getItemPanelSumaaryBayarPelunasanRWI()
                
            ]

        }
    );
    
    return FormPanelPembayaranSummryPelunasanRWI
    
}


function getItemPanelSumaaryBayarPelunasanRWI(){
 var items = new Ext.Panel({
        bodyStyle:'padding: 10px 10px 10px 10px',
        layout:'form',
        border:false,
        autoScroll: true,
        items:[
            {
                xtype : 'fieldset',
                layout: 'absolute',
                width : 525,
                height :200,
                border: true,
                items:[
                    {
                        x: 0,
                        y: 0,
                        xtype: 'label',
                        text: 'Total tagihan '
                    }, {
                        x: 70,
                        y: 0,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 80,
                        y: 0,
                        xtype: 'textfield',
                        id: 'txtTotalTagihanPelunasanRWI',
                        width: 170,
                        readOnly:true,
                        style: {'text-align': 'right'}
                    }, 
                    {
                        x: 0,
                        y: 30,
                        xtype: 'label',
                        text: 'Sudah bayar'
                    }, {
                        x: 70,
                        y: 30,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 80,
                        y: 30,
                        xtype: 'textfield',
                        id: 'txtSudahBayarPelunasanRWI',
                        width: 170,
                        readOnly:true,
                        style: {'text-align': 'right'}
                    },
                    {
                        x: 0,
                        y: 60,
                        xtype: 'label',
                        text: 'Sisa tagihan'
                    }, {
                        x: 70,
                        y: 60,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 80,
                        y: 60,
                        xtype: 'textfield',
                        id: 'txtSisatagihanPelunasanRWI',
                        width: 170,
                        readOnly:true,
                        style: {'text-align': 'right'}
                    },
                    {
                        x: 0,
                        y: 90,
                        xtype: 'label',
                        text: 'Total bayar'
                    }, {
                        x: 70,
                        y: 90,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 80,
                        y: 90,
                        xtype: 'numberfield',
                        id: 'txtTotalBayarSummaryPelunasanRWI',
                        width: 170,
                        style: {'text-align': 'right','font-weight':'bold'},
                        enableKeyEvents: true,
                        listeners:
                        {
                            'keyup': function (a, b)
                            {
                                // Ext.getCmp('txtTotalBayarDetailPelunasanRWI').setValue(Ext.getCmp('txtTotalBayarSummaryPelunasanRWI').getValue());
                                var nominal             = parseInt(Ext.getCmp('txtTotalBayarSummaryPelunasanRWI').getValue());
                                // console.log(asli_sisa_tagihan);
                                Ext.getCmp('txtSisatagihanPelunasanRWI').setValue(asli_sisa_tagihan);
                                var sisa_tagihan = asli_sisa_tagihan;
                                sisa_tagihan = sisa_tagihan - nominal;
                                Ext.getCmp('txtSisatagihanPelunasanRWI').setValue(sisa_tagihan);
                                if (nominal == 0 || Ext.getCmp('txtTotalBayarSummaryPelunasanRWI').getValue() == '') {
                                    Ext.getCmp('txtSisatagihanPelunasanRWI').setValue(asli_sisa_tagihan);
                                }
                            }
                        }
                    },
                ]
            }
        ]
    })
    return items;
}


function DataPanelPembayaranDetailPelunasanRWI(){
    var paneltotal = new Ext.Panel
    (
        {
            id: 'paneltotal',
            region: 'center',
            border: true,
            bodyStyle: 'padding:0px 0px 0px 0px',
            layout: 'column',
            frame: true,
            height: 40,
            width: 545,
            autoScroll: false,
            items:
            [
                {
                    xtype: 'compositefield',
                    anchor: '100%',
                    labelSeparator: '',
                    border: true,
                    style: {'margin-top': '3px'},
                    items:
                    [
                        {
                            xtype: 'label',
                            style: {'margin-left': '5px'},
                            text: 'Jumlah bayar : '
                        }, 
                        {
                            xtype: 'numberfield',
                            id: 'txtTotalBayarDetailPelunasanRWI',
                            width: 110,
                            enableKeyEvents: true,
                            style: {'text-align': 'right','font-weight':'bold', 'margin-left': '3px'},
                            listeners:
                            {
                                'specialkey': function ()
                                {
                                    if (Ext.EventObject.getKey() === 13)
                                    {
                                        // Ext.getCmp('txtTotalBayarDetailPelunasanRWI').setValue(Ext.getCmp('txtTotalBayarSummaryPelunasanRWI').getValue());
                                        var nominal             = parseInt(Ext.getCmp('txtTotalBayarDetailPelunasanRWI').getValue());
                                        // console.log(asli_sisa_tagihan);
                                        Ext.getCmp('txtJumlah2EditData_viPelunasanrwi').setValue(asli_sisa_tagihan);
                                        var sisa_tagihan = asli_sisa_tagihan;
                                        sisa_tagihan = sisa_tagihan - nominal;
                                        Ext.getCmp('txtJumlah2EditData_viPelunasanrw11i').setValue(sisa_tagihan);
                                        if (nominal == 0 || Ext.getCmp('txtTotalBayarDetailPelunasanRWI').getValue() == '') {
                                            Ext.getCmp('txtJumlah2EditData_viPelunasanrw11i').setValue(asli_sisa_tagihan);
                                        }
                                    }
                                },
                                keyup : function(){
                                    // Ext.getCmp('txtTotalBayarDetailPelunasanRWI').setValue(Ext.getCmp('txtTotalBayarSummaryPelunasanRWI').getValue());
                                    var nominal             = parseInt(Ext.getCmp('txtTotalBayarDetailPelunasanRWI').getValue());
                                    // console.log(asli_sisa_tagihan);
                                    Ext.getCmp('txtJumlah2EditData_viPelunasanrwi').setValue(asli_sisa_tagihan);
                                    var sisa_tagihan = asli_sisa_tagihan;
                                    sisa_tagihan = sisa_tagihan - nominal;
                                    Ext.getCmp('txtJumlah2EditData_viPelunasanrw11i').setValue(sisa_tagihan);
                                    if (nominal == 0 || Ext.getCmp('txtTotalBayarDetailPelunasanRWI').getValue() == '') {
                                        Ext.getCmp('txtJumlah2EditData_viPelunasanrw11i').setValue(asli_sisa_tagihan);
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'label',
                            style: {'font-size': '11px','margin-top': '7px'},
                            text: '*) Enter untuk hitung'
                        },/*
                        {
                            layout: 'form',
                            style: {'text-align': 'right', 'margin-left': '90px'},
                            border: false,
                            html: " Total :"
                        },*/
                        {
                            xtype: 'textfield',
                            id: 'txtJumlah2EditData_viPelunasanrw11i',
                            name: 'txtJumlah2EditData_viPelunasanrw11i',
                            style: {'text-align': 'right', 'margin-left': '45px'},
                            width: 82,
                            readOnly: true,
                        },/*
                        {
                            layout: 'form',
                            style: {'text-align': 'right', 'margin-left': '90px'},
                            border: false,
                            html: " Total :"
                        },*/
                        {
                            xtype: 'textfield',
                            id: 'txtJumlah2EditData_viPelunasanrwi',
                            name: 'txtJumlah2EditData_viPelunasanrwi',
                            style: {'text-align': 'right', 'margin-left': '45px'},
                            width: 82,
                            readOnly: true,
                        },
                                //-------------- ## --------------
                    ]
                },
                {
                    xtype: 'compositefield',
                    anchor: '100%',
                    labelSeparator: '',
                    border: true,
                    style: {'margin-top': '5px'},
                    items:
                    [
                        {
                            layout: 'form',
                            style: {'text-align': 'right', 'margin-left': '370px'},
                            border: false,
                            html: ""
                        },
                        {
                            xtype: 'numberfield',
                            id: 'txtJumlahEditData_viPelunasanrwi',
                            name: 'txtJumlahEditData_viPelunasanrwi',
                            style: {'text-align': 'right', 'margin-left': '390px'},
                            width: 82,
                            hidden: true,
                            //readOnly: true,
                        }
                        //-------------- ## --------------
                    ]
                }
            ]

        }
    )
    var FormPanelPembayaranDetailPelunasanRWI = new Ext.Panel
    (
        {
            id: 'FormPanelPembayaranDetailPelunasanRWI',
            width: 530,
            height: 100,
            layout: 'form',
            title: 'Detail Bayar',
            border: false,
            shadhow: true,
            items: 
            [ 
                GetDTLTRPelunasanrwiGrid(), 
                paneltotal
            ]

        }
    );
    return FormPanelPembayaranDetailPelunasanRWI;
}


function GetDTLTRPelunasanrwiGrid()
{
    gridDTLTRPelunasanrwinap = new Ext.grid.EditorGridPanel
    (
        {
            title: '',
            stripeRows: true,
            id: 'gridDTLTRPelunasanrwinap',
            store: dsTRDetailPelunasanrwiPelunasanList,
            border: false,
            columnLines: true,
            frame: false,
            height: 205,
            width: 545,
            autoScroll: true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                    }
                }
            ),
            cm: TRPelunasanRawatInapColumModel(),
            viewConfig: {
                forceFit: true
            },  
        }
    );



    return gridDTLTRPelunasanrwinap;
}
;


function TRPelunasanRawatInapColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiKasirrwi',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 250,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiKasirrwi',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            id: 'colURUTKasirrwi',
                            header: 'Urut',
                            dataIndex: 'URUT',
                            sortable: false,
                            hidden: true,
                            menuDisabled: true,
                            width: 250

                        }
                        ,
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            hidden: true,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colQtyKasirrwi',
                            header: 'Qty',
                            width: 91,
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                        },
                        {
                            id: 'colHARGAKasirrwi',
                            header: 'Harga',
                            align: 'right',
                            hidden: false,
                            menuDisabled: true,
                            dataIndex: 'BAYARTR',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYARTR);

                            }
                        },
                        {
                            id: 'colPiutangKasirrwi',
                            header: 'Puitang',
                            width: 80,
                            dataIndex: 'PIUTANG',
                            align: 'right',
                            hidden: true,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolPuitangrwi',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    )
                            ,
                            renderer: function (v, params, record)
                            {

                                //getTotalDetailProdukRWI();

                                return formatCurrency(record.data.PIUTANG);




                            }
                        },
                        {
                            id: 'colTunaiKasirrwi',
                            header: 'Tunai',
                            width: 80,
                            dataIndex: 'BAYARTR',
                            align: 'right',
                            hidden: true,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolTunairwi',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {

                                getTotalDetailProdukRWI();

                                return formatCurrency(record.data.BAYARTR);


                            }

                        },
                        {
                            id: 'colDiscountKasirrwi',
                            header: 'Discount',
                            width: 80,
                            dataIndex: 'DISCOUNT',
                            align: 'right',
                            hidden: true,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolDiscountrwi',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    )
                            ,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.DISCOUNT);


                            }


                        }

                    ]
                    )
};

function RefreshDataKasirrwiKasirDetail(no_transaksi)
{
    var strKriteriaKasirrwi = '';

    strKriteriaKasirrwi = 'no_transaksi = ~' + no_transaksi + '~ and kd_kasir=~02~';

    dsTRDetailPelunasanrwiPelunasanList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target: 'ViewDetailbayarRWIPelunasan',
                param: strKriteriaKasirrwi
            }
        }
    );
    return dsTRDetailPelunasanrwiPelunasanList;
};

function getDataTransaksi(){ 
    Ext.Ajax.request
    ({
        url: baseURL + "index.php/rawat_inap/pelunasan/get_data_transaksi",
        params: {
        no_transaksi:rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI,
        kd_kasir    :rowSelectedPelunasanPasienRWIPelunasan.data.KD_KASIR,
        },
        failure: function (o){
        },
        success: function (o){
        }
    })
};

function save_pembayaran(){ 
    Ext.Ajax.request
    ({
        url: baseURL + "index.php/main/Controller_pembayaran/simpan_pembayaran_revisi",
        params: {
        no_transaksi    :rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI,
        kd_kasir        :rowSelectedPelunasanPasienRWIPelunasan.data.KD_KASIR,
        payment         :tapungkd_pay,
        nominal_1       :Ext.getCmp('txtTotalBayarDetailPelunasanRWI').getValue(),
        nominal         :Ext.getCmp('txtTotalBayarSummaryPelunasanRWI').getValue(),
        tgl_pembayaran  :Ext.getCmp('dtpTanggalDetransaksi').getValue(),
        sisa_bayar      :asli_sisa_tagihan,
        data            :getArrDetailTrKasirrwi(),
        jumlah          :dsTRDetailPelunasanrwiPelunasanList.getCount(),
        object          :'pelunasan',
        modul           :'Rawat Inap',
        },
        failure: function (o){
        },
        success: function (o){
            var cst = Ext.decode(o.responseText);
            if (cst.status == true) {
                ShowPesanInfoPelunasanPasienRWI("Pembayaran berhasil ","Informasi");
                RefreshDataKasirrwiKasirDetail(rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI);
                RefreshDatahistoribayar(rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI);
                getDataDetail_bayaran();
            }else{
                ShowPesanWarningPelunasanrwi("Gagal Menghapus data pembayaran","Informasi");
            }
        }
    })
};

function update_verified(verified){ 
    Ext.Ajax.request
    ({
        url: baseURL + "index.php/rawat_inap/pelunasan/update_verified",
        params: {
        no_transaksi    :rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI,
        kd_kasir        :rowSelectedPelunasanPasienRWIPelunasan.data.KD_KASIR,
        verified        :verified,
        },
        failure: function (o){
        },
        success: function (o){
            var cst = Ext.decode(o.responseText);
            if (cst.status == true) {
                ShowPesanInfoPelunasanPasienRWI("Data berhasil diperbarui" , "Informasi");
                RefreshDataFilterPelunasanPasienRWIPelunasan();
            }else{
                ShowPesanWarningPelunasanrwi("Data gagal diperbarui" , "Peringatan");
            }
        }
    })
};

function cek_verified(){ 
    Ext.Ajax.request
    ({
        url: baseURL + "index.php/rawat_inap/pelunasan/cek_verified",
        params: {
        no_transaksi    : rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI,
        kd_kasir        : rowSelectedPelunasanPasienRWIPelunasan.data.KD_KASIR,
        verified        : 1,
        },
        failure: function (o){
        },
        success: function (o){
            Ext.getCmp('btnUnVerifiedPelunasan').disable();
            Ext.getCmp('btnVerifiedPelunasan').disable();
            var cst = Ext.decode(o.responseText);
            if (cst.status == true) {
                Ext.getCmp('btnUnVerifiedPelunasan').enable();
                Ext.getCmp('btnPelunasanrwi').disable();
                Ext.getCmp('btnHapusPelunasanrwi').disable();
            }else{
                Ext.getCmp('btnVerifiedPelunasan').enable();
                Ext.getCmp('btnPelunasanrwi').enable();
                Ext.getCmp('btnHapusPelunasanrwi').enable();
            }
        }
    })
};

function delete_pembayaran(alasan){ 
    Ext.Ajax.request
    ({
        url: baseURL + "index.php/main/Controller_pembayaran/delete_pembayaran_revisi",
        params: {
        no_transaksi    : rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI,
        kd_kasir        : rowSelectedPelunasanPasienRWIPelunasan.data.KD_KASIR,
        urut            : dsTRDetailPelunasanList.data.items[indexRow].data.URUT,
        alasan          : alasan,
        object          : 'pelunasan',
        modul           : 'Rawat Inap',
        },
        failure: function (o){
        },
        success: function (o){
            var cst = Ext.decode(o.responseText);
            if (cst.status == true) {
                ShowPesanInfoPelunasanPasienRWI("Pembayaran telah dihapus ","Informasi");
                RefreshDataKasirrwiKasirDetail(rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI);
                RefreshDatahistoribayar(rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI);
                getDataDetail_bayaran();
            }else{
                ShowPesanWarningPelunasanrwi("Gagal Menghapus data pembayaran","Informasi");
            }
        }
    })
};

function getArrDetailTrKasirrwi()
{
    var x = '';
    var totalBayar = Ext.getCmp('txtTotalBayarDetailPelunasanRWI').getValue();
    var bayar = 0;
    for (var i = 0; i < dsTRDetailPelunasanrwiPelunasanList.getCount(); i++)
    {
        // if (gridDTLTRPelunasanrwinap.getStore().getRange()[i].get('BAYARTR') > 0) {
            if (dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.KD_PRODUK != '' && dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.DESKRIPSI != '')
            {
                var y = '';
                var z = '@@##$$@@';
                /*if (totalBayar>dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.HARGA * dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.QTY) {
                    totalBayar = totalBayar-dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.HARGA * dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.QTY;
                    if (totalBayar>0) {
                        bayar = dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.HARGA * dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.QTY;
                        gridDTLTRPelunasanrwinap.getStore().getRange()[i].set('BAYARTR', gridDTLTRPelunasanrwinap.getStore().getRange()[i].get('BAYARTR') - bayar);
                    }else{
                        bayar = 0;
                        gridDTLTRPelunasanrwinap.getStore().getRange()[i].set('BAYARTR', 0);
                    }

                    
                }else if (totalBayar<=dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.HARGA * dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.QTY) {
                    bayar       = totalBayar;
                    gridDTLTRPelunasanrwinap.getStore().getRange()[i].set('BAYARTR', gridDTLTRPelunasanrwinap.getStore().getRange()[i].get('BAYARTR') - bayar);
                    totalBayar  = 0;
                }*/

                    // bayar = dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.BAYARTR * dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.QTY;
                    bayar = dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.BAYARTR;
                    console.log("1. Bayar TR"+dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.BAYARTR);
                    console.log("2. QTY"+dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.QTY);
                    y   = dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.URUT
                    y   += z + dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.KD_PRODUK
                    y   += z + dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.QTY
                    y   += z + bayar
                    y   += z + dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.KD_TARIF
                    y   += z + dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.URUT
                    //y += z + dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.BAYARTR
                    y   += z + bayar
                    y   += z + bayar
                    y   += z + bayar
                    y   += z + dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.TGL_TRANSAKSI
                    y   += z + dsTRDetailPelunasanrwiPelunasanList.data.items[i].data.TGL_BERLAKU

                    if (i === (dsTRDetailPelunasanrwiPelunasanList.getCount() - 1))
                    {
                        x += y
                    } else
                    {
                        x += y + '##[[]]##'
                    };
                }
        // };
    }

    return x;
};

function getDataDetail_bayaran(){ 

    Ext.Ajax.request({
        url: baseURL + "index.php/rawat_inap/pelunasan/get_data_detail_transaksi",
        params: {
        no_transaksi:rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI,
        kd_kasir    :rowSelectedPelunasanPasienRWIPelunasan.data.KD_KASIR,
        },
        failure: function (o){
        },
        success: function (o){
            var cst = Ext.decode(o.responseText);
            Ext.getCmp('txtTotalTagihanPelunasanRWI').setValue(cst.data.harga);
        }
    });

    Ext.Ajax.request
    ({
        url: baseURL + "index.php/rawat_inap/pelunasan/get_data_detail_tr_bayar_lunas",
        params: {
        no_transaksi:rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI,
        kd_kasir    :rowSelectedPelunasanPasienRWIPelunasan.data.KD_KASIR,
        },
        failure: function (o){
        },
        success: function (o){
            var cst = Ext.decode(o.responseText);
            Ext.getCmp('txtSudahBayarPelunasanRWI').setValue(cst.data.harga);
        }
    });

    Ext.Ajax.request
    ({
        url: baseURL + "index.php/rawat_inap/pelunasan/get_data_sisa_tagihan",
        params: {
        no_transaksi:rowSelectedPelunasanPasienRWIPelunasan.data.NO_TRANSAKSI,
        kd_kasir    :rowSelectedPelunasanPasienRWIPelunasan.data.KD_KASIR,
        },
        failure: function (o){
        },
        success: function (o){
            var cst = Ext.decode(o.responseText);
            asli_sisa_tagihan   = cst.data.harga;
            Ext.getCmp('txtJumlah2EditData_viPelunasanrwi').setValue(cst.data.harga);
            Ext.getCmp('txtSisatagihanPelunasanRWI').setValue(cst.data.harga);
            Ext.getCmp('txtTotalBayarDetailPelunasanRWI').setValue(cst.data.harga);
        }
    });

    // sisa_tagihan      = parseInt(total_bayar) - parseInt(sudah_dibayar);
};



function GetDTLTRRPelunasanGrid()
{
    var gridDTLTRPelunasan = new Ext.grid.EditorGridPanel
        (
            {
                title: 'Pelunasan Bayar',
                stripeRows: true,
                store: dsTRDetailPelunasanList,
                border: false,
                columnLines: true,
                frame: false,
                id: "idPelunasanBayarKwitansi",
                height: 400,
                anchor: '100%',
                autoScroll: true,
                cm: TRPelunasanKwitansiColumModel(),
                tbar:[

                ],
                viewConfig: {forceFit: true},
            }
        );
    return gridDTLTRPelunasan;
};


function TRPelunasanKwitansiColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: true,
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            hidden: true,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'colKdUnitbayar_Infopasien',
                            header: 'KD Unit',
                            dataIndex: 'KD_UNIT',
                            hidden: true,
                            //hidden: true,
                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Bayar',
                            dataIndex: 'URUT',
                            hidden: true,

                        }

                        ,
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        }
                        ,
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }

                        }
                        ,
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true

                        }
                        ,
                        {
                            id: 'colStatPelunasan',
                            header: 'Pelunasan',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colPetugasPelunasan',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME',
                            hidden: true,
                        },
                        {
                            id: 'colCheckPelunasan',
                            header: 'Tag',
                            width: 130,
                            xtype:'checkcolumn',
                            dataIndex: 'TAG',
                            editor:{
                                xtype:'checkbox',
                            }
                        },
                    ]
                    )
};

function ShowPesanWarningPelunasanrwi(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
};