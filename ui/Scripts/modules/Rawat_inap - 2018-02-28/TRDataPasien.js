// Data Source ExtJS # --------------

/**
 *	Nama File 		: TRDataPasien.js
 *	Menu 			: Pendaftaran
 *	Model id 		: 030101
 *	Keterangan 		: Data Pasien adalah proses untuk melihat list/ daftar pasien, selain itu data pasien ini digunakan untuk___
 *					  ___melengkapi atau memperbaiki biodata dari pasien
 *	Di buat tanggal : 15 April 2014
 *	Oleh 			: SDY_RI
 *	Edit                    : HDHT
 */

// Deklarasi Variabel pada Data Pasien # --------------
var CurrentHistorydatapasien =
        {
            data: Object,
            details: Array,
            row: 0
        };
var datapasienvariable = {};
datapasienvariable.kd_pasien;
datapasienvariable.urut;
datapasienvariable.tglkunjungan;
datapasienvariable.kd_unit;
var mod_name_viDataPasien = "viDataPasien";
var NamaForm_viDataPasien = "Data Pasien";
var selectSetJK;
var selectSetGolDarah;
var selectSetSatusMarital;
var icons_viDataPasien = "Data_pegawai";
var DfltFilterBtn_viDataPasien = 0;
var slctCount_viDataPasien = 10;
var now_viDataPasien = new Date();
var rowSelectedGridDataView_viDataPasien;
var setLookUpsGridDataView_viDataPasien;
var addNew_viDataPasien;
var tmphasil;
var tmpjk;
/* 
	PERBARUAN VARIALBLE GANTI KELOMPOK PASIEN
	LEH 	: HADAD
	TANGGAL : 2016 - 12 -31
 */
var cellSelecteddeskripsi_datapasien;
var jeniscus_RWJ;
var labelisi_RWJ;
var FormLookUpsdetailTRKelompokPasien_rwj;
var vkode_customer_RWJ;
var KelompokPasienAddNew_RWJ=true;
var vCustomer;
var vCo_status;
var vNoSEP;
var vKd_Pasien;
var vTanggal;
var vKdUnit;
var vKdUnitDulu = "";
var vKdDokter;
var vNoAsuransi;
var vNamaUnit;
var vNamaDokter;
var vKdKasir;
var vUrutMasuk;
var vNoTransaksi;
var panelActiveDataPasien;
var statusCekDetail;
var dsDokterRequestEntry;
/* ------------------------------- END --------------------------- */
var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});
CurrentPage.page = dataGrid_viDataPasien(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


function loaddatastoredokter(){
	dsDokterRequestEntry.load({
         params	:{
            Skip	: 0,
		    Take	: 1000,
            Sort	: 'nama',
		    Sortdir	: 'ASC',
		    target	: 'ViewComboDokter',
		    param	: 'where dk.kd_unit=~'+ vKdUnit+ '~'
		}
    });
}

// Start Project Data Pasien # --------------

// --------------------------------------- # Start Function # ---------------------------------------

/**
 *	Function : ShowPesanWarning_viDataPasien
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
 */

function ShowPesanWarning_viDataPasien(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            )
}
// End Function ShowPesanWarning_viDataPasien # --------------

/**
 *	Function : ShowPesanError_viDataPasien
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
 */

function ShowPesanError_viDataPasien(str, modul){
    Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.ERROR,
		width: 250
	});
}
// End Function ShowPesanError_viDataPasien # --------------

/**
 *	Function : ShowPesanInfo_viDataPasien
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat data berhasil
 */

function ShowPesanInfo_viDataPasien(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            )
}
// End Function ShowPesanInfo_viDataPasien # --------------

/**
 *	Function : getCriteriaFilterGridDataView_viDataPasien
 *	
 *	Sebuah fungsi untuk memfilter data yang diambil dari Net.
 */
function getCriteriaFilterGridDataView_viDataPasien(){
    var strKriteriaGridDataView_viDataPasienGridDataView_viDataPasien = "";

    //strKriteriaGridDataView_viDataPasien = " WHERE PS.KD_CUSTOMER = '" + DfltFilter_KD_CUSTOMER_viDataPasien + "' ";

    if (DfltFilterBtn_viDataPasien == 1)
    {
        if (Ext.getCmp('TxtFilterGridDataView_NO_RM_viDataPasien').getValue() != "")
        {
            strKriteriaGridDataView_viDataPasien += " AND PS.KD_PASIEN LIKE '" + DfltFilter_KD_CUSTOMER_viDataPasien + ".%" + Ext.getCmp('TxtFilterGridDataView_NO_RM_viDataPasien').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NAMA_viDataPasien').getValue() != "")
        {
            strKriteriaGridDataView_viDataPasien += " AND PS.NAMA LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NAMA_viDataPasien').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NO_TELP_viDataPasien').getValue() != "")
        {
            strKriteriaGridDataView_viDataPasien += " AND PS.NO_TELP LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NO_TELP_viDataPasien').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NO_HP_viDataPasien').getValue() != "")
        {
            strKriteriaGridDataView_viDataPasien += " AND PS.NO_HP LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NO_HP_viDataPasien').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_ALAMAT_viDataPasien').getValue() != "")
        {
            strKriteriaGridDataView_viDataPasien += " AND PS.ALAMAT LIKE '%" + Ext.getCmp('TxtFilterGridDataView_ALAMAT_viDataPasien').getValue() + "%' ";
        }
    }

    //strKriteriaGridDataView_viDataPasien += " ORDER BY PS.KD_PASIEN ASC ";

    // DfltFilterBtn_viDataPasien = 0; // Tidak diaktifkan agar saat tutup windows popup tetap di filter
    //return strKriteriaGridDataView_viDataPasien;
}
// End Function getCriteriaFilterGridDataView_viDataPasien # --------------

/**
 *	Function : ValidasiEntry_viDataPasien
 *	
 *	Sebuah fungsi untuk mengecek isian
 */
function ValidasiEntry_viDataPasien(modul, mBolHapus)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').getValue() === 'Tempat Lahir' ||
            (Ext.get('TxtWindowPopup_ALAMAT_viDataPasien').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === '' ||
                    (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').getValue() === '' || (Ext.get('TxtWindowPopup_ALAMAT_viDataPasien').getValue() === ''))))))
    {
        if (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === ''))
        {
            ShowPesanWarning_viDataPasien('Nama belum terisi', modul);
            x = 0;
        } else if (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').getValue() === 'Tempat Lahir' || (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').getValue() === ''))
        {
            ShowPesanWarning_viDaftar("Tempat Lahir belum terisi", modul);
            x = 0;
        } else if (Ext.get('TxtWindowPopup_ALAMAT_viDataPasien').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_ALAMAT_viDataPasien').getValue() === ''))
        {
            ShowPesanWarning_viDaftar("Alamat belum terisi", modul);
            x = 0;
        }
    }

    return x;
}


function ValidasiEntryHistori_viDataPasien(modul)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === ''))
    {
        ShowPesanWarning_viDataPasien('Nama belum terisi', modul);
        x = 0;
    }

    return x;
}
// End Function ValidasiEntry_viDataPasien # --------------

/**
 *	Function : DataRefresh_viDataPasien
 *	
 *	Sebuah fungsi untuk mengambil data dari Net.
 *	Digunakan pada View Grid Pertama, Filter Grid 
 */

function DataRefresh_viDataPasien(criteria){
    dataSourceGrid_viDataPasien.load({
		params:{
			Skip: 0,
			Take: slctCount_viDataPasien,
			Sort: 'kunjungan.tgl_masuk',
			Sortdir: 'ASC',
			target: 'Vi_ViewDataPasien',
			param: criteria

		}
	});
    return dataSourceGrid_viDataPasien;
}
// End Function DataRefresh_viDataPasien # --------------

/**
 *	Function : datainit_viDataPasien
 *	
 *	Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
 */

function datainit_viDataPasien(rowdata){
	var tmp_data;
    addNew_viDataPasien = false;
    //-------------- # textfield # --------------
    Ext.get('TxtWindowPopup_KD_CUSTOMER_viDataPasien').dom.value = rowdata.TGL_MASUK;
    Ext.get('TxtWindowPopup_KD_PASIEN_viDataPasien').dom.value = rowdata.KD_UNIT;
    Ext.get('TxtWindowPopup_NO_RM_viDataPasien').dom.value = rowdata.KD_PASIEN;
    Ext.get('TxtWindowPopup_Urut_Masuk_viDataPasien').dom.value = rowdata.URUT_MASUK;
    Ext.get('TxtWindowPopup_NAMA_viDataPasien').dom.value = rowdata.NAMA;
    Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').dom.value = rowdata.TEMPAT_LAHIR;
    Ext.get('TxtWindowPopup_TAHUN_viDataPasien').dom.value = rowdata.TAHUN;
    Ext.get('TxtWindowPopup_BULAN_viDataPasien').dom.value = rowdata.BULAN;
    Ext.get('TxtWindowPopup_HARI_viDataPasien').dom.value = rowdata.HARI;
    Ext.get('TxtWindowPopup_ALAMAT_viDataPasien').dom.value = rowdata.ALAMAT;
    Ext.get('TxtTmpAgama_viDataPasien').dom.value = rowdata.KD_AGAMA;
    Ext.get('TxtTmpPekerjaan_viDataPasien').dom.value = rowdata.KD_PEKERJAAN;
    Ext.get('TxtTmpPendidikan_viDataPasien').dom.value = rowdata.KD_PENDIDIKAN;
    //Ext.get('TxtWindowPopup_NO_HP_viDataPasien').dom.value=TmpNoHP;
    Ext.get('TxtWindowPopup_TAHUN_viDataPasien').dom.value = '';
    Ext.get('TxtWindowPopup_BULAN_viDataPasien').dom.value = '';
    Ext.get('TxtWindowPopup_HARI_viDataPasien').dom.value = '';
    setUsia(rowdata.TGL_LAHIR_PASIEN);
    //-------------- # datefield # --------------
    Ext.getCmp('DateWindowPopup_TGL_LAHIR_viDataPasien').setValue(rowdata.TGL_LAHIR_PASIEN);

    //-------------- # combobox # --------------
    if (rowdata.JENIS_KELAMIN === "t" || rowdata.JENIS_KELAMIN === 1){
        tmpjk = "1";
        tmp_data = "Laki- laki";
    }else{
        tmpjk = "2";
        tmp_data = "Perempuan";
    }
    ID_JENIS_KELAMIN = rowdata.JENIS_KELAMIN;
    Ext.getCmp('cboJK').setValue(tmpjk);
    Ext.getCmp('cboGolDarah').setValue(rowdata.GOL_DARAH);
    Ext.getCmp('cboStatusMarital').setValue(rowdata.STATUS_MARITA);
    Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowdata.PEKERJAAN);
    Ext.getCmp('cboAgamaRequestEntry').setValue(rowdata.AGAMA);
    Ext.getCmp('cboPendidikanRequestEntry').setValue(rowdata.PENDIDIKAN);
    Ext.getCmp('TxtWindowPopup_NO_TELP_viDataPasien').setValue(rowdata.TELEPON);
    Ext.getCmp('cbokelurahan_editdatapasien').setValue(rowdata.KELURAHAN);
    Ext.getCmp('cboKecamatan_editdatapasien').setValue(rowdata.KECAMATAN);
    Ext.getCmp('cboKabupaten_editdatapasien').setValue(rowdata.KABUPATEN);
    Ext.getCmp('cboPropinsi_EditdataPasien').setValue(rowdata.PROPINSI);
    loaddatastorekelurahan(rowdata.KD_KECAMATAN);
    loaddatastorekecamatan(rowdata.KD_KABUPATEN);
    loaddatastorekabupaten(rowdata.KD_PROPINSI);
    selectPropinsiRequestEntry = rowdata.KD_PROPINSI;
    selectKabupatenRequestEntry = rowdata.KD_KABUPATEN;
    selectKecamatanpasien = rowdata.KD_KECAMATAN;
    kelurahanpasien = rowdata.KD_KELURAHAN;
    Ext.getCmp('TxtWindowPopup_Email_viDataPasien').setValue(rowdata.EMAIL);
    Ext.getCmp('TxtWindowPopup_HP_viDataPasien').setValue(rowdata.HP);
    Ext.getCmp('TxtWindowPopup_Nama_ayah_viDataPasien').setValue(rowdata.AYAH);
    Ext.getCmp('TxtWindowPopup_NamaIbu_viDataPasien').setValue(rowdata.IBU);
    Ext.getCmp('TxtWindowPopup_Nama_keluarga_viDataPasien').setValue(rowdata.NAMA_KELUARGA);
}
// End Function datainit_viDataPasien # --------------

/**
 *	Function : dataparam_datapasviDataPasien
 *	
 *	Sebuah fungsi untuk mengirim balik isian ke Net.
 *	Digunakan saat save, edit, delete
 */

function setUsia(Tanggal){
    Ext.Ajax.request({
		url: baseURL + "index.php/main/GetUmur",
		params: {
			TanggalLahir: Tanggal
		},
		success: function (o){
			var tmphasil = o.responseText;
			var tmp = tmphasil.split(' ');
			if (tmp.length == 6){
				Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
				Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[2]);
				Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[4]);
			} else if (tmp.length == 4)
			{
				if (tmp[1] === 'years' && tmp[3] === 'day')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[2]);
				} else if (tmp[1] === 'year' && tmp[3] === 'days')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[2]);
				} else
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[2]);
				}
			} else if (tmp.length == 2)
			{

				if (tmp[1] == 'year')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
				} else if (tmp[1] == 'years')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
				} else if (tmp[1] == 'mon')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
				} else if (tmp[1] == 'mons')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
				} else {
					Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[0]);
				}
			} else if (tmp.length == 1)
			{
				Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
				Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
				Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('1');
			} else
			{
				alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
			}
		}


            });



}
/*
    PERBARUAN UPDATE DATA PASIEN
    OLEH    : HADAD AL GOJALI 
    TANGGAL : 2016 - 01 - 30
    ALASAN  : DATA TIDAK MENGUPDATE KE SQL SERVER 
 */

function dataparam_datapasviDataPasien(){
    var params_viDataPasien ={
                //-------------- # modelist Net. # --------------
		Table: 'Vi_ViewDataPasien',
		//-------------- # textfield # --------------
		NO_MEDREC: Ext.getCmp('TxtWindowPopup_NO_RM_viDataPasien').getValue(),
		NAMA: Ext.getCmp('TxtWindowPopup_NAMA_viDataPasien').getValue(),
		TEMPAT_LAHIR: Ext.getCmp('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').getValue(),
		ALAMAT: Ext.getCmp('TxtWindowPopup_ALAMAT_viDataPasien').getValue(),
		NO_TELP: Ext.getCmp('TxtWindowPopup_NO_TELP_viDataPasien').getValue(),
		//NO_HP: Ext.get('TxtWindowPopup_NO_HP_viDataPasien').getValue(),

		//-------------- # datefield # --------------
		TGL_LAHIR: Ext.get('DateWindowPopup_TGL_LAHIR_viDataPasien').getValue(),
		//-------------- # combobox # --------------
		ID_JENIS_KELAMIN: Ext.getCmp('cboJK').getValue(),
		ID_GOL_DARAH: Ext.getCmp('cboGolDarah').getValue(),
		KD_STS_MARITAL: Ext.getCmp('cboStatusMarital').getValue(),
		NAMA_PEKERJAAN: Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
		NAMA_AGAMA: Ext.getCmp('cboAgamaRequestEntry').getValue(),
		NAMA_PENDIDIKAN: Ext.getCmp('cboPendidikanRequestEntry').getValue(),
		//KD_PEKERJAAN:Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
		//KD_AGAMA:Ext.getCmp('cboAgamaRequestEntry').getValue(),

		KD_AGAMA: Ext.getCmp('TxtTmpAgama_viDataPasien').getValue(),
		KD_PEKERJAAN: Ext.getCmp('TxtTmpPekerjaan_viDataPasien').getValue(),
		KD_PENDIDIKAN: Ext.getCmp('TxtTmpPendidikan_viDataPasien').getValue(),
        TMPPARAM: '0',
		KD_KELURAHAN: kelurahanpasien,
		AYAHPASIEN: Ext.getCmp('TxtWindowPopup_Nama_ayah_viDataPasien').getValue(),
		IBUPASIEN: Ext.getCmp('TxtWindowPopup_NamaIbu_viDataPasien').getValue(),
		NAMA_KELUARGA: Ext.getCmp('TxtWindowPopup_Nama_keluarga_viDataPasien').getValue(),
		Emailpasien: Ext.getCmp('TxtWindowPopup_Email_viDataPasien').getValue(),
		HPpasien: Ext.getCmp('TxtWindowPopup_HP_viDataPasien').getValue()
	}
    return params_viDataPasien
}
/**
 *	Function : datasave_dataPasiDataPasien
 *	
 *	Sebuah fungsi untuk menyimpan dan mengedit data entry 
 */

function getParamRequest()
{
    var params =
            {
                Table: 'Vi_ViewDataPasien',
                NOMEDREC: Ext.get('TxtWindowPopup_NO_RM_viDataPasien').getValue(),
                TGLMASUK: Ext.get('TxtWindowPopup_KD_CUSTOMER_viDataPasien').getValue(),
                URUTMASUK: Ext.get('TxtWindowPopup_Urut_Masuk_viDataPasien').getValue(),
                KDUNIT: Ext.get('TxtWindowPopup_KD_PASIEN_viDataPasien').getValue(),
                KET: Ext.get('TxtHistoriDeleteDataPasien').getValue(),
                TMPPARAM: '1'
            };
    return params
}
;

function datasavehistori_viDataPasien()
{
    if (ValidasiEntryHistori_viDataPasien('Simpan Data', true) === 1)
    {
        Ext.Ajax.request
                (
                        {
                            url: baseURL + "index.php/main/CreateDataObj",
                            params: getParamRequest(),
                            success: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfo_viDataPasien('Data transaksi pasien berhasil di hapus', 'Edit Data');
                                    DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
                                    //tmphasil = true;
                                } else
                                {
                                    ShowPesanError_viDataPasien('Data transaksi pasien tidak berhasil di hapus' + cst.pesan, 'Edit Data');
                                    //tmphasil = false;
                                }
                            }
                        }
                )
    } else
    {
        if (mBol === true)
        {
            return false;
        }
    }
}

function datasave_dataPasiDataPasien(mBol){
    var cbo_kabupaten = Ext.getCmp('cboKabupaten_editdatapasien').getValue();
    var cbo_kecamatan = Ext.getCmp('cboKecamatan_editdatapasien').getValue();    
    var cbo_kelurahan = Ext.getCmp('cbokelurahan_editdatapasien').getValue();    
    if (cbo_kabupaten.length == 0 || cbo_kecamatan.length == 0 || cbo_kelurahan.length == 0) {
        ShowPesanWarning_viDataPasien('Cek kembali alamat lengkap pasien', 'Peringatan');
    }else{
        if (ValidasiEntry_viDataPasien('Simpan Data', true) == 1){
            Ext.Ajax.request({
    			url: baseURL + "index.php/main/CreateDataObj",
    			params: dataparam_datapasviDataPasien(),
    			failure: function (o){
    				ShowPesanError_viDataPasien('Data tidak berhasil diupdate. Hubungi admin! ', 'Edit Data');
    			},
    			success: function (o){
    				var cst = Ext.decode(o.responseText);
    				if (cst.success === true){
    					ShowPesanInfo_viDataPasien('Data berhasil disimpan', 'Edit Data');
    					DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
    				} else if (cst.success === false && cst.pesan === 0){
    					ShowPesanError_viDataPasien('Data tidak berhasil diupdate ', 'Edit Data');
    				} else{
    					ShowPesanError_viDataPasien('Data tidak berhasil diupdate. ', 'Edit Data');
    				}
    			}
    		});
        } else{
            if (mBol === true){
                return false;
            }
        }
    }
}
// End Function datasave_dataPasiDataPasien # --------------

/**
 *	Function : datadelete_viDataPasien
 *	
 *	Sebuah fungsi untuk menghapus data entry 
 */

function datadelete_viDataPasien()
{
    //}
}
// End Function datadelete_viDataPasien # --------------

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
 *	Function : dataGrid_viDataPasien
 *	
 *	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
 */

function dataGrid_viDataPasien(mod_id_viDataPasien)
{/*
 
 
 Public $;
 public $;
 public $;
 public $; */

    var FieldGrid_viDataPasien =
            [
                'KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'TGL_LAHIR_PASIEN', 'AGAMA',
                'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN', 'PENDIDIKAN', 'PEKERJAAN',
                'KD_UNIT', 'TGL_MASUK', 'URUT_MASUK', 'KD_KABUPATEN', 'KABUPATEN', 'KECAMATAN', 'KD_KECAMATAN', 'PROPINSI',
                'KD_PROPINSI', 'KELURAHAN', 'EMAIL', 'HP', 'AYAH', 'IBU',
                'KD_AGAMA', 'KD_PEKERJAAN', 'KD_PENDIDIKAN', 'TELEPON', 'NAMA_UNIT', 'NO_ASURANSI'
            ];
    // Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSourceGrid_viDataPasien = new WebApp.DataStore({
        fields: FieldGrid_viDataPasien

    });



    // Pemangilan Function untuk memangil data yang akan ditampilkan # --------------

    // Grid Data Pasien # --------------
    var GridDataView_viDataPasien = new Ext.grid.EditorGridPanel({
        xtype       : 'editorgrid',
        store       : dataSourceGrid_viDataPasien,
        title       : '',
        // anchor      : '100% 40%',
        columnLines : true,
        border      : false,
		flex:1,
        plugins     : [new Ext.ux.grid.FilterRow()],
        selModel    : new Ext.grid.RowSelectionModel
                        // Tanda aktif saat salah satu baris dipilih # --------------
                        ({
                            singleSelect: true,
                            listeners:
                                {
                                    rowselect: function (sm, row, rec)
                                    {
                                        //rowSelectedGridDataView_viDataPasien = undefined;
    									Ext.getCmp('btnGantiKekompokPasien').disable();
    									Ext.getCmp('btnGantiDokter').disable();
                                        Ext.getCmp('btnUnitPasien').disable();
    									Ext.getCmp('btnHpsdatapasien').disable();
                                        rowSelectedGridDataView_viDataPasien = dataSourceGrid_viDataPasien.getAt(row);
                                    }
                                }
                        }),
                        // Proses eksekusi baris yang dipilih # --------------
                        listeners:
                        {
                            // Function saat ada event double klik maka akan muncul form view # --------------
                            rowdblclick: function (sm, ridx, cidx)
                            {
                                rowSelectedGridDataView_viDataPasien = dataSourceGrid_viDataPasien.getAt(ridx);
                                if (rowSelectedGridDataView_viDataPasien != undefined)
                                {
                                    setLookUpGridDataView_viDataPasien(rowSelectedGridDataView_viDataPasien.data);
                                }else{
                                    ShowPesanWarning_viDataPasien('Silahkan pilih Data Pasien ', 'Edit Data');
                                }
                            },
                            rowclick: function (sm, ridx, cidx)
                            {
                                rowSelectedGridDataView_viDataPasien.data.KD_PASIEN;
                                vNoAsuransi = rowSelectedGridDataView_viDataPasien.data.NO_ASURANSI;
                                RefreshDatahistoribayar(rowSelectedGridDataView_viDataPasien.data.KD_PASIEN);
								
                                //rowSelectedGridDataView_viDataPasien.data.KD_PASIEN;

                            }
                        },
                        /**
                         *	Mengatur tampilan pada Grid Data Pasien
                         *	Terdiri dari : Judul, Isi dan Event
                         *	Isi pada Grid di dapat dari pemangilan dari Net.
                         *	dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSourceGrid_viDataPasien
                         *	didapat dari Function DataRefresh_viDataPasien()
                         */
                        colModel: new Ext.grid.ColumnModel
                                        (
                                                [
                                                    new Ext.grid.RowNumberer(),
                                                    {
                                                        id: 'colNRM_viDaftar',
                                                        header: 'No.Medrec',
                                                        dataIndex: 'KD_PASIEN',
                                                        sortable: true,
                                                        width: 20
//						filter:
//						{
//							type: 'int'
//						}
                                                    },
                                                    {
                                                        id: 'colNMPASIEN_viDaftar',
                                                        header: 'Nama',
                                                        dataIndex: 'NAMA',
                                                        sortable: true,
                                                        width: 40
//						filter:
//						{}
                                                    },
                                                    {
                                                        id: 'colALAMAT_viDaftar',
                                                        header: 'Alamat',
                                                        dataIndex: 'ALAMAT',
                                                        width: 60,
                                                        sortable: true
//						filter: {}
                                                    },
                                                    {
                                                        id: 'colTglKunj_viDaftar',
                                                        header: 'Tgl Lahir Pasien',
                                                        dataIndex: 'TGL_LAHIR_PASIEN',
                                                        width: 20,
                                                        sortable: true,
                                                        //format: 'd/M/Y',
//						filter: {},
														/* 
                                                        renderer: function (v, params, record)
                                                        {
                                                            return ShowDate(record.data.TGL_LAHIR_PASIEN);
                                                        }  */
                                                    }

                                                    //-------------- ## --------------
                                                ]
                                                ),
                                // Tolbar ke Dua # --------------
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            id: 'ToolbarGridDataView_viDataPasien',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Edit Data',
                                                            iconCls: 'Edit_Tr',
                                                            tooltip: 'Edit Data',
                                                            width: 100,
                                                            id: 'BtnEditGridDataView_viDataPasien',
                                                            handler: function (sm, row, rec)
                                                            {
                                                                if (rowSelectedGridDataView_viDataPasien != undefined)
                                                                {
                                                                    setLookUpGridDataView_viDataPasien(rowSelectedGridDataView_viDataPasien.data);
                                                                } else
                                                                {
                                                                    ShowPesanWarning_viDataPasien('Silahkan pilih Data Pasien ', 'Edit Data');
                                                                }
                                                            }
                                                        }
                                                    ]
                                        },
                                bbar: bbar_paging(mod_name_viDataPasien, slctCount_viDataPasien, dataSourceGrid_viDataPasien),
                                // End Button Bar Pagging # --------------
                                viewConfig:
                                {
                                    forceFit: true
                                }
    })
                    DataRefresh_viDataPasien();

                    var top = new Ext.FormPanel(
                            {
                                labelAlign: 'top',
                                frame: true,
                                title: '',
                                bodyStyle: 'padding:5px 5px 0',
                                //width: 600,
                                items: [{
                                        layout: 'column',
                                        items:
                                                [
                                                    {
                                                        columnWidth: .3,
                                                        layout: 'form',
                                                        items:
                                                                [
                                                                    {
                                                                        columnWidth: .5,
                                                                        layout: 'form',
                                                                        items: [
                                                                            {
                                                                                xtype: 'textfield',
                                                                                fieldLabel: 'Alamat Pasien ',
                                                                                name: 'txtAlamatPasien_DataPasien',
                                                                                enableKeyEvents: true,
                                                                                id: 'txtAlamatPasien_DataPasien',
                                                                                anchor: '95%',
                                                                                listeners:
                                                                                        {
                                                                                            'keyup': function ()
                                                                                            {
                                                                                                var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                                DataRefresh_viDataPasien(tmpkriteria);
																								
                                                                                            }
                                                                                        }
                                                                            }
                                                                        ]
                                                                    }
                                                                ]
                                                    }
                                                ]
                                    }]
                            });

                    // Kriteria filter pada Grid # --------------
                    var FrmFilterGridDataView_viDataPasien = new Ext.Panel
                            (
                                    {
                                        title: NamaForm_viDataPasien,
                                        iconCls: icons_viDataPasien,
                                        id: mod_id_viDataPasien,
                                        // region: 'center',
                                        layout: {
											type:'vbox',
											align:'stretch'
										},
                                        closable: true,
										// flex:1,
                                        border: false,
                                        // margins: '0 5 5 0',
                                        items: [
                                            {
                                                layout: 'form',
												// title:'Pencarian',
                                                bodyStyle: 'padding-top:2px',
                                                border: false,
												labelAlign:'right',
                                                items:
                                                        [
                                                            // {
                                                                // xtype: 'tbspacer',
                                                                // height: 3
                                                            // },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'No. Medrec / NIK',
																// labelWidth:200,
                                                                id: 'txtNoMedrec_DataPasien',
                                                                anchor: '40%',
                                                                onInit: function () { },
                                                                listeners:
                                                                        {
                                                                            'specialkey': function ()
                                                                            {
                                                                                var tmpNoMedrec = Ext.get('txtNoMedrec_DataPasien').getValue()
                                                                                // if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                                                {
                                                                                    if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                                                                    {

                                                                                        var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrec_DataPasien').getValue())
                                                                                        Ext.getCmp('txtNoMedrec_DataPasien').setValue(tmpgetNoMedrec);
                                                                                        var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                        DataRefresh_viDataPasien(tmpkriteria);
																						RefreshDatahistoribayar(tmpgetNoMedrec);


                                                                                    } else
                                                                                    {
                                                                                        if (tmpNoMedrec.length === 10)
                                                                                        {
                                                                                            tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                            DataRefresh_viDataPasien(tmpkriteria);
																							RefreshDatahistoribayar(Ext.get('txtNoMedrec_DataPasien').getValue());
																							Ext.Ajax.request
																							(
																									{
																										url: baseURL + "index.php/rawat_jalan/viewkunjungan/ceknikpasien2",
																										params: {part_number_nik: Ext.get('txtNoMedrec_DataPasien').getValue()},
																										failure: function (o)
																										{
																											ShowPesanError_viDataPasien('Pencarian error ! ', 'Data Pasien');
																										},
																										success: function (o)
																										{
																											var cst = Ext.decode(o.responseText);
																											if (cst.success === true)
																											{
																												Ext.getCmp('txtNoMedrec_DataPasien').setValue(cst.nomedrec);
																											}
																											else
																											{
																												//Ext.getCmp('txtNoMedrec_DataPasien').setValue('');
																											}
																										}
																									}
																							) 
                                                                                        } else
                                                                                        {
                                                                                            Ext.getCmp('txtNoMedrec_DataPasien').setValue('')
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                        }
                                                            },
                                                            // {
                                                                // xtype: 'tbspacer',
                                                                // height: 3
                                                            // },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Pasien',
                                                                name: 'txtNamaPasien_DataPasien',
                                                                id: 'txtNamaPasien_DataPasien',
                                                                enableKeyEvents: true,
                                                                anchor: '40%',
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                if (Ext.EventObject.getKey() === 13)
                                                                                {
                                                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    DataRefresh_viDataPasien(tmpkriteria);
																					console.log(dataSourceGrid_viDataPasien.getCount());
																					var x=dataSourceGrid_viDataPasien.getCount();
																					if (x===1)
																					{
																						RefreshDatahistoribayar(Ext.get('txtNoMedrec_DataPasien').getValue());
																					}
																					else
																					{
																						RefreshDatahistoribayar();
																					}
                                                                                }
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Alamat',
                                                                name: 'txtAlamatPasien_DataPasien',
                                                                id: 'txtAlamatPasien_DataPasien',
                                                                enableKeyEvents: true,
                                                                anchor: '60%',
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                if (Ext.EventObject.getKey() === 13)
                                                                                {
                                                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    DataRefresh_viDataPasien(tmpkriteria);
                                                                                    // console.log(dataSourceGrid_viDataPasien.getCount());
                                                                                    var x=dataSourceGrid_viDataPasien.getCount();
                                                                                    if (x===1)
                                                                                    {
                                                                                        RefreshDatahistoribayar(Ext.get('txtNoMedrec_DataPasien').getValue());
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        RefreshDatahistoribayar();
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Keluarga',
                                                                name: 'txtKeluargaPasien_DataPasien',
                                                                id: 'txtKeluargaPasien_DataPasien',
                                                                enableKeyEvents: true,
                                                                anchor: '60%',
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                if (Ext.EventObject.getKey() === 13)
                                                                                {
                                                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    DataRefresh_viDataPasien(tmpkriteria);
                                                                                    // console.log(dataSourceGrid_viDataPasien.getCount());
                                                                                    var x=dataSourceGrid_viDataPasien.getCount();
                                                                                    if (x===1)
                                                                                    {
                                                                                        RefreshDatahistoribayar(Ext.get('txtNoMedrec_DataPasien').getValue());
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        RefreshDatahistoribayar();
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                            },
                                                            // {
                                                                // xtype: 'tbspacer',
                                                                // height: 3
                                                            // },
                                                            //getItemPanelcombofilter(),
                                                            //	getItemPaneltgl_filter()
                                                        ]},
                                            GridDataView_viDataPasien,
                                            // {
                                                // xtype: 'tbspacer',
                                                // height: 7
                                            // }, 
											gridKunjunganpasien_editdata()]

                                    }
                            )
                    return FrmFilterGridDataView_viDataPasien;

                    //-------------- # End form pencarian # --------------
                }
// End Function dataGrid_viDataPasien # --------------

        function gridKunjunganpasien_editdata()
        {
            var fldDetail = ['KD_PASIEN', 'TGL_MASUK', 'NAMA_UNIT', 'URUT', 'DOKTER', 'KD_UNIT', 'JAM', 'TGL_KELUAR', 'CUST', 'NO_TRANSAKSI', 'CO_STATUS', 'NO_SEP', 'KD_DOKTER', 'KD_KASIR', 'KD_CUSTOMER'];
            dskunjunganpasien = new WebApp.DataStore({fields: fldDetail})
            var griddatapasienrwj = new Ext.grid.EditorGridPanel({
                title: 'Kunjungan',
                stripeRows: true,
				height:200,
                store: dskunjunganpasien,
                border: false,
                autoScroll: true,
                columnLines: true,
                frame: false,
                // anchor: '100% 35%',
                sm: new Ext.grid.RowSelectionModel
                ({
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function (sm, row, rec) {
                            cellSelecteddeskripsi_datapasien = dskunjunganpasien.getAt(row);
							
							vCo_status = cellSelecteddeskripsi_datapasien.data.CO_STATUS;

							/* 
								PERBARUAN VARIABLE YANG DIKIRIM UNTUK GANTI KELOMPOK PASIEN
								OLEH 	: HADAD
								TANGGAL : 2016 - 12 - 31
							*/

							vNamaUnit 	= cellSelecteddeskripsi_datapasien.data.NAMA_UNIT;
							vNamaDokter = cellSelecteddeskripsi_datapasien.data.DOKTER;
							vKdDokter 	= cellSelecteddeskripsi_datapasien.data.DOKTER;
							if(vCo_status == "Belum" || vCo_status === false){
								Ext.getCmp('btnGantiKekompokPasien').enable();
								Ext.getCmp('btnGantiDokter').enable();
                                Ext.getCmp('btnUnitPasien').enable();
								Ext.getCmp('btnHpsdatapasien').enable();
							}else{
								Ext.getCmp('btnGantiKekompokPasien').disable();														
								Ext.getCmp('btnUnitPasien').disable();
                                Ext.getCmp('btnGantiDokter').disable();                                                     
								Ext.getCmp('btnHpsdatapasien').disable();														
							}
							selectKdCustomer = cellSelecteddeskripsi_datapasien.data.KD_CUSTOMER; 
							vKdKasir 	= cellSelecteddeskripsi_datapasien.data.KD_KASIR;
							vNoTransaksi= cellSelecteddeskripsi_datapasien.data.NO_TRANSAKSI;
							vCustomer 	= cellSelecteddeskripsi_datapasien.data.CUST;
							vNoSEP 		= cellSelecteddeskripsi_datapasien.data.NO_SEP;
							vKd_Pasien	= cellSelecteddeskripsi_datapasien.data.KD_PASIEN;
							vTanggal	= cellSelecteddeskripsi_datapasien.data.TGL_MASUK;
							vKdUnit		= cellSelecteddeskripsi_datapasien.data.KD_UNIT;
							vUrutMasuk	= cellSelecteddeskripsi_datapasien.data.URUT;
                            console.log(cellSelecteddeskripsi_datapasien.data.NO_TRANSAKSI);
							//loaddatastoredokter();
							cekDetailTransaksi();
							/* ------------------------------------------ END --------------------- */
                            CurrentHistorydatapasien.row = row;
                            CurrentHistorydatapasien.data = cellSelecteddeskripsi_datapasien;
                            datapasienvariable.kd_pasien = CurrentHistorydatapasien.data.data.KD_PASIEN;
                            datapasienvariable.urut = CurrentHistorydatapasien.data.data.URUT;
                            datapasienvariable.tglkunjungan = CurrentHistorydatapasien.data.data.TGL_MASUK;
                            datapasienvariable.kd_unit = CurrentHistorydatapasien.data.data.KD_UNIT;
							
                        }
                    }
				}),
                        tbar: [
                            {
								id: 'btnHpsdatapasien', 
								text: 'Hapus Kunjungan', 
								tooltip: 'Hapus Kunjungan', 
								iconCls: 'RemoveRow',
                                disabled:true,
                                handler: function () {
									if(statusCekDetail == false){
										var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan kunjungan:', function(btn, combo){
											if (btn == 'ok')
											{
												if (combo!='') {
													var params = {
														kd_unit: datapasienvariable.kd_unit,
														Tglkunjungan: datapasienvariable.tglkunjungan,
														Kodepasein: datapasienvariable.kd_pasien,
														urut: datapasienvariable.urut,
														alasan:combo
													};
													loadMask.show();
													Ext.Ajax.request
													({
														url: baseURL + "index.php/main/functionRWJ/deletekunjungan_Revisi",
														// url: baseURL + "index.php/main/functionRWJ/deletekunjungan",
														params: params,
														failure: function (o)
														{
															loadMask.hide();
															// console.log(o);
															ShowPesanError_viDataPasien('Error proses database', 'Hapus Data');
														},
														success: function (o)
														{
															var cst = Ext.decode(o.responseText);
															/*if (cst.success === true && cst.transaksi === true && cst.bayar === false)
															{
																ShowPesanWarning_viDataPasien('Data transaksi tidak berhasil ditemukan', 'Hapus Data Kunjungan');
															} else if (cst.success === true && cst.transaksi === true && cst.bayar === true)
															{
																ShowPesanWarning_viDataPasien('Anda telah melakukan pembayaran', 'Hapus Data Kunjungan');
															} else */if (cst.success === true)
															{
																RefreshDatahistoribayar(datapasienvariable.kd_pasien);
																ShowPesanInfo_viDataPasien('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
															}/* else if (cst.success === false && cst.pesan === 0)
															{
																ShowPesanWarning_viDataPasien('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
															} */else
															{
																if (cst.kd_kasir) {
																	ShowPesanWarning_viDataPasien('Anda tidak diberikan hak akses untuk menghapus data kunjungan '+cst.kd_kasir, 'Hapus Data Kunjungan');
																}else{
																	ShowPesanError_viDataPasien('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
																}
															};
														}
													});
													loadMask.hide();
												} else {
													ShowPesanWarning_viDataPasien('Silahkan isi alasan terlebih dahulu!','Keterangan');

												}
											}

										});
									}else{
										//console.log("kd_kasir = "+vKdKasir+" - no_transaksi = "+vNoTransaksi);
										ShowPesanInfo_viDataPasien("Tidak dapat hapus kunjungan karena sudah ada produk atau tindakan", "Failed");
									}
                                }
                            },{								
								id: 'btnGantiKekompokPasien', text: 'Ganti kelompok pasien', tooltip: 'Ganti kelompok pasien', iconCls: 'gantipasien', disabled:true,hidden:true,
                                handler: function () {
									//Button Ganti Kelompok;
									panelActiveDataPasien = 0;
									KelompokPasienLookUp_rwj();
									/* Ext.Ajax.request
										(
											{
												url: baseURL +  "index.php/rawat_jalan/functionCekDetailTransaksi/cek",   
												params: {
															NOTRANSAKSI  	: cellSelecteddeskripsi_datapasien.data.NO_TRANSAKSI,
															KDKASIR     	: cellSelecteddeskripsi_datapasien.data.KD_KASIR,
															TGLTRANSAKSI   	: cellSelecteddeskripsi_datapasien.data.TGL_MASUK
												},
												failure: function(o)
												{
													ShowPesanWarning_viDataPasien('Cek Item Tindakan gagal', 'Gagal');
												},  
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) 
													{
														
													}else 
													{
														ShowPesanWarning_viDataPasien('Masih terdapat Item Tindakan', 'Gagal');
													};
												}
											}
										); */
									
                                }
							},{								
								id: 'btnGantiDokter', text: 'Ganti Dokter', tooltip: 'Ganti dokter pasien', iconCls: 'gantipasien', disabled:true,hidden:true,
                                handler: function () {
									//Button Ganti Dokter;
									panelActiveDataPasien = 1;
									loaddatastoredokter();
									GantiDokterPasienLookUp_rwj();
                                }
							},{								
								id: 'btnUnitPasien', text: 'Ganti Unit', tooltip: 'Ganti unit pasien', iconCls: 'gantipasien', disabled:true,hidden:true,
                                handler: function () {
									//Button Ganti Unit;
									panelActiveDataPasien = 2;
									//loaddatastoredokter();
									//console.log(statusCekDetail);
									if(statusCekDetail == false){
										KonsultasiDataPasienLookUp_rwj();
									}else{
										//console.log("kd_kasir = "+vKdKasir+" - no_transaksi = "+vNoTransaksi);
										ShowPesanInfo_viDataPasien("Tidak dapat ganti unit karena sudah ada produk atau tindakan", "Failed");
									}
                                }
							}
                        ],
                        cm: kunjunganpasiencolummodel(),
                        viewConfig: {forceFit: true}
                    });
            return griddatapasienrwj;
        }
        ;


        function RefreshDatahistoribayar(kd_pasien) {
            var strKriteriaKasirrwj = '';
            strKriteriaKasirrwj = ' k.kd_pasien= ~' + kd_pasien + '~';
            dskunjunganpasien.load
            ({
                params: {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'tgl_transaksi',
                    //Sort: 'tgl_transaksi',
                    Sortdir: 'ASC',
                    target: 'ViewkunjunganPasien',
                    param: strKriteriaKasirrwj
                }
            });
            return dskunjunganpasien;
        };


        function kunjunganpasiencolummodel()
        {
            return new Ext.grid.ColumnModel(
                    [new Ext.grid.RowNumberer(),
                        {id: 'colKdTransaksi', header: 'No. Transaksi', dataIndex: 'NO_TRANSAKSI', width: 100, menuDisabled: true, hidden: false},
                        {id: 'colenamunit', header: 'Poli', dataIndex: 'NAMA_UNIT', width: 80, hidden: false},
                        {id: 'coldok', header: 'Dokter', dataIndex: 'DOKTER', width: 150, menuDisabled: true, hidden: false},
                        {id: 'colkelpas', header: 'Kelompok Pasien', dataIndex: 'CUST', width: 150, menuDisabled: true, hidden: false},
                        {id: 'colTGlMasuk', header: 'Tgl Masuk', dataIndex: 'TGL_MASUK', menuDisabled: true, width: 100,/* renderer:
                                    function (v, params, record) {
                                        return ShowDate(record.data.TGL_MASUK);
                                    }*/},
                        {
                            id: 'coletglkeluar',
                            header: 'tgl Keluar',
                            menuDisabled: true,
                            dataIndex: 'TGL_KELUAR'

                        }
                        ,
                        {
                            id: 'colStatHistory',
                            header: 'Jam Masuk',
                            menuDisabled: true,
                        dataIndex: 'JAM',
                        }
                        ,
                        {
                            id: 'colStatHistory',
                            header: 'NO SEP',
                            menuDisabled: true,
							dataIndex: 'NO_SEP',
                        }
                        ,
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Masuk',
                            menuDisabled: true,
                            dataIndex: 'URUT',
                            hidden:true

                        },{
                            id: 'colStatusTransaksi',
                            header: 'Status Transaksi',
                            menuDisabled: true,
                            dataIndex: 'CO_STATUS',
                            //hidden:true

                        }



                    ]
                    )
        }
        ;
        /**
         *	Function : setLookUpGridDataView_viDataPasien
         *	
         *	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
         */
function setLookUpGridDataView_viDataPasien(rowdata){
	var lebar = 819; // Lebar Form saat PopUp
	setLookUpsGridDataView_viDataPasien = new Ext.Window({
		id: 'setLookUpsGridDataView_viDataPasien',
		title: NamaForm_viDataPasien,
		closeAction: 'destroy',
		autoScroll: true,
		width: 640,
		height: 425,
		resizable: false,
		border: false,
		plain: true,
		layout: 'fit',
		iconCls: icons_viDataPasien,
		modal: true,
		items: getFormItemEntryGridDataView_viDataPasien(lebar, rowdata),
		listeners:{
			activate: function (){
			},
			afterShow: function (){
				this.activate();
			},
			deactivate: function (){
				rowSelectedGridDataView_viDataPasien = undefined;
				DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
			}
		}
	});
	setLookUpsGridDataView_viDataPasien.show();
	if (rowdata == undefined){
		dataaddnew_viDataPasien();
	} else{
		datainit_viDataPasien(rowdata)
	}
}
// End Function setLookUpGridDataView_viDataPasien # --------------

/**
 *	Function : getFormItemEntryGridDataView_viDataPasien
 *	
 *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
 *	Di pangil dari Function setLookUpGridDataView_viDataPasien
 */
function getFormItemEntryGridDataView_viDataPasien(lebar, rowdata)
{
	var pnlFormDataWindowPopup_viDataPasien = new Ext.FormPanel
			(
					{
						title: '',
						// region: 'center',
						fileUpload: true,
						margin:true,
						// layout: 'anchor',
						// padding: '8px',
						bodyStyle: 'padding-top:5px;',
						// Tombol pada tollbar Edit Data Pasien
						tbar: {
							xtype: 'toolbar',
							items:
									[
										{
											xtype: 'button',
											text: 'Save',
											id: 'btnSimpanWindowPopup_viDataPasien',
											iconCls: 'save',
											handler: function (){
												datasave_dataPasiDataPasien(false); //1.1
												DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
											}
										},
										//-------------- ## --------------
										{
											xtype: 'tbseparator'
										},
										//-------------- ## --------------
										{
											xtype: 'button',
											text: 'Save & Close',
											id: 'btnSimpanExitWindowPopup_viDataPasien',
											iconCls: 'saveexit',
											handler: function ()
											{
												var x = datasave_dataPasiDataPasien(false); //1.2
												DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
												if (x === undefined)
												{
													setLookUpsGridDataView_viDataPasien.close();
												}
											}
										},
										//-------------- ## --------------
										{
											xtype: 'tbseparator'
										},
										//-------------- ## --------------
									]
						},
						//-------------- #items# --------------
						items:
								[
									// Isian Pada Edit Data Pasien
									{
										xtype: 'panel',
										title: '',
										layout:'form',
										margin:true,
										border:false,
										labelAlign: 'right',
										width: 610,
										items:
												[//---------------# Penampung data untuk fungsi update # ---------------
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpAgama_viDataPasien',
														name: 'TxtTmpAgama_viDataPasien',
														readOnly: true,
														hidden: true
													},
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpPekerjaan_viDataPasien',
														name: 'TxtTmpPekerjaan_viDataPasien',
														readOnly: true,
														hidden: true
													},
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpPendidikan_viDataPasien',
														name: 'TxtTmpPendidikan_viDataPasien',
														readOnly: true,
														hidden: true
													},
													//------------------------ end ---------------------------------------------								
													//-------------- # Pelengkap untuk kriteria ke Net. # --------------
													{
														xtype: 'textfield',
														fieldLabel: 'KD_CUSTOMER',
														id: 'TxtWindowPopup_KD_CUSTOMER_viDataPasien',
														name: 'TxtWindowPopup_KD_CUSTOMER_viDataPasien',
														readOnly: true,
														hidden: true
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'URUT_MASUK',
														id: 'TxtWindowPopup_Urut_Masuk_viDataPasien',
														name: 'TxtWindowPopup_Urut_Masuk_viDataPasien',
														readOnly: true,
														hidden: true
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'KD_PASIEN',
														id: 'TxtWindowPopup_KD_PASIEN_viDataPasien',
														name: 'TxtWindowPopup_KD_PASIEN_viDataPasien',
														readOnly: true,
														hidden: true
													},
													//-------------- # End Pelengkap untuk kriteria ke Net. # --------------
													{
														xtype: 'textfield',
														fieldLabel: 'No. MedRec',
														id: 'TxtWindowPopup_NO_RM_viDataPasien',
														name: 'TxtWindowPopup_NO_RM_viDataPasien',
														emptyText: 'No. MedRec',
														labelSeparator: '',
														readOnly: true,
														flex: 1,
														width: 195
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'Nama',
														id: 'TxtWindowPopup_NAMA_viDataPasien',
														name: 'TxtWindowPopup_NAMA_viDataPasien',
														emptyText: 'Nama',
														labelSeparator: '',
														flex: 1,
														anchor: '100%'
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Tempat Lahir',
														labelSeparator: '',
														anchor: '100%',
														width: 250,
														items:
																[
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_TEMPAT_LAHIR_viDataPasien',
																		name: 'TxtWindowPopup_TEMPAT_LAHIR_viDataPasien',
																		emptyText: 'Tempat Lahir',
																		flex: 1,
																		width: 195
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'tbspacer',
																		width: 17,
																		height: 23
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Tgl. Lahir',
																		style: {'text-align': 'right'},
																		id: '',
																		name: '',
																		flex: 1,
																		width: 58
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'datefield',
																		fieldLabel: 'Tanggal',
																		id: 'DateWindowPopup_TGL_LAHIR_viDataPasien',
																		name: 'DateWindowPopup_TGL_LAHIR_viDataPasien',
																		width: 198,
																		format: 'd/m/Y',
                                                                        listeners:{
                                                                            'specialkey' : function(){                      
                                                                                if (Ext.EventObject.getKey() == 9 ||  Ext.EventObject.getKey() == 13){                 
                                                                                    var tmptanggal = Ext.get('DateWindowPopup_TGL_LAHIR_viDataPasien').getValue();
                                                                                    Ext.Ajax.request({
                                                                                        url: baseURL + "index.php/main/GetUmur",
                                                                                        params: {
                                                                                            TanggalLahir: Ext.getCmp('DateWindowPopup_TGL_LAHIR_viDataPasien').getValue()
                                                                                        },
                                                                                        success: function(o){
                                                                                            var tmphasil = o.responseText;
                                                                                            var tmp = tmphasil.split(' ');
                                                                                            if (tmp.length == 6){
                                                                                                Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                                                                                                Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[2]);
                                                                                                Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[4]);
                                                                                                getParamBalikanHitungUmurDataPasien(tmptanggal);
                                                                                            }else if(tmp.length == 4){
                                                                                                if(tmp[1]== 'years' && tmp[3] == 'day'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[2]);  
                                                                                                }else if(tmp[1]== 'years' && tmp[3] == 'days'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[2]);  
                                                                                                }else if(tmp[1]== 'year' && tmp[3] == 'days'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[2]);  
                                                                                                }else{
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[2]);
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
                                                                                                }
                                                                                                getParamBalikanHitungUmurDataPasien(tmptanggal);
                                                                                            }else if(tmp.length == 2 ){
                                                                                                if (tmp[1] == 'year' ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
                                                                                                }else if (tmp[1] == 'years' ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
                                                                                                }else if (tmp[1] == 'mon'  ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
                                                                                                }else if (tmp[1] == 'mons'  ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
                                                                                                }else{
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[0]);
                                                                                                }
                                                                                                getParamBalikanHitungUmurDataPasien(tmptanggal);
                                                                                            }else if(tmp.length == 1){
                                                                                                Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
                                                                                                Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                                                                                                Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('1');
                                                                                                getParamBalikanHitungUmurDataPasien(tmptanggal);
                                                                                            }else{
                                                                                                alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                                                                                            }                                                               
                                                                                        }
                                                                                    });
                                                                                }
                                                                            },
                                                                        }
																	}
																	//-------------- ## --------------				
																]
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Umur',
														labelSeparator: '',
														anchor: '100%',
														width: 50,
														items:
																[
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_TAHUN_viDataPasien',
																		name: 'TxtWindowPopup_TAHUN_viDataPasien',
																		emptyText: 'Thn',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Thn',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_BULAN_viDataPasien',
																		name: 'TxtWindowPopup_BULAN_viDataPasien',
																		emptyText: 'Bln',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Bln',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_HARI_viDataPasien',
																		name: 'TxtWindowPopup_HARI_viDataPasien',
																		emptyText: 'Hari',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Hari',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	}
																	//-------------- ## --------------			
																]
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Jenis Kelamin',
														labelSeparator: '',
														anchor: '100%',
														width: 250,
														items:
																[
																	mComboJK(),
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Golongan Darah',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 90
																	},
																	mComboGolDarah()
																			//-------------- ## --------------				
																]
													},
													//-------------- ## --------------						

													//-------------- ## --------------
												]
									},
									//-------------- ## --------------
									/*{
									 xtype: 'spacer',
									 width: 10,
									 height: 1
									 },*/
									//-------------- ## --------------
									tabsWindowPopupDataLainnya_viDataPasien(rowdata)
											//-------------- ## --------------
								]
								//-------------- #items# --------------
					}
			)
	return pnlFormDataWindowPopup_viDataPasien;
}
// End Function getFormItemEntryGridDataView_viDataPasien # --------------

        /**
         *	Function : tabsWindowPopupDataLainnya_viDataPasien
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit pada panel
         *	Di pangil dari Function getFormItemEntryGridDataView_viDataPasien
         */
        function tabsWindowPopupDataLainnya_viDataPasien(rowdata)
        {
            var TabsDataLainnya_viDataPasien = new Ext.TabPanel
                    (
                            {
                                id: 'GDtabDetailIGD',
                                //region: 'center',
                                activeTab: 2,
                                // anchor: '100% 400%',
                                border: false,
                                // plain: true,
                                defaults:
                                        {
                                            autoScroll: true
                                        },
                                items: [
                                    tabsDetailWindowPopupDataLainnyaAktif_viDataPasien(rowdata),
                                    tabsDetailWindowPopupkontak_viDataPasien(rowdata),
                                    tabsDetailWindowPopupalamatpasien_viDataPasien(rowdata),
                                    tabsDetailWindowPopupkeluarga_viDataPasien(rowdata)


                                ]
                            }
                    );

            return TabsDataLainnya_viDataPasien;
        }
// End Function tabsWindowPopupDataLainnya_viDataPasien # --------------

        /**
         *	Function : tabsDetailWindowPopupDataLainnyaAktif_viDataPasien
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit pada panel
         *	Di pangil dari Function tabsWindowPopupDataLainnya_viDataPasien
         */
        function tabsDetailWindowPopupDataLainnyaAktif_viDataPasien(rowdata)
        {
            var itemstabsDetailWindowPopupDataLainnyaAktif_viDataPasien =
                    {
                        xtype: 'panel',
                        title: 'Data Pasien',
                        id: 'tabsDetailWindowPopupDataLainnyaAktifItem_viDataPasien',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 110,
                        border: false,
                        items:
                                [
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboAgamaRequestEntry(),
                                                    mComboSatusMarital()
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboPekerjaanRequestEntry(),
                                                    mComboPendidikanRequestEntry()
                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupDataLainnyaAktif_viDataPasien;
        }
// End Function tabsDetailWindowPopupDataLainnyaAktif_viDataPasien # --------------



        function tabsDetailWindowPopupalamatpasien_viDataPasien(rowdata)
        {
            var itemstabsDetailWindowPopupalamatpasien_viDataPasien =
                    {
                        xtype: 'panel',
                        title: 'Tempat Tinggal',
                        id: 'tabsDetailWindowPopupAlamatpasienItem_viDataPasien',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 150,
                        border: false,
                        items:
                                [
                                    //-------------- ## --------------
                                    {
                                        xtype: 'textarea',
                                        fieldLabel: 'Alamat',
                                        id: 'TxtWindowPopup_ALAMAT_viDataPasien',
                                        name: 'TxtWindowPopup_ALAMAT_viDataPasien',
                                        emptyText: 'Alamat',
                                        labelSeparator: '',
                                        anchor: '100%',
                                        flex: 1
                                    },
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboPropinsi_EditdataPasien(),
                                                    mComboKabupaten_datapasien(),
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboKecamatan_editdatapasien(),
                                                    mCombokelurahan_editdatapasien()
                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupalamatpasien_viDataPasien;
        }


        function loaddatastorekelurahan(kd_kecamatan)
        {
            dsKelurahan.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            Sort: 'kelurahan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKelurahan',
                                            param: 'kd_kecamatan=' + kd_kecamatan
                                        }
                            }
                    )
        }


        function mCombokelurahan_editdatapasien()
        {
            var Field = ['KD_KELURAHAN', 'KD_KECAMATAN', 'KELURAHAN'];
            dsKelurahan = new WebApp.DataStore({fields: Field});

            var cbokelurahan_editdatapasien = new Ext.form.ComboBox
                    (
                            {
                                id: 'cbokelurahan_editdatapasien',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Kelurahan...',
                                fieldLabel: 'Kelurahan',
                                align: 'Right',
                                tabIndex: 21,
//		    anchor:'60%',
                                store: dsKelurahan,
                                valueField: 'KD_KELURAHAN',
                                displayField: 'KELURAHAN',
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                kelurahanpasien = b.data.KD_KELURAHAN;
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    {
                                                        Ext.getCmp('txtpos').focus();
                                                    }
                                                    //atau Ext.EventObject.ENTER
                                                    else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {

                                                        kelurahanpasien = c.value;
                                                    }
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cbokelurahan_editdatapasien;
        }
        ;


function mComboPropinsi_EditdataPasien(){
	var Field = ['KD_PROPINSI', 'PROPINSI'];
	dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
	dsPropinsiRequestEntry.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'propinsi',
			Sortdir: 'ASC',
			target: 'ViewComboPropinsi',
			param: ''
		}
	});
	var cboPropinsi_EditdataPasien = new Ext.form.ComboBox({
		id: 'cboPropinsi_EditdataPasien',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		forceSelection: true,
		emptyText: 'Pilih a Propinsi...',
		selectOnFocus: true,
		fieldLabel: 'Propinsi',
		align: 'Right',
		store: dsPropinsiRequestEntry,
		valueField: 'KD_PROPINSI',
		displayField: 'PROPINSI',
		anchor: '99%',
		tabIndex: 18,
		listeners:{
			'select': function (a, b, c){
                Ext.getCmp('cboKabupaten_editdatapasien').setValue('');
                Ext.getCmp('cboKecamatan_editdatapasien').setValue('');
                Ext.getCmp('cbokelurahan_editdatapasien').setValue('');
				selectPropinsiRequestEntry = b.data.KD_PROPINSI;
				loaddatastorekabupaten(b.data.KD_PROPINSI);
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) //atau Ext.EventObject.ENTER
					{
						Ext.getCmp('cboKabupaten_editdatapasien').focus();
						selectPropinsiRequestEntry = c.value;
						loaddatastorekabupaten(selectPropinsiRequestEntry);
					} else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
					{
						selectPropinsiRequestEntry = c.value;
						loaddatastorekabupaten(selectPropinsiRequestEntry);
					}
				}, c);
			}
		}
	});
	return cboPropinsi_EditdataPasien;
}
        function mComboKabupaten_datapasien()
        {
            var Field = ['KD_KABUPATEN', 'KD_PROPINSI', 'KABUPATEN'];
            dsKabupatenRequestEntry = new WebApp.DataStore({fields: Field});

            var cboKabupaten_editdatapasien = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboKabupaten_editdatapasien',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Kabupaten...',
                                fieldLabel: 'Kab/Kod',
                                align: 'Right',
                                store: dsKabupatenRequestEntry,
                                valueField: 'KD_KABUPATEN',
                                displayField: 'KABUPATEN',
                                tabIndex: 19,
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                Ext.getCmp('cboKecamatan_editdatapasien').setValue('');
                                                Ext.getCmp('cbokelurahan_editdatapasien').setValue('');
                                                selectKabupatenRequestEntry = b.data.KD_KABUPATEN;
                                                loaddatastorekecamatan(b.data.KD_KABUPATEN);
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    {
                                                        Ext.getCmp('cboKecamatan_editdatapasien').focus();
                                                        selectKabupatenRequestEntry = c.value;
                                                        loaddatastorekecamatan(selectKabupatenRequestEntry);
                                                    } else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {
                                                        selectKabupatenRequestEntry = c.value;
                                                        loaddatastorekecamatan(selectKabupatenRequestEntry);
                                                    }
                                                }, c);
                                            }

                                        }
                            }
                    );
            return cboKabupaten_editdatapasien;
        }
        ;


        function loaddatastorekabupaten(kd_propinsi)
        {
            dsKabupatenRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'kabupaten',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKabupaten',
                                            param: 'kd_propinsi=' + kd_propinsi
                                        }
                            }
                    )
        }

        function loaddatastorekecamatan(kd_kabupaten)
        {
            dsKecamatanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            Sort: 'kecamatan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKecamatan',
                                            param: 'kd_kabupaten=' + kd_kabupaten
                                        }
                            }
                    )
        }


        function mComboKecamatan_editdatapasien()
        {
            var Field = ['KD_KECAMATAN', 'KD_KABUPATEN', 'KECAMATAN'];
            dsKecamatanRequestEntry = new WebApp.DataStore({fields: Field});

            var cboKecamatan_editdatapasien = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboKecamatan_editdatapasien',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'pilih Kecamatan...',
                                fieldLabel: 'Kecamatan',
                                align: 'Right',
                                tabIndex: 20,
                                store: dsKecamatanRequestEntry,
                                valueField: 'KD_KECAMATAN',
                                displayField: 'KECAMATAN',
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                Ext.getCmp('cbokelurahan_editdatapasien').setValue();
                                                selectKecamatanpasien = b.data.KD_KECAMATAN;
                                                loaddatastorekelurahan(b.data.KD_KECAMATAN)
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13)
                                                    {
                                                        Ext.getCmp('cbokelurahan_editdatapasien').focus();
                                                        selectKecamatanpasien = c.value;
                                                    }																//atau Ext.EventObject.ENTER
                                                    else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {
                                                        loaddatastorekelurahan(c.value);
                                                        selectKecamatanpasien = c.value;
                                                    }
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboKecamatan_editdatapasien;
        }
        ;


        function tabsDetailWindowPopupkontak_viDataPasien(rowdata)
        {
            var itemstabsDetailWindowPopupkontak_viDataPasien =
                    {
                        xtype: 'panel',
                        title: 'Kontak',
                        id: 'tabsDetailWindowPopupDatakontakItem_viDataPasien',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 90,
                        border: false,
                        items:
                                [
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: ' No. Tlpn' + ' ',
                                                        id: 'TxtWindowPopup_NO_TELP_viDataPasien',
                                                        name: 'TxtWindowPopup_NO_TELP_viDataPasien',
                                                        //emptyText: '',
                                                        anchor: '95%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: ' Email' + ' ',
                                                        id: 'TxtWindowPopup_Email_viDataPasien',
                                                        name: 'TxtWindowPopup_Email_viDataPasien',
                                                        anchor: '95%'
                                                    }
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'No. Hp',
                                                        id: 'TxtWindowPopup_HP_viDataPasien',
                                                        name: 'TxtWindowPopup_HP_viDataPasien',
                                                        anchor: '95%'
                                                    }


                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupkontak_viDataPasien;
        }




        function tabsDetailWindowPopupkeluarga_viDataPasien(rowdata)
        {
            var itemstabsDetailWindowPopupkeluarga_viDataPasien =
                    {
                        xtype: 'panel',
                        title: 'Keluarga',
                        id: 'tabsDetailWindowPopupDataKeluargaItem_viDataPasien',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 90,
                        border: false,
                        items:
                                [
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nama Ayah' + ' ',
                                                        id: 'TxtWindowPopup_Nama_ayah_viDataPasien',
                                                        name: 'TxtWindowPopup_Nama_ayah_viDataPasien',
                                                        //emptyText: '',
                                                        anchor: '95%'
                                                    }
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nama Ibu',
                                                        id: 'TxtWindowPopup_NamaIbu_viDataPasien',
                                                        name: 'TxtWindowPopup_NamaIbu_viDataPasien',
                                                        anchor: '95%'
                                                    }


                                                ]
                                            }]
                                    },{
										xtype: 'textfield',
										fieldLabel: 'Nama Keluarga',
										id: 'TxtWindowPopup_Nama_keluarga_viDataPasien'
									}
                                ]
                    }
            return itemstabsDetailWindowPopupkeluarga_viDataPasien;
        }


        function getCriteriaFilter_viDaftar()
        {
            var strKriteria = "";

            if (Ext.get('txtNoMedrec_DataPasien').getValue() != "")
            {
                strKriteria = " pasien.kd_pasien = " + "'" + Ext.get('txtNoMedrec_DataPasien').getValue() + "'"+ " or pasien.part_number_nik= "+ "'" + Ext.get('txtNoMedrec_DataPasien').dom.value + "'" ;
				
            }

            if (Ext.get('txtNamaPasien_DataPasien').getValue() != "")
            {
                if (strKriteria == "")
                {
                    strKriteria = " lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasien_DataPasien').getValue() + "%')";
                } else
                {
                    strKriteria += " and lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasien_DataPasien').getValue() + "%')";
                }

            }
            
            if (Ext.get('txtAlamatPasien_DataPasien').getValue() != "")
            {
                if (strKriteria == "")
                {
                    strKriteria = " lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamatPasien_DataPasien').getValue() + "%')";
                } else
                {
                    strKriteria += " and lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamatPasien_DataPasien').getValue() + "%')";
                }

            }

            if (Ext.get('txtKeluargaPasien_DataPasien').getValue() != "")
            {
                if (strKriteria == "")
                {
                    strKriteria = " lower(pasien.nama_keluarga) " + "LIKE lower('" + Ext.get('txtKeluargaPasien_DataPasien').getValue() + "%')";
                } else
                {
                    strKriteria += " and lower(pasien.nama_keluarga) " + "LIKE lower('" + Ext.get('txtKeluargaPasien_DataPasien').getValue() + "%')";
                }

            }
            return strKriteria;
        }

// --------------------------------------- # End Form # ---------------------------------------


// End Project Data Pasien # --------------

        function mComboPekerjaanRequestEntry()
        {
            var Field = ['KD_PEKERJAAN', 'PEKERJAAN'];

            dsPekerjaanRequestEntry = new WebApp.DataStore({fields: Field});
            dsPekerjaanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'pekerjaan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboPekerjaan',
                                            param: ''
                                        }
                            }
                    )

            var cboPekerjaanRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboPekerjaanRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Pekerjaan...',
                                fieldLabel: 'Pekerjaan',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsPekerjaanRequestEntry,
                                valueField: 'KD_PEKERJAAN',
                                displayField: 'PEKERJAAN',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectPekerjaanRequestEntry = b.data.KD_PROPINSI;
                                                Ext.getCmp('cboPekerjaanRequestEntry').setValue(b.data.KD_PEKERJAAN);
                                                Ext.getCmp('TxtTmpPekerjaan_viDataPasien').setValue(b.data.KD_PEKERJAAN);
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboSukuRequestEntry').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboPekerjaanRequestEntry;
        }
        ;

        function mComboPendidikanRequestEntry()
        {
            var Field = ['KD_PENDIDIKAN', 'PENDIDIKAN'];

            dsPendidikanRequestEntry = new WebApp.DataStore({fields: Field});
            dsPendidikanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'pendidikan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboPendidikan',
                                            param: ''
                                        }
                            }
                    )

            var cboPendidikanRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboPendidikanRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Pendidikan...',
                                fieldLabel: 'Pendidikan',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsPendidikanRequestEntry,
                                valueField: 'KD_PENDIDIKAN',
                                displayField: 'PENDIDIKAN',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectPendidikanRequestEntry = b.data.KD_PENDIDIKAN;
                                                Ext.getCmp('cboPendidikanRequestEntry').setValue(b.data.KD_PENDIDIKAN);
                                                Ext.getCmp('TxtTmpPendidikan_viDataPasien').setValue(b.data.KD_PENDIDIKAN);
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboPekerjaanRequestEntry').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboPendidikanRequestEntry;
        }
        ;

        function mComboAgamaRequestEntry()
        {
            var Field = ['KD_AGAMA', 'AGAMA'];

            dsAgamaRequestEntry = new WebApp.DataStore({fields: Field});
            dsAgamaRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'agama',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboAgama',
                                            param: ''
                                        }
                            }
                    )

            var cboAgamaRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboAgamaRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Agama...',
                                fieldLabel: 'Agama',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsAgamaRequestEntry,
                                valueField: 'KD_AGAMA',
                                displayField: 'AGAMA',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectAgamaRequestEntry = b.data.KD_AGAMA;
                                                Ext.getCmp('cboAgamaRequestEntry').setValue(b.data.KD_AGAMA);
                                                Ext.getCmp('TxtTmpAgama_viDataPasien').setValue(b.data.KD_AGAMA);

                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboGolDarah').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboAgamaRequestEntry;
        }
        ;

        function mComboSatusMarital()
        {
            var cboStatusMarital = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboStatusMarital',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Status...',
                                fieldLabel: 'Status Marital ',
                                width: 110,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[0, 'Blm Kawin'], [1, 'Kawin'], [2, 'Janda'], [3, 'Duda']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetSatusMarital,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetSatusMarital = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtAlamat').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboStatusMarital;
        }
        ;

        function mComboGolDarah()
        {
            var cboGolDarah = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboGolDarah',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                //allowBlank: false,
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Gol. Darah...',
                                fieldLabel: 'Gol. Darah ',
                                width: 50,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[0, '-'], [1, 'A+'], [2, 'B+'], [3, 'AB+'], [4, 'O+'], [5, 'A-'], [6, 'B-'], [7, 'AB-'], [8, 'O-']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetGolDarah,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetGolDarah = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboWarga').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboGolDarah;
        }
        ;

        function mComboJK()
        {
			
            var cboJK = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboJK',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Jenis Kelamin...',
                                fieldLabel: 'Jenis Kelamin ',
                                width: 100,
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetJK,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetJK = b.data.displayText;
//                                        getdatajeniskelamin(b.data.displayText)
                                                //alert(jenis_kelamin)
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtTempatLahir').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboJK;
        }
        ;


        function form_histori() {
            var form = new Ext.form.FormPanel({
                baseCls: 'x-plain',
                labelWidth: 55,
                url: 'save-form.php',
                defaultType: 'textfield',
                items:
                        [
                            {
                                x: 0,
                                y: 60,
                                xtype: 'textarea',
                                id: 'TxtHistoriDeleteDataPasien',
                                hideLabel: true,
                                name: 'msg',
                                anchor: '100% 100%'  // anchor width by percentage and height by raw adjustment
                            }

                        ]
            });

            var window = new Ext.Window({
                title: 'Pesan',
                width: 400,
                height: 300,
                minWidth: 300,
                minHeight: 200,
                layout: 'fit',
                plain: true,
                bodyStyle: 'padding:5px;',
                buttonAlign: 'center',
                items: form,
                buttons: [{
                        xtype: 'button',
                        text: 'Save',
                        //height: 20,
                        id: 'btnSimpanHistori_viDataPasien',
                        iconCls: 'save',
                        handler: function ()
                        {
                            datasavehistori_viDataPasien(false); //1.1
                            window.close();
                            setLookUpsGridDataView_viDataPasien.close();
                            //DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
                        }
                    }]
            });

            window.show();
        }
        ;

        function item()
        {
            var pnlFormDataHistori_viDataPasien = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'center',
                                fileUpload: true,
                                layout: 'anchor',
                                padding: '8px',
                                bodyStyle: 'padding:10px 0px 10px 10px;',
                                items:
                                        [
                                        ]
                            }
                    );
            return pnlFormDataHistori_viDataPasien;
        }
		
/* 
	PERBARUAN GANTI KELOMPOK PASIEN 
	OLEH 	: HADAD
	TANGGAL : 2016 - 12 - 30
*/
/* =============================================================================================================================================== */
function KelompokPasienLookUp_rwj(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien_rwj = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien_rwj(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien_rwj.show();
    KelompokPasienbaru_rwj();

};

function KelompokPasienbaru_rwj() 
{
	jeniscus_RWJ=0;
    KelompokPasienAddNew_RWJ = true;
    Ext.getCmp('cboKelompokpasien_RWJ').show()
	Ext.getCmp('txtCustomer_rwjLama').disable();
	Ext.get('txtCustomer_rwjLama').dom.value=vCustomer;
	Ext.get('txtRWJNoSEP').dom.value = vNoSEP;
	
	RefreshDatacombo_rwj(jeniscus_RWJ);
};

function getFormEntryTRKelompokPasien_rwj(lebar) 
{
    var pnlTRKelompokPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien_igd',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
					getItemPanelInputKelompokPasien_rwj(lebar),
					getItemPanelButtonKelompokPasien_rwj(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasien = new Ext.Panel
	(
		{
		    id: 'FormDepanKelompokPasien',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien_igd	
				
			]

		}
	);

    return FormDepanKelompokPasien
};

function getItemPanelInputKelompokPasien_rwj(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama_rwj(lebar),	
					getItemPanelNoTransksiKelompokPasien_rwj(lebar)	,
					
				]
			}
		]
	};
    return items;
};


/* =============================================================================================================================================== */
/* 
	PERBARUAN GANTI DOKTER  
	OLEH 	: HADAD
	TANGGAL : 2017 - 01 - 05
*/
/* =============================================================================================================================================== */
function GantiDokterPasienLookUp_rwj(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRGantiDokter_rwj = new Ext.Window
    (
        {
            id: 'idGantiDokter',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiDokter_rwj(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRGantiDokter_rwj.show();
    GantiPasien_rwj();

};

function GantiPasien_rwj() 
{
	//RefreshDatacombo_rwj(jeniscus_RWJ);
};

function getFormEntryTRGantiDokter_rwj(lebar) 
{
    var pnlTRKelompokPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRGanti',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
					getItemPanelInputGantiDokter_rwj(lebar),
					getItemPanelButtonKelompokPasien_rwj(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiDokter = new Ext.Panel
	(
		{
		    id: 'FormDepanGantiDokter',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien_igd	
				
			]

		}
	);

    return FormDepanGantiDokter
};

function getItemPanelInputGantiDokter_rwj(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[	
					{
						xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'txtUnitAsal_DataPasien',
					    id: 'txtUnitAsal_DataPasien',
						value:vNamaUnit,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'txtDokterAsal_DataPasien',
					    id: 'txtDokterAsal_DataPasien',
						value:vNamaDokter,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},
					mComboDokterGantiEntry()
				]
			}
		]
	};
    return items;
};


/* =============================================================================================================================================== */
/* 
	PERBARUAN GANTI UNIT
	HADAD
	2017 - 01 -05
 */
 
/* =============================================================================================================================================== */
function KonsultasiDataPasienLookUp_rwj(lebar){
	
    var lebar = 440;
    FormLookUpsdetailTRKonsultasi_DataPasien = new Ext.Window
    (
        {
            id: 'idKonsultasiDataPasien',
            title: 'Ganti Unit',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiUnit_rwj(lebar),
            listeners:
            {
                
            }
        }
    );
    FormLookUpsdetailTRKonsultasi_DataPasien.show();
    //KonsultasiAddNew();
};



function getFormEntryTRGantiUnit_rwj(lebar) 
{
    var pnlTRUnitPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRGantiUnit',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar-35,
            border: false,
            items: [
					getItemPanelInputGantiUnit_rwj(lebar),
					getItemPanelButtonKelompokPasien_rwj(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiUnit = new Ext.Panel
	(
		{
		    id: 'FormDepanGantiUnit',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRUnitPasien_igd]

		}
	);

    return FormDepanGantiUnit
};


function getItemPanelInputGantiUnit_rwj(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[	
					{
						xtype: 'textfield',
					    fieldLabel:  'Poli Asal ',
					    name: 'txtUnitAsal_DataPasien',
					    id: 'txtUnitAsal_DataPasien',
						value:vNamaUnit,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},
					mComboPoliklinik(),
					mComboDokterGantiEntry()
				]
			}
		]
	};
    return items;
};
/* =============================================================================================================================================== */

function getItemPanelButtonKelompokPasien_rwj(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id:Nci.getId(),
						handler:function()
						{
							if(panelActiveDataPasien == 0){
								Datasave_Kelompokpasien_rwj();
							}
							if(panelActiveDataPasien == 1){
								Datasave_GantiDokter_rwj();
							}
							if(panelActiveDataPasien == 2){
								Datasave_GantiUnit_rwj();
							}
						}
					},
					{
						xtype:'button',
						text:'Tutup',
						width:70,
						hideLabel:true,
						id:Nci.getId(),
						handler:function() 
						{
							if(panelActiveDataPasien == 0){
								FormLookUpsdetailTRKelompokPasien_rwj.close();
							}
							
							if(panelActiveDataPasien == 1){
								FormLookUpsdetailTRGantiDokter_rwj.close();
							}
							
							if(panelActiveDataPasien == 2){
								FormLookUpsdetailTRKonsultasi_DataPasien.close();
							}
						}
					}
				]
			}
		]
	}
    return items;
};

function getKelompokpasienlama_rwj(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height: 2
						},
						{
							xtype: 'textfield',
							fieldLabel: 'Kelompok Pasien Asal',
							name: 'txtCustomer_rwjLama',
							id: 'txtCustomer_rwjLama',
							labelWidth:130,
							editable: false,
							width: 100,
							anchor: '95%'
						 }
					]
			}
			
		]
	}
    return items;
};
function getItemPanelNoTransksiKelompokPasien_rwj(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height:3
					},{ 
					    xtype: 'combo',
						fieldLabel: 'Kelompok Pasien Baru',
						id: 'kelPasien_RWJ',
						editable: false,
						store: new Ext.data.ArrayStore
							(
								{
								id: 0,
								fields:
								[
								'Id',
								'displayText'
								],
								   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
								}
							),
							  displayField: 'displayText',
							  mode: 'local',
							  width: 100,
							  forceSelection: true,
							  triggerAction: 'all',
							  emptyText: 'Pilih Salah Satu...',
							  selectOnFocus: true,
							  anchor: '95%',
							  listeners:
								 {
										'select': function(a, b, c)
									{
										if(b.data.displayText =='Perseorangan')
										{jeniscus_RWJ='0'
											//Ext.getCmp('txtRWJNoSEP').disable();
											//Ext.getCmp('txtRWJNoAskes').disable();
											}
										else if(b.data.displayText =='Perusahaan')
										{jeniscus_RWJ='1';
											//Ext.getCmp('txtRWJNoSEP').disable();
											//Ext.getCmp('txtRWJNoAskes').disable();
											}
										else if(b.data.displayText =='Asuransi')
										{jeniscus_RWJ='2';
											//Ext.getCmp('txtRWJNoSEP').enable();
											//Ext.getCmp('txtRWJNoAskes').enable();
										}
										
										RefreshDatacombo_rwj(jeniscus_RWJ);
									}

								}
						},{
							columnWidth: .990,
							layout: 'form',
							border: false,
							labelWidth:130,
							items:
							[
												mComboKelompokpasien_rwj()
							]
						},{
							xtype: 'textfield',
							fieldLabel:'No SEP  ',
							name: 'txtRWJNoSEP',
							id: 'txtRWJNoSEP',
							width: 100,
							anchor: '99%'
						 }, {
                            xtype: 'textfield',
                            fieldLabel:'No Asuransi  ',
                            name: 'txtRWJNoAskes',
                            id: 'txtRWJNoAskes',
                            width: 100,
                            anchor: '99%',
                            value : vNoAsuransi,
						 }
									
				]
			}
			
		]
	}
    return items;
};

function mComboKelompokpasien_rwj()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viPJ_IGD = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	if (jeniscus_RWJ===undefined || jeniscus_RWJ==='')
	{
		jeniscus_RWJ=0;
	}
	ds_customer_viPJ_IGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_RWJ +'~'
            }
        }
    )
    var cboKelompokpasien_RWJ = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien_RWJ',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: labelisi_RWJ,
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viPJ_IGD,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien_RWJ;
};


function RefreshDatacombo_rwj(jeniscus_RWJ) 
{

    ds_customer_viPJ_IGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_RWJ +'~ and kontraktor.kd_customer not in(~'+ vkode_customer_RWJ+'~)'
            }
        }
    )
	
    return ds_customer_viPJ_IGD;
};

/* 
	============================= AJAX 
 */
function Datasave_Kelompokpasien_rwj(mBol) 
{	
    if (ValidasiEntryUpdateKelompokPasien_RWJ(nmHeaderSimpanData,false) == 1 )
    {           

        var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan ganti kelompok:', function(btn, combo){
            if (btn == 'ok')
            {
                if (combo!='') {
                    Ext.Ajax.request
                    (
                        {
                            url: baseURL +  "index.php/main/functionRWJ/UpdateGantiKelompok",   
                            params: {
                                        KDCustomer  : selectKdCustomer,
                                        KDNoSJP     : Ext.get('txtRWJNoSEP').getValue(),
                                        KDNoAskes   : Ext.get('txtRWJNoAskes').getValue(),
                                        KdPasien    : vKd_Pasien,
                                        TglMasuk    : vTanggal,
                                        KdUnit      : vKdUnit,
                                        UrutMasuk   : vUrutMasuk,
                                        KdDokter    : vKdDokter,
                                        alasan      : combo,
                            },
                            failure: function(o)
                            {
                                ShowPesanWarning_viDataPasien('Simpan kelompok pasien gagal', 'Gagal');
                            },  
                            success: function(o) 
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true) 
                                {
                                    panelActiveDataPasien = 1;
                                    FormLookUpsdetailTRKelompokPasien_rwj.close();
                                    RefreshDatahistoribayar(vKd_Pasien);
                                    Ext.getCmp('btnGantiKekompokPasien').disable(); 
                                    Ext.getCmp('btnGantiDokter').disable(); 
                                    Ext.getCmp('btnUnitPasien').disable();  
                                    Ext.getCmp('btnHpsdatapasien').disable();   
                                    ShowPesanInfo_viDataPasien("Mengganti kelompok pasien berhasil", "Success");

                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                    DataRefresh_viDataPasien(tmpkriteria);
                                }else 
                                {
                                    panelActiveDataPasien = 1;
                                    ShowPesanWarning_viDataPasien('Simpan kelompok pasien gagal', 'Gagal');
                                };
                            }
                        }
                    );
                }else{
                    ShowPesanWarning_viDataPasien('Harap memasukkan alasan perpindahan', 'Peringatan');
                }
            }
        });
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
	
};

function Datasave_GantiUnit_rwj(mBol) 
{	
	if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntry').dom.value  === 'Pilih Dokter...'))
	{
		ShowPesanWarning_viDataPasien('Dokter baru harap diisi', "Informasi");
	}else{	
			Ext.Ajax.request
			(
				{
					url: baseURL +  "index.php/main/functionRWJ/UpdateGantiUnit",	
					params: getParamKelompokpasien_RWJ(),
					failure: function(o)
					{
						ShowPesanWarning_viDataPasien('Simpan unit pasien gagal', 'Gagal');
						loadMask.hide();
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.totalrecords === 1) 
						{
                            panelActiveDataPasien = 1;
							vKdUnitDulu = "";
							FormLookUpsdetailTRKonsultasi_DataPasien.close();
							RefreshDatahistoribayar(vKd_Pasien);
							Ext.getCmp('btnGantiKekompokPasien').disable();	
							Ext.getCmp('btnGantiDokter').disable();	
                            Ext.getCmp('btnUnitPasien').disable();  
							Ext.getCmp('btnHpsdatapasien').disable();	
							ShowPesanInfo_viDataPasien("Mengganti unit pasien berhasil", "Success");
						}else 
						{
							ShowPesanWarning_viDataPasien('Simpan   pasien gagal', 'Gagal');
						};
					}
				}
			 )
	}
	
};

function Datasave_GantiDokter_rwj(mBol) 
{	
	//console.log(Ext.get('cboDokterRequestEntry').getValue());
	if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntry').dom.value  === 'Pilih Dokter...'))
	{
		ShowPesanWarning_viDataPasien('Dokter baru harap diisi', "Informasi");
	}else{
		Ext.Ajax.request
		(
			{
				url: baseURL +  "index.php/main/functionRWJ/UpdateGantiDokter",	
				params: getParamKelompokpasien_RWJ(),
				failure: function(o)
				{
					ShowPesanWarning_viDataPasien('Simpan dokter pasien gagal', 'Gagal');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
                        panelActiveDataPasien = 1;
						FormLookUpsdetailTRGantiDokter_rwj.close();
						RefreshDatahistoribayar(vKd_Pasien);
						Ext.getCmp('btnGantiKekompokPasien').disable();	
						Ext.getCmp('btnGantiDokter').disable();	
                        Ext.getCmp('btnUnitPasien').disable();  
						Ext.getCmp('btnHpsdatapasien').disable();	
						ShowPesanInfo_viDataPasien("Mengganti dokter pasien berhasil", "Success");
					}else 
					{
						ShowPesanWarning_viDataPasien('Simpan dokter pasien gagal', 'Gagal');
					};
				}
			}
		) 
	}
	
};

function cekDetailTransaksi() 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL +  "index.php/main/functionRWJ/cekDetailTransaksi",	
			params: getParamKelompokpasien_RWJ(),
			failure: function(o)
			{
				ShowPesanWarning_viDataPasien('Data gagal di periksa', 'Gagal');
				loadMask.hide();
			},	
			success: function(o) 
			{
				//console.log(o.responseText);
				if(o.responseText > 0){
					statusCekDetail = true;
				}else{
					statusCekDetail = false;
				}
			}
		}
	)
};

function ValidasiEntryUpdateKelompokPasien_RWJ(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('kelPasien_RWJ').getValue() == '') || (Ext.get('kelPasien_RWJ').dom.value  === undefined ))
	{
		if (Ext.get('kelPasien_RWJ').getValue() == '' && mBolHapus === true) 
		{
			ShowPesanWarning_viDataPasien(nmGetValidasiKosong('Kelompok Pasien'), modul);
			x = 0;
		}
	};
	return x;
};

function ValidasiEntryUpdateGantiDokter_RWJ(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ))
	{
		ShowPesanWarning_viDataPasien(nmGetValidasiKosong('Dokter baru harap diisi'), modul);
		x = 0;
	};
	return x;
};

function getParamKelompokpasien_RWJ() 
{
	var params;
	if(panelActiveDataPasien == 0){
		params = {
			KDCustomer 	: selectKdCustomer,
			KDNoSJP  	: Ext.get('txtRWJNoSEP').getValue(),
			KDNoAskes  	: Ext.get('txtRWJNoAskes').getValue(),
			KdPasien  	: vKd_Pasien,
			TglMasuk  	: vTanggal,
			KdUnit  	: vKdUnit,
			UrutMasuk  	: vUrutMasuk,
			KdDokter  	: vKdDokter,
		}
	}else if(panelActiveDataPasien == 1 || panelActiveDataPasien == 2){
		params = {
			KdPasien  	: vKd_Pasien,
			TglMasuk  	: vTanggal,
			KdUnit  	: vKdUnit,
			UrutMasuk  	: vUrutMasuk,
			KdDokter  	: vKdDokter,
			NoTransaksi	: vNoTransaksi,
			KdKasir  	: vKdKasir,
			KdUnitDulu 	: vKdUnitDulu,
		}
	}else if(panelActiveDataPasien == 'undefined'){
		params = { data : "null", }
	}else{
		params = { data : "null", }
	}
    return params
};

function mComboDokterGantiEntry(){ 
	/* var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry = new WebApp.DataStore({fields: Field}); */
    var cboDokterGantiEntry = new Ext.form.ComboBox({
	    id: 'cboDokterRequestEntry',
	    typeAhead: true,
	    triggerAction: 'all',
		name:'txtdokter',
	    lazyRender: true,
	    mode: 'local',
	    selectOnFocus:true,
        forceSelection: true,
	    emptyText:'Pilih Dokter...',
	    fieldLabel: 'Dokter Baru',
	    align: 'Right',
	    store: dsDokterRequestEntry,
	    valueField: 'KD_DOKTER',
	    displayField: 'NAMA',
		anchor:'100%',
	    listeners:{
		    'select': function(a,b,c){
				vKdDokter = b.data.KD_DOKTER;
            },
		}
    });
    return cboDokterGantiEntry;
};

function mComboPoliklinik(){
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
	ds_Poli_viDaftar.load({
        params	:{
            Skip	: 0,
            Take	: 1000,
            Sort	: 'NAMA_UNIT',
            Sortdir	: 'ASC',
            target	: 'ViewSetupUnitB',
            param	: " parent<>'0'"
        }
    });
    var cboPoliklinikRequestEntry = new Ext.form.ComboBox({
        id				: 'cboPoliklinikRequestEntry',
        typeAhead		: true,
        triggerAction	: 'all',
		width			: 170,
        lazyRender		: true,
        mode			: 'local',
        selectOnFocus	: true,
        forceSelection	: true,
        emptyText		: 'Pilih Poliklinik...',
        fieldLabel		: 'Poliklinik ',
        align			: 'Right',
        store			: ds_Poli_viDaftar,
        valueField		: 'KD_UNIT',
        displayField	: 'NAMA_UNIT',
        anchor			: '100%',
        listeners:{
            select: function(a, b, c){
				vKdUnitDulu 	= vKdUnit;
				vKdUnit  		= b.data.KD_UNIT;
				loaddatastoredokter();
            }
		}
    });
    return cboPoliklinikRequestEntry;
};

/*
    
    PERBARUAN FORMAT TANGGAL LAHIR 
    OLEH    : HADAD AL GOJALI 
    TANGGAL : 2017 - 01 - 21 

 */
function getParamBalikanHitungUmurDataPasien(tgl){
    var param;
    var tglPisah=tgl.split("/");
    var tglAsli;
    if (tglPisah[1]==="01"){
        tglAsli=tglPisah[0]+"/"+"Jan"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="02"){
        tglAsli=tglPisah[0]+"/"+"Feb"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="03"){
        tglAsli=tglPisah[0]+"/"+"Mar"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="04"){
        tglAsli=tglPisah[0]+"/"+"Apr"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="05"){
        tglAsli=tglPisah[0]+"/"+"May"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="06"){
        tglAsli=tglPisah[0]+"/"+"Jun"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="07"){
        tglAsli=tglPisah[0]+"/"+"Jul"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="08"){
        tglAsli=tglPisah[0]+"/"+"Aug"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="09"){
        tglAsli=tglPisah[0]+"/"+"Sep"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="10"){
        tglAsli=tglPisah[0]+"/"+"Oct"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="11"){
        tglAsli=tglPisah[0]+"/"+"Nov"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="12"){
        tglAsli=tglPisah[0]+"/"+"Dec"+"/"+tglPisah[2];
    }else{
        tglAsli=tgl;
    }
    Ext.getCmp('DateWindowPopup_TGL_LAHIR_viDataPasien').setValue(tgl);
}