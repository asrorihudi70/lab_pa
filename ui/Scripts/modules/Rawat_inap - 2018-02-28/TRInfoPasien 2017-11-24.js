var now = new Date();
var DefJamPerpindahan    = now.format('H:i:s');
var selectCountStatusByr_viHistoryPasienRWIKasir;
var selectCountHistoryPasienRWIKasir = '50';
var rowSelectedHistoryPasienRWIKasir;
var sebulanyanglalu = new Date().add(Date.DAY, -30);
var dsRekapitulasiTindakanHistory_RWI;

var PenataJasaHistoryPasienRWI = {};
var Current_HistoryPasienRWI = {
    data: Object,
    details: Array,
    row: 0
};
var griddetailtrdokter_HistoryPasienRWI;
var varkd_tarif_rwi;
PenataJasaHistoryPasienRWI.form = {};
PenataJasaHistoryPasienRWI.form.Class = {};
PenataJasaHistoryPasienRWI.form.DataStore = {};
PenataJasaHistoryPasienRWI.dsGridTindakan;
PenataJasaHistoryPasienRWI.form.ComboBox = {};
PenataJasaHistoryPasienRWI.form.Grid = {};
PenataJasaHistoryPasienRWI.func = {};
PenataJasaHistoryPasienRWI.form.DataStore.produk = new Ext.data.ArrayStore({id: 0, fields: ['kd_produk', 'deskripsi', 'harga', 'tglberlaku','"kd_tarif'], data: []});
PenataJasaHistoryPasienRWI.form.DataStore.trdokter = new Ext.data.ArrayStore({id: 0, fields: ['kd_dokter', 'nama'], data: []});
PenataJasaHistoryPasienRWI.form.DataStore.dokter_inap_int = new Ext.data.ArrayStore({id: 0, fields: ['kd_job', 'label'], data: []});
var TmpUrl      = "";
var TmpUrlResume= "";

CurrentPage.page = getPanelHistoryPasienRWI(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelHistoryPasienRWI(mod_id)
{
    var Field = ['KD_PASIEN','NAMA','TGL_MASUK','TGL_PULANG','KD_KASIR','NO_TRANSAKSI','DOKTER','LUNAS','KD_UNIT','UNIT','CO_STATUS', 'KD_UNIT_KAMAR'];
    dsTRHistoryPasienRWIKasirList = new WebApp.DataStore({fields: Field});
    RefreshDataHistoryPasienRWIKasir();
    grListTRHistoryPasienRWI = new Ext.grid.EditorGridPanel
            (
                    {
                        stripeRows: true,
                        store: dsTRHistoryPasienRWIKasirList,
                        columnLines: false,
                        autoScroll: true,
                        anchor: '100% 25%',
                        height: 200,
                        border: false,
                        sort: false,
                        sm: new Ext.grid.RowSelectionModel
                        (
                            {
                                singleSelect: true,
                                listeners:
                                        {
                                            rowselect: function (sm, row, rec)
                                            {
                                                rowSelectedHistoryPasienRWIKasir = dsTRHistoryPasienRWIKasirList.getAt(row);
                                            }
                                        }
                            }
                        ),
                        listeners:
                                {
                                    rowdblclick: function (sm, ridx, cidx)
                                    {

                                        rowSelectedHistoryPasienRWIKasir = dsTRHistoryPasienRWIKasirList.getAt(ridx);

                                        if (rowSelectedHistoryPasienRWIKasir != undefined)
                                        {

                                        }

                                    },
                                    rowclick: function (sm, ridx, cidx)
                                    {
										// console.log(rowSelectedHistoryPasienRWIKasir);
                                        var kodepasien      = rowSelectedHistoryPasienRWIKasir.data.KD_PASIEN; 
                                        var kodeunit        = rowSelectedHistoryPasienRWIKasir.data.KD_UNIT; 
                                        var tmpTanggalMasuk = rowSelectedHistoryPasienRWIKasir.data.TGL_MASUK; 
                                        var notransaksi     = rowSelectedHistoryPasienRWIKasir.data.NO_TRANSAKSI; 
                                        TmpUrl = baseURL+"index.php/rawat_inap/lap_bill_kwitansi/preview_billing/"+kodepasien+"/"+kodeunit+"/"+tmpTanggalMasuk+"/02/"+"/"+notransaksi+"/true/true";                           // 
                                        TmpUrlResume = baseURL+"index.php/rawat_inap/function_lap_RWI/laporan_resume/"+kodepasien+"/"+kodeunit+"/"+tmpTanggalMasuk+"/";  
                                        RefreshDatahistoribayar(rowSelectedHistoryPasienRWIKasir.data.NO_TRANSAKSI);
                                        RefreshDataDetail_HistoryPasienRWI(rowSelectedHistoryPasienRWIKasir.data.NO_TRANSAKSI);
                                        RefreshRekapitulasiTindakanHistory_RWI(rowSelectedHistoryPasienRWIKasir.data.NO_TRANSAKSI, '02');
                                    }


                                },
                        cm: new Ext.grid.ColumnModel
                                (
                                        [
                                            {
                                                id: 'colLUNAScoba',
                                                header: 'Lunas',
                                                dataIndex: 'LUNAS',
                                                sortable: true,
                                                width: 25,
                                                align: 'center',
                                                renderer: function (value, metaData, record, rowIndex, colIndex, store)
                                                {
                                                    switch (value)
                                                    {
                                                        case 't':
                                                            metaData.css = 'StatusHijau'; // 
                                                            break;
                                                        case 'f':
                                                            metaData.css = 'StatusMerah'; // rejected

                                                            break;
                                                    }
                                                    return '';
                                                }
                                            },
                                            {
                                                id: 'coltutuptr',
                                                header: 'Tutup transaksi',
                                                dataIndex: 'CO_STATUS',
                                                sortable: true,
                                                hidden: true,
                                                width: 25,
                                                align: 'center',
                                                renderer: function (value, metaData, record, rowIndex, colIndex, store)
                                                {
                                                    switch (value)
                                                    {
                                                        case 't':
                                                            metaData.css = 'StatusHijau'; // 
                                                            break;
                                                        case 'f':
                                                            metaData.css = 'StatusMerah'; // rejected

                                                            break;
                                                    }
                                                    return '';
                                                }
                                            },
                                            {
                                                id: 'colReqIdViewHistoryPasienRWI',
                                                header: 'No. Transaksi',
                                                dataIndex: 'NO_TRANSAKSI',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 40
                                            },
                                            {
                                                id: 'colTglHistoryPasienRWIViewHistoryPasienRWI',
                                                header: 'Tgl Masuk',
                                                dataIndex: 'TGL_MASUK',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 40,
                                                renderer: function (v, params, record)
                                                {
                                                    return ShowDate(record.data.TGL_MASUK);

                                                }
                                            },
                                            {
                                                id: 'colTglHistoryPasienRWITglPulang',
                                                header: 'Tgl Pulang',
                                                dataIndex: 'TGL_PULANG',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 40,
                                                renderer: function (v, params, record)
                                                {
                                                    return ShowDate(record.data.TGL_PULANG);

                                                }
                                            },
                                            {
                                                header: 'No. Medrec',
                                                width: 40,
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                dataIndex: 'KD_PASIEN',
                                                id: 'colHistoryPasienRWIerViewHistoryPasienRWI'
                                            },
                                            {
                                                header: 'Pasien',
                                                width: 190,
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                dataIndex: 'NAMA',
                                                id: 'colHistoryPasienRWIerViewHistoryPasienRWI'
                                            },
                                            {
                                                id: 'colDeptViewHistoryPasienRWI',
                                                header: 'Dokter',
                                                dataIndex: 'DOKTER',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 75
                                            },
                                            {
                                                id: 'colunitViewHistoryPasienRWI',
                                                header: 'Unit',
                                                dataIndex: 'UNIT',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 100
                                            }

                                        ]
                                        ), 
                        viewConfig: {forceFit: true},
                        tbar:[
                                /*{
                                    xtype: 'button',
                                    text: 'Cetak',
                                    iconCls: 'print',
                                    id: 'btnPrint_viHistoryPasien',
                                    handler: function ()
                                    {
                                        fnDlgRWICetakBilling(rowSelectedHistoryPasienRWIKasir.data);
                                    },
                                },*/
                                {
                                    xtype:'splitbutton',
                                    text: 'Cetak',
                                    iconCls: 'print',
                                    arrowAlign:'right',
                                    menu: [
                                            {
                                                // xtype: 'button',
                                                text: 'Kwitansi & Billing',
                                                iconCls: 'print',
                                                id: 'btnPrint_viHistoryPasien',
                                                handler: function ()
                                                {
                                                    fnDlgRWICetakBilling(rowSelectedHistoryPasienRWIKasir.data);
                                                },
                                            },{
                                                text: 'Resume Pasien',
                                                id: 'btnPrintResume_viHistoryRwi',
                                                iconCls: 'print',
                                                handler: function () {
                                                    // DISINI
                                                        
                                                    // function GetDTLTRGridPreviewBilling() {
                                                    // if (kodepasien.length > 0 && kodeunit.length > 0 && tmpTanggalMasuk.length > 0) {
                                                    console.log(TmpUrlResume);

                                                        var url = TmpUrlResume+false;
                                                        new Ext.Window({
                                                            title: 'Resume Pasien',
                                                            width: 1000,
                                                            height: 600,
                                                            constrain: true,
                                                            modal: true, 
                                                            html: "<iframe  style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
                                                            tbar: [
                                                                {
                                                                    text: 'Excel',
                                                                    id: 'btnPrintResumeExcel_KasirRWI',
                                                                    tooltip: nmLookup,
                                                                    iconCls: 'print',
                                                                    handler: function () {
                                                                        var form = document.createElement("form");
                                                                        form.setAttribute("method", "post");
                                                                        form.setAttribute("target", "_blank");
                                                                        form.setAttribute("action", TmpUrlResume+true+"/"+true+"/ResumePasien");
                                                                        var hiddenField = document.createElement("input");
                                                                        hiddenField.setAttribute("type", "hidden");
                                                                        hiddenField.setAttribute("name", "data");
                                                                        /*hiddenField.setAttribute("value", Ext.encode(params));*/
                                                                        form.appendChild(hiddenField);
                                                                        document.body.appendChild(form);
                                                                        form.submit();  
                                                                    }
                                                                },
                                                                {
                                                                    text: 'PDF',
                                                                    id: 'btnPrintResumePdf_KasirRWI',
                                                                    tooltip: nmLookup,
                                                                    iconCls: 'print',
                                                                    handler: function () {
                                                                        // console.log("cek");
                                                                        var form = document.createElement("form");
                                                                        form.setAttribute("method", "post");
                                                                        form.setAttribute("target", "_blank");
                                                                        form.setAttribute("action", TmpUrlResume+true+"/"+false+"/ResumePasien");
                                                                        var hiddenField = document.createElement("input");
                                                                        hiddenField.setAttribute("type", "hidden");
                                                                        hiddenField.setAttribute("name", "data");
                                                                        /*hiddenField.setAttribute("value", Ext.encode(params));*/
                                                                        form.appendChild(hiddenField);
                                                                        document.body.appendChild(form);
                                                                        form.submit();  
                                                                    }
                                                                },
                                                            ] 
                                                        }).show();
                                                    // };
                                                    // }
                                                    // KelompokPasienLookUp_penatajasarwi();
                                                }
                                        },
                                       
                                    ]
                                },
                                {
                                    xtype: 'button',
                                    text: 'Preview Billing',
                                    iconCls: 'Edit_Tr',
                                    id: 'btnLookUpPreviewBilling',
                                    handler: function ()
                                    {
                                        GetDTLTRGridPreviewBilling();
                                    },
                                    /*
                                    {
                                        id      :'btnLookUpPreviewBilling',
                                        text    : 'Preview Billing',
                                        iconCls : 'Edit_Tr',
                                        handler : function(){
                                          GetDTLTRGridPreviewBilling();
                                        }
                                    },*/
                                },
                        ]
                    }
            );

    var LegendViewCMRequest = new Ext.Panel
            (
                    {
                        id: 'LegendViewCMRequest',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        //height:32,
                        anchor: '100% 5%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        columnWidth: .033,
                                        layout: 'form',
                                        style: {'margin-top': '-1px'},
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        border: false,
                                        html: '<img src="' + baseURL + 'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                                    },
                                    {
                                        columnWidth: .08,
                                        layout: 'form',
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        style: {'margin-top': '1px'},
                                        border: false,
                                        html: " Lunas"
                                    },
                                    {
                                        columnWidth: .033,
                                        layout: 'form',
                                        style: {'margin-top': '-1px'},
                                        border: false,
                                        //height:35,
                                        anchor: '100% 8.0001%',
                                        //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                                        html: '<img src="' + baseURL + 'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                                    },
                                    {
                                        columnWidth: .1,
                                        layout: 'form',
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        style: {'margin-top': '1px'},
                                        border: false,
                                        html: " Belum Lunas"
                                    }
                                ]

                    }
            )
    var GDtabDetailrwi = new Ext.TabPanel
            (
                    {
                        id: 'GDtabDetailrwi',
                        region: 'center',
                        activeTab: 0,
                        anchor: '100% 53%',
                        border: false,
                        plain: true,
                        defaults:
                                {
                                    autoScroll: true
                                },
                        items: [
                            GetDTLTRGrid_panatajasarwiSecond(),
                            GetDTLTRHistoryGrid(), 
                            GetDTLTRGridRekapitulasiTindakanHistory_RWI(),
                                    //-------------- ## --------------
                        ],
                        listeners:
                                {
                                }
                    }

            );
    var FormDepanHistoryPasienRWI = new Ext.Panel
            (
                    {
                        id: mod_id,
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        title: 'History Pasien',
                        border: false,
                        shadhow: true,
                        iconCls: 'Request',
                        margins: '0 5 5 0',
                        items: [
                                {
                                    xtype: 'panel',
                                    plain: true,
                                    activeTab: 0,
                                    height: 150,
                                    defaults:
                                            {
                                                bodyStyle: 'padding:10px',
                                                autoScroll: true
                                            },
                                    items: [
                                        {
                                            layout: 'form',
                                            margins: '0 5 5 0',
                                            border: true,
                                            items:
                                                    [
                                                        {
                                                            xtype: 'textfield',
                                                            fieldLabel: ' No. Medrec' + ' ',
                                                            id: 'txtFilterNomedrec_InfoPasienrwi',
                                                            anchor: '70%',
                                                            onInit: function () { },
                                                            listeners:
                                                                    {
                                                                        'specialkey': function ()
                                                                        {
                                                                            var tmpNoMedrec = Ext.get('txtFilterNomedrec_InfoPasienrwi').getValue()
                                                                            if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 )
                                                                            {
                                                                                if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                                                                {

                                                                                    var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec_InfoPasienrwi').getValue())
                                                                                    Ext.getCmp('txtFilterNomedrec_InfoPasienrwi').setValue(tmpgetNoMedrec);
                                                                                    // var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    RefreshDataFilterHistoryPasienRWIKasir();

                                                                                } else
                                                                                {
                                                                                    if (tmpNoMedrec.length === 10)
                                                                                    {
                                                                                        RefreshDataFilterHistoryPasienRWIKasir();
                                                                                    } else
                                                                                        Ext.getCmp('txtFilterNomedrec_InfoPasienrwi').setValue('')
                                                                                }
                                                                            }
                                                                        }

                                                                    }
                                                        },
                                                        {
                                                            xtype: 'tbspacer',
                                                            height: 3
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            fieldLabel: ' Pasien' + ' ',
                                                            id: 'TxtFilterGridDataView_NAMA_viHistoryPasienRWIKasir',
                                                            anchor: '70%',
                                                            enableKeyEvents: true,
                                                            listeners:
                                                                    {
                                                                        'specialkey': function ()
                                                                        {
                                                                            if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)// || tmpNoMedrec.length === 10)
                                                                            {
                                                                            RefreshDataFilterHistoryPasienRWIKasir();
                                                                            }
                                                                        },
                                                                    }
                                                        },
                                                        {
                                                            xtype: 'tbspacer',
                                                            height: 3
                                                        },
                                                        getItemPanelcombofilter(),
                                                        getItemPaneltglmasukfilter(),
                                                        getItemPaneltglkeluarfilter(),
                                                        
                                                    ]}
                                    ]},
                                grListTRHistoryPasienRWI, 
                                GDtabDetailrwi, 
                                LegendViewCMRequest
                            ],
                    }
            );



    return FormDepanHistoryPasienRWI

}
;

function GetDTLTRHistoryGrid()
{
    var fldDetail = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'KD_UNIT'];
    dsTRDetailHistoryList = new WebApp.DataStore({fields: fldDetail})
    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
        (
            {
                title: 'History Bayar',
                stripeRows: true,
                store: dsTRDetailHistoryList,
                border: false,
                columnLines: true,
                frame: false,
                height: 400,
                anchor: '100%',
                autoScroll: true,
                sm: new Ext.grid.CellSelectionModel
                        (
                                {
                                    singleSelect: true
                                }
                        ),
                cm: TRHistoryColumModel(),
                viewConfig: {forceFit: true}
            }


        );



    return gridDTLTRHistory;
};

function TRHistoryColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'colKdUnitbayar_Infopasien',
                            header: 'KD Unit',
                            dataIndex: 'KD_UNIT',
                            //hidden: true,
                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Bayar',
                            dataIndex: 'URUT',

                        }

                        ,
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        }
                        ,
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }

                        }
                        ,
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true

                        }
                        ,
                        {
                            id: 'colStatHistory',
                            header: 'History',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true




                        },
                        {
                            id: 'colPetugasHistory',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME'
                        }
                    ]
                    )
};

/* 
    CREATE  : HADAD AL GOJALI
    TGL     : 05-05-2017
    KET     : REKAPITULASI TINDAKAN RWI

 */
 function GetDTLTRGridRekapitulasiTindakanHistory_RWI() {

    var fldDetail = ['kd_produk', 'kd_unit', 'deskripsi', 'qty', 'dokter', 'tgl_tindakan', 'qty', 'desc_req', 'tgl_berlaku', 
            'no_transaksi', 'urut', 'desc_status', 'tgl_transaksi', 'kd_tarif', 'harga', 'jumlah_dokter', 'tarif', 'no_faktur', 'folio'];
    dsRekapitulasiTindakanHistory_RWI = new WebApp.DataStore({fields: fldDetail});
    
    var GridRekapitulasiTindakanHistory_RWI = new Ext.grid.EditorGridPanel({
        title: 'Rekapitulasi Tindakan Yang Diberikan',
        id: 'GridRekapTindakanHistory_RWI',
        stripeRows: true,
        height: 130,
        store: dsRekapitulasiTindakanHistory_RWI,
        border: false,
        frame: false,
        columnLines: true,
        anchor: '100% 100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
                   
                }
            }
        }),
        cm: TRRekapitulasiTindakanHistory_RWIColumnModel(),
        viewConfig: {forceFit: true},
    });
    return GridRekapitulasiTindakanHistory_RWI;
};

function TRRekapitulasiTindakanHistory_RWIColumnModel() {
    return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            id: 'colKdUnitRekapitulasiHistory_RWI',
            header: 'Unit',
            dataIndex: 'kd_unit',
            width: 75,
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colReffRekapitulasiHistory_RWI',
            header: 'Reff',
            dataIndex: 'no_faktur',
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colKdProdukRekapitulasiHistory_RWI',
            header: 'Kode Produk',
            dataIndex: 'kd_produk',
            width: 100,
            menuDisabled: true
        },
        {
            id: 'colDeskripsiRekapitulasiHistory_RWI',
            header: 'Nama Produk',
            dataIndex: 'deskripsi',
            sortable: false,
            hidden: false,
            menuDisabled: true,
            width: 250
        },{
            id: 'colHARGARekapitulasiHistory_RWI',
            header: 'Jumlah',
            align: 'right',
            hidden: false,
            menuDisabled: true,
            dataIndex: 'harga',
            renderer: function (v, params, record)
            {
                return formatCurrency(record.data.harga);

            }
        },
        {
            id: 'colQTYRekapitulasiHistory_RWI',
            header: 'Qty',
            width: 50,
            align: 'left',
            menuDisabled: true,
            dataIndex: 'qty'
        },

    ]);
};


function RefreshRekapitulasiTindakanHistory_RWI(no_transaksi,kd_kasir){
    Ext.Ajax.request
    ({
        url: baseURL + "index.php/main/functionRWI/getRekapitulasiTindakan",
        params: {
            no_transaksi:no_transaksi,
            kd_kasir:kd_kasir,
            co_status:true,
        },
        failure: function (o)
        {
            ShowPesanErrorKasirrwi('Error membaca rekapitulasi tindakan. Hubungi Admin!', 'WARNING');
        },
        success: function (o)
        {
            var cst = Ext.decode(o.responseText);
            if (cst.success === true)
            {
                dsRekapitulasiTindakanHistory_RWI.removeAll();
                var recs=[],
                    recType=dsRekapitulasiTindakanHistory_RWI.recordType;
                    
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                dsRekapitulasiTindakanHistory_RWI.add(recs);
                //GridRekapitulasiTindakanKasirRWI.getView().refresh();
            } else{
                ShowPesanWarningKasirrwi('Gagal membaca rekapitulasi tindakan!', 'WARNING');
            }
        }
    })
}

function GetDTLTRGrid_panatajasarwiSecond() {

    var fldDetail = ['KD_PRODUK','KP_PRODUK', 'KD_UNIT', 'DESKRIPSI', 'QTY', 'DOKTER', 'TGL_TINDAKAN', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 
            'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'KD_TARIF', 'HARGA', 'JUMLAH_DOKTER', 'TARIF', 'NO_FAKTUR', 'FOLIO'];
    dsTRDetailHistoryPasienRWIList_panatajasarwi = new WebApp.DataStore({fields: fldDetail});
    PenataJasaHistoryPasienRWI.form.Grid.produk = new Ext.grid.EditorGridPanel({
        title: 'Tindakan Yang Diberikan',
        id: 'PjTransGrid2',
        stripeRows: true,
        height: 130,
        //plugins: [editor],
        store: dsTRDetailHistoryPasienRWIList_panatajasarwi,
        border: false,
        frame: false,
        columnLines: true,
        anchor: '100% 100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true
        }),
        cm: TRRawatInapColumModel2_InfoPasien(),
        viewConfig: {forceFit: true},
    });
    return PenataJasaHistoryPasienRWI.form.Grid.produk;
};

function TRRawatInapColumModel2_InfoPasien() {
    return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            header      : 'Tanggal Transaksi',
            dataIndex   : 'TGL_TRANSAKSI',
            width       : 130,
            menuDisabled: true
        },
        {
            id: 'colReffRWI',
            header: 'Reff',
            dataIndex: 'NO_FAKTUR',
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colKdUnitRWI',
            header: 'Unit',
            dataIndex: 'KD_UNIT',
            width: 75,
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colKdProduk_panatajasarwi2',
            header: 'Kode Produk',
            dataIndex: 'KP_PRODUK',
            width: 100,
            menuDisabled: true
        },
        {
            id: 'colDeskripsiRWI2',
            header: 'Nama Produk',
            dataIndex: 'DESKRIPSI',
            sortable: false,
            hidden: false,
            menuDisabled: true,
            width: 250
        },{
            id: 'colHARGA_penatajasa2',
            header: 'Harga',
            align: 'right',
            hidden: true,
            menuDisabled: true,
            dataIndex: 'HARGA',
            renderer: function (v, params, record)
            {
                return formatCurrency(record.data.HARGA);

            }
        },
        {
            id: 'coljmlhdokRWI2',
            header: 'Dok',
            width: 50,
            dataIndex: 'JUMLAH_DOKTER',
            hidden: false
        },
        {
            id: 'colQTY_penatajasa2',
            header: 'Qty',
            width: 50,
            align: 'right',
            menuDisabled: true,
            dataIndex: 'QTY'
        },
        {
            id: 'colTarifRWI',
            header: 'Tarif',
            dataIndex: 'TARIF',
            hidden: false,
        },
        {
            id: 'colFolioRWI',
            header: 'Folio',
            dataIndex: 'FOLIO',
            hidden: false,
        },
        {
            id: 'xbu',
            header: 'xbu',
            align: 'right',
            hidden: true,
            menuDisabled: true,
            renderer: function(v, params, record)
                {
                    var a='';
                    //console.log(record.data.NO_TRANSAKSI);
                    if (record.data.NO_TRANSAKSI !== '')
                    {
                        //getVisiteDokterDiawal(record.data.NO_TRANSAKSI, record.data.TGL_TRANSAKSI, record.data.KD_PRODUK,record.data.URUT);
                    }
                }

        }, 

    ]
            );
}
;

function getItemPanelcombofilter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            ComboKelasUnit_HistoryPasienRWI()
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                       [
                                            ComboKamar_HistoryPasienRWI(),
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getItemPaneltglmasukfilter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal Masuk',
                                                id: 'dtpTglAwalFilterHistoryPasienRWI',
                                                name: 'dtpTglAwalFilterHistoryPasienRWI',
                                                format: 'd/M/Y',
                                                value: sebulanyanglalu,
                                                width: '100px',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterHistoryPasienRWIKasir();
                                                                }
                                                            }
                                                        }
                                            },
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                       [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 's/d',
                                                id: 'dtpTglAkhirFilterHistoryPasienRWI',
                                                name: 'dtpTglAkhirFilterHistoryPasienRWI',
                                                format: 'd/M/Y',
                                                value: now,
                                                width: '100px',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterHistoryPasienRWIKasir();
                                                                }
                                                            }
                                                        }
                                            },
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getItemPaneltglkeluarfilter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal Keluar ',
                                                id: 'dtpTglkeluarAwalFilterHistoryPasienRWI',
                                                name: 'dtpTglkeluarAwalFilterHistoryPasienRWI',
                                                format: 'd/M/Y',
                                                value: sebulanyanglalu,
                                                width: '100px',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterHistoryPasienRWIKasir();
                                                                }
                                                            }
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                       [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 's/d',
                                                id: 'dtpTglkeluarAkhirFilterHistoryPasienRWI',
                                                name: 'dtpTglkeluarAkhirFilterHistoryPasienRWI',
                                                format: 'd/M/Y',
                                                value: now,
                                                width: '100px',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterHistoryPasienRWIKasir();
                                                                }
                                                            }
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
};

var CurrentNoKamar_HistoryPasienRWI;
function ComboKamar_HistoryPasienRWI(){
    dsKamar_HistoryPasienRWI = new WebApp.DataStore({fields: ['no_kamar','nama_kamar']})
    loadKamar_HistoryPasienRWI();
    cboKamar_HistoryPasienRWI = new Ext.form.ComboBox
    (
        {
            id: 'cboKamar_HistoryPasienRWI',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            fieldLabel: 'Kamar ',
            store: dsKamar_HistoryPasienRWI,
            valueField: 'no_kamar',
            displayField: 'nama_kamar',
            width:150,
            listeners:
            {
                'select': function(a, b, c)
                {
                    CurrentNoKamar_HistoryPasienRWI=b.data.no_kamar;
                    RefreshDataFilterHistoryPasienRWIKasir()
                }
            }
        }
    )
    return cboKamar_HistoryPasienRWI;
} 
var CurrentKdUnit_HistoryPasienRWI;
function ComboKelasUnit_HistoryPasienRWI(){
    dsKelasUnit_HistoryPasienRWI = new WebApp.DataStore({fields: ['kd_unit','nama_unit']})
    loadKelasUnit_HistoryPasienRWI();
    cboKelasUnit_HistoryPasienRWI = new Ext.form.ComboBox
    (
        {
            id: 'cboKelasUnit_HistoryPasienRWI',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            fieldLabel: 'Kelas ',
            store: dsKelasUnit_HistoryPasienRWI,
            valueField: 'kd_unit',
            displayField: 'nama_unit',
            width:150,
            listeners:
            {
                'select': function(a, b, c)
                {
                    Ext.getCmp('cboKamar_HistoryPasienRWI').setValue('');
                    loadKamar_HistoryPasienRWI(b.data.kd_unit);
                    CurrentKdUnit_HistoryPasienRWI=b.data.kd_unit;
                    RefreshDataFilterHistoryPasienRWIKasir()
                }
            }
        }
    )
    return cboKelasUnit_HistoryPasienRWI;
} 

function loadKelasUnit_HistoryPasienRWI(param){
    if (param==='' || param===undefined) {
        param={
            text: '0',
        };
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/rawat_inap/viewgridinfopasienrwi/getunitkelas",
        params: {
            text:param,
        },
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            dsKelasUnit_HistoryPasienRWI.removeAll();
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = dsKelasUnit_HistoryPasienRWI.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                dsKelasUnit_HistoryPasienRWI.add(recs);
                //console.log(o);
            }
        }
    });
};

function loadKamar_HistoryPasienRWI(param){
    if (param==='' || param===undefined) {
        param={
            text: '0',
        };
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/rawat_inap/viewgridinfopasienrwi/getkamar",
        params: {
            text:param,
        },
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            dsKamar_HistoryPasienRWI.removeAll();
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = dsKamar_HistoryPasienRWI.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                dsKamar_HistoryPasienRWI.add(recs);
                console.log(o);
            }
        }
    });
};


function RefreshDataHistoryPasienRWIKasir()
{
    var KataKunci = " t.CO_Status = 'T' AND t.Kd_Unit IN (SELECT Kd_Unit FROM Unit WHERE Parent in ('1001','1002','1003','1'))";
    //KataKunci += "AND K.TGL_MASUK >= '"+sebulanyanglalu.format("d/M/Y")+"' and K.TGL_MASUK <= '"+now.format("d/M/Y")+"' AND t.Tgl_CO >= '"+sebulanyanglalu.format("d/M/Y")+"' and t.Tgl_CO  <= '"+now.format("d/M/Y")+"' and ng.akhir = 't' limit 50";
    KataKunci += "AND  K.TGL_MASUK <= '"+now.format("d/M/Y")+"' AND t.Tgl_CO >= '"+sebulanyanglalu.format("d/M/Y")+"' and t.Tgl_CO  <= '"+now.format("d/M/Y")+"' and ng.akhir = 't' limit 50";
    dsTRHistoryPasienRWIKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountHistoryPasienRWIKasir,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'DESC',
                                    target: 'ViewHistoryPasienRWI',
                                    param: KataKunci
                                }
                    }
            );

    return dsTRHistoryPasienRWIKasirList;
}
;

function loadDataComboUserLapPerincianPasienRWIKasir(param){
    if (param==='' || param===undefined) {
        param='';
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRWI/kelaskamar",
        params: {kode:param},
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            cbokelaskamarrwi.store.removeAll();
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = dsget_data_kamarrwi.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                dsget_data_kamarrwi.add(recs);
                //console.log(o);
            }
        }
    });
}

function RefreshDatahistoribayar(no_transaksi)
{
    var strKriteriaHistoryPasienRWI = '';

    strKriteriaHistoryPasienRWI = 'no_transaksi= ~' + no_transaksi + '~';

    dsTRDetailHistoryList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewHistoryBayarRWI',
                                    param: strKriteriaHistoryPasienRWI
                                }
                    }
            );
    return dsTRDetailHistoryList;
}
;

function RefreshDataDetail_HistoryPasienRWI(no_transaksi)
{
    var strKriteriaRWI = '';
    strKriteriaRWI = "\"no_transaksi\" = ~" + no_transaksi + "~";

    dsTRDetailHistoryPasienRWIList_panatajasarwi.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailRWIGridBawah_REVISI',
                                    param: strKriteriaRWI
                                }
                    }
            );
    return dsTRDetailHistoryPasienRWIList_panatajasarwi;
}
;

function RefreshDataFilterHistoryPasienRWIKasir()
{

    var KataKunci = "t.CO_Status = 'T' ";
    if (Ext.get('txtFilterNomedrec_InfoPasienrwi').getValue() != '')
    {
         KataKunci += " and  t.kd_pasien =  '" + Ext.get('txtFilterNomedrec_InfoPasienrwi').getValue() + "'";

    };

    if (Ext.get('TxtFilterGridDataView_NAMA_viHistoryPasienRWIKasir').getValue() != '')
    {
        
            KataKunci += ' and  LOWER(p.nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viHistoryPasienRWIKasir').getValue() + '%~)';
    };

    KataKunci += "AND t.Kd_Unit IN (SELECT Kd_Unit FROM Unit WHERE Parent in ('1001','1002','1003')) ";
    //KataKunci += "AND K.TGL_MASUK >= '"+Ext.get('dtpTglAwalFilterHistoryPasienRWI').getValue()+"' and K.TGL_MASUK <= '"+Ext.get('dtpTglAkhirFilterHistoryPasienRWI').getValue()+"' ";
    KataKunci += "AND K.TGL_MASUK >= '"+Ext.get('dtpTglAwalFilterHistoryPasienRWI').getValue()+"' and K.TGL_MASUK <= '"+Ext.get('dtpTglAkhirFilterHistoryPasienRWI').getValue()+"' ";
    KataKunci += "AND t.Tgl_CO >= '"+Ext.get('dtpTglkeluarAwalFilterHistoryPasienRWI').getValue()+"' and t.Tgl_CO  <= '"+Ext.get('dtpTglkeluarAkhirFilterHistoryPasienRWI').getValue()+"' and ng.akhir = 't' order by k.TGL_MASUK desc";
    loadMask.show();


   if (Ext.get('cboKelasUnit_HistoryPasienRWI').getValue() != ''){
       
            KataKunci += ' and t.Kd_Unit =  ~'+ CurrentKdUnit_HistoryPasienRWI + '~';
    };

    if (Ext.get('cboKamar_HistoryPasienRWI').getValue() != ''){
       
            KataKunci += ' and q.no_kamar =  ~'+ CurrentNoKamar_HistoryPasienRWI + '~';
    };

    if (KataKunci != undefined || KataKunci != '')
    {
        dsTRHistoryPasienRWIKasirList.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCountHistoryPasienRWIKasir,
                                        Sort: 'tgl_transaksi',
                                        Sortdir: 'DESC',
                                        target: 'ViewHistoryPasienRWI',
                                        param: KataKunci
                                    },
                            callback: function(){
                                loadMask.hide();
                            }
                        }
                );
    } else
    {
        dsTRHistoryPasienRWIKasirList.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCountHistoryPasienRWIKasir,
                                        Sort: 'tgl_transaksi',
                                        Sortdir: 'DESC',
                                        target: 'ViewHistoryPasienRWI',
                                        param: KataKunci
                                    },
                            callback: function(){
                                loadMask.hide();
                            }
                        }
                );
    }
    ;

    return dsTRHistoryPasienRWIKasirList;
};

/*-------------------------------Penambahan Cetak Billing-------------------------------------------
Oleh    : HDHT
Tanggal : 16 Maret 2017
Tempat  : Bandung
---------------------------------------------------------------------------------------------------*/

function fnDlgRWICetakBilling(data)
{
    console.log(data);
    winRWICetakBilling = new Ext.Window
    (
        {
            id: 'winRWICetakBilling',
            title: 'Cetak Billing',
            closeAction: 'destroy',
            width:314,
            height: 300,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'cost',
            modal: true,
            items: [ItemDlgRWICetakBilling()],
            listeners:{
                 'show':function(win){
                        Ext.getCmp('dtpTanggalawalTransaksiCetakBillRWI').hide();
                        Ext.getCmp('dtpTanggalakhirTransaksiCetakBillRWI').hide();
                        Ext.getCmp('lbltanggalawal1').hide();
                        Ext.getCmp('lbltanggalawal2').hide();
                        Ext.getCmp('lbltanggalawal3').hide();
                        Ext.getCmp('cboJenisPrintHistoryPasienRWI').setValue('Billing');
                        // console.log(data);
                        Ext.getCmp('TxtCariNamaPembayaran').setValue(data.NAMA);
                        Ext.getCmp('cboSpesialisasicetakbillHistoryPasienRWI').setValue('Semua Spesialisasi');
                        tmpcetakkwitansi = 'true';
                  }

            },
            fbar:[
                {
                    id: 'btnOkLookupCetakBillingHistoryPasienRWI',
                    text: 'OK',
                    handler: function (sm, row, rec)
                    {      
                        // if (Ext.getCmp('cboJenisPrintHistoryPasienRWI').getValue() === 'Billing') {
                            var Spesialisasi = "";
                            var nama_Spesialisasi = "";
                                if (Ext.getCmp('cboSpesialisasicetakbillHistoryPasienRWI').getValue() === 'Semua Spesialisasi')
                                {
                                    Spesialisasi = 'NULL';
                                    nama_Spesialisasi = 'Semua Spesialisasi';
                                }else{
                                    Spesialisasi = kdspesialisasi;
                                    nama_Spesialisasi = namaSpesialisasi;
                                }
                                var params={
                                            Spesialisasi    : Spesialisasi, //split[2]
                                            dtlspesialisasi : nama_Spesialisasi,
                                            Tglpulang       : Ext.getCmp('dtpTanggalPulang').getValue(), //split[0]
                                            jenisprint      : Ext.getCmp('cboJenisPrintHistoryPasienRWI').getValue(), //split[1]
                                            folio           : tmpcgfolio,//split[5]
                                            notrans         : data.NO_TRANSAKSI, //split[3]
                                            KdKasir         : data.KD_KASIR,
                                            nama            : data.NAMA,
                                            kdpasien        : data.KD_PASIEN,
                                            reff            : Ext.getCmp('TxtCariReferensi').getValue(), //split[6]
                                            TglMasuk        : data.TGL_MASUK,
                                            kdunit          : data.KD_UNIT,
                                            kdunit_kamar    : data.KD_UNIT_KAMAR,
                                            costat          : 'true',
                                            cetakan         : Ext.getCmp('cboJenisPrintHistoryPasienRWI').getValue(),
                                            cetakkwitansi   : Ext.getCmp('cbCetakWitansi').getValue(),
                                            detailData      : Ext.getCmp('cbDetailData').getValue(),
                                };
                                Ext.Ajax.request({
                                //url: baseURL + "index.php/rawat_inap/lap_bill_kasir_rwi/Cetak",
                                url: baseURL + "index.php/rawat_inap/lap_bill_kwitansi/Cetak",
                                params: params,
                                success: function(o){
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true){
                                        ShowPesanInfoHistoryPasienRWI('Sedang di Print.','Information');
                                    }else if  (cst.success === false && cst.pesan===0){
                                        ShowPesanWarningHistoryPasienRWI('Data tidak berhasil di Print '  + cst.pesan,'Print Data');
                                    }else{
                                        ShowPesanErrorHistoryPasienRWI('Data tidak berhasil di Print '  + cst.pesan,'Print Data');
                                    }
                                }
                            });
                            // var form = document.createElement("form");
                            // form.setAttribute("method", "post");
                            // form.setAttribute("target", "_blank");
                            // form.setAttribute("action", baseURL + "index.php/rawat_inap/lap_bill_kasir_rwi/cetakkwitansi");
                            // var hiddenField = document.createElement("input");
                            // hiddenField.setAttribute("type", "hidden");
                            // hiddenField.setAttribute("name", "data");
                            // hiddenField.setAttribute("value", Ext.encode(params));
                            // form.appendChild(hiddenField);
                            // document.body.appendChild(form);
                            // form.submit();  
                            // frmDlgRWJPerLaporandetail.close();
                        // }else{
                        //     printbillRwi(data);
                        // }               
                        
                    }
                },
                {
                    id: 'btnCancelLookupCetakBillingHistoryPasienRWI',
                    text: 'Batal',
                    handler: function (sm, row, rec)
                    {
                        winRWICetakBilling.close();
                    }
                }
            
            ]

        }
    );
    winRWICetakBilling.show();
    
    // selectDataCetakBillingHistoryPasienRWI(data);
};
var tmpcgfolio = '';
var tmpcetakan = 'Billing';
var tmpcetakkwitansi = 'true';
function ItemDlgRWICetakBilling(){
 var items = new Ext.Panel({
        bodyStyle:'padding: 0px 0px 0px 0px',
        layout:'form',
        border:false,
        autoScroll: true,
        items:[
            {
                xtype : 'fieldset',
                title : '',
                layout: 'absolute',
                width : 300,
                height : 225,
                border: true,
                items:[
                    {
                        x: 0,
                        y: 0,
                        xtype: 'label',
                        text: 'Jenis '
                    }, {
                        x: 90,
                        y: 0,
                        xtype: 'label',
                        text: ' : '
                    },
                   mComboJenisPrintHistoryPasienRWI(),
                    {
                        x: 0,
                        y: 30,
                        xtype: 'label',
                        text: 'Tanggal Pulang '
                    }, {
                        x: 90,
                        y: 30,
                        xtype: 'label',
                        text: ' : '
                    },
                   {
                        x: 100,
                        y: 30,
                        xtype: 'datefield',
                        name: 'dtpTanggalPulang',
                        id: 'dtpTanggalPulang', 
                        format: 'd/M/Y',
                        value: now,
                        width: 122
                    },
                    {
                        x: 0,
                        y: 60,
                        xtype: 'label',
                        text: 'Pembayaran'
                    }, 
                    {
                        x: 90,
                        y: 60,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 60,
                        xtype: 'textfield',
                        name: 'TxtCariNamaPembayaran',
                        id: 'TxtCariNamaPembayaran',
                        width: 170,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                            // load_pengkajian(tmpkriteriaPengkajian);
                                        }
                                    }
                                }
                    },
                    {
                        x: 0,
                        y: 90,
                        xtype: 'label',
                        text: 'Referensi'
                    }, 
                    {
                        x: 90,
                        y: 90,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 90,
                        xtype: 'textfield',
                        name: 'TxtCariReferensi',
                        id: 'TxtCariReferensi',
                        width: 170,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                            // load_pengkajian(tmpkriteriaPengkajian);
                                        }
                                    }
                                }
                    },
                    {
                        x: 0,
                        y: 120,
                        xtype: 'label',
                        text: 'Cetak Kwitansi',
                        id:'lblchc1'
                    },
                    {
                        x: 90,
                        y: 120,
                        xtype: 'label',
                        text: ' : ',
                        id:'lblchc2'
                    },
                    {
                        x: 100,
                        y: 120,
                        xtype: 'checkboxgroup',
                        boxMaxWidth: 20,
                        id:'checkboxkwitansi',
                        itemCls: 'x-check-group-alt',
                        columns: 1,
                        items: [
                            {boxLabel: '', name: 'cbCetakWitansi', id: 'cbCetakWitansi', checked: true}
                        ]
                    },
                    {
                        x: 0,
                        y: 150,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 90,
                        y: 150,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboRWISpesialisasicetakbillHistoryPasienRWI(),
                    {
                        x: 0,
                        y: 180,
                        xtype: 'label',
                        text: 'Tanggal Transaksi ',
                        id:'lbltanggalawal1'
                    },
                    {
                        x: 90,
                        y: 180,
                        xtype: 'label',
                        text: ' : ',
                        id:'lbltanggalawal2'
                    },
                    {
                        x: 100,
                        y: 180,
                        xtype: 'datefield',
                        name: 'dtpTanggalawalTransaksiCetakBillRWI',
                        id: 'dtpTanggalawalTransaksiCetakBillRWI', 
                        
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 210,
                        y: 185,
                        xtype: 'label',
                        text: 'S/D',
                        id:'lbltanggalawal3'

                    },
                    {
                        x: 240,
                        y: 180,
                        xtype: 'datefield',
                        name: 'dtpTanggalakhirTransaksiCetakBillRWI',
                        id: 'dtpTanggalakhirTransaksiCetakBillRWI', 
                        
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 0,
                        y: 180,
                        xtype: 'label',
                        text: 'Detail data',
                        id:'checkDetail'
                    },
                    {
                        x: 90,
                        y: 180,
                        xtype: 'label',
                        text: ' : ',
                        id:'checkDetailLabel'
                    },
                    {
                        x: 100,
                        y: 180,
                        xtype: 'checkboxgroup',
                        boxMaxWidth: 20,
                        id:'checkboxDetailData',
                        itemCls: 'x-check-group-alt',
                        columns: 1,
                        items: [
                            {boxLabel: '', name: 'cbDetailData', id: 'cbDetailData', checked: false}
                        ],
                    },
                ]
            }
        ]
    })
    return items;
}

var selectSetJenisPrinter;
function mComboJenisPrintHistoryPasienRWI()
{
    var cboJenisPrintHistoryPasienRWI = new Ext.form.ComboBox
        (
            {
                x: 100,
                y: 0,
                id: 'cboJenisPrintHistoryPasienRWI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                selectOnFocus: true,
                forceSelection: true,
                emptyText: '',
                width: 170,
                store: new Ext.data.ArrayStore
                        (
                                {
                                    id: 0,
                                    fields:
                                            [
                                                'Id',
                                                'displayText'
                                            ],
                                    data: [[0, 'Billing'], [1, 'Kwitansi']]
                                }
                        ),
                valueField: 'Id',
                displayField: 'displayText',
                value: selectSetJenisPrinter,
                listeners:
                        {
                            'select': function (a, b, c)
                            {
                                if(b.data.displayText === 'Kwitansi')
                                {
                                    tmpcetakan = b.data.displayText;
                                    Ext.getCmp('dtpTanggalawalTransaksiCetakBillRWI').hide();
                                    Ext.getCmp('dtpTanggalakhirTransaksiCetakBillRWI').hide();
                                    Ext.getCmp('lbltanggalawal1').hide();
                                    Ext.getCmp('lbltanggalawal2').hide();
                                    Ext.getCmp('lbltanggalawal3').hide();
                                    Ext.getCmp('lblchc1').hide();
                                    Ext.getCmp('lblchc2').hide();
                                    Ext.getCmp('checkboxkwitansi').hide();
                                    tmpcetakkwitansi = 'false';

                                }else{
                                    Ext.getCmp('dtpTanggalawalTransaksiCetakBillRWI').hide();
                                    Ext.getCmp('dtpTanggalakhirTransaksiCetakBillRWI').hide();
                                    Ext.getCmp('lbltanggalawal1').hide();
                                    Ext.getCmp('lbltanggalawal2').hide();
                                    Ext.getCmp('lbltanggalawal3').hide();
                                    Ext.getCmp('lblchc1').show();
                                    Ext.getCmp('lblchc2').show();
                                    Ext.getCmp('checkboxkwitansi').show();
                                }
                            }
                        }
            }
        );
    return cboJenisPrintHistoryPasienRWI;
}
;

var ds_SpesialisasicetakbillHistoryPasienRWI;
var kdspesialisasi;
var namaSpesialisasi;
function mComboRWISpesialisasicetakbillHistoryPasienRWI()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];

    ds_SpesialisasicetakbillHistoryPasienRWI = new WebApp.DataStore({fields: Field});
    ds_SpesialisasicetakbillHistoryPasienRWI.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_spesial',
            Sortdir: 'ASC',
            target: 'ViewSetupSpesial',
            param: 'kd_spesial <> 0'
        }
    })

    var cboSpesialisasicetakbillHistoryPasienRWI = new Ext.form.ComboBox
    ({
        x:100,
        y:150,
        id: 'cboSpesialisasicetakbillHistoryPasienRWI',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        selectOnFocus: true,
        forceSelection: true,
        emptyText: 'Pilih Spesialisasi...',
        fieldLabel: 'Spesialisasi ',
        align: 'Right',
        store: ds_SpesialisasicetakbillHistoryPasienRWI,
        valueField: 'KD_SPESIAL',
        displayField: 'SPESIALISASI',
        width : 170,
        listeners:
        {
            'select': function (a, b, c)
            {
                kdspesialisasi = b.data.KD_SPESIAL;
                namaSpesialisasi = b.data.SPESIALISASI;
            },
            'render': function (c)
            {
                // c.getEl().on('keypress', function (e) {
                //     if (e.getKey() == 13 || e.getKey() == 9) //atau Ext.EventObject.ENTER
                //         Ext.getCmp('cboKelas_panatajasarwi').focus();
                // }, c);
            }
        }
    })

    return cboSpesialisasicetakbillHistoryPasienRWI;
}
;

function printbillRwi(data)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakbill_vikasirDaftarRWI(data),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningHistoryPasienRWI(cst.pesan, 'Cetak Kwitansi');
                            } else
                            {
                                ShowPesanErrorHistoryPasienRWI( cst.pesan, 'Cetak Kwitansi');
                            }
                        }
                    }
            );
};

function dataparamcetakbill_vikasirDaftarRWI(data)
{
    var paramscetakbill_vikasirDaftarRWI =
            {
                Table: 'DirectKwitansirwi',
                No_TRans         : data.NO_TRANSAKSI, //split[3]
                KdKasir         : data.KD_KASIR,
            };
    return paramscetakbill_vikasirDaftarRWI;
}
;


function ShowPesanWarningHistoryPasienRWI(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
};

function ShowPesanErrorHistoryPasienRWI(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 250
                    }
            );
};

function ShowPesanInfoHistoryPasienRWI(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
};

/*
    =============================================================================================
                                        PREVIEW BILLING
    =============================================================================================
*/

    function GetDTLTRGridPreviewBilling() {
        var url = TmpUrl;
        new Ext.Window({
            title: 'Preview Billing',
            width: 1000,
            height: 600,
            constrain: true,
            modal: true,
            html: "<iframe  style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
        }).show();
    };