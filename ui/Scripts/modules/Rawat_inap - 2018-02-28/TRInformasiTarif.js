// Data Source ExtJS

/**
*	Nama File 		: TRInformasiTarif.js
*	Menu 			: Pendaftaran
*	Model id 		: 030103
*	Keterangan 		: Untuk View Informasi Tarif
*	Di buat tanggal : 15 April 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var kode_tarif;
var kode_produk;
var kode_unit;
var tgl_berlaku;
var dsInfotarifList;
var rowSelected_viKasirIgd;
var selectCountInfoTarif=1;
var rowSelectedInfotarif;
var kd_prod;
var dsDataUnsur_viInformasiTarif;
var dataSource_viInformasiTarif;
var selectCount_viInformasiTarif=50;
var NamaForm_viInformasiTarif="Informasi Tarif";
var icons_viInformasiTarif="Gaji";
var addNew_viInformasiTarif;
var rowSelected_viInformasiTarif;
var rowSelected_viInformasiTarifList;
var setLookUps_viInformasiTarif;
var setLookUps_viInformasiTarifList;
var now_viInformasiTarif = new Date();

var BlnIsDetail;
var SELECTDATAPENILAIANPGW; // cek lagi

var CurrentData_viInformasiTarif =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInformasiTarif(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// Start Shortcut Key
// Kegunaan : Tombol Cepat untuk Keyboard
	Ext.getDoc().on('keypress', function(event, target) {
    if (event.altKey && !event.shiftKey)
	{
        event.stopEvent();
    /*if (Ext.EventObject.getKey() === 18 && !Ext.EventObject.getKey() === 16) {
        Ext.EventObject.stopEvent();*/

        switch(event.getKey()) {
        //switch(Ext.EventObject.getKey()) {

            case event.F1 :
            		dataaddnew_viInformasiTarif();
            	break;

            case event.F2 :
            		datasave_viInformasiTarif(false);
					datarefresh_viInformasiTarif();
				break;

            case event.F3 : // Alt + F3 untuk Edit
	                if (rowSelected_viInformasiTarif === undefined)
	                {
	                    setLookUp_viInformasiTarif();
	                }
	                else
	                {
	                    setLookUp_viInformasiTarif(rowSelected_viInformasiTarif.data);
	                }
                break;

            case event.F5 :
            		var x = datasave_viInformasiTarif(true);
							datarefresh_viInformasiTarif();
							if (x===undefined)
							{
								setLookUps_viInformasiTarif.close();
							}
				break;

			case event.F10 :
					alert("Delete")
				break;

            // other cases...
        }
        
    }
});
// End Shortcut Key

// Start Project Informasi Tarif

/**
*	Function : dataGrid_viInformasiTarif
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/
function dataGrid_viInformasiTarif(mod_id)
{

    // Field kiriman dari Project Net.
    var FieldMaster = ['KD_PRODUK', 'DESKRIPSI', 'TARIF','KD_UNIT', 'NAMA_UNIT','KD_TARIF','TGL_BERLAKU'];
	// Deklarasi penamaan yang akan digunakan pada Grid
    dataSource_viInformasiTarif = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
    datarefresh_viInformasiTarif();
    // Grid Informasi Tarif
    var grData_viInformasiTarif = new Ext.grid.EditorGridPanel
    (
    {
        xtype: 'editorgrid',
        title: 'Tarif',
        store: dataSource_viInformasiTarif,
        autoScroll: true,
        columnLines: true,
        border:false,
		height:450,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
        (
				{
					singleSelect: true,
					listeners:
					{
		
					}
				}
				),
				listeners:
				{
					// Function saat ada event double klik maka akan muncul form view
					rowclick: function (sm, ridx, cidx)
					{
						rowSelected_viKasirIgd = dataSource_viInformasiTarif.getAt(ridx);
                                       kode_produk = rowSelected_viKasirIgd.data.KD_PRODUK;
										kode_tarif = rowSelected_viKasirIgd.data.KD_TARIF;
										kode_unit =rowSelected_viKasirIgd.data.KD_UNIT;
										tgl_berlaku=rowSelected_viKasirIgd.data.TGL_BERLAKU;
						RefreshDataRwjTarif();
					}
				},
				/**
				*	Mengatur tampilan pada Grid Informasi Tarif
				*	Terdiri dari : Judul, Isi dan Event
				*	Isi pada Grid di dapat dari pemangilan dari Net.
				*	dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSource_viInformasiTarif
				*	didapat dari Function datarefresh_viInformasiTarif()
				*/
				colModel: new Ext.grid.ColumnModel
				(
					[
						new Ext.grid.RowNumberer(),
						{
							id: 'colKdTraif_viInformasiTarif',
							header: 'Kd. Tarif/Tindakan',
							dataIndex: 'KD_PRODUK',
							sortable: true,
							hidden:true,
							width: 100,
							filter :{
				
									}
												
						},
						//-------------- ## --------------  
						{
							id: 'colNama_viInformasiTarif',
							header: 'Nama',
							dataIndex: 'DESKRIPSI',
							sortable: true,
							width: 320,
							filter :{
				
									}
						
						},
						
						//-------------- ## --------------
						{
							id: 'colPoli_viInformasiTarif',
							header: 'Poli',
							dataIndex: 'NAMA_UNIT',
							sortable: true,
							width: 130,
							filter :{
				
									}
						},
						//-------------- ## --------------     
						{
							id: 'colTarif_viInformasiTarif',
							header: 'Tarif',
							dataIndex: 'TARIF',
							sortable: true,
							width: 100,
							align: 'right',
							filter :{
				
									}	
							// style:{'text-align':'right'},
							
						
						}
						//-------------- ## --------------
					]
				)
			
			}
    )

    var Field =['KD_COMPONENT','KD_TARIF','KD_PRODUK','COMPONENT','KD_UNIT','TGL_BERLAKU','TARIF'];
     dsInfotarifList = new WebApp.DataStore({ fields: Field });
     
	
     var grListSetTypeBayar = new Ext.grid.EditorGridPanel
     (
        {
            id: 'grListSetTypeBayar',
            stripeRows: true,
			    title: 'Tarif Komponen',
            store: dsInfotarifList,
            autoScroll: true,
            columnLines: true,
            border: false,
			height:450,
            anchor: '100% 100%',
            sm: new Ext.grid.RowSelectionModel
				(
					{
                    singleSelect: true,
                    listeners:
                        {
                           
                        }
					}
				),
            listeners:
			{
			rowdblclick: function (sm, ridx, cidx)
				{
					
				}
			},
           cm: new Ext.grid.ColumnModel
            (
				[
                    new Ext.grid.RowNumberer(),
							
							{
							//'','','','','','TGL_BERLAKU',''
							id: 'colSetDaftarByr',
						
							dataIndex: 'KD_COMPONENT',
							width: 100,
							hidden:true,
							sortable: true
							},
							{
							id: 'colSetDaftarByr',
							header: 'Komponen Tarif',
							dataIndex: 'COMPONENT',
							width: 200,
							sortable: true
							},
							{
							id: 'colSetDaftarByr',
							header: 'Tarif',
							align: 'right',
							dataIndex: 'TARIF',
							//hidden : true,
							width: 100,
							sortable: true,
							
							renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.TARIF);
							
							}	
							// style:{'text-align':'right'},
						
							},
							{
							id: 'colSetDaftarByr',
							
							hidden:true,
							dataIndex: 'KD_TARIF',
							
							hidden : true,
							width: 100,
							sortable: true
							},
							
							{
							id: 'colSetDaftarByr',
							
							dataIndex: 'KD_PRODUK',
							//hidden : true,
							width: 100,
							hidden:true,
							sortable: true
							},
							{
							id: 'colSetDaftarByr',
							
							dataIndex: 'KD_UNIT',
							hidden : true,
							width: 100,
							sortable: true
							},
		
				]
			//'','','','','','',''
            )
			
		    ,viewConfig: { forceFit: false}
		}
	); 
	// Kriteria filter pada Grid
	
	var FrmTabs_viInformasiTarif = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
		    title:  'Tarif Komponen',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: icons_viInformasiTarif,
		    items: 
			[
				{
					columnWidth: .49,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding:6px 3px 3px 6px',
					items:
					[grData_viInformasiTarif
						
					]
				},
				{
					columnWidth: .51,
					layout: 'form',
					bodyStyle: 'padding:6px 6px 3px 3px',
					border: false,
					anchor: '100% 100%',
					items:
					[grListSetTypeBayar
						
					]
				}
			]
		,
		tbar:
		[
			
			//-------------- ## --------------
			{ 
				xtype: 'tbtext', 
				text: 'Nama : ', 
				cls: 'left-label', 
				width: 60
			},
			{
				xtype: 'textfield',
				id: 'txtfilternama_viInformasiTarif',
				width: 200,
				enableKeyEvents: true,
				listeners:
				{ 
				
				
				'render': function(c) {
										c.getEl().on('keypress',
													function(e) 
													{
														if(e.getKey() == 13) //atau Ext.EventObject.ENTER
														{Ext.getCmp('txtfilpoli_viInformasiTarif').focus();};
														datarefresh_viInformasiTarifFilter();
                                                    }, 
											c);
                                      }
					
				}
			},
			//-------------- ## --------------	
			{ 
				xtype: 'tbtext', 
				text: 'Poli : ', 
				cls: 'left-label', 
				width: 60 
			},
			{
				xtype: 'textfield',
				id: 'txtfilpoli_viInformasiTarif',
				width: 200,
				listeners:
				{ 
				
				
				'render': function(c) {
										c.getEl().on('keypress',
													function(e) 
													{
														datarefresh_viInformasiTarifFilter();
                                                    }, 
											c);
                                      }
					
				}
			},
			
			//-------------- ## --------------
			{ 
				xtype: 'tbfill' 
			},
			//-------------- ## --------------
			{
				xtype: 'button',
				id: 'btnRefreshFilterDatatidakhadir_viInformasiTarif',
				iconCls: 'refresh',
				handler: function()
				{
					datarefresh_viInformasiTarifFilter();
				}
			}
			//-------------- ## --------------
		]
		
    }
    )
    // datarefresh_viInformasiTarif();
    return FrmTabs_viInformasiTarif;
}

/*
SELECT     KD_COMPONENT, KD_TARIF, KD_PRODUK, KD_UNIT, TGL_BERLAKU, TARIF, TARIF_PERCENT, KD_INDUK
FROM         TARIF_COMPONENT where KD_TARIF='TU'
*/

function RefreshDataSetTypeBayarFilter()
{
	var KataKunci='';

    if (Ext.get('txtSetDeskripsiFilter').getValue() != '')
    {
		if (KataKunci === '')
		{
			//KataKunci = ' where Customer like  ~%' + Ext.get('txtSetDeskripsiFilter').getValue() + '%~';
                        KataKunci = ' deskripsi like  ~%' + Ext.get('txtSetDeskripsiFilter').getValue() + '%~';
		}
		else
		{
			//KataKunci += ' and  deskripsi like  ~%' + Ext.get('txtSetDeskripsiFilter').getValue() + '%~';
                        KataKunci += ' and deskripsi like  ~%' + Ext.get('txtSetDeskripsiFilter').getValue() + '%~';
		};
	};

    if (KataKunci != undefined)
    {
		dsSetTypeBayarList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountInfoTarif,
					//Sort: 'Customer_ID',
                                        Sort: 'jenis_pay',
					Sortdir: 'ASC',
					target:'ViewSetupJenisBayar',
					param : KataKunci
				}
			}
		);
    }
	else
	{
		RefreshDataSetTypeBayar();
	};
};








function ShowPesanWarning_viInformasiTarif(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanError_viInformasiTarif(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

function ShowPesanInfo_viInformasiTarif(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}










function datainit_viInformasiTarif(rowdata)
{
    
}


function dataaddnew_viInformasiTarif()
{
   
}
///---------------------------------------------------------------------------------------///


/**
*	Function : gridDataForm_viInformasiTarif
*	
*	Sebuah fungsi untuk menampilkan isian grid pada edit Informasi Tarif
*	yang di pangil dari Function getFormItemEntry_viInformasiTarif
*/
//-------------------------------------------- Hapus baris -------------------------------------

//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------

//---------------------------- end Split row ------------------------------



function datarefresh_viInformasiTarif()
{
    dataSource_viInformasiTarif.load
    (
		{
			params:
			{
				Skip: 0,
				Take: selectCount_viInformasiTarif,
				Sort: '',
				Sortdir: 'ASC',
				target:'viInfoTarifRWI',
				param: 'kd_tarif=~TU~ and tgl_berlaku = (~2014-03-01~)'
			}
		}
    );
	
    return dataSource_viInformasiTarif;
    //alert("refersh")
}

function datarefresh_viInformasiTarifFilter()
{
	var KataKunci='';


	
	
	
	 if (Ext.get('txtfilternama_viInformasiTarif').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and  LOWER(deskripsi) like  LOWER( ~' + Ext.get('txtfilternama_viInformasiTarif').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(deskripsi) like  LOWER( ~' + Ext.get('txtfilternama_viInformasiTarif').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('txtfilpoli_viInformasiTarif').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and  LOWER(nama_unit) like  LOWER( ~' + Ext.get('txtfilpoli_viInformasiTarif').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama_unit) like  LOWER( ~' + Ext.get('txtfilpoli_viInformasiTarif').getValue() + '%~)';
		};

	};
	
	


    if (KataKunci != undefined )
    {
		dataSource_viInformasiTarif.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCount_viInformasiTarif,
                                        Sort: 'kd_tarif',
					Sortdir: 'ASC',
					target:'viInfoTarifRWI',
					param : 'kd_tarif=~TU~ and tgl_berlaku = (~2014-03-01~)'+ KataKunci
				}
			}
		);
    }
	else
	{
		datarefresh_viInformasiTarif();
	};
};

function RefreshDataRwjTarif()
{
	dsInfotarifList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountInfoTarif,
					Sort: '',
					Sortdir: 'ASC',
					target:'viInfoTarifRWI',
					param : 'kd_produk =~'+kode_produk+'~  and kd_tarif=~'+ kode_tarif +'~ and kd_unit= ~' + kode_unit+'~ and tgl_berlaku=~'+tgl_berlaku+'~'
				}
			}
		);

	rowSelectedInfotarif = undefined;
	return dsInfotarifList;
};
