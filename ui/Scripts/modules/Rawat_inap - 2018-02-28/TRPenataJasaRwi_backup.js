var CurrentKasirRwi =
{
    data: Object,
    details: Array,
    row: 0
};
var syssetting='igd_default_klas_produk';
var nilai_kd_tarif;
var tanggaltransaksitampung;
var mRecordRwi = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var CurrentDiagnosa =
{
    data: Object,
    details: Array,
    row: 0
};

var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create
(
    [
       {name: 'KASUS', mapping:'KASUS'},
       {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
       {name: 'PENYAKIT', mapping:'PENYAKIT'},
       {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);


var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwi='Belum Posting';
var dsTRKasirRwiList;
var dsTRDetailKasirRwiList;
var AddNewKasirRwi = true;
var selectCountKasirRwi = 50;
var now = new Date();
var rowSelectedKasirRwi;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRRwi;
var valueStatusCMRwiView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
//var FocusCtrlCMRwi;
var vkode_customer
CurrentPage.page = getPanelRwi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelRwi(mod_id) 
{
    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT','TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK','POSTING_TRANSAKSI','NAMA_KAMAR'];
    dsTRKasirRwiList = new WebApp.DataStore({ fields: Field });
    refeshkasirRwi();
    var grListTRRwi = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            store: dsTRKasirRwiList,
            anchor: '100% 91.9999%',
            columnLines: false,
            autoScroll:true,
            border: false,
			sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedKasirRwi = dsTRKasirRwiList.getAt(row);
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedKasirRwi = dsTRKasirRwiList.getAt(ridx);
                    if (rowSelectedKasirRwi != undefined)
                    {
                        RwiLookUp(rowSelectedKasirRwi.data);
                    }
                    else
                    {
                        RwiLookUp();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   
                    {
                        id: 'colReqIdViewRwi',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 80
                    },
                    {
                        id: 'colTglRwiViewRwi',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TANGGAL_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TANGGAL_TRANSAKSI);

                        }
                    },
					{
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colRwierViewRwi'
                    },
					{
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colRwierViewRwi'
                    },
                    {
                        id: 'colLocationViewRwi',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewRwi',
                        header: 'Dokter',
                        dataIndex: 'NAMA_DOKTER',
                        sortable: false,
							hideable:false,
							menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colImpactViewRwi',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					 {
                        id: 'colImpactViewRwi',
                        header: 'Kamar',
                        dataIndex: 'NAMA_KAMAR',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					{
                        header: 'Status Transaksi',
                        width: 150,
                        sortable: false,
						hideable:true,
						hidden:false,
						menuDisabled:true,
                        dataIndex: 'POSTING_TRANSAKSI',
                        id: 'txtposting',
						renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
						
                    },
                   
                ]
            ),
            tbar:
                [
                    {
                        id: 'btnEditRwi',
                        text: nmEditData,
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedKasirRwi != undefined)
                            {
                                    RwiLookUp(rowSelectedKasirRwi.data);
                            }
                            else
                            {
							ShowPesanWarningRwi('Pilih data data tabel  ','Edit data');
                            }
                        }
                    }
                ]
            }
	);
	
	

    var FormDepanRwi = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Penata Jasa ',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
			
			{
						       xtype:'panel',
							   plain:true,
							   activeTab: 0,
								height:180,
							   defaults:
							   {
								bodyStyle:'padding:10px',
								autoScroll: true
							   },
							   items:[
							{
							layout: 'form',
							margins: '0 5 5 0',
							border: true ,
							items:
							[
								 {
								xtype: 'textfield',
								fieldLabel: ' No. Medrec' + ' ',
								id: 'txtFilterNomedrecRwi',
								anchor : '70%',
								onInit: function() { },
								listeners:
									{
										'specialkey' : function()
										{
											var tmpNoMedrec = Ext.get('txtFilterNomedrecRwi').getValue()
											if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
											{
												if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
													{
													   
														 var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrecRwi').getValue())
														 Ext.getCmp('txtFilterNomedrecRwi').setValue(tmpgetNoMedrec);
														var tmpkriteria = getCriteriaFilter_viDaftar();
														   RefreshDataFilterKasirRwi();
												  
													}
													else
														{
															if (tmpNoMedrec.length === 10)
																{
																   RefreshDataFilterKasirRwi();
																}
																else
																Ext.getCmp('txtFilterNomedrecRwi').setValue('')
														}
											}
										}

									}
							},
				
							{	 
							xtype: 'tbspacer',
							height: 3
							},	
								{
									xtype: 'textfield',
									fieldLabel: ' Pasien' + ' ',
									id: 'TxtFilterGridDataView_NAMA_viKasirRwi',
									anchor :'70%',
									 enableKeyEvents: true,
									listeners:
									{ 
								   		'specialkey' : function()
										{
											if (Ext.EventObject.getKey() === 13)
											{
													    RefreshDataFilterKasirRwi();
											}
										}
									}
								},
								{	 
								xtype: 'tbspacer',
								height: 3
								},	
						
						
						
						{
							xtype: 'textfield',
							fieldLabel: ' Dokter' + ' ',
							id: 'TxtFilterGridDataView_DOKTER_viKasirRwi',
							anchor:'70%',
							enableKeyEvents: true,
							listeners:
							{ 
								'specialkey' : function()
										{
											if (Ext.EventObject.getKey() === 13)
											{
													    RefreshDataFilterKasirRwi();
											}
										}
							}
						},getItemcombo_filterRwi(),getItemPaneltgl_filterRwi()
						]}
						]},
			
			grListTRRwi],
           /* tbar:
            [
              '' , '' , '' ,'' ,'' , '' , '' ,'' ,' ' , '' , '' ,'' ,' ' , '' , '' ,'' ,' ' , '' , '' ,'-','No. Medrec' + ' : ', ' ',
                {
                    xtype: 'textfield',
                   // fieldLabel: 'No. Medrec' + ' ',
                    id: 'txtFilterNomedrecRwi',
                    width: 100,
                    onInit: function() { },
					listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrecRwi').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
										   
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrecRwi').getValue())
                                             Ext.getCmp('txtFilterNomedrecRwi').setValue(tmpgetNoMedrec);
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
											 RefreshDataFilterKasirRwi();
                                      
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                       // tmpkriteria = getCriteriaFilter_viDaftar();
                                                     RefreshDataFilterKasirRwi();
                                                    }
                                                    else
                                                    Ext.getCmp('txtFilterNomedrecRwi').setValue('')
                                            }
                                }
                            }

                        }
                },
				'-','Nama Pasien ', '-',
					
					{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viKasirRwi',
							//emptyText: 'Nama',
							width: 120,
							//height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									        RefreshDataFilterKasirRwi();

									} 						
								}
							}
						}, ' ', ' ',  ' ', ' ', '-','Poli tujuan', '-',
						
						mComboUnit_viKasirRwi(),
						'-','Doktor', '-',
				{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_DOKTER_viKasirRwi',
							//emptyText: 'Alamat',
							width: 120,
							//height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
									RefreshDataFilterKasirRwi();

						
									} 						
								}
							}
						}
						,'' ,' ' , '' , '' ,'' ,' ' , '' , '' , ' ', '' , '' , ' ','-', '' ,' ', ' ','' , ' ','' , ' ','' , ' ','' ,
							
					{
                    text: nmRefresh,
                    tooltip: nmRefresh,
                    iconCls: 'refresh',
                    anchor: '25%',
                    handler: function(sm, row, rec)
                    {
                       // rowSelectedKasirRwi=undefined;
                        RefreshDataFilterKasirRwi();
                    }
					},'', '', '' + ' ', ' ','' + ' ','' + ' ','' + ' ','' + ' ', ' ','' + '-',
            ],*/
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	
   return FormDepanRwi

};


function getItemPaneltgl_filterRwi() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTglAwalFilterRwi',
					    name: 'dtpTglAwalFilterRwi',
						//readOnly : true,
					    value: now,
					    anchor: '99%',
						format: 'd/M/Y',
						altFormats: 'dmy',
					    listeners:
						{
						
						 'specialkey' : function()
                            {
                               // var tmpNoMedrec = Ext.get('txtFilterNomedrecRwi').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 )
                                {
								
								  RefreshDataFilterKasirRwi();
								}
						}
						},
						//renderer: Ext.util.Format.dateRenderer('d/M/Y')
					}
				]
			},
			 {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[ 
		
					{
					    xtype: 'datefield',
					   // fieldLabel: 'Tanggal ',
					    id: 'dtpTglAkhirFilterRwi',
					    name: 'dtpTglAkhirFilterRwi',
					    format: 'd/M/Y',
						
						//readOnly : true,
					    value: now,
					    anchor: '99%',
						   listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrecRwi').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								  RefreshDataFilterKasirRwi();
								}
						}
						}
					}
				]
			}
		]
	}
    return items;
};

function getItemcombo_filterRwi() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
				mComboStatusBayar_viKasirRwi()
				]
			},
			// {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:20,
			    items:
				[ 
		
					mComboUnit_viKasirRwi()
				]
			}
		]
	}
    return items;
};

function mComboStatusBayar_viKasirRwi()
{
  var cboStatus_viKasirRwi = new Ext.form.ComboBox
	(
		{
			id:'cboStatus_viKasirRwi',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
		   anchor : '96.5%',
		   // width: 100,
			emptyText:'',
			fieldLabel: 'JENIS',
			//width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusByr_viKasirRwi,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusByr_viKasirRwi=b.data.displayText ;
					//RefreshDataSetDokter();
					RefreshDataFilterKasirRwi();

				}
			}
		}
	);
	return cboStatus_viKasirRwi;
};


function mComboUnit_viKasirRwi() 
{
	var Field = ['NO_KAMAR','NAMA_KAMAR'];

    dsunit_viKasirRwi = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirRwi.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			  
                            Sort: 'no_kamar',
			    Sortdir: 'ASC',
			    target: 'ComboUnitSps',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboUNIT_viKasirRwi = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirRwi',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' ',
		    align: 'Right',
			//  width: 100,
		    anchor:'100%',
		    store: dsunit_viKasirRwi,
		    valueField: 'NAMA_KAMAR',
		    displayField: 'NAMA_KAMAR',
			value:'all',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{					       
						   RefreshDataFilterKasirRwi();

			        //selectStatusCMRwiView = b.data.DEPT_ID;
					//
			    }

			}
		}
	);
	
    return cboUNIT_viKasirRwi;
};



function RwiLookUp(rowdata) 
{
    var lebar = 600;
    FormLookUpsdetailTRRwi = new Ext.Window
    (
        {
            id: 'gridRwi',
            title: 'Penata Jasa Rawat Inap',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRRwi(lebar,rowdata),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRRwi.show();
    if (rowdata == undefined) 
	{
        RwiAddNew();
    }
    else 
	{
        TRRwiInit(rowdata)
    }

};


function mComboPoliklinik(lebar)
{

    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: 'kd_bagian=2 and type_unit=false and kd_unit not in (~'+Ext.get('txtKdUnitRWI').dom.value +'~)'
            }
        }
    )

    var cboPoliklinikRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboPoliklinikRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
			width: 170,
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poliklinik...',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   loaddatastoredokter(b.data.KD_UNIT)
								   selectKlinikPoli=b.data.KD_UNIT
                                }
                   


		}
        }
    )

    return cboPoliklinikRequestEntry;
}

function loaddatastoredokter(kd_unit)
{
          dsDokterRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'nama',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokter',
			    param: 'where dk.kd_unit=~'+ kd_unit+ '~'
			}
                    }
                )
}

function mComboDokterRequestEntry()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});


    var cboDokterRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    labelWidth:80,
			fieldLabel: 'Dokter      ',
		    align: 'Right',
                     
//		    anchor:'60%',
		    store: dsDokterRequestEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
            anchor: '95%',
		    listeners:
			{
			    'select': function(a,b,c)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
									selectDokter = b.data.KD_DOKTER;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboDokterRequestEntry;
};

function KonsultasiLookUp(rowdata) 
{
    var lebar = 350;
    FormLookUpsdetailTRKonsultasi = new Ext.Window
    (
        {
            id: 'gridKonsultasi',
            title: 'Konsultasi',
            closeAction: 'destroy',
            width: lebar,
            height: 130,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
           items: {
						layout: 'hBox',
					//	width:222,
						border: false,
						bodyStyle: 'padding:5px 0px 5px 5px',
						defaults: { margins: '3 3 3 3' },
						anchor: '90%',
					
								items:
								[
								
										
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:80,
												border: false,
												items:
												[
													{
														xtype: 'textfield',
														//fieldLabel:'Dokter  ',
														name: 'txtKdUnitRWIKonsultasi',
														id: 'txtKdUnitRWIKonsultasi',
														//emptyText:nmNomorOtomatis,
														hidden :true,
														readOnly:true,
														anchor: '80%'
													},
													
													{
														xtype: 'textfield',
														fieldLabel: 'Poli Asal ',
														//hideLabel:true,
														name: 'txtnamaunitKonsultasi',
														id: 'txtnamaunitKonsultasi',
														readOnly:true,
														anchor: '95%',
														listeners: 
														{ 
															
														}
													}, 
														mComboPoliklinik(lebar),
													
														mComboDokterRequestEntry()
														
												]
											},
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:70,
												border: false,
												items:
												[
														{
																	xtype:'button',
																	text:'Kosultasi',
																	width:70,
																	style:{'margin-left':'0px','margin-top':'0px'},
																	hideLabel:true,
																	id: 'btnOkkonsultasi',
																	handler:function()
																	{
																	Datasave_Konsultasi(false);
																	FormLookUpsdetailTRKonsultasi.close()	
																	}
														},
														{	 
															xtype: 'tbspacer',
															
															height: 3
														},				
														{
															xtype:'button',
															text:'Batal' ,
															width:70,
															hideLabel:true,
															id: 'btnCancelkonsultasi',
															handler:function() 
															{
																FormLookUpsdetailTRKonsultasi.close()
															}
														}
											 ]
											
											
											}
											
											
										
									,
									
								]
			},
            listeners:
            {
              
            }
        }
    );

    FormLookUpsdetailTRKonsultasi.show();
  
      KonsultasiAddNew();
	

};
function KonsultasiAddNew() 
{
    AddNewKasirKonsultasi = true;
	Ext.get('txtKdUnitRWIKonsultasi').dom.value =Ext.get('txtKdUnitRWI').dom.value   ;
	Ext.get('txtnamaunitKonsultasi').dom.value=Ext.get('txtNamaUnit').dom.value;
	

};

function getFormEntryTRRwi(lebar,data) 
{
    var pnlTRRwi = new Ext.FormPanel
    (
        {
            id: 'PanelTRRwi',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:189,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputRwi(lebar)],
           tbar:
            [
              
		              
		                
					    	//-------------- ## --------------
        					{
						        text: 'Pindah Kamar',
						        id:'btnLookUpKonsultasi_viKasirIgd',
						        iconCls: 'Konsultasi',
						        handler: function()
								{
								setLookUp_viPindahKamarRWI()
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: ' Ganti Dokter',
						        id:'btnLookUpGantiDokter_viKasirIgd',
						        iconCls: 'gantidok',
						        handler: function(){
						           // FormSetLookupGantiDokter_viKasirIgd('1','2');
								   GantiDokterLookUp();
						        },
					    	},
							{
						        text: 'Ganti Kelompok Pasien',
						        id:'btngantipasien',
						        iconCls: 'gantipasien',
						        handler: function(){
						           // FormSetLookupGantiDokter_viKasirIgd('1','2');
								   KelompokPasienLookUp();
						        },
					    	},
							{
						        text: 'Posting Ke Kasir',
						        id:'btnposting',
						        iconCls: 'gantidok',
						        handler: function(){
						           // FormSetLookupGantiDokter_viKasirIgd('1','2');
								  setpostingtransaksi(data.NO_TRANSAKSI);
						        },
							}
					    	
		               
		          
               
            ]
        }
    );
var x;
 var GDtabDetailRwi = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailRwi',
        region: 'center',
        activeTab: 0,
		height:250,
        anchor: '100% 100%',
        border:false,
        plain: true,
        defaults:
                {
                autoScroll: true
				},
                items: [GetDTLTRRwiGrid(),GetDTLTRDiagnosaGrid()
				
		                //-------------- ## --------------
					],
        tbar:
                [			{
                       
								id:'btnTambahBrsRWI',
                                text: 'Tambah baris',
                                tooltip: nmTambahBaris,
                                iconCls: 'AddRow',
                                handler: function()
                                {
                                        TambahBarisRwi();
                                }},
						{
							text: 'Tambah Produk',
							id: 'btnLookupRwi',
							tooltip: nmLookup,
							iconCls: 'find',
							hidden:true,
							handler: function()
							{
						  
									var p = RecordBaruRWJ();
									var str='';
									if (Ext.get('txtKdUnitRWI').dom.value  != undefined && Ext.get('txtKdUnitRWI').dom.value  != '')
									{
											//str = ' where kd_dokter =~' + Ext.get('txtKdDokter').dom.value  + '~';
											//str = "\"kd_dokter\" = ~" + Ext.get('txtKdDokter').dom.value  + "~";
											str = Ext.get('txtKdUnitRWI').dom.value;
									};
									FormLookupKasirRWJ(str, dsTRDetailKasirRwiList, p, true, '', true);
							}
						},
                       
							 {
								text: 'Simpan',
								id: 'btnSimpanRwi',
								tooltip: nmSimpan,
								iconCls: 'save',
								handler: function()
									{
									
									/* if (dsTRDetailKasirRwiList.getCount() > 0 )
													{
															if (cellSelecteddeskripsi != undefined)
															{
																	if(CurrentKasirRwi != undefined)
																	{
																			 Dataupdate_KasirRwi(false);
																			RefreshDataFilterKasirRwi();
																	}
															}
															else
															{
																	ShowPesanWarningRwi('Pilih record lalu ubah qty  ','Ubah data');
															}
													}*/
													Datasave_KasirRwi(false);
								   
								}
							},   {
                                id:'btnHpsBrsRwi',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
                                handler: function()
                                {
                                        if (dsTRDetailKasirRwiList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentKasirRwi != undefined)
                                                        {
                                                                HapusBarisRwi();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningRwi('Pilih record ','Hapus data');
                                                }
                                        }
                                }
                        },
						{
							text: 'Tambah Diagnosa',
							id: 'btnLookupDiagnosa',
							tooltip: nmLookup,
							iconCls: 'find',
							handler: function()
							{
								
									var p = RecordBaruDiagnosa();
									var str='';
									FormLookupDiagnosa(str, dsTRDetailDiagnosaList, p, true, '', true);
						}
						},
                        
							 {
							text: 'Simpan',
							id: 'btnSimpanDiagnosa',
							tooltip: nmSimpan,
							iconCls: 'save',
							handler: function()
								{
								
								 if (dsTRDetailDiagnosaList.getCount() > 0 )
												{
														if (cellSelecteddeskripsi != undefined)
														{
																if(CurrentDiagnosa != undefined)
																{
																		 
																		Datasave_Diagnosa(false);
																		//FormLookUpsdetailTRDiagnosa.close();
																		//RefreshDataFilterDiagnosa();
																		
																}
														}
														else
														{
																ShowPesanWarningDiagnosa('Pilih record lalu ubah STAT_DIAG  ','Ubah data');
														}
												}
                       
								}
							},
							{
                                id:'btnHpsBrsDiagnosa',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
                                handler: function()
                                {
                                        if (dsTRDetailDiagnosaList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentDiagnosa != undefined)
                                                        {
                                                                HapusBarisDiagnosa();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningDiagnosa('Pilih record ','Hapus data');
                                                }
                                        }
                                }
							}
                ],
		listeners:
            {
               'tabchange' : function (panel, tab) {
				//GDtabDetailRwi.setActiveTab(1);
				if (x ==1)
				{
				 Ext.getCmp('btnLookupRwi').hide()
                 Ext.getCmp('btnSimpanRwi').hide()
				 Ext.getCmp('btnHpsBrsRwi').hide()
				 Ext.getCmp('btnTambahBrsRWI').hide()
				 
                 Ext.getCmp('btnHpsBrsDiagnosa').show()
				  Ext.getCmp('btnSimpanDiagnosa').show()
				  Ext.getCmp('btnLookupDiagnosa').show()
				//alert('a');
					x=2;
					return x;
				}else 
				{ 	
				
				 Ext.getCmp('btnTambahBrsRWI').show()
				 Ext.getCmp('btnLookupRwi').hide()
                 Ext.getCmp('btnSimpanRwi').show()
				 Ext.getCmp('btnHpsBrsRwi').show()
                 Ext.getCmp('btnHpsBrsDiagnosa').hide()
				  Ext.getCmp('btnSimpanDiagnosa').hide()
				  Ext.getCmp('btnLookupDiagnosa').hide()
				 ,
				x=1;	
				return x;
				}
				
			}
		//bbar :			/**/
        }
		}
		
    );
	
   
   
   var pnlTRRwi2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRRwi2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:260,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailRwi
			
			]
        }
    );

	
   
   
    var FormDepanRwi = new Ext.Panel
	(
		{
		    id: 'FormDepanRwi',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRRwi,pnlTRRwi2	
				

			]

		}
	);
	
	if( data.POSTING_TRANSAKSI == 't')
	{
	setdisablebutton();
	}
	else
	{
	setenablebutton();	
	//Ext.getCmp('PanelTRRwi').enable();	
	}


    return FormDepanRwi
};

function RecordBaruDiagnosa()
{
	var p = new mRecordDiagnosa
	(
		{
			'KASUS':'',
			'KD_PENYAKIT':'',
		    'PENYAKIT':'', 
		  //  'KD_TARIF':'', 
		   // 'HARGA':'',
		    'STAT_DIAG':'',
		    'TGL_TRANSAKSI':Ext.get('dtpTanggalDetransaksi').dom.value, 
		    //'DESC_REQ':'',
		   // 'KD_TARIF':'',
		    'URUT_MASUK':''
		}
	);
	
	return p;
};

function HapusBarisDiagnosa()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.PENYAKIT != '' && cellSelecteddeskripsi.data.KD_PENYAKIT != '')
        {
            Ext.Msg.show
            (
                {
                   title:nmHapusBaris,
                   msg: 'Anda yakin akan menghapus produk' + ' : ' + cellSelecteddeskripsi.data.PENYAKIT ,
                   buttons: Ext.MessageBox.YESNO,
                   fn: function (btn)
                   {
                       if (btn =='yes')
                        {
                            if(dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.URUT_MASUK === '')
                            {
                                dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                            }
                            else
                            {
                                
                                            if (btn =='yes')
                                            {
                                               DataDeleteDiagnosaDetail();
                                            };
                                
                            };
                        };
                   },
                   icon: Ext.MessageBox.QUESTION
                }
            );
        }
        else
        {
            dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
        };
    }
};

function DataDeleteDiagnosaDetail()
{
    Ext.Ajax.request
    (
        {
            
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteDiagnosaDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                    cellSelecteddeskripsi=undefined;
                  RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWI').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                    AddNewDiagnosa = false;
                }
           
                else
                {
                    ShowPesanWarningDiagnosa(nmPesanHapusError,nmHeaderHapusData);
					RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWI').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                };
            }
        }
    )
};

function getParamDataDeleteDiagnosaDetail()
{
    var params =
    {
        Table: 'ViewDiagnosa',
        KdPasien: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.get('txtKdUnitRWI').getValue(),
		TglMasuk:CurrentDiagnosa.data.data.TGL_MASUK,
		KdPenyakit : CurrentDiagnosa.data.data.KD_PENYAKIT,
		UrutMasuk:CurrentDiagnosa.data.data.URUT_MASUK,
		Urut:CurrentDiagnosa.data.data.URUT,
    };
	
    return params
};



function getParamDetailTransaksiDiagnosa2() 
{
    var params =
	{
		Table:'ViewTrDiagnosa',
		
		
		KdPasien: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.get('txtKdUnitRWI').getValue(),
		UrutMasuk:Ext.get('txtKdUrutMasuk').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		List:getArrDetailTrDiagnosa(),
		JmlField: mRecordDiagnosa.prototype.fields.length-4,
		JmlList:GetListCountDetailDiagnosa(),
		Hapus:1,
		Ubah:0
	};
    return params
};


function GetListCountDetailDiagnosa()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++)
	{
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' || dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT  != '')
		{
			x += 1;
		};
	}
	return x;
	
};




function getArrDetailTrDiagnosa()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++)
	{
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' && dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT
			y += z + dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KASUS
			
			
			if (i === (dsTRDetailDiagnosaList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};




function Datasave_Diagnosa(mBol) 
{	
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionRWI/saveDiagnosa",
					params: getParamDetailTransaksiDiagnosa2(),
					success: function(o) 
					{
	
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoDiagnosa(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataDiagnosa();
							if(mBol === false)
							{
						
							RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWI').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
								
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWI').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
							ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						
						else 
						{
							RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWI').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
							ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		
	
};

function ShowPesanWarningDiagnosa(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorDiagnosa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoDiagnosa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};







function GetDTLTRDiagnosaGrid() 
{
    var fldDetail = ['KD_PENYAKIT','PENYAKIT','KD_PASIEN','URUT','URUT_MASUK','TGL_MASUK','KASUS','STAT_DIAG'];
	
    dsTRDetailDiagnosaList = new WebApp.DataStore({ fields: fldDetail })

    var gridDTLTRDiagnosa = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Diagnosa',
            stripeRows: true,
            store: dsTRDetailDiagnosaList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
                autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailDiagnosaList.getAt(row);
                            CurrentDiagnosa.row = row;
                            CurrentDiagnosa.data = cellSelecteddeskripsi;
                           // FocusCtrlCMDiagnosa='txtAset';
                        }
                    }
                }
            ),
            cm: TRDiagnosaColumModel()
                //, viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRDiagnosa;
};

function TRDiagnosaColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colKdProduk',
                header: 'No.ICD',
                dataIndex: 'KD_PENYAKIT',
                width:70,
					menuDisabled:true,
                hidden:false
            },
			{
                id: 'colePenyakitDiagnosa',
                header: 'Penyakit',
                dataIndex: 'PENYAKIT',
					menuDisabled:true,
				width:200
                
            }
            ,
			{
                id: 'colePasien',
                header: 'kd_pasien',
                dataIndex: 'KD_PASIEN',
				hidden:true
                
            }
			,
			{
                id: 'coleurut',
                header: 'urut',
                dataIndex: 'URUT',
				hidden:true
                
            }
			,
			{
                id: 'coleurutmasuk',
                header: 'urut masuk',
                dataIndex: 'URUT_MASUK',
				hidden:true
                
            }
            ,
			{
                id: 'coletglmasuk',
                header: 'tgl masuk',
                dataIndex: 'TGL_MASUK',
				hidden:true
                
            }
            ,
            {
                id: 'colProblemDiagnosa',
                header: 'Diagnosa',
                width:130,
				menuDisabled:true,
				//align: 'right',
				//hidden :true,
                dataIndex: 'STAT_DIAG',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboDiagnosa',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							//fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Diagnosa Awal'],[2, 'Diagnosa Utama'],[3, 'Komplikasi'],[4, 'Diagnosa Sekunder']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                ),
				
				
              
            },
            {
                id: 'colKasusDiagnosa',
                header: 'Kasus',
                width:130,
				//align: 'right',
				//hidden :true,
				menuDisabled:true,
                dataIndex: 'KASUS',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboKasus',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							//fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Baru'],[2, 'Lama']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                ),
				
				
              
            }
			

        ]
    )
};

///---------------------------------------------------------------------------------------///
function form_histori(){
    var form = new Ext.form.FormPanel({
        baseCls: 'x-plain',
        labelWidth: 55,
        url:'save-form.php',
        defaultType: 'textfield',

        items: 
            [
                {
                    x:0,
                    y:60,
                    xtype: 'textarea',
                    id:'TxtHistoriDeleteDataPasien',
                    hideLabel: true,
                    name: 'msg',
                    anchor: '100% 100%'  // anchor width by percentage and height by raw adjustment
                }

            ]
    })};
function TambahBarisRwi()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRWJ();
        dsTRDetailKasirRwiList.insert(dsTRDetailKasirRwiList.getCount(), p);
    };
};

function HapusBarisRwi()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PRODUK != '')
        {
		
           
                                          
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
											if (btn == 'ok')
														{
														variablehistori=combo;
														//alert(variablehistori=text);
														// process text value and close...
														DataDeleteKasirRwiDetail();
													     dsTRDetailKasirRwiList.removeAt(CurrentKasirRwi.row);
														}
												});

												
                                         
                                
               
        }
        else
        {
            dsTRDetailKasirRwiList.removeAt(CurrentKasirRwi.row);
        };
    }
};

function DataDeleteKasirRwiDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteKasirRwiDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoRwi(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailKasirRwiList.removeAt(CurrentKasirRwi.row);
                    cellSelecteddeskripsi=undefined;
                    RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
                    AddNewKasirRwi = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningRwi(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningRwi(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeleteKasirRwiDetail()
{
    var params =
    {
		Table: 'ViewTrKasirRwi',
        TrKodeTranskasi: CurrentKasirRwi.data.data.NO_TRANSAKSI,
		TrTglTransaksi:  CurrentKasirRwi.data.data.TGL_TRANSAKSI,
		TrKdPasien :	 CurrentKasirRwi.data.data.KD_PASIEN,
		TrKdNamaPasien : Ext.get('txtNamaPasienDetransaksi').getValue(),	
		TrKdUnit :		 Ext.get('txtKdUnitRWI').getValue(),
		TrNamaUnit :	 Ext.get('txtNamaUnit').getValue(),
		Uraian :		 CurrentKasirRwi.data.data.DESKRIPSI2,
		AlasanHapus : variablehistori,
		TrHarga :		 CurrentKasirRwi.data.data.HARGA,
		
		TrKdProduk :	 CurrentKasirRwi.data.data.KD_PRODUK,
        RowReq: CurrentKasirRwi.data.data.URUT,
        Hapus:2
    };
	
    return params
};

function getParamDataupdateKasirRwiDetail()
{
    var params =
    {
        Table: 'ViewTrKasirRwi',
        TrKodeTranskasi: CurrentKasirRwi.data.data.NO_TRANSAKSI,
        RowReq: CurrentKasirRwi.data.data.URUT,

        Qty: CurrentKasirRwi.data.data.QTY,
        Ubah:1
    };
	
    return params
};



function GetDTLTRRwiGrid() 
{
    var fldDetail = ['KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI'];
	
    dsTRDetailKasirRwiList = new WebApp.DataStore({ fields: fldDetail })
   RefreshDataKasirRwiDetail() ;
    var gridDTLTRRwi = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Input Tindakan',
            stripeRows: true,
            store: dsTRDetailKasirRwiList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
             autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailKasirRwiList.getAt(row);
                            CurrentKasirRwi.row = row;
                            CurrentKasirRwi.data = cellSelecteddeskripsi;
                           // FocusCtrlCMRwi='txtAset';
                        }
                    }
                }
            ),
            cm: TRRawatJalanColumModel()
                //, viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRRwi;
};

function TRRawatJalanColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsiRwi',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
				menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiRwi',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
				menuDisabled:true,
                width:320,
				editor: new Ext.form.TextField
                (
                    {
                        id:'fieldAsetNameRWI',
                        allowBlank: false,
                        enableKeyEvents : true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                if (Ext.EventObject.getKey() === 13)
                                {
                                    var str='';

                                   if (Ext.get('txtKdUnitRWI').dom.value  != undefined && Ext.get('txtKdUnitRWI').dom.value  != '')
									{
											//str = ' where kd_dokter =~' + Ext.get('txtKdDokterIGD').dom.value  + '~';
											//str = "\"kd_dokter\" = ~" + Ext.get('txtKdDokterIGD').dom.value  + "~";
											str =  Ext.get('txtKdUnitRWI').dom.value;
									};
									strb='';
									
									//alert(strb);
									if(Ext.get('fieldAsetNameRWI').dom.value != undefined || Ext.get('fieldAsetNameRWI').dom.value  != '')
									{
									strb= "and lower(deskripsi) like lower(~"+ Ext.get('fieldAsetNameRWI').dom.value+"%~)";
									}
									else
									{
									strb='';
									}
									GetLookupAssetCMRwi(str,strb);
									Ext.get('fieldAsetNameRWI').dom.value='';
                                };
                            }
                        }
                    }
                )
                
            }
			,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
			   	menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colHARGARwi',
                header: 'Harga',
				align: 'right',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },
            {
                id: 'colProblemRwi',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemRwi',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
							{ 
								'specialkey' : function()
								{
									
											Dataupdate_KasirRwi(false);
											//RefreshDataFilterKasirRwi();
									        //RefreshDataFilterKasirRwi();

								}
							}
                    }
                ),
				
				
              
            },

            {
                id: 'colImpactRwi',
                header: 'CR',
                width:80,
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactRwi',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
				
            }

        ]
    )
};

function GetLookupAssetCMRwi(str)
{
	if (AddNewKasirRwi === true)
	{
		var p = new mRecordRwi
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.KD_TARIF,
				'URUT':''
			}
		);
		FormLookupKasir(str,strb,nilai_kd_tarif,dsTRDetailKasirRwiList,p,true,'',false,syssetting);
		//FormLookupKasirRWJ(str,dsTRDetailKasirRwiList,p,true,'',false);
	}
	else
	{	
		var p = new mRecordRwi
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.KD_TARIF,
				'URUT':dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.URUT
			}
		);
		FormLookupKasir(str,strb,nilai_kd_tarif,dsTRDetailKasirRwiList,p,true,CurrentKasirRwi,false,syssetting);
		};
};

function RecordBaruRWJ()
{

	var p = new mRecordRwi
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
function RefreshDataSetDiagnosa(medrec,unit,tgl)
{	
 var strKriteriaDiagnosa='';
    //strKriteriaDiagnosa = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaDiagnosa = 'kd_pasien = ~' + medrec + '~ and kd_unit=~'+unit+'~ and tgl_masuk in(~'+tgl+'~)';
    //strKriteriaDiagnosa = 'no_transaksi = ~0000004~';
	
	dsTRDetailDiagnosaList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountDiagnosa, 
				//Sort: 'EMP_ID',
                Sort: 'kd_penyakit',
				Sortdir: 'ASC', 
				target:'ViewDiagnosa',
				param: strKriteriaDiagnosa
			} 
		}
	);
	rowSelectedDiagnosa = undefined;
	return dsTRDetailDiagnosaList;
};

function TRRwiInit(rowdata)
{
    AddNewKasirRwi = false;
	
	Ext.get('txtNoTransaksiKasirRwi').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
	Ext.get('txtNoMedrecDetransaksi').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	Ext.get('txtKdDokter').dom.value   = rowdata.KD_DOKTER;
	Ext.get('txtNamaDokter').dom.value = rowdata.NAMA_DOKTER;
	Ext.get('txtKdUnitRWI').dom.value   = rowdata.KD_UNIT;
	Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT;
	Ext.get('txtCustomer').dom.value = rowdata.CUSTOMER;
//	Ext.get('txtCustomerLama').dom.value=rowdata.CUSTOMER;
	Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
	vkode_customer = rowdata.KD_CUSTOMER;
	RefreshDataKasirRwiDetail(rowdata.NO_TRANSAKSI);
	RefreshDataSetDiagnosa(rowdata.KD_PASIEN,rowdata.KD_UNIT,rowdata.TANGGAL_TRANSAKSI);
	Ext.Ajax.request(
	{
	    //url: "./home.mvc/getModule",
	    //url: baseURL + "index.php/main/getTrustee",
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        //UserID: 'Admin',
	        command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			
		
	        //var cst = Ext.decode(o.responseText);
			tampungshiftsekarang=o.responseText
			//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;



	    }
	
	});
		Ext.Ajax.request(
	{
	    //url: "./home.mvc/getModule",
	    //url: baseURL + "index.php/main/getTrustee",
	    url: baseURL + "index.php/main/Getkdtarif",
		 params: {
	        //UserID: 'Admin',
	        customer: rowdata.KD_CUSTOMER,
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			
		
	        //var cst = Ext.decode(o.responseText);
			nilai_kd_tarif=o.responseText;
			//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;



	    }
	

	});
	//,,
	
	
};

function mEnabledRwiCM(mBol)
{
	 Ext.get('btnLookupRwi').dom.disabled=mBol;

	 Ext.get('btnHpsBrsRwi').dom.disabled=mBol;
};


///---------------------------------------------------------------------------------------///
function RwiAddNew() 
{
    AddNewKasirRwi = true;
	Ext.get('txtNoTransaksiKasirRwi').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksi').dom.value='';
	Ext.get('txtNamaPasienDetransaksi').dom.value = '';
	Ext.get('txtKdDokter').dom.value   = undefined;
	Ext.get('txtNamaDokter').dom.value = '';
	Ext.get('txtKdUrutMasuk').dom.value = '';
	Ext.get('cboStatus_viKasirRwi').dom.value= ''
	rowSelectedKasirRwi=undefined;
	dsTRDetailKasirRwiList.removeAll();
	mEnabledRwiCM(false);
	

};

function RefreshDataKasirRwiDetail(no_transaksi) 
{
    var strKriteriaRwi='';
    //strKriteriaRwi = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaRwi = "\"no_transaksi\" = ~" + no_transaksi + "~ and kd_kasir=~05~";
    //strKriteriaRwi = 'no_transaksi = ~0000004~';
   
    dsTRDetailKasirRwiList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailTRRWJ',
			    param: strKriteriaRwi
			}
		}
	);
    return dsTRDetailKasirRwiList;
};

///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamDetailTransaksiRwi() 
{
    var params =
	{
		Table:'ViewTrKasirRwi',
		
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirRwi').getValue(),
		KdUnit: Ext.get('txtKdUnitRWI').getValue(),
		//DeptId:Ext.get('txtKdDokter').dom.value ,tampungshiftsekarang
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		Shift: tampungshiftsekarang,
		List:getArrDetailTrRwi(),
		JmlField: mRecordRwi.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		Hapus:1,
		Ubah:0
	};
    return params
};

function getParamKonsultasi() 
{

    var params =
	{
		
		Table:'ViewTrKasirRwi', //data access listnya belum dibuat
		
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirRwi').getValue(),
		KdUnitAsal : Ext.get('txtKdUnitRWI').getValue(),
		KdDokterAsal : Ext.get('txtKdDokter').getValue(),
		KdUnit: selectKlinikPoli,
		KdDokter:selectDokter,
		KdPasien:Ext.get('txtNoMedrecDetransaksi').getValue(),
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer :vkode_customer,
	};
    return params
};

function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirRwiList.getCount();i++)
	{
		if (dsTRDetailKasirRwiList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirRwiList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getTotalDetailProduk()
{
var TotalProduk=0;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirRwiList.getCount();i++)
	{		
			var recordterakhir;
			var y='';
			var z='@@##$$@@';
			
			
			recordterakhir=dsTRDetailKasirRwiList.data.items[i].data.DESC_REQ
			TotalProduk=TotalProduk+recordterakhir
			
			Ext.get('txtJumlah1EditData_viKasirRwi').dom.value=formatCurrency(TotalProduk);
			if (i === (dsTRDetailKasirRwiList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		
	}	
	
	return x;
};





function getArrDetailTrRwi()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirRwiList.getCount();i++)
	{
		if (dsTRDetailKasirRwiList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirRwiList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirRwiList.data.items[i].data.URUT
			y += z + dsTRDetailKasirRwiList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirRwiList.data.items[i].data.QTY
			y += z + ShowDate(dsTRDetailKasirRwiList.data.items[i].data.TGL_BERLAKU)
			y += z +dsTRDetailKasirRwiList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirRwiList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirRwiList.data.items[i].data.URUT
			
			
			if (i === (dsTRDetailKasirRwiList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getItemPanelInputRwi(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:149,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiRwi(lebar),getItemPanelmedrec(lebar),getItemPanelUnit(lebar) ,getItemPanelDokter(lebar)			
				]
			}
		]
	};
    return items;
};



function getItemPanelUnit(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Unit  ',
					    name: 'txtKdUnitRWI',
					    id: 'txtKdUnitRWI',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaUnit',
					    id: 'txtNamaUnit',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokter',
					    id: 'txtKdDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtCustomer',
					    id: 'txtCustomer',
					    anchor: '99%',
						listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaDokter',
					    id: 'txtNamaDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					},
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtKdUrutMasuk',
					    id: 'txtKdUrutMasuk',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoTransksiRwi(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirRwi',
					    id: 'txtNoTransaksiKasirRwi',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi',
					    name: 'dtpTanggalDetransaksi',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelmedrec(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi',
					    id: 'txtNamaPasienDetransaksi',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	}
    return items;
};


function RefreshDataKasirRwi() 
{
    dsTRKasirRwiList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountKasirRwi,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target:'ViewTrKasirRwi',
                param : ''
            }		
        }
    );
	
    rowSelectedKasirRwi = undefined;
    return dsTRKasirRwiList;
};
function refeshkasirRwi()
{
dsTRKasirRwiList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirRwi, 
					//Sort: 'no_transaksi',
                     Sort: '',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirRwi',
					param : ''
				}			
			}
		);   
		return dsTRKasirRwiList;
}

function RefreshDataFilterKasirRwi() 
{

	var KataKunci='';
	
	 if (Ext.get('txtFilterNomedrecRwi').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrecRwi').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrecRwi').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('TxtFilterGridDataView_NAMA_viKasirRwi').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwi').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwi').getValue() + '%~)';
		};

	};
	
	
	 if (Ext.get('cboUNIT_viKasirRwi').getValue() != '' && Ext.get('cboUNIT_viKasirRwi').getValue() != 'all')
    {
		if (KataKunci == '')
		{
	
                        KataKunci = ' and  nama_kamar like  ~%' + Ext.get('cboUNIT_viKasirRwi').getValue() + '%~';
		}
		else
		{
	
                        KataKunci += ' and nama_kamar like  ~%' + Ext.get('cboUNIT_viKasirRwi').getValue() + '%~';
		};
	};
	if (Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwi').getValue() != '')
		{
		if (KataKunci == '')
		{
			
                        KataKunci = ' and nama_dokter like ~%' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwi').getValue() + '%~';
		}
		else
		{
		
                        KataKunci += ' and nama_dokter like  ~%' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwi').getValue() + '%~';
		};
	};
		
		
	if (Ext.get('cboStatus_viKasirRwi').getValue() == 'Posting')
	{
		if (KataKunci == '')
		{

                        KataKunci = ' and  posting_transaksi = TRUE';
		}
		else
		{
		
                        KataKunci += ' and posting_transaksi =  TRUE';
		};
	
	};
		
		
		
	if (Ext.get('cboStatus_viKasirRwi').getValue() == 'Belum Posting')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  posting_transaksi = FALSE';
		}
		else
		{
	
                        KataKunci += ' and posting_transaksi =  FALSE';
		};
		
		
	};
	
	if (Ext.get('cboStatus_viKasirRwi').getValue() == 'Semua')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		}
		else
		{
	
                        KataKunci += ' and (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		};
		
		
	};
	
		
	if (Ext.get('dtpTglAwalFilterRwi').getValue() != '')
	{
		if (KataKunci == '')
		{                      
						KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterRwi').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterRwi').getValue() + "~)";
		}
		else
		{
			
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterRwi').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterRwi').getValue() + "~)";
		};
	
	};
	
     
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
		dsTRKasirRwiList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirRwi, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirRwi',
					param : KataKunci
				}			
			}
		);   
    }
	else
	{
	
	dsTRKasirRwiList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirRwi, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirRwi',
					param : ''
				}			
			}
		);   
	};
    
	return dsTRKasirRwiList;
};


function Datasave_Konsultasi(mBol) 
{	
	if (ValidasiEntryKonsultasi(nmHeaderSimpanData,false) == 1 )
	{
		
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionRWI/KonsultasiPenataJasa",					
					params: getParamKonsultasi(),
					failure: function(o)
					{
					ShowPesanWarningRwi('Konsultasi ulang gagal', 'Gagal');
					RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRwi(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirRwi();
							if(mBol === false)
							{
								RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
							};
						}
						else 
						{
								ShowPesanWarningRwi('Konsultasi ulang gagal', 'Gagal');
						
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};



function Datasave_KasirRwi(mBol) 
{	
	if (ValidasiEntryCMRwi(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionRWI/savedetailpenyakit",
					params: getParamDetailTransaksiRwi(),
					failure: function(o)
					{
					ShowPesanWarningRwi('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
					RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRwi(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirRwi();
							if(mBol === false)
							{
								RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
							};
						}
						else 
						{
								ShowPesanWarningRwi('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};


function Dataupdate_KasirRwi(mBol) 
{	
	if (ValidasiEntryCMRwi(nmHeaderSimpanData,false) == 1 )
	{
		
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",
					params: getParamDataupdateKasirRwiDetail(),
					success: function(o) 
					{
						RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRwi(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirRwi();
							if(mBol === false)
							{
								RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRwi(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningRwi(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorRwi(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};
function ValidasiEntryCMRwi(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtNoTransaksiKasirRwi').getValue() == '') || (Ext.get('txtNoMedrecDetransaksi').getValue() == '') || (Ext.get('txtNamaPasienDetransaksi').getValue() == '') || (Ext.get('txtNamaDokter').getValue() == '') || (Ext.get('dtpTanggalDetransaksi').getValue() == '') || dsTRDetailKasirRwiList.getCount() === 0 || (Ext.get('txtKdDokter').dom.value  === undefined ))
	{
		if (Ext.get('txtNoTransaksiKasirRwi').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
		{
			ShowPesanWarningRwi(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '') 
		{
			ShowPesanWarningRwi(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}
		else if (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
		{
			ShowPesanWarningRwi(nmGetValidasiKosong('Tanggal Kunjungan'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaDokter').getValue() == '' || Ext.get('txtKdDokter').dom.value  === undefined) 
		{
			ShowPesanWarningRwi(nmGetValidasiKosong(nmDeptRequest), modul);
			x = 0;
		}
		else if (dsTRDetailKasirRwiList.getCount() === 0) 
		{
			ShowPesanWarningRwi(nmGetValidasiKosong(nmTitleDetailFormRequest),modul);
			x = 0;
		};
	};
	return x;
};

function ValidasiEntryKonsultasi(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboPoliklinikRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').getValue() == '') )
	{
		if (Ext.get('cboPoliklinikRequestEntry').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('cboDokterRequestEntry').getValue() == '') 
		{
			ShowPesanWarningRwi(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
	};
	return x;
};




function ShowPesanWarningRwi(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorRwi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoRwi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function DataDeleteKasirRwi() 
{
   if (ValidasiEntryCMRwi(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                //url: "./Datapool.mvc/DeleteDataObj",
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiRwi(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoRwi(nmPesanHapusSukses,nmHeaderHapusData);
                                        RefreshDataKasirRwi();
                                        RwiAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningRwi(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningRwi(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorRwi(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};


/*---LOOK UP GANTI DOKTER BESERTA ELEMENNYA----------------------------------*/

//function GantiDokterLookUp(rowdata)
function GantiDokterLookUp(mod_id) 
{
   
   

    var FormDepanDokter = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Ganti Dokter',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [DokterLookUp()],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	

	
   return FormDepanDokter

};









function DokterLookUp(rowdata) 
{
    var lebar = 350;
    FormLookUpGantidokter = new Ext.Window
    (
        {
            id: 'gridDokter',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 180,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryDokter(lebar),
            listeners:
            {
                activate: function()
                {
                     if (varBtnOkLookupEmp === true)
                    {
                        Ext.get('txtKdDokter').dom.value   = rowSelectedLookdokter.data.KD_DOKTER;
                        Ext.get('txtNamaDokter').dom.value = rowSelectedLookdokter.data.NAMA_DOKTER;
                        varBtnOkLookupEmp=false;
                    };
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
                    rowSelectedKasirDokter=undefined;
                    //RefreshDataFilterKasirDokter();
                }
            }
        }
    );

    FormLookUpGantidokter.show();
 

};
function getItemPanelButtonGantidokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:310,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:100,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkGantiDokter',
						handler:function()
						{
						GantiDokter(false);
						FormLookUpGantidokter.close();	
						}
					},
					{
							xtype:'button',
							text:'Tutup' ,
							width:70,
							hideLabel:true,
							id: 'btnCancelGantidokter',
							handler:function() 
							{
								FormLookUpGantidokter.close();
							}
					}
				]
			}
		]
	}
    return items;
};
function getFormEntryDokter(lebar) 
{
    var pnlTRGantiDokter = new Ext.FormPanel
    (
        {
            id: 'PanelTRDokter',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:190,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputGantidokter(lebar),getItemPanelButtonGantidokter(lebar)],
           tbar:
            [
               
               
            ]
        }
    );


	
   
   
  
   
   
   
   
    var FormDepanDokter = new Ext.Panel
	(
		{
		    id: 'FormDepanDokter',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRGantiDokter
				

			]

		}
	);

    return FormDepanDokter
};
///---------------------------------------------------------------------------------------///


///---------------------------------------------------------------------------------------///


function getItemPanelInputGantidokter(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:95,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiDokter(lebar)		
				]
			}
		]
	};
    return items;
};

function ValidasiEntryTutupDokter(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtNilaiDokter').getValue() == '' || (Ext.get('txtNilaiDokterSelanjutnya').getValue() == ''))
	{
		if (Ext.get('txtNilaiDokter').getValue() == '' && mBolHapus === true)
		{
			x=0;
		}
		else if (Ext.get('txtNilaiDokterSelanjutnya').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};

function getItemPanelNoTransksiDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: 1.0,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'cmbUnitAsal',
					    id: 'cmbUnitAsal',
						value:Ext.get('txtNamaUnit').getValue(),
						readOnly:true,
					    anchor: '100%'
					},
					
					{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'cmbDokterAsal',
					    id: 'cmbDokterAsal',
						value:Ext.get('txtNamaDokter').getValue(),
						readOnly:true,
					    anchor: '100%'
						
					},
			
					mComboDokterGantiEntry()
				
					
				]
				
			
			}
			
			
		]
	}
    return items;
};


function mComboDokterGantiEntry()
{ 
	

    //-------------- # End form filter # --------------

 var Field = ['KD_DOKTER','NAMA'];

    dsDokterGantiEntry = new WebApp.DataStore({fields: Field});
	var kDUnit = Ext.get('txtKdUnitRWI').getValue();
	var kddokter = Ext.get('txtKdDokter').getValue();
   dsDokterGantiEntry.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								//Sort: 'DEPT_ID',
								Sort: 'nama',
								Sortdir: 'ASC',
								target: 'ViewComboDokterRWI',
								param: 'where dk.kd_unit=~'+ kDUnit+ '~ and d.kd_dokter not in (~'+kddokter+'~) group by d.kd_dokter'
								//param: 'where d.kd_dokter not in (~'+kddokter+'~)'
							}
                    }
                )

    var cboDokterGantiEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
			name:'txtdokter',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    fieldLabel: 'Dokter Baru',
		    align: 'Right',
             
		    store: dsDokterGantiEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
			anchor:'100%',
		    listeners:
			{
			    'select': function(a,b,c)
				{

									selectDokter = b.data.KD_DOKTER;
									NamaDokter = b.data.NAMA;
									 Ext.get('txtKdDokter').dom.value = b.data.KD_DOKTER
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);
	
    return cboDokterGantiEntry;

};

/*-----------Update Dokter----------------------------------*/
function GantiDokter(mBol)
{
    if (ValidasiGantiDokter(nmHeaderSimpanData,false) == 1 )
    {
            
                    Ext.Ajax.request
                     (
                            {
                                   url: WebAppUrl.UrlUpdateData,
                                    params: getParamGantiDokter(),
									failure: function(o)
										{
										 ShowPesanErrorRwi('Ganti Dokter Gagal','Ganti Dokter');
										},	
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoRwi(nmPesanSimpanSukses,'Ganti Dokter');
													Ext.get('txtKdDokter').dom.value = selectDokter;
													Ext.get('txtNamaDokter').dom.value = NamaDokter;
													FormDepanDokter.close();
                                                    FormLookUpGantidokter.close();
                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanErrorRwi('Ganti Dokter Gagal','Ganti Dokter');
                                            }
                                            else
                                            {
                                                    ShowPesanErrorRwi('Ganti Dokter Gagal','Ganti Dokter');
                                            };
                                    }
                            }
                    )
            
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };

};//END FUNCTION TutupDokterSave

/*---------------------------------------------*/

function ValidasiGantiDokter(modul,mBolHapus)
{
	var x = 1;
	if ((Ext.get('cboDokterRequestEntry').getValue() == ''))
	{
	  if (Ext.get('cboDokterRequestEntry').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};


function getParamGantiDokter()
{
    var params =
	{
        Table: 'ViewGantiDokter',
		TxtMedRec : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TxtTanggal:Ext.get('dtpTanggalDetransaksi').getValue(),
		KdUnit :  Ext.get('txtKdUnitRWI').getValue(),
		KdDokter : selectDokter,
		kodebagian : 2
		
	};
    return params
};

/*-----AKHIR LOOK UP GANTI DOKTER BESERTA ELEMENYNYA-----------------------------*/


/*------------AWAL LOOK UP GANTI KD CUSTOMER PASIEN BESERTA ELEMENNYA -------------- */

function KelompokPasienLookUp(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien.show();
    KelompokPasienbaru();

};


function getFormEntryTRKelompokPasien(lebar) 
{
    var pnlTRKelompokPasien = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputKelompokPasien(lebar),getItemPanelButtonKelompokPasien(lebar)],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasien = new Ext.Panel
	(
		{
		    id: 'FormDepanKelompokPasien',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien	
				
			]

		}
	);

    return FormDepanKelompokPasien
};

function getItemPanelInputKelompokPasien(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama(lebar),	getItemPanelNoTransksiKelompokPasien(lebar)	,
					
				]
			}
		]
	};
    return items;
};




function getItemPanelButtonKelompokPasien(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkKelompokPasien',
						handler:function()
						{
					
					Datasave_Kelompokpasien();
					FormLookUpsdetailTRKelompokPasien.close();
							
						}
					},
					{
							xtype:'button',
							text:'Tutup',
							width:70,
							hideLabel:true,
							id: 'btnCancelKelompokPasien',
							handler:function() 
							{
								FormLookUpsdetailTRKelompokPasien.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function KelompokPasienbaru() 
{
	jeniscus=0;
    KelompokPasienAddNew = true;
	//Ext.getCmp('cboPerseorangan').show()
    Ext.getCmp('cboKelompokpasien').show()
	Ext.getCmp('txtNoSJP').disable();
	Ext.getCmp('txtNoAskes').disable();
	RefreshDatacombo(jeniscus);
	Ext.get('txtCustomerLama').dom.value=	Ext.get('txtCustomer').dom.value
    //Ext.getCmp('cboPerusahaanRequestEntry').hide()
	

};

function RefreshDatacombo(jeniscus) 
{

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customer+'~)'
            }
        }
    )
	
   // rowSelectedKasirRwi = undefined;
    return ds_customer_viDaftar;
};
function mComboKelompokpasien()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    )
    var cboKelompokpasien = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: labelisi,
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien;
};

function getKelompokpasienlama(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
	//	title: 'Kelompok Pasien Lama',
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
				//title: 'Kelompok Pasien Lama',
			    items:
				[
					
					
						{	 
															xtype: 'tbspacer',
														
															height: 2
						},
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'Kelompok Pasien Asal',
                                        //maxLength: 200,
                                        name: 'txtCustomerLama',
                                        id: 'txtCustomerLama',
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     },
					
					
				]
			}
			
		]
	}
    return items;
};


function getItemPanelNoTransksiKelompokPasien(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[
					
					
														{	 
															xtype: 'tbspacer',
															
															height:3
														},
									{  

                                    xtype: 'combo',
                                    fieldLabel: 'Kelompok Pasien Baru',
                                    id: 'kelPasien',
                                     editable: false,
                                    //value: 'Perseorangan',
                                    store: new Ext.data.ArrayStore
                                        (
                                            {
                                            id: 0,
                                            fields:
                                            [
											'Id',
											'displayText'
                                            ],
                                               data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                            }
                                        ),
										  displayField: 'displayText',
										  mode: 'local',
										  width: 100,
										  forceSelection: true,
										  triggerAction: 'all',
										  emptyText: 'Pilih Salah Satu...',
										  selectOnFocus: true,
										  anchor: '95%',
										  listeners:
											 {
													'select': function(a, b, c)
												{
												if(b.data.displayText =='Perseorangan')
												{jeniscus='0'
												Ext.getCmp('txtNoSJP').disable();
												Ext.getCmp('txtNoAskes').disable();}
												else if(b.data.displayText =='Perusahaan')
												{jeniscus='1';
												Ext.getCmp('txtNoSJP').disable();
												Ext.getCmp('txtNoAskes').disable();}
												else if(b.data.displayText =='Asuransi')
												{jeniscus='2';
												Ext.getCmp('txtNoSJP').enable();
												Ext.getCmp('txtNoAskes').enable();
											}
												
												RefreshDatacombo(jeniscus);
												}

											}
                                  }, 
								  {
										columnWidth: .990,
										layout: 'form',
										border: false,
										labelWidth:130,
										items:
										[
															mComboKelompokpasien()
										]
									},
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'No. SJP',
                                        maxLength: 200,
                                        name: 'txtNoSJP',
                                        id: 'txtNoSJP',
                                        width: 100,
                                        anchor: '95%'
                                     },
									  {
                                        xtype: 'textfield',
                                        fieldLabel: 'No. Askes',
                                        maxLength: 200,
                                        name: 'txtNoAskes',
                                        id: 'txtNoAskes',
                                        width: 100,
                                        anchor: '95%'
                                     }
									
									//mComboKelompokpasien
					
					
				]
			}
			
		]
	}
    return items;
};

function Datasave_Kelompokpasien(mBol) 
{	
	if (ValidasiEntryUpdateKelompokPasien(nmHeaderSimpanData,false) == 1 )
	{
		
		
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL +  "index.php/main/functionRWI/UpdateKdCustomer",	
					params: getParamKelompokpasien(),
					failure: function(o)
					{
					ShowPesanWarningRwi('Simpan kelompok pasien gagal', 'Gagal');
					RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
						Ext.get('txtCustomer').dom.value = selectNamaCustomer;
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRwi(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirRwi();
							if(mBol === false)
							{
								RefreshDataKasirRwiDetail(Ext.get('txtNoTransaksiKasirRwi').dom.value);
							};
						}

						else 
						{
								ShowPesanWarningRwi('Simpan kelompok pasien gagal', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};


function getParamKelompokpasien() 
{
	
    var params =
	{
		
		Table:'ViewTrKasirRwi', 
		TrKodePasien : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirRwi').getValue(),
		KdUnit: Ext.get('txtKdUnitRWI').getValue(),
		KdDokter:Ext.get('txtKdDokter').dom.value ,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer:selectKdCustomer,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDNoSJP :Ext.get('txtNoSJP').dom.value,
		KDNoAskes :Ext.get('txtNoAskes').dom.value
		
		
	};
    return params
};

function ValidasiEntryUpdateKelompokPasien(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('kelPasien').getValue() == '') || (Ext.get('kelPasien').dom.value  === undefined ))
	{
		if (Ext.get('kelPasien').getValue() == '' && mBolHapus === true) 
		{
			ShowPesanWarningRwi(nmGetValidasiKosong('Kelompok Pasien'), modul);
			x = 0;
		}
	};
	return x;
};


/*------------------------- AKHIR LOOK UP GANTI KD CUSTOMER PASIEN ------------------------------------*/

function setpostingtransaksi(notransaksi) 
{
	//if (ValidasiEntrySetJadwalDokter(nmHeaderHapusData,true) == 1 )
	//{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: 'Kirim Data Transaksi ini Ke Kasir ? ' ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								//url: WebAppUrl.UrlDeleteData,
								url : baseURL + "index.php/main/posting",
								params: 
								{
								_notransaksi : 	notransaksi,
								},
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										//refeshkasirRwi();
										RefreshDataFilterKasirRwi();
										ShowPesanInfoDiagnosa('Posting Berhasil Dilakukan',nmHeaderHapusData);
										//setLookUps_viDaftar.close();
										//dataaddnew_viJadwal();
										FormLookUpsdetailTRRwi.close();
										
										
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningDiagnosa(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanWarningDiagnosa(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
	



function setdisablebutton()
{
Ext.getCmp('btnLookUpKonsultasi_viKasirIgd').disable();
Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').disable();
Ext.getCmp('btngantipasien').disable();
Ext.getCmp('btnposting').disable();	

Ext.getCmp('btnLookupRwi').disable()
Ext.getCmp('btnSimpanRwi').disable()
Ext.getCmp('btnHpsBrsRwi').disable()
Ext.getCmp('btnHpsBrsDiagnosa').disable()
Ext.getCmp('btnSimpanDiagnosa').disable()
Ext.getCmp('btnLookupDiagnosa').disable()
}

function setenablebutton()
{
Ext.getCmp('btnLookUpKonsultasi_viKasirIgd').enable();
Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').enable();
Ext.getCmp('btngantipasien').enable();
Ext.getCmp('btnposting').enable();	

Ext.getCmp('btnLookupRwi').enable()
Ext.getCmp('btnSimpanRwi').enable()
Ext.getCmp('btnHpsBrsRwi').enable()
Ext.getCmp('btnHpsBrsDiagnosa').enable()
Ext.getCmp('btnSimpanDiagnosa').enable()
Ext.getCmp('btnLookupDiagnosa').enable()	
}

function setLookUp_viPindahKamarRWI(rowdata)
{
    var lebar = 500;
    setLookUps_viPindahDokterRWI = new Ext.Window
    (
    {
        id: 'setLookUps_viPindahDokterRWI',
        name: 'setLookUps_viPindahDokterRWI',
        title: 'Pindah Kamar Pasien Rawat Inap', 
        closeAction: 'destroy',        
        width: lebar,
        height: 200,//575,
        resizable:false,
		autoScroll: false,
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viDaftarRWI(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboSukuRequestEntry').hide(); //suku di hide
                Ext.getCmp('txtJamKunjung').hide();
                Ext.getCmp('dptTanggal').hide();
                Ext.getCmp('txtNama_viDaftarRWIPeserta').hide();
                Ext.getCmp('txtNoAskes').hide();
                Ext.getCmp('txtNoSJP').hide();
                Ext.getCmp('cboPerseorangan').show();
                Ext.getCmp('cboAsuransi').hide();
                Ext.getCmp('cboPerusahaanRequestEntry').hide();
                if(rowdata !== undefined)
                {
                    Ext.getCmp('txtNama_viDaftarRWI').disable();
                    Ext.getCmp('txtNoRequest_viDaftarRWI').disable();
                    Ext.getCmp('txtNama_viDaftarRWI').disable();
                    Ext.getCmp('txtNama_viDaftarRWIKeluarga').disable();
                    Ext.getCmp('txtTempatLahir_viDaftarRWI').disable();
                    Ext.getCmp('cboPendidikanRequestEntry').disable();
                    Ext.getCmp('cboPekerjaanRequestEntry').disable();
                    Ext.getCmp('cboWarga').disable();
                    Ext.getCmp('txtAlamat_viDaftarRWI').disable();
                    Ext.getCmp('cboAgamaRequestEntry').disable();
                    Ext.getCmp('cboGolDarah').disable();
                    Ext.getCmp('dtpTanggalLahir_viDaftarRWI').disable();
                    Ext.getCmp('cboStatusMarital').disable();
                    Ext.getCmp('cboJK').disable();
                    Ext.getCmp('cboWarga').disable();
                    Ext.getCmp('cboPropinsiRequestEntry').disable();
                    Ext.getCmp('cboKabupatenRequestEntry').disable();
                    Ext.getCmp('cboKecamatanRequestEntry').disable();
                }
            },
            afterShow: function()
            {
                //this.activate();
            },
            deactivate: function()
            {
               
            }
        }
    }
    );

    setLookUps_viPindahDokterRWI.show();
    if (rowdata == undefined)
    {
        dataaddnew_viDaftarRWI();
    }
    else
    {
        datainit_viDaftarRWI(rowdata);
    }
}

function getFormItemEntry_viDaftarRWI(lebar,rowdata)
{
    var pnlFormDataBasic_viDaftarRWI = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',//'center',
			layout: 'form',//'anchor',
			bodyStyle: 'padding:10px 15px 10px 15px',
			anchor: '100%',
                        autoScroll: true,
			width: 500,//lebar-55,
                        height: 400,
			border: false,

                        items:[
                            getItemPanelInputBiodata_viDaftarRWI(),
                            //getPenelItemDataKunjungan_viDaftarRWI(lebar)
                                ],
			fileUpload: true,
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					
                                        
				]
			}
//                        ,items:
			
		}
    )
    

    return pnlFormDataBasic_viDaftarRWI;
}


//form view data biodata pasien
function getItemPanelInputBiodata_viDaftarRWI() 
{
    var items =
	{
	    layout: 'fit',
	    border: false,
        labelAlign: 'top',
	    items:
		[
                       
			{
			    columnWidth: .20,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
				
				
					FieldsetRuanganLama()
					
					
				]
			}
		]
        };
    return items;
};
//-------------------------end--------------------------------------------------



function FieldsetRuanganLama() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
        labelAlign: 'top',
	    items:
		[
                        {
			    columnWidth: .40,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
					mComboPoliklinik(),
				]
			},
			{
			    columnWidth: .20,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
					mComboKelasSpesialRequestEntry(),
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
				mComboRuanganRequestEntry(),	
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
				mComboKamarRequestEntry(),
				
				]
			},
				{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
                labelWidth:90,
			    items:
				[
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'Sisa Kamar',
                                        maxLength: 100,
                                        name: 'txtSisaKamar_viDaftarRWI',
                                        id: 'txtSisaKamar_viDaftarRWI',
                                        width: 50,
                                        disabled: true,
                                        anchor: '95%'
                                     }, 
									 
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'No. Transaksi',
                                        maxLength: 100,
                                        name: 'txtnotransaksi',
                                        id: 'txtnotransaksi',
										value:Ext.get('txtNoTransaksiKasirRwi').getValue(),
										hidden:true,
                                        width: 50,
                                        disabled: true,
                                        anchor: '95%'
                                     }, 
									 
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'No. Medrec',
                                        maxLength: 100,
                                        name: 'txtkdpasien',
                                        id: 'txtkdpasien',
										value:Ext.get('txtNoMedrecDetransaksi').getValue(),
                                        width: 50,
                                        disabled: true,
										hidden:true,
                                        anchor: '95%'
                                     },
									 {
														xtype: 'textfield',
														//fieldLabel:'Dokter  ',
														name: 'txttanggal',
														id: 'txttanggal',
														//emptyText:nmNomorOtomatis,
														value:Ext.get('dtpTanggalDetransaksi').getValue(),
														hidden :false,
														readOnly:true,
														hidden:true,
														anchor: '80%'
													}
									 
								
										
				
				]
			},
			{
			    columnWidth: .90,
			    layout: 'form',
			    border: false,
                labelWidth:90,
			    items:
				[
				 {
                                        xtype: 'button',
                                        text: 'Simpan Data Pindah',
                                        anchor: '20%',
										style: {'padding':'20px 10px','margin-left':'-10px','margin-top':'-10px'},
				    					hideLabel: true,
										heigth:30,
				    					id: 'btnOkpgw',
				    					handler: function()
										{
										savepindahkamar();	
										//GetPasien(nFormAsal,nName_ID);
				    					}
                                     },  
									  /*{
                                        xtype: 'button',
                                        text: 'Close',
                                        anchor: '95%',
										style: {'padding':'25px 10px'},
				    					hideLabel: true,
				    					id: 'btnClose',
				    					handler: function()
										{
										
				    					}
                                     },  */
				]
			},
			
			
		]
			
        };
    return items;
};


function mComboPoliklinik()
{
    var Field = ['KD_SPESIAL','SPESIALISASI'];
    ds_Poli_viDaftarRWI = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftarRWI.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'kd_spesial',
                Sortdir: 'ASC',
                target:'ViewSetupSpesial',
                param: "kd_spesial <> 0"
            }
        }
    )

    var cboPoliklinikRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboPoliklinikRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Spesialisasi...',
            fieldLabel: 'Spesialisasi ',
            align: 'Right',
            store: ds_Poli_viDaftarRWI,
            valueField: 'KD_SPESIAL',
            displayField: 'SPESIALISASI',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   loaddatastorekelas(b.data.KD_SPESIAL)
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }
        	}
        }
    )

    return cboPoliklinikRequestEntry;
};


function mComboKelasSpesialRequestEntry()
{
    var Field = ['KD_KELAS','KELAS'];

    dsKelasRequestEntry = new WebApp.DataStore({fields: Field});


    var cboKelasRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKelasRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Kelas...',
		    fieldLabel: 'Kelas ',
		    align: 'Right',
                     
//		    anchor:'60%',
		    store: dsKelasRequestEntry,
		    valueField: 'KD_KELAS',
		    displayField: 'KELAS',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   loaddatastoreruangan(b.data.KD_KELAS)
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboKelasRequestEntry;
};


function mComboRuanganRequestEntry()
{
    var Field = ['KD_UNIT','NAMA'];

    dsRuanganRequestEntry = new WebApp.DataStore({fields: Field});


    var cboRuanganRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboRuanganRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Ruangan...',
		    fieldLabel: 'Ruang / Unit ',
		    align: 'Right',
		    store: dsRuanganRequestEntry,
		    valueField: 'KD_UNIT',
		    displayField: 'NAMA',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   loaddatastorekamar(b.data.KD_UNIT)
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboRuanganRequestEntry;
};

function loaddatastorekamar(KD_UNIT)
{
    dsKamarRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
                            Sort: 'no_kamar',
			    Sortdir: 'ASC',
			    target: 'ViewComboKamar',
			    param: 'kd_unit =~'+ KD_UNIT + '~'
			}
                    }
                )
}
var tmpkdunitkamar;

function mComboKamarRequestEntry()
{
    var Field = ['KD_UNIT','NO_KAMAR','NAMA_KAMAR'];

    dsKamarRequestEntry = new WebApp.DataStore({fields: Field});


    var cboKamarRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKamarRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Kamar...',
		    fieldLabel: 'Kamar ',
		    align: 'Right',
		    store: dsKamarRequestEntry,
		    valueField: 'NO_KAMAR',
		    displayField: 'NAMA_KAMAR',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                    viewsisakamar();
                                    tmpkdunitkamar = b.data.KD_UNIT;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboKamarRequestEntry;
};

function loaddatastorekelas(KD_SPESIAL)
{
          dsKelasRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
                            Sort: 'kd_kelas',
			    Sortdir: 'ASC',
			    target: 'ViewComboKelasSpesial',
			    param: 'where kd_spesial =~'+ KD_SPESIAL+ '~'
			}
                    }
                )
};


function loaddatastoreruangan(KD_KELAS)
{
    var kd_spesial = Ext.getCmp('cboPoliklinikRequestEntry').getValue();
    dsRuanganRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
                            Sort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ViewComboRuangan',
			    param: 'where kd_spesial =~'+ kd_spesial + '~'+' And '+'k.kd_kelas =~'+ KD_KELAS + '~'
			}
                    }
                )
    
}


function viewsisakamar()
{
    Ext.Ajax.request
            (
                {
                        url: baseURL + "index.php/main/ReadData",
                        params: datavaramsisakamar(),
                        success: function(o)
                        {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
//                                        ShowPesanInfo_viDaftarRWI('Data berhasil di simpan','Simpan Data');
                                        Ext.get('txtSisaKamar_viDaftarRWI').dom.value = cst.totalrecords;
                                       
                                }else{
                                    Ext.get('txtSisaKamar_viDaftarRWI').dom.value = 0;
                                }
                                 if(cst.totalrecords <= 0)
                                        {
                                          ShowPesanInfo_viDaftarRWI('Tidak ada kamar kosong, silahkan pilih kamar lain','Simpan Data');
                                        }
                                }
                }
            )
}

function datavaramsisakamar()
{
    var params_ViPendaftaranIGD =
		{
                    Table: 'View_SisaKamar',
                    Spesialisasi: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
                    Kelas: Ext.getCmp('cboKelasRequestEntry').getValue(),
                    Ruangan: Ext.getCmp('cboRuanganRequestEntry').getValue(),
                    Kamar: Ext.getCmp('cboKamarRequestEntry').getValue(),
		};
    return params_ViPendaftaranIGD
    
}


function dataparam_PindahKamar()
{
  
		var params_pindahkamar =
		{
      
                    Table: 'vipindahkamar',
                    NoMedrec:  Ext.get('txtkdpasien').getValue(),
                    NoTransaksi: Ext.get('txtnotransaksi').getValue(),
                    
                     //NamaPerujuk : Ext.get('txtNama_viDaftarRWIPerujuk').getValue(),
                    //AlamatPerujuk : Ext.get('txtAlamat_viDaftarRWIPerujuk').getValue(),
                    Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
                    Kamar : Ext.getCmp('cboKamarRequestEntry').getValue(),
                    Kelas : Ext.getCmp('cboKelasRequestEntry').getValue(),
                    Ruang : Ext.getCmp('cboRuanganRequestEntry').getValue(),
                    KDUnitKamar : Ext.getCmp('cboKamarRequestEntry').getValue(),
					tgltransaksi : Ext.getCmp('txttanggal').getValue(),
                    
		};
    return params_pindahkamar
}


function savepindahkamar(mBol)
{	

        //alert('a')
   Ext.Ajax.request
            (
				{
					url: baseURL + "index.php/main/CreateDataObj",
					params: dataparam_PindahKamar(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfo('Data berhasil di simpan','Simpan Data');
							setLookUps_viPindahDokterRWI.close();
//							Ext.getCmp('btnDelete_viDaftarRWI').enable();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
						else
						{
							ShowPesanError('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
					}
				}
            )
 
       

}

function ShowPesanInfo(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}

function ShowPesanWarning(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanError(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

