var dsRWIDetailPasien;
var selectNamaRWIDetailPasien;
var now = new Date();
var selectCount_viInformasiUnitdokter=50;
var icons_viInformasiUnitdokter="Gaji";
var dataSource_kunjungan;
var dsDataSource_kunjungan = new Ext.data.JsonStore();
var selectSetJenisPasien='Semua Pasien';
var frmDlgRWIDetailPasien;
var secondGridStore;
var varDlgBukaLaporan= ShowFormDlgBukaLaporan();
var selectSetUmum;
var firstGrid;
var secondGrid;
var comboOne;
var type_file=0;
var title_unit='';
var CurrentData_viDaftarRWI =
{
    data: Object,
    details: Array,
    row: 0
};

function ShowFormDlgBukaLaporan()
{
    frmDlgRWIDetailPasien= fnDlgRWIBukaTransaksi();
    frmDlgRWIDetailPasien.show();
};
function fnDlgRWIBukaTransaksi()
{
    // console.log("Cek");
    var winRWIBukatransaksi = new Ext.Window
    (
        {
            id: 'winRWIBukatransaksi',
            title: 'Buka Transaksi',
            closeAction: 'destroy',
            width:500,
            height: '100%',
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [dataGrid_viSummeryPasien()],
            listeners:
        {
            activate: function()
            {
            }
        }

        }
    );

    return winRWIBukatransaksi;
};


function ShowPesanWarningRWIDetailPasienReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function mComboKunjunganPasien()
{
    var Field = ['TANGGAL_MASUK'];

        /*var cols = [
            { id : 'KD_DOKTER', header: "Kode Dokter", width: .25, sortable: true, dataIndex: 'KD_DOKTER'},
        ];*/
    dsDataSource_kunjungan = new WebApp.DataStore({ fields: Field });
    // console.log(dataSource_kunjungan);
    // datarefresh_dataSourceKunjungan();
    var cboDataKunjungan = new Ext.form.ComboBox
    (
        {

            id:'cboDataKunjungan',
            typeAhead       : true,
            triggerAction   : 'all',
            lazyRender      : true,
            mode            : 'local',
            selectOnFocus   : true,
            forceSelection  : true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: 'Tgl Kunjungan ',
            align: 'Right',
            width: 150,
            // value: now.format("Y-m-d"),
            store: dsDataSource_kunjungan,
            valueField: 'TANGGAL_MASUK',
            displayField: 'TANGGAL_MASUK',
            listeners:
            {
                select : function(a, b){
                    // console.log(b.data.TANGGAL_MASUK);
                    loadMask.show();
                    Ext.Ajax.request({
                        url: baseURL + "index.php/rawat_inap/data_kunjungan/data_kunjungan_inap",
                        params: {
                            kd_kasir     : '02',
                            kd_pasien    : Ext.getCmp('textfieldNoMedrec').getValue(),
                            tgl_masuk    : b.data.TANGGAL_MASUK,
                        },
                        success: function(response) {
                            var cst  = Ext.decode(response.responseText);
                            if (cst.status_inap == false) {
                                Ext.getCmp('btnOkDlgBukaLaporan').disable(); 
                                ShowPesanWarningBukaTransaksi('Pasien masih menginap');
                            }else{
                                console.log("Cek");
                                Ext.getCmp('btnOkDlgBukaLaporan').enable(); 
                            }
                            loadMask.hide();
                        },
                    });
                }      
            }
        }
    );
    return cboDataKunjungan;
};


function datarefresh_dataSourceKunjungan()
{
    loadMask.show();
    dsDataSource_kunjungan.removeAll();
    Ext.getCmp('btnOkDlgBukaLaporan').disable(); 
    Ext.Ajax.request({
        url: baseURL + "index.php/rawat_inap/data_kunjungan/kunjungan",
        params: {
            kd_pasien : Ext.getCmp('textfieldNoMedrec').getValue(),
        },
        success: function(response) {
            //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
            var cst  = Ext.decode(response.responseText);
            //dsDataPerawatPenindak_KASIR_RWI.load(myData);
            if (cst['DATA'].length > 0) {
                for(var i=0,iLen=cst['DATA'].length; i<iLen; i++){
                    var recs    = [],recType = dsDataSource_kunjungan.recordType;
                    var o       = cst['DATA'][i];
                    recs.push(new recType(o));
                    dsDataSource_kunjungan.add(recs);
                    // console.log(cst['DATA'][i]);
                }
                Ext.getCmp('textfieldNama').setValue(cst.NAMA);
            }else{
                ShowPesanWarningBukaTransaksi("Pasien belum pernah Rawat Inap", "Information");
            }
            loadMask.hide();
        },     
    });
}

function dataGrid_viSummeryPasien(mod_id)
{
    var FrmTabs_viInformasiUnitdokter = new Ext.Panel
        (
            {
                id          : mod_id,
                closable    : true,
                region      : 'center',
                layout      : 'column',
                height      : 130,
                itemCls     : 'blacklabel',
                bodyStyle   : 'padding: 5px 5px 5px 5px',
                border      : false,
                shadhow     : true,
                margins     : '5 5 5 5',
                anchor      : '99%',
                iconCls     : icons_viInformasiUnitdokter,
                items       : 
                [
                                    {
                        columnWidth: .97,
                        layout: 'form',
                        border: true,
                        autoScroll: true,
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                                            labelAlign: 'right',
                                            labelWidth: 50,
                        items:
                        [

                            {
                                columnWidth: .50,
                                layout: 'form',
                                border: false,
                                labelAlign: 'right',
                                labelWidth: 100,
                                items:
                                [
                                    {
                                        xtype       : 'textfield',
                                        fieldLabel  : 'No Medrec ',
                                        id          : 'textfieldNoMedrec',
                                        anchor      : '100%',
                                        listeners   : {
                                            specialkey: function (){
                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
                                                    var tmpNoMedrec = Ext.get('textfieldNoMedrec').getValue();
                                                    if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                                    {   
                                                        var tmpgetNoMedrec = formatnomedrec(Ext.get('textfieldNoMedrec').getValue());
                                                        Ext.getCmp('textfieldNoMedrec').setValue(tmpgetNoMedrec);
                                                        // var tmpkriteria = getCriteriaFilter_viDaftar();
                                                        Ext.getCmp('cboDataKunjungan').setValue();
                                                        datarefresh_dataSourceKunjungan();

                                                    } else{
                                                        if (tmpNoMedrec.length === 10){
                                                            Ext.getCmp('cboDataKunjungan').setValue();
                                                            datarefresh_dataSourceKunjungan();
                                                        } else{
                                                            Ext.getCmp('cboDataKunjungan').setValue();
                                                            datarefresh_dataSourceKunjungan();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Nama ',
                                        id: 'textfieldNama',
                                        anchor: '100%'
                                    },
                                    mComboKunjunganPasien(),
                                ]
                            },
                        ]
                    },{
                        columnWidth: .9999,
                        layout: 'hBox',
                        border: false,
                        defaults: { margins: '0 5 0 0' },
                        style:{'margin-left':'30px','margin-top':'5px'},
                        anchor: '100%',
                        layoutConfig:
                        {
                            padding: '3',
                            pack: 'end',
                            align: 'middle'
                        },
                        items:
                        [
                            {
                                xtype: 'button',
                                text: 'Ok',
                                width: 70,
                                hideLabel: true,
                                id: 'btnOkDlgBukaLaporan',
                                disabled : true,
                                handler: function()
                                {

                                    if (Ext.getCmp('textfieldNoMedrec').getValue().length == 0) {
                                        ShowPesanWarningBukaTransaksi('Periksa kembali No Medrec Pasien', "Peringatan");
                                    }else if (Ext.getCmp('cboDataKunjungan').getValue().length == 0) {
                                        ShowPesanWarningBukaTransaksi('Periksa kembali tanggal kunjungan', "Peringatan");
                                    }else{

                                        var dlg = Ext.MessageBox.prompt('Password', 'Masukan password untuk menghapus :', function(btn, combo){
                                            if (btn == 'ok'){
                                                Ext.Ajax.request({
                                                     url         : baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
                                                     params      : {
                                                         select  : " COALESCE(count(*),0) as count ",
                                                         where   : " key_data = 'rwi_password_buka_transaksi' AND setting = md5('"+combo+"') ",
                                                         table   : " sys_setting ",
                                                     },
                                                     success: function(o){
                                                         var cst     = Ext.decode(o.responseText);
                                                         if (cst[0].count > 0) {
                                                            var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan buka transaksi:', function (btn, combo) {
                                                            if (btn == 'ok')
                                                            {
                                                                if (combo.length>0) {
                                                                    loadMask.show();
                                                                    Ext.Ajax.request({
                                                                        url: baseURL + "index.php/rawat_inap/control_buka_transaksi/buka_transaksi",
                                                                        params: {
                                                                            kd_kasir        : '02',
                                                                            kd_pasien       : Ext.getCmp('textfieldNoMedrec').getValue(),
                                                                            tgl_masuk       : Ext.getCmp('cboDataKunjungan').getValue(),
                                                                            alasan          : combo,
                                                                        },
                                                                        success: function(response) {
                                                                            var cst  = Ext.decode(response.responseText);
                                                                            if (cst.status === true) {
                                                                                ShowPesanInfoBukaTransaksi("Data berhasil diproses", "Information");
                                                                            }else{
                                                                                ShowPesanWarningBukaTransaksi("Data gagal diproses", "Information");
                                                                            }
                                                                            loadMask.hide();
                                                                        },
                                                                    });
                                                                }else{
                                                                    ShowPesanWarningBukaTransaksi('Alasan penghapusan harap diisi', "Peringatan");
                                                                }
                                                            }
                                                            });
                                                        }else{
                                                            ShowPesanInfoBukaTransaksi("Password salah", "Information");
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            },
                            {
                                xtype: 'button',
                                text: 'Cancel' ,
                                width: 70,
                                hideLabel: true,
                                id: 'btnCancelDlgBukaLaporan',
                                handler: function()
                                {
                                    frmDlgRWIDetailPasien.close();
                                }
                            }
                        ]
                    }
                ]        
        }
    )
    return FrmTabs_viInformasiUnitdokter;
}


function ShowPesanWarningBukaTransaksi(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
};

function ShowPesanInfoBukaTransaksi(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
};