var now = new Date();
var anow=now.format('Y-m-d');
var InfoPasienRWI={};
var dsunit_viInfoPasienRWI;
InfoPasienRWI.form={};
InfoPasienRWI.func={};
InfoPasienRWI.vars={};
InfoPasienRWI.func.parent=InfoPasienRWI;
InfoPasienRWI.form.DataStore={};
InfoPasienRWI.form.Grid={};
InfoPasienRWI.form.Panel={};
InfoPasienRWI.form.TextField={};
InfoPasienRWI.form.ComboBox={};
InfoPasienRWI.form.DateField={};
InfoPasienRWI.form.Button={};
InfoPasienRWI.form.DataStore.InfoPasien=new WebApp.DataStore({fields:['TANGGAL','JAM','KD_PASIEN','NAMA_PASIEN','TGL_MASUK','NAMA_UNIT','DOKTER','ALAMAT']});

InfoPasienRWI.func.getId=function(){
	$this=this.parent;
	if($this.vars.genNum == undefined)$this.vars.genNum=0;
	$this.vars.genNum+=1;
	return 'nci-InfoPasien-rj-'+$this.vars.genNum;
};
function mComboUnit_viInfoPasienRWI()
{
	var Field = ['KD_UNIT','NAMA_UNIT'];

    dsunit_viInfoPasienRWI = new WebApp.DataStore({ fields: Field });
    dsunit_viInfoPasienRWI.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				ort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ViewComboKelasKamarRWI',
                param: "" //+"~ )"
			}
		}
	);

    $this.form.ComboBox.txtSearch = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirRWIKasir',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Ruangan ',
		    align: 'Right',
			width: 60,
		    anchor:'100%',
		    store: dsunit_viInfoPasienRWI,
		    valueField: 'KD_UNIT',
		    displayField: 'NAMA_UNIT',
			value:'All',
		    listeners:
			{

			    'select': function(a, b, c)
				{
						  // RefreshDataFilterKasirRWIKasir();

			        //selectStatusCMKasirRWIView = b.data.DEPT_ID;
					//
			    }

			}
		}
	);

    return $this.form.ComboBox.txtSearch;
};
InfoPasienRWI.func.getInit=function(mod_id){
	$this=this.parent;
	$this.func.getInfoPasien();
	setInterval(function(){
		$this.func.getInfoPasien();
	},15000);
	
	
	var grid= new Ext.Panel({
        id			: $this.func.getId(),
        bodyStyle	: 'margin: 5px 5px;padding: 5px 0 0 5px;',
        layout		: 'column',
        border		: true,
        items		:[
			{
			    layout		: 'form',
			    labelWidth	: 150,
			    border		: false,
			    items		: [
         		   
				   /* {
						xtype: 'textfield',
						fieldLabel: 'Tanggal ',
						name: 'tglInfoPasienRWI',
						id	: 'tglInfoPasienRWI',
						format: 'd/M/Y',
						value: now,
						anchor: '95%',
						listeners	: {
         				   keyup	: function(a, b,c){
         					   if(b.getKey()==13){
								   //console.log(typeof $this.form.DateField.caritanggal.getValue());
         						  $this.func.getInfoPasien();
         					   }
         				   }
         			   }
                   },  */
				   $this.form.DateField.caritanggal=new Ext.form.DateField({
         			   id			: 'tglInfoPasienRWI',
         			   fieldLabel	: 'Tanggal',
         			   width		: 150,
					   format		: 'd/M/Y',
					   value: anow,
         			   enableKeyEvents: true,
         			   listeners	: {
         				   keyup	: function(a, b,c){
         					   if(b.getKey()==13){
         						  $this.func.getInfoPasien();
         					   }
         				   }
         			   }
         		   }),
					/* $this.form.ComboBox.txtSearch=new Ext.form.ComboBox({
         			   id			: $this.func.getId(),
         			   fieldLabel	: 'Poli',
         			   width		: 150,
         			   enableKeyEvents: true,
					   store: dsunit_viInfoPasienRWI,
					   valueField: 'KD_UNIT',
					   displayField: 'NAMA_UNIT',
					   value:'All',
         			   listeners	: {
         				   keyup	: function(a, b,c){
         					   if(b.getKey()==13){
								   console.log(typeof $this.form.ComboBox.txtSearch.getValue());
         						  $this.func.getInfoPasien();
         					   }
         				   }
         			   }
         		   }) */mComboUnit_viInfoPasienRWI(),				   
				   $this.form.Button.btnSearch= new Ext.Button({
					text	: 'Cari',
					id		: $this.func.getId(),
					tooltip	: nmLookup,
					iconCls	: 'find',
					handler	: function(){
						$this.func.getInfoPasien();
					}
				}),
			    ]
			},
			
			/* {
                layout		: 'form',
                border		: false,
                html		: ' &nbsp; *Table Auto Refresh.'
            } */
        ]
    });
	this.parent.form.Panel.InfoPasien= new Ext.Panel({
	    id			: mod_id,
	    closable	: true,
        layout		: 'form',
        title		: 'Info Pasien ',
        border		: false,
        shadhow		: true,
        iconCls		: 'Request',
        margins		: '0 5 5 0',
        autoScroll	: false,
	    items		:[
 		  	grid,
 		  	$this.func.getGridMain()
	    ]
	});
	return this.parent.form.Panel.InfoPasien;
};

InfoPasienRWI.func.getGridMain=function(){
	$this=this.parent;
    $this.form.Grid.InfoPasien	= new Ext.grid.EditorGridPanel({
        title		: 'Info Pasien',
		id			: $this.func.getId(),
		stripeRows	: true,
//		height		: 130,
        store		: $this.form.DataStore.InfoPasien,
        border		: false,
        frame		: false,
        anchor		: '100% 100%',
        autoScroll	: true,
        cm			: new Ext.grid.ColumnModel([
                  new Ext.grid.RowNumberer(),
            {
    			id			: $this.func.getId(),
            	header		: 'Tanggal',
                dataIndex	: 'TGL_MASUK',
                width		: 80,
    			menuDisabled: true,
                hidden		: false
            },/* {
    			id			: $this.func.getId(),
            	header		: 'Jam',
                dataIndex	: 'JAM',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            }, */{
            	id			: $this.func.getId(),
            	header		: 'Kode Pasien',
                dataIndex	: 'KD_PASIEN',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Nama Pasien',
                dataIndex	: 'NAMA_PASIEN',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Unit',
                dataIndex	: 'NAMA_UNIT',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Dokter',
                dataIndex	: 'DOKTER',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Alamat',
                dataIndex	: 'ALAMAT',
                width		: 150,
    			menuDisabled: true,
                hidden		: false
            }
       ]),
        viewConfig	: {forceFit: true}
    });
    
    return $this.form.Grid.InfoPasien;
};

InfoPasienRWI.func.getInfoPasien=function(tglmasuk){
	$this=this.parent;
	var txtSearch='';
	var cariTanggal=anow;
	if($this.form.ComboBox.txtSearch != undefined){
		txtSearch=$this.form.ComboBox.txtSearch.getValue();
	}
	if($this.form.DateField.caritanggal != undefined){
		cariTanggal=$this.form.DateField.caritanggal.getValue().format('Y-m-d');
		console.log(cariTanggal);
	}
	
	if (txtSearch === 'All' || txtSearch === '')
	{
		var criteria='tgl_masuk=~'+cariTanggal+'~ and left(kd_unit,1)=~'+1+'~';
	}
	else
	{
		var criteria='tgl_masuk=~'+cariTanggal+'~ and kd_unit=~'+txtSearch+'~';
	}	
	//console.log(Ext.get('tglInfoPasienRWI'));
	//console.log(anow);
	InfoPasienRWI.form.DataStore.InfoPasien.load({ 
		params: { 
			Skip: 0, 
			Take: 0, 
            Sort: 'kd_penyakit',
			Sortdir: 'ASC', 
			target:'ViewGridInfoPasienRWI',
			param: criteria
		} 
	});
	//
};

CurrentPage.page = InfoPasienRWI.func.getInit(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
console.log(InfoPasienRWI);