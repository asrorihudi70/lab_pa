var now = new Date();
var anow=now.format('Y-m-d');
var InfoPasienRWI={};
var dsunit_viInfoPasienRWI;
var dsKelasUnit_InfoPasienRWI;
var dsKamar_InfoPasienRWI;
var CurrentKdUnit_InfoPasienRWI;
var CurrentNoKamar_InfoPasienRWI;
var CurrentNama_InfoPasienRWI;
var CurrentAlamat_InfoPasienRWI;
var pagging = 100;
var order_by = "Nama Pasien";
var CurrentTglMasuk_InfoPasienRWI;
var GridDetailPasien_InfoPasienRWI;
var dsDataStoreGridKomponent = new Ext.data.JsonStore();
InfoPasienRWI.form={};
InfoPasienRWI.func={};
InfoPasienRWI.vars={};
InfoPasienRWI.func.parent=InfoPasienRWI;
InfoPasienRWI.form.DataStore={};
InfoPasienRWI.form.Grid={};
InfoPasienRWI.form.Panel={};
InfoPasienRWI.form.TextField={};
InfoPasienRWI.form.ComboBox={};
InfoPasienRWI.form.DateField={};
InfoPasienRWI.form.Button={};
dsInfoPasien_InfoPasienRWI=new WebApp.DataStore({fields:['TANGGAL','JAM','KD_PASIEN','NAMA_PASIEN','TGL_MASUK','NAMA_UNIT','DOKTER','ALAMAT','NO_TRANSAKSI','LOS']});


CurrentPage.page = getInitInfoPasienRWI(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
console.log(InfoPasienRWI);

InfoPasienRWI.func.getId=function(){
	//InfoPasienRWI=this.parent;
	if(InfoPasienRWI.vars.genNum == undefined)InfoPasienRWI.vars.genNum=0;
	InfoPasienRWI.vars.genNum+=1;
	return 'nci-InfoPasien-rj-'+InfoPasienRWI.vars.genNum;
};

function getInitInfoPasienRWI(mod_id){
	//InfoPasienRWI=this.parent;
	getInfoPasien();
	getBed_InfoPasienRWI();
	loadKelasUnit_InfoPasienRWI();
	/* setInterval(function(){
		getInfoPasien();
	},15000); */
	
	
	var pencarianPasien = new Ext.Panel({
        id			: 'PanelPencarianPasien_InfoPasienRWI',
        bodyStyle	: 'padding: 5px 5px 5px 5px;',
        layout		: 'column',
        border		: true,
        items		:[
			{
			    layout		: 'form',
			    labelWidth	: 70,
				//bodyStyle	: 'padding: 5px 5px 5px 5px;',
			    border		: false,
			    items		: [
					{
						xtype		: 'textfield',
						id			: 'Nama_InfoPasienRWI',
						fieldLabel	: 'Nama ',
						width		: 150,
						enableKeyEvents: true,
						listeners	: {
         				   keyup	: function(a, b,c){
								CurrentNama_InfoPasienRWI=b.data
								if(b.getKey()==13){
								  
								}
         				   },
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									getInfoPasien(Ext.getCmp('Nama_InfoPasienRWI').getValue(),Ext.getCmp('Alamat_InfoPasienRWI').getValue(),
													null,
													null,Ext.getCmp('cboKelasUnit_InfoPasienRWI').getValue(),Ext.getCmp('cboKamar_InfoPasienRWI').getValue());
								}
							}
         			   }
					},
					{
						xtype		: 'textfield',
						id			: 'Alamat_InfoPasienRWI',
						fieldLabel	: 'Alamat ',
						width		: 230,
						enableKeyEvents: true,
						listeners	: {
         				   keyup	: function(a, b,c){
         					   if(b.getKey()==13){
         						 
         					   }
         				   },
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									getInfoPasien(Ext.getCmp('Nama_InfoPasienRWI').getValue(),Ext.getCmp('Alamat_InfoPasienRWI').getValue(),
													null,
													null,Ext.getCmp('cboKelasUnit_InfoPasienRWI').getValue(),Ext.getCmp('cboKamar_InfoPasienRWI').getValue());
								}
							}
         			   }
					},
					{
						xtype			: 'datefield',
						id				: 'tglmasuk_InfoPasienRWI',
						fieldLabel		: 'Tgl masuk ',
						width			: 230,
						format			: 'd/M/Y',
						value			: anow,
						enableKeyEvents	: true,
						listeners		: {
							keyup		: function(a, b,c){
								if(b.getKey()==13){
									
								}
							},
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									getInfoPasien(Ext.getCmp('Nama_InfoPasienRWI').getValue(),Ext.getCmp('Alamat_InfoPasienRWI').getValue(),Ext.getCmp('tglmasuk_InfoPasienRWI').getValue(),
													Ext.getCmp('tglmasuk2_InfoPasienRWI').getValue(),Ext.getCmp('cboKelasUnit_InfoPasienRWI').getValue(),Ext.getCmp('cboKamar_InfoPasienRWI').getValue());
								}
							}
						}
					},
					{
						xtype			: 'checkbox',
						id				: 'FilterTanggal_InfoPasienRWI',
						fieldLabel		: 'Filter Tanggal',
						width			: 230,
						hidden 			: true,
					},
				]
			},
			{
			    layout		: 'form',
			    labelWidth	: 60,
				bodyStyle	: 'padding: 0px 0px 0px 10px;',
			    border		: false,
			    items		: [
					ComboKelasUnit_InfoPasienRWI(),
					ComboKamar_InfoPasienRWI(),
					{
						xtype			: 'datefield',
						id				: 'tglmasuk2_InfoPasienRWI',
						fieldLabel		: 's/d ',
						width			: 230,
						format			: 'd/M/Y',
						value			: anow,
						enableKeyEvents	: true,
						listeners		: {
							keyup		: function(a, b,c){
								if(b.getKey()==13){
									
								}
							},
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									getInfoPasien(Ext.getCmp('Nama_InfoPasienRWI').getValue(),Ext.getCmp('Alamat_InfoPasienRWI').getValue(),Ext.getCmp('tglmasuk_InfoPasienRWI').getValue(),
													Ext.getCmp('tglmasuk2_InfoPasienRWI').getValue(),Ext.getCmp('cboKelasUnit_InfoPasienRWI').getValue(),Ext.getCmp('cboKamar_InfoPasienRWI').getValue());
								}
							}
						}
					},
			    ]
			},
			{
			    layout		: 'form',
			    labelWidth	: 70,
				bodyStyle	: 'padding: 0px 0px 0px 10px;',
			    border		: false,
			    items		: [			   
					{
						xtype		: 'button',
						text	: 'Load',
						id		: 'btnCari_infoPasienRWI',
						tooltip	: nmLookup,
						width	: 50,
						iconCls	: 'refresh',
						handler	: function(){
							getInfoPasien(Ext.getCmp('Nama_InfoPasienRWI').getValue(),Ext.getCmp('Alamat_InfoPasienRWI').getValue(),null,
													null,Ext.getCmp('cboKelasUnit_InfoPasienRWI').getValue(),Ext.getCmp('cboKamar_InfoPasienRWI').getValue());
						}
					},

					{
                		xtype	: 'button',	
               			id      :'btnLookUpInfoKamar',
               			tooltip	: nmLookup,
		                text    : 'Info Kamar',
		                iconCls : '',
		                handler : function(){
		              		     // dsDataStoreGridPoduk.removeAll();
		           		         formLookupInfoKamarRWI();
		              	  }
		         	},

					{
						xtype		: 'button',
						text	: 'Print',
						id		: 'btnPrint_infoPasienRWI',
						tooltip	: nmLookup,
						width	: 50,
						iconCls	: 'print',
						handler	: function(){
								var params={
									nama:Ext.getCmp('Nama_InfoPasienRWI').getValue(),
									alamat:Ext.getCmp('Alamat_InfoPasienRWI').getValue(),
									tgl_masuk:Ext.getCmp('tglmasuk_InfoPasienRWI').getValue(),
									tgl_masuk2:Ext.getCmp('tglmasuk2_InfoPasienRWI').getValue(),
									kd_unit:Ext.getCmp('cboKelasUnit_InfoPasienRWI').getValue(),
									kelas:Ext.get('cboKelasUnit_InfoPasienRWI').getValue(),
									no_kamar:Ext.getCmp('cboKamar_InfoPasienRWI').getValue(),
									nama_kamar:Ext.get('cboKamar_InfoPasienRWI').getValue(),
									
								} 
								var form = document.createElement("form");
								form.setAttribute("method", "post");
								form.setAttribute("target", "_blank");
								form.setAttribute("action", baseURL + "index.php/rawat_inap/viewgridinfopasienrwi/cetak");
								var hiddenField = document.createElement("input");
								hiddenField.setAttribute("type", "hidden");
								hiddenField.setAttribute("name", "data");
								hiddenField.setAttribute("value", Ext.encode(params));
								form.appendChild(hiddenField);
								document.body.appendChild(form);
								form.submit();		
						}
					},
			    ]
			},
			{
			    layout		: 'form',
			    labelWidth	: 70,
				bodyStyle	: 'padding: 0px 0px 0px 20px;',
			    border		: false,
			    items		: [
					{
						xtype		: 'textfield',
						id			: 'JumlahBed_InfoPasienRWI',
						fieldLabel	: 'Jumlah bed ',
						width		: 40,
						readOnly	: true,
						style		:{'text-align':'center'}	
					},
					{
						xtype		: 'textfield',
						id			: 'SisaBed_InfoPasienRWI',
						fieldLabel	: 'Sisa bed ',
						width		: 40,
						style		:{'text-align':'center'},	
						readOnly	: true,
					},
				]
			},
        ]
    });
	var LegendView_InfoPasienRWI = new Ext.Panel
	(
		{
            id: 'LegendView_InfoPasienRWI',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            anchor: '100% 8.0001%',
            autoScroll:false,
            items:
            [
                {
                    columnWidth: .025,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/abu.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Belum ada transaksi"
                },
				{
                    columnWidth: .025,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/biru.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " 0 Hari"
                },
				 {
                    columnWidth: .025,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " 1 Hari"
                },
                {
                    columnWidth: .025,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    border: false,
                    //height:35,
                    anchor: '100% 8.0001%',
                    //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/kuning.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " 2 Hari"
                },
				{
                    columnWidth: .025,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " > = 3 Hari"
                },
				{
                    columnWidth: .025,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/pink.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Tgl sekarang < Tgl trans"
                },
				{
					columnWidth: .2,
					layout		: 'form',
					border		: false,
					bodyStyle	: 'padding: 0px 0px 0px 10px;',
					style		:{'margin-bottom':'10px',color : 'darkblue'},
					// html		: '&nbsp; * Max data ditampilkan 100.'
				}
            ]

        }
    )
	PanelDasarInfoPasien_InfoPasienRWI= new Ext.Panel({
	    id			: mod_id,
	    closable	: true,
        layout		: 'form',
        title		: 'Info Pasien ',
        border		: false,
        shadhow		: true,
        iconCls		: 'Request',
		bodyStyle	: 'padding: 5px 5px 5px 5px;',
        margins		: '5 5 5 5',
        autoScroll	: false,
	    items		:[
 		  	pencarianPasien,
 		  	getGridMain_InfoPasienRWI(),
			LegendView_InfoPasienRWI
	    ]
	});
	return PanelDasarInfoPasien_InfoPasienRWI;
};

function getGridMain_InfoPasienRWI(){
	GridDetailPasien_InfoPasienRWI	= new Ext.grid.EditorGridPanel({
        title		: 'Info Pasien Rawat Inap',
		id			: 'gridDetailPasien_InfoPasienRWI',
		stripeRows	: true,
//		height		: 130,
        store		: dsInfoPasien_InfoPasienRWI,
        border		: true,
        frame		: false,
        anchor		: '100% 75%',
        autoScroll	: true,
		plugins 	: [new Ext.ux.grid.FilterRow()],
        colModel 	: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				id: 'colKeterangan_InfoPasienRWI',
				header: 'Ket.',
				dataIndex: 'KETERANGAN',
				menuDisabled : true,
				width: 50,
				align:'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store)
				{
					
					 switch (value)
					 { 
						case 'Belum Ada Detil Transaksi':
							metaData.css = 'StatusAbu';
							break;
						case '0hari':
							metaData.css = 'StatusBiru';
							break;
						case '1hari':
							metaData.css = 'StatusHijau';
							break;
						case '2hari':
							metaData.css = 'StatusKuning'; 
							break;
						case '>=3hari':
							metaData.css = 'StatusMerah';
							break;
						case 'pink':
							metaData.css = 'StatusPink';
							break;
					 }
					
					 return '';
				}
			},
			{
            	id			: 'colLmDirawat_infoPasienRWI',
            	header		: 'LOS',
                dataIndex	: 'LOS',
				sortable 	: true,
                width		: 30,
                hidden		: false,
            },
            {
            	id			: 'colKdPasien_infoPasienRWI',
            	header		: 'Kode Pasien',
                dataIndex	: 'KD_PASIEN',
				sortable 	: true,
                width		: 70,
                hidden		: false,
            },
            {
            	id			: 'colNoTransaksi_infoPasienRWI',
            	header		: 'No Transaksi',
                dataIndex	: 'NO_TRANSAKSI',
				sortable 	: true,
                width		: 70,
                hidden		: false,
            },{
            	id			: 'colNamaPasien_infoPasienRWI',
            	header		: 'Nama Pasien',
                dataIndex	: 'NAMA_PASIEN',
                width		: 150,
                hidden		: false,
            },{
            	id			: 'colJK_infoPasienRWI',
            	header		: 'JK',
                dataIndex	: 'JK',
                width		: 30,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: 'colUsia_infoPasienRWI',
            	header		: 'Usia',
                dataIndex	: 'USIA',
                width		: 50,
                hidden		: false,
            },{
            	id			: 'colAlamat_infoPasienRWI',
            	header		: 'Alamat',
                dataIndex	: 'ALAMAT',
                width		: 230,
                hidden		: false
            },{
    			id			: 'colTglMasuk_infoPasienRWI',
            	header		: 'Tanggal Masuk',
                dataIndex	: 'TGL_MASUK',
                width		: 80,
                hidden		: false
            },{
            	id			: 'colKelas_infoPasienRWI',
            	header		: 'Kelas',
                dataIndex	: 'NAMA_UNIT',
                width		: 80,
                hidden		: true,
            },{
            	id			: 'colKamar_infoPasienRWI',
            	header		: 'Kamar',
                dataIndex	: 'NAMA_KAMAR',
                width		: 120,
                hidden		: false,
            },
			
		]),
		tbar : [
			ComboOrderBy_InfoPasienRWI(),
		],
		bbar: bbar_pagingPendaftaran("InformasiPasien", 100, dsInfoPasien_InfoPasienRWI),
		viewConfig	: {forceFit: true}
    });
    
    return GridDetailPasien_InfoPasienRWI;
};

function getInfoPasien(nama,alamat,tgl_masuk,tgl_masuk2,kd_unit,no_kamar){

	/* var txtSearch='';
	var cariTanggal=anow;
	if(InfoPasienRWI.form.ComboBox.txtSearch != undefined){
		txtSearch=InfoPasienRWI.form.ComboBox.txtSearch.getValue();
	}
	if(InfoPasienRWI.form.DateField.caritanggal != undefined){
		cariTanggal=InfoPasienRWI.form.DateField.caritanggal.getValue().format('Y-m-d');
		console.log(cariTanggal);
	}
	
	if (txtSearch === 'All' || txtSearch === '')
	{
		var criteria='tgl_masuk=~'+cariTanggal+'~ and left(kd_unit,1)=~'+1+'~';
	}
	else
	{
		var criteria='tgl_masuk=~'+cariTanggal+'~ and kd_unit=~'+txtSearch+'~';
	}	
	//console.log(Ext.get('tglInfoPasienRWI'));
	//console.log(anow);
	dsInfoPasien_InfoPasienRWI.load({ 
		params: { 
			Skip: 0, 
			Take: 0, 
            Sort: 'kd_penyakit',
			Sortdir: 'ASC', 
			target:'ViewGridInfoPasienRWI',
			param: criteria
		} 
	}); */
	
	var params ={
		nama:nama,
		alamat:alamat,
		tgl_masuk:tgl_masuk,
		tgl_masuk2:tgl_masuk2,
		kd_unit:kd_unit,
		no_kamar:no_kamar,
		pagging:pagging,
		order_by:order_by,
	}
	
	Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_inap/viewgridinfopasienrwi/read",
		params: params,
		failure: function(o)
		{
			ShowPesanErrorInfoPasienRWI('Hubungi Admin!', 'Error');
		},	
		success: function(o) 
		{   
			dsInfoPasien_InfoPasienRWI.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsInfoPasien_InfoPasienRWI.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsInfoPasien_InfoPasienRWI.add(recs);
				GridDetailPasien_InfoPasienRWI.getView().refresh();
				getBed_InfoPasienRWI(kd_unit,no_kamar);
			} 
			else 
			{
				ShowPesanErrorInfoPasienRWI('Gagal membaca data pasien rawat inap', 'Error');
			};
		}
	});
};

function getBed_InfoPasienRWI(kd_unit,no_kamar){
	
	Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_inap/viewgridinfopasienrwi/getbed",
		params: {
			kd_unit:kd_unit,
			no_kamar:no_kamar
			
		},
		failure: function(o)
		{
			ShowPesanErrorInfoPasienRWI('Hubungi Admin!', 'Error');
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				Ext.getCmp('JumlahBed_InfoPasienRWI').setValue(cst.jml);
				Ext.getCmp('SisaBed_InfoPasienRWI').setValue(cst.sisa);
			} 
			else 
			{
				ShowPesanErrorInfoPasienRWI('Gagal membaca data pasien rawat inap', 'Error');
			};
		}
	});
};

function loadKelasUnit_InfoPasienRWI(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_inap/viewgridinfopasienrwi/getunitkelas",
		params: {
			text:param,
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			dsKelasUnit_InfoPasienRWI.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsKelasUnit_InfoPasienRWI.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsKelasUnit_InfoPasienRWI.add(recs);
				console.log(o);
			}
		}
	});
};

function ComboKelasUnit_InfoPasienRWI(){
	dsKelasUnit_InfoPasienRWI = new WebApp.DataStore({fields: ['kd_unit','nama_unit']})
	loadKelasUnit_InfoPasienRWI();
	cboKelasUnit_InfoPasienRWI = new Ext.form.ComboBox
	(
		{
			id: 'cboKelasUnit_InfoPasienRWI',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			fieldLabel: 'Kelas ',
			store: dsKelasUnit_InfoPasienRWI,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			width:150,
			listeners:
			{
				'select': function(a, b, c)
				{
					Ext.getCmp('cboKamar_InfoPasienRWI').setValue('');
					loadKamar_InfoPasienRWI(b.data.kd_unit);
					CurrentKdUnit_InfoPasienRWI=b.data.kd_unit;
				}
			}
		}
	)
	return cboKelasUnit_InfoPasienRWI;
} 

function ComboOrderBy_InfoPasienRWI(){
	var cboOrderBy_InfoPasienRWI = new Ext.form.ComboBox
	(
		{
			id: 'cboOrderBy_InfoPasienRWI',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			fieldLabel: 'Order By ',
			emptyText: 'Order By',
			store   : new Ext.data.ArrayStore({
				id: 0,
				fields:
						[
							'Id',
							'displayText'
						],
				data: [
					[1, 'Kode Pasien'], 
					[2, 'Nama Pasien'], 
					[3, 'Jenis Kelamin'],
					[4, 'Usia'],
					[5, 'Alamat'],
					[6, 'Tanggal Masuk'],
					[7, 'Kamar'],
					[8, 'Keterangan'],
				]
			}),
			valueField: 'Id',
			displayField: 'displayText',
			width:150,
			// value 	: order_by,
			listeners:
			{
				'select': function(a, b, c)
				{
					order_by = b.data.displayText;
					getInfoPasien(
						Ext.getCmp('Nama_InfoPasienRWI').getValue(),
					    Ext.getCmp('Alamat_InfoPasienRWI').getValue(),
						null,
						null,
						Ext.getCmp('cboKelasUnit_InfoPasienRWI').getValue(),
						Ext.getCmp('cboKamar_InfoPasienRWI').getValue()
					);
				}
			}
		}
	)
	return cboOrderBy_InfoPasienRWI;
} 

function loadKamar_InfoPasienRWI(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_inap/viewgridinfopasienrwi/getkamar",
		params: {
			text:param,
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			dsKamar_InfoPasienRWI.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsKamar_InfoPasienRWI.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsKamar_InfoPasienRWI.add(recs);
				console.log(o);
			}
		}
	});
};

function ComboKamar_InfoPasienRWI(){
	dsKamar_InfoPasienRWI = new WebApp.DataStore({fields: ['no_kamar','nama_kamar']})
	loadKamar_InfoPasienRWI();
	cboKamar_InfoPasienRWI = new Ext.form.ComboBox
	(
		{
			id: 'cboKamar_InfoPasienRWI',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			fieldLabel: 'Kamar ',
			store: dsKamar_InfoPasienRWI,
			valueField: 'no_kamar',
			displayField: 'nama_kamar',
			width:150,
			listeners:
			{
				'select': function(a, b, c)
				{
					CurrentNoKamar_InfoPasienRWI=b.data.no_kamar;
				}
			}
		}
	)
	return cboKamar_InfoPasienRWI;
} 


function ShowPesanErrorInfoPasienRWI(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function formLookupInfoKamarRWI(){    
        var paneltotal = new Ext.Panel
        (
            {
                id: 'paneltotal',
                region: 'center',
                border: true,
                bodyStyle: 'padding:0px 0px 0px 0px',
                layout: 'column',
                frame: true,
                height: 40,
                width: 600,
                autoScroll: false,
                items:
                [
                    {
                        xtype: 'compositefield',
                        anchor: '100%',
                        labelSeparator: '',
                        border: true,
                        style: {'margin-top': '3px'},
                        items:
                        [
                            {
                                layout: 'form',
                                style: {'text-align': 'right', 'margin-left': '90px'},
                                border: false,
                                html: " Total :"
                            },
                            {
                                xtype: 'textfield',
                                id: 'txtJumlah2EditData_viKasirrwi',
                                name: 'txtJumlah2EditData_viKasirrwi',
                                style: {'text-align': 'right', 'margin-left': '95px'},
                                width: 82,
                                readOnly: true,
                            },
                                    //-------------- ## --------------
                        ]
                    }
                ]

            }
        );
        
        var formLookupInfoKamarRWI = new Ext.Window
		(
			{
				id: 'gridInfoKamarRWI',
				title: 'Informasi Kamar',
				closeAction: 'destroy',
				width: 600,
				height: 400,
				border: false,
				resizable: false,
				plain: true,
				layout: 'fit',
				iconCls: 'Request',
				modal: true,
				bodyStyle:'padding: 3px;',
				items: [
                {
					columnWidth: .40,
					layout: 'form',
					labelWidth: 100,
					border: false,
					bodyStyle:'padding: 10px;',
					items:[
						gridInfoKamarRWI(),
                        
					]
				},
				],
				bbar:[                            
            
				],
                tbar:[
                           
                ],
				listeners:
				{
				
				}
            }
		);

        formLookupInfoKamarRWI.show();
    };

    function gridInfoKamarRWI(){
      //  dsDataStoreGridKomponent.removeAll();

        
	    var Field = ['kd_kamar','nama_kamar','keterangan'];
	    DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
    	loaddatastoreProdukComponent();

    var cm =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        /*
			{
				header			: 'KD PRODUK',
				dataIndex		: 'kd_produk',
				width			: 25,
				menuDisabled	: true,
				hidden 			: true
			},*/
	    	        {
	    	            header: 'No',
	    	            dataIndex: 'no_kamar',
	    	            width: 40,
	    	            hidden : true,
	    				filter: {},
	    	        },
	    	        {
	    	            header: 'Nama Kamar',
	    	            dataIndex: 'nama_kamar',
	    	            width: 160,
	    				filter: {},
	    	        }, 
	    	        {
	    	            header: 'Keterangan',
	    	            dataIndex: 'keterangan',
	    	            width: 120,
	    	            hidden : false,
	    				filter: {},
	    	        }, 
	    	        {
	    	            header: 'Sisa',
	    	            dataIndex: 'sisa',
	    	            width: 60,
	    	        },
	    	        {
	    	            header: 'Jumlah',
	    	            dataIndex: 'jumlah',
	    	            hidden : false,
	    	            width: 60,
	    	        }, 
        ]
    );

        var gridInfoKamarRWI = new Ext.grid.EditorGridPanel
        (
            {
                title: '',
                id: 'gridInfoKamarrwi',
                store: dsDataStoreGridKomponent,
                clicksToEdit: 1, 
				plugins 	: [new Ext.ux.grid.FilterRow()],
                editable: true,
                border: true,
                columnLines: true,
                frame: false,
                stripeRows       : true,
                trackMouseOver   : true,
                height: 300,
                width: 590,
                autoScroll: true,
                sm: new Ext.grid.CellSelectionModel
                (
                    {
                        singleSelect: true,
                        listeners:
                        {
                        }
                    }
                ),
                cm: cm,
                viewConfig: {
                    forceFit: true
                },  
            }
        );
        return gridInfoKamarRWI;
    };

    function loaddatastoreProdukComponent(){
    	dsDataStoreGridKomponent.removeAll();
        Ext.Ajax.request({
            url: baseURL +  "index.php/rawat_inap/viewgridinfopasienrwi/listKamar",
            params: {
                null : null,
            },
            success: function(response) {
                //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
                var cst  = Ext.decode(response.responseText);
                //dsDataPerawatPenindak_KASIR_RWI.load(myData);
                for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
                    var recs    = [],recType = DataStoreGridKomponent.recordType;
                    var o       = cst['ListDataObj'][i];
                    recs.push(new recType(o));
                    dsDataStoreGridKomponent.add(recs);
                }
            },
        });
    }

function bbar_pagingPendaftaran(mod_name, count, source)
{
    // Deklarasi Variabel pada bbar_paging # --------------
    var combo_mod_name = "combo_"+mod_name;
    var count_mod_name = "count_"+mod_name;
    var source_mod_name = "source_"+mod_name;
    var bbar_mod_name = "bbar_"+mod_name;
    var count_mod_name = count;
    var source_mod_name = source;
    // End Deklarasi Variabel pada bbar_paging # --------------

    // Pengaturan Bar Paging Bawah # --------------

    // Combo Per Halaman # --------------
    var combo_mod_name = new Ext.form.ComboBox
    (
        {
            id      : 'perpage_bbar_paging',
            name    : 'perpage_bbar_paging',
            width   : 40,
            store   : new Ext.data.ArrayStore
            (
                {
                    fields: ['id'],
                    data  : [
							['25'],
							['50'],
							['100'],
							['200'],
							['300'],
							['400'],
							['500'],
							['600'],
                    ]
                }
            ),
			mode : 'local',
			value: count_mod_name,
			
			listWidth     : 40,
			triggerAction : 'all',
			displayField  : 'id',
			valueField    : 'id',
			editable      : false,
			forceSelection: true,
			listeners       :{
				select: function(a,b,c){
						pagging = Ext.getCmp('perpage_bbar_paging').getValue();
							getInfoPasien(
								Ext.getCmp('Nama_InfoPasienRWI').getValue(),
								Ext.getCmp('Alamat_InfoPasienRWI').getValue(),
								null,
								null,
								Ext.getCmp('cboKelasUnit_InfoPasienRWI').getValue(),
								Ext.getCmp('cboKamar_InfoPasienRWI').getValue()
							);
				}
			}
        }
    );
    // End Combo Per Halaman # --------------

    // ExtJS Paging # --------------
    var bbar_mod_name = new WebApp.PaggingBar
    (
        {
            //store: source,
            displayInfo: false,
            //pageSize: count_mod_name,
            //displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;",
            items : [
                        '-',
                        'Per Halaman: ',
                        combo_mod_name
                    ]
        }
    );
    // End ExtJS Paging # --------------
    
    // Event refresh tampilan grid sesuai jumlah saat dipilih pada combobox  # --------------    
    /*combo_mod_name.on('select', function(combo_mod_name, record_mod_name) 
        {
            bbar_mod_name.pageSize = parseInt(record_mod_name.get('id'), 10);
            bbar_mod_name.doLoad(bbar_mod_name.cursor);
        }, 
    this
    );*/
    // End Event refresh tampilan grid sesuai jumlah saat dipilih pada combobox  # --------------    

    // End Pengaturan Bar Paging Bawah # --------------
    return bbar_mod_name;
}
