var kdUnit = '1';

var CurrentKasirRwi ={
	data: Object,
	details: Array,
	row: 0
};
var CurrentHistory_rwi ={
	data: Object,
	details: Array,
	row: 0
};
var status_customer = false;
var statusPulang = null;
var statusCaraKeluar = null;
var rowIndex = 0;
var tmp_data_inap = "";
var storeProduk;
var cek_rawat_gabung=false;
var dsGridJasaDokterPenindak_RWI;
var dsDataSource_kunjungan = new Ext.data.JsonStore();
var currentJasaDokterKdTarif_RWI;
var tanggaltransaksitampung_utama;
var currentJasaDokterKdProduk_RWI;
var currentJasaDokterUrutDetailTransaksi_RWI;
var currentJasaDokterHargaJP_RWI;
var tgl_isian='n';
var actKamar;
var jeniscus;
var batal_isi_tindakan='n';
var dsgridpilihdokterpenindak_RWI;
var kodetarifnya;
var cellSelecteddeskripsi_kasir_rwi;
var nilai_kd_tarif;
var kdkasirnya;
var vkode_customer_PJRWI;
var vnama_customer_PJRWI;
// ----selsih-------
var cell_select_selisih;
var Row_viKasirSelisih;
// -----------------
var vKdDokter;
var syssetting_rwi = 'igd_default_klas_produk';
var mRecordRwi = Ext.data.Record.create([
	{name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
	{name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
	{name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
	{name: 'KD_TARIF', mapping: 'KD_TARIF'},
	{name: 'HARGA', mapping: 'HARGA'},
	{name: 'QTY', mapping: 'QTY'},
	{name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
	{name: 'DESC_REQ', mapping: 'DESC_REQ'},
	{name: 'KD_UNIT', mapping: 'KD_UNIT'},
	{name: 'KD_UNIT', mapping: 'KD_UNIT'},
	//{name: 'KD_TARIF', mapping:'KD_TARIF'},
	{name: 'URUT', mapping: 'URUT'}
]);
var tittle_trdok;
var vKdPasien;
var nmDokDiAwal;
var tapungalasan = '01';
var PenataJasaKasirRWI = {};
var Current_kasirrwi = {
    data: Object,
    details: Array,
    row: 0
};
var griddetailtrdokter_kasirrwi;
var varkd_tarif_rwi;
PenataJasaKasirRWI.form = {};
PenataJasaKasirRWI.form.Class = {};
PenataJasaKasirRWI.form.DataStore = {};
PenataJasaKasirRWI.dsGridTindakan;
PenataJasaKasirRWI.form.ComboBox = {};
PenataJasaKasirRWI.form.Grid = {};
PenataJasaKasirRWI.func = {};
PenataJasaKasirRWI.form.DataStore.produk = new Ext.data.ArrayStore({id: 0, fields: ['kd_produk', 'deskripsi', 'harga', 'tglberlaku','"kd_tarif'], data: []});
PenataJasaKasirRWI.form.DataStore.trdokter = new Ext.data.ArrayStore({id: 0, fields: ['kd_dokter', 'nama'], data: []});
PenataJasaKasirRWI.form.DataStore.dokter_inap_int = new Ext.data.ArrayStore({id: 0, fields: ['kd_job', 'label'], data: []});
var dsStatusPulangPasien;
dsStatusPulangPasien = new WebApp.DataStore({fields: ['KD_STATUS_PULANG','STATUS_PULANG']});
var dsCaraKeluar;
dsCaraKeluar = new WebApp.DataStore({fields: ['kd_cara_keluar','cara_keluar']});
var rowpasien;
var KDunitkamar;
var TGLNGINAP;
var kdokter_btl;
var tampungtypedata;
var tmp_group_dokter = 0;
var tmpkdkasirrwi;
var variablebatalhistori_rwi;
var tanggaltransaksitampung;
var urutmasuk;
var mRecordKasirrwi = Ext.data.Record.create([
	{name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
	{name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
	{name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
	{name: 'KD_TARIF', mapping: 'KD_TARIF'},
	{name: 'HARGA', mapping: 'HARGA'},
	{name: 'QTY', mapping: 'QTY'},
	{name: 'KD_UNIT', mapping: 'KD_UNIT'},
	{name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
	{name: 'DESC_REQ', mapping: 'DESC_REQ'},
	{name: 'URUT', mapping: 'URUT'}
]);
var Kdtransaksi;
var tapungkd_pay;
var dsTRDetailHistoryList;
var AddNewHistory = true;
var selectCountHistory = 50;
var now = new Date();
var rowSelectedHistory;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRHistory;
var cellSelectedtutup_kasirrwi;
var vkd_unit;
var CurrentKDUnitRWI;
var nowTglTransaksiRWI = now.format('d-M-Y');
var CurrentKDCustomerRWI;
var DataProdukRWI;
var dsprinter_kasirrwi;
var CurrentDataRWI;
var winRWIPasswordDulu;
var tampungshiftsekarang;
var labelisi;
var jenispay;
var variablehistori;
var selectCountStatusByr_viKasirrwiKasir = 'Semua';
var dsTRKasirrwiKasirList;
var dsHistoryKasirrwiKasirList;
var dsTRDetailKasirrwiKasirList;
var dsSelisihDetailKasirrwiKasirList;
var kdUnitSelisih;
var kdKelasSelisih;
var fldDetailSelisih = ['KD_PRODUK','URUT','TGL_TRANSAKSI','FAKTUR','KELAS','KD_TARIF','DESKRIPSI','QTY','HARGA','JATAH','SELISIH','TGL_BERLAKU'];
dsSelisihDetailKasirrwiKasirList = new WebApp.DataStore({fields: fldDetailSelisih});
var AddNewKasirrwiKasir = true;
var selectCountKasirrwiKasir = 50;
var now = new Date();
var rowSelectedKasirrwiKasir;
var dsget_data_kamarrwirwi;
var FormLookUpsdetailTRKasirrwi;
var valueStatusCMKasirrwiView = 'All';
var nowTglTransaksi = new Date();

var nowTglTransaksi2 = nowTglTransaksi.format("d-M-Y");
var nowTglTransaksi3 = nowTglTransaksi.format("Y-m-d");
var dsComboBayar;
var vkode_customer;
var vflag;
var gridDTLTRKasirrwinap;
var vUrutMasuk;

var currentJasaDokterUrutDetailTransaksi_KasirRWI;
var currentJasaDokterKdProduk_KasirRWI;
var currentJasaDokterKdTarif_KasirRWI;
var currentJasaDokterHargaJP_KasirRWI;
var currentNoTransaksi_KasirRWI;
var currentTglTransaksi_KasirRWI;
var currentKdKasir_KasirRWI;
var dsGridJasaDokterPenindak_KasirRWI;
var currentKDUnit_KasirRWI;
var currentJumlahSudahDibayarKasirRWI;
var currentTotalTagihanKasirRWI;
var currentSisaTagihanKasirRWI;
var dsRekapitulasiTindakanKasirRWI;
var GridRekapitulasiTindakanKasirRWI;

dsgridpilihdokterpenindak_KasirRWI	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_dokter','nama'],
	data: []
});

var kodekasir;
var notransaksi;
var tgltrans;
var kodepasien;
var namapasien;
var kodeunit;
var namaunit;
var kodepay;
var kdcustomeraigd;
var uraianpay;
var tmpKdCustomer;
var grListTRKasirrwi;

/*
    UPDATE RAWAT INAP PENAMBAHAN VARIABLE
    OLEH    : HADAD AL GOJALI
    TANGGAL : 2017 - 01 - 08
    ALASAN  : DITAMBAHKAN SEBAGAI MEDIA PENYIMPANAN NILAI PRODUK
 */
var gridEditTarifKasirrwi;
var gridEditCitoKasirrwi;
var recordterakhir;
var statusHitung = false;
var arrayTmp = [];
var arrayTmpBiaya = [];
var limaharilalu = new Date().add(Date.DAY, -30);
var dataRowIndex         = 0;
var dataRowIndexDetail   = 0;
var tmpkd_unit           = "";
var tmpkd_unitKamar   	 = "";
var tmpkp_produk         = "";
var tmpkd_tarif          = "";
var tmpdeskripsi         = "";
var tmpharga             = "";
var tmptgl_berlaku       = "";
var tmptgl_transaksi     = "";
var tmptgl_transaksiInduk= "";
var tmpjumlah            = "";
var tmpRowGrid           = "";
var tmpKelas             = "";
var tmpUnit              = "";
var tmpNoFaktur 		 = "";
var tmpKdUnitKamar       = "";
var tmpKamar             = "";
var tmpKdSpesial         = "";
var tmpNamaDokter        = "";
var tmpstatus_konsultasi = "";
var tmpno_transaksi      = "";
var tmpTanggalMasuk      = "";
var tmpKdDokter          = "";
var tmpNamaDokter        = "";
var tmpKeterangan        = "";
var tmpKD_unit_tr        = "";
var tmp_pasien_transfer;
var tmp_jumlah_bayar     = 0;
var tmp_sisa_bayar     	 = 0;
var tmp_total_tagihan 	 = 0;
var tmpKdJob             = 1;
var dsDokterRequestEntry;
var jPanel = 0;
var DefJamPerpindahan    = now.format('H:i:s');
var tmpNominal 	= 0;
var KdPasien    = "";
var TglMasuk    = "";
var KdUnit      = "";
var UrutMasuk   = "";
var KdDokter    = "";
var NoTransaksi = "";
var KdKasir     = "";
var KdUnitDulu  = "";
var tmpKdUser   = "";
var KDCustomer  = "";
var TmpUrl  	= "";
var dsDataPerawatPenindak_KASIR_RWI;
var dsDataDokterPenindak_KASIR_RWI;
var dsDataDaftarPenangan_KASIR_RWI;
var dsDataDokterPenindak_KASIR_RWI;
var DataStorefirstGridStore  = new Ext.data.JsonStore();
var DataStoreGridPoduk;
var dsDataStoreGridPoduk          = new Ext.data.JsonStore();
var dsDataStoreGridKomponent      = new Ext.data.JsonStore();
var dsDataStoreGridTarifKomponent = new Ext.data.JsonStore();
var dsDataStoreGridHistoryPindah  = new Ext.data.JsonStore();
var DataStoreGridKomponent;
var DataStoreGridHistoryPindah;
var DataStoreSecondGridStore = new Ext.data.JsonStore();

CurrentPage.page = getPanelKasirrwi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var Field = ['KD_DOKTER','NAMA'];
dsDokterRequestEntry = new WebApp.DataStore({fields: Field});

var fieldsDokterPenindak = [
	{name: 'KD_DOKTER', mapping : 'KD_DOKTER'},
	{name: 'NAMA', mapping : 'NAMA'}
];
dsDataDokterPenindak_KASIR_RWI = new WebApp.DataStore({ fields: fieldsDokterPenindak });

function getPanelKasirrwi(mod_id){
    var Field = ['HAK_KELAS','KELAS_RAWAT','PINDAH_KELAS','NO_SJP','KD_DOKTER', 'NO_TRANSAKSI', 'KD_UNIT','KD_UNIT_KAMAR', 'KD_PASIEN', 'NAMA', 'NAMA_UNIT', 'ALAMAT',
        'TANGGAL_TRANSAKSI', 'TANGGAL_MASUK', 'JAM_MASUK', 'NAMA_DOKTER', 'KD_CUSTOMER', 'CUSTOMER', 'URUT_MASUK', 'FLAG', 'LUNAS','KD_TARIF', 'KET_PAYMENT', 'CARA_BAYAR',
        'JENIS_PAY', 'KD_PAY', 'KETERANGAN', 'POSTING', 'TYPE_DATA', 'CO_STATUS', 'NAMA_KAMAR', 'KELAS', 'KD_SPESIAL', 'SPESIALISASI', 'KD_KELAS', 'NO_KAMAR', 'KD_UNIT_KAMAR', 'TGL_NGINAP',
        'KD_KASIR','PARENTNYA','BBL','RAWAT_GABUNG'];
    dsTRKasirrwiKasirList = new WebApp.DataStore({fields: Field});
    DataProdukRWI = new WebApp.DataStore({fields: ['KD_PRODUK', 'KD_KLAS', 'DESKRIPSI', 'TARIF', 'TGL_BERLAKU', 'KD_TARIF', 'KD_UNIT']});
    //refeshKasirrwiKasir();
    grListTRKasirrwi = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id: 'GridListKasirRWI',
		store: dsTRKasirrwiKasirList,
		columnLines: false,
		autoScroll: true,
		style:'padding-top: 4px;',
		flex:1,
		sort: false,
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:{
				rowselect: function (sm, row, rec){
					dataRowIndex = row;
					rowSelectedKasirrwiKasir = dsTRKasirrwiKasirList.getAt(row);
					CurrentKDUnitRWI         = rowSelectedKasirrwiKasir.data.KD_UNIT;
					tmpkd_unit               = rowSelectedKasirrwiKasir.data.KD_UNIT;
					CurrentKDCustomerRWI     = rowSelectedKasirrwiKasir.data.KD_CUSTOMER;
					vKdPasien                = rowSelectedKasirrwiKasir.data.KD_PASIEN;
					vKdDokter                = rowSelectedKasirrwiKasir.data.KD_DOKTER;
					vUrutMasuk               = rowSelectedKasirrwiKasir.data.URUT_MASUK;
					//Kodepasein = rowSelectedKasirrwiKasir.data.KD_PASIEN;
					//getProdukRWI(DataProdukRWI);
					tmptgl_transaksiInduk 		 = rowSelectedKasirrwiKasir.data.TANGGAL_TRANSAKSI;
					status_customer = false;
				}
			}
		}),
		listeners:{
			rowdblclick: function (sm, ridx, cidx){
				rowSelectedKasirrwiKasir = dsTRKasirrwiKasirList.getAt(ridx);
				if (rowSelectedKasirrwiKasir != undefined){
					if (rowSelectedKasirrwiKasir.data.LUNAS == '0'){
						KasirLookUprwi(rowSelectedKasirrwiKasir.data);
					} else{
						ShowPesanWarningKasirrwi('Transaksi Sudah Lunas', 'Pembayaran');
					}
				} else {
					ShowPesanWarningKasirrwi('Silahkan Pilih data   ', 'Pembayaran');
				}
			},
			rowclick: function (sm, ridx, cidx){
				console.log(rowSelectedKasirrwiKasir.data);
				rowpasien                  = rowSelectedKasirrwiKasir.data;
				cellSelectedtutup_kasirrwi = rowSelectedKasirrwiKasir.data.NO_TRANSAKSI;
				kodepasien                 = rowpasien.KD_PASIEN;
				namapasien                 = rowpasien.NAMA;
				tmpKelas                   = rowpasien.KELAS;
				tmpKamar                   = rowpasien.NAMA_KAMAR;
				tmpUnit                    = rowpasien.KD_UNIT;
				tmpNoKamar                 = rowpasien.NO_KAMAR;
				tmpKdSpesial               = rowpasien.KD_SPESIAL;
				Ext.getCmp('btnEditKasirrwi').enable();
				if (rowSelectedKasirrwiKasir.data.LUNAS == '1') {
					Ext.getCmp('btnEditKasirrwi').disable();
				}
				if (rowSelectedKasirrwiKasir.data.LUNAS == '1' && rowSelectedKasirrwiKasir.data.CO_STATUS == '1'){
					Ext.getCmp('btnLookUpProduk_RawatInap').disable();
					Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
					Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').disable();
					Ext.getCmp('btnDepositKasirrwi').disable();
					Ext.getCmp('btnDiscountKasirrwi').disable();
					Ext.getCmp('btnTutupTransaksiKasirrwi').disable();
					Ext.getCmp('btnHpsBrsKasirrwi').disable();
					// Ext.getCmp('btnLookupRwi').disable();
					//Ext.getCmp('btnSimpanRwi').disable();
					//Ext.getCmp('btnHpsBrsRwi').disable();
				} else if (rowSelectedKasirrwiKasir.data.LUNAS == '0' && rowSelectedKasirrwiKasir.data.CO_STATUS == '0'){
					Ext.getCmp('btnTutupTransaksiKasirrwi').disable();
					Ext.getCmp('btnHpsBrsKasirrwi').enable();
					Ext.getCmp('btnLookUpProduk_RawatInap').enable();
					Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').enable();
					Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').enable();
					Ext.getCmp('btnDepositKasirrwi').enable();
					 Ext.getCmp('btnDiscountKasirrwi').enable();
					//Ext.getCmp('btnLookupRwi').disable();
					//Ext.getCmp('btnSimpanRwi').disable();
					//Ext.getCmp('btnHpsBrsRwi').disable();
				} else if (rowSelectedKasirrwiKasir.data.LUNAS == '1' && rowSelectedKasirrwiKasir.data.CO_STATUS == '0'){
					Ext.getCmp('btnLookUpProduk_RawatInap').disable();
					Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
					Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').disable();
					Ext.getCmp('btnDepositKasirrwi').disable();
					Ext.getCmp('btnDiscountKasirrwi').disable();
					Ext.getCmp('btnTutupTransaksiKasirrwi').enable();
					Ext.getCmp('btnHpsBrsKasirrwi').enable();
					//Ext.getCmp('btnLookupRwi').disable();
					//Ext.getCmp('btnSimpanRwi').disable();
					//Ext.getCmp('btnHpsBrsRwi').disable();
				}
				kodekasir = 1;
				tmpkd_unitKamar 	 = rowSelectedKasirrwiKasir.data.KD_UNIT_KAMAR;
				kdkasirnya           = rowSelectedKasirrwiKasir.data.KD_KASIR;
				notransaksi          = rowSelectedKasirrwiKasir.data.NO_TRANSAKSI;
				tmpno_transaksi      = rowSelectedKasirrwiKasir.data.NO_TRANSAKSI;
				tgltrans             = rowSelectedKasirrwiKasir.data.TANGGAL_TRANSAKSI;
				kodepasien           = rowSelectedKasirrwiKasir.data.KD_PASIEN;
				urutmasuk            = rowSelectedKasirrwiKasir.data.URUT_MASUK;
				namapasien           = rowSelectedKasirrwiKasir.data.NAMA;
				kodeunit             = rowSelectedKasirrwiKasir.data.KD_UNIT;
				namaunit             = rowSelectedKasirrwiKasir.data.NAMA_UNIT;
				kdcustomeraigd       = rowSelectedKasirrwiKasir.data.KD_CUSTOMER;
				vkode_customer_PJRWI = rowSelectedKasirrwiKasir.data.KD_CUSTOMER;
				vnama_customer_PJRWI = rowSelectedKasirrwiKasir.data.CUSTOMER;
				kdokter_btl          = rowSelectedKasirrwiKasir.data.KD_DOKTER;
				kodepay              = rowSelectedKasirrwiKasir.data.KD_PAY;
				tmpNamaDokter        = rowSelectedKasirrwiKasir.data.NAMA_DOKTER;
				uraianpay            = rowSelectedKasirrwiKasir.data.CARA_BAYAR;
				tmpTanggalMasuk      = rowSelectedKasirrwiKasir.data.TANGGAL_MASUK;
				tmpKdUnitKamar       = rowSelectedKasirrwiKasir.data.KD_UNIT_KAMAR;
				tmpKeterangan        = rowSelectedKasirrwiKasir.data.KETERANGAN;
				Ext.Ajax.request({
					url: baseURL + "index.php/main/functionRWI/cek_cominal_pembayaran",
					params: {
						kd_kasir 	: '02',
						no_transaksi: rowSelectedKasirrwiKasir.data.NO_TRANSAKSI,
					},
					failure: function (o){},
					success: function (o) {
						var cst = Ext.decode(o.responseText);
						var sisaPembayaran = cst.harus_membayar - cst.telah_membayar;
						Ext.getCmp('txtJumlahBayar_KASIRRWI').setValue(cst.harus_membayar);
						// Ext.getCmp('txtJumlahSisaBayar_KASIRRWI').setValue(sisaPembayaran);
					}
				});

				RefreshDataDetail_kasirrwi(notransaksi);
				// cekTransferTindakan(true);
				// kd_pasien, tgl_masuk, kd_unit, no_transaksi
				// RefreshDataDetail_kasirrwi
				RefreshDatahistoribayar(rowSelectedKasirrwiKasir.data.NO_TRANSAKSI);
				// RefreshDataDetail_kasirrwi(rowSelectedKasirrwiKasir.data.NO_TRANSAKSI);
				// RefreshRekapitulasiTindakanKasirRWI(rowSelectedKasirrwiKasir.data.NO_TRANSAKSI,rowSelectedKasirrwiKasir.data.KD_KASIR);
				loaddatastoreperawat();
				// rowSelectedKasirrwiKasir.data.TANGGAL_MASUK;
				/* ==================================== STORE HISTORY PINDAH KAMAR =================================================*/
				dsDataStoreGridHistoryPindah.removeAll();        
				var Field                  = ['nama_kamar','tgl_inap','jam_inap','tgl_keluar','jam_keluar','lama_rawat','urut_nginap'];
				DataStoreGridHistoryPindah = new WebApp.DataStore({ fields: Field });
				loaddatastoreHistoryPindahKamar();
				/* ==================================== STORE HISTORY PINDAH KAMAR =================================================*/

				TmpUrl = baseURL+"index.php/rawat_inap/lap_bill_kwitansi/preview_billing/"+kodepasien+"/"+kodeunit+"/"+tmpTanggalMasuk+"/02/"+"/"+notransaksi+"/false/true";
				// TmpUrl = baseURL+"data_billing.txt";
				// $kd_pasien=null, $kd_unit=null, $kd_kasir=null, $no_transaksi=null, $co_status=null, $print=null
				// TmpUrl = baseURL+"index.php/rawat_inap/1456214904DRJP.pdf";
				// DataDTLTRGridPreviewBilling(rowSelectedKasirrwiKasir.data.KD_PASIEN, rowSelectedKasirrwiKasir.data.TANGGAL_MASUK, rowSelectedKasirrwiKasir.data.KD_UNIT, rowSelectedKasirrwiKasir.data.NO_TRANSAKSI);
				//loaddatastoredokter();
				tmpkd_unit 		= rowSelectedKasirrwiKasir.data.KD_UNIT_KAMAR;
				tmpKdJob 		= 1;
				tmpRowGrid 		= 0;
				loaddatastoredokter_REVISI();
				//RefreshDataKasirRwiDetail(rowSelectedKasirrwiKasir.data.NO_TRANSAKSI);
				CurrentKDUnitRWI = rowSelectedKasirrwiKasir.data.KD_UNIT;
				CurrentKDCustomerRWI = rowSelectedKasirrwiKasir.data.KD_CUSTOMER;
				kodetarifnya=rowSelectedKasirrwiKasir.data.JENIS_PAY;
				Ext.Ajax.request({
					url: baseURL + "index.php/main/functionLABPoliklinik/gettarif",
					params: {
						kd_customer: rowSelectedKasirrwiKasir.data.KD_CUSTOMER,
					},
					failure: function (o){},
					success: function (o) {
						var cst = Ext.decode(o.responseText);
						varkd_tarif_rwi = cst.kd_tarif;
						getProdukRWI(rowSelectedKasirrwiKasir.data.KD_UNIT);
					}
				});
				Ext.Ajax.request({
					//url: "./home.mvc/getModule",
					//url: baseURL + "index.php/main/getTrustee",
					url: baseURL + "index.php/main/getcurrentshift",
					params: {
						//UserID: 'Admin',
						command: '0',
						// parameter untuk url yang dituju (fungsi didalam controller)
					},
					failure: function (o){},
					success: function (o) {
						//var cst = Ext.decode(o.responseText);
						tampungshiftsekarang = o.responseText
						//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;
					}
				});
				Ext.Ajax.request({
					//url: "./home.mvc/getModule",
					//url: baseURL + "index.php/main/getTrustee",
					url: baseURL + "index.php/main/Getkdtarif",
					params: {
						//UserID: 'Admin',
						customer: rowSelectedKasirrwiKasir.data.KD_CUSTOMER,
						// parameter untuk url yang dituju (fungsi didalam controller)
					},
					failure: function (o){},
					success: function (o) {
						//var cst = Ext.decode(o.responseText);
						nilai_kd_tarif = o.responseText;
						//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;
					}
				});
				currentNoTransaksi_KasirRWI  = rowSelectedKasirrwiKasir.data.NO_TRANSAKSI;
				currentTglTransaksi_KasirRWI = rowSelectedKasirrwiKasir.data.TANGGAL_TRANSAKSI;
				currentKdKasir_KasirRWI      = rowSelectedKasirrwiKasir.data.KD_KASIR;
				currentKDUnit_KasirRWI       = rowSelectedKasirrwiKasir.data.KD_UNIT;
				getTotalBayar_KasirRWI();
			}
		},
		cm: new Ext.grid.ColumnModel([
			//CUSTOMER
			{
				id: 'colLUNAScoba',
				header: 'Lunas',
				dataIndex: 'LUNAS',
				sortable: true,
				width: 25,
				align: 'center',
				renderer: function (value, metaData, record, rowIndex, colIndex, store){
					switch (value){
						case 1:
							metaData.css = 'StatusHijau'; // 
							break;
						case 0:
							metaData.css = 'StatusMerah'; // rejected
							break;
					}
					return '';
				}
			},{
				id: 'coltutuptr',
				header: 'Pulang',
				dataIndex: 'CO_STATUS',
				sortable: true,
				width: 35,
				align: 'center',
				renderer: function (value, metaData, record, rowIndex, colIndex, store){
					switch (value){
						case 1:
							metaData.css = 'StatusHijau'; // 
							break;
						case 0:
							metaData.css = 'StatusMerah'; // rejected
							break;
					}
					return '';
				}
			},{
				id: 'colReqIdViewKasirRWI',
				header: 'No. Transaksi',
				dataIndex: 'NO_TRANSAKSI',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 40
			},{
				id: 'colTglKasirrwiViewKasirRWI',
				header: 'Tgl Masuk',
				dataIndex: 'TANGGAL_TRANSAKSI',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 40,
				renderer: function (v, params, record){
					return ShowDate(record.data.TANGGAL_TRANSAKSI);
				}
			},{
				id: 'colIdJamMasukKasirRWI',
				header: 'Jam Masuk',
				dataIndex: 'JAM_MASUK',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 40
			},{
				header: 'No. Medrec',
				width: 40,
				sortable: false,
				hideable: false,
				menuDisabled: true,
				dataIndex: 'KD_PASIEN',
				id: 'colKasirrwierViewKasirRWI'
			},{
				header: 'Pasien',
				width: 190,
				sortable: false,
				hideable: false,
				menuDisabled: true,
				dataIndex: 'NAMA',
				id: 'colKasirrwierViewKasirRWI'
			},{
				id: 'colLocationViewKasirRWI',
				header: 'Alamat',
				dataIndex: 'ALAMAT',
				sortable: false,
				hideable: false,
				hidden: true,
				menuDisabled: true,
				width: 170
			},{
				id: 'colDeptViewKasirRWI',
				header: 'Dokter',
				dataIndex: 'NAMA_DOKTER',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 75
			},{
				id: 'colunitViewKasirRWI',
				header: 'Unit',
				dataIndex: 'KETERANGAN',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 100
			},{
				id: 'colSpesialisasiViewKasirRWI',
				header: 'Spesialisasi',
				dataIndex: 'SPESIALISASI',
				sortable: false,
				hideable: false,
				hidden: true,
				menuDisabled: true,
				width: 90
			},{
				id: 'colcustomerViewKasirRWI',
				header: 'Kel. Pasien',
				dataIndex: 'CUSTOMER',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 90
			},{
				id: 'colHakKelasViewKasirRWI',
				header: 'Hak Kelas',
				dataIndex: 'HAK_KELAS',
				sortable: false,
				hideable: false,
                hidden: true,
				menuDisabled: true,
				width: 32,
				renderer: function (value, metaData, record, rowIndex, colIndex, store){
					console.log(record);
					var newvalue;
					if (value != '-') {
						if (record.data.KELAS_RAWAT !== value) {
							newvalue = "<span style='color:red;' >"+value+"</span>";
						}else{
							newvalue = value;
						}
					}
					else {
						newvalue = value;
					}
			        return newvalue;
				}
			},{
				id: 'colParentKasirRWI',
				header: 'Parent',
				dataIndex: 'PARENTNYA',
				sortable: false,
				hideable: true,
				hidden: true,
				menuDisabled: true,
				width: 150
			}
		]),
		viewConfig: {forceFit: true},
		tbar:
		[{
			xtype: 'buttongroup',
			//title: 'Transaksi',
			columns: 6,
			defaults: {
				scale: 'small'
			},
			items: [
				{
					id: 'btnEditKasirrwi',
					text: 'Pembayaran',
					tooltip: nmEditData,
					iconCls: 'Edit_Tr',
					handler: function (sm, row, rec){
						if (rowSelectedKasirrwiKasir != undefined){
							KasirLookUprwi(rowSelectedKasirrwiKasir.data);
						} else{
							ShowPesanWarningKasirrwi('Pilih data tabel  ', 'Pembayaran');
						}
					}, disabled: true
				},{
					text: 'Tutup Transaksi',
					id: 'btnTutupTransaksiKasirrwi',
					tooltip: nmHapus,
					hidden: true,
					iconCls: 'remove',
					handler: function (){
						if (cellSelectedtutup_kasirrwi == '' || cellSelectedtutup_kasirrwi == 'undefined'){
							ShowPesanWarningKasirrwi('Pilih data tabel  ', 'Pembayaran');
						} else{   
							UpdateTutuptransaksi(false);
						   // pasien_pulang(rowSelectedKasirrwiKasir.data);
							//UpdateTutuptransaksi(false);  
							Ext.getCmp('btnEditKasirrwi').disable();
							Ext.getCmp('btnLookUpProduk_RawatInap').disable();
							Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
							Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').disable();
							Ext.getCmp('btnTutupTransaksiKasirrwi').disable();
							Ext.getCmp('btnHpsBrsKasirrwi').disable();
						}
					},
					disabled: true
				},{
					text: 'Batal Transaksi',
					id: 'btnBatalTransaksiKasirrwi',
					tooltip: nmHapus,
					hidden:true,
					iconCls: 'remove',
					handler: function (){
						if (cellSelectedtutup_kasirrwi == '' || cellSelectedtutup_kasirrwi == 'undefined'){
							ShowPesanWarningKasirrwi('Pilih data tabel  ', 'Pembayaran');
						} else if (rowSelectedKasirrwiKasir.data.CO_STATUS == 'f'){
							ShowPesanWarningKasirrwi('Transaksi Belum ditutup tidak bisa dibatalkan', 'Pembayaran');
						} else if (rowSelectedKasirrwiKasir.data.BATAL == 't'){
							ShowPesanWarningKasirrwi('Transaksi Sudah dibatalkan tidak bisa dibatalkan kembali', 'Pembayaran');
						} else{
							fnDlgRWIPasswordDulu();
						}
					},
					disabled: false
				},{
					xtype:'splitbutton',
					text: 'Bayar lain',
					iconCls: 'Edit_Tr',
					arrowAlign:'right',
					menu: [
						{
							id: 'btnTransferKasirrwi',
							text: 'Transfer',
							tooltip: nmEditData,
							iconCls: 'Edit_Tr',
							handler: function (sm, row, rec){
								GetDTLTRKasirrwiGrid();
								TransferLookUp();
							}, disabled: false
						},{
							id: 'btnDepositKasirrwi',
							text: 'Deposit',
							iconCls: 'cost',
							hidden:false,
							disabled: true,
							handler: function (sm, row, rec){
								if (rowSelectedKasirrwiKasir != undefined){
									fnDlgRWIDeposit(rowSelectedKasirrwiKasir.data);
								} else{
									ShowPesanWarningKasirrwi('Pilih data tabel  ', 'Pembayaran');
								}
							}
						},{
							id: 'btnDiscountKasirrwi',
							text: 'Discount',
							iconCls: 'cost',
							hidden:false,
							disabled: true,
							handler: function (sm, row, rec){
								if (rowSelectedKasirrwiKasir != undefined){
									fnDlgRWIDiscount(rowSelectedKasirrwiKasir.data);
								} else{
									ShowPesanWarningKasirrwi('Pilih data tabel  ', 'Pembayaran');
								}
							}
						}
					]
				},{
					text: 'Pulang',
					id: 'btnPasienPulang_KASIRRWI',
					tooltip: 'Pasien Pulang',
					hidden:false,
					iconCls: 'gantidok',
					handler: function (){
						if (rowSelectedKasirrwiKasir != undefined){
							fnDlgRWIPasienPulangRWI('form_pulang', rowSelectedKasirrwiKasir.data);
						} else{
							ShowPesanWarningKasirrwi('Pilih data tabel  ', 'Pasien Pulang');
						}
					},
				},{
					text: 'Tutup Trans.',
					id: 'btnPasienTutupTransaksi_KASIRRWI',
					tooltip: 'Tutup Transaksi',
					hidden:false,
					iconCls: 'remove',
					handler: function (){
						if (rowSelectedKasirrwiKasir != undefined){
							fnDlgRWIPasienPulangRWI('form_tutup', rowSelectedKasirrwiKasir.data);
						} else{
							ShowPesanWarningKasirrwi('Pilih data tabel  ', 'Pasien Pulang');
						}
					}
				}

			]
		},{
			xtype: 'buttongroup',
			//title: 'Other Bogus Actions',
			columns: 7,
			defaults: {
				scale: 'small'
			},
			items: [
				{
					xtype:'splitbutton',
					text: 'Kamar',
					iconCls: 'Konsultasi',
					arrowAlign:'right',
					menu: [
						{
							text: 'Pindah Kamar',
							id: 'btnLookUpKonsultasi_panatajasarwi',
							iconCls: 'Konsultasi',
							handler: function () {
								jPanel = 1;
								actKamar = 'Pindah';
								if(rowSelectedKasirrwiKasir.data.BBL == 't'){
									//check bayi rawat gabung
									Ext.Ajax.request({
										url     : baseURL + "index.php/rawat_inap/function_rwi/cek_bayi_rawat_gabung",
										params  :  {
											kd_pasien   : rowSelectedKasirrwiKasir.data.KD_PASIEN,
										},
										failure : function(o){},
										success : function(o){
											var cst = Ext.decode(o.responseText);
											console.log(cst.rawat_gabung);
											if (cst.success == true) {
												if(cst.rawat_gabung == 'f'){
													Ext.Msg.show({
														title: 'Konfirmasi',
														msg: 'Apakah bayi Rawat Gabung?',
														buttons: Ext.MessageBox.YESNO,
														fn: function (btn) {
															if (btn == 'yes'){
																formLookup_kunjunganibu();
																cek_rawat_gabung=true;
															}else{
																setLookUp_viPindahKamarRWI();
																cek_rawat_gabung=false;
															}
														},
														icon: Ext.MessageBox.QUESTION
													});
												}else{
													setLookUp_viPindahKamarRWI();
													cek_rawat_gabung=false;
												}
											}
										}
									});
									
									
								}else{
									setLookUp_viPindahKamarRWI();
									cek_rawat_gabung=false;
								}
								
								
							}
						},{
							text: 'Ganti Kamar',
							id: 'btnLookUpGantiKamar_panatajasarwi',
							iconCls: 'Konsultasi',
							handler: function () {
								jPanel = 2;
								actKamar = 'Ganti';
								if(rowSelectedKasirrwiKasir.data.BBL == 't'){
									//check bayi rawat gabung
									Ext.Ajax.request({
										url     : baseURL + "index.php/rawat_inap/function_rwi/cek_bayi_rawat_gabung",
										params  :  {
											kd_pasien   : rowSelectedKasirrwiKasir.data.KD_PASIEN,
										},
										failure : function(o){},
										success : function(o){
											var cst = Ext.decode(o.responseText);
											console.log(cst.rawat_gabung);
											if (cst.success == true) {
												if(cst.rawat_gabung == 'f'){
													Ext.Msg.show({
														title: 'Konfirmasi',
														msg: 'Apakah bayi Rawat Gabung?',
														buttons: Ext.MessageBox.YESNO,
														fn: function (btn) {
															if (btn == 'yes'){
																formLookup_kunjunganibu();
																cek_rawat_gabung=true;
															}else{
																setLookUp_viPindahKamarRWI();
																cek_rawat_gabung=false;
															}
														},
														icon: Ext.MessageBox.QUESTION
													});
												}else{
													setLookUp_viPindahKamarRWI();
													cek_rawat_gabung=false;
												}
											}
										}
									});
									
									
								}else{
									setLookUp_viPindahKamarRWI();
									cek_rawat_gabung=false;
								}
							}
						},{
							text: 'History Pindah Kamar',
							id: 'btnLookUpHistoryKamar_panatajasarwi',
							iconCls: 'Konsultasi',
							handler: function () {
								// jPanel = 3;
								PilihHistoryPindahKamarLookUp_KasirRWI();
								actKamar = 'History';
							}
						},
					]
				},{
					xtype:'splitbutton',
					text: 'Update Data',
					iconCls: 'Konsultasi',
					arrowAlign:'right',
					menu: [
						{
							text: ' Ganti Dokter',
							id: 'btnLookUpGantiDokter_panatajasarwi',
							iconCls: 'gantidok',
							handler: function () {
								//GantiDokterLookUp_penatajasarwi_penatajasarwi();
								GantiDokterPasienLookUp_rwi_REVISI();
							}
						},{
							text: 'Ganti Kelompok Pasien',
							id: 'btngantipasien_panatajasarwi',
							iconCls: 'gantipasien',
							handler: function () {
								KelompokPasienLookUp_penatajasarwi();
							}
						}
					]
				},{
					xtype: 'button',
					text: 'Cetak',
					iconCls: 'print',
					id: 'btnPrint_viDaftarRwi',
					arrowAlign:'right',
					menu: [
						{
							text: 'Billing',
							id: 'btnLookUpBilling_KasirRwi',
							iconCls: 'print',
							handler: function () {
								// fnDlgRWICetakBilling_KASIRRWI(rowSelectedKasirrwiKasir.data);
                                console.log(rowSelectedKasirrwiKasir);
								if (rowSelectedKasirrwiKasir != undefined ||rowSelectedKasirrwiKasir != undefined) {
									GetDTLTRGridPreviewBilling(false);
								}
							}
						},{
							text: 'Ledger',
							id: 'btnLookUpLedger_KasirRwi',
							iconCls: 'print',
							handler: function () {
								var url = baseURL + "index.php/laporan/lap_billing_rwi/cetak_ledger";
                                window.open(url+"/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.KD_KASIR+"/false/3", '_blank');
							}
						},{
							text: 'Kwitansi',
							id: 'btnLookUpKwitansi_KasirRwi',
                            hidden:true,
							iconCls: 'print',
							handler: function () {
								LookUpEditKwitansiPanel();
							}
						},'-',{
							text: 'Rekapitulasi',
							id: 'btnLookUpRekapitulasi_KasirRwi',
							iconCls: 'print',
							handler: function () {
								// var url = baseURL + "index.php/laporan/lap_rekapitulasi/cetak_rwi";
                                var url = baseURL + "index.php/laporan/lap_rekapitulasi/cetak_rwi_exe";
                                window.open(url+"/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.KD_KASIR+"/false/3", '_blank');
								// new Ext.Window({
								// 	title: 'Preview',
								// 	width: 1000,
								// 	height: 600,
								// 	constrain: true,
								// 	modal: true,
								// 	html: "<iframe  style ='width: 100%; height: 100%;' src='" + url+"/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.KD_KASIR+"/true/3'></iframe>",
								// 	tbar : [
								// 		{
								// 			xtype   : 'button',
								// 			text    : 'Cetak Direct',
								// 			iconCls : 'print',
								// 			handler : function(){
								// 				window.open(url+"/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.KD_KASIR+"/false/3", '_blank');
								// 			}
								// 		}
								// 	]
								// }).show();
							}
						},{
							text: 'Rekapitulasi Apotek',
							id: 'btnLookUpRekapitulasi_apt_KasirRwi',
							iconCls: 'print',
							handler: function () {
								var url = baseURL + "index.php/laporan/lap_rekapitulasi/cetak_rekap_apotek_rwi";
								new Ext.Window({
									title: 'Preview',
									width: 1000,
									height: 600,
									constrain: true,
									modal: true,
									html: "<iframe  style ='width: 100%; height: 100%;' src='" + url+"/"+rowSelectedKasirrwiKasir.data.KD_PASIEN+"/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.TANGGAL_MASUK+"/true/3'></iframe>",
									tbar : [
										{
											xtype   : 'button',
											text    : 'Cetak Direct',
											iconCls : 'print',
											handler : function(){
												window.open(url+"/"+rowSelectedKasirrwiKasir.data.KD_PASIEN+"/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.TANGGAL_MASUK+"/false/3", '_blank');
											}
										}
									]
								}).show();
							}
						},{
							text: 'Surat Pulang',
							hidden 	: true,
							id: 'btnLookUpPulang_KasirRwi',
							iconCls: 'print',
							handler: function () {
								// LookUpEditKwitansiPanel();
		                    	var tmp_url = baseURL+"index.php/laporan/lap_pulang/print_surat_pulang/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.KD_KASIR;
								window.open(tmp_url,'_blank');
							}
						}
					],
					// handler: function (){
					// 	fnDlgRWICetakBilling_KASIRRWI(rowSelectedKasirrwiKasir.data);
					// },
					// menu: new Ext.menu.Menu({
					// items: [
					// 	{
					// 		xtype: 'button',
					// 		text: 'Print Bill',
					// 		id: 'btnPrintBillPreviewRwi',
					// 		handler: function (){
					// 			fnDlgRWICetakBilling_KASIRRWI(rowSelectedKasirrwiKasir.data);
					// 		}
					// 	},
					// ]
					// })
				},{
					text: 'Save Pulang',
					id: 'btnSavePulangPasien_KasirRwi',
                    hidden:true,
					iconCls: 'save',
					handler: function () {
						savePulangPasien_KASIRRWI();
					}
				},{
					xtype: 'datefield',
					fieldLabel: 'Tanggal',
                    hidden:true,
					format:'d/M/Y',
					width:100,
					id: 'txtTanggalPulangPasien_KasirRwi',
					name: 'txtTanggalPulangPasien_KasirRwi',
					value:now,
				},{
					xtype: 'textfield',
					fieldLabel: 'Jam',
                    hidden:true,
					width:100,
					id: 'txtJamPulangPasien_KasirRwi',
					name: 'txtJamPulangPasien_KasirRwi',
					value:DefJamPerpindahan,
				}
			]
		}]
	});
    var LegendViewCMRequest = new Ext.Panel
	(
		{
			id: 'LegendViewCMRequest',
			border: false,
			bodyStyle: 'padding:4px;',
			layout: 'column',
			// frame: true,
			//height:32,
			anchor: '100% 10%',
			autoScroll: false,
			items:
			[
				{
					columnWidth: .033,
					layout: 'form',
					// style: {'margin-top': '-1px'},
					//height:32,
					anchor: '100% 8.0001%',
					border: false,
					html: '<img src="' + baseURL + 'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
				},
				{
					columnWidth: .08,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style: {'margin-top': '1px'},
					border: false,
					html: " Lunas"
				},
				{
					columnWidth: .033,
					layout: 'form',
					style: {'margin-top': '-1px'},
					border: false,
					//height:35,
					anchor: '100% 8.0001%',
					//html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
					html: '<img src="' + baseURL + 'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
				},
				{
					columnWidth: .1,
					layout: 'form',
					//height:32,
					anchor: '100% 8.0001%',
					style: {'margin-top': '1px'},
					border: false,
					html: " Belum Lunas"
				},
				
				{
					columnWidth: .6,
					layout: 'form',
					//height:32,
					style: {'float':'right'},
					border: false,
					items : [

						// {
							// xtype: 'textfield',
							// fieldLabel: 'Total Bayar',
							// style: {'padding-left':'10px'},
							// id: 'txtJumlahBayar_KASIRRWI',
							// name: 'txtJumlahBayar_KASIRRWI',
							// readOnly: true,
						// },
						/*{
							columnWidth: .4,
							layout: 'column',
							hidden: false,
							//height:32,
							//style: {'margin-right': '100px','float':'right'},
							border: true,
							items : [
							{
								xtype: 'textfield',
								fieldLabel: 'Total Bayar',
								style: {'padding-left':'10px'},
								id: 'txtJumlahBayar_KASIRRWI',
								name: 'txtJumlahBayar_KASIRRWI',
								readOnly: true,
							},
							]
						},*//*{
							columnWidth: .3,
							layout: 'column',
							hidden: true,
							//height:32,
							//style: {'margin-left': '10px','float':'right'},
							border: true,
							items : [
							{
								xtype: 'textfield',
								// fieldLabel: 'Tanggal',
								style: {'padding-left':'10px'},
								id: 'txtJumlahSisaBayar_KASIRRWI',
								name: 'txtJumlahSisaBayar_KASIRRWI',
								readOnly: true,
							},
							]
						},*/
					]
				},
			]
		}
	)
    var GDtabDetailrwi = new Ext.TabPanel({
		id: 'GDtabDetailrwi',
		region: 'center',
		activeTab: 0,
		flex:1,
		style:'padding-top: 4px;',
		plain: true,
		fbar:[
			{
				border: false,
				html: '<img src="' + baseURL + 'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
			},{
				xtype:'displayfield',
				value:'Lunas'
			},{
				border: false,
				html: '<img src="' + baseURL + 'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
			},{
				xtype:'displayfield',
				value:'Belum Lunas'
			},{
				xtype:'displayfield',
				value:'Total Bayar : &nbsp;'
			},{
				xtype: 'textfield',
				id: 'txtJumlahBayar_KASIRRWI',
				name: 'txtJumlahBayar_KASIRRWI',
				readOnly: true,
			},
		],
		defaults:{
			autoScroll: true
		},
		items: [
			GetDTLTRGridTindakanYangDiberikanKasirRWI(),
			GetDTLTRHistoryGrid(), 
			// GetDTLTRGridRekapitulasiTindakanKasirRWI(),
			// GetDTLTRGridPreviewBilling(),
			//-------------- ## --------------
		]
	});
    var FormDepanKasirrwi = new Ext.Panel({
		id: mod_id,
		closable: true,
		layout:{
			type:'vbox',
			align:'stretch'
		},
		title: 'Kasir Rawat Inap',
		border: false,
		autoScroll: true,
		bodyStyle:'padding: 4px;',
		items: [
			{
				xtype: 'form',
				height: 57,
				bodyStyle:'padding: 4px;',
				items: [
					{
						layout:'column',
						border:false,
						items:[
							{
								layout:'form',
								border:false,
								items:{
									xtype: 'textfield',
									fieldLabel:'No. Medrec',
									id: 'txtFilterNomedrec_rwi',
									onInit: function () { },
									listeners:{
										'specialkey': function (){
											var tmpNoMedrec = Ext.get('txtFilterNomedrec_rwi').getValue()
											if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
												if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10){
													var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec_rwi').getValue());
													Ext.getCmp('txtFilterNomedrec_rwi').setValue(tmpgetNoMedrec);
													// var tmpkriteria = getCriteriaFilter_viDaftar();
													RefreshDataFilterKasirrwiKasir();
												} else{
													if (tmpNoMedrec.length === 10){
														RefreshDataFilterKasirrwiKasir();
													} else
														Ext.getCmp('txtFilterNomedrec_rwi').setValue('')
												}
											}
										}

									}
								}
								
							},{
								layout:'form',
								style:'padding-left: 4px;',
								border:false,
								items:{
									xtype: 'textfield',
									fieldLabel:'Nama Pasien',
									id: 'TxtFilterGridDataView_NAMA_viKasirrwiKasir',
									enableKeyEvents: true,
									listeners:{
										'specialkey': function (){
											// var tmpNoMedrec = Ext.get('txtFilterNomedrec_rwi').getValue()
											if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
												RefreshDataFilterKasirrwiKasir();
											}
										},
									}
								}
							}
						]
					},
					getItemPanelcombofilter()
				]
			},
			grListTRKasirrwi, 
			GDtabDetailrwi, 
			// LegendViewCMRequest
		],
	});
    // RefreshDataKasirrwiKasir();
    return FormDepanKasirrwi
}

/* window pencarian kunjungan ibu*/
function formLookup_kunjunganibu(){
    var lebar = 900;
    var Lookup_KunjunganIbu = new Ext.Window({
        id: 'Lookup_KunjunganIbu',
        name: 'Lookup_KunjunganIbu',
        title: 'Data Kunjungan Ibu',
        closeAction: 'destroy',
        width: 420,
        height: 200, //575,
        resizable: false,
        constrain: true,
        autoScroll: false,
        iconCls: 'Studi_Lanjut',
        modal: true,
        items: getFormItemEntry_Lookup_KunjunganIbu(), 
        listeners:{
            afterShow: function (){
                this.activate();

            }
        },
		fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnSaveBayiRawatGabung',
					handler: function()
					{
						setLookUp_viPindahKamarRWI();
						Lookup_KunjunganIbu.close();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelBayiRawatGabung',
					handler: function()
					{
						Lookup_KunjunganIbu.close();
					}
				}
			]
    });
    Lookup_KunjunganIbu.show();
}

function getFormItemEntry_Lookup_KunjunganIbu(){
	 var pnlFormDataBasic_Lookup_KunjunganIbu = new Ext.Panel({
        id: 'pnlFormDataBasic_Lookup_KunjunganIbu',
		fileUpload: true,
		layout: 'form',
		bodyStyle:{"padding":"5px"}, 
		anchor: '100%',
		border: false,
        width: 420,
        height: 200,
        items: [
					{
						layout: 'column',
						border: false,
						items: [{
							layout: 'absolute',
							bodyStyle:{"background-color":"white"}, 
							border: true,
							width:  400,
							height: 150,
							anchor: '100% 100%',
							items: [
								{
									x: 10,
									y: 20,
									id : 'lblMedrec_LookupKunjunganIbu',
									xtype: 'label',
									text: 'No. Medrec '
								},
								{
									x: 110,
									y: 20,
									xtype: 'label',
									text: ' : '
								},
								{
									xtype: 'textfield',
									x: 120,
									y:20,
									name: 'txtMedrec_LookupKunjunganIbu',
									id: 'txtMedrec_LookupKunjunganIbu',
									width: 180,
									emptyText: 'Masukkan No. Medrec Ibu...',
									listeners:{ 
										'select': function(a,b,c){
										},
										'specialkey' : function(){
											if (Ext.EventObject.getKey() === 13) {
												var tmpNoIIMedrec = Ext.getCmp('txtMedrec_LookupKunjunganIbu').getValue();
												if (tmpNoIIMedrec.length !== 0 && tmpNoIIMedrec.length < 10)
												{
													var tmpgetNoIIMedrec = formatnomedrec(Ext.getCmp('txtMedrec_LookupKunjunganIbu').getValue());
													Ext.getCmp('txtMedrec_LookupKunjunganIbu').setValue(tmpgetNoIIMedrec);
													
													// get kunjungan ibu 
													getKamarIbu(Ext.getCmp('txtMedrec_LookupKunjunganIbu').getValue());
												} else{
													if (tmpNoIIMedrec.length === 10)
													{
														Ext.getCmp('txtMedrec_LookupKunjunganIbu').setValue(tmpNoIIMedrec);
														// get kunjungan ibu 
														getKamarIbu(Ext.getCmp('txtMedrec_LookupKunjunganIbu').getValue());
													}												
												}
											
											} 						
										}
									}
								},
								
								{
									x: 10,
									y: 50,
									id : 'lblNamaIbu_LookupKunjunganIbu',
									xtype: 'label',
									text: 'Nama Pasien '
								},
								{
									x: 110,
									y: 50,
									xtype: 'label',
									text: ' : '
								},
								{
									xtype: 'textfield',
									x: 120,
									y:50,
									name: 'txtNamaIbu_LookupKunjunganIbu',
									id: 'txtNamaIbu_LookupKunjunganIbu',
									width: 250,
									disabled:true,
								},
								
								{
									x: 10,
									y: 80,
									id : 'lbllKamar_LookupKunjunganIbu',
									xtype: 'label',
									text: 'Kamar '
								},
								{
									x: 110,
									y: 80,
									xtype: 'label',
									text: ' : '
								},
								{
									xtype: 'textfield',
									x: 120,
									y:80,
									name: 'txtKamars_LookupKunjunganIbu',
									id: 'txtKamars_LookupKunjunganIbu',
									width: 250,
									disabled:true,
								},
								//parameter yang berbeda
								{
									x: 140,
									y:80,
									xtype: 'textfield',
									name: 'txtspesialisasi_LookupKunjunganIbu',
									id: 'txtspesialisasi_LookupKunjunganIbu',
									width: 250,
									disabled:true,
									hidden:true,
								},
								{
									x: 140,
									y:80,
									xtype: 'textfield',
									name: 'txtKelas_LookupKunjunganIbu',
									id: 'txtKelas_LookupKunjunganIbu',
									width: 250,
									disabled:true,
									hidden:true,
								},
								{
									x: 140,
									y:80,
									xtype: 'textfield',
									name: 'txtPoli_LookupKunjunganIbu',
									id: 'txtPoli_LookupKunjunganIbu',
									width: 250,
									disabled:true,
									hidden:true,
								},
								{
									x: 140,
									y:80,
									xtype: 'textfield',
									name: 'txtKamar_LookupKunjunganIbu',
									id: 'txtKamar_LookupKunjunganIbu',
									width: 250,
									disabled:true,
									hidden:true,
								},
								//
								{
									x: 140,
									y:80,
									xtype: 'textfield',
									name: 'lblspesialisasi_LookupKunjunganIbu',
									id: 'lblspesialisasi_LookupKunjunganIbu',
									width: 250,
									disabled:true,
									hidden:true,
								},
								{
									x: 140,
									y:80,
									xtype: 'textfield',
									name: 'lblKelas_LookupKunjunganIbu',
									id: 'lblKelas_LookupKunjunganIbu',
									width: 250,
									disabled:true,
									hidden:true,
								},
								{
									x: 140,
									y:80,
									xtype: 'textfield',
									name: 'lblPoli_LookupKunjunganIbu',
									id: 'lblPoli_LookupKunjunganIbu',
									width: 250,
									disabled:true,
									hidden:true,
								},
								{
									x: 140,
									y:80,
									xtype: 'textfield',
									name: 'lblKamar_LookupKunjunganIbu',
									id: 'lblKamar_LookupKunjunganIbu',
									width: 250,
									disabled:true,
									hidden:true,
								},//
								{
									x: 140,
									y:80,
									xtype: 'textfield',
									name: 'txtmedrec_LookupKunjunganIbu',
									id: 'txtmedrec_LookupKunjunganIbu',
									width: 250,
									disabled:true,
									hidden:true,
								},
								{
									
									xtype: 'textfield',
									name: 'txtalamat_LookupKunjunganIbu',
									id: 'txtalamat_LookupKunjunganIbu',
									hidden:true,
								},
								{
									
									xtype: 'textfield',
									name: 'txtpropinsi_LookupKunjunganIbu',
									id: 'txtpropinsi_LookupKunjunganIbu',
									hidden:true,
								},
								{
									
									xtype: 'textfield',
									name: 'txtkab_LookupKunjunganIbu',
									id: 'txtkab_LookupKunjunganIbu',
									hidden:true,
								},
								{
									
									xtype: 'textfield',
									name: 'txtkec_LookupKunjunganIbu',
									id: 'txtkec_LookupKunjunganIbu',
									hidden:true,
								},
								{
									
									xtype: 'textfield',
									name: 'txtkel_LookupKunjunganIbu',
									id: 'txtkel_LookupKunjunganIbu',
									hidden:true,
								},
								{
									
									xtype: 'textfield',
									name: 'txtkdpos_LookupKunjunganIbu',
									id: 'txtkdpos_LookupKunjunganIbu',
									hidden:true,
								},
								
								//
								{
									
									xtype: 'textfield',
									name: 'id_txtpropinsi_LookupKunjunganIbu',
									id: 'id_txtpropinsi_LookupKunjunganIbu',
									hidden:true,
								},
								{
									
									xtype: 'textfield',
									name: 'id_txtkab_LookupKunjunganIbu',
									id: 'id_txtkab_LookupKunjunganIbu',
									hidden:true,
								},
								{
									
									xtype: 'textfield',
									name: 'id_txtkec_LookupKunjunganIbu',
									id: 'id_txtkec_LookupKunjunganIbu',
									hidden:true,
								},
								{
									
									xtype: 'textfield',
									name: 'id_txtkel_LookupKunjunganIbu',
									id: 'id_txtkel_LookupKunjunganIbu',
									hidden:true,
								},
								{
									
									xtype: 'textfield',
									name: 'txt_cboAgamaRawat_inap_bbl',
									id: 'txt_cboAgamaRawat_inap_bbl',
									hidden:true,
								},
								{
									xtype: 'textfield',
									name: 'id_txt_cboAgamaRawat_inap_bbl',
									id: 'id_txt_cboAgamaRawat_inap_bbl',
									hidden:true,
								},
								{
									
									xtype: 'textfield',
									name: 'txt_cboDokterSpesialRWIRawat_inap_bbl',
									id: 'txt_cboDokterSpesialRWIRawat_inap_bbl',
									hidden:true,
								},
								{
									xtype: 'textfield',
									name: 'id_txt_cboDokterSpesialRWIRawat_inap_bbl',
									id: 'id_txt_cboDokterSpesialRWIRawat_inap_bbl',
									hidden:true,
								},
							]
						}]
					}		
        ],
        fileUpload: true,
    });
    return pnlFormDataBasic_Lookup_KunjunganIbu;
}

function getKamarIbu(medrec){
	loadMask.show();
	  Ext.Ajax.request({
        url: baseURL + "index.php/rawat_inap/function_rwi/getKamarIbu",
        params: {
            medrec:medrec
        },
        failure: function (o){
            loadMask.hide();
        },
        success: function (o){
			loadMask.hide();
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
				tmp_bayi_rawat_gabung_cek = true;
               //set info kunjungan ibu
				var cst = 	Ext.decode(o.responseText);
				var o	=	cst['listData'];
				Ext.getCmp('txtNamaIbu_LookupKunjunganIbu').setValue(o.nama);
				Ext.getCmp('txtKamars_LookupKunjunganIbu').setValue(o.nama_kamars);
				
				Ext.getCmp('txtspesialisasi_LookupKunjunganIbu').setValue(o.kd_spesial);
				Ext.getCmp('txtKelas_LookupKunjunganIbu').setValue(o.kd_kelas);
				Ext.getCmp('txtPoli_LookupKunjunganIbu').setValue(o.kd_unit_kamar);
				Ext.getCmp('txtKamar_LookupKunjunganIbu').setValue(o.no_kamar);
				Ext.getCmp('txtmedrec_LookupKunjunganIbu').setValue(medrec);
				
				Ext.getCmp('lblspesialisasi_LookupKunjunganIbu').setValue(o.spesialisasi);
				Ext.getCmp('lblKelas_LookupKunjunganIbu').setValue(o.kelas);
				Ext.getCmp('lblPoli_LookupKunjunganIbu').setValue(o.nama_unit);
				Ext.getCmp('lblKamar_LookupKunjunganIbu').setValue(o.nama_kamar);
	
				
            } else {
                ShowPesanWarningKasirrwi('Data kunjungan tidak ditemukan!', 'Warning');
            };
        }
    })
}
/*END UPDATE*/


function BatalTransaksi_kasirrwi(mBol)
{

    Ext.Ajax.request
	({
		//url: "./Datapool.mvc/CreateDataObj",
		url: baseURL + "index.php/main/functionRWI/batal_transaksi",
		params: getParamDetailBatalTransaksiRWI(),
		failure: function (o)
		{
			ShowPesanWarningKasirrwi(Ext.decode(o.responseText), 'Gagal');
			RefreshDataFilterKasirrwiKasir();
		},
		success: function (o)
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				ShowPesanInfoKasirrwi('Transaksi berhasil dibatalkan', 'Information');
				BatalTransaksi_kasirrwi_SQL(cst.notrans,cst.notransbaru,cst.urutmasuk,cst.urutmasukbaru);
				if (mBol === false)
				{
					RefreshDataFilterKasirrwiKasir();
				}
				;
				cellSelectedtutup_kasirrwi = '';
			} else
			{
				ShowPesanWarningKasirrwi(Ext.decode(o.responseText), 'Gagal');
				cellSelectedtutup_kasirrwi = '';
			}
			;
		}
	})
};

function getParamDetailBatalTransaksiRWI()
{
    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    };

    var params =
            {
                kdPasien: kodepasien,
                noTrans: notransaksi,
                kdUnit: kodeunit,
                kdDokter: kdokter_btl,
                tglTrans: tgltrans,
                kdCustomer: kdcustomeraigd,
                Keterangan: variablebatalhistori_rwi,
            };
    return params
};
function fnDlgRWIPasswordDulu()
{
    winRWIPasswordDulu = new Ext.Window
    (
        {
            id: 'winRWIPasswordDulu',
            title: 'Password',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgRWIPasswordDulu()]

        }
    );

    winRWIPasswordDulu.show();
};
function ItemDlgRWIPasswordDulu()
{
    var PnlLapRWISPasswordDulu = new Ext.Panel
    (
        {
            id: 'PnlLapRWISPasswordDulu',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapRWIPasswordDulu_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                         {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnOkLapRWIPasswordDulu',
                            handler: function()
                            {
                               Ext.Ajax.request
								 (
									{
										//url: "./Datapool.mvc/CreateDataObj",
										url: baseURL + "index.php/main/functionRWI/cekpassword_rwi",
										params: {passDulu:Ext.getCmp('TxPasswordDuluRwi').getValue()},
										failure: function(o)
										{
											ShowPesanWarningKasirrwj('Proses Gagal segera Hubungi Admin', 'Gagal');
										//RefreshDataKasirRwiDetail(notransaksi);
										},
										success: function(o)
										{
											//RefreshDataKasirRwiDetail(notransaksi);
											var cst = Ext.decode(o.responseText);
											if (cst.success === true)
											{
												winRWIPasswordDulu.close();
												var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
                                                    if (btn == 'ok')
                                                    {
                                                        variablebatalhistori_rwi = combo;
                                                        if (variablehistori != '')
                                                        {
                                                            BatalTransaksi_kasirrwi(false);
                                                        } else
                                                        {
                                                            ShowPesanWarningKasirrwi('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

                                                        }
                                                    }

                                                });
											}
											else
											{
													ShowPesanWarningKasirrwi('Password salah segera Hubungi Admin', 'Gagal');
											};
										}
									}
								)
							   /* */

								    
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnCancelLapRWIPasswordDulu',
                            handler: function()
                            {
                                    winRWIPasswordDulu.close();
                            }
                        } 
                    ]
                }
            ]
        }
    );

    return PnlLapRWISPasswordDulu;
};
function getItemLapRWIPasswordDulu_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .90,
			    layout: 'form',
				 labelWidth:120,
				  border: false,
				 
			    items:
				[
					{
						xtype: 'textfield',
						inputType: 'password',
						fieldLabel: 'Masukan Password ',
						id: 'TxPasswordDuluRwi',
					    anchor: '99%'
					},
				
					
				]
			},
     
        ]
    }
    return items;
};
function getItemPaneltgl_filter(){
    var items ={
		layout: 'column',
		border: false,
		items:[
			{
				layout: 'form',
				labelWidth: 100,
				border: false,
				items:[
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal ',
						id: 'dtpTglAwalFilterKasirrwi',
						name: 'dtpTglAwalFilterKasirrwi',
						format: 'd/M/Y',
						width: 100,
						value: limaharilalu,
						listeners:{
							'specialkey': function (){
								/*var tmpNoMedrec = Ext.get('txtFilterKasirNomedrec').getValue()
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10){
									RefreshDataFilterKasirrwiKasir();
								}*/
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
									RefreshDataFilterKasirrwiKasir();
								}
								// RefreshDataKasirrwiKasir();
							}
						}
					}
				]
			},{
				xtype: 'displayfield', value: '&nbsp;s/d&nbsp;'
			},{
				xtype: 'datefield',
				id: 'dtpTglAkhirFilterKasirrwi',
				name: 'dtpTglAkhirFilterKasirrwi',
				format: 'd/M/Y',
				width: 100,
				value: now,
				listeners:{
					'specialkey': function (){
						// var tmpNoMedrec = Ext.get('txtFilterKasirNomedrec').getValue()
						if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10){
							RefreshDataFilterKasirrwiKasir();
						}
					}
				}
			}
		]
	}
    return items;
}
function getItemPanelcombofilter(){
    var items ={
		layout: 'column',
		border: false,
		items:[
			{
				// columnWidth: .35,
				layout: 'form',
				labelWidth: 100,
				border: false,
				items:[
					mComboStatusBayar_viKasirRwiKasir()
				]
			},{
				// columnWidth: .35,
				layout: 'form',
				style:'padding-left: 4px;',
				border: false,
				labelWidth: 80,
				items:[
					// mCombokelas_kamarrwi(),
					mComboViewDataKasirRWI(),
					//mComboUnit_viKasirrwiKasir()
				]
			},{
				layout: 'form',
				labelWidth: 60,
				style:'padding-left: 4px;',
				border: false,
				items:[
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal ',
						id: 'dtpTglAwalFilterKasirrwi',
						name: 'dtpTglAwalFilterKasirrwi',
						format: 'd/M/Y',
						width: 100,
						value: limaharilalu,
						listeners:{
							'specialkey': function (){
								/*var tmpNoMedrec = Ext.get('txtFilterKasirNomedrec').getValue()
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10){
									RefreshDataFilterKasirrwiKasir();
								}*/
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
									RefreshDataFilterKasirrwiKasir();
								}
								// RefreshDataKasirrwiKasir();
							}
						}
					}
				]
			},{
				xtype: 'displayfield', value: '&nbsp;s/d&nbsp;'
			},{
				xtype: 'datefield',
				id: 'dtpTglAkhirFilterKasirrwi',
				name: 'dtpTglAkhirFilterKasirrwi',
				format: 'd/M/Y',
				width: 100,
				value: now,
				listeners:{
					'specialkey': function (){
						// var tmpNoMedrec = Ext.get('txtFilterKasirNomedrec').getValue()
						if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10){
							RefreshDataFilterKasirrwiKasir();
						}
					}
				}
			},{
				xtype: 'displayfield', value: '&nbsp;Hak Kelas&nbsp;'
			},{
				xtype: 'combo',
				id: 'dtp_HakKelas_FilterKasirrwi',
				name: 'dtp_HakKelas_FilterKasirrwi',
				typeAhead 		: true,
				triggerAction 	: 'all',
				lazyRender 		: true,
				mode 			: 'local',
				selectOnFocus 	: true,
				forceSelection 	: true,
				emptyText 		: '',
				width 			: 120,
				store: new Ext.data.ArrayStore({
					fields:['id', 'label'],
					data: [[0, 'Semua'], [1, 'Sesuai Kelas'], [2, 'Naik Kelas'], ]
				}),
				valueField: 'id',
				displayField: 'label',
				value: 0,
				listeners:{
					'select': function (a, b, c){
						RefreshDataFilterKasirrwiKasir();
					}
				}
			}
			// getItemPaneltgl_filter()
		]
	};
    return items;
}
function getParamDetailTransaksiRwi()
{
    var params =
            {
                Table: 'ViewTrKasirRwi',
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                //DeptId:Ext.get('txtKdDokter').dom.value ,tampungshiftsekarang
                Tgl: tgltrans,
                Shift: tampungshiftsekarang,
                List: getArr2DetailTrRwi(),
                JmlField: mRecordRwi.prototype.fields.length - 4,
                JmlList: GetListCoun2tDetailTransaksi(),
                Hapus: 1,
                Ubah: 0
            };
    return params
}
;

function getArr2DetailTrRwi()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirRwiList.getCount(); i++)
    {
        if (dsTRDetailKasirRwiList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirRwiList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirRwiList.data.items[i].data.URUT
            y += z + dsTRDetailKasirRwiList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirRwiList.data.items[i].data.QTY
            y += z + ShowDate(dsTRDetailKasirRwiList.data.items[i].data.TGL_BERLAKU)
            y += z + dsTRDetailKasirRwiList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirRwiList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirRwiList.data.items[i].data.URUT


            if (i === (dsTRDetailKasirRwiList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }

    return x;
}
;
function GetListCoun2tDetailTransaksi()
{

    var x = 0;
    for (var i = 0; i < dsTRDetailKasirRwiList.getCount(); i++)
    {
        if (dsTRDetailKasirRwiList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirRwiList.data.items[i].data.DESKRIPSI != '')
        {
            x += 1;
        }
        ;
    }
    return x;

}
;

function setLookUp_viPindahKamarRWI(rowdata)
{
    var label;
    if (jPanel == 1) {
        label = "Pindah";
    }else if (jPanel == 2){
        label = "Ganti";
    }else{
        label = "History";
    }

    var lebar = 500;
    var item;

    setLookUps_viPindahDokterRWI = new Ext.Window
	(
		{
			id: 'setLookUps_viPindahDokterRWI',
			name: 'setLookUps_viPindahDokterRWI',
			title: label+' Kamar Pasien Rawat Inap',
			closeAction: 'destroy',
			width: lebar,
			height: 500,
			resizable: false,
			autoScroll: false,
			iconCls: 'Studi_Lanjut',
			modal: true,
			items: getFormItemEntry_viDaftarRWIPindahKamar(lebar, rowdata), //1
			listeners:
			{
				activate: function ()
				{
				},
				afterShow: function ()
				{
				},
				deactivate: function ()
				{
					cek_rawat_gabung=false;
				}
			},
			fbar:[
				{
					xtype: 'button',
					text: 'Simpan Data Pindah',
					hideLabel: true,
					id: 'btnOkpgw',
					handler: function ()
					{
						savepindahkamar(cek_rawat_gabung);
					}
				},
			]
		}
	);

		Ext.getCmp('txtTanggalPerpindahan').show();
		Ext.getCmp('txtJamlPerpindahan').show();
		if (label == 'Ganti') {
			Ext.getCmp('txtTanggalPerpindahan').hide();
			Ext.getCmp('txtJamlPerpindahan').hide();
		}
	    if (rowdata == undefined)
	    {
	        //dataaddnew_viDaftarRWI();
	    } else
	    {
	        //datainit_viDaftarRWI(rowdata);
	    }
	setLookUps_viPindahDokterRWI.show();
	if(cek_rawat_gabung == true){
		datainitBayiRawatGabung_viDaftarRWI();
	}
}

function datainitBayiRawatGabung_viDaftarRWI(){
	
	Ext.getCmp('cboPoliklinik_gantkamar_panatajasarwi').setValue(Ext.getCmp('lblspesialisasi_LookupKunjunganIbu').getValue());
	Ext.getCmp('cboKelas_panatajasarwi').setValue(Ext.getCmp('lblKelas_LookupKunjunganIbu').getValue());
	Ext.getCmp('cboRuanganRawat_inap').setValue(Ext.getCmp('lblPoli_LookupKunjunganIbu').getValue());
	Ext.getCmp('cboKamar_panatajasarwi').setValue(Ext.getCmp('lblKamar_LookupKunjunganIbu').getValue());
	
	Ext.getCmp('id_cboPoliklinik_gantkamar_panatajasarwi').setValue(Ext.getCmp('txtspesialisasi_LookupKunjunganIbu').getValue());
	Ext.getCmp('id_cboKelas_panatajasarwi').setValue(Ext.getCmp('txtKelas_LookupKunjunganIbu').getValue());
	Ext.getCmp('id_cboRuanganRawat_inap').setValue(Ext.getCmp('txtPoli_LookupKunjunganIbu').getValue());
	Ext.getCmp('id_cboKamar_panatajasarwi').setValue(Ext.getCmp('txtKamar_LookupKunjunganIbu').getValue());
	tmpkdunitkamar = Ext.getCmp('txtKamar_LookupKunjunganIbu').getValue();
	viewsisakamar_panatajasarwi(true);
	
}

function getFormItemEntry_viDaftarRWIPindahKamar(lebar, rowdata)
{
    var pnlFormDataBasic_viDaftarRWI = new Ext.FormPanel
	(
		{
			title: '',
			region: 'north', //'center',
			layout: 'form', //'anchor',
			bodyStyle: 'padding:10px 15px 10px 15px',
			anchor: '100%',
			autoScroll: true,
			width: 500, //lebar-55,
			height: 500,
			border: false,
			items: [
				getItemPanelInputBiodata_viDaftarRWIGantiKamar(),
						//getPenelItemDataKunjungan_viDaftarRWI(lebar)
			],
			fileUpload: true,
			tbar:
					{
						xtype: 'toolbar',
						items: [
						]
					}
//                        ,items:

		}
	)


    return pnlFormDataBasic_viDaftarRWI;
}

function getItemPanelInputBiodata_viDaftarRWIGantiKamar(){
    var items =
    {
        layout: 'fit',
        border: false,
        labelAlign: 'top',
        items:
        [
            {
                columnWidth: .20,
                layout: 'form',
                labelWidth: 100,
                border: false,
                items:
                [ 
                    FieldsetRuanganLama(),
                    FieldsetRuanganBaru(),
                ]
            }
        ]
    };
    Ext.getCmp('cboPoliklinik_gantkamar_panatajasarwi').focus();
    return items;
};

function FieldsetRuanganLama(){
var items =
{
    layout: 'column',
    border: false,
    labelAlign: 'top',
    items:
    [{
        xtype : 'fieldset',
        title : 'Asal',
        layout: 'column',
        columnWidth: .97,
        border:true,
        bodyStyle:'margin: 5px',
        items:[
            {
                columnWidth: .40,
                layout: 'form',
                border: false,
                labelWidth: 90,
                items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Pasien',
                        maxLength: 100,
                        name: 'txtMedrecPasien_RWI',
                        id: 'txtMedrecPasien_RWI',
                        value : kodepasien,
                        width: 50,
                        disabled: true,
                        anchor: '95%'
                    },
                ]
            },{
                columnWidth: .60,
                layout: 'form',
                border: false,
                labelWidth: 90,
                items:
                [
                    {
                        xtype: 'textfield',
                        maxLength: 100,
                        name: 'txtNamaPasien_RWI',
                        id: 'txtNamaPasien_RWI',
                        value : namapasien,
                        width: 50,
                        disabled: true,
                        anchor: '100%'
                    },
                ]
            },{
                columnWidth: .40,
                layout: 'form',
                border: false,
                labelWidth: 90,
                items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kelas',
                        maxLength: 100,
                        name: 'txtKelas_RWI',
                        id: 'txtKelas_RWI',
                        value : tmpKelas,
                        width: 50,
                        disabled: true,
                        anchor: '95%'
                    },
                ]
            },{
                columnWidth: .60,
                layout: 'form',
                border: false,
                labelWidth: 90,
                items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kamar',
                        maxLength: 100,
                        name: 'txtKamar_RWI',
                        id: 'txtKamar_RWI',
                        value : tmpKamar,
                        width: 50,
                        disabled: true,
                        anchor: '100%'
                    },
                ]
            },{
                columnWidth: .40,
                layout: 'form',
                border: false,
                labelWidth: 90,
                items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Tanggal Masuk',
                        maxLength: 100,
                        name: 'txtTglMasuk_RWI',
                        id: 'txtTglMasuk_RWI',
                        value : rowSelectedKasirrwiKasir.data.TANGGAL_MASUK,
                        width: 50,
                        disabled: true,
                        anchor: '95%'
                    },
                ]
            },{
                columnWidth: .40,
                layout: 'form',
                border: false,
                labelWidth: 90,
                items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Jam Masuk',
                        maxLength: 100,
                        name: 'txtJamMasuk_RWI',
                        id: 'txtJamMasuk_RWI',
                        value : rowSelectedKasirrwiKasir.data.JAM_MASUK,
                        width: 50,
                        disabled: true,
                        anchor: '95%'
                    },
                ]
            },
        ]
    }]
};
return items;
};
function FieldsetRuanganBaru(){
var items =
{
    layout: 'column',
    border: false,
    labelAlign: 'top',
    items:
    [{
        xtype : 'fieldset',
        title : 'Tujuan',
        layout: 'column',
        columnWidth: .97,
        border:true,
        bodyStyle:'margin: 5px',
        items:[
            {
                columnWidth: .60,
                layout: 'form',
                labelWidth: 100,
                border: false,
                items:
                [
                    mComboRWIPoliklinik_gantkamar_panatajasarwi(),
					{
						xtype: 'textfield',
						name: 'id_cboPoliklinik_gantkamar_panatajasarwi',
						id: 'id_cboPoliklinik_gantkamar_panatajasarwi',
						hidden:true,
					},
					{
						xtype: 'textfield',
						name: 'id_cboKelas_panatajasarwi',
						id: 'id_cboKelas_panatajasarwi',
						hidden:true,
					},
					{
						xtype: 'textfield',
						name: 'id_cboRuanganRawat_inap',
						id: 'id_cboRuanganRawat_inap',
						hidden:true,
					},
					{
						xtype: 'textfield',
						name: 'id_cboKamar_panatajasarwi',
						id: 'id_cboKamar_panatajasarwi',
						hidden:true,
					},
                ]
            },{
                columnWidth: .40,
                layout: 'form',
                labelWidth: 100,
                border: false,
                items:
                [
                    mComboKelasSpesial_panatajasarwi(),
                ]
            },{
                columnWidth: .99,
                layout: 'form',
                border: false,
                labelWidth: 90,
                items:
                [
                    mComboRuangan_panatajasarwi(),
                ]
            },{
                columnWidth: .60,
                layout: 'form',
                border: false,
                labelWidth: 90,
                items:
                [
                    mComboKamar_panatajasarwi(),
                ]
            },
            {
                columnWidth: .40,
                layout: 'form',
                border: false,
                labelWidth: 90,
                items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Sisa Tempat Tidur',
                        maxLength: 100,
                        name: 'txtSisaKamar_panatajasarwi',
                        id: 'txtSisaKamar_panatajasarwi',
                        width: 50,
                        disabled: true,
                        anchor: '100%'
                    },
                ]
            },
            {
                columnWidth: .50,
                layout: 'form',
                border: false,
                labelWidth: 90,
                items:
                [
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Tanggal',
                        maxLength: 100,
                        name: 'txtTanggalPerpindahan',
                        id: 'txtTanggalPerpindahan',
                        width: 50,
                        disabled: false,
                        anchor: '100%',
                        value   : now
                    },
                ]
            },
            {
                columnWidth: .50,
                layout: 'form',
                border: false,
                labelWidth: 90,
                items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Jam',
                        maxLength: 100,
                        name: 'txtJamPerpindahan',
                        id: 'txtJamlPerpindahan',
                        width: 50,
                        disabled: false,
                        anchor: '100%',
                        value   : DefJamPerpindahan
                    },
                ]
            },
        ],
    }]
};
return items;
};

var spesialisasikamar;
var kdspesial;
var kelas_kamar;

function mComboRWIPoliklinik_gantkamar_panatajasarwi()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];

    ds_Poli_panatajasarwi_RWI2 = new WebApp.DataStore({fields: Field});
    ds_Poli_panatajasarwi_RWI2.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_spesial',
            Sortdir: 'ASC',
			target: 'ViewSetupSpesial',
			param: 'kd_spesial <> 0 AND aktif = ~1~'
        }
    })

    var cboPoliklinik_gantkamar_panatajasarwi = new Ext.form.ComboBox
    ({
        id: 'cboPoliklinik_gantkamar_panatajasarwi',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        selectOnFocus: true,
        forceSelection: true,
        emptyText: 'Pilih Spesialisasi...',
        fieldLabel: 'Spesialisasi ',
        align: 'Right',
        store: ds_Poli_panatajasarwi_RWI2,
        valueField: 'KD_SPESIAL',
        displayField: 'SPESIALISASI',
        anchor: '95%',
        listeners:
        {
            'select': function (a, b, c)
            {
                loaddatastorekelas_panatajasarwi(b.data.KD_SPESIAL);
                spesialisasikamar = b.data.KD_SPESIAL;
                //loaddatastoredokter_panatajasarwiSpesial(b.data.KD_SPESIAL);
                kdspesial = b.data.KD_SPESIAL;
            },
            'render': function (c)
            {
                c.getEl().on('keypress', function (e) {
                    if (e.getKey() == 13 || e.getKey() == 9) //atau Ext.EventObject.ENTER
                        Ext.getCmp('cboKelas_panatajasarwi').focus();
                }, c);
            }
        }
    })

    return cboPoliklinik_gantkamar_panatajasarwi;
}
;


function mComboKelasSpesial_panatajasarwi()
{
    var Field = ['KD_KELAS', 'KELAS', 'NAMA_UNIT', 'KD_UNIT'];

    dsKelasRawat_inap = new WebApp.DataStore({fields: Field});


    var cboKelas_panatajasarwi = new Ext.form.ComboBox
    ({
        id: 'cboKelas_panatajasarwi',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        selectOnFocus: true,
        forceSelection: true,
        emptyText: 'Pilih Kelas...',
        fieldLabel: 'Kelas ',
        align: 'Right',
        store: dsKelasRawat_inap,
        valueField: 'KD_KELAS',
        displayField: 'KELAS',
        anchor: '100%',
        listeners:
        {
            'select': function (a, b, c)
            {
                Ext.getCmp('cboRuanganRawat_inap').setValue('');
                loaddatastoreruangan_panatajasarwi(b.data.KD_KELAS)
                kelas_kamar = b.data.KD_KELAS;
            },
            'render': function (c)
            {
                c.getEl().on('keypress', function (e) {
                    if (e.getKey() == 13 || e.getKey() == 9) //atau Ext.EventObject.ENTER
                        Ext.getCmp('cboRuanganRawat_inap').focus();
                }, c);
            }
        }
    });

    return cboKelas_panatajasarwi;
}
;

function mComboRuangan_panatajasarwi()
{
    var Field = ['KD_UNIT', 'NAMA'];

    dsRuangan_panatajasarwi = new WebApp.DataStore({fields: Field});


    var cboRuanganRawat_inap = new Ext.form.ComboBox
    ({
        id: 'cboRuanganRawat_inap',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        selectOnFocus: true,
        forceSelection: true,
        emptyText: 'Pilih Ruangan...',
        fieldLabel: 'Ruang / Unit ',
        align: 'Right',
        store: dsRuangan_panatajasarwi,
        valueField: 'KD_UNIT',
        displayField: 'NAMA',
        anchor: '100%',
        listeners:
        {
            'select': function (a, b, c)
            {
                loaddatastorekamar_panatajasarwi(b.data.KD_UNIT)
            },
            'render': function (c)
            {
                c.getEl().on('keypress', function (e) {
                    if (e.getKey() == 13 || e.getKey() == 9) //atau Ext.EventObject.ENTER
                    Ext.getCmp('cboKamar_panatajasarwi').focus();
                }, c);
            }
        }
    }
    );
    return cboRuanganRawat_inap;
};

function mComboKamar_panatajasarwi()
{
    var Field = ['KD_UNIT', 'NO_KAMAR', 'NAMA_KAMAR'];
    dsKamarRawat_inap = new WebApp.DataStore({fields: Field});
    var cboKamar_panatajasarwi = new Ext.form.ComboBox
    ({
        id: 'cboKamar_panatajasarwi',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        selectOnFocus: true,
        forceSelection: true,
        emptyText: 'Pilih Kamar...',
        fieldLabel: 'Kamar ',
        align: 'Right',
        store: dsKamarRawat_inap,
        valueField: 'NO_KAMAR',
        displayField: 'NAMA_KAMAR',
        anchor: '95%',
        listeners:
        {
            'select': function (a, b, c)
            {
                tmpKdUnitKamar = b.data.KD_UNIT;
                viewsisakamar_panatajasarwi(false);
                //tmpNoKamar
                
            },
            'render': function (c)
            {
                c.getEl().on('keypress', function (e) {
                    if (e.getKey() == 13 || e.getKey() == 9) //atau Ext.EventObject.ENTER
                    Ext.getCmp('txtTanggalPerpindahan').focus();
                }, c);
            }
        }
    });

    return cboKamar_panatajasarwi;
}
;

function loaddatastorekelas_panatajasarwi(KD_SPESIAL)
{
    dsKelasRawat_inap.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_kelas',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboKelasSpesial_Revisi',
                                    param: 'where kd_spesial =~' + KD_SPESIAL + '~ AND aktif = ~t~'
                                }
                    }
            )
}
;

function loaddatastoreruangan_panatajasarwi(KD_KELAS)
{
    var kd_spesial = Ext.getCmp('cboPoliklinik_gantkamar_panatajasarwi').getValue();
    dsRuangan_panatajasarwi.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_unit',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboRuangan',
                                    param: 'where kd_spesial =~' + kd_spesial + '~' + ' And ' + 'k.kd_kelas =~' + KD_KELAS + '~'
                                }
                    }
            )

}

function loaddatastorekamar_panatajasarwi(KD_UNIT)
{
    var kriteriakamar = 'where no_kamar in ( select spc_kamar.no_Kamar from spc_kamar ';
    kriteriakamar += ' inner join unit on spc_kamar.kd_unit=unit.kd_unit inner join kelas k ON k.kd_kelas = UNIT.kd_kelas ';
    kriteriakamar += 'where  unit.kd_unit=~' + KD_UNIT + '~ and spc_kamar.kd_spesial = ' + spesialisasikamar + ' And k.kd_kelas =~' + kelas_kamar + '~)';
    kriteriakamar += 'and kd_unit in ( select spc_kamar.kd_unit from spc_kamar ';
    kriteriakamar += ' inner join unit on spc_kamar.kd_unit=unit.kd_unit inner join kelas k ON k.kd_kelas = UNIT.kd_kelas ';
    kriteriakamar += ' where  unit.kd_unit=~' + KD_UNIT + '~ and spc_kamar.kd_spesial = ' + spesialisasikamar + ' And k.kd_kelas =~' + kelas_kamar + '~)';
    /*
     
     
     
     )   */
    dsKamarRawat_inap.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'no_kamar',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboKamar',
                                    param: kriteriakamar
                                }
                    }
            )
}


function viewsisakamar_panatajasarwi(BRG)
{
    Ext.Ajax.request
    ({
        url: baseURL + "index.php/main/ReadData",
        params: datavaramsisakamar_KASIRRWI(BRG),
        success: function (o)
        {
            var cst = Ext.decode(o.responseText);
            if (cst.success === true)
            {
                Ext.get('txtSisaKamar_panatajasarwi').dom.value = cst.totalrecords;
            } else {
                Ext.get('txtSisaKamar_panatajasarwi').dom.value = 0;
            }
            if (cst.totalrecords <= 0)
            {
				if(BRG == false){
					ShowPesanInfoKasirrwi('Tidak ada kamar kosong, silahkan pilih kamar lain', 'Simpan Data');
				}
               
            }
        }
    })
}

function datavaramsisakamar_KASIRRWI(BRG)
{
    if(BRG == true){
		var params_ViPendaftaranRWI ={
			Table: 'View_SisaKamar',
			Spesialisasi: Ext.getCmp('id_cboPoliklinik_gantkamar_panatajasarwi').getValue(),
			Kelas: Ext.getCmp('id_cboKelas_panatajasarwi').getValue(),
			Ruangan: Ext.getCmp('id_cboRuanganRawat_inap').getValue(),
			Kamar: Ext.getCmp('id_cboKamar_panatajasarwi').getValue(),
			KDUnitKamar: tmpkdunitkamar
		};
	}else{
		var params_ViPendaftaranRWI =
		{
			Table: 'View_SisaKamar',
			Spesialisasi: Ext.getCmp('cboPoliklinik_gantkamar_panatajasarwi').getValue(),
			Kelas: Ext.getCmp('cboKelas_panatajasarwi').getValue(),
			Ruangan: Ext.getCmp('cboRuanganRawat_inap').getValue(),
			Kamar: Ext.getCmp('cboKamar_panatajasarwi').getValue(),
			KDUnitKamar: tmpKdUnitKamar
		};
	}
	
	
    return params_ViPendaftaranRWI

}


function dataparam_PindahKamar()
{
    var params_pindahkamar =
    {
        //Table: 'vipindahkamar',
        NoMedrec        : kodepasien,
        NoTransaksi     : notransaksi,
        // Poli            : Ext.getCmp('cboPoliklinik_gantkamar_panatajasarwi').getValue(),
        // Kamar           : Ext.getCmp('cboKamar_panatajasarwi').getValue(),
        // Kelas           : Ext.getCmp('cboKelas_panatajasarwi').getValue(),
        // Ruang           : Ext.getCmp('cboRuanganRawat_inap').getValue(),
        // KDUnitKamar     : Ext.getCmp('cboKamar_panatajasarwi').getValue(),
        tgl_pindah      : Ext.getCmp('txtTanggalPerpindahan').getValue(),
        UnitLast        : rowSelectedKasirrwiKasir.data.KD_UNIT_KAMAR,
        kd_unit_kunjungan: rowSelectedKasirrwiKasir.data.KD_UNIT,
        tgltransaksi    : tgltrans,
        tglmasuk        : tmpTanggalMasuk,
        Act             : actKamar,
        Urut_masuk      : urutmasuk,
		no_kamar        : tmpNoKamar,
		bayi_rawat_gabung   : cek_rawat_gabung,

    };
	
	if(cek_rawat_gabung == true){
		params_pindahkamar['Poli'] 			= Ext.getCmp('id_cboPoliklinik_gantkamar_panatajasarwi').getValue();
		params_pindahkamar['Kelas'] 		= Ext.getCmp('id_cboKelas_panatajasarwi').getValue();
		params_pindahkamar['Kamar'] 		= Ext.getCmp('id_cboKamar_panatajasarwi').getValue();
		params_pindahkamar['KDUnitKamar'] 	= Ext.getCmp('id_cboKamar_panatajasarwi').getValue();
		params_pindahkamar['Ruang'] 		= Ext.getCmp('id_cboRuanganRawat_inap').getValue();
	}else{
		params_pindahkamar['Poli'] 			= Ext.getCmp('cboPoliklinik_gantkamar_panatajasarwi').getValue();
		params_pindahkamar['Kelas'] 		= Ext.getCmp('cboKelas_panatajasarwi').getValue();
		params_pindahkamar['Kamar'] 		= Ext.getCmp('cboKamar_panatajasarwi').getValue();
		params_pindahkamar['KDUnitKamar'] 	= Ext.getCmp('cboKamar_panatajasarwi').getValue();
		params_pindahkamar['Ruang'] 		= Ext.getCmp('cboRuanganRawat_inap').getValue();
	}
    return params_pindahkamar
}
function savepindahkamar_SQL(mBol)
{
        Ext.Ajax.request
        ({
            url: baseURL + "index.php/rawat_inap_sql/functionRWI/pindahgantikamar",   
            params: dataparam_PindahKamar(),
            success: function (o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    
                } else
                {
                    ShowPesanErrorKasirrwi('SQL. Data tidak berhasil di simpan !', 'Simpan Data');
                }
            }
        })
}
function savepindahkamar(BRG)
{
	console.log(BRG);
    var Vurl;
    if (jPanel==1) {
        Vurl= baseURL + "index.php/rawat_inap/control_pindah_kamar/pindah_kamar";
    }else if(jPanel==2){
        Vurl= baseURL + "index.php/rawat_inap/control_pindah_kamar/ganti_kamar";
    }
	
	var status_warning=0;
    if (Ext.getCmp('cboKamar_panatajasarwi').getValue() == tmpNoKamar && tmpKelas == Ext.getCmp('cboKelas_panatajasarwi').getValue()) {
        ShowPesanInfoKasirrwi('Harap tidak dipindahkan ke kamar yang sama', 'Informasi');
		status_warning =1;
	}
	
	if(BRG == false){
		if (Ext.getCmp('txtSisaKamar_panatajasarwi').getValue() =='0' || Ext.getCmp('txtSisaKamar_panatajasarwi').getValue() == 0 ) {
			ShowPesanInfoKasirrwi('Kamar sudah terisi pernuh', 'Informasi');
			status_warning =1;
		}
	}
	
	console.log(status_warning);	
	if(status_warning == 0){	
        Ext.Ajax.request
    	({
            url     : Vurl,
    		params  : dataparam_PindahKamar(BRG),
    		success : function (o)
    		{
    			var cst = Ext.decode(o.responseText);
    			if (cst.success === true)
    			{
    				ShowPesanInfoKasirrwi('Data berhasil di simpan', 'Simpan Data');
                    tmpkd_unitKamar = cst.kd_unit;
                    tmpkd_unit 		= cst.kd_unit;
					tmpUnit 		= cst.kd_unit;
                    Vurl = "";
                    jPanel=0;

                    grListTRKasirrwi.getStore().getRange()[dataRowIndex].set('KETERANGAN', cst.spesialisasi+"/ "+cst.kelas+"/ "+cst.no_kamar+" [ "+cst.nama_kamar+" ]");
                    //dataRowIndex
                    // if (cst.ganti_kamar == true) {
                    // }
                    RefreshDataKasirrwiKasir();
                    //RefreshDataDetail_kasirrwi();
    				setLookUps_viPindahDokterRWI.close();
    			} else if (cst.success === false && cst.pesan === 0)
    			{
    				ShowPesanWarningKasirrwi('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
    			} else
    			{
    				ShowPesanErrorKasirrwi('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
    			}
    		}
    	})
    }
}

function GetDTLTRGridTindakanYangDiberikanKasirRWI() {
	/*
		PERBARUAN SELECTED ROW SINGLE SELECT
		OLEH    : HADAD AL GOJALI
		TANGGAL : 2017 - 01 -09
		ALASAN  : TIDAK MENGYIMPAN DATA PADA TMP VARIABLE
	*/
    var fldDetail = ['KD_PRODUK', 'KP_PRODUK', 'KD_UNIT', 'DESKRIPSI', 'QTY', 'DOKTER', 'TGL_TINDAKAN', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'KD_UNIT_TR', 
		'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'KD_TARIF', 'HARGA', 'JUMLAH_DOKTER', 'TARIF','KD_KLAS','MANUAL', 'NO_FAKTUR', 'FOLIO', 'GROUP'];
    dsTRDetailKasirRWIList_panatajasarwi = new WebApp.DataStore({fields: fldDetail});
    // RefreshDataDetail_kasirrwi();
    PenataJasaKasirRWI.dsGridTindakan = dsTRDetailKasirRWIList_panatajasarwi;
    PenataJasaKasirRWI.form.Grid.produk = new Ext.grid.EditorGridPanel({
        title: 'Tindakan Yang Diberikan',
        id: 'PjTransGrid2',
        stripeRows: true,
        height: 90,
        //plugins: [editor],
        store: PenataJasaKasirRWI.dsGridTindakan,
        border: false,
        frame: false,
        columnLines: true,
        anchor: '100% 80%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
                    cellSelecteddeskripsi_kasir_rwi = dsTRDetailKasirRWIList_panatajasarwi.getAt(row);
                    var line = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
                    dataRowIndexDetail = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
                    Current_kasirrwi.row = row;
                    tmp_group_dokter = cellSelecteddeskripsi_kasir_rwi.data.GROUP;
                    Current_kasirrwi.data = cellSelecteddeskripsi_kasir_rwi;
                    tmpkd_produk    = cellSelecteddeskripsi_kasir_rwi.data.KD_PRODUK;
                    tmpkd_tarif     = cellSelecteddeskripsi_kasir_rwi.data.KD_TARIF;
                    tmpharga        = cellSelecteddeskripsi_kasir_rwi.data.HARGA;
                    //tmpkd_unit      = cellSelecteddeskripsi_kasir_rwi.data.KD_UNIT;
                    //tmpno_transaksi = cellSelecteddeskripsi_kasir_rwi.data.NO_TRANSAKSI;
                    tmptgl_transaksi= cellSelecteddeskripsi_kasir_rwi.data.TGL_TRANSAKSI;
                    tmpRowGrid      = cellSelecteddeskripsi_kasir_rwi.data.URUT;
                    // cekTransferTindakan(false);
                    currentJasaDokterUrutDetailTransaksi_KasirRWI = cellSelecteddeskripsi_kasir_rwi.data.URUT;
                    currentJasaDokterKdProduk_KasirRWI =  cellSelecteddeskripsi_kasir_rwi.data.KD_PRODUK;
                    currentJasaDokterKdTarif_KasirRWI = cellSelecteddeskripsi_kasir_rwi.data.KD_TARIF;
                    currentJasaDokterHargaJP_KasirRWI = cellSelecteddeskripsi_kasir_rwi.data.HARGA;
					PenataJasaKasirRWI.KD_PRODUK = cellSelecteddeskripsi_kasir_rwi.data.KD_PRODUK;
                    PenataJasaKasirRWI.QTY = cellSelecteddeskripsi_kasir_rwi.data.QTY;
                    PenataJasaKasirRWI.TGL_BERLAKU = cellSelecteddeskripsi_kasir_rwi.data.TGL_BERLAKU;
                    PenataJasaKasirRWI.HARGA = cellSelecteddeskripsi_kasir_rwi.data.HARGA;
                    PenataJasaKasirRWI.KD_TARIF = cellSelecteddeskripsi_kasir_rwi.data.KD_TARIF;
                    PenataJasaKasirRWI.TGL_TRANSAKSI = cellSelecteddeskripsi_kasir_rwi.data.TGL_TRANSAKSI;
                    PenataJasaKasirRWI.TGL_TRANSAKSI_detailtransaksi = cellSelecteddeskripsi_kasir_rwi.data.TGL_TRANSAKSI;
                    PenataJasaKasirRWI.DESKRIPSI = cellSelecteddeskripsi_kasir_rwi.data.DESKRIPSI;
                }
            }
        }),
        cm: TRRawatInapColumModel2(),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                jstrdok = PenataJasaKasirRWI.dsGridTindakan.getAt(ridx);
                PenataJasaKasirRWI.URUT = jstrdok.data.URUT;
                PenataJasaKasirRWI.TGL_TRANSAKSI = jstrdok.data.TGL_TRANSAKSI;
				/* if (jstrdok.data.NO_TRANSAKSI !== '')
					{
						cekKeVisiteDokter(jstrdok.data.NO_TRANSAKSI, tanggaltransaksitampung,jstrdok.data.KD_PRODUK,jstrdok.data.URUT)
					} */
            }
		}, tbar: [
            {
                text: 'Tambah',
                id: 'btnLookupRWI_PenataJasaKasirRWI',
                tooltip: 'Tambah Item',
                iconCls: 'add',
                handler: function () {
					var tmpTanggalDefault 	= Ext.getCmp('txtTanggalTransaksiKASIRRWI').getValue();
					if (tmpTanggalDefault.format("Y-m-d")<tmptgl_transaksiInduk.substring(0, 10)) {
						ShowPesanWarningKasirrwi("Tanggal default setting tidak boleh kurang dari tanggal transaksi !",'Peringatan');
					}else if(now.format("Y-m-d") < tmpTanggalDefault.format("Y-m-d")){
						ShowPesanWarningKasirrwi("Tanggal default setting tidak boleh melebihi dari tanggal sekarang !",'Peringatan');
					}else{
	                    PenataJasaKasirRWI.dsGridTindakan.insert(PenataJasaKasirRWI.dsGridTindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
	                    Ext.getCmp('PjTransGrid2').startEditing(PenataJasaKasirRWI.dsGridTindakan.getCount()-1,4);
						batal_isi_tindakan='y';
					}
                }
            },
			/* {
				text: 'Simpan',
				id: 'btnSimpanIGD',
				tooltip: nmSimpan,
				iconCls: 'save',
				disabled :true,
				handler: function()
				{	
					Datasave_PenataJasaKasirRWI_on_kasirrrwi(false,false);
				}
			}, */ 
			{
				id: 'btnHapusBaris_PenataJasaKasirRWI',
				text: 'Hapus',
				tooltip: 'Hapus Baris',
				iconCls: 'RemoveRow',
				handler: function () {
					// var dlg = Ext.MessageBox.prompt('Password', 'Masukan password untuk menghapus :', function(btn, combo){
					// 	if (btn == 'ok'){
					// 		Ext.Ajax.request({
					// 			url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
					// 			params 		: {
					// 				select 	: " COALESCE(count(*),0) as count ",
					// 				where 	: " key_data = 'rwi_password_hapus_tindakan' AND setting = md5('"+combo+"') ",
					// 				table 	: " sys_setting ",
					// 			},
					// 			success: function(o){
					// 				var cst 	= Ext.decode(o.responseText);
					// 				if (cst[0].count > 0) {
										if (cellSelecteddeskripsi_kasir_rwi.data.KD_KLAS != '9' || cellSelecteddeskripsi_kasir_rwi.data.MANUAL == 't') {
											HapusBaris_panatajasarwi();
										}else{
											ShowPesanWarningKasirrwi('Produk transfer tidak dapat dihapus !','Peringatan');
										}
					// 				}else{
					// 					ShowPesanWarningKasirrwi('Password salah !','Peringatan');
					// 				}
					// 			}
					// 		});
					// 	}
					// });
				}
            },'-'
			/*
            {
                id      :'btnLookUpEditDokterPenindak_KasirRWI',
                text    : 'Edit Dokter Penindak',
                iconCls : 'Edit_Tr',
                handler : function(){
                    //loadDataPerawat_KasirRWI();
                    loaddatastoredokter_REVISI();
                    loaddatastoredokterVisite_REVISI();
                    if(cellSelecteddeskripsi_kasir_rwi == '' || cellSelecteddeskripsi_kasir_rwi == undefined){
                        ShowPesanWarningKasirrwi('Pilih item dokter penindak yang akan diedit!','Error');
                    } else{
                        if(currentJasaDokterUrutDetailTransaksi_KasirRWI == 0 || currentJasaDokterUrutDetailTransaksi_KasirRWI == undefined){
                            ShowPesanErrorKasirrwi('Item ini belum ada dokter penindaknya!','Error');
                        } else{
                            //PilihDokterLookUpKasir_RWI(true);
                            PilihDokterLookUpKasir_RWI_REVISI();
                        }
                    }
                }
            },'-',*/
            ,{
                id      :'btnLookUpProduk_RawatInap',
                text    : 'Lookup Produk',
                iconCls : 'Edit_Tr',
                handler : function(){
                	formLookUpProduk_KasirRawatInap();
                }
            },'-',{
				xtype: 'buttongroup',
				columns: 7,
				defaults: {
					scale: 'small'
				},
				items: [
					{
						xtype:'splitbutton',
						text: 'Option',
						iconCls: 'Edit_Tr',
						arrowAlign:'right',
						menu: [
							{
								id      :'btnLookUpPreviewBilling',
								text    : 'Preview Billing',
								iconCls : 'Edit_Tr',
								hidden 	: true,
								handler : function(){
									GetDTLTRGridPreviewBilling();
								}
							},{
								id      :'btnLookUpPostingManual_KASIRRWI',
								text    : 'Posting Manual',
								iconCls : 'Edit_Tr',
								handler : function(){
									dsDataStoreGridPoduk.removeAll();
									formLookupPostingManual_KASIRRWI();
								}
							}
						]
					}
				]
			}
            /*{
                id      :'btnLookUpPreviewBilling',
                text    : 'Preview Billing',
                iconCls : 'Edit_Tr',
                handler : function(){
                	GetDTLTRGridPreviewBilling();
                }
            },'-',
            {
                // PENAMBAHAN POSTING MANUAL 2017 - 02 -27
                id      :'btnLookUpPostingManual_KASIRRWI',
                text    : 'Posting Manual',
                iconCls : 'Edit_Tr',
                handler : function(){
                    dsDataStoreGridPoduk.removeAll();
                    formLookupPostingManual_KASIRRWI();
                }
            }*/
			,'-',
			{
				xtype: 'checkbox',
				hidden:true,
				//boxLabel: 'Perawat',
				id: 'checkTindakanMenyusul_KASIRRWI',
				hideLabel: true,
				margin: '0 10 0 20',
				checked : false,
				value : true,
				listeners: {
					check: function (checkbox, isChecked) {
						// cboDataTindakanMenyusul_KASIRRWI
						if (isChecked == true) {
							Ext.getCmp('cboDataTindakanMenyusul_KASIRRWI').enable();
						}else{
							Ext.getCmp('cboDataTindakanMenyusul_KASIRRWI').disable();
						}
					}
				}
			},
			//'-',
			mCombo_TindakanMenyusul_KASIRRWI(),
			{
				xtype: 'checkbox',
				//boxLabel: 'Perawat',
				id: 'checkPerawat_KASIRRWI',
				hideLabel: true,
				hidden:true,
				margin: '0 10 0 20',
				checked : false,
				value : true,
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked == true) {
							tmpKdJob = 3;
							Ext.getCmp('cboDataPerawatPenindak_KASIR_RWI').enable();
						}else{
							tmpKdJob    = 1;
							Ext.getCmp('cboDataPerawatPenindak_KASIR_RWI').disable();
							Ext.getCmp('cboDataPerawatPenindak_KASIR_RWI').setValue('');
							tmpKdDokter = "";
						}
					}
				}
			},
			//'-',
			mCombo_Perawat_KASIRRWI(),
           // '-',
            {
				xtype: 'checkbox',
				// boxLabel: 'Dokter',
				id: 'checkDokter_KASIRRWI',
				hideLabel: true,
				hidden:true,
				margin: '0 10 0 20',
				checked : false,
				value : true,
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked == true) {
							tmpKdJob = 3;
							Ext.getCmp('cboDataDokterPenindak_KASIR_RWI').enable();
						}else{
							tmpKdJob    = 1;
							Ext.getCmp('cboDataDokterPenindak_KASIR_RWI').disable();
							Ext.getCmp('cboDataDokterPenindak_KASIR_RWI').setValue('');
							tmpKdDokter = "";
						}
					}
				}
			},
			//'-',
			mCombo_dokter_KASIRRWI(),
            {
                xtype: 'datefield',
                fieldLabel: 'Tanggal Transaksi',
                maxLength: 100,
                name: 'txtTanggalTransaksiKASIRRWI',
                id: 'txtTanggalTransaksiKASIRRWI',
                width: 100,
                disabled: false,
                anchor: '100%',
                format: 'd/m/Y',
                value   : now,
				listeners: {
					render: function (c){
						c.getEl().on('keypress', function (e) {
							if (e.getKey() == 13){
								var tmpTanggalDefault 	= Ext.getCmp('txtTanggalTransaksiKASIRRWI').getValue();
								if (tmpTanggalDefault.format("Y-m-d")<tmptgl_transaksiInduk.substring(0, 10)) {
									Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
								}else if(now.format("Y-m-d") < tmpTanggalDefault.format("Y-m-d")){
									Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
								}else{
									Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').enable();
								}
								/*
								if (tmpTanggalDefault.format("Y-m-d")==tmptgl_transaksi.substring(0, 10)) { Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').enable(); }else{
									Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
								}*/
								//YYYY-MM-DD
							}
						}, c);
                    }
				}
			}
		],
        viewConfig: {forceFit: true},
    });
	loaddatastoredokter_REVISI();
	Ext.getCmp('cboDataPerawatPenindak_KASIR_RWI').disable();
	Ext.getCmp('cboDataDokterPenindak_KASIR_RWI').disable();
    return PenataJasaKasirRWI.form.Grid.produk;
};
function GantiDokterLookUp_penatajasarwi_penatajasarwi(mod_id){
    var FormDepanDokter_penatajasarwi = new Ext.Panel({
		id: mod_id,
		closable: true,
		region: 'center',
		layout: 'form',
		title: 'Ganti Dokter',
		border: false,
		shadhow: true,
		autoScroll: false,
		iconCls: 'Request',
		margins: '0 5 5 0',
		items: [DokterLookUp_penatajasarwi()]
	});
    return FormDepanDokter_penatajasarwi;
}
function DokterLookUp_penatajasarwi(rowdata){
    var lebar = 350;
    FormLookUpGantidokter_panatajasarwi = new Ext.Window({
		id: 'gridDokter_penatajasarwi',
		title: 'Ganti Dokter',
		closeAction: 'destroy',
		width: lebar,
		height: 180,
		border: false,
		resizable: false,
		plain: true,
		layout: 'fit',
		constrain: true,
		iconCls: 'Request',
		modal: true,
		items: [getFormEntryDokter_penatajasarwi(lebar)],
	});
    FormLookUpGantidokter_panatajasarwi.show();
}
function getItemPanelButtonGantidokter_penatajasarwi(lebar){
    var items ={
		layout: 'column',
		border: false,
		height: 39,
		anchor: '100%',
		style: {'margin-top': '-1px'},
		items:[
			{
				layout: 'hBox',
				width: 310,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: {margins: '3 3 3 3'},
				anchor: '90%',
				layoutConfig:{
					align: 'middle',
					pack: 'end'
				},
				items:[
					{
						xtype: 'button',
						text: 'Simpan',
						width: 100,
						style: {'margin-left': '0px', 'margin-top': '0px'},
						hideLabel: true,
						id: 'btnOkGantiDokter',
						handler: function (){
							GantiDokterPasienLookUp_rwi_REVISI();
							//FormLookUpGantidokter_panatajasarwi.close();
						}
					},{
						xtype: 'button',
						text: 'Tutup',
						width: 70,
						hideLabel: true,
						id: 'btnCancelGantidokter',
						handler: function (){
							FormLookUpGantidokter_panatajasarwi.close();
						}
					}
				]
			}
		]
	};
    return items;
}
function getFormEntryDokter_penatajasarwi(lebar){
    var pnlTRGantiDokter_penatajasarwi = new Ext.FormPanel({
		id: 'PanelTRDokter',
		fileUpload: true,
		region: 'north',
		layout: 'column',
		bodyStyle: 'padding:10px 10px 10px 10px',
		height: 190,
		anchor: '100%',
		width: lebar,
		border: false,
		items: [getItemPanelInputGantidokter_penatajasarwi(lebar), getItemPanelButtonGantidokter_penatajasarwi(lebar)],
	});
	var FormDepanDokter_penatajasarwi = new Ext.Panel({
		id: 'FormDepanDokter_penatajasarwi',
		region: 'center',
		width: '100%',
		anchor: '100%',
		layout: 'form',
		title: '',
		bodyStyle: 'padding:15px',
		border: true,
		bodyStyle: 'background:#FFFFFF;',
				shadhow: true,
		items: [pnlTRGantiDokter_penatajasarwi]
	});
    return FormDepanDokter_penatajasarwi;
}
function getItemPanelInputGantidokter_penatajasarwi(lebar){
    var items ={
		layout: 'fit',
		anchor: '100%',
		width: lebar - 35,
		labelAlign: 'right',
		bodyStyle: 'padding:10px 10px 10px 0px',
		border: false,
		height: 95,
		items:[
			{
				columnWidth: .9,
				width: lebar - 35,
				labelWidth: 100,
				layout: 'form',
				border: false,
				items:[
					getItemPanelNoTransksiDokter_penatajasarwi(lebar)
				]
			}
		]
	};
    return items;
}
function ValidasiEntryTutupDokter_penatajasarwi(modul, mBolHapus){
    var x = 1;
    if (Ext.get('txtNilaiDokter').getValue() == '' || (Ext.get('txtNilaiDokterSelanjutnya').getValue() == '')){
        if (Ext.get('txtNilaiDokter').getValue() == '' && mBolHapus === true){
            x = 0;
        }else if (Ext.get('txtNilaiDokterSelanjutnya').getValue() === ''){
            x = 0;
            if (mBolHapus === false){
                ShowPesanWarningKasirrwi(nmGetValidasiKosong(nmSatuan), modul);
            }
        }
    }
    return x;
}
function getItemPanelNoTransksiDokter_penatajasarwi(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: 1.0,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Unit Asal ',
                                                name: 'cmbUnitAsal_penatajasarwi',
                                                id: 'cmbUnitAsal_penatajasarwi',
                                                value: namaunit,
                                                readOnly: true,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Dokter Asal ',
                                                name: 'cmbDokterAsal_penatajasarwi',
                                                id: 'cmbDokterAsal_penatajasarwi',
                                                value: tmpNamaDokter,
                                                readOnly: true,
                                                anchor: '100%'

                                            },
                                            mComboDokterGantiEntryl_penatajasarwi()


                                        ]


                            }


                        ]
            };
    return items;
};

function mComboDokterGantiEntryl_penatajasarwi()
{
    dsDokterGantiEntry_penatajasarwi = new WebApp.DataStore({fields: Field});
    var kDUnit = '';//Ext.get('txtKdUnit_penatajasarwi').getValue();
    var kddokter = '';//Ext.get('txtKdDokter_penatajasarwi').getValue();

    dsDokterGantiEntry_penatajasarwi.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'nama',
                Sortdir: 'ASC',
                target: 'ViewComboDokterSpesial',
                param: 'dokter_spesial.kd_spesial =' + tmpKdSpesial
            }
        }
    )
    var Field = ['KD_DOKTER', 'NAMA'];

    var cboDokterGantiEntry_penatajasarwi = new Ext.form.ComboBox
            (
                    {
                        id: 'cboDokterGantiEntry_penatajasarwi',
                        typeAhead: true,
                        triggerAction: 'all',
                        name: 'txtdokter',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Dokter...',
                        fieldLabel: 'Dokter Baru',
                        align: 'Right',
                        store: dsDokterGantiEntry_penatajasarwi,
                        valueField: 'KD_DOKTER',
                        displayField: 'NAMA',
                        anchor: '100%',
                        listeners:
                                {
                                    /*'select': function (a, b, c)
                                    {

                                        selectDokter = b.data.KD_DOKTER;
                                        NamaDokter = b.data.NAMA;
                                        Ext.get('txtKdDokter_penatajasarwi').dom.value = b.data.KD_DOKTER;
                                    },
                                    'render': function (c)
                                    {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() == 13) // atau
                                                // Ext.EventObject.ENTER
                                                Ext.getCmp('kelPasien_penatajasarwi').focus();
                                        }, c);
                                    }*/
                                }
                    }
            );

    return cboDokterGantiEntry_penatajasarwi;

}
;

/*-----------Update Dokter----------------------------------*/
function GantiDokter_penatajasarwi(mBol)
{
    if (ValidasiGantiDokter_penatajasarwi(nmHeaderSimpanData, false) == 1)
    {

        Ext.Ajax.request
                (
                        {
                            url: WebAppUrl.UrlUpdateData,
                            params: getParamGantiDokter_penatajasarwi(),
                            failure: function (o)
                            {
                                ShowPesanErrorKasirrwi('Ganti Dokter Gagal', 'Ganti Dokter');
                            },
                            success: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfoKasirrwi(nmPesanSimpanSukses, 'Ganti Dokter');
									GantiDokter_SQL();
                                    Ext.get('txtKdDokter_penatajasarwi').dom.value = selectDokter;
                                    Ext.get('txtNamaDokter_penatajasarwi').dom.value = NamaDokter;
                                    FormDepanDokter_penatajasarwi.close();
                                    FormLookUpGantidokter_panatajasarwi.close();
                                } else if (cst.success === false && cst.pesan === 0)
                                {
                                    ShowPesanErrorKasirrwi('Ganti Dokter Gagal', 'Ganti Dokter');
                                } else
                                {
                                    ShowPesanErrorKasirrwi('Ganti Dokter Gagal', 'Ganti Dokter');
                                }
                                ;
                            }
                        }
                );

    } else
    {
        if (mBol === true)
        {
            return false;
        }
        ;
    }
    ;

}
;// END FUNCTION TutupDokterSave

/*---------------------------------------------*/

function ValidasiGantiDokter_penatajasarwi(modul, mBolHapus)
{
    var x = 1;
    if ((Ext.get('cboDokter_panatajasarwi').getValue() == ''))
    {
        if (Ext.get('cboDokter_panatajasarwi').getValue() === '')
        {
            x = 0;
            if (mBolHapus === false)
            {
                ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan), modul);
            }
            ;

        }
        ;
    }
    ;


    return x;
}
;


function getParamGantiDokter_penatajasarwi() {
    var params = {
        Table: 'ViewGantiDokter',
        TxtMedRec: Ext.get('txtNoMedrecDetransaksi_penatajasarwi').getValue(),
        TxtTanggal: Ext.get('dtpTanggalDetransaksi_penatajasarwi').getValue(),
        KdUnit: Ext.get('txtKdUnit_penatajasarwi').getValue(),
        KdDokter: selectDokter,
        kodebagian: 2,
		urut_masuk:Ext.getCmp('txtKdUrutMasuk_penatajasarwi').getValue()
    };
    return params;
}
;

/*-----AKHIR LOOK UP GANTI DOKTER BESERTA ELEMENYNYA-----------------------------*/


/*------------AWAL LOOK UP GANTI KD CUSTOMER PASIEN BESERTA ELEMENNYA -------------- */

function KelompokPasienLookUp_penatajasarwi(rowdata)
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien = new Ext.Window
            (
                    {
                        id: 'gridKelompokPasien',
                        title: 'Ganti Kelompok Pasien',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 300,
                        border: false,
                        resizable: false,
                        plain: false,
                        constrain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryTRKelompokPasien_penatajasarwi(lebar),
                        listeners:
						{
						},
						fbar:[
							{
								xtype: 'button',
								text: 'Simpan',
								width: 70,
								style: {'margin-left': '0px', 'margin-top': '0px'},
								hideLabel: true,
								id: 'btnOkKelompokPasien',
								handler: function ()
								{

									Datasave_Kelompokpasien();

								}
							},
							{
								xtype: 'button',
								text: 'Tutup',
								width: 70,
								hideLabel: true,
								id: 'btnCancelKelompokPasien',
								handler: function ()
								{
									FormLookUpsdetailTRKelompokPasien.close();
								}
							}
						]
                    }
            );

    FormLookUpsdetailTRKelompokPasien.show();
    KelompokPasienbaru_penatajasarwi();

}
;


function getFormEntryTRKelompokPasien_penatajasarwi(lebar)
{
    var pnlTRKelompokPasien_penatajasarwi = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKelompokPasien',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 300,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputKelompokPasien_penatajasarwi(lebar)],
                        tbar:
                                [
                                ]
                    }
            );

    var FormDepanKelompokPasien_penatajasarwi = new Ext.Panel
            (
                    {
                        id: 'FormDepanKelompokPasien_penatajasarwi',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        items: [pnlTRKelompokPasien_penatajasarwi

                        ]

                    }
            );

    return FormDepanKelompokPasien_penatajasarwi;
}
;

function getItemPanelInputKelompokPasien_penatajasarwi(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: false,
                height: 250,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getKelompokpasienlama_penatajasarwi(lebar), 
                                            getItemPanelNoTransksiKelompokPasien_penatajasarwi(lebar),
                                        ]
                            }
                        ]
            };
    return items;
};
function RefreshDatacombo_penatajasarwi(jeniscus)
{

    ds_customer_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: '',
                                    target: 'ViewComboKontrakCustomer',
                                    param: 'jenis_cust=~' + jeniscus + '~ '
                                }
                    }
            );

    // rowSelectedKasirRWI = undefined;
    return ds_customer_viDaftar;
}
;
function mComboKelompokpasien_penatajasarwi()
{

    var Field_poli_viDaftar = ['KD_CUSTOMER', 'CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: '',
                                    target: 'ViewComboKontrakCustomer',
                                    param: 'jenis_cust=~' + jeniscus + '~'
                                }
                    }
            );
    var cboKelompokpasien_penatajasarwi = new Ext.form.ComboBox
            (
                    {
                        id: 'cboKelompokpasien_penatajasarwi',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih...',
                        fieldLabel: labelisi,
                        align: 'Right',
                        anchor: '95%',
                        store: ds_customer_viDaftar,
                        valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectSetKelompokpasien = b.data.displayText;
                                        selectKdCustomer = b.data.KD_CUSTOMER;
                                        selectNamaCustomer = b.data.CUSTOMER;

                                    }
                                }
                    }
            );
    return cboKelompokpasien_penatajasarwi;
}
;
function getKelompokpasienlama_penatajasarwi(lebar)
{
    var items =
            {
                Width: lebar,
                height: 40,
                layout: 'column',
                border: false,
                // title: 'Kelompok Pasien Lama',
                items:
                        [
                            {
                                columnWidth: .990,
                                layout: 'form',
                                Width: lebar - 10,
                                labelWidth: 130,
                                border: false,
                                // title: 'Kelompok Pasien Lama',
                                items:
                                        [
                                            {
                                                xtype: 'tbspacer',
                                                height: 2
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Kelompok Pasien Asal',
                                                // maxLength: 200,
                                                name: 'txtCustomer_penatajasarwiLama',
                                                id: 'txtCustomer_penatajasarwiLama',
                                                labelWidth: 130,
                                                width: 100,
                                                anchor: '95%'
                                            },
                                        ]
                            }

                        ]
            };
    return items;
}
;


function getItemPanelNoTransksiKelompokPasien_penatajasarwi(lebar)
{
    var items =
            {
                Width: lebar,
                height: 150,
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .990,
                                layout: 'form',
                                Width: lebar - 10,
                                labelWidth: 130,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            {
                                                xtype: 'combo',
                                                fieldLabel: 'Kelompok Pasien Baru',
                                                id: 'kelPasien_penatajasarwi',
                                                editable: false,
                                                // value: 'Perseorangan',
                                                store: new Ext.data.ArrayStore
                                                        (
                                                                {
                                                                    id: 0,
                                                                    fields:
                                                                            [
                                                                                'Id',
                                                                                'displayText'
                                                                            ],
                                                                    data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                                                }
                                                        ),
                                                displayField: 'displayText',
                                                mode: 'local',
                                                width: 100,
                                                forceSelection: true,
                                                triggerAction: 'all',
                                                emptyText: 'Pilih Salah Satu...',
                                                selectOnFocus: true,
                                                anchor: '95%',
                                                listeners:
                                                        {
                                                            'select': function (a, b, c)
                                                            {
                                                                Ext.getCmp('txtNoSJP_penatajasarwi').hide();
                                                                Ext.getCmp('txtNoAskes_penatajasarwi').hide();
                                                                if (b.data.displayText == 'Perseorangan')
                                                                {   
																	Ext.getCmp('kelHakKelas_penatajasarwi').disable();
                                                                    Ext.getCmp('kelHakKelas_penatajasarwi').setValue('');
                                                                    jeniscus = '0';
                                                                    Ext.getCmp('cboKelompokpasien_penatajasarwi').setValue('');
                                                                    Ext.getCmp('txtNoSJP_penatajasarwi').hide();
                                                                    Ext.getCmp('txtNoAskes_penatajasarwi').hide();
                                                                } else if (b.data.displayText == 'Perusahaan')
                                                                {
																	Ext.getCmp('kelHakKelas_penatajasarwi').disable();
                                                                    Ext.getCmp('kelHakKelas_penatajasarwi').setValue('');
                                                                    jeniscus = '1';
                                                                    Ext.getCmp('cboKelompokpasien_penatajasarwi').setValue('');
                                                                    Ext.getCmp('txtNoSJP_penatajasarwi').show();
                                                                    Ext.getCmp('txtNoAskes_penatajasarwi').show();
                                                                    Ext.getCmp('txtNoSJP_penatajasarwi').hide();
                                                                    Ext.getCmp('txtNoAskes_penatajasarwi').enable();
                                                                } else if (b.data.displayText == 'Asuransi')
                                                                {
																	Ext.getCmp('kelHakKelas_penatajasarwi').enable();
                                                                    Ext.getCmp('kelHakKelas_penatajasarwi').setValue('');
                                                                    jeniscus = '2';
                                                                    Ext.getCmp('cboKelompokpasien_penatajasarwi').setValue('');
                                                                    Ext.getCmp('txtNoSJP_penatajasarwi').show();
                                                                    Ext.getCmp('txtNoAskes_penatajasarwi').show();
                                                                    Ext.getCmp('txtNoSJP_penatajasarwi').enable();
                                                                    Ext.getCmp('txtNoAskes_penatajasarwi').enable();
                                                                }

                                                                RefreshDatacombo_penatajasarwi(jeniscus);
                                                            }

                                                        }
                                            },
                                            {
                                                xtype: 'combo',
                                                fieldLabel: 'Hak Kelas',
                                                id: 'kelHakKelas_penatajasarwi',
                                                disabled: true,
                                                // value: 'Perseorangan',
                                                store: new Ext.data.ArrayStore
                                                        (
                                                                {
                                                                    id: 0,
                                                                    fields:
                                                                            [
                                                                                'Id',
                                                                                'displayText'
                                                                            ],
                                                                    data: [[1, '1'], [2, '2'], [3, '3']]
                                                                }
                                                        ),
                                                displayField: 'displayText',
                                                mode: 'local',
                                                width: 100,
                                                forceSelection: true,
                                                triggerAction: 'all',
                                                emptyText: 'Pilih Salah Satu...',
                                                selectOnFocus: true,
                                                anchor: '95%',
                                            },
                                            {
                                                columnWidth: .990,
                                                layout: 'form',
                                                border: false,
                                                labelWidth: 130,
                                                items:
                                                        [
                                                            mComboKelompokpasien_penatajasarwi()
                                                        ]
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. SJP',
                                                maxLength: 200,
                                                name: 'txtNoSJP_penatajasarwi',
                                                id: 'txtNoSJP_penatajasarwi',
                                                width: 100,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Askes',
                                                maxLength: 200,
                                                name: 'txtNoAskes_penatajasarwi',
                                                id: 'txtNoAskes_penatajasarwi',
                                                width: 100,
                                                anchor: '95%'
                                            }

                                            // mComboKelompokpasien_penatajasarwi


                                        ]
                            }

                        ]
            };
    return items;
}
;

function Datasave_Kelompokpasien(mBol)
{

    if (vkode_customer_PJRWI == selectKdCustomer) {
        ShowPesanInfoKasirrwi('Harap ganti kelompok pasien dengan kelompok yang lain','Information');
    }else{
        if (ValidasiEntryUpdateKelompokPasien_penatajasarwi(nmHeaderSimpanData, false) == 1)
        {
            Ext.Ajax.request({
                // url: "./Datapool.mvc/CreateDataObj",
                //url: baseURL + "index.php/main/functionRWI/UpdateKdCustomer",
                url: baseURL +  "index.php/main/functionRWI/UpdateGantiKelompok",
                params: getParamKelompokpasien_penatajasarwi(),
                failure: function (o)
                {
                    ShowPesanWarningKasirrwi('Simpan kelompok pasien gagal', 'Gagal');
                    RefreshDataDetail_panatajasarwi(Ext.get('txtNoTransaksi_penatajasarwi').dom.value);
                },
                success: function (o)
                {

                    var cst = Ext.decode(o.responseText);
                    FormLookUpsdetailTRKelompokPasien.close();
                    ShowPesanInfoKasirrwi('Ganti kelompok pasien berhasil','Information');
                    vkode_customer_PJRWI = selectKdCustomer;
                   	grListTRKasirrwi.getStore().getRange()[dataRowIndex].set('CUSTOMER', cst.customer);
                    //refeshKasirrwiKasir();
                    //RefreshDataDetail_panatajasarwi(Ext.get('txtNoTransaksi_penatajasarwi').dom.value);
                    
                    /*Ext.get('txtCustomer_penatajasarwi').dom.value = selectNamaCustomer;
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                        ShowPesanInfoKasirrwi('Ganti kelompok pasien berhasil','Information');
                        //Datasave_Kelompokpasien_SQL();
                        //RefreshData_penatajasarwi();
                        if (mBol === false)
                        {
                            RefreshDataDetail_panatajasarwi(Ext.get('txtNoTransaksi_penatajasarwi').dom.value);
                        }
                        ;
                        FormLookUpsdetailTRKelompokPasien.close();
                    } else
                    {
                        ShowPesanWarningKasirrwi('Simpan kelompok pasien gagal', 'Gagal');
                    }
                    ;*/
                }
            });

        } else
        {
            if (mBol === true)
            {
                return false;
            };
        };
    }
};


function Datasave_Kelompokpasien_SQL(){
    Ext.Ajax.request ({
        url: baseURL +  "index.php/rawat_jalan_sql/functionRWJ/UpdateKdCustomer",   
        params: getParamKelompokpasien_penatajasarwi(),
        failure: function(o){
            ShowPesanWarningKasirrwi('SQL, Error ganti customer, Hubungi Admin!', 'Gagal');
        },  
        success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
            }else{
                ShowPesanWarningKasirrwi('SQL, Simpan kelompok pasien gagal!', 'Gagal');
            }
        }
    });
}
function getParamKelompokpasien_penatajasarwi()
{
    /*DISINI*/
    var params =
            {
               /* Table: 'ViewTrKasirRwi',
                TrKodePasien: Ext.get('txtNoMedrecDetransaksi_penatajasarwi').getValue(),
                TrKodeTranskasi: Ext.get('txtNoTransaksi_penatajasarwi').getValue(),
                KdUnit: Ext.get('txtKdUnit_penatajasarwi').getValue(),
                KdDokter: Ext.get('txtKdDokter_penatajasarwi').dom.value,
                TglTransaksi: Ext.get('dtpTanggalDetransaksi_penatajasarwi').dom.value,
                KDCustomer: selectKdCustomer,
                TglTransaksi : Ext.get('dtpTanggalDetransaksi_penatajasarwi').dom.value,
				KDNoSJP: Ext.get('txtNoSJP_penatajasarwi').dom.value,
                KDNoAskes: Ext.get('txtNoAskes_penatajasarwi').dom.value,
				UrutMasuk : Ext.getCmp('txtKdUrutMasuk_penatajasarwi').getValue(),*/
                /*
                
                $_POST['KDCustomer'];
                $_POST['KDNoSJP'];
                $_POST['KDNoAskes'];
                $_POST['KdPasien'];
                $_POST['TglMasuk'];
                $_POST['KdUnit'];
                $_POST['UrutMasuk'];
                 */
                KDCustomer  : selectKdCustomer,
                KDNoSJP     : Ext.get('txtNoSJP_penatajasarwi').getValue(),
                KDNoAskes   : Ext.get('txtNoAskes_penatajasarwi').getValue(),
                KdPasien    : vKdPasien,
                TglMasuk    : tgltrans,
                KdUnit      : tmpkd_unit,
                UrutMasuk   : vUrutMasuk,
				hak_kelas 	: Ext.getCmp('kelHakKelas_penatajasarwi').getValue(),
            };
    return params;
}
;

function ValidasiEntryUpdateKelompokPasien_penatajasarwi(modul, mBolHapus)
{
    var x = 1;

    if ((Ext.get('kelPasien_penatajasarwi').getValue() == '') || (Ext.get('kelPasien_penatajasarwi').dom.value === undefined) || (Ext.get('cboKelompokpasien_penatajasarwi').getValue() == '') || (Ext.get('cboKelompokpasien_penatajasarwi').dom.value === undefined))
    {
        if (Ext.get('kelPasien_penatajasarwi').getValue() == '' && mBolHapus === true)
        {
            ShowPesanWarningKasirrwi(nmGetValidasiKosong('Kelompok pasien'), modul);
            x = 0;
        }
        ;
        if (Ext.get('cboKelompokpasien_penatajasarwi').getValue() == '' && mBolHapus === true)
        {
            ShowPesanWarningKasirrwi(nmGetValidasiKosong('Combo kelompok pasien'), modul);
            x = 0;
        }
        ;
    }
    ;
    return x;
}
;
function KelompokPasienbaru_penatajasarwi()
{
    jeniscus = 0;
    KelompokPasienAddNew = true;
    // Ext.getCmp('cboPerseorangan').show()
    Ext.getCmp('cboKelompokpasien_penatajasarwi').show();
    Ext.getCmp('txtNoSJP_penatajasarwi').disable();
    Ext.getCmp('txtNoAskes_penatajasarwi').disable();
    RefreshDatacombo_penatajasarwi(jeniscus);
    Ext.get('txtCustomer_penatajasarwiLama').dom.value = vnama_customer_PJRWI;
    // Ext.getCmp('cboPerusahaanRequestEntry').hide()


}
;

PenataJasaKasirRWI.form.Class.trdokter = Ext.data.Record.create([
    {name: 'kd_nama', mapping: 'kd_nama'},
    {name: 'kd_lab', mapping: 'kd_lab'},
    {name: 'jp', mapping: 'jp'},
    {name: 'jpp', mapping: 'jpp'},
]);
function cekKeVisiteDokter(no_transaksi, tgl_transaksi, kd_produk,urut)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionRWI/cekKeVisiteDokter",
                        params: {no_transaksi: no_transaksi,
								 tgl_transaksi: tgl_transaksi,
								 kd_produk:kd_produk,
								 urut: urut
								},
                        failure: function (o)
                        {
                            ShowPesanErrorKasirrwi('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.jumlah > 0)
                            {
                                /* PenataJasaRWI.ds_trbokter_rwi.removeAll();
                                var recs = [],
                                        recType = PenataJasaRWI.ds_trbokter_rwi.recordType;

                                for (var i = 0; i < cst.listData.length; i++) {

                                    recs.push(new recType(cst.listData[i]));

                                }
                                PenataJasaRWI.ds_trbokter_rwi.add(recs);
                                PenataJasaRWI.form.Grid.grid_trdokter.getView().refresh();  */
								//getVisiteDokter(no_transaksi, tgl_transaksi, kd_produk,cst.kd_klas,urut);
                            }
                        }
                    }

            )
}
function getVisiteDokter(no_transaksi, tgl_transaksi, kd_produk,kd_klas,urut)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionRWI/getVisiteDokter",
                        params: {no_transaksi: no_transaksi,
								 tgl_transaksi: tgl_transaksi,
								 kd_produk:kd_produk,
								 kd_klas:kd_klas,
								 urut: urut
								},
                        failure: function (o)
                        {
                            ShowPesanErrorKasirrwi('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.processResult === 'SUCCESS')
                            {
								
								
								//trdokter_penatajasarwi();
								Ext.getCmp('txtjumlah_byrdok_kasir_rwi').setValue(formatCurrency(cst.iniTarif));
								PenataJasaKasirRWI.ds_trbokter_rwi.removeAll();
                                var recs = [],
                                        recType = PenataJasaKasirRWI.ds_trbokter_rwi.recordType;

                                for (var i = 0; i < cst.listData.length; i++) {

                                    recs.push(new recType(cst.listData[i]));

                                }
                                PenataJasaKasirRWI.ds_trbokter_rwi.add(recs);
                                PenataJasaKasirRWI.form.Grid.grid_trdokter.getView().refresh(); 
                            }
                        }
                    }

            )
}
function getTotalbayar_dok()
{
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < PenataJasaKasirRWI.ds_trbokter_rwi.getCount(); i++)
    {

        var recordterakhir;


        tampunggrid = parseInt(PenataJasaKasirRWI.ds_trbokter_rwi.data.items[i].data.jpp);
        TotalProduk += tampunggrid
        recordterakhir = TotalProduk
        Ext.get('txtjumlah_dbyrdok_kasir_rwi').dom.value = formatCurrency(recordterakhir);
    }
    bayar = Ext.get('txtjumlah_dbyrdok_kasir_rwi').getValue();
    return bayar;
}
;

function HapusBaris_panatajasarwi(){
    //var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function (btn, combo) {
        //if (btn == 'ok')
        //{
            //variablehistori = combo;
            senderProdukDelete(currentKdKasir_KasirRWI, tmpkd_produk, notransaksi, tmpRowGrid, tmptgl_transaksi); 
            //DataDeleteDetail_panatajasarwi();
        //}
    //});
};

function Datadelete_trdokter(mBol, callback)
{


    Ext.Ajax.request
            (
                    {
                        // url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionRWI/hapustrdokter",
                        params: getParamDetailtrdokter(),
                        failure: function (o)
                        {
                            ShowPesanWarningKasirrwi('Konsultasi ulang gagal', 'Gagal');
                            //	RefreshDataDetail_panatajasarwi(Ext.get('txtNoTransaksi_penatajasarwi').dom.value);
                        },
                        success: function (o)
                        {
                            //RefreshDataDetail_panatajasarwi(Ext.get('txtNoTransaksi_penatajasarwi').dom.value);
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                if (callback != undefined)
                                    callback(cst.id);
                                ShowPesanInfoKasirrwi(nmPesanSimpanSukses, nmHeaderSimpanData);
                                griddetailtrdokter_kasirrwi.close();
                                if (mBol === false)
                                {
                                    //RefreshDataDetail_panatajasarwi(Ext.get('txtNoTransaksi_penatajasarwi').dom.value);
                                }
                                ;
                            } else
                            {
                                ShowPesanWarningKasirrwi('Konsultasi ulang gagal', 'Gagal');

                            }
                            ;
                        }
                    }
            );




}
;
function DataDeleteDetail_panatajasarwi()
{
    Ext.Ajax.request
	({
		// url: "./Datapool.mvc/DeleteDataObj",
		url: baseURL + "index.php/main/DeleteDataObj",
		params: getParamDataDeleteDetail_panatajasarwi(),
		success: function (o)
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				ShowPesanInfoKasirrwi('Hapus baris berhasil', 'Informasi');
				DataDeleteDetail_panatajasarwi_SQL();
				dsTRDetailKasirRWIList_panatajasarwi.removeAt(Current_kasirrwi.row);
				cellSelecteddeskripsi_kasir_rwi = undefined;
				RefreshDataDetail_kasirrwi(notransaksi);

			} else if (cst.success === false && cst.pesan === 0)
			{
				ShowPesanWarningKasirrwi('Gagal menghapus baris ini!', 'WARNING');
			} else
			{
				ShowPesanWarningKasirrwi('Gagal mennghapus baris ini!', 'WARNING');
			};
		}
	});
};
function getParamDataDeleteDetail_panatajasarwi()
{
    var params =
            {
                Table: 'ViewTrKasirRwi',
                TrKodeTranskasi: Current_kasirrwi.data.data.NO_TRANSAKSI,
                TrTglTransaksi: Current_kasirrwi.data.data.TGL_TRANSAKSI,
                TrKdPasien: Current_kasirrwi.data.data.KD_PASIEN,
				kodePasien: vKdPasien,
				KODEDOKTER: vKdDokter,
                TrKdNamaPasien: namapasien,
                TrKdUnit: kodeunit,
                TrNamaUnit: namaunit,
                Uraian: Current_kasirrwi.data.data.DESKRIPSI2,
                AlasanHapus: variablehistori,
                TrHarga: Current_kasirrwi.data.data.HARGA,
                TrKdProduk: Current_kasirrwi.data.data.KD_PRODUK,
                RowReq: Current_kasirrwi.data.data.URUT,
                Hapus: 2
            };

    return params;
};

function Datasave_PenataJasaKasirRWI_on_kasirrrwi(mBol,jasa) {
    //if (ValidasiEntry_PenataJasaKasirRWI(nmHeaderSimpanData,false) == 1 ){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRWI/savedetailpenyakit",
        params: getParamDetailTransaksi_Kasir(),
        failure: function (o) {
            ShowPesanWarningKasirrwi('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
            RefreshDataDetail_kasirrwi(notransaksi);
        },
        success: function (o) {
            RefreshDataDetail_kasirrwi(notransaksi);
            var cst = Ext.decode(o.responseText);
           /*  if (cst.success === true && cst.compo === true)
            {
                //ShowPesanInfoKasirrwi(nmPesanSimpanSukses,nmHeaderSimpanData);
                //trdokter_penatajasarwi();
				Datasave_PenataJasaKasirRWI_on_kasirrrwi_SQL();
//                Ext.getCmp('txtjumlah_byrdok_kasir_rwi').setValue(formatCurrency(cst.tarif));
            } else  */if (cst.success === true) {
                ShowPesanInfoKasirrwi('Tambah tindakan berhasil disimpan', 'Information');
				tgl_isian='n';
				Datasave_PenataJasaKasirRWI_on_kasirrrwi_SQL();
				if(jasa == true){
					SimpanJasaDokterPenindak_KasirRWI(currentJasaDokterKdProduk_KasirRWI,currentJasaDokterKdTarif_KasirRWI,currentJasaDokterUrutDetailTransaksi_KasirRWI,currentJasaDokterHargaJP_KasirRWI);
				}
                //RefreshDataFilter_panatajasarwi();
                if (mBol === false) {
                    RefreshDataDetail_kasirrwi(notransaksi);
                }
            } else {
                ShowPesanWarningKasirrwi('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
            }
        }
    });
    /* }else{
     if(mBol === true){
     return false;
     };
     }; */

};

function trdokter_penatajasarwi() {
    griddetailtrdokter_kasirrwi = new Ext.Window({
        id: 'griddetailtrdokter_kasirrwi',
        title: 'Detail Dokter',
        closeAction: 'destroy',
        width: 600,
        height: 360,
        border: false,
        resizable: false,
        closable: true,
        plain: true,
        constrain: true,
        layout: 'fit',
        iconCls: 'Request',
        modal: true,
        items: [getItemPanel_trdokter_kasir()],
        listeners: {}
    });

    griddetailtrdokter_kasirrwi.show();
}


function getItemPanel_trdokter_kasir()
{
    var items =
            {
                layout: 'fit',
                width: 200,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 10px',
                border: false,
                height: 100,
                items:
                        [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 10px 10px 10px 10px',
                                border: true,
                                width: 100,
                                height: 100,
                                items:
                                        [
                                            grid_trdokterkasir_rwi(),
                                            {
                                                x: 270,
                                                y: 255,
                                                xtype: 'label',
                                                text: 'Tarif  '
                                            },
                                            {
                                                x: 350,
                                                y: 255,
                                                xtype: 'label',
                                                text: ':'
                                            },
                                            {
                                                x: 390,
                                                y: 255,
                                                id: 'txtjumlah_byrdok_kasir_rwi',
                                                name: 'txtjumlah_byrdok_kasir_rwi',
                                                xtype: 'textfield',
                                                readOnly: true,
                                                style: {'text-align': 'right'},
                                                fieldLabel: 'No. Transaksi ',
                                            },
                                            {
                                                x: 270,
                                                y: 277,
                                                xtype: 'label',
                                                text: 'Total'
                                            },
                                            {
                                                x: 350,
                                                y: 277,
                                                xtype: 'label',
                                                text: ':'
                                            },
                                            {
                                                x: 390,
                                                y: 277,
                                                id: 'txtjumlah_dbyrdok_kasir_rwi',
                                                name: 'txtjumlah_dbyrdok_kasir_rwi',
                                                xtype: 'textfield',
                                                readOnly: true,
                                                style: {'text-align': 'right'},
                                                fieldLabel: 'No. Transaksi ',
                                            }
                                        ]
                            }


                        ]
            };
    return items;
}
;

function grid_trdokterkasir_rwi()
{
    var detail_tr_dokter_rwi_store = ['kd_nama', 'kd_lab', 'jp', 'jpp'];
    PenataJasaKasirRWI.ds_trbokter_rwi = new WebApp.DataStore({fields: detail_tr_dokter_rwi_store});
    PenataJasaKasirRWI.form.Grid.grid_trdokter = new Ext.grid.EditorGridPanel({
        x: 10,
        y: 10,
        title: tittle_trdok,
        id: 'PjRWI_Trans_grid_trdokter',
        stripeRows: true,
        height: 240,
        anchor: '100%,100%',
        border: true,
        store: PenataJasaKasirRWI.ds_trbokter_rwi,
        frame: false,
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
            }
        }),
        cm: rwi_grid_detail(),
        viewConfig: {forceFit: true},
        tbar: [{
                text: 'Tambah Dokter',
                id: 'btntambahtr_dokter_penataRWI',
                tooltip: nmLookup,
                iconCls: 'find',
                handler: function () {
                    PenataJasaKasirRWI.ds_trbokter_rwi.insert(PenataJasaKasirRWI.ds_trbokter_rwi.getCount(), PenataJasaKasirRWI.func.getNulltrdokter());

                }
            }, {
                id: 'btnsimpantr_dokter_penataRWI',
                text: 'Simpan',
                tooltip: nmLookup,
                iconCls: 'save',
                handler: function () {
                    if (Ext.getCmp('txtjumlah_byrdok_kasir_rwi').getValue() === Ext.getCmp('txtjumlah_dbyrdok_kasir_rwi').getValue())
                    {
                        var e = false;
                        for (var i = 0, iLen = PenataJasaKasirRWI.ds_trbokter_rwi.getRange().length; i < iLen; i++) {
                            var o = PenataJasaKasirRWI.ds_trbokter_rwi.data.items[i].data;
                            if (o.kd_nama == '' || o.kd_nama == null) {
                                PenataJasaKasirRWI.alertError(iLen);
                                e = true;
                                break;
                            } else {
                            }

                        }
                        if (e == false)
                        {
                            Datasave_trdokter_penatajasarwi(false);
                        }
                    } else
                    {
                        ShowPesanWarningKasirrwi("Pembayaran tidak sama", "Komponen dokter");
                    }

                }
            }, {
                id: 'btnhapustr_dokter_penataRWI',
                text: 'Hapus',
                tooltip: nmLookup,
                iconCls: 'RemoveRow',
                handler: function () {


                    Ext.Msg.show({
                        title: nmHapusBaris,
                        msg: 'Anda yakin akan menghapus data dokter',
                        buttons: Ext.MessageBox.YESNO,
                        fn: function (btn) {
                            if (btn == 'yes')
                            {
                                Datadelete_trdokter();
                                PenataJasaKasirRWI.ds_trbokter_rwi.removeAt(
                                        PenataJasaKasirRWI.form.Grid.grid_trdokter.getSelectionModel().selection.cell[0]);
                            }
                        },
                        icon: Ext.MessageBox.QUESTION
                    });

                    // PenataJasaKasirRWI.ds_trbokter_rwi.removeAt(PenataJasaKasirRWI.form.Grid.grid_trdokter.getSelectionModel().selection.cell[0]);
                }
            },
        ]
    });

    return PenataJasaKasirRWI.form.Grid.grid_trdokter;
}

function Datasave_trdokter_penatajasarwi(mBol, callback)
{


    Ext.Ajax.request
            (
                    {
                        // url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionRWI/savetrdokter",
                        params: getParamDetailtrdokter(),
                        failure: function (o)
                        {
                            ShowPesanWarningKasirrwi('Konsultasi ulang gagal', 'Gagal');
                            griddetailtrdokter_kasirrwi.close();
                            //	RefreshDataDetail_kasirrwi(Ext.get('txtNoTransaksi_penatajasarwi').dom.value);
                        },
                        success: function (o)
                        {
                            //RefreshDataDetail_kasirrwi(Ext.get('txtNoTransaksi_penatajasarwi').dom.value);
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                RefreshDataDetail_kasirrwi(notransaksi);
                                if (callback != undefined)
                                    callback(cst.id);
                                ShowPesanInfoKasirrwi(nmPesanSimpanSukses, nmHeaderSimpanData);
                                griddetailtrdokter_kasirrwi.close();
                                //RefreshDataFilter_panatajasarwi();
                                if (mBol === false)
                                {
                                    //RefreshDataDetail_kasirrwi(Ext.get('txtNoTransaksi_penatajasarwi').dom.value);
                                }
                                ;
                            } else
                            {
                                ShowPesanWarningKasirrwi('Konsultasi ulang gagal', 'Gagal');
                                griddetailtrdokter_kasirrwi.close();

                            }
                            ;
                        }
                    }
            );




}
;
function getParamDetailtrdokter()
{

    var params =
            {
                /*
                 TrKodeTranskasi	: ,
                 KdUnit			: ,
                 Tgl				: ,
                 Shift			: tampungshiftsekarang,*/
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                Shift: tampungshiftsekarang,
                Urut: PenataJasaKasirRWI.URUT,
                TGLTRANSAKSI: PenataJasaKasirRWI.TGL_TRANSAKSI_detailtransaksi,
                KD_PRODUK: PenataJasaKasirRWI.KD_PRODUK,
                TGLBERLAKU: ShowDate(PenataJasaKasirRWI.TGL_BERLAKU),
                HARGA: PenataJasaKasirRWI.HARGA,
                KD_TARIF: PenataJasaKasirRWI.KD_TARIF,
                JmlField: 1,
                JmlList: 1,
                List: getArrDetailTrtrdokter_kasirrwi()
            };
    return params
}
;


function getArrDetailTrtrdokter_kasirrwi() {
    var x = '';
    for (var i = 0; i < PenataJasaKasirRWI.ds_trbokter_rwi.getCount(); i++) {
        if (PenataJasaKasirRWI.ds_trbokter_rwi.data.items[i].data.KD_PRODUK != '' && PenataJasaKasirRWI.ds_trbokter_rwi.data.items[i].data.DESKRIPSI != '') {
            var y = '';
            var z = '@@##$$@@';
            y = 'URUT=' + PenataJasaKasirRWI.ds_trbokter_rwi.data.items[i].data.kd_nama;
            y += z + PenataJasaKasirRWI.ds_trbokter_rwi.data.items[i].data.kd_nama;
            y += z + PenataJasaKasirRWI.ds_trbokter_rwi.data.items[i].data.jp;

            y += z + PenataJasaKasirRWI.ds_trbokter_rwi.data.items[i].data.jpp;
            y += z + PenataJasaKasirRWI.ds_trbokter_rwi.data.items[i].data.kd_lab;
            if (i === (PenataJasaKasirRWI.ds_trbokter_rwi.getCount() - 1)) {
                x += y;
            } else {
                x += y + '##[[]]##';
            }
        }
    }
    return x;
}

function rwi_grid_detail() {
    return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            id: 'col_trdok_dokter',
            header: 'Dokter',
            dataIndex: 'kd_nama',
            width: 250,
            menuDisabled: true,
            hidden: false,
            editor: PenataJasaKasirRWI.form.ComboBox.produk = Nci.form.Combobox.autoComplete({
                store: PenataJasaKasirRWI.form.DataStore.trdokter,
                select: function (a, b, c) {

                },
                param: function () {
                    var params = {};
                    return params;
                },
                insert: function (o) {
                    return {
                        kd_dokter: o.kd_dokter,
                        nama: o.nama,
                        kd_nama: o.kd_dokter + '-' + o.nama
                    }
                },
                url: baseURL + "index.php/main/functionRWI/gettrdokter",
                valueField: 'kd_nama',
                displayField: 'kd_nama'
            })
        }, {
            id: 'col_trdok_job',
            header: 'Job',
            dataIndex: 'kd_lab',
            width: 250,
            menuDisabled: true,
            hidden: false,
            editor: PenataJasaKasirRWI.form.ComboBox.produk = Nci.form.Combobox.autoComplete({
                store: PenataJasaKasirRWI.form.DataStore.dokter_inap_int,
                select: function (a, b, c) {

                },
                param: function () {
                    var params = {};
                    return params;
                },
                insert: function (o) {
                    return {
                        kd_dokter: o.kd_job,
                        label: o.label,
                        kd_lab: o.kd_job + '-' + o.label
                    }
                },
                url: baseURL + "index.php/main/functionRWI/getdokter_inap_int",
                valueField: 'kd_lab',
                displayField: 'kd_lab'
            })
        }, {
            id: 'col_trdok_jp',
            header: 'JP(%)',
            dataIndex: 'jp',
            width: 100,
            hidden: true,
            menuDisabled: true
        }, {
            id: 'col_trdok_jp_rp',
            header: 'JP(Rp)',
            dataIndex: 'jpp',
            width: 100,
            menuDisabled: false,
            hidden: false,
            align: 'right',
            editor: new Ext.form.TextField
                    (
                            {
                                id: 'saasasaaa',
                                allowBlank: false,
                                enableKeyEvents: true,
                                width: 100,
                                width:30,
                                        listeners:
                                        {
                                        }

                            }
                    ),
            renderer: function (v, params, record)
            {


                getTotalbayar_dok();
                return record.data.jpp;




            }

        }
    ]);
}

PenataJasaKasirRWI.func.getNulltrdokter = function () {
    //var o=grListTRRWI.getSelectionModel().getSelections()[0].data;

    return new PenataJasaKasirRWI.form.Class.trdokter({
        kd_nama: '',
        kd_lab: '',
        jp: 0,
        jpp: 0,
    });
};
function loaddata_trdokter_PenataJasaKasirRWI(no_trans, urut, tgl)
{
    var strKriteriatrdokter_penatajasarwi = '';
    strKriteriatrdokter_penatajasarwi = 'and trd.no_transaksi = ~' + no_trans + '~ and trd.urut = ~' + urut + '~ and trd.tgl_transaksi = ~' + tgl + '~';
    PenataJasaKasirRWI.ds_trbokter_rwi.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: '',
                                    Sort: 'kd_penyakit',
                                    Sortdir: 'ASC',
                                    target: 'Lookuptr_dokter_rwi',
                                    param: strKriteriatrdokter_penatajasarwi
                                }
                    }
            );

    return PenataJasaKasirRWI.ds_trbokter_rwi;
}
;
function getParamDetailTransaksi_Kasir() {


    var params = {
        Table: 'ViewTrKasirRwi',
        TrKodeTranskasi: notransaksi,
        KdUnit: kodeunit,
        //Tgl: tgltrans,
        Tgl: tanggaltransaksitampung,
        Shift: tampungshiftsekarang,
		urut_masuk:urutmasuk,
		kd_pasien:kodepasien,
		kdDokter: vKdDokter,
        Urut: PenataJasaKasirRWI.URUT,
        KD_PRODUK: PenataJasaKasirRWI.KD_PRODUK,
        QTY: PenataJasaKasirRWI.QTY,
        TGLBERLAKU: ShowDate(PenataJasaKasirRWI.TGL_BERLAKU),
        HARGA: PenataJasaKasirRWI.HARGA,
        KD_TARIF: PenataJasaKasirRWI.KD_TARIF,
        JmlField: 0,
        JmlList: 0,
        Hapus: 1,
        Ubah: 0
    };

    return params;
}
;
PenataJasaKasirRWI.form.Class.diagnosa = Ext.data.Record.create([
    /*  {name: 'KD_PENYAKIT', 	mapping: 'KD_PENYAKIT'},
     {name: 'PENYAKIT', 	mapping: 'PENYAKIT'},
     {name: 'KD_PASIEN', 	mapping: 'KD_PASIEN'},
     {name: 'URUT', 	mapping: 'URUT'},
     {name: 'URUT_MASUK', 	mapping: 'URUT_MASUK'},
     {name: 'TGL_MASUK', 	mapping: 'TGL_MASUK'},
     {name: 'KASUS', 	mapping: 'KASUS'},
     {name: 'STAT_DIAG', 	mapping: 'STAT_DIAG'},
     {name: 'NOTE', 	mapping: 'NOTE'}, */
]);

PenataJasaKasirRWI.func.getNullProduk = function () {
    //var line=grListTRRWI.getSelectionModel().getSelections()[0].data;
    tanggaltransaksitampung=tanggaltransaksitampung_utama;
	var tmpTglTransaksi = Ext.getCmp('txtTanggalTransaksiKASIRRWI').getValue();
    return new PenataJasaKasirRWI.form.Class.diagnosa({
        KD_UNIT: tmpkd_unitKamar,
        KD_UNIT_TR: tmpkd_unitKamar,
        KD_PRODUK: '',
        DESKRIPSI: '',
        QTY: 0,
        DOKTER: '',
        TGL_TINDAKAN: '',
        QTY			: 0,
        DESC_REQ: '',
        TGL_BERLAKU: '',
        NO_TRANSAKSI: '',
        URUT: '',
        DESC_STATUS: '',
        TGL_TRANSAKSI: tmpTglTransaksi.format("Y-m-d"),
        KD_TARIF: '',
        HARGA: '',
    });
};

/*
    PERBARUAN GRID TINDAKAN/PRODUK 
    OLEH    : HADAD AL GOJALI
    TANGGAL : 2017 - 01 -09 
    ALASAN  : BELUM ADA PROSES SAVING
 */

function senderProduk(no_transaksi, kd_produk, kd_tarif, kd_kasir, urut, unit, tgl_transaksi, tgl_berlaku, quantity, harga, folio, fungsi,line, tahap, data_cst, lookup_dokter = null, transfer = null){
	if (Ext.getCmp('checkPerawat_KASIRRWI').getValue() === true && Ext.getCmp('checkDokter_KASIRRWI').getValue() === true) {
		tmpKdDokter = Ext.getCmp('cboDataDokterPenindak_KASIR_RWI').getValue()+"-"+Ext.getCmp('cboDataPerawatPenindak_KASIR_RWI').getValue();
	}else if(Ext.getCmp('checkPerawat_KASIRRWI').getValue() === true && Ext.getCmp('checkDokter_KASIRRWI').getValue() === false){
		tmpKdDokter = Ext.getCmp('cboDataPerawatPenindak_KASIR_RWI').getValue();
	}else if(Ext.getCmp('checkDokter_KASIRRWI').getValue() === true && Ext.getCmp('checkPerawat_KASIRRWI').getValue() === false){
		tmpKdDokter = Ext.getCmp('cboDataDokterPenindak_KASIR_RWI').getValue();
	}else{
		tmpKdDokter = "";
	}

	if (folio == "E") {
		no_faktur = rowSelectedKasirrwiKasir.data.NO_TRANSAKSI;
	}else{
		no_faktur = "";
	}
    Ext.Ajax.request({
        url     : baseURL + "index.php/main/functionRWI/insertDataProduk",
        params  :  {
			no_transaksi    : no_transaksi,
			kd_produk       : tmpkd_produk,
			kd_tarif        : tmpkd_tarif,
			kd_kasir        : kd_kasir,
			urut            : parseInt(urut)+1,
			unit            : tmpkd_unit,
			kd_unit_tr 		: tmpkd_unitKamar,
			tgl_transaksi   : tgl_transaksi,
			tgl_berlaku     : tgl_berlaku,
			quantity        : quantity,
			folio           : folio,
			harga           : harga,
			fungsi          : fungsi,
			no_faktur       : no_faktur,
			kd_dokter       : tmpKdDokter,
			kd_job          : tmpKdJob,
        },
        failure : function(o)
        {

        },
        success : function(o)
        {
            //tmpRowGrid = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
            var cst = Ext.decode(o.responseText);
            if (cst.status == true) {
	            tmpRowGrid = cst.urut;
				urut 			= cst.urut;
				no_transaksi 	= cst.no_transaksi;
				//tmpno_transaksi	= cst.no_transaksi;
                console.log(data_cst);
                console.log(tmpharga);
				if (transfer == null) {
		            if (cst.action == "insert") {
	                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('KP_PRODUK', cst.kp_produk);
	                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('DESKRIPSI', data_cst.deskripsi);
	                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('UNIT',data_cst.kd_unit);
	                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('QTY', 1);
	                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('TARIF',tmpharga);
	                    PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.KD_PRODUK         = data_cst.kd_produk;
	                    PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.KD_KLAS           = data_cst.KD_KLAS;
	                    PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.KD_UNIT           = data_cst.kd_unit;
	                    PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.HARGA             = data_cst.tarifx;
	                    PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.DESKRIPSI         = data_cst.deskripsi;
	                    PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.TGL_BERLAKU       = data_cst.tgl_berlaku;
	                    PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.KD_TARIF          = data_cst.kd_tarif;
	                    PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.STATUS_KONSULTASI = data_cst.status_konsultasi;
	                    PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.QTY               = 1;
	                    PenataJasaKasirRWI.form.Grid.produk.startEditing(line, 8);
	                    tmpRowGrid = cst.urut;
						if(cst.komponen === true){
		                    if (Ext.getCmp('checkPerawat_KASIRRWI').getValue() === false && Ext.getCmp('checkDokter_KASIRRWI').getValue() === false && lookup_dokter != false){
								loaddatastoredokter_REVISI();
								loaddatastoredokterVisite_REVISI();
								PilihDokterLookUpKasir_RWI_REVISI();
		                    }
						}else{
						}
		            }else{
		            	var tmp_tarif = 0;
			            // PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.TARIF             = data_cst.harga;
	                    tmp_tarif = PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('TARIF');
		            	console.log(line);
	                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('TARIF', (tmp_tarif * quantity));
		            }

					if (Ext.getCmp('checkPerawat_KASIRRWI').getValue() === false && Ext.getCmp('checkDokter_KASIRRWI').getValue() === false) {
		            	loaddatastoredokterVisite_REVISI();
					}else if (cst.dokter === true){
						if (fungsi === true && cst.komponen === true) {
							PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('DESKRIPSI', PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('DESKRIPSI')+" ("+cst.nama_dokter+")");
						}
					}else{
							PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('DESKRIPSI', PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('DESKRIPSI'));
					}
					PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('JUMLAH_DOKTER', cst.jumlah_penangan);
					// console.log(parseInt(Ext.getCmp('txtJumlahBayar_KASIRRWI').getValue()));
					// Ext.getCmp('txtJumlahBayar_KASIRRWI').setValue(parseInt(Ext.getCmp('txtJumlahBayar_KASIRRWI').getValue()) - parseInt(dsTRDetailKasirRWIList_panatajasarwi.data.items[Current_kasirrwi.row].data.TARIF));
	            	// Ext.getCmp('txtJumlahBayar_KASIRRWI').setValue(parseInt(Ext.getCmp('txtJumlahBayar_KASIRRWI').getValue()) + (tmp_tarif * quantity));
	            	// console.log(PenataJasaKasirRWI.dsGridTindakan);

                    //hani 2023-02-22 
                    console.log(PenataJasaKasirRWI.dsGridTindakan.data);
	            	var tmp_total_bayar = 0;
	            	for (var i = 0; i < PenataJasaKasirRWI.dsGridTindakan.data.length; i++) {
                         console.log(PenataJasaKasirRWI.dsGridTindakan.data.items[i].data.KD_PRODUK );
                         console.log(PenataJasaKasirRWI.dsGridTindakan.data.items[i].data.TARIF );
                    if (PenataJasaKasirRWI.dsGridTindakan.data.items[i].data.KD_PRODUK != "" || PenataJasaKasirRWI.dsGridTindakan.data.items[i].data.KD_PRODUK != 0) {
	            			// tmp_total_bayar += parseInt(PenataJasaKasirRWI.dsGridTindakan.data.items[i].data.TARIF);
	            			tmp_total_bayar += PenataJasaKasirRWI.dsGridTindakan.data.items[i].data.TARIF;
	            		}
	            	}
                    console.log(tmp_total_bayar);
	            	Ext.getCmp('txtJumlahBayar_KASIRRWI').setValue(tmp_total_bayar);
                    
	            }else{
					Ext.Ajax.request ({
						url: baseURL + "index.php/main/Controller_pembayaran/simpan_pembayaran",
						params 	: paramsTransfer_KasirRWI(rowSelectedKasirrwiKasir.data, parseInt(urut), transfer),
						success: function(o) {
							var cst = Ext.decode(o.responseText);
							if (cst.status === true){
								RefreshDataKasirrwiKasir();
								FormLookUpsdetailTRTransfer_RWI.close();
							}
						}
					});
	            }
	        }
	        // getTotalBayar_KasirRWI();
	        // Ext.getCmp('txtJumlahBayar_KASIRRWI').setValue(tmp_total_tagihan);
           /* if (cst.action == 'insert') {
                PenataJasaKasirRWI.dsGridTindakan.insert(PenataJasaKasirRWI.dsGridTindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
            }else{
                PenataJasaKasirRWI.form.Grid.produk.startEditing(tmpRowGrid+1, 3);
            }*/

            //PenataJasaKasirRWI.dsGridTindakan.insert(PenataJasaKasirRWI.dsGridTindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
            //PenataJasaKasirRWI.form.Grid.produk.startEditing(line+1, 3);
        }
    });
};


function cekTransferTindakan(reload){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWI/cek_transfer_tindakan",
		params: {
				kd_kasir 		: '02',
				no_transaksi 	: notransaksi,
				tgl_transaksi 	: tgltrans,
				//kd_produk       : kd_produk,
				unit            : tmpkd_unit,
				//tgl_berlaku     : tgl_berlaku,
		},
		failure: function(o)
		{
			//ShowPesanWarningKasirrwi("Migrasi data detail transaksi gagal !",'WARNING');
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);
			RefreshDataDetail_kasirrwi(cst.no_transaksi);
		}
	});
}

function senderProdukDelete(kd_kasir, kd_produk, no_transaksi, urut, tgl_transaksi){
    Ext.Ajax.request({
        url     : baseURL + "index.php/main/functionRWI/hapusDataProduk",
        params  :  {
            kd_produk       : kd_produk,
            no_transaksi    : no_transaksi,
            kd_kasir        : kd_kasir,
            urut            : urut,
            tgl_transaksi   : tgl_transaksi,
        },
        failure : function(o)
        {
        },
        success : function(o)
        {
            var cst = Ext.decode(o.responseText);
            Ext.getCmp('txtJumlahBayar_KASIRRWI').setValue(parseInt(Ext.getCmp('txtJumlahBayar_KASIRRWI').getValue()) - parseInt(dsTRDetailKasirRWIList_panatajasarwi.data.items[Current_kasirrwi.row].data.TARIF));
            dsTRDetailKasirRWIList_panatajasarwi.removeAt(Current_kasirrwi.row);
            // Ext.Ajax.request({
			// 	url: baseURL + "index.php/main/functionRWI/cek_cominal_pembayaran",
			// 	params: {
			// 		kd_kasir 	: '01',
			// 		no_transaksi: notransaksi, 
			// 	},
			// 	failure: function (o){},
			// 	success: function (o){
			// 		var cst = Ext.decode(o.responseText);
			// 		var sisaPembayaran = cst.harus_membayar - cst.telah_membayar;
			// 		//Ext.getCmp('txtJumlahBayar_KASIR_RWJ').setValue(cst.harus_membayar);
			// 		Ext.get('txtJumlahBayar_KASIR_RWJ').dom.value=formatCurrency(cst.harus_membayar);
			// 	}
			// });

        }
    });
};

function TRRawatInapColumModel2() {
    return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            header      : 'Tanggal Transaksi',
            dataIndex   : 'TGL_TRANSAKSI',
            width       : 130,
            menuDisabled: true,
            editor      : new Ext.form.TextField({
                    id                  : 'fieldcolTglTransaksirwi',
                    allowBlank          : true,
                    enableKeyEvents     : true,
                    width               : 30,
                    listeners           :
                    {
                        'specialkey' : function(a, b){
                            var line 			= PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
                            //dataRowIndexDetail 	= PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
                            if (b.getKey() === b.ENTER) {
	                            if (PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('FOLIO') == "A") {
	                            	var tmp_urut = PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('URUT');
	                            	tmp_urut = tmp_urut-1;
	                                senderProduk(
	                                    notransaksi, 
	                                    tmpkd_produk, 
	                                    tmpkd_tarif, 
	                                    currentKdKasir_KasirRWI, 
	                                    tmp_urut, 
	                                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('UNIT'), 
	                                    Ext.getCmp('fieldcolTglTransaksirwi').getValue(),
	                                    PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.TGL_BERLAKU,
	                                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('QTY'),
	                                    tmpharga,
	                                    'A',
	                                    false,
	                                    line,
	                                    'tanggal',
	                                    0
	                                );
	                                PenataJasaKasirRWI.form.Grid.produk.startEditing(line, 3);
	                            }else{
									ShowPesanWarningKasirrwi('Maaf produk transfer tidak dapat di rubah!','WARNING');
	                            }
                            }
                        }
                    }
            }),
        },
        {
            id: 'colReffRWI',
            header: 'No Faktur',
            dataIndex: 'NO_FAKTUR',
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colKdUnitRWI',
            header: 'Unit',
            dataIndex: 'KD_UNIT_TR',
            width: 75,
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colKdProduk_panatajasarwi2',
            header: 'Kode Produk',
            dataIndex: 'KP_PRODUK',
            selectOnFocus: true,
            width: 100,
            menuDisabled: true,
            editor      : new Ext.form.TextField({
                    id                  : 'fieldcolKdProdukrwi',
                    allowBlank          : true,
                    enableKeyEvents     : true,
                    width               : 30,
                    listeners           :
                    { 
                        'specialkey' : function(a, b)
                        {
                            var line = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
                            //dataRowIndexDetail = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
                            if (b.getKey() === b.ENTER) {
                                Ext.Ajax.request
                                ({
                                    store   : PenataJasaKasirRWI.form.DataStore.produk,
                                    url     : baseURL + "index.php/main/functionRWI/getProdukKey",
                                    params  :  {
                                        kd_unit         : tmpkd_unitKamar,
                                        //kd_unit         : kodeunit,
                                        kd_customer     : CurrentKDCustomerRWI,
                                        text            : Ext.getCmp('fieldcolKdProdukrwi').getValue(),
                                    },
                                    failure : function(o)
                                    {
                                        PenataJasaKasirRWI.form.Grid.produk.startEditing(line, 2);
                                        ShowPesanWarningKasirrwi("Data produk tidak ada!",'WARNING');
                                    },
                                    success : function(o)
                                    {
                                        var cst = Ext.decode(o.responseText);
                                        if (cst.processResult == 'SUCCESS'){
                                            //var lineKasirRWI = Ext.getCmp('KasirRWJ_gridTindakan').getSelectionModel().selection.cell[0];
                                            tmpkd_produk 			= cst.listData.kd_produk;
                                            tmpkd_tarif            	= cst.listData.kd_tarif;
                                            tmpdeskripsi           	= cst.listData.deskripsi;
                                            tmpharga               	= cst.listData.tarifx;
                                            tmptgl_berlaku         	= cst.listData.tgl_berlaku;
                                            tmpjumlah              	= cst.listData.jumlah;
                                            tmpstatus_konsultasi   	= cst.listData.status_konsultasi;
                                            PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('TARIF', cst.listData.tarifx);
                                            /*PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('DESKRIPSI', cst.listData.deskripsi);
                                            PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('UNIT',cst.listData.kd_unit);
                                            PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('QTY', 1);
                                            PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('TARIF', cst.listData.tarifx);
                                            PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.KD_PRODUK         = cst.listData.kd_produk;
                                            PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.KD_KLAS           = cst.listData.KD_KLAS;
                                            PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.KD_UNIT           = cst.listData.kd_unit;
                                            PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.HARGA             = cst.listData.tarifx;
                                            PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.DESKRIPSI         = cst.listData.deskripsi;
                                            PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.TGL_BERLAKU       = cst.listData.tgl_berlaku;
                                            PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.KD_TARIF          = cst.listData.kd_tarif;
                                            PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.STATUS_KONSULTASI = cst.listData.status_konsultasi;
                                            PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.QTY               = 1;
                                            PenataJasaKasirRWI.form.Grid.produk.startEditing(line, 8);*/

                                            //PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.TGL_TRANSAKSI     = nowTglTransaksiRWI;
                                            //cekKomponen(cst.listData.kd_produk, cst.listData.kd_tarif, cst.listData.kd_unit, line, cst.listData.tarifx); 
                                            var kasirRWIUrut=0;
                                            if(line>=1){
                                                kasirRWIUrut=toInteger(PenataJasaKasirRWI.dsGridTindakan.data.items[line-1].data.URUT); 
                                            }
                                            PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.URUT               = kasirRWIUrut+1;
                                            senderProduk(
                                                notransaksi, 
                                                tmpkd_produk, 
                                                tmpkd_tarif, 
                                                currentKdKasir_KasirRWI, 
                                                kasirRWIUrut, 
                                                // PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('UNIT'), 
                                                cst.listData.kd_unit, 
                                                PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('TGL_TRANSAKSI'),
                                                cst.listData.tgl_berlaku,
                                                1,
                                                tmpharga,
                                                'A',
                                                true,
                                                line,
                                                'produk',
                                                cst.listData
                                                //PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('DESKRIPSI')
                                            );
											// RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
                                        }else{
                                            ShowPesanWarningKasirrwi('Data produk tidak ada!','WARNING');
                                        };
                                    }
                                })
                            }else if(b.getKey() === b.TAB){
                                PenataJasaKasirRWI.form.Grid.produk.startEditing(line, 3);
                            }
                        },
                    }
                }),
        },
		{
            id: 'colDeskripsiRWI2',
            header: 'Nama Produk',
            dataIndex: 'DESKRIPSI',
            sortable: false,
            hidden: false,
            menuDisabled: true,
            width: 250,
            editor: PenataJasaKasirRWI.form.ComboBox.produk = Nci.form.Combobox.autoComplete({
                store: PenataJasaKasirRWI.form.DataStore.produk,
                select: function (a, b, c) {
                    console.log(b.data)
                    tmpkd_produk           = b.data.kd_produk;
                    tmpkd_tarif            = b.data.kd_tarif;
                    tmpdeskripsi           = b.data.deskripsi;
                    tmpharga               = b.data.harga;
                    tmptgl_berlaku         = b.data.tglberlaku;
                    tmpjumlah              = b.data.jumlah;
                    tmpstatus_konsultasi   = b.data.status_konsultasi;
                    var line = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
					//dataRowIndexDetail = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].data.KD_PRODUK = b.data.kd_produk;
                    //PenataJasaKasirRWI.dsGridTindakan.getRange()[line].data.DESKRIPSI = b.data.deskripsi;
                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].data.KD_TARIF = b.data.kd_tarif;
                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].data.HARGA = b.data.harga;
                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].data.TGL_BERLAKU = b.data.tglberlaku;
                    PenataJasaKasirRWI.dsGridTindakan.getRange()[line].data.QTY = 1;
                    PenataJasaKasirRWI.URUT = PenataJasaKasirRWI.dsGridTindakan.getRange()[line].data.URUT;
                    //PenataJasaKasirRWI.KD_PRODUK = b.data.kd_produk;
                    PenataJasaKasirRWI.QTY = 1;
                    PenataJasaKasirRWI.TGL_BERLAKU = b.data.tglberlaku;
                    PenataJasaKasirRWI.HARGA = b.data.tarif;
                    PenataJasaKasirRWI.KD_TARIF = b.data.kd_tarif;
                    PenataJasaKasirRWI.TGL_TRANSAKSI = b.data.TGL_TRANSAKSI;
                    PenataJasaKasirRWI.TGL_TRANSAKSI_detailtransaksi = b.data.TGL_TRANSAKSI;
                    PenataJasaKasirRWI.DESKRIPSI = b.data.deskripsi;
                    tittle_trdok = b.data.deskripsi; 
                    //Datasave_PenataJasaKasirRWI_on_kasirrrwi(false,false);
                   // PenataJasaKasirRWI.form.Grid.produk.getView().refresh();
					
					var currenturut= line + 1;//b.data.kd_tarif
					//cekKomponen(b.data.kd_produk,b.data.kd_tarif,kodeunit,currenturut,b.data.harga); 
					//PenataJasaKasirRWI.dsGridTindakan.insert(PenataJasaKasirRWI.dsGridTindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
                    if (line === 0)
                    {
                        PenataJasaKasirRWI.URUT = 1;
                    } else {
                        PenataJasaKasirRWI.URUT = parseInt(PenataJasaKasirRWI.dsGridTindakan.getRange()[line - 1].data.URUT) + 1;
                    }

                    var kasirRWIUrut=0;
                    if(line>=1){
                        kasirRWIUrut=toInteger(PenataJasaKasirRWI.dsGridTindakan.data.items[line-1].data.URUT); 
                    }
                    PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.URUT               = kasirRWIUrut+1;

                    senderProduk(
                        notransaksi, 
                        tmpkd_produk, 
                        tmpkd_tarif, 
                        currentKdKasir_KasirRWI, 
                        kasirRWIUrut, 
                        PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('UNIT'), 
                        PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('TGL_TRANSAKSI'),
                        PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.TGL_BERLAKU,
                        PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('QTY'),
                        tmpharga,
                        'A',
                        true,
                        line,
                        'deskripsi',
                        b.data
                    );
					// RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
                },
                param: function () {
                    // var line = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
                    // var o = grListTRKasirrwi.getSelectionModel().getSelections()[0].data;
                    var params = {};
                    params['kd_unit'] = tmpkd_unitKamar;
                    // params['kd_customer'] = o.KD_CUSTOMER;
                    params['kd_customer'] = CurrentKDCustomerRWI;
                    // params['line'] = line;

                    return params;
                },
                insert: function (o) {
                    return {
                        kdbagian : o.kd_bagian,
                        kdCustomer : o.kd_customer,
                        kd_produk: o.kd_produk,
                        deskripsi: o.deskripsi,
                        harga: o.tarif,
                        tglberlaku: o.tgl_berlaku,
						kd_tarif:o.kd_tarif,
                        text: '<table style="font-size: 11px;"><tr><td width="50">' + o.kp_produk + '</td><td width="160" align="left">' + o.deskripsi + '</td><td width="40" align="left">' + o.tarif + '</td></tr></table>'
                    }
                },
                url: baseURL + "index.php/main/functionRWI/getProdukdeskripsi",
                valueField: 'deskripsi',
				displayField: 'text'
            })
        },{
            id: 'colHARGA_penatajasa2',
            header: 'Harga',
            align: 'right',
            hidden: true,
            menuDisabled: true,
            dataIndex: 'HARGA',
            renderer: function (v, params, record)
            {
                return formatCurrency(record.data.HARGA);

            }
        },
		{
			id: 'coljmlhdokRWI2',
			header: 'Dok',
			width: 50,
			dataIndex: 'JUMLAH_DOKTER',
			hidden: false,
			listeners:{
				dblclick: function(dataview, index, item, e) {
					loaddatastoredokter_REVISI();
					loaddatastoredokterVisite_REVISI();
					PilihDokterLookUpKasir_RWI_REVISI();
				}
			}
            /*editor: new Ext.form.TextField
                    ({
                        id: 'coltext_jmlhRWI2',
                        allowBlank: true,
                        readOnly: true,
                        enableKeyEvents: true,
                        width: 100,
                        listeners:
                                {'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13)
                                        {
                                            var params = {
                                                no_transaksi: jstrdok.data.NO_TRANSAKSI,
                                                tgl_transaksi: jstrdok.data.TGL_TRANSAKSI,
                                                urut: jstrdok.data.URUT,
                                            };
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/functionRWI/cari_trdokter",
                                                params: params,
                                                failure: function (o) {},
                                                success: function (o) {
                                                    var cst = Ext.decode(o.responseText);
                                                    if (cst.success === true) {

                                                    }
                                                }
                                            });
                                        }
                                    }
                                }

                    })*/
        },
        {
            id: 'colQTY_penatajasa2',
            header: 'Qty',
            width: 50,
            align: 'right',
            menuDisabled: true,
            dataIndex: 'QTY',
            editor: new Ext.form.TextField
                    (
                            {
                                id: 'fieldcolQTY_penatajasa2',
                                allowBlank: true,
                                enableKeyEvents: true,
                                width: 100,
                                listeners:
								{
									'blur': function (a)
									{
										PenataJasaKasirRWI.URUT = Current_kasirrwi.data.data.URUT;
										PenataJasaKasirRWI.KD_PRODUK = Current_kasirrwi.data.data.KD_PRODUK;
										PenataJasaKasirRWI.QTY = a.getValue();
										PenataJasaKasirRWI.HARGA = Current_kasirrwi.data.data.HARGA;
										PenataJasaKasirRWI.KD_TARIF = Current_kasirrwi.data.data.KD_TARIF;
										PenataJasaKasirRWI.TGL_BERLAKU = Current_kasirrwi.data.data.TGL_BERLAKU;
									},
                                    'specialkey' : function(a, b)
                                    {
										var line = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
										//dataRowIndexDetail = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
                                        //var lineKasirRWJ = Ext.getCmp('KasirRWJ_gridTindakan').getSelectionModel().selection.cell[0];
                                        var kasirRWIUrut=toInteger(PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.URUT)-1;
                                        if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
                                            console.log(tmpharga+'qty');
                                            // var jumlah = parseInt(tmpharga)*parseInt(Ext.getCmp("fieldcolQTY_penatajasa2").getValue());
                                            // PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('TARIF', jumlah);
                                            senderProduk(
                                                notransaksi, 
                                                tmpkd_produk, 
                                                tmpkd_tarif, 
                                                currentKdKasir_KasirRWI, 
                                                kasirRWIUrut, 
                                                tmpkd_unit, 
                                                PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('TGL_TRANSAKSI'),
                                                PenataJasaKasirRWI.dsGridTindakan.data.items[line].data.TGL_BERLAKU,
                                                Ext.getCmp("fieldcolQTY_penatajasa2").getValue(),
                                                tmpharga,
                                                'A',
                                                false,
                                                line,
                                                'qty',
                                                PenataJasaKasirRWI.dsGridTindakan.getRange()[line].data
                                            );
											// RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
                                            PenataJasaKasirRWI.dsGridTindakan.getRange()[line].set('UNIT',tmpkd_unit);
											PenataJasaKasirRWI.dsGridTindakan.insert(PenataJasaKasirRWI.dsGridTindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
											Ext.getCmp('PjTransGrid2').startEditing(PenataJasaKasirRWI.dsGridTindakan.getCount()-1,4);
                                        }
                                    },

                                }

                            }
                    )



        },
        {
            id: 'colTarifRWI',
            header: 'Tarif',
            //align: 'right',
            dataIndex: 'TARIF',
            hidden: false,
			listeners:{
				dblclick: function(dataview, index, item, e) {
					PilihEditTarifLookUp_KasirRWI(PenataJasaKasirRWI.dsGridTindakan.data.items[item].data);
					//PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0]
				}
			}
        },
        {
            id: 'colFolioRWI',
            header: 'Folio',
            //align: 'right',
            dataIndex: 'FOLIO',
            hidden: false,
        },
	    {
	        id: 'colUrutRWI',
	        header: 'urut',
	        dataIndex: 'URUT',
	        menuDisabled: true,
	        hidden: true
	    },
		{
            id: 'xbu',
            header: 'xbu',
            align: 'right',
            hidden: true,
            menuDisabled: true,
            //dataIndex: 'HARGA',
            renderer: function(v, params, record)
				{
					var a='';
					if (record.data.NO_TRANSAKSI !== '')
					{
						//getVisiteDokterDiawal(record.data.NO_TRANSAKSI, record.data.TGL_TRANSAKSI, record.data.KD_PRODUK,record.data.URUT);
					}
				}

        }, 

    ]
            );
}
;

/*
    UPDATE FUNNGSI TAMBAH BARIS

 */
function TambahBarisProdukRWI()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRWJ();
        dsTRDetailKasirIGDList.insert(dsTRDetailKasirIGDList.getCount(), p);
    };
};

function cekKomponen(kd_produk,kd_tarif,kd_unit,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/cekKomponen",
		params: {
			kd_produk:kd_produk,
			kd_tarif:kd_tarif,
			kd_unit:kd_unit
		},
		failure: function(o)
		{
			ShowPesanErrorKasirrwi('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				if(cst.komponen === true){
                    if (Ext.getCmp('checkPerawat_KASIRRWI').getValue() === false && Ext.getCmp('checkDokter_KASIRRWI').getValue() === false){
						PilihDokterLookUpKasir_RWI_REVISI();
                    }
				}else
				{
					currentJasaDokterKdTarif_KasirRWI=kd_tarif;
					currentJasaDokterKdProduk_KasirRWI=kd_produk;
					currentJasaDokterUrutDetailTransaksi_KasirRWI=urut;
					currentJasaDokterHargaJP_KasirRWI=harga;

					loaddatastoredokter_REVISI();
					loaddatastoredokterVisite_REVISI();
				}
			} 
			else 
			{
				ShowPesanErrorKasirrwi('Gagal cek komponen pelayanan', 'Error');
			};
		}
	});
}

function GetgridEditDokterPenindakJasa_RWI(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgrideditjasadokterpenindak",
		params: {
			
			kd_unit:currentKDUnit_KasirRWI,
			urut:currentJasaDokterUrutDetailTransaksi_KasirRWI,
			kd_kasir:currentKdKasir_KasirRWI,
			no_transaksi:currentNoTransaksi_KasirRWI,
			tgl_transaksi:PenataJasaKasirRWI.TGL_TRANSAKSI,
			
			/* kd_unit:kodeunit,
			urut:currentJasaDokterUrutDetailTransaksi_RWI,
			kd_kasir:kdkasirnya,
			no_transaksi:notransaksi,
			tgl_transaksi:tgltrans, */
			
		},
		failure: function(o)
		{
			ShowPesanErrorKasirrwi('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_RWI.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				if(cst.totalrecords > 0){
					var recs=[],
						recType=dsGridJasaDokterPenindak_RWI.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsGridJasaDokterPenindak_RWI.add(recs);
					GridDokterTr_RWI.getView().refresh();
				} else{
					GetgridPilihDokterPenindakJasa_RWI(currentJasaDokterKdProduk_KasirRWI,currentJasaDokterKdTarif_KasirRWI);
				}
			} 
			else 
			{
				ShowPesanErrorKasirrwi('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}
function GetgridPilihDokterPenindakJasa_RWI(kd_produk,kd_tarif){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridjasadokterpenindak",
		params: {
			kd_produk:kd_produk,
			kd_tarif:kd_tarif,
			kd_unit:kodeunit
		},
		failure: function(o)
		{
			ShowPesanErrorKasirrwi('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_RWI.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsGridJasaDokterPenindak_RWI.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsGridJasaDokterPenindak_RWI.add(recs);
				GridDokterTr_RWI.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorKasirrwi('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}
function savetransaksi(){
	var e=false;
	if(PenataJasaKasirRWI.dsGridTindakan.getRange().length > 0){
		for(var i=0,iLen=PenataJasaKasirRWI.dsGridTindakan.getRange().length; i<iLen ; i++){
			var o=PenataJasaKasirRWI.dsGridTindakan.getRange()[i].data;
			if(o.QTY == '' || o.QTY==0 || o.QTY == null){
				ShowPesanErrorKasirrwi('Tindakan Yang Diberikan : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
				e=true;
				break;
			}
			
		}
	}else{
		ShowPesanErrorKasirrwi('Isi Tindakan Yang Diberikan','Peringatan');
		e=true;
	}
	/* for(var i=0,iLen=PenataJasaKasirRWI.dsGridObat.getRange().length; i<iLen ; i++){
		var o=PenataJasaKasirRWI.dsGridObat.getRange()[i].data;
		if(o.nama_obat == '' || o.nama_obat == null){
			ShowPesanErrorKasirrwi('Terapi Obat : "Nama Obat" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
			e=true;
			break;
		}
		if(o.jumlah == '' || o.jumlah==0 || o.jumlah == null){
			ShowPesanErrorKasirrwi('Terapi Obat : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
			e=true;
			break;
		}
		if(o.cara_pakai == ''||  o.cara_pakai == null){
			ShowPesanErrorKasirrwi('Terapi Obat : "Cara Pakai" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
			e=true;
			break;
		}
		if(o.verified == '' || o.verified==null){
			ShowPesanErrorKasirrwi('Terapi Obat : "Verified" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
			e=true;
			break;
		}
	} */
	if(e==false){
		Datasave_PenataJasaKasirRWI_on_kasirrrwi(false,true);
	}
}
function getVisiteDokterDiawal(no_transaksi, tgl_transaksi, kd_produk,urut)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionRWI/getVisiteDokterDiawal",
                        params: {no_transaksi: no_transaksi,
								 tgl_transaksi: tgl_transaksi,
								 kd_produk:kd_produk,
								 urut:urut
								},
                        failure: function (o)
                        {
                            ShowPesanErrorKasirrwi('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.processResult === 'SUCCESS')
                            {	
									nmDokDiAwal="( "+cst.nama_dok+" )";
								for (var i = 0; i < PenataJasaKasirRWI.dsGridTindakan.getCount(); i++) {
									//namaProduk=PenataJasaKasirRWI.dsGridTindakan.getRange()[i].data.DESKRIPSI;
									if (kd_produk===PenataJasaKasirRWI.dsGridTindakan.getRange()[i].data.KD_PRODUK && urut===PenataJasaKasirRWI.dsGridTindakan.getRange()[i].data.URUT)
									{
										//PenataJasaKasirRWI.dsGridTindakan.getRange()[i].set("DESKRIPSI", namaProduk+nmDokDiAwal);
										PenataJasaKasirRWI.dsGridTindakan.getRange()[i].set("JUMLAH_DOKTER", cst.jumlah);
									}

                                }   
								
                            }
                        }
                    }

            )
}
function RefreshDataDetail_kasirrwi(no_transaksi)
{
	dsTRDetailKasirRWIList_panatajasarwi.removeAll();
	loadMask.show();
    var strKriteriaRWI = '';
    // strKriteriaRWI = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaRWI = "no_transaksi = '" + no_transaksi + "' and kd_kasir = '02' "; //order by tgl_transaksi, urut asc
    // strKriteriaRWI = 'no_transaksi = ~0000004~';

    dsTRDetailKasirRWIList_panatajasarwi.load
	(
		{
			params:
			{
				Skip: 0,
				Take: 1000,
				Sort: 'tgl_transaksi',
				// Sort: 'tgl_transaksi',
				Sortdir: 'ASC',
				target: 'ViewDetailRWIGridBawah_REVISI',
				param: strKriteriaRWI
			},
			callback:function(){
				loadMask.hide();
			}
		}
	);
    return dsTRDetailKasirRWIList_panatajasarwi;
}
;

function TambahBarisIGD()
{
    var x = true;

    if (x === true)
    {
        var p = RecordBaruRWJ();
        dsTRDetailKasirRwiList.insert(dsTRDetailKasirRwiList.getCount(), p);
    }
    ;
}
;

function HapusBarisRwi()
{
    if (cellSelecteddeskripsi != undefined)
    {
        if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PRODUK != '')
        {



            var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function (btn, combo) {
                if (btn == 'ok')
                {
                    variablehistori = combo;
                    DataDeleteKasirRwiDetail();
                    dsTRDetailKasirRwiList.removeAt(CurrentKasirRwi.row);
                }
            });





        } else
        {
            dsTRDetailKasirRwiList.removeAt(CurrentKasirRwi.row);
        }
        ;
    }
}
;
function DataDeleteKasirRwiDetail()
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/DeleteDataObj",
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: getParamDataDeleteKasirRwiDetail(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirrwi(nmPesanHapusSukses, nmHeaderHapusData);
                                dsTRDetailKasirRwiList.removeAt(CurrentKasirRwi.row);
                                cellSelecteddeskripsi = undefined;
                                RefreshDataKasirRwiDetail(notransaksi);
                                AddNewKasirRwi = false;
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningKasirrwi(nmPesanHapusGagal, nmHeaderHapusData);
                            } else
                            {
                                ShowPesanWarningKasirrwi(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirRwiDetail()
{

    var params =
            {
                Table: 'ViewTrKasirRwi',
                TrKodeTranskasi: notransaksi,
                TrTglTransaksi: CurrentKasirRwi.data.data.TGL_TRANSAKSI,
                TrKdPasien: CurrentKasirRwi.data.data.KD_PASIEN,
                TrKdNamaPasien: namapasien,
                TrKdUnit: kodeunit,
                TrNamaUnit: namaunit,
                Uraian: CurrentKasirRwi.data.data.DESKRIPSI2,
                AlasanHapus: variablehistori,
                TrHarga: CurrentKasirRwi.data.data.HARGA,
                TrKdProduk: CurrentKasirRwi.data.data.KD_PRODUK,
                RowReq: CurrentKasirRwi.data.data.URUT,
                Hapus: 2
            };

    return params
}
;

function RefreshDataKasirRwiDetail(no_transaksi)
{
    var strKriteriaRwi = '';
    //strKriteriaRwi = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaRwi = "\"no_transaksi\" = ~" + no_transaksi + "~";
    //strKriteriaRwi = 'no_transaksi = ~0000004~';

    dsTRDetailKasirRwiList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailTRRWJ',
                                    param: strKriteriaRwi
                                }
                    }
            );
    return dsTRDetailKasirRwiList;
}
;
function RecordBaruRWJ()
{

    var p = new mRecordRwi
            (
                    {
                        'DESKRIPSI2': '',
                        'KD_PRODUK': '',
                        'DESKRIPSI': '',
                        'KD_TARIF': '',
                        'HARGA': '',
                        'QTY': '',
                        'TGL_TRANSAKSI': tanggaltransaksitampung,
                        'DESC_REQ': '',
                        'KD_TARIF':'',
                                'URUT': ''
                    }
            );

    return p;
}
;


function TRRawatJalanColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiRwi',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 100,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiRwi',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 220,
                            editor: getItemProdukRWI(),
                            /* new Ext.form.TextField
                             (
                             {
                             id:'fieldcolKasirRWI',
                             allowBlank: false,
                             enableKeyEvents : true,
                             listeners:
                             {
                             'specialkey' : function()
                             {
                             if (Ext.EventObject.getKey() === 13)
                             {
                             var str='';
                             
                             
                             str =  kodeunit;
                             strb='';
                             
                             if(Ext.get('fieldcolKasirRWI').dom.value != undefined || Ext.get('fieldcolKasirRWI').dom.value  != '')
                             {
                             strb= "and lower(deskripsi) like lower(~"+ Ext.get('fieldcolKasirRWI').dom.value+"%~)";
                             }
                             else
                             {
                             strb='';
                             }
                             GetLookupAssetCMRIGD(str,strb);
                             Ext.get('fieldcolKasirRWI').dom.value='';
                             };
                             }
                             }
                             }
                             ) */

                        }
                        ,
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {
                                if (record.data.TGL_TRANSAKSI == undefined || record.data.TGL_TRANSAKSI == null) {
                                    record.data.TGL_TRANSAKSI = nowTglTransaksi2;
                                    return record.data.TGL_TRANSAKSI;
                                } else {
                                    if (record.data.TGL_TRANSAKSI.substring(5, 4) == '-') {
                                        return ShowDate(record.data.TGL_TRANSAKSI);
                                    } else {
                                        var tglb = record.data.TGL_TRANSAKSI.split("-");

                                        if (tglb[2].length == 4 && isNaN(tglb[1])) {
                                            return record.data.TGL_TRANSAKSI;
                                        } else {
                                            return ShowDate(record.data.TGL_TRANSAKSI);
                                        }
                                    }

                                }
                                //return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colHARGARwi',
                            header: 'Harga',
                            align: 'right',
                            hidden: true,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colProblemRwi',
                            header: 'Qty',
                            width: 91,
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolProblemRwi',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {

                                                                Dataupdate_KasirRwi(false);
                                                                //RefreshDataFilterKasirRwi();
                                                                //RefreshDataFilterKasirRwi();

                                                            }
                                                        }
                                            }
                            ),
                        },
                        {
                            id: 'colImpactRwi',
                            header: 'CR',
                            width: 80,
                            dataIndex: 'IMPACT',
                            hidden: true,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolImpactRwi',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30
                                            }
                                    )

                        }

                    ]
                    )
}
;


function getItemProdukRWI() {

    var ItemComboboxRWI = new Ext.form.ComboBox({
        id: 'cboTindakanRequestRWI',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        hideTrigger: true,
        forceSelection: true,
        selectOnFocus: true,
        mode: 'local',
        store: DataProdukRWI,
        valueField: 'DESKRIPSI',
        displayField: 'DESKRIPSI',
        anchor: '95%',
        listeners: {
            'select': function (a, b, c) {
                dsTRDetailKasirRwiList.data.items[CurrentDataRWI].data.KD_PRODUK = b.data.KD_PRODUK;
                dsTRDetailKasirRwiList.data.items[CurrentDataRWI].data.KD_KLAS = b.data.KD_KLAS;
                dsTRDetailKasirRwiList.data.items[CurrentDataRWI].data.HARGA = b.data.TARIF;
                dsTRDetailKasirRwiList.data.items[CurrentDataRWI].data.TGL_BERLAKU = b.data.TGL_BERLAKU;
                dsTRDetailKasirRwiList.data.items[CurrentDataRWI].data.KD_TARIF = b.data.KD_TARIF;
                dsTRDetailKasirRwiList.data.items[CurrentDataRWI].data.TGL_TRANSAKSI = nowTglTransaksi2;
            }
        }
    });
    return ItemComboboxRWI;
}

function getProdukRWI(kd_unit) {
    var str = 'LOWER(tarif.kd_tarif)=LOWER(~' + varkd_tarif_rwi + '~) and tarif.kd_unit= ~' + kd_unit + '~ ';
    DataProdukRWI.load({
        params: {
            Skip: 0,
            Take: 50,
            target: 'LookupProduk',
            param: str
        }
    });
    return DataProdukRWI;
}



function GetLookupAssetCMRIGD(str)
{

    var p = new mRecordRwi
            (
                    {
                        'DESKRIPSI2': '',
                        'KD_PRODUK': '',
                        'DESKRIPSI': '',
                        'KD_TARIF': '',
                        'HARGA': '',
                        'QTY': dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.QTY,
                        'TGL_TRANSAKSI': dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.TGL_TRANSAKSI,
                        'DESC_REQ': dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.DESC_REQ,
                        'KD_TARIF':dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.KD_TARIF,
                                'URUT': dsTRDetailKasirRwiList.data.items[CurrentKasirRwi.row].data.URUT
                    }
            );

    FormLookupKasir(str, strb, nilai_kd_tarif, dsTRDetailKasirRwiList, p, false, CurrentKasirRwi.row, false, syssetting_rwi);

}
function mComboStatusBayar_viKasirRwiKasir(){
    var cboStatuslunas_viKasirrwiKasir = new Ext.form.ComboBox({
		id: 'cboStatuslunas_viKasirrwiKasir',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Jenis',
		width:100,
		store: new Ext.data.ArrayStore({
			id: 0,
			fields:['Id','displayText'],
			data: [[1, 'Semua'], [2, 'Lunas'], [3, 'Belum Lunas']]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		value: selectCountStatusByr_viKasirrwiKasir,
		listeners:{
			'select': function (a, b, c){
				selectCountStatusByr_viKasirrwiKasir = b.data.displayText;
				//RefreshDataSetDokter();
				RefreshDataFilterKasirrwiKasir();
			}
		}
	});
    return cboStatuslunas_viKasirrwiKasir;
}
function loadDataComboUserLapPerincianPasienRWIKasir(param){
	if (param==='' || param===undefined) {
		param='';
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWI/kelaskamar",
		params: {kode:param},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbokelaskamarrwi.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsget_data_kamarrwi.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsget_data_kamarrwi.add(recs);
			}
		}
	});
}
function mCombokelas_kamarrwi()
{
    var Field = ['kelas_kamar','kamar','kelas'];
    dsget_data_kamarrwi = new WebApp.DataStore({fields: Field});
	loadDataComboUserLapPerincianPasienRWIKasir();
    cbokelaskamarrwi = new Ext.form.ComboBox
    (
        {
          
            id: 'cbokelaskamarrwirwi',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
			hideTrigger		: true,
            store: dsget_data_kamarrwi,
            valueField: 'kelas_kamar',
            displayField: 'kelas_kamar',
            width: 100,
			anchor: '100%',
			fieldLabel: ' Kelas',
            listeners:
			{
				'select': function(a, b, c)
				{RefreshDataFilter_panatajasarwi();
				/* 	Ext.getCmp('txtNamaPasien_LapPerincianPasienRWI').setValue(b.data.nama);
					Ext.getCmp('txtNoKamar_LapPerincianPasienRWI').setValue(b.data.kamar);
					Ext.getCmp('txtKelas_LapPerincianPasienRWI').setValue(b.data.kelas);
					Ext.getCmp('dtpTglMasukAwalFilterHasilLab').setValue(ShowDate(b.data.tgl_masuk));
					Ext.getCmp('dtpTglMasukAkhirFilterHasilLab').setValue(ShowDate(b.data.tgl_inap));
					KdKasir=b.data.kd_kasir; */
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cbokelaskamarrwi.lastQuery != '' ){
								var value="";
								
								if (value!=cbokelaskamarrwi.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url:  baseURL + "index.php/main/functionRWI/kelaskamar",
										params: {kode:cbokelaskamarrwi.lastQuery},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											cbokelaskamarrwi.store.removeAll();
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsget_data_kamarrwi.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsget_data_kamarrwi.add(recs);
											}
											a.expand();
											if(dsget_data_kamarrwi.onShowList != undefined)
												dsget_data_kamarrwi.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
										}
									});
									value=cbokelaskamarrwi.lastQuery;
								}
							}
						},1000);
					}
				} 
			}
        }
    )
    return cbokelaskamarrwi;
};
function mComboUnit_viKasirrwiKasir()
{
    var Field = ['KD_UNIT', 'NAMA_UNIT'];

    dsunit_viKasirrwiKasir = new WebApp.DataStore({fields: Field});
    dsunit_viKasirrwiKasir.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_unit',
                                    Sortdir: 'ASC',
                                    target: 'ComboUnit',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboUNIT_viKasirrwiKasir = new Ext.form.ComboBox
            (
                    {
                        id: 'cboUNIT_viKasirrwiKasir',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        // fieldLabel:  ' Poli Tujuan',
                        align: 'Right',
                        width: 100,
                        anchor: '100%',
                        store: dsunit_viKasirrwiKasir,
                        valueField: 'NAMA_UNIT',
                        displayField: 'NAMA_UNIT',
                        value: 'All',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        RefreshDataFilterKasirrwiKasir();

                                        //selectStatusCMKasirrwiView = b.data.DEPT_ID;
                                        //
                                    }

                                }
                    }
            );

    return cboUNIT_viKasirrwiKasir;
}
;

function mComboAlasan_pulang()
{
    var Field = ['KD_ALASAN', 'ALASAN_PULANG'];
    var dsAlasan_pulang;
    dsAlasan_pulang = new WebApp.DataStore({fields: Field});
    dsAlasan_pulang.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_unit',
                                    Sortdir: 'ASC',
                                    target: 'ComboAlasanPulang',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboAlasan_pulang = new Ext.form.ComboBox
            (
                    {
                        id: 'cboAlasan_pulang',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Alasan Pulang',
                        align: 'Right',
                        width: 100,
                        anchor: '100%',
                        store: dsAlasan_pulang,
                        valueField: 'KD_ALASAN',
                        displayField: 'ALASAN_PULANG',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tapungalasan = b.data.KD_ALASAN;
                                    }

                                }
                    }
            );

    return cboAlasan_pulang;
};

function KasirLookUprwi(rowdata){
    var lebar = 580;
	hiddenpnlInacgbpenjasrwi=true;
	// if((rowdata.KD_CUSTOMER=='0000000002' || rowdata.KD_CUSTOMER=='0000000009') && (rowdata.PINDAH_KELAS=='1' || rowdata.PINDAH_KELAS=='2' || rowdata.PINDAH_KELAS=='3' || rowdata.PINDAH_KELAS=='VIP')){
		hiddenpnlInacgbpenjasrwi=false;
	// }
    FormLookUpsdetailTRKasirrwi = new Ext.Window({
		id: 'gridKasirrwi',
		title: 'Kasir Rawat Inap',
		closeAction: 'destroy',
		width: lebar,
		height: 600,
		border: false,
		layout:'fit',
		resizable: false,
		iconCls: 'Edit_Tr',
		modal: true,
		items: getFormEntryKasirrwi(lebar,rowdata),
	});
    FormLookUpsdetailTRKasirrwi.show();
    

	Ext.Ajax.request({
		url: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
		params: {
	    	select 	: " setting ",
			where 	: " key_data = 'eklaim_customer_bpjs' ", 
			table 	: " sys_setting ",
		},
		failure: function (o)
		{
			ShowPesanErrorKasirrwi('Error Memulangkan Pasien. Hubungi Admin!', 'WARNING');
		},
		success: function (o)
		{
			var cst    = Ext.decode(o.responseText);
			var string = cst[0].setting;
			var array  = string.split(",");
			for (var i = 0; i < array.length; i++) {
				if (rowdata.KD_CUSTOMER == array[i] && status_customer === false) {
					status_customer = true;
				}		
			}

			if(status_customer === true){
				getInacbgPenjasRwi(rowdata.NO_SJP);
			}
		}
	});

    if (rowdata == undefined){
        KasirrwiAddNew();
    }else{
        TRKasirRwiInit(rowdata);
		if(rowdata.LUNAS == '0'){
			getTotalBayarSummaryKasirRWI();
		} 
    }
}
function getInacbgPenjasRwi(sep){
	Ext.Ajax.request({
		url: baseURL + "index.php/main/eklaim?sep="+sep+"&metode=get",
		params: {
			metode 	: 'get',
			no_transaksi: rowSelectedKasirrwiKasir.data.NO_TRANSAKSI,
		},
		failure: function (o){},
		success: function (o){
			var cst = Ext.decode(o.responseText);
			if(cst.metadata.code==200){
				console.log(cst);
				if (cst.response.data.grouper.response != null) {
					var tagihan=0;
					var dijamin=cst.response.data.grouper.response.cbg.tariff;
					var vip=0;
					var kelas='';
					if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='1'){
						tagihan=cst.response.data.grouper.tarif_alt[0].tarif_inacbg;
					}else if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='2'){
						tagihan=cst.response.data.grouper.tarif_alt[1].tarif_inacbg;
					}else if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='3'){
						tagihan=cst.response.data.grouper.tarif_alt[2].tarif_inacbg;
					}else if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='VIP'){
						tagihan=cst.response.data.grouper.tarif_alt[0].tarif_inacbg;
						// vip=((cst.response.data.grouper.tarif_alt[0].tarif_inacbg/1000)*7.5);
						vip=( (cst.response.data.grouper.tarif_alt[0].tarif_inacbg*0.75) );
						if(rowSelectedKasirrwiKasir.data.HAK_KELAS=='1'){
							// dijamin=0;
							dijamin=cst.response.data.grouper.tarif_alt[0].tarif_inacbg;
						}else if(rowSelectedKasirrwiKasir.data.HAK_KELAS=='2'){
							dijamin=cst.response.data.grouper.tarif_alt[1].tarif_inacbg;
							
						}else if(rowSelectedKasirrwiKasir.data.HAK_KELAS=='3'){
							dijamin=cst.response.data.grouper.tarif_alt[2].tarif_inacbg;
						}
					}
					Ext.getCmp('pnlInacgbpenjasrwi').setTitle('Tambahan Biaya Yang Dibayar Pasien Untuk Naik Kelas '+rowSelectedKasirrwiKasir.data.KELAS_RAWAT);
					Ext.getCmp('txtinacbg1penjasrwj').setValue(tagihan);
					Ext.getCmp('txtinacbg2penjasrwj').setValue(dijamin);
					Ext.getCmp('txtinacbg4penjasrwj').setValue(Math.round(vip));
					Ext.getCmp('txtinacbg3penjasrwj').setValue(tagihan-dijamin+Math.round(vip));
					var lbl=parseInt(tagihan).toLocaleString(window.navigator.language,{style: 'currency', currency:'IDR',minimumFractionDigits:0 })+' - '+
						parseInt(dijamin).toLocaleString(window.navigator.language,{style: 'currency', currency:'IDR',minimumFractionDigits:0 });
					if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='VIP'){
						lbl+=' + ( '+
						parseInt(cst.response.data.grouper.tarif_alt[0].tarif_inacbg).toLocaleString(window.navigator.language,{style: 'currency', currency:'IDR',minimumFractionDigits:0 })+' X 0.75 ) ';
					}
					Ext.getCmp('lblinacbhiting').setValue(lbl);
					Ext.getCmp('lblinacbdiagnosa').setValue(cst.response.data.grouper.response.cbg.code+' - '+cst.response.data.grouper.response.cbg.description);
					// ShowPesanInfoKasirrwi('Pembayaran berhasil disimpan', 'Information');
				}else{
					ShowPesanWarningKasirrwi("Data belum di grouping", 'Keterangan');
				}
			}else{
				ShowPesanWarningKasirrwi(cst.metadata.message, 'Gagal');
				Ext.getCmp('txtinacbg1penjasrwj').setValue(0);
				Ext.getCmp('txtinacbg2penjasrwj').setValue(0);
				Ext.getCmp('txtinacbg3penjasrwj').setValue(0);
				Ext.getCmp('txtinacbg4penjasrwj').setValue(0);
				Ext.getCmp('lblinacbhiting').setValue('');
				Ext.getCmp('lblinacbdiagnosa').setValue('');
			}
		}
	});
}
/*
    FORM POSTING MANUAL
 */

	function formLookupPostingManual_KASIRRWI() 
	{    
        var paneltotal = new Ext.Panel
        (
            {
                id: 'paneltotal',
                region: 'center',
                border: true,
                bodyStyle: 'padding:0px 0px 0px 0px',
                layout: 'column',
                frame: true,
                height: 40,
                width: 360,
                autoScroll: false,
                items:
                [
                    {
                        xtype: 'compositefield',
                        anchor: '100%',
                        labelSeparator: '',
                        border: true,
                        style: {'margin-top': '3px'},
                        items:
                        [
                            {
                                layout: 'form',
                                style: {'text-align': 'right', 'margin-left': '90px'},
                                border: false,
                                html: " Total :"
                            },
                            {
                                xtype: 'textfield',
                                id: 'txtJumlah2EditData_viKasirrwi',
                                name: 'txtJumlah2EditData_viKasirrwi',
                                style: {'text-align': 'right', 'margin-left': '95px'},
                                width: 82,
                                readOnly: true,
                            },
                                    //-------------- ## --------------
                        ]
                    }
                ]

            }
        );
        FormLookUpPostingManualKasirrwi = new Ext.Window
		(
			{
				id: 'gridPostingManual_KASIRRWI',
				title: 'Posting Manual',
				closeAction: 'destroy',
				width: 360,
				height: 400,
				border: false,
				resizable: false,
				plain: true,
				layout: 'fit',
				iconCls: 'Request',
				modal: true,
				bodyStyle:'padding: 3px;',
				items: [
                {
					columnWidth: .40,
					layout: 'form',
					labelWidth: 100,
					border: false,
					bodyStyle:'padding: 10px;',
					items:[
						cboProdukPosting_KASIRRWI(),
						{
							xtype : 'textfield',
							id : 'TxtReferensi',
							fieldLabel : 'Referensi',
                            anchor: '100%',
                            bodyStyle:'margin:10px;',
						},
						GridPostingManual(),
                        // paneltotal
					]
				},
				],
				bbar:[                            
                    {xtype: 'tbspacer', width: 200},
                    {
                        xtype       : 'label',
                        text        :'Total :'
                    },
                    {xtype: 'tbspacer', width: 10},
                    {
                        xtype       : 'numberfield',
                        id          : 'txtFieldTotalHarga',
                        width       : 100,
                        readOnly    : true,
                    },
				],
                tbar:[
                    {
                        text: 'Simpan',
                        id: 'btnSimpanPosting',
                        tooltip: 'Posting Manual',
                        iconCls: 'save',
                        handler: function ()
                        {
                            savePostingManualKasirRWI();
                        }
                    },
                    {
                     text: 'Batal',
                     id: 'btnBatalPosting',
                     iconCls: 'cancel',
                     handler: function ()
                     {
                     }
                    },        
                ],
				listeners:
				{
				
				}
            }
		);

        FormLookUpPostingManualKasirrwi.show();
    };
    var FormLookUpPostingManualKasirrwi;

    function cboProdukPosting_KASIRRWI(){
	    var Field = ['KD_PRODUK','DESKRIPSI'];

	    DataStoreGridPoduk = new WebApp.DataStore({ fields: Field });
    	loaddatastoreProdukPosting();
        var cboPostingManual_KASIRRWI       = new Ext.form.ComboBox
        ({
            id: 'cboPostingManual',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            labelWidth:120,
            emptyText: '',
            fieldLabel: 'Produk',
            text:'Produk',
            align: 'Right',
            anchor: '100%',
            bodyStyle:'margin:10px;',
            store: dsDataStoreGridPoduk,
            valueField: 'KD_PRODUK',
            displayField: 'DESKRIPSI',
            listeners:
                {
                    'select': function (a, b, c)
                    {
                        tmpkd_produkpostingmanual = b.data.KD_PRODUK;
                    },
                }
        })
        return cboPostingManual_KASIRRWI;
    };
    var tmpkd_produkpostingmanual;
    var gridManualPostingKasirrwi;
    function GridPostingManual(){
        dsDataStoreGridKomponent.removeAll();

        
	    var Field = ['KD_COMPONENT','COMPONENT','KD_JENIS'];
	    DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
    	loaddatastoreProdukComponent();

	    var cm = new Ext.grid.ColumnModel({
	        // specify any defaults for each column
	        defaults: {
	            sortable: false // columns are not sortable by default           
	        },
	        columns: [
            {
	            header: 'Component',
	            dataIndex: 'COMPONENT',
	            width: 200
	        }, 
            {
	            header: 'Tarif',
	            dataIndex: 'TARIF',
                align: 'right',
	            width: 70,
                editor: new Ext.form.TextField
                (
                        {
                            id: 'fieldcolTunairwi',
                            allowBlank: true,
                            enableKeyEvents: true,
                            width: 30,
                        }
                ),
                renderer: function (v, params, record)
                {
                    gettotalpostingmanual();
                    return formatCurrency(record.data.TARIF);

                },
	        }]
	    });

        gridManualPostingKasirrwi = new Ext.grid.EditorGridPanel
        (
            {
                title: '',
                id: 'gridManualPostingKasirrwi',
                store: dsDataStoreGridKomponent,
                clicksToEdit: 1, 
                editable: true,
                border: true,
                columnLines: true,
                frame: false,
                stripeRows       : true,
                trackMouseOver   : true,
                height: 250,
                width: 325,
                autoScroll: true,
                sm: new Ext.grid.CellSelectionModel
                (
                    {
                        singleSelect: true,
                        listeners:
                        {
                        }
                    }
                ),
                cm: cm,
                viewConfig: {
                    forceFit: true
                },  
            }
        );
        return gridManualPostingKasirrwi;
    };

    function loaddatastoreProdukPosting(){
        Ext.Ajax.request({
            url: baseURL +  "index.php/rawat_inap/control_cmb_posting/cmbProduk",
            params: {
                null : null,
            },
            success: function(response) {
                //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
                var cst  = Ext.decode(response.responseText);
                //dsDataPerawatPenindak_KASIR_RWI.load(myData);
                for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                    var recs    = [],recType = DataStoreGridPoduk.recordType;
                    var o       = cst['data'][i];
                    recs.push(new recType(o));
                    dsDataStoreGridPoduk.add(recs);
                }
            },
        });
    }

    function loaddatastoreProdukComponent(){
        Ext.Ajax.request({
            url: baseURL +  "index.php/rawat_inap/control_cmb_posting/gridComponent",
            params: {
                null : null,
            },
            success: function(response) {
                //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
                var cst  = Ext.decode(response.responseText);
                //dsDataPerawatPenindak_KASIR_RWI.load(myData);
                for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                    var recs    = [],recType = DataStoreGridKomponent.recordType;
                    var o       = cst['data'][i];
                    recs.push(new recType(o));
                    dsDataStoreGridKomponent.add(recs);
                }
            },
        });
    }

function load_data_printer_kasirrwi(param)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/printer",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			//cbopasienorder_mng_apotek.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsprinter_kasirrwi.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsprinter_kasirrwi.add(recs);
			}
		}
	});
}
function mCombo_printer_kasirrwi()
{ 
 
	var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];

    dsprinter_kasirrwi = new WebApp.DataStore({ fields: Field });
	
	load_data_printer_kasirrwi();
	var cbo_printer_kasirrwi= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_printer_kasirrwi',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: 'Pilih Printer',
			fieldLabel:  '',
			align: 'Right',
			width: 100,
			store: dsprinter_kasirrwi,
			valueField: 'name',
			displayField: 'name',
			//hideTrigger		: true,
			listeners:
			{
								
			}
		}
	);return cbo_printer_kasirrwi;
};



function getFormEntryKasirrwi(lebar,rowdata){
    var pnlTRKasirrwi = new Ext.FormPanel({
		id: 'PanelTRKasirrwi',
		fileUpload: true,
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:4px;',
		style: 'padding:4px;',
		height: 119,
		border: true,
		items: [
			getItemPanelNoTransksirwiKasir(lebar), 
			getItemPanelHakKelasrwiKasir(lebar),
			getItemPanelmedreckasir(lebar), 
			getItemPanelUnitKasir(lebar)
		]
	});
    var x;
    var pnlTRKasirrwi2 = new Ext.FormPanel({
		id: 'PanelTRKasirrwi2',
		fileUpload: true,
		layout: 'fit',
		flex:1,
		bodyStyle: 'padding:0px 4px 4px 4px;',
		border: false,
		items: [getTabPanelPembayaranSummaryDetailKasirRWI(rowdata)]
	});
    var FormDepanKasirrwi = new Ext.Panel({
			id: 'FormDepanKasirrwi',
			layout: {
				type:'vbox',
				align:'stretch'
			},
			bodyStyle: 'padding:4px;',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			items: [pnlTRKasirrwi, pnlTRKasirrwi2]
		}
	);
    return FormDepanKasirrwi
}

function pasien_pulang(rowdata)
{
    var lebar = 580;



    var Formpasien_pulang = new Ext.FormPanel
            (
                    {
                        id: 'Formpasien_pulang',
                        region: 'center',
                        height: 200,
                        anchor: '100%',
                        layout: 'form',
                        border: true,
                        items:
                                [
                                    getItemPanelInputKasirRWIpulang(lebar)
                                ]
                    }
            );

    FormLookUpPasienPulang = new Ext.Window
            (
                    {
                        id: 'FormLookUpPasienPulang',
                        title: 'Kasir Rawat Inap',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 190,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: [Formpasien_pulang],
                        tbar:
                         [
                                    {
                                        text: 'Pulang',
                                        id: 'btnSimpanDiagnosa',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        handler: function ()
                                        {

                                            UpdateTutuptransaksi(false);

                                        }
                                    }
                                ], listeners:
                                {
                                }
                    }
            );

    FormLookUpPasienPulang.show();
    TRKasirRwiInitPasienPulang(rowdata);

}






function TambahBarisKasirrwi()
{
    var x = true;

    if (x === true)
    {
        var p = RecordBaruKasirrwi();
        dsTRDetailKasirrwiKasirList.insert(dsTRDetailKasirrwiKasirList.getCount(), p);
    }
    ;
}
;





function GetDTLTRHistoryGrid()
{

    var fldDetail = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'KD_UNIT', 'KD_USER'];

    dsTRDetailHistoryList = new WebApp.DataStore({fields: fldDetail})

    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'History Bayar',
                        stripeRows: true,
                        store: dsTRDetailHistoryList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        height: 400,
                        anchor: '100%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
                                                            cellSelecteddeskripsi = dsTRDetailHistoryList.getAt(row);
                                                            CurrentHistory_rwi.row = row;
                                                            CurrentHistory_rwi.data = cellSelecteddeskripsi;
                                                            // tmpKdUser = 
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRHistoryColumModel()
                        , viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnHpsBrsKasirrwi',
                                        text: 'Hapus Pembayaran',
                                        tooltip: 'Hapus Baris',
                                        iconCls: 'RemoveRow',
                                        //hidden :true,
                                        handler: function ()
                                        {
                                            if (dsTRDetailHistoryList.getCount() > 0)
                                            {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                    if (CurrentHistory_rwi != undefined)
                                                    {
                                                        HapusBarisDetailbayar();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningKasirrwi('Pilih record ', 'Hapus data');
                                                }
                                            }
                                            Ext.getCmp('btnEditKasirrwi').disable();
                                            Ext.getCmp('btnLookUpProduk_RawatInap').disable();
                                            Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
                                            Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirrwi').disable();
                                            Ext.getCmp('btnHpsBrsKasirrwi').disable();
                                        },
                                        disabled: true
                                    }



                                ]

                    }


            );



    return gridDTLTRHistory;
}
;

function TRHistoryColumModel()
{
    return new Ext.grid.ColumnModel
            (//'','','','','',''
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'colKdUnitbayar',
                            header: 'KD Unit',
                            dataIndex: 'KD_UNIT',
                            //hidden: true,
                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'Urut Bayar',
                            dataIndex: 'URUT',
                            //hidden:true

                        },
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        },
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }

                        }
                        ,
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true

                        }
                        ,
                        {
                            id: 'colStatHistory',
                            header: 'History',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true




                        },
                        {
                            id: 'colPetugasHistory',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME',
                            //hidden:true




                        }


                    ]
                    )
}
;
function HapusBarisDetailbayar()
{
    if (cellSelecteddeskripsi != undefined)
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {


            //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
            var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function (btn, combo) {
                if (btn == 'ok')
                {
                    variablehistori = combo;
                    if (variablehistori != '')
                    {
                        DataDeleteKasirrwiKasirDetail();
                    } else
                    {
                        ShowPesanWarningKasirrwi('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

                    }
                }

            });






        } else
        {
            dsTRDetailHistoryList.removeAt(CurrentHistory_rwi.row);
        }
        ;
    }
}
;

function DataDeleteKasirrwiKasirDetail()
{
    Ext.Ajax.request
	({
		url: baseURL + "index.php/main/Controller_pembayaran/delete_pembayaran",
		params: getParamDataDeleteKasirrwiKasirDetail(),
		success: function (o)
		{
			RefreshDataFilterKasirrwiKasir();
			// RefreshDatahistoribayar('0');
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				RefreshDatahistoribayar(currentNoTransaksi_KasirRWI);
				RefreshDatahistoribayar(notransaksi);
				ShowPesanInfoKasirrwi('Berhasil menghapus pembayaran', 'Informasi');
				dsTRDetailHistoryList.removeAll();
				
				// DataDeleteKasirrwiKasirDetail_SQL();

				//refeshKasirrwiKasir();
			} else if (cst.success === false && cst.pesan === 0)
			{
				ShowPesanWarningKasirrwi('Gagal menghapus pembayaran', 'WARNING');
			} else
			{
				ShowPesanWarningKasirrwi('Gagal menghapus pembayaran', 'WARNING');
			}
			;
		}
	})
}
;

function getParamDataDeleteKasirrwiKasirDetail()
{
    var params =
            {
                //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
                //Table: 'ViewKasirRWI',// 
                TrKodeTranskasi: CurrentHistory_rwi.data.data.NO_TRANSAKSI,
                TrTglbayar: CurrentHistory_rwi.data.data.TGL_BAYAR,
                Urut: CurrentHistory_rwi.data.data.URUT,
                Tgltransaksi: tgltrans,
                Kodepasein: kodepasien,
                NamaPasien: namapasien,
                KodeUnit: kodeunit,
                Namaunit: namaunit,
                Kodepay: CurrentHistory_rwi.data.data.KD_PAY,
                Uraian: CurrentHistory_rwi.data.data.DESKRIPSI,
                Jumlah:  CurrentHistory_rwi.data.data.BAYAR,
                Username :CurrentHistory_rwi.data.data.USERNAME,
                Shift: 1,//shift na dipatok hela ke ganti di phpna
                Shift_hapus: 1,//shift na dipatok hela ke ganti di phpna
                Kd_user: CurrentHistory_rwi.data.data.KD_USER,
                alasan:variablehistori,
	    		KDkasir: 'default_kd_kasir_rwi',

            };
    Kdtransaksi = CurrentHistory_rwi.data.data.NO_TRANSAKSI;
    return params
}
;



function GetDTLTRKasirrwiGrid(){
    var fldDetail = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'BAYARTR', 'DISCOUNT', 'PIUTANG'];
    dsTRDetailKasirrwiKasirList = new WebApp.DataStore({fields: fldDetail})
    // RefreshDataKasirrwiKasirDetail();
    gridDTLTRKasirrwinap = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id: 'gridDTLTRKasirrwinap',
		store: dsTRDetailKasirrwiKasirList,
		border: true,
		flex:1,
		style:'padding: 4px;',
		columnLines: true,
		autoScroll: true,
		sm: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners:{}
		}),
		cm: TRKasirRawatJalanColumModel(),
		viewConfig: {
			forceFit: true
		},
		fbar:[
			{
				xtype: 'label',
				text: 'Jumlah bayar : '
			},{
				xtype: 'numberfield',
				id: 'txtTotalBayarDetailKasirRWI',
				width: 110,
				enableKeyEvents: true,
				listeners:{
					'specialkey': function (){
						if (Ext.EventObject.getKey() === 13){
							//hitungtotaltagihankasirrwi();
							var tmpHarga = 0;
							/*for(var x=0; x<dsTRDetailKasirrwiKasirList.getCount() ; x++){
								var o=dsTRDetailKasirrwiKasirList.getRange()[x].data;
								tmpHarga += parseInt(gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR'));
								//Ext.getCmp('txtJumlah2EditData_viKasirrwi').setValue(tmpHarga);
							}*/
							Ext.getCmp('txtJumlah2EditData_viKasirrw11i').setValue(parseInt(Ext.getCmp('txtJumlah2EditData_viKasirrwi').getValue()) - parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()) );/*
							Ext.getCmp('txtJumlah2EditData_viKasirrwi').setValue(formatCurrency(Ext.getCmp('txtJumlah2EditData_viKasirrwi').getValue()));
							Ext.getCmp('txtTotalBayarDetailKasirRWI').setValue(formatCurrency(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()));*/
						}
					},
					keyup : function(){
						var tmpHarga = 0;
						/*for(var x=0; x<dsTRDetailKasirrwiKasirList.getCount() ; x++){
							var o=dsTRDetailKasirrwiKasirList.getRange()[x].data;
							tmpHarga += parseInt(gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR'));
							//Ext.getCmp('txtJumlah2EditData_viKasirrwi').setValue(tmpHarga);
						}*/
						/*Ext.getCmp('txtJumlah2EditData_viKasirrw11i').setValue(tmpHarga - parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()));
						Ext.getCmp('txtJumlah2EditData_viKasirrwi').setValue(formatCurrency(Ext.getCmp('txtJumlah2EditData_viKasirrwi').getValue()));
						Ext.getCmp('txtTotalBayarDetailKasirRWI').setValue(formatCurrency(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()));
						if (Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()=='' || parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()) ==0) {
							Ext.getCmp('txtJumlah2EditData_viKasirrw11i').setValue(0);
						}*/
						Ext.getCmp('txtJumlah2EditData_viKasirrw11i').setValue(parseInt(Ext.getCmp('txtJumlah2EditData_viKasirrwi').getValue()) - parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()) );
					}
				}
			},{
				xtype: 'label',
				style: {'font-size': '11px','margin-top': '7px'},
				text: '*) Enter untuk hitung'
			},{
				xtype: 'textfield',
				id: 'txtJumlah2EditData_viKasirrw11i',
				name: 'txtJumlah2EditData_viKasirrw11i',
				width: 82,
				readOnly: true,
			},{
				xtype: 'textfield',
				id: 'txtJumlah2EditData_viKasirrwi',
				name: 'txtJumlah2EditData_viKasirrwi',
				width: 82,
				readOnly: true,
			},
		]		
	});
    return gridDTLTRKasirrwinap;
}
function TRKasirRawatJalanColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiKasirrwi',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 250,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiKasirrwi',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            id: 'colURUTKasirrwi',
                            header: 'Urut',
                            dataIndex: 'URUT',
                            sortable: false,
                            hidden: true,
                            menuDisabled: true,
                            width: 250

                        }
                        ,
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            hidden: true,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colQtyKasirrwi',
                            header: 'Qty',
                            width: 91,
                            align: 'right',
                            menuDisabled: true,
							dataIndex: 'QTY',
                        },
                        {
                            id: 'colHARGAKasirrwi',
                            header: 'Harga',
                            align: 'right',
                            hidden: false,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colPiutangKasirrwi',
                            header: 'Puitang',
                            width: 80,
                            dataIndex: 'PIUTANG',
                            align: 'right',
                            //hidden: false,

                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolPuitangrwi',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    )
                            ,
                            renderer: function (v, params, record)
                            {

                                //getTotalDetailProdukRWI();

                                return formatCurrency(record.data.PIUTANG);




                            }
                        },
                        {
                            id: 'colTunaiKasirrwi',
                            header: 'Tunai',
                            width: 80,
                            dataIndex: 'BAYARTR',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolTunairwi',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {

                                getTotalDetailProdukRWI();

                                return formatCurrency(record.data.BAYARTR);


                            }

                        },
                        {
                            id: 'colDiscountKasirrwi',
                            header: 'Discount',
                            width: 80,
                            dataIndex: 'DISCOUNT',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolDiscountrwi',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    )
                            ,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.DISCOUNT);


                            }


                        }

                    ]
                    )
}
;




function RecordBaruKasirrwi()
{

    var p = new mRecordKasirrwi
            (
                    {
                        'DESKRIPSI2': '',
                        'KD_PRODUK': '',
                        'DESKRIPSI': '',
                        'KD_TARIF': '',
                        'HARGA': '',
                        'QTY': '',
                        'TGL_TRANSAKSI': tanggaltransaksitampung,
                        'DESC_REQ': '',
                        'KD_TARIF':'',
                                'URUT': ''
                    }
            );

    return p;
}
;
function TRKasirRwiInit(rowdata)
{
    AddNewKasirrwiKasir = false;
    vkd_unit = rowdata.KD_UNIT;
    RefreshDataKasirrwiKasirDetail(rowdata.NO_TRANSAKSI);
    Ext.get('txtNoTransaksiKasirrwiKasir').dom.value = rowdata.NO_TRANSAKSI;
    tanggaltransaksitampung_utama = rowdata.TANGGAL_TRANSAKSI;
    tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
    Ext.get('txtNoMedrecDetransaksi').dom.value = rowdata.KD_PASIEN;
    Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
    Ext.get('cboPembayaranKasirRWI').dom.value = rowdata.KET_PAYMENT;
    Ext.get('cboJenisByr').dom.value = rowdata.CARA_BAYAR; // take the displayField value 
    //loaddatastorePembayaran(rowdata.JENIS_PAY);
    vkode_customer = rowdata.KD_CUSTOMER;
    tampungtypedata = 0;
    tapungkd_pay = rowdata.KD_PAY;
    jenispay = 1;
    var vkode_customer = rowdata.LUNAS


    showCols(Ext.getCmp('gridDTLTRKasirrwinap'));
    hideCols(Ext.getCmp('gridDTLTRKasirrwinap'));
    vflag = rowdata.FLAG;
    //(rowdata.NO_TRANSAKSI;


    Ext.Ajax.request(
	{
		url: baseURL + "index.php/main/getcurrentshift",
		params: {
			command: '0',
		},
		failure: function (o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			tampungshiftsekarang = o.responseText
		}

	});
	
	/* INI BUAT INFO SHIFT */
	Ext.Ajax.request({
									   
		url: baseURL + "index.php/main/functionShift/getshift",
		 params: {
			kd_unit:'1001'
            // kd_unit:tmpKdUnitKamar
		},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var shift_tampung = o.responseText;
			Ext.getCmp('txtShiftAktif').setValue(shift_tampung);
		}
	
	});

}
;

function TRKasirRwiInitPasienPulang(rowdata)
{


    Notransaksi = rowdata.NO_TRANSAKSI;
    KDUnit = rowdata.KD_UNIT;
    KDCustomer = rowdata.KD_CUSTOMER;
    URUTMasuk = rowdata.URUT_MASUK;
    NOkamar = rowdata.NO_KAMAR;
    KDkelas = rowdata.KD_KELAS;
    Ext.get('txtkdPasienPasienPulang').dom.value = rowdata.KD_PASIEN;
    Ext.get('txtPasienPasienPulang').dom.value = rowdata.NAMA;
    Ext.get('txtKelasPasienPulang').dom.value = rowdata.KELAS;
    Ext.get('txtkamarPasienPulang').dom.value = rowdata.NAMA_KAMAR;
    Ext.get('dtpTanggalMasuktransaksiPasienPulang').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
    KDunitkamar = rowdata.KD_UNIT_KAMAR;
    TGLNGINAP = rowdata.TGL_NGINAP;

}
;

function mEnabledKasirrwiCM(mBol)
{

    Ext.get('btnLookupKasirrwi').dom.disabled = mBol;
    Ext.get('btnHpsBrsKasirrwi').dom.disabled = mBol;
}
;


///---------------------------------------------------------------------------------------///
function KasirrwiAddNew()
{
    AddNewKasirrwiKasir = true;
    Ext.get('txtNoTransaksiKasirrwiKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
    Ext.get('txtNoMedrecDetransaksi').dom.value = '';
    Ext.get('txtNamaPasienDetransaksi').dom.value = '';

    //Ext.get('txtKdUrutMasuk').dom.value = '';
    Ext.get('cboStatuslunas_viKasirrwiKasir').dom.value = ''
    rowSelectedKasirrwiKasir = undefined;
    dsTRDetailKasirrwiKasirList.removeAll();
    mEnabledKasirrwiCM(false);


}
;

function RefreshDataKasirrwiKasirDetail(no_transaksi)
{
    var strKriteriaKasirrwi = '';

    strKriteriaKasirrwi = 'no_transaksi = ~' + no_transaksi + '~ and kd_kasir=~02~';

    dsTRDetailKasirrwiKasirList.load
	(
		{
			params:
			{
				Skip: 0,
				Take: 1000,
				Sort: 'tgl_transaksi',
				//Sort: 'tgl_transaksi',
				Sortdir: 'ASC',
				target: 'ViewDetailbayarRWI',
				param: strKriteriaKasirrwi
			}
		}
	);
    return dsTRDetailKasirrwiKasirList;
}
;

function RefreshDatahistoribayar(no_transaksi)
{
    var strKriteriaKasirrwi = '';

    strKriteriaKasirrwi = 'no_transaksi= ~' + no_transaksi + '~'+' AND kd_kasir=~02~';

    dsTRDetailHistoryList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewHistoryBayarRWI',
                                    param: strKriteriaKasirrwi
                                }
                    }
            );
    return dsTRDetailHistoryList;
}
;


///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamDetailTransaksiKasirrwi()
{
    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    }
    ;

    /*var params =
            {
                //	Table:'',

                kdUnit: vkd_unit,
                TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwiKasir').getValue(),
                Tgl: Ext.get('txtTanggalTransaksiKASIRRWI').getValue(),
                Shift: tampungshiftsekarang,
                Flag: vflag,
                bayar: tapungkd_pay,
                Typedata: tampungtypedata,
                //Totalbayar: getTotalDetailProdukRWI(),
                Totalbayar: Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue(),
                List: getArrDetailTrKasirrwi(),
                JmlField: mRecordKasirrwi.prototype.fields.length - 4,
                JmlList: GetListCountDetailTransaksi(),
                Hapus: 1,
                Ubah: 0,
				urut_masuk:vUrutMasuk,
				kd_kasir:'default_kd_kasir_rwi'
            };*/
    var params = {
    	no_transaksi    :Ext.get('txtNoTransaksiKasirrwiKasir').getValue(),
        kd_kasir        :'02',
        payment         :tapungkd_pay,
        dijamin         :Ext.getCmp('txtinacbg2penjasrwj').getValue(),
        bayar	         :Ext.getCmp('txtinacbg3penjasrwj').getValue(),
        nominal_1       :Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue(),
        nominal         :Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue(),
        tgl_pembayaran  :Ext.getCmp('txtTanggalTransaksiKASIRRWI').getValue(),
        sisa_bayar      :Ext.getCmp('txtJumlah2EditData_viKasirrwi').getValue(),
        data            :getArrDetailTrKasirrwi(),
        jumlah          :GetListCountDetailTransaksi(),
        catatan          :Ext.getCmp('lblinacbhiting').getValue(),
        diagnosa          :Ext.getCmp('lblinacbdiagnosa').getValue(),
        object          :'kasir',
        modul           :'Rawat Inap',
		kd_unit			:tmpKdUnitKamar  //update maya
    };
    return params;
}
;

function paramUpdateTransaksi()
{
    var params =
            {
                //  TrKodeTranskasi: cellSelectedtutup_kasirrwi

                TrKodeTranskasi: notransaksi,
              /*   TrKDUnit: KDUnit,
                KDcus: KDCustomer,
                TrURUTMasuk: URUTMasuk,
                TrNOkamar: NOkamar,
                TrKDkelas: KDkelas,
                TrTgl: Ext.get('dtpTanggalMasuktransaksiPasienPulang').getValue(),
                TrKdPasien: Ext.get('txtkdPasienPasienPulang').getValue(),
                Trtapungalasan: tapungalasan,
                TrUnit_kamar: KDunitkamar,
                TrTGLNGINAP: ShowDate(TGLNGINAP),
                KDUnit: kdUnit */
            };
    return params
};

function GetListCountDetailTransaksi()
{

    var x = 0;
    for (var i = 0; i < dsTRDetailKasirrwiKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirrwiKasirList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirrwiKasirList.data.items[i].data.DESKRIPSI != '')
        {
            x += 1;
        }
        ;
    }
    return x;

}
;

function getTotalDetailProdukRWI(){
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < dsTRDetailKasirrwiKasirList.getCount(); i++){
        if (tampungtypedata == 0){
            tampunggrid = parseInt(dsTRDetailKasirrwiKasirList.data.items[i].data.BAYARTR);
            //recordterakhir= tampunggrid
            //TotalProduk.toString().replace(/./gi, "");
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            // Ext.get('txtJumlah2EditData_viKasirrwi').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 3){
            tampunggrid = parseInt(dsTRDetailKasirrwiKasirList.data.items[i].data.PIUTANG);
            //TotalProduk.toString().replace(/./gi, "");
            //recordterakhir=tampunggrid
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            // Ext.get('txtJumlah2EditData_viKasirrwi').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 1){
            tampunggrid = parseInt(dsTRDetailKasirrwiKasirList.data.items[i].data.DISCOUNT);
            //	TotalProduk.toString().replace(/./gi, "");
            //recordterakhir=dsTRDetailKasirrwiKasirList.data.items[i].data.DISCOUNT
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            // Ext.get('txtJumlah2EditData_viKasirrwi').dom.value = formatCurrency(recordterakhir);
        }
    }
    // bayar = Ext.get('txtJumlah2EditData_viKasirrwi').getValue();
    return bayar;
}


function getArrDetailTrKasirrwi()
{
    var x = '';
    var totalBayar = tmp_sisa_bayar;
    var bayar = 0;
    for (var i = 0; i < dsTRDetailKasirrwiKasirList.getCount(); i++)
    {
		if (gridDTLTRKasirrwinap.getStore().getRange()[i].get('BAYARTR') != 0) {
	        if (dsTRDetailKasirrwiKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirrwiKasirList.data.items[i].data.DESKRIPSI != '')
	        {
	            var y = '';
				var z = '@@##$$@@';
				if (totalBayar>dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA * dsTRDetailKasirrwiKasirList.data.items[i].data.QTY) {
					totalBayar = totalBayar-dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA * dsTRDetailKasirrwiKasirList.data.items[i].data.QTY;
					if (totalBayar>0) {
						bayar = dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA * dsTRDetailKasirrwiKasirList.data.items[i].data.QTY;
						gridDTLTRKasirrwinap.getStore().getRange()[i].set('BAYARTR', gridDTLTRKasirrwinap.getStore().getRange()[i].get('BAYARTR') - bayar);
					}else{
						bayar = 0;
						gridDTLTRKasirrwinap.getStore().getRange()[i].set('BAYARTR', 0);
						//gridDTLTRKasirrwinap.getStore().getRange()[i].set('BAYARTR', 0);
					}

					//totalTagihanRWI = gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR') - totalTagihanRWI;
					
				}else if (totalBayar<=dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA * dsTRDetailKasirrwiKasirList.data.items[i].data.QTY) {
					//totalBayar = dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA-totalBayar;
	            	bayar 		= totalBayar;
					gridDTLTRKasirrwinap.getStore().getRange()[i].set('BAYARTR', gridDTLTRKasirrwinap.getStore().getRange()[i].get('BAYARTR') - bayar);
					totalBayar 	= 0;
					//gridDTLTRKasirrwinap.getStore().getRange()[i].set('BAYARTR',0);
					/*if (dsTRDetailKasirrwiKasirList.data.items[i].data.HARGA-totalBayar >= 0) {
					}else{
						bayar = 0;
					}*/
				}

					y   = dsTRDetailKasirrwiKasirList.data.items[i].data.URUT
					y   += z + dsTRDetailKasirrwiKasirList.data.items[i].data.KD_PRODUK
					y   += z + dsTRDetailKasirrwiKasirList.data.items[i].data.QTY
					//y   += z + bayar
					y   += z + dsTRDetailKasirrwiKasirList.data.items[i].data.PIUTANG
					y   += z + dsTRDetailKasirrwiKasirList.data.items[i].data.KD_TARIF
					y   += z + dsTRDetailKasirrwiKasirList.data.items[i].data.URUT
					//y += z + dsTRDetailKasirrwiKasirList.data.items[i].data.BAYARTR
					y   += z + bayar
					y   += z + bayar
					y   += z + bayar
					y   += z + dsTRDetailKasirrwiKasirList.data.items[i].data.TGL_TRANSAKSI
					y   += z + dsTRDetailKasirrwiKasirList.data.items[i].data.TGL_BERLAKU

		            if (i === (dsTRDetailKasirrwiKasirList.getCount() - 1))
		            {
		                x += y
		            } else
		            {
		                x += y + '##[[]]##'
		            };
				}
        };
    }

	return x;
}
function getItemPanelInputKasir(lebar){
    var items ={
		width: 545,
		labelAlign: 'right',
		bodyStyle: 'padding:10px 10px 10px 0px',
		border: true,
		// height: 110,
		items:[
			{
				columnWidth: .9,
				width: lebar - 35,
				labelWidth: 100,
				layout: 'form',
				border: false,
				items:[
					getItemPanelNoTransksirwiKasir(lebar), 
					getItemPanelmedreckasir(lebar), 
					getItemPanelUnitKasir(lebar)
				]
			}
		]
	};
    return items;
}
function getItemPanelInputKasirRWIpulang(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: true,
                height: 180,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getItemPanelNoTransksiRWIpulangPasien(lebar), getItemPanelKamar(lebar), getItemPaneltanggalPulang(lebar)
                                        ]
                            }
                        ]
            };
    return items;
}
;

function getItemPanelUnitKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboJenisByrView()

                                        ]
                            }, {
                                columnWidth: .58,
                                layout: 'form',
                                labelWidth: 0.9,
                                border: false,
                                items:
                                        [mComboPembayaran()

                                        ]
                            }
                        ]
            }
    return items;
}
;


function getItemPaneltanggalPulang(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .45,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal Masuk ',
                                                id: 'dtpTanggalMasuktransaksiPasienPulang',
                                                name: 'dtpTanggalMasuktransaksiPasienPulang',
                                                format: 'd/MM/Y',
                                                readOnly: true,
                                                value: now,
                                                anchor: '99%'
                                            }

                                        ]
                            }, {
                                columnWidth: .39,
                                layout: 'form',
                                labelWidth: 55,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Jam  ',
                                                id: 'dtpJamMasuktransaksiPasienPulang',
                                                name: 'dtpJamMasuktransaksiPasienPulang',
                                                format: 'h:m:s',
                                                readOnly: true,
                                                value: now,
                                                anchor: '60%'
                                            }
                                        ]
                            }, {
                                columnWidth: .50,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboAlasan_pulang()
                                        ]
                            }
                        ]
            }
    return items;
}
;

function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'nama',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboBayar',
                                    param: 'jenis_pay=~' + jenis_pay + '~'
                                }
                    }
            )
}

function mComboPembayaran()
{
    var Field = ['KD_PAY', 'JENIS_PAY', 'PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});


    var cboPembayaranKasirRWI = new Ext.form.ComboBox
            (
                    {
                        id: 'cboPembayaranKasirRWI',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        labelWidth: 80,
                        align: 'Right',
                        store: dsComboBayar,
                        valueField: 'KD_PAY',
                        displayField: 'PAYMENT',
                        anchor: '100%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        tapungkd_pay = b.data.KD_PAY;
                                        //getTotalDetailProdukRWI();


                                    },
                                }
                    }
            );

    return cboPembayaranKasirRWI;
}
;


function mComboJenisByrView()
{
    var Field = ['JENIS_PAY', 'DESKRIPSI', 'TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({fields: Field});
    dsJenisbyrView.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'jenis_pay',
                                    Sortdir: 'ASC',
                                    target: 'ComboJenis',
                                }
                    }
            );

    var cboJenisByr = new Ext.form.ComboBox
            (
                    {
                        id: 'cboJenisByr',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Pembayaran      ',
                        align: 'Right',
                        anchor: '100%',
                        store: dsJenisbyrView,
                        valueField: 'JENIS_PAY',
                        displayField: 'DESKRIPSI',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        loaddatastorePembayaran(b.data.JENIS_PAY);
                                        tampungtypedata = b.data.TYPE_DATA;
                                        jenispay = b.data.JENIS_PAY;
                                        showCols(Ext.getCmp('gridDTLTRKasirrwinap'));
                                        hideCols(Ext.getCmp('gridDTLTRKasirrwinap'));
                                        getTotalDetailProdukRWI();
                                        Ext.get('cboPembayaranKasirRWI').dom.value = 'Pilih Pembayaran...';



                                    },
                                }
                    }
            );

    return cboJenisByr;
}
;
function hideCols(grid)
{
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);

    }
}
;
function showCols(grid) {
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);

    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
    }

}
;



function getItemPanelDokter(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Dokter  ',
                                                name: 'txtKdDokter',
                                                id: 'txtKdDokter',
                                                //emptyText:nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%'
                                            }, {
                                                xtype: 'textfield',
                                                fieldLabel: 'Kelompok Pasien',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtCustomer',
                                                id: 'txtCustomer',
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .600,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                // fieldLabel:'Unit : ',
                                                name: 'txtNamaDokter',
                                                id: 'txtNamaDokter',
                                                //emptyText:nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                // fieldLabel:'Unit : ',
                                                name: 'txtKdUrutMasuk',
                                                id: 'txtKdUrutMasuk',
                                                //emptyText:nmNomorOtomatis,
                                                readOnly: true,
                                                hidden: true,
                                                anchor: '100%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getItemPanelHakKelasrwiKasir(lebar){
    var items = {
		layout: 'column',
		border: false,
		items:[
			{
				columnWidth: .40,
				layout: 'form',
				labelWidth: 100,
				border: false,
				items:[
					{
						xtype: 'textfield',
						fieldLabel: 'Hak Kelas',
						name: 'txtHakKelasKasirrwiKasir',
						id: 'txtHakKelasKasirrwiKasir',
                        hidden: true,
						readOnly: true,
						value:rowSelectedKasirrwiKasir.data.HAK_KELAS,
						anchor: '99%'
					}
				]
			},{
				columnWidth: .40,
				layout: 'form',
				border: false,
				labelWidth: 100,
				items:[
					{
						xtype: 'textfield',
						fieldLabel: 'No. Sep',
						name: 'txtNoSepKasirrwiKasir',
						id: 'txtNoSepKasirrwiKasir',
						value:rowSelectedKasirrwiKasir.data.NO_SJP,
						anchor: '99%',
						listeners:{
							'specialkey': function (a){
								var tmpNoMedrec = Ext.get('txtFilterNomedrec_rwi').getValue()
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
									getInacbgPenjasRwi(a.getValue());
								}
							}
						}
					}
				]
			}
		]
	};
    return items;
}
function getItemPanelNoTransksirwiKasir(lebar){
    var items = {
		layout: 'column',
		border: false,
		items:[
			{
				columnWidth: .40,
				layout: 'form',
				labelWidth: 100,
				border: false,
				items:[
					{
						xtype: 'textfield',
						fieldLabel: 'No. Transaksi ',
						name: 'txtNoTransaksiKasirrwiKasir',
						id: 'txtNoTransaksiKasirrwiKasir',
						emptyText: nmNomorOtomatis,
						readOnly: true,
						anchor: '99%'
					}
				]
			},{
				columnWidth: .58,
				layout: 'form',
				border: false,
				labelWidth: 55,
				items:[
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal ',
						id: 'dtpTanggalDetransaksi',
						name: 'dtpTanggalDetransaksi',
						format: 'd/M/Y',
						readOnly: true,
						value: now,
						anchor: '100%'
					}
				]
			}
		]
	};
    return items;
}
function getItemPanelNoTransksiRWIpulangPasien(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .45,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Pasien ',
                                                name: 'txtkdPasienPasienPulang',
                                                id: 'txtkdPasienPasienPulang',
                                                //emptyText:nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%'
                                            }
                                        ]
                            },
                            {
                                columnWidth: .55,
                                layout: 'form',
                                border: false,
                                labelWidth: 1,
                                items:
                                        [
                                            /*{
                                             xtype: 'datefield',
                                             fieldLabel: 'Tanggal ',
                                             id: 'dtpTanggalDetransaksi',
                                             name: 'dtpTanggalDetransaksi',
                                             format: 'd/M/Y',
                                             readOnly : true,
                                             value: now,
                                             anchor: '100%'
                                             }*/
                                            {
                                                xtype: 'textfield',
                                                name: 'txtPasienPasienPulang',
                                                id: 'txtPasienPasienPulang',
                                                //emptyText:nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getItemPanelKamar(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .45,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Kelas ',
                                                name: 'txtKelasPasienPulang',
                                                id: 'txtKelasPasienPulang',
                                                readOnly: true,
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .55,
                                layout: 'form',
                                border: false,
                                labelWidth: 55,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Kamar',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtkamarPasienPulang',
                                                id: 'txtkamarPasienPulang',
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getItemPanelmedreckasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Medrec ',
                                                name: 'txtNoMedrecDetransaksi',
                                                id: 'txtNoMedrecDetransaksi',
                                                readOnly: true,
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .58,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: '',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtNamaPasienDetransaksi',
                                                id: 'txtNamaPasienDetransaksi',
                                                anchor: '100%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function RefreshDataKasirrwiKasir()
{
	loadMask.show();
	var tgl_pertama = Ext.getCmp('dtpTglAwalFilterKasirrwi').getValue();
	var tgl_kedua 	= Ext.getCmp('dtpTglAkhirFilterKasirrwi').getValue();

    var KataKunci = " and (t.tgl_transaksi between ~" + tgl_pertama.format("d/M/Y") + "~ and ~" + tgl_kedua.format("d/M/Y") + "~)";
    KataKunci += ' and (t.lunas = ~0~ or  t.lunas = ~1~)';
    dsTRKasirrwiKasirList.load
            (
                    {
                        params: {
								Skip: Ext.getCmp('mComboViewDataKasirRWI').getValue(),
								Take: selectCountKasirrwiKasir,
								Sort: 'tgl_transaksi',
								//Sort: 'tgl_transaksi',
								Sortdir: 'DESC',
								target: 'ViewKasirRWI',
								param: KataKunci
							},	
							callback:function(){
								loadMask.hide();
							}
                    }
            );

    return dsTRKasirrwiKasirList;
};

function refeshKasirrwiKasir()
{
	loadMask.show();
    dsTRKasirrwiKasirList.load
            (
                    {
                        params:{
									Skip: Ext.getCmp('mComboViewDataKasirRWI').getValue(),
									Take: selectCountKasirrwiKasir,
									//Sort: 'no_transaksi',
									Sort: '',
									//Sort: 'no_transaksi',
									Sortdir: 'ASC',
									target: 'ViewKasirRWI',
									param: ''
						},	
						callback:function(){
							loadMask.hide();
						}
                    }
            );
    return dsTRKasirrwiKasirList;
}

/*
    UPDATE REFRESH DATA 
    OLEH    : HADAD AL GOJALI
    TANGGAL : 2017 - 01 - 07
    DATA TIDAK MERELOAD PADA SAAT MENGGANTI STATUS LUNAS
 */
 //-------------------------------------------------------
 /*
    UPDATE REFRESH DATA 
    OLEH    : HDHT
    TANGGAL : 2017 - 03 - 09
    eror ketika pencarian no medrec pasien karena kode_pasien tidak ada di field di ganti oleh p.kd_pasien
 */
function RefreshDataFilterKasirrwiKasir()
{

    var KataKunci = '';
    loadMask.show();
    if (Ext.get('txtFilterNomedrec_rwi').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = ' and   LOWER(p.kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec_rwi').getValue() + '%~)';

        } else
        {

            KataKunci += ' and  LOWER(p.kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec_rwi').getValue() + '%~)';
        }        ;

    };

    if (Ext.get('TxtFilterGridDataView_NAMA_viKasirrwiKasir').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = ' and   LOWER(p.nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirrwiKasir').getValue() + '%~)';

        } else
        {

            KataKunci += ' and  LOWER(p.nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirrwiKasir').getValue() + '%~)';
        };
    };


    /* if (Ext.get('cboUNIT_viKasirrwiKasir').getValue() != '' && Ext.get('cboUNIT_viKasirrwiKasir').getValue() != 'All')
    {
        if (KataKunci == '')
        {

            KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirrwiKasir').getValue() + '%~)';
        } else
        {

            KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirrwiKasir').getValue() + '%~)';
        }
        ;
    }
    ; */

   if (Ext.get('cboStatuslunas_viKasirrwiKasir').getValue() == 'Lunas'){
        if (KataKunci == ''){
            KataKunci = ' and  t.lunas = ~1~';
        }else{
            KataKunci += ' and t.lunas =  ~1~';
        };
    };


    if (Ext.get('cboStatuslunas_viKasirrwiKasir').getValue() == 'Semua')
    {
        if (KataKunci == ''){
            KataKunci = ' and (t.lunas = ~0~ or  t.lunas = ~1~)';
        }else{
            KataKunci += ' and (t.lunas = ~0~ or  t.lunas = ~1~)';
        };
    };

    if (Ext.get('cboStatuslunas_viKasirrwiKasir').getValue() == 'Belum Lunas'){
        if (KataKunci == ''){
            KataKunci = ' and  t.lunas = ~0~';
        }else{
            KataKunci += ' and t.lunas =  ~0~';
        };
    };

    if (Ext.getCmp('dtp_HakKelas_FilterKasirrwi').getValue() != '' && Ext.getCmp('dtp_HakKelas_FilterKasirrwi').getValue() != 0)
    {
    	var criteria_hak_kelas = "";
    	var persamaan = "";
    	if (Ext.getCmp('dtp_HakKelas_FilterKasirrwi').getValue() == 1) {
    		criteria_hak_kelas = "is null";
    		persamaan = " OR K.hak_kelas = u.kelas ";
    	}else{
    		criteria_hak_kelas = "is not null";
    		persamaan = " AND K.hak_kelas <> u.kelas ";
    	}

        if (KataKunci == '')
        {
            KataKunci = " and (k.hak_kelas "+criteria_hak_kelas+persamaan+")";
        } else
        {

            KataKunci += " and (k.hak_kelas "+criteria_hak_kelas+persamaan+")";
        };

    };

    if (KataKunci == '')
    {
        KataKunci = " and (t.tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirrwi').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirrwi').getValue() + "~)";
    } else
    {

        KataKunci += " and (t.tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirrwi').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirrwi').getValue() + "~)";
    }
    ;

   /* if (Ext.get('dtp_HakKelas_FilterKasirrwi').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = " and (t.tgl_transaksi1 between ~" + Ext.get('dtpTglAwalFilterKasirrwi').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirrwi').getValue() + "~)";
        } else
        {

            KataKunci += " and (t.tgl_transaksi1 between ~" + Ext.get('dtpTglAwalFilterKasirrwi').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirrwi').getValue() + "~)";
        }
        ;

    };*/

    if (KataKunci != undefined || KataKunci != '')
    {
        dsTRKasirrwiKasirList.load
                (
                        {
                            params:
                                    {
										Skip: Ext.getCmp('mComboViewDataKasirRWI').getValue(),
                                        Take: selectCountKasirrwiKasir,
                                        //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                                        //Sort: 'no_transaksi',
                                        Sortdir: 'DESC',
                                        target: 'ViewKasirRWI',
                                        param: KataKunci
                                    },
                            callback: function(){
                            	// rowSelectedKasirrwiKasir
                            	if (Ext.get('txtFilterNomedrec_rwi').getValue() != ''){
									if (dsTRKasirrwiKasirList.totalLength > 0) {
										rowSelectedKasirrwiKasir   = dsTRKasirrwiKasirList.data.items[0];
										rowpasien                  = rowSelectedKasirrwiKasir.data;
										cellSelectedtutup_kasirrwi = rowSelectedKasirrwiKasir.data.NO_TRANSAKSI;
										kodepasien                 = rowpasien.KD_PASIEN;
										namapasien                 = rowpasien.NAMA;
										tmpKelas                   = rowpasien.KELAS;
										tmpKamar                   = rowpasien.NAMA_KAMAR;
										tmpUnit                    = rowpasien.KD_UNIT;
										tmpNoKamar                 = rowpasien.NO_KAMAR;
										tmpKdSpesial               = rowpasien.KD_SPESIAL;
										Ext.getCmp('btnEditKasirrwi').enable();
										if (rowSelectedKasirrwiKasir.data.LUNAS == '1') {
											Ext.getCmp('btnEditKasirrwi').disable();
										}
										if (rowSelectedKasirrwiKasir.data.LUNAS == '1' && rowSelectedKasirrwiKasir.data.CO_STATUS == '1'){
											Ext.getCmp('btnLookUpProduk_RawatInap').disable();
											Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
											Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').disable();
											Ext.getCmp('btnDepositKasirrwi').disable();
											Ext.getCmp('btnDiscountKasirrwi').disable();
											Ext.getCmp('btnTutupTransaksiKasirrwi').disable();
											Ext.getCmp('btnHpsBrsKasirrwi').disable();
										} else if (rowSelectedKasirrwiKasir.data.LUNAS == '0' && rowSelectedKasirrwiKasir.data.CO_STATUS == '0'){
											Ext.getCmp('btnTutupTransaksiKasirrwi').disable();
											Ext.getCmp('btnHpsBrsKasirrwi').enable();
											Ext.getCmp('btnLookUpProduk_RawatInap').enable();
											Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').enable();
											Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').enable();
											Ext.getCmp('btnDepositKasirrwi').enable();
											 Ext.getCmp('btnDiscountKasirrwi').enable();
										} else if (rowSelectedKasirrwiKasir.data.LUNAS == '1' && rowSelectedKasirrwiKasir.data.CO_STATUS == '0'){
											Ext.getCmp('btnLookUpProduk_RawatInap').disable();
											Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
											Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').disable();
											Ext.getCmp('btnDepositKasirrwi').disable();
											Ext.getCmp('btnDiscountKasirrwi').disable();
											Ext.getCmp('btnTutupTransaksiKasirrwi').enable();
											Ext.getCmp('btnHpsBrsKasirrwi').enable();
										}else if (rowSelectedKasirrwiKasir.data.LUNAS == '0' && rowSelectedKasirrwiKasir.data.CO_STATUS == '1'){
											Ext.getCmp('btnHpsBrsKasirrwi').enable();
										}
										kodekasir = 1;
										tmpkd_unitKamar 	 = rowSelectedKasirrwiKasir.data.KD_UNIT_KAMAR;
										kdkasirnya           = rowSelectedKasirrwiKasir.data.KD_KASIR;
										notransaksi          = rowSelectedKasirrwiKasir.data.NO_TRANSAKSI;
										tmpno_transaksi      = rowSelectedKasirrwiKasir.data.NO_TRANSAKSI;
										tgltrans             = rowSelectedKasirrwiKasir.data.TANGGAL_TRANSAKSI;
										kodepasien           = rowSelectedKasirrwiKasir.data.KD_PASIEN;
										urutmasuk            = rowSelectedKasirrwiKasir.data.URUT_MASUK;
										namapasien           = rowSelectedKasirrwiKasir.data.NAMA;
										kodeunit             = rowSelectedKasirrwiKasir.data.KD_UNIT;
										namaunit             = rowSelectedKasirrwiKasir.data.NAMA_UNIT;
										kdcustomeraigd       = rowSelectedKasirrwiKasir.data.KD_CUSTOMER;
										vkode_customer_PJRWI = rowSelectedKasirrwiKasir.data.KD_CUSTOMER;
										vnama_customer_PJRWI = rowSelectedKasirrwiKasir.data.CUSTOMER;
										kdokter_btl          = rowSelectedKasirrwiKasir.data.KD_DOKTER;
										kodepay              = rowSelectedKasirrwiKasir.data.KD_PAY;
										tmpNamaDokter        = rowSelectedKasirrwiKasir.data.NAMA_DOKTER;
										uraianpay            = rowSelectedKasirrwiKasir.data.CARA_BAYAR;
										tmpTanggalMasuk      = rowSelectedKasirrwiKasir.data.TANGGAL_MASUK;
										tmpKdUnitKamar       = rowSelectedKasirrwiKasir.data.KD_UNIT_KAMAR;
										tmpKeterangan        = rowSelectedKasirrwiKasir.data.KETERANGAN;
										Ext.Ajax.request({
											url: baseURL + "index.php/main/functionRWI/cek_cominal_pembayaran",
											params: {
												kd_kasir 	: '02',
												no_transaksi: rowSelectedKasirrwiKasir.data.NO_TRANSAKSI,
											},
											failure: function (o){},
											success: function (o) {
												var cst = Ext.decode(o.responseText);
												var sisaPembayaran = cst.harus_membayar - cst.telah_membayar;
												Ext.getCmp('txtJumlahBayar_KASIRRWI').setValue(cst.harus_membayar);
											}
										});

										RefreshDataDetail_kasirrwi(notransaksi);
										RefreshDatahistoribayar(rowSelectedKasirrwiKasir.data.NO_TRANSAKSI);
									}
								}
                                loadMask.hide();
                            }
                        }
                );
    } else
    {

        dsTRKasirrwiKasirList.load
                (
                        {
                            params:
                                    {
										Skip: Ext.getCmp('mComboViewDataKasirRWI').getValue(),
                                        Take: selectCountKasirrwiKasir,
                                        //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                                        //Sort: 'no_transaksi',
                                        Sortdir: 'DESC',
                                        target: 'ViewKasirRWI',
                                        param: KataKunci
                                    },
                            callback: function(){
                            	// rowSelectedKasirrwiKasir
                            	
                            	if (Ext.get('txtFilterNomedrec_rwi').getValue() != ''){
									if (dsTRKasirrwiKasirList.totalLength > 0) {
										rowSelectedKasirrwiKasir   = dsTRKasirrwiKasirList.data.items[0];
										rowpasien                  = rowSelectedKasirrwiKasir.data;
										cellSelectedtutup_kasirrwi = rowSelectedKasirrwiKasir.data.NO_TRANSAKSI;
										kodepasien                 = rowpasien.KD_PASIEN;
										namapasien                 = rowpasien.NAMA;
										tmpKelas                   = rowpasien.KELAS;
										tmpKamar                   = rowpasien.NAMA_KAMAR;
										tmpUnit                    = rowpasien.KD_UNIT;
										tmpNoKamar                 = rowpasien.NO_KAMAR;
										tmpKdSpesial               = rowpasien.KD_SPESIAL;
										Ext.getCmp('btnEditKasirrwi').enable();
										if (rowSelectedKasirrwiKasir.data.LUNAS == '1') {
											Ext.getCmp('btnEditKasirrwi').disable();
										}
										if (rowSelectedKasirrwiKasir.data.LUNAS == '1' && rowSelectedKasirrwiKasir.data.CO_STATUS == '1'){
											Ext.getCmp('btnLookUpProduk_RawatInap').disable();
											Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
											Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').disable();
											Ext.getCmp('btnDepositKasirrwi').disable();
											Ext.getCmp('btnDiscountKasirrwi').disable();
											Ext.getCmp('btnTutupTransaksiKasirrwi').disable();
											Ext.getCmp('btnHpsBrsKasirrwi').disable();
										} else if (rowSelectedKasirrwiKasir.data.LUNAS == '0' && rowSelectedKasirrwiKasir.data.CO_STATUS == '0'){
											Ext.getCmp('btnTutupTransaksiKasirrwi').disable();
											Ext.getCmp('btnHpsBrsKasirrwi').enable();
											Ext.getCmp('btnLookUpProduk_RawatInap').enable();
											Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').enable();
											Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').enable();
											Ext.getCmp('btnDepositKasirrwi').enable();
											 Ext.getCmp('btnDiscountKasirrwi').enable();
										} else if (rowSelectedKasirrwiKasir.data.LUNAS == '1' && rowSelectedKasirrwiKasir.data.CO_STATUS == '0'){
											Ext.getCmp('btnLookUpProduk_RawatInap').disable();
											Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
											Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').disable();
											Ext.getCmp('btnDepositKasirrwi').disable();
											Ext.getCmp('btnDiscountKasirrwi').disable();
											Ext.getCmp('btnTutupTransaksiKasirrwi').enable();
											Ext.getCmp('btnHpsBrsKasirrwi').enable();
										}
										kodekasir = 1;
										tmpkd_unitKamar 	 = rowSelectedKasirrwiKasir.data.KD_UNIT_KAMAR;
										kdkasirnya           = rowSelectedKasirrwiKasir.data.KD_KASIR;
										notransaksi          = rowSelectedKasirrwiKasir.data.NO_TRANSAKSI;
										tmpno_transaksi      = rowSelectedKasirrwiKasir.data.NO_TRANSAKSI;
										tgltrans             = rowSelectedKasirrwiKasir.data.TANGGAL_TRANSAKSI;
										kodepasien           = rowSelectedKasirrwiKasir.data.KD_PASIEN;
										urutmasuk            = rowSelectedKasirrwiKasir.data.URUT_MASUK;
										namapasien           = rowSelectedKasirrwiKasir.data.NAMA;
										kodeunit             = rowSelectedKasirrwiKasir.data.KD_UNIT;
										namaunit             = rowSelectedKasirrwiKasir.data.NAMA_UNIT;
										kdcustomeraigd       = rowSelectedKasirrwiKasir.data.KD_CUSTOMER;
										vkode_customer_PJRWI = rowSelectedKasirrwiKasir.data.KD_CUSTOMER;
										vnama_customer_PJRWI = rowSelectedKasirrwiKasir.data.CUSTOMER;
										kdokter_btl          = rowSelectedKasirrwiKasir.data.KD_DOKTER;
										kodepay              = rowSelectedKasirrwiKasir.data.KD_PAY;
										tmpNamaDokter        = rowSelectedKasirrwiKasir.data.NAMA_DOKTER;
										uraianpay            = rowSelectedKasirrwiKasir.data.CARA_BAYAR;
										tmpTanggalMasuk      = rowSelectedKasirrwiKasir.data.TANGGAL_MASUK;
										tmpKdUnitKamar       = rowSelectedKasirrwiKasir.data.KD_UNIT_KAMAR;
										tmpKeterangan        = rowSelectedKasirrwiKasir.data.KETERANGAN;
										Ext.Ajax.request({
											url: baseURL + "index.php/main/functionRWI/cek_cominal_pembayaran",
											params: {
												kd_kasir 	: '02',
												no_transaksi: rowSelectedKasirrwiKasir.data.NO_TRANSAKSI,
											},
											failure: function (o){},
											success: function (o) {
												var cst = Ext.decode(o.responseText);
												var sisaPembayaran = cst.harus_membayar - cst.telah_membayar;
												Ext.getCmp('txtJumlahBayar_KASIRRWI').setValue(cst.harus_membayar);
											}
										});

										RefreshDataDetail_kasirrwi(notransaksi);
										RefreshDatahistoribayar(rowSelectedKasirrwiKasir.data.NO_TRANSAKSI);
									}
								}
                                loadMask.hide();
                            }
                        }
                );
    }
    ;

    return dsTRKasirrwiKasirList;
};



function Datasave_KasirrwiKasir(mBol)
{
	loadMask.show();
    if (ValidasiEntryCMKasirrwi(nmHeaderSimpanData, false) == 1)
    {
		var urinya=baseURL + "index.php/main/Controller_pembayaran/simpan_pembayaran_revisi";
		if(Ext.getCmp('txtinacbg3penjasrwj').getValue()>0 || Ext.getCmp('txtinacbg2penjasrwj').getValue()>0){
			urinya=baseURL + "index.php/main/Controller_pembayaran/simpan_pembayaran_revisi_bpjs";
		}
        Ext.Ajax.request
		({
			//url: "./Datapool.mvc/CreateDataObj",
			//url: baseURL + "index.php/main/functionRWI/savepembayaran",
			// url: baseURL + "index.php/main/Controller_pembayaran/simpan_pembayaran",
			
			
			url: urinya,
			params: getParamDetailTransaksiKasirrwi(), //update maya
			failure: function (o)
			{
				ShowPesanWarningKasirrwi('Error simpan pembayaran, Hubungi Admin!', 'Gagal');
				RefreshDataFilterKasirrwiKasir();
			},
			success: function (o)
			{
				//   refeshKasirrwiKasir();
				var cst = Ext.decode(o.responseText);
				if (cst.status === true)
				{
					//RefreshDataKasirrwiKasirDetail(Ext.get('txtNoTransaksiKasirrwiKasir').dom.value);
					ShowPesanInfoKasirrwi('Pembayaran berhasil disimpan', 'Information');
					getTotalBayarSummaryKasirRWI();
					
					// Datasave_KasirrwiKasir_SQL();
					//RefreshDataKasirrwiKasir();
					if (mBol === false)
					{
						RefreshDataFilterKasirrwiKasir();
						RefreshDatahistoribayar(notransaksi);
					};
				} else
				{
					ShowPesanWarningKasirrwi('Data Belum Simpan segera Hubungi Admin', 'Gagal');
				};
				RefreshDataKasirrwiKasirDetail(notransaksi);
				loadMask.hide();
			}
		})

    } else
    {
        if (mBol === true)
        {
            return false;
        };
    }
    ;

};

// ---------------------------------------------Proses Bayar Selisih-----------------------------------------------------
function ValidasiEntryCMKasirrwiSelisih(modul, mBolHapus)
{
    var x = 1;

    if ((Ext.get('txtNoTransaksiKasirrwiKasir').getValue() == '')

            || (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
            || (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
            || (Ext.get('dtpTanggalDetransaksi').getValue() == '')
            || dsSelisihDetailKasirrwiKasirList.getCount() === 0
            || (Ext.get('cboPembayaranKasirRWI').getValue() == '')
            || (Ext.get('cboPembayaranKasirRWI').getValue() == 'Pilih Pembayaran...'))
    {
        if (Ext.get('txtNoTransaksiKasirrwiKasir').getValue() == '' && mBolHapus === true)
        {
            x = 0;
        } else if (Ext.get('cboPembayaranKasirRWI').getValue() == '' || Ext.get('cboPembayaranKasirRWI').getValue() == 'Pilih Pembayaran...')
        {
            ShowPesanWarningKasirrwi(('Data pembayaran tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirrwi(('Data no. medrec tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirrwi('Nama pasien belum terisi', modul);
            x = 0;
        } else if (Ext.get('dtpTanggalDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirrwi(('Data tanggal kunjungan tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('cboKelasKasirRWISelisih').getValue() == '')
        {
            ShowPesanWarningKasirrwi(('Jatah Kelas Perawatan tidak boleh kosong'), modul);
            x = 0;
        }else if (Ext.get('cboRuanganSelisih').getValue() == '' || Ext.get('cboRuanganSelisih').getValue() == 'Pilih Ruangan...')
        {
            ShowPesanWarningKasirrwi(('Ruang / Unit tidak  boleh kosong'), modul);
            x = 0;
        } 
        //cboPembayaranKasirRWI

        else if (dsSelisihDetailKasirrwiKasirList.getCount() === 0)
        {
            ShowPesanWarningKasirrwi('Data dalam tabel kosong', modul);
            x = 0;
        }
        ;
    }
    ;
    return x;
};

function Datasave_KasirrwiKasirSelisih(mBol)
{
	loadMask.show();
    if (ValidasiEntryCMKasirrwiSelisih(nmHeaderSimpanData, false) == 1)
    {
		var urinya=baseURL + "index.php/main/Controller_pembayaran/simpan_pembayaran_selisih";
        Ext.Ajax.request
		({	
			url: urinya,
			params: getParamDetailTransaksiKasirrwiSelisih(), //update maya
			failure: function (o)
			{
				ShowPesanWarningKasirrwi('Error simpan pembayaran, Hubungi Admin!', 'Gagal');
				RefreshDataFilterKasirrwiKasir();
			},
			success: function (o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.status === true)
				{
					ShowPesanInfoKasirrwi('Pembayaran berhasil disimpan', 'Information');
					getTotalBayarSummaryKasirRWI();
					if (mBol === false)
					{
						RefreshDataFilterKasirrwiKasir();
						RefreshDatahistoribayar(notransaksi);
					};
                    FormLookUpsSelisihKasirrwi.close();
				} else
				{
					ShowPesanWarningKasirrwi('Data Belum Simpan segera Hubungi Admin', 'Gagal');
				};
				RefreshDataKasirrwiKasirDetail(notransaksi);
				loadMask.hide();
			}
		})

    } else
    {
        if (mBol === true)
        {
            return false;
        };
    }
    ;

};

function getParamDetailTransaksiKasirrwiSelisih()
{
    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    }
    ;
    var params = {
    	no_transaksi    :Ext.get('txtNoTransaksiKasirrwiKasir').getValue(),
        kd_kasir        :'02',
        payment         :tapungkd_pay,
        dijamin         :Ext.getCmp('txtinacbg2penjasrwj').getValue(),
        bayar	        :Ext.getCmp('txtinacbg3penjasrwj').getValue(),
        total_selisih   :Ext.getCmp('txtTotalSelisihKasirRWI').getValue(),
        total_jatah     :Ext.getCmp('txtTotalJumlahSelisihKasirRWI').getValue(),
        tgl_pembayaran  :Ext.getCmp('txtTanggalTransaksiKASIRRWI').getValue(),
        sisa_bayar      :Ext.getCmp('txtTotalBiayaSelisihKasirRWI').getValue(),
        data            :getArrDetailTrKasirrwiSelisih(),
        jumlah          :GetListCountDetailTransaksiSelisih(),
        catatan         :Ext.getCmp('lblinacbhiting').getValue(),
        diagnosa        :Ext.getCmp('lblinacbdiagnosa').getValue(),
        object          :'kasir',
        modul           :'Rawat Inap',
		kd_unit			:tmpKdUnitKamar  //update maya
    };
    return params;
}
;

function GetListCountDetailTransaksiSelisih()
{

    var x = 0;
    for (var i = 0; i < dsSelisihDetailKasirrwiKasirList.getCount(); i++)
    {
        if (dsSelisihDetailKasirrwiKasirList.data.items[i].data.KD_PRODUK != '' || dsSelisihDetailKasirrwiKasirList.data.items[i].data.DESKRIPSI != '')
        {
            x += 1;
        }
        ;
    }
    return x;

}
;

function getArrDetailTrKasirrwiSelisih()
{
    var x = '';
    var totalBayar = tmp_sisa_bayar;
    var bayar = 0;
    for (var i = 0; i < dsSelisihDetailKasirrwiKasirList.getCount(); i++)
    {
        console.log(gridDTLTRKasirrwinap.getStore().getRange()[i].get('BAYARTR'));
		if (gridDTLTRKasirrwinap.getStore().getRange()[i].get('BAYARTR') != 0) {
            console.log(dsSelisihDetailKasirrwiKasirList.data.items[i]);
	        if (dsSelisihDetailKasirrwiKasirList.data.items[i].data.KD_PRODUK != '' && dsSelisihDetailKasirrwiKasirList.data.items[i].data.DESKRIPSI != '')
	        {
	            var y = '';
				var z = '@@##$$@@';
				// if (totalBayar>dsSelisihDetailKasirrwiKasirList.data.items[i].data.HARGA * dsSelisihDetailKasirrwiKasirList.data.items[i].data.QTY) {
				// 	totalBayar = totalBayar-dsSelisihDetailKasirrwiKasirList.data.items[i].data.HARGA * dsSelisihDetailKasirrwiKasirList.data.items[i].data.QTY;
				// 	if (totalBayar>0) {
				// 		bayar = dsSelisihDetailKasirrwiKasirList.data.items[i].data.HARGA * dsSelisihDetailKasirrwiKasirList.data.items[i].data.QTY;
				// 		gridDTLTRKasirrwinap.getStore().getRange()[i].set('BAYARTR', gridDTLTRKasirrwinap.getStore().getRange()[i].get('BAYARTR') - bayar);
				// 	}else{
				// 		bayar = 0;
				// 		gridDTLTRKasirrwinap.getStore().getRange()[i].set('BAYARTR', 0);
				// 	}
					
				// }else if (totalBayar<=dsSelisihDetailKasirrwiKasirList.data.items[i].data.HARGA * dsSelisihDetailKasirrwiKasirList.data.items[i].data.QTY) {
	            // 	bayar 		= totalBayar;
				// 	gridDTLTRKasirrwinap.getStore().getRange()[i].set('BAYARTR', gridDTLTRKasirrwinap.getStore().getRange()[i].get('BAYARTR') - bayar);
				// 	totalBayar 	= 0;
				// }

					y   = dsSelisihDetailKasirrwiKasirList.data.items[i].data.URUT
					y   += z + dsSelisihDetailKasirrwiKasirList.data.items[i].data.KD_PRODUK
					y   += z + dsSelisihDetailKasirrwiKasirList.data.items[i].data.QTY
					y   += z + dsSelisihDetailKasirrwiKasirList.data.items[i].data.HARGA
                    y   += z + dsSelisihDetailKasirrwiKasirList.data.items[i].data.JATAH
                    y   += z + dsSelisihDetailKasirrwiKasirList.data.items[i].data.SELISIH
					y   += z + dsSelisihDetailKasirrwiKasirList.data.items[i].data.KD_TARIF
					// y   += z + bayar
					// y   += z + bayar
					// y   += z + bayar
					y   += z + dsSelisihDetailKasirrwiKasirList.data.items[i].data.TGL_TRANSAKSI
					y   += z + dsSelisihDetailKasirrwiKasirList.data.items[i].data.TGL_BERLAKU

		            if (i === (dsSelisihDetailKasirrwiKasirList.getCount() - 1))
		            {
		                x += y
		            } else
		            {
		                x += y + '##[[]]##'
		            };
			}
        };
    }

	return x;
}

//------------------------------------------------------------------------------------------------------

function UpdateTutuptransaksi(mBol)
{

    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionRWI/ubah_co_status_transksi",
                        params: paramUpdateTransaksi(),
                        failure: function (o)
                        {
                            ShowPesanWarningKasirrwi('Data tidak tersimpan segera hubungi Admin', 'Gagal');
                            RefreshDataFilterKasirrwiKasir();
                        },
                        success: function (o)
                        {
                            //RefreshDatahistoribayar(Ext.get('cellSelectedtutup_kasirrwi);

                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirrwi(nmPesanSimpanSukses, nmHeaderSimpanData);
								UpdateTutuptransaksi_SQL();
                                //RefreshDataKasirrwiKasir();
                                if (mBol === false)
                                {
                                    RefreshDataFilterKasirrwiKasir();
                                }
                                ;
                                cellSelectedtutup_kasirrwi = '';
                            } else
                            {
                                ShowPesanWarningKasirrwi('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                                cellSelectedtutup_kasirrwi = '';
                            }
                            ;
                        }
                    }
            )



};

function printbillRwi()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakbill_vikasirDaftarRWI(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningKasirrwi('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            } else
                            {
                                ShowPesanErrorKasirrwi('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            }
                        }
                    }
            );
};

tmpkdkasirrwi = '05';
function dataparamcetakbill_vikasirDaftarRWI()
{
    var paramscetakbill_vikasirDaftarRWI =
            {
                Table: 'DirectPrintingrwi',
                No_TRans: Ext.get('txtNoTransaksiKasirrwiKasir').getValue(),
                KdKasir: tmpkdkasirrwi,
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirrwi').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirrwi').getValue()
            };
    return paramscetakbill_vikasirDaftarRWI;
}
;

function printkwitansirwi()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakkwitansi_vikasirDaftarRWI(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan !== '')
                            {
                                ShowPesanWarningKasirrwi('Tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanErrorKasirrwi('Tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            );
}
;
function printbillRwiPreview()
{
	var params=dataparamcetakbillpreview_vikasirDaftarRWI();
    var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	form.setAttribute("action", baseURL + "index.php/rawat_inap/viewprintbillrwipreview/cetakBill");
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();            
}
function dataparamcetakbillpreview_vikasirDaftarRWI()
{
    var paramscetakbill_vikasirDaftarRWI =
            {
                No_TRans: Ext.get('txtNoTransaksiKasirrwiKasir').getValue(),
                KdKasir: tmpkdkasirrwi,
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirrwi').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirrwi').getValue()
            };
    return paramscetakbill_vikasirDaftarRWI;
}
;
function dataparamcetakkwitansi_vikasirDaftarRWI()
{
    var paramscetakbill_vikasirDaftarRWI =
            {
                Table: 'DirectKwitansirwi',
                No_TRans: Ext.get('txtNoTransaksiKasirrwiKasir').getValue(),
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirrwi').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirrwi').getValue(),
				printer: ''//Ext.getCmp('cbopasienorder_printer_kasirrwi').getValue()
            };
    return paramscetakbill_vikasirDaftarRWI;
};



function ValidasiEntryCMKasirrwi(modul, mBolHapus)
{
    var x = 1;

    if ((Ext.get('txtNoTransaksiKasirrwiKasir').getValue() == '')

            || (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
            || (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
            || (Ext.get('dtpTanggalDetransaksi').getValue() == '')
            || dsTRDetailKasirrwiKasirList.getCount() === 0
            || (Ext.get('cboPembayaranKasirRWI').getValue() == '')
            || (Ext.get('cboPembayaranKasirRWI').getValue() == 'Pilih Pembayaran...'))
    {
        if (Ext.get('txtNoTransaksiKasirrwiKasir').getValue() == '' && mBolHapus === true)
        {
            x = 0;
        } else if (Ext.get('cboPembayaranKasirRWI').getValue() == '' || Ext.get('cboPembayaranKasirRWI').getValue() == 'Pilih Pembayaran...')
        {
            ShowPesanWarningKasirrwi(('Data pembayaran tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirrwi(('Data no. medrec tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirrwi('Nama pasien belum terisi', modul);
            x = 0;
        } else if (Ext.get('dtpTanggalDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirrwi(('Data tanggal kunjungan tidak boleh kosong'), modul);
            x = 0;
        }
        //cboPembayaranKasirRWI

        else if (dsTRDetailKasirrwiKasirList.getCount() === 0)
        {
            ShowPesanWarningKasirrwi('Data dalam tabel kosong', modul);
            x = 0;
        }
        ;
    }
    ;
    return x;
};






function ShowPesanWarningKasirrwi(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
};

function ShowPesanErrorKasirrwi(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 250
                    }
            );
};

function ShowPesanInfoKasirrwi(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
};

function PilihDokterLookUp_KasirRWI(edit) 
{
	var fldDetail = [];
    dsGridJasaDokterPenindak_KasirRWI = new WebApp.DataStore({ fields: fldDetail });
    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			header			: 'kd_component',
			dataIndex		: 'kd_component',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },
		{
			header			: 'tgl_berlaku',
			dataIndex		: 'tgl_berlaku',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },
		{
			header			: 'Komponent',
			dataIndex		: 'component',
			width			: 200,
			menuDisabled	: true,
			hidden 		: false
        },
        {
			header			: 'kd_dokter',
			dataIndex		: 'kd_dokter',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },{
            header			:'Nama Dokter',
            dataIndex		: 'nama',
            sortable		: false,
            hidden			: false,
			menuDisabled	: true,
			width			: 250,
            editor			: new Nci.form.Combobox.autoComplete({
				store	: dsgridpilihdokterpenindak_KasirRWI,
				select	: function(a,b,c){
					var line	= GridDokterTr_KasirRWI.getSelectionModel().selection.cell[0];
					dsGridJasaDokterPenindak_KasirRWI.getRange()[line].data.kd_dokter=b.data.kd_dokter;
					dsGridJasaDokterPenindak_KasirRWI.getRange()[line].data.nama=b.data.nama;
					GridDokterTr_KasirRWI.getView().refresh();
				},
				insert	: function(o){
					return {
						kd_dokter       : o.kd_dokter,
						nama       		: o.nama,
						text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_dokter+'</td><td width="200">'+o.nama+'</td></tr></table>'
					}
				},
				param	: function(){
					var o = grListTRKasirrwi.getSelectionModel().getSelections()[0].data;
					var params={};
					params['kd_unit'] = o.PARENTNYA;
					params['penjas'] = 'rwi';
					return params;
				},
				url		: baseURL + "index.php/main/functionRWJ/getdokterpenindak",
				valueField: 'nama_obat',
				displayField: 'text',
				listWidth: 380
			})
			//getTrDokter(dsTrDokter)
	    },
        ]
    );
	
	
	GridDokterTr_KasirRWI= new Ext.grid.EditorGridPanel({
		id			: 'GridDokterTr_KasirRWI',
		stripeRows	: true,
		width		: 487,
		height		: 160,
        store		: dsGridJasaDokterPenindak_KasirRWI,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridTrDokterColumnModel,
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				//trcellCurrentTindakan_KasirRWI = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				
			}
		},
		viewConfig	: {forceFit: true},
    });
 
	
    var lebar = 500;
    var FormLookUDokter_KasirRWI = new Ext.Window
    (
        {
            id: 'winTRDokterPenindak_KasirRWI',
            title: 'Pilih Dokter Penindak',
            closeAction: 'destroy',
            width: 500,
            height: 220,
            border: false,
            resizable: false,
            iconCls: 'Request',
			constrain : true,    
			modal: true,
           	items: [ 
				GridDokterTr_KasirRWI
			],
			tbar :
			[
				{
					xtype:'button',
					text:'Simpan',
					iconCls	: 'save',
					hideLabel:true,
					id: 'BtnOktrDokter',
					handler:function()
					{
						Datasave_PenataJasaKasirRWI_on_kasirrrwi(false,true);
					}
				},
				'-',
			],
            listeners:
            { 
            }
        }
    );
    loaddatastoredokterVisite_REVISI();
	FormLookUDokter_KasirRWI.show();
	/*if(edit == true){
		GetgridEditDokterPenindakJasa_KasirRWI();
	} else{
		GetgridPilihDokterPenindakJasa_KasirRWI(currentJasaDokterKdProduk_KasirRWI,currentJasaDokterKdTarif_KasirRWI);
	}*/
	
};

/*
	FORM EDIT TARIF
	OLEH 	: HADAD AL GOJALI
	TANGGAL : 2017 - 04 - 20 
	
 */

function PilihEditTarifLookUp_KasirRWI(data) 
{
	loaddatastoreTarifComponent(data);
        var formLookupEditTarif_KasirRWI = new Ext.Window
		(
			{
				id: 'formEditTarif_KASIRRWI',
				title: 'Edit data tarif',
				closeAction: 'destroy',
				width: 600,
				height: 400,
				border: false,
				resizable: false,
				plain: true,
				layout: 'fit',
				iconCls: 'Request',
				modal: true,
				bodyStyle:'padding: 3px;',
				items: [
                {
					columnWidth: .40,
					layout: 'form',
					labelWidth: 100,
					border: false,
					bodyStyle:'padding: 10px;',
					items:[
						{
							xtype 		: 'textfield',
							id 			: 'TxtNamaProduk',
							fieldLabel 	: 'Produk',
							anchor		: '100%',
							readOnly    : true,
                            value		: data.DESKRIPSI,
                            bodyStyle	: 'margin:10px;',
						},
						GridEditTarif_KasirRWI(),
                        // paneltotal
					]
				},
				],
				bbar:[  
					{
						text: 'Cito',
						id: 'btnHitungCito_KASIRRWI',
						tooltip: 'Hitung Cito',
						//iconCls: 'save',
						handler: function ()
						{
							PilihEditCitoLookUp_KasirRWI(data);
						},
						callback : function(){
							Ext.getCmp('TxtPercentaseTarif').focus();
						}
					},'-',
					{
						text: 'Discount',
						id: 'btnDiscount_KASIRRWI',
						tooltip: 'Hitung Discount',
						//iconCls: 'save',
						handler: function ()
						{
							//savePostingManualKasirRWI();
						}
					},'-',
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalTarif',
						width       : 100,
						readOnly    : true,
					},
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalMarkup',
						width       : 100,
						readOnly    : true,
					},
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalDisk',
						width       : 100,
						readOnly    : true,
					},
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalTarif_Baru',
						width       : 100,
						readOnly    : true,
					},
				],
                tbar:[
                    {
                        text: 'Simpan',
                        id: 'btnSimpanEditTarif_KASIR_RWI',
                        tooltip: 'Edit Tarif',
                        iconCls: 'save',
                        handler: function ()
                        { 
							Ext.Ajax.request({
								url 	: baseURL + "index.php/rawat_inap/control_data_tarif_component/update_tarif",
								params 	: listDataParams_EditTarif_Kasir_RWI(0, false, false),
								success : function (o)
								{
									var cst = Ext.decode(o.responseText);
									if (cst.status === true) {
										ShowPesanInfoKasirrwi('Tarif berhasil diperbarui', 'Informasi');
										PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('TARIF', cst.total_harga);
									}else{
										ShowPesanWarningKasirrwi('Tarif tidak berhasil diperbarui', 'Peringatan');
									}
								}
							});
                        }
					},{
						text: 'Reset',
						id: 'btnEditTarif_KASIR_RWI',
						iconCls: 'cancel',
						handler: function ()
						{
							dsDataStoreGridTarifKomponent.removeAll();
							loaddatastoreTarifComponent(data);
						}
					},        
                ],
				listeners:
				{
				
				}
            }
		);
        formLookupEditTarif_KasirRWI.show();
};

	function listDataParams_EditTarif_Kasir_RWI(persentase, mark, disc){
		var data_grid = gridEditTarifKasirrwi.getSelectionModel().grid.store.data;
		var list      = "";
		for(var x=0; x<data_grid.length; x++){
			var result = data_grid.items[x].data;
			var y      = '';
			var z      = '@@##$$@@';

			var tmpMarkup 	= result.MARKUP;
			var hasilMarkup = result.TARIF;
			var tmpDisc  	= result.DISC;
			var hasilDisc  	= result.TARIF;
			var Totaltarif  = result.TARIF;

			if (persentase > 0 && mark === true) {
				hasilMarkup = (tmpMarkup / 100) * persentase;
				if (result.MARKUP > 0) {
					hasilMarkup = hasilMarkup + tmpMarkup;
				}
			}else{
				hasilMarkup	= result.MARKUP;
			}

			if (persentase > 0 && disc === true) {
				hasilDisc 	= tmpDisc / 100 * persentase;
				if (result.MARKUP > 0) {
					hasilDisc 	= hasilDisc + tmpDisc;
				}
			}else{
				hasilDisc 	= result.DISC;
			}

			y   = result.KD_COMPONENT
			y   += z + hasilMarkup
			y   += z + hasilDisc


			if (x === (data_grid.length - 1))
			{
				list += y
			} else
			{
				list += y + '##[[]]##'
			};
		}
		//return list;
		
		var params =
		{
			no_transaksi: notransaksi,
			list 		: list,
			urut 		: result.URUT,
			kd_kasir	: '02',
			total_tarif	: Ext.getCmp('txtFieldTotalTarif_Baru').getValue(),
		};
		return params
	}



    function GridEditTarif_KasirRWI(){
    	var rowLine = 0;
    	var rowData = "";
		dsDataStoreGridTarifKomponent.removeAll();        
	    var Field = ['TARIF','DISC','MARKUP'];
	    DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
    	//loaddatastoreProdukComponent();

		var cm = new Ext.grid.ColumnModel({
			// specify any defaults for each column
			defaults: {
				sortable: false // columns are not sortable by default           
			},
			columns: [
			{
				header 		: 'Kd Kasir',
				dataIndex 	: 'KD_KASIR',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'No Transaksi',
				dataIndex 	: 'NO_TRANSAKSI',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Urut',
				dataIndex 	: 'URUT',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Tgl Transaksi',
				dataIndex 	: 'TGL_TRANSAKSI',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Kd Component',
				dataIndex 	: 'KD_COMPONENT',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header: 'Component',
				dataIndex: 'COMPONENT',
				width: 200
			}, 
			{
				header: 'Tarif Lama',
				dataIndex: 'TARIF',
                align: 'right',
				width: 75,
                renderer: function (v, params, record)
				{
					return formatCurrency(record.data.TARIF);
				},
			}, 
			{
				header: 'Markup',
				dataIndex: 'MARKUP',
				width: 75,
				editor: new Ext.form.NumberField
				(
					{
						id: 'fieldcolMarkKasirRWI',
						allowBlank: true,
						enableKeyEvents: true,
						width: 30,
						enableKeyEvents : true,
						listeners : {
							specialkey : function(a, b, c){
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
									var tmpTotalPerhitungan = gridEditTarifKasirrwi.getStore().getRange()[rowLine].get('TARIF');
									tmpTotalPerhitungan = (tmpTotalPerhitungan+Ext.getCmp('fieldcolMarkKasirRWI').getValue())- gridEditTarifKasirrwi.getStore().getRange()[rowLine].get('DISC');
									gridEditTarifKasirrwi.getStore().getRange()[rowLine].set('TARIF_BARU', tmpTotalPerhitungan);
									gridEditTarifKasirrwi.getStore().getRange()[rowLine].set('MARKUP', Ext.getCmp('fieldcolMarkKasirRWI').getValue());

									var totalMarkup = 0;
									var totalDisc 	= 0;
									var totalTarif_Baru= 0;
					                for(var i = 0 ; i < dsDataStoreGridTarifKomponent.getCount();i++)
									{
										totalMarkup 	+= gridEditTarifKasirrwi.getStore().getRange()[i].get('MARKUP');
										totalDisc 		+= gridEditTarifKasirrwi.getStore().getRange()[i].get('DISC');
										totalTarif_Baru += gridEditTarifKasirrwi.getStore().getRange()[i].get('TARIF_BARU');
									}
									Ext.getCmp('txtFieldTotalMarkup').setValue(totalMarkup);
									Ext.getCmp('txtFieldTotalDisk').setValue(totalDisc);
									Ext.getCmp('txtFieldTotalTarif_Baru').setValue(totalTarif_Baru);
								}
							}
						},
					}
				),
			}, 
			{
				header: 'Discount',
				dataIndex: 'DISC',
				width: 75,
				editor: new Ext.form.NumberField
				(
					{
						id: 'fieldcolDiscKasirRWI',
						allowBlank: true,
						enableKeyEvents: true,
						width: 30,
						enableKeyEvents : true,
						listeners : {
							specialkey : function(a, b, c){
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
									var tmpTotalPerhitungan = gridEditTarifKasirrwi.getStore().getRange()[rowLine].get('TARIF');
									tmpTotalPerhitungan = (tmpTotalPerhitungan+gridEditTarifKasirrwi.getStore().getRange()[rowLine].get('MARKUP'))- Ext.getCmp('fieldcolDiscKasirRWI').getValue();
									gridEditTarifKasirrwi.getStore().getRange()[rowLine].set('TARIF_BARU', tmpTotalPerhitungan);
									gridEditTarifKasirrwi.getStore().getRange()[rowLine].set('DISC', Ext.getCmp('fieldcolDiscKasirRWI').getValue());

									var totalMarkup = 0;
									var totalDisc 	= 0;
									var totalTarif_Baru= 0;
					                for(var i = 0 ; i < dsDataStoreGridTarifKomponent.getCount();i++)
									{
										totalMarkup 	+= gridEditTarifKasirrwi.getStore().getRange()[i].get('MARKUP');
										totalDisc 		+= gridEditTarifKasirrwi.getStore().getRange()[i].get('DISC');
										totalTarif_Baru += gridEditTarifKasirrwi.getStore().getRange()[i].get('TARIF_BARU');
									}
									Ext.getCmp('txtFieldTotalMarkup').setValue(totalMarkup);
									Ext.getCmp('txtFieldTotalDisk').setValue(totalDisc);
									Ext.getCmp('txtFieldTotalTarif_Baru').setValue(totalTarif_Baru);
								}
							}
						},
					}
				),
			}, 
            {
				id: 'fieldcolTarifBaruKasirRWI',
	            header: 'Tarif Baru',
	            dataIndex: 'TARIF_BARU',
                align: 'right',
	            width: 75,
                editor: new Ext.form.TextField
                (
                        {
                            id: 'fieldcolTunaiBarurwi',
                            allowBlank: true,
                            enableKeyEvents: true,
                            width: 30,
							listeners : {
								specialkey : function(a, b, c){
									if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
										var tmpTotalPerhitungan = gridEditTarifKasirrwi.getStore().getRange()[rowLine].get('TARIF');
										var tmpTotalTarifBaru   = Ext.getCmp('fieldcolTunaiBarurwi').getValue();

										if (parseInt(tmpTotalTarifBaru) - parseInt(tmpTotalPerhitungan) < 0) {
											gridEditTarifKasirrwi.getStore().getRange()[rowLine].set('DISC', parseInt(tmpTotalPerhitungan) - parseInt(tmpTotalTarifBaru));
											gridEditTarifKasirrwi.getStore().getRange()[rowLine].set('MARKUP', 0);
										}else{
											gridEditTarifKasirrwi.getStore().getRange()[rowLine].set('DISC', 0);
											gridEditTarifKasirrwi.getStore().getRange()[rowLine].set('MARKUP', parseInt(tmpTotalTarifBaru) - parseInt(tmpTotalPerhitungan));
										}
										gridEditTarifKasirrwi.getStore().getRange()[rowLine].set('TARIF_BARU', Ext.getCmp('fieldcolTunaiBarurwi').getValue());
										var totalMarkup     = 0;
										var totalDisc       = 0;
										var totalTarif_Baru = 0;
										for(var i = 0 ; i < dsDataStoreGridTarifKomponent.getCount();i++)
										{
											totalMarkup 	+= gridEditTarifKasirrwi.getStore().getRange()[i].get('MARKUP');
											totalDisc 		+= gridEditTarifKasirrwi.getStore().getRange()[i].get('DISC');
											totalTarif_Baru += gridEditTarifKasirrwi.getStore().getRange()[i].get('TARIF_BARU');
										}
										Ext.getCmp('txtFieldTotalMarkup').setValue(totalMarkup);
										Ext.getCmp('txtFieldTotalDisk').setValue(totalDisc);
										Ext.getCmp('txtFieldTotalTarif_Baru').setValue(totalTarif_Baru);
									}
								}
							},
                        }
                ),
                renderer: function (v, params, record)
				{
					return formatCurrency(record.data.TARIF_BARU);
				},
	        }]
	    });

        gridEditTarifKasirrwi = new Ext.grid.EditorGridPanel
        (
            {
				title: '',
				id: 'gridEditTarifKasirrwi',
				store: dsDataStoreGridTarifKomponent,
				clicksToEdit: 1, 
				editable: true,
				border: true,
				columnLines: true,
				frame: false,
				stripeRows       : true,
				trackMouseOver   : true,
				height: 250,
				width: 560,
				autoScroll: true,
				sm: new Ext.grid.CellSelectionModel
				(
					{
						singleSelect: true,
						listeners:
						{
						}
					}
				),
				sm: new Ext.grid.RowSelectionModel
				(
					{
						singleSelect: true,
						listeners:
						{
							rowselect: function (sm, row, rec)
							{
								rowLine = row;
							}
						}
					}
				),
                cm: cm,
                viewConfig: {
                    forceFit: true
                },  
            }
        );
        return gridEditTarifKasirrwi;
    };

    function loaddatastoreTarifComponent(params){
        Ext.Ajax.request({
            url: baseURL +  "index.php/rawat_inap/control_data_tarif_component/get_data_component",
            params: {
                no_transaksi 	: rowpasien.NO_TRANSAKSI,
                // no_transaksi 	: params.NO_TRANSAKSI,
                kd_produk 		: params.KD_PRODUK,
                kd_unit 		: params.KD_UNIT,
                kd_tarif 		: params.KD_TARIF,
                tgl_berlaku 	: params.TGL_BERLAKU,
                tgl_transaksi 	: params.TGL_TRANSAKSI,
                urut 			: params.URUT,
                kd_kasir 		: '02',
            },
            success: function(response) {
                //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
                var cst  = Ext.decode(response.responseText);
				var totalTarif 	= 0;
				var totalMarkup = 0;
				var totalDisc 	= 0;
				var totalTarif_Baru= 0;
                //dsDataPerawatPenindak_KASIR_RWI.load(myData);
                for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                    var recs    = [],recType = DataStoreGridKomponent.recordType;
                    var o       = cst['data'][i];
                    recs.push(new recType(o));
                    dsDataStoreGridTarifKomponent.add(recs);
                }
                for(var i = 0 ; i < dsDataStoreGridTarifKomponent.getCount();i++)
				{
					totalTarif 	+= dsDataStoreGridTarifKomponent.data.items[i].data.TARIF;
					totalMarkup += dsDataStoreGridTarifKomponent.data.items[i].data.MARKUP;
					totalDisc 	+= dsDataStoreGridTarifKomponent.data.items[i].data.DISC;
					totalTarif_Baru 	+= dsDataStoreGridTarifKomponent.data.items[i].data.TARIF_BARU;
				}
				Ext.getCmp('txtFieldTotalTarif').setValue(totalTarif);
				Ext.getCmp('txtFieldTotalMarkup').setValue(totalMarkup);
				Ext.getCmp('txtFieldTotalDisk').setValue(totalDisc);
				Ext.getCmp('txtFieldTotalTarif_Baru').setValue(totalTarif_Baru);
            },
        });
    }

/*
	FITUR FORM EDIT CITO KASIR RWI
	OLEH 	: HADAD AL GOJALI
	TANGGAL : 2017 - 04 - 25

 */
function PilihEditCitoLookUp_KasirRWI(data) 
{
	//loaddatastoreTarifComponent(data);
        var formLookupEditCito_KasirRWI = new Ext.Window
		(
			{
				id: 'formEditCito_KASIRRWI',
				title: 'Edit tarif cito',
				closeAction: 'destroy',
				width: 300,
				height: 250,
				border: false,
				resizable: false,
				plain: true,
				layout: 'fit',
				iconCls: 'Request',
				modal: true,
				bodyStyle:'padding: 3px;',
				items: [
				{
					columnWidth: .4,
					layout: 'form',
					labelWidth: 85,
					border: false,
					bodyStyle:'padding: 10px;',
					items:[
						{
							xtype 		: 'textfield',
							id 			: 'TxtPercentaseTarif',
							fieldLabel 	: 'Kenaikan (%)',
                            anchor		: '100%',
							readOnly    : false,
							value    	: 0,
                            bodyStyle	: 'margin:10px;',
						},
						GridEditCito_KasirRWI(),
					]
				},
				],
                tbar:[
                    {
                        text: 'Simpan',
                        id: 'btnSimpanEditTarif_KASIR_RWI',
                        tooltip: 'Edit Tarif',
                        iconCls: 'save',
                        handler: function ()
                        { 
                        	hitungComponentPersentase_KASIRRWI(Ext.getCmp('TxtPercentaseTarif').getValue(), true, false);
                        	formLookupEditCito_KasirRWI.close();
							/*Ext.Ajax.request({
								url 	: baseURL + "index.php/rawat_inap/control_data_tarif_component/update_tarif",
								params 	: {null:null},//listDataParams_EditTarif_Kasir_RWI(Ext.getCmp('TxtPercentaseTarif').getValue(), true, false),
								success : function (o)
								{
									var cst = Ext.decode(o.responseText);
									if (cst.status === true) {
										ShowPesanInfoKasirrwi('Tarif berhasil diperbarui', 'Informasi');
										PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('TARIF', cst.total_harga);
									}else{
										ShowPesanWarningKasirrwi('Tarif tidak berhasil diperbarui', 'Peringatan');
									}
								}
							});*/
                        }
					},      
                ],
				listeners:
				{
					callback : function(){
						Ext.getCmp('TxtPercentaseTarif').focus();
					}
				}
            }
		);

        formLookupEditCito_KasirRWI.show();
};

function GridEditCito_KasirRWI(){
    	var rowLine = 0;
    	var rowData = "";
        //dsDataStoreGridTarifKomponent.removeAll();        
	    var Field = ['TARIF','DISC','MARKUP'];
	    //DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
    	//loaddatastoreProdukComponent();

		var cm = new Ext.grid.ColumnModel({
			// specify any defaults for each column
			defaults: {
				sortable: false // columns are not sortable by default           
			},
			columns: [
			{
				header 		: 'Kd Kasir',
				dataIndex 	: 'KD_KASIR',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'No Transaksi',
				dataIndex 	: 'NO_TRANSAKSI',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Urut',
				dataIndex 	: 'URUT',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Tgl Transaksi',
				dataIndex 	: 'TGL_TRANSAKSI',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Kd Component',
				dataIndex 	: 'KD_COMPONENT',
				width 		: 200,
				hidden 		: true,
			},{
				xtype 		: 'checkcolumn',
				header 		: '',
				id 			: 'CheckKomponen',
				dataIndex	: 'CHECK_COMPONENT',
				width 		: 75,
			},
			{
				header: 'Component',
				dataIndex: 'COMPONENT',
				width: 200,
			}, 
			{
				header 		: 'Tarif',
				dataIndex 	: 'TARIF',
				width 		: 200,
				hidden 		: true,
			}, 
			]
	    });

        gridEditCitoKasirrwi = new Ext.grid.EditorGridPanel
        (
            {
				id: 'GridEditCito_KasirRWI',
				store: dsDataStoreGridTarifKomponent,
				clicksToEdit: 1, 
				editable: true,
				border: true,
				columnLines: true,
				frame: false,
				stripeRows       : true,
				trackMouseOver   : true,
				height: 240,
				width: 450,
				autoScroll: true,
				sm: new Ext.grid.CellSelectionModel
				(
					{
						singleSelect: true,
						listeners:
						{
						}
					}
				),
				sm: new Ext.grid.RowSelectionModel
				(
					{
						singleSelect: true,
						listeners:
						{
							rowselect: function (sm, row, rec)
							{
								rowLine = row;
							}
						}
					}
				),
                cm: cm,
                viewConfig: {
                    forceFit: true
                },  
            }
        );
        return gridEditCitoKasirrwi;
    };

	function hitungComponentPersentase_KASIRRWI(persentase, mark, disc){
		var data_grid 		= gridEditTarifKasirrwi.getSelectionModel().grid.store.data;
		var data_grid_cito 	= gridEditCitoKasirrwi.getSelectionModel().grid.store.data;
		var list      = "";
		var grandTotal 	= 0;
		var TotalMarkup = 0;
		var TotalDisc 	= 0;
		for(var x=0; x<data_grid.length; x++){
			var result = data_grid.items[x].data;

			var tmpMarkup 	= result.MARKUP;
			var hasilMarkup = result.TARIF;
			var tmpDisc  	= result.DISC;
			var hasilDisc  	= result.TARIF;
			var Totaltarif  = result.TARIF;

			var tmpCheckComponent = gridEditTarifKasirrwi.getStore().getRange()[x].get('CHECK_COMPONENT');


			if (persentase > 0 && mark === true && tmpCheckComponent === true) {
				hasilMarkup = (hasilMarkup / 100) * persentase;
				if (result.MARKUP > 0) {
					hasilMarkup = hasilMarkup + tmpMarkup;
				}
			}else{
				hasilMarkup	= result.MARKUP;
			}

			if (persentase > 0 && disc === true && tmpCheckComponent === true) {
				hasilDisc 	= hasilDisc / 100 * persentase;
				if (result.MARKUP > 0) {
					hasilDisc 	= hasilDisc + tmpDisc;
				}
			}else{
				hasilDisc 	= result.DISC;
			}

			gridEditTarifKasirrwi.getStore().getRange()[x].set('MARKUP', hasilMarkup);
			gridEditTarifKasirrwi.getStore().getRange()[x].set('DISC', hasilDisc);
			gridEditTarifKasirrwi.getStore().getRange()[x].set('TARIF_BARU', (result.TARIF+hasilMarkup)-hasilDisc);
			TotalMarkup += hasilMarkup;
			TotalDisc 	+= hasilDisc;
			grandTotal 	+= (result.TARIF+hasilMarkup)-hasilDisc;
		}

		Ext.getCmp('txtFieldTotalMarkup').setValue(TotalMarkup);
		Ext.getCmp('txtFieldTotalDisk').setValue(TotalDisc);
		Ext.getCmp('txtFieldTotalTarif_Baru').setValue(grandTotal);
	}

function GetgridEditDokterPenindakJasa_KasirRWI(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgrideditjasadokterpenindak",
		params: {
			kd_unit:currentKDUnit_KasirRWI,
			urut:currentJasaDokterUrutDetailTransaksi_KasirRWI,
			kd_kasir:currentKdKasir_KasirRWI,
			no_transaksi:currentNoTransaksi_KasirRWI,
			tgl_transaksi:tanggaltransaksitampung,
			
		},
		failure: function(o)
		{
			ShowPesanErrorKasirrwi('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_RWI.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				if(cst.totalrecords > 0){
					var recs=[],
						recType=dsGridJasaDokterPenindak_RWI.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsGridJasaDokterPenindak_RWI.add(recs);
					GridDokterTr_RWI.getView().refresh();
				} else{
					GetgridPilihDokterPenindakJasa_KasirRWI(currentJasaDokterKdProduk_KasirRWI,currentJasaDokterKdTarif_KasirRWI);
				}
			} 
			else 
			{
				ShowPesanErrorKasirrwi('Gagal membaca list dokter penindak', 'Error');
			};
		}
	});
}

function GetgridPilihDokterPenindakJasa_KasirRWI(kd_produk,kd_tarif){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridjasadokterpenindak",
		params: {
			kd_produk:kd_produk,
			kd_tarif:kd_tarif,
			kd_unit:currentKDUnit_KasirRWI
		},
		failure: function(o)
		{
			ShowPesanErrorKasirrwi('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_RWI.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsGridJasaDokterPenindak_RWI.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsGridJasaDokterPenindak_RWI.add(recs);
				GridDokterTr_KasirRWI.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorKasirrwi('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}


function SimpanJasaDokterPenindak_KasirRWI(kd_produk,kd_tarif,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/savejasadokterpenindak",
		params: getParamsJasaDokterPenindak_KasirRWI(urut,harga),
		failure: function(o)
		{
			ShowPesanErrorKasirrwi('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			//dsGridJasaDokterPenindak_KasirRWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				ShowPesanInfoKasirrwi('Dokter penindak berhasil disimpan','Information');
				SimpanJasaDokterPenindak_KasirRWI_SQL(kd_produk,kd_tarif,urut,harga);
				GetgridEditDokterPenindakJasa_KasirRWI();
				PenataJasaKasirRWI.dsGridTindakan.insert(PenataJasaKasirRWI.dsGridTindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
			} 
			else 
			{
				ShowPesanErrorKasirrwi('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}

function getParamsJasaDokterPenindak_KasirRWI(urut,harga){
	var params = {
		kd_kasir:currentKdKasir_KasirRWI,
		no_transaksi:currentNoTransaksi_KasirRWI,
		//tgl_transaksi:currentTglTransaksi_KasirRWI,
		tgl_transaksi:tanggaltransaksitampung,
		urut:urut,
		harga:harga
	};
	params['jumlah'] = dsGridJasaDokterPenindak_RWI.getCount();
	for(var i = 0 ; i < dsGridJasaDokterPenindak_RWI.getCount();i++)
	{
		params['kd_component-'+i]=dsGridJasaDokterPenindak_RWI.data.items[i].data.kd_component;
		params['kd_dokter-'+i]=dsGridJasaDokterPenindak_RWI.data.items[i].data.kd_dokter;
	}
	return params;
}


// =====================================================INTEGRASI SQL SERVER====================================================================================================
function Datasave_KasirrwiKasir_SQL()
{
    Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_inap_sql/functionRWI/savepembayaran",
		params: getParamDetailTransaksiKasirrwi(),
		failure: function (o)
		{
			ShowPesanWarningKasirrwi('SQL. Error simpan pembayaran, Hubungi Admin!', 'Gagal');
		},
		success: function (o)
		{
			RefreshDatahistoribayar(0);
			//   refeshKasirrwiKasir();
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				
			} else
			{
				ShowPesanWarningKasirrwi('SQL. Data Belum Simpan segera Hubungi Admin', 'Gagal');
			};
		}
	})
};

function Datasave_PenataJasaKasirRWI_on_kasirrrwi_SQL(mBol) {
    Ext.Ajax.request({
        url: baseURL + "index.php/rawat_inap_sql/functionRWI/savedetailpenyakitkasir",
        params: getParamDetailTransaksi_Kasir(),
        failure: function (o) {
            ShowPesanWarningKasirrwi('SQL. Error tambah tindakan, Hubungi Admin!', 'Gagal');
        },
        success: function (o) {
            RefreshDataDetail_kasirrwi(notransaksi);
            var cst = Ext.decode(o.responseText);
            if (cst.success === true && cst.compo === true)
            {
               
            } else if (cst.success === true) {
                
            } else {
                ShowPesanWarningKasirrwi('SQL. Gagal tambah tindakan!', 'Gagal');
            }
        }
    });

};

function DataDeleteDetail_panatajasarwi_SQL()
{
    Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_inap_sql/functionRWI/deletetindakantambahan",
		params: getParamDataDeleteDetail_panatajasarwi(),
		failure: function (o) {
            ShowPesanWarningKasirrwi('SQL. Error hapus baris tindakan, Hubungi Admin!', 'Gagal');
        },
		success: function (o)
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				
			} else {
				ShowPesanWarningKasirrwi('SQL. Gagal mennghapus baris ini!', 'WARNING');
			};
		}
	});
};

function DataDeleteKasirrwiKasirDetail_SQL()
{
    Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_inap_sql/functionRWI/deletedetail_bayar",
		params: getParamDataDeleteKasirrwiKasirDetail(),
		failure: function (o) {
            ShowPesanWarningKasirrwi('SQL. Error hapus baris pembayaran, Hubungi Admin!', 'Gagal');
        },
		success: function (o)
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				
			} else
			{
				ShowPesanWarningKasirrwi('SQL. Gagal menghapus pembayaran!', 'WARNING');
			}
			;
		}
	})
};


function BatalTransaksi_kasirrwi_SQL(notrans,notransbaru,urutmasuk,urutmasukbaru)
{
    Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_inap_sql/functionRWI/batal_transaksi",
		params: getParamDetailBatalTransaksiRWI_SQL(notrans,notransbaru,urutmasuk,urutmasukbaru),
		failure: function (o)
		{
			ShowPesanWarningKasirrwi('Error' + Ext.decode(o.responseText), 'Gagal');
		},
		success: function (o)
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				cellSelectedtutup_kasirrwi = '';
			} else
			{
				ShowPesanWarningKasirrwi('Error' + Ext.decode(o.responseText), 'Gagal');
				cellSelectedtutup_kasirrwi = '';
			};
		}
	})
};

function getParamDetailBatalTransaksiRWI_SQL(notrans,notransbaru,urutmasuk,urutmasukbaru)
{
    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    };

    var params =
	{
		notransaksi : notrans,
		notransbaru	: notransbaru,
		urutmasuk	: urutmasuk,
		urutmasukbaru: urutmasukbaru,
		kdPasien: kodepasien,
		noTrans: notransaksi,
		kdUnit: kodeunit,
		kdDokter: kdokter_btl,
		tglTrans: tgltrans,
		kdCustomer: kdcustomeraigd,
		Keterangan: variablebatalhistori_rwi,
	};
    return params
};


function UpdateTutuptransaksi_SQL(mBol)
{
    Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_inap_sql/functionRWI/ubah_co_status_transksi",
		params: paramUpdateTransaksi(),
		failure: function (o)
		{
			ShowPesanWarningKasirrwi('SQL. Error, Hubungi Admin!', 'Gagal');
		},
		success: function (o)
		{
			
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				
				cellSelectedtutup_kasirrwi = '';
			} else
			{
				ShowPesanWarningKasirrwi('SQL. Tutup transaksi gagal!', 'Gagal');
				cellSelectedtutup_kasirrwi = '';
			}
			;
		}
	})
};

function SimpanJasaDokterPenindak_KasirRWI_SQL(kd_produk,kd_tarif,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/savejasadokterpenindak",
		params: getParamsJasaDokterPenindak_KasirRWI(urut,harga),
		failure: function(o)
		{
			ShowPesanErrorKasirrwi('SQL. Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			//dsGridJasaDokterPenindak_KasirRWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				
			} 
			else 
			{
				ShowPesanErrorKasirrwi('SQL. Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}


/*
    
    UPDATE GANTI DOKTER 
    OLEH    : HADAD AL GOJALI
    TANGGAL : 2017 - 02 - 18
    ALASAN  : GANTI DOKTER BELUM BERFUNGSI
 */

function GantiDokterPasienLookUp_rwi_REVISI() 
{
    var lebar = 440;
    FormLookUpsdetailTRGantiDokter_rwi_REVISI = new Ext.Window
    (
        {
            id: 'idGantiDokter',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiDokter_rwi_REVISI(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRGantiDokter_rwi_REVISI.show();
    //GantiPasien_rwj();
};


function getFormEntryTRGantiDokter_rwi_REVISI(lebar) 
{
    var pnlTRKelompokPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRGanti',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
                    getItemPanelInputGantiDokter_rwi_REVISI(lebar),
                    getItemPanelButtonKelompokPasien_rwi_REVISI(lebar)
            ],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiDokter = new Ext.Panel
    (
        {
            id: 'FormDepanGantiDokter',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: true,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: [pnlTRKelompokPasien_igd 
                
            ]

        }
    );

    return FormDepanGantiDokter
};

function getItemPanelInputGantiDokter_rwi_REVISI(lebar) 
{
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding:10px 10px 10px 0px',
        border:false,
        height:170,
        items:
        [
            {
                columnWidth: .9,
                width: lebar -35,
                labelWidth:100,
                layout: 'form',
                border: false,
                items:
                [   
                    {

                        xtype: 'textfield',
                        fieldLabel:  'Unit Asal ',
                        name: 'txtUnitAsal_DataPasien',
                        id: 'txtUnitAsal_DataPasien',
                        value:namaunit,
                        readOnly:true,
                        width: 100,
                        anchor: '99%'
                    },{
                        xtype: 'textfield',
                        fieldLabel: 'Dokter Asal ',
                        name: 'txtDokterAsal_DataPasien',
                        id: 'txtDokterAsal_DataPasien',
                        value:tmpNamaDokter,
                        readOnly:true,
                        width: 100,
                        anchor: '99%'
                    },
                    mComboDokterGantiEntry()
                ]
            }
        ]
    };
    return items;
};

function mComboDokterGantiEntry(){ 
    /* var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry = new WebApp.DataStore({fields: Field}); */
    loaddatastoredokter();
    var cboDokterGantiEntry = new Ext.form.ComboBox({
        id: 'cboDokterRequestEntry',
        typeAhead: true,
        triggerAction: 'all',
        name:'txtdokter',
        lazyRender: true,
        mode: 'local',
        selectOnFocus:true,
        forceSelection: true,
        emptyText:'Pilih Dokter...',
        fieldLabel: 'Dokter Baru',
        align: 'Right',
        store: dsDokterRequestEntry,
        valueField: 'KD_DOKTER',
        displayField: 'NAMA',
        anchor:'100%',
        listeners:{
            'select': function(a,b,c){
                vKdDokter = b.data.KD_DOKTER;
            },
        }
    });
    return cboDokterGantiEntry;
};

function getItemPanelButtonKelompokPasien_rwi_REVISI(lebar) 
{
    var items =
    {
        layout: 'column',
        border: false,
        height:30,
        anchor:'100%',
        style:{'margin-top':'-1px'},
        items:
        [
            {
                layout: 'hBox',
                width:400,
                border: false,
                bodyStyle: 'padding:5px 0px 5px 5px',
                defaults: { margins: '3 3 3 3' },
                anchor: '90%',
                layoutConfig: 
                {
                    align: 'middle',
                    pack:'end'
                },
                items:
                [
                    {
                        xtype:'button',
                        text:'Simpan',
                        width:70,
                        style:{'margin-left':'0px','margin-top':'0px'},
                        hideLabel:true,
                        id:Nci.getId(),
                        handler:function()
                        {
                            Datasave_GantiDokter_rwi();
                        }
                    },
                    {
                        xtype:'button',
                        text:'Tutup',
                        width:70,
                        hideLabel:true,
                        id:Nci.getId(),
                        handler:function() 
                        {
                            FormLookUpsdetailTRGantiDokter_rwi_REVISI.close();
                        }
                    }
                ]
            }
        ]
    }
    return items;
};


function loaddatastoredokter(){
    dsDokterRequestEntry.load({
         params :{
            Skip: 0,
            Take: 1000,
            Sort: 'dokter.nama',
            Sortdir: 'ASC',
            target: 'ViewComboDokterSpesial',
            param: 'dokter_spesial.kd_spesial ='+ tmpKdSpesial
        }
    });
}


function Datasave_GantiDokter_rwi(mBol) 
{   
    if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntry').dom.value  === 'Pilih Dokter...'))
    {
        ShowPesanWarningKasirrwi('Dokter baru harap diisi', "Informasi");
    }else if(kdokter_btl == vKdDokter){
        ShowPesanWarningKasirrwi('Harap merubah dokter dengan dokter yang lain', "Informasi");
    }else{
        Ext.Ajax.request
        (
            {
                url: baseURL +  "index.php/main/functionRWI/UpdateGantiDokter", 
                params: {
                    KdPasien    : kodepasien,
                    TglMasuk    : tmpTanggalMasuk,
                    KdUnit      : tmpUnit,
                    UrutMasuk   : urutmasuk,
                    KdDokter    : vKdDokter,
                },
                failure: function(o)
                {
                    ShowPesanWarningKasirrwi('Simpan dokter pasien gagal', 'Gagal');
                },  
                success: function(o) 
                {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true) 
                    { 	

                    	grListTRKasirrwi.getStore().getRange()[dataRowIndex].set('NAMA_DOKTER', cst.nama_dokter);
                        //refeshKasirrwiKasir();
                        FormLookUpsdetailTRGantiDokter_rwi_REVISI.close();
                        ShowPesanInfoKasirrwi("Mengganti dokter pasien berhasil", "Success");
                    }else 
                    {
                        ShowPesanWarningKasirrwi('Simpan dokter pasien gagal', 'Gagal');
                    };
                }
            }
        ) 
    }
    
};

/*
    COMBO BOX PERAWAT
    OLEH    : HADAD AL GOJALI 
    TANGGAL : 2017 - 02 - 20
    ALASAN  : BELUM ADA FITUR DAFTAR PERAWAT YANG MENANGANI
 */

function loaddatastoreperawat(){
    Ext.Ajax.request({
        url: baseURL +  "index.php/general/control_cmb_perawat/getCmbPerawat",
        params: {
            kd_kasir        : '02',
            tgl_transaksi   : tgltrans,
            tgl_masuk       : tmpTanggalMasuk,
            no_medrec       : kodepasien,
            kd_unit_kamar   : tmpKdUnitKamar,
            kd_job          : '3',
            no_transaksi    : notransaksi,
        },
        success: function(response) {
            //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
            var cst  = Ext.decode(response.responseText);
            //dsDataPerawatPenindak_KASIR_RWI.load(myData);
            for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                var recs    = [],recType = dsDataPerawatPenindak_KASIR_RWI.recordType;
                var o       = cst['data'][i];
                recs.push(new recType(o));
                dsDataPerawatPenindak_KASIR_RWI.add(recs);
            }
        },
        //jsonData: Ext.JSON.encode(queryform.getValues())
    });
    /*dsDataPerawatPenindak_KASIR_RWI.load({
        }
    });*/
}

function mCombo_TindakanMenyusul_KASIRRWI()
{      
    var cboDataPerawatPenindak_KASIR_RWI = new Ext.form.ComboBox
    (
        {
            id: 'cboDataTindakanMenyusul_KASIRRWI',
            typeAhead       : true,
            triggerAction   : 'all',
            lazyRender      : true,
			hidden:true,
            mode            : 'local',
            disabled 		: true,
            emptyText: 'Kamar',
            fieldLabel:  '',
            align: 'Right',
            width: 100,
            store: dsDataStoreGridHistoryPindah,
            valueField: 'kd_unit_kamar',
			displayField: 'nama_kamar',
			remoteSort:true,
			remoteFilter:true,
			remoteGroup:true,
			sortInfo: { field: 'urut_nginap', direction: 'DESC' },
			listeners:
            {
                select : function(a, b){
                	tmpkd_unitKamar = b.data.kd_unit_kamar;
                	tmpkd_unit = b.data.kd_unit_kamar;
                }      
            }
        }
    );return cboDataPerawatPenindak_KASIR_RWI;
};

function mCombo_Perawat_KASIRRWI()
{ 
    var Field = ['KD_DOKTER','NAMA'];

    dsDataPerawatPenindak_KASIR_RWI = new WebApp.DataStore({ fields: Field });
    loaddatastoreperawat();
    var cboDataPerawatPenindak_KASIR_RWI = new Ext.form.ComboBox
    (
        {
            id: 'cboDataPerawatPenindak_KASIR_RWI',
            typeAhead       : true,
			hidden:true,
            triggerAction   : 'all',
            lazyRender      : true,
            mode            : 'local',
            emptyText: 'Daftar Perawat',
            fieldLabel:  '',
            align: 'Right',
            width: 150,
            store: dsDataPerawatPenindak_KASIR_RWI,
            valueField: 'KD_DOKTER',
            displayField: 'NAMA',
            listeners:
            {
                select : function(a, b){
                    tmpKdDokter = Ext.getCmp('cboDataPerawatPenindak_KASIR_RWI').getValue();
                    tmpNamaDokter = b.data.NAMA;
                }      
            }
        }
    );return cboDataPerawatPenindak_KASIR_RWI;
};

function mCombo_dokter_KASIRRWI()
{ 
    var Field = ['KD_DOKTER','NAMA'];

    dsDataDokterPenindak_KASIR_RWI = new WebApp.DataStore({ fields: Field });
    //loaddatastoreperawat();
    var cboDataDokter_KASIR_RWI = new Ext.form.ComboBox
    (
        {
            id: 'cboDataDokterPenindak_KASIR_RWI',
            typeAhead       : true,
            triggerAction   : 'all',
			hidden:true,
            lazyRender      : true,
            mode            : 'local',
            emptyText: 'Daftar Dokter',
            fieldLabel:  '',
            align: 'Right',
            width: 150,
            store: DataStorefirstGridStore,
            valueField: 'KD_DOKTER',
            displayField: 'NAMA',
            listeners:
            {
                select : function(a, b){
                    tmpKdDokter = Ext.getCmp('cboDataDokterPenindak_KASIR_RWI').getValue();
                    tmpNamaDokter = b.data.NAMA;
                }      
            }
        }
    );return cboDataDokter_KASIR_RWI;
};

/*
    FUNGSI TAMBAHAN UNTUK SAVE PASIEN PULANG
    OLEH    : HADAD AL GOJALI
    TANGGAL : 2017 - 02 - 20
    ALASAN  : BELUM ADA FUNGSI DATA SAVE PASIEN PULANG

 */
function savePulangPasien_KASIRRWI(mBol)
{
        Ext.Ajax.request
        ({
            url: baseURL + "index.php/rawat_inap/control_pindah_kamar/savePasienPulang",   
            params: dataParamsPulangPasien_KASIRRWI(),
            success: function (o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {   
                	tmpkd_unitKamar = cst.kd_unit;
                    ShowPesanInfoKasirrwi('Data berhasil di simpan', 'Simpan Data');
                    jPanel=0;
                } else
                {
                    ShowPesanWarningKasirrwi('Gagal menyimpan data !', 'Informasi');
                }
            },
            failure:function(o){
                ShowPesanErrorKasirrwi('Gagal menyimpan data ke SQL !', 'Informasi');
            }
        })
}


function dataParamsPulangPasien_KASIRRWI()
{
    var dataParamsPulangPasien =
    {
        kd_kasir        : '02',
        no_medrec       : kodepasien,
        no_transaksi    : notransaksi,
        tgl_boleh_plg   : Ext.getCmp('txtTanggalPulangPasien_KasirRwi').getValue(),
        jam_boleh_plg   : Ext.getCmp('txtJamPulangPasien_KasirRwi').getValue(),
    };
    return dataParamsPulangPasien

}


function PilihDokterLookUpKasir_RWI(edit) 
{
    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            header          : 'kd_component',
            dataIndex       : 'kd_component',
            width           : 80,
            menuDisabled    : true,
            hidden      : true
        },
        {
            header          : 'tgl_berlaku',
            dataIndex       : 'tgl_berlaku',
            width           : 80,
            menuDisabled    : true,
            hidden      : true
        },
        {
            header          : 'Komponent',
            dataIndex       : 'component',
            width           : 200,
            menuDisabled    : true,
            hidden      : false
        },
        {
            header          : 'kd_dokter',
            dataIndex       : 'kd_dokter',
            width           : 80,
            menuDisabled    : true,
            hidden      : true
        },{
            header          :'Nama Dokter',
            dataIndex       : 'nama',
            sortable        : false,
            hidden          : false,
            menuDisabled    : true,
            width           : 250,
            editor          : new Nci.form.Combobox.autoComplete({
                store   : dsgridpilihdokterpenindak_KasirRWI,
                select  : function(a,b,c){
                    var line    = GridDokterTr_RWI.getSelectionModel().selection.cell[0];
                    dsGridJasaDokterPenindak_RWI.getRange()[line].data.kd_dokter=b.data.kd_dokter;
                    dsGridJasaDokterPenindak_RWI.getRange()[line].data.nama=b.data.nama;
                    GridDokterTr_RWI.getView().refresh();
                },
                insert  : function(o){
                    return {
                        kd_dokter       : o.kd_dokter,
                        nama            : o.nama,
                        text            :  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_dokter+'</td><td width="200">'+o.nama+'</td></tr></table>'
                    }
                },
                param   : function(){
                    var o = grListTRKasirrwi.getSelectionModel().getSelections()[0].data;
                    var params={};
                    params['kd_unit'] = o.PARENTNYA;
                    params['penjas'] = 'rwi';
                    return params;
                },
                url     : baseURL + "index.php/main/functionRWJ/getdokterpenindak",
                valueField: 'nama_obat',
                displayField: 'text',
                listWidth: 380
            })
            //getTrDokter(dsTrDokter)
        },
        ]
    );
    
    var fldDetail = [];
    dsGridJasaDokterPenindak_RWI = new WebApp.DataStore({ fields: fldDetail });
    GridDokterTr_RWI= new Ext.grid.EditorGridPanel({
        id          : 'GridDokterTr_RWI',
        stripeRows  : true,
        width       : 487,
        height      : 160,
        store       : dsGridJasaDokterPenindak_RWI,
        border      : true,
        frame       : false,
        autoScroll  : true,
        cm          : GridTrDokterColumnModel,
        listeners   : {
            rowclick: function( $this, rowIndex, e )
            {
                //trcellCurrentTindakan_RWJ = rowIndex;
            },
            celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
                
            }
        },
        viewConfig  : {forceFit: true},
    });
    
    var lebar = 500;
    var FormLookUDokter_RWI = new Ext.Window
    (
        {
            id: 'winTRDokterPenindak_RWI',
            title: 'Pilih Dokter Penindak',
            closeAction: 'destroy',
            width: 500,
            height: 220,
            border: false,
            resizable: false,
            iconCls: 'Request',
            constrain : true,    
            modal: true,
            items: [ 
                GridDokterTr_RWI
            ],
            tbar :
            [
                {
                    xtype:'button',
                    text:'Simpan',
                    iconCls : 'save',
                    hideLabel:true,
                    id: 'BtnOktrDokter',
                    handler:function()
                    {
                        savetransaksi();
                    }
                },
                '-',
            ],
            listeners:
            {
                activate: function()
                {
                    /*  if (varBtnOkLookupEmp === true)
                    {
                        Ext.get('txtKdDokterIGD').dom.value   = rowSelectedLookdokter.data.KD_DOKTER;
                        Ext.get('txtNamaDokter_igd').dom.value = rowSelectedLookdokter.data.NAMA_DOKTER;
                        varBtnOkLookupEmp=false;
                    }; */
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
                    batal_isi_tindakan='y';
               
                }
            }
        }
    );
    FormLookUDokter_RWI.show();
    if(edit == true){
        GetgridEditDokterPenindakJasa_RWI()
    } else{
        GetgridPilihDokterPenindakJasa_RWI(currentJasaDokterKdProduk_KasirRWI,currentJasaDokterKdTarif_KasirRWI);
    }
    
};


function PilihDokterLookUpKasir_RWI_REVISI(edit) 
{    
    var FormLookUDokter_RWI = new Ext.Window
    (
        {
            xtype : 'fieldset',
            layout:'column',
            id: 'winTRDokterPenindak_RWI_REVISI',
            title: 'Pilih Dokter Penindak',
            closeAction: 'destroy',
            width: 720,
            height: 400,
            border: false,
            resizable: false,
            iconCls: 'Request',
            constrain : true,    
            modal: true,
            items: [ 
                {
                    border:false,
                    columnWidth:.45,
                    bodyStyle:'margin-top: 5px',
                    items:[
                        cboPenangan_KASIRRWI(),
                        seconGridPenerimaan(),
                    ]
                },
                {
                    border:false,
                    columnWidth:.1,
                    bodyStyle:'margin-top: 5px',
                },
                {
                    border:false,
                    columnWidth:.45,
                    bodyStyle:'margin-top: 5px; align:right;',
                    items:[
                        {
                            xtype   : 'textfield',
                            anchor  : '100%',
                            id      : 'txtSearchDokter_KASIRRWI',
                            name    : 'txtSearchDokter_KASIRRWI',
							listeners : {
								specialkey: function (){
									if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
										DataStorefirstGridStore.removeAll();
										tmpSearchDokter = Ext.getCmp('txtSearchDokter_KASIRRWI').getValue();
										/*tmpKdJob 		= Ext.getCmp('cboPenangan_KASIRRWI').getValue();
										if (tmpKdJob == 'Dokter') {
											tmpKdJob = 1;
										}else{
											tmpKdJob = 3;
										}*/
										loaddatastoredokter_REVISI(Ext.getCmp('txtSearchDokter_KASIRRWI').getValue());
										//loaddatastoredokter_REVISI();
									}
								},
							}
                        },
                        firstGridPenerimaan(),
                    ]
                }
            ],
            tbar :
            [
                {
                    xtype:'button',
                    text:'Simpan',
                    iconCls : 'save',
                    hideLabel:true,
                    id: 'BtnOktrDokter',
                    handler:function()
                    {
						//RefreshDataDetail_kasirrwi(tmpno_transaksi);
						
						//PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('DESKRIPSI')+" ("+tmpNamaDokter+")");
                    	FormLookUDokter_RWI.close();
                    }
                },
                '-',
            ],
            listeners:
            {
                activate: function()
                {
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
                    batal_isi_tindakan='y';
               
                }
            }
        }
    );
	FormLookUDokter_RWI.show();
};


	function loaddatastoredokter_REVISI(dokter){
    	DataStorefirstGridStore.removeAll();
		/*var tmpJenis_dokter;
		if (tmpKdJob == 3 || Ext.getCmp('checkPerawat_KASIRRWI').getValue() === true) {
			tmpJenis_dokter = 0;
		}else{
			tmpJenis_dokter = 1;
		}
		
		
		if (tmpKdJob === 4 || tmpKdJob === 3 && Ext.getCmp('checkPerawat_KASIRRWI').getValue() === true) {
			tmpJenis_dokter = 0;
			Ext.getCmp('checkPerawat_KASIRRWI').setValue(true);
		}else{
			tmpJenis_dokter = 1;
			Ext.getCmp('checkPerawat_KASIRRWI').setValue(false);
		}*/
		
		Ext.Ajax.request({
			url: baseURL +  "index.php/general/control_cmb_dokter/getCmbDokterSpesialisasi",
			params: {
                kd_unit         : tmpkd_unit,
                jenis_dokter    : tmpKdJob,
                kd_kasir        : '02',
                no_transaksi    : tmpno_transaksi,
                urut            : tmpRowGrid,
                tgl_transaksi   : tmptgl_transaksi,
                kd_job          : tmpKdJob,
                txtDokter       : dokter,
			},
			success: function(response) {
				var cst  = Ext.decode(response.responseText);
				for(var i=0,iLen=cst['data'].length; i<iLen; i++){
					var recs    = [],recType = dsDataDokterPenindak_KASIR_RWI.recordType;
					var o       = cst['data'][i];
					recs.push(new recType(o));
					DataStorefirstGridStore.add(recs);
				}
			}
		});
	}

	function loaddatastoredokterVisite_REVISI(){
		Ext.Ajax.request({
			url: baseURL +  "index.php/general/control_cmb_dokter/getCmbDokterVisite",
			params: {
				kd_kasir 		: '02',
				no_transaksi 	: tmpno_transaksi,
				tgl_transaksi 	: tmptgl_transaksi,
				kd_produk 		: tmpkd_produk,
                urut            : tmpRowGrid,
			},
			success: function(response) {
				var cst  = Ext.decode(response.responseText);
				for(var i=0,iLen=cst['data'].length; i<iLen; i++){
					var recs    = [],recType = dsDataDokterPenindak_KASIR_RWI.recordType;
					var o       = cst['data'][i];
					recs.push(new recType(o));
					DataStoreSecondGridStore.add(recs);
				}
			},
		});
	}


    function firstGridPenerimaan(){
    	DataStorefirstGridStore.removeAll();
        var cols = [
            { id : 'KD_DOKTER', header: "Kode Dokter", width: .25, sortable: true, dataIndex: 'KD_DOKTER',hidden : false},
            { id : 'NAMA', header: "Nama", width: .75, sortable: true, dataIndex: 'NAMA'}
        ];
        var firstGrid;
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : DataStorefirstGridStore,
			columns          : cols,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 250,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Dokter/ Perawat penangan',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()], 
                    colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNMKd_Dokter',
                                                    header: 'KD Dokter',
                                                    dataIndex: 'KD_DOKTER',
                                                    sortable: true,
                                                    hidden : false,
                                                    width: .25
                                            },
                                            {
                                                    id: 'colNMDokter',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA',
                                                    sortable: true,
                                                    width: .75
                                            }
                                    ]
                            ),
            listeners : {
                afterrender : function(comp) {
						var firstGridDropTargetEl =  firstGrid.getView().scroller.dom;
						var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroup',
							notifyDrop : function(ddSource, e, data){
									var records   =  ddSource.dragData.selections;
									var kd_dokter = "";
									for(var i=0,iLen=records.length; i<iLen; i++){
										var o       = records[i];
										kd_dokter = o.data.KD_DOKTER;
									}
									Ext.Ajax.request({
										url: baseURL +  "index.php/rawat_inap/control_visite_dokter/deleteDokter",
										params: {
											kd_kasir 		: '02',
											no_transaksi 	: tmpno_transaksi,
											urut 			: tmpRowGrid,
											tgl_transaksi 	: tmptgl_transaksi,
											kd_dokter 		: kd_dokter,
											kd_produk 		: tmpkd_produk,
											kd_unit 		: tmpkd_unit,
											line 			: data.selections[0].data.LINE,
										},
										success: function(response) {
											var cst  = Ext.decode(response.responseText);
											PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
											PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('JUMLAH_DOKTER', cst.JUMLAH_DOKTER);
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											firstGrid.store.add(records);
											firstGrid.getView().refresh();
											return true
										},
									});
								}
						});

                },
				rowdblclick: function(dataview, index, item, e) {
					var group_dokter = 0;
					if (tmp_group_dokter!= 0 || tmp_group_dokter != '' || tmp_group_dokter != 'undefined' || tmp_group_dokter != null) {
						group_dokter = cellSelecteddeskripsi_kasir_rwi.data.GROUP;
					}
					Ext.Ajax.request({
						url: baseURL +  "index.php/rawat_inap/control_visite_dokter/insertDokter",
						params: {
							label 			: Ext.getCmp("cboPenangan_KASIRRWI").getValue(),
							kd_job          : tmpKdJob,
							no_transaksi 	: tmpno_transaksi,
							tgl_transaksi 	: tmptgl_transaksi,
							kd_produk 		: tmpkd_produk,
							urut 			: tmpRowGrid,
							kd_kasir 		: '02',
							kd_unit 		: tmpkd_unit,
							kd_dokter 		: DataStorefirstGridStore.data.items[index].data.KD_DOKTER,
							kd_tarif 		: tmpkd_tarif,
							tgl_berlaku 	: PenataJasaKasirRWI.TGL_BERLAKU,
							group 			: group_dokter,
						},
						success: function(response) {
							DataStoreSecondGridStore.removeAll();
							loaddatastoredokterVisite_REVISI();
							//loaddatastoredokter_REVISI(Ext.getCmp('txtSearchDokter_KASIRRWI'),getValue());
							//
							//var selection = firstGrid.getView().getSelectionModel().getSelection()[0];
							// RefreshRekapitulasiTindakanKasirRWI(notransaksi, kdkasirnya);
							var cst  = Ext.decode(response.responseText);
							PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.data.DESKRIPSI+" ("+cst.data.DAFTAR_DOKTER+")");
							PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('JUMLAH_DOKTER', cst.data.JUMLAH_DOKTER);
							/*Ext.each(index, firstGrid.store.remove, firstGrid.store);
							firstGrid.store.remove(index);
							firstGrid.getView().refresh();*/
							var selected = firstGrid.getSelectionModel().getSelections();
							
							if(selected.length>0) {
								for(var i    =0;i<selected.length;i++) {
									DataStorefirstGridStore.remove(selected[i]);
								}
							}
						},
					});
				}
            },
			viewConfig: 
			{
				forceFit: true,
			}
        });
        return firstGrid;
    };

function seconGridPenerimaan(){
		DataStoreSecondGridStore.removeAll();
		var secondGrid;
		var fields = [
			{name: 'KD_UNIT', mapping : 'KD_UNIT'},
			{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
		];
		secondGridStoreLapPenerimaan = new Ext.data.JsonStore({
			fields : fieldsDokterPenindak,
			root   : 'data'
		});
		var cols = [
			{ id : 'KD_DOKTER', header: "Kode Dokter", width: .25, sortable: true, dataIndex: 'KD_DOKTER',hidden : false},
			{ header: "Nama", width: .75, sortable: true, dataIndex: 'NAMA'},
			{ header: "JP", width: .25, dataIndex: 'JP'},
			{ header: "PRC", width: .25, dataIndex: 'PRC'},
		];
			secondGrid = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroup',
					store            : DataStoreSecondGridStore,
					columns          : cols,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 250,
					stripeRows       : true,
					autoExpandColumn : 'KD_DOKTER',
					title            : 'Dokter/ Perawat yang menangani',
					plugins          : [new Ext.ux.grid.FilterRow()],
					listeners : {
						afterrender : function(comp) {

							var group_dokter = 0;
							if (tmp_group_dokter!= 0 || tmp_group_dokter != '' || tmp_group_dokter != 'undefined' || tmp_group_dokter != null) {
								group_dokter = cellSelecteddeskripsi_kasir_rwi.data.GROUP;
							}

							var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
								ddGroup    : 'secondGridDDGroup',
								notifyDrop : function(ddSource, e, data){
									var records   =  ddSource.dragData.selections;
									var kd_dokter = "";
									for(var i=0,iLen=records.length; i<iLen; i++){
										var o       = records[i];
										kd_dokter = o.data.KD_DOKTER;
									}
									Ext.Ajax.request({
										url: baseURL +  "index.php/rawat_inap/control_visite_dokter/insertDokter",
										params: {
											label 			: Ext.getCmp("cboPenangan_KASIRRWI").getValue(),
                                            kd_job          : tmpKdJob,
											no_transaksi 	: tmpno_transaksi,
											tgl_transaksi 	: tmptgl_transaksi,
											kd_produk 		: tmpkd_produk,
											urut 			: tmpRowGrid,
											kd_kasir 		: '02',
											kd_unit 		: tmpkd_unit,
											kd_dokter 		: kd_dokter,
											kd_tarif 		: tmpkd_tarif,
											tgl_berlaku 	: PenataJasaKasirRWI.TGL_BERLAKU,
											group 			: group_dokter,
										},
										success: function(response) {
											DataStoreSecondGridStore.removeAll();
											loaddatastoredokterVisite_REVISI();
											// RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
											var cst  = Ext.decode(response.responseText);
											PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.data.DESKRIPSI+" ("+cst.data.DAFTAR_DOKTER+")");
											PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('JUMLAH_DOKTER', cst.data.JUMLAH_DOKTER);
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											//secondGrid.store.add(records);
											secondGrid.getView().refresh();
											return true
										},
									});
								}
							});
						},
					rowdblclick: function(dataview, index, item, e) {
						//DataStorefirstGridStore.data.items[index].data.KD_DOKTER
						Ext.Ajax.request({
							url: baseURL +  "index.php/rawat_inap/control_visite_dokter/deleteDokter",
							params: {
								kd_kasir 		: '02',
								no_transaksi 	: tmpno_transaksi,
								urut 			: tmpRowGrid,
								tgl_transaksi 	: tmptgl_transaksi,
								kd_dokter 		: DataStoreSecondGridStore.data.items[index].data.KD_DOKTER,
								kd_produk 		: tmpkd_produk,
								kd_unit 		: tmpkd_unit,
								line 			: dataview.selModel.selections.items[0].data.LINE,
							},
							success: function(response) {
								var cst  = Ext.decode(response.responseText);
								PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
								PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('JUMLAH_DOKTER', cst.JUMLAH_DOKTER);
								DataStoreSecondGridStore.removeAll();
								loaddatastoredokterVisite_REVISI();

							},
						});
					},
					callback : function(){
						Ext.getCmp('txtSearchDokter_KASIRRWI').focus();
					}
			},
			viewConfig: 
			{
				forceFit: true,
			}
		});
		return secondGrid;
	};
	function loaddatastorePenangan(){
		Ext.Ajax.request({
			url: baseURL +  "index.php/general/control_cmb_dokter/getCmbDokterInapInt",
			params: {
				groups : cellSelecteddeskripsi_kasir_rwi.data.GROUP,
			},
			success: function(response) {
				var cst  = Ext.decode(response.responseText);
				for(var i=0,iLen=cst['data'].length; i<iLen; i++){
					var recs    = [],recType = dsDataDaftarPenangan_KASIR_RWI.recordType;
					var o       = cst['data'][i];
					recs.push(new recType(o));
					dsDataDaftarPenangan_KASIR_RWI.add(recs);
				}
			},
			callback : function(){
				Ext.getCmp('txtSearchDokter_KASIRRWI').focus();
			}
		});
	}

    function cboPenangan_KASIRRWI(){
		var tmpValue;
		/*if (tmpKdJob == 3) {
			tmpValue = "Perawat";
		}else{
			tmpValue = "Dokter";
		}*/

		var Field = ['KD_JOB','KD_COMPONENT','LABEL'];
		
		dsDataDaftarPenangan_KASIR_RWI = new WebApp.DataStore({ fields: Field });
		loaddatastorePenangan();
		var cboPenangan_KASIRRWI       = new Ext.form.ComboBox
		({
			id: 'cboPenangan_KASIRRWI',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus: true,
			forceSelection: true,
			emptyText: 'Pilih penangan...',
			fieldLabel: 'Penangan ',
			align: 'Right',
			store: dsDataDaftarPenangan_KASIR_RWI,
			valueField: 'KD_JOB',
			displayField: 'LABEL',
			//anchor           : '100%',
			value           : tmpValue,
			listeners:
			{
                select : function(a, b, c){
                    DataStorefirstGridStore.removeAll();
                    tmpKdJob =  b.data.KD_JOB;
                    loaddatastoredokter_REVISI();
                }
			}
		})

        return cboPenangan_KASIRRWI;
    }
/* 
	CREATE 	: MSD
	TGL 	: 13-03-2017
	KET		: FITUR DEPOSIT

 */
	
function fnDlgRWIDeposit(data)
{
    winRWIDeposit = new Ext.Window
    (
        {
            id: 'winRWIDeposit',
            title: 'Deposit',
            closeAction: 'destroy',
            width:420,
            height: 370,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'cost',
            modal: true,
            items: [ItemDlgRWIDeposit()],
			fbar:[
				{
					id: 'btnOkLookupDepositKasirrwi',
					text: 'OK',
					handler: function (sm, row, rec)
					{
						saveDepositKasirRWI();
					}
				},
				{
					id: 'btnCancelLookupDepositKasirrwi',
					text: 'Batal',
					handler: function (sm, row, rec)
					{
						winRWIDeposit.close();
					}
				},
				{
					id: 'btnPrintLookupDepositKasirrwi',
					text: 'Print Kwitansi',
					iconCls: 'print',
					handler: function (sm, row, rec)
					{
                        //3 12 W 1 N
                        // 2 September 2020
                        //===============================
                        //kirim total deposit
                        var tot_deposit= Ext.getCmp('txtJumlahDepositKasirRWI').getValue() ;


                        LookUpEditKwitansiPanel( tot_deposit);
                        //===============================
                        
					}
				}
			
			]

        }
    );
    winRWIDeposit.show();
	
	selectDataDepositKasirRWI(data);
};

function ItemDlgRWIDeposit(){
 var items = new Ext.Panel({
		bodyStyle:'padding: 0px 0px 5px 5px',
		layout:'form',
		border:false,
		autoScroll: true,
		items:[
			{
				xtype : 'fieldset',
				title : 'Informasi Pembayaran',
				layout: 'absolute',
				width : 395,
				height : 55,
				border: true,
				items:[
					{
						x: 0,
						y: 0,
						xtype: 'label',
						text: 'Tanggal '
					}, {
						x: 70,
						y: 0,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 80,
						y: 0,
						xtype: 'datefield',
						id: 'dtpTglDepositKasirRWI',
						format: 'd/M/Y',
						value: now,
						width: 110,
						readOnly:true,
					}, 
					{
						x: 230,
						y: 0,
						xtype: 'label',
						text: 'No. Bukti '
					}, {
						x: 280,
						y: 0,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 290,
						y: 0,
						xtype: 'textfield',
						id: 'txtNoBuktiDepositKasirRWI',
						width: 83,
						readOnly:true,
						
					}, 
				]
			},
			{
				xtype : 'fieldset',
				title : 'Informasi Pembayar',
				layout: 'absolute',
				width : 395,
				height : 55,
				border: true,
				items:[
					{
						x: 0,
						y: 0,
						xtype: 'label',
						text: 'Nama '
					}, {
						x: 70,
						y: 0,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 80,
						y: 0,
						xtype: 'textfield',
						id: 'txtNamaPembayarDepositKasirRWI',
						width: 293,
						readOnly:true,
						
					}, 
				]
			},
			{
				xtype : 'fieldset',
				title : 'Detail Pembayaran',
				layout: 'absolute',
				width : 395,
				height : 150,
				border: true,
				items:[
					{
						x: 0,
						y: 0,
						xtype: 'label',
						text: 'Untuk '
					}, {
						x: 70,
						y: 0,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 80,
						y: 0,
						xtype: 'textfield',
						id: 'txtKetUntukDepositKasirRWI',
						width: 293,
						
					}, 
					{
						x: 0,
						y: 30,
						xtype: 'label',
						text: 'Pasien '
					}, {
						x: 70,
						y: 30,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 80,
						y: 30,
						xtype: 'textfield',
						id: 'txtKdPasienDepositKasirRWI',
						width: 80,
						readOnly:true,
					}, 
					{
						x: 165,
						y: 30,
						xtype: 'textfield',
						id: 'txtNamaPasienDepositKasirRWI',
						width: 208,
						readOnly:true,
					}, 
					{
						x: 0,
						y: 60,
						xtype: 'label',
						text: 'Kelas/Kamar '
					}, {
						x: 70,
						y: 60,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 80,
						y: 60,
						xtype: 'textfield',
						id: 'txtKelasDepositKasirRWI',
						width: 80,
						readOnly:true,
					}, 
					{
						x: 165,
						y: 60,
						xtype: 'textfield',
						id: 'txtKamarDepositKasirRWI',
						width: 208,
						readOnly:true,
					}, 
					{
						x: 0,
						y: 90,
						xtype: 'label',
						text: 'Jumlah '
					}, {
						x: 70,
						y: 90,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 80,
						y: 90,
						xtype: 'numberfield',
						id: 'txtJumlahDepositKasirRWI',
						width: 170,
						style: {'text-align': 'right','font-weight':'bold'},
						listeners:
						{
							'specialkey': function ()
							{
								if (Ext.EventObject.getKey() === 13)
								{
									saveDepositKasirRWI();
								}
							}
						}
					}, 
				]
			}
		]
	})
	return items;
}

function selectDataDepositKasirRWI(data){
	var nobukti='';
	nobukti = data.KD_KASIR + '-' + data.NO_TRANSAKSI;
	Ext.getCmp('txtNoBuktiDepositKasirRWI').setValue(nobukti);
	Ext.getCmp('txtNamaPembayarDepositKasirRWI').setValue(data.NAMA);
	Ext.getCmp('txtKdPasienDepositKasirRWI').setValue(data.KD_PASIEN);
	Ext.getCmp('txtNamaPasienDepositKasirRWI').setValue(data.NAMA);
	Ext.getCmp('txtKelasDepositKasirRWI').setValue(data.NAMA_UNIT);
	Ext.getCmp('txtKamarDepositKasirRWI').setValue(data.NAMA_KAMAR);
	getKeteranganDepositKasirRWI();
}
function getKeteranganDepositKasirRWI(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWI/getKeteranganDeposit",
		params: {text:''},
		failure: function (o)
		{
			ShowPesanErrorKasirrwi('Error membaca keterangan deposit. Hubungi Admin!', 'WARNING');
		},
		success: function (o)
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				Ext.getCmp('txtKetUntukDepositKasirRWI').setValue(cst.ket);
			} else{
				ShowPesanWarningKasirrwi('Keterangan deposit belum di setting!', 'WARNING');
			}
		}
	})
}

function saveDepositKasirRWI(){
	if(Ext.getCmp('txtJumlahDepositKasirRWI').getValue() == '' || Ext.getCmp('txtJumlahDepositKasirRWI').getValue()== 0){
		ShowPesanWarningKasirrwi('Jumlah deposit harus di isi!','WARNING');
	} else{
		Ext.Ajax.request
		({
			url: baseURL + "index.php/main/functionRWI/savedeposit",
			params: {
				no_transaksi:currentNoTransaksi_KasirRWI,
				kd_kasir:currentKdKasir_KasirRWI,
				tgl_bayar:Ext.getCmp('dtpTglDepositKasirRWI').getValue(),
				jumlah:Ext.getCmp('txtJumlahDepositKasirRWI').getValue(),
				kd_unit:kodeunit,
				//jumlah:Ext.getCmp('txtJumlahDepositKasirRWI').getValue(),
				
			},
			failure: function (o)
			{
				ShowPesanErrorKasirrwi('Error menyimpan deposit. Hubungi Admin!', 'WARNING');
			},
			success: function (o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					RefreshDatahistoribayar(currentNoTransaksi_KasirRWI);
					ShowPesanInfoKasirrwi('Deposit berhasil disimpan.','Information');
                    //winRWIDeposit.close();
                    
				} else{
					ShowPesanErrorKasirrwi('Deposit gagal disimpan!', 'WARNING');
				}
			}
		})
	}
}

function getTabPanelPembayaranSummaryDetailKasirRWI(rowdata){
    var items ={
		xtype:'tabpanel',
		activeTab: 1,
		defferedRender: true,
		defaults:{
            autoScroll: true
		},
		items:[
			DataPanelPembayaranSummryKasirRWI(),
			DataPanelPembayaranDetailKasirRWI()
		],
		tbar:[
			{
				text: 'Simpan',
				id: 'btnSimpanKasirrwi',
				tooltip: nmSimpan,
				iconCls: 'save',
				handler: function (){
					//if (statusHitung == false) {
						// hitungtotaltagihankasirrwi();
					//}
					Datasave_KasirrwiKasir(false);
					Ext.getCmp('btnEditKasirrwi').disable();
					Ext.getCmp('btnLookUpProduk_RawatInap').disable();
					Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
					Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').disable();
				}
			},' ','-',
						/* INI BUAT INFO SHIFT */
					{
						xtype: 'displayfield',				
						width: 50,								
						value: 'Shift Aktif',
						fieldLabel: 'Label',				
					},{
						xtype: 'displayfield',				
						width: 10,								
						value: ':',
						fieldLabel: 'Label',				
					},{
						xtype: 'displayfield',	
						id:'txtShiftAktif',
						width: 50,								
						fieldLabel: 'Label',				
					}
            //hani 2023-02-28
            ,' ','-',
                /* INI BUAT Hitung Selisih */
                {
                    text: 'Selisih',
                    id: 'btnSelisih_KASIRRWI',
                    tooltip: 'Hitung Selisih',
                    //iconCls: 'save',
                    handler: function ()
                    {
                        console.log('cekSelisih');
                        SelisihLookUp_KasirRWI(rowdata);
                    },
                }
            //================================
			/*
			Programmer : HDHT
			Bandung, 09-maret-2017
			alasan :
				tidak terpakai karena setting printer sudah berdasarkan masing-masing user

			mCombo_printer_kasirrwi(),
			*/
			// {
			// 	xtype: 'splitbutton',
			// 	text: 'Cetak',
			// 	iconCls: 'print',
			// 	id: 'btnPrint_viDaftarRwi',
			// 	handler: function ()
			// 	{
			// 	},
			// 	menu: new Ext.menu.Menu({
			// 	items: [
			// 		{
			// 			xtype: 'button',
			// 			text: 'Print Bill',
			// 			hidden: true,
			// 			id: 'btnPrintBillRwi',
			// 			handler: function ()
			// 			{
			// 				printbillRwi();
			// 			}
			// 		},
					
			// 		{
			// 			xtype: 'button',
			// 			text: 'Print Bill',
			// 			id: 'btnPrintBillPreviewRwi',
			// 			handler: function ()
			// 			{
			// 				printbillRwiPreview();
			// 			}
			// 		},
			// 		{
			// 			xtype: 'button',
			// 			text: 'Print Kwitansi',
			// 			id: 'btnPrintKwitansiRwi',
			// 			handler: function ()
			// 			{
			// 				// if (Ext.getCmp('cbopasienorder_printer_kasirrwi').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirrwi').getValue()===undefined)
			// 				// 					{
			// 				// 						ShowPesanWarningKasirrwi('Pilih printer terlebih dahulu !', 'Cetak Data');
			// 				// 					}else
			// 				// 					{
			// 				//                      printkwitansirwi();
			// 				// 					}
			// 				printkwitansirwi();
							
			// 			}
			// 		},
			// 	]
			// 	})
			// },
		
		]
	};
    return items;
}

//hani 2023-02-28
function SelisihLookUp_KasirRWI(rowdata) 
{
    console.log(rowdata);
    kdKelasSelisih = rowdata.KD_KELAS;
    kdUnitSelisih = rowdata.KD_UNIT;
    RefreshDataKasirrwiSelisihDetail(rowdata.NO_TRANSAKSI);
    var lebar = 700;
    FormLookUpsSelisihKasirrwi = new Ext.Window({
		id: 'gridKasirrwiBayarPRSH',
		title: 'Pembayaran Pasien Perusahaan',
		closeAction: 'destroy',
		width: lebar,
		height: 450,
		border: false,
		layout:'fit',
		resizable: false,
		iconCls: 'Edit_Tr',
		modal: true,
		items: getFormSelisihKasirrwi(lebar,rowdata),
	});
    FormLookUpsSelisihKasirrwi.show();

};

function getFormSelisihKasirrwi(lebar,rowdata){
    var pnlSelisihKasirrwi = new Ext.FormPanel({
		id: 'PanelSelisihKasirrwi',
		fileUpload: true,
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:4px;',
		style: 'padding:4px;',
		height: 90,
		border: true,
		items: [
			getItemPanelPerusahaanrwiKasir(rowdata), 
			getItemPanelPerusahaan2rwiKasir(rowdata)
		]
	});
    var x;
    var pnlSelisihKasirrwi2 = new Ext.FormPanel({
		id: 'PanelSelisihKasirrwi2',
		fileUpload: true,
		layout: 'fit',
		flex:1,
		bodyStyle: 'padding:0px 4px 4px 4px;',
		border: false,
		items: [getTabPanelSelisihSummaryDetailKasirRWI()]
	});
    var FormDepanSelisihKasirrwi = new Ext.Panel({
			id: 'FormDepanSelisihKasirrwi',
			layout: {
				type:'vbox',
				align:'stretch'
			},
			bodyStyle: 'padding:4px;',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			items: [pnlSelisihKasirrwi, pnlSelisihKasirrwi2]
		}
	);
    return FormDepanSelisihKasirrwi
}

function getItemPanelPerusahaanrwiKasir(rowdata){
    var items = {
		layout: 'column',
		border: false,
		items:[
			{
				columnWidth: .98,
				layout: 'form',
				labelWidth: 100,
				border: false,
				items:[
					{
						xtype: 'textfield',
						fieldLabel: 'Perusahaan ',
						name: 'txtPerusahaanKasirrwiSelisih',
						id: 'txtPerusahaanKasirrwiSelisih',
						value: rowdata.CUSTOMER +' | '+ rowdata.KD_CUSTOMER,
						readOnly: true,
						anchor: '99%'
					}
				]
			}
		]
	};
    return items;
}

function getItemPanelPerusahaan2rwiKasir(rowdata){
    var items = {
		layout: 'column',
		border: false,
		items:[
			{
				columnWidth: .30,
				layout: 'form',
				labelWidth: 100,
				border: false,
				items:[
					{
						xtype: 'datefield',
						fieldLabel: 'Tgl Valid ',
						id: 'dtpTglValidSelisih',
						name: 'dtpTglValidSelisih',
						format: 'd/M/Y',
						readOnly: true,
						value: rowdata.TANGGAL_MASUK,
						anchor: '100%'
					}
				]
			},
            {
				columnWidth: .70,
				layout: 'form',
				border: false,
                style:'margin-left:18px;',
				labelWidth: 140,
				items:[
                    mComboKelasSelisih(rowdata),
                    mComboRuanganSelisih(rowdata)
				]
			}
		]
	};
    return items;
}

function mComboKelasSelisih(data)
{
    var Field = ['KD_KELAS', 'KELAS'];
    dsComboKelasSelisih = new WebApp.DataStore({fields: Field});
    dsComboKelasSelisih.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kelas',
            Sortdir: 'ASC',
            target: 'ViewSetupKelasSelisih',
            param: 'kd_spesial ='+data.KD_SPESIAL
        }
    })
    var cboKelasKasirRWISelisih = new Ext.form.ComboBox
    (
        {
            id: 'cboKelasKasirRWISelisih',
            typeAhead: true,
            fieldLabel: 'Jatah Kelas Perawatan ',
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus: true,
            forceSelection: true,
            labelWidth: 'auto',
            align: 'Right',
            store: dsComboKelasSelisih,
            value:data.KELAS,
            valueField: 'KD_KELAS',
            displayField: 'KELAS',
            anchor: '60%',
            listeners:
            {
                'select': function (a, b, c)
                {
                     console.log(b.data);
                    //  getJatahSelisih(b.data.KD_KELAS,data)
                    // Ext.getCmp('cboRuanganSelisih').show();
                    kdKelasSelisih = b.data.KD_KELAS;
                    loaddatastoreruangan(kdKelasSelisih,data)
                },
            }
        }
    );
    return cboKelasKasirRWISelisih;
}

function loaddatastoreruangan(KD_KELAS,data)
{
    dsRuanganSelisih.load
    (
        {
        params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'kd_unit',
                Sortdir: 'ASC',
                target: 'ViewComboRuangan',
                param: 'where kd_spesial =~' + data.KD_SPESIAL + '~' + ' And ' + 'k.kd_kelas =~' + KD_KELAS + '~'
            }
        }
    )

}

function mComboRuanganSelisih(data)
{
    var Field = ['KD_UNIT', 'NAMA'];
    dsRuanganSelisih = new WebApp.DataStore({fields: Field});
    var cboRuanganSelisih = new Ext.form.ComboBox
    ({
        id: 'cboRuanganSelisih',
        // hidden:true,
        typeAhead: true,
        triggerAction: 'all',
        enableKeyEvents:true,
        mode: 'local',
        selectOnFocus: true,
        forceSelection: true,
        emptyText: 'Pilih Ruangan...',
        fieldLabel: 'Ruang / Unit ',
        align: 'Right',
        store: dsRuanganSelisih,
        value:data.NAMA_UNIT,
        valueField: 'KD_UNIT',
        displayField: 'NAMA',
        anchor: '70%',
        listeners:
        {
            'select': function (a, b, c)
            {
                console.log(b.data);
                kdUnitSelisih = b.data.KD_UNIT;
                getJatahSelisih(kdUnitSelisih,data)
            },
        }
    });

    return cboRuanganSelisih;
};

function getJatahSelisih(kd_unit,data){
    console.log(data);
    console.log(kd_unit);
    Ext.Ajax.request
    ({
        url: baseURL + "index.php/main/functionRWI/getTarifSelisih",
        params: getParamsKelasJatah(kd_unit,data),
        failure: function (o)
        {
            ShowPesanErrorKasirrwi('Error Mendapatkan Jatah Kelas. Hubungi Admin!', 'WARNING');
        },
        success: function (o)
        {
            var cst = Ext.decode(o.responseText);
            console.log(cst);
            if (cst.success === true)
            {
                console.log(dsSelisihDetailKasirrwiKasirList.data);
                for (var i = 0; i < cst.listData.length; i++) {
                    var o = dsSelisihDetailKasirrwiKasirList.data.items[i].data;
                    var d = cst.listData[i];
                    console.log(cst);
                    console.log(i);
                    console.log(o);
                    console.log(d);
                    if(o.KD_PRODUK == d.kd_produk && o.URUT == d.urut){
                        dsSelisihDetailKasirrwiKasirList.getRange()[i].set('JATAH', d.tarif);
                        dsSelisihDetailKasirrwiKasirList.getRange()[i].set('SELISIH', o.HARGA - d.tarif);
                        dsSelisihDetailKasirrwiKasirList.getRange()[i].set('KD_TARIF', d.kd_tarif);
                        dsSelisihDetailKasirrwiKasirList.getRange()[i].set('TGL_BERLAKU', d.tgl_berlaku);
                    }
                }
                totalSelisih();
            } else{
                ShowPesanErrorKasirrwi('Gagal Mendapatkan Jatah Kelas!', 'WARNING');
            }
        }
    })
}

function getParamsKelasJatah(kd_unit,data){
    var params = {
            no_transaksi:currentNoTransaksi_KasirRWI,
            kd_kasir:currentKdKasir_KasirRWI,
            kd_customer:data.KD_CUSTOMER,
            kd_spesial:data.KD_SPESIAL,
            kd_unit:kd_unit,
            // kd_kelas:kd_kelas
    };
    // params['jumlah'] = dsSelisihDetailKasirrwiKasirList.getCount();
        // var line = 0;
        // console.log(dsSelisihDetailKasirrwiKasirList.data);
        // for (var i = 0; i < dsSelisihDetailKasirrwiKasirList.getCount(); i++) {
        //     var o = dsSelisihDetailKasirrwiKasirList.data.items[i].data;
        // }
    return params;
}

function getTabPanelSelisihSummaryDetailKasirRWI(){
    var items ={
		xtype:'tabpanel',
		activeTab: 0,
		defferedRender: true,
		defaults:{
            autoScroll: true
		},
		items:[
			DataPanelSelisihDetailKasirRWI()
		],
		tbar:[
            // {
			// 	text: 'Hitung',
			// 	id: 'btnHitungSelisihKasirrwi',
			// 	tooltip: nmSimpan,
			// 	iconCls: 'save',
			// 	handler: function (){
			// 	}
			// },' ','-',
			{
				text: 'Bayar',
				id: 'btnBayarSelisihKasirrwi',
				tooltip: nmSimpan,
				iconCls: 'save',
				handler: function (){
                    var items = dsSelisihDetailKasirrwiKasirList.data.items;
                    const totalSelisih = items.reduce((total,item) => total + item.data.SELISIH,0);
                    console.log(totalSelisih);
                    if(totalSelisih > 0){
                        Datasave_KasirrwiKasirSelisih(false);
                        Ext.getCmp('btnEditKasirrwi').disable();
                        Ext.getCmp('btnLookUpProduk_RawatInap').disable();
                        Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
                        Ext.getCmp('btnHapusBaris_PenataJasaKasirRWI').disable();
                    }else{
                        ShowPesanWarningKasirrwi('Selisih Tidak Ada','Warning');
                        // Datasave_KasirrwiKasir(false);
                    }
				}
			},' ','-',
            {
                text: 'Batal',
                id: 'btnBatalSelisihKasirrwi',
                tooltip: 'cancel',
                //iconCls: 'save',
                handler: function ()
                {
                    FormLookUpsSelisihKasirrwi.close();
                },
            }
		]
	};
    return items;
}

function DataPanelSelisihDetailKasirRWI(){
	var FormPanelSelisihDetailKasirRWI = new Ext.Panel({
		id: 'FormPanelSelisihDetailKasirRWI',
		layout: {
			type:'vbox',
			align:'stretch'
		},
		title: 'Detail Selisih',
		border: false,
		items: [ 
			GetDTLSelisihKasirrwiGrid(), 
		]
	});
    return FormPanelSelisihDetailKasirRWI;
}

function GetDTLSelisihKasirrwiGrid(){
    // var fldDetail = ['TGL_TRANSAKSI','REFF','KELAS','URAIAN','QTY','BIAYA','JUMLAH','SELISIH'];
    // dsSelisihDetailKasirrwiKasirList = new WebApp.DataStore({fields: fldDetail})
    gridDTLSelisihKasirrwinap = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id: 'gridDTLSelisihKasirrwinap',
		store: dsSelisihDetailKasirrwiKasirList,
		border: true,
		flex:1,
		style:'padding: 4px;',
		columnLines: true,
		autoScroll: true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function (sm, row, rec) {
                    console.log(rec);
					cell_select_selisih = rec;
                    Row_viKasirSelisih = row;
                    // Row_viKasirSelisih = dsSelisihDetailKasirrwiKasirList.getAt(row);
                    console.log(Row_viKasirSelisih);
				}
			}
		}),
        listeners: {
			beforeedit: function (plugin, edit) {
			},
			rowdblclick: function (sm, ridx, cidx) {
				console.log(cell_select_selisih); 
			}
		},
		cm: TRKasirSelisihColumModel(),
		viewConfig: {
			forceFit: true
		},
		fbar:[
            {
				xtype: 'textfield',
				id: 'txtTotalBiayaSelisihKasirRWI',
                name: 'txtTotalBiayaSelisihKasirRWI',
				width: 82,
				readOnly: true,
			},{
				xtype: 'textfield',
				id: 'txtTotalJumlahSelisihKasirRWI',
				name: 'txtTotalJumlahSelisihKasirRWI',
				width: 82,
				readOnly: true,
			},{
				xtype: 'textfield',
				id: 'txtTotalSelisihKasirRWI',
				name: 'txtTotalSelisihKasirRWI',
				width: 82,
				readOnly: true,
			},
		]		
	});
    return gridDTLSelisihKasirrwinap;
}
function TRKasirSelisihColumModel()
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coltglSelisihKasirrwi',
                header: 'Tanggal',
                dataIndex: 'TGL_TRANSAKSI',
                width: 75,
                menuDisabled: true,
                hidden: false,
                renderer: function (v, params, record)
                {

                    return ShowDate(record.data.TGL_TRANSAKSI);
                }
            },
            {
                id: 'colRefSelisihKasirrwi',
                header: 'Reff',
                dataIndex: 'FAKTUR',
                width: 70,
                menuDisabled: true,
                hidden: false
            },
            {
                id: 'colKelasSelisihKasirrwi',
                header: 'Kelas',
                dataIndex: 'KELAS',
                sortable: false,
                hidden: false,
                menuDisabled: true,
                width: 70

            },
            {
                id: 'colUraianSelisihKasirrwi',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden: false,
                menuDisabled: true,
                width: 250

            },
            {
                id: 'colQtySelisihKasirrwi',
                header: 'Qty',
                width: 50,
                menuDisabled: true,
                dataIndex: 'QTY',
            },
            {
                id: 'colBiayaSelisihKasirrwi',
                header: 'Biaya',
                align: 'right',
                hidden: false,
                menuDisabled: true,
                dataIndex: 'HARGA',
                width: 80,
                renderer: function (v, params, record)
                {
                    return formatCurrency(record.data.HARGA);

                }
            },
            {
                id: 'colJumlahSelisihKasirrwi',
                header: 'Jatah',
                width: 80,
                dataIndex: 'JATAH',
                align: 'right',
                editor: new Ext.form.TextField
                (
                    {
                        id: 'fieldcolJumlahrwiSelisih',
                        allowBlank: true,
                        enableKeyEvents: true,
                        width: 30,
						listeners : {
							specialkey : function(a, b, c){
                                console.log(a);
                                console.log(b);
                                console.log(c);
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
                                    console.log(dsSelisihDetailKasirrwiKasirList);
                                    console.log(dsSelisihDetailKasirrwiKasirList.length);
                                    console.log(Row_viKasirSelisih);
                                    var i = Row_viKasirSelisih;
                                    var o = dsSelisihDetailKasirrwiKasirList.data.items[i].data;
                                    dsSelisihDetailKasirrwiKasirList.getRange()[i].set('JATAH', a.value);
                                    dsSelisihDetailKasirrwiKasirList.getRange()[i].set('SELISIH', o.HARGA - a.value);
                                    totalSelisih();
                                }
							}
						},
                    }
                ),
                renderer: function (v, params, record)
                {
                    return formatCurrency(record.data.JATAH);
                }
            },
            {
                id: 'colSelisihKasirrwi',
                header: 'Selisih',
                width: 80,
                dataIndex: 'SELISIH',
                align: 'right',
                hidden: false,
                editor: new Ext.form.TextField
                (
                        {
                            id: 'fieldcolSelisihrwiSelisih',
                            allowBlank: true,
                            enableKeyEvents: true,
                            width: 30,
                        }
                ),
                renderer: function (v, params, record)
                {
                    // getTotalDetailProdukRWI();
                    return formatCurrency(record.data.SELISIH);
                }
            },
        ]
    )
}

function RefreshDataKasirrwiSelisihDetail(no_transaksi)
{
    var strKriteriaKasirrwi = '';

    strKriteriaKasirrwi = 'd.no_transaksi = ~' + no_transaksi + '~ and d.kd_kasir=~02~';

    dsSelisihDetailKasirrwiKasirList.load
	(
		{
			params:
			{
				Skip: 0,
				Take: 1000,
				Sort: 'tgl_transaksi',
				//Sort: 'tgl_transaksi',
				Sortdir: 'ASC',
				target: 'ViewDetailSelisihRWI',
				param: strKriteriaKasirrwi
			},
            callback: function(records, operation, success) {
                console.log('hitung');
                totalSelisih();
            }
		}
	);
    return dsSelisihDetailKasirrwiKasirList;
}

function totalSelisih(){
    console.log(dsSelisihDetailKasirrwiKasirList);
    var totBiaya = 0;
    var totJatah = 0;
    var totSelisih = 0;
    for (var i = 0; i < dsSelisihDetailKasirrwiKasirList.getCount(); i++) {
        var o = dsSelisihDetailKasirrwiKasirList.getRange()[i].data;
        totBiaya += parseInt(o.HARGA);
        totJatah += parseInt(o.JATAH);
        totSelisih += parseInt(o.SELISIH);
    }
    console.log(totBiaya);
    console.log(totJatah);
    console.log(totSelisih);
    Ext.getCmp('txtTotalBiayaSelisihKasirRWI').setValue(totBiaya);
    Ext.getCmp('txtTotalJumlahSelisihKasirRWI').setValue(totJatah);
    Ext.getCmp('txtTotalSelisihKasirRWI').setValue(totSelisih);
}

//==============================================

function DataPanelPembayaranSummryKasirRWI(){
	var FormPanelPembayaranSummryKasirRWI = new Ext.Panel({
		id: 'FormPanelPembayaranSummryKasirRWI',
		layout: 'fit',
		title: 'Summary Bayar',
		border: false,
		items:getItemPanelSumaaryBayarKasirRWI()
	});
    return FormPanelPembayaranSummryKasirRWI
}
function DataPanelPembayaranDetailKasirRWI(){
	// var paneltotal = new Ext.Panel
	// (
		// {
			// id: 'paneltotal',
			// region: 'center',
			// border: true,
			// layout: 'column',
			// frame: true,
			// autoScroll: false,
			// items:
			// [
				// {
					// xtype: 'compositefield',
					// anchor: '100%',
					// labelSeparator: '',
					// border: true,
					// style: {'margin-top': '3px'},
					// items:
					// [
						// {
							// xtype: 'label',
							// style: {'margin-left': '5px'},
							// text: 'Jumlah bayar : '
						// }, 
						// {
							// xtype: 'numberfield',
							// id: 'txtTotalBayarDetailKasirRWI',
							// width: 110,
							// enableKeyEvents: true,
							// style: {'text-align': 'right','font-weight':'bold', 'margin-left': '3px'},
							// listeners:
							// {
								// 'specialkey': function ()
								// {
									// if (Ext.EventObject.getKey() === 13)
									// {
										// //hitungtotaltagihankasirrwi();
										// var tmpHarga = 0;
										// /*for(var x=0; x<dsTRDetailKasirrwiKasirList.getCount() ; x++){
											// var o=dsTRDetailKasirrwiKasirList.getRange()[x].data;
											// tmpHarga += parseInt(gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR'));
											// //Ext.getCmp('txtJumlah2EditData_viKasirrwi').setValue(tmpHarga);
										// }*/
										// Ext.getCmp('txtJumlah2EditData_viKasirrw11i').setValue(parseInt(Ext.getCmp('txtJumlah2EditData_viKasirrwi').getValue()) - parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()) );/*
										// Ext.getCmp('txtJumlah2EditData_viKasirrwi').setValue(formatCurrency(Ext.getCmp('txtJumlah2EditData_viKasirrwi').getValue()));
										// Ext.getCmp('txtTotalBayarDetailKasirRWI').setValue(formatCurrency(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()));*/
									// }
								// },
								// keyup : function(){
										// var tmpHarga = 0;
										// /*for(var x=0; x<dsTRDetailKasirrwiKasirList.getCount() ; x++){
											// var o=dsTRDetailKasirrwiKasirList.getRange()[x].data;
											// tmpHarga += parseInt(gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR'));
											// //Ext.getCmp('txtJumlah2EditData_viKasirrwi').setValue(tmpHarga);
										// }*/
										// /*Ext.getCmp('txtJumlah2EditData_viKasirrw11i').setValue(tmpHarga - parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()));
										// Ext.getCmp('txtJumlah2EditData_viKasirrwi').setValue(formatCurrency(Ext.getCmp('txtJumlah2EditData_viKasirrwi').getValue()));
										// Ext.getCmp('txtTotalBayarDetailKasirRWI').setValue(formatCurrency(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()));
										// if (Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()=='' || parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()) ==0) {
											// Ext.getCmp('txtJumlah2EditData_viKasirrw11i').setValue(0);
										// }*/

										// Ext.getCmp('txtJumlah2EditData_viKasirrw11i').setValue(parseInt(Ext.getCmp('txtJumlah2EditData_viKasirrwi').getValue()) - parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()) );
								// }
							// }
						// },
						// {
							// xtype: 'label',
							// style: {'font-size': '11px','margin-top': '7px'},
							// text: '*) Enter untuk hitung'
						// },/*
						// {
							// layout: 'form',
							// style: {'text-align': 'right', 'margin-left': '90px'},
							// border: false,
							// html: " Total :"
						// },*/
						// {
							// xtype: 'textfield',
							// id: 'txtJumlah2EditData_viKasirrw11i',
							// name: 'txtJumlah2EditData_viKasirrw11i',
							// style: {'text-align': 'right', 'margin-left': '45px'},
							// width: 82,
							// readOnly: true,
						// },/*
						// {
							// layout: 'form',
							// style: {'text-align': 'right', 'margin-left': '90px'},
							// border: false,
							// html: " Total :"
						// },*/
						// {
							// xtype: 'textfield',
							// id: 'txtJumlah2EditData_viKasirrwi',
							// name: 'txtJumlah2EditData_viKasirrwi',
							// style: {'text-align': 'right', 'margin-left': '45px'},
							// width: 82,
							// readOnly: true,
						// },
								// //-------------- ## --------------
					// ]
				// },{
					// xtype: 'compositefield',
					// anchor: '100%',
					// labelSeparator: '',
					// border: true,
					// style: {'margin-top': '5px'},
					// items:[
						// {
							// layout: 'form',
							// style: {'text-align': 'right', 'margin-left': '370px'},
							// border: false,
							// html: ""
						// },{
							// xtype: 'numberfield',
							// id: 'txtJumlahEditData_viKasirrwi',
							// name: 'txtJumlahEditData_viKasirrwi',
							// style: {'text-align': 'right', 'margin-left': '390px'},
							// width: 82,
							// hidden: true,
							// //readOnly: true,
						// }
					// ]
				// }
			// ]
		// }
	// )
	var FormPanelPembayaranDetailKasirRWI = new Ext.Panel({
		id: 'FormPanelPembayaranDetailKasirRWI',
		layout: {
			type:'vbox',
			align:'stretch'
		},
		title: 'Detail Bayar',
		border: false,
		items: [ 
			GetDTLTRKasirrwiGrid(), 
			{
				id:'pnlInacgbpenjasrwi',
				layout:'form',
				hidden:hiddenpnlInacgbpenjasrwi,
				labelAlign:'right',
				style:'padding: 0px 4px 4px 4px;',
				items:[
					{
						xtype:'numberfield',
						id:'txtinacbg1penjasrwj',
						fieldLabel:'Tagihan',
						width: 100,
						hidden:true,
						readOnly:true,
					},{
						xtype:'numberfield',
						id:'txtinacbg2penjasrwj',
						width: 100,
						fieldLabel:'Dijamin',
						readOnly:true,
						hidden:true,
					},{
						xtype:'numberfield',
						id:'txtinacbg4penjasrwj',
						width: 100,
						fieldLabel:'VIP',
						readOnly:true,
						hidden:true,
					},{
						xtype:'displayfield',
						id:'lblinacbdiagnosa',
						// hidden:true,
						height: 15,
						value:''
					}
				],
				title:'Tambahan Biaya Yang Dibayar Pasien Untuk Naik Kelas VIP',
				fbar:[
					{
						xtype:'displayfield',
						id:'lblinacbhiting',
						value:''
					},{
						xtype:'displayfield',
						value:'= Rp.'
					},{
						xtype:'numberfield',
						id:'txtinacbg3penjasrwj',
						width: 100,
						fieldLabel:'Bayar',
						readOnly:true,
					}
				]
			}
		]
	});
    return FormPanelPembayaranDetailKasirRWI;
}

function getItemPanelSumaaryBayarKasirRWI(){
 var items = new Ext.Panel({
		style:'padding: 4px;',
		layout:'form',
		border:true,
		autoScroll: true,
		items:[
			{
				xtype : 'fieldset',
				layout: 'absolute',
				width : 525,
				height :200,
				border: false,
				items:[
					{
						x: 0,
						y: 0,
						xtype: 'label',
						text: 'Total tagihan '
					}, {
						x: 70,
						y: 0,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 80,
						y: 0,
						xtype: 'textfield',
						id: 'txtTotalTagihanKasirRWI',
						width: 170,
						readOnly:true,
						style: {'text-align': 'right'}
					}, 
					{
						x: 0,
						y: 30,
						xtype: 'label',
						text: 'Sudah bayar'
					}, {
						x: 70,
						y: 30,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 80,
						y: 30,
						xtype: 'textfield',
						id: 'txtSudahBayarKasirRWI',
						width: 170,
						readOnly:true,
						style: {'text-align': 'right'}
					},
					{
						x: 0,
						y: 60,
						xtype: 'label',
						text: 'Sisa tagihan'
					}, {
						x: 70,
						y: 60,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 80,
						y: 60,
						xtype: 'textfield',
						id: 'txtSisatagihanKasirRWI',
						width: 170,
						readOnly:true,
						style: {'text-align': 'right'}
					},
					{
						x: 0,
						y: 90,
						xtype: 'label',
						text: 'Total bayar'
					}, {
						x: 70,
						y: 90,
						xtype: 'label',
						text: ' : '
					},
					{
						x: 80,
						y: 90,
						xtype: 'numberfield',
						id: 'txtTotalBayarSummaryKasirRWI',
						width: 170,
						style: {'text-align': 'right','font-weight':'bold'},
						enableKeyEvents: true,
						listeners:
						{
							'keyup': function ()
							{
								Ext.getCmp('txtTotalBayarDetailKasirRWI').setValue(Ext.getCmp('txtTotalBayarSummaryKasirRWI').getValue());
							}
						}
					},
				]
			}
		]
	})
	return items;
}

function getTotalBayarSummaryKasirRWI(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWI/gettotalbayar",
		params: {
			no_transaksi:currentNoTransaksi_KasirRWI,
			kd_kasir:currentKdKasir_KasirRWI,
		},
		failure: function (o)
		{
			ShowPesanErrorKasirrwi('Error mengambil total bayar summary. Hubungi Admin!', 'WARNING');
		},
		success: function (o)
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				Ext.getCmp('txtTotalTagihanKasirRWI').setValue(formatCurrency(cst.totaltagihan));
				Ext.getCmp('txtSudahBayarKasirRWI').setValue(formatCurrency(cst.jumlah));
				Ext.getCmp('txtSisatagihanKasirRWI').setValue(formatCurrency(cst.sisabayar));
				Ext.getCmp('txtJumlah2EditData_viKasirrwi').setValue(cst.sisabayar);
				Ext.getCmp('txtTotalBayarSummaryKasirRWI').setValue(cst.sisabayar);
				Ext.getCmp('txtTotalBayarDetailKasirRWI').setValue(cst.sisabayar);
				tmp_sisa_bayar = cst.sisabayar;
				currentJumlahSudahDibayarKasirRWI=cst.jumlah;
				currentTotalTagihanKasirRWI=cst.totaltagihan;
				currentSisaTagihanKasirRWI=cst.sisabayar;
				if(cst.sisabayar == 0 || cst.sisabayar == '0'){
					FormLookUpsdetailTRKasirrwi.close();
				}
			} else{
				ShowPesanWarningKasirrwi('Gagal mengambil total bayar summary!', 'WARNING');
			}
		}
	})
}

function getTotalBayar_KasirRWI(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWI/gettotalbayar",
		params: {
			no_transaksi:currentNoTransaksi_KasirRWI,
			kd_kasir:currentKdKasir_KasirRWI,
		},
		failure: function (o)
		{
			ShowPesanErrorKasirrwi('Error mengambil total bayar summary. Hubungi Admin!', 'WARNING');
		},
		success: function (o)
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				tmp_jumlah_bayar 	= cst.jumlah;
				tmp_sisa_bayar 		= cst.sisabayar;
				tmp_total_tagihan	= cst.totaltagihan;
			} else{
				ShowPesanWarningKasirrwi('Gagal mengambil total bayar summary!', 'WARNING');
			}
		}
	})
}


function hitungtotaltagihankasirrwi(){
	var tmpHarga = 0;
	var totalArray = 0;
	var TmpHargaBayarDanAsli = 0;
	Ext.getCmp('txtJumlah2EditData_viKasirrwi').setValue('');
	statusHitung = true;
/*
	if (parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()) == 0 || Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue() == '') {

		if (arrayTmp.length > 0) {
			for(var x=0; x<dsTRDetailKasirrwiKasirList.getCount() ; x++){
				var o=dsTRDetailKasirrwiKasirList.getRange()[x].data;
				TmpHargaBayarDanAsli = arrayTmp[x] + parseInt(gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR'));
				//if (TmpHargaBayarDanAsli < parseInt(gridDTLTRKasirrwinap.getStore().getRange()[x].get('HARGA'))) {
				
				arrayTmp[x] = 0;
				//}
			}
			Ext.getCmp('txtJumlah2EditData_viKasirrw11i').setValue('');
			//arrayTmp.destroy;
		}
		//arrayTmp.splice(0, arrayTmp.length);
		statusHitung = false;
	}else{
		statusHitung = true;
	}
*/

	for(var i=0; i<dsTRDetailKasirrwiKasirList.getCount() ; i++){
		arrayTmp[i] = 0;
	}

	if (parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()) == 0 || Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue() == '') {
		Ext.getCmp('txtTotalBayarDetailKasirRWI').setValue('0');
	}

	if (statusHitung == true) {
		//Ext.getCmp('txtJumlah2EditData_viKasirrw11i').setValue(recordterakhir-Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue());
		var totalTagihanRWI = parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue());
		var i = 1;
		if (parseInt(Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue()) != 0 || Ext.getCmp('txtTotalBayarDetailKasirRWI').getValue() != '') {

			for(var x=0; x<dsTRDetailKasirrwiKasirList.getCount() ; x++){
				var o=dsTRDetailKasirrwiKasirList.getRange()[x].data;
				//
					//if (arrayTmp[x] != 0 || arrayTmp[x] != '') {
						if (totalTagihanRWI < gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR')) {
							arrayTmp[x] 	= totalTagihanRWI;
							totalTagihanRWI = gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR') - totalTagihanRWI;
							gridDTLTRKasirrwinap.getStore().getRange()[x].set('BAYARTR',totalTagihanRWI);
						}else{
							arrayTmp[x] 	= gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR');
							totalTagihanRWI = totalTagihanRWI - gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR');
							gridDTLTRKasirrwinap.getStore().getRange()[x].set('BAYARTR',0);
						}
					//}
				
				/*if (totalTagihanRWI >= parseInt(gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR'))) {
				}else */
				/*if (gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR') > totalTagihanRWI){
					gridDTLTRKasirrwinap.getStore().getRange()[x].set('BAYARTR',(parseInt(gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR')) + totalTagihanRWI));
				}*/
			}
		}else{
			for(var x=0; x<dsTRDetailKasirrwiKasirList.getCount() ; x++){
				var o=dsTRDetailKasirrwiKasirList.getRange()[x].data;
				if (arrayTmp[x] > 0) {
					gridDTLTRKasirrwinap.getStore().getRange()[x].set('BAYARTR',arrayTmp[x] + parseInt(gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR')));
					arrayTmp[x] = 0;
				}else{
					gridDTLTRKasirrwinap.getStore().getRange()[x].set('BAYARTR',parseInt(gridDTLTRKasirrwinap.getStore().getRange()[x].get('BAYARTR')));
				}
			}
			arrayTmp.length = 0;
		}
	}else{
		statusHitung = false;
	}



}

/* ========================== AKHIR FITUR DEPOSIT============================================= */

/*-------------------------------Penambahan Discount-------------------------------------------
Oleh    : HDHT
Tanggal : 15 Maret 2017
Tempat  : Bandung
-----------------------------------------------------------------------------------------------*/

function fnDlgRWIDiscount(data)
{
    winRWIDiscount = new Ext.Window
    (
        {
            id: 'winRWIDiscount',
            title: 'Discount',
            closeAction: 'destroy',
            width:255,
            height: 300,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'cost',
            modal: true,
            items: [ItemDlgRWIDiscount()],
            fbar:[
                {
                    id: 'btnOkLookupDiscountKasirrwi',
                    text: 'OK',
                    handler: function (sm, row, rec)
                    {
                        saveDiscountKasirRWI();
                    }
                },
                {
                    id: 'btnCancelLookupDiscountKasirrwi',
                    text: 'Batal',
                    handler: function (sm, row, rec)
                    {
                        winRWIDiscount.close();
                    }
                },
                {
                    id: 'btnPrintLookupDiscountKasirrwi',
                    text: 'Print Kwitansi',
                    iconCls: 'print',
                    handler: function (sm, row, rec)
                    {
                        
                    }
                }
            
            ]

        }
    );
    winRWIDiscount.show();
    
    selectDataDiscountKasirRWI(data);
};

function ItemDlgRWIDiscount(){
 var items = new Ext.Panel({
        bodyStyle:'padding: 0px 0px 5px 5px',
        layout:'form',
        border:false,
        autoScroll: true,
        items:[
            {
                xtype : 'fieldset',
                title : '',
                layout: 'absolute',
                width : 235,
                height : 55,
                border: true,
                items:[
                    {
                        x: 0,
                        y: 0,
                        xtype: 'label',
                        text: 'Jumlah Tagihan '
                    }, {
                        x: 90,
                        y: 0,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 0,
                        xtype: 'textfield',
                        id: 'txtjumlahTagihanDiscountKasirRWI',
                        width: 103,
                        readOnly:true,
                        style: {'text-align': 'right'},
                    }
                ]
            },
            {
                xtype:'fieldset',
                checkboxToggle:true,
                title: 'Dengan Persentase',
                width : 235,
                height : 60,
                layout: 'absolute',
                items :[
                    {
                        x: 100,
                        y: 0,
                        xtype: 'textfield',
                        id: 'txtPersentaseTagihanDiscountKasirRWI',
                        width: 100,
                        style: {'text-align': 'right'},
                        enableKeyEvents: true,
                        listeners:{
                                'keyup': function ()
                                {
                                    if(Ext.EventObject.getKey() >= 48 && Ext.EventObject.getKey() <= 57){
                                        if (Ext.getCmp('txtPersentaseTagihanDiscountKasirRWI').getValue() > 100) {
                                            Ext.getCmp('txtPersentaseTagihanDiscountKasirRWI').setValue('100');
                                        }else{

                                        }
                                        var tmphasildiscount = (parseInt(tmpjumlahdiscount) * parseInt(Ext.getCmp('txtPersentaseTagihanDiscountKasirRWI').getValue())) / 100;
                                        Ext.get('txtjumlahDiscountDiscountKasirRWI').dom.value = formatCurrency(tmphasildiscount);
                                        tmpjumlahtotaldiscount = tmphasildiscount;
                                    }else{
                                        Ext.getCmp('txtPersentaseTagihanDiscountKasirRWI').setValue('');
                                    }

                                }
                        }
                    },
                    {
                        x: 200,
                        y: 5,
                        xtype: 'label',
                        text: '%'
                    }
                    
                ]
            },
            {
                xtype : 'fieldset',
                title : '',
                layout: 'absolute',
                width : 235,
                height : 55,
                border: true,
                items:[
                    {
                        x: 0,
                        y: 0,
                        xtype: 'label',
                        text: 'Jumlah Discount '
                    }, {
                        x: 90,
                        y: 0,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 0,
                        xtype: 'textfield',
                        id: 'txtjumlahDiscountDiscountKasirRWI',
                        width: 103,
                        style: {'text-align': 'right'},
                        enableKeyEvents: true,
                        listeners:{
                                'keyup': function ()
                                {
                                    if(Ext.EventObject.getKey() >= 48 && Ext.EventObject.getKey() <= 57){
                                        if (parseInt(Ext.getCmp('txtjumlahDiscountDiscountKasirRWI').getValue()) > parseInt(tmpjumlahdiscount)) {
                                            Ext.getCmp('txtPersentaseTagihanDiscountKasirRWI').setValue(tmpjumlahdiscount);
                                        }else{

                                        }
                                        var tmphasildiscount = (parseInt(tmpjumlahdiscount) / parseInt(Ext.getCmp('txtjumlahDiscountDiscountKasirRWI').getValue())) * 100;
                                        tmpjumlahtotaldiscount = tmphasildiscount;
                                    }else{
                                        Ext.getCmp('txtPersentaseTagihanDiscountKasirRWI').setValue('');
                                    }

                                }
                            }
                    }
                ]
            },
        ]
    })
    return items;
}
function selectDataDiscountKasirRWI(data){
    getJumlahTagihanKasirRWI(data.KD_KASIR,data.NO_TRANSAKSI);
}
var tmpjumlahdiscount = '';
var tmpjumlahtotaldiscount = '';

function getJumlahTagihanKasirRWI(kodekasir,notrans){
    Ext.Ajax.request
    ({
        url: baseURL + "index.php/main/functionRWI/getJumlahTagihan",
        params: {KODE:kodekasir,NO:notrans},
        failure: function (o)
        {
            ShowPesanErrorKasirrwi('Error membaca Jumlah Tagihan. Hubungi Admin!', 'WARNING');
        },
        success: function (o)
        {
            var cst = Ext.decode(o.responseText);
            if (cst.success === true)
            {
                Ext.get('txtjumlahTagihanDiscountKasirRWI').dom.value = formatCurrency(cst.ket);
                tmpjumlahdiscount = cst.ket;
            } else{
                ShowPesanWarningKasirrwi('Tagihan Tidak Ada', 'WARNING');
            }
        }
    })
}

function saveDiscountKasirRWI(){
    if(Ext.getCmp('txtjumlahDiscountDiscountKasirRWI').getValue() == '' || Ext.getCmp('txtjumlahDiscountDiscountKasirRWI').getValue()== 0){
        ShowPesanWarningKasirrwi('Jumlah Discount harus di isi!','WARNING');
    } else{
        Ext.Ajax.request
        ({
            url: baseURL + "index.php/main/functionRWI/savediscount",
            params: {
                no_transaksi:currentNoTransaksi_KasirRWI,
                kd_kasir:currentKdKasir_KasirRWI,
                jumlah:tmpjumlahtotaldiscount,
                kd_unit:kodeunit,
                persen:Ext.getCmp('txtPersentaseTagihanDiscountKasirRWI').getValue()
                
            },
            failure: function (o)
            {
                ShowPesanErrorKasirrwi('Error menyimpan Discount. Hubungi Admin!', 'WARNING');
            },
            success: function (o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    RefreshDatahistoribayar(currentNoTransaksi_KasirRWI);
                    ShowPesanInfoKasirrwi('Discount berhasil disimpan.','Information');
                    winRWIDiscount.close();
                    
                } else{
                    ShowPesanErrorKasirrwi('Discount gagal disimpan!', 'WARNING');
                }
            }
        })
    }
}



var recordterakhir;
function gettotalpostingmanual(){
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < dsDataStoreGridKomponent.getCount(); i++)
    {

        

        tampunggrid = parseInt(dsDataStoreGridKomponent.data.items[i].data.TARIF);
        TotalProduk += tampunggrid

        recordterakhir = TotalProduk
        Ext.get('txtFieldTotalHarga').dom.value = formatCurrency(recordterakhir);
    }
    bayar = Ext.get('txtFieldTotalHarga').getValue();
    return bayar;
}


function savePostingManualKasirRWI(){
    if(Ext.getCmp('cboPostingManual').getValue() == ''){
        ShowPesanWarningKasirrwi('Data Belum Lengkap!','WARNING');
    } else{
        Ext.Ajax.request
        (
            {
            url: baseURL + "index.php/main/functionRWI/savePostingManual",
            params: {
                no_transaksi: currentNoTransaksi_KasirRWI,
                kd_kasir: currentKdKasir_KasirRWI,
                jumlah: recordterakhir,
                list : getgridDetailPostingManualRwi(),
                faktur: Ext.get('TxtReferensi').getValue(),
                KODE: tmpkd_produkpostingmanual,
                unit:kodeunit
                
            },
            failure: function (o)
            {
                ShowPesanErrorKasirrwi('Error menyimpan Posting Manual. Hubungi Admin!', 'WARNING');
            },
            success: function (o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.status === true)
                {
                    RefreshDataDetail_kasirrwi(currentNoTransaksi_KasirRWI);
                    ShowPesanInfoKasirrwi('Posting Manual berhasil disimpan.','Information');
                    FormLookUpPostingManualKasirrwi.close();
                    var tmpkriteria = getCriteriaFilter_viDaftar();
                    RefreshDataFilterKasirrwiKasir();
					
                } else{
                    ShowPesanErrorKasirrwi('Posting Manual gagal disimpan!', 'WARNING');
                }
            }
        })
    }
}

function getgridDetailPostingManualRwi()
{
        var x='';
        var arr=[];
        for(var i = 0 ; i < dsDataStoreGridKomponent.getCount();i++)
        {
            if (dsDataStoreGridKomponent.data.items[i].data.TARIF != '' && dsDataStoreGridKomponent.data.items[i].data.DESKRIPSI != 0)
            {
                var o={};
                var y='';
                var z='@@##$$@@';
                o['KD_COMPONENT']= dsDataStoreGridKomponent.data.items[i].data.KD_COMPONENT;
                o['TARIF']= dsDataStoreGridKomponent.data.items[i].data.TARIF;
                arr.push(o);
            };
        }   
        
        return Ext.encode(arr);
};

/* 
	CREATE 	: MSD
	TGL 	: 16-03-2017
	KET 	: REKAPITULASI TINDAKAN RWI

 */
 function GetDTLTRGridRekapitulasiTindakanKasirRWI() {

    var fldDetail = ['kd_produk', 'kd_unit', 'deskripsi', 'qty', 'dokter', 'tgl_tindakan', 'qty', 'desc_req', 'tgl_berlaku', 
			'no_transaksi', 'urut', 'desc_status', 'tgl_transaksi', 'kd_tarif', 'harga', 'jumlah_dokter', 'tarif', 'no_faktur', 'folio'];
    dsRekapitulasiTindakanKasirRWI = new WebApp.DataStore({fields: fldDetail});
    
    GridRekapitulasiTindakanKasirRWI = new Ext.grid.EditorGridPanel({
        title: 'Rekapitulasi Tindakan Yang Diberikan',
        id: 'GridRekapTindakanKasirRWI',
        stripeRows: true,
        height: 130,
        store: dsRekapitulasiTindakanKasirRWI,
        border: false,
        frame: false,
        columnLines: true,
        anchor: '100% 100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
                   
                }
            }
        }),
        cm: TRRekapitulasiTindakanKasirRWIColumnModel(),
        viewConfig: {forceFit: true},
    });
    return GridRekapitulasiTindakanKasirRWI;
};

function TRRekapitulasiTindakanKasirRWIColumnModel() {
    return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
            id: 'colKdUnitRekapitulasiKasirRWI',
            header: 'Unit',
            dataIndex: 'kd_unit',
            width: 75,
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colReffRekapitulasiKasirRWI',
            header: 'Reff',
            dataIndex: 'no_faktur',
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colKdProdukRekapitulasiKasirRWI',
            header: 'Kode Produk',
            dataIndex: 'kd_produk',
            width: 100,
            menuDisabled: true
        },
		{
            id: 'colDeskripsiRekapitulasiKasirRWI',
            header: 'Nama Produk',
            dataIndex: 'deskripsi',
            sortable: false,
            hidden: false,
            menuDisabled: true,
            width: 250
        },{
            id: 'colHARGARekapitulasiKasirRWI',
            header: 'Jumlah',
            align: 'right',
            hidden: false,
            menuDisabled: true,
            dataIndex: 'harga',
            renderer: function (v, params, record)
            {
                return formatCurrency(record.data.harga);

            }
        },
        {
            id: 'colQTYRekapitulasiKasirRWI',
            header: 'Qty',
            width: 50,
            align: 'left',
            menuDisabled: true,
            dataIndex: 'qty'
        },

    ]);
};
function RefreshRekapitulasiTindakanKasirRWI(no_transaksi,kd_kasir){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWI/getRekapitulasiTindakan",
		params: {
			no_transaksi:no_transaksi,
			kd_kasir:kd_kasir,
            co_status:0,
		},
		failure: function (o)
		{
			ShowPesanErrorKasirrwi('Error membaca rekapitulasi tindakan. Hubungi Admin!', 'WARNING');
		},
		success: function (o)
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				dsRekapitulasiTindakanKasirRWI.removeAll();
				var recs=[],
					recType=dsRekapitulasiTindakanKasirRWI.recordType;
					
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsRekapitulasiTindakanKasirRWI.add(recs);
				//GridRekapitulasiTindakanKasirRWI.getView().refresh();
			} else{
				ShowPesanWarningKasirrwi('Gagal membaca rekapitulasi tindakan!', 'WARNING');
			}
		}
	})
}

/*-------------------------------Penambahan Cetak Billing-------------------------------------------
Oleh    : HDHT
Tanggal : 16 Maret 2017
Tempat  : Bandung
---------------------------------------------------------------------------------------------------*/

function fnDlgRWICetakBilling_KASIRRWI(data)
{
    winRWICetakBilling = new Ext.Window
    (
        {
            id: 'winRWICetakBilling',
            title: 'Cetak Billing',
            closeAction: 'destroy',
            width:314,
            height: 300,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'cost',
            modal: true,
            items: [ItemDlgRWICetakBilling()],
            listeners:{
                 'show':function(win){
                        Ext.getCmp('dtpTanggalawalTransaksiCetakBillRWI').hide();
	                    Ext.getCmp('dtpTanggalakhirTransaksiCetakBillRWI').hide();
						Ext.getCmp('lbltanggalawal1').hide();
						Ext.getCmp('lbltanggalawal2').hide();
	                    Ext.getCmp('lbltanggalawal3').hide();
	                    Ext.getCmp('cboJenisPrintKasirRWI').setValue('Billing');
	                    Ext.getCmp('TxtCariNamaPembayaran').setValue(data.NAMA);
	                    Ext.getCmp('cboSpesialisasicetakbillkasirRWI').setValue('Semua Spesialisasi');
	                    tmpcetakkwitansi = 'true';
                  }

         	},
            fbar:[
                {
                    id: 'btnOkLookupCetakBillingKasirrwi',
                    text: 'OK',
                    handler: function (sm, row, rec)
                    {   
                    	if (Ext.getCmp('cboJenisPrintKasirRWI').getValue() == 'Billing' || Ext.getCmp('cboJenisPrintKasirRWI').getValue() == '0' || Ext.getCmp('cboJenisPrintKasirRWI').getValue() == 0) {
                            var url_laporan = baseURL + "index.php/laporan/lap_billing_penunjang/";
                            var params={
                                no_transaksi    : data.NO_TRANSAKSI,
                                kd_kasir        : data.KD_KASIR,
                            } ;
                            console.log(params);
                            var form = document.createElement("form");
                            form.setAttribute("method", "post");
                            form.setAttribute("target", "_blank");
                            form.setAttribute("action", url_laporan+"/preview_pdf");
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", "data");
                            hiddenField.setAttribute("value", Ext.encode(params));
                            form.appendChild(hiddenField);
                            document.body.appendChild(form);
                            form.submit();  
                        }

                    	var Spesialisasi = "";
						var nama_Spesialisasi = "";
							if (Ext.getCmp('cboSpesialisasicetakbillkasirRWI').getValue() === 'Semua Spesialisasi')
							{
								Spesialisasi = 'NULL';
								nama_Spesialisasi = 'Semua Spesialisasi';
							}else{
								Spesialisasi = kdspesialisasi;
								nama_Spesialisasi = namaSpesialisasi;
							}
							var params={
										Spesialisasi    : Spesialisasi, //split[2]
										dtlspesialisasi : nama_Spesialisasi,
										Tglpulang	    : Ext.getCmp('dtpTanggalPulang').getValue(), //split[0]
										jenisprint 	    : Ext.getCmp('cboJenisPrintKasirRWI').getValue(), //split[1]
										folio           : tmpcgfolio,//split[5]
										notrans         : data.NO_TRANSAKSI, //split[3]
										KdKasir         : data.KD_KASIR,
										nama 		    : data.NAMA,
										nama_pembayaran : Ext.getCmp('TxtCariNamaPembayaran').getValue(),
										kdpasien	    : data.KD_PASIEN,
										reff 		    : Ext.getCmp('TxtCariReferensi').getValue(), //split[6]
										TglMasuk    	: data.TANGGAL_MASUK,
										kdunit 			: data.KD_UNIT,
										costat 			: 'false',
										cetakan 		: tmpcetakan,
										cetakkwitansi   : tmpcetakkwitansi,
										detailData   	: Ext.getCmp('cbDetailData').getValue(),
							};
						    Ext.Ajax.request({
								//url: baseURL + "index.php/rawat_inap/lap_bill_kasir_rwi/Cetak",
								url: baseURL + "index.php/rawat_inap/lap_bill_kwitansi/Cetak",
								params: params,
								success: function(o){
									var cst = Ext.decode(o.responseText);
									if (cst.success === true){
										ShowPesanInfoKasirrwi('Sedang di Print.','Information');
									}else if  (cst.success === false && cst.pesan===0){
										ShowPesanWarningKasirrwi('Data tidak berhasil di Print '  + cst.pesan,'Print Data');
									}else{
										ShowPesanErrorKasirrwi('Data tidak berhasil di Print '  + cst.pesan,'Print Data');
									}
								}
							});

                            
      //                   var form = document.createElement("form");
						// form.setAttribute("method", "post");
						// form.setAttribute("target", "_blank");
						// form.setAttribute("action", baseURL + "index.php/rawat_inap/lap_bill_kasir_rwi/Cetak");
						// var hiddenField = document.createElement("input");
						// hiddenField.setAttribute("type", "hidden");
						// hiddenField.setAttribute("name", "data");
						// hiddenField.setAttribute("value", Ext.encode(params));
						// form.appendChild(hiddenField);
						// document.body.appendChild(form);
						// form.submit();	
						// frmDlgRWJPerLaporandetail.close();
                    }
                },
                {
                    id: 'btnCancelLookupCetakBillingKasirrwi',
                    text: 'Batal',
                    handler: function (sm, row, rec)
                    {
                        winRWICetakBilling.close();
                    }
                }
            
            ]

        }
    );
    winRWICetakBilling.show();
    
    selectDataCetakBillingKasirRWI(data);
};
var tmpcgfolio = '';
function ItemDlgRWICetakBilling(){
 var items = new Ext.Panel({
        bodyStyle:'padding: 0px 0px 0px 0px',
        layout:'form',
        border:false,
        autoScroll: true,
        items:[
            {
                xtype : 'fieldset',
                title : '',
                layout: 'absolute',
                width : 300,
                height : 225,
                border: true,
                items:[
                    {
                        x: 0,
                        y: 0,
                        xtype: 'label',
                        text: 'Jenis '
                    }, {
                        x: 90,
                        y: 0,
                        xtype: 'label',
                        text: ' : '
                    },
                   mComboJenisPrintKasirRWI(),
		            {
                        x: 0,
                        y: 30,
                        xtype: 'label',
                        text: 'Tanggal Pulang '
                    }, {
                        x: 90,
                        y: 30,
                        xtype: 'label',
                        text: ' : '
                    },
                   {
                        x: 100,
                        y: 30,
                        xtype: 'datefield',
                        name: 'dtpTanggalPulang',
                        id: 'dtpTanggalPulang', 
                        format: 'd/M/Y',
                        value: now,
                        width: 122
                    },
                    {
                        x: 0,
                        y: 60,
                        xtype: 'label',
                        text: 'Pembayaran'
                    }, 
                    {
                        x: 90,
                        y: 60,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 60,
                        xtype: 'textfield',
                        name: 'TxtCariNamaPembayaran',
                        id: 'TxtCariNamaPembayaran',
                        width: 170,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                            // load_pengkajian(tmpkriteriaPengkajian);
                                        }
                                    }
                                }
                    },
                    {
                        x: 0,
                        y: 90,
                        xtype: 'label',
                        text: 'Referensi'
                    }, 
                    {
                        x: 90,
                        y: 90,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 90,
                        xtype: 'textfield',
                        name: 'TxtCariReferensi',
                        id: 'TxtCariReferensi',
                        width: 170,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                            // load_pengkajian(tmpkriteriaPengkajian);
                                        }
                                    }
                                }
                    },
                    {
                        x: 0,
                        y: 120,
                        xtype: 'label',
                        text: 'Cetak Kwitansi',
                        id:'lblchc1',
                        hidden : true,
                    },
                    {
                        x: 90,
                        y: 120,
                        xtype: 'label',
                        text: ' : ',
                        hidden : true,
                        id:'lblchc2'
                    },
	                {
	                   	x: 100,
	                    y: 120,
	                   	xtype: 'checkboxgroup',
					    boxMaxWidth: 20,
                        hidden : true,
					    id:'checkboxkwitansi',
					    itemCls: 'x-check-group-alt',
					    columns: 1,
					    items: [
					        {boxLabel: '', name: 'cbCetakWitansi', id: 'cbCetakWitansi', checked: true}
					    ],
					    listeners: {
                            change: function (checkbox, newValue, oldValue, eOpts) {
                                if (Ext.getCmp('cbCetakWitansi').checked === true)
                                {
                                    tmpcetakkwitansi = 'true';
                                } else
                                {
                                    tmpcetakkwitansi = 'false';
                                }
                            }
                        }
	                },
	                {
                        x: 0,
                        y: 150,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 90,
                        y: 150,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboRWISpesialisasicetakbillkasirRWI(),
                    {
                        x: 0,
                        y: 180,
                        xtype: 'label',
                        text: 'Tanggal Transaksi ',
                        id:'lbltanggalawal1'
                    },
                    {
                        x: 90,
                        y: 180,
                        xtype: 'label',
                        text: ' : ',
                        id:'lbltanggalawal2'
                    },
                    {
                        x: 100,
                        y: 180,
                        xtype: 'datefield',
                        name: 'dtpTanggalawalTransaksiCetakBillRWI',
                        id: 'dtpTanggalawalTransaksiCetakBillRWI', 
                        
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 210,
                        y: 185,
                        xtype: 'label',
                        text: 'S/D',
                        id:'lbltanggalawal3'

                    },
                    {
                        x: 240,
                        y: 180,
                        xtype: 'datefield',
                        name: 'dtpTanggalakhirTransaksiCetakBillRWI',
                        id: 'dtpTanggalakhirTransaksiCetakBillRWI', 
                        
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 0,
                        y: 180,
                        xtype: 'label',
                        text: 'Detail data',
                        id:'checkDetail'
                    },
                    {
                        x: 90,
                        y: 180,
                        xtype: 'label',
                        text: ' : ',
                        id:'checkDetailLabel'
                    },
                    {
                        x: 100,
                        y: 180,
	                   	xtype: 'checkboxgroup',
					    boxMaxWidth: 20,
					    id:'checkboxDetailData',
					    itemCls: 'x-check-group-alt',
					    columns: 1,
					    items: [
					        {boxLabel: '', name: 'cbDetailData', id: 'cbDetailData', checked: false}
					    ],
                    },
	            ]
    		}
    	]
    })
    return items;
}
function selectDataCetakBillingKasirRWI(data){
    getJumlahTagihanKasirRWI(data.KD_KASIR,data.NO_TRANSAKSI);
}
var selectSetJenisPrinter;
var tmpcetakan = 'Billing';
var tmpcetakkwitansi = 'true';
function mComboJenisPrintKasirRWI()
{
    var cboJenisPrintKasirRWI = new Ext.form.ComboBox
        (
            {
            	x: 100,
                y: 0,
                id: 'cboJenisPrintKasirRWI',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                selectOnFocus: true,
                forceSelection: true,
                emptyText: '',
                width: 170,
                store: new Ext.data.ArrayStore
                        (
                                {
                                    id: 0,
                                    fields:
                                            [
                                                'Id',
                                                'displayText'
                                            ],
                                    data: [[0, 'Billing']]
                                }
                        ),
                valueField: 'Id',
                displayField: 'displayText',
                value: selectSetJenisPrinter,
                listeners:
                        {
                            'select': function (a, b, c)
                            {
                            	tmpcetakan = b.data.displayText;
                                if(b.data.displayText === 'Kwitansi')
                            	{
                            		Ext.getCmp('dtpTanggalawalTransaksiCetakBillRWI').hide();
	                                Ext.getCmp('dtpTanggalakhirTransaksiCetakBillRWI').hide();
	                                Ext.getCmp('lbltanggalawal1').hide();
                					Ext.getCmp('lbltanggalawal2').hide();
                                    Ext.getCmp('lbltanggalawal3').hide();
                                    Ext.getCmp('lblchc1').hide();
                                    Ext.getCmp('lblchc2').hide();
                                    Ext.getCmp('checkboxkwitansi').hide();
                                    tmpcetakkwitansi = 'false';

                            	}else{
                                	Ext.getCmp('dtpTanggalawalTransaksiCetakBillRWI').hide();
	                                Ext.getCmp('dtpTanggalakhirTransaksiCetakBillRWI').hide();
                					Ext.getCmp('lbltanggalawal1').hide();
                					Ext.getCmp('lbltanggalawal2').hide();
                                    Ext.getCmp('lbltanggalawal3').hide();
                                    Ext.getCmp('lblchc1').show();
                                    Ext.getCmp('lblchc2').show();
                                    Ext.getCmp('checkboxkwitansi').show();
                                }
                            }
                        }
            }
        );
    return cboJenisPrintKasirRWI;
}
;

var ds_SpesialisasicetakbillkasirRWI;
var kdspesialisasi;
var namaSpesialisasi;
function mComboRWISpesialisasicetakbillkasirRWI()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];

    ds_SpesialisasicetakbillkasirRWI = new WebApp.DataStore({fields: Field});
    ds_SpesialisasicetakbillkasirRWI.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_spesial',
            Sortdir: 'ASC',
            target: 'ViewSetupSpesial',
            param: 'kd_spesial <> 0'
        }
    })

    var cboSpesialisasicetakbillkasirRWI = new Ext.form.ComboBox
    ({
    	x:100,
    	y:150,
        id: 'cboSpesialisasicetakbillkasirRWI',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        selectOnFocus: true,
        forceSelection: true,
        emptyText: 'Pilih Spesialisasi...',
        fieldLabel: 'Spesialisasi ',
        align: 'Right',
        store: ds_SpesialisasicetakbillkasirRWI,
        valueField: 'KD_SPESIAL',
        displayField: 'SPESIALISASI',
        width : 170,
        listeners:
        {
            'select': function (a, b, c)
            {
                kdspesialisasi = b.data.KD_SPESIAL;
                namaSpesialisasi = b.data.SPESIALISASI;
            },
            'render': function (c)
            {
                // c.getEl().on('keypress', function (e) {
                //     if (e.getKey() == 13 || e.getKey() == 9) //atau Ext.EventObject.ENTER
                //         Ext.getCmp('cboKelas_panatajasarwi').focus();
                // }, c);
            }
        }
    })

    return cboSpesialisasicetakbillkasirRWI;
}
;

/*-------------------------------Penambahan Pasien Pulang-------------------------------------------
Oleh    : HDHT
Tanggal : 29 Maret 2017
Tempat  : Bandung
---------------------------------------------------------------------------------------------------*/
function startTime(){
	var startToday = new Date();
	var start_h = startToday.getHours();
	var start_m = startToday.getMinutes();
	var start_s = startToday.getSeconds();
	start_m = checkTime(start_m);
	start_s = checkTime(start_s);
	Ext.get('dtpJamPulangPasien_PasienPulangRWI').dom.value = start_h+":"+start_m+":"+start_s;
	var t = setTimeout(startTime, 500);
}

function checkTime(i){
	if (i<10) { i = "0"+i};
	return i;
}

function fnDlgRWIPasienPulangRWI(form, data)
{
	var title 		= "";
	var link_url 	= "";
	if (form == 'form_pulang') {
		title    	= "Pasien Pulang";
		link_url 	= baseURL + "index.php/main/functionRWI/UpdatePasienPulangKasirRWI";
	}else{
		title = "Tutup Transaksi";
		link_url = baseURL + "index.php/main/functionRWI/UpdateTutuptransaksi";
	}
    winRWIPasienPulangRWI = new Ext.Window
    (
        {
            id: 'winRWIPasienPulangRWI',
            title: title,
            closeAction: 'destroy',
            width:415,
            height: 300,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'cost',
            modal: true,
            items: [ItemDlgRWIPasienPulangRWI(form)],
            listeners:{
                 'show':function(win){
	                    Ext.getCmp('TxtKodePasien_PasienPulangRWI').setValue(data.KD_PASIEN);
	                    Ext.getCmp('TxtNamaPasien_PasienPulangRWI').setValue(data.NAMA);
	                    Ext.getCmp('TxtKelasPasien_PasienPulangRWI').setValue(data.NAMA_UNIT);
	                    Ext.getCmp('TxtKamarPasien_PasienPulangRWI').setValue(data.NAMA_KAMAR);
	                    var tmptgl = data.TANGGAL_MASUK;
	                    var tglmasuk = tmptgl.split(" ");
	                    Ext.getCmp('dtpMasukPasien_PasienPulangRWI').setValue(tglmasuk[0]);
	                    Ext.Ajax.request
	                    ({
	                    	url: baseURL + "index.php/main/functionRWI/getdatakunjunganpasien",
	                    	params: {
	                    		kd_pasien :data.KD_PASIEN,
	                    		kd_unit :data.KD_UNIT,
	                    		tgl_masuk : data.TANGGAL_MASUK
	                    	},
	                    	failure: function (o)
	                    	{
	                    		ShowPesanErrorKasirrwi('Error membaca Data Pasien. Hubungi Admin!', 'WARNING');
	                    	},
	                    	success: function (o)
	                    	{
	                    		var cst = Ext.decode(o.responseText);
	                    		if (cst.success === true)
	                    		{
	                    			var tmpjam = cst.jam;
	                    			var tgljam = tmpjam.split(" ");
	                    			Ext.getCmp('dtpJamMasukPasien_PasienPulangRWI').setValue(tgljam[1]);
	                    		} else{
	                    			ShowPesanWarningKasirrwi('Gagal membaca Data Pasien!', 'WARNING');
	                    		}
	                    	}
	                    })
                  }, 

         	},
            fbar:[
                {
                    id: 'btnOkLookupPasienPulangRWIKasirrwi',
                    text: 'OK',
                    handler: function (sm, row, rec)
                    {        
                    	if ((statusPulang == null || statusPulang == 'null' || statusCaraKeluar == null || statusCaraKeluar == 'null') && form == "form_pulang") { 
							ShowPesanWarningKasirrwi('Keterangan belum lengkap!', 'Peringatan');
                    	}else{         	
	                    	Ext.Ajax.request({
			                	url: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
			                	params: {
				                	select 	: " count(*) as counter ",
									where 	: " kd_kasir = '"+data.KD_KASIR+"' AND no_transaksi = '"+data.NO_TRANSAKSI+"' AND co_status = 'true'", 
									table 	: " transaksi ",
			                	},
			                	failure: function (o)
			                	{
			                		ShowPesanErrorKasirrwi('Error Memulangkan Pasien. Hubungi Admin!', 'WARNING');
			                	},
			                	success: function (o)
			                	{

									var cst 	= Ext.decode(o.responseText);
									if( cst[0].counter > 0 && form == 'form_pulang'){
								    	// var tmp_url = baseURL+"index.php/laporan/lap_pulang/print_surat_pulang/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.KD_KASIR+"/"+statusPulang+"/"+statusCaraKeluar;
										// window.open(tmp_url,'_blank');
									}else{
		                    			Ext.Ajax.request({
					                    	url: link_url,
					                    	params: {
												kd_pasien 	: data.KD_PASIEN,
												kd_unit 	: data.KD_UNIT,
												tgl_masuk 	: data.TANGGAL_MASUK,
												urut_masuk 	: data.URUT_MASUK,
												kd_kasir 	: data.KD_KASIR,
												notrans 	: data.NO_TRANSAKSI,
												status 		: statusPulang,
					                    		tglkeluar  	: Ext.getCmp('dtpPulangPasien_PasienPulangRWI').getValue(),
					                    		jamkeluar  	: Ext.getCmp('dtpJamPulangPasien_PasienPulangRWI').getValue()
					                    	},
					                    	failure: function (o)
					                    	{
					                    		ShowPesanErrorKasirrwi('Error Memulangkan Pasien. Hubungi Admin!', 'WARNING');
					                    	},
					                    	success: function (o)
					                    	{
					                    		var cst = Ext.decode(o.responseText);
												if (form == 'form_pulang') {
						                    		// if (cst.status_lunas === false) {
							                    		// ShowPesanWarningKasirrwi('Status pembayaran belum balance!', 'WARNING');
						                    		// }else{
							                    		if (cst.status === true)
							                    		{
									                    	// var tmp_url = baseURL+"index.php/laporan/lap_pulang/print_surat_pulang/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.KD_KASIR+"/"+statusPulang+"/"+statusCaraKeluar;
															// window.open(tmp_url,'_blank');
							                    			ShowPesanInfoKasirrwi('Pasien Berhasil Di Pulangkan', 'Information');
						                        			winRWIPasienPulangRWI.close();
						                        			RefreshDataFilterKasirrwiKasir();

							                    		} else{
							                    			ShowPesanWarningKasirrwi('Gagal Memulangkan Pasien!', 'WARNING');
							                    		}	
						                    		// }
						                    	}else{
							                    	if (cst.status === true)
							                    	{
							                    		ShowPesanInfoKasirrwi('Transaksi berhasil di tutup', 'Information');
					                        		winRWIPasienPulangRWI.close();
					                        		RefreshDataFilterKasirrwiKasir();
						                    	} else{
						                    		ShowPesanWarningKasirrwi('Gagal menutup transaksi !', 'WARNING');
						                    	}	
					                    	}
				                    	}
				                    });
									}
				                }
				            });
                    	}
                    }
                },
                {
                    id: 'btnCancelLookupPasienPulangRWIKasirrwi',
                    text: 'Batal',
                    handler: function (sm, row, rec)
                    {
                        winRWIPasienPulangRWI.close();
                    }
                }
            
            ]

        }
    );
    winRWIPasienPulangRWI.show();
    
    // selectDataPasienPulangRWIKasirRWI(data);
};
var tmpcgfolio = '';
function ItemDlgRWIPasienPulangRWI(form){
	loaddatastoreStatusPulang();
	loaddatastoreCaraKeluar();
	// DISINI
	var label_tgl_target_ 	= "";
	var label_jam_target_ 	= "";
	var hide_status  		= true;
	if (form == 'form_pulang') {
		label_tgl_target_  	= "Pulang";
		label_jam_target_ 	= "Jam Pulang";
		hide_status 		= false;
	}else{
		label_tgl_target_  	= "Tutup Trans.";
		label_jam_target_ 	= "Jam Tutup";
	}

	var items = new Ext.Panel({
			bodyStyle:'padding: 0px 0px 0px 0px',
			layout:'form',
			border:false,
			autoScroll: true,
			items:[
	            {
	                xtype : 'fieldset',
	                title : '',
	                layout: 'absolute',
	                width : 400,
	                height : 200,
	                border: true,
	                items:[
	                    {
	                        x: 0,
	                        y: 0,
	                        xtype: 'label',
	                        text: 'Pasien '
	                    }, {
	                        x: 50,
	                        y: 0,
	                        xtype: 'label',
	                        text: ' : '
	                    },
	                    {
	                    	x: 60,
	                		y: 0,
	                		xtype: 'textfield',
	                        name: 'TxtKodePasien_PasienPulangRWI',
	                        id: 'TxtKodePasien_PasienPulangRWI',
	                        width: 100,
	                    },
	                    {
	                    	x: 170,
	                		y: 0,
	                		xtype: 'textfield',
	                        name: 'TxtNamaPasien_PasienPulangRWI',
	                        id: 'TxtNamaPasien_PasienPulangRWI',
	                        width: 200,
	                    },
	                   // mComboJenisPrintKasirRWI(),
			            {
	                        x: 0,
	                        y: 30,
	                        xtype: 'label',
	                        text: 'Kelas '
	                    }, {
	                        x: 50,
	                        y: 30,
	                        xtype: 'label',
	                        text: ' : '
	                    },
	                    {
	                    	x: 60,
	                		y: 30,
	                		xtype: 'textfield',
	                        name: 'TxtKelasPasien_PasienPulangRWI',
	                        id: 'TxtKelasPasien_PasienPulangRWI',
	                        width: 100,
	                    },
	                    {
	                        x: 170,
	                        y: 30,
	                        xtype: 'label',
	                        text: 'Kamar '
	                    }, {
	                        x: 210,
	                        y: 30,
	                        xtype: 'label',
	                        text: ' : '
	                    },
	                    {
	                    	x: 220,
	                		y: 30,
	                		xtype: 'textfield',
	                        name: 'TxtKamarPasien_PasienPulangRWI',
	                        id: 'TxtKamarPasien_PasienPulangRWI',
	                        width: 100,
	                    },
	                    {
	                        x: 0,
	                        y: 60,
	                        xtype: 'label',
	                        text: 'Masuk'
	                    }, 
	                    {
	                        x: 60,
	                        y: 60,
	                        xtype: 'label',
	                        text: ' : '
	                    },
	                    {
	                        x: 70,
	                        y: 60,
	                        xtype: 'datefield',
	                        name: 'dtpMasukPasien_PasienPulangRWI',
	                        id: 'dtpMasukPasien_PasienPulangRWI', 
	                        format: 'd/M/Y',
	                        value: now,
	                        width: 100
	                    },
	                    {
	                        x: 180,
	                        y: 60,
	                        xtype: 'label',
	                        text: 'Jam Masuk '
	                    }, {
	                        x: 240,
	                        y: 60,
	                        xtype: 'label',
	                        text: ' : '
	                    },
	                    {
	                        x: 240,
	                        y: 60,
	                        xtype: 'datefield',
	                        name: 'dtpJamMasukPasien_PasienPulangRWI',
	                        id: 'dtpJamMasukPasien_PasienPulangRWI', 
	                        format: 'H:i:s',
	                        value: now,
	                        width: 100
	                    },
	                    {
	                        x: 0,
	                        y: 90,
	                        xtype: 'label',
	                        text: label_tgl_target_
	                    }, 
	                    {
	                        x: 60,
	                        y: 90,
	                        xtype: 'label',
	                        text: ' : '
	                    },
	                    {
	                        x: 70,
	                        y: 90,
	                        xtype: 'datefield',
	                        name: 'dtpPulangPasien_PasienPulangRWI',
	                        id: 'dtpPulangPasien_PasienPulangRWI', 
	                        format: 'd/M/Y',
	                        value: now,
	                        width: 100
	                    },
	                    {
	                        x: 180,
	                        y: 90,
	                        xtype: 'label',
	                        text: label_jam_target_
	                    }, {
	                        x: 240,
	                        y: 90,
	                        xtype: 'label',
	                        text: ' : '
	                    },
	                    {
	                        x: 240,
	                        y: 90,
	                        xtype: 'datefield',
	                        name: 'dtpJamPulangPasien_PasienPulangRWI',
	                        id: 'dtpJamPulangPasien_PasienPulangRWI', 
	                        format: 'H:i:s',
	                        value: now,
	                        width: 100
						},{
							x: 0,
							y: 120,
							xtype 		: 'label',
							text 		: "Keadaan",
							hidden 		: hide_status,
						},{
							x: 60,
							y: 120,
							xtype 		: 'label',
							text 		: ' : ',
							hidden 		: hide_status,
						},{
							x: 70,
							y: 120,
							xtype 			: 'combo',
							id 				: 'cboStatusPulang',
							typeAhead 		: true,
							triggerAction 	: 'all',
							lazyRender 		: true,
							mode 			: 'local',
							selectOnFocus: true,
							forceSelection: true,
							emptyText: '',
							hidden 			: hide_status,
							width: 300,
							store: dsStatusPulangPasien,
							valueField: 'KD_STATUS_PULANG',
							displayField: 'STATUS_PULANG',
							value: selectSetJenisPrinter,
							listeners:{
								select : function(a,b){
									statusPulang = b.data.KD_STATUS_PULANG;
								}
							}
						},{
							x: 0,
							y: 150,
							xtype 		: 'label',
							text 		: "Cara Keluar",
							hidden 		: hide_status,
						},{
							x: 60,
							y: 150,
							xtype 		: 'label',
							text 		: ' : ',
							hidden 		: hide_status,
						},{
							x: 70,
							y: 150,
							xtype 			: 'combo',
							id 				: 'cboCaraKeluar',
							typeAhead 		: true,
							triggerAction 	: 'all',
							lazyRender 		: true,
							mode 			: 'local',
							selectOnFocus 	: true,
							forceSelection 	: true,
							emptyText 		: '',
							hidden 			: hide_status,
							width 			: 300,
							store 			: dsCaraKeluar,
							valueField: 'kd_cara_keluar',
							displayField: 'cara_keluar',
							value: selectSetJenisPrinter,
							listeners:{
								select : function(a,b){
									statusCaraKeluar = b.data.kd_cara_keluar;
								}
							}
						}
		            ]
	    		}
	    	]
	    })
    return items;
}
/*
    =============================================================================================
										PREVIEW BILLING ANDALAS
										Update : Hadad Al Gojali
	=============================================================================================
*/
	function GetDTLTRGridPreviewBilling(co_status = null) {
        var tmp_url = baseURL+"index.php/laporan/lap_billing_rwi/cetak/"+co_status+"/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.KD_KASIR;
						window.open(tmp_url,'_blank');
		// var url = TmpUrl;
		// var url = baseURL+"index.php/laporan/lap_billing_rwi/preview/"+co_status+"/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.KD_KASIR;
		// new Ext.Window({
		// 	title: 'Preview Billing',
		// 	width: 1000,
		// 	height: 600,
		// 	constrain: true,
		// 	modal: true,
		// 	html: "<iframe  style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
		// 	tbar : [
		// 		{
		// 			text 	: 'Cetak Direct',
		// 			iconCls : 'print',
		// 			handler : function(){
        //             	var tmp_url = baseURL+"index.php/laporan/lap_billing_rwi/cetak/"+co_status+"/"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirrwiKasir.data.KD_KASIR;
		// 				window.open(tmp_url,'_blank');
		// 			}
		// 		}
		// 	]
		// }).show();
	};

/*
    =============================================================================================
										PREVIEW BILLING
	=============================================================================================


	function GetDTLTRGridPreviewBilling() {
		var url = TmpUrl;
		new Ext.Window({
			title: 'Preview Billing',
			width: 1000,
			height: 600,
			constrain: true,
			modal: true,
			html: "<iframe  style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
		}).show();
	};
*/

function mComboViewDataKasirRWI(){
    var mComboViewDataKasirRWI = new Ext.form.ComboBox({
		fieldLabel 		: 'Jumlah data',
		id 				: 'mComboViewDataKasirRWI',
		typeAhead 		: true,
		triggerAction 	: 'all',
		lazyRender 		: true,
		mode 			: 'local',
		selectOnFocus 	: true,
		forceSelection 	: true,
		emptyText 		: '',
		width 			: 50,
		store: new Ext.data.ArrayStore({
			fields:['id',],
			data: [[5],[10],[25],[50],[100],]
		}),
		valueField: 'id',
		displayField: 'id',
		value: 5,
		listeners:{
			'select': function (a, b, c){
				RefreshDataFilterKasirrwiKasir();
			}
		}
	});
    return mComboViewDataKasirRWI;
}

/*
	===============================================================================================
	LOOKUP TINDAKAN untuk RWI
	UPDATE 	: HADAD AL GOJALI
	TANGGAL : 2017-11-10
	===============================================================================================
 */
function formLookUpProduk_KasirRawatInap(kdunit,namaunit) {
    var cookie_value_add = new Array();
	// LoadDataStoreProdukKasirIGD('');
	var chkgetTindakanKasirRawatInap = new Ext.grid.CheckColumn({
		xtype: 'checkcolumn',
		width: 5,
		sortable: false,
		id: 'check1KasirRWI',
		dataIndex: 'checkProduk',
		editor: {
			xtype: 'checkbox',
			cls: 'x-grid-checkheader-editor'
		}
	}); 
    var cbxSelModel = new Ext.grid.CheckboxSelectionModel({
        checkOnly: false,
        singleSelect: false,
        sortable: false,
        dataIndex: 'visible',
        width: 20,
        listeners: {
            selectionchange : function(selModel) {
            },
            scope: this
        }
    });
	var selectModel = new Ext.grid.CheckboxSelectionModel();
    var GridTrDokterColumnModelLookUpProduk_RWI =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		selectModel,
		// chkgetTindakanKasirRawatInap,
		{
			header			: 'KP PRODUK',
			dataIndex		: 'kp_produk',
			id				: 'col_kp_produk',
			width			: 15,
			menuDisabled	: true,
			hidden 			: false,
			filter: {},
		},{
			header			: 'DESKRIPSI',
			dataIndex		: 'deskripsi',
			id				: 'col_deskripsi',
			width			: 65,
			menuDisabled	: true,
			hidden 			: false,
			filter: {},
		},{
			header			: 'TARIF',
			dataIndex		: 'tarifx',
			width			: 35,
			menuDisabled	: true,
			hidden 			: false,
			renderer: function(v, params, record){
				 return formatCurrency(record.data.tarifx);
			 }
		}
	]);
    GridDokterTrKasir_RawatInap= new Ext.grid.EditorGridPanel({
        id          : 'formGridDokterTrKasir_RawatInap',
        stripeRows  : true,
        width       : '100%',
        height      : '100%',
        store       : dsDataStoreGridPoduk,
        border      : true,
        frame       : false,
        autoScroll  : true,
		plugins 	: [new Ext.ux.grid.FilterRow()],
		sm 			: selectModel,
		cm          : GridTrDokterColumnModelLookUpProduk_RWI,
        listeners   : {
           /*  rowclick: function( $this, rowIndex, e )
            {
                //trcellCurrentTindakan_RWJ = rowIndex;
            }, */
			rowclick : function (in_this, rowIndex, e) {
				var tmp_kd_kelas;
				for (var i = 0; i < in_this.selModel.selections.length; i++) {
					tmp_kd_kelas = in_this.selModel.selections.items[i].data.kd_klas;
				}

				if(tmp_kd_kelas.length > 0){
					for (var i = 0; i < cookie_value_add.length; i++) {
						if(cookie_value_add[i].kd_kelas == tmp_kd_kelas){
							cookie_value_add[i].kd_produk = 'none';
						}
					}
				}

				for (var i = 0; i < in_this.selModel.selections.length; i++) {
					var valueToPush = new Array();
						valueToPush["kd_produk"]   = in_this.selModel.selections.items[i].data.kd_produk;
						valueToPush["kd_kelas"]    = in_this.selModel.selections.items[i].data.kd_klas;
						valueToPush["kd_tarif"]    = in_this.selModel.selections.items[i].data.kd_tarif;
						valueToPush["deskripsi"]   = in_this.selModel.selections.items[i].data.deskripsi;
						valueToPush["tarifx"]      = in_this.selModel.selections.items[i].data.tarifx;
						valueToPush["tgl_berlaku"] = in_this.selModel.selections.items[i].data.tgl_berlaku;
						valueToPush["jumlah"]      = in_this.selModel.selections.items[i].data.jumlah;
					cookie_value_add.push(valueToPush);
				}
			}
        },
        viewConfig  : {forceFit: true},
    });
    var lebar = 500;
    var LookUpProduk_KasirRawatInap = new Ext.Window
    (
        {
            id: 'winTRformLookUpProduk_KasirRawatInap',
            title: 'Look Up Produk',
            closeAction: 'destroy',
            width: 950,
            height: 400,
            layout: {
				type:'hbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
            resizable: false,
            iconCls: 'Request',
			constrain : true,    
			modal: true,
           	items: [ 
				mComboDefaultUnitKelasRawatInap(),
				GridDokterTrKasir_RawatInap
			],
			tbar :
			[
				{
					xtype:'button',
					text:'Simpan',
					iconCls	: 'save',
					hideLabel:true,
					id: 'BtnOktrformLookUpProduk',
					handler:function(){
						kasir_urut = PenataJasaKasirRWI.dsGridTindakan.data.length;
						for (var i = 0; i < cookie_value_add.length; i++) {
							if (cookie_value_add[i].kd_produk != 'none') {
	                    		PenataJasaKasirRWI.dsGridTindakan.insert(PenataJasaKasirRWI.dsGridTindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
                                tmpkd_produk 			= cookie_value_add[i].kd_produk;
                                tmpkd_tarif            	= cookie_value_add[i].kd_tarif;
                                tmpdeskripsi           	= cookie_value_add[i].deskripsi;
                                tmpharga               	= cookie_value_add[i].tarifx;
                                tmptgl_berlaku         	= cookie_value_add[i].tgl_berlaku;
                                tmpjumlah              	= cookie_value_add[i].jumlah;
                                tmpstatus_konsultasi   	= cookie_value_add[i].kd_produk;

                                senderProduk(
                                    notransaksi, 
                                    tmpkd_produk, 
                                    tmpkd_tarif, 
                                    currentKdKasir_KasirRWI, 
                                    kasir_urut+1, 
                                    // PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('UNIT'), 
                                    CurrentKDUnitRWI, 
                                    Ext.getCmp('txtTanggalTransaksiKASIRRWI').getValue(),
                                    tmptgl_berlaku,
                                    1,
                                    tmpharga,
                                    'A',
                                    true,
                                    kasir_urut,
                                    'produk',
                                    cookie_value_add[i],
                                    false
                                );
                                PenataJasaKasirRWI.dsGridTindakan.data.items[kasir_urut].data.URUT               = kasir_urut;
                                kasir_urut++;
								// RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
							}
						}
						LookUpProduk_KasirRawatInap.close();
					}
				},
				'-',{
					xtype 	: 'label',
					text 	: tmpKeterangan,
				},
			],
            listeners:{ 
				close:function(){
					shortcut.remove('edit_dokter');
				},
				hide:function(){
					shortcut.remove('edit_dokter');
				}
            }
        }
    );
    // DataProduk();
	LookUpProduk_KasirRawatInap.show();
};




function mComboDefaultUnitKelasRawatInap(){
	var Tree = Ext.tree;
	var tree = new Tree.TreePanel({
		id:'pickerDefaultUnitKelasRWI',
		xtype : 'mytreegrid',
		cls : 'x-treegrid',
		title: namaunit, 
		animate:true, 
		useArrows:true,
		
		autoScroll:true,
		//loader: NavTreeLoader, 
		loader: new Ext.tree.TreeLoader({
			// url: baseURL + 'index.php/general/daftarbarang/lookup_produk/'+kodeunit, 
			// requestMethod: 'GET',
			// preloadChildren: true,

		            /*type: 'ajax',
		            url: 'assets/extjs-build/examples/layout-browser/tree-data.json'*/
		            type: 'ajax',
		            url: baseURL + 'index.php/main/functionRWI/tree_master',
		            actionMethods: {
		                read: 'POST'
		            },
		            reader: {
		                type: 'json',
		                root: '' // there is no root
		            },
		            root: {
		                children: []
		            },
		            autoLoad: false,
		}),
		enableDD:true,
		containerScroll: true,
		border: false,
		anchor:'100%',
        rootVisible: false,
		width: 200,
		height: '100%',
		dropConfig: {appendOnly:true},
		listeners: {
            'click': function(n)
            {
				if (n.leaf == true) {
					var grid = Ext.getCmp('pickerDefaultUnitKelasRWI');
					grid.setTitle(namaunit+' ('+n.text+')');
					LoadDataStoreProdukKasirRWI(n.id);
					CompUnitKelasRWJ = n.id;
                }
            }
        },
	});
	
	//new Tree.TreeSorter(tree, {folderSort:true});
	
	// set the root node
	var root = new Tree.AsyncTreeNode({
		text: 'Ext JS', 
		odeType: 'async',
		draggable:false, 
		expanded: true,
	});
	tree.setRootNode(root);
	
	root.expand(false, false);
	return tree;
};


	function DataProduk(){
		storeProduk = Ext.create('Ext.data.TreeStore', {
		        root: {
		            expanded: true
		        },
		        proxy: {
		            /*type: 'ajax',
		            url: 'assets/extjs-build/examples/layout-browser/tree-data.json'*/
		            type: 'ajax',
		            url: baseURL + 'index.php/MasterProduk/tree_master',
		            actionMethods: {
		                read: 'POST'
		            },
		            reader: {
		                type: 'json',
		                root: '' // there is no root
		            },
		            root: {
		                children: []
		            },
		            autoLoad: false,
		        }
		    });
	};

function LoadDataStoreProdukKasirRWI(kd_klas){
	var Field                = ['kd_produk','deskripsi', 'tarifx', 'kp_produk'];
	DataStoreProduk_KasirIGD = new WebApp.DataStore({ fields: Field });

	Ext.Ajax.request({
		url: baseURL +  "index.php/main/functionRWI/getLookUpProdukList",
		params: {
			kd_klas 	: kd_klas,
			kd_unit 	: tmpkd_unitKamar,
			kd_customer : vkode_customer_PJRWI,
			kd_tarif 	: kodepay,
		},

		success: function(response) {
			//var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
			var cst       = Ext.decode(response.responseText);
			//dsDataPerawatPenindak_KASIR_RWI.load(myData);
			dsDataStoreGridPoduk.removeAll();
			for(var i     =0,iLen=cst['data'].length; i<iLen; i++){
				var recs = [],recType = DataStoreProduk_KasirIGD.recordType;
				var o    = cst['data'][i];
				recs.push(new recType(o));
				dsDataStoreGridPoduk.add(recs);
			}
		},
	});
}


function TransferLookUp(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_RWI = new Ext.Window
    (
        {
            id: 'gridTransfer',
            title: 'Transfer Rawat Inap',
            closeAction: 'destroy',
            width: lebar,
            height: 420,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRTransfer_RWI(lebar),
			fbar:[
				{
					xtype:'button',
					text:'Simpan',
					width:70,
					style:{'margin-left':'0px','margin-top':'0px'},
					hideLabel:true,
					id: 'btnOkTransfer',
					handler:function()
					{
						Ext.Ajax.request ({
							url: baseURL + "index.php/main/functionRWI/get_tarif_produk_bayi",
							params 	: {
								kd_unit 	: tmp_pasien_transfer[6],
								kd_tarif 	: tmp_pasien_transfer[13],
								no_transaksi: tmp_pasien_transfer[3],
								kd_kasir 	: tmp_pasien_transfer[7],
							},
							success: function(data) {
								var cst_bayi = Ext.decode(data.responseText);

								tmpkd_produk 	= cst_bayi.kd_produk;
								tmpkd_tarif 	= tmp_pasien_transfer[13];
								tmpkd_unit 		= rowSelectedKasirrwiKasir.data.KD_UNIT;
								// var cst_unit_asal = Ext.decode(data.responseText);
								if (cst_bayi.status === true){
									senderProduk(
										Ext.getCmp('txtTranfernoTransaksiKasirRWI').getValue(), 
										cst_bayi.kd_produk, 
										tmp_pasien_transfer[13], 
										tmp_pasien_transfer[7], 
										cst_bayi.urut, 
										tmp_pasien_transfer[6], 
										Ext.getCmp('dtpTanggalTransfertransaksibayarKasirRWI').getValue(), 
										cst_bayi.tgl_berlaku, 
										1, 
										Ext.getCmp('txtjumlahtranfer_KasirRWI').getValue(), 
										'E',
										true,
										parseInt(cst_bayi.urut), 
										'produk', 
										rowSelectedKasirrwiKasir.data, 
										false,
										tmp_pasien_transfer
									);
								}
							},
							failure: function(o) {

							}

						});
						// TransferData_IGD(false);	
					}
				},
				{
						xtype:'button',
						text:'Tutup',
						width:70,
						hideLabel:true,
						id: 'btnCancelTransfer',
						handler:function() 
						{
							FormLookUpsdetailTRTransfer_RWI.close();
						}
				}
			],
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRTransfer_RWI.show();
  //  Transferbaru();

};

function paramsTransfer_KasirRWI(rowdata, urut = null, tmp_pasien_transfer = null){
	var tmp_list = "";
	// var tgl_transaksi_asal = data_pasien_lama.TANGGAL_TRANSAKSI;
	var params = {
		kdUnit 				: tmpKD_unit_tr,
		TrKodeTranskasi 	: rowdata.NO_TRANSAKSI,
		Tgl 				: Ext.getCmp('dtpTanggalTransfertransaksibayarKasirRWI').getValue(),
		Shift 				: tampungshiftsekarang,
		Flag 				: rowdata.FLAG,
		bayar 				: 'TR',
		Typedata 			: tampungtypedata,
		Totalbayar 			: Ext.getCmp('txtjumlahtranfer_KasirRWI').getValue(),
		List 				: getArrDetailTrKasirrwi(),
		JmlField 			: mRecordKasirrwi.prototype.fields.length - 4,
		JmlList 			: dsTRDetailKasirrwiKasirList.data.length,
		Hapus 				: 1,
		Ubah 				: 0,
		urut_masuk 			: rowdata.URUT_MASUK,
		urut_asal			: urut,
		no_transaksi_asal 	: tmp_pasien_transfer[3],
		kd_kasir_asal 		: tmp_pasien_transfer[7],
		tgl_transaksi_asal	: Ext.getCmp('dtpTanggalTransfertransaksibayarKasirRWI').getValue(),
		alasan 				: Ext.getCmp('cboalasan_transferKasir_RWI').getValue(),
		kd_kasir 			: 'default_kd_kasir_rwi',
	};


	return params;
}

function getFormEntryTRTransfer_RWI(lebar) 
{
    var pnlTRTransfer_RWI = new Ext.FormPanel
    (
        {
            id: 'PanelTRTransfer_RWI',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:420,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputTransfer_KasirRWI(lebar)],
			tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanTransfer_RWI = new Ext.Panel
	(
		{
		    id: 'FormDepanTransfer_RWI',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [
		    pnlTRTransfer_RWI	
				
			]

		}
	);

    return FormDepanTransfer_RWI;
};


function getItemPanelInputTransfer_KasirRWI(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    // bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:330,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
				height:330,
			    border: false,
			    items:
				[
					getTransfertujuan_KasirRWI(lebar),
					{	 
						xtype: 'tbspacer',
						height: 5
					},	
					getItemPanelNoTransksiTransfer_KasirRWI(lebar),
					{	 
						xtype: 'tbspacer',									
						height:3
					},
					mComboalasan_transfer_KasirRWI()
				]
			},
		]
	};
    return items;
};

function mComboalasan_transfer_KasirRWI() 
{
	var Field = ['KD_ALASAN','ALASAN'];

	var dsalasan_transfer_KasirRWI = new WebApp.DataStore({ fields: Field });
	dsalasan_transfer_KasirRWI.load
	(
		{
			params:
			{
				Skip: 0,
				Take: 1000,
				Sort: 'kd_alasan',
				Sortdir: 'ASC',
				target: 'ComboAlasanTransfer',
				param: "" //+"~ )"
			}
		}
	);
	
    var cboalasan_transferKasir_RWI = new Ext.form.ComboBox
	(
		{
		    id: 'cboalasan_transferKasir_RWI',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Alasan Transfer ',
		    align: 'Right',
			width: 100,
		    value:'Pembayaran Disatukan',
		    anchor:'100%',
		    store: dsalasan_transfer_KasirRWI,
		    valueField: 'ALASAN',
		    displayField: 'ALASAN',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{			
					//
			    }

			}
		}
	);
	
    return cboalasan_transferKasir_RWI;
};


function getTransfertujuan_KasirRWI(lebar) 
{
    var items =
	{
		Width		: lebar-2,
		height		: 160,
		layout		: 'form',
		border		: true,
		labelWidth	: 130,		
	    items:
		[
			{	 
				xtype: 'tbspacer',
				height: 2
			},
			{
				xtype 		: 'textfield',
				fieldLabel 	: 'No Medrec',
				//maxLength: 200,
				name 		: 'txtTranfernomedrecKasirRWI',
				id 		 	: 'txtTranfernomedrecKasirRWI',
				labelWidth 	:130,
				width 		: 100,
				anchor 		: '95%',
				listeners 	:
				{
					specialkey: function (){

						var tmpNoMedrec = Ext.get('txtTranfernomedrecKasirRWI').getValue()
						if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
						{
							if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
							{
								var tmpgetNoMedrec = formatnomedrec(Ext.get('txtTranfernomedrecKasirRWI').getValue());
								Ext.getCmp('txtTranfernomedrecKasirRWI').setValue(tmpgetNoMedrec);
								TransferSeleksiTanggalLookUp();
								datarefresh_dataSourceKunjungan();
							}else{
								if (tmpNoMedrec.length === 10)
								{
									TransferSeleksiTanggalLookUp();
									datarefresh_dataSourceKunjungan();
								} else
									Ext.getCmp('txtTranfernomedrecKasirRWI').setValue('')
							}
						}
					}
				},
			},{
				xtype 	 	: 'datefield',
				fieldLabel 	: 'Tanggal Trans',
				id 	 		: 'dtpTanggalTransfertransaksiKasirRWI',
				name 	 	: 'dtpTanggalTransfertransaksiKasirRWI',
				format 		: 'd/M/Y',
				readOnly  	: true,
				labelWidth 	:130,
				width 		: 100,
				anchor 		: '95%'
			},{
				xtype 		: 'textfield',
				fieldLabel 	: 'No Transaksi',
				name 		: 'txtTranfernoTransaksiKasirRWI',
				id 			: 'txtTranfernoTransaksiKasirRWI',
				labelWidth 	: 130,
				width 		: 100,
				readOnly  	: true,
				anchor 		: '95%'
			},{
				xtype 	 	: 'textfield',
				fieldLabel 	: 'Nama Pasien',
				//maxLength: 200,
				name 		: 'txtTranfernamapasienKasirRWI',
				id  		: 'txtTranfernamapasienKasirRWI',
				labelWidth 	:130,
				readOnly  	: true,
				width 	 	: 100,
				anchor 	 	: '95%'
			},{
				xtype 		: 'textfield',
				fieldLabel 	: 'Unit Perawatan',
				//maxLength: 200,
				name 		: 'txtTranferunitKasirRWI',
				id 			: 'txtTranferunitKasirRWI',
				labelWidth 	:130,
				width 		: 100,
				hidden  	: true,
				anchor 		: '95%'
			},{
				xtype 		: 'textfield',
				fieldLabel 	: 'Unit Perawatan',
				//maxLength : 200,
				name 		: 'txtTranferkelaskamarRWI',
				id 			: 'txtTranferkelaskamarRWI',
				readOnly  	: true,
				labelWidth 	:130,
				width 		: 100,
				anchor 		: '95%'
			},
			{
				xtype 		: 'datefield',
				fieldLabel 	: 'Tanggal Transfer',
				id 			: 'dtpTanggalTransfertransaksibayarKasirRWI',
				name 		: 'dtpTanggalTransfertransaksibayarKasirRWI',
				format 		: 'd/M/Y',
				value  		: now,
				readOnly  	: false,
				labelWidth 	: 130,
				width 		: 100,
				anchor 		: '95%'
			},			
		]
	}
    return items;
};


function getItemPanelNoTransksiTransfer_KasirRWI(lebar) 
{
    var items =
	{
		Width:lebar,
		height:110,
	    layout: 'column',
	    border: true,
		
		
	    items:
		[
			{
				columnWidth	: .990,
				layout		: 'form',
				Width		: lebar-10,
				labelWidth	: 130,
				border		: false,
				items		:
				[
					{	 
						xtype	: 'tbspacer',
						height	: 3
					},{
						xtype		: 'textfield',
						fieldLabel	: 'Jumlah Biaya',
						maxLength	: 200,
						style		: {'text-align':'right'},
						name		: 'txtjumlahbiayasal_KasirRWI',
						id			: 'txtjumlahbiayasal_KasirRWI',
						width		: 100,
						value 		: tmp_total_tagihan,
						readOnly 	: true,
						anchor		: '95%'
					},{
						xtype 		: 'textfield',
						fieldLabel 	: 'Paid',
						maxLength 	: 200,
						style 		: {'text-align':'right'},
						name 		: 'txtpaid_KasirRWI',
						id 			: 'txtpaid_KasirRWI',
						width 		: 100,
						value 		: 0,
						readOnly 	: true,
						value 		: tmp_jumlah_bayar,
						anchor 		: '95%'
					},{
						xtype 		: 'textfield',
						fieldLabel 	: 'Jumlah dipindahkan',
						maxLength 	: 200,
						style 		: {'text-align':'right'},
						name 		: 'txtjumlahtranfer_KasirRWI',
						id 			: 'txtjumlahtranfer_KasirRWI',
						readOnly 	: false,
						enableKeyEvents:true,
						value 		: tmp_sisa_bayar,
						width 		: 100,
						anchor 		: '95%',
						listeners:
						{ 
							keyup:function(text,e)
							{
								// getsaldotagihan_KasirIGD();
							}
						}
					},{
						xtype 		: 'textfield',
						fieldLabel 	: 'Saldo tagihan',
						maxLength 	: 200,
						style 		: {'text-align':'right'},
						name 		: 'txtsaldotagihan_KasirRWI',
						id 			: 'txtsaldotagihan_KasirRWI',
						value 		: 0,
						value 		: tmp_sisa_bayar,
						readOnly 	: true,
						width 		: 100,
						anchor 		: '95%'
					}
				]
			}
			
		]
	}
    return items;
};


function getFormSeleksiTGLMasukTransfer_RWI(lebar) 
{
    var pnlFormSeleksiTGLMasukRWI = new Ext.FormPanel
    (
        {
            id: 'FormSeleksiTGLMasuk_RWI',
            fileUpload: true,
            region: 'north',
            layout: 'form',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:240,
            width: lebar,
            border: false,
            items: [
				{
					xtype 		: 'textfield',
					fieldLabel 	: 'Medrec',
					maxLength 	: 200,
					name 		: 'txtSeleksiTanggalMedrec_KasirRWI',
					id 			: 'txtSeleksiTanggalMedrec_KasirRWI',
					value 		: 0,
					value 		: Ext.getCmp('txtTranfernomedrecKasirRWI').getValue(),
					readOnly 	: true,
					width 		: 100,
					anchor 		: '95%'
				},{
					xtype 		: 'textfield',
					fieldLabel 	: 'Nama Pasien',
					maxLength 	: 200,
					name 		: 'txtSeleksiTanggalNama_KasirRWI',
					id 			: 'txtSeleksiTanggalNama_KasirRWI',
					value 		: 0,
					// value 		: Ext.getCmp('txtTranfernomedrecKasirRWI').getValue(),
					readOnly 	: true,
					width 		: 100,
					anchor 		: '95%'
				},
				mComboKunjunganPasien_KasirRWI(),
			],
			tbar:
            [
            ]
        }
    );
 
    var FormDepanFormSeleksiTGLMasuk_RWI = new Ext.Panel
	(
		{
		    id: 'FormDepanFormSeleksiTGLMasuk_RWI',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [
		    	pnlFormSeleksiTGLMasukRWI	
			]

		}
	);

    return FormDepanFormSeleksiTGLMasuk_RWI;
};


function TransferSeleksiTanggalLookUp() 
{
    var lebar = 440;
    FormLookUpSeleksiTanggalTRTransfer_RWI = new Ext.Window
    (
        {
            id: 'gridSeleksiTanggalTransfer',
            title: 'Kunjungan Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 240,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormSeleksiTGLMasukTransfer_RWI(lebar),
			fbar:[
				{
					xtype:'button',
					text:'Simpan',
					width:70,
					style:{'margin-left':'0px','margin-top':'0px'},
					hideLabel:true,
					id: 'btnOkSeleksiTanggalTransfer',
					handler:function()
					{
						Ext.Ajax.request ({
							url: baseURL + "index.php/main/GetPasienTranfer",
							params: {
								notr 		: Ext.getCmp('txtSeleksiTanggalMedrec_KasirRWI').getValue(),
								tgl_masuk 	: Ext.getCmp('cboDataKunjungan_KasirRWI').getValue(),
							},
							success: function(o) {	
								o.responseText
								var tmphasil = o.responseText;
								var tmp = tmphasil.split("<>");
								Ext.getCmp('txtTranfernoTransaksiKasirRWI').setValue(tmp[3]);
								Ext.getCmp('dtpTanggalTransfertransaksiKasirRWI').setValue(ShowDate(tmp[4]));
								Ext.getCmp('txtTranfernamapasienKasirRWI').setValue(tmp[2]);
								Ext.getCmp('txtTranferkelaskamarRWI').setValue(tmp[9]);
								tmpKD_unit_tr = tmp[8];
								var kasir=tmp[7];
								tmp_pasien_transfer = tmp;
								RefreshDataKasirrwiKasirDetail(rowSelectedKasirrwiKasir.data.NO_TRANSAKSI);
							},
							failure: function(o) {
								ShowPesanWarningKasirigd('Data Tidak dapat di transfer,hubungi Admin', 'Gagal');
								FormLookUpSeleksiTanggalTRTransfer_RWI.close();
							}

						});
						FormLookUpSeleksiTanggalTRTransfer_RWI.close();
					}
				},
				{
					xtype:'button',
					text:'Tutup',
					width:70,
					hideLabel:true,
					id: 'btnCancelSeleksiTanggalTransfer',
					handler:function() 
					{
						FormLookUpSeleksiTanggalTRTransfer_RWI.close();
					}
				}
			],
            listeners:
            {
                
            }
        }
    );

    FormLookUpSeleksiTanggalTRTransfer_RWI.show();
  //  Transferbaru();

};

function datarefresh_dataSourceKunjungan()
{
    loadMask.show();
    dsDataSource_kunjungan.removeAll();
    Ext.getCmp('btnOkSeleksiTanggalTransfer').disable(); 
    Ext.Ajax.request({
        url: baseURL + "index.php/rawat_inap/data_kunjungan/kunjungan",
        params: {
            kd_pasien : Ext.getCmp('txtSeleksiTanggalMedrec_KasirRWI').getValue(),
        },
        success: function(response) {
            //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
            var cst  = Ext.decode(response.responseText);
            //dsDataPerawatPenindak_KASIR_RWI.load(myData);
            if (cst['DATA'].length > 0) {
                for(var i=0,iLen=cst['DATA'].length; i<iLen; i++){
                    var recs    = [],recType = dsDataSource_kunjungan.recordType;
                    var o       = cst['DATA'][i];
                    if (cst['DATA'][i].TANGGAL_KELUAR == null) {
                    	recs.push(new recType(o));
                    	dsDataSource_kunjungan.add(recs);
                    }
                }
                Ext.getCmp('txtSeleksiTanggalNama_KasirRWI').setValue(cst.NAMA);
            }else{
                ShowPesanWarningBukaTransaksi("Pasien belum pernah Rawat Inap", "Information");
            }
            loadMask.hide();
        },     
    });
}


function mComboKunjunganPasien_KasirRWI()
{
    var Field = ['TANGGAL_MASUK'];
    dsDataSource_kunjungan = new WebApp.DataStore({ fields: Field });
    var cboDataKunjungan_KasirRWI = new Ext.form.ComboBox
    (
        {

            id:'cboDataKunjungan_KasirRWI',
            typeAhead       : true,
            triggerAction   : 'all',
            lazyRender      : true,
            mode            : 'local',
            selectOnFocus   : true,
            forceSelection  : true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: 'Tgl Kunjungan ',
            align: 'Right',
            width: 150,
            // value: now.format("Y-m-d"),
            store: dsDataSource_kunjungan,
            valueField: 'TANGGAL_MASUK',
            displayField: 'TANGGAL_MASUK',
            listeners:
            {
                select : function(a, b){
    				Ext.getCmp('btnOkSeleksiTanggalTransfer').enable(); 
                }      
            }
        }
    );
    return cboDataKunjungan_KasirRWI;
};


/*
	FORM HISTORY PINDAH KAMAR
	OLEH 	: HADAD AL GOJALI
	TANGGAL : 2017 - 04 - 20 
	
 */

function PilihHistoryPindahKamarLookUp_KasirRWI(data) 
{
	// loaddatastoreTarifComponent(data);
        var formLookupDetailHistoryPindahKamar_KasirRWI = new Ext.Window
		(
			{
				id: 'formLookupDetailHistoryPindahKamar_KasirRWI',
				title: 'History Pindah Kamar',
				closeAction: 'destroy',
				width: 800,
				height: 400,
				border: false,
				resizable: false,
				plain: true,
				layout: 'fit',
				iconCls: 'Request',
				modal: true,
				bodyStyle:'padding: 3px;',
				items: [
                {
					columnWidth: .40,
					layout: 'form',
					labelWidth: 100,
					border: false,
					bodyStyle:'padding: 10px;',
					items:[
						FieldsetRuanganLama(),
						/*{
							xtype 		: 'textfield',
							id 			: 'TxtNamaProduk',
							fieldLabel 	: 'Produk',
							anchor		: '100%',
							readOnly    : true,
                            value		: data.DESKRIPSI,
                            bodyStyle	: 'margin:10px;',
						},*/
						GridHistoryPindahKamar_KasirRWI(),
                        // paneltotal
					]
				},
				],
				listeners:
				{
				
				}
            }
		);
        formLookupDetailHistoryPindahKamar_KasirRWI.show();
};



    function loaddatastoreHistoryPindahKamar(){
        Ext.Ajax.request({
			url: baseURL +  "index.php/rawat_inap/control_pindah_kamar/history_pindah_kamar",
			params: {
				kd_unit  	: rowSelectedKasirrwiKasir.data.KD_UNIT,
				tgl_masuk 	: rowSelectedKasirrwiKasir.data.TANGGAL_MASUK,
				kd_pasien 	: rowSelectedKasirrwiKasir.data.KD_PASIEN,
				urut_masuk 	: rowSelectedKasirrwiKasir.data.URUT_MASUK,
			},
            success: function(response) {
                //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
                var cst  = Ext.decode(response.responseText);
                //dsDataPerawatPenindak_KASIR_RWI.load(myData);
                for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                    var recs    = [],recType = DataStoreGridHistoryPindah.recordType;
                    var o       = cst['data'][i];
                    recs.push(new recType(o));
                    tmp_data_inap 	= recs[0].data.kd_unit_kamar;
                    tmpkd_unitKamar = recs[0].data.kd_unit_kamar;
                    tmpkd_unit 		= recs[0].data.kd_unit_kamar;
					Ext.getCmp('cboDataTindakanMenyusul_KASIRRWI').setValue(recs[0].data.nama_kamar);
                    dsDataStoreGridHistoryPindah.add(recs);
                }
            },
        });
    }

    function GridHistoryPindahKamar_KasirRWI(){
    	var rowLine = 0;
    	var rowData = "";
		/*dsDataStoreGridHistoryPindah.removeAll();        
		var Field                  = ['nama_kamar','tgl_inap','jam_inap','tgl_keluar','jam_keluar','lama_rawat','urut_nginap'];
		DataStoreGridHistoryPindah = new WebApp.DataStore({ fields: Field });
		loaddatastoreHistoryPindahKamar();*/
		var cm = new Ext.grid.ColumnModel({
			// specify any defaults for each column
			defaults: {
				sortable: false // columns are not sortable by default           
			},
			columns: [ 
				{
					header 		: 'Urut Nginap',
					dataIndex 	: 'urut_nginap',
					width 		: 200,
					hidden 		: false,
					align 		: 'center',
				},{
					header 		: 'Kamar',
					dataIndex 	: 'nama_kamar',
					width 		: 200,
					hidden 		: false,
				}, {
					header 		: 'Tgl Inap',
					dataIndex 	: 'tgl_inap',
					width 		: 200,
					hidden 		: false,
				}, {
					header 		: 'Jam Inap',
					dataIndex 	: 'jam_inap',
					width 		: 200,
					hidden 		: false,
				}, {
					header 		: 'Tgl Keluar',
					dataIndex 	: 'tgl_keluar',
					width 		: 200,
					hidden 		: false,
				}, {
					header 		: 'Jam Keluar',
					dataIndex 	: 'jam_keluar',
					width 		: 200,
					hidden 		: false,
				}, {
					header 		: 'Lama Rawat',
					dataIndex 	: 'lama_rawat',
					width 		: 200,
					hidden 		: false,
				},
			]
	    });

        gridHistoryPindahKamar_Kasirrwi = new Ext.grid.EditorGridPanel
        (
            {
				title: '',
				id: 'gridHistoryPindahKamar_Kasirrwi',
				store: dsDataStoreGridHistoryPindah,
				clicksToEdit: 1, 
				editable: true,
				border: true,
				columnLines: true,
				frame: false,
				stripeRows       : true,
				trackMouseOver   : true,
				height: 240,
				width: 755,
				autoScroll: true,
				sm: new Ext.grid.CellSelectionModel
				(
					{
						singleSelect: true,
						listeners:
						{
						}
					}
				),
				sm: new Ext.grid.RowSelectionModel
				(
					{
						singleSelect: true,
						listeners:
						{
							rowselect: function (sm, row, rec)
							{
								rowLine = row;
							}
						}
					}
				),
                cm: cm,
                viewConfig: {
                    forceFit: true
                },  
            }
        );
        return gridHistoryPindahKamar_Kasirrwi;
    };
RefreshDataKasirrwiKasir();

function LookUpEditKwitansiPanel(rowdata)
{
    console.log(rowdata);
    //console.log(tot_deposit);
    if (rowdata != undefined){
        $TotNilai = rowdata;
    }else {
        $TotNilai = Ext.getCmp('txtJumlahBayar_KASIRRWI').getValue();
    }			


    var no_kwitansi;
	// console.log(Ext.getCmp('gridDTLTRKasirigd').getStore().data.items);
	/*var items=Ext.getCmp('gridDTLTRKasirigd').getStore().data.items;
	var parameter = [];
	var xparameter = "";
	for(var i=0,iLen=items.length; i<iLen; i++){
		if(items[i].data.TAG==true){
			// parameter.push(items[i].data.URUT);
			xparameter += "'"+items[i].data.URUT+"',";
		}
	}
*/
	var lebar          = 440;
	LookUpEditKwitansi = new Ext.Window
    (
        {
			id 			: 'panelEditKwitansi',
			title 		: 'Cetak Kwitansi',
			closeAction : 'destroy',
			width 		: lebar,
			height 		: 430,
			border 		: false,
			resizable 	: false,
			plain 		: false,
			layout 		: 'fit',
			iconCls 	: 'Request',
			modal 		: true,
			items: 	[
						{
							id 			: 'panelInnerEditKwitansi',
							region 		: 'north',
							layout 		: 'column',
							bodyStyle 	: 'padding:10px 10px 10px 10px',
							height 		: 420,
							anchor 		: '100%',
							width 		: lebar,
							border 		: false,
							items 		: [
								{

									layout 		: 'fit',
									anchor 		: '100%',
									width 		: lebar-35,
									labelAlign 	: 'right',
									bodyStyle 	: 'padding:10px 10px 10px 0px',
									border 		:false,
									height 		:340,
									items:
									[
										{
										    columnWidth : .9,
										    width 		: lebar -35,
											labelWidth 	: 100,
										    layout 		: 'form',
											height 		: 340,
										    border 		: false,
										    items:
											[ 
												{
													Width 		: lebar-2,
													height 		: 50,
												    layout 		: 'form',
												    border 		: true,
													bodyStyle 	: 'padding:10px 10px 10px 10px',
													labelWidth 	: 130,
												    items:
													[
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'Kasir',
															maxLength 	: 200,
															name 		: 'txtKodeKasir',
															id 			: 'txtKodeKasir',
															width 		: 100,
															readOnly 	: true,
															anchor 		: '100%',
															value 		:  rowSelectedKasirrwiKasir.data.KD_KASIR,
															style 		: {'text-align':'left'},
														},
													]
												},
												{
													xtype: 'tbspacer',
													height: 5
												},
												{
													Width 		: lebar-2,
													height 		: 260,
													layout 		: 'form',
													border 		: true,
													bodyStyle 	: 'padding:10px 10px 10px 10px',
													labelWidth 	: 130,
													items:
													[
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'Medrec',
															maxLength 	: 200,
															name 		: 'txtKodePasien',
															id 			: 'txtKodePasien',
															width 		: 100,
															readOnly 	: true,
															anchor 		: '100%',
															value 		: rowSelectedKasirrwiKasir.data.KD_PASIEN,
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'No Transaksi',
															maxLength 	: 200,
															name 		: 'txtNoTransaksi',
															id 			: 'txtNoTransaksi',
															width 		: 100,
															readOnly 	: true,
															anchor 		: '100%',
															value 		: rowSelectedKasirrwiKasir.data.NO_TRANSAKSI,
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'No Kwitansi',
															maxLength 	: 200,
															name 		: 'txtNoKwitansi',
															id 			: 'txtNoKwitansi',
															width 		: 100,
															readOnly 	: true,
															anchor 		: '100%',
															// value 		: no_kwitansi,
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'Nama Pembayar',
															maxLength 	: 200,
															name 		: 'txtNamaPembayar',
															id 			: 'txtNamaPembayar',
															width 		: 100,
															readOnly 	: false,
															anchor 		: '100%',
															value 		: rowSelectedKasirrwiKasir.data.NAMA,
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textarea',
															fieldLabel 	: 'Untuk pembayaran',
															maxLength 	: 200,
															name 		: 'txtKeteranganBayar',
															id 			: 'txtKeteranganBayar',
															width 		: 100,
															readOnly 	: false,
															anchor 		: '100%',
															value 		: "Untuk Pembayaran Biaya Rawat Inap",
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textfield',
															fieldLabel 	: '',
															maxLength 	: 200,
															name 		: 'txtKeteranganPembayar',
															id 			: 'txtKeteranganPembayar',
															width 		: 100,
															readOnly 	: false,
															anchor 		: '100%',
															value 		: "a/n "+rowSelectedKasirrwiKasir.data.NAMA,
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'Unit yang dituju',
															maxLength 	: 200,
															name 		: 'txtUnitTujuan',
															id 			: 'txtUnitTujuan',
															width 		: 100,
															readOnly 	: true,
															anchor 		: '100%',
															// value 		: "a/n "+Ext.getCmp('txtNamaPasienDetransaksi').getValue(),
															style 		: {'text-align':'left'},
                                                        },
                                                        
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'Jumlah bayar',
															maxLength 	: 200,
															name 		: 'txtJumlahBayar',
															id 			: 'txtJumlahBayar',
															width 		: 100,
															readOnly 	: false,
															anchor 		: '100%',
                                                            
                                                            //value 		: Ext.getCmp('txtJumlahBayar_KASIRRWI').getValue(),
                                                            value 		: $TotNilai,
                                                            style 		: {'text-align':'left'},
														},
													]
												},
											]
										},
									]
								}
							],
						},
					],
			fbar:[
				{
					xtype:'button',
					text:'Print',
					width:70,
					style:{'margin-left':'0px','margin-top':'0px'},
					hideLabel:true,
					id: 'btnOkPrint',
					handler:function(){
						var url_laporan = baseURL + "index.php/laporan/lap_kwitansi/";
						var params={
							kd_kasir 		: Ext.getCmp('txtKodeKasir').getValue(),
							kd_pasien 		: Ext.getCmp('txtKodePasien').getValue(),
							no_transaksi 	: Ext.getCmp('txtNoTransaksi').getValue(),
							no_kwitansi		: Ext.getCmp('txtNoKwitansi').getValue(),
							nama_pembayar	: Ext.getCmp('txtNamaPembayar').getValue(),
							ket_bayar		: Ext.getCmp('txtKeteranganBayar').getValue(),
							ket_pembayar	: Ext.getCmp('txtKeteranganPembayar').getValue(),
							unit_tujuan		: Ext.getCmp('txtUnitTujuan').getValue(),
							jumlah_bayar	: Ext.getCmp('txtJumlahBayar').getValue(),
							// no_urut			: xparameter,
						};
						console.log(params);
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", url_laporan+"/print_rwi_");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();  
						// printbillIGD();
					}
				},
				{
						xtype:'button',
						text:'Tutup',
						width:70,
						hideLabel:true,
						id: 'btnCancelTransfer',
						handler:function()
						{
							LookUpEditKwitansi.close();
						}
				}
			],
			listeners:
			{
			
			}
		}
	);

	Ext.Ajax.request({
		url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
		params 		: {
			select 	: " max(no_nota) as kwitansi ",
			where 	: " kd_kasir = '"+ rowSelectedKasirrwiKasir.data.KD_KASIR+"' ",
			table 	: " nota_bill ",
		},
		success: function(o){
			var cst 	= Ext.decode(o.responseText);
			no_kwitansi = parseInt(cst[0].kwitansi)+1;
			Ext.getCmp('txtNoKwitansi').setValue(no_kwitansi);
		}
	});

	Ext.Ajax.request({
		url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
		params 		: {
			select 	: " no_transaksi, kd_unit ",
			where 	: " no_transaksi = '"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"' and kd_kasir = '"+ rowSelectedKasirrwiKasir.data.KD_KASIR+"' ",
			table 	: " transaksi ",
		},
		success: function(o){
			var cst 	= Ext.decode(o.responseText);

			Ext.Ajax.request({
				url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
				params 		: {
					select 	: " nama_unit, kd_unit ",
					where 	: " kd_unit = '"+cst[0].kd_unit+"' ",
					table 	: " unit ",
				},
				success: function(o){
					var cst 	= Ext.decode(o.responseText);
					Ext.getCmp('txtUnitTujuan').setValue(cst[0].nama_unit);
					// Ext.getCmp('txtKeteranganBayar').setValue("Untuk Pembayaran Biaya "+cst[0].nama_unit);
				}
			});
		}
	});
	// rowSelectedKasirrwiKasir.data
	if((rowSelectedKasirrwiKasir.data.KD_CUSTOMER=='0000000002' || rowSelectedKasirrwiKasir.data.KD_CUSTOMER=='0000000009') && (rowSelectedKasirrwiKasir.data.PINDAH_KELAS=='1' || rowSelectedKasirrwiKasir.data.PINDAH_KELAS=='2' || rowSelectedKasirrwiKasir.data.PINDAH_KELAS=='3' || rowSelectedKasirrwiKasir.data.PINDAH_KELAS=='VIP')){
		// getInacbgPenjasRwi(rowdata.NO_SJP);

		Ext.Ajax.request({
			url: baseURL + "index.php/main/eklaim?sep="+rowSelectedKasirrwiKasir.data.NO_SJP+"&metode=get",
			params: {
				metode 	: 'get',
				no_transaksi: rowSelectedKasirrwiKasir.data.NO_TRANSAKSI,
			},
			failure: function (o){},
			success: function (o){
				var cst = Ext.decode(o.responseText);
				var tagihan=0;
				var dijamin=cst.response.data.grouper.response.cbg.tariff;
				var vip=0;
				var kelas='';
				if(cst.metadata.code==200){
					if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='1'){
						tagihan=cst.response.data.grouper.tarif_alt[0].tarif_inacbg;
					}else if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='2'){
						tagihan=cst.response.data.grouper.tarif_alt[1].tarif_inacbg;
					}else if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='3'){
						tagihan=cst.response.data.grouper.tarif_alt[2].tarif_inacbg;
					}else if(rowSelectedKasirrwiKasir.data.KELAS_RAWAT=='VIP'){
						tagihan=cst.response.data.grouper.tarif_alt[0].tarif_inacbg;
						// vip=((cst.response.data.grouper.tarif_alt[0].tarif_inacbg/1000)*7.5);
						vip=( (cst.response.data.grouper.tarif_alt[0].tarif_inacbg*0.75) );
						if(rowSelectedKasirrwiKasir.data.HAK_KELAS=='1'){
							// dijamin=0;
							dijamin=cst.response.data.grouper.tarif_alt[0].tarif_inacbg;
						}else if(rowSelectedKasirrwiKasir.data.HAK_KELAS=='2'){
							dijamin=cst.response.data.grouper.tarif_alt[1].tarif_inacbg;
							
						}else if(rowSelectedKasirrwiKasir.data.HAK_KELAS=='3'){
							dijamin=cst.response.data.grouper.tarif_alt[2].tarif_inacbg;
						}
					}
					Ext.getCmp('txtJumlahBayar').setValue(tagihan-dijamin+Math.round(vip));
				}
			}
		});
	}
	/*Ext.Ajax.request({
		url 		: baseURL + "index.php/main/Controller_kunjungan/get_harga_data",
		params 		: {
			select 		: " sum(qty*harga) as total ",
			field 		: " urut ",
			parameter 	: xparameter,
			where 		: " no_transaksi = '"+rowSelectedKasirrwiKasir.data.NO_TRANSAKSI+"' and kd_kasir = '"+rowSelectedKasirrwiKasir.data.KD_KASIR+"'",
			table 		: " detail_transaksi ",
		},
		success: function(o){
			var cst 	= Ext.decode(o.responseText);
			Ext.getCmp('txtJumlahBayar').setValue(cst[0].total);
		}
	});*/
	LookUpEditKwitansi.show();
};

// ViewMrStatusPulang
function loaddatastoreStatusPulang(){
    dsStatusPulangPasien.load({
		params:
		{
			Skip: 0,
			Take: 1000,
			//Sort: 'DEPT_ID',
			Sort: '',
			Sortdir: 'ASC',
			target: 'ViewComboStatusPulang',
			param: " kd_bagian=~1~ "
		}
	});
}
function loaddatastoreCaraKeluar(){
    dsCaraKeluar.load({
		params:
		{
			Skip: 0,
			Take: 1000,
			//Sort: 'DEPT_ID',
			Sort: '',
			Sortdir: 'ASC',
			target: 'ViewComboCaraKeluar',
			param: " "
		}
	});
}