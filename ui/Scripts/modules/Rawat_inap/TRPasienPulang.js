// Data Source ExtJS # --------------

/**
*	Nama File 		: TRKasirIgd.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Kasir IGD adalah proses untuk pembayaran pasien pada IGD
*	Di buat tanggal : 18 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Kasir IGD # --------------

var dataSource_viPasienPulang;
var selectCount_viPasienPulang=50;
var NamaForm_viPasienPulang="Pasien Pulang ";
var mod_name_viPasienPulang="viKasirIgd";
var now_viPasienPulang= new Date();
var addNew_viPasienPulang;
var rowSelected_viPasienPulang;
var setLookUps_viPasienPulang;
var mNoKunjungan_viPasienPulang='';

var CurrentData_viPasienPulang =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viPasienPulang(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir IGD # --------------

// Start Project Kasir IGD # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viPasienPulang
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viPasienPulang(mod_id_viPasienPulang)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viPasienPulang = 
	[
		 'NO_KUNJUNGAN', 'KD_viPasienPulang', 'KD_UNIT', 'KD_viPasienPulang', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viPasienPulang = new WebApp.DataStore
	({
        fields: FieldMaster_viPasienPulang
    });
    
    // Grid Kasir IGD # --------------
	var GridDataView_viPasienPulang = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viPasienPulang,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viPasienPulang = undefined;
							rowSelected_viPasienPulang = dataSource_viPasienPulang.getAt(row);
							CurrentData_viPasienPulang
							CurrentData_viPasienPulang.row = row;
							CurrentData_viPasienPulang.data = rowSelected_viPasienPulang.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viPasienPulang = dataSource_viPasienPulang.getAt(ridx);
					if (rowSelected_viPasienPulang != undefined)
					{
						setLookUp_viPasienPulang(rowSelected_viPasienPulang.data);
					}
					else
					{
						setLookUp_viPasienPulang();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir IGD
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_viPasienPulang',
						header: 'No. Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMPASIEN_viPasienPulang',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 35,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colALAMAT_viPasienPulang',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					//-------------- ## --------------
					{
						id: 'colTglKunj_viPasienPulang',
						header:'Tgl Kunjungan',
						dataIndex: 'TGL_KUNJUNGAN',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_KUNJUNGAN);
						}
					},
					//-------------- ## --------------
					{
						id: 'colPoliTujuan_viPasienPulang',
						header:'Poli Tujuan',
						dataIndex: 'NAMA_UNIT',
						width: 50,
						sortable: true,
						filter: {}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viPasienPulang',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viPasienPulang',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viPasienPulang != undefined)
							{
								setLookUp_viPasienPulang(rowSelected_viPasienPulang.data)
							}
							else
							{								
								setLookUp_viPasienPulang();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viPasienPulang, selectCount_viPasienPulang, dataSource_viPasienPulang),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viPasienPulang = new Ext.Panel
    (
		{
			title: NamaForm_viPasienPulang,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viPasienPulang,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viPasienPulang],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viPasienPulang,
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'Jenis Transaksi : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_viPasienPulang(125, "ComboJenisTransaksi_viPasienPulang"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'No. : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMedrec_viPasienPulang',
							emptyText: 'No. Medrec',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viPasienPulang = 1;
										DataRefresh_viPasienPulang(getCriteriaFilterGridDataView_viPasienPulang());
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Kunjungan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_viPasienPulang',
							value: now_viPasienPulang,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viPasienPulang();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Kelompok : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_viPasienPulang(125, "ComboKelompok_viPasienPulang"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viPasienPulang',
							emptyText: 'Nama',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viPasienPulang = 1;
										DataRefresh_viPasienPulang(getCriteriaFilterGridDataView_viPasienPulang());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viPasienPulang',
							value: now_viPasienPulang,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viPasienPulang();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Poli Tujuan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_viPasienPulang(125, "ComboPoli_viPasienPulang"),
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Alamat : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_ALAMAT_viPasienPulang',
							emptyText: 'Alamat',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viPasienPulang = 1;
										DataRefresh_viPasienPulang(getCriteriaFilterGridDataView_viPasienPulang());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viPasienPulang',
							handler: function() 
							{					
								DfltFilterBtn_viPasienPulang = 1;
								DataRefresh_viPasienPulang(getCriteriaFilterGridDataView_viPasienPulang());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viPasienPulang;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viPasienPulang # --------------

function viComboJenisTransaksi_viPasienPulang(lebar,Nama_ID)
{
  var cbo_viComboJenisTransaksi_viPasienPulang = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Transaksi..',
            fieldLabel: "Transaksi",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdTrnsksi',
                        'displayTextTrnsksi'
                    ],
                    data: [['all', "Semua"],[1, "Bayar"],[2, "Belum Bayar"]]
                }
            ),
            valueField: 'IdTrnsksi',
            displayField: 'displayTextTrnsksi',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisTransaksi_viPasienPulang;
};

function Vicbo_viPasienPulang(lebar,Nama_ID)
{
    var Field_viPasienPulang = ['KD_viPasienPulang', 'KD_CUSTOMER', 'KD_TYPE', 'KELOMPOK'];
    ds_viPasienPulang = new WebApp.DataStore({fields: Field_viPasienPulang});
    
	viRefresh_viPasienPulang();
    var cbo_viPasienPulang = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Kelompok',
			valueField: 'KD_viPasienPulang',
            displayField: 'KELOMPOK',
			store: ds_viPasienPulang,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )    
    return cbo_viPasienPulang;
}

function Vicbo_viPasienPulang(lebar,Nama_ID)
{
    var Field_viPasienPulang = ['KD_viPasienPulang',  'KD_CUSTOMER', 'NAMA'];
    ds_viPasienPulang = new WebApp.DataStore({fields: Field_viPasienPulang});
    
	// viRefresh_viPasienPulang();
    var cbo_viPasienPulang = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Dokter',
			valueField: 'KD_viPasienPulang',
            displayField: 'NAMA',
			store: ds_viPasienPulang,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )    
    return cbo_viPasienPulang;
}

/**
*	Function : setLookUp_viPasienPulang
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viPasienPulang(rowdata)
{
    var lebar = 760;
    setLookUps_viPasienPulang = new Ext.Window
    (
    {
        id: 'SetLookUps_viPasienPulang',
		name: 'SetLookUps_viPasienPulang',
        title: NamaForm_viPasienPulang, 
        closeAction: 'destroy',        
        width: 775,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viPasienPulang(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viPasienPulang=undefined;
                //datarefresh_viPasienPulang();
				mNoKunjungan_viPasienPulang = '';
            }
        }
    }
    );

    setLookUps_viPasienPulang.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viPasienPulang();
		// Ext.getCmp('btnDelete_viPasienPulang').disable();	
    }
    else
    {
        // datainit_viPasienPulang(rowdata);
    }
}
// End Function setLookUpGridDataView_viPasienPulang # --------------

/**
*	Function : getFormItemEntry_viPasienPulang
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viPasienPulang(lebar,rowdata)
{
    var pnlFormDataBasic_viPasienPulang = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:[getItemPanelInputBiodata_viPasienPulang(lebar), getItemGridTransaksi_viPasienPulang(lebar)],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viPasienPulang',
						handler: function(){
							dataaddnew_viPasienPulang();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viPasienPulang',
						handler: function()
						{
							datasave_viPasienPulang(addNew_viPasienPulang);
							datarefresh_viPasienPulang();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viPasienPulang',
						handler: function()
						{
							var x = datasave_viPasienPulang(addNew_viPasienPulang);
							datarefresh_viPasienPulang();
							if (x===undefined)
							{
								setLookUps_viPasienPulang.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viPasienPulang',
						handler: function()
						{
							datadelete_viPasienPulang();
							datarefresh_viPasienPulang();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					}
					// ,
					// //-------------- ## --------------
					// {
		                // text: 'Lookup ',
		                // iconCls:'find',
		                // menu: 
		                // [
		                	// {
						        // text: 'Lookup Produk/Tindakan',
						        // id:'btnLookUpProduk_viPasienPulang',
						        // iconCls: 'find',
						        // handler: function(){
						            // FormSetLookupProduk_viPasienPulang('1','2');
						        // },
					    	// },
					    	// //-------------- ## --------------
        					// {
						        // text: 'Lookup Konsultasi',
						        // id:'btnLookUpKonsultasi_viPasienPulang',
						        // iconCls: 'find',
						        // handler: function(){
						            // FormSetLookupKonsultasi_viPasienPulang('1','2');
						        // },
					    	// },
					    	// //-------------- ## --------------
					    	// {
						        // text: 'Lookup Ganti Dokter',
						        // id:'btnLookUpGantiDokter_viPasienPulang',
						        // iconCls: 'find',
						        // handler: function(){
						            // FormSetLookupGantiDokter_viPasienPulang('1','2');
						        // },
					    	// },
					    	// //-------------- ## --------------
		                // ],
		            // },
					// //-------------- ## --------------
					// {
						// xtype:'tbseparator'
					// },
					// //-------------- ## --------------
					// {
		                // text: 'Pembayaran ',
		                // iconCls:'print',
		                // menu: 
		                // [
		                	// {
						        // text: 'Pembayaran',
						        // id:'btnPembayaran_viPasienPulang',
						        // iconCls: 'print',
						        // handler: function(){
						            // FormSetPembayaran_viPasienPulang('1','2');
						        // },
					    	// },
					    	// //-------------- ## --------------
        					// {
						        // text: 'Transfer ke Rawat Inap',
						        // id:'btnTransferPembayaran_viPasienPulang',
						        // iconCls: 'print',
						        // handler: function(){
						            // FormSetTransferPembayaran_viPasienPulang('1','2');
						        // },
					    	// },
					    	// //-------------- ## --------------
					    	// {
						        // text: 'Edit Tarif',
						        // id:'btnEditTarif_viPasienPulang',
						        // iconCls: 'print',
						        // handler: function(){
						            // FormSetEditTarif_viPasienPulang('1','2');
						        // },
					    	// },
					    	// //-------------- ## --------------
					    	// {
						        // text: 'Posting Manual',
						        // id:'btnPostingManual_viPasienPulang',
						        // iconCls: 'print',
						        // handler: function(){
						            // FormSetPostingManual_viPasienPulang('1','2');
						        // },
					    	// },
					    	// //-------------- ## --------------
		                // ],
		            // },
					//-------------- ## --------------
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viPasienPulang;
}
// End Function getFormItemEntry_viPasienPulang # --------------

/**
*	Function : getItemPanelInputBiodata_viPasienPulang
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viPasienPulang(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Medrec',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',
						width: 100,
						name: 'txtNoMedrec_viPasienPulang',
						id: 'txtNoMedrec_viPasienPulang',
						style:{'text-align':'left'},
						emptyText: 'No. Medrec',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',
						flex: 1,
						width: 35,
						name: '',
						value: 'Nama:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',														
						width : 465,	
						name: 'txtNama_viPasienPulang',
						id: 'txtNama_viPasienPulang',
						emptyText: 'Nama',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					//-------------- ## --------------
				]
			},	
			//-------------- ## --------------				
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Transaksi',
				name: 'compNoreg_viPasienPulang',
				id: 'compNoreg_viPasienPulang',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 200,
						name: 'txtNotrans_viPasienPulang',
						id: 'txtNotrans_viPasienPulang',
						emptyText: 'No. Transaksi',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 54,								
						value: 'Tanggal:',
						fieldLabel: 'Label',
					},
					{
						xtype: 'datefield',					
						fieldLabel: 'Tgl. Masuk',
						name: 'DtpTglmasuk_viPasienPulang',
						id: 'DtpTglmasuk_viPasienPulang',
						format: 'd/M/Y',
						width: 100,
						value: now_viPasienPulang,
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 30,								
						value: 'Reff:',
						fieldLabel: 'Label',
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 50,
						name: 'txtNoReff_viPasienPulang',
						id: 'txtNoReff_viPasienPulang',
						emptyText: 'No. Reff',
					},
					{
						xtype: 'displayfield',				
						width: 75,								
						value: 'Acc. DOkter',
						fieldLabel: 'Label',
					},
					{
						xtype: 'checkbox',
						fieldLabel: 'Acc. DOkter',
						width: 5,						
						name: 'txtchkAccDokter_viPasienPulang',
						id: 'txtchkAccDokter_viPasienPulang'
					}

				]
			},
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Kelompok',
				name: 'compUnit_viPasienPulang',
				id: 'compUnit_viPasienPulang',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 110,
						name: 'txtKelompok_viPasienPulang',
						id: 'txtKelompok_viPasienPulang',
						emptyText: 'Kelompok',
					},
					{
						xtype: 'displayfield',				
						width: 65,								
						value: 'Kelas/Kamar',
						fieldLabel: 'Label',
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 426,
						name: 'txtKlsKamar_viPasienPulang',
						id: 'txtKlsKamar_viPasienPulang',
						emptyText: 'Kelas/Kamar',
					}
					//-------------- ## --------------
				]
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viPasienPulang # --------------

/**
*	Function : getItemGridTransaksi_viPasienPulang
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viPasienPulang(lebar) 
{
    var items =
	{
		title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		width: lebar-80,
		height:390, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viPasienPulang(),
					{
						xtype: 'compositefield',
						fieldLabel: '',
						anchor: '100%',						
						id:'comptotal_viPasienPulang',
						items: 
						[
							{ 
								xtype: 'tbspacer',									
								height: 5
							},
							{
								xtype: 'textfield',
								id: 'txttotal1_viPasienPulang',
								name: 'txttotal1_viPasienPulang',
								style:{'text-align':'right','margin-left':'400px'},
								width: 100,
								value: 0,
								readOnly: true
							},
							{
								xtype: 'textfield',
								id: 'txttotal2_viPasienPulang',
								name: 'txttotal2_viPasienPulang',
								style:{'text-align':'right','margin-left':'500px'},
								width: 100,
								value: 0,
								readOnly: true
							}
						]
					}
					// ,
					// {
						// xtype: 'compositefield',
						// fieldLabel: '',
						// anchor: '100%',						
						// id:'compTombol_viPasienPulang',
						// items: 
						// [
							// { 
								// xtype: 'tbspacer',									
								// height: 5
							// },
							// {
								// xtype:'button',
								// text:'Transaksi',
								// tooltip: 'Transaksi',
								// id:'btntransaksi_viPasienPulang',
								// name:'btntransaksi_viPasienPulang',								
								// handler:function()
								// {
									
								// }
							// },	
							// {
								// xtype:'button',
								// text:'Rekapitulasi',
								// tooltip: 'Rekapitulasi',
								// id:'btnRekapitulasi_viPasienPulang',
								// name:'btnRekapitulasi_viPasienPulang',								
								// handler:function()
								// {
									
								// }
							// },	
							// {
								// xtype:'button',
								// text:'Billing',
								// tooltip: 'Billing',
								// id:'btnBilling_viPasienPulang',
								// name:'btnBilling_viPasienPulang',								
								// handler:function()
								// {
									
								// }
							// },
							// {
								// xtype:'button',
								// text:'Rincian Billing',
								// tooltip: 'Rincian Billing',
								// id:'btnRincian_viPasienPulang',
								// name:'btnRincian_viPasienPulang',								
								// handler:function()
								// {
									
								// }
							// }
						// ]
					// }
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viPasienPulang # --------------

/**
*	Function : gridDataViewEdit_viPasienPulang
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viPasienPulang()
{
    
    var FieldGrdKasir_viPasienPulang = 
	[
	];
	
    dsDataGrdJab_viPasienPulang= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viPasienPulang
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viPasienPulang,
        height: 220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Tanggal',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Reff.',
				sortable: true,
				width: 40
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Unit',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}			
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 250,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dok',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Debit',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kredit',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'F',
				sortable: true,
				width: 20,
				renderer: function(v, params, record) 
				{
					
				}	
			}
        ]
    }    
    return items;
}
// End Function gridDataViewEdit_viPasienPulang # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupProduk_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan windows popup List Produk
*/

function FormSetLookupProduk_viPasienPulang(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryLookupProduk_viPasienPulang = new Ext.Window
    (
        {
            id: 'FormGrdLookupProduk_viPasienPulang',
            title: 'List Produk',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryProdukGridDataView_viPasienPulang(subtotal,NO_KUNJUNGAN),
                //-------------- ## --------------
            ],
            tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAddTreeProduk_viPasienPulang',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDeleteTreeProduk_viPasienPulang',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
				]
			}
        }
    );
    vWinFormEntryLookupProduk_viPasienPulang.show();
    mWindowGridLookupLookupProduk  = vWinFormEntryLookupProduk_viPasienPulang; 
}
// End Function FormSetLookupProduk_viPasienPulang # --------------

/**
*   Function : getFormItemEntryProdukGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form produk
*/
function getFormItemEntryProdukGridDataView_viPasienPulang(subtotal,NO_KUNJUNGAN)
{
	var lebar = 515;
	var itemsListProdukGridDataView_viPasienPulang = 
    {
        xtype: 'panel',
        title: '',
        name: 'PanelListProdukGridDataView_viPasienPulang',
        id: 'PanelListProdukGridDataView_viPasienPulang',        
        bodyStyle: 'padding: 5px;',
        layout: 'hbox',
        height: 1170,
        border:false,
        items: 
        [           
            itemsTreeListProdukGridDataView_viPasienPulang(),
            //-------------- ## -------------- 
            { 
                xtype: 'tbspacer',
                width: 5,
            },
            //-------------- ## -------------- 
            getItemTreeGridTransaksiGridDataView_viPasienPulang(lebar),
            //-------------- ## -------------- 
        ]
    }
    return itemsListProdukGridDataView_viPasienPulang;
}
// End Function getFormItemEntryProdukGridDataView_viPasienPulang # --------------

/**
*   Function : itemsTreeListProdukGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan tree prosuk
*/
function itemsTreeListProdukGridDataView_viPasienPulang()
{
	var children = 
	[
		{
		     text:'First Level Child 1',
		     children:
		     [
		     	{
		        	text:'Second Level Child 1',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		        {
		        	text:'Second Level Child 2',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		    ]
		},
		{
		     text:'First Level Child 2',
		     children:
		     [
		     	{
		        	text:'Second Level Child 1',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		        {
		        	text:'Second Level Child 2',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		    ]
		},
	];

    var tree = new Ext.tree.TreePanel
    (
    	{
        	loader:new Ext.tree.TreeLoader(),
        	width:200,
        	height:340,
        	renderTo:Ext.getBody(),
        	root:new Ext.tree.AsyncTreeNode
        	(
        		{
             		expanded:true,
             		leaf:false,
             		text:'Tree Root',
             		children:children
             	}
            )
    	}
    );

    var pnlTreeFormDataWindowPopup_viPasienPulang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                {
		            xtype: 'radiogroup',
		            //fieldLabel: 'Single Column',
		            itemCls: 'x-check-group-alt',
		            columns: 1,
		            items: 
		            [
		                {
		                	boxLabel: 'Semua Unit', 
		                	name: 'ckboxTreeSemuaUnit_viPasienPulang',
		                	id: 'ckboxTreeSemuaUnit_viPasienPulang',  
		                	inputValue: 1,
		                },
		                //-------------- ## -------------- 
		                {
		                	boxLabel: 'Unit Penyakit Dalam', 
		                	name: 'ckboxTreePerUnit_viPasienPulang',
		                	id: 'ckboxTreePerUnit_viPasienPulang',  
		                	inputValue: 2, 
		                	checked: true,
		                },
		                //-------------- ## -------------- 
		            ]
		        },
				//-------------- ## -------------- 
                tree,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viPasienPulang;
}
// End Function itemsTreeListProdukGridDataView_viPasienPulang # --------------

/**
*   Function : getItemTreeGridTransaksiGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemTreeGridTransaksiGridDataView_viPasienPulang(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:2px 2px 2px 2px',
        border:true,
        width: lebar,
        height:402, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridTreeDataViewEditGridDataView_viPasienPulang()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemTreeGridTransaksiGridDataView_viPasienPulang # --------------

/**
*   Function : gridTreeDataViewEditGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridTreeDataViewEditGridDataView_viPasienPulang()
{
    
    var FieldTreeGrdKasirGridDataView_viPasienPulang = 
    [
    ];
    
    dsDataTreeGrdJabGridDataView_viPasienPulang= new WebApp.DataStore
    ({
        fields: FieldTreeGrdKasirGridDataView_viPasienPulang
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataTreeGrdJabGridDataView_viPasienPulang,
        height: 395,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Cek',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi Produk',
                sortable: true,
                width: 330,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridTreeDataViewEditGridDataView_viPasienPulang # --------------

// ## END MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupKonsultasi_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan windows popup Konsultasi
*/

function FormSetLookupKonsultasi_viPasienPulang(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryKonsultasi_viPasienPulang = new Ext.Window
    (
        {
            id: 'FormGrdLookupKonsultasi_viPasienPulang',
            title: 'Konsultasi',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupKonsultasi_viPasienPulang(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryKonsultasi_viPasienPulang.show();
    mWindowGridLookupKonsultasi  = vWinFormEntryKonsultasi_viPasienPulang; 
};

// End Function FormSetLookupKonsultasi_viPasienPulang # --------------

/**
*   Function : getFormItemEntryLookupKonsultasi_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/
function getFormItemEntryLookupKonsultasi_viPasienPulang(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataKonsultasiWindowPopup_viPasienPulang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputKonsultasiDataView_viPasienPulang(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataKonsultasiWindowPopup_viPasienPulang;
}
// End Function getFormItemEntryLookupKonsultasi_viPasienPulang # --------------

/**
*   Function : getItemPanelInputKonsultasiDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/

function getItemPanelInputKonsultasiDataView_viPasienPulang(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Asal Unit',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPasienPulang(250,'CmboAsalUnit_viPasienPulang'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Konsultasi ke',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPasienPulang(250,'CmboKonsultasike_viPasienPulang'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPasienPulang(250,'CmboDokter_viPasienPulang'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputKonsultasiDataView_viPasienPulang # --------------

// ## END MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupGantiDokter_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupGantiDokter_viPasienPulang(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_viPasienPulang = new Ext.Window
    (
        {
            id: 'FormGrdLookupGantiDokter_viPasienPulang',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupGantiDokter_viPasienPulang(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_viPasienPulang.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_viPasienPulang; 
};

// End Function FormSetLookupGantiDokter_viPasienPulang # --------------

/**
*   Function : getFormItemEntryLookupGantiDokter_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupGantiDokter_viPasienPulang(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataGantiDokterWindowPopup_viPasienPulang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputGantiDokterDataView_viPasienPulang(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataGantiDokterWindowPopup_viPasienPulang;
}
// End Function getFormItemEntryLookupGantiDokter_viPasienPulang # --------------

/**
*   Function : getItemPanelInputGantiDokterDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputGantiDokterDataView_viPasienPulang(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Tanggal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtTglGantiDokter_viPasienPulang',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtBlnGantiDokter_viPasienPulang',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtThnGantiDokter_viPasienPulang',
                        emptyText: '2014',
                        width: 50,
                    },
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Asal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPasienPulang(250,'CmboDokterAsal_viPasienPulang'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Ganti',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPasienPulang(250,'CmboDokterGanti_viPasienPulang'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputGantiDokterDataView_viPasienPulang # --------------

// ## END MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

// ## END MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

/**
*   Function : FormSetPembayaran_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan windows popup Pembayaran
*/

function FormSetPembayaran_viPasienPulang(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPembayaran_viPasienPulang = new Ext.Window
    (
        {
            id: 'FormGrdPembayaran_viPasienPulang',
            title: 'Pembayaran',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryGridDataView_viPasienPulang(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPembayaran_viPasienPulang.show();
    mWindowGridLookupPembayaran  = vWinFormEntryPembayaran_viPasienPulang; 
};

// End Function FormSetPembayaran_viPasienPulang # --------------

/**
*   Function : getFormItemEntryGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function getFormItemEntryGridDataView_viPasienPulang(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataWindowPopup_viPasienPulang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataGridDataView_viPasienPulang(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiGridDataView_viPasienPulang(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnOkGridTransaksiPembayaranGridDataView_viPasienPulang',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnCancelGridTransaksiPembayaranGridDataView_viPasienPulang',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtBiayaGridTransaksiPembayaranGridDataView_viPasienPulang',
                            name: 'txtBiayaGridTransaksiPembayaranGridDataView_viPasienPulang',
							style:{'text-align':'right','margin-left':'150px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtPiutangGridTransaksiPembayaranGridDataView_viPasienPulang',
                            name: 'txtPiutangGridTransaksiPembayaranGridDataView_viPasienPulang',
							style:{'text-align':'right','margin-left':'146px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtTunaiGridTransaksiPembayaranGridDataView_viPasienPulang',
                            name: 'txtTunaiGridTransaksiPembayaranGridDataView_viPasienPulang',
							style:{'text-align':'right','margin-left':'142px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtDiscGridTransaksiPembayaranGridDataView_viPasienPulang',
                            name: 'txtDiscGridTransaksiPembayaranGridDataView_viPasienPulang',
							style:{'text-align':'right','margin-left':'138px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataWindowPopup_viPasienPulang;
}
// End Function getFormItemEntryGridDataView_viPasienPulang # --------------

/**
*   Function : getItemPanelInputBiodataGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemPanelInputBiodataGridDataView_viPasienPulang(lebar) 
{
    var items =
    {
        title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
				xtype: 'textfield',
				fieldLabel: 'No. Transaksi',
				id: 'txtNoTransaksiPembayaranGridDataView_viPasienPulang',
				name: 'txtNoTransaksiPembayaranGridDataView_viPasienPulang',
				emptyText: 'No. Transaksi',
				readOnly: true,
				flex: 1,
				width: 195,				
			},
			//-------------- ## --------------
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				id: 'txtNamaPasienPembayaranGridDataView_viPasienPulang',
				name: 'txtNamaPasienPembayaranGridDataView_viPasienPulang',
				emptyText: 'Nama Pasien',
				flex: 1,
				anchor: '100%',					
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran',
				anchor: '100%',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viPasienPulang(195,'CmboPembayaranGridDataView_viPasienPulang'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				anchor: '100%',
				labelSeparator: '',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viPasienPulang(195,'CmboPembayaranDuaGridDataView_viPasienPulang'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataGridDataView_viPasienPulang # --------------

/**
*   Function : getItemGridTransaksiGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemGridTransaksiGridDataView_viPasienPulang(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar-80,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridDataView_viPasienPulang()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiGridDataView_viPasienPulang # --------------

/**
*   Function : gridDataViewEditGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridDataViewEditGridDataView_viPasienPulang()
{
    
    var FieldGrdKasirGridDataView_viPasienPulang = 
    [
    ];
    
    dsDataGrdJabGridDataView_viPasienPulang= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viPasienPulang
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viPasienPulang,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kode',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Qty',
                sortable: true,
                width: 40
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Biaya',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Piutang',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tunai',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Disc',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridDataView_viPasienPulang # --------------

// ## END MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

/**
*   Function : FormSetTransferPembayaran_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan windows popup Transfer ke Rawat Inap
*/

function FormSetTransferPembayaran_viPasienPulang(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryTransferPembayaran_viPasienPulang = new Ext.Window
    (
        {
            id: 'FormGrdTrnsferPembayaran_viPasienPulang',
            title: 'Pemindahan Tagihan Ke Unit Rawat Inap',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 450,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormTransferPembayaranItemEntryGridDataView_viPasienPulang(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryTransferPembayaran_viPasienPulang.show();
    mWindowGridLookupTransferPembayaran  = vWinFormEntryTransferPembayaran_viPasienPulang; 
};

// End Function FormSetTransferPembayaran_viPasienPulang # --------------

/**
*   Function : getFormTransferPembayaranItemEntryGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form transfer ke rawat inap
*/
function getFormTransferPembayaranItemEntryGridDataView_viPasienPulang(subtotal,NO_KUNJUNGAN)
{
    var pnlFormTransferPembayaranDataWindowPopup_viPasienPulang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            items:
            //-------------- #items# --------------
            [
                {
                    xtype: 'fieldset',
                    title: 'Tagihan dipindahkan ke',
                    frame: false,
                    width: 350,
                    height: 165,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Transaksi',
                            id: 'txtNoTransaksiTransaksiTransferPembayaran_viPasienPulang',
                            name: 'txtNoTransaksiTransaksiTransferPembayaran_viPasienPulang',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
							xtype: 'compositefield',
							fieldLabel: 'Tgl. Lahir',
							anchor: '100%',
							width: 130,
							items: 
							[
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'DateTransaksiTransferPembayaran_viPasienPulang',
									name: 'DateTransaksiTransferPembayaran_viPasienPulang',
									width: 130,
									format: 'd/M/Y',
								}
								//-------------- ## --------------				
							]
						},
						//-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Rekam Medis',
                            id: 'txtNoRekamMedisTransaksiTransferPembayaran_viPasienPulang',
                            name: 'txtNoRekamMedisTransaksiTransferPembayaran_viPasienPulang',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nama Pasien',
                            id: 'txtNamaPasienTransaksiTransferPembayaran_viPasienPulang',
                            name: 'txtNamaPasienTransaksiTransferPembayaran_viPasienPulang',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Unit Perawatan',
                            id: 'txtUnitPerawatanTransaksiTransferPembayaran_viPasienPulang',
                            name: 'txtUnitPerawatanTransaksiTransferPembayaran_viPasienPulang',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: '',
                    frame: false,
                    width: 350,
                    height: 125,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jumlah biaya',
                            id: 'txtJumlhBiayaTransaksiTransferPembayaran_viPasienPulang',
                            name: 'txtJumlhBiayaTransaksiTransferPembayaran_viPasienPulang',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'PAID',
                            id: 'txtPAIDTransaksiTransferPembayaran_viPasienPulang',
                            name: 'txtPAIDTransaksiTransferPembayaran_viPasienPulang',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jmlh dipindahkan',
                            id: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viPasienPulang',
                            name: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viPasienPulang',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Saldo Tagihan',
                            id: 'txtSaldoTagihanTransaksiTransferPembayaran_viPasienPulang',
                            name: 'txtSaldoTagihanTransaksiTransferPembayaran_viPasienPulang',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: 'Alasan Transfer',
                    frame: false,
                    width: 350,
                    height: 62,
                    items: 
                    [
						{
				            xtype: 'combo',
				            fieldLabel: 'Alsn Transfer',
				            labelSeparator: '',
				        },
						//-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					width: 160,
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnOkTransferPembayaran_viPasienPulang',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
						{
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnCancelTransferPembayaran_viPasienPulang',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
					]
				},
                //-------------- ## --------------
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormTransferPembayaranDataWindowPopup_viPasienPulang;
}
// End Function getFormTransferPembayaranItemEntryGridDataView_viPasienPulang # --------------

// ## END MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

/**
*   Function : FormSetEditTarif_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan windows popup Edit Tarif
*/

function FormSetEditTarif_viPasienPulang(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryEditTarif_viPasienPulang = new Ext.Window
    (
        {
            id: 'FormGrdEditTarif_viPasienPulang',
            title: 'Edit Tarif',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryEditTarifGridDataView_viPasienPulang(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryEditTarif_viPasienPulang.show();
    mWindowGridLookupEditTarif  = vWinFormEntryEditTarif_viPasienPulang; 
};

// End Function FormSetEditTarif_viPasienPulang # --------------

/**
*   Function : getFormItemEntryEditTarifGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/
function getFormItemEntryEditTarifGridDataView_viPasienPulang(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataEditTarifWindowPopup_viPasienPulang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukEditTarifGridDataView_viPasienPulang(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiEditTarifGridDataView_viPasienPulang(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiEditTarifGridDataView_viPasienPulang',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiEditTarifGridDataView_viPasienPulang',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viPasienPulang',
                            name: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viPasienPulang',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viPasienPulang',
                            name: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viPasienPulang',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataEditTarifWindowPopup_viPasienPulang;
}
// End Function getFormItemEntryEditTarifGridDataView_viPasienPulang # --------------

/**
*   Function : getItemPanelInputProdukEditTarifGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemPanelInputProdukEditTarifGridDataView_viPasienPulang(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukEditTarifGridDataView_viPasienPulang',
                name: 'txtProdukEditTarifGridDataView_viPasienPulang',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukEditTarifGridDataView_viPasienPulang # --------------

/**
*   Function : getItemGridTransaksiEditTarifGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemGridTransaksiEditTarifGridDataView_viPasienPulang(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridEditTarifDataView_viPasienPulang()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiEditTarifGridDataView_viPasienPulang # --------------

/**
*   Function : gridDataViewEditGridEditTarifDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function gridDataViewEditGridEditTarifDataView_viPasienPulang()
{
    
    var FieldGrdKasirGridDataView_viPasienPulang = 
    [
    ];
    
    dsDataGrdJabGridDataView_viPasienPulang= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viPasienPulang
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viPasienPulang,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridEditTarifDataView_viPasienPulang # --------------

// ## END MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

/**
*   Function : FormSetPostingManual_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan windows popup Posting Manual
*/

function FormSetPostingManual_viPasienPulang(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPostingManual_viPasienPulang = new Ext.Window
    (
        {
            id: 'FormGrdPostingManual_viPasienPulang',
            title: 'Posting Manual',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryPostingManualGridDataView_viPasienPulang(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPostingManual_viPasienPulang.show();
    mWindowGridLookupPostingManual  = vWinFormEntryPostingManual_viPasienPulang; 
};

// End Function FormSetPostingManual_viPasienPulang # --------------

/**
*   Function : getFormItemEntryPostingManualGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/
function getFormItemEntryPostingManualGridDataView_viPasienPulang(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataPostingManualWindowPopup_viPasienPulang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukPostingManualGridDataView_viPasienPulang(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiPostingManualGridDataView_viPasienPulang(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiPostingManualGridDataView_viPasienPulang',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiPostingManualGridDataView_viPasienPulang',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viPasienPulang',
                            name: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viPasienPulang',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viPasienPulang',
                            name: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viPasienPulang',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataPostingManualWindowPopup_viPasienPulang;
}
// End Function getFormItemEntryPostingManualGridDataView_viPasienPulang # --------------

/**
*   Function : getItemPanelInputProdukPostingManualGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemPanelInputProdukPostingManualGridDataView_viPasienPulang(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukPostingManualGridDataView_viPasienPulang',
                name: 'txtProdukPostingManualGridDataView_viPasienPulang',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukPostingManualGridDataView_viPasienPulang # --------------

/**
*   Function : getItemGridTransaksiPostingManualGridDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemGridTransaksiPostingManualGridDataView_viPasienPulang(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridPostingManualDataView_viPasienPulang()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiPostingManualGridDataView_viPasienPulang # --------------

/**
*   Function : gridDataViewEditGridPostingManualDataView_viPasienPulang
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function gridDataViewEditGridPostingManualDataView_viPasienPulang()
{
    
    var FieldGrdKasirGridDataView_viPasienPulang = 
    [
    ];
    
    dsDataGrdJabGridDataView_viPasienPulang= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viPasienPulang
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viPasienPulang,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridPostingManualDataView_viPasienPulang # --------------

// ## END MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

// ## END MENU PEMBAYARAN ## ------------------------------------------------------------------

// --------------------------------------- # End Form # ---------------------------------------

// End Project Kasir IGD # --------------