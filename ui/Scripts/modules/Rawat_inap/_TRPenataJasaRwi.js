// Data Source ExtJS # --------------
// Deklarasi Variabel pada Kasir Rawat Jalan # --------------

var dataSource_viPntJasaRwi;
var selectCount_viPntJasaRwi=50;
var NamaForm_viPntJasaRwi="Penata Jasa RWI ";
var mod_name_viPntJasaRwi="viKasirRwi";
var now_viPntJasaRwi= new Date();
var addNew_viPntJasaRwi;
var rowSelected_viPntJasaRwi;
var setLookUps_viPntJasaRwi;
var mNoKunjungan_viPntJasaRwi='';

var CurrentData_viPntJasaRwi =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viPntJasaRwi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Rawat Jalan # --------------

// Start Project Kasir Rawat Jalan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viPntJasaRwi
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viPntJasaRwi(mod_id_viPntJasaRwi)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viPntJasaRwi = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viPntJasaRwi = new WebApp.DataStore
	({
        fields: FieldMaster_viPntJasaRwi
    });
    
    // Grid Kasir Rawat Jalan # --------------
	var GridDataView_viPntJasaRwi = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viPntJasaRwi,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viPntJasaRwi = undefined;
							rowSelected_viPntJasaRwi = dataSource_viPntJasaRwi.getAt(row);
							CurrentData_viPntJasaRwi
							CurrentData_viPntJasaRwi.row = row;
							CurrentData_viPntJasaRwi.data = rowSelected_viPntJasaRwi.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viPntJasaRwi = dataSource_viPntJasaRwi.getAt(ridx);
					if (rowSelected_viPntJasaRwi != undefined)
					{
						setLookUp_viPntJasaRwi(rowSelected_viPntJasaRwi.data);
					}
					else
					{
						setLookUp_viPntJasaRwi();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir Rawat Jalan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_viPntJasaRwi',
						header: 'No. Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMPASIEN_viPntJasaRwi',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 35,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colALAMAT_viPntJasaRwi',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					//-------------- ## --------------
					{
						id: 'colTglKunj_viPntJasaRwi',
						header:'Tgl Kunjungan',
						dataIndex: 'TGL_KUNJUNGAN',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_KUNJUNGAN);
						}
					},
					//-------------- ## --------------
					{
						id: 'colPoliTujuan_viPntJasaRwi',
						header:'Poli Tujuan',
						dataIndex: 'NAMA_UNIT',
						width: 50,
						sortable: true,
						filter: {}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viPntJasaRwi',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viPntJasaRwi',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viPntJasaRwi != undefined)
							{
								setLookUp_viPntJasaRwi(rowSelected_viPntJasaRwi.data)
							}
							else
							{								
								setLookUp_viPntJasaRwi();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viPntJasaRwi, selectCount_viPntJasaRwi, dataSource_viPntJasaRwi),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viPntJasaRwi = new Ext.Panel
    (
		{
			title: NamaForm_viPntJasaRwi,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viPntJasaRwi,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viPntJasaRwi],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viPntJasaRwi,
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'Jenis Transaksi : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_viPntJasaRwi(125, "ComboJenisTransaksi_viPntJasaRwi"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'No. : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMedrec_viPntJasaRwi',							
							emptyText: 'No. Medrec',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viPntJasaRwi = 1;
										DataRefresh_viPntJasaRwi(getCriteriaFilterGridDataView_viPntJasaRwi());
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Kunjungan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_viPntJasaRwi',
							value: now_viPntJasaRwi,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viPntJasaRwi();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Kelompok : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_viPntJasaRwi(125, "ComboKelompok_viPntJasaRwi"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viPntJasaRwi',
							emptyText: 'Nama',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viPntJasaRwi = 1;
										DataRefresh_viPntJasaRwi(getCriteriaFilterGridDataView_viPntJasaRwi());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viPntJasaRwi',
							value: now_viPntJasaRwi,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viPntJasaRwi();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Poli Tujuan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_viPntJasaRwi(125, "ComboPoli_viPntJasaRwi"),
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Alamat : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_ALAMAT_viPntJasaRwi',
							emptyText: 'Alamat',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viPntJasaRwi = 1;
										DataRefresh_viPntJasaRwi(getCriteriaFilterGridDataView_viPntJasaRwi());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viPntJasaRwi',
							handler: function() 
							{					
								DfltFilterBtn_viPntJasaRwi = 1;
								DataRefresh_viPntJasaRwi(getCriteriaFilterGridDataView_viPntJasaRwi());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viPntJasaRwi;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viPntJasaRwi # --------------

/**
*	Function : setLookUp_viPntJasaRwi
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viPntJasaRwi(rowdata)
{
    var lebar = 860;
    setLookUps_viPntJasaRwi = new Ext.Window
    (
    {
        id: 'SetLookUps_viPntJasaRwi',
		name: 'SetLookUps_viPntJasaRwi',
        title: NamaForm_viPntJasaRwi, 
        closeAction: 'destroy',        
        width: 875,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viPntJasaRwi(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viPntJasaRwi=undefined;
                //datarefresh_viPntJasaRwi();
				mNoKunjungan_viPntJasaRwi = '';
            }
        }
    }
    );

    setLookUps_viPntJasaRwi.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viPntJasaRwi();
		// Ext.getCmp('btnDelete_viPntJasaRwi').disable();	
    }
    else
    {
        // datainit_viPntJasaRwi(rowdata);
    }
}
// End Function setLookUpGridDataView_viPntJasaRwi # --------------

/**
*	Function : getFormItemEntry_viPntJasaRwi
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viPntJasaRwi(lebar,rowdata)
{
    var pnlFormDataBasic_viPntJasaRwi = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:[getItemPanelInputBiodata_viPntJasaRwi(lebar), getItemDataKunjungan_viPntJasaRwi(lebar)], //,getItemPanelBawah_viPntJasaRwi(lebar)],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viPntJasaRwi',
						handler: function(){
							dataaddnew_viPntJasaRwi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viPntJasaRwi',
						handler: function()
						{
							datasave_viPntJasaRwi(addNew_viPntJasaRwi);
							datarefresh_viPntJasaRwi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viPntJasaRwi',
						handler: function()
						{
							var x = datasave_viPntJasaRwi(addNew_viPntJasaRwi);
							datarefresh_viPntJasaRwi();
							if (x===undefined)
							{
								setLookUps_viPntJasaRwi.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viPntJasaRwi',
						handler: function()
						{
							datadelete_viPntJasaRwi();
							datarefresh_viPntJasaRwi();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Lookup ',
		                iconCls:'find',
		                menu: 
		                [
		                	{
						        text: 'Lookup Produk/Tindakan',
						        id:'btnLookUpProduk_viPntJasaRwi',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupProduk_viPntJasaRwi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
        					{
						        text: 'Lookup Konsultasi',
						        id:'btnLookUpKonsultasi_viPntJasaRwi',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupKonsultasi_viPntJasaRwi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Lookup Ganti Dokter',
						        id:'btnLookUpGantiDokter_viPntJasaRwi',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupGantiDokter_viPntJasaRwi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
		                ],
		            },
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					// {
		                // text: 'Pembayaran ',
		                // iconCls:'print',
		                // menu: 
		                // [
		                	// {
						        // text: 'Pembayaran',
						        // id:'btnPembayaran_viPntJasaRwi',
						        // iconCls: 'print',
						        // handler: function(){
						            // FormSetPembayaran_viPntJasaRwi('1','2');
						        // },
					    	// },
					    	// //-------------- ## --------------
        					// {
						        // text: 'Transfer ke Rawat Inap',
						        // id:'btnTransferPembayaran_viPntJasaRwi',
						        // iconCls: 'print',
						        // handler: function(){
						            // FormSetTransferPembayaran_viPntJasaRwi('1','2');
						        // },
					    	// },
					    	// //-------------- ## --------------
					    	// {
						        // text: 'Edit Tarif',
						        // id:'btnEditTarif_viPntJasaRwi',
						        // iconCls: 'print',
						        // handler: function(){
						            // FormSetEditTarif_viPntJasaRwi('1','2');
						        // },
					    	// },
					    	// //-------------- ## --------------
					    	// {
						        // text: 'Posting Manual',
						        // id:'btnPostingManual_viPntJasaRwi',
						        // iconCls: 'print',
						        // handler: function(){
						            // FormSetPostingManual_viPntJasaRwi('1','2');
						        // }
					    	// }
					    	// //-------------- ## --------------
		                // ]
		            // }
					//-------------- ## --------------
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viPntJasaRwi;
}
// End Function getFormItemEntry_viPntJasaRwi # --------------

/**
*	Function : getItemPanelInputBiodata_viPntJasaRwi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viPntJasaRwi(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Medrec',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',
						width: 100,
						name: 'txtNoMedrec_viPntJasaRwi',
						id: 'txtNoMedrec_viPntJasaRwi',
						style:{'text-align':'left'},
						emptyText: 'No. Medrec',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',
						flex: 1,
						width: 35,
						name: '',
						value: 'Nama:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',														
						width : 567,	
						name: 'txtNama_viPntJasaRwi',
						id: 'txtNama_viPntJasaRwi',
						emptyText: 'Nama',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					//-------------- ## --------------
				]
			},	
			//-------------- ## --------------				
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Transaksi',
				name: 'compNoTrans_viPntJasaRwi',
				id: 'compNoTrans_viPntJasaRwi',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 100,
						name: 'txtNotrans_viPntJasaRwi',
						id: 'txtNotrans_viPntJasaRwi',
						emptyText: 'No. Transaksi',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 54,								
						value: 'Tanggal:',
						fieldLabel: 'Label',
					},
					{
						xtype: 'datefield',					
						fieldLabel: 'Tanggal',
						name: 'DtpTglLahir_viPntJasaRwi',
						id: 'DtpTglLahir_viPntJasaRwi',
						format: 'd/M/Y',
						width: 90,
						value: now_viPntJasaRwi,
					},
					//-------------- ## --------------
					{
						xtype:'button',
						text:'Info Seluruh Trans.',
						tooltip: 'Info Seluruh Trans.',
						id:'btnTrans_viPntJasaRwi',
						name:'btnTrans_viPntJasaRwi',
						width: 110,
						handler:function()
						{
							
						}
					},
					{
						xtype: 'displayfield',				
						width: 100,								
						value: 'Kamar Sementara:',
						fieldLabel: 'Label',
						id: 'lblKmrsmntr_viPntJasaRwi',
						name: 'lblKmrsmntr_viPntJasaRwi'
					},
					{
						xtype: 'textfield',					
						fieldLabel: 'Kamar Sementara',								
						width: 155,
						name: 'txtKmrSmentara_viPntJasaRwi',
						id: 'txtKmrSmentara_viPntJasaRwi',
						emptyText: 'Kamar Sementara',
					},					
					{
						xtype:'button',
						text:'Pindah Kamar',
						tooltip: 'Pindah Kamar',
						id:'btnPindahKamar_viPntJasaRwi',
						name:'btnPindahKamar_viPntJasaRwi',
						width: 50,
						handler:function()
						{
							
						}
					}
				]
			},
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Kelompok',
				name: 'compKlp_viPntJasaRwi',
				id: 'compKlp_viPntJasaRwi',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 100,
						name: 'txtKelompok_viPntJasaRwi',
						id: 'txtKelompok_viPntJasaRwi',
						emptyText: 'Kelompok'
					},
					{
						xtype: 'displayfield',				
						width: 65,								
						value: 'Kelas/Kamar:',
						fieldLabel: 'Label',
						id: 'lblkelaskamar_viPntJasaRwi',
						name: 'lblkelaskamar_viPntJasaRwi'
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 537,
						name: 'txtKelaskamar_viPntJasaRwi',
						id: 'txtKelaskamar_viPntJasaRwi',
						emptyText: 'Kelompok'
					}
					//-------------- ## --------------
				]
			}
			//-------------- ## --------------				
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viPntJasaRwi # --------------

/**
*	Function : getItemDataKunjungan_viPntJasaRwi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataKunjungan_viPntJasaRwi(lebar) 
{
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height:400,
	    items:
		[				
			{
				xtype: 'panel',
				title: '',
				border:false,	
				height:375,
				items: 
				[
					{
						xtype: 'tabpanel',
						activeTab: 0,		
						height: 260,
						items: 
						[
							{
								xtype: 'panel',
								title: 'Transaksi',				
								padding: '4px',
								items: 
								[
									gridDataTrans_viPntJasaRwi(),
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'textfield',
										id: 'txttotaltrans_viPntJasaRwi',
										name: 'txttotaltrans_viPntJasaRwi',
										style:{'text-align':'right','margin-left':'700px'},
										width: 107,
										value: 0,
										readOnly: true,
									}
								]
							},
							{
								xtype: 'panel',
								title: 'Transaksi Sudah Terkirim',				
								padding: '4px',
								items: 
								[
									gridDataTransPntJasa_viPntJasaRwi(),
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'textfield',
										id: 'txttotaltranspntjasa_viPntJasaRwi',
										name: 'txttotaltranspntjasa_viPntJasaRwi',
										style:{'text-align':'right','margin-left':'700px'},
										width: 107,
										value: 0,
										readOnly: true,
									}
								]
							},
							{
								xtype: 'panel',
								title: 'Rekapitulasi',				
								padding: '4px',
								items: 
								[
									gridDataTransrekap_viPntJasaRwi(),
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'textfield',
										id: 'txttotaltransrekap_viPntJasaRwi',
										name: 'txttotaltransrekap_viPntJasaRwi',
										style:{'text-align':'right','margin-left':'700px'},
										width: 107,
										value: 0,
										readOnly: true,
									}
								
								]
							}
						]
					},
					{ 
						xtype: 'tbspacer',
						width: 15,
						height: 5
					},
					{
						xtype: 'compositefield',
						fieldLabel: 'Tanggal',
						anchor: '100%',
						// width: 250,
						id:'compbutton_viPntJasaRwi',
						items: 
						[
							{
								xtype:'button',
								text:'Kirim Transaksi Ke Kasir',
								tooltip: 'Kirim Transaksi Ke Kasir',
								id:'btnkirimtrans_viPntJasaRwi',
								name:'btnkirimtrans_viPntJasaRwi',
								// width: 50,
								handler:function()
								{
									
								}
							},		
							{
								xtype:'button',
								text:'Diagnosa',
								tooltip: 'Diagnosa',
								id:'btnDiagnosa_viPntJasaRwi',
								name:'btnDiagnosa_viPntJasaRwi',
								// width: 50,
								handler:function()
								{
									
								}
							}	
						]
					},
					{ 
						xtype: 'tbspacer',						
						height: 5
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Catatan:',
						fieldLabel: 'Label',
						id: 'lblCatatan_viPntJasaRwi',
						name: 'lblCatatan_viPntJasaRwi'
					},
					{
						xtype: 'textarea',						
						fieldLabel: 'Catatan',
						width: 817,
						name: 'txtCatatan_viPntJasaRwi',
						id: 'txtCatatan_viPntJasaRwi'
					}					
				]
			}
		]
	};
    return items;
};
// End Function getItemDataKunjungan_viPntJasaRwi # --------------

/**
*	Function : getItemPanelBawah_viPntJasaRwi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

/**
*	Function : gridDataTrans_viPntJasaRwi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataTrans_viPntJasaRwi()
{
    
    var FieldGrdKasir_viPntJasaRwi = 
	[
	];
	
    dsDataGrdJab_viPntJasaRwi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viPntJasaRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viPntJasaRwi,
        height: 190,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Tanggal',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Reff',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 50
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 355,
				renderer: function(v, params, record) 
				{
					
				}			
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dok',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Tarif',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			}			
        ]
    }    
    return items;
}

function gridDataTransPntJasa_viPntJasaRwi()
{
    
    var FieldGrdKasir_viPntJasaRwi = 
	[
	];
	
    dsDataGrdJab_viPntJasaRwi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viPntJasaRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viPntJasaRwi,
        height: 190,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Tanggal',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Reff',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 50
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 355,
				renderer: function(v, params, record) 
				{
					
				}			
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dok',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Tarif',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			}			
        ]
    }    
    return items;
}

function gridDataTransrekap_viPntJasaRwi()
{
    
    var FieldGrdKasir_viPntJasaRwi = 
	[
	];
	
    dsDataGrdJab_viPntJasaRwi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viPntJasaRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viPntJasaRwi,
        height: 190,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Tanggal',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Reff',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 50
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 355,
				renderer: function(v, params, record) 
				{
					
				}			
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dok',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Tarif',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			}			
        ]
    }    
    return items;
}
// End Function gridDataTrans_viPntJasaRwi # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupProduk_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup List Produk
*/

function FormSetLookupProduk_viPntJasaRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryLookupProduk_viPntJasaRwi = new Ext.Window
    (
        {
            id: 'FormGrdLookupProduk_viPntJasaRwi',
            title: 'List Produk',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryProdukGridDataView_viPntJasaRwi(subtotal,NO_KUNJUNGAN),
                //-------------- ## --------------
            ],
            tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAddTreeProduk_viPntJasaRwi',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDeleteTreeProduk_viPntJasaRwi',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
				]
			}
        }
    );
    vWinFormEntryLookupProduk_viPntJasaRwi.show();
    mWindowGridLookupLookupProduk  = vWinFormEntryLookupProduk_viPntJasaRwi; 
}
// End Function FormSetLookupProduk_viPntJasaRwi # --------------

/**
*   Function : getFormItemEntryProdukGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form produk
*/
function getFormItemEntryProdukGridDataView_viPntJasaRwi(subtotal,NO_KUNJUNGAN)
{
	var lebar = 515;
	var itemsListProdukGridDataView_viPntJasaRwi = 
    {
        xtype: 'panel',
        title: '',
        name: 'PanelListProdukGridDataView_viPntJasaRwi',
        id: 'PanelListProdukGridDataView_viPntJasaRwi',        
        bodyStyle: 'padding: 5px;',
        layout: 'hbox',
        height: 1170,
        border:false,
        items: 
        [           
            itemsTreeListProdukGridDataView_viPntJasaRwi(),
            //-------------- ## -------------- 
            { 
                xtype: 'tbspacer',
                width: 5,
            },
            //-------------- ## -------------- 
            getItemTreeGridTransaksiGridDataView_viPntJasaRwi(lebar),
            //-------------- ## -------------- 
        ]
    }
    return itemsListProdukGridDataView_viPntJasaRwi;
}
// End Function getFormItemEntryProdukGridDataView_viPntJasaRwi # --------------

/**
*   Function : itemsTreeListProdukGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan tree prosuk
*/
function itemsTreeListProdukGridDataView_viPntJasaRwi()
{
	var children = 
	[
		{
		     text:'First Level Child 1',
		     children:
		     [
		     	{
		        	text:'Second Level Child 1',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		        {
		        	text:'Second Level Child 2',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		    ]
		},
		{
		     text:'First Level Child 2',
		     children:
		     [
		     	{
		        	text:'Second Level Child 1',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		        {
		        	text:'Second Level Child 2',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		    ]
		},
	];

    var tree = new Ext.tree.TreePanel
    (
    	{
        	loader:new Ext.tree.TreeLoader(),
        	width:200,
        	height:340,
        	renderTo:Ext.getBody(),
        	root:new Ext.tree.AsyncTreeNode
        	(
        		{
             		expanded:true,
             		leaf:false,
             		text:'Tree Root',
             		children:children
             	}
            )
    	}
    );

    var pnlTreeFormDataWindowPopup_viPntJasaRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                {
		            xtype: 'radiogroup',
		            //fieldLabel: 'Single Column',
		            itemCls: 'x-check-group-alt',
		            columns: 1,
		            items: 
		            [
		                {
		                	boxLabel: 'Semua Unit', 
		                	name: 'ckboxTreeSemuaUnit_viPntJasaRwi',
		                	id: 'ckboxTreeSemuaUnit_viPntJasaRwi',  
		                	inputValue: 1,
		                },
		                //-------------- ## -------------- 
		                {
		                	boxLabel: 'Unit Penyakit Dalam', 
		                	name: 'ckboxTreePerUnit_viPntJasaRwi',
		                	id: 'ckboxTreePerUnit_viPntJasaRwi',  
		                	inputValue: 2, 
		                	checked: true,
		                },
		                //-------------- ## -------------- 
		            ]
		        },
				//-------------- ## -------------- 
                tree,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viPntJasaRwi;
}
// End Function itemsTreeListProdukGridDataView_viPntJasaRwi # --------------

/**
*   Function : getItemTreeGridTransaksiGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemTreeGridTransaksiGridDataView_viPntJasaRwi(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:2px 2px 2px 2px',
        border:true,
        width: lebar,
        height:402, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridTreeDataViewEditGridDataView_viPntJasaRwi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemTreeGridTransaksiGridDataView_viPntJasaRwi # --------------

/**
*   Function : gridTreeDataViewEditGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridTreeDataViewEditGridDataView_viPntJasaRwi()
{
    
    var FieldTreeGrdKasirGridDataView_viPntJasaRwi = 
    [
    ];
    
    dsDataTreeGrdJabGridDataView_viPntJasaRwi= new WebApp.DataStore
    ({
        fields: FieldTreeGrdKasirGridDataView_viPntJasaRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataTreeGrdJabGridDataView_viPntJasaRwi,
        height: 395,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Cek',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi Produk',
                sortable: true,
                width: 330,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridTreeDataViewEditGridDataView_viPntJasaRwi # --------------

// ## END MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupKonsultasi_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Konsultasi
*/

function FormSetLookupKonsultasi_viPntJasaRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryKonsultasi_viPntJasaRwi = new Ext.Window
    (
        {
            id: 'FormGrdLookupKonsultasi_viPntJasaRwi',
            title: 'Konsultasi',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupKonsultasi_viPntJasaRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryKonsultasi_viPntJasaRwi.show();
    mWindowGridLookupKonsultasi  = vWinFormEntryKonsultasi_viPntJasaRwi; 
};

// End Function FormSetLookupKonsultasi_viPntJasaRwi # --------------

/**
*   Function : getFormItemEntryLookupKonsultasi_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/
function getFormItemEntryLookupKonsultasi_viPntJasaRwi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataKonsultasiWindowPopup_viPntJasaRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputKonsultasiDataView_viPntJasaRwi(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataKonsultasiWindowPopup_viPntJasaRwi;
}
// End Function getFormItemEntryLookupKonsultasi_viPntJasaRwi # --------------

/**
*   Function : getItemPanelInputKonsultasiDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/

function viComboJenisTransaksi_viPntJasaRwi(lebar,Nama_ID)
{
  var cbo_viComboJenisTransaksi_viPntJasaRwi = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Transaksi..',
            fieldLabel: "Transaksi",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdTrnsksi',
                        'displayTextTrnsksi'
                    ],
                    data: [['all', "Semua"],[1, "Bayar"],[2, "Belum Bayar"]]
                }
            ),
            valueField: 'IdTrnsksi',
            displayField: 'displayTextTrnsksi',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisTransaksi_viPntJasaRwi;
};
// End Function viComboJenisTransaksi_viPntJasaRwi # --------------

function getItemPanelInputKonsultasiDataView_viPntJasaRwi(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Asal Unit',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPntJasaRwi(250,'CmboAsalUnit_viPntJasaRwi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Konsultasi ke',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPntJasaRwi(250,'CmboKonsultasike_viPntJasaRwi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPntJasaRwi(250,'CmboDokter_viPntJasaRwi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputKonsultasiDataView_viPntJasaRwi # --------------

// ## END MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupGantiDokter_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupGantiDokter_viPntJasaRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_viPntJasaRwi = new Ext.Window
    (
        {
            id: 'FormGrdLookupGantiDokter_viPntJasaRwi',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupGantiDokter_viPntJasaRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_viPntJasaRwi.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_viPntJasaRwi; 
};

// End Function FormSetLookupGantiDokter_viPntJasaRwi # --------------

/**
*   Function : getFormItemEntryLookupGantiDokter_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupGantiDokter_viPntJasaRwi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataGantiDokterWindowPopup_viPntJasaRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputGantiDokterDataView_viPntJasaRwi(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataGantiDokterWindowPopup_viPntJasaRwi;
}
// End Function getFormItemEntryLookupGantiDokter_viPntJasaRwi # --------------

/**
*   Function : getItemPanelInputGantiDokterDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputGantiDokterDataView_viPntJasaRwi(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Tanggal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtTglGantiDokter_viPntJasaRwi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtBlnGantiDokter_viPntJasaRwi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtThnGantiDokter_viPntJasaRwi',
                        emptyText: '2014',
                        width: 50,
                    },
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Asal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPntJasaRwi(250,'CmboDokterAsal_viPntJasaRwi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Ganti',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viPntJasaRwi(250,'CmboDokterGanti_viPntJasaRwi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputGantiDokterDataView_viPntJasaRwi # --------------

// ## END MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

// ## END MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

/**
*   Function : FormSetPembayaran_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Pembayaran
*/

function FormSetPembayaran_viPntJasaRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPembayaran_viPntJasaRwi = new Ext.Window
    (
        {
            id: 'FormGrdPembayaran_viPntJasaRwi',
            title: 'Pembayaran',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryGridDataView_viPntJasaRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPembayaran_viPntJasaRwi.show();
    mWindowGridLookupPembayaran  = vWinFormEntryPembayaran_viPntJasaRwi; 
};

// End Function FormSetPembayaran_viPntJasaRwi # --------------

/**
*   Function : getFormItemEntryGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function getFormItemEntryGridDataView_viPntJasaRwi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataWindowPopup_viPntJasaRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataGridDataView_viPntJasaRwi(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiGridDataView_viPntJasaRwi(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnOkGridTransaksiPembayaranGridDataView_viPntJasaRwi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnCancelGridTransaksiPembayaranGridDataView_viPntJasaRwi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtBiayaGridTransaksiPembayaranGridDataView_viPntJasaRwi',
                            name: 'txtBiayaGridTransaksiPembayaranGridDataView_viPntJasaRwi',
							style:{'text-align':'right','margin-left':'150px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtPiutangGridTransaksiPembayaranGridDataView_viPntJasaRwi',
                            name: 'txtPiutangGridTransaksiPembayaranGridDataView_viPntJasaRwi',
							style:{'text-align':'right','margin-left':'146px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtTunaiGridTransaksiPembayaranGridDataView_viPntJasaRwi',
                            name: 'txtTunaiGridTransaksiPembayaranGridDataView_viPntJasaRwi',
							style:{'text-align':'right','margin-left':'142px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtDiscGridTransaksiPembayaranGridDataView_viPntJasaRwi',
                            name: 'txtDiscGridTransaksiPembayaranGridDataView_viPntJasaRwi',
							style:{'text-align':'right','margin-left':'138px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataWindowPopup_viPntJasaRwi;
}
// End Function getFormItemEntryGridDataView_viPntJasaRwi # --------------

/**
*   Function : getItemPanelInputBiodataGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemPanelInputBiodataGridDataView_viPntJasaRwi(lebar) 
{
    var items =
    {
        title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
				xtype: 'textfield',
				fieldLabel: 'No. Transaksi',
				id: 'txtNoTransaksiPembayaranGridDataView_viPntJasaRwi',
				name: 'txtNoTransaksiPembayaranGridDataView_viPntJasaRwi',
				emptyText: 'No. Transaksi',
				readOnly: true,
				flex: 1,
				width: 195,				
			},
			//-------------- ## --------------
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				id: 'txtNamaPasienPembayaranGridDataView_viPntJasaRwi',
				name: 'txtNamaPasienPembayaranGridDataView_viPntJasaRwi',
				emptyText: 'Nama Pasien',
				flex: 1,
				anchor: '100%',					
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran',
				anchor: '100%',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viPntJasaRwi(195,'CmboPembayaranGridDataView_viPntJasaRwi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				anchor: '100%',
				labelSeparator: '',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viPntJasaRwi(195,'CmboPembayaranDuaGridDataView_viPntJasaRwi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataGridDataView_viPntJasaRwi # --------------

/**
*   Function : getItemGridTransaksiGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemGridTransaksiGridDataView_viPntJasaRwi(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar-80,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridDataView_viPntJasaRwi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiGridDataView_viPntJasaRwi # --------------

/**
*   Function : gridDataViewEditGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridDataViewEditGridDataView_viPntJasaRwi()
{
    
    var FieldGrdKasirGridDataView_viPntJasaRwi = 
    [
    ];
    
    dsDataGrdJabGridDataView_viPntJasaRwi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viPntJasaRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viPntJasaRwi,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kode',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Qty',
                sortable: true,
                width: 40
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Biaya',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Piutang',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tunai',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Disc',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridDataView_viPntJasaRwi # --------------

// ## END MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

/**
*   Function : FormSetTransferPembayaran_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Transfer ke Rawat Inap
*/

function FormSetTransferPembayaran_viPntJasaRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryTransferPembayaran_viPntJasaRwi = new Ext.Window
    (
        {
            id: 'FormGrdTrnsferPembayaran_viPntJasaRwi',
            title: 'Pemindahan Tagihan Ke Unit Rawat Inap',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 450,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormTransferPembayaranItemEntryGridDataView_viPntJasaRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryTransferPembayaran_viPntJasaRwi.show();
    mWindowGridLookupTransferPembayaran  = vWinFormEntryTransferPembayaran_viPntJasaRwi; 
};

// End Function FormSetTransferPembayaran_viPntJasaRwi # --------------

/**
*   Function : getFormTransferPembayaranItemEntryGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form transfer ke rawat inap
*/
function getFormTransferPembayaranItemEntryGridDataView_viPntJasaRwi(subtotal,NO_KUNJUNGAN)
{
    var pnlFormTransferPembayaranDataWindowPopup_viPntJasaRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            items:
            //-------------- #items# --------------
            [
                {
                    xtype: 'fieldset',
                    title: 'Tagihan dipindahkan ke',
                    frame: false,
                    width: 350,
                    height: 165,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Transaksi',
                            id: 'txtNoTransaksiTransaksiTransferPembayaran_viPntJasaRwi',
                            name: 'txtNoTransaksiTransaksiTransferPembayaran_viPntJasaRwi',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
							xtype: 'compositefield',
							fieldLabel: 'Tgl. Lahir',
							anchor: '100%',
							width: 130,
							items: 
							[
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'DateTransaksiTransferPembayaran_viPntJasaRwi',
									name: 'DateTransaksiTransferPembayaran_viPntJasaRwi',
									width: 130,
									format: 'd/M/Y',
								}
								//-------------- ## --------------				
							]
						},
						//-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Rekam Medis',
                            id: 'txtNoRekamMedisTransaksiTransferPembayaran_viPntJasaRwi',
                            name: 'txtNoRekamMedisTransaksiTransferPembayaran_viPntJasaRwi',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nama Pasien',
                            id: 'txtNamaPasienTransaksiTransferPembayaran_viPntJasaRwi',
                            name: 'txtNamaPasienTransaksiTransferPembayaran_viPntJasaRwi',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Unit Perawatan',
                            id: 'txtUnitPerawatanTransaksiTransferPembayaran_viPntJasaRwi',
                            name: 'txtUnitPerawatanTransaksiTransferPembayaran_viPntJasaRwi',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: '',
                    frame: false,
                    width: 350,
                    height: 125,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jumlah biaya',
                            id: 'txtJumlhBiayaTransaksiTransferPembayaran_viPntJasaRwi',
                            name: 'txtJumlhBiayaTransaksiTransferPembayaran_viPntJasaRwi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'PAID',
                            id: 'txtPAIDTransaksiTransferPembayaran_viPntJasaRwi',
                            name: 'txtPAIDTransaksiTransferPembayaran_viPntJasaRwi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jmlh dipindahkan',
                            id: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viPntJasaRwi',
                            name: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viPntJasaRwi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Saldo Tagihan',
                            id: 'txtSaldoTagihanTransaksiTransferPembayaran_viPntJasaRwi',
                            name: 'txtSaldoTagihanTransaksiTransferPembayaran_viPntJasaRwi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: 'Alasan Transfer',
                    frame: false,
                    width: 350,
                    height: 62,
                    items: 
                    [
						{
				            xtype: 'combo',
				            fieldLabel: 'Alsn Transfer',
				            labelSeparator: '',
				        },
						//-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					width: 160,
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnOkTransferPembayaran_viPntJasaRwi',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
						{
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnCancelTransferPembayaran_viPntJasaRwi',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
					]
				},
                //-------------- ## --------------
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormTransferPembayaranDataWindowPopup_viPntJasaRwi;
}
// End Function getFormTransferPembayaranItemEntryGridDataView_viPntJasaRwi # --------------

// ## END MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

/**
*   Function : FormSetEditTarif_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Edit Tarif
*/

function FormSetEditTarif_viPntJasaRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryEditTarif_viPntJasaRwi = new Ext.Window
    (
        {
            id: 'FormGrdEditTarif_viPntJasaRwi',
            title: 'Edit Tarif',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryEditTarifGridDataView_viPntJasaRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryEditTarif_viPntJasaRwi.show();
    mWindowGridLookupEditTarif  = vWinFormEntryEditTarif_viPntJasaRwi; 
};

// End Function FormSetEditTarif_viPntJasaRwi # --------------

/**
*   Function : getFormItemEntryEditTarifGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/
function getFormItemEntryEditTarifGridDataView_viPntJasaRwi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataEditTarifWindowPopup_viPntJasaRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukEditTarifGridDataView_viPntJasaRwi(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiEditTarifGridDataView_viPntJasaRwi(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiEditTarifGridDataView_viPntJasaRwi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiEditTarifGridDataView_viPntJasaRwi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viPntJasaRwi',
                            name: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viPntJasaRwi',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viPntJasaRwi',
                            name: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viPntJasaRwi',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataEditTarifWindowPopup_viPntJasaRwi;
}
// End Function getFormItemEntryEditTarifGridDataView_viPntJasaRwi # --------------

/**
*   Function : getItemPanelInputProdukEditTarifGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemPanelInputProdukEditTarifGridDataView_viPntJasaRwi(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukEditTarifGridDataView_viPntJasaRwi',
                name: 'txtProdukEditTarifGridDataView_viPntJasaRwi',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukEditTarifGridDataView_viPntJasaRwi # --------------

/**
*   Function : getItemGridTransaksiEditTarifGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemGridTransaksiEditTarifGridDataView_viPntJasaRwi(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridEditTarifDataView_viPntJasaRwi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiEditTarifGridDataView_viPntJasaRwi # --------------

/**
*   Function : gridDataViewEditGridEditTarifDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function gridDataViewEditGridEditTarifDataView_viPntJasaRwi()
{
    
    var FieldGrdKasirGridDataView_viPntJasaRwi = 
    [
    ];
    
    dsDataGrdJabGridDataView_viPntJasaRwi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viPntJasaRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viPntJasaRwi,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridEditTarifDataView_viPntJasaRwi # --------------

// ## END MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

/**
*   Function : FormSetPostingManual_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Posting Manual
*/

function FormSetPostingManual_viPntJasaRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPostingManual_viPntJasaRwi = new Ext.Window
    (
        {
            id: 'FormGrdPostingManual_viPntJasaRwi',
            title: 'Posting Manual',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryPostingManualGridDataView_viPntJasaRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPostingManual_viPntJasaRwi.show();
    mWindowGridLookupPostingManual  = vWinFormEntryPostingManual_viPntJasaRwi; 
};

// End Function FormSetPostingManual_viPntJasaRwi # --------------

/**
*   Function : getFormItemEntryPostingManualGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/
function getFormItemEntryPostingManualGridDataView_viPntJasaRwi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataPostingManualWindowPopup_viPntJasaRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukPostingManualGridDataView_viPntJasaRwi(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiPostingManualGridDataView_viPntJasaRwi(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiPostingManualGridDataView_viPntJasaRwi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiPostingManualGridDataView_viPntJasaRwi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viPntJasaRwi',
                            name: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viPntJasaRwi',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viPntJasaRwi',
                            name: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viPntJasaRwi',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataPostingManualWindowPopup_viPntJasaRwi;
}
// End Function getFormItemEntryPostingManualGridDataView_viPntJasaRwi # --------------

/**
*   Function : getItemPanelInputProdukPostingManualGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemPanelInputProdukPostingManualGridDataView_viPntJasaRwi(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukPostingManualGridDataView_viPntJasaRwi',
                name: 'txtProdukPostingManualGridDataView_viPntJasaRwi',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukPostingManualGridDataView_viPntJasaRwi # --------------

/**
*   Function : getItemGridTransaksiPostingManualGridDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemGridTransaksiPostingManualGridDataView_viPntJasaRwi(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridPostingManualDataView_viPntJasaRwi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiPostingManualGridDataView_viPntJasaRwi # --------------

/**
*   Function : gridDataViewEditGridPostingManualDataView_viPntJasaRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function gridDataViewEditGridPostingManualDataView_viPntJasaRwi()
{
    
    var FieldGrdKasirGridDataView_viPntJasaRwi = 
    [
    ];
    
    dsDataGrdJabGridDataView_viPntJasaRwi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viPntJasaRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viPntJasaRwi,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridPostingManualDataView_viPntJasaRwi # --------------

// ## END MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

// ## END MENU PEMBAYARAN ## ------------------------------------------------------------------

// --------------------------------------- # End Form # ---------------------------------------

// End Project Kasir Rawat Jalan # --------------