/*
	*
	*
	* DIBUAT OLEH HADAD
	* UNTUK MEMBUAT GRID RWI DAPAT DI AKSES DI PENATA JASA DAN KASIR RWI
 */
var grid_;
function grid_field_tindakan_rwi() 
{	
    var fldDetail = [
    	'KD_PRODUK', 
    	'KP_PRODUK', 
    	'KD_UNIT', 
    	'DESKRIPSI', 
    	'QTY', 'DOKTER', 
    	'TGL_TINDAKAN', 
    	'QTY', 
    	'DESC_REQ', 
    	'TGL_BERLAKU', 
    	'KD_UNIT_TR', 
		'NO_TRANSAKSI', 
		'URUT', 
		'DESC_STATUS', 
		'TGL_TRANSAKSI', 
		'KD_TARIF', 
		'HARGA', 
		'JUMLAH_DOKTER', 
		'TARIF',
		'KD_KLAS',
		'MANUAL', 
		'NO_FAKTUR', 
		'FOLIO', 
		'GROUP'];
    var field_list = new WebApp.DataStore({fields: fldDetail});
    return field_list;
};

function grid_tindakan_rwi(id,tindakan) 
{	
	grid_ = new Ext.grid.EditorGridPanel({
		title: 'Tindakan Yang Diberikan',
		id: 'PjTransGrid2'+id,
		stripeRows: true,
		height: 90,
		//plugins: [editor],
		store: tindakan,
		border: false,
		frame: false,
		columnLines: true,
		anchor: '100% 80%',
		autoScroll: true,
		sm: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function (sm, row, rec) {
					cellSelecteddeskripsi_kasir_rwi                  = tindakan.getAt(row);
					var line                                         = grid_.getSelectionModel().selection.cell[0];
					dataRowIndexDetail                               = grid_.getSelectionModel().selection.cell[0];
					Current_kasirrwi.row                             = row;
					tmp_group_dokter                                 = cellSelecteddeskripsi_kasir_rwi.data.GROUP;
					Current_kasirrwi.data                            = cellSelecteddeskripsi_kasir_rwi;
					tmpkd_produk                                     = cellSelecteddeskripsi_kasir_rwi.data.KD_PRODUK;
					tmpkd_tarif                                      = cellSelecteddeskripsi_kasir_rwi.data.KD_TARIF;
					tmpharga                                         = cellSelecteddeskripsi_kasir_rwi.data.HARGA;
					tmptgl_transaksi                                 = cellSelecteddeskripsi_kasir_rwi.data.TGL_TRANSAKSI;
					tmpRowGrid                                       = cellSelecteddeskripsi_kasir_rwi.data.URUT;
					currentJasaDokterUrutDetailTransaksi_KasirRWI    = cellSelecteddeskripsi_kasir_rwi.data.URUT;
					currentJasaDokterKdProduk_KasirRWI               = cellSelecteddeskripsi_kasir_rwi.data.KD_PRODUK;
					currentJasaDokterKdTarif_KasirRWI                = cellSelecteddeskripsi_kasir_rwi.data.KD_TARIF;
					currentJasaDokterHargaJP_KasirRWI                = cellSelecteddeskripsi_kasir_rwi.data.HARGA;
					set_variable(cellSelecteddeskripsi_kasir_rwi);
                }
            }
        }),
        cm: TRRawatInapColumModel2(tindakan),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
				jstrdok                          = tindakan.getAt(ridx);
				set_variable(jstrdok);
            }}, tbar: [
            {
                text: 'Tambah',
                id: 'btnLookupRWI_PenataJasaKasirRWI',
                tooltip: 'Tambah Item',
                iconCls: 'add',
                handler: function () {
					var tmpTanggalDefault 	= Ext.getCmp('txtTanggalTransaksiKASIRRWI').getValue();
					if (tmpTanggalDefault.format("Y-m-d")<tmptgl_transaksiInduk.substring(0, 10)) {
						ShowPesanWarningKasirrwi("Tanggal default setting tidak boleh kurang dari tanggal transaksi !",'Peringatan');
					}else if(now.format("Y-m-d") < tmpTanggalDefault.format("Y-m-d")){
						ShowPesanWarningKasirrwi("Tanggal default setting tidak boleh melebihi dari tanggal sekarang !",'Peringatan');
					}else{
	                    tindakan.insert(tindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
	                    Ext.getCmp('PjTransGrid2'+id).startEditing(tindakan.getCount()-1,4);
						batal_isi_tindakan='y';
					}
                }
            },{
				id: 'btnHapusBaris_PenataJasaKasirRWI',
				text: 'Hapus',
				tooltip: 'Hapus Baris',
				iconCls: 'RemoveRow',
				handler: function () {
					if (cellSelecteddeskripsi_kasir_rwi.data.KD_KLAS != '9' || cellSelecteddeskripsi_kasir_rwi.data.MANUAL == 't') {
						HapusBaris_panatajasarwi();
					}else{
						ShowPesanWarningKasirrwi('Produk transfer tidak dapat dihapus !','Peringatan');
					}
				}
            },'-',{
                id      :'btnLookUpProduk_RawatInap',
                text    : 'Lookup Produk',
                iconCls : 'Edit_Tr',
                handler : function(){
                	formLookUpProduk_KasirRawatInap();
                }
            },'-',
            {
                id      :'btnLookUpPreviewBilling',
                text    : 'Preview Billing',
                iconCls : 'Edit_Tr',
                handler : function(){
                	GetDTLTRGridPreviewBilling();
                }
            },'-',
            {
                /* PENAMBAHAN POSTING MANUAL 2017 - 02 -27 */
                id      :'btnLookUpPostingManual_KASIRRWI',
                text    : 'Posting Manual',
                iconCls : 'Edit_Tr',
                handler : function(){
                    dsDataStoreGridPoduk.removeAll();
                    formLookupPostingManual_KASIRRWI();
                }
            },'-',
			{
				xtype: 'checkbox',
				id: 'checkPerawat_KASIRRWI',
				hideLabel: true,
				margin: '0 10 0 20',
				checked : false,
				value : true,
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked == true) {
							tmpKdJob = 3;
							Ext.getCmp('cboDataPerawatPenindak_KASIR_RWI').enable();
						}else{
							tmpKdJob    = 1;
							Ext.getCmp('cboDataPerawatPenindak_KASIR_RWI').disable();
							Ext.getCmp('cboDataPerawatPenindak_KASIR_RWI').setValue('');
							tmpKdDokter = "";
						}
					}
				}
			},'-',
                mCombo_Perawat_KASIRRWI(),
            '-',
            {
				xtype: 'checkbox',
				id: 'checkDokter_KASIRRWI',
				hideLabel: true,
				margin: '0 10 0 20',
				checked : false,
				value : true,
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked == true) {
							tmpKdJob = 3;
							Ext.getCmp('cboDataDokterPenindak_KASIR_RWI').enable();
						}else{
							tmpKdJob    = 1;
							Ext.getCmp('cboDataDokterPenindak_KASIR_RWI').disable();
							Ext.getCmp('cboDataDokterPenindak_KASIR_RWI').setValue('');
							tmpKdDokter = "";
						}
					}
				}
			},'-',
				mCombo_dokter_KASIRRWI(),
            {
                xtype: 'datefield',
                fieldLabel: 'Tanggal Transaksi',
                maxLength: 100,
                name: 'txtTanggalTransaksiKASIRRWI',
                id: 'txtTanggalTransaksiKASIRRWI',
                width: 100,
                disabled: false,
                anchor: '100%',
                format: 'd/m/Y',
                value   : now,
				listeners: {
					render: function (c)
					{
						c.getEl().on('keypress', function (e) {
							if (e.getKey() == 13){
								var tmpTanggalDefault 	= Ext.getCmp('txtTanggalTransaksiKASIRRWI').getValue();
								if (tmpTanggalDefault.format("Y-m-d")<tmptgl_transaksiInduk.substring(0, 10)) {
									Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
								}else if(now.format("Y-m-d") < tmpTanggalDefault.format("Y-m-d")){
									Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').disable();
								}else{
									Ext.getCmp('btnLookupRWI_PenataJasaKasirRWI').enable();
								}
							}
						}, c);
                    }
				},
			},
		],
        viewConfig: {forceFit: true},
    });
	return grid_;
};


function TRRawatInapColumModel2(tindakan) {
    return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            header      : 'Tanggal Transaksi',
            dataIndex   : 'TGL_TRANSAKSI',
            width       : 130,
            menuDisabled: true,
            editor      : new Ext.form.TextField({
                    id                  : 'fieldcolTglTransaksirwi',
                    allowBlank          : true,
                    enableKeyEvents     : true,
                    width               : 30,
                    listeners           :
                    {
                        'specialkey' : function(a, b){
                            var line 			= grid_.getSelectionModel().selection.cell[0];
                            if (b.getKey() === b.ENTER) {
                            	console.log(tindakan.getRange()[line].get('URUT'));
                            // console.log(PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('FOLIO')); 
	                            if (tindakan.getRange()[line].get('FOLIO') == "A") {
	                            	var tmp_urut = tindakan.getRange()[line].get('URUT');
	                            	tmp_urut = tmp_urut-1;
	                                senderProduk(
	                                    notransaksi, 
	                                    tmpkd_produk, 
	                                    tmpkd_tarif, 
	                                    currentKdKasir_KasirRWI, 
	                                    tmp_urut, 
	                                    tindakan.getRange()[line].get('UNIT'), 
	                                    Ext.getCmp('fieldcolTglTransaksirwi').getValue(),
	                                    tindakan.data.items[line].data.TGL_BERLAKU,
	                                    tindakan.getRange()[line].get('QTY'),
	                                    tmpharga,
	                                    'A',
	                                    false,
	                                    line,
	                                    'tanggal',
	                                    0
	                                );
	                                grid.startEditing(line, 3);
	                            }else{
									ShowPesanWarningKasirrwi('Maaf produk transfer tidak dapat di rubah!','WARNING');
	                            }
                            }
                        }
                    }
            }),
        },
        {
            id: 'colReffRWI',
            header: 'No Faktur',
            dataIndex: 'NO_FAKTUR',
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colKdUnitRWI',
            header: 'Unit',
            dataIndex: 'KD_UNIT_TR',
            width: 75,
            menuDisabled: true,
            hidden: false
        },
        {
            id: 'colKdProduk_panatajasarwi2',
            header: 'Kode Produk',
            dataIndex: 'KP_PRODUK',
            selectOnFocus: true,
            width: 100,
            menuDisabled: true,
            editor      : new Ext.form.TextField({
                    id                  : 'fieldcolKdProdukrwi',
                    allowBlank          : true,
                    enableKeyEvents     : true,
                    width               : 30,
                    listeners           :
                    { 
                        'specialkey' : function(a, b)
                        {
                            var line = grid_.getSelectionModel().selection.cell[0];
                            if (b.getKey() === b.ENTER) {
                                Ext.Ajax.request
                                ({
                                    store   : data_store.produk,
                                    url     : baseURL + "index.php/main/functionRWI/getProdukKey",
                                    params  :  {
                                        kd_unit         : tmpkd_unitKamar,
                                        //kd_unit         : kodeunit,
                                        kd_customer     : CurrentKDCustomerRWI,
                                        text            : Ext.getCmp('fieldcolKdProdukrwi').getValue(),
                                    },
                                    failure : function(o)
                                    {
                                        grid_.startEditing(line, 2);
                                        ShowPesanWarningKasirrwi("Data produk tidak ada!",'WARNING');
                                    },
                                    success : function(o)
                                    {
                                        var cst = Ext.decode(o.responseText);
                                        if (cst.processResult == 'SUCCESS'){
                                            tmpkd_produk 			= cst.listData.kd_produk;
                                            tmpkd_tarif            	= cst.listData.kd_tarif;
                                            tmpdeskripsi           	= cst.listData.deskripsi;
                                            tmpharga               	= cst.listData.tarifx;
                                            tmptgl_berlaku         	= cst.listData.tgl_berlaku;
                                            tmpjumlah              	= cst.listData.jumlah;
                                            tmpstatus_konsultasi   	= cst.listData.status_konsultasi;
                                            var kasirRWIUrut=0;
                                            if(line>=1){
                                                kasirRWIUrut=toInteger(tindakan.data.items[line-1].data.URUT); 
                                            }
                                            tindakan.data.items[line].data.URUT               = kasirRWIUrut+1;
                                            senderProduk(
                                                notransaksi, 
                                                tmpkd_produk, 
                                                tmpkd_tarif, 
                                                currentKdKasir_KasirRWI, 
                                                kasirRWIUrut, 
                                                cst.listData.kd_unit, 
                                                tindakan.getRange()[line].get('TGL_TRANSAKSI'),
                                                cst.listData.tgl_berlaku,
                                                1,
                                                tmpharga,
                                                'A',
                                                true,
                                                line,
                                                'produk',
                                                cst.listData
                                            );
											RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
                                        }else{
                                            ShowPesanWarningKasirrwi('Data produk tidak ada!','WARNING');
                                        };
                                    }
                                })
                            }else if(b.getKey() === b.TAB){
                                grid_.startEditing(line, 3);
                            }
                        },
                    }
                }),
        },
		{
            id: 'colDeskripsiRWI2',
            header: 'Nama Produk',
            dataIndex: 'DESKRIPSI',
            sortable: false,
            hidden: false,
            menuDisabled: true,
            width: 250,/*
            editor: PenataJasaKasirRWI.form.ComboBox.produk = Nci.form.Combobox.autoComplete({
                store: PenataJasaKasirRWI.form.DataStore.produk,
                select: function (a, b, c) {
					tmpkd_produk                                                        = b.data.kd_produk;
					tmpkd_tarif                                                         = b.data.kd_tarif;
					tmpdeskripsi                                                        = b.data.deskripsi;
					tmpharga                                                            = b.data.harga;
					tmptgl_berlaku                                                      = b.data.tglberlaku;
					tmpjumlah                                                           = b.data.jumlah;
					tmpstatus_konsultasi                                                = b.data.status_konsultasi;
					var line                                                            = grid_.getSelectionModel().selection.cell[0];
					console.log(b);
					tindakan.getRange()[line].data.KD_PRODUK   = b.data.kd_produk;
					tindakan.getRange()[line].data.KD_TARIF    = b.data.kd_tarif;
					tindakan.getRange()[line].data.HARGA       = b.data.harga;
					tindakan.getRange()[line].data.TGL_BERLAKU = b.data.tglberlaku;
					tindakan.getRange()[line].data.QTY         = 1;
					PenataJasaKasirRWI.URUT                                             = tidnakan.getRange()[line].data.URUT;
					PenataJasaKasirRWI.QTY                                              = 1;
					PenataJasaKasirRWI.TGL_BERLAKU                                      = b.data.tglberlaku;
					PenataJasaKasirRWI.HARGA                                            = b.data.tarif;
					PenataJasaKasirRWI.KD_TARIF                                         = b.data.kd_tarif;
					PenataJasaKasirRWI.TGL_TRANSAKSI                                    = b.data.TGL_TRANSAKSI;
					PenataJasaKasirRWI.TGL_TRANSAKSI_detailtransaksi                    = b.data.TGL_TRANSAKSI;
					PenataJasaKasirRWI.DESKRIPSI                                        = b.data.deskripsi;
					tittle_trdok                                                        = b.data.deskripsi; 
					var currenturut= line + 1;
                    if (line === 0)
                    {
                        PenataJasaKasirRWI.URUT = 1;
                    } else {
                        PenataJasaKasirRWI.URUT = parseInt(tindakan.getRange()[line - 1].data.URUT) + 1;
                    }

                    var kasirRWIUrut=0;
                    if(line>=1){
                        kasirRWIUrut=toInteger(tindakan.data.items[line-1].data.URUT); 
                    }
                    tindakan.data.items[line].data.URUT               = kasirRWIUrut+1;

                    senderProduk(
                        notransaksi, 
                        tmpkd_produk, 
                        tmpkd_tarif, 
                        currentKdKasir_KasirRWI, 
                        kasirRWIUrut, 
                        tindakan.getRange()[line].get('UNIT'), 
                        tindakan.getRange()[line].get('TGL_TRANSAKSI'),
                        tindakan.data.items[line].data.TGL_BERLAKU,
                        tindakan.getRange()[line].get('QTY'),
                        tmpharga,
                        'A',
                        true,
                        line,
                        'deskripsi',
                        b.data
                    );
					RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
                },
                param: function () {
                    var o = grListTRKasirrwi.getSelectionModel().getSelections()[0].data;
                    var params = {};
                    params['kd_unit'] = o.KD_UNIT;
                    params['kd_customer'] = o.KD_CUSTOMER;

                    return params;
                },
                insert: function (o) {
                    return {
                        kdbagian : o.kd_bagian,
                        kdCustomer : o.kd_customer,
                        kd_produk: o.kd_produk,
                        deskripsi: o.deskripsi,
                        harga: o.tarif,
                        tglberlaku: o.tgl_berlaku,
						kd_tarif:o.kd_tarif,
                        text: '<table style="font-size: 11px;"><tr><td width="50">' + o.kp_produk + '</td><td width="160" align="left">' + o.deskripsi + '</td><td width="40" align="left">' + o.tarif + '</td></tr></table>'
                    }
                },
                url: baseURL + "index.php/main/functionRWI/getProdukdeskripsi",
                valueField: 'deskripsi',
				displayField: 'text'
            })*/
        },{
            id: 'colHARGA_penatajasa2',
            header: 'Harga',
            align: 'right',
            hidden: true,
            menuDisabled: true,
            dataIndex: 'HARGA',
            renderer: function (v, params, record)
            {
                return formatCurrency(record.data.HARGA);

            }
        },
		{
			id: 'coljmlhdokRWI2',
			header: 'Dok',
			width: 50,
			dataIndex: 'JUMLAH_DOKTER',
			hidden: false,
			listeners:{
				dblclick: function(dataview, index, item, e) {
					console.log(cellSelecteddeskripsi_kasir_rwi);
					loaddatastoredokter_REVISI();
					loaddatastoredokterVisite_REVISI();
					PilihDokterLookUpKasir_RWI_REVISI();
				}
			}
        },
        {
            id: 'colQTY_penatajasa2',
            header: 'Qty',
            width: 50,
            align: 'right',
            menuDisabled: true,
            dataIndex: 'QTY',
            editor: new Ext.form.TextField
                    (
                            {
                                id: 'fieldcolQTY_penatajasa2',
                                allowBlank: true,
                                enableKeyEvents: true,
                                width: 100,
                                listeners:
								{
									'blur': function (a)
									{
										PenataJasaKasirRWI.URUT = Current_kasirrwi.data.data.URUT;
										PenataJasaKasirRWI.KD_PRODUK = Current_kasirrwi.data.data.KD_PRODUK;
										PenataJasaKasirRWI.QTY = a.getValue();
										PenataJasaKasirRWI.HARGA = Current_kasirrwi.data.data.HARGA;
										PenataJasaKasirRWI.KD_TARIF = Current_kasirrwi.data.data.KD_TARIF;
										PenataJasaKasirRWI.TGL_BERLAKU = Current_kasirrwi.data.data.TGL_BERLAKU;
									},
                                    'specialkey' : function(a, b)
                                    {
										var line = grid.getSelectionModel().selection.cell[0];
                                        var kasirRWIUrut=toInteger(tindakan.data.items[line].data.URUT)-1;
                                        if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
                                            var jumlah = parseInt(tmpharga)*parseInt(Ext.getCmp("fieldcolQTY_penatajasa2").getValue());
                                            console.log(tindakan.getRange()[line]);
                                            tindakan.getRange()[line].set('TARIF', jumlah);
                                            senderProduk(
                                                notransaksi, 
                                                tmpkd_produk, 
                                                tmpkd_tarif, 
                                                currentKdKasir_KasirRWI, 
                                                kasirRWIUrut, 
                                                tmpkd_unit, 
                                                tindakan.getRange()[line].get('TGL_TRANSAKSI'),
                                                tindakan.data.items[line].data.TGL_BERLAKU,
                                                Ext.getCmp("fieldcolQTY_penatajasa2").getValue(),
                                                tmpharga,
                                                'A',
                                                false,
                                                line,
                                                'qty',
                                                tindakan.getRange()[line].data
                                            );
											RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
                                            tindakan.getRange()[line].set('UNIT',tmpkd_unit);
											tindakan.insert(tindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
											Ext.getCmp('PjTransGrid2'+id).startEditing(tindakan.getCount()-1,4);
                                        }
                                    },

                                }

                            }
                    )



        },
        {
            id: 'colTarifRWI',
            header: 'Tarif',
            dataIndex: 'TARIF',
            hidden: false,
			listeners:{
				dblclick: function(dataview, index, item, e) {
					PilihEditTarifLookUp_KasirRWI(tindakan.data.items[item].data);
				}
			}
        },
        {
            id: 'colFolioRWI',
            header: 'Folio',
            dataIndex: 'FOLIO',
            hidden: false,
        },
	    {
	        id: 'colUrutRWI',
	        header: 'urut',
	        dataIndex: 'URUT',
	        menuDisabled: true,
	        hidden: true
	    },
		{
            id: 'xbu',
            header: 'xbu',
            align: 'right',
            hidden: true,
            menuDisabled: true,
            renderer: function(v, params, record)
				{
					var a='';
					if (record.data.NO_TRANSAKSI !== '')
					{
					}
				}

        }, 

    ]
            );
};
///---------------------------------------------------------------------------------------///