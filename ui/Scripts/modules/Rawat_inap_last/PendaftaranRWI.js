// Data Source
var TmpRWI = '2';
var dataSource_viDaftarRWI;
var datasource_gettempat;
var datagetappto;
var selectCount_viDaftarRWI=50;
var NamaForm_viDaftarRWI="Pendaftaran RWI";
var mod_name_viDaftarRWI="viDaftarIGD";

var now_viDaftarRWI= new Date;
var Today_PendaftaranIGD = new Date;
var h=now_viDaftarRWI.getHours();
var m=now_viDaftarRWI.getMinutes();
var s=now_viDaftarRWI.getSeconds();
var year=now_viDaftarRWI.getFullYear();
var nowSerahIjazah_viDaftarRWI= new Date();
var urutJenjang_viDaftarRWI;
var ds_PROPINSI_viKasirRwj;
var addNew_viDaftarRWI;
var rowSelected_viDaftarRWI;
var tmp_tempat;
var setLookUps_viDaftarRWI;
var ds_KABUPATEN_viKasirRwj;
var ds_KECAMATAN_viKasirRwj;
var tampung;
var BlnIsDetail;
var SELECTDATASTUDILANJUT;

var tmpnotransaksi;
var tmpkdkasir;

var selectPoliPendaftaranIGD;
var selectKelompokPoli;

var selectPendidikanPendaftaranIGD;
var selectPekerjaanPendaftaranIGD;
var selectPendaftaranIGDStatusMarital;
var selectAgamaPendaftaranIGD;
var mNoKunjungan_viKasir='1';
var selectSetJK;
var selectSetPerseorangan;
var selectSetAsuransi;
var selectSetGolDarah;
var selectSetSatusMarital;
var selectSetKelompokPasien;
var selectSetRujukanDari;
var selectSetNamaRujukan;
var dsAgamaRequestEntry;
//var selectAgamaRequestEntry;
var dsPropinsiRequestEntry;
var selectPropinsiRequestEntry;
var dsKecamatanRequestEntry;
//var selectKecamtanRequestEntry;
var dsPendidikanRequestEntry;
var dsPekerjaanRequestEntry;
var dsDokterRequestEntry;
var dsKabupatenRequestEntry;
var selectSetWarga;
var txtedit = "Tambah Data";
var selectAgamaRequestEntry;
var selectPropinsiRequestEntry;
var selectKabupatenRequestEntry;
var selectKecamatanRequestEntry;
var tmpcriteriaRWI = "transaksi.kd_kasir = '02'";
var tmpbayibrlahir;


//var tmpapp = FALSE;
//var tmpkontrol = FALSE;
//var tmpcetakkartu = FALSE;


var CurrentData_viDaftarRWI =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viDaftarRWI(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

datarefresh_viDaftarRWI(tmpcriteriaRWI);

function dataGrid_viDaftarRWI(mod_id)
{
       var FieldMaster_viDaftarRWI = 
	[
                    'KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'AGAMA',
                    'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN', 'PENDIDIKAN', 'PEKERJAAN',
                    'NAMA_UNIT','TGL_MASUK', 'URUT_MASUK','KD_KELURAHAN','KABUPATEN','KECAMATAN','PROPINSI',
                    'KD_KABUPATEN','KD_KECAMATAN','KD_PROPINSI','KD_PENDIDIKAN','KD_PEKERJAAN','KD_AGAMA'
	];

    dataSource_viDaftarRWI = new WebApp.DataStore({fields: FieldMaster_viDaftarRWI});
    
	datarefresh_viDaftarRWI(tmpcriteriaRWI);
            
	var grData_viDaftarRWI = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viDaftarRWI,
			autoScroll: true,
			columnLines: true,
			border:false,
                        trackMouseOver: true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viDaftarRWI = undefined;
							rowSelected_viDaftarRWI = dataSource_viDaftarRWI.getAt(row);
							CurrentData_viDaftarRWI;
							CurrentData_viDaftarRWI.row = row;
							CurrentData_viDaftarRWI.data = rowSelected_viDaftarRWI.data;
                                                        
                                                }
                                        }
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
                                        
                                        rowSelected_viDaftarRWI = dataSource_viDaftarRWI.getAt(ridx);
					if (rowSelected_viDaftarRWI !== undefined)
					{
						setLookUp_viDaftarRWI(rowSelected_viDaftarRWI.data);
                                                
					}
					else
					{
						setLookUp_viDaftarRWI();
					}
				},
                        },
                                

			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNRM_viDaftarRWI',
						header: 'No.Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 35
//						filter:
//						{
//							type: 'int'
//						}
					},
					{
						id: 'colNMPASIEN_viDaftarRWI',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 35
//						filter:
//						{}
					},
					{
						id: 'colALAMAT_viDaftarRWI',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true
//						filter: {}
					},
					{
						id: 'colTglKunj_viDaftarRWI',
						header:'Tgl Lahir',
						dataIndex: 'TGL_LAHIR',
						width: 50,
						sortable: true,
						format: 'd/M/Y',
//						filter: {},
						renderer: function(v, params, record)
						{

                                                       return ShowDate(record.data.TGL_LAHIR);
						}
					},
					{
						id: 'colPoliTujuan_viDaftarRWI',
						header:'Poli Tujuan',
						dataIndex: 'NAMA_UNIT',
						width: 50,
						sortable: true
//						filter: {}
					}
				]
				),
			//tbar1:
                        tbar:
                            {
				xtype: 'toolbar',
				id: 'toolbar_viDaftarRWI',
				items:
                                    [
                                            {
						xtype: 'button',
						text: 'Kunjungan',
                                                hidden : false,
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viDaftarRWI',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viDaftarRWI != undefined)
							{
                                                            setLookUp_viDaftarRWI(rowSelected_viDaftarRWI.data);
                                                            
							}
							else
							{
                                                            ShowPesanWarning_viDaftarRWI('Silahkan pilih Data Pasien ','Edit Data');
							}
						}
					},
                                        {
                                                xtype: 'checkbox',
                                                id: 'chkWithTglRequest',
                                                text: '',
                                                hideLabel:false,
                                                checked: false,

                                                handler: function()
                                                  {
                                                    if (this.getValue()===true)
                                                    {
                                                        criteria = "kunjungan.tgl_masuk = " + "'" + now_viDaftarRWI.format('Y/M/d') + "'" + "and " + tmpcriteriaRWI;
                                                        datarefresh_viDaftarRWI(criteria);
                                                    }
                                                    else
                                                    {
                                                        criteria = tmpcriteriaRWI
                                                        datarefresh_viDaftarRWI(criteria);
                                                    }
                                                  }
                                            },
                                            {xtype: 'tbtext', text: 'Data Pasien Hari Ini ', cls: 'left-label', width: 90},
                                    ]},

			bbar : bbar_paging(mod_name_viDaftarRWI, selectCount_viDaftarRWI, dataSource_viDaftarRWI),
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
     var top = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [{
            layout:'column',
            items:[{
                columnWidth: .3,
                layout: 'form',
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Medrec (Enter Untuk Mencari)',
                        name: 'txtNoMedrecD_vidaftarIGD',
                        id: 'txtNoMedrecD_vidaftarIGD',
                        //width: 50,
                        //emptyText:'Tekan Enter Untuk Mencari...',
                        enableKeyEvents: true,
                        anchor: '95%',
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrecD_vidaftarIGD').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrecD_vidaftarIGD').getValue())
                                             Ext.getCmp('txtNoMedrecD_vidaftarIGD').setValue(tmpgetNoMedrec);
                                             var tmpkriteria = getCriteriaFilter_viDaftarRWI();
                                             datarefresh_viDaftarRWI(tmpkriteria);
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                        tmpkriteria = getCriteriaFilter_viDaftarRWI();
                                                        datarefresh_viDaftarRWI(tmpkriteria);
                                                    }
                                                    else
                                                    Ext.getCmp('txtNoMedrec_vidaftarIGD').setValue('')
                                            }
                                }
                            }

                        }

                    },{
                        xtype: 'textfield',
                        fieldLabel: 'Tanggal Lahir (ddMMYYYY) ',
                        name: 'txtTglLahir_vidaftarIGD',
                        id: 'txtTglLahir_vidaftarIGD',
                        enableKeyEvents: true,
                        anchor: '95%',
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmptanggal = Ext.get('txtTglLahir_vidaftarIGD').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                       if (tmptanggal.length !== 0)
                                        {
                                            if(tmptanggal.length === 8)
                                                {
                                                    var tmptgl = ShowDateUbah(Ext.get('txtTglLahir_vidaftarIGD').getValue())
                                                    Ext.getCmp('txtTglLahir_vidaftarIGD').setValue(tmptgl);
                                                }
                                                else
                                                    {
                                                         if(tmptanggal.length === 10)
                                                            {
                                                                 var tmpkriteria = getCriteriaFilter_viDaftarRWI();
                                                                 datarefresh_viDaftarRWI(tmpkriteria);
                                                            }
                                                            else
                                                                {
                                                                    alert("Format yang Anda masukan salah...");
                                                                }
                                                     }
                                        }
                                            tmpkriteria = getCriteriaFilter_viDaftarRWI();
                                            datarefresh_viDaftarRWI(tmpkriteria);
                                }
                            }

                        }

                    }
             ]
            },{
                columnWidth:.5,
                layout: 'form',
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Pasien ',
                        name: 'txtNama_viDaftarRWIPasien_vidaftarIGD',
                        id: 'txtNama_viDaftarRWIPasien_vidaftarIGD',
                        enableKeyEvents: true,
                        anchor: '95%',
                        listeners:
                        {
                            'keyup': function()
                            {
                                        var tmpkriteria = getCriteriaFilter_viDaftarRWI();
                                       datarefresh_viDaftarRWI(tmpkriteria);
                            }
                        }
                    },
                    {
                    xtype: 'textfield',
                    fieldLabel: 'Alamat Pasien ',
                    name: 'txtAlamat_viDaftarRWIPasien_vidaftarIGD',
                    enableKeyEvents: true,
                    id: 'txtAlamat_viDaftarRWIPasien_vidaftarIGD',
                    anchor: '95%',
                    listeners:
                        {
                            'keyup': function()
                            {
                                        var tmpkriteria = getCriteriaFilter_viDaftarRWI();
                                        datarefresh_viDaftarRWI(tmpkriteria);
                            }
                        }
                }
             ]
            }]
        }]       
    });

    //top.render(document.body);

    var FrmTabs_viDaftarRWI = new Ext.Panel
    (
		{
			id: mod_id,
			region: 'center',
			layout: 'form',
			title: NamaForm_viDaftarRWI,          
			border: false,  
			closable: true,
			iconCls: 'Studi_Lanjut',
			margins: '5 5 5 5',
                        items: [ top , grData_viDaftarRWI],
//			tbar2 
			listeners: 
			{ 
				'afterrender': function() 
				{           
					datarefresh_viDaftarRWI(tmpcriteriaRWI);
				}
			}
		}
    )
    // datarefresh_viDaftarRWI();
    return FrmTabs_viDaftarRWI;
}

function setLookUp_viDaftarRWI(rowdata)
{
    var lebar = 900;
    setLookUps_viDaftarRWI = new Ext.Window
    (
    {
        id: 'SetLookUps_viDaftarRWI',
        name: 'SetLookUps_viDaftarRWI',
        title: NamaForm_viDaftarRWI, 
        closeAction: 'destroy',        
        width: lebar,
        height: 650,//575,
        resizable:false,
	autoScroll: false,

        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viDaftarRWI(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboSukuRequestEntry').hide(); //suku di hide
                Ext.getCmp('txtJamKunjung').hide();
                Ext.getCmp('dptTanggal').hide();
                Ext.getCmp('txtNama_viDaftarRWIPeserta').hide();
                Ext.getCmp('txtNoAskes').hide();
                Ext.getCmp('txtNoSJP').hide();
                Ext.getCmp('cboPerseorangan').show();
                Ext.getCmp('cboAsuransi').hide();
                Ext.getCmp('cboPerusahaanRequestEntry').hide();
                if(rowdata !== undefined)
                {
                    Ext.getCmp('txtNama_viDaftarRWI').disable();
                    Ext.getCmp('txtNoRequest_viDaftarRWI').disable();
                    Ext.getCmp('txtNama_viDaftarRWI').disable();
                    Ext.getCmp('txtNama_viDaftarRWIKeluarga').disable();
                    Ext.getCmp('txtTempatLahir_viDaftarRWI').disable();
                    Ext.getCmp('cboPendidikanRequestEntry').disable();
                    Ext.getCmp('cboPekerjaanRequestEntry').disable();
                    Ext.getCmp('cboWarga').disable();
                    Ext.getCmp('txtAlamat_viDaftarRWI').disable();
                    Ext.getCmp('cboAgamaRequestEntry').disable();
                    Ext.getCmp('cboGolDarah').disable();
                    Ext.getCmp('dtpTanggalLahir_viDaftarRWI').disable();
                    Ext.getCmp('cboStatusMarital').disable();
                    Ext.getCmp('cboJK').disable();
                    Ext.getCmp('cboWarga').disable();
                    Ext.getCmp('cboPropinsiRequestEntry').disable();
                    Ext.getCmp('cboKabupatenRequestEntry').disable();
                    Ext.getCmp('cboKecamatanRequestEntry').disable();
                }
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viDaftarRWI=undefined;
                datarefresh_viDaftarRWI(tmpcriteriaRWI);
		mNoKunjungan_viKasir = '';
                Ext.getCmp('txtNoMedrecD_vidaftarIGD').setValue('');
                Ext.getCmp('txtTglLahir_vidaftarIGD').setValue('');
                Ext.getCmp('txtAlamat_viDaftarRWIPasien_vidaftarIGD').setValue('');
                Ext.getCmp('txtNama_viDaftarRWIPasien_vidaftarIGD').setValue('');
            }
        }
    }
    );

    setLookUps_viDaftarRWI.show();
    if (rowdata == undefined)
    {
        dataaddnew_viDaftarRWI();
    }
    else
    {
        datainit_viDaftarRWI(rowdata);
    }
}

// From view popup data daftar pasien / kunjungan
function getFormItemEntry_viDaftarRWI(lebar,rowdata)
{
    var pnlFormDataBasic_viDaftarRWI = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',//'center',
			layout: 'form',//'anchor',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
                        autoScroll: true,
			width: lebar,//lebar-55,
                        height: 900,
			border: false,

                        items:[
                            getItemPanelInputBiodata_viDaftarRWI(),
                            getPenelItemDataKunjungan_viDaftarRWI(lebar)
                                ],
			fileUpload: true,
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viDaftarRWI',
						handler: function(){
							dataaddnew_viDaftarRWI();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viDaftarRWI',
						handler: function()
						{
							datasave_viDaftarRWI(addNew_viDaftarRWI);
							datarefresh_viDaftarRWI(tmpcriteriaRWI);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viDaftarRWI',
						handler: function()
						{
							var x = datasave_viDaftarRWI(addNew_viDaftarRWI);
							datarefresh_viDaftarRWI(tmpcriteriaRWI);
							if (x===undefined)
							{
								setLookUps_viDaftarRWI.close();
							}
						}
					},
                                        {
						xtype: 'tbseparator'
					},
					{
						xtype:'button',
						text:'Lookup Pasien',
						iconCls:'find',
						id:'btnLookUp_viDaftarRWI',
						handler:function(){
							FormLookupPasien("","","","","","");
						}
					},					
					{
						xtype:'tbseparator'
					},
					{
						xtype:'splitbutton',
						text:'Print Kartu Pasien',
						iconCls:'print',
						id:'btnPrint_viDaftarRWI',
						handler:function()
						{							
                                                    GetPrintKartu();								
						},
                                                //handler: optionsHandler, // handle a click on the button itself
                                                menu: new Ext.menu.Menu({
                                                    items: [
                                                        // these items will render as dropdown menu items when the arrow is clicked:
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
//                                                            iconCls: 'remove',
                                                            id: 'btnPrintBill',
                                                            handler: function()
                                                            {
                                                                printbill()
//                                                                    datadelete_viDaftarRWI();
//                                                                    datarefresh_viDaftarRWI();

                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print ',
//                                                            iconCls: 'remove',
                                                            id: 'btnPrint',
                                                            handler: function()
                                                            {
                                                                
                                                                 var criteria = 'kd_pasien = '+ Ext.get('txtNoRequest_viDaftarRWI').getValue() +' And kd_unit = '+ Ext.getCmp('cboPoliklinikRequestEntry').getValue();
                                                                 ShowReport('0', 1010204, criteria);

                                                            }
                                                        },
                                                    ]
                                                })
					}
                                        
				]
			}
//                        ,items:
			
		}
    )
    

    return pnlFormDataBasic_viDaftarRWI;
}
//-----------------------------end----------------------------------------------

//form view data biodata pasien
function getItemPanelInputBiodata_viDaftarRWI() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
            labelAlign: 'top',
	    items:
		[
                        {
			    columnWidth: .20,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpKecamatan_viDaftarRWI',
					    id: 'txtTmpKecamatan_viDaftarRWI',
                                            emptyText:'',
                                            hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpKabupaten_viDaftarRWI',
					    id: 'txtTmpKabupaten_viDaftarRWI',
                                            emptyText:'',
                                            hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpPropinsi_viDaftarRWI',
					    id: 'txtTmpPropinsi_viDaftarRWI',
                                            emptyText:'',
                                            hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpPendidikan_viDaftarRWI',
					    id: 'txtTmpPendidikan_viDaftarRWI',
                                            emptyText:'',
                                            hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpPekerjaan_viDaftarRWI',
					    id: 'txtTmpPekerjaan_viDaftarRWI',
                                            emptyText:'',
                                            hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpAgama_viDaftarRWI',
					    id: 'txtTmpAgama_viDaftarRWI',
                                            emptyText:'',
                                            hidden : true,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .20,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'No. Medrec ',
					    name: 'txtNoRequest_viDaftarRWI',
					    id: 'txtNoRequest_viDaftarRWI',
                                            emptyText:'Automatic from the system ...',
                                            readOnly:true,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Nama ',
					    name: 'txtNama_viDaftarRWI',
					    id: 'txtNama_viDaftarRWI',
                                            emptyText:' ',
                                            disabled:false,
					    anchor: '95%',
//                                            regex:/^((([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z\s?]{2,5}){1,25})*(\s*?;\s*?)*)*$/,
                                            enableKeyEvents: true,
                                            listeners: {
                                                    'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('txtNama_viDaftarRWIKeluarga').focus();
                                                    }, c);
                                                }
                                            }
					}
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Nama Keluarga ',
					    name: 'txtNama_viDaftarRWIKeluarga',
					    id: 'txtNama_viDaftarRWIKeluarga',
                                            emptyText:' ',
					    anchor: '95%',
                                            listeners: {
                                                    'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('cboJK').focus();
                                                    }, c);
                                                }
                                            }
					}
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                  mComboJK()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Tempat Lahir ',
					    name: 'txtTempatLahir_viDaftarRWI',
					    id: 'txtTempatLahir_viDaftarRWI',
                                            emptyText:' ',
					    anchor: '95%',
                                            listeners: {
                                                    'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('dtpTanggalLahir_viDaftarRWI').focus();
                                                    }, c);
                                                }
                                            }
					}
				]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal Lahir ',
					    name: 'dtpTanggalLahir_viDaftarRWI',
					    id: 'dtpTanggalLahir_viDaftarRWI',
                                            //format: 'M/d/Y',
                                            format : "d/M/Y",
                                            value: Today_PendaftaranIGD,
					    anchor: '95%',
                                            listeners:
                                                {
						'specialkey' : function()
                                                    {
							if (Ext.EventObject.getKey() === 9 || Ext.EventObject.getKey() === 13) // 9 Kode Kode Char Untuk Tab
							{
                                                            var year1=Ext.get('dtpTanggalLahir_viDaftarRWI').getValue();
                                                                var fullyearnow= now_viDaftarRWI.format('d/M/Y');
                                                                var tmp = fullyearnow.substring(7, 11);
                                                                var tmp2 = year1.substring(7, 11);
                                                                var Umur = tmp - tmp2
//                                                                alert(year1)
//                                                                alert(fullyearnow)
                                                            if(tmp2 < tmp)
                                                                {
                                                                    Ext.getCmp('txtThnLahir_viDaftarRWI').setValue(Umur);
                                                                    Ext.getCmp('txtBlnLahir_viDaftarRWI').setValue('0');
                                                                    Ext.getCmp('txtHariLahir_viDaftarRWI').setValue('0');
                                                                }
                                                                else if(tmp2 == tmp)
                                                                    {
                                                                        var tmpblnlahir = ShowNewDate(year1)
                                                                        var tmpblnsekarang = ShowNewDate(fullyearnow)
                                                                        //var bulan =  tmpblnsekarang - tmpblnlahir
                                                                        Ext.getCmp('txtThnLahir_viDaftarRWI').setValue('0');
                                                                        Ext.getCmp('txtBlnLahir_viDaftarRWI').setValue('0');
                                                                        Ext.getCmp('txtHariLahir_viDaftarRWI').setValue('0');
                                                                        //alert(tmpblnlahir)
                                                                        //alert(tmpblnsekarang)
//                                                                       var umurku = datediff(year1,fullyearnow,'Y')
//                                                                       alert(umurku)
                                                                    }
                                                                else
                                                                    {
                                                                        Ext.getCmp('txtThnLahir_viDaftarRWI').setValue('0');
                                                                        Ext.getCmp('txtBlnLahir_viDaftarRWI').setValue('0');
                                                                        Ext.getCmp('txtHariLahir_viDaftarRWI').setValue('0');
                                                                    }
                                                                
								
							}
                                                    },
                                                   'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('cboAgamaRequestEntry').focus();
                                                    }, c);

                                                    }
                                                }

					}
				]
                                
			},
                          {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:10,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Thn ',
					    name: 'txtThnLahir_viDaftarRWI',
					    id: 'txtThnLahir_viDaftarRWI',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                                                {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:10,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Bln ',
					    name: 'txtBlnLahir_viDaftarRWI',
					    id: 'txtBlnLahir_viDaftarRWI',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                                                {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:10,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Hari ',
					    name: 'txtHariLahir_viDaftarRWI',
					    id: 'txtHariLahir_viDaftarRWI',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                        {
			    columnWidth: .22,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
                                 mComboAgamaRequestEntry(),
				]
			},
                        {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:0,
                            
			    items:
				[
                                 mComboGolDarah()
				]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
                                  mComboWargaNegara()
                                ]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
				mComboSatusMarital()
				]
			},
                        
                        {
			    columnWidth: .70,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Alamat ',
					    name: 'txtAlamat_viDaftarRWI',
					    id: 'txtAlamat_viDaftarRWI',
                                            emptyText:' ',
					    anchor: '95%',
                                            listeners: {
                                                    'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('cboPropinsiRequestEntry').focus();
                                                    }, c);
                                                }
                                            }
					}
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    mComboPropinsiRequestEntry()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    mComboKabupatenRequestEntry()
                                ]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                      mComboKecamatanRequestEntry()
				]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                     mComboPendidikanRequestEntry()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    mComboPekerjaanRequestEntry()
				]
			},
                        {
			    columnWidth: .13,
			    layout: 'form',
			    border: false,
                            
                            labelWidth:90,
			    items:
				[
                                     mComboSukuRequestEntry()
                                ]
                        },
                ]
        };
    return items;
};
//-------------------------end--------------------------------------------------
var autohideandshowunitperawat;

//sub form view data kunjungan pasien 1
function getItemDataKunjungan_viDaftarRWI1()
{
   
        var items =
            {

                layout: 'column',
                border: false,
                labelAlign: 'top',
                items:
		[
			{
			    columnWidth: .15,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    name: 'dtpTanggal',
					    id: 'dptTanggal',
                                            format: 'd/M/Y',
                                            value: now_viDaftarRWI,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
                                        xtype: 'textfield',
                                        id: 'txtJamKunjung',
                                        value: h+':'+m+':'+s,
					width: 120,
                                        anchor: '95%',
					listeners:
					{
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13)
							{
								 
							}
						}
					}
                                    }
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
                                    mComboPoliklinik(),
                                    mComboKelasSpesialRequestEntry(),
                                    mComboRuanganRequestEntry()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
                                   mComboKamarRequestEntry(),
                                   {
                                        xtype: 'textfield',
                                        fieldLabel: 'Sisa Kamar',
                                        maxLength: 100,
                                        name: 'txtSisaKamar_viDaftarRWI',
                                        id: 'txtSisaKamar_viDaftarRWI',
                                        width: 50,
                                        disabled: true,
                                        anchor: '95%'
                                     },  
                                     {
                                     
                                        xtype: 'checkbox',
                                        id: 'chkbayibrlahir',
                                        fieldLabel: 'Bayi Baru Lahir',
                                        handler: function()
                                          {
                                            if (this.getValue()===true)
                                            {
                                                  tmpbayibrlahir = 'True';
                                            }
                                            else
                                            {
                                                tmpbayibrlahir = 'False';
                                            }
                                          
                                            }
                                  },                                          
				]
			},
                        
                        {
			    columnWidth: .30,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                  {  

                                    xtype: 'combo',
                                    fieldLabel: 'Kelompok Pasien',
                                    id: 'kelPasien',
                                     editable: false,
                                    //value: 'Perseorangan',
                                    store: new Ext.data.ArrayStore
                                        (
                                            {
                                            id: 0,
                                            fields:
                                            [
						'Id',
						'displayText'
                                            ],
                                               data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                            }
                                        ),
                                  displayField: 'displayText',
                                  mode: 'local',
                                  width: 100,
                                  forceSelection: true,
                                  triggerAction: 'all',
                                  emptyText: 'Pilih Salah Satu...',
                                  selectOnFocus: true,
                                  anchor: '95%',
                                  listeners:
                                     {
                                            'select': function(a, b, c)
                                        {
                                           Combo_Select(b.data.displayText)
                                        }

                                    }
                                  }
				]
			},
                        
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    mComboPerseorangan()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    mComboPerusahaan()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    mComboAsuransi()
				]
			},
                        {
                            columnWidth: .20,
                            layout: 'form',
                            border: false,
                            labelWidth:90,
                            items:
                                [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Nama Peserta',
                                        maxLength: 200,
                                        name: 'txtNama_viDaftarRWIPeserta',
                                        id: 'txtNama_viDaftarRWIPeserta',
                                        width: 100,
                                        anchor: '95%'
                                     }
                                ]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'No. Askes',
                                        maxLength: 200,
                                        name: 'txtNoAskes',
                                        id: 'txtNoAskes',
                                        width: 100,
                                        anchor: '95%'
                                     }
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                     {
                                        xtype: 'textfield',
                                        fieldLabel: 'No. SJP',
                                        maxLength: 200,
                                        name: 'txtNoSJP',
                                        id: 'txtNoSJP',
                                        width: 100,
                                        anchor: '95%'
                                     }
				]
			}
                        
                        
                ]

        };
    return items;
};
//-------------------------end--------------------------------------------------

//form data pasien tab
function getPenelItemDataKunjungan_viDaftarRWI(lebar)
{
    var items =
	{
	   xtype:'tabpanel',
           plain:true,
           activeTab: 0,
           height:300,
           //deferredRender: false,
           defaults:
           {
            bodyStyle:'padding:10px',
            autoScroll: true
           },
           items:[
                    DataPanel1(),
                    DataPanel2(),
                    DataPanel3()
                            
                 ]
        }
    return items;
};
//------------------end---------------------------------------------------------

function subdatapanel1()
{
    var items=
    {
           xtype: 'fieldset',
            title: 'Rujukan',
            //Height: 50,
            items: [
                      {
                columnWidth: .80,
                layout: 'form',
                labelWidth:90,
                border: false,
                items:
                    [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Anamnese ',
                                name: 'txtAnamnese',
                                id: 'txtAnamnese',
                                emptyText:' ',
                                anchor: '95%'
                            }
                    ]
            },
            {
                columnWidth: .80,
                layout: 'form',
                border: false,
                labelWidth:90,
                items:
                    [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Alergi ',
                                name: 'txtAlergi',
                                id: 'txtAlergi',
                                emptyText:' ',
                                anchor: '95%'
                            }
                    ]
            }  

            ]   
    };
    return items;
}

//sub form data pasien tab1
function DataPanel1()
{
        var items =
        {
            title:'Kunjungan',
            layout:'form',
            items:
                [
                    getItemDataKunjungan_viDaftarRWI1(),
                    subdatapanel1()
                ]
        };
        return items;
}
//------------------end---------------------------------------------------------

//sub form data pasein tab2
function DataPanel2()
{
    var items =
        {
            title:'Penerimaan',
            layout:'form',
            items:
                [
                    {
                        xtype: 'fieldset',
                        title: 'Rujukan',
                        //Height: 50,
                        items: [{
                                    xtype: 'radiogroup',
                                    id: 'rbrujukan',
                                    //fieldLabel: 'Auto Layout',
                                    items: [
                                    {boxLabel: 'Perseorangan', name: 'rb-auto', inputValue: 1},
                                    {boxLabel: 'Rujukan', name: 'rb-auto', inputValue: 2}
                                            ]
                                }
                        ]
                    },
//                       mComboRujukanDari(),
                        mcomborujukandari(),
                        mComboRujukan(),
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama ',
                        name: 'txtNama_viDaftarRWIPerujuk',
                        id: 'txtNama_viDaftarRWIPerujuk',
                        width: 250
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Alamat ',
                        name: 'txtAlamat_viDaftarRWIPerujuk',
                        id: 'txtAlamat_viDaftarRWIPerujuk',
                        width: 500
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kota ',
                        name: 'txtKotaPerujuk',
                        id: 'txtKotaPerujuk'
                    }
                ]
                                
                               
                                 
        };
        return items;
};
//------------------end---------------------------------------------------------

//sub foem data pasien tab3
function DataPanel3()
{
    var items =
    {
       
                                 title:'Penanggung Jawab',
                                 layout:'form',
                                 items:
                                     [
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Nama ',
					    name: 'txtNama_viDaftarRWIPenanggungjawab',
					    id: 'txtNama_viDaftarRWIPenanggungjawab',
                                            width: 200
                                        },
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Alamat ',
					    name: 'txtAlamat_viDaftarRWIPenanggungjawab',
					    id: 'txtAlamat_viDaftarRWIPenanggungjawab',
                                            width: 300
                                        },
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Kota ',
					    name: 'txtKotaPenanggungjawab',
					    id: 'txtKotaPenanggungjawab',
                                            width: 150
                                        },
                                        {

                                            xtype: 'textfield',
					    fieldLabel: 'Kode Pos ',
					    name: 'txtKdPosPerusahaanPenanggungjawab',
					    id: 'txtKdPosPerusahaanPenanggungjawab',
                                            width: 50
                                        },
                                        {

                                            xtype: 'textfield',
					    fieldLabel: 'Tlp ',
					    name: 'txtTlpPenanggungjawab',
					    id: 'txtTlpPenanggungjawab',
                                            width: 100
                                        },
                                        {

                                            xtype: 'textfield',
					    fieldLabel: 'Pekerjaan ',
					    name: 'txtPekerjaanPenanggungjawab',
					    id: 'txtPekerjaanPenanggungjawab',
                                            width: 100
                                        },
                                        {

                                            xtype: 'textfield',
					    fieldLabel: 'Nama Prsh ',
					    name: 'txtNama_viDaftarRWIPerusahaanPenanggungjawab',
					    id: 'txtNama_viDaftarRWIPerusahaanPenanggungjawab',
                                            width: 200
                                        },
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Alamat Prsh ',
					    name: 'txtAlamat_viDaftarRWIPrshPenanggungjawab',
					    id: 'txtAlamat_viDaftarRWIPrshPenanggungjawab',
                                            width: 300
                                        },
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Hub. Kel ',
					    name: 'txtHubKelPenanggungjawab',
					    id: 'txtHubKelPenanggungjawab',
                                            width: 100
                                        }
                                     ]
            
    };
    return items;
}
//------------------end---------------------------------------------------------

function datasave_viDaftarRWI(mBol)
{	
    if (ValidasiEntry_viDaftarRWI('Simpan Data',false) === 1 || ValidasiEntry_viDaftarRWI('Simpan Data',false) === 0 )
    {
        //alert('a')
        if (addNew_viDaftarRWI === true)
        {
            Ext.Ajax.request
            (
				{
					url: baseURL + "index.php/main/CreateDataObj",
					params: dataparam_viDaftarRWI(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfo_viDaftarRWI('Data berhasil di simpan','Simpan Data');
							datarefresh_viDaftarRWI(tmpcriteriaRWI);
							// if(mBol === false)
							// {
								// Ext.get('txtID_viDaftarRWI').dom.value=cst.ID_SETUP;
							// };
							addNew_viDaftarRWI = false;
							//Ext.get('txtNoKunjungan_viDaftarRWI').dom.value = cst.NO_KUNJUNGAN
							Ext.get('txtNoRequest_viDaftarRWI').dom.value = cst.KD_PASIEN;
                                                        printbill();
							Ext.getCmp('btnSimpan_viDaftarRWI').disable();
							Ext.getCmp('btnSimpanExit_viDaftarRWI').disable();
//							Ext.getCmp('btnDelete_viDaftarRWI').enable();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning_viDaftarRWI('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
						else
						{
							ShowPesanError_viDaftarRWI('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
					}
				}
            )
        }
        else
        {
            Ext.Ajax.request
            (
            {
                url: WebAppUrl.UrlUpdateData,
                params: dataparam_viDaftarRWI(),
                success: function(o)
                {
                    //alert(o);
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                        ShowPesanInfo_viDaftarRWI('Data berhasil disimpan','Edit Data');
                        datarefresh_viDaftarRWI(tmpcriteriaRWI);
                    }
                    else if  (cst.success === false && cst.pesan===0)
                    {
                        ShowPesanWarning_viDaftarRWI('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                    }
                    else
                    {
                        ShowPesanError_viDaftarRWI('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                    }
                }
            }
            )
        }
    }
    else
    {
        if(mBol === true)
        {
            return false;
        }
    }
}

function datadelete_viDaftarRWI()
{
    // var DataHapus = Ext.get('txtNPP_viDaftarRWI').getValue();
    if (ValidasiEntry_viDaftarRWI('Hapus Data',true) == 1 )
    {
        Ext.Msg.show
        (
        {
            title:'Hapus Data',
             msg: "Akan menghapus data?" ,
            buttons: Ext.MessageBox.YESNO,
            width:300,
            fn: function (btn)
            {
                if (btn =='yes')
                {
                    Ext.Ajax.request
                    (
                    {
                        url: WebAppUrl.UrlDeleteData,
                        params: dataparamDelete_viDaftarRWI(), //dataparam_viDaftarRWI(),
                        success: function(o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfo_viDaftarRWI('Data berhasil dihapus','Hapus Data');
                                datarefresh_viDaftarRWI(tmpcriteriaRWI);
                                dataaddnew_viDaftarRWI();
								Ext.getCmp('btnSimpan_viDaftarRWI').disable();
								Ext.getCmp('btnSimpanExit_viDaftarRWI').disable();	
								Ext.getCmp('btnDelete_viDaftarRWI').disable();	
								
                            }
                            else if (cst.success === false && cst.pesan===0)
                            {
                                ShowPesanWarning_viDaftarRWI('Data tidak berhasil dihapus ' ,'Hapus Data');
                            }
                            else if (cst.success === false && cst.pesan===1)
                            {
                                ShowPesanError_viDaftarRWI('Data tidak berhasil dihapus '  + cst.msg ,'Hapus Data');
                            }
                        }
                    }
                    )//end Ajax.request
                } // end if btn yes
            }// end fn
        }
        )//end msg.show
    }
}
//-----------------------------------------------------------------------------------------------------------------///

function ValidasiEntry_viDaftarRWI(modul,mBolHapus)
{
    var x = 1;
	
	if (Ext.get('txtNama_viDaftarRWI').getValue() === " ")
	{
		ShowPesanWarning_viDaftarRWI("Nama belum terisi...",modul);
		x=0;
	}
	if (Ext.get('txtNama_viDaftarRWIKeluarga').getValue() === " ")
	{
		ShowPesanWarning_viDaftarRWI("Nama keluarga belum terisi...",modul);
		x=0;
	}
        if (Ext.getCmp('cboJK').getValue() === "")
	{
		ShowPesanWarning_viDaftarRWI("Jenis Kelamin belum dipilih...",modul);
		x=0;
	}
	if (Ext.get('txtTempatLahir_viDaftarRWI').getValue() === " ")
	{
		ShowPesanWarning_viDaftarRWI("Tempat Lahir belum terisi...",modul);
		x=0;
	}
//        if (Ext.get('dtpTanggalLahir_viDaftarRWI').getValue() > now_viDaftarRWI.format('d/M/Y'))
//	{
//		ShowPesanWarning_viDaftarRWI("Tanggal Lahir lebih besar dari tanggal sekarang...",modul);
//		x=0;
//	}
                            
                    
//                   Ext.getCmp('cboAgamaRequestEntry').getValue(),
//                    Ext.getCmp('cboGolDarah').getValue(),
//                   Ext.getCmp('cboStatusMarital').getValue(),
//                   StatusWargaNegara,
//                    Ext.get('txtAlamat_viDaftarRWI').getValue(),
//                    Ext.getCmp('cboPropinsiRequestEntry').getValue(),
//                    Ext.getCmp('cboPendidikanRequestEntry').getValue(),
//                    Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
//                    Ext.get('txtNama_viDaftarRWIPeserta').getValue(),
//                    Ext.get('txtNoAskes').getValue(),
//                    Ext.get('txtNoSJP').getValue(),

    return x;
}

function ShowPesanWarning_viDaftarRWI(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanError_viDaftarRWI(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

function ShowPesanInfo_viDaftarRWI(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}

function datarefresh_viDaftarRWI(criteria)
{
	
    criteria = "((left(kunjungan.kd_unit,1) in ('2','9') and kunjungan.tgl_masuk >= '2015-02-16') or (left(kunjungan.kd_unit,1)='3')) Order By kunjungan.Tgl_masuk desc, kunjungan.Jam_masuk Desc"

    dataSource_viDaftarRWI.load
    (
		{
			params:
			{
				Skip: 0,
				Take: selectCount_viDaftarRWI,
				Sort: '',
				Sortdir: 'ASC',
				target:'ViDaftarRwi',
				param: criteria
			}
		}
    );
    return dataSource_viDaftarRWI;
}

function getCriteriaFilter_viDaftarRWI()
{
      	 var strKriteria = "";

           if (Ext.get('txtNoMedrecD_vidaftarIGD').getValue() != "")
            {
                strKriteria = " pasien.kd_pasien = " + "'" + Ext.get('txtNoMedrecD_vidaftarIGD').getValue() +"'" + "And " + tmpcriteriaRWI;
            }
            if (Ext.get('txtTglLahir_vidaftarIGD').getValue() != "")
            {
                if (strKriteria == "")
                    {
                         strKriteria = " pasien.tgl_lahir = " + "'" + Ext.get('txtTglLahir_vidaftarIGD').getValue() +"'" + "And " + tmpcriteriaRWI;
                    }
                    else
                        {
                            strKriteria += " and pasien.tgl_lahir = " + "'" + Ext.get('txtTglLahir_vidaftarIGD').getValue() +"'"+ "And " + tmpcriteriaRWI;
                        }
                
            }
            if (Ext.get('txtAlamat_viDaftarRWIPasien_vidaftarIGD').getValue() != "")
            {
                if (strKriteria == "")
                    {
                         strKriteria = " lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamat_viDaftarRWIPasien_vidaftarIGD').getValue() +"%')"+ "And " + tmpcriteriaRWI;
                    }
                    else
                        {
                            strKriteria += " and lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamat_viDaftarRWIPasien_vidaftarIGD').getValue() +"%')"+ "And " + tmpcriteriaRWI;
                        }
                
            }
            if (Ext.get('txtNama_viDaftarRWIPasien_vidaftarIGD').getValue() != "")
            {
                if (strKriteria == "")
                    {
                        strKriteria = " lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNama_viDaftarRWIPasien_vidaftarIGD').getValue() +"%')"+ "And " + tmpcriteriaRWI;
                    }
                    else
                        {
                            strKriteria += " and lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNama_viDaftarRWIPasien_vidaftarIGD').getValue() +"%')"+ "And " + tmpcriteriaRWI;
                        }
                
            }
	 return strKriteria;
}

function datainit_viDaftarRWI(rowdata)
{
        addNew_viDaftarRWI = false;
        addNew_viDaftarRWI = true;
        dsPropinsiRequestEntry = undefined;
	Ext.getCmp('txtNoRequest_viDaftarRWI').setValue(rowdata.KD_PASIEN);
	Ext.getCmp('txtNama_viDaftarRWI').setValue(rowdata.NAMA);
	Ext.getCmp('txtNama_viDaftarRWIKeluarga').setValue(rowdata.NAMA_KELUARGA);
	Ext.getCmp('txtTempatLahir_viDaftarRWI').setValue(rowdata.TEMPAT_LAHIR);
	Ext.getCmp('cboPendidikanRequestEntry').setValue(rowdata.PENDIDIKAN);
        Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowdata.PEKERJAAN);
	Ext.getCmp('cboWarga').setValue(rowdata.WNI);
	Ext.getCmp('txtAlamat_viDaftarRWI').setValue(rowdata.ALAMAT);
	Ext.getCmp('cboAgamaRequestEntry').setValue(rowdata.AGAMA);
        Ext.getCmp('cboGolDarah').setValue(rowdata.GOL_DARAH);
        Ext.get('dtpTanggalLahir_viDaftarRWI').dom.value = ShowDate(rowdata.TGL_LAHIR);
        Ext.getCmp('cboStatusMarital').setValue(rowdata.STATUS_MARITA);
        var tmpjk = "";
        var tmpwni = "";
        if (rowdata.JENIS_KELAMIN === "t")
            {
                tmpjk = "Laki - Laki";
            }
            else
                {
                    tmpjk = "Perempuan";
                }
        Ext.getCmp('cboJK').setValue(tmpjk);
        if (rowdata.WNI === "t")
            {
                tmpwni = "WNI";
            }
            else
                {
                    tmpwni = "WNA";
                }
        Ext.getCmp('cboWarga').setValue(tmpwni);
        //getdatatempat_viDaftarRWI(rowdata.KD_PASIEN);
       
        Ext.getCmp('cboPropinsiRequestEntry').setValue(rowdata.PROPINSI);
        Ext.getCmp('cboKabupatenRequestEntry').setValue(rowdata.KABUPATEN);
        Ext.getCmp('cboKecamatanRequestEntry').setValue(rowdata.KECAMATAN);
        Ext.getCmp('txtTmpPropinsi_viDaftarRWI').setValue(rowdata.KD_PROPINSI);
        Ext.getCmp('txtTmpKabupaten_viDaftarRWI').setValue(rowdata.KD_KABUPATEN);
        Ext.getCmp('txtTmpKecamatan_viDaftarRWI').setValue(rowdata.KD_KECAMATAN);
        Ext.getCmp('txtTmpPendidikan_viDaftarRWI').setValue(rowdata.KD_PENDIDIKAN);
        Ext.getCmp('txtTmpPekerjaan_viDaftarRWI').setValue(rowdata.KD_PEKERJAAN);
        Ext.getCmp('txtTmpAgama_viDaftarRWI').setValue(rowdata.KD_AGAMA);
       	
       	
	//Ext.getCmp('btnSimpan_viDaftarRWI').disable();
	//Ext.getCmp('btnSimpanExit_viDaftarRWI').disable();	
	Ext.getCmp('btnDelete_viDaftarRWI').enable();
        Ext.getCmp('btnUlang_viDaftarRWI').enable();
	mNoKunjungan_viKasir = rowdata.NO_KUNJUNGAN;
	
}

function dataaddnew_viDaftarRWI()
{
    addNew_viDaftarRWI = true;
    dsPropinsiRequestEntry = undefined;
	Ext.getCmp('txtNoRequest_viDaftarRWI').setValue('');
	Ext.getCmp('txtNama_viDaftarRWI').setValue('');
	Ext.getCmp('txtNama_viDaftarRWIKeluarga').setValue('');
	Ext.getCmp('txtTempatLahir_viDaftarRWI').setValue('');
	Ext.getCmp('txtThnLahir_viDaftarRWI').setValue('');
	Ext.getCmp('txtBlnLahir_viDaftarRWI').setValue('');
	Ext.getCmp('txtHariLahir_viDaftarRWI').setValue('');
	Ext.getCmp('cboWarga').setValue('');
	Ext.getCmp('txtAlamat_viDaftarRWI').setValue('');
	Ext.getCmp('txtJamKunjung').setValue(h+':'+m+':'+s);
        Ext.getCmp('txtNama_viDaftarRWIPeserta').setValue('');
        Ext.getCmp('txtNoAskes').setValue('');
        Ext.getCmp('txtNoSJP').setValue('');
        Ext.getCmp('txtAnamnese').setValue('');
        Ext.getCmp('txtAlergi').setValue('');
        Ext.getCmp('txtNama_viDaftarRWIPerujuk').setValue('');
        Ext.getCmp('txtAlamat_viDaftarRWIPerujuk').setValue('');
        Ext.getCmp('txtAlamat_viDaftarRWIPenanggungjawab').setValue('');
        Ext.getCmp('txtKotaPenanggungjawab').setValue('');
        Ext.getCmp('txtKdPosPerusahaanPenanggungjawab').setValue('');
        Ext.getCmp('txtTlpPenanggungjawab').setValue('');
        Ext.getCmp('txtPekerjaanPenanggungjawab').setValue('');
        Ext.getCmp('txtNama_viDaftarRWIPerusahaanPenanggungjawab').setValue('');
        Ext.getCmp('txtAlamat_viDaftarRWIPrshPenanggungjawab').setValue('');
        Ext.getCmp('txtHubKelPenanggungjawab').setValue('');
        Ext.getCmp('cboJK').setValue('');
	Ext.getCmp('cboAgamaRequestEntry').setValue('');
        Ext.getCmp('cboGolDarah').setValue('');
        Ext.getCmp('cboPropinsiRequestEntry').setValue('');
        Ext.getCmp('cboKabupatenRequestEntry').setValue('');
        Ext.getCmp('cboKecamatanRequestEntry').setValue('');
        Ext.getCmp('cboPendidikanRequestEntry').setValue('');
        Ext.getCmp('cboSukuRequestEntry').setValue('');
        Ext.getCmp('cboStatusMarital').setValue('');
//        Ext.getCmp('cboRujukanDari').setValue('');
//        Ext.getCmp('cboDokterRequestEntry').setValue('');
        Ext.getCmp('cboPerseorangan').setValue('');
        Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
        Ext.getCmp('cboAsuransi').setValue('');
        Ext.getCmp('cboPoliklinikRequestEntry').setValue('');
        Ext.getCmp('dtpTanggalLahir_viDaftarRWI').setValue(now_viDaftarRWI);
        Ext.getCmp('cboPekerjaanRequestEntry').setValue
        Ext.getCmp('txtTmpPropinsi_viDaftarRWI').setValue('');
        Ext.getCmp('txtTmpKabupaten_viDaftarRWI').setValue('');
        Ext.getCmp('txtTmpKecamatan_viDaftarRWI').setValue('');
        Ext.getCmp('txtTmpPendidikan_viDaftarRWI').setValue('');
        Ext.getCmp('txtTmpPekerjaan_viDaftarRWI').setValue('');
        Ext.getCmp('txtTmpAgama_viDaftarRWI').setValue('');


	Ext.getCmp('btnSimpan_viDaftarRWI').enable();
	Ext.getCmp('btnSimpanExit_viDaftarRWI').enable();
//	Ext.getCmp('btnDelete_viDaftarRWI').disable();
//        Ext.getCmp('btnUlang_viDaftarRWI').disable();

//	mNoKunjungan_viKasir = '';
	
    rowSelected_viDaftarRWI   = undefined;
}
///---------------------------------------------------------------------------------------///

function dataparam_viDaftarRWI()
{
    var jenis_kelamin;
    if (Ext.getCmp('cboJK').getValue() === 1)
    {
        jenis_kelamin = true;
    }
    else
        {
            jenis_kelamin = false;
        }
    var tmpwarga;    
    if (Ext.getCmp('cboWarga').getValue() === 'WNI')
    {
        tmpwarga = 'true';
    }
    else
        {
            tmpwarga = 'false';
        }

		var params_ViPendaftaranIGD =
		{
      
                    Table: 'ViDaftarRwi',
                    NoMedrec:  Ext.get('txtNoRequest_viDaftarRWI').getValue(),
                    NamaPasien: Ext.get('txtNama_viDaftarRWI').getValue(),
                    NamaKeluarga: Ext.get('txtNama_viDaftarRWIKeluarga').getValue(),
                    JenisKelamin: jenis_kelamin,
                    Tempatlahir: Ext.get('txtTempatLahir_viDaftarRWI').getValue(),
                    TglLahir : Ext.get('dtpTanggalLahir_viDaftarRWI').getValue(),
                    Agama: Ext.getCmp('cboAgamaRequestEntry').getValue(),
                    GolDarah: Ext.getCmp('cboGolDarah').getValue(),
                    StatusMarita: Ext.getCmp('cboStatusMarital').getValue(),
                    StatusWarga: tmpwarga,
                    Alamat : Ext.get('txtAlamat_viDaftarRWI').getValue(),
                    No_Tlp: '',
                    
                    Pendidikan: Ext.getCmp('cboPendidikanRequestEntry').getValue(),
                    Pekerjaan: Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
                    NamaPeserta : Ext.get('txtNama_viDaftarRWIPeserta').getValue(),
                    KD_Asuransi : 1,
                    NoAskes : Ext.get('txtNoAskes').getValue(),
                    NoSjp : Ext.get('txtNoSJP').getValue(),
                    Kd_Suku: 0,
                    Jabatan : '',
                    Perusahaan: Ext.getCmp('cboPerusahaanRequestEntry').getValue(),
                    Perusahaan1 : 0,
                    Cek: Ext.get('kelPasien').getValue(),

                    
                    TanggalMasuk : Ext.get('dptTanggal').getValue(),
                    UrutMasuk:0,
                    JamKunjungan : now_viDaftarRWI,
                    CaraPenerimaan: Ext.getCmp('cboRujukanDariRequestEntry').getValue(),
                    KdRujukan: Ext.getCmp('cboRujukanRequestEntry').getValue(),
                    
                    Baru: false,
                    KdCustomer: Ext.getCmp('cboAsuransi').getValue(),
                    Shift: 1,
                    Karyawan: 0,
                    Kontrol: false,
                    Antrian:0,
                    NoSurat:'',
                    Alergi: Ext.get('txtAlergi').getValue(),
                    Anamnese: Ext.get('txtAnamnese').getValue(),

                    TahunLahir : Ext.get('txtThnLahir_viDaftarRWI').getValue(),
                    BulanLahir : Ext.get('txtBlnLahir_viDaftarRWI').getValue(),
                    HariLahir : Ext.get('txtHariLahir_viDaftarRWI').getValue(),
                    
                     //NamaPerujuk : Ext.get('txtNama_viDaftarRWIPerujuk').getValue(),
                    //AlamatPerujuk : Ext.get('txtAlamat_viDaftarRWIPerujuk').getValue(),
                    Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
                    Kamar : Ext.getCmp('cboKamarRequestEntry').getValue(),
                    Kelas : Ext.getCmp('cboKelasRequestEntry').getValue(),
                    Ruang : Ext.getCmp('cboRuanganRequestEntry').getValue(),
                    KDUnitKamar : tmpkdunitkamar,
                    BayiBrLahir: tmpbayibrlahir,
                    
                    Kota: Ext.getCmp('cboKabupatenRequestEntry').getValue(),
                    
                    Kd_Kecamatan: Ext.getCmp('cboKecamatanRequestEntry').getValue(),
                    Kelurahan: Ext.getCmp('cboPropinsiRequestEntry').getValue(),
                    AsalPasien: Ext.getCmp('cboKabupatenRequestEntry').getValue(),
                    
                    KDPROPINSI : Ext.getCmp('txtTmpPropinsi_viDaftarRWI').getValue(),
                    KDKABUPATEN: Ext.getCmp('txtTmpKabupaten_viDaftarRWI').getValue(),
                    KDKECAMATAN: Ext.getCmp('txtTmpKecamatan_viDaftarRWI').getValue(),
                    
                    KDPENDIDIKAN : Ext.getCmp('txtTmpPendidikan_viDaftarRWI').getValue(),
                    KDPEKERJAAN: Ext.getCmp('txtTmpPekerjaan_viDaftarRWI').getValue(),
                    KDAGAMA: Ext.getCmp('txtTmpAgama_viDaftarRWI').getValue()  
		};
    return params_ViPendaftaranIGD
}

function dataparamDelete_viDaftarRWI()
{
		var paramsDelete_ViPendaftaranIGD =
		{
			Table: 'viKunjungan',			
			// KD_KELOMPOK: Ext.getCmp('ComboKelompokDaftarIGD_viDaftarRWI').getValue(),
			// KD_UNIT: Ext.getCmp('ComboPoliDaftarIGD_viDaftarRWI').getValue(),
			// KD_DOKTER: Ext.getCmp('ComboDokterDaftarIGD_viDaftarRWI').getValue(),
			NO_KUNJUNGAN : Ext.get('txtNoKunjungan_viDaftarRWI').getValue(), //Ext.getCmp('txtNoKunjungan_viDaftarRWI').setValue(rowdata.NO_KUNJUNGAN);		
			KD_CUSTOMER: strKD_CUST
			// KD_PASIEN: Ext.get('txtKDPasien_viDaftarRWI').getValue(),
			// TGL_KUNJUNGAN: ShowDate(Ext.get('dtpKunjungan_viDaftarRWI').getValue()),
			// TINGGI_BADAN : Ext.get('txtTinggiBadan_viDaftarRWI').getValue(),
			// BERAT_BADAN : Ext.get('txtBeratBadan_viDaftarRWI').getValue(),
			// TEKANAN_DRH : Ext.get('txtTekananDarah_viDaftarRWI').getValue(),
			// NADI : Ext.get('txtNadi_viDaftarRWI').getValue(),
			// ALERGI :  Ext.get('txtchkAlergi_viDaftarRWI').dom.checked,
			// KELUHAN : Ext.get('txtkeluhan_viDaftarRWI').getValue(),  
			// PASIEN_BARU : Ext.get('txtchkPasienBaru_viDaftarRWI').dom.checked,						
			// KD_PENDIDIKAN : Ext.getCmp('ComboPendidikan_viDaftarRWI').getValue(), //PASIEN
			// KD_STS_MARITAL : Ext.getCmp('ComboSTS_MARITAL_viDaftarRWI').getValue(),
			// KD_AGAMA :  Ext.getCmp('ComboAgama_viDaftarRWI').getValue(),
			// KD_PEKERJAAN : Ext.getCmp('ComboPekerjaan_viDaftarRWI').getValue(),
			// NAMA : Ext.get('txtNama_viDaftarRWIPs_viDaftarRWI').getValue(),
			// TEMPAT_LAHIR : Ext.get('txtTmpLahir_viDaftarRWI').getValue(),
			// TGL_LAHIR : ShowDate(Ext.get('DtpTglLahir_viDaftarRWI').getValue()),
			// JENIS_KELAMIN: Ext.getCmp('ComboJK_viDaftarRWI').getValue(),
			// ALAMAT : Ext.get('txtAlamat_viDaftarRWI_viDaftarRWI').getValue(),
			// NO_TELP : Ext.get('txtNoTlp_viDaftarRWI').getValue(),
			// NO_HP : Ext.get('txtHP_viDaftarRWI').getValue(),
			// GOL_DARAH : Ext.getCmp('ComboGolDRH_viDaftarRWI').getValue(),
		
		}
	
    return paramsDelete_ViPendaftaranIGD
}
//============================================ Grid Data ======================================

//-------------------------------------------- Hapus baris -------------------------------------
function HapusBarisNgajar(nBaris)
{
	if (CurrentData_viDaftarRWI.row >= 0
	/*SELECTDATASTUDILANJUT.data.NO_SURAT_STLNJ != '' ||  SELECTDATASTUDILANJUT.data.TGL_MULAI_SURAT != '' ||
		SELECTDATASTUDILANJUT.data.TGL_AKHIR_SURAT != '' || SELECTDATASTUDILANJUT.data.ID_DANA != '' ||
		SELECTDATASTUDILANJUT.data.SURAT_DARI != '' || SELECTDATASTUDILANJUT.data.TGL_SURAT != '' || SELECTDATASTUDILANJUT.data.THN_PERKIRAAN_LULUS != ''*/) 
		{
			Ext.Msg.show
			(
				{
					title: 'Hapus Baris',
					msg: 'Apakah baris ini akan dihapus ?',
					buttons: Ext.MessageBox.YESNO,
					fn: function(btn) 
					{
						if (btn == 'yes') 
						{
							DataDeletebaris_viDaftarRWI()
							dsDetailSL_viDaftarRWI.removeAt(CurrentData_viDaftarRWI.row);
							SELECTDATASTUDILANJUT = undefined;
						}
					},
					icon: Ext.MessageBox.QUESTION
				}
			);
		}
	else 
		{
			dsDetailSL_viDaftarRWI.removeAt(CurrentData_viDaftarRWI.row);
			SELECTDATASTUDILANJUT = undefined;
		}
}

function DataDeletebaris_viDaftarRWI() 
{
    Ext.Ajax.request
	({url: "./Datapool.mvc/DeleteDataObj",
		params: dataparam_viDaftarRWI(),
		success: function(o) 
		{
			var cst = o.responseText;
			if (cst == '{"success":true}') 
			{
				ShowPesanInfo_viDaftarRWI('Data berhasil dihapus','Hapus Data');                
			}
			else
			{ 
				ShowPesanInfo_viDaftarRWI('Data gagal dihapus','Hapus Data'); 
			}
		}
	})       
};

var mRecord = Ext.data.Record.create
(
	[
		'NIP', 
		'ID_STUDI_LANJUT', 
		'NO_SURAT_STLNJ', 
		'TGL_MULAI_SURAT', 
		'TGL_AKHIR_SURAT', 
		'ID_DANA', 
		'SURAT_DARI', 
		'TGL_SURAT', 
		'THN_PERKIRAAN_LULUS'  
	]
);
//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------
function getArrDetail_viDaftarRWI() 
{
    var x = '';
    for (var i = 0; i < dsDetailSL_viDaftarRWI.getCount(); i++) 
	{
        var y = '';
        var z = '@@##$$@@';
        
        y += 'NIP=' + Ext.get('txtNPP_viDaftarRWI').getValue()
        y += z + 'ID_STUDI_LANJUT=' + Ext.get('txtID_viDaftarRWI').getValue()
        y += z + 'NO_SURAT_STLNJ=' + dsDetailSL_viDaftarRWI.data.items[i].data.NO_SURAT_STLNJ
		
		/*if (dsDetailSL_viDaftarRWI.data.items[i].data.TGL_MULAI_SURAT.length == 8) 
		{	
			
			y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftarRWI.data.items[i].data.TGL_MULAI_SURAT)
		}
		else
		{
		
			y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftarRWI.data.items[i].data.TGL_MULAI_SURAT)
		}
		
		if (dsDetailSL_viDaftarRWI.data.items[i].data.TGL_AKHIR_SURAT.length == 8) 
		{		
			y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftarRWI.data.items[i].data.TGL_AKHIR_SURAT)
		}
		else
		{
			y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftarRWI.data.items[i].data.TGL_AKHIR_SURAT)
		}*/
		y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftarRWI.data.items[i].data.TGL_MULAI_SURAT)
		y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftarRWI.data.items[i].data.TGL_AKHIR_SURAT)
        y += z + 'ID_DANA=' + dsDetailSL_viDaftarRWI.data.items[i].data.ID_DANA
        y += z + 'SURAT_DARI=' + dsDetailSL_viDaftarRWI.data.items[i].data.SURAT_DARI
        //alert(dsDetailSL_viDaftarRWI.data.items[i].data.ID_DANA)
		
		/*if (dsDetailSL_viDaftarRWI.data.items[i].data.TGL_SURAT.length == 8) 
		{		
			y += z + 'TGL_SURAT=' + dsDetailSL_viDaftarRWI.data.items[i].data.TGL_SURAT
		}
		else
		{
			y += z + 'TGL_SURAT=' + ShowDate(dsDetailSL_viDaftarRWI.data.items[i].data.TGL_SURAT)
		}*/
		
		y += z + 'TGL_SURAT=' + ShowDate(dsDetailSL_viDaftarRWI.data.items[i].data.TGL_SURAT)
		
		y += z + 'THN_PERKIRAAN_LULUS=' + dsDetailSL_viDaftarRWI.data.items[i].data.THN_PERKIRAAN_LULUS
		
        if (i === (dsDetailSL_viDaftarRWI.getCount() - 1)) 
		{
            x += y
        }
        else {
            x += y + '##[[]]##'
        }
    }
    return x;
};
//---------------------------- end Split row ------------------------------
function DatarefreshDetailSL_viDaftarRWI(rowdataaparam)
{
    dsDetailSL_viDaftarRWI.load
    (
		{
			params:
			{
				Skip: 0,
				Take: 50,
				Sort: '',
				Sortdir: 'ASC',
				target:'viviewDetailStudiLanjut',
				param: rowdataaparam
			}
		}
    );
    return dsDetailSL_viDaftarRWI;
}

function GetPasienDaftarIGD(strCari)
{
	Ext.Ajax.request
	 (
		{
            url: "./Module.mvc/ExecProc",
            params: 
			{
                UserID: 'Admin',
                ModuleID: 'getKunjunganDataPasien',
				Params:	getParamKunjunganKdPasien(strCari)
            },
            success: function(o) 
			{
				
                var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{					
//					Ext.getCmp('txtKDPasien_viDaftarRWI').setValue(cst.KD_PASIEN);
//					Ext.getCmp('txtNama_viDaftarRWIPs_viDaftarRWI').setValue(cst.NAMA);
//					Ext.get('txtchkPasienBaru_viDaftarRWI').dom.checked = 0;
//					Ext.getCmp('txtTmpLahir_viDaftarRWI').setValue(cst.TEMPAT_LAHIR);
//					Ext.getCmp('DtpTglLahir_viDaftarRWI').setValue(ShowDate(cst.TGL_LAHIR));
//					Ext.getCmp('txtUmurThn_viDaftarRWI').setValue(cst.TAHUN);
//					Ext.getCmp('txtUmurBln_viDaftarRWI').setValue(cst.BULAN);
//					Ext.getCmp('txtUmurHari_viDaftarRWI').setValue(cst.HARI);
//					Ext.getCmp('ComboJK_viDaftarRWI').setValue(cst.JENIS_KELAMIN);
//					Ext.getCmp('ComboGolDRH_viDaftarRWI').setValue(cst.GOL_DARAH);
//					Ext.getCmp('txtNoTlp_viDaftarRWI').setValue(cst.NO_TELP);
//					Ext.getCmp('txtHP_viDaftarRWI').setValue(cst.NO_HP);
//					Ext.getCmp('txtAlamat_viDaftarRWI_viDaftarRWI').setValue(cst.ALAMAT);
//					Ext.getCmp('ComboPoliDaftarIGD_viDaftarRWI').setValue(cst.KD_UNIT);
//					Ext.getCmp('ComboDokterDaftarIGD_viDaftarRWI').setValue(cst.KD_DOKTER);
//					Ext.getCmp('ComboKelompokDaftarIGD_viDaftarRWI').setValue(cst.KD_KELOMPOK);
//					// Ext.getCmp('dtpKunjungan_viDaftarRWI').setValue(ShowDate(cst.TGL_KUNJUNGAN));
//					Ext.getCmp('txtNadi_viDaftarRWI').setValue(cst.NADI);
//					Ext.getCmp('txtchkAlergi_viDaftarRWI').setValue(cst.ALERGI);
//					Ext.getCmp('txtTinggiBadan_viDaftarRWI').setValue(cst.TINGGI_BADAN);
//					Ext.getCmp('txtBeratBadan_viDaftarRWI').setValue(cst.BERAT_BADAN);
//					Ext.getCmp('txtTekananDarah_viDaftarRWI').setValue(cst.TEKANAN_DARAH);
//					Ext.getCmp('txtkeluhan_viDaftarRWI').setValue(cst.KELUHAN);
//					Ext.getCmp('ComboSTS_MARITAL_viDaftarRWI').setValue(cst.KD_STS_MARITAL);
//					Ext.getCmp('ComboPekerjaan_viDaftarRWI').setValue(cst.KD_PEKERJAAN);
//					Ext.getCmp('ComboAgama_viDaftarRWI').setValue(cst.KD_AGAMA);
//					Ext.getCmp('ComboPendidikan_viDaftarRWI').setValue(cst.KD_PENDIDIKAN);
					
					// Ext.getCmp('ComboPoliDaftarIGD_viDaftarRWI').setValue(rowdata.KD_UNIT);
					// Ext.get('ComboJK_viDaftarRWI').dom.value = rowdata.JNS_KELAMIN;	
					// Ext.get('ComboPoliDaftarIGD_viDaftarRWI').dom.value = rowdata.NAMA_UNIT;
					// Ext.get('ComboDokterDaftarIGD_viDaftarRWI').dom.value = rowdata.DOKTER;
					// Ext.get('ComboKelompokDaftarIGD_viDaftarRWI').dom.value = rowdata.KELOMPOK;
					// Ext.get('ComboSTS_MARITAL_viDaftarRWI').dom.value = rowdata.STS_MARITAL;
					// Ext.get('ComboPekerjaan_viDaftarRWI').dom.value = rowdata.PEKERJAAN;
					// Ext.get('ComboAgama_viDaftarRWI').dom.value = rowdata.AGAMA;
					// Ext.get('ComboPendidikan_viDaftarRWI').dom.value = rowdata.PENDIDIKAN;
				}
				else
				{
					ShowPesanWarning_viDaftarRWI('No.Medrec tidak di temukan '  + cst.pesan,'Informasi');
					// Ext.get('txtNama_viDaftarRWIKRSMahasiswa').dom.value = '';								
					// Ext.get('txtFakJurKRSMahasiswa').dom.value = '';					
					// Ext.get('txtKdJurKRSMahasiswa').dom.value = '';					
					// Ext.get('txtBatasStudyKRSMahasiswa').dom.value = '';					
					// Ext.get('txtBarcode').dom.focus();

					// var criteria = ' WHERE NIM IS NOT NULL AND J.KD_JURUSAN IN ' + strKD_JURUSAN + ' ';
						// if (Ext.get('txtNIMKRSMahasiswa').dom.value != '')
						// {
							// criteria += ' AND NIM like ~%' + Ext.get('txtNIMKRSMahasiswa').dom.value + '%~';
							
						// };
						// FormLookupMahasiswa('txtNIMKRSMahasiswa','txtNama_viDaftarRWIKRSMahasiswa','txtFakJurKRSMahasiswa','txtBatasStudyKRSMahasiswa','txtIPSKRSMahasiswa','','','','txtKdJurKRSMahasiswa',criteria);
				};
            }

        }
	);
}

function getParamKunjunganKdPasien(kdCari)
{					
	var strKriteria = "";	
	var x ="x";
		
	
	if (kdCari == 1)
	{	
		if(Ext.get('txtKDPasien_viDaftarRWI').getValue() != '')
		{
			strKriteria += Ext.get('txtKDPasien_viDaftarRWI').getValue() + "##";							
		}	
	}
	else if(kdCari == 2) 
	{
		if(Ext.get('txtNoTlp_viDaftarRWI').getValue() != '')
		{
			strKriteria += Ext.get('txtNoTlp_viDaftarRWI').getValue() + "##";							
		}	
	}else
	{
		if(Ext.get('txtHP_viDaftarRWI').getValue() != '')
		{
			strKriteria += Ext.get('txtHP_viDaftarRWI').getValue() + "##";							
		}	
	};
	strKriteria += strKD_CUST + "##"
	strKriteria += kdCari + "##"
	
	return strKriteria;
}

function getKriteriaCariLookup()
{					
	var strKriteria = "";	
		
	//strKriteria=" WHERE RIGHT(PS.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftarRWI').dom.value + "%' and ps.kd_customer='" + strKD_CUST +"' "	
	
	if(Ext.get('txtKDPasien_viDaftarRWI').getValue() != '')
	{
		strKriteria += " WHERE RIGHT(x.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftarRWI').dom.value + "%'"
	}	
	
	if(Ext.get('txtNama_viDaftarRWIPs_viDaftarRWI').getValue() != '')	
	{	
		if (strKriteria != "")	
		{
			strKriteria += " AND x.NAMA like '%" + Ext.get('txtNama_viDaftarRWIPs_viDaftarRWI').dom.value + "%'"
		}
		else
		{
			strKriteria += " WHERE x.NAMA like '%" + Ext.get('txtNama_viDaftarRWIPs_viDaftarRWI').dom.value + "%'"
		}
	}
	
	// if(Ext.get('txtNoTlp_viDaftarRWI').getValue() != '')	
	// {	
		// if (strKriteria != "")	
		// {
			// strKriteria += " OR PS.NO_TELP like '%" + Ext.get('txtNoTlp_viDaftarRWI').dom.value + "%'"
		// }
		// else
		// {
			// strKriteria += " WHERE PS.NO_TELP like '%" + Ext.get('txtNoTlp_viDaftarRWI').dom.value + "%'"
		// }
	// }
	
	// if(Ext.get('txtHP_viDaftarRWI').getValue() != '')	
	// {	
		// if (strKriteria != "")	
		// {
			// strKriteria += " OR PS.NO_HP like '%" + Ext.get('txtHP_viDaftarRWI').dom.value + "%'"
		// }
		// else
		// {
			// strKriteria += " WHERE PS.NO_HP like '%" + Ext.get('txtHP_viDaftarRWI').dom.value + "%'"
		// }
	// }
	
	if(Ext.get('txtAlamat_viDaftarRWI_viDaftarRWI').getValue() != '')	
	{	
		if (strKriteria != "")	
		{
			strKriteria += " AND x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftarRWI_viDaftarRWI').dom.value + "%'"
		}
		else
		{
			strKriteria += " WHERE x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftarRWI_viDaftarRWI').dom.value + "%'"
		}
	}

	if (strKriteria != "")	
	{
		strKriteria += " AND x.KD_CUSTOMER = '" + strKD_CUST + "'"
	}
	else
	{
		strKriteria += " WHERE x.KD_CUSTOMER = '" + strKD_CUST + "'"
	}		
	
	return strKriteria;
}

function dataprint_viDaftarRWI()
{
    var params_ViPendaftaranIGD =
    {

        Table: 'ViewPrintBill',

        No_TRans: tmpnotransaksi,
        KdKasir : tmpkdkasir
//        NoMedrec:  Ext.get('txtNoRequest_viDaftarRWI').getValue(),
//        NamaPasien: Ext.get('txtNama_viDaftarRWI').getValue(),
//        TglLahir : Ext.get('dtpTanggalLahir_viDaftarRWI').getValue(),
//        Alamat : Ext.get('txtAlamat_viDaftarRWI').getValue(),
//
//        Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
//        TanggalMasuk : Ext.get('dptTanggal').getValue(),
//        KdDokter: Ext.getCmp('cboDokterRequestEntry').getValue(),
//        KdCustomer: Ext.getCmp('cboAsuransi').getValue()

    };
    return params_ViPendaftaranIGD
}

function GetPrintKartu()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/LaporanDataObj",
            params: dataprint_viDaftarRWI(),
            success: function(o) 
                        {

                var cst = Ext.decode(o.responseText);

                                if (cst.success === true) 
                                {
                                }
                                else
                                {				

                                };
                }

        }
    );
}

function mComboJK()
{
    var cboJK = new Ext.form.ComboBox
	(
		{
			id:'cboJK',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Jenis Kelamin...',
			fieldLabel: 'Jenis Kelamin ',
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetJK,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJK=b.data.displayText ;
//                                        getdatajeniskelamin(b.data.displayText)
                                        //alert(jenis_kelamin)
				},
                                'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('txtTempatLahir_viDaftarRWI').focus();
                                                    }, c);
                                                }
			}
		}
	);
	return cboJK;
};

function mComboWargaNegara()
{
    var cboWarga = new Ext.form.ComboBox
	(
		{
			id:'cboWarga',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih...',
                        selectOnFocus:true,
                        forceSelection: true,
			fieldLabel: 'Warga Negara ',
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'WNI'], [2, 'WNA']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetWarga,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetWarga=b.data.displayText ;
                                        GetDataWargaNegara(b.data.displayText)
                                        //alert(StatusWargaNegara)
				},
                                'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboStatusMarital').focus();
                                    }, c);
                                }
			}
		}
	);
	return cboWarga;
};

var StatusWargaNegara
function GetDataWargaNegara(warga)
{
    var tampung = warga
    if (tampung === 'WNI')
    {
        StatusWargaNegara = 'true'
    }
    else
        {
            StatusWargaNegara = 'false'
        }
}

function mComboGolDarah()
{
    var cboGolDarah = new Ext.form.ComboBox
	(
		{
			id:'cboGolDarah',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        //allowBlank: false,
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Gol. Darah...',
			fieldLabel: 'Gol. Darah ',
			width:50,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, '-'], [2, 'A+'],[3, 'B+'], [4, 'AB+'],[5, 'O+'], [6, 'A-'], [7, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetGolDarah,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGolDarah=b.data.displayText ;
				},
                                'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboWarga').focus();
                                    }, c);
                                }
			}
		}
	);
	return cboGolDarah;
};

function mComboPerseorangan()
{
    var cboPerseorangan = new Ext.form.ComboBox
	(
		{
			id:'cboPerseorangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Jenis',
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Umum']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetPerseorangan,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetPerseorangan=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseorangan;
};

function mComboSatusMarital()
{
    var cboStatusMarital = new Ext.form.ComboBox
	(
		{
			id:'cboStatusMarital',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Status...',
			fieldLabel: 'Status Marital ',
			width:110,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Blm Kawin'], [2, 'Kawin'],[3, 'Janda'], [4, 'Duda']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetSatusMarital,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetSatusMarital=b.data.displayText ;
				},
                                'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('txtAlamat_viDaftarRWI').focus();
                                    }, c);
                                }
			}
		}
	);
	return cboStatusMarital;
};

function mComboNamaRujukan()
{
    var cboNamaRujukan = new Ext.form.ComboBox
	(
		{
			id:'cboNamaRujukan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Rujukan...',
			fieldLabel: 'Nama ',
			width:110,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
                                       data: [[1, ''], [2, ''],[3, ''],[4, '']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetNamaRujukan,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetNamaRujukan=b.data.displayText ;
				}
			}
		}
	);
	return cboNamaRujukan;
};

function mComboPoli_viDaftarRWI(lebar,Nama_ID)
{
    var Field_poli_viDaftarRWI = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftarRWI = new WebApp.DataStore({fields: Field_poli_viDaftarRWI});

	ds_Poli_viDaftarRWI.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )

    var cbo_Poli_viDaftarRWI = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Poli',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store: ds_Poli_viDaftarRWI,
            width: lebar,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poli...',
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            name: Nama_ID,
            lazyRender: true,
            id: 'cboPoliviDaftarIGD',
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )

    return cbo_Poli_viDaftarRWI;
}

function mComboAgamaRequestEntry()
{
    var Field = ['KD_AGAMA','AGAMA'];

    dsAgamaRequestEntry = new WebApp.DataStore({fields: Field});
    dsAgamaRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_agama',
			    Sortdir: 'ASC',
			    target: 'ViewComboAgama',
			    param: ''
			}
		}
	)

    var cboAgamaRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboAgamaRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Agama...',
		    fieldLabel: 'Agama',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsAgamaRequestEntry,
		    valueField: 'KD_AGAMA',
		    displayField: 'AGAMA',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                    selectAgamaRequestEntry = b.data.KD_AGAMA;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboGolDarah').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboAgamaRequestEntry;
};

function mComboPropinsiRequestEntry()
{
     var Field = ['KD_PROPINSI','PROPINSI'];

    dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
    dsPropinsiRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboPropinsi',
			    param: ''
			}
		}
	)

    var cboPropinsiRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPropinsiRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    forceSelection: true,
                    emptyText:'Select a Propinsi...',
                    selectOnFocus:true,
                     //editable: false,
		    //emptyText: ' ',
		    fieldLabel: 'Propinsi',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPropinsiRequestEntry,
		    valueField: 'KD_PROPINSI',
		    displayField: 'PROPINSI',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   selectPropinsiRequestEntry = b.data.KD_PROPINSI;

                                   loaddatastorekabupaten(b.data.KD_PROPINSI);
                                },
                            'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKabupatenRequestEntry').focus();
                                                }, c);
                                            }

			}
                }
	);
        return cboPropinsiRequestEntry;
        
    
};

function loaddatastorekabupaten(kd_propinsi)
{
          dsKabupatenRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboKabupaten',
			    param: 'kd_propinsi='+ kd_propinsi
			}
                    }
                )
}

function mComboKabupatenRequestEntry()
{
      var Field = ['KD_KABUPATEN','KD_PROPINSI','KABUPATEN'];
      dsKabupatenRequestEntry = new WebApp.DataStore({fields: Field});

      var cboKabupatenRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKabupatenRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Select a Kabupaten...',
		    fieldLabel: 'Kab/Kod',
		    align: 'Right',
                    
		    store: dsKabupatenRequestEntry,
		    valueField: 'KD_KABUPATEN',
		    displayField: 'KABUPATEN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   selectKabupatenRequestEntry = b.data.KD_KABUPATEN;

                                    loaddatastorekecamatan(b.data.KD_KABUPATEN);
                                },
                                'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKecamatanRequestEntry').focus();
                                                }, c);
                                            }
                                
			}
                }
	);
    return cboKabupatenRequestEntry;
};

function loaddatastorekecamatan(kd_kabupaten)
{
    dsKecamatanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kabupaten',
			    Sortdir: 'ASC',
			    target: 'ViewComboKecamatan',
			    param: 'kd_kabupaten='+ kd_kabupaten
			}
		}
	)
}

function mComboKecamatanRequestEntry()
{
    var Field = ['KD_KECAMATAN','KD_KABUPATEN','KECAMATAN'];
    dsKecamatanRequestEntry = new WebApp.DataStore({fields: Field});
 
    var cboKecamatanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKecamatanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Select a Kecamatan...',
                    fieldLabel: 'Kecamatan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsKecamatanRequestEntry,
		    valueField: 'KD_KECAMATAN',
		    displayField: 'KECAMATAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
                                },
                                'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboPendidikanRequestEntry').focus();
                                                }, c);
                                            }
			}
                }
	);

    return cboKecamatanRequestEntry;
};

function mComboPendidikanRequestEntry()
{
    var Field = ['KD_PENDIDIKAN','PENDIDIKAN'];

    dsPendidikanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPendidikanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_pendidikan',
			    Sortdir: 'ASC',
			    target: 'ViewComboPendidikan',
			    param: ''
			}
		}
	)

    var cboPendidikanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPendidikanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Pendidikan...',
		    fieldLabel: 'Pendidikan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPendidikanRequestEntry,
		    valueField: 'KD_PENDIDIKAN',
		    displayField: 'PENDIDIKAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectPendidikanRequestEntry = b.data.KD_PROPINSI;
                                },
                                'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboPekerjaanRequestEntry').focus();
                                                }, c);
                                            }
			}
                }
	);

    return cboPendidikanRequestEntry;
};

function mComboAsuransi()
{
var Field_poli_viDaftarRWI = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftarRWI = new WebApp.DataStore({fields: Field_poli_viDaftarRWI});

	ds_customer_viDaftarRWI.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomer',
                param: "status=true"
            }
        }
    )
    var cboAsuransi = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransi',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Asuransi...',
                        fieldLabel: 'Asuransi',
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftarRWI,
			valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.displayText ;
				}
			}
		}
	);
	return cboAsuransi;
};

function mComboPekerjaanRequestEntry()
{
    var Field = ['KD_PEKERJAAN','PEKERJAAN'];

    dsPekerjaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPekerjaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_pekerjaan',
			    Sortdir: 'ASC',
			    target: 'ViewComboPekerjaan',
			    param: ''
			}
		}
	)

    var cboPekerjaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPekerjaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Pekerjaan...',
		    fieldLabel: 'Pekerjaan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPekerjaanRequestEntry,
		    valueField: 'KD_PEKERJAAN',
		    displayField: 'PEKERJAAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectPekerjaanRequestEntry = b.data.KD_PROPINSI;
                                },
                                'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboSukuRequestEntry').focus();
                                                }, c);
                                            }
			}
                }
	);

    return cboPekerjaanRequestEntry;
};

function mComboSukuRequestEntry()
{
    var Field = ['KD_SUKU','SUKU'];

    dsSukuRequestEntry = new WebApp.DataStore({fields: Field});
    dsSukuRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_suku',
			    Sortdir: 'ASC',
			    target: 'ViewComboSuku',
			    param: ''
			}
		}
	)

    var cboSukuRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboSukuRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: ' ',
                     editable: false,
		    fieldLabel: 'Suku ',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsSukuRequestEntry,
		    valueField: 'KD_SUKU',
		    displayField: 'SUKU',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectSukuRequestEntry = b.data.KD_PROPINSI;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13 || e.getKey() == 9 ) //atau Ext.EventObject.ENTER
                                     Ext.getCmp('setunit').collapsed = true;
                                    }, c);
                                }
			}
                }
	);

    return cboSukuRequestEntry;
};

function mComboPoliklinik()
{
    var Field = ['KD_SPESIAL','SPESIALISASI'];
    ds_Poli_viDaftarRWI = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftarRWI.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'kd_spesial',
                Sortdir: 'ASC',
                target:'ViewSetupSpesial',
                param: "kd_spesial <> 0"
            }
        }
    )

    var cboPoliklinikRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboPoliklinikRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Spesialisasi...',
            fieldLabel: 'Spesialisasi ',
            align: 'Right',
            store: ds_Poli_viDaftarRWI,
            valueField: 'KD_SPESIAL',
            displayField: 'SPESIALISASI',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   loaddatastorekelas(b.data.KD_SPESIAL)
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }
        	}
        }
    )

    return cboPoliklinikRequestEntry;
};

function loaddatastorekelas(KD_SPESIAL)
{
          dsKelasRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
                            Sort: 'kd_kelas',
			    Sortdir: 'ASC',
			    target: 'ViewComboKelasSpesial',
			    param: 'where kd_spesial =~'+ KD_SPESIAL+ '~'
			}
                    }
                )
};

function mComboKelasSpesialRequestEntry()
{
    var Field = ['KD_KELAS','KELAS'];

    dsKelasRequestEntry = new WebApp.DataStore({fields: Field});


    var cboKelasRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKelasRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Kelas...',
		    fieldLabel: 'Kelas ',
		    align: 'Right',
                     
//		    anchor:'60%',
		    store: dsKelasRequestEntry,
		    valueField: 'KD_KELAS',
		    displayField: 'KELAS',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   loaddatastoreruangan(b.data.KD_KELAS)
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboKelasRequestEntry;
};

function loaddatastoreruangan(KD_KELAS)
{
    var kd_spesial = Ext.getCmp('cboPoliklinikRequestEntry').getValue();
    dsRuanganRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
                            Sort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ViewComboRuangan',
			    param: 'where kd_spesial =~'+ kd_spesial + '~'+' And '+'k.kd_kelas =~'+ KD_KELAS + '~'
			}
                    }
                )
    
}

function mComboRuanganRequestEntry()
{
    var Field = ['KD_UNIT','NAMA'];

    dsRuanganRequestEntry = new WebApp.DataStore({fields: Field});


    var cboRuanganRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboRuanganRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Ruangan...',
		    fieldLabel: 'Ruang / Unit ',
		    align: 'Right',
		    store: dsRuanganRequestEntry,
		    valueField: 'KD_UNIT',
		    displayField: 'NAMA',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   loaddatastorekamar(b.data.KD_UNIT)
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboRuanganRequestEntry;
};

function loaddatastorekamar(KD_UNIT)
{
    dsKamarRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
                            Sort: 'no_kamar',
			    Sortdir: 'ASC',
			    target: 'ViewComboKamar',
			    param: 'kd_unit =~'+ KD_UNIT + '~'
			}
                    }
                )
}
var tmpkdunitkamar;

function mComboKamarRequestEntry()
{
    var Field = ['KD_UNIT','NO_KAMAR','NAMA_KAMAR'];

    dsKamarRequestEntry = new WebApp.DataStore({fields: Field});


    var cboKamarRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKamarRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Kamar...',
		    fieldLabel: 'Kamar ',
		    align: 'Right',
		    store: dsKamarRequestEntry,
		    valueField: 'NO_KAMAR',
		    displayField: 'NAMA_KAMAR',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                    viewsisakamar();
                                    tmpkdunitkamar = b.data.KD_UNIT;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboKamarRequestEntry;
};

function viewsisakamar()
{
    Ext.Ajax.request
            (
                {
                        url: baseURL + "index.php/main/ReadData",
                        params: datavaramsisakamar(),
                        success: function(o)
                        {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
//                                        ShowPesanInfo_viDaftarRWI('Data berhasil di simpan','Simpan Data');
                                        Ext.get('txtSisaKamar_viDaftarRWI').dom.value = cst.totalrecords;
                                       
                                }else{
                                    Ext.get('txtSisaKamar_viDaftarRWI').dom.value = 0;
                                }
                                 if(cst.totalrecords <= 0)
                                        {
                                          ShowPesanInfo_viDaftarRWI('Tidak ada kamar kosong, silahkan pilih kamar lain','Simpan Data');
                                        }
                                }
                }
            )
}

function datavaramsisakamar()
{
    var params_ViPendaftaranIGD =
		{
                    Table: 'View_SisaKamar',
                    Spesialisasi: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
                    Kelas: Ext.getCmp('cboKelasRequestEntry').getValue(),
                    Ruangan: Ext.getCmp('cboRuanganRequestEntry').getValue(),
                    Kamar: Ext.getCmp('cboKamarRequestEntry').getValue(),
		};
    return params_ViPendaftaranIGD
    
}

function mcomborujukandari()
{
    var Field = ['CARA_PENERIMAAN','PENERIMAAN'];
    ds_Poli_viDaftarRWI = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftarRWI.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewComboRujukanDari',
                param: ""
            }
        }
    )

    var cboRujukanDariRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanDariRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Rujukan...',
            fieldLabel: 'Rujukan Dari ',
            align: 'Right',
            store: ds_Poli_viDaftarRWI,
            valueField: 'CARA_PENERIMAAN',
            displayField: 'PENERIMAAN',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   loaddatastorerujukan(b.data.CARA_PENERIMAAN)
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }


		}
        }
    )

    return cboRujukanDariRequestEntry;
}

function loaddatastorerujukan(cara_penerimaan)
{
          dsRujukanRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    Sort: 'rujukan',
			    Sortdir: 'ASC',
			    target: 'ViewComboRujukan',
			    param: 'cara_penerimaan=~'+ cara_penerimaan+ '~'
			}
                    }
                )
}

function mComboRujukan()
{
    var Field = ['KD_RUJUKAN','RUJUKAN'];

    dsRujukanRequestEntry = new WebApp.DataStore({fields: Field});


    var cboRujukanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboRujukanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Rujukan...',
		    fieldLabel: 'Rujukan ',
		    align: 'Right',
                     
//		    anchor:'60%',
		    store: dsRujukanRequestEntry,
		    valueField: 'KD_RUJUKAN',
		    displayField: 'RUJUKAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboRujukanRequestEntry;
};

function mComboPerusahaan()
{
    var Field = ['KD_PERUSAHAAN','PERUSAHAAN'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboPerusahaan',
			    param: ''
			}
		}
	)
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_PERUSAHAAN',
		    displayField: 'PERUSAHAAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//			        var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                }
			}
                }
	);

    return cboPerusahaanRequestEntry;
};

var rowSelectedLookPasien;
var rowSelectedLookSL;
var mWindowLookup;
var nFormPendaftaranIGD=1;
var dsLookupPasienList;

var criteria ='';

function FormLookupPasien(criteria,nFormAsal,nName_ID,strRM,strNama,strAlamat)
{
    var vWinFormEntry = new Ext.Window
	(
		{
		    id: 'FormPasienLookup',
		    title: 'Lookup Pasien',
		    closable: true,
		    width: 600,//450,//
		    height: 400,
		    border: true,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: 'find',
		    modal: true,
		    items:
			[
			    // fnGetDTLGridLookUp(criteria, nFormAsal),
				fnGetDTLGridLookUpPas(criteria, nFormAsal,nName_ID),
				{
				    xtype: 'button',
				    text: 'Ok',
				    width: 70,
				    style: {'margin-left': '510px', 'margin-top': '7px'},
				    hideLabel: true,
				    id: 'btnOkpgw',
				    handler: function()
					{
						GetPasien(nFormAsal,nName_ID);
				    }
				}
			],
			tbar:
			{
				xtype: 'toolbar',
				items:
				[
					{
						xtype: 'tbtext',
						text: 'No. RM : '
					},
					{
						xtype: 'textfield',
						id: 'txtNoRMLookupPasien',
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);
								}
							}
						}

					},
					{
						xtype: 'tbtext',
						text: 'Nama : '
					},
					{
						xtype: 'textfield',
						id: 'txtNama_viDaftarRWIMLookupPasien',
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);
								}
							}
						}
					},
					{
					xtype: 'tbtext',
					text: 'Alamat : '
					},
					{
						xtype: 'textfield',
						id: 'txtAlamat_viDaftarRWIMLookupPasien',
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);
								}
							}
						}
					},
					{
						xtype: 'tbfill'
					},
					{
						xtype: 'button',
						id: 'btnRefreshGLLookupCalonMHS',
						iconCls: 'refresh',
						handler: function()
						{
							// var criteria ='';
							criteria = getQueryCariPasien();
							RefreshDataLookupPasien(criteria);
						}
					}
				]
			}
			,
		    listeners:
				{
				    activate: function()
				    {
						Ext.get('txtNoRMLookupPasien').dom.value = strRM;
						Ext.get('txtNama_viDaftarRWIMLookupPasien').dom.value = strNama;
						Ext.get('txtAlamat_viDaftarRWIMLookupPasien').dom.value = strAlamat;

					}
				}
		}
	);
    vWinFormEntry.show();
	mWindowLookup = vWinFormEntry;
};

///---------------------------------------------------------------------------------------///


function fnGetDTLGridLookUpPas(criteria,nFormAsal,nName_ID)
{
    var fldDetail =
	['KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'AGAMA',
        'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN', 'PENDIDIKAN', 'PEKERJAAN',
        'NAMA_UNIT','TGL_MASUK', 'URUT_MASUK','KD_KELURAHAN','KABUPATEN','KECAMATAN','PROPINSI',
	];

	// var dsLookupPasienList = new WebApp.DataStore({ fields: fldDetail });
	dsLookupPasienList = new WebApp.DataStore({fields: fldDetail});

	RefreshDataLookupPasien(criteria);
	var vGridLookupPasienFormEntry = new Ext.grid.EditorGridPanel
	(
		{
			id:'vGridLookupPasienFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookupPasienList,
			height: 310, //330,
			columnLines:true,
			bodyStyle: 'padding:0px',
			enableKeyEvents: true,
			border: false,
			sm: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelectedLookPasien = dsLookupPasienList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
                                    var nFormAsal = 1;
					GetPasien(nFormAsal,nName_ID);
				},

				'specialkey': function()
				{
					if (Ext.EventObject.getKey() == 13)
					{
						GetPasien(nFormAsal,nName_ID);
					}
				}
			},
			cm: fnGridLookPasienColumnModel(),
			viewConfig: {forceFit: true}
		});

	return vGridLookupPasienFormEntry;
};

function fnGridLookPasienColumnModel()
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colLookupPasien',
				header: "No. RM",
				dataIndex: 'KD_PASIEN',
				width: 200
			},
			{
				id: 'colLookupNamaPasien',
				header: "Nama",
				dataIndex: 'NAMA',
				width: 300
			},
			{
				id: 'colLookupAlamatPasien',
				header: "Alamat",
				dataIndex: 'ALAMAT',
				width: 300
			}

		]
	)
};

function GetPasien(nFormAsal,nName_ID)
{
	if (rowSelectedLookPasien != undefined || nName_ID != undefined)
	{
		if (nFormAsal === nFormPendaftaranIGD)
		{

                        Ext.getCmp('txtNoRequest_viDaftarRWI').setValue(rowSelectedLookPasien.data.KD_PASIEN);
                        Ext.getCmp('txtNama_viDaftarRWI').setValue(rowSelectedLookPasien.data.NAMA);
                        Ext.getCmp('txtNama_viDaftarRWIKeluarga').setValue(rowSelectedLookPasien.data.NAMA_KELUARGA);
                        Ext.getCmp('txtTempatLahir_viDaftarRWI').setValue(rowSelectedLookPasien.data.TEMPAT_LAHIR);
                        Ext.getCmp('cboPendidikanRequestEntry').setValue(rowSelectedLookPasien.data.PENDIDIKAN);
                        Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowSelectedLookPasien.data.PEKERJAAN);
                        Ext.getCmp('cboWarga').setValue(rowSelectedLookPasien.data.WNI);
                        Ext.getCmp('txtAlamat_viDaftarRWI').setValue(rowSelectedLookPasien.data.ALAMAT);
                        Ext.getCmp('cboAgamaRequestEntry').setValue(rowSelectedLookPasien.data.AGAMA);
                        Ext.getCmp('cboGolDarah').setValue(rowSelectedLookPasien.data.GOL_DARAH);
                        Ext.get('dtpTanggalLahir_viDaftarRWI').dom.value = ShowDate(rowSelectedLookPasien.data.TGL_LAHIR);
                        Ext.getCmp('cboStatusMarital').setValue(rowSelectedLookPasien.data.STATUS_MARITA);
                        var tmpjk = "";
                        var tmpwni = "";
                        if (rowSelectedLookPasien.data.JENIS_KELAMIN === "t")
                            {
                                tmpjk = "Laki - Laki";
                            }
                            else
                                {
                                    tmpjk = "Perempuan";
                                }
                        Ext.getCmp('cboJK').setValue(tmpjk);
                        if (rowSelectedLookPasien.data.WNI === "t")
                            {
                                tmpwni = "WNI";
                            }
                            else
                                {
                                    tmpwni = "WNA";
                                }
                        Ext.getCmp('cboWarga').setValue(tmpwni);
                        //getdatatempat_viDaftarRWI(rowdata.KD_PASIEN);

                        Ext.getCmp('cboPropinsiRequestEntry').setValue(rowSelectedLookPasien.data.PROPINSI);
                        Ext.getCmp('cboKabupatenRequestEntry').setValue(rowSelectedLookPasien.data.KABUPATEN);
                        Ext.getCmp('cboKecamatanRequestEntry').setValue(rowSelectedLookPasien.data.KECAMATAN);

                        Ext.getCmp('btnSimpan_viDaftarRWI').disable();
                        Ext.getCmp('btnSimpanExit_viDaftarRWI').disable();
                        Ext.getCmp('btnDelete_viDaftarRWI').enable();
                        Ext.getCmp('btnUlang_viDaftarRWI').enable();
//                        mNoKunjungan_viKasir = rowdata.NO_KUNJUNGAN;
		}

	}
	rowSelectedLookPasien=undefined;
	mWindowLookup.close();
}

function getQueryCariPasien()
{
	var strKriteria = "";

	if(Ext.get('txtNoRMLookupPasien').getValue() != '')
	{
		strKriteria += " RIGHT(pasien.KD_PASIEN,10) like '%" + Ext.get('txtNoRMLookupPasien').dom.value + "%'"
	}

	if(Ext.get('txtNama_viDaftarRWIMLookupPasien').getValue() != '')
	{
		if (strKriteria != "")
		{
			strKriteria += " AND pasien.NAMA like '%" + Ext.get('txtNama_viDaftarRWIMLookupPasien').dom.value + "%'"
		}
		else
		{
			strKriteria += " pasien.NAMA like '%" + Ext.get('txtNama_viDaftarRWIMLookupPasien').dom.value + "%'"
		}
	}

	if(Ext.get('txtAlamat_viDaftarRWIMLookupPasien').getValue() != '')
	{
		if (strKriteria != "")
		{
			strKriteria += " AND pasien.ALAMAT like '%" + Ext.get('txtAlamat_viDaftarRWIMLookupPasien').dom.value + "%'"
		}
		else
		{
			strKriteria += " pasien.ALAMAT like '%" + Ext.get('txtAlamat_viDaftarRWIMLookupPasien').dom.value + "%'"
		}
	}

	return strKriteria;
}

function RefreshDataLookupPasien(criteria)
{
	dsLookupPasienList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 100,
					Sortdir: 'ASC',
					target: 'View_DaftarIGD',//'Viview_viDataPasien',
					param: criteria
				}
			}
		);

	return dsLookupPasienList;
}

function Combo_Select(combo)
{
   var value = combo;
   //var txtgetnamaPeserta = Ext.getCmp('txtnamaPeserta') ;

   if(value === "Perseorangan")
   {    
        Ext.getCmp('txtNama_viDaftarRWIPeserta').hide();
        Ext.getCmp('txtNoAskes').hide();
        Ext.getCmp('txtNoSJP').hide();
        Ext.getCmp('cboPerseorangan').show();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').hide();
        

   }
   else if(value === "Perusahaan")
   {    
        Ext.getCmp('txtNama_viDaftarRWIPeserta').hide();
        Ext.getCmp('txtNoAskes').hide();
        Ext.getCmp('txtNoSJP').hide();
        Ext.getCmp('cboPerseorangan').hide();
        Ext.getCmp('cboAsuransi').hide();
        Ext.getCmp('cboPerusahaanRequestEntry').show();
        
   }
   else
       {
         Ext.getCmp('txtNama_viDaftarRWIPeserta').show();
         Ext.getCmp('txtNoAskes').show();
         Ext.getCmp('txtNoSJP').show();
         Ext.getCmp('cboPerseorangan').hide();
         Ext.getCmp('cboAsuransi').show();
         Ext.getCmp('cboPerusahaanRequestEntry').hide();
         
       }
}

function printbill()
{
    Ext.Ajax.request
            (
				{
					url: baseURL + "index.php/main/CreateDataObj",
					params: dataparamreport_viDaftarRWI(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfo_viDaftarRWI('Data berhasil di simpan','Simpan Data');
							datarefresh_viDaftarRWI(tmpcriteriaRWI);
							// if(mBol === false)
							// {
								// Ext.get('txtID_viDaftarRWI').dom.value=cst.ID_SETUP;
							// };
							addNew_viDaftarRWI = false;
							//Ext.get('txtNoKunjungan_viDaftarRWI').dom.value = cst.NO_KUNJUNGAN
							Ext.get('txtNoRequest_viDaftarRWI').dom.value = cst.KD_PASIEN
							Ext.getCmp('btnSimpan_viDaftarRWI').disable();
							Ext.getCmp('btnSimpanExit_viDaftarRWI').disable();
//							Ext.getCmp('btnDelete_viDaftarRWI').enable();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning_viDaftarRWI('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
						else
						{
							ShowPesanError_viDaftarRWI('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
					}
				}
            )
}

function dataparamreport_viDaftarRWI()
{
    var paramsreport_ViPendaftaranIGD =
		{
      
                    Table: 'DirectPrinting',
                    No_TRans: tmpnotransaksi,
                    KdKasir : tmpkdkasir
//                    NoMedrec:  Ext.get('txtNoRequest_viDaftarRWI').getValue(),
//                    KelPasien: Ext.getCmp('kelPasien').getValue(),
//                    Dokter: Ext.getCmp('cboDokterRequestEntry').getValue(),
//                    NamaPasien: Ext.get('txtNama_viDaftarRWI').getValue(),
//                    Alamat : Ext.get('txtAlamat_viDaftarRWI').getValue(),
//                    Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue()
		};
    return paramsreport_ViPendaftaranIGD
}