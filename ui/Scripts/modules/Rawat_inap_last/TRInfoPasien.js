// Data Source ExtJS

/**
*	Nama File 		: TRGizi.js
*	Menu 			: Pendaftaran
*	Model id 		: 030103
*	Keterangan 		: Untuk View Informasi Tarif
*	Di buat tanggal : 15 April 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var isikriteria;
var nilai =0;
var dsGiziPasienList;
var rowSelected_viGiziIgd;
var selectCountGiziPasien=1;
var rowSelectedGiziPasien;
var kd_prod;
var dsDataUnsur_viGizi;
var dataSource_viGizi;
var selectCount_viGizi=50;
var NamaForm_viGizi="Informasi Tarif";
var icons_viGizi="Gaji";
var addNew_viGizi;
var rowSelected_viGizi;
var rowSelected_viGiziList;
var setLookUps_viGizi;
var setLookUps_viGiziList;
var now_viGizi = new Date();

var BlnIsDetail;
var SELECTDATAPENILAIANPGW; // cek lagi

var CurrentData_viGizi =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viGizi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// Start Shortcut Key
// Kegunaan : Tombol Cepat untuk Keyboard
	Ext.getDoc().on('keypress', function(event, target) {
    if (event.altKey && !event.shiftKey)
	{
        event.stopEvent();
    /*if (Ext.EventObject.getKey() === 18 && !Ext.EventObject.getKey() === 16) {
        Ext.EventObject.stopEvent();*/

        switch(event.getKey()) {
        //switch(Ext.EventObject.getKey()) {

            case event.F1 :
            	//	dataaddnew_viGizi();
            	break;

            case event.F2 :
            		datasave_viGizi(false);
					datarefresh_viGizi();
				break;

            case event.F3 : // Alt + F3 untuk Edit
	                if (rowSelected_viGizi === undefined)
	                {
	                    setLookUp_viGizi();
	                }
	                else
	                {
	                    setLookUp_viGizi(rowSelected_viGizi.data);
	                }
                break;

            case event.F5 :
            		var x = datasave_viGizi(true);
							datarefresh_viGizi();
							if (x===undefined)
							{
								setLookUps_viGizi.close();
							}
				break;

			case event.F10 :
					alert("Delete")
				break;

            // other cases...
        }
        
    }
});
// End Shortcut Key

// Start Project Informasi Tarif

/**
*	Function : dataGrid_viGizi
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

 function hideCols (grid) {
 
grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('waktu'), true);
grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('menu'), true);
};


 function showCols (grid) 
 {             grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('waktu'), false);
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('menu'), false);
 };
 
 
function dataGrid_viGizi(mod_id)

{



    // Field kiriman dari Project Net.
    var FieldMaster = ['KD_PRODUK', 'DESKRIPSI', 'TARIF','KD_UNIT', 'NAMA_UNIT','KD_TARIF','TGL_BERLAKU'];
	// Deklarasi penamaan yang akan digunakan pada Grid
    dataSource_viGizi = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
   // datarefresh_viGizi();
    // Grid Informasi Tarif
	
	
	
		var datastoreGrid = new Ext.data.Store({
		reader: new Ext.data.JsonReader
		({
			//fields: ['No',  'Nama_Pasien',  'Dokter',  {name: 'Spesialisasi',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			fields: ['No',  'Nama_Pasien',  'Dokter',  'Spesialisasi',  'Ruangan']
		}),
		data: 
		[
			{No: '0-00-00-11', Nama_Pasien: 'BAMBANG PAMUNGKAS', Dokter: 'dr. Iriani Siombo, Sp.A', Spesialisasi: 'Anak', Ruangan: 'VIP'},
			{No: '0-00-00-12', Nama_Pasien: 'HENDRO', Dokter: 'dr. Novita Tombokan', Spesialisasi: 'Anak', Ruangan: 'Melati'},
			{No: '0-00-00-13', Nama_Pasien: 'MELODY', Dokter: 'dr. Iriani Siombo, Sp.A', Spesialisasi: 'Gigi', Ruangan: 'Mawar'},
			{No: '0-00-00-14', Nama_Pasien: 'YUDA', Dokter: 'dr. Anisa Ratih Dewi', Spesialisasi: 'Umum', Ruangan: 'Melati'},
			{No: '0-00-00-15', Nama_Pasien: 'KAKA', Dokter: 'dr. Novita Tombokan', Spesialisasi: 'Anak', Ruangan: 'Mawar'}
			
			
			// {field1: 'C', field2: 'D'},
			// {field1: 'E', field2: 'E'}
		]
	});

    var grData_vimenu = new Ext.grid.EditorGridPanel
    (
    {
		id: 'grData_vimenu',
        xtype: 'editorgrid',
        title: 'Pasien',
        store: datastoreGrid,
        autoScroll: true,
        columnLines: true,
        border:false,
		height:450,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
        (
				
				),
				listeners:
				{
					// Function saat ada event double klik maka akan muncul form view
					rowclick: function (sm, ridx, cidx)
					{
					 rowSelected_viGiziIgd = datastoreGrid.getAt(ridx);
                               var tampung= rowSelected_viGiziIgd.data.Nama_Pasien;
							   //alert(tampung);
							   if(tampung==='HENDRA')
							   {
							  // alert('a');
							   isikriteria=1;
							   showCols(Ext.getCmp('grListSetMenu'));
							   }else{
							   hideCols(Ext.getCmp('grListSetMenu'));
							   }
                                       //setLookUp_viDaftar(roles);
					//handler: function() {
					
					
					//Ext.getCmp('grListSetMenu').columns[0].setVisible(false);
					//}
					}
           
				},
				/**
				*	Mengatur tampilan pada Grid Informasi Tarif
				*	Terdiri dari : Judul, Isi dan Event
				*	Isi pada Grid di dapat dari pemangilan dari Net.
				*	dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSource_viGizi
				*	didapat dari Function datarefresh_viGizi()
				*/
				colModel: new Ext.grid.ColumnModel
				(
					[
						{
				dataIndex: 'No',
				header: 'No. Med Rec',
				sortable: true,
				width: 100,
				filter :{}
			},
			//-------------- ## --------------
			{
				dataIndex: 'Nama_Pasien',
				header: 'Nama Pasien',
				sortable: true,
				width: 200,
				filter :{}
			},
			//-------------- ## --------------			
			{			
				dataIndex: 'Dokter',
				header: 'Dokter',
				sortable: true,
				width: 200,
				filter :{}
			},
			
			//-------------- ## --------------
			{
				dataIndex: 'Spesialisasi',
				header: 'Spesialisasi',
				sortable: true,
				width: 200,
				filter :{
				
				}
				//renderer: function(v, params, record) 
				// {
					
				// }			
			},
			//-------------- ## --------------
			{
				dataIndex: 'Ruangan',
				header: 'Ruangan',
				sortable: true,
				width: 100,
				filter :{}
				// renderer: function(v, params, record) 
				// {
					
				// }	
			}
						//-------------- ## --------------
					]
				)
			
			}
    )

    //var Field =['KD_COMPONENT','KD_TARIF','KD_PRODUK','COMPONENT','KD_UNIT','TGL_BERLAKU','TARIF'];
    // dsGiziPasienList = new WebApp.DataStore({ fields: Field });
     	
	
		var dsGiziPasienList = new Ext.data.Store({
			reader: new Ext.data.JsonReader
				({
					//fields: ['waktu',  'menu',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
					fields: ['Nama','waktu',  'menu']
				}),
			data: 
				[
					{Nama: 'Maman', waktu: 'Pagi', menu: 'SONDE DH III'},
					//{Nama: 'Maman', waktu: 'Pagi', menu: 'SONDE DM 1500'},
					{Nama: 'Maman', waktu: 'Siang', menu: 'BBK'},
					//{Nama: 'Maman', waktu: 'Siang', menu: 'DCA RG' },
					{Nama: 'Maman', waktu: 'Malam', menu: 'SONDE LLM'},
					//{Nama: 'Maman', waktu: 'Malam', menu: 'SONDE DJ I' }
					// {field1: 'C', field2: 'D'},
					// {field1: 'E', field2: 'E'}
				]
			});
	
		
     var grListSetMenu = new Ext.grid.EditorGridPanel
     (
        {
            id: 'grListSetMenu',
            stripeRows: true,
			    title: 'Menu',
            store: dsGiziPasienList,
            autoScroll: true,
            columnLines: true,
            border: false,
			height:450,
            anchor: '100% 100%',
            sm: new Ext.grid.RowSelectionModel
				(
					{
                    singleSelect: true,
                    listeners:
                        {
                           
                        }
					}
				),
            listeners:
			{
			rowdblclick: function (sm, ridx, cidx)
				{
					
				}
			},
			colModel: new Ext.grid.ColumnModel
				(
				[
                  
							
							{
							//'','','','','','TGL_BERLAKU',''
							header: 'Nama',
							id: 'colSetDaftarByr',
							dataIndex: 'Nama',
							width: 100,
							hidden:true,
							sortable: true,
							 renderer: function (value, metaData, record, rowIndex, colIndex, store) {

                                   // hideCols(Ext.getCmp('grListSetMenu'));
                                
                            }
							},
							
							
							{
							id: 'colSetDaftarByr',
							header: 'Waktu',
							dataIndex: 'waktu',
							width: 80,
							hidden:true,
							sortable: true
							},
							{
							id: 'colSetDaftarByr',
							header: 'Menu',
							//align: 'right',
							dataIndex: 'menu',
							hidden : true,
							width: 420,
							sortable: true,
							renderer: function (value, metaData, record, rowIndex, colIndex, store) {

                             if ( isikriteria==1)
							 {
                                    //value = 'SONDE DM 1500';
                                
							}
                                return value;
                            }
							}
		
				]
			//'','','','','','',''
         
			)
		}
	); 
	// Kriteria filter pada Grid
	
	var FrmTabs_viGizi = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
		    title:  'Pasien RWI',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: icons_viGizi,
		    items: 
			[
				grData_vimenu
						
					
			]
		
		
		
    }
    )
    // datarefresh_viGizi();
    return FrmTabs_viGizi;
}

/*
SELECT     KD_COMPONENT, KD_TARIF, KD_PRODUK, KD_UNIT, TGL_BERLAKU, TARIF, TARIF_PERCENT, KD_INDUK
FROM         TARIF_COMPONENT where KD_TARIF='TU'
*/

function RefreshDataSetTypeBayarFilter()
{
	var KataKunci='';

    if (Ext.get('txtSetDeskripsiFilter').getValue() != '')
    {
		if (KataKunci === '')
		{
			//KataKunci = ' where Customer like  ~%' + Ext.get('txtSetDeskripsiFilter').getValue() + '%~';
                        KataKunci = ' deskripsi like  ~%' + Ext.get('txtSetDeskripsiFilter').getValue() + '%~';
		}
		else
		{
			//KataKunci += ' and  deskripsi like  ~%' + Ext.get('txtSetDeskripsiFilter').getValue() + '%~';
                        KataKunci += ' and deskripsi like  ~%' + Ext.get('txtSetDeskripsiFilter').getValue() + '%~';
		};
	};

    if (KataKunci != undefined)
    {
		dsSetTypeBayarList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountGiziPasien,
					//Sort: 'Customer_ID',
                                        Sort: 'jenis_pay',
					Sortdir: 'ASC',
					target:'ViewSetupJenisBayar',
					param : KataKunci
				}
			}
		);
    }
	else
	{
		RefreshDataSetTypeBayar();
	};
};



function gridPembayaran()
{ 

	
	
};


///----------------------------------------------------------------------------------------------///
function datasave_viGizi(mBol)
{
   
}



function viCombostsPeg_viGizi(lebar,Nama_ID)
{
  var cbo_viCombostsPeg_viGizi = new Ext.form.ComboBox
	(
	
		{
			id:Nama_ID,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: "Kategori",			
			width:lebar,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[0, "Pegawai"], [1, "Dosen"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cbo_viCombostsPeg_viGizi;
};

function ShowPesanWarning_viGizi(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanError_viGizi(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

function ShowPesanInfo_viGizi(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}


function DatarefreshPegawai_viGizi()
{
    dsPenilaianPegawai_viGizi.load
    (
		{
			params:
			{
				Skip: 0,
				Take: 50,
				Sort: "",
				Sortdir: "ASC",
				target:"viviewloadpenilaian",
				param: Ext.getCmp('Cbounit_viGizi').getValue() + Ext.getCmp('Cbojnspegawai_viGizi').getValue()
			}
		}
    );
    return dsPenilaianPegawai_viGizi;
}

function DataRefreshFilter_viDtPNLPGWNew(criteria)
{
    
    dataSource_viGizi.load
    (
		{
			params:
			{
				Skip: 0,
				Take: selectCount_viGizi,
				Sort: '',
				Sortdir: 'ASC',
				target:'viGiziPasien',
				param: criteria
				
			}
		}
    );    
    return dataSource_viGizi;
}


function getCriteriaGListDataPNLPGW()
{
	var strKriteria = "";
	
	
	
	if (Ext.getCmp('cbourut_vidtPNLPGW').getValue() === '') 
	{
	strKriteria +=  " Order b.JNS_PENILAIAN ASC"
	}
	else{
	
		if (Ext.getCmp('cbourut_vidtPNLPGW').getValue() === 0)
		{
		
			strKriteria +=  " Order BY b.JNS_PENILAIAN ASC"
		}
		
		if (Ext.getCmp('cbourut_vidtPNLPGW').getValue() == 1)
		{
			strKriteria +=  " Order BY c.NAMA_UNIT_KERJA ASC"
		}
	
		if (Ext.getCmp('cbourut_vidtPNLPGW').getValue() == 2)
		{
			strKriteria +=  " Order BY a.TGL_PENILAIAN_AWAL ASC"
		}
		if (Ext.getCmp('cbourut_vidtPNLPGW').getValue() == 3)
		{
			strKriteria +=  " Order BY a.TGL_PENILAIAN_AKHIR DESC"
		}
	
	}
	return strKriteria;
}


function viCombourut_trPNLPGW(lebar,Nama_ID)
{
  var cbo_viCombourut_trPNLPGW = new Ext.form.ComboBox
	(
		{
			id:Nama_ID,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: "Kategori",			
			width:lebar,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[0, "JENIS PENILAIAN"], [1, "UNIT KERJA"],[2, "Periode AWAL"], [3, "Periode Akhir"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
			}
		}
	);
	return cbo_viCombourut_trPNLPGW;
};

function datainit_viGizi(rowdata)
{
    
}


function dataaddnew_viGizi()
{
   
}
///---------------------------------------------------------------------------------------///



/**
*	Function : gridDataForm_viGizi
*	
*	Sebuah fungsi untuk menampilkan isian grid pada edit Informasi Tarif
*	yang di pangil dari Function getFormItemEntry_viGizi
*/

//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------//---------------------------- end Split row ------------------------------



