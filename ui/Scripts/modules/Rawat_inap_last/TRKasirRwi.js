// Data Source ExtJS # --------------
// Deklarasi Variabel pada Kasir Rawat Jalan # --------------

var dataSource_vikasirRwi;
var selectCount_vikasirRwi=50;
var NamaForm_vikasirRwi="Kasir RWI ";
var mod_name_vikasirRwi="viKasirRwi";
var now_vikasirRwi= new Date();
var addNew_vikasirRwi;
var rowSelected_vikasirRwi;
var setLookUps_vikasirRwi;
var mNoKunjungan_vikasirRwi='';

var CurrentData_vikasirRwi =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_vikasirRwi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Rawat Jalan # --------------

// Start Project Kasir Rawat Jalan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_vikasirRwi
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_vikasirRwi(mod_id_vikasirRwi)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_vikasirRwi = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_vikasirRwi = new WebApp.DataStore
	({
        fields: FieldMaster_vikasirRwi
    });
    
    // Grid Kasir Rawat Jalan # --------------
	var GridDataView_vikasirRwi = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_vikasirRwi,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_vikasirRwi = undefined;
							rowSelected_vikasirRwi = dataSource_vikasirRwi.getAt(row);
							CurrentData_vikasirRwi
							CurrentData_vikasirRwi.row = row;
							CurrentData_vikasirRwi.data = rowSelected_vikasirRwi.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_vikasirRwi = dataSource_vikasirRwi.getAt(ridx);
					if (rowSelected_vikasirRwi != undefined)
					{
						setLookUp_vikasirRwi(rowSelected_vikasirRwi.data);
					}
					else
					{
						setLookUp_vikasirRwi();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir Rawat Jalan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_vikasirRwi',
						header: 'No. Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMPASIEN_vikasirRwi',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 35,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colALAMAT_vikasirRwi',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					//-------------- ## --------------
					{
						id: 'colTglKunj_vikasirRwi',
						header:'Tgl Kunjungan',
						dataIndex: 'TGL_KUNJUNGAN',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_KUNJUNGAN);
						}
					},
					//-------------- ## --------------
					{
						id: 'colPoliTujuan_vikasirRwi',
						header:'Ruangan :',
						dataIndex: 'NAMA_UNIT',
						width: 50,
						sortable: true,
						filter: {}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_vikasirRwi',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_vikasirRwi',
						handler: function(sm, row, rec)
						{
							if (rowSelected_vikasirRwi != undefined)
							{
								setLookUp_vikasirRwi(rowSelected_vikasirRwi.data)
							}
							else
							{								
								setLookUp_vikasirRwi();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_vikasirRwi, selectCount_vikasirRwi, dataSource_vikasirRwi),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_vikasirRwi = new Ext.Panel
    (
		{
			title: NamaForm_vikasirRwi,
			iconCls: 'Studi_Lanjut',
			id: mod_id_vikasirRwi,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_vikasirRwi],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_vikasirRwi,
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'Jenis Transaksi : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_vikasirRwi(125, "ComboJenisTransaksi_vikasirRwi"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'No. : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMedrec_vikasirRwi',							
							emptyText: 'No. Medrec',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_vikasirRwi = 1;
										DataRefresh_vikasirRwi(getCriteriaFilterGridDataView_vikasirRwi());
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Kunjungan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_vikasirRwi',
							value: now_vikasirRwi,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_vikasirRwi();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Kelompok : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						Vicbo_Kelompok(125, "ComboKelompok_vikasirRwi"),
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_vikasirRwi',
							emptyText: 'Nama',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_vikasirRwi = 1;
										DataRefresh_vikasirRwi(getCriteriaFilterGridDataView_vikasirRwi());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_vikasirRwi',
							value: now_vikasirRwi,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_vikasirRwi();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Ruangan : ',
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viCombo_UnitRWI(125, "ComboPoli_vikasirRwi"),
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Alamat : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_ALAMAT_vikasirRwi',
							emptyText: 'Alamat',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_vikasirRwi = 1;
										DataRefresh_vikasirRwi(getCriteriaFilterGridDataView_vikasirRwi());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_vikasirRwi',
							handler: function() 
							{					
								DfltFilterBtn_vikasirRwi = 1;
								DataRefresh_vikasirRwi(getCriteriaFilterGridDataView_vikasirRwi());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_vikasirRwi;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_vikasirRwi # --------------

/**
*	Function : setLookUp_vikasirRwi
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_vikasirRwi(rowdata)
{
    var lebar = 860;
    setLookUps_vikasirRwi = new Ext.Window
    (
    {
        id: 'SetLookUps_vikasirRwi',
		name: 'SetLookUps_vikasirRwi',
        title: NamaForm_vikasirRwi, 
        closeAction: 'destroy',        
        width: 875,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_vikasirRwi(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_vikasirRwi=undefined;
                //datarefresh_vikasirRwi();
				mNoKunjungan_vikasirRwi = '';
            }
        }
    }
    );

    setLookUps_vikasirRwi.show();
    if (rowdata == undefined)
    {
        // dataaddnew_vikasirRwi();
		// Ext.getCmp('btnDelete_vikasirRwi').disable();	
    }
    else
    {
        // datainit_vikasirRwi(rowdata);
    }
}
// End Function setLookUpGridDataView_vikasirRwi # --------------

/**
*	Function : getFormItemEntry_vikasirRwi
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_vikasirRwi(lebar,rowdata)
{
    var pnlFormDataBasic_vikasirRwi = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:[getItemPanelInputBiodata_vikasirRwi(lebar), getItemDataKunjungan_vikasirRwi(lebar)], //,getItemPanelBawah_vikasirRwi(lebar)],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_vikasirRwi',
						handler: function(){
							dataaddnew_vikasirRwi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_vikasirRwi',
						handler: function()
						{
							datasave_vikasirRwi(addNew_vikasirRwi);
							datarefresh_vikasirRwi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_vikasirRwi',
						handler: function()
						{
							var x = datasave_vikasirRwi(addNew_vikasirRwi);
							datarefresh_vikasirRwi();
							if (x===undefined)
							{
								setLookUps_vikasirRwi.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_vikasirRwi',
						handler: function()
						{
							datadelete_vikasirRwi();
							datarefresh_vikasirRwi();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Lookup ',
		                iconCls:'find',
		                menu: 
		                [
		                	{
						        text: 'Lookup Produk/Tindakan',
						        id:'btnLookUpProduk_vikasirRwi',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupProduk_vikasirRwi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
        					{
						        text: 'Lookup Konsultasi',
						        id:'btnLookUpKonsultasi_vikasirRwi',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupKonsultasi_vikasirRwi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Lookup Ganti Dokter',
						        id:'btnLookUpGantiDokter_vikasirRwi',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupGantiDokter_vikasirRwi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
		                ],
		            },
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Pembayaran ',
		                iconCls:'print',
		                menu: 
		                [
		                	{
						        text: 'Pembayaran',
						        id:'btnPembayaran_vikasirRwi',
						        iconCls: 'print',
						        handler: function(){
						            FormSetPembayaran_vikasirRwi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
        					{
						        text: 'Transfer ke Rawat Inap',
						        id:'btnTransferPembayaran_vikasirRwi',
						        iconCls: 'print',
						        handler: function(){
						            FormSetTransferPembayaran_vikasirRwi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Edit Tarif',
						        id:'btnEditTarif_vikasirRwi',
						        iconCls: 'print',
						        handler: function(){
						            FormSetEditTarif_vikasirRwi('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Posting Manual',
						        id:'btnPostingManual_vikasirRwi',
						        iconCls: 'print',
						        handler: function(){
						            FormSetPostingManual_vikasirRwi('1','2');
						        }
					    	}
					    	//-------------- ## --------------
		                ]
		            },
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Pasien Pulang',
						iconCls: '',
						id: 'btnPsnPulang_vikasirRwi',
						handler: function()
						{
							Formcheckout_vikasirRwi()
						}
					},
					
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Deposit',
						iconCls: '',
						id: 'btnDeposit_vikasirRwi',
						handler: function()
						{
							FormDeposit_vikasirRwi()	
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Move Room',
						iconCls: '',
						id: 'btnMoveRoom_vikasirRwi',
						handler: function()
						{
							FormMoveRoom_vikasirRwi()
						}
					}
					//-------------- ## --------------
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_vikasirRwi;
}
// End Function getFormItemEntry_vikasirRwi # --------------

/**
*	Function : getItemPanelInputBiodata_vikasirRwi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_vikasirRwi(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Medrec',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',
						width: 100,
						name: 'txtNoMedrec_vikasirRwi',
						id: 'txtNoMedrec_vikasirRwi',
						style:{'text-align':'left'},
						emptyText: 'No. Medrec',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',
						flex: 1,
						width: 35,
						name: '',
						value: 'Nama:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',														
						width : 567,	
						name: 'txtNama_vikasirRwi',
						id: 'txtNama_vikasirRwi',
						emptyText: 'Nama',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					//-------------- ## --------------
				]
			},	
			//-------------- ## --------------				
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Transaksi',
				name: 'compNoTrans_vikasirRwi',
				id: 'compNoTrans_vikasirRwi',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 100,
						name: 'txtNotrans_vikasirRwi',
						id: 'txtNotrans_vikasirRwi',
						emptyText: 'No. Transaksi',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 54,								
						value: 'Tanggal:',
						fieldLabel: 'Label',
					},
					{
						xtype: 'datefield',					
						fieldLabel: 'Tanggal',
						name: 'DtpTglLahir_vikasirRwi',
						id: 'DtpTglLahir_vikasirRwi',
						format: 'd/M/Y',
						width: 90,
						value: now_vikasirRwi,
					},
					//-------------- ## --------------
					{
						xtype:'button',
						text:'Info Seluruh Trans.',
						tooltip: 'Info Seluruh Trans.',
						id:'btnTrans_vikasirRwi',
						name:'btnTrans_vikasirRwi',
						width: 110,
						handler:function()
						{
							
						}
					},
					{
						xtype: 'displayfield',				
						width: 100,								
						value: 'Kamar Sementara:',
						fieldLabel: 'Label',
						id: 'lblKmrsmntr_vikasirRwi',
						name: 'lblKmrsmntr_vikasirRwi'
					},
					{
						xtype: 'textfield',					
						fieldLabel: 'Kamar Sementara',								
						width: 155,
						name: 'txtKmrSmentara_vikasirRwi',
						id: 'txtKmrSmentara_vikasirRwi',
						emptyText: 'Kamar Sementara',
					},					
					{
						xtype:'button',
						text:'Pindah Kamar',
						tooltip: 'Pindah Kamar',
						id:'btnPindahKamar_vikasirRwi',
						name:'btnPindahKamar_vikasirRwi',
						width: 50,
						handler:function()
						{
							
						}
					}
				]
			},
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Kelompok',
				name: 'compKlp_vikasirRwi',
				id: 'compKlp_vikasirRwi',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 100,
						name: 'txtKelompok_vikasirRwi',
						id: 'txtKelompok_vikasirRwi',
						emptyText: 'Kelompok'
					},
					{
						xtype: 'displayfield',				
						width: 65,								
						value: 'Kelas/Kamar:',
						fieldLabel: 'Label',
						id: 'lblkelaskamar_vikasirRwi',
						name: 'lblkelaskamar_vikasirRwi'
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 537,
						name: 'txtKelaskamar_vikasirRwi',
						id: 'txtKelaskamar_vikasirRwi',
						emptyText: 'Kelompok'
					}
					//-------------- ## --------------
				]
			}
			//-------------- ## --------------				
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_vikasirRwi # --------------

/**
*	Function : getItemDataKunjungan_vikasirRwi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataKunjungan_vikasirRwi(lebar) 
{
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height:400,
	    items:
		[				
			{
				xtype: 'panel',
				title: '',
				border:false,	
				height:375,
				items: 
				[
					{
						xtype: 'tabpanel',
						activeTab: 0,		
						height: 260,
						items: 
						[
							{
								xtype: 'panel',
								title: 'Transaksi',				
								padding: '4px',
								items: 
								[
									gridDataTrans_vikasirRwi(),
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'textfield',
										id: 'txttotaltrans_vikasirRwi',
										name: 'txttotaltrans_vikasirRwi',
										style:{'text-align':'right','margin-left':'700px'},
										width: 107,
										value: 0,
										readOnly: true,
									}
								]
							},
							{
								xtype: 'panel',
								title: 'Transaksi Sudah Terkirim',				
								padding: '4px',
								items: 
								[
									gridDataTransPntJasa_vikasirRwi(),
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'textfield',
										id: 'txttotaltranspntjasa_vikasirRwi',
										name: 'txttotaltranspntjasa_vikasirRwi',
										style:{'text-align':'right','margin-left':'700px'},
										width: 107,
										value: 0,
										readOnly: true,
									}
								]
							},
							{
								xtype: 'panel',
								title: 'Rekapitulasi',				
								padding: '4px',
								items: 
								[
									gridDataTransrekap_vikasirRwi(),
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'textfield',
										id: 'txttotaltransrekap_vikasirRwi',
										name: 'txttotaltransrekap_vikasirRwi',
										style:{'text-align':'right','margin-left':'700px'},
										width: 107,
										value: 0,
										readOnly: true,
									}
								
								]
							}
						]
					},
					{ 
						xtype: 'tbspacer',
						width: 15,
						height: 5
					},
					{
						xtype: 'compositefield',
						fieldLabel: 'Tanggal',
						anchor: '100%',
						// width: 250,
						id:'compbutton_vikasirRwi',
						items: 
						[
							{
								xtype:'button',
								text:'Kirim Transaksi Ke Kasir',
								tooltip: 'Kirim Transaksi Ke Kasir',
								id:'btnkirimtrans_vikasirRwi',
								name:'btnkirimtrans_vikasirRwi',
								// width: 50,
								handler:function()
								{
									
								}
							},		
							{
								xtype:'button',
								text:'Laboratorium',
								tooltip: 'Laboratorium',
								id:'btnLaboratorium_vikasirRwi',
								name:'btnLaboratorium_vikasirRwi',
								handler:function()
								{
									
								}
							},	
							{
								xtype:'button',
								text:'Radiologi',
								tooltip: 'Radiologi',
								id:'btnRadiologi_vikasirRwi',
								name:'btnRadiologi_vikasirRwi',
								handler:function()
								{
									
								}
							},	
							{
								xtype:'button',
								text:'Resep',
								tooltip: 'Resep',
								id:'btnResep_vikasirRwi',
								name:'btnResep_vikasirRwi',
								handler:function()
								{
									
								}
							},	
							{
								xtype:'button',
								text:'Kamar Operasi',
								tooltip: 'Kamar Operasi',
								id:'btnKmroperasi_vikasirRwi',
								name:'btnKmroperasi_vikasirRwi',
								handler:function()
								{
									
								}
							},	
							{
								xtype:'button',
								text:'Diagnosa',
								tooltip: 'Diagnosa',
								id:'btnDiagnosa_vikasirRwi',
								name:'btnDiagnosa_vikasirRwi',
								// width: 50,
								handler:function()
								{
									
								}
							}	
						]
					},
					{ 
						xtype: 'tbspacer',						
						height: 5
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Catatan:',
						fieldLabel: 'Label',
						id: 'lblCatatan_vikasirRwi',
						name: 'lblCatatan_vikasirRwi'
					},
					{
						xtype: 'textarea',						
						fieldLabel: 'Catatan',
						width: 817,
						name: 'txtCatatan_vikasirRwi',
						id: 'txtCatatan_vikasirRwi'
					}					
				]
			}
		]
	};
    return items;
};
// End Function getItemDataKunjungan_vikasirRwi # --------------

/**
*	Function : getItemPanelBawah_vikasirRwi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

/**
*	Function : gridDataTrans_vikasirRwi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataTrans_vikasirRwi()
{
    
    var FieldGrdKasir_vikasirRwi = 
	[
	];
	
    dsDataGrdJab_vikasirRwi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_vikasirRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_vikasirRwi,
        height: 190,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Tanggal',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Reff',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 50
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 355,
				renderer: function(v, params, record) 
				{
					
				}			
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dok',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Tarif',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			}			
        ]
    }    
    return items;
}

function gridDataTransPntJasa_vikasirRwi()
{
    
    var FieldGrdKasir_vikasirRwi = 
	[
	];
	
    dsDataGrdJab_vikasirRwi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_vikasirRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_vikasirRwi,
        height: 190,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Tanggal',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Reff',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 50
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 355,
				renderer: function(v, params, record) 
				{
					
				}			
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dok',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Tarif',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			}			
        ]
    }    
    return items;
}

function gridDataTransrekap_vikasirRwi()
{
    
    var FieldGrdKasir_vikasirRwi = 
	[
	];
	
    dsDataGrdJab_vikasirRwi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_vikasirRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_vikasirRwi,
        height: 190,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Tanggal',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Reff',
				sortable: true,
				width: 60
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 50
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 355,
				renderer: function(v, params, record) 
				{
					
				}			
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dok',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Tarif',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			}			
        ]
    }    
    return items;
}
// End Function gridDataTrans_vikasirRwi # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupProduk_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup List Produk
*/

function FormSetLookupProduk_vikasirRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryLookupProduk_vikasirRwi = new Ext.Window
    (
        {
            id: 'FormGrdLookupProduk_vikasirRwi',
            title: 'List Produk',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryProdukGridDataView_vikasirRwi(subtotal,NO_KUNJUNGAN),
                //-------------- ## --------------
            ],
            tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAddTreeProduk_vikasirRwi',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDeleteTreeProduk_vikasirRwi',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
				]
			}
        }
    );
    vWinFormEntryLookupProduk_vikasirRwi.show();
    mWindowGridLookupLookupProduk  = vWinFormEntryLookupProduk_vikasirRwi; 
}
// End Function FormSetLookupProduk_vikasirRwi # --------------

/**
*   Function : getFormItemEntryProdukGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form produk
*/
function getFormItemEntryProdukGridDataView_vikasirRwi(subtotal,NO_KUNJUNGAN)
{
	var lebar = 515;
	var itemsListProdukGridDataView_vikasirRwi = 
    {
        xtype: 'panel',
        title: '',
        name: 'PanelListProdukGridDataView_vikasirRwi',
        id: 'PanelListProdukGridDataView_vikasirRwi',        
        bodyStyle: 'padding: 5px;',
        layout: 'hbox',
        height: 1170,
        border:false,
        items: 
        [           
            itemsTreeListProdukGridDataView_vikasirRwi(),
            //-------------- ## -------------- 
            { 
                xtype: 'tbspacer',
                width: 5,
            },
            //-------------- ## -------------- 
            getItemTreeGridTransaksiGridDataView_vikasirRwi(lebar),
            //-------------- ## -------------- 
        ]
    }
    return itemsListProdukGridDataView_vikasirRwi;
}
// End Function getFormItemEntryProdukGridDataView_vikasirRwi # --------------

/**
*   Function : itemsTreeListProdukGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan tree prosuk
*/
function itemsTreeListProdukGridDataView_vikasirRwi()
{
	var children = 
	[
		{
		     text:'First Level Child 1',
		     children:
		     [
		     	{
		        	text:'Second Level Child 1',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		        {
		        	text:'Second Level Child 2',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		    ]
		},
		{
		     text:'First Level Child 2',
		     children:
		     [
		     	{
		        	text:'Second Level Child 1',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		        {
		        	text:'Second Level Child 2',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		    ]
		},
	];

    var tree = new Ext.tree.TreePanel
    (
    	{
        	loader:new Ext.tree.TreeLoader(),
        	width:200,
        	height:340,
        	renderTo:Ext.getBody(),
        	root:new Ext.tree.AsyncTreeNode
        	(
        		{
             		expanded:true,
             		leaf:false,
             		text:'Tree Root',
             		children:children
             	}
            )
    	}
    );

    var pnlTreeFormDataWindowPopup_vikasirRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                {
		            xtype: 'radiogroup',
		            //fieldLabel: 'Single Column',
		            itemCls: 'x-check-group-alt',
		            columns: 1,
		            items: 
		            [
		                {
		                	boxLabel: 'Semua Unit', 
		                	name: 'ckboxTreeSemuaUnit_vikasirRwi',
		                	id: 'ckboxTreeSemuaUnit_vikasirRwi',  
		                	inputValue: 1,
		                },
		                //-------------- ## -------------- 
		                {
		                	boxLabel: 'Unit Penyakit Dalam', 
		                	name: 'ckboxTreePerUnit_vikasirRwi',
		                	id: 'ckboxTreePerUnit_vikasirRwi',  
		                	inputValue: 2, 
		                	checked: true,
		                },
		                //-------------- ## -------------- 
		            ]
		        },
				//-------------- ## -------------- 
                tree,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_vikasirRwi;
}
// End Function itemsTreeListProdukGridDataView_vikasirRwi # --------------

/**
*   Function : getItemTreeGridTransaksiGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemTreeGridTransaksiGridDataView_vikasirRwi(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:2px 2px 2px 2px',
        border:true,
        width: lebar,
        height:402, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridTreeDataViewEditGridDataView_vikasirRwi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemTreeGridTransaksiGridDataView_vikasirRwi # --------------

/**
*   Function : gridTreeDataViewEditGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridTreeDataViewEditGridDataView_vikasirRwi()
{
    
    var FieldTreeGrdKasirGridDataView_vikasirRwi = 
    [
    ];
    
    dsDataTreeGrdJabGridDataView_vikasirRwi= new WebApp.DataStore
    ({
        fields: FieldTreeGrdKasirGridDataView_vikasirRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataTreeGrdJabGridDataView_vikasirRwi,
        height: 395,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Cek',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi Produk',
                sortable: true,
                width: 330,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridTreeDataViewEditGridDataView_vikasirRwi # --------------

// ## END MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupKonsultasi_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Konsultasi
*/

function FormSetLookupKonsultasi_vikasirRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryKonsultasi_vikasirRwi = new Ext.Window
    (
        {
            id: 'FormGrdLookupKonsultasi_vikasirRwi',
            title: 'Konsultasi',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupKonsultasi_vikasirRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryKonsultasi_vikasirRwi.show();
    mWindowGridLookupKonsultasi  = vWinFormEntryKonsultasi_vikasirRwi; 
};

// End Function FormSetLookupKonsultasi_vikasirRwi # --------------

/**
*   Function : getFormItemEntryLookupKonsultasi_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/
function getFormItemEntryLookupKonsultasi_vikasirRwi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataKonsultasiWindowPopup_vikasirRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputKonsultasiDataView_vikasirRwi(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataKonsultasiWindowPopup_vikasirRwi;
}
// End Function getFormItemEntryLookupKonsultasi_vikasirRwi # --------------

/**
*   Function : getItemPanelInputKonsultasiDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/

function viComboJenisTransaksi_vikasirRwi(lebar,Nama_ID)
{
  var cbo_viComboJenisTransaksi_vikasirRwi = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Transaksi..',
            fieldLabel: "Transaksi",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdTrnsksi',
                        'displayTextTrnsksi'
                    ],
                    data: [['all', "Semua"],[1, "Bayar"],[2, "Belum Bayar"]]
                }
            ),
            valueField: 'IdTrnsksi',
            displayField: 'displayTextTrnsksi',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisTransaksi_vikasirRwi;
};
// End Function viComboJenisTransaksi_vikasirRwi # --------------

function getItemPanelInputKonsultasiDataView_vikasirRwi(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Asal Unit',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_vikasirRwi(250,'CmboAsalUnit_vikasirRwi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Konsultasi ke',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_vikasirRwi(250,'CmboKonsultasike_vikasirRwi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_vikasirRwi(250,'CmboDokter_vikasirRwi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputKonsultasiDataView_vikasirRwi # --------------

// ## END MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupGantiDokter_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupGantiDokter_vikasirRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_vikasirRwi = new Ext.Window
    (
        {
            id: 'FormGrdLookupGantiDokter_vikasirRwi',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupGantiDokter_vikasirRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_vikasirRwi.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_vikasirRwi; 
};

// End Function FormSetLookupGantiDokter_vikasirRwi # --------------

/**
*   Function : getFormItemEntryLookupGantiDokter_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupGantiDokter_vikasirRwi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataGantiDokterWindowPopup_vikasirRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputGantiDokterDataView_vikasirRwi(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataGantiDokterWindowPopup_vikasirRwi;
}
// End Function getFormItemEntryLookupGantiDokter_vikasirRwi # --------------

/**
*   Function : getItemPanelInputGantiDokterDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputGantiDokterDataView_vikasirRwi(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Tanggal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtTglGantiDokter_vikasirRwi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtBlnGantiDokter_vikasirRwi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtThnGantiDokter_vikasirRwi',
                        emptyText: '2014',
                        width: 50,
                    },
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Asal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_vikasirRwi(250,'CmboDokterAsal_vikasirRwi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Ganti',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_vikasirRwi(250,'CmboDokterGanti_vikasirRwi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputGantiDokterDataView_vikasirRwi # --------------

// ## END MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

// ## END MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

/**
*   Function : FormSetPembayaran_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Pembayaran
*/

function FormSetPembayaran_vikasirRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPembayaran_vikasirRwi = new Ext.Window
    (
        {
            id: 'FormGrdPembayaran_vikasirRwi',
            title: 'Pembayaran',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryGridDataView_vikasirRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPembayaran_vikasirRwi.show();
    mWindowGridLookupPembayaran  = vWinFormEntryPembayaran_vikasirRwi; 
};

// End Function FormSetPembayaran_vikasirRwi # --------------

/**
*   Function : getFormItemEntryGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function getFormItemEntryGridDataView_vikasirRwi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataWindowPopup_vikasirRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataGridDataView_vikasirRwi(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiGridDataView_vikasirRwi(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnOkGridTransaksiPembayaranGridDataView_vikasirRwi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnCancelGridTransaksiPembayaranGridDataView_vikasirRwi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtBiayaGridTransaksiPembayaranGridDataView_vikasirRwi',
                            name: 'txtBiayaGridTransaksiPembayaranGridDataView_vikasirRwi',
							style:{'text-align':'right','margin-left':'150px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtPiutangGridTransaksiPembayaranGridDataView_vikasirRwi',
                            name: 'txtPiutangGridTransaksiPembayaranGridDataView_vikasirRwi',
							style:{'text-align':'right','margin-left':'146px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtTunaiGridTransaksiPembayaranGridDataView_vikasirRwi',
                            name: 'txtTunaiGridTransaksiPembayaranGridDataView_vikasirRwi',
							style:{'text-align':'right','margin-left':'142px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtDiscGridTransaksiPembayaranGridDataView_vikasirRwi',
                            name: 'txtDiscGridTransaksiPembayaranGridDataView_vikasirRwi',
							style:{'text-align':'right','margin-left':'138px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataWindowPopup_vikasirRwi;
}
// End Function getFormItemEntryGridDataView_vikasirRwi # --------------

/**
*   Function : getItemPanelInputBiodataGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemPanelInputBiodataGridDataView_vikasirRwi(lebar) 
{
    var items =
    {
        title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
				xtype: 'textfield',
				fieldLabel: 'No. Transaksi',
				id: 'txtNoTransaksiPembayaranGridDataView_vikasirRwi',
				name: 'txtNoTransaksiPembayaranGridDataView_vikasirRwi',
				emptyText: 'No. Transaksi',
				readOnly: true,
				flex: 1,
				width: 195,				
			},
			//-------------- ## --------------
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				id: 'txtNamaPasienPembayaranGridDataView_vikasirRwi',
				name: 'txtNamaPasienPembayaranGridDataView_vikasirRwi',
				emptyText: 'Nama Pasien',
				flex: 1,
				anchor: '100%',					
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran',
				anchor: '100%',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_vikasirRwi(195,'CmboPembayaranGridDataView_vikasirRwi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				anchor: '100%',
				labelSeparator: '',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_vikasirRwi(195,'CmboPembayaranDuaGridDataView_vikasirRwi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataGridDataView_vikasirRwi # --------------

/**
*   Function : getItemGridTransaksiGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemGridTransaksiGridDataView_vikasirRwi(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar-80,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridDataView_vikasirRwi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiGridDataView_vikasirRwi # --------------

/**
*   Function : gridDataViewEditGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridDataViewEditGridDataView_vikasirRwi()
{
    
    var FieldGrdKasirGridDataView_vikasirRwi = 
    [
    ];
    
    dsDataGrdJabGridDataView_vikasirRwi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_vikasirRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_vikasirRwi,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kode',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Qty',
                sortable: true,
                width: 40
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Biaya',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Piutang',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tunai',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Disc',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridDataView_vikasirRwi # --------------

// ## END MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------
function FormMoveRoom_vikasirRwi() 
{
    vWinFormEntryMoveRoom_vikasirRwi = new Ext.Window
    (
        {
            id: 'FormMoveRoom_vikasirRwi',
            title: 'Move Room',
            closeAction: 'destroy',
            closable:true,
            width: 480,
            height: 345,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,            
            modal: true,                                   
            items: 
            [
				getFormMoveRoom_vikasirRwi()
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryMoveRoom_vikasirRwi.show();  
};

function getFormMoveRoom_vikasirRwi()
{
	var pnlFormMoveRoom_vikasirRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'form',
            // padding: '8px',
            bodyStyle: 'padding:5px 5px 5px 5px;',
            items:
			[
				{
					xtype: 'panel',
					title: 'Asal',
					border:true,		
					id: 'panelAsalMoveRoom_vikasirRwi',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Pasien',
							anchor: '100%',
							id: 'compPasienMoveRoom_vikasirRwi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Pasien:',
									id: 'labelPasienMoveRoom_vikasirRwi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'KdPasien',														
									// width : 340,	
									width : 120,	
									name: 'txtKdPasienMoveRoom_vikasirRwi',
									id: 'txtKdPasienMoveRoom_vikasirRwi',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'NMPasien',														
									// width : 340,	
									width : 215,	
									name: 'txtNMPasienMoveRoom_vikasirRwi',
									id: 'txtNMPasienMoveRoom_vikasirRwi',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcKamarMoveRoom_vikasirRwi',
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Kamar',
							anchor: '100%',
							id: 'compKamarMoveRoom_vikasirRwi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Kelas:',
									id: 'labelKelasMoveRoom_vikasirRwi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'KelasPasien',														
									// width : 340,	
									width : 120,	
									name: 'txtKelasMoveRoom_vikasirRwi',
									id: 'txtKelasMoveRoom_vikasirRwi',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'displayfield',
									flex: 1,
									width: 50,
									name: '',
									id: 'labelKamarMoveRoom_vikasirRwi',
									value: 'Kamar:',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'NMPasien',														
									width : 160,	
									name: 'txtKamarMoveRoom_vikasirRwi',
									id: 'txtKamarMoveRoom_vikasirRwi',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						}
					]
				},
				{ 
					xtype: 'tbspacer',					
					id: 'spcAsalMoveRoom_vikasirRwi',					
					height: 5
				},
				{
					xtype: 'panel',
					title: 'Tujuan',
					border:true,		
					id: 'panelTujuanMoveRoom_vikasirRwi',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Pasien',
							anchor: '100%',
							id: 'compSpesialisMoveRoom_vikasirRwi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Spesialis:',
									id: 'labelSpcMoveRoom_vikasirRwi',
									fieldLabel: 'Label'
								},
								viComboSpescialis_vikasirRwi(),
								{
									xtype: 'displayfield',
									flex: 1,
									width: 50,
									name: '',
									value: 'Kelas:',
									id: 'labelKlstujuanMoveRoom_vikasirRwi',
									fieldLabel: 'Label'
								},
								viComboKelas_vikasirRwi(),								
							]
						},
						{ 
							xtype: 'tbspacer',					
							id: 'spcRuangMoveRoom_vikasirRwi',					
							height: 5
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Ruang',
							anchor: '100%',
							id: 'compRuangMoveRoom_vikasirRwi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Ruang:',
									id: 'labelRuangMoveRoom_vikasirRwi',
									fieldLabel: 'Label'
								},
								viComboRuangan_vikasirRwi()														
							]
						},
						{ 
							xtype: 'tbspacer',					
							id: 'spcKamarMoveRoom_vikasirRwi',					
							height: 5
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Ruang',
							anchor: '100%',
							id: 'compKamarMoveRoom_vikasirRwi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Kamar:',
									id: 'labelKamarMoveRoom_vikasirRwi',
									fieldLabel: 'Label'
								},
								viComboKamar_vikasirRwi(),
								{
									xtype: 'displayfield',
									flex: 1,
									width: 50,
									name: '',
									value: 'Sisa Bed:',
									id: 'labelSisaBedMoveRoom_vikasirRwi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									id: 'txtSisaBedMoveRoom_vikasirRwi',
									name: 'txtSisaBedMoveRoom_vikasirRwi',									
									width: 100,							
									readOnly: true
								}								
							]
						},
						{ 
							xtype: 'tbspacer',					
							id: 'spcTglMoveRoom_vikasirRwi',					
							height: 5
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Ruang',
							anchor: '100%',
							id: 'compTglMoveRoom_vikasirRwi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Tanggal:',
									id: 'dsplTglMoveRoom_vikasirRwi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'dateTglMoveRoom_vikasirRwi',
									name: 'dateTglMoveRoom_vikasirRwi',
									width: 160,
									format: 'd/M/Y',
								},
								{
									xtype: 'displayfield',
									flex: 1,
									width: 80,
									name: '',
									value: 'Jam Pindah:',
									id: 'labelJamPindahMoveRoom_vikasirRwi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'date2TglMoveRoom_vikasirRwi',
									name: 'date2TglMoveRoom_vikasirRwi',
									width: 90								
								}
							]
						}							
					]
				},
				{ 
					xtype: 'tbspacer',					
					id: 'spcTujuanMoveRoom_vikasirRwi',					
					height: 5
				},
				{
					xtype: 'panel',
					title: '',
					border:true,		
					id: 'panelAlasanMoveRoom_vikasirRwi',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[					
						{
							xtype: 'compositefield',
							fieldLabel: 'Alasan',
							anchor: '100%',
							id: 'compAlasanMoveRoom_vikasirRwi',							
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Tanggal:',
									id: 'dsplAlasanMoveRoom_vikasirRwi',
									fieldLabel: 'Label'
								},
								viComboAlasanPindah_vikasirRwi()
							]
						}							
					]
				},
				{
					xtype: 'panel',
					title: '',
					border:false,		
					id: 'panelButtonMoveRoom_vikasirRwi',					
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[	
						{
							xtype: 'compositefield',
							fieldLabel: 'Alasan',
							anchor: '100%',
							id: 'compAlasanMoveRoom_vikasirRwi',							
							items: 
							[					
								{
									xtype:'button',
									text:'Ok',
									width:70,
									style:{'margin-left':'300px'},
									hideLabel:true,
									id: 'ButtonOKMoveRoom_vikasirRwi',
									handler:function()
									{
										
									}   
								},
								{
									xtype:'button',
									text:'Batal',
									width:70,
									style:{'margin-left':'300px'},
									hideLabel:true,
									id: 'ButtonBatalMoveRoom_vikasirRwi',
									handler:function()
									{
										
									}   
								},
							]
						}
					]
				}
			]
		}
	)	
	return pnlFormMoveRoom_vikasirRwi
}

function FormDeposit_vikasirRwi() 
{
    vWinFormEntryDeposit_vikasirRwi = new Ext.Window
    (
        {
            id: 'FormDeposit_vikasirRwi',
            title: 'Deposit',
            closeAction: 'destroy',
            closable:true,
            width: 480,
            height: 325,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,            
            modal: true,                                   
            items: 
            [
				getFormDeposit_vikasirRwi()
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryDeposit_vikasirRwi.show();  
};

function Formcheckout_vikasirRwi() 
{
    vWinFormEntrycheckout_vikasirRwi = new Ext.Window
    (
        {
            id: 'Formcheckout_vikasirRwi',
            title: 'Check Out',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 225,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,            
            modal: true,                                   
            items: 
            [
				getFormCheckout_vikasirRwi()
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntrycheckout_vikasirRwi.show();
    // mWindowGridLookupTransferPembayaran  = vWinFormEntrycheckout_vikasirRwi; 
};

/**
*   Function : FormSetTransferPembayaran_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Transfer ke Rawat Inap
*/

function FormSetTransferPembayaran_vikasirRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryTransferPembayaran_vikasirRwi = new Ext.Window
    (
        {
            id: 'FormGrdTrnsferPembayaran_vikasirRwi',
            title: 'Pemindahan Tagihan Ke Unit Rawat Inap',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 450,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormTransferPembayaranItemEntryGridDataView_vikasirRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryTransferPembayaran_vikasirRwi.show();
    mWindowGridLookupTransferPembayaran  = vWinFormEntryTransferPembayaran_vikasirRwi; 
};

// End Function FormSetTransferPembayaran_vikasirRwi # --------------

function getFormDeposit_vikasirRwi()
{
    var pnlFormDeposit_vikasirRwi = new Ext.FormPanel
    (
        {
            title: 'Informasi Pembayaran',
            region: 'center',
            layout: 'form',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            items:
            //-------------- #items# --------------
            [			
				{
					xtype: 'compositefield',
					fieldLabel: 'Tanggal',
					anchor: '100%',
					width: 130,
					items: 
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Tanggal',
							id: 'DatePembayaran_vikasirRwi',
							name: 'DatePembayaran_vikasirRwi',
							width: 100,
							format: 'd/M/Y'
						},
						{
							xtype: 'displayfield',
							flex: 1,
							width: 70,
							name: '',
							value: 'No. Bukti:',
							fieldLabel: 'Label'
						},
						{
							xtype: 'textfield',
							flex: 1,
							fieldLabel: 'Label',														
							width : 165,	
							name: 'txtNobukti_vikasirRwi',
							id: 'txtNobukti_vikasirRwi',
							emptyText: 'No. Bukti',
							listeners:
							{
							}
						}
						//-------------- ## --------------				
					]
				},		
				{
					xtype: 'panel',
					title: 'Informasi Pembayar',
					border:false,	
					// height:375,
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Nama',
							anchor: '100%',
							id: 'compNamadeposit_vikasirRwi',
							// width: 130,
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Nama:',
									id: 'labelNamadeposit_vikasirRwi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'Nama',														
									width : 340,	
									name: 'txtNamadeposit_vikasirRwi',
									id: 'txtNamadeposit_vikasirRwi',
									emptyText: 'Nama',
									listeners:
									{
									}
								}
							]
						}
					]
				},
				{
					xtype: 'panel',
					title: 'Detail Pembayar',
					border:false,	
					// height:375,
					bodyStyle: 'padding:5px 5px 5px 5px;',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Nama',
							anchor: '100%',
							id: 'compuntukdeposit_vikasirRwi',
							// width: 130,
							items: 
							[
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Untuk:',
									id: 'labeluntukdeposit_vikasirRwi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: 'Untuk',														
									width : 340,	
									name: 'txtuntukdeposit_vikasirRwi',
									id: 'txtuntukdeposit_vikasirRwi',
									emptyText: 'Untuk',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcPasiendeposit_vikasirRwi'
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Pasien',
							anchor: '100%',
							id: 'compPasiendeposit_vikasirRwi',
							// width: 130,
							items: 
							[								
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Pasien:',
									id: 'labelPasiendeposit_vikasirRwi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 120,	
									name: 'txtPasiendeposit_vikasirRwi',
									id: 'txtPasiendeposit_vikasirRwi',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 215,	
									name: 'txtNmPasiendeposit_vikasirRwi',
									id: 'txtNmPasiendeposit_vikasirRwi',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcKamardeposit_vikasirRwi'
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Kelas',
							anchor: '100%',
							id: 'compKamardeposit_vikasirRwi',
							// width: 130,
							items: 
							[								
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Kamar:',
									id: 'labelKamardeposit_vikasirRwi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 120,	
									name: 'txtKelasdeposit_vikasirRwi',
									id: 'txtKelasdeposit_vikasirRwi',
									emptyText: '',
									listeners:
									{
									}
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 215,	
									name: 'txtKamardeposit_vikasirRwi',
									id: 'txtKamardeposit_vikasirRwi',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcTotaldeposit_vikasirRwi'
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Total',
							anchor: '100%',
							id: 'compTotaldeposit_vikasirRwi',
							// width: 130,
							items: 
							[								
								{
									xtype: 'displayfield',
									flex: 1,
									width: 95,
									name: '',
									value: 'Jumlah:',
									id: 'labelTotaldeposit_vikasirRwi',
									fieldLabel: 'Label'
								},
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',														
									width : 120,	
									name: 'txtTotaldeposit_vikasirRwi',
									id: 'txtTotaldeposit_vikasirRwi',
									emptyText: '',
									listeners:
									{
									}
								}
							]
						},
						{ 
							xtype: 'tbspacer',									
							height: 5,
							id: 'spcDepositdeposit_vikasirRwi'
						},
						{
							xtype: 'panel',
							title: '',				
							border:false,
							labelAlign: 'right',						
							items: 
							[
								{
									xtype: 'compositefield',
									fieldLabel: '',
									id: 'CompButtonDeposit_vikasirRwi',
									items: 
									[								
										{
											xtype: 'button',
											text:'Ok',
											id: 'btnokDeposit_vikasirRwi',
											name: 'btnokDeposit_vikasirRwi',
											style:{'margin-left':'250px'},
											width: 60
										}
										,	
										{
											xtype: 'button',
											text:'Batal',
											id: 'btnbatalDeposit_vikasirRwi',
											name: 'btnbatalDeposit_vikasirRwi',
											style:{'margin-left':'250px'},
											width: 60
										},	
										{
											xtype: 'button',
											text:'Print',
											id: 'btnCetakDeposit_vikasirRwi',
											name: 'btnbatalDeposit_vikasirRwi',
											style:{'margin-left':'250px'},
											width: 60
										}

									]
								}
							]
						}
					]
				}			
			]
		}
	)
	return pnlFormDeposit_vikasirRwi
}

function getFormCheckout_vikasirRwi()
{
    var pnlFormCheckout_vikasirRwi = new Ext.FormPanel
    (
        {
            title: 'Informasi Pasien Pulang',
            region: 'center',
            layout: 'form',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            items:
            //-------------- #items# --------------
            [
               {
					xtype: 'compositefield',
					fieldLabel: 'Pasien',
					id: 'compkdpasiencheckout_vikasirRwi',
					items: 
                    [
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtkdpasiencheckout_vikasirRwi',
                            name: 'txtkdpasiencheckout_vikasirRwi',
                            width: 80,
                            readOnly: true,
                        },
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtnmpasiencheckout_vikasirRwi',
                            name: 'txtnmpasiencheckout_vikasirRwi',
                            width: 160,
                            readOnly: true,
                        }
					]
				},
               {
					xtype: 'compositefield',
					fieldLabel: 'Kelas',
					id: 'CompKelascheckout_vikasirRwi',
					items: 
                    [
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtKelascheckout_vikasirRwi',
                            name: 'txtKelascheckout_vikasirRwi',
                            width: 100,
                            readOnly: true,
                        },
						{
							xtype: 'displayfield',
							flex: 1,
							width: 35,
							name: '',
							value: 'Kamar:',
							fieldLabel: 'Label'
						},
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtkamarcheckout_vikasirRwi',
                            name: 'txtkamarcheckout_vikasirRwi',
                            width: 100,
                            readOnly: true,
                        }
					]
				},
                {
					xtype: 'compositefield',
					fieldLabel: 'Tgl. Masuk',
					id: 'CompTglmasukcheckout_vikasirRwi',
					items: 
                    [
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txttglmasukcheckout_vikasirRwi',
                            name: 'txttglmasukcheckout_vikasirRwi',
                            width: 100,
                            readOnly: true,
                        },
						{
							xtype: 'displayfield',
							flex: 1,
							width: 35,
							name: '',
							value: 'Jam:',
							id: 'dspljammasukcheckout_vikasirRwi',
							fieldLabel: 'Label'
						},
						{
                            xtype: 'textfield',
                            fieldLabel: '',
                            id: 'txtjammasukcheckout_vikasirRwi',
                            name: 'txtjammasukcheckout_vikasirRwi',
                            width: 100,
                            readOnly: true,
                        }
					]
				},	
				{
					xtype: 'compositefield',
					fieldLabel: 'Tgl. Pulang',
					id: 'CompTglpulangcheckout_vikasirRwi',
					items: 
                    [
						{
							xtype: 'datefield',
							id: 'dtfpulangcheckout_vikasirRwi',
							value: now_vikasirRwi,
							format: 'd/M/Y',
							width: 100
						},
						{
							xtype: 'displayfield',
							flex: 1,
							width: 35,
							name: '',
							value: 'Jam:',
							id: 'dspljammasukcheckout_vikasirRwi',
							fieldLabel: 'Label'
						},
						{
							xtype: 'datefield',
							id: 'dtfjampulangcheckout_vikasirRwi',
							// value: now_vikasirRwi,
							// format: 'HH:MM',
							width: 100
						}
					]
				},
				viComboKeadaanPsn_vikasirRwi(),						
				{
					xtype: 'panel',
					title: '',				
					border:false,
					labelAlign: 'right',
					// padding: '4px',
					items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'CompButtoncheckout_vikasirRwi',
							items: 
							[								
								{
									xtype: 'button',
									text:'Ok',
									id: 'btnok_vikasirRwi',
									name: 'btnok_vikasirRwi',
									style:{'margin-left':'210px'},
									width: 60
								}
								,	
								{
									xtype: 'button',
									text:'Batal',
									id: 'btnbatal_vikasirRwi',
									name: 'btnbatal_vikasirRwi',
									style:{'margin-left':'210px'},
									width: 75
								}
							]
						}
					]
				}
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormCheckout_vikasirRwi;
}

function viComboKeadaanPsn_vikasirRwi()
{
  var cbo_viComboKeadaanPsn_vikasirRwi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKeadaanPsn_vikasirRwi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Keadaan Pasien..',
            fieldLabel: "Keadaan Pasien",
            value:1,        
            width:245,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKeadaan',
                        'displayTextKeadaan'
                    ],
                    data: [[1, "Sembuh"],[2, "Sakit"]]
                }
            ),
            valueField: 'IdKeadaan',
            displayField: 'displayTextKeadaan',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKeadaanPsn_vikasirRwi;
};
function viComboKamar_vikasirRwi()
{
  var cbo_viComboKamar_vikasirRwi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKamar_vikasirRwi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Kamar..',
            fieldLabel: "Kamar",
            value:1,        
            width:180,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKamar',
                        'displayTextKamar'
                    ],
                    data: [[1, "011 VVIP"],[2, "012 VVIP"]]
                }
            ),
            valueField: 'IdKamar',
            displayField: 'displayTextKamar',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKamar_vikasirRwi;
};

function viComboRuangan_vikasirRwi()
{
  var cbo_viComboRuangan_vikasirRwi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboRuangan_vikasirRwi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Ruang..',
            fieldLabel: "Ruang",
            value:1,        
            width:340,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdRuang',
                        'displayTextRuang'
                    ],
                    data: [[1, "Kelas VVIP"],[2, "Kelas VVIP"]]
                }
            ),
            valueField: 'IdRuang',
            displayField: 'displayTextRuang',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboRuangan_vikasirRwi;
};

function viComboSpescialis_vikasirRwi()
{
  var cbo_viComboSpescialis_vikasirRwi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboSpescialis_vikasirRwi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Specialis..',
            fieldLabel: "Spesialis",
            value:1,        
            width:180,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdSpesialis',
                        'displayTextSpesialis'
                    ],
                    data: [[1, "Penyakit Dalam"],[2, "Jantung"]]
                }
            ),
            valueField: 'IdSpesialis',
            displayField: 'displayTextSpesialis',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboSpescialis_vikasirRwi;
};

function viComboKelas_vikasirRwi()
{
	var cbo_viComboKelas_vikasirRwi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKelas_vikasirRwi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Kelas..',
            fieldLabel: "Kelas",
            value:1,        
            width:100,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKelas',
                        'displayTextKelas'
                    ],
                    data: [[1, "VVIP"],[2, "VIP"]]
                }
            ),
            valueField: 'IdKelas',
            displayField: 'displayTextKelas',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKelas_vikasirRwi;
};

function viComboAlasanPindah_vikasirRwi()
{
  var ComboAlasanPindah_vikasirRwi = new Ext.form.ComboBox
    (
    
        {
            id:"ComboAlasanPindah_vikasirRwi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Alasan..',
            fieldLabel: "Alasan",
            value:1,        
            width:340,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdAlasanPindah',
                        'displayTextAlasanPindah'
                    ],
                    data: [[1, "Tidak Nyaman"],[2, "Tidak Mampu Bayar"]]
                }
            ),
            valueField: 'IdAlasanPindah',
            displayField: 'displayTextAlasanPindah',
            listeners:  
            {
			
            }
        }
    );
    return ComboAlasanPindah_vikasirRwi;
};

/**
*   Function : getFormTransferPembayaranItemEntryGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form transfer ke rawat inap
*/
function getFormTransferPembayaranItemEntryGridDataView_vikasirRwi(subtotal,NO_KUNJUNGAN)
{
    var pnlFormTransferPembayaranDataWindowPopup_vikasirRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            items:
            //-------------- #items# --------------
            [
                {
                    xtype: 'fieldset',
                    title: 'Tagihan dipindahkan ke',
                    frame: false,
                    width: 350,
                    height: 165,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Transaksi',
                            id: 'txtNoTransaksiTransaksiTransferPembayaran_vikasirRwi',
                            name: 'txtNoTransaksiTransaksiTransferPembayaran_vikasirRwi',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
							xtype: 'compositefield',
							fieldLabel: 'Tgl. Lahir',
							anchor: '100%',
							width: 130,
							items: 
							[
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'DateTransaksiTransferPembayaran_vikasirRwi',
									name: 'DateTransaksiTransferPembayaran_vikasirRwi',
									width: 130,
									format: 'd/M/Y',
								}
								//-------------- ## --------------				
							]
						},
						//-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Rekam Medis',
                            id: 'txtNoRekamMedisTransaksiTransferPembayaran_vikasirRwi',
                            name: 'txtNoRekamMedisTransaksiTransferPembayaran_vikasirRwi',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nama Pasien',
                            id: 'txtNamaPasienTransaksiTransferPembayaran_vikasirRwi',
                            name: 'txtNamaPasienTransaksiTransferPembayaran_vikasirRwi',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Unit Perawatan',
                            id: 'txtUnitPerawatanTransaksiTransferPembayaran_vikasirRwi',
                            name: 'txtUnitPerawatanTransaksiTransferPembayaran_vikasirRwi',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: '',
                    frame: false,
                    width: 350,
                    height: 125,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jumlah biaya',
                            id: 'txtJumlhBiayaTransaksiTransferPembayaran_vikasirRwi',
                            name: 'txtJumlhBiayaTransaksiTransferPembayaran_vikasirRwi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'PAID',
                            id: 'txtPAIDTransaksiTransferPembayaran_vikasirRwi',
                            name: 'txtPAIDTransaksiTransferPembayaran_vikasirRwi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jmlh dipindahkan',
                            id: 'txtJumlahDipindahkanTransaksiTransferPembayaran_vikasirRwi',
                            name: 'txtJumlahDipindahkanTransaksiTransferPembayaran_vikasirRwi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Saldo Tagihan',
                            id: 'txtSaldoTagihanTransaksiTransferPembayaran_vikasirRwi',
                            name: 'txtSaldoTagihanTransaksiTransferPembayaran_vikasirRwi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: 'Alasan Transfer',
                    frame: false,
                    width: 350,
                    height: 62,
                    items: 
                    [
						{
				            xtype: 'combo',
				            fieldLabel: 'Alsn Transfer',
				            labelSeparator: '',
				        },
						//-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					width: 160,
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnOkTransferPembayaran_vikasirRwi',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
						{
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnCancelTransferPembayaran_vikasirRwi',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
					]
				},
                //-------------- ## --------------
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormTransferPembayaranDataWindowPopup_vikasirRwi;
}
// End Function getFormTransferPembayaranItemEntryGridDataView_vikasirRwi # --------------

// ## END MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

/**
*   Function : FormSetEditTarif_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Edit Tarif
*/

function FormSetEditTarif_vikasirRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryEditTarif_vikasirRwi = new Ext.Window
    (
        {
            id: 'FormGrdEditTarif_vikasirRwi',
            title: 'Edit Tarif',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryEditTarifGridDataView_vikasirRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryEditTarif_vikasirRwi.show();
    mWindowGridLookupEditTarif  = vWinFormEntryEditTarif_vikasirRwi; 
};

// End Function FormSetEditTarif_vikasirRwi # --------------

/**
*   Function : getFormItemEntryEditTarifGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/
function getFormItemEntryEditTarifGridDataView_vikasirRwi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataEditTarifWindowPopup_vikasirRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukEditTarifGridDataView_vikasirRwi(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiEditTarifGridDataView_vikasirRwi(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiEditTarifGridDataView_vikasirRwi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiEditTarifGridDataView_vikasirRwi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiEditTarifGridDataView_vikasirRwi',
                            name: 'txtTarifLamaGridTransaksiEditTarifGridDataView_vikasirRwi',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiEditTarifGridDataView_vikasirRwi',
                            name: 'txtTarifBaruGridTransaksiEditTarifGridDataView_vikasirRwi',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataEditTarifWindowPopup_vikasirRwi;
}
// End Function getFormItemEntryEditTarifGridDataView_vikasirRwi # --------------

/**
*   Function : getItemPanelInputProdukEditTarifGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemPanelInputProdukEditTarifGridDataView_vikasirRwi(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukEditTarifGridDataView_vikasirRwi',
                name: 'txtProdukEditTarifGridDataView_vikasirRwi',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukEditTarifGridDataView_vikasirRwi # --------------

/**
*   Function : getItemGridTransaksiEditTarifGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemGridTransaksiEditTarifGridDataView_vikasirRwi(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridEditTarifDataView_vikasirRwi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiEditTarifGridDataView_vikasirRwi # --------------

/**
*   Function : gridDataViewEditGridEditTarifDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function gridDataViewEditGridEditTarifDataView_vikasirRwi()
{
    
    var FieldGrdKasirGridDataView_vikasirRwi = 
    [
    ];
    
    dsDataGrdJabGridDataView_vikasirRwi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_vikasirRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_vikasirRwi,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridEditTarifDataView_vikasirRwi # --------------

// ## END MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

/**
*   Function : FormSetPostingManual_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan windows popup Posting Manual
*/

function FormSetPostingManual_vikasirRwi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPostingManual_vikasirRwi = new Ext.Window
    (
        {
            id: 'FormGrdPostingManual_vikasirRwi',
            title: 'Posting Manual',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryPostingManualGridDataView_vikasirRwi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPostingManual_vikasirRwi.show();
    mWindowGridLookupPostingManual  = vWinFormEntryPostingManual_vikasirRwi; 
};

// End Function FormSetPostingManual_vikasirRwi # --------------

/**
*   Function : getFormItemEntryPostingManualGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/
function getFormItemEntryPostingManualGridDataView_vikasirRwi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataPostingManualWindowPopup_vikasirRwi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukPostingManualGridDataView_vikasirRwi(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiPostingManualGridDataView_vikasirRwi(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiPostingManualGridDataView_vikasirRwi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiPostingManualGridDataView_vikasirRwi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiPostingManualGridDataView_vikasirRwi',
                            name: 'txtTarifLamaGridTransaksiPostingManualGridDataView_vikasirRwi',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiPostingManualGridDataView_vikasirRwi',
                            name: 'txtTarifBaruGridTransaksiPostingManualGridDataView_vikasirRwi',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataPostingManualWindowPopup_vikasirRwi;
}
// End Function getFormItemEntryPostingManualGridDataView_vikasirRwi # --------------

/**
*   Function : getItemPanelInputProdukPostingManualGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemPanelInputProdukPostingManualGridDataView_vikasirRwi(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukPostingManualGridDataView_vikasirRwi',
                name: 'txtProdukPostingManualGridDataView_vikasirRwi',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukPostingManualGridDataView_vikasirRwi # --------------

/**
*   Function : getItemGridTransaksiPostingManualGridDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemGridTransaksiPostingManualGridDataView_vikasirRwi(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridPostingManualDataView_vikasirRwi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiPostingManualGridDataView_vikasirRwi # --------------

/**
*   Function : gridDataViewEditGridPostingManualDataView_vikasirRwi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function gridDataViewEditGridPostingManualDataView_vikasirRwi()
{
    
    var FieldGrdKasirGridDataView_vikasirRwi = 
    [
    ];
    
    dsDataGrdJabGridDataView_vikasirRwi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_vikasirRwi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_vikasirRwi,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridPostingManualDataView_vikasirRwi # --------------

// ## END MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

// ## END MENU PEMBAYARAN ## ------------------------------------------------------------------

// --------------------------------------- # End Form # ---------------------------------------

// End Project Kasir Rawat Jalan # --------------