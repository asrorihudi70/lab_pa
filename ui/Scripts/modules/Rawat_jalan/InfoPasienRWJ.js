var now = new Date();
var anow=now.format('Y-m-d');
var InfoPasienRWJ={};
var dsunit_viInfoPasienrwj;
InfoPasienRWJ.form={};
InfoPasienRWJ.func={};
InfoPasienRWJ.vars={};
InfoPasienRWJ.func.parent=InfoPasienRWJ;
InfoPasienRWJ.form.DataStore={};
InfoPasienRWJ.form.Grid={};
InfoPasienRWJ.form.Panel={};
InfoPasienRWJ.form.TextField={};
InfoPasienRWJ.form.ComboBox={};
InfoPasienRWJ.form.DateField={};
InfoPasienRWJ.form.Button={};
InfoPasienRWJ.form.DataStore.InfoPasien=new WebApp.DataStore({fields:['TANGGAL','JAM','KD_PASIEN','NAMA_PASIEN','TGL_MASUK','NAMA_UNIT','DOKTER','ALAMAT','TELEPON','OPERATOR','KUNJUNGAN']});

InfoPasienRWJ.func.getId=function(){
	$this=this.parent;
	if($this.vars.genNum == undefined)$this.vars.genNum=0;
	$this.vars.genNum+=1;
	return 'nci-InfoPasien-rj-'+$this.vars.genNum;
};
function mComboUnit_viInfoPasienrwj()
{
	var Field = ['KD_UNIT','NAMA_UNIT'];

    dsunit_viInfoPasienrwj = new WebApp.DataStore({ fields: Field });
    dsunit_viInfoPasienrwj.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				ort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ComboUnitInfoPasienRWJ',
                param: "" //+"~ )"
			}
		}
	);

    $this.form.ComboBox.txtSearch = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirrwjKasir',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Poli ',
		    align: 'Right',
			width: 60,
		    anchor:'100%',
		    store: dsunit_viInfoPasienrwj,
		    valueField: 'KD_UNIT',
		    displayField: 'NAMA_UNIT',
			value:'All',
		    listeners:
			{

			    'select': function(a, b, c)
				{
						  // RefreshDataFilterKasirrwjKasir();

			        //selectStatusCMKasirrwjView = b.data.DEPT_ID;
					//
			    }

			}
		}
	);

    return $this.form.ComboBox.txtSearch;
};
InfoPasienRWJ.func.getInit=function(mod_id){
	$this=this.parent;
	$this.func.getInfoPasien();
	// setInterval(function(){
	// 	$this.func.getInfoPasien();
	// },15000);
	
	
	var grid= new Ext.Panel({
        id			: $this.func.getId(),
        bodyStyle	: 'margin: 5px 5px;padding: 5px 0 0 5px;',
        layout		: 'column',
        border		: true,
        items		:[
			{
				layout		: 'form',
				labelWidth	: 150,
				border		: false,
				items		: [
					$this.form.DateField.caritanggal =new Ext.form.DateField({
						id			: 'tglInfoPasienRWJ',
						fieldLabel	: 'Tanggal',
						width		: 150,
						format		: 'd/M/Y',
						value: anow,
						enableKeyEvents: true,
						listeners	: {
							keyup	: function(a, b,c){
								if(b.getKey() ==13){
									$this.func.getInfoPasien();
								}
							}
						}
					}),
					mComboUnit_viInfoPasienrwj(),		
				]
			},{
				layout		: 'form',
				labelWidth	: 150,
				border		: false,
				bodyStyle 	: "margin-left : 10px; margin-right : 10px;",
				items		: [
					$this.form.TextField.CariMedrec =new Ext.form.TextField({
						fieldLabel	: 'Medrec',
						width		: 150,
						enableKeyEvents: true,
						listeners	: {
							keyup	: function(a, b,c){
								if(b.getKey() == 13){
									$this.func.getInfoPasien();
								}
							}
						}
					}),	$this.form.TextField.CariNama = new Ext.form.TextField({
						fieldLabel	: 'Nama Pasien',
						width		: 150,
						enableKeyEvents: true,
						listeners	: {
							keyup	: function(a, b,c){
								if(b.getKey() == 13){
									$this.func.getInfoPasien();
								}
							}
						}
					}),	
				]
			},
			$this.form.Button.btnSearch = new Ext.Button({
				text	: 'Cari',
				id		: $this.func.getId(),
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					$this.func.getInfoPasien();
				}
			}),
        ]
    });
	this.parent.form.Panel.InfoPasien= new Ext.Panel({
	    id			: mod_id,
	    closable	: true,
        layout		: 'form',
        title		: 'Info Pasien ',
        border		: false,
        shadhow		: true,
        iconCls		: 'Request',
        margins		: '0 5 5 0',
        autoScroll	: false,
	    items		:[
 		  	grid,
 		  	$this.func.getGridMain()
	    ]
	});
	return this.parent.form.Panel.InfoPasien;
};

InfoPasienRWJ.func.getGridMain=function(){
	$this=this.parent;
    $this.form.Grid.InfoPasien	= new Ext.grid.EditorGridPanel({
        title		: 'Info Pasien',
		id			: $this.func.getId(),
		stripeRows	: true,
//		height		: 130,
        store		: $this.form.DataStore.InfoPasien,
        border		: false,
        frame		: false,
        anchor		: '100% 100%',
        autoScroll	: true,
        cm			: new Ext.grid.ColumnModel([
                  new Ext.grid.RowNumberer({width: 40}),
            {
    			id			: $this.func.getId(),
            	header		: 'Tanggal',
                dataIndex	: 'TGL_MASUK',
                width		: 80,
    			menuDisabled: true,
                hidden		: false
            },/* {
    			id			: $this.func.getId(),
            	header		: 'Jam',
                dataIndex	: 'JAM',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            }, */{
            	id			: $this.func.getId(),
            	header		: 'Kode Pasien',
                dataIndex	: 'KD_PASIEN',
                width		: 80,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Nama Pasien',
                dataIndex	: 'NAMA_PASIEN',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Unit',
                dataIndex	: 'NAMA_UNIT',
                width		: 125,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Dokter',
                dataIndex	: 'DOKTER',
                width		: 125,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Telepon',
                dataIndex	: 'TELEPON',
                width		: 100,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Alamat',
                dataIndex	: 'ALAMAT',
                width		: 150,
    			menuDisabled: true,
                hidden		: false
            },{
            	id			: $this.func.getId(),
            	header		: 'Petugas RM',
                dataIndex	: 'OPERATOR',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Kunjungan',
                dataIndex	: 'KUNJUNGAN',
                width		: 75,
    			menuDisabled: true,
                hidden		: false,
            },
       ]),
        viewConfig	: {forceFit: true}
    });
    
    return $this.form.Grid.InfoPasien;
};

InfoPasienRWJ.func.getInfoPasien=function(tglmasuk){
	$this=this.parent;
	var txtSearch='';
	var cariTanggal=anow;
	if($this.form.ComboBox.txtSearch != undefined){
		txtSearch=$this.form.ComboBox.txtSearch.getValue();
	}
	if($this.form.DateField.caritanggal != undefined){
		cariTanggal=$this.form.DateField.caritanggal.getValue().format('Y-m-d');
		console.log(cariTanggal);
	}
	
	if (txtSearch === 'All' || txtSearch === '')
	{
		var criteria='tgl_masuk=~'+cariTanggal+'~ and left(kd_unit,1)=~'+2+'~';
	}
	else
	{
		var criteria='tgl_masuk=~'+cariTanggal+'~ and kd_unit=~'+txtSearch+'~';
	}	
	/*
	
	var tmpNoMedrec = Ext.get('txtNoMedrecD_vidaftarRWI').getValue()
	if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
	{
	if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
	{
	var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrecD_vidaftarRWI').getValue())
	Ext.getCmp('txtNoMedrecD_vidaftarRWI').setValue(tmpgetNoMedrec);
	 */
	// console.log(Ext.get('tglInfoPasienRWJ'));
	// console.log(anow);
	
	// console.log($this.form.TextField.CariNama);
	if ($this.form.TextField.CariMedrec != undefined) {
		if ($this.form.TextField.CariMedrec.getValue().length !== 0 && $this.form.TextField.CariMedrec.getValue().length < 10){
			// var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrecD_vidaftarRWI').getValue())
			$this.form.TextField.CariMedrec.setValue(formatnomedrec($this.form.TextField.CariMedrec.getValue()));
			criteria += " and kd_pasien = '"+$this.form.TextField.CariMedrec.getValue()+"' ";
			console.log(criteria);
		}
	}

	if ($this.form.TextField.CariNama != undefined) {
		if ($this.form.TextField.CariNama.getValue() != "") {
			criteria += " and lower(nama_pasien) like lower('%"+$this.form.TextField.CariNama.getValue()+"%') ";
		}
	}

	InfoPasienRWJ.form.DataStore.InfoPasien.load({ 
		params: { 
			Skip: 0, 
			Take: 0, 
            Sort: 'kd_penyakit',
			Sortdir: 'ASC', 
			target:'ViewGridInfoPasienRWJ',
			param: criteria
		} 
	});
	//
};

CurrentPage.page = InfoPasienRWJ.func.getInit(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
console.log(InfoPasienRWJ);